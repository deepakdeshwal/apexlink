/* initialisation on load */

jQuery(document).ready(function () {

    var id =  getParameterByName('id');

    /*initaiate jquery tables*/
        jqGridListBuilding('All', true, id);

    /*initaiate jquery tables*/

    fetchAllUnittypes(false);


    var status =  localStorage.getItem("active_inactive_status");

    if(status !== undefined) {
        if ($("#building-table-edit")[0].grid) {
            $('#building-table-edit').jqGrid('GridUnload');
        }
        //intializing jqGrid
        if(status == 'all'){
            jqGridListBuilding('All', true,id);
        }  else {
            jqGridListBuilding(status, true,id);
        }
        $('#jqGridStatus option[value='+status+']').attr("selected", "selected");

    }else{
        if ($("#properttype-table")[0].grid) {
            $('#properttype-table').jqGrid('GridUnload');
        }
        jqGridListBuilding('All', true,id);
    }


    //jqGrid status
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        $('#building-table-edit').jqGrid('GridUnload');
        changeGridStatus(selected);
        jqGridListBuilding(selected, true,id);

    });


    // getPropertyDetail(id)
    $("#property_id").val(id)

    function getParameterByName( name ){
        var regexS = "[\\?&]"+name+"=([^&#]*)",
            regex = new RegExp( regexS ),
            results = regex.exec( window.location.search );
        if( results == null ){
            return "";
        } else{
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }



    $(document).on("change", ".key_access_codes_list", function () {

        var value = $(this).val();
        if (parseInt(value) >= 1) {
            $(this).parent().find('.key_access_codeclass').show();
            $(this).parent().find('.key_access_codeclass').val(' ');
            console.log(value);
        } else {
            $(this).parent().find('.key_access_codeclass').hide();
            console.log('select');
        }
    });

    $(document).on("click", "#NewkeyMinus", function () {
        $(this).parents('#key_optionsDiv').remove();
    });
    //jquery grid for property insurance
    function jqGridListBuilding(status, deleted_at,id) {
        var table = 'building_detail';
        var columns = ['Building Name', 'Building Id', 'No of Units', 'Status', 'Action'];
        var select_column = ['Edit', 'Delete'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [{column: 'property_id', value: id, condition: '='}];
        var extra_columns = [''];
        var columns_options = [
            {name: 'Building Name', index: 'building_name', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Building Id', index: 'building_id', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'No of Units', index: 'no_of_units', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Status', index: 'status', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionBuildingFmatter,title:false}
        ];
        var ignore_array = [];
        jQuery("#building-table-edit").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height:'100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "building_detail",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at: deleted_at,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname:'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Buildings",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}

            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }

    /**
     * jqGrid function to format action column
     * @param status
     */

    function actionBuildingFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var editable = $(cellvalue).attr('editable');
            var select = '';
            if(rowObject.Status == 1)  select = ['<span><i class="fa fa-edit" data_id="' + rowObject.id + '" style="font-size:24px"></i></span>','<span></span><i data_id="' + rowObject.id + '" class="fa fa-check" style="font-size:24px" aria-hidden="true"></i></span>'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['<span><i data_id="' + rowObject.id + '" class="fa fa-edit" style="font-size:24px"></i></span>','<i data_id="' + rowObject.id + '" style="font-size:24px" class="fa fa-window-close" aria-hidden="true"></i>'];
            var data = '';
            if(select != '') {
                $.each(select, function (key, val) {
                    if(editable == '0' && (val == 'delete' || val == 'Delete')){
                        return true;
                    }
                    data += val
                });
            }
            return data;
        }
    }



    //add announcement
    $("#add_building").validate({
        rules: { building_id: {
                required: true
            },building_name: {
                required: true
            },address: {
                required: true
            },address: {
                required: true
            },total_keys: {
                number: true
            },no_of_units: {
                required: true,
                number: true,
                min: 1
            },unit_type: {
                required: true,
            },base_rent: {
                required: true,
                number: true,
            },market_rent: {
                required: true,
                number: true,
            },security_deposit: {
                required: true,
                number: true,
            },

        },
        messages: {
            property_type: {
                required: "* This field is required",
            },
            no_of_units: {
                min: "* Minimum value is 1",
            }
        },
        submitHandler: function (event) {


            event.preventDefault();
            var uploadform = new FormData();
            var formData = $('#add_building').serialize();
            var form_type = $("#form_type").val()
            var action = '';
            var custom_field = [];
            combine__data_photo_document_array=[];
            combine__data_photo_document_array=[];
            $(".custom_field_html input").each(function(){
                var data = {'name':$(this).attr('name'),'value':$(this).val(),'id':$(this).attr('data_id'),'is_required':$(this).attr('data_required'),'data_type':$(this).attr('data_type'),'default_value':$(this).attr('data_value')};
                custom_field.push(data);
            });
           var  combine_photo_document_array =[];
            var length = $('#photo_video_uploads > div').length;

            // console.log(combine_photo_document_array);
            // console.log(file_library); return false;
            combine_photo_document_array =  $.merge( photo_videos, file_library );
            var dataaa = Array.from(new Set(combine_photo_document_array)); // #=> ["foo", "bar"]

            if(length > 0) {

                var data = convertSerializeDatatoArrayPhotos();
                var data1 = convertSerializeDatatoArray();
                var combine__data_photo_document_array =  $.merge( data, data1 );
                // var count = photo_videos.length;

                $.each(dataaa, function (key, value) {
                    if(compareArray(value,combine__data_photo_document_array) == 'true'){
                        uploadform.append(key, value);
                    }
                    // if(key+1 === count){
                    //     savePhotoVideos(uploadform);
                    // }
                });
            }

            // var length1 = $('#file_library_uploads > div').length;
            // if(length1 > 0) {
            //     var data1 = convertSerializeDatatoArray();
            //     var count1 = file_library.length;
            //     $.each(file_library, function (key, value) {
            //         if(compareArray(value,data1) == 'true'){
            //             uploadform.append(key, value);
            //         }
            //         // if(key+1 === count){
            //         //     saveLibraryFiles(uploadform);
            //         // }
            //     });
            // } else {
            //
            // }


            uploadform.append('class', "buildingDetail");
            uploadform.append('action', "addBuilding");
            uploadform.append('custom_field', JSON.stringify(custom_field));
            uploadform.append('form', formData);

            $.ajax
            ({
                type: 'post',
                url: '/Building/AddBuildingDetail',
                processData: false,
                contentType: false,
                data: uploadform,
                beforeSend: function(xhr) {
                    // checking custom field validations
                    $(".custom_field_html input").each(function() {
                        var res = validateCustomField($(this).val(), $(this).attr('data_required'), $(this).attr('data_type'), this, this.id);
                        if(res === false) {
                            xhr.abort();
                            return false;
                        }
                    });
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){

                        localStorage.setItem("Message",response.message)
                        localStorage.setItem("rowcolor",true)
                       window.location.reload() ;
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }else{
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });

    function compareArray(data,compare){
        for(var i =0;i < compare.length;i++){
            if(compare[i].name == data['name'] && compare[i].size == data['size']){
                return 'true';
            }
        }
        return 'false';
    }

    $(".grey-btn").click(function () {
        let box = $(this).closest('.add-popup');
        $(this).closest('.add-popup').find('input[type=text]').val('');
        $(box).hide();


    });

    $(document).on("click","#addnewkey",function(){
        $("#add_key_tracker").show(500);
        $("#track_key_tracker").hide(500);
    });

    $(document).on("click","#addtrackkey",function(){
        $("#add_key_tracker").hide(500);
        $("#track_key_tracker").show(500);
    });

    $('input[type=radio]').change(function() {
        if (this.value == 'add_key') {
            $("#addnewkey").show();
            $("#addtrackkey").hide();
            $("#add_key_tracker").hide(500);
            $("#track_key_tracker").hide(500);
        }
        else if (this.value == 'track_key') {
            $("#addnewkey").hide();
            $("#addtrackkey").show();
            $("#add_key_tracker").hide(500);
            $("#track_key_tracker").hide(500);
        }
    });

    /*for multiple phone number textbox*/
    $(document).on("click", "#multiplephoneno", function () {
        var clone = $(".multiplephonediv:first").clone().find("input:text").val("").end();
        var divlength = $(".multiplephonediv").clone();
        if (divlength.length <= 2) {
            $(".multiplephonediv").last().after(clone);
            $(".multiplephonediv:not(:eq(0))  #multiplephoneno").remove();
            $(".multiplephonediv:not(:eq(0))  #multiplephonenocross").show();
        }
        if (divlength.length == 2) {
            $(this).hide();
        }
    });
    /*remove multiple phone number textbox*/
    $(document).on("click", "#multiplephonenocross", function () {
        $(this).parents('.multiplephonediv').remove();
        $("#multiplephoneno").show();

    });

    // $(function() {
    //     var regExp = /[a-z]/i;
    //     $(document).on('keydown keyup','.p-number-format', function(e) {
    //         var value = String.fromCharCode(e.which) || e.key;
    //         // No letters
    //         if (regExp.test(value)) {
    //             e.preventDefault();
    //             return false;
    //         }else{
    //             return true;
    //         }
    //     });
    // });
    setTimeout(function(){
    var divlength = $(".multiplefaxdiv")
    if (divlength.length > 2) {
        $(".multiplefaxdiv:not(:eq(0))  #multiplefaxno").remove();
        $(".multiplefaxdiv:not(:eq(0))  #multiplefaxnocross").show();
    }
    if (divlength.length == 3) {
        $(".multiplefaxdiv:eq(0)  #multiplefaxno").hide();
    }

        var divlength = $(".multiplephonediv");
        if (divlength.length > 2) {
            $(".multiplephonediv:not(:eq(0))  #multiplephoneno").remove();
            $(".multiplephonediv:not(:eq(0))  #multiplephonenocross").show();
        }
        if (divlength.length == 3) {
            $(".multiplephonediv:eq(0)  #multiplephoneno").hide();
        }

    }, 400);

    /*for multiple fax number textbox*/
    $(document).on("click", "#multiplefaxno", function () {
        var clone = $(".multiplefaxdiv:first").clone().find("input:text").val("").end();;
        var divlength = $(".multiplefaxdiv").clone();
        if (divlength.length <= 2) {
            $(".multiplefaxdiv").last().after(clone);
            $(".multiplefaxdiv:not(:eq(0))  #multiplefaxno").remove();
            $(".multiplefaxdiv:not(:eq(0))  #multiplefaxnocross").show();
        }
        if (divlength.length == 2) {
            $(this).hide();
        }
    });
    /*remove multiple phone number textbox*/
    $(document).on("click", "#multiplefaxnocross", function () {
        $(this).parents('.multiplefaxdiv').remove();
        $("#multiplefaxno").show();

    });

    $("input[name='last_renovation_date']").datepicker({
        dateFormat: date_format,
        autoclose: true,
        changeMonth: true,
        changeYear: true
    });
    //open a modal
    $(document).on('click', '#newRenovation', function (e) {
        $("#lastRenovationModal").modal('show');
        $("input[name='new_renovation_date']").datepicker({
            dateFormat: date_format,
            autoclose: true,
            changeMonth: true,
            changeYear: true
        });
        $("input[name='new_renovation_time']").timepicker({ timeFormat: 'h:mm p',defaultTime: new Date()});
    })


    $("#no_of_units").val(1);

    // jQuery(document).on("keyup keydown",'.p-number-format',function() {
    //     $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
    // });

    jQuery(document).on("click",'#no_seprate_unit_checkbox',function() {
        var no_of_units = $("#no_of_units").val();
        if (no_of_units == 1) {
            if ($(this).is(":checked"))
                $("#no_seprate_unit").show(500);
            else
                $("#no_seprate_unit").hide(500)
        } else {
            toastr.error("Number of units must be equal to 1");
            return false;
        }


    });

    jQuery(document).on("click",'#selectAllamennity input[type=checkbox]',function() {
        if ($(this).is(":checked") && $(this).val()== 'selectall')
            $('.inputAllamenity').prop('checked', true);
        else
            $('.inputAllamenity').prop('checked', false);

    });

    jQuery(document).on("keyup keydown",'#no_of_units',function() {
        if ($("#no_seprate_unit_checkbox").is(":checked") && $(this).val() > 1){
            $(this).val(1)
        }
    });

    $(document).on('blur','#base_rent', function(e) {
        var base_rent = parseInt($(this).val());
        $("#security_deposit").val(base_rent.toFixed(2));
        $("#base_rent").val(base_rent.toFixed(2));

    });

    $(document).on('click','#add_building_cancel',function(){
        bootbox.confirm("Do you want to cancel this action now ?", function (result) {
            if (result == true) {
                window.location.href = '/Building/BuildingModule/?id='+id;
            }
        });
    });




});
function fetchAllUnittypes(id) {


    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllUnittypes'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#unit_type').html(res.data);
            if (id != false) {
                $('#unit_type').val(id);

            }
            $("#default_rent").val(response.default_rent);

        },
    });

}


function validateCustomField(data_value, data_required, arg, element, id) {
    var msg = '';
    if (data_value == "") {
        if (data_required == 1) {
            msg = 'This field is required';
            $('#' + id).after("<span>" + msg + "</span>");
            return false;
        }
    }
    if (arg == 'text') {
        if (data_value.length > 150) {
            msg = 'Please enter character less than 150';
            $('#' + id).next('.customError').text(msg);
            return false;
        }

    } else if (arg == 'number') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            $('#' + id).next('.customError').text(msg);
            return false;
        } else {
            if (data_value > 20) {
                msg = 'Please enter less than 20';
                $('#' + id).next('.customError').text(msg);
                return false;
            }
        }
    } else if (arg == 'currency') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            console.log(msg);
            $('#' + id).next('.customError').text(msg);
            return false;
        }

    } else if (arg == 'percentage') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            console.log(msg);
            $('#' + id).next('.customError').text(msg);
            return false;
        } else {
            if (data_value > 100) {
                msg = 'Maximum value is 100';
                $('#' + id).next('.customError').text(msg);
                return false;
            }
        }
    } else if (arg == 'url') {
        var expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm;
        var regex = new RegExp(expression);
        if (data_value.match(regex)) {
        } else {
            msg = 'Please enter a valid URL';
            $('#' + id).next('.customError').text(msg);
            return false;
        }
    }
    $('#' + id).next('.customError').text('');
    return true;
}


