/*inspection area add*/


jQuery(document).ready(function($){

    var id = getParameterByName('id');

    var inspection_value = localStorage.getItem("inspection_value");
    if(inspection_value == "edit_inspection") {
        getInspectionListView();
        editInspectionareaView();
        editInspectionData();

        $("#main-contentId .editInspectionDetails").css("display","block");
        $("#main-contentId .addInspectionDetails").css("display","none");
    }else {
        localStorage.removeItem("inspection_value");
        $("#main-contentId .editInspectionDetails").css("display","none");
        $("#main-contentId .addInspectionDetails").css("display","block");
    }

    function getInspectionListView() {
        var id = $(".main-tabs #addInspectionid").val();
      InspectionData(id);
    }



    function editInspectionareaView() {
        var id = $(".main-tabs #addInspectionid").val();
        $.ajax({
            type: 'POST',
            url: '/Building/AddBuildingDetail',
            data: {
                class: "buildingDetail", action: "editInspectionareaView", id: id},
            success: function (response) {
                var response = JSON.parse(response);

                if (response.code == 200) {
                    if(response.data.inspection_area[0] !== ""){
                        /*$(".check-input-outer input[value='" + response.data.inspection_area[0].name + "']").prop('checked', true);*/
                        setTimeout(function(){
                            $("#editInspectionDetails .check-input-outer input:checkbox[value='" + response.data.inspection_area[0].name + "']").prop('checked', true);

                            if (response.data.inspection_area[0].rating == '1'){
                                var inspection_rating = "A";
                            }else if (response.data.inspection_area[0].rating == '2'){
                                var inspection_rating = "B";
                            }else if(response.data.inspection_area[0].rating == '3'){
                                var inspection_rating = "C";
                            }
                            $("#editInspectionDetails .check-input-outer input:checkbox[value='" + response.data.inspection_area[0].name + "']").parent().parent().next().find("select option").text(inspection_rating);

                            if(response.data.inspection_area[0].subInspection.length > 0){
                            for (var i = 0; i < response.data.inspection_area[0].subInspection.length; i++) {
                                $(".inner-checkbox-cls .check-input-outer input:checkbox[value='" + response.data.inspection_area[0].subInspection[i].name + "']").prop('checked', true);
                                $(".inner-checkbox-cls .check-input-outer input:checkbox[value='" + response.data.inspection_area[0].subInspection[i].name + "']").prop('disabled', false);

                                if (response.data.inspection_area[0].subInspection[i].rating == '1'){
                                    var inspection_rating1 = "A";
                                }else if (response.data.inspection_area[0].subInspection[i].rating == '2'){
                                    var inspection_rating1 = "B";
                                }else if(response.data.inspection_area[0].subInspection[i].rating == '3'){
                                    var inspection_rating1 = "C";
                                }

                                 $("#editInspectionDetails .inner-checkbox-cls .check-input-outer input:checkbox[value='" + response.data.inspection_area[0].subInspection[i].name + "']").parent().parent().next().find("select option").text(inspection_rating1);
                            }
                        }
                    }, 500);

                }
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}


    /**
     * jqGrid Intialization function
     * @param status
     */
    jqGridPhotovideos('All');
    function jqGridPhotovideos(status) {
        var table = 'inspection_photos';
        var columns = ['Name','Preview','Action'];
        var select_column = ['Delete'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['inspection_photos.module_type'];
        var extra_where = [];
        var columns_options = [
            {name:'Name',index:'file_name',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Preview',index:'file_location',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:photoFormatter},
            {name:'Action',index:'select',width:430,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,formatter: 'select', edittype: 'select',search:false,table:table,title:true}
        ];
        var ignore_array = [];
        jQuery("#inspectionPhotos-table").jqGrid({
            url: '/Companies/List/jqgrid',
            datatype: "json",
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            sortname: 'updated_at',
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns:extra_columns,
                extra_where:extra_where
            },
            viewrecords: true,
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Photos/Virtual Tour Videos",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, /* edit options*/
            {}, /* add options*/
            {}, /*del options*/
            {top:10,left:200,drag:true,resize:false} /* search options*/
        );
    }


    /**
     * function to format photos and videos
     * @param cellvalue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function photoFormatter(cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var path = upload_url+'company/'+rowObject.Preview;
            var appendType = '<a href="' + path + '" target="_blank"><img width=100 height=100 src="' + path + '"></a>';
            return appendType;
        }
    }

    /*Delete row data from select option*/
    $(document).on('change', '#inspectionPhotos-table .select_options', function() {

        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index() ;
        if(opt == 'View Details' || opt == 'VIEW DETAILS'){

        } else if (opt == 'Delete' || opt == 'DELETE') {
            opt = opt.toLowerCase();
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/Building/AddBuildingDetail',
                            data: {class: 'buildingDetail', action: "deleteFileInspection", id: id},
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                    $('#inspectionPhotos-table').trigger('reloadGrid');
                }
            });
        }else if(opt == 'Edit' || opt == 'edit'){
            /*window.location.href = '/Inspection/InspectionView?id='+id;*/
        }
    });





    /**
     * Get Parameters by id
     * @param status
     */
    function getParameterByName(name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

    $("#building_id").val(id);

    $("#addPropertyInspection #modelInspectionarea ").on('click', '.inspection-outer input', function (e) {

        if ($(this).prop("checked") == true) {
            $(this).parents(".inspection-sub-child").find(".inner-checkbox-cls input[type='checkbox']").prop("disabled",false);
        }else{
            $(this).parents(".inspection-sub-child").find(".inner-checkbox-cls input[type='checkbox']").prop("disabled",true);
        }
    });

    /* Select Inspection area checkbox and select option */
    getInspectionCheckValue();
    function getInspectionCheckValue() {
        $("#addPropertyInspection #modelInspectionarea ").on('click', '.inspection-outer input', function (e) {
            if ($(this).prop("checked") == true) {
                if ($(this).parent().parent().next().find("select").prop("option:selected") == false) {
                    toastr.warning("Please select the rating");
                }
            }
        });

        $("#addPropertyInspection #modelInspectionarea ").on('change', '.inspection-sub-child select', function (e) {
            if ($(this).parent().prev().find("input").prop("checked") == false) {
                toastr.warning("Please check the checkbox");
            }

        });
    }

    /* Select Inspection area checkbox and select option */
    var building_id = $(".main-content #building_id").val();

    /*add custom field button*/
    $(document).on('click','#buildingInspection,#buildingInspection1',function() {
        $('#addInspectionAreaForm').trigger("reset");

    });

    $("#addInspectionAreaForm").validate({
        rules: {
            inspection_area_name: {
                required:true
            },
        },

    });
    $("#addInspectionAreaForm").on("submit", function (e) {
      if($("#addInspectionAreaForm").valid()){
        var id = $("#addInspectionAreaForm #object_id").val();
        e.preventDefault();
        var formData = $('#addInspectionAreaForm').serializeArray();

        $.ajax({
            type: 'post',
            url: '/Building/AddBuildingDetail',
            data: {
                form: formData,
                class: 'buildingDetail',
                action: 'addInspectionAreaModule',
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);

                html="";
                if (response.status == 'success' && response.code == 200) {

                    var html = '<div class="inspection-sub-child">\n' +
                        '<div class="col-sm-12">\n' +
                        '<div class="row">\n' +
                        '<div class="col-sm-3">\n' +
                        '<div class="check-input-outer check-outer">\n' +
                        '<input type="checkbox" value=""><label>' + response.data.parent.inspection_area + '</label>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '<div class="col-xs-1">\n' +
                        '<select id="rating" name="" class="form-control">\n' +
                        '<option value="1">A</option>\n' +
                        '<option value="2">B</option>\n' +
                        '<option value="3">C</option>\n' +
                        '</select>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</div>';

                if(response.data.child != undefined){
                    if(response.data.child.length > 0){
                        for (var i = 0; i < response.data.child.length; i++) {
                            html += '<div class="col-sm-6 inner-checkbox-cls">\n' +
                                '<div class="col-sm-3">\n' +
                                '<div class="check-input-outer check-outer">\n' +
                                '<input type="checkbox" disabled="disabled"><label>' + response.data.child[i].inspection_area + '</label>\n' +
                                '</div>\n' +
                                '</div>\n' +
                                '<div class="col-xs-2">\n' +
                                '<select id="rating" name="" class="form-control">\n' +
                                '<option value="1">A</option>\n' +
                                '<option value="2">B</option>\n' +
                                '<option value="3">C</option>\n' +
                                '</select>\n' +
                                '</div>\n' +
                                '</div>';
                            if(response.data.child.length == 0){
                                html +='</div>';
                            }
                        }

                    }else{
                        html += '<div class="col-sm-6 inner-checkbox-cls">\n' +
                            '<div class="col-sm-3">\n' +
                            '<div class="check-input-outer check-outer">\n' +
                            '<input type="checkbox" disabled="disabled"><label>' + response.data.child.inspection_area + '</label>\n' +
                            '</div>\n' +
                            '</div>\n' +
                            '<div class="col-xs-2">\n' +
                            '<select id="rating" name="" class="form-control">\n' +
                            '<option value="1">A</option>\n' +
                            '<option value="2">B</option>\n' +
                            '<option value="3">C</option>\n' +
                            '</select>\n' +
                            '</div>\n' +
                            '</div>';
                    }

                }
                    html +='</div>';

                    $('#modelInspectionarea .inspection-outer').append(html);
                    $('#modelInspectionarea1 .inspection-outer').append(html);
                    $('#AddNewInspectionModal').modal('hide');
                    $('#AddNewInspectionModal').trigger("reset");
                    window.location.reload();
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
   }
    });

    getInspectionfield();
    function getInspectionfield(){
        $.ajax({
            type: 'get',
            url: '/Building/AddBuildingDetail',
            data: {
                class: "buildingDetail",
                action: "viewInspectionArea",
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log('check here u r >>',response);
                if(response.status == 'success' && response.code == 200){
                    $('#modelInspectionarea .inspection-outer').append(response.data);
                    $('#modelInspectionarea1 .inspection-outer').append(response.data);
                }else{
                    toastr.error(response.message);
                }


            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    /* Photo Upload Start */
    var photo_videos = [];
    $(document).on('change','#photosVideos',function(){
        photo_videos = [];
        var fileData = this.files;
        $.each(this.files, function (key, value) {
            var type = value['type'];
            var size = isa_convert_bytes_to_specified(value['size'], 'k');
            var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
            var validVideoTypes = ['video/mp4','video/avi'];
            var uploadType = ($.inArray(type, validImageTypes) < 0)? 'video':'image';
            var validSize = (uploadType == 'video')? '5120':'1030';
            var arrayType = (uploadType == 'video')? validVideoTypes:validImageTypes;
            if(uploadType == 'video' && fileData.length > 1) {
                toastr.warning('Please select one Video to upload!');
                return false;
            }
            if ($.inArray(type, arrayType) < 0) {
                toastr.warning('Please select file with valid extension only!');
            } else {
                if (parseInt(size) > validSize) {
                    var validMb = (validSize == 5120)? 5 : 1;
                    toastr.warning('Please select documents less than '+validMb+' mb!');
                } else {
                    size = isa_convert_bytes_to_specified(value['size'], 'k') + 'kb';
                    photo_videos.push(value);
                    var src = '';
                    var reader = new FileReader();
                    $('#photo_video_uploads,#photo_video_uploads1').html('');
                    reader.onload = function (e) {
                        src = e.target.result;
                        if(uploadType == 'image'){
                            var appendType = '<img class="img-upload-tab' + key + '" width=100 height=100 src=' + src + '>';
                        } else {
                            var appendType = '<video class="img-upload-tab' + key + '" width=240 height=140 controls>' +
                                '<source src="'+src+'" type="'+type+'">' +
                                '</video>';
                        }
                        $("#photo_video_uploads,#photo_video_uploads1").append(
                            '<div class="row" style="margin:20px">' +
                            '<div class=" img-upload-library-div">' +
                            '<div class="col-sm-3">'+appendType+'</div>' +
                            '<div class="col-sm-3 show-library-list-imgs-name' + key + '">' + value['name'] + '</div>' +
                            '<input type="hidden" class="photoVideoInput" name="photoVideoName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                            '<div class="col-sm-2 show-library-list-imgs-size' + key + '">' + size + '</div>' +
                            '<div class="col-sm-2 show-library-list-imgs-checkboc' + key + '">' +
                            /*'<input type="checkbox" id="checkbox'+value['size']+'" name="maintananceSiteCheckbox' + key + '"><br>'+
                            '<span style="font-size:small;color:red;">Display this Photo/Virtual Tour Video on Marketing Page</span></div>' +*/
                            '<div class="col-sm-2"><span id=' + key + ' class="delete_pro_img cursor"><a href="javascript:;"> <input type="button" class="orange-btn" value="Delete"></a></span></div></div></div>');
                    };
                    reader.readAsDataURL(value);
                }
            }
        });
    });


    $(document).on('click','.delete_pro_img',function(){
        toastr.success('The record deleted successfully.');
        $(this).parent().parent().parent().parent('.row').remove();
    });

    $(document).on('click','#remove_library_file',function(){
        bootbox.confirm("Do you want to remove all files?", function (result) {
            if (result == true) {
                toastr.success('The record deleted successfully.');
                $('#photo_video_uploads,#photo_video_uploads1').html('');
                $('#file_library').val('');
            }
        });

    });

    function isa_convert_bytes_to_specified(bytes, to) {
        var formulas =[];
        formulas['k']= (bytes / 1024).toFixed(1);
        formulas['M']= (bytes / 1048576).toFixed(1);
        formulas['G']= (bytes / 1073741824).toFixed(1);
        return formulas[to];
    }
    /* Photo Upload End */

    /*Get Inspection Name List*/
    getInspectionNameList();
    function getInspectionNameList(){
        $.ajax({
            type: 'get',
            url: '/Building/AddBuildingDetail',
            data: {
                class: "buildingDetail",
                action: "getInspectionNameList",
            },
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    for(var i=0; i<response.data.length ; i++){
                        var html='<option value="'+ response.data[i].id +'">'+ response.data[i].inspection_name +'</option>';
                        $("#addPropertyInspection .inspectionNmelist").append(html);
                        $("#editInspectionDetails .inspectionNmelist").append(html);
                    }

                }else{
                    toastr.error(response.message);
                }


            },
            error: function (data) {
                console.log(data);
            }
        });
    }


    /*Get Property/Building Details Name List*/
    getPropertyBuildingDetails();
    function getPropertyBuildingDetails(){
        var id = $(".main-content #property_id").val();
        var id1 = $(".main-content #building_id").val();
        $.ajax({
            type: 'POST',
            url: '/Building/AddBuildingDetail',
            data: {
                class: "buildingDetail",
                action: "getPropertyBuildingDetails",
                id:id,
                id1:id1
            },
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    $("#addPropertyInspection  #property_name").val(response.data.property_name);
                    $("#addPropertyInspection  #property_address").val(response.data.address1);
                    $("#addPropertyInspection  #building_name").val(response.data.building_name);
                    $("#addPropertyInspection  #building_address").val(response.data.address);
                }else{
                    toastr.error(response.message);
                }


            },
            error: function (data) {
                console.log(data);
            }
        });
    }
    /*Get Property/Building Details Name List*/
    function convertSerializeDatatoArrayPhotos(){
        var newData = [];
        $(".photoVideoInput").each(function( index ) {
            var name = $(this).val();
            var size = $(this).attr('data_id');
            var id = $("#checkbox"+size);
            var checked = id.prop('checked')?'1':'0';
            newData.push({'name':name,'size':size,'siteMain':checked});
        });
        return newData;
    }


    $("#addPropertyInspection").validate({
        rules: {
            inspection_type: {
                required: true
            },
            inspectionNmelist: {
                required: true
            },


        },
        messages: {
            inspection_type: {
                required: "* This field is required",
            },
            inspectionNmelist: {
                min: "* This field is required",
            }
        },
        submitHandler: function (form) {

            var uploadform = new FormData();
            var formData = $('#addPropertyInspection').serialize();
            var form_type = $("#form_type").val()
            var action = '';
            var custom_field = [];
            var building_id = $(".main-content #building_id").val();
            combine__data_photo_document_array=[];
            /*combine__data_photo_document_array=[];*/
            $(".custom_field_html input").each(function(){
                var data = {'name':$(this).attr('name'),'value':$(this).val(),'id':$(this).attr('data_id'),'is_required':$(this).attr('data_required'),'data_type':$(this).attr('data_type'),'default_value':$(this).attr('data_value')};
                custom_field.push(data);
            });
            var  combine_photo_document_array =[];
            var length = $('#photo_video_uploads > div').length;

            if(length > 0) {
                var data = convertSerializeDatatoArrayPhotos();
                $.each(photo_videos, function (key, value) {
                    if(compareArray(value,data) == 'true'){
                        uploadform.append(key, value);
                    }
                });
            }

            var ckb_status = $("#modelInspectionarea .inspection-outer .inspection-sub-child .check-input-outer .checkboxInspection").prop('checked');
            if(ckb_status === false){
                bootbox.alert("Please select atleast one inspection area");
            }



            uploadform.append('class', "buildingDetail");
            uploadform.append('action', "addInspectionData");
            uploadform.append('custom_field', JSON.stringify(custom_field));
            uploadform.append('form', formData);
            uploadform.append('building_id', building_id);
            uploadform.append('property_id', property_id);

            $.ajax
            ({
                type: 'post',
                url: '/Building/AddBuildingDetail',
                processData: false,
                contentType: false,
                data: uploadform,
                beforeSend: function(xhr) {
                    $(".custom_field_html input").each(function() {
                        var res = validateCustomField($(this).val(), $(this).attr('data_required'), $(this).attr('data_type'), this, this.id);
                        if(res === false) {
                            xhr.abort();
                            return false;
                        }
                    });
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        var base_url = window.location.origin;
                        localStorage.setItem("Message",response.message)
                        localStorage.setItem("rowcolor",true);
                        localStorage.removeItem('addInspectionDetails');
                        window.location.href = base_url + "/Inspection/Inspection";
                        $("#inspection-table").trigger('reloadGrid');
                        onTop(true);

                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }else{
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });

    function compareArray(data,compare){
        for(var i =0;i < compare.length;i++){
            if(compare[i].name == data['name'] && compare[i].size == data['size']){
                return 'true';
            }
        }
        return 'false';
    }


    function editInspectionData(){
        $('#editInspectionDetails').on('submit', function (e) {
            e.preventDefault();

            var id = $(".main-tabs #addInspectionid").val();

            var uploadform = new FormData();
            var formData = $('#editInspectionDetails').serialize();
            var form_type = $("#form_type").val()
            var object_type = $("#editInspectionDetails #object_type1").val();

            var action = '';
            var custom_field = [];
            combine__data_photo_document_array = [];
            $(".custom_field_html input").each(function () {
                var data = {
                    'name': $(this).attr('name'),
                    'value': $(this).val(),
                    'id': $(this).attr('data_id'),
                    'is_required': $(this).attr('data_required'),
                    'data_type': $(this).attr('data_type'),
                    'default_value': $(this).attr('data_value')
                };
                custom_field.push(data);
            });
            var combine_photo_document_array = [];
            var length = $('#photo_video_uploads > div').length;


            if (length > 0) {

                var data = convertSerializeDatatoArrayPhotos();

                $.each(photo_videos, function (key, value) {
                    if (compareArray(value, data) == 'true') {
                        uploadform.append(key, value);
                    }
                });
            }

            var ckb_status = $("#editInspectionDetails #modelInspectionarea1 .inspection-outer .inspection-sub-child .check-input-outer .checkboxInspection").prop('checked');
            if (ckb_status === false) {
                bootbox.alert("Please select atleast one inspection area");
            }


            uploadform.append('class', "buildingDetail");
            uploadform.append('action', "editInspectionData");
            uploadform.append('custom_field', JSON.stringify(custom_field));
            uploadform.append('form', formData);
            uploadform.append('id', id);
            uploadform.append('object_type', object_type);

            $.ajax
            ({
                type: 'post',
                url: '/Building/AddBuildingDetail',
                processData: false,
                contentType: false,
                data: uploadform,
                beforeSend: function (xhr) {
                    $(".custom_field_html input").each(function () {
                        var res = validateCustomField($(this).val(), $(this).attr('data_required'), $(this).attr('data_type'), this, this.id);
                        if (res === false) {
                            xhr.abort();
                            return false;
                        }
                    });
                },
                success: function (response) {
                    var response = JSON.parse(response);

                    if (response.status == 'success' && response.code == 200) {
                        localStorage.removeItem("inspection_value");
                        var base_url = window.location.origin;
                        localStorage.setItem("Message", response.message);
                        localStorage.setItem("rowcolor", true);
                        localStorage.removeItem("editInspection");

                        window.location.href = base_url + "/Inspection/Inspection";
                        edit_date_time(response.data.updated_at);
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.' + key).html(value);
                        });
                    } else {
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {

                        $('#' + key + '_err').text(value);
                    });
                }
            });
        });
    }

    $(document).on('click','.clearForm',function () {
    var id = $('#property_id').val();
    window.location.href = base_url + "/Property/PropertyInspection?id="+id;
    });

    $(document).on('click','.clearBuildingForm',function () {
        var id = $('#building_id').val();
        window.location.href = base_url + "/Building/BuildingInspection?id="+id;
    });

    $(document).on('click','.clearUnitForm',function () {
        var id = $('#building_id').val();
        bootbox.confirm("Do you want to reset this form?", function (result) {
            if (result == true) {
                window.location.href = base_url + "/Unit/UnitInspection?id=" + id;
            }
        });
    });



 $(document).on('click','.reset_form',function () {
 var id =  $(".main-tabs #addInspectionid").val();
     bootbox.confirm("Do you want to reset this form?", function (result) {
         if (result == true) {
             InspectionData(id);
         }
     });
 });

$(document).on('click','.cancelBtnData',function () {

    bootbox.confirm("Do you want to cancel this form?", function (result) {
        if (result == true) {
            localStorage.removeItem("inspection_value");
            window.location.href = '/Inspection/Inspection';
        }
    });
});


function InspectionData(id) {
    $.ajax({
        type: 'POST',
        url: '/Building/AddBuildingDetail',
        data: {
            class: "buildingDetail",
            action: "getInspectionListView",
            id:id
        },
        success: function (response) {
            var response = JSON.parse(response);
            var id_insp = response.data.id;
            if (response.code == 200) {
                $("#building_name_inspection").val(response.data.building_name);
                $("#property_address_inspection").val(response.data.property_address);
                $("#building_address_inspection").val(response.data.building_address);
                $("#property_name_inspection").val(response.data.property_name);
                if (response.data.inspection_type == '1'){
                    var inspection_type1 = "Interior";
                }else if (response.data.inspection_type == '2'){
                    var inspection_type1= "Exterior";
                }else if (response.data.inspection_type == ''){
                    var inspection_type1= "Select";
                }

                $("#inspTypeId select").html("<option value="+ response.data.inspection_type +">"+inspection_type1+"</option>");

                if (response.data.inspection_name == ''){
                    var inspection_name = "Select";
                }else if (response.data.inspection_name == '1'){
                    var inspection_name = "Move In Inspection";
                }else if (response.data.inspection_name == '2'){
                    var inspection_name= "Move Out Inspection";
                }else if (response.data.inspection_name == '3'){
                    var inspection_name= "Weekly Inspection";
                }else if (response.data.inspection_name == '4'){
                    var inspection_name= "Monthly Inspection";
                }else if (response.data.inspection_name == '5'){
                    var inspection_name= "Six-Month Inspection";
                }else if (response.data.inspection_name == '6'){
                    var inspection_name= "Annual Inspection";
                }else if (response.data.inspection_name == '7'){
                    var inspection_name= "General Inspection";
                }else if (response.data.inspection_name == '8'){
                    var inspection_name= "Emergency Inspection";
                }else if (response.data.inspection_name == '9'){
                    var inspection_name= "Fd";
                }else if (response.data.inspection_name == '10'){
                    var inspection_name= "Others";
                }
                $("#abcsss select").text(inspection_name);
                $(".inspectionNotesI").val(response.data.notes);
                defaultFormData=$("#editInspectionDetails").serializeArray();
                edit_date_time(response.data.updated_at);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}


    $(document).on('click','.cancelBtnInspection',function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = '/Property/PropertyModules';
            }
        });

    });

    $(document).on('click','.clearFormResetunit',function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.history.back();
            }
        });

    });

    $(document).on('click','.cancel-btn-inspect',function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.history.back();
            }
        });
    });

    $(document).on('click','.back-inspection-btn',function(){
        window.location.href = '/Inspection/Inspection';
    });

    $(document).on('click','#editInspectionDetails #modelInspectionarea1 .inspection-sub-child .check-input-outer .checkboxInspection',function () {
        if($(this).is(':checked'))
           $(this).parent().parent().parent().parent().parent().children().find('.check-input-outer input[type="checkbox"]').prop('disabled', false);
    });

});