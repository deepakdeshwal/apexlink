$(document).ready(function() {

    var id = getParameterByName('id');
    getBuildingDetail();
    setTimeout(function(){
        jqGrid('All');

    }, 1000);


    /**
     * jquery to redirect for edits
     */
    $(document).on('click', '.edit_redirection', function () {

       var redirection_data = $(this).attr('redirection_data');
        localStorage.setItem("AccordionHref",'#'+redirection_data);
        window.location.href = window.location.origin+'/Building/EditBuilding?id='+id;
    });



    //jqGrid status
    $('#jqGridStatus').on('change', function () {
        var selected = this.value;
        var deleted_at = true;
        $('#property-building-table').jqGrid('GridUnload');
        if (selected == 4) deleted_at = false;
        jqGrid(selected);
    });

    /**
     * Get Parameters by id
     * @param status
     */
    function getParameterByName(name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }


    /**
     * jqGrid Initialization function
     * @param status
     */


    function jqGrid(status) {
        var property_id = $("#building_property_id").val();
        var table = 'building_detail';
        var columns = ['Building Name', 'Building Id', '# of Units', 'Linked Units', 'Status', 'Actions'];
        var select_column = ['Edit', 'Flag Bank', 'Inspection', 'Add In-Touch', 'In-Touch History', 'Deactivate', 'Activate', 'Print Envelope', 'Delete'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['building_detail.deleted_at'];
        var extra_where = [{column: 'property_id', value: property_id, condition: '='}];
        var columns_options = [
            {
                name: 'Building Name',
                index: 'building_name',
                width: 90,
                title:false,
                align: "center",
                searchoptions: {sopt: conditions},
                table: table,formatter: buildingName,classes: 'pointer',
                attr:[{name:'flag',value:'building'}]
            },
            {name: 'Building Id', index: 'building_id', width: 100, searchoptions: {sopt: conditions}, table: table,classes: 'pointer'},
            {
                name: '# of units',
                index: 'no_of_units',
                width: 80,
                align: "center",
                searchoptions: {sopt: conditions},
                table: table,classes: 'pointer'
            },
            {
                name: 'Linked Units',
                index: 'linked_units',
                width: 80,
                align: "center",
                searchoptions: {sopt: conditions},
                table: table,classes: 'pointer',
                formatter:linkedUnits
            },
            {
                name: 'status',
                index: 'status',
                width: 80,
                align: "center",
                searchoptions: {sopt: conditions},
                table: table,
                formatter: statusFmatter,classes: 'pointer'
            },
            {
                name: 'Action',
                index: 'select',
                title: false,
                width: 80,
                align: "right",
                sortable: false,
                cellEdit: true,
                cellsubmit: 'clientArray',
                editable: true,
                formatter: 'select',
                edittype: 'select',
                search: false,
                table: table,
                formatter: actionFmatter
            }
        ];
        var ignore_array = [];
        jQuery("#property-building-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Building",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }

    /**
     *  function to format property name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function buildingName(cellValue, options, rowObject) {
       // return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        if(rowObject !== undefined) {

            var flagValue = $(rowObject.Action).attr('flag');

            var id = $(rowObject.Action).attr('data_id');

            var flag = '';

            if (flagValue == 'yes') {

                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a id="flag" href="javascript:void(0);" data_url="/Property/PropertyView?id="' + id + '"><img src="/company/images/Flag.png"></a></span>';

            } else {

                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';

            }

        }
    }




    /**
     * jqGrid Building Keys function
     * @param status
     */
    buildingKeys();

    function buildingKeys(status) {
        var table = 'building_keys';
        var columns = ['Key Tag', 'Description', 'Total Keys','Available Keys'];
        //var select_column = ['Edit', 'Flag Bank', 'Inspection', 'Add In-Touch', 'In-Touch History', 'Deactivate', 'Activate', 'Print Envelope', 'Delete'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['building_keys.total_keys','building_keys.available_keys'];
        var extra_where = [{column: 'building_id', value: id, condition: '='}];
        var columns_options = [
            {name: 'Key Tag',index: 'key_id',width: 90,align: "center",searchoptions: {sopt: conditions},table: table},
            {name: 'Description', index: 'key_description', width: 100, searchoptions: {sopt: conditions}, table: table},
            {name: 'Total Keys', index: 'total_keys', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:totalkeyFormatter},
            {name: 'AvailableKeys', index: 'available_keys', width: 300, align: "center", hidden:true,searchoptions: {sopt: conditions}, table: table},
        ];
        var ignore_array = [];
        jQuery("#building-keys").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Keys",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: false, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }

    function totalkeyFormatter(cellValue, options, rowObject){
        var totalkeys =''
        if(rowObject !== undefined){
            if(rowObject.AvailableKeys == ''){ var avail=0 }else{ var avail= rowObject.AvailableKeys}
            var totalkeys=avail +'/'+cellValue;
            return totalkeys;
        }
        return totalkeys;
    }



    /**
     * jqGrid Building Keys function
     * @param status
     */
    fileLibrary();

    function fileLibrary(status) {
        var table = 'building_file_uploads';
        var columns = ['File Name', 'File','Location','Action'];
        //var select_column = ['Edit', 'Flag Bank', 'Inspection', 'Add In-Touch', 'In-Touch History', 'Deactivate', 'Activate', 'Print Envelope', 'Delete'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [{column: 'building_id', value: id, condition: '='},{column: 'file_type', value: "2", condition: '='}];
        var columns_options = [
            {name: 'File Name',index: 'file_name',width: 90,align: "center",searchoptions: {sopt: conditions},table: table},
            {name:'Preview',index:'file_extension',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
            {name:'Location',index:'file_location',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,hidden:true},
            {name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionLibraryFmatter,title:false}
        ];
        var ignore_array = [];
        jQuery("#file-library").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Files",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }




    /**
     * jqGrid Building Keys function
     * @param status
     */
    buildingflags();

    function buildingflags(status) {
        var table = 'flags';
        var columns = ['Date', 'Flag Name', 'Phone Number','Flag Reason','Completed','Note'];
        //var select_column = ['Edit', 'Flag Bank', 'Inspection', 'Add In-Touch', 'In-Touch History', 'Deactivate', 'Activate', 'Print Envelope', 'Delete'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [{column: 'object_id', value: id, condition: '='},{column: 'object_type', value: "building", condition: '='}];
        var columns_options = [
            {name:'Date',index:'date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Flag Name',index:'flag_name',width:200,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Phone',index:'flag_phone_number', width:200,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,table:table,formatter:phoneNumberFormat},
            {name:'Flag Reason',index:'flag_reason',width:200,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Completed',index:'completed', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,table:table,formatter:isCompletedFormatter},
            {name:'Note',index:'flag_note',width:450,align:"center",searchoptions: {sopt: conditions},table:table},
        ];
        var ignore_array = [];
        jQuery("#building-flags").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Flags",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }


    /**
     * jqGrid Building Keys function
     * @param status
     */
    buildingComplaints();

    function buildingComplaints(status) {
        var table = 'complaints';
        var columns = ['Complaint Id', 'Complaint', 'Complaint Type','Notes','Date','Other'];
        //var select_column = ['Edit', 'Flag Bank', 'Inspection', 'Add In-Touch', 'In-Touch History', 'Deactivate', 'Activate', 'Print Envelope', 'Delete'];
        var joins = [{table: 'complaints', column: 'complaint_type_id', primary: 'id', on_table: 'complaint_types'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [{column: 'object_id', value: id, condition: '='},{column: 'module_type', value: "building", condition: '='}];
        var columns_options = [
            {name:'Complaint Id',index:'complaint_id', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Complaint',index:'complaint_name', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Complaint Type',index:'complaint_type', width:100,align:"center",searchoptions: {sopt: conditions},table:"complaint_types"},
            {name:'Notes',index:'complaint_note', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Date',index:'complaint_date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Other',index:'other_notes',width:200,align:"center",searchoptions: {sopt: conditions},search:false,table:table},
        ];
        var ignore_array = [];
        jQuery("#building-complaints").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'complaints.updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Complaints",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }
    /**
     * jqGrid function to format action column
     * @param status
     */

    function actionLibraryFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {

            var select = '';
            select = ['<span><i class="fa fa-trash delete_file_library pointer" data_id="' + rowObject.id + '" style="font-size:24px"></i></span>'];
            var data = '';
            if(select != '') {
                $.each(select, function (key, val) {
                    data += val
                });
            }
            return data;
        }
    }

    $(document).on("click",".delete_file_library",function(){
        var data_id = $(this).attr("data_id");

        bootbox.confirm({
            message: "Are you sure you want to delete this file ?",
            buttons: {confirm: {label: 'Ok'}, cancel: {label: 'Cancel'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/building-ajax',
                        data: {class: 'buildingDetail', action: 'deleteFile', id: data_id},
                        success : function(response){
                            var response =  JSON.parse(response);
                            if(response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                // window.location.href = '/MasterData/AddPropertyType';
                            }else if(response.status == 'error' && response.code == 503) {
                                toastr.error(response.message);
                                // window.location.href = '/MasterData/AddPropertyType';
                            }else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                }
                triggerFileReload();
            }
        });
    });

    /**
     * jqGrid function to format file extension
     * @param status
     */
    function imageFormatter(cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var path = upload_url+'company/'+rowObject.Location;
            var src = '';
            if (rowObject.Preview == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
            } else if (rowObject.Preview == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
            } else if (rowObject.Preview == 'docx') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
            }else if (rowObject.Preview == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
            }
            return '<img class="img-upload-tab open_file_location" data-location="'+path+'" width=40 height=40 src="' + src + '">';
        }
    }

    $(document).on('click',".open_file_location",function(){
        var location =    $(this).attr("data-location");
        window.open(location, '_blank');
    });



    /**
     * jqGrid function to format completed column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isCompletedFormatter(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "True";
        else if (cellValue == '0')
            return "False";
        else
            return '';
    }


    /**
     * jqGrid function to format completed column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function phoneNumberFormat(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            if(cellValue !== undefined || cellValue != '' ){
                return cellValue.replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
            }
            else {
                return '';
            }
        }
    }






/**
 * jqGrid function to format is_default column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function isDefaultFormatter(cellValue, options, rowObject) {
    if (cellValue == '1')
        return "Yes";
    else if (cellValue == '0')
        return "No";
    else
        return '';
}



function getBuildingDetail() {
    var base_url = window.location.origin;
    $.ajax({
        type: 'post',
        url: '/building-ajax',
        async: true,
        data: {
            class: 'buildingDetail',
            action: "getBuildingDetail",
            id: id
        },
        success: function (response) {
            var response = JSON.parse(response);
            $(".renovation_detail").html(response.renovation_detail)
            $(".school_district_html").html(response.school_district_muncipiality_data)
            getCustomfield(response.custom_data);
            $.each(response.images_data, function(index, img) {
                var image=  base_url+'/company/'+img.file_location
            $('.owl-carousel').trigger('add.owl.carousel', ['<div class="item"><img src="'+image+'" alt=""></div>'])
                .trigger('refresh.owl.carousel');
            });
            if (response.code == 200) {
                $.each(response.data, function (key, value) {
                    // alert(value);
                    if(key=='key_access_codes_info'){
                        $('.key_access_codes_info').html(response.key_access_codes_info_string);
                    }else{

                        $('.' + key).html(value);
                    }

                });
                $("#building_property").html(response.data.property_name);
                $("#property_link").prop("href", base_url+"/Property/PropertyModule/");
                $("#building_span_name").html(response.data.building_name);
                $("#building_property_id").val(response.data.building_property_id);
                $("#new_building_href").prop("href", base_url+"/Building/AddBuilding?id="+response.data.building_property_id);
                $(".building_list_href").prop("href", base_url+"/Building/BuildingModule?id="+response.data.building_property_id);
                $("#new_unit_href").prop("href", base_url+"/Unit/AddUnit?id="+response.data.building_property_id+"&bid="+response.data.id);
                $(".unit_list_href").prop("href", base_url+"/Unit/UnitModule?id="+response.data.building_property_id+"&bid="+response.data.id);

            }
        }
    });
}




    /**
     * Function for view building on clicking row
     */
    $(document).on('click', '#property-building-table tr td', function () {
        var property_id = $("#building_property_id").val();
        var base_url = window.location.origin;
        var id = $(this).closest('tr').attr('id');
        if ($(this).index() == 5) {
            return false;
        }else if($(this).index() == 3){
            window.location.href = base_url+'/Unit/UnitModule?id='+property_id+'&bid='+id;
        } else {
            window.location.href = base_url+'/Building/View?id='+id;
        }
    });

    /**
     * function to format linked units
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function linkedUnits(cellValue, options, rowObject) {
        if(cellValue == ''){
            cellValue ='0';
        }
        return '<span style="text-decoration: underline;">' + cellValue + '</span>';
    }



    /**
     *change function to perform various actions(edit ,activate,deactivate)
     * @param status
     */
    jQuery(document).on('change','.select_options',function () {
        var select_options = $(this).val();
        var data_id = $(this).attr('data_id');
        if(select_options == 'Edit')
        {
            window.location.href =  window.location.origin+'/Building/EditBuilding?id='+data_id;

        }else if (select_options == 'Flag Bank'){
            window.location.href =  window.location.origin+'/Building/EditBuilding?id='+data_id+'#flags';

        }else if (select_options == 'Inspection'){
            window.location.href = '/Building/BuildingInspection/100';

        }else if (select_options == 'Add In-Touch'){
            window.location.href =   '/Communication/InTouch';
        }else if (select_options == 'In-Touch History'){
            window.location.href =   '/Communication/InTouch';
        }else if (select_options == 'Print Envelope'){
            $.ajax({
                type: 'post',
                url: '/get-company-data',
                data: {class: 'buildingDetail', action: 'getCompanyData','building_id':data_id},
                success : function(response){
                    var response =  JSON.parse(response);
                    if(response.status == 'success' && response.code == 200) {
                        $("#PrintEnvelope").modal('show');
                        $("#company_name").text(response.data.data.company_name)
                        $("#address1").text(response.data.data.address1)
                        $("#address2").text(response.data.data.address2)
                        $("#address3").text(response.data.data.address3)
                        $("#address4").text(response.data.data.address4)
                        $(".city").text(response.data.data.city)
                        $(".state").text(response.data.data.state)
                        $(".postal_code").text(response.data.data.zipcode)
                        $("#building_name").text(response.building.data.building_name)
                        $("#building_address").text(response.building.data.address)
                    }else {
                        toastr.warning('Record not updated due to technical issue.');
                    }
                }
            });


        }else if(select_options == 'Deactivate')
        {
            bootbox.confirm({
                message: "Do you want to deactivate the record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/building-ajax',
                            data: {class: 'buildingDetail', action: 'deactivate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Building deactivated successfully.');
                                }else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                }else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                }
            });
        }else if(select_options == 'Activate') {
            bootbox.confirm({
                message: "Do you want to activate the record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/building-ajax',
                            data: {class: 'buildingDetail', action: 'activate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Building activated successfully.');
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                }
            });
        }else if(select_options == 'Delete') {
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/building-ajax',
                            data: {class: 'buildingDetail', action: 'delete', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Building deleted successfully.');
                                } else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                }else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                }
            });
        }
        $('.select_options').prop('selectedIndex',0);
    });




function triggerFileReload(){
    var grid = $("#file-library");
    grid[0].p.search = false;
    $.extend(grid[0].p.postData,{filters:""});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}

/**
 * jqGrid function to format status
 * @param status
 */
function statusFmatter (cellvalue, options, rowObject){
    if (cellvalue == 1)
        return "Active";
    else if(cellvalue == 0)
        return "InActive";
    else
        return '';
}


/**
 * jqGrid function to format avialable keys
 * @param status
 */
function avialablekeys (cellvalue, options, rowObject){
    if (cellvalue == 1)
        return "Active";
    else if(cellvalue == 0)
        return "InActive";
    else
        return '';
}

function PrintElem(elem)
{
    Popup($(elem).html());
}

function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
        'font-weight:bold;} .right-detail{\n' +
        '        position:relative;\n' +
        '        left:+400px;\n' +
        '    }</style>');
    $(mywindow.document.body).html( '<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if(mywindow.close()){

    }
    $("#PrintEnvelope").modal('hide');
    return true;
}

/**
 * jqGrid function to format action column
 * @param status
 */

function actionFmatter (cellvalue, options, rowObject){

    if(rowObject !== undefined) {
        //console.log(rowObject.status);
        var editable = $(cellvalue).attr('editable');
        var select = '';
        if(rowObject.status == 1)  select = ['Edit','Flag Bank','Inspection','Add In-Touch','In-Touch History','Deactivate','Print Envelope','Delete'];
        if(rowObject.status == 0 || rowObject.Status == '')  select = ['Edit','Inspection','Add In-Touch','Activate','Print Envelope'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                if(editable == '0' && (val == 'delete' || val == 'Delete')){
                    return true;
                }
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}

function goBack() {
    window.history.back();
}

});