$(document).ready(function() {

    var base_url = window.location.origin;
    /*get query string parameter  */
    var id =  getParameterByName('id');
    /*get active inactive status of grids */
    var status =  localStorage.getItem("active_inactive_status");
    /*initaiate jquery tables*/
    setTimeout(function(){
   jqGrid('All', true, id);
   }, 2000);
    buildingKeys();
    fileLibrary();
    // buildingflags();
    buildingComplaints();
    /*fetch onload data from tables*/
    fetchAllUnittypes(false);
    getBuildingDetail();


    if(localStorage.getItem("AccordionHref")) {
        var message = localStorage.getItem("AccordionHref");
        $('html, body').animate({
            'scrollTop' : $(message).position().top
        });
        localStorage.removeItem('AccordionHref');
    }

    $("input").css('textTransform', 'capitalize');
    $("textarea").css('textTransform', 'capitalize');
    // $("input").keyup(function () {
    //     $(this).css('textTransform', 'capitalize');
    // });
    // $("textarea").keyup(function () {
    //     $(this).css('textTransform', 'capitalize');
    // });

setTimeout(function(){
   
   
    if(status !== undefined) {
        if ($("#building-table-edit")[0].grid) {
            $('#building-table-edit').jqGrid('GridUnload');
        }

        //intializing jqGrid
        if(status == 'all'){
            jqGrid('All', true,id);
        }  else {
            jqGrid(status, true,id);
        }
        $('#jqGridStatus option[value='+status+']').attr("selected", "selected");

    }else{
        if ($("#properttype-table")[0].grid) {
            $('#properttype-table').jqGrid('GridUnload');
        }
        jqGrid('All', true,id);
    }
}, 2000);

    /**
     * jqGrid Initialization function for list of buildings
     * @param status
     */

    function jqGrid(status, deleted_at,id) {
        var property_id = $("#property_id").val();
        var table = 'building_detail';
        var columns = ['Building Name','Building Id', '# of Units', 'Linked Units', 'Status', 'Actions'];
        var select_column = ['Edit','Flag Bank','Inspection','Add In-Touch','In-Touch History','Deactivate','Activate','Print Envelope','Delete'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_where = [{column: 'property_id', value: property_id, condition: '='}];
        var extra_columns = ['building_detail.deleted_at'];
        var columns_options = [
            {name:'Building Name',index:'building_name',title:false, width:90,align:"center",searchoptions: {sopt: conditions},table:table,formatter: buildingName,classes: 'pointer',attr:[{name:'flag',value:'building'}]},
            {name:'Building Id',index:'building_id', width:100,searchoptions: {sopt: conditions},table:table,classes: 'pointer'},
            {name:'# of units',index:'no_of_units', width:80,align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer'},
            {name:'Linked Units',index:'linked_units', width:80, align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer',formatter:linkedUnits},
            {name:'status',index:'status', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter,classes: 'pointer'},
            {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionBuildingListFmatter}
        ];
        var ignore_array = [];
        jQuery("#building-table-edit").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_where :extra_where,
                extra_columns:extra_columns
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Building",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }

    /**
     * function to format linked units
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function linkedUnits(cellValue, options, rowObject) {
        if(cellValue == ''){
            cellValue ='0';
        }
        return '<span style="text-decoration: underline;">' + cellValue + '</span>';
    }


    /**
     *  function to format property name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function buildingName(cellValue, options, rowObject) {
        //return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        if(rowObject !== undefined) {

            var flagValue = $(rowObject.Action).attr('flag');

            var id = $(rowObject.Action).attr('data_id');

            var flag = '';

            if (flagValue == 'yes') {

                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a id="flag" href="javascript:void(0);" data_url="/Property/PropertyView?id="' + id + '"><img src="/company/images/Flag.png"></a></span>';

            } else {

                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';

            }

        }
    }

    function goBack() {
        window.location.href=base_url+'/Companies/List';
        //window.history.back();
    }

    /*redirect to edit page */
    $(document).on("click",".edit_building",function(){
        var id = $(this).attr('data_id')
        window.location.href =  base_url+'/Building/EditBuilding?id='+id;
    });

    /*ajax to deactivate building */

    $(document).on("click",".deactivate_building",function(){
        var id = $(this).attr('data_id')
        bootbox.confirm({
            message: "Do you want to deactivate the record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/building-ajax',
                        data: {class: 'buildingDetail', action: 'deactivate', id: id},
                        success : function(response){
                            var response =  JSON.parse(response);
                            if(response.status == 'success' && response.code == 200) {
                                toastr.success('Building deactivated successfully.');
                            }else if(response.status == 'error' && response.code == 503) {
                                toastr.error(response.message);
                            }else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                }
                triggerReload();
            }
        });

    });


    /**
     * Function for view building on clicking row
     */
    $(document).on('click', '#building-table-edit tr td', function () {
        var base_url = window.location.origin;
        var property_id = $("#property_id").val();
        var id = $(this).closest('tr').attr('id');
        if ($(this).index() == 5) {
            return false;
        }else if($(this).index() == 3){
            window.location.href = base_url+'/Unit/UnitModule?id='+property_id+'&bid='+id;
        } else {
            window.location.href = base_url+'/Building/View?id='+id;
        }
    });


    /*trigger to reload building listing table */
    function triggerReload(){
        var grid = $("#building-table-edit");
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    /* on change function to get selected value for select option */
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        var deleted_at = true;
        $('#building-table-edit').jqGrid('GridUnload');
        if(selected == 4) deleted_at = false;
        jqGrid(selected);
    });

    /**
     *change function to perform various actions(edit ,activate,deactivate)
     * @param status
     */
    jQuery(document).on('change','#building-table-edit .select_options',function () {
        var select_options = $(this).val();
        var data_id = $(this).attr('data_id');
        if(select_options == 'Edit')
        {
            window.location.href = base_url+'/Building/EditBuilding?id='+data_id;
            // window.location.href = '/MasterData/AddPropertyType';

        }else if (select_options == 'Flag Bank'){
            localStorage.setItem("AccordionHref",'#flags');
            window.location.href = base_url+'/Building/EditBuilding?id='+data_id;

        }else if (select_options == 'Inspection'){
            window.location.href = base_url+'/Building/BuildingInspection?id='+data_id;
        }else if (select_options == 'Add In-Touch'){
            window.location.href =   base_url+'/Communication/InTouch';
        }else if (select_options == 'In-Touch History'){
            window.location.href =   base_url+'/Communication/InTouch';
        }else if (select_options == 'Print Envelope'){
            $.ajax({
                type: 'post',
                url: '/get-company-data',
                data: {class: 'buildingDetail', action: 'getCompanyData','building_id':data_id},
                success : function(response){
                    var response =  JSON.parse(response);
                    if(response.status == 'success' && response.code == 200) {
                        $("#PrintEnvelope").modal('show');
                        $("#company_name").text(response.data.data.company_name)
                        $("#address1").text(response.data.data.address1)
                        $("#address2").text(response.data.data.address2)
                        $("#address3").text(response.data.data.address3)
                        $("#address4").text(response.data.data.address4)
                        $(".city").text(response.data.data.city)
                        $(".state").text(response.data.data.state)
                        $(".postal_code").text(response.data.data.zipcode)
                        $("#building_name").text(response.building.data.building_name)
                        $("#building_address").text(response.building.data.address)


                    }else {
                        toastr.warning('Record not updated due to technical issue.');
                    }
                }
            });


        }else if(select_options == 'Deactivate')
        {
            bootbox.confirm({
                message: "Do you want to deactivate the record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/building-ajax',
                            data: {class: 'buildingDetail', action: 'deactivate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Building deactivated successfully.');
                                    // window.location.href = '/MasterData/AddPropertyType';
                                }else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                    // window.location.href = '/MasterData/AddPropertyType';
                                }else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                }
            });
        }else if(select_options == 'Activate') {
            bootbox.confirm({
                message: "Do you want to activate the record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/building-ajax',
                            data: {class: 'buildingDetail', action: 'activate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Property type activated successfully.');
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                }
            });
        }else if(select_options == 'Delete') {
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/building-ajax',
                            data: {class: 'buildingDetail', action: 'delete', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Company deleted successfully.');
                                } else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                }else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                }
            });
        }
        $('.select_options').prop('selectedIndex',0);
    });


    /*get property id  */



    /*jquery to open new complaint box */
    $(document).on("click","#new_complaint_button",function(){
        $(".clear_complaint").show();
        $('.reset_complaint').hide();
        $("#new_complaint").show(500)
        $("#complaint_save").html('Save')
        $('#new_complaint :input').val('');
        $('#edit_complaint_id').val('');
        $('#complaint_id').prop('readonly',false);
        $("#complaint_id").val(Math.random().toString(36).slice(2).substr(0, 6).toUpperCase());
        /*date picker for complaint date */
        $("#complaint_date").datepicker({
            dateFormat: date_format,
            autoclose: true,
            changeMonth: true,
            changeYear: true
        }).datepicker("setDate", new Date());

    });

    /*jquery to open modal for complaint box to print */
    $(document).on("click",'#print_email_button',function(){
        favorite=[];
        var no_of_checked =    $('[name="complaint_checkbox[]"]:checked').length
        if(no_of_checked == 0){
            toastr.error('Please select atleast one Complaint.');
            return false;
        }
        $.each($("input[name='complaint_checkbox[]']:checked"), function(){
            favorite.push($(this).attr('data_id'));
        });

        $.ajax({
            type: 'post',
            url: '/building-ajax',
            data: {class: 'buildingDetail', action: 'getComplaintsData','complaint_ids':favorite,'building_id':id},
            success : function(response){
                var response =  JSON.parse(response);
                if(response.status == 'success' && response.code == 200) {
                    $("#print_complaint").modal('show');
                    $("#modal-body-complaints").html(response.html)
                }else {
                    toastr.warning('Record not updated due to technical issue.');
                }
            }
        });
    });


    /*function to getquery string parameter by id */
    function getParameterByName( name ){
        var regexS = "[\\?&]"+name+"=([^&#]*)",
            regex = new RegExp( regexS ),
            results = regex.exec( window.location.search );
        if( results == null ){
            return "";
        } else{
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }



    $(document).on("change", ".key_access_codes_list", function () {

        var value = $(this).val();
        if (parseInt(value) >= 1) {
            $(this).parent().find('.key_access_codeclass').show();
            $(this).parent().find('.key_access_codeclass').val(' ');
        } else {
            $(this).parent().find('.key_access_codeclass').hide();
        }
    });

    $(document).on("click", "#NewkeyMinus", function () {
        $(this).parents('#key_optionsDiv').remove();
    });




    $(".grey-btn").click(function () {
        let box = $(this).closest('.add-popup');
        $(this).closest('.add-popup').find('input[type=text]').val('');
        $(box).hide();


    });

    $("#complainttype").click(function () {
       $('.customValidateComplaint').val('');


    });

    complainttype

    $(document).on("click","#addnewkey",function(){
        $('#add_key_tracker :input').val('');
        $('#total_keys').val(5);
        $('#AddKeyButton').text('Save');
        $("#add_key_tracker").show(500);
        $("#track_key_tracker").hide(500);
        $("#KeycheckoutDiv").hide(500);
        $("#edit_key_id").val('');
        $("#clear_add_key").show();
        $("#reset_update_key").hide();
    });

    $(document).on("blur","#total_keys",function(){
        var total_keys=  $(this).val();

        if(total_keys == ''){
            $(this).val(5);
        }

    });

    $(document).on("click","#addtrackkey",function(){
        $("#add_key_tracker").hide(500);
        $('#track_key_tracker :input').val('');
        $("#track_key_tracker").show(500);
        $("#edit_track_key_id").val('');
        $("#pick_up_date").datepicker({dateFormat: date_format,
            changeYear: true,
            minDate: 0,
            yearRange: "-100:+20",
            changeMonth: true,
            onClose: function (selectedDate) {
                $("#return_date").datepicker("option", "minDate", selectedDate);
            }}).datepicker("setDate", new Date());


        $("#pick_up_time").timepicker({ timeFormat: 'h:mm p',defaultTime: new Date()});
        $("#return_date").datepicker({dateFormat: date_format, changeYear: true,
            minDate: 0,
            yearRange: "-100:+20",
            changeMonth: true,
            onClose: function (selectedDate) {
                $("#return_date").datepicker("option", "maxDate", selectedDate);
            }}).datepicker("setDate", new Date());
        $("#return_time").timepicker({ timeFormat: 'h:mm p',defaultTime: new Date()});
        $("#clear_track_key").show();
        $("#reset_update_track_key").hide();

    });

    $('input[type=radio]').change(function() {
        if (this.value == 'add_key') {
            $("#addnewkey").show();
            $("#addtrackkey").hide();
            $("#add_key_tracker").hide(500);
            $("#track_key_tracker").hide(500);
        }
        else if (this.value == 'track_key') {
            $("#addnewkey").hide();
            $("#addtrackkey").show();
            $("#add_key_tracker").hide(500);
            $("#track_key_tracker").hide(500);
        }
    });

    /*for multiple phone number textbox*/
    $(document).on("click", "#multiplephoneno", function () {
        var clone = $(".multiplephonediv:first").clone().find("input:text").val("").end();
        var divlength = $(".multiplephonediv").clone();
        if (divlength.length <= 2) {
            $(".multiplephonediv").last().after(clone);
            $(".multiplephonediv:not(:eq(0))  #multiplephoneno").remove();
            $(".multiplephonediv:not(:eq(0))  #multiplephonenocross").show();
        }
        if (divlength.length == 2) {
            $(this).hide();
        }
    });
    /*remove multiple phone number textbox*/
    $(document).on("click", "#multiplephonenocross", function () {
        $(this).parents('.multiplephonediv').remove();
        $("#multiplephoneno").show();

    });

    $(function() {
        var regExp = /[a-z]/i;
        $(document).on('keydown keyup','.p-number-format', function(e) {
            if((event.which >= 48 && event.which <= 57) && event.shiftKey) {
                event.preventDefault();
            }
            if((event.which >= 95 && event.which <= 105) && event.shiftKey) {
                event.preventDefault();
            }
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)&& (event.which < 96 || event.which > 105) && event.which != 8) {
                event.preventDefault();
            }
        });
    });
    setTimeout(function(){
        var divlength = $(".multiplefaxdiv")
        if (divlength.length > 2) {
            $(".multiplefaxdiv:not(:eq(0))  #multiplefaxno").remove();
            $(".multiplefaxdiv:not(:eq(0))  #multiplefaxnocross").show();
        }
        if (divlength.length == 3) {
            $(".multiplefaxdiv:eq(0)  #multiplefaxno").hide();
        }

        var divlength = $(".multiplephonediv");
        if (divlength.length > 2) {
            $(".multiplephonediv:not(:eq(0))  #multiplephoneno").remove();
            $(".multiplephonediv:not(:eq(0))  #multiplephonenocross").show();
        }
        if (divlength.length == 3) {
            $(".multiplephonediv:eq(0)  #multiplephoneno").hide();
        }

    }, 400);

    /*for multiple fax number textbox*/
    $(document).on("click", "#multiplefaxno", function () {
        var clone = $(".multiplefaxdiv:first").clone().find("input:text").val("").end();;
        var divlength = $(".multiplefaxdiv").clone();
        if (divlength.length <= 2) {
            $(".multiplefaxdiv").last().after(clone);
            $(".multiplefaxdiv:not(:eq(0))  #multiplefaxno").remove();
            $(".multiplefaxdiv:not(:eq(0))  #multiplefaxnocross").show();
        }
        if (divlength.length == 2) {
            $(this).hide();
        }
    });
    /*remove multiple phone number textbox*/
    $(document).on("click", "#multiplefaxnocross", function () {
        $(this).parents('.multiplefaxdiv').remove();
        $("#multiplefaxno").show();

    });

    $("input[name='last_renovation_date']").datepicker({
        dateFormat: date_format,
        autoclose: true,
        changeMonth: true,
        changeYear: true
    });
    //open a modal
    $(document).on('click', '#newRenovation', function (e) {
        $("#lastRenovationModal").modal('show');
        $("input[name='new_renovation_date']").datepicker({
            dateFormat: date_format,
            autoclose: true,
            changeMonth: true,
            changeYear: true
        });
        $("input[name='new_renovation_time']").timepicker({ timeFormat: 'h:mm p',defaultTime: new Date()});
    })


    jQuery(document).on("keyup keydown",'.p-number-format',function() {
     //   $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
        $(this).val($(this).val().replace(/^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/, "$1-$2-$3"));
    });

    jQuery(document).on("click",'#no_seprate_unit_checkbox',function() {
        var no_of_units = $("#no_of_units").val();
        if (no_of_units == 1) {
            if ($(this).is(":checked"))
                $("#no_seprate_unit").show(500);
            else
                $("#no_seprate_unit").hide(500)
        } else {
            toastr.error("Number of units must be equal to 1");
            return false;
        }


    });

    jQuery(document).on("click",'#selectAllamennity input[type=checkbox]',function() {
        if ($(this).is(":checked") && $(this).val()== 'selectall')
            $('.inputAllamenity').prop('checked', true);
        else
            $('.inputAllamenity').prop('checked', false);

    });

    jQuery(document).on("keyup keydown",'#no_of_units',function() {
        if ($("#no_seprate_unit_checkbox").is(":checked") && $(this).val() > 1){
            $(this).val(1)
        }
    });

    $(document).on('blur','#base_rent', function(e) {
        var base_rent = parseInt($(this).val());
        $("#security_deposit").val(base_rent.toFixed(2));
        $("#base_rent").val(base_rent.toFixed(2));

    });

    $(document).on('click','#building_cancel',function(){
        var property_id = $("#property_id").val();
        bootbox.confirm("Do you want to cancel this action now ?", function (result) {
            if (result == true) {
                window.location.href = base_url+'/Building/BuildingModule?id='+property_id;
            }
        });
    });

    //add announcement
    $("#new_renovation_form").validate({
        submitHandler: function (form) {
            event.preventDefault();
            var formData = $('#new_renovation_form').serializeArray();

            $.ajax({
                type: 'post',
                url: '/building-ajax',
                data: {form: formData,
                    class: 'buildingDetail',
                    action: 'addNewRenovation'},
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        toastr.success(response.message);
                        $("#lastRenovationModal").modal('hide');
                        window.location.reload();

                    }   else if (response.status == 'error' && response.code == 500) {
                        toastr.error(response.message);
                        return false;
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('#' + key).text(value);
                        });
                    }else if (response.status == 'error' && response.code == 400) {
                        toastr.warning(response.message);
                    }
                },
                error: function (response) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });

    /**
     * jqGrid Building Keys function
     * @param status
     */
    function buildingKeys(status) {
        var table = 'building_keys';
        var columns = ['Id','Key Tag', 'Description', 'Total Keys','Available Keys','Status','Action'];
        //var select_column = ['Edit', 'Flag Bank', 'Inspection', 'Add In-Touch', 'In-Touch History', 'Deactivate', 'Activate', 'Print Envelope', 'Delete'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['building_keys.total_keys','building_keys.available_keys','building_keys.deleted_at', 'building_keys.updated_at'];
        var extra_where = [{column: 'building_id', value: id, condition: '='}];
        var columns_options = [
            {name: 'Id',index: 'id',width: 90,align: "center",searchoptions: {sopt: conditions},table: table,hidden:true},
            {name: 'Key Tag',index: 'key_id',width: 90,align: "center",searchoptions: {sopt: conditions},table: table},
            {name: 'Description', index: 'key_description', width: 100, searchoptions: {sopt: conditions}, table: table},
            {name: 'Total Keys', index: 'total_keys', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:totalkeyFormatter},
            {name: 'AvailableKeys', index: 'available_keys', width: 300, align: "center", hidden:true,searchoptions: {sopt: conditions}, table: table},
            {name: 'Status', index: 'status', width: 300, align: "center", hidden:true,searchoptions: {sopt: conditions}, table: table},
            {name: 'Action', index: 'select', title: false, width: 200, align: "center", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table, formatter:actionKeysFormatter},
        ];
        var ignore_array = [];
        jQuery("#building-keys").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Keys",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }

    function actionKeysFormatter(cellValue, options, rowObject) {
        if(rowObject !== undefined){
            if (rowObject.Status == 1) {
                var select = ['Edit', 'Delete', 'checkout','return'];
                var data = '';
                if (select != '') {
                    var data = '<select ' +
                        ' class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                    $.each(select, function (key, val) {
                        data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                    });
                    data += '</select>';
                }
                return data;
            }
            else {
                var select = ['Edit', 'Delete', 'checkout'];
                var data = '';
                if (select != '') {
                    var data = '<select ' +
                        ' class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                    $.each(select, function (key, val) {
                        data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                    });
                    data += '</select>';
                }
                return data;
            }
        }
    }

    // function totalkeyFormatter(cellValue, options, rowObject){
    //     var totalkeys =''
    //     row_id="";
    //     column_id="";
    //     row='';
    //     if(rowObject !== undefined){
    //         console.log(options);
    //         if(rowObject.AvailableKeys == ''){ var avail=0 }else{ var avail= rowObject.AvailableKeys}
    //         var totalkeys=avail +'/'+cellValue;
    //         if(avail ==  cellValue){
    //             var row_id =options.rowId;
    //             var column_id =options.rowId;
    //             //jQuery('#building-keys').find('tr:eq(1)').find('td:eq(1)').addClass("red_star");
    //         }
    //         return totalkeys;
    //     }
    //     return totalkeys;
    // }

    function totalkeyFormatter(cellValue, options, rowObject){
        var totalkeys =''
        row_id="";
        column_id="";
        row='';
        if(rowObject !== undefined){
            console.log(options);
            if(rowObject.AvailableKeys == ''){ var avail=0 }else{ var avail= rowObject.AvailableKeys}
            var totalkeys=avail +'/'+cellValue;
            if(avail ==  cellValue){
                var row_id =options.rowId;
                var column_id =options.rowId;
                //jQuery('#building-keys').find('tr:eq(1)').find('td:eq(1)').addClass("red_star");
            }
            if(avail == cellValue){
                return "<span class='addcheckoutcolor'>" +totalkeys + "</span>";
            }
            else{
                return "<span class='addcheckoutcolorblue' rel="+rowObject.Id+">" +totalkeys + "</span>";
            }
        }
        return totalkeys;
    }




    /**
     * jqGrid Building Keys function
     * @param status
     */

    function fileLibrary(status) {
        var table = 'building_file_uploads';
        var columns = ['File Name', 'File', 'Action'];
        //var select_column = ['Edit', 'Flag Bank', 'Inspection', 'Add In-Touch', 'In-Touch History', 'Deactivate', 'Activate', 'Print Envelope', 'Delete'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [{column: 'building_id', value: id, condition: '='},{column: 'file_type', value: "2", condition: '='}];
        var columns_options = [
            {name: 'File Name',index: 'file_name',width: 90,align: "center",searchoptions: {sopt: conditions},table: table},
            {name:'Preview',index:'file_extension',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
            {name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionLibraryFmatter,title:false}
        ];
        var ignore_array = [];
        jQuery("#file-library").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Building Files",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }


    /**
     * jqGrid Building Keys function
     * @param status
     */

    function buildingComplaints(status) {
        var table = 'complaints';
        var columns = ['<input type="checkbox" id="select_all_complaint_checkbox">','Complaint Id', 'Complaint Type','Notes','Date','Other','Action'];
        var select_column = ['Edit', 'Delete'];
        var joins = [{table: 'complaints', column: 'complaint_type_id', primary: 'id', on_table: 'complaint_types'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['complaints.status'];
        var extra_where = [{column: 'object_id', value: id, condition: '='},{column: 'module_type', value: "building", condition: '='}];
        var columns_options = [
            {name:'Id',index:'id', width:100,align:"center",sortable:false,searchoptions: {sopt: conditions},table:table,search:false,formatter:actionCheckboxFmatter},
            {name:'Complaint Id',index:'complaint_id', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Complaint Type',index:'complaint_type', width:100,align:"center",searchoptions: {sopt: conditions},table:"complaint_types"},
            {name:'Notes',index:'complaint_note', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Date',index:'complaint_date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Other',index:'other_notes',width:200,align:"center",searchoptions: {sopt: conditions},search:false,table:table},
            {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionsFmatter}
        ];
        var ignore_array = [];
        jQuery("#building-complaints").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'complaints.updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Complaints",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }
    /**
     * jqGrid function to format action column
     * @param status
     */

    function actionLibraryFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {

            var select = '';
            select = ['<span><i class="fa fa-trash delete_file_library pointer" data_id="' + rowObject.id + '" style="font-size:24px"></i></span>'];
            var data = '';
            if(select != '') {
                $.each(select, function (key, val) {
                    data += val
                });
            }
            return data;
        }
    }

    /*ajax to delete file libraries*/
    $(document).on("click",".delete_file_library",function(){
        var data_id = $(this).attr("data_id");

        bootbox.confirm({
            message: "Are you sure you want to delete this file ?",
            buttons: {confirm: {label: 'Ok'}, cancel: {label: 'Cancel'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/building-ajax',
                        data: {class: 'buildingDetail', action: 'deleteFile', id: data_id},
                        success : function(response){
                            var response =  JSON.parse(response);
                            if(response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                // window.location.href = '/MasterData/AddPropertyType';
                            }else if(response.status == 'error' && response.code == 503) {
                                toastr.error(response.message);
                                // window.location.href = '/MasterData/AddPropertyType';
                            }else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                }
                triggerFileReload();
            }
        });
    });

    /*ajax to delete images*/
    $(document).on("click",".delete_image",function(){
        var data_id = $(this).attr("data_id");

        bootbox.confirm({
            message: "Are you sure you want to delete this file ?",
            buttons: {confirm: {label: 'Ok'}, cancel: {label: 'Cancel'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/building-ajax',
                        data: {class: 'buildingDetail', action: 'deleteFile', id: data_id},
                        success : function(response){
                            var response =  JSON.parse(response);
                            if(response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                // window.location.href = '/MasterData/AddPropertyType';
                            }else if(response.status == 'error' && response.code == 503) {
                                toastr.error(response.message);
                                // window.location.href = '/MasterData/AddPropertyType';
                            }else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                }
                triggerPhotoReload();
            }
        });
    });

    /**
     * jqGrid function to format file extension
     * @param status
     */
    function imageFormatter(cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var src = '';
            if (rowObject.Preview == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
            } else if (rowObject.Preview == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
            } else if (rowObject.Preview == 'docx') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
            }else if (rowObject.Preview == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
            }
            return '<img class="img-upload-tab" width=100 height=100 src="' + src + '">';
        }
    }



    /**
     * jqGrid function to format completed column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isCompletedFormatter(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "True";
        else if (cellValue == '0')
            return "False";
        else
            return '';
    }


    /**
     * jqGrid function to format completed column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function phoneNumberFormat(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            if(cellValue !== undefined || cellValue != '' ){
                return cellValue.replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
            }
            else {
                return '';
            }
        }
    }


    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isDefaultFormatter(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "Yes";
        else if (cellValue == '0')
            return "No";
        else
            return '';
    }



    function getBuildingDetail() {
        var base_url = window.location.origin;
        $.ajax({
            type: 'post',
            url: '/building-ajax',
            async: true,
            data: {
                class: 'buildingDetail',
                action: "getBuildingDetail",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log(response);
                $(".renovation_detail").html(response.renovation_detail)
                $("#property_name_span").html(response.data.property_name)
                $("#property_id_span").html(response.data.property_id)
                $(".school_district_html").html(response.school_district_muncipiality_data)
                getCustomfield(response.custom_data);
                $.each(response.images_data, function(index, img) {
                    var image=  base_url+'/company/'+img.file_location
                    $('.owl-carousel').trigger('add.owl.carousel', ['<div class="item"><img src="'+image+'" alt=""></div>'])
                        .trigger('refresh.owl.carousel');
                });
                if (response.code == 200) {
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                    $("#building_property").html(response.data.property_name)
                    $("#property_link").prop("href", base_url+"/Property/PropertyModules")
                    $("#building_span_name").html(response.data.building_name)
                    $("#building_property_id").val(response.data.id);
                    $("#property_id").val(response.data.building_property_id);
                    $("#new_building_href").prop("href", base_url+"/Building/AddBuilding?id="+response.data.building_property_id);
                    $("#new_unit_href").attr("href", base_url+"/Unit/AddUnit?id="+response.data.building_property_id);
                    $("#property_inspection_link").attr("href", base_url+"/Property/PropertyInspection?id="+response.data.building_property_id);
                    $("#property_tab_link").prop("href", base_url+"/Property/PropertyModules");
                    $("#property_tab_link").prop("href", base_url+"/Property/PropertyModules");
                    $("#new_property_unit_list_href").prop("href", base_url+"/Unit/UnitModule");
                    $("#building_tab_link").prop("href", base_url+"/Building/BuildingModule?id="+response.data.building_property_id);
                }
            }
        });
    }


    /**
     * Function for view company on clicking row
     */
    $(document).on('click', '#companyUser-table tr td:not(:last-child)', function () {
        var id = $(this).closest('tr').attr('id')
        window.location.href = base_url+'/Companies/View?id=' + id;
    });


    $(document).on('click',"#keyCancel",function(){
        bootbox.confirm("Do you want to cancel this action now ?", function (result) {
            if (result == true) {
                $("#add_key_tracker").hide(500);
            }
        });

    });

    $(document).on('click',"#keyTrackCancel",function(){
        bootbox.confirm("Do you want to cancel this action now ?", function (result) {
            if (result == true) {
                $("#track_key_tracker").hide(500);
            }
        });

    });

    $(document).on('click',"#KeycheckoutCancel",function(){
        bootbox.confirm("Do you want to cancel this action now ?", function (result) {
            if (result == true) {
                $("#KeycheckoutDiv").hide(500);
            }
        });

    });

    $(document).on('click',".open_file_location",function(){
     var location =    $(this).attr("data-location");
     window.open(location, '_blank');
    });








    /**
     * To hide complaint box on cancel
     */
    $(document).on('click', '#complaint_cancel', function () {
        $("#new_complaint").hide(500);
    })

    /*date picker for complaint date */
    $("#complaint_date").datepicker({
        dateFormat: date_format,
        autoclose: true,
        changeMonth: true,
        changeYear: true
    }).datepicker("setDate", new Date());



    /*function to save new complaint */
    $(document).on('click','#complaint_save', function (e) {
        e.preventDefault();
        var formData = $('#new_complaint :input').serializeArray();
        var building_id =  $("#edit_building_id").val();

        $.ajax({
            type: 'post',
            url: '/building-ajax',
            data: {form: formData,
                class: 'buildingDetail',
                action: 'addNewComplaint',
                edit_building_id:building_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#new_complaint').hide(500);
                    $("#building-complaints").trigger('reloadGrid');
                    toastr.success(response.message);
                    setTimeout(function(){
                        jQuery('#building-complaints').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('#building-complaints').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                    }, 1000);
                }   else if (response.status == 'error' && response.code == 500) {
                    toastr.error(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text(value);
                    });
                }else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    /*to show other notes text box for other notes */
    $(document).on("change","#complaint_type_options",function(){
        var complaint_type = $("#complaint_type_options option:selected").text();
        if(complaint_type == "Other"){
            $("#other_notes_div").show();
        }else{
            $("#other_notes_div").hide();
        }
    });

    $("#select_all_complaint_checkbox").click(function () {
        $(".complaint_checkbox").prop('checked', $(this).prop('checked'));
    });



    /**
     *change function to perform various actions(edit ,delete)
     * @param status
     */
    jQuery(document).on('change','#building-complaints .select_options',function () {
        var select_options = $(this).val();
        var data_id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index() ;
        if(select_options == 'Edit')
        {
            $(".clear_complaint").hide();
            $('.reset_complaint').show();
            $('#building-complaints').find('.green_row_left, .green_row_right').each(function(){
                $(this).removeClass("green_row_left green_row_right");
            });
            jQuery('#building-complaints').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
            jQuery('#building-complaints').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
            $('#new_complaint :input').val('');
            $('#new_complaint').show(500);
            $('#edit_complaint_id').val(data_id);
            $('#complaint_save').html('Update');
            $('#complaint_id').prop('readonly','readonly');


            $.ajax({
                type: 'post',
                url: '/building-ajax',
                data: {class: 'buildingDetail', action: 'getComplaintDetail', id: data_id},
                success : function(response){
                    var response =  JSON.parse(response);
                    if(response.status == 'success' && response.code == 200) {

                        $.each(response.data.data, function (key, value) {
                            $('#'+key+ ':input').val(value);
                            if(key == 'complaint_type_id' && value!=''){
                                $('#complaint_type_options option[value='+value+']').attr('selected','selected');
                            }

                        });
                        setTimeout(function () {
                            edit_date_time(response.data.data.updated_at);
                        },2000);
                    } else if(response.status == 'error' && response.code == 503) {
                        toastr.error(response.message);
                    }else {
                        toastr.warning('Record not updated due to technical issue.');
                    }
                    defaultFormData = getDivInputs('#complaints');
                    if(response.code == 200){
                        defaultFormData.push({'name':"complaint_type_options",value:response.data.data.complaint_type_id})
                        defaultFormData.push({'name':"complaint_note",value:response.data.data.complaint_note});
                        defaultFormData.push({'name':"other_notes",value:response.data.data.other_notes});
                    }
                }
            });
           // $("#building-complaints").trigger('reloadGrid');
            // window.location.href = '/MasterData/AddPropertyType';

        }else if (select_options == 'Print Envelope'){
            $.ajax({
                type: 'post',
                url: '/get-company-data',
                data: {class: 'buildingDetail', action: 'getCompanyData','building_id':data_id},
                success : function(response){
                    var response =  JSON.parse(response);
                    if(response.status == 'success' && response.code == 200) {
                        $("#PrintEnvelope").modal('show');
                        $("#company_name").text(response.data.data.company_name)
                        $("#address1").text(response.data.data.address1)
                        $("#address2").text(response.data.data.address2)
                        $("#address3").text(response.data.data.address3)
                        $("#address4").text(response.data.data.address4)
                        $(".city").text(response.data.data.city)
                        $(".state").text(response.data.data.state)
                        $(".postal_code").text(response.data.data.zipcode)
                        $("#building_name").text(response.building.data.building_name)
                        $("#building_address").text(response.building.data.address)


                    }else {
                        toastr.warning('Record not updated due to technical issue.');
                    }
                }
            });


        }else if(select_options == 'Delete') {
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/building-ajax',
                            data: {class: 'buildingDetail', action: 'deleteComplaint', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Complaint deleted successfully.');
                                } else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                }else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    $("#building-complaints").trigger('reloadGrid');
                }
            });
        }
        $('.select_options').prop('selectedIndex',0);
    });

    $(document).on('click', '.addcheckoutcolorblue', function () {
        var building_id =  $("#edit_building_id").val();
        $("#return-key-div-history").modal('show');
        var propertyEditid = $("#property_editunique_id").val();
        var id = $(this).attr('rel');
        $.ajax({
            type: 'post',
            url: '/building-ajax',
            data: {
                building_id: building_id,
                id: id,
                class: 'buildingDetail',
                action: 'getReturnkeyDatahistory'},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.code == 200) {
                    $("#tbodyCheckOutDetailhistory").html(response.html);
                } else if (response.status == "error") {
                    toastr.error(response.message);
                } else {
                    toastr.error(response.message);
                }
                $('#key-table').trigger('reloadGrid');
            }
        });
    });

});

/*trigger to reload files jqgrid*/
function triggerFileReload(){
    var grid = $("#buildingFileLibrary-table");
    grid[0].p.search = false;
    $.extend(grid[0].p.postData,{filters:""});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}

/*trigger to reload images jqgrid*/
function triggerPhotoReload(){
    var grid = $("#buildingPhotovideos-table");
    grid[0].p.search = false;
    $.extend(grid[0].p.postData,{filters:""});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}

var key_tracker_checkbox_val = $("input[name='@key_tracker']:checked").val()
if(key_tracker_checkbox_val == "add_key"){
    $("#add_key_grid").show();
    $("#track_key_grid").hide()
}else{
    $("#add_key_grid").hide();
    $("#track_key_grid").show()
}

$(document).on("change",'.key_tracker_radio',function(){
    if($(this).val() == "add_key"){
        $("#add_key_grid").show();
        $("#track_key_grid").hide()
    }else{
        $("#add_key_grid").hide();
        $("#track_key_grid").show()
        $("#KeycheckoutDiv").hide();
    }
});

$("#pick_up_date").datepicker({dateFormat: date_format,
    changeYear: true,
    minDate: 0,
    yearRange: "-100:+20",
    changeMonth: true,
    onClose: function (selectedDate) {
        $("#return_date").datepicker("option", "minDate", selectedDate);
    }}).datepicker("setDate", new Date());

// $("#pick_up_time").timepicker({ timeFormat: 'h:mm p',defaultTime: new Date()});
$("#return_date").datepicker({dateFormat: date_format, changeYear: true,
    minDate: 0,
    yearRange: "-100:+20",
    changeMonth: true,
    onClose: function (selectedDate) {
        $("#return_date").datepicker("option", "maxDate", selectedDate);
    }}).datepicker("setDate", new Date());


// $("#return_time").datepicker({dateFormat: datepicker});
// $("#return_time").timepicker({ timeFormat: 'h:mm p',defaultTime: new Date()});

/*function to save new complaint */
$(document).on('click','#KeyTrackSave', function (e) {
    e.preventDefault();
    var formData = $('#track_key_tracker :input').serializeArray();
    var building_id =  $("#edit_building_id").val();

    $.ajax({
        type: 'post',
        url: '/building-ajax',
        data: {form: formData,
            class: 'buildingDetail',
            action: 'editTrackKeys',
            edit_building_id:building_id
        },

        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidateTrackKey").each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });

        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#new_complaint').hide(500);
                $("#building-track-keys").trigger('reloadGrid');
                $("#track_key_tracker").hide();
                $('#track_key_tracker :input').val('');
                setTimeout(function(){
                    jQuery('#building-track-keys').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('#building-track-keys').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                }, 1000);
                toastr.success(response.message);
            }   else if (response.status == 'error' && response.code == 500) {
                toastr.error(response.message);
                return false;
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('#' + key).text(value);
                });
            }else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});


//add announcement
$("#edit_building").validate({
    rules: { building_id: {
            required: true
        },building_name: {
            required: true
        },address: {
            required: true
        },address: {
            required: true
        },total_keys: {
            number: true
        },no_of_units: {
            required: true,
            number: true,
            min: 1
        },unit_type: {
            required: true,
        },base_rent: {
            required: true,
            number: true,
        },market_rent: {
            required: true,
            number: true,
        },security_deposit: {
            required: true,
            number: true,
        },key_name: {
            required: true,
        },email: {
            email: true

        }

    },
    messages: {
        property_type: {
            required: "* This field is required",
        },
        no_of_units: {
            min: "* Minimum value is 1",
        }
    },
    submitHandler: function (form) {
       // event.preventDefault();
        var uploadform = new FormData();
        var formData = $('#edit_building').serialize();
        var form_type = $("#form_type").val()
        var action = '';
        var custom_field = [];
        combine__data_photo_document_array=[];
        combine__data_photo_document_array=[];
        $(".custom_field_html input").each(function(){
            var data = {'name':$(this).attr('name'),'value':$(this).val(),'id':$(this).attr('data_id'),'is_required':$(this).attr('data_required'),'data_type':$(this).attr('data_type'),'default_value':$(this).attr('data_value')};
            custom_field.push(data);
        });
        var  combine_photo_document_array =[];
        var length = $('#photo_video_uploads > div').length;
        var length1 = $('#file_library_uploads > div').length;

        // console.log(combine_photo_document_array);
        // console.log(file_library); return false;
        combine_photo_document_array =  $.merge( photo_videos, file_library );
        var dataaa = Array.from(new Set(combine_photo_document_array)); // #=> ["foo", "bar"]

        if(length > 0 || length1 > 0 ) {

            var data = convertSerializeDatatoArrayPhotos();
            var data1 = convertSerializeDatatoArray();
            var combine__data_photo_document_array =  $.merge( data, data1 );
            // var count = photo_videos.length;

            $.each(dataaa, function (key, value) {
                if(compareArray(value,combine__data_photo_document_array) == 'true'){
                    uploadform.append(key, value);
                }
                // if(key+1 === count){
                //     savePhotoVideos(uploadform);
                // }
            });
        }
        uploadform.append('class', "buildingDetail");
        uploadform.append('action', "addBuilding");
        uploadform.append('custom_field', JSON.stringify(custom_field));
        uploadform.append('form', formData);

        $.ajax
        ({
            type: 'post',
            url: '/Building/AddBuildingDetail',
            processData: false,
            contentType: false,
            data: uploadform,
            beforeSend: function (xhr) {
                // checking portfolio validations

                var radioValue = $("input[name='@key_tracker']:checked").val();
                if(radioValue == 'track_key'  && $("#track_key_tracker").is(":visible")) {
                    // checking portfolio validations
                    $(".customValidateTrackKey").each(function () {
                        var res = validations(this);
                        if (res === false) {
                            xhr.abort();
                            return false;
                        }
                    });
                }
            },
            success: function (response) {
                var response = JSON.parse(response);
                var base_url = window.location.origin;
                if(response.status == 'success' && response.code == 200){
                    toastr.success(response.message);
                    window.location.href = base_url+'/Building/BuildingModule?id='+response.property_id

                } else if(response.status == 'error' && response.code == 400){
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.'+key).html(value);
                    });
                }else{
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});

function compareArray(data,compare){
    for(var i =0;i < compare.length;i++){
        if(compare[i].name == data['name'] && compare[i].size == data['size']){
            return 'true';
        }
    }
    return 'false';
}

/**
 * jqGrid function to format status
 * @param status
 */
function statusFmatter (cellvalue, options, rowObject){
    if (cellvalue == 1)
        return "Active";
    else if(cellvalue == 0)
        return "InActive";
    else
        return '';
}


/**
 * jqGrid function to format avialable keys
 * @param status
 */
function avialablekeys (cellvalue, options, rowObject){
    if (cellvalue == 1)
        return "Active";
    else if(cellvalue == 0)
        return "InActive";
    else
        return '';
}

/*function to print element by id */
function PrintElem(elem)
{
    Popup($(elem).html());
}
/*function to print element by id */
function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.body).html( '<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if(mywindow.close()){

    }
    $("#print_complaint").modal('hide');
    $("#building-complaints").trigger('reloadGrid');
    return true;
}

/*function to print element by id */
function sendComplaintsEmail(elem)
{
    SendMail($(elem).html());
}


function SendMail(data){
    $.ajax({
        type: 'post',
        url: '/building-ajax',
        data: {class: 'buildingDetail', action: 'SendComplaintMail', data_html: data},
        success : function(response){
            var response =  JSON.parse(response);
            if(response.status == 'success' && response.code == 200) {
                toastr.success('Mail send successfully.');
                $("#building-complaints").trigger('reloadGrid');
            }else {
                toastr.warning('Mail not send due to technical issue.');
            }
        }
    });
}

/**
 * jqGrid function to format action column
 * @param status
 */

function actionsFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var editable = $(cellvalue).attr('editable');
        var select = '';
        select = ['Edit','Delete'];
        var data = '';

        if(select != '') {

            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                if(editable == '0' && (val == 'delete' || val == 'Delete')){
                    return true;
                }
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}

/**
 * jqGrid function to format action column
 * @param status
 */

function actionBuildingListFmatter (cellvalue, options, rowObject){

    if(rowObject !== undefined) {
        var editable = $(cellvalue).attr('editable');
        var select = '';
        if(rowObject.status == 1)  select = ['Edit','Flag Bank','Inspection','Add In-Touch','In-Touch History','Deactivate','Print Envelope','Delete'];
        if(rowObject.status == 0 || rowObject.Status == '')  select = ['Edit','Inspection','Add In-Touch','Activate','Print Envelope'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                if(editable == '0' && (val == 'delete' || val == 'Delete')){
                    return true;
                }
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}

/**
 * jqGrid function to format checkbox column
 * @param status
 */

function actionCheckboxFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {

        var data = '';
        var data = '<input type="checkbox" name="complaint_checkbox[]" class="complaint_checkbox" id="complaint_checkbox_'+rowObject.id+'" data_id="' + rowObject.id + '"/>';
        return data;
    }
}

/*function to go back to previous page */
function goBack() {
    window.history.back();
}


function fetchAllUnittypes(id) {
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllUnittypes'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#unit_type').html(res.data);
            if (id != false) {
                $('#unit_type').val(id);

            }
            $("#default_rent").val(response.default_rent);

        },
    });

}

function validateCustomField(data_value, data_required, arg, element, id) {
    var msg = '';
    if (data_value == "") {
        if (data_required == 1) {
            msg = 'This field is required';
            $('#' + id).after("<span>" + msg + "</span>");
            return false;
        }
    }
    if (arg == 'text') {
        if (data_value.length > 150) {
            msg = 'Please enter character less than 150';
            $('#' + id).next('.customError').text(msg);
            return false;
        }

    } else if (arg == 'number') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            $('#' + id).next('.customError').text(msg);
            return false;
        } else {
            if (data_value > 20) {
                msg = 'Please enter less than 20';
                $('#' + id).next('.customError').text(msg);
                return false;
            }
        }
    } else if (arg == 'currency') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';

            $('#' + id).next('.customError').text(msg);
            return false;
        }

    } else if (arg == 'percentage') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';

            $('#' + id).next('.customError').text(msg);
            return false;
        } else {
            if (data_value > 100) {
                msg = 'Maximum value is 100';
                $('#' + id).next('.customError').text(msg);
                return false;
            }
        }
    } else if (arg == 'url') {
        var expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm;
        var regex = new RegExp(expression);
        if (data_value.match(regex)) {
        } else {
            msg = 'Please enter a valid URL';
            $('#' + id).next('.customError').text(msg);
            return false;
        }
    }
    $('#' + id).next('.customError').text('');
    return true;
}
