//add key tag
$(document).ready(function() {

    $("#total_keys").val('5');

    jqGridTrackkey();


    $(document).on("click",'#keySaveBtnId',function(e){
    e.preventDefault();
        var formData = $('#add_key_tracker :input').serializeArray();
        var button_value = $('#keySaveBtnId').html();
        var building_id = $("#edit_building_id").val();
        var id= $("#key_edit_id").val();


        $.ajax({
            type: 'post',
            url: '/add-building-keytag',
            data: {form: formData,
                edit_building_id:building_id,
                class: 'buidingDetail',
                action: 'addKeytag',
                key_edit_id: id

            },beforeSend: function (xhr) {
                // checking portfolio validations
                $(".customValidatekeys").each(function() {
                    var res = validations(this);
                    if(res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success') && (button_value == 'Save')) {
                    $("#add_key_tracker").hide(500);
                    toastr.success('This record saved successfully.');
                    triggerkeysReload();
                    setTimeout(function(){
                        jQuery('#building-keys').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('#building-keys').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                    }, 1000);

                } else if ((response.status == 'success') && (button_value == 'Update')) {
                    $("#add_key_tracker").hide(500);
                    toastr.success('This record Updated successfully.');
                    triggerkeysReload();
                    setTimeout(function(){
                        jQuery('#building-keys').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('#building-keys').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                    }, 1000);
                } else if (response.status == 'error' && response.code == 500) {
                    toastr.warning(response.message);
                } else {
                    toastr.success('failed.Please try again.');
                }
                $("#key-table").trigger('reloadGrid');
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
});


    $(document).on('change', '#building-keys .select_options', function () {
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var formData = $('#add_key_tracker :input').serializeArray();
        var row_num = $(this).parent().parent().index() ;
        $('#AddKeyButton').text('Update');
        if (opt == 'Edit' || opt == 'EDIT') {
            $('#add_key_tracker :input').val('');
            $("#KeycheckoutDiv").hide();
            $('#KeycheckoutDiv :input').val('');

            $('#building-keys').find('.green_row_left, .green_row_right').each(function(){
                $(this).removeClass("green_row_left green_row_right");
            });
            jQuery('#building-keys').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
            jQuery('#building-keys').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
            $("#clear_add_key").hide();
            $("#reset_update_key").show();
            $.ajax({
                type: 'post',
                url: '/get-building-key',
                data: {
                    form: formData,
                    id: id,
                    class: 'buildingDetail',
                    action: 'getbuildingKey'},
                success: function (response) {
                    $("#add_key_tracker").show(500);
                    $('#keySaveBtnId').html('Update');
                    $("#edit_key_id").val(id);
                    var data = $.parseJSON(response);
                    if (data.code == 200) {
                        $("#key_id").val(data.data.key_id);
                        $("#key_description").val(data.data.key_description);
                        $('#total_keys').val(data.data.total_keys);
                    } else if (data.status == "error") {
                        toastr.error(data.message);
                    } else {
                        toastr.error(data.message);
                    }
                   // defaultFormData = $('#add_key_tracker').serializeArray();
                    defaultFormData = getDivInputs('#add_key_tracker');


                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
           // $("#building-keys").trigger('reloadGrid');
        } else if (opt == 'Delete' || opt == 'DELETE') {
            opt = opt.toLowerCase();
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/building-ajax',
                            data: {
                                id: id,
                                class: 'buildingDetail',
                                action: 'deleteKey'
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                } else if (response.code == 500) {
                                    toastr.warning(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                                $('#building-keys').trigger('reloadGrid');
                            }
                        });
                    }

                }
            });
        }
        else if (opt == 'checkout' || opt == 'CHECKOUT') {
            $("#KeycheckoutDiv").show();
            $('#KeycheckoutDiv :input').val('');
            $("#add_key_tracker").hide();
            $("#edit_key_id").val(id);


        }
        else if (opt == 'return' || opt == 'RETURN') {
            $("#return-key-div").modal('show');
            var building_id = $("#edit_building_id").val();
            $.ajax({
                type: 'post',
                url: '/building-ajax',
                data: {
                    building_id:building_id,
                    id: id,
                    class: 'buildingDetail',
                    action: 'getReturnkeyData'},
                success: function (response) {
                    var response =  JSON.parse(response);
                    if (response.code == 200) {
                        $("#tbodyCheckOutDetail").html(response.html);
                        console.log(response.html);
                    } else if (response.status == "error") {
                        toastr.error(response.message);
                    } else {
                        toastr.error(response.message);
                    }
                    $('#building-keys').trigger('reloadGrid');
                }
            });
        }
        $('.select_options').prop('selectedIndex',0);
    });

    //combo grid for owner_pre_vendor
    $('.owner_pre_vendor').combogrid({
        panelWidth: 500,
        url: '/get-vendorsdata',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getVendorsdata',
            class: 'propertyDetail'
        },
        columns: [[
            {field: 'id', hidden: true},
            {field: 'name', title: 'Name', width: 60},
            {field: 'address1', title: 'Address', width: 120},
            {field: 'email', title: 'Email', width: 120},
        ]],
        onSelect: function (index, row) {
            var desc = row.address1;  // the product's description
            $("#vendor_address").val(desc);
            $("#vendor_name").val(row.name);
            $("#key_holder_id").val(row.id);
            $("#vendor_address").attr("disabled", false);
        }
    });

    //combo grid for owner_pre_vendor
    $('.owner_pre_vendor1').combogrid({
        panelWidth: 500,
        url: '/get-vendorsdata',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getVendorsdata',
            class: 'propertyDetail'
        },
        columns: [[
            {field: 'id', hidden: true},
            {field: 'name', title: 'Name', width: 60},
            {field: 'address1', title: 'Address', width: 120},
            {field: 'email', title: 'Email', width: 120},
        ]],
        onSelect: function (index, row) {
            var desc = row.address1;  // the product's description
            $("#vendor_address").val(desc);
            $("#vendor_name").val(row.name);
            $("#track_key_holder_id").val(row.name);
            $("#vendor_address").attr("disabled", false);
        }
    });
    $("#_easyui_textbox_input1").attr("placeholder", "Click here to pick a vendor");

    $("#_easyui_textbox_input1").css("width",'100%');
    $("#_easyui_textbox_input1").css("height",'34px');

    $(document).on('click','#KeycheckoutSave',function(e){

    e.preventDefault();
    var formData = $('#KeycheckoutDiv :input').serializeArray();
    var building_id = $("#edit_building_id").val();
    var key_id = $("#edit_key_id").val();
    $.ajax({
        type: 'post',
        url: '/building-ajax',
        data: {
            form: formData,
            id: key_id,
            class: 'buildingDetail',
            action: 'keyCheckout',
            building_id:building_id
        },
        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidatecheckoutkeys").each(function() {
                var res = validations(this);
                if(res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (response) {
            var response =  JSON.parse(response);
            if (response.code == 200) {
                toastr.success(response.message);
                $("#KeycheckoutDiv:input").val('');
                $("#KeycheckoutDiv").hide(500);
            } else if (response.code == 500) {
                toastr.error(response.message);
            }
            else if(response.code == 400){
                toastr.error(response.message);
            }
            $('#building-keys').trigger('reloadGrid');
        }
    });
});

    //return key checkout
    $(document).on("click", "#returnbuttonId", function (e) {
        e.preventDefault();

        var val = [];
        $('.chkKeyReturn:checkbox:checked').each(function(i){
            val[i] = $(this).val();
        });
        var building_id =  $("#edit_building_id").val();
        var formData = $('#returnkeyForm :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/building-ajax',
            data: {
                class: 'buildingDetail',
                action: 'returnCheckout',
                val: val,
                building_id:building_id
            },
            success: function (result) {
                var response = JSON.parse(result);
                if (response.status == 'success') {
                    $('#return-key-div').modal('hide');
                    toastr.success('This record saved successfully.');
                } else if (response.status == 'error' && response.code == 500) {
                    toastr.warning(response.message);
                } else {
                    toastr.success('failed.Please try again.');
                }
                $("#building-keys").trigger('reloadGrid');
            },
        });
    });

    //track key lising

    function jqGridTrackkey() {
        var table = 'building_track_key';
        var columns = ['Name', 'Email', 'Company Name', 'Phone#', 'Pick Up Date', 'Pick Up Time', 'Return Date', 'Return Time', 'Key Designator', 'Action'];
        var select_column = ['Edit', 'Delete'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['building_track_key.deleted_at', 'building_track_key.updated_at'];
        var columns_options = [
            {name: 'Name', index: 'key_holder', width: 100, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Email', index: 'email', width: 400, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Company Name', index: 'company_name', width: 400, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Phone#', index: 'phone', title: false, width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Pick Up Date', index: 'pick_up_date', title: false, width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Pick Up Time', index: 'pick_up_time', title: false, width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Return Date', index: 'return_date', title: false, width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Return Time', index: 'return_time', title: false, width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Key Designator', index: 'key_designator', title: false, width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Action', index: 'select', title: false, width: 200, align: "center", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table,formatter:actionBuildingFmatter,}

        ];
        var ignore_array = [];
        jQuery("#building-track-keys").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Notes",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }

    /**
     * jqGrid function to format action column
     * @param status
     */

    function actionBuildingFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var editable = $(cellvalue).attr('editable');
            var select = '';
            select = ['<span><i class="fa fa-edit edit_track_key" data_id="' + rowObject.id + '" style="font-size:24px"></i></span>','<span><i data_id="' + rowObject.id + '" class="fa fa-window-close delete_track_key " style="font-size:24px" aria-hidden="true"></i></span>'];

            var data = '';
            if(select != '') {
                $.each(select, function (key, val) {
                    if(editable == '0' && (val == 'delete' || val == 'Delete')){
                        return true;
                    }
                    data += val
                });
            }
            return data;
        }
    }

    //track key
    $(document).on('change', '#building-track-keys .select_options', function () {
      // /  alert('fdsfds');
        $("#track_key_tracker").show();
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        $("#edit_track_key_id").val(id);
        var formData = $('#track_key_tracker :input').serializeArray();
        $('#KeyTrackSave').text('Update');
        if (opt == 'Edit' || opt == 'EDIT') {
            $.ajax({
                type: 'post',
                url: '/building-ajax',
                data: {
                    form: formData,
                    id: id,
                    class: 'buildingDetail',
                    action: 'getTrackKey'},

                success: function (response) {


                    $('#TrackKeyButton').val('Update');
                    $("#trackkey_id").val(id);
                    var data = $.parseJSON(response);
                    if (data.code == 200) {
                        $('.owner_pre_vendor1').combogrid('setValue',data.data.key_holder);
                        $(".trackkey_name").val(data.data.key_name);
                        $("#trackemail").val(data.data.email);
                        $('#trackcompany_name').val(data.data.company_name);
                        $('#trackphone').val(data.data.phone);
                        $('#trackAddress1').val(data.data.Address1);
                        $('#trackAddress2').val(data.data.Address1);
                        $('#trackAddress3').val(data.data.Address1);
                        $('#trackAddress4').val(data.data.Address1);
                        $('#trackkey_number').val(data.data.key_number);
                        $('#trackkey_quality').val(data.data.key_quality);
                        $("#pick_up_date").datepicker("setDate", data.data.pick_up_date);
                        $("#pick_up_time").timepicker("destroy");
                        $("#pick_up_time").timepicker({ timeFormat: 'h:mm p',defaultTime: data.data.pick_up_time});
                        $("#return_date").datepicker("setDate", data.data.return_date);
                        $("#return_time").timepicker({ timeFormat: 'h:mm p',defaultTime: data.data.return_time});
                        $('#key_designator').val(data.data.key_designator);
                    } else if (data.status == "error") {
                        toastr.error(data.message);
                    } else {
                        toastr.error(data.message);
                    }
                    defaultFormData = getDivInputs('#track_key_tracker');
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
            $("#trackkey-table").trigger('reloadGrid');
        } else if (opt == 'Delete' || opt == 'DELETE') {
            opt = opt.toLowerCase();
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/building-ajax',
                            data: {
                                id: id,
                                class: 'buildingDetail',
                                action: 'deleteTrackKey'
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                } else if (response.code == 500) {
                                    toastr.warning(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                                $('#building-track-keys').trigger('reloadGrid');
                            }
                        });
                    }
                    $('#building-track-keys').trigger('reloadGrid');
                }
            });
        }
        $('.select_options').prop('selectedIndex',0);
    });


    /*redirect to edit page */
    $(document).on("click",".edit_track_key",function(){

        $("#clear_track_key").hide();
        $("#reset_update_track_key").show();
        var row_num = $(this).closest('tr').index()
        $('#building-track-keys').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        jQuery('#building-track-keys').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('#building-track-keys').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
        // var id = $(this).attr('data_id');
        $("#track_key_tracker").show();
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        $("#edit_track_key_id").val(id);

        var formData = $('#track_key_tracker :input').serializeArray();
        $('#KeyTrackSave').text('Update');
        $.ajax({
            type: 'post',
            url: '/building-ajax',
            data: {
                form: formData,
                id: id,
                class: 'buildingDetail',
                action: 'getTrackKey'},

            success: function (response) {


                $('#TrackKeyButton').val('Update');
                $("#trackkey_id").val(id);

                var data = $.parseJSON(response);
                if (data.code == 200) {


                    $('.owner_pre_vendor1').combogrid('setValue',data.data.key_holder);
                    $("#_easyui_textbox_input2").val(data.data.key_holder);
                    $(".trackkey_name").val(data.data.key_name);
                    $("#trackemail").val(data.data.email);
                    $('#trackcompany_name').val(data.data.company_name);
                    $('#trackphone').val(data.data.phone);
                    $('#trackAddress1').val(data.data.Address1);
                    $('#trackAddress2').val(data.data.Address1);
                    $('#trackAddress3').val(data.data.Address1);
                    $('#trackAddress4').val(data.data.Address1);
                    $('#trackkey_number').val(data.data.key_number);
                    $('#trackkey_quality').val(data.data.key_quality);
                    $("#pick_up_date").datepicker("setDate", data.data.pick_up_date);
                    $("#pick_up_time").timepicker("destroy");
                    $("#pick_up_time").timepicker({ timeFormat: 'h:mm p',defaultTime: data.data.pick_up_time});
                    $("#return_date").datepicker("setDate", data.data.return_date);
                    $("#return_time").timepicker({ timeFormat: 'h:mm p',defaultTime: data.data.return_time});
                    $('#key_designator').val(data.data.key_designator);



                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }

                defaultFormData = getDivInputs('#track_key_tracker');

                if(data.code == 200){
                    defaultFormData.push({'name':"_easyui_textbox_input2",value:data.data.key_holder})

                }

                console.log(defaultFormData);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
       // $("#trackkey-table").trigger('reloadGrid');

    });

    /*ajax to deactivate building */

    $(document).on("click",".delete_track_key",function(){
        var id = $(this).attr('data_id');
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/building-ajax',
                        data: {
                            id: id,
                            class: 'buildingDetail',
                            action: 'deleteTrackKey'
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else if (response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                            $('#building-track-keys').trigger('reloadGrid');
                        }
                    });
                }
                $('#building-track-keys').trigger('reloadGrid');
            }
        });

    });


});

/*trigger to reload keys jqgrid*/
function triggerkeysReload(){
    var grid = $("#building-keys");
    grid[0].p.search = false;
    $.extend(grid[0].p.postData,{filters:""});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}