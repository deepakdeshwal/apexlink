var table_green_id  = localStorage.getItem('table_green_id');
var table_green_tableid  = localStorage.getItem('table_green_tableid');
localStorage.removeItem('table_green_id');
localStorage.removeItem('table_green_tableid');
$('.to').tagsinput({
    // typeahead: {
    //     source: function(query,process) {
    //         var query = {
    //             "search": query,
    //             "action": 'getUsersSearch',
    //             "class": 'getUsersSearch'
    //         };
    //         // return $.get('/Communication/ComposeMailAjax',query, function (data) {
    //         //     console.log(data);
    //         //     data = $.parseJSON(data);
    //         //     return process(data);
    //         // })
    //
    //         $.ajax({
    //             url:'/Communication/ComposeMailAjax',
    //             type: 'POST',
    //             data: query,
    //             async:false,
    //             success: function (response) {
    //                 return response;
    //             }
    //         });
    //     }
    //
    // },
    freeInput: true
});
$('.to_name').tagsinput({
    // typeahead: {
    //     source: function(query,process) {
    //         var query = {
    //             "search": query,
    //             "action": 'getUsersSearch',
    //             "class": 'getUsersSearch'
    //         };
    //         // return $.get('/Communication/ComposeMailAjax',query, function (data) {
    //         //     console.log(data);
    //         //     data = $.parseJSON(data);
    //         //     return process(data);
    //         // })
    //
    //         $.ajax({
    //             url:'/Communication/ComposeMailAjax',
    //             type: 'POST',
    //             data: query,
    //             async:false,
    //             success: function (response) {
    //                 return response;
    //             }
    //         });
    //     }
    //
    // },
    freeInput: true
});
$('.to_usertype').tagsinput();
function process(data)
{
    console.log(data)
    return data;
}


$('.to').on('itemAdded', function(event) {
    setTimeout(function(){
        $(">input[type=text]",".bootstrap-tagsinput").val("");
    }, 1);
});

$(".popup_to").on('itemRemoved', function(event) {
    autoRemoveEmailCheck(event.item);
    setTimeout(function () {
        var to = $('.to').val();
        $(".popup_to").tagsinput('remove', event.item);
        $(".to").tagsinput('remove', event.item);
    },200);
});

$('.to').on('itemRemoved', function(event) {
    setTimeout(function () {
        var to = $('.to').val();
        $(".popup_to").tagsinput('remove', event.item);
        $(".to").tagsinput('remove', event.item);
    },200);
});

$('.popup_to').tagsinput();
$('.to_name').tagsinput();

$('.bootstrap-tagsinput input').removeAttr('style');
$('.bootstrap-tagsinput').css('width','100%');
$('input.form-control.subject').css('margin-bottom','10px');
getView();

setTimeout(function () {
    $('.to_field .bootstrap-tagsinput input').focus();
},500);

function getView()
{
    var compose_mail_id = localStorage.getItem("composer_message_id");
    console.log(compose_mail_id);
    var composer_mail_type = localStorage.getItem("composer_message_type");
    // console.log("compose_mail_id = "+compose_mail_id);
    $.ajax({
        url:'/Communication/composeTextMessageAjax',
        method: 'post',
        data: {
            class: "composeVendorTextMessageAjax",
            action: "getComposeForView",
            'id': compose_mail_id,
        },
        success: function (data) {
            localStorage.removeItem('composer_message_id');
            localStorage.removeItem('composer_message_type');
            info =  JSON.parse(data);
            if(info.status=="success"){
                var res = info.data;
                if(composer_mail_type == 'draft')
                {
                    if(res.email_to)
                    {
                        $('.to').tagsinput('add', res.email_to);
                        $('.to_name').tagsinput('add', res.user_name+'>'+res.email_to);
                        $('.to_usertype').tagsinput('add', res.selected_user_type+'>'+res.email_to);
                    }
                    $('#compose_mail_id').val(res.id);
                }
                if(composer_mail_type == 'forward')
                {
                    // $("#compose_mail_id").val("");
                    localStorage.removeItem('composer_message_id')
                }
                $('.subject').val(res.email_subject);
                $('#mesgbody').text(res.email_message);
                var count_elem = $('#mesgbody').val();
                count_elem =  parseInt(300) - parseInt(count_elem.length);
                $('#chracter_count').text(count_elem);
            }
        },
    });
}

$(document).ready(function () {
    $(".slide-toggle").click(function () {
        $(".box").animate({
            width: "toggle"
        });
    });

    $(".slide-toggle2").click(function () {
        $(".box2").animate({
            width: "toggle"
        });
    });

    $('.summernote').summernote({
        addclass: {
            debug: false,
            classTags: [{title:"Button","value":"btn btn-success"},"jumbotron", "lead","img-rounded","img-circle", "img-responsive","btn", "btn btn-success","btn btn-danger","text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
        },
        width: '100%',
        height: '300px',
        //margin-left: '15px',
        toolbar: [
            // [groupName, [list of button]]
            ['img', ['picture']],
            ['style', ['style', 'addclass', 'clear']],
            ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
            ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
            ['extra', ['video', 'table', 'height']],
            ['misc', ['undo', 'redo', 'codeview', 'help']]
        ]
    });

    /*local storage guest card phone number */
    if(localStorage.getItem("phoneNumber_guest")) {
        var phone = localStorage.getItem("phoneNumber_guest");
        $("#sendEmail .to_field input").val(phone);
        $("#sendEmail .to_field input").focusout();
        localStorage.removeItem('phoneNumber_guest');


        //localStorage.removeItem('scrollDown');
    }
    /*local storage guest card phone number */
});
//Summernote JS Ends


$("#sendEmail").validate({
    ignore: ".mesgbody",
    rules: {
        to: {
            required:true,
            number:true
        },
        mesgbody: {
            required:true
        }

    },
    submitHandler: function (e) {

        var tenant_id = $(".tenant_id").val();
        var form = $('#sendEmail')[0];
        var formData = new FormData(form);
        var to = $(".to").val();
        formData.append('to_users',to);
        formData.append('action','saveEmail');
        formData.append('class','saveEmail');
        $.ajax({
            url:'/VendorPortal/Communication/ComposeVendorTextMessageAjax',
            type: 'POST',
            data: formData,
            success: function (data) {
                info =  JSON.parse(data);
                if(info.status=="success"){

                    var mail_type = $('.mail_type').val();
                    localStorage.removeItem('composer_mail_id')
                    localStorage.removeItem('composer_mail_type')
                    localStorage.removeItem('predefined_text')
                    localStorage.removeItem('predefined_mail')
                    if(mail_type == 'draft')
                    {
                        toastr.success("You Message saved successfully");
                        setTimeout(function () {
                            window.location.href='/VendorPortal/Communication/TextMessageDrafts';
                        },1000);

                    } else {
                        toastr.success(" The Message was sent successfully");
                        setTimeout(function () {
                            window.location.href='/VendorPortal/Communication/TextMessage';
                        },1000);
                    }
                    //  localStorage.setItem("Message", 'Record added successfully.');
                    localStorage.setItem('rowcolor', 'rowColor');
                } else{
                    toastr.warning(info.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});

$(document).on("click",".compose-email-btn",function(){
    $('.mail_type').val('send');
    $('#sendEmail').trigger('submit');
    $('label#mesgbody-error').remove().insertAfter('.note-editor.note-frame.panel');
    $('label#to-error').remove().insertAfter('.to_field .bootstrap-tagsinput');


});

$(document).on("click",".compose-vendor-text-save-btn",function(){
    $('.mail_type').val('draft');
    $('#sendEmail').trigger('submit');
    $('label#mesgbody-error').remove().insertAfter('.note-editor.note-frame.panel');
    $('label#to-error').remove().insertAfter('.to_field .bootstrap-tagsinput');

});

$(document).on("click",".addToRecepent",function(){
    $('#torecepents').modal('show');
});

// getUsers(2);
$(document).on("change",".selectUsers",function(){
    var type = $(this).val();
    getUsers(type);
    autoSelectEmailCheck('popup_to');

    if(type == 'Select'){
        $('.popup_values').hide();
    }else{
        $('.popup_values').show();
    }
});

function autoSelectEmailCheck(fieldClassName,) {
    var fieldClassName = '.'+fieldClassName;
    var popup_to = $(fieldClassName).val();
    popup_to = popup_to.split(',');
    if(popup_to)
    {
        $.each(popup_to,function (key,value) {
            setTimeout(function () {
                $("input[data-email='"+value+"']").prop( "checked", false );
            },200)
        });
    }
}

function autoRemoveEmailCheck(fieldClassName,) {
    $("input[data-email='"+fieldClassName+"']").prop( "checked", false );
}

function getUsers(type)
{
    $.ajax({
        url:'/Communication/composeTextMessageAjax',
        type: 'POST',
        data: {
            "type": type,
            "action": 'getUsers',
            "class": 'composeTextMessageAjax'
        },
        success: function (response) {
            $(".userDetails").html(response);

        }
    });
}

$(document).on("click",".getEmails",function(){
    var email = $(this).attr('data-email');
    var name = $(this).attr('data-name');
    var type = $(this).attr('data-utype');
    if(this.checked)
    {
        $('.popup_to').tagsinput('add', email);
        $('.to_name').tagsinput('add', name);
        $('.to_usertype').tagsinput('add', type);
    }
    else
    {
        $('.popup_to').tagsinput('remove', email);
        $('.to_name').tagsinput('remove', name);
        $('.to_usertype').tagsinput('remove', type);
    }
});

$(document).on("click","#SendselectToUsers",function(){
    var check_data = [];
    $('.getEmails:checked').each(function () {
        $('.popup_to').tagsinput('add', $(this).attr('data-email'));
        $('.to_name').tagsinput('add', $(this).attr('data-name'));
        $('.to_usertype').tagsinput('add', $(this).attr('data-utype'));
    });
    var hidden_to = $('.popup_to').val();

    if(!hidden_to)
    {
        toastr.warning('Please select atleast one recipient');
        return false;
    }

    var popup_to = $('.popup_to').val();

    $('.to').val();
    $('.to').tagsinput('add','');
    $('.to').tagsinput('add',popup_to);
    $('#torecepents').modal('hide');

    // $('.getEmails').prop('checked', false);
});



$(document).on('input','#toSearch',function () {
    var search = $(this).val();
    var search_len = search.length;
    // var user_type = '';
    var id_type = $(this).prop('id');
    var user_type = $('.selectUsers').val();

    // if(search_len >= 3)
    // {
    $.ajax({
        url:'/Communication/composeTextMessageAjax',
        type: 'POST',
        data: {
            "search": search,
            "id_type": id_type,
            "user_type": user_type,
            "action": 'getSearchUsers',
            "class": 'EditOwnerAjax'
        },
        success: function (response) {
            if(response)
            {
                if(id_type == 'bccSearch')
                {
                    $(".userBccDetails").html(response);
                } else if(id_type == 'ccSearch')
                {
                    $(".userCcDetails").html(response);
                } else if(id_type == 'toSearch')
                {
                    $(".userDetails").html(response);
                }
            }

        }
    });
    // }
});

$(document).on('click','#cancel_email',function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {

            var last_url = localStorage.getItem('table_green_url');
            if(last_url)
            {
                localStorage.removeItem('table_green_url');
                localStorage.setItem('cancelRecord',true);
                localStorage.setItem('rowcolor',true);
                localStorage.setItem('table_green_tableid',table_green_tableid);
                updateUserTable(table_green_id);
                window.location.href = last_url;

            }else {
                window.location.href = '/VendorPortal/Communication/TextMessageDrafts';
            }
        }

    });
});
$(document).on('input','#mesgbody',function () {
    var count_elem = $(this).val();
    count_elem =  parseInt(300) - parseInt(count_elem.length);
    $('#chracter_count').text(count_elem);
});
$('.to_name').next('.bootstrap-tagsinput').hide();
$('.to_usertype').next('.bootstrap-tagsinput').hide();

setTimeout(function () {
    var predefined_text = localStorage.getItem('predefined_text');
    if(predefined_text)
    {
        $.ajax({
            url:'/Communication/composeTextMessageAjax',
            type: 'POST',
            data: {
                "email": predefined_text,
                "action": 'getSearchUsersEmail',
                "class": 'EditOwnerAjax'
            },
            success: function (response) {
                response =JSON.parse(response);
                if(response)
                {
                    $('.to').tagsinput('add', response.phone_number);
                    $('.to_name').tagsinput('add', response.name+'>'+response.phone_number);
                    $('.to_usertype').tagsinput('add', response.user_type+'>'+response.phone_number);
                    $('input.form-control.to').removeAttr('autofocus');
                    $('input.form-control.subject').attr('autofocus',true);
                    $('input.form-control.subject').focus();
                }

            }
        });
        localStorage.removeItem('composer_mail_id');
        localStorage.removeItem('composer_mail_type');
        localStorage.removeItem('predefined_text');
        localStorage.removeItem('predefined_mail');
    }
},500);


function updateUserTable(userId) {
    $.ajax({
        url:'/Communication/ComposeMailAjax',
        type: 'POST',
        data: {
            "userId": userId,
            "action": 'updateUserTable',
            "class": 'updateUserTable'
        },
        success: function (response) {
            response =JSON.parse(response);
        }
    });
}