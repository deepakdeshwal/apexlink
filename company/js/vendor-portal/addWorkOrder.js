$(document).ready(function () {

        $('.owner_pre_vendor2').combogrid({
            panelWidth: 500,
            url: '/work-order-ajax',
            idField: 'name',
            textField: 'name',
            mode: 'remote',
            fitColumns: true,
            queryParams: {
                action: 'getPreffVendorsdata',
                class: 'workOrder',
                ven_type: 'preferred'
            },
            columns: [[
                {field: 'id', hidden: true},
                {field: 'name', title: 'Name', width: 60},
                {field: 'address1', title: 'Address', width: 120},
                {field: 'email', title: 'Email', width: 120},
            ]],
            onSelect: function (index, row) {
                var desc = row.address1;  // the product's description
                $("#vendor_address").text(desc);
                $("#vendor_name").val(row.name);
                $("#vendor_address").attr("disabled", false);
                $(".addressupdbtn").show();
                $(".addressupdbtn").attr('rel', row.id);

                $('.vendor_info_id').val(row.id);

                $("#ven_address3").val(row.A1);
                $("#ven_address2").val(row.A2);
                $("#ven_address1").val(row.A3);
                $("#ven_city").val(row.city);
                $("#ven_state").val(row.state);
                $("#ven_country").val(row.country);
                $("#ven_zip_code").val(row.zip);
                $("#nametable").html(row.name);
                $("#cattable").html(row.vendor_type);
                $("#emailtable").html(row.email);
                $("#phntable").html(row.phone_number);


            }
        });

    $(document).on("click",".cancel-work-order",function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = '/WorkOrder/WorkOrders';
            }
        });

    });
    $(document).on('click','.textbox-text',function(){
        var comboVal=$(".vendor-info-radio:checked").val();
        if(comboVal == 'all'){
           $(".owner_pre_vendor").combogrid('showPanel');
        }else if(comboVal == 'preferred'){
            $(".owner_pre_vendor2").combogrid('showPanel');
        }
    })
    $(".textbox-text").attr("placeholder", "Click here to pick a vendor");
    $(document).on("click",".vendor-info-radio",function() {
        $(".preferredvendorType").val(this.value);
        var vendortypeRadio = $(".preferredvendorType").val();
        if (vendortypeRadio == 'all') {
            $(".preffDiv").hide();
            $(".allDiv").show();
            //combo grid for owner_pre_vendor
            $('.owner_pre_vendor').combogrid({
                panelWidth: 500,
                url: '/work-order-ajax',
                idField: 'name',
                textField: 'name',
                mode: 'remote',
                fitColumns: true,
                queryParams: {
                    action: 'getVendorsdata',
                    class: 'workOrder',
                    ven_type: 'all'
                },
                columns: [[
                    {field: 'id', hidden: true},
                    {field: 'name', title: 'Name', width: 60},
                    {field: 'address1', title: 'Address', width: 120},
                    {field: 'email', title: 'Email', width: 120},
                ]],
                onSelect: function (index, row) {
                    var desc = row.address1;  // the product's description
                    $("#vendor_address").text(desc);
                    $("#vendor_name").val(row.name);
                    $("#vendor_address").attr("disabled", false);
                    $(".addressupdbtn").show();
                    $(".addressupdbtn").attr('rel', row.id);

                    $('.vendor_info_id').val(row.id);

                    $("#ven_address3").val(row.A1);
                    $("#ven_address2").val(row.A2);
                    $("#ven_address1").val(row.A3);
                    $("#ven_city").val(row.city);
                    $("#ven_state").val(row.state);
                    $("#ven_country").val(row.country);
                    $("#ven_zip_code").val(row.zip);
                    $("#nametable").html(row.name);
                    $("#cattable").html(row.vendor_type);
                    $("#emailtable").html(row.email);
                    $("#phntable").html(row.phone_number);


                }
            });

            $(".textbox-text").attr("placeholder", "Click here to pick a vendor");
        }
        if (vendortypeRadio == 'preferred') {
            $(".preffDiv").show();
            $(".allDiv").hide();
            $('.owner_pre_vendor2').combogrid({
                panelWidth: 500,
                url: '/work-order-ajax',
                idField: 'name',
                textField: 'name',
                mode: 'remote',
                fitColumns: true,
                queryParams: {
                    action: 'getPreffVendorsdata',
                    class: 'workOrder',
                    ven_type: 'preferred'
                },
                columns: [[
                    {field: 'id', hidden: true},
                    {field: 'name', title: 'Name', width: 60},
                    {field: 'address1', title: 'Address', width: 120},
                    {field: 'email', title: 'Email', width: 120},
                ]],
                onSelect: function (index, row) {
                    var desc = row.address1;  // the product's description
                    $("#vendor_address").text(desc);
                    $("#vendor_name").val(row.name);
                    $("#vendor_address").attr("disabled", false);
                    $(".addressupdbtn").show();
                    $(".addressupdbtn").attr('rel', row.id);

                    $('.vendor_info_id').val(row.id);

                    $("#ven_address3").val(row.A1);
                    $("#ven_address2").val(row.A2);
                    $("#ven_address1").val(row.A3);
                    $("#ven_city").val(row.city);
                    $("#ven_state").val(row.state);
                    $("#ven_country").val(row.country);
                    $("#ven_zip_code").val(row.zip);
                    $("#nametable").html(row.name);
                    $("#cattable").html(row.vendor_type);
                    $("#emailtable").html(row.email);
                    $("#phntable").html(row.phone_number);


                }
            });
            $(".textbox-text").attr("placeholder", "Click here to pick a vendor");
        }
    });
    $("#FormWorkOrder").validate({
        rules: {
            property_id: {
                required: true
            },
            building_id: {
                required: true
            },
            unit_id :{
                required: true
            },
            portfolio_id: {
                required: true
            }
        }
    });
    fetchAllVendorType(false);
    $("#work-order-randomno").val(getRandomNumber(6));

    $(".work-order-duration").change(function(){
        if(this.value == "0"){
            $("#monthddl").hide();
        }else{
            $("#monthddl").show();
        }
    });

    $("#workCreatedAt").datepicker({dateFormat: datepicker,
        changeYear: true,
        yearRange: "-100:+20",
        changeMonth: true,
    }).datepicker("setDate", new Date());
    $("#workScheduledon").datepicker({dateFormat: datepicker,
        changeYear: true,
        yearRange: "-100:+20",
        changeMonth: true,
    }).datepicker("setDate", new Date());
    $("#workCompletedon").datepicker({dateFormat: datepicker,
        changeYear: true,
        yearRange: "-100:+20",
        changeMonth: true,
    }).datepicker("setDate", new Date());



});


$(document).on('click',"#add-work-cat",function(){
    $("#NewCategoryPopup").show();

})
$(document).on('click',"#add-priority-cat",function(){
    $("#NewPriorityPopup").show();
})
$(document).on('click', '.cancelPopup', function () {
    $(this).parent().parent().parent().find('input[type=text]').val('');
    $(this).parent().parent().parent().find('span').text('');
});
$(document).on('click', '#Newportfolio', function () {
    $('#portfolio_id').val(getRandomNumber(6));

    $("#NewportfolioPopup").show();
});
$(document).on('click', '#add-work-status', function () {
    $("#NewWorkStatusPopup").show();
});
$(document).on('click', '#add-work-source', function () {
    $("#NewWorkSourcePopup").show();
});

$(document).on('click', '#add-request-by', function () {
    $("#NewRequestByPopup").show();
});


function clearPopUps(id) {
    $(id).find('input[type=text]').val('');
    $(id).find('span').text('');
}
$(document).on('click', ".grey-btn", function () {

    var box = $(this).closest('.add-popup');
    $(box).hide();

});
function getRandomNumber(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
$(document).on("change","#ddlWorkOrderAuthority",function(){
    if(this.value == "5"){
        $("#txtOther").show();
    }else{
        $("#txtOther").hide();
    }
});

$(document).on('click', '#open-vendor-modal', function () {
    bootbox.confirm("Do you want to add a New Vendor?", function (result) {
        if (result == true) {
            $("#vendorModel").modal('show');
            $("#FormSaveNewVendor").trigger('reset');
            $(".vendor_random_id").val(getRandomNumber(6));
            jQuery('.phone_format').mask('000-000-0000', {reverse: true});
            response({success: false});
        }
    });

});

$(document).on('click', '#cancelVendorAdd', function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $("#vendorModel").modal('hide');
        }
    });

});


$(document).on("change","#salutation",function(){
    var value = $(this).val();
    if(value == 1){
        $('#gender').prop('selectedIndex',0);
        $('.hidemaiden').hide();
    }
    if(value==2)
    {
        $("#gender").val("1");
        $('.hidemaiden').hide();
    }
    if(value==3)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    } if(value==4)
    {
        $('#gender').prop('selectedIndex',0);
        $('.hidemaiden').hide();
    } if(value==5)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    } if(value==6)
    {
        $("#gender").val("1");
        $('.hidemaiden').hide();
    } if(value==7)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    }
    if(value==8)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    } if(value==9)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    }


});
$(document).on("click", ".vendortypeplus", function () {
    $("#Newvendortype").show();
});
// add vendor type
$(document).on('click', '#NewvendortypeSave', function (e) {
    e.preventDefault();
    var formData = $('#Newvendortype :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {form: formData,
            class: 'addVendor',
            action: 'addVendorType'},
        beforeSend: function (xhr) {
            var res = true;
            $(".customValidatePortfoio").each(function () {
                res = validations(this);
            });
            if (res === false) {
                xhr.abort();
                return false;
            }
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#Newvendortype').hide(500);
                toastr.success(response.message);
                fetchAllVendorType(response.lastid);
            } else if (response.status == 'error' && response.code == 500) {
                toastr.warning(response.message);
            } else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});

function fetchAllVendorType(id) {
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {
            class: 'addVendor',
            action: 'fetchAllVendorType'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#vendor_type_options').html(res.data);
            if (id != false) {
                $('#vendor_type_options').val(id);
            }
        },
    });
}
function getAddressInfoByZip(zip) {
    if (zip.length >= 5 && typeof google != 'undefined') {
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': zip}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++) {
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number") {
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment") {
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political") {
                            addr.city = results[0].address_components[ii].short_name;
                        }
                        if (types == "administrative_area_level_1,political") {
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code") {
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political") {
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                     console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    setTimeout(function () {
                        response(addr);
                    }, 2000);

                } else {
                    response({success: false});
                }
            } else {
                response({success: false});
            }
        });
    } else {
        response({success: false});
    }
}
function response(obj) {
    if (obj.success) {

        $('#country').val(obj.country);
        $('.citys').val(obj.city);
        $('.states').val(obj.state);

    } else {
        $('.citys').val('');
        $('.states').val('');
        $('#country').val('');
    }
}
$(document).on('focusout', '#zip_code', function () {
    getAddressInfoByZip($(this).val());
});
$("#zipcode").keydown(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        getAddressInfoByZip($(this).val());
    }
});
$("#FormSaveNewVendor").validate({
    rules: {
        first_name: {
            required: true
        },
        last_name: {
            required: true
        },
        vendor_random_id :{
            required: true
        },
        vendor_rate: {
            maxlength: 10
        }
    }
});
//add vendor ajax
$("#FormSaveNewVendor").on('submit',function(event){
    event.preventDefault();
    if($("#FormSaveNewVendor").valid()){
        // checking carrier field validations
        var form = $('#FormSaveNewVendor')[0];
        var formData = new FormData(form);
        formData.append('action', 'addVendor');
        formData.append('class', 'workOrder');
        $.ajax({
            url: '/work-order-ajax',
            type: 'POST',
            data: formData, cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $("#vendorModel").modal("hide");
                } else if (response.message == 'Validation Errors!' && response.code == 400) {
                    toastr.warning('Email already exits!');
                } else if (response.message == 'Validation Errors!' && response.code == 400) {
                    toastr.warning('Email already exits!');
                } else if (response.code == 500) {
                    toastr.warning(response.message);
                }
            }
        });
    }
});
//add vendor notes clone
$(document).on("click", ".vendor-notes-plus", function () {
    var length = $(".vendor-notes-clone-divclass").length;
    if (length <= 2) {
        var newnode = $("#vendor-notes-clone-div").clone().find(".notes").val("").end().insertAfter("div.vendor-notes-clone-divclass:last");
        $(".vendor-notes-clone-divclass:not(:eq(0))  .vendor-notes-plus").remove();
        $(".vendor-notes-clone-divclass:not(:eq(0))  .vendor-notes-minus").show();
        if (length == 2) {
            $('.vendor-notes-plus').hide();
        }
    } else {
        $('.vendor-notes-plus').hide();
    }
    newnode.find('.notes').on('keydown', function(event) {
        if (this.selectionStart == 0 && event.keyCode >= 65 && event.keyCode <= 90 && !(event.shiftKey) && !(event.ctrlKey) && !(event.metaKey) && !(event.altKey)) {
            var $t = $(this);
            event.preventDefault();
            var char = String.fromCharCode(event.keyCode);
            $t.val(char + $t.val().slice(this.selectionEnd));
            this.setSelectionRange(1,1);
        }
    });

});
$(document).on('click', '.vendor-notes-minus', function () {
    $(this).parent().parent().remove();
    $('.vendor-notes-plus').show();
});

$(document).on('click', '.addressupdbtn', function () {
    $('#open-vendor-address-edit').modal("show");
    var relId=$(this).attr("rel");
    $(".addressUserId").val(relId);


});

//add vendor ajax
$("#FormUpdateAddress").on('submit',function(event){
    event.preventDefault();
    if($("#FormUpdateAddress").valid()){
        var id= $(".addressUserId").val();
        var formData = $('#FormUpdateAddress :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/work-order-ajax',
            data: {
                form: formData,
                id: id,
                class: 'workOrder',
                action: 'updateAddress'},
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.code == 200) {
                    toastr.success(response.message);
                    $("#open-vendor-address-edit").modal("hide");
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});

$(document).on("click",".addSingleData",function(){
    var value = $(this).attr('data-value');
    var value2 = $(this).attr('data-value2');
    var fieldValue = $("#"+value).val();
    var fieldValue2 = $("#"+value2).val();
    var table = $(this).attr('data-table');
    var column = $(this).attr('data-cell');
    var column2 = $(this).attr('data-cell2');
    var select = $(this).attr('data-select');
    var hide = $(this).attr('data-hide');
    var error = $(this).attr('data-error');

    $.ajax({
        url:'/work-order-ajax',
        type: 'POST',
        data: {
            "action": 'addSingleData',
            "class": 'workOrder',
            "table": table,
            "column": column,
            "column2":column2,
            "fieldValue2":fieldValue2,
            "fieldValue": fieldValue,
        },
        beforeSend: function (xhr) {
            var res = true;
            // checking portfolio validations
            $("."+error).each(function () {
                res = validations(this);
            });
            if (res === false) {
                xhr.abort();
                return false;
            }
        },
        success: function (response) {
            var info = JSON.parse(response);
            var option = "<option value="+info.id+">"+info.value+"</option>";
            $("."+select).append(option);
            $("#"+value).val('');
            $('.'+select).val(info.id);
            $(this).parents('.add-popup').hide();
            $("."+hide).hide();
            toastr.success('Data Added Successfully.');

        }
    });

});

$(document).on("click",".addPopUpData",function(){
    var value = $(this).attr('data-value');
    var fieldValue = $("#"+value).val();
    var table = $(this).attr('data-table');
    var column = $(this).attr('data-cell');
    var select = $(this).attr('data-select');
    var hide = $(this).attr('data-hide');
    var error = $(this).attr('data-error');

    $.ajax({
        url:'/work-order-ajax',
        type: 'POST',
        data: {
            "action": 'addPopUpData',
            "class": 'workOrder',
            "table": table,
            "column": column,
            "fieldValue": fieldValue,
        },
        beforeSend: function (xhr) {
            var res = true;
            // checking portfolio validations
            $("."+error).each(function () {
                res = validations(this);
            });
            if (res === false) {
                xhr.abort();
                return false;
            }

        },
        success: function (response) {
            var info = JSON.parse(response);
            var option = "<option value="+info.id+">"+info.value+"</option>";
            $("."+select).append(option);
            $("#"+value).val('');
            $('.'+select).val(info.id);
            $(this).parents('.add-popup').hide();
            $("."+hide).hide();
            toastr.success('Data Added Successfully.');

        }
    });

});
$(document).on("click",'.requestedbyCheckbox',function(){
    if(this.checked){
        $(".ddlWorkOrderRequest").attr("disabled",false);
    }else{
        $(".ddlWorkOrderRequest").attr("disabled",true);
    }

});


$(document).on("change",'.getallproperties',function(){
    var id=this.value;
    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'getAllBuildings',
            id:id
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('.getAllBuildings').html(res.html);



        },
    });
});
$(document).on("change",'.getAllBuildings',function(){
    var id=this.value;
    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'getallunits',
            id:id
        },
        success: function (response) {
            var res = JSON.parse(response);
            console.log(res.html);
            $('.getallunits').html(res.html);

        },
    });
});

$(document).on("change",'.getallunits',function(){
    var id=this.value;
    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'petinUnits',
            id:id
        },
        success: function (response) {
            var res = JSON.parse(response);

            $('.petinUnits').html(res.html);
            console.log('pett',res.datatenantuser);
            $(".tenant_info_id").val(res.datatenantuser.id);
            $("#nametableVendor").html(res.datatenantuser.name);
            $("#emailtableVendor").html(res.datatenantuser.email);
            $("#phntableVendor").html(res.datatenantuser.phone_number);

        },
    });
});


$(document).on("change",'.ddlPortfolioType',function(){
    var id=this.value;
    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'getAllProperties',
            id:id
        },
        success: function (response) {
            var res = JSON.parse(response);
            console.log(res.html);
            $('.getallproperties').html(res.html);

        },
    });
})

fetchAllPortfolio(false);
function fetchAllPortfolio(id) {

    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-portfolioname',
        data: {
            propertyEditid: propertyEditid,
            class: 'workOrder',
            action: 'fetchPortfolioname'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#portfolio_name_options').html(res.data);
            if (id != false) {
                $('#portfolio_name_options').val(id);

            }


        },
    });


}
setTimeout(function(){
    var portfolio_name_options=$('#portfolio_name_options').val();
    reloadproperties(portfolio_name_options);
}, 100);


function reloadproperties(idd){
    var id = idd;
    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'getAllProperties',
            id:id
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('.getallproperties').html(res.html);

        },
    });
}
fetchAllPriorities(false);
function fetchAllPriorities(id) {

    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'fetchAllPriorities'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#ddlPriorityTypeID').html(res.html);
            if (id != false) {
                $('#ddlPriorityTypeID').val(id);

            }

        },
    });

}
fetchAllCategory(false);
function fetchAllCategory(id) {

    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'fetchAllCategory'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#ddlWorkOrderTypeID').html(res.html);
            if (id != false) {
                $('#ddlWorkOrderTypeID').val(id);

            }

        },
    });

}
fetchAllStatus(false);
function fetchAllStatus(id) {

    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'workOrderStatus'},
        success: function (response) {
            var res = JSON.parse(response);

            $('.NewWorkStatusPopupID').html(res.html);
            if (id != false) {
                $('.NewWorkStatusPopupID').val(id);

            }

        },
    });

}
fetchAllSource(false);
function fetchAllSource(id) {

    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'workOrderSource'},
        success: function (response) {
            var res = JSON.parse(response);
            $('.ddlWorkOrderSource').html(res.html);
            if (id != false) {
                $('.ddlWorkOrderSource').val(id);

            }

        },
    });

}
fetchAllRequest(false);
function fetchAllRequest(id) {

    $.ajax({
        type: 'post',
        url: '/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'workOrderRequest'
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('.ddlWorkOrderRequest').html(res.html);
            if (id != false) {
                $('.ddlWorkOrderRequest').val(id);

            }

        },
    });
}

function onTop(rowdata,table){
    if(rowdata){
        setTimeout(function(){
            if (table == ""){
                jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                jQuery('.table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
            }else{
                jQuery('#'+table).find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                jQuery('#'+table).find('tr:eq(1)').find('td').last().addClass("green_row_right");
            }
        }, 300);
    }
}

$(document).on("click",".property_info_plus",function(){
    var newnode = $("#propertyInfoDivID").clone().insertAfter("div.propertyInfoDiv:last");
    newnode.find(".property_info_plus").remove();
    newnode.find(".property_info_minus").show();
    newnode.find('input').val('');
});
$(document).on("click", ".property_info_minus", function () {
    $(this).closest('.propertyInfoDiv').hide();
});
$("#FormWorkOrder").on('submit',function(event){
    event.preventDefault();
    if($("#FormWorkOrder").valid()){
        var form = $('#FormWorkOrder')[0];
        var formData = new FormData(form);
        var data = convertSerializeDatatoArray();
        var count = file_library.length;
        var custom_field = [];
        $(".custom_field_html input").each(function () {
            var data = {'name': $(this).attr('name'), 'value': $(this).val(), 'id': $(this).attr('data_id'), 'is_required': $(this).attr('data_required'), 'data_type': $(this).attr('data_type'), 'default_value': $(this).attr('data_value')};
            custom_field.push(data);
        });
        var custom_field = JSON.stringify(custom_field);
        $.each(file_library, function (key, value) {
            if (compareArray(value, data) == 'true') {
                formData.append(key, value);
            }
        });
        var approved_by_owner='';
        if ($('.approved_by_owner').is(":checked")) {
            var approved_by_owner = 'on';
        }
        var publish_to_tenant='';
        if ($('.publish_to_tenant').is(":checked")) {
            var publish_to_tenant = 'on';
        }
        var send_alert ='';
        if ($('.send_alert').is(":checked")) {
            var send_alert = 'on';
        }
        var publish_to_owner='';
        if ($('.publish_to_owner').is(":checked")) {
            var publish_to_owner = 'on';
        }
        var publish_to_vendor='';
        if ($('.publish_to_vendor').is(":checked")) {
            var publish_to_vendor = 'on';
        }

        var user_id = getParameterByName('id');
        var tenant_id= $(".tenant_info_id").val();
        var vendor_id= $('.vendor_info_id').val();
        formData.append('action', 'newWorkOrder');
        formData.append('class', 'workOrder');
        formData.append('custom_field', custom_field);
        formData.append('approved_by_owner', approved_by_owner);
        formData.append('publish_to_tenant', publish_to_tenant);
        formData.append('send_alert', send_alert);
        formData.append('publish_to_owner', publish_to_owner);
        formData.append('publish_to_vendor', publish_to_vendor);
        formData.append('tenant_id', tenant_id);
        formData.append('vendor_id', vendor_id);
        formData.append('user_id', user_id);

        $.ajax({
            url:'/work-order-ajax',
            type: 'POST',
            data: formData, cache: false,
            contentType: false,
            processData: false,
            success: function (response) {

                var response = JSON.parse(response);

                if (response.code == 200) {
                   // alert("dfsdf");
                    localStorage.setItem("Message", 'Record added successfully.');
                    localStorage.setItem('rowcolor_vendor', 'rowColor');
                    localStorage.removeItem('redirection_module');
                    window.location.href = window.location.origin + '/WorkOrder/WorkOrders';

                } else if (response.message == 'Validation Errors!' && response.code == 400) {
                    toastr.warning('Email already exits!');
                } else if (response.message == 'Validation Errors!' && response.code == 400) {
                    toastr.warning('Email already exits!');
                } else if (response.code == 500) {
                    toastr.warning(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
    }
});

function clearPopUps(id) {
    $(id).find('input[type=text]').val('');
    $(id).find('span').text('');
}
// var id=$('#id_from_other').val();
// if(id  != 0){
//     propinfo();
// }

function getParameterByName( name ){
    var regexS = "[\\?&]"+name+"=([^&#]*)",
        regex = new RegExp( regexS ),
        results = regex.exec( window.location.search );
    if( results == null ){
        return "";
    } else{
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}
var idd=$('#id_from_other').val();
var id =  getParameterByName('id');

var redirection_from = localStorage.getItem('redirection_module');
if (redirection_from != '' && id != '' && idd != 0){
    propinfo();
}

function propinfo(){
    //
    // var id=$('#id_from_other').val();
    // console.log(id);

    $.ajax({
        type: 'post',
        url: '/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'getpropdata',
            id : id,
            redirection_from : redirection_from
        },

        success: function (response) {
            var data = JSON.parse(response);
            if (data.status == "success"){
                switch (redirection_from) {
                    case "property_module":
                        setTimeout(function(){
                            $(".ddlPortfolioType ").val(data.data.portfolio_id).trigger("change");
                        }, 500);
                        setTimeout(function(){
                            $('.getallproperties').val(data.data.id).attr('selected',true).trigger("change");
                        }, 600);
                        if(data.data1.length <= 1){
                            setTimeout(function(){
                                $('.getAllBuildings').val(data.data1[0].id).trigger("change");
                            }, 700);
                        }
                        if(data.data2.length <= 1){
                            setTimeout(function(){
                                $('.getallunits').val(data.data2[0].id).trigger("change");
                            }, 800);
                        }
                        toastr.success(data.message);
                        break;

                    case "ticket_module":
                        setTimeout(function(){

                            $(".ddlPortfolioType").val(data.data4.portfolio_id).trigger("change");

                        }, 500);
                        setTimeout(function(){
                            $('.getallproperties').val(data.data4.id).attr('selected',true).trigger("change");
                        }, 600);

                        setTimeout(function(){
                            $('.getAllBuildings').val(data.data5.building_id).trigger("change");
                        }, 700);


                        setTimeout(function(){
                            $('.getallunits').val(data.data5.id).trigger("change");
                        }, 800);
                        $(".tenant_info_id").val(data.data7.id);
                        $("#nametableVendor").html(data.data7.name);
                        $("#emailtableVendor").html(data.data7.email);
                        $("#phntableVendor").html(data.data7.phone_number);

                        break;

                    case "owner_module":
                        var properties = '<option value="">Select</option>';
                        $.each(data.data, function (key, value) {
                            properties += '<option value="'+value.property_id+'">'+value.property_name+'</option>'
                        });
                        setTimeout(function(){
                            $('.getallproperties').html(properties);
                        }, 600);

                        break;

                    case "vendor_module":
                        $(".ddlPortfolioType").trigger('change');
                        $('.vendor_info_id').val(data.data.id);
                        $('.owner_pre_vendor').val(data.data.name);
                        $('#vendor_address').val(data.data.address1+' '+data.data.address2+' '+data.data.address3);
                        $("#nametable").html(data.data.name);
                        $("#cattable").html(data.resvendortype);
                        $("#emailtable").html(data.data.email);
                        $("#phntable").html(data.data.phone_number);
                        break;

                    case "tenant_module":
                        console.log(data.data);
                        setTimeout(function(){

                            $(".ddlPortfolioType ").val(data.data.portfolio_id).trigger("change");


                        }, 500);
                        setTimeout(function(){
                            $('.getallproperties').val(data.data.property_id).attr('selected',true).trigger("change");
                        }, 600);

                        setTimeout(function(){
                            $('.getAllBuildings').val(data.data.building_id).trigger("change");
                        }, 700);


                        setTimeout(function(){
                            $('.getallunits').val(data.data.unit_id);
                        }, 800);
                        $(".tenant_info_id").val(data.data.id);
                        $("#nametableVendor").html(data.data.name);
                        $("#emailtableVendor").html(data.data.email);
                        $("#phntableVendor").html(data.data.phone_number);

                    default:


                }

            } else {
                toastr.error(data.message);
            }
        }
    });
}