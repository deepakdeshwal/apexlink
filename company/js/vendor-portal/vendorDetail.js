var id = vendor_id;

fetchVendorDetails();
function fetchVendorDetails() {
    var vendor_id = id;
    $.ajax({
        type: 'post',
        url: '/Vendor-portal-ajax',
        data: {class: 'vendorPortal', action: "getVendorDetail", vendor_id: vendor_id},
        success: function (response) {
            var response = JSON.parse(response);
            console.log(response.data);
            if (response.code == 200) {
                $.each(response.data, function (key, value) {
                    if(key == 'image' && value != ''){
                        console.log(value);
                        if(value.startsWith("<img")){
                            $('.tenant_image').html(value);
                        } else {
                            $('.tenant_image').html('<img id="vendor_image_data" src="'+value+'">');
                        }
                        $('#vendor_image_data').attr('src',value);
                        
                    } else {
                        $('.' + key + '_account').text(value);
                        $("#vendor_type_id_acc").text(response.data.vendor_type_id_acc);

                        if(response.data.gender == "1"){
                            $('.gender_account').text('Male');
                        }else if(response.data.gender == "2"){
                            $('.gender_account').text('Female');
                        }else if(response.data.gender == "3"){
                            $('.gender_account').text('Prefer Not To Say');
                        }else if(response.data.gender == "4"){
                            $('.gender_account').text('Other');
                        }
                        $('.address_account').text(response.data.address1+' '+response.data.address2+' '+response.data.address3+' '+response.data.address4);
                    }
                });

                $.each(response.emergency, function (key, value) {
                    $('.' + key+'_account').text(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

/**
 * Get Parameters by id
 * @param status
 */
function getParameterByName(name) {
    var regexS = "[\\?&]" + name + "=([^&#]*)",
        regex = new RegExp(regexS),
        results = regex.exec(window.location.search);
    if (results == null) {
        return "";
    } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}


/*
To cropit image change
 */
$(document).on("change",".cropit-image-input",function(){
    photo_videos = [];
    var fileData = this.files;
    var type= fileData.type;
    var elem = $(this);
    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
        var uploadType = 'image';
        var validSize = '1030';
        var arrayType = validImageTypes;

        if ($.inArray(type, arrayType) < 0) {
            toastr.warning('Please select file with valid extension only!');
        } else {

            if (parseInt(size) > validSize) {
                var validMb =  1;
                toastr.warning('Please select documents less than '+validMb+' mb!');
            } else {
                size = isa_convert_bytes_to_specified(value['size'], 'k') + 'kb';
                photo_videos.push(value);
                var src = '';
                var reader = new FileReader();
                $('#photo_video_uploads').html('');


                $(".popup-bg").show();
                elem.next().show();

            }
        }
    });
});

function isa_convert_bytes_to_specified(bytes, to) {
    var formulas =[];
    formulas['k']= (bytes / 1024).toFixed(1);
    formulas['M']= (bytes / 1048576).toFixed(1);
    formulas['G']= (bytes / 1073741824).toFixed(1);
    return formulas[to];
}


$(document).ready(function(){
    var cropper = $('.image-editor').cropit({
        imageState: {
            src: subdomain_url+'500/400',
        },
    });
    // Handle rotation
    document.getElementById('rotate-ccw').addEventListener('click', function() {
        $('.image-editor').cropit('rotateCCW')
    });

    document.getElementById('rotate-cw').addEventListener('click', function() {
        $('.image-editor').cropit('rotateCW')
    });


    $(document).on("click",'.export',function(){
        $(this).parents('.cropItData').hide();
        $(this).parent().prev().val('');
        var dataVal = $(this).attr("data-val");
        var imageData = $(this).parents('.image-editor').cropit('export');
        var image = new Image();
        image.src = imageData;
        $(".popup-bg").hide();
        $(this).parent().parent().parent().prev().find('div').html(image);
        saveImage(imageData);
    });


    function saveImage(image){
        if(image!==undefined)
        {
            var vendor_image = image;
            var vendorImage = JSON.stringify(vendor_image);
            $.ajax({
                url:'/Vendor-portal-ajax',
                type: 'POST',
                data: {
                    "vendor_id": id,
                    "action": 'editVendorImage',
                    "class": 'VendorPortal',
                    "vendor_image": vendor_image
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status=='success') {
                        if(response.function != ''){
                            $('.tenant_image').html('<img id="vendor_image_data" src="'+response.function+'">');
                        }
                        toastr.success(response.message);
                    }
                }
            });
        }
    }

    showCustomFields();
    function showCustomFields(){
        $.ajax({
            type: 'post',
            url: '/vendor-view-ajax',
            data: {id:id,
                class: 'ViewVendor',
                action: 'getCustomFieldData'},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    getCustomFieldForViewMode(response.data);
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on('click','#editEmergencyData',function(){
        $('#emergency_view').hide();
        $('#emergency_edit_div').show();
        editEmergencyData();
    });

    $(document).on('click','#emergencyCancel',function(){
        $('#emergency_view').show();
        $('#emergency_edit_div').hide();
        showEmergencyData();
    });


    function showEmergencyData(){
        $.ajax({
            type: 'post',
            url: '/Vendor-portal-ajax',
            data: {id:id,
                class: 'vendorPortal',
                action: 'viewEmergencyData'},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $.each(response.emergency, function (key, value) {
                        $('.' + key+'_account').text(value);
                    });
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    function editEmergencyData(){
        $.ajax({
            type: 'post',
            url: '/Vendor-portal-ajax',
            data: {id:id,
                class: 'vendorPortal',
                action: 'getEmergencyInfo'},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#emergency_edit').html(response.data);
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    /**
     * function to show custom field
     * @param editCustomFieldData
     */
    function getCustomFieldForViewMode(editCustomFieldData){
        var module = 'vendor';
        $.ajax({
            type: 'POST',
            url: '/CustomField/get',
            data:{module:module},
            success: function (response) {
                var response = JSON.parse(response);
                var custom_array = [];
                if(response.code == 200 && response.message == 'Record retrieved successfully'){
                    $('.custom_field_html_view_mode').html('');
                    $('.custom_field_msg').html('');
                    var html ='';
                    $.each(response.data, function (key, value) {
                        var default_val = '';
                        if(editCustomFieldData) {
                            default_val = (value.default_value != '' && value.default_value != null) ? value.default_value : '';
                            $.each(editCustomFieldData.custom_field, function (key1, value1) {
                                if (value1.id == value.id) {
                                    default_val = value1.value;
                                }
                            });

                        } else {
                            default_val = (value.default_value != '' && value.default_value != null) ? value.default_value : '';
                        }

                        var required = (value.is_required == '1')?' <em class="red-star">*</em>':'';
                        var name = value.field_name.replace(/ /g, "_");
                        var currency_class = (value.data_type == 'currency')?' amount':'';
                        if(value.data_type == 'date') {
                            custom_array.push(name);
                        }
                        var readonly = 'readonly';
                        html += '<div class="row custom_field_class">';
                        html += '<div class=col-sm-6>';
                        html += '<label>'+value.field_name+required+'</label>';
                        html += '<input type="text" readonly data_type="'+value.data_type+'" data_value="'+value.default_value+'" '+readonly+' data_id="'+value.id+'" data_required="'+value.is_required+'" name="'+name+'" id="'+name+'" value="'+default_val+'" class="form-control changeCustom add-input'+currency_class+'">';
                        //html += '<span class="customError required"></span>';
                        //html += '</div>';
                        if(value.is_editable == 1 || value.is_deletable == 1) {
                            //html += '<div class="col-sm-6">';
                            if (value.is_editable == 1) {
                                html += '<span id="' + value.id + '" class="editCustomField"><i class="fa fa-edit"></i></span>';
                            }
                            if (value.is_deletable == 1) {
                                html += '<span id="' + value.id + '" class="deleteCustomField"><i class="fa fa-times-circle""></i></span>';
                            }
                            //html += '</div>';
                        }
                        html += '<span class="customError required"></span>';
                        html += '</div>';
                        html += '</div>';

                    });
                    $(html).prependTo('.custom_field_html_view_mode');
                } else {
                    $('.custom_field_html_view_mode').html('No Custom Fields');
                }
                triggerDatepicker(custom_array)
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function triggerDatepicker(custom_array){
        $.each(custom_array, function (key, value) {
            $('#customField'+value).datepicker({
                dateFormat: datepicker
            });
        });
    }

    $(document).on("click", ".add-emergency-contant", function () {
        console.log('here');
        var clone = $(".tenant-emergency-contact:first").clone();
        clone.find('input[type=text]').val('');
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        $(".tenant-emergency-contact").last().after(clone);
        clone.find(".add-emergency-contant").hide();
        clone.find(".remove-emergency-contant").show();
        clone.find("#otherRelationDiv").hide();

    });

    $(document).on("click", ".remove-emergency-contant", function () {
        $(this).parents(".tenant-emergency-contact").remove();
    });


    $(document).on('click','#updateEmergencyButton',function(event){

        // checking carrier field validations
        var form = $('#editEmergency')[0];
        var formData = new FormData(form);
        formData.append('action', 'updateEmergencyContant');
        formData.append('class', 'vendorPortal');
        formData.append('vendor_id', id);
        $.ajax({
            url: '/Vendor-portal-ajax',
            type: 'POST',
            data: formData, cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success') {
                    toastr.success(response.message);
                    $('#emergency_view').show();
                    $('#emergency_edit_div').hide();
                    showEmergencyData();
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });

    notes('all');
    function notes(status) {
        var id = vendor_id;
        var table = 'tenant_chargenote';
        var columns = ['Date', 'Time', 'Notes'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        // var extra_columns = ['tenant_chargenote.deleted_at'];
        var extra_columns = [];
        var extra_where = [{column: 'user_id', value: id, condition: '='}];
        var columns_options = [
            {name: 'Date', index: 'updated_at', width: 420, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Time', index: 'updated_at', width: 420, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'updated_at_time'},
            {name: 'Notes', index: 'note', width: 420, align: "center", searchoptions: {sopt: conditions}, table: table, },
        ];
        var ignore_array = [];
        jQuery("#notes-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_where: extra_where,
                deleted_at: 'no'
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Notes/History",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}

            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: false, refresh: false, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 400, drag: true, resize: false}
        );
    }

    // $(document).on('click','#saveVendorImage',function(){
    //     var vendor_image = $('.tenant_image').html();
    //     var vendorImage = JSON.stringify(vendor_image);
    //     var formData = new FormData(form);
    //     formData.append('action', 'vendorPortal');
    //     formData.append('class', 'saveVendorImage');
    //     formData.append('vendor_image', vendorImage);
    //     formData.append('vendor_id', vendor_id);
    //     $.ajax({
    //         url: '/Vendor-portal-ajax',
    //         type: 'POST',
    //         data: formData, cache: false,
    //         contentType: false,
    //         processData: false,
    //         success: function (response) {
    //             var response = JSON.parse(response);
    //             if (response.status == 'success' && response.code == 200) {
    //
    //             }
    //         },
    //         error: function (data) {
    //
    //         }
    //
    //     });
    // });
});



/** Conversation div js  starts*/
$(document).on("click","#add_owner_conversation_btn",function(){
    $('.add_owner_conversation_div').find('textarea').val('');
    var validator = $("#add_owner_conversation_form_id").validate();
    validator.resetForm();
    fetchAllProblemCategory();
    getVendorPropertyManager(vendor_unique_id, 'vendor_tab');
    $('.add_owner_conversation_div').show();
});

$(document).on("click","#cancel_add_owner_conversation",function(){
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $('.add_owner_conversation_div').hide();
        }
    });
});

getVendorPropertyManager(vendor_unique_id, 'vendor_tab');
function getVendorPropertyManager(vendor_unique_id, vendor_tab) {
    $.ajax({
        type: 'POST',
        url: '/Conversation-Ajax',
        data: {
            class: "conversationAjax",
            action: "getUserPropertyManagerList",
            id:vendor_unique_id,
            active_tab:vendor_tab
        },
        success: function (response) {
            var response = JSON.parse(response);
            $('.select_mangers_option').html(response.data);

        }
    });
}

fetchAllConversations('', vendor_unique_id);
function fetchAllConversations(selected_manager,selected_user){
    var active_tab = 'vendor_tab';
    $.ajax({
        type: 'post',
        url: '/Conversation-Ajax',
        data: {
            class: 'conversationAjax',
            action: 'getAllConversationByType',
            active_tab : active_tab,
            selected_manager : selected_manager,
            selected_user : selected_user,
            is_portal: 'Vendor_Portal'
        },
        success: function (response) {
            var res = JSON.parse(response);
            var html = '';
            $.each(res.data, function (key, val) {
                var user_type = '';
                if (val.comment_by == 'yes'){
                    user_type = val.user_type;
                } else {
                    if (val.user_type == 1) {
                        user_type = 'Tenant'
                    } else if (val.user_type == 2) {
                        user_type = 'Owner'
                    } else {
                        user_type = 'Vendor'
                    }
                }
                var user_image = '<img src='+default_Image+'>';
                if(val.selected_user_image){
                    if (val.selected_user_image.owner_image && val.selected_user_image.owner_image !='undefined') {
                        user_image = val.selected_user_image.owner_image;
                    }
                    if (val.selected_user_image.tenant_image && val.selected_user_image.tenant_image !='undefined') {
                        user_image = val.selected_user_image.tenant_image;
                    }
                    if (val.selected_user_image.image && val.selected_user_image.image !='undefined') {
                        user_image = val.selected_user_image.image;
                    }
                }

                html += '<li >\n' +
                    '<div class="row">\n' +
                    '<div class="col-sm-1 conversation-lt">\n' +
                    '<div class="img-outer">\n' +user_image+
                    '</div>\n' +
                    '</div>\n' +
                    '<div class="col-sm-10 pad-none conversation-rt">\n' +
                    '<span class="dark_name">'+val.selected_user_name+'('+user_type+') :</span>\n' +
                    '<span class="light_name">'+val.login_user_name+'('+val.login_user_role+')</span>\n' +
                    '<span class="dark_name">'+val.problem_category_name+'</span>\n' +
                    '<span class="black_text" >'+val.description+'</span>\n' +
                    '<div class="conversation-time clear">\n' +
                    '<span class="black_text" >'+val.created_date+'-</span>\n' +
                    '<a class="light_name comment_on_conversation" conversation_id="'+val.id+'">Comment</a>\n' +
                    '</div>\n' +
                    '<div class="viewConversationCommentDiv">\n' +
                    '</div>'+
                    '<div class="textConversationCommentDiv">\n' +
                    '</div>'+
                    '</div>\n' +
                    '<div class="col-sm-1 pull-right">\n' +
                    '<span class="viewAllComment" conversation_id="'+val.id+'" ><i class="fa fa-plus-circle" aria-hidden="true"></i></span> \n' +
                    '</div>\n' +
                    '</div>\n' +
                    '</li>';
            });
            $('.conversation_list').html(html);
        },
    });
}

$(document).on("click",".viewAllComment",function () {
    var active_tab = 'vendor_tab';
    var conversation_id = $(this).attr('conversation_id');
    var viewCommentHtml = $(this).parent().prev().find('.viewConversationCommentDiv');
    var viewCommentFindActive = $(this).parent().prev().find('.viewConversationCommentDiv').hasClass('active');
    if(viewCommentFindActive) {
        viewCommentHtml.html('');
        viewCommentHtml.removeClass('active');
    } else {
        $.ajax({
            type: 'post',
            url: '/Conversation-Ajax',
            data: {
                class: 'conversationAjax',
                action: 'getAllConversationByType',
                conversation_id: conversation_id,
                active_tab: active_tab,
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    var comment_html = '';
                    $.each(response.data, function (key, val) {
                        var user_image = '<img src='+default_Image+'>';
                        if(val.selected_user_image){
                            if (val.selected_user_image.owner_image && val.selected_user_image.owner_image !='undefined') {
                                user_image = val.selected_user_image.owner_image;
                            }
                            if (val.selected_user_image.tenant_image && val.selected_user_image.tenant_image !='undefined') {
                                user_image = val.selected_user_image.tenant_image;
                            }
                            if (val.selected_user_image.image && val.selected_user_image.image !='undefined') {
                                user_image = val.selected_user_image.image;
                            }
                        }

                        comment_html += '<div style="" id="ConversationOwnerComment_0">\n' +
                            '<div class="conversation-comment-grey removeClass">\n' +
                            '<div class="conversation-arrow"></div>\n' +
                            '<div class="col-sm-1 conversation-lt">\n' +
                            '<div class="img-outer">\n' +user_image+
                            '</div>\n' +
                            '</div>\n' +
                            '<div class="col-sm-10 conversation-rt">\n' +
                            '<span class="dark_name">' + val.login_user_name + '(' + val.login_user_role + ') :</span>\n' +
                            '<span class="black_text" >' + val.description + '</span>\n' +
                            '<div class="conversation-comment-time">' + val.created_date + '</div>\n' +
                            '</div>\n' +
                            '</div>\n' +
                            '</div>'
                    });
                    viewCommentHtml.append(comment_html);
                    viewCommentHtml.addClass('active');
                } else if (response.status == 'error' && response.code == 502) {
                    toastr.error(response.message);
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            }
        });
    }
});


$(document).on("click",".comment_on_conversation",function () {
    var conversation_id = $(this).attr('conversation_id');
    $('.textConversationCommentDiv').html('');
    var commentHtml = '<form id="sendConversationCommentForm">\n' +
        '<input type="hidden" name="conversation_id" value="'+conversation_id+'">\n' +
        '<textarea class="form-control capital" name="comment_text" rows="6"></textarea>\n' +
        '<div class="btn-outer top-marginCls">\n' +
        '<button class="blue-btn sendConversationCommentBtn">Send</button>\n' +
        '<button  type="button" class="grey-btn cancelConversationCommentDiv">Cancel</button>\n' +
        '</div>\n' +
        '</form>\n';
    $(this).parent().next().next().html(commentHtml);
});
$(document).on("click",".cancelConversationCommentDiv",function () {
    console.log('cancel>>>',  $(this).parent());
    $(this).parents('.textConversationCommentDiv').html('');
});

$(document).on('submit','#sendConversationCommentForm',function(e) {
    e.preventDefault();
    var active_tab = 'vendor_tab';
    var form = $('#sendConversationCommentForm')[0];
    var formData = new FormData(form);

    formData.append('class', 'conversationAjax');
    formData.append('action', 'sendCommentOnConversation');
    formData.append('active_tab', active_tab);
    formData.append('is_portal', 'Vendor_Portal');

    $.ajax({
        url: '/Conversation-Ajax',
        type: 'post',
        data:  formData,
        contentType: false,
        processData: false,
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status == 'success' && response.code == 200){
                fetchAllConversations('',vendor_unique_id);
                toastr.success("Record created successfully.");
            }else if(response.status == 'error' && response.code == 502){
                toastr.error(response.message);
            } else if(response.status == 'error' && response.code == 400){
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.'+key).html(value);
                });
            }
        }
    });
});

$("#add_owner_conversation_form_id").validate({
    rules: {
        selected_user_id: {
            required: true
        },
        problem_category: {
            required: true
        },
        description: {
            required: true
        }
    },
    submitHandler: function (form) {
        event.preventDefault();

        var form = $('#add_owner_conversation_form_id')[0];
        var formData = new FormData(form);


        formData.append('class', 'conversationAjax');
        formData.append('action', 'addConversation');
        formData.append('active_tab', 'vendor_tab');
        formData.append('is_portal', 'Vendor_Portal');

        $.ajax({
            url: '/Conversation-Ajax',
            type: 'post',
            data:  formData,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    $('.add_owner_conversation_div').hide();
                    fetchAllConversations('',vendor_unique_id);
                    toastr.success("Record created successfully.");
                }else if(response.status == 'error' && response.code == 502){
                    toastr.error(response.message);
                } else if(response.status == 'error' && response.code == 400){
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.'+key).html(value);
                    });
                }
            }
        });
    }
});

function fetchAllProblemCategory(){
    $.ajax({
        type: 'post',
        url: '/Conversation-Ajax',
        data: {
            class: 'conversationAjax',
            action: 'getAllProblemCategory',
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('.problem_category').html(res.data);
        },
    });
}

/** Conversation div js ends **/

/**** Save problem category popup data starts ****/

$(document).on("click",".pop-add-icon",function () {
    $(".add-popup-body input[type='text']").val('');
    var text_id = $(this).closest('div').find('.add-popup').attr('id');
    $("#"+text_id+" .required").text('');
    $(this).closest('div').find('.add-popup').show();
});

$(document).on("click",".add-popup .grey-btn",function(){
    var box = $(this).closest('.add-popup');
    $(box).hide();
});

$(document).on("click",".add_single1", function () {
    var tableName = $(this).attr("data-table");
    var colName = $(this).attr("data-cell");
    var className = $(this).attr("data-class");
    var selectName = $(this).attr("data-name");
    var val = $(this).parent().prev().find("."+className).val();

    if (val == ""){
        $(this).parent().prev().find("."+className).next().text("* This field is required");
        return false;
    }else{
        $(this).parent().prev().find("."+className).next().text("");
    }

    savePopUpData(tableName, colName, val, selectName);
    var val = $(this).parent().prev().find("input[type='text']").val('');
});

function  savePopUpData(tableName, colName, val, selectName) {
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/savePopUpData',
        data: {
            class: "TenantAjax",
            action: "savePopUpData",
            tableName:tableName,
            colName: colName,
            val:val
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                $("select[name='"+selectName+"']").append(option);
                $("select[name='"+selectName+"']").next().hide();
                toastr.success(data.message);
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

$(document).ready(function () {
    $(document).on('click','#search_ledger',function () {
        ten_ledger_search();
    });
    $(".ledger_tab").click(function () {
        ten_ledger();
    });

    function ten_ledger() {
        var  user_id= localStorage.getItem('ven_id');
        console.log(user_id);
        var a=$('.st_date').val();
        a = new Date(a);
        var yr=a.getFullYear();
        var mon=a.getMonth();
        var day=a.getDate();
        var st_date=yr+'-'+mon+'-'+day;
        var b= $('.end_date').val();
        b=new Date(b);
        var yr1=b.getFullYear();
        var mon1=b.getMonth() + 1;
        var end_date=yr1+'-'+mon1;
        console.log((st_date,end_date));
        var firstDay = new Date(yr, mon, 1);
        var lastDay = new Date(yr1, mon1, 0);
        st_date=firstDay.getFullYear()+'-'+(firstDay.getMonth()) +'-'+firstDay.getDate();
        end_date=lastDay.getFullYear()+'-'+(lastDay.getMonth()+1) +'-'+lastDay.getDate();
        $.ajax({
            url:'/vendor-add-ajax',
            type: 'POST',
            data: {
                "action": 'gettransactiondata',
                "class": 'addVendor',
                "user_id":user_id,
                'st_date':st_date,
                'end_date':end_date
            },
            success: function (response) {
                var res=JSON.parse(response);
                console.log(res);
                var html="";
                for(var i=0;i<res.data.length;i++){
                    html +='<tr class="ledger_tr">' +
                        '                                                                    <td>'+res.dates[i]+'</td>' +
                        '                                                                    <td>'+res.data[i].transaction_id+'</td>' +
                        '                                                                    <td>'+res.data[i].payment_response+'</td>' +
                        '                                                                    <td>'+res.data[i].reference_no+'</td>' +
                        '                                                                    <td>'+currencySign+''+res.data[i].total_charge_amount+'</td>' +
                        '                                                                </tr>';
                }
                $("#vendor_ledger").html('').append(html);
            }
        });

    }
    function ten_ledger_search() {
        var  user_id= localStorage.getItem('ven_id');
        console.log(user_id);
        var a=$('.st_date').val();
        a = new Date(a);
        var yr=a.getFullYear();
        var mon=a.getMonth();
        var day=a.getDate();
        var st_date=yr+'-'+mon+'-'+day;
        var b= $('.end_date').val();
        b=new Date(b);
        var yr1=b.getFullYear();
        var mon1=b.getMonth() + 1;
        var end_date=yr1+'-'+mon1;
        console.log((st_date,end_date));
        var firstDay = new Date(yr, mon, 1);
        var lastDay = new Date(yr1, mon1, 0);
        st_date=firstDay.getFullYear()+'-'+(firstDay.getMonth()) +'-'+firstDay.getDate();
        end_date=lastDay.getFullYear()+'-'+(lastDay.getMonth()+1) +'-'+lastDay.getDate();
        $.ajax({
            url:'/vendor-add-ajax',
            type: 'POST',
            data: {
                "action": 'gettransactiondatasearch',
                "class": 'addVendor',
                "user_id":user_id,
                'st_date':st_date,
                'end_date':end_date
            },
            success: function (response) {
                var res=JSON.parse(response);
                console.log(res);
                var html="";
                for(var i=0;i<res.data.length;i++){
                    html +='<tr class="ledger_tr">' +
                        '                                                                    <td>'+res.dates[i]+'</td>' +
                        '                                                                    <td>'+res.data[i].transaction_id+'</td>' +
                        '                                                                    <td>'+res.data[i].payment_response+'</td>' +
                        '                                                                    <td>'+res.data[i].reference_no+'</td>' +
                        '                                                                    <td>'+currencySign+''+res.data[i].total_charge_amount+'</td>' +
                        '                                                                </tr>';
                }
                $("#vendor_ledger").html('').append(html);
            }
        });

    }

    $(document).on('click','#exportexcel',function () {
        exportTableToExcel();
    });
    $(document).on('click','#exportpdf',function () {
        exppdf();
    });
    $(document).on('click','#printsample',function () {
        exppdf();
    });
    function exportTableToExcel(tableID, filename = ''){
        var innerHTML=$("#ledger_table").html();
        var html = innerHTML;
        var blob = new Blob(['\ufeff', html], {
            type: 'application/vnd.ms-excel'
        });

        // Specify link url
        var url = 'data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent(html);

        // Specify file name
        filename = filename?filename+'.xls':'excel.xls';


        // Create download link element
        var downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);

        if(navigator.msSaveOrOpenBlob ){
            navigator.msSaveOrOpenBlob(blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = url;

            // Setting the file name
            downloadLink.download = filename;

            //triggering the function
            downloadLink.click();
        }

        document.body.removeChild(downloadLink);
    }


    function exppdf() {
        var htmls=$("#ledger_table").html();
        console.log('htmls',htmls);
        $.ajax({
            type: 'post',
            url: '/vendor-add-ajax',
            data: {
                class: "VendorPortalAjax",
                action: "addVendor",
                htmls: htmls
            },
            success: function (res) {
                var res= JSON.parse(res);
                console.log(res.data);
                if (res.code == 200) {
                    $('.reports-loader').css('visibility','hidden');
                    var link=document.createElement('a');
                    document.body.appendChild(link);
                    link.target="_blank";
                    link.download="Report.pdf";
                    link.href=res.data.record;
                    link.click();
                    $("#wait").css("display", "none");
                } else if(res.code == 500) {
                    //toastr.warning(response.message);
                } else {
                    //toastr.error(response.message);
                }
            }
        });
    }


});
/**** Save problem category popup data ends here****/