

$(document).ready(function () {

    /*To Reset form */
    $(document).on("click", "#cancel", function () {
        $("#ownerPortal-login-form")[0].reset();
        var validator = $("#ownerPortal-login-form").validate();
        validator.resetForm();
    });
    /*To Reset form */
    $(document).on("click", "#forgot_cancel_owner,#reset_cancel", function () {
        window.location.href = '/Owner/Login';
    });

    $(document).on('click','#help_id',function(){
        $('#help-submenu').toggle();
    });
});

//forgot password validation
$("#forgot_password").validate({
    rules: {
        email: {
            required: true,
            email: true,
            maxlength: 50
        }
    },
    messages: {
        email: {
            email: "* Invalid email address",
        }
    },
    submitHandler: function (form) {
        var email = $("#email").val();
        $.ajax
        ({
            type: 'post',
            url: '/VendorPortalLogin-Ajax',
            data: {
                class: "portalLoginAjax",
                action: "forgotPasswordEmail",
                email: email,
            },
            success: function (response) {
                var data = $.parseJSON(response);
                console.log('data>>', data);
                if(data.status == 'success') {
                    toastr.success('The Email was sent successfully.');
                    $('.login-inner').hide();
                    $('#click_here_login_link').show();
                } else {
                    toastr.warning(data.message);
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



});


$.validator.addMethod("password_regex", function(value, element) {
    var password_regex = this.optional(element) || /^(?=.*[a-zA-Z])(?=.*[0-9])/.test(value);
    return password_regex;
}, "* Atleast one alphanumeric and a numeric character required");

//reset password validation
$("#reset_password").validate({
    rules: {
        new_password: {
            required: true,
            minlength: 8,
            password_regex: true
        },
        confirm_password: {
            required: true,
            minlength: 8,
            password_regex: true,
            equalTo: "#new_password"
        },
    },
    messages: {
        new_password: {
            minlength: "* Minimum 8 characters allowed",
        },
        confirm_password: {
            minlength: "* Minimum 8 characters allowed",
            equalTo: "* Fields do not match",
        },
    },
    submitHandler: function (form) {
        var owner_id = $("#owner_id").val();
        var password = $("#new_password").val();
        var forgot_password_token = $("#forgot_password_token").val();
        $.ajax
        ({
            type: 'post',
            url: '/VendorPortalLogin-Ajax',
            data: {
                class: "portalLoginAjax",
                action: "resetPassword",
                id: owner_id,
                password: password,
                forgot_password_token: forgot_password_token
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    toastr.success(data.message);
                    $('#reset_password_div').hide();
                    $('#click_here_login_id').show();
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});

//reset password validation
$("#set_new_password").validate({
    rules: {
        new_password: {
            required: true,
            minlength: 8,
            password_regex : true
        },
        confirm_password: {
            required: true,
            minlength: 8,
            password_regex : true,
            equalTo: "#new_password"
        },
    },
    messages: {
        new_password: {
            minlength: "* Minimum 8 characters allowed",
        },
        confirm_password: {
            minlength: "* Minimum 8 characters allowed",
            equalTo:  "* Fields do not match",
        },
    },
    submitHandler: function (form) {
        var owner_id = $(".vendor_id").val();
        var password = $("#new_password").val();
        var confirm_password = $("#confirm_password").val();
        var secretkey = $(".secretkey").val();
        $.ajax
        ({
            type: 'post',
            url: '/VendorPortalLogin-Ajax',
            data: {
                class: "portalLoginAjax",
                action: "setNewPassword",
                id: owner_id,
                password: password,
                confirm_password: confirm_password,
                secretkey: secretkey,
            },
            success: function (response) {
                var data = $.parseJSON(response);
                console.log('owner_id>>', data);
                // return;
                if(data.code == 200 && data.status == "success") {
                    toastr.success(data.message);
                    setTimeout(function () {
                        window.location.href = window.location.origin+'/Owner/Login';
                    }, 1000);
                } else if(data.code == 503) {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



});


