fetchPaymentTabData();
function fetchPaymentTabData(){
    $.ajax({
        type: 'post',
        url: '/vendor-view-ajax',
        data: {
            class: "ViewVendor",
            action: "getVendorDetail",
            vendor_id:vendor_unique_id
        },
        success: function (response) {

            var res = $.parseJSON(response);
            if (res.status == "success") {
                $('#tax_payer_name_payment').val(res.dataVendors.vendor_detail.tax_payer_name);
                $('.Payer_Name').text(res.dataVendors.vendor_detail.tax_payer_name);
                $('#tax_payer_id_payment').val(res.dataVendors.vendor_detail.tax_payer_id);
                $('.Payer_ID').text(res.dataVendors.vendor_detail.tax_payer_id);
                $('#consolidate_check_payment').val(res.dataVendor.vendor_additional_detail.consolidate_checks);
                $('.checks').text(res.dataVendor.vendor_additional_detail.consolidate_checks);
                $('.default_security_bank_account_payment').val(res.dataVendor.vendor_additional_detail.default_GL);
                $('.Expense_Account').text(res.dataVendor.vendor_additional_detail.default_GL);
                $('#order_limit_payment').val(res.dataVendor.vendor_additional_detail.order_limit);
                $('.Order_Limit').text(res.dataVendor.vendor_additional_detail.order_limit);
                $('.account_name_payment').val(res.dataVendor.vendor_additional_detail.account_name);
                $('.Account_Name').text(res.dataVendor.vendor_additional_detail.account_name);
                $('#bank_account_number_payment').val(res.dataVendor.vendor_additional_detail.bank_account_number);
                $('.Account_Number').text(res.dataVendor.vendor_additional_detail.bank_account_number);
                $('#routing_number_payment').val(res.dataVendor.vendor_additional_detail.routing_number);
                $('.Routing_Number').text(res.dataVendor.vendor_additional_detail.routing_number);
                $('#eligible_payment').val(res.dataVendor.vendor_additional_detail.eligible_for_1099);
                $('.Eligible_1099').text(res.dataVendor.vendor_additional_detail.eligible_for_1099);
                $('#comment_payment').val(res.dataVendor.vendor_additional_detail.comment);
                $('.Comments').text(res.dataVendor.vendor_additional_detail.comment);
            } else if (res.status == "error") {
                toastr.error(res.message);
            } else {
                toastr.error(res.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

$("#payment_setting_form").on('submit',function(event){
    event.preventDefault();
    var form = $("#payment_setting_form").serializeArray();
    if($("#payment_setting_form").valid()){
        $.ajax({
            type: 'post',
            url: '/vendorEdit/payment',
            data: {
                class: "paymentVendor",
                action: "editPaymentVendor",
                form: form,
                vendor_id:vendor_unique_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    fetchPaymentTabData();
                    $(".paymentDivViewTab").show();
                    $("#paymentDivTab2").hide();
                } else if (response.message == 'Validation Errors!' && response.code == 400) {
                    toastr.warning('Email already exits!');
                } else if (response.message == 'Validation Errors!' && response.code == 400) {
                    toastr.warning('Email already exits!');
                } else if (response.code == 500) {
                    toastr.warning(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
    }
});


$("#sendEmailTemplate").validate({
    rules: {
        sendEmail: {
            required:true
        }
    },
    submitHandler: function (e) {
        var tenant_id = vendor_unique_id;
        var form = $('#sendEmailTemplate')[0];
        var formData = new FormData(form);
        formData.append('action','sendEmailTemplate');
        formData.append('class','EditTenant');
        formData.append('tenant_id',tenant_id);
        formData.append('temp_key',"newVendorWelcome_Key");
        $.ajax({
            url:'/editTenant',
            type: 'POST',
            data: formData,
            success: function (data) {
                var info = JSON.parse(data);
                if(info.status=='success')
                {
                    toastr.success(info.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});


$(".acquireDateclass").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());

$(".expirationDateclass").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());

$(document).on('click', '.edit_redirectionTab2', function (event) {
    event.preventDefault();
    $(".paymentDivViewTab").hide();
    $("#paymentDivTab2").show();
});
fetchChartAccounts(false);
function fetchChartAccounts(id) {
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {
            class: 'addVendor',
            action: 'fetchChartAccounts'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#default_security_bank_account').html(res.data);
            if (id != false) {
                $('#default_security_bank_account').val(id);
            }

        },
    });

}
fetchAllAccountName(false);
function fetchAllAccountName(id) {
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {
            class: 'addVendor',
            action: 'fetchAllAccountName'},
        success: function (response) {
            var res = JSON.parse(response);
            $('.bankAccountName').html(res.data);
            if (id != false) {
                $('.bankAccountName').val(id);
            }

        },   
    });

}

$(document).on("change",".setType",function(){
    var value = $(this).val();
    if(value=='1')
    {

        $(".accounts").hide();
        $(".cards").show();

    }
    else
    {
        var validator = $("#addCards").validate();
        validator.resetForm();
        $(".accounts").show();
        $(".cards").hide();
    }

});
$(document).ready(function(){


    cardLists();
    bankLists();
    checkAutoPay();
    $('.exp_date').datepicker({
        yearRange: '2019:2040',
        changeMonth: true,
        changeYear: true,
        dateFormat: 'mm/yy'
    });

});


$("#addCards").validate({
    rules: {
        card_number: {
            required:true,
            number:true,
            maxlength: 16
        },
        exp_date: {
            required: true,
        },
        cvc: {
            required: true,
            number:true
        },
        holder_name: {
            required: true,
        },
    },
    submitHandler: function (e) {
        var form = $('#addCards')[0];
        var formData = new FormData(form);
        formData.append('action','addVendorsCardDetails');
        formData.append('class','Stripe');
        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status=="true")
                {
                    toastr.success(response.message);
                    cardLists();
                    $("#addCards").trigger("reset");
                }
                else
                {
                    toastr.error(response.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});


function cardLists()
{

    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        data: {
            "action": 'getAllVendorCards',
            "class": 'Stripe'
        },
        success: function (response) {


            $(".cardDetails").html(response);


        }
    });

}





function bankLists()
{

    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        data: {
            "action": 'getVendorBanks',
            "class": 'Stripe'
        },
        success: function (response) {


            $(".accountDetails").html(response);


        }
    });

}



$(document).on("change",".cardAction",function(){
    var cardAction = $(this).val();
    var card_id = $(this).attr('data-cardId');
    if(cardAction=='delete')
    {
        $(".cardAction").val('');
        bootbox.confirm("Do you want to delete this card?", function (result) {
            if (result == true) {

        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
                "action": 'deleteVendorCard',
                "class": 'Stripe',
                "card_id":card_id,
            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);

                cardLists();
                bankLists();




            }
        });

            }
        });

    }
    else if(cardAction=='default')
    {
        $(".cardAction").val('');
        bootbox.confirm("Do you want to set this method to default?", function (result) {
            if (result == true) {
        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
                "action": 'defaultVendorCard',
                "class": 'Stripe',
                "card_id":card_id,
            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);

                cardLists();
                bankLists();



            }
        });
            }
        });
    }


});


function checkAutoPay()
{
    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        data: {
            "action": 'checkAutoPayVendor',
            "class": 'Stripe'
        },
        success: function (data_res) {
            var info = JSON.parse(data_res);
            if(info.data=="ON"){

                $('.auto_pay').prop('checked', true);
            }
            else

                $('.auto_pay').prop('checked', false);
            }

    });

}

$(document).on("click",".cancel",function(){
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            location.reload();
        }
    });

});