$(document).ready(function () {


    $(document).on('click','#cancel_order',function () {
        window.location.href = window.location.origin + '/WorkOrder/WorkOrders';
    });
 $(document).on('click','#saveWorkOrderTab',function () {
       $('#hidden_val').val(1);
    });
    $(document).on('click','#save_another',function () {
       $('#hidden_val').val(0);
    });


    setTimeout(function(){
    $(".portfolio_id ").trigger("change");

    }, 500);
    // workorderedit();
    function workorderedit(){

        var id=$('#workorder_id').val();
        console.log(id);

        $.ajax({
            type: 'post',
            url: '/work-order-ajax',
            data: {
                class: 'workOrder',
                action: 'getworkorderdata',
                id : id
            },

            success: function (response) {
                var data = JSON.parse(response);


                if (data.status == "success"){
                  if(data.data.work_order_type==0){
                      $('#one_time').attr('checked',true);
                  }
                  else if(data.data.work_order_type==1){

                      $('#recurring').attr('checked',true);
                  }
                 if(data.data.send_alert == 'on'){
                     $('.send_alert').attr('checked',true);
                 }

                  $('.tenant_name').html(data.data.tenant_name);
                    $('.tenant_email').html(data.data.email);
                    $('.tenant_phone').html(data.data.phone);
                    $('.tenant_info_id').val(data.data.ten_id);


                    $('.work_categoryy').val(data.data.work_order_cat);
                    $('.priority_typee').val(data.data.priority_id);
                    setTimeout(function(){
                         $('#property_idd').val(data.data.property_id).trigger("change");
                    }, 600);
                    setTimeout(function(){
                        $('#building_id').val(data.data.building_id).trigger("change");
                    }, 700);
                    setTimeout(function(){
                        $('#unit_id').val(data.data.unit_id).trigger("change");
                    }, 800);



                    $.each(data.data, function (key,value) {

                        $('.'+key).val(value);

                    });

                  $('.vendor_adress').val(data.data1.vendor_adress1+' '+data.data1.vendor_adress2);
                  $('.vendor_name1').val(data.data1.vendor_name);
                  $('.vendor_info_id').val(data.data1.vendor_id);

                    $.each(data.data1, function (key,value) {

                        $('.'+key).html(value);

                    });


                    toastr.success(data.message);
                    jQuery('#workorder_table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('#workorder_table').find('tr:eq(1)').find('td:eq(11)').addClass("green_row_right");

                } else {
                    toastr.error(data.message);
                }
            }
        });
    }

    function onTop(rowdata,table){
        if(rowdata){
            setTimeout(function(){
                if (table == ""){
                    jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('.table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                }else{
                    jQuery('#'+table).find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('#'+table).find('tr:eq(1)').find('td').last().addClass("green_row_right");
                }
            }, 300);
        }
    }



    $(document).on("click",".vendor-info-radio",function() {

        $(".preferredvendorType").val(this.value);
        var vendortypeRadio = $(".preferredvendorType").val();
        if(vendortypeRadio == 'all'){
            $(".allDiv").show();
            $(".preffDiv").hide();
        }

  if (vendortypeRadio == 'preferred') {

      $(".allDiv").hide();
            $(".preffDiv").show();


        }

    });


     $(document).on('click','.owner_pre_vendor',function () {

           allvendor();

        });


    $(document).on('click','.owner_pre_vendor2',function () {

        preferredvendor();

    });


    $(document).on("click",".vendor_iddd", function(){
        var idd=$(this).attr('data-id');

       $('.vendor_info_id').val(idd);
        var name = $(this).attr('data-name');
        var add=$(this).attr('data-address');
        var city=$(this).attr('data-city');

        var state=$(this).attr('data-state');

        var country=$(this).attr('data-country');
        var zip=$(this).attr('data-zip');
        var phone=$(this).attr('data-phone');
        var email=$(this).attr('data-email');
        var type=$(this).attr('data-type');

        $("input[name='vendor_name']").val(name);
        $('#vendor_address').val(add);

        $(".addressupdbtn").show();

         $("#ven_address1").val(add);
         $("#ven_city").val(city);
         $("#ven_state").val(state);
         $("#ven_country").val(country);
         $("#ven_zip_code").val(zip);
         $("#nametable").html(name);
          $("#cattable").html(type);
          $("#emailtable").html(email);
          $("#phntable").html(phone);


        $('.list_container').removeClass("in");

    });



    function allvendor(){
        $.ajax({
            type: 'post',
            url: '/work-order-ajax',
            data: {
                class: 'workOrder',
                action: 'getVendorsdata',
                ven_type: 'all'
            },
            success: function (response) {

                var data = $.parseJSON(response);


                        var tableRow = '';
                        $.each(data.rows, function (key, value) {
                            console.log('dataaa',value.vendor_rate);
                            tableRow += "<tr style='cursor: pointer;'>";
                            tableRow +=     "<td style='display: none;'>"+value.id+"</td>";
                            tableRow +=     "<td ><a href='JavaScript:Void(0)' class='vendor_iddd' data-id="+value.id+" data-name="+value.name+" data-address="+value.address1+" " +
                                "data-city="+value.city+" data-state="+value.state+" data-country="+value.country+" " +
                                "data-email="+value.email+" data-type="+value.vendor_type+" data-phone="+value.phone_number+" data-zip="+value.zip+">"+value.name+"</td>";
                            tableRow +=     "<td>"+value.address1+"</td>";
                            tableRow +=     "<td>"+value.email+"</td>";
                            tableRow +=     "<td>"+value.vendor_type+"</td>";
                            tableRow +=     "<td>"+value.vendor_rate+"</a></td>";

                        });

                        $('#vendorlist1 tbody').html(tableRow);
                        $('.list_container').addClass("in");


            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



// $(document).on('click','html body :not("#tabledropdown1")',function () {
//     $('.list_container').removeClass("in");
// });

    $(document.body).click( function(e) {
        var container = $("#tabledropdown1");
        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            $('.list_container').removeClass("in");
        }

    });


    function preferredvendor(){
        $.ajax({
            type: 'post',
            url: '/work-order-ajax',
            data: {
                class: 'workOrder',
                action: 'getPreffVendorsdata',
                ven_type: 'preferred'
            },
            success: function (response) {
                var data = $.parseJSON(response);

                if (data.status == "success") {

                        var tableRow = '';
                        $.each(data.data, function (key, value) {
                            console.log(value.vendor_type);
                            tableRow += "<tr style='cursor: pointer;'>";
                            tableRow +=     "<td style='display: none;'>"+value.id+"</td>";
                            tableRow +=     "<td ><a href='JavaScript:Void(0)' class='vendor_iddd' data-id="+value.id+" data-name="+value.name+" data-address="+value.address1+" " +
                                "data-city="+value.city+" data-state="+value.state+" data-country="+value.country+" " +
                                "data-email="+value.email+" data-type="+value.vendor_type+" data-phone="+value.phone_number+" data-zip="+value.zip+">"+value.name+"</td>";
                            tableRow +=     "<td>"+value.address1+"</td>";
                            tableRow +=     "<td>"+value.email+"</td>";
                            tableRow +=     "<td>"+value.vendor_type+"</td>";
                            tableRow +=     "<td>"+value.vendor_rate+"</a></td>";
                        });

                        $('#vendorlist1 tbody').html(tableRow);
                        $('.list_container').addClass("in");

                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    //toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



    $("#VendorAddressDetial").on('click',function(event){
        event.preventDefault();
        if($("#FormUpdateAddress").valid()){
            var id=   $('.vendor_info_id').val();
            var formData = $('#FormUpdateAddress :input').serializeArray();
            $.ajax({
                type: 'post',
                url: '/work-order-ajax',
                data: {
                    form: formData,
                    id: id,
                    class: 'workOrder',
                    action: 'updateAddress'},
                success: function (response) {
                    var data = $.parseJSON(response);
                    if (data.code == 200) {
                        toastr.success(response.message);
                        $("#open-vendor-address-edit").modal("hide");
                    } else if (data.status == "error") {
                        toastr.error(data.message);
                    } else {
                        toastr.error(data.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });





    $("#FormWorkOrder").validate({
        rules: {
            property_id: {
                required: true
            },
            building_id: {
                required: true
            },
            unit_id :{
                required: true
            },
            portfolio_id: {
                required: true
            }
        }
    });
    fetchAllVendorType(false);
    $("#work-order-randomno").val(getRandomNumber(6));

    $(".work-order-duration").change(function(){
        if(this.value == "0"){
            $("#monthddl").hide();
        }else{
            $("#monthddl").show();
        }
    });

    $("#workCreatedAt").datepicker({dateFormat: datepicker,
        changeYear: true,
        yearRange: "-100:+20",
        changeMonth: true,
    }).datepicker("setDate", new Date());
    $("#workScheduledon").datepicker({dateFormat: datepicker,
        changeYear: true,
        yearRange: "-100:+20",
        changeMonth: true,
    }).datepicker("setDate", new Date());
    $("#workCompletedon").datepicker({dateFormat: datepicker,
        changeYear: true,
        yearRange: "-100:+20",
        changeMonth: true,
    }).datepicker("setDate", new Date());


})


$(document).on('click',"#add-work-cat",function(){
    $("#NewCategoryPopup").show();
})
$(document).on('click',"#add-priority-cat",function(){
    $("#NewPriorityPopup").show();
})
$(document).on('click', '.cancelPopup', function () {
    $(this).parent().parent().parent().find('input[type=text]').val('');
    $(this).parent().parent().parent().find('span').text('');
});
$(document).on('click', '#Newportfolio', function () {
    $('#portfolio_id').val(getRandomNumber(6));

    $("#NewportfolioPopup").show();
});
$(document).on('click', '#add-work-status', function () {
    $("#NewWorkStatusPopup").show();
});
$(document).on('click', '#add-work-source', function () {
    $("#NewWorkSourcePopup").show();
});

$(document).on('click', '#add-request-by', function () {
    $("#NewRequestByPopup").show();
});


function clearPopUps(id) {
    $(id).find('input[type=text]').val('');
    $(id).find('span').text('');
}
$(document).on('click', ".grey-btn", function () {

    var box = $(this).closest('.add-popup');
    $(box).hide();

});
function getRandomNumber(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
$(document).on("change","#ddlWorkOrderAuthority",function(){
    if(this.value == "5"){
        $("#txtOther").show();
    }else{
        $("#txtOther").hide();
    }
});

$(document).on('click', '#open-vendor-modal', function () {
    bootbox.confirm("Do you want to add a New Vendor?", function (result) {
        if (result == true) {
            $("#vendorModel").modal('show');
            $("#FormSaveNewVendor").trigger('reset');
            $(".vendor_random_id").val(getRandomNumber(6));
            jQuery('.phone_format').mask('000-000-0000', {reverse: true});
            response({success: false});
        }
    });

});

$(document).on('click', '#cancelVendorAdd', function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $("#vendorModel").modal('hide');
        }
    });

});
$(document).on('click', '#cancelVendoraddressAdd', function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $("#open-vendor-address-edit").modal('hide');
        }
    });

});

$(document).on("change","#salutation",function(){
    var value = $(this).val();
    if(value == 1){
        $('#gender').prop('selectedIndex',0);
        $('.hidemaiden').hide();
    }
    if(value==2)
    {
        $("#gender").val("1");
        $('.hidemaiden').hide();
    }
    if(value==3)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    } if(value==4)
    {
        $('#gender').prop('selectedIndex',0);
        $('.hidemaiden').hide();
    } if(value==5)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    } if(value==6)
    {
        $("#gender").val("1");
        $('.hidemaiden').hide();
    } if(value==7)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    }
    if(value==8)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    } if(value==9)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    }


});
$(document).on("click", ".vendortypeplus", function () {
    $("#Newvendortype").show();
});
// add vendor type
$(document).on('click', '#NewvendortypeSave', function (e) {
    e.preventDefault();
    var formData = $('#Newvendortype :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {form: formData,
            class: 'addVendor',
            action: 'addVendorType'},
        beforeSend: function (xhr) {
            var res = true;
            $(".customValidatePortfoio").each(function () {
                res = validations(this);
            });
            if (res === false) {
                xhr.abort();
                return false;
            }
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#Newvendortype').hide(500);
                toastr.success(response.message);
                fetchAllVendorType(response.lastid);
            } else if (response.status == 'error' && response.code == 500) {
                toastr.warning(response.message);
            } else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});

function fetchAllVendorType(id) {
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {
            class: 'addVendor',
            action: 'fetchAllVendorType'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#vendor_type_options').html(res.data);
            if (id != false) {
                $('#vendor_type_options').val(id);
            }
        },
    });
}
function getAddressInfoByZip(zip) {
    if (zip.length >= 5 && typeof google != 'undefined') {
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': zip}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++) {
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number") {
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment") {
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political") {
                            addr.city = results[0].address_components[ii].short_name;
                        }
                        if (types == "administrative_area_level_1,political") {
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code") {
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political") {
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                     console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    setTimeout(function () {
                        response(addr);
                    }, 2000);

                } else {
                    response({success: false});
                }
            } else {
                response({success: false});
            }
        });
    } else {
        response({success: false});
    }
}
function response(obj) {
    console.log(obj);
    if (obj.success) {

        $('#country').val(obj.country);
        $('.citys').val(obj.city);
        $('.states').val(obj.state);

    } else {
        $('.citys').val('');
        $('.states').val('');
        $('#country').val('');
    }
}
$(document).on('focusout', '#zip_code', function () {
    getAddressInfoByZip($(this).val());
});
$("#zipcode").keydown(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        getAddressInfoByZip($(this).val());
    }
});
$("#FormSaveNewVendor").validate({
    rules: {
        first_name: {
            required: true
        },
        last_name: {
            required: true
        },
        vendor_random_id :{
            required: true
        },
        vendor_rate: {
            maxAmount: 10
        }
    }
});
//add vendor ajax
$("#FormSaveNewVendor").on('submit',function(event){
    event.preventDefault();

    if($("#FormSaveNewVendor").valid()){
        // checking carrier field validations
        var form = $('#FormSaveNewVendor')[0];
        var formData = new FormData(form);
        formData.append('action', 'addVendor');
        formData.append('class', 'workOrder');
        $.ajax({
            url: '/work-order-ajax',
            type: 'POST',
            data: formData, cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $("#vendorModel").modal("hide");
                } else if (response.message == 'Validation Errors!' && response.code == 400) {
                    toastr.warning('Email already exits!');
                } else if (response.message == 'Validation Errors!' && response.code == 400) {
                    toastr.warning('Email already exits!');
                } else if (response.code == 500) {
                    toastr.warning(response.message);
                }
            }
        });
    }
});
//add vendor notes clone
$(document).on("click", ".vendor-notes-plus", function () {
    var length = $(".vendor-notes-clone-divclass").length;
    if (length <= 2) {
        var newnode = $("#vendor-notes-clone-div").clone().find(".notes").val("").end().insertAfter("div.vendor-notes-clone-divclass:last");
        $(".vendor-notes-clone-divclass:not(:eq(0))  .vendor-notes-plus").remove();
        $(".vendor-notes-clone-divclass:not(:eq(0))  .vendor-notes-minus").show();
        if (length == 2) {
            $('.vendor-notes-plus').hide();
        }
    } else {
        $('.vendor-notes-plus').hide();
    }
    newnode.find('.notes').on('keydown', function(event) {
        if (this.selectionStart == 0 && event.keyCode >= 65 && event.keyCode <= 90 && !(event.shiftKey) && !(event.ctrlKey) && !(event.metaKey) && !(event.altKey)) {
            var $t = $(this);
            event.preventDefault();
            var char = String.fromCharCode(event.keyCode);
            $t.val(char + $t.val().slice(this.selectionEnd));
            this.setSelectionRange(1,1);
        }
    });

});
$(document).on('click', '.vendor-notes-minus', function () {
    $(this).parent().parent().remove();
    $('.vendor-notes-plus').show();
});

$(document).on('click', '.addressupdbtn', function () {
    $('#open-vendor-address-edit').modal("show");
    var relId=$(this).attr("rel");
    $(".addressUserId").val(relId);


});

//add vendor ajax


$(document).on("click",".addSingleData",function(){
    var value = $(this).attr('data-value');
    var value2 = $(this).attr('data-value2');
    var fieldValue = $("#"+value).val();
    var fieldValue2 = $("#"+value2).val();
    var table = $(this).attr('data-table');
    var column = $(this).attr('data-cell');
    var column2 = $(this).attr('data-cell2');
    var select = $(this).attr('data-select');
    var hide = $(this).attr('data-hide');
    var error = $(this).attr('data-error');

    $.ajax({
        url:'/work-order-ajax',
        type: 'POST',
        data: {
            "action": 'addSingleData',
            "class": 'workOrder',
            "table": table,
            "column": column,
            "column2":column2,
            "fieldValue2":fieldValue2,
            "fieldValue": fieldValue,
        },
        beforeSend: function (xhr) {
            var res = true;
            // checking portfolio validations
            $("."+error).each(function () {
                res = validations(this);
            });
            if (res === false) {
                xhr.abort();
                return false;
            }
        },
        success: function (response) {
            var info = JSON.parse(response);
            var option = "<option value="+info.id+">"+info.value+"</option>";
            $("."+select).append(option);
            $("#"+value).val('');
            $('.'+select).val(info.id);
            $(this).parents('.add-popup').hide();
            $("."+hide).hide();
            toastr.success('Data Added Successfully.');

        }
    });

});

$(document).on("click",".addPopUpData",function(){
    var value = $(this).attr('data-value');
    var fieldValue = $("#"+value).val();
    var table = $(this).attr('data-table');
    var column = $(this).attr('data-cell');
    var select = $(this).attr('data-select');
    var hide = $(this).attr('data-hide');
    var error = $(this).attr('data-error');

    $.ajax({
        url:'/work-order-ajax',
        type: 'POST',
        data: {
            "action": 'addPopUpData',
            "class": 'workOrder',
            "table": table,
            "column": column,
            "fieldValue": fieldValue,
        },
        beforeSend: function (xhr) {
            var res = true;
            // checking portfolio validations
            $("."+error).each(function () {
                res = validations(this);
            });
            if (res === false) {
                xhr.abort();
                return false;
            }

        },
        success: function (response) {
            var info = JSON.parse(response);
            var option = "<option value="+info.id+">"+info.value+"</option>";
            $("."+select).append(option);
            $("#"+value).val('');
            $('.'+select).val(info.id);
            $(this).parents('.add-popup').hide();
            $("."+hide).hide();
            toastr.success('Data Added Successfully.');

        }
    });

});
$(document).on("click",'.requestedbyCheckbox',function(){
    if(this.checked){
        $(".ddlWorkOrderRequest").attr("disabled",false);
    }else{
        $(".ddlWorkOrderRequest").attr("disabled",true);
    }

});


$(document).on("change",'.getallproperties',function(){
    var id=this.value;
    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'getAllBuildings',
            id:id
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('.getAllBuildings').html(res.html);
            $(".tenant_info_id").val(res.datatenantuser.id);

            $("#nametableVendor").html(res.datatenantuser.name);
            $("#emailtableVendor").html(res.datatenantuser.email);
            $("#phntableVendor").html(res.datatenantuser.phone_number);

        },
    });
});
$(document).on("change",'.getAllBuildings',function(){
    var id=this.value;
    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'getallunits',
            id:id
        },
        success: function (response) {
            var res = JSON.parse(response);
            console.log(res.html);
            $('.getallunits').html(res.html);

        },
    });
});

$(document).on("change",'.getallunits',function(){
    var id=this.value;
    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'petinUnits',
            id:id
        },
        success: function (response) {
            var res = JSON.parse(response);
            console.log(res.html);
            $('.petinUnits').html(res.html);

        },
    });
});
$(document).on("change",'.ddlPortfolioType',function(){
    var id=this.value;
    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'getAllProperties',
            id:id
        },
        success: function (response) {
            var res = JSON.parse(response);
            console.log(res.html);
            $('.getallproperties').html(res.html);

        },
    });
})
fetchAllPortfolio(false);
function fetchAllPortfolio(id) {

    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-portfolioname',
        data: {
            propertyEditid: propertyEditid,
            class: 'workOrder',
            action: 'fetchPortfolioname'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#portfolio_name_options').html(res.data);
            if (id != false) {
                $('#portfolio_name_options').val(id);

            }

        },
    });

}
fetchAllPriorities(false);
function fetchAllPriorities(id) {

    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'fetchAllPriorities'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#ddlPriorityTypeID').html(res.html);
            if (id != false) {
                $('#ddlPriorityTypeID').val(id);

            }

        },
    });

}
fetchAllCategory(false);
function fetchAllCategory(id) {
    var idd=$('#workorder_id').val();
    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'fetchAllCategory'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#ddlWorkOrderTypeID').empty().html(res.html);
            if (id != false) {
                $('#ddlWorkOrderTypeID').val(id);

            }

        },
    });

}
fetchAllStatus(false);
function fetchAllStatus(id) {

    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'workOrderStatus'},
        success: function (response) {
            var res = JSON.parse(response);

            $('.NewWorkStatusPopupID').html(res.html);
            if (id != false) {
                $('.NewWorkStatusPopupID').val(id);

            }

        },
    });

}
fetchAllSource(false);
function fetchAllSource(id) {

    $.ajax({
        type: 'post',
        url:'/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'workOrderSource'},
        success: function (response) {
            var res = JSON.parse(response);
            $('.ddlWorkOrderSource').html(res.html);
            if (id != false) {
                $('.ddlWorkOrderSource').val(id);

            }

        },
    });

}
fetchAllRequest(false);
function fetchAllRequest(id) {

    $.ajax({
        type: 'post',
        url: '/work-order-ajax',
        data: {
            class: 'workOrder',
            action: 'workOrderRequest'
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('.ddlWorkOrderRequest').html(res.html);
            if (id != false) {
                $('.ddlWorkOrderRequest').val(id);

            }

        },
    });
}

$(document).on("click",".property_info_plus",function(){
    var newnode = $("#propertyInfoDivID").clone().insertAfter("div.propertyInfoDiv:last");
    newnode.find(".property_info_plus").remove();
    newnode.find(".property_info_minus").show();
    newnode.find('input').val('');
});
$(document).on("click", ".property_info_minus", function () {
    $(this).closest('.propertyInfoDiv').hide();
});
$("#FormWorkOrder").on('submit',function(event){
    event.preventDefault();
    if($("#FormWorkOrder").valid()){
        var hidden=$('#hidden_val').val();

        var id=$('#workorder_id').val();
        var form = $('#FormWorkOrder')[0];
        var formData = new FormData(form);
        var data = convertSerializeDatatoArray();
        var count = file_library.length;
        var custom_field = [];
        $(".custom_field_html input").each(function () {
            var data = {'name': $(this).attr('name'), 'value': $(this).val(), 'id': $(this).attr('data_id'), 'is_required': $(this).attr('data_required'), 'data_type': $(this).attr('data_type'), 'default_value': $(this).attr('data_value')};
            custom_field.push(data);
        });
        var custom_field = JSON.stringify(custom_field);
        $.each(file_library, function (key, value) {
            if (compareArray(value, data) == 'true') {
                formData.append(key, value);
            }
        });
        var approved_by_owner='';
        if ($('.approved_by_owner').is(":checked")) {
            var approved_by_owner = 'on';
        }
        var publish_to_tenant='';
        if ($('.publish_to_tenant').is(":checked")) {
            var publish_to_tenant = 'on';
        }
        var send_alert ='';
        if ($('.send_alert').is(":checked")) {
            var send_alert = 'on';
        }
        var publish_to_owner='';
        if ($('.publish_to_owner').is(":checked")) {
            var publish_to_owner = 'on';
        }
        var publish_to_vendor='';
        if ($('.publish_to_vendor').is(":checked")) {
            var publish_to_vendor = 'on';
        }
        var tenant_id= $(".tenant_info_id").val();
        var vendor_id= $('.vendor_info_id').val();
        formData.append('action', 'updateworkorder');
        formData.append('class', 'workOrder');
        formData.append('custom_field', custom_field);
        formData.append('approved_by_owner', approved_by_owner);
        formData.append('publish_to_tenant', publish_to_tenant);
        formData.append('send_alert', send_alert);
        formData.append('publish_to_owner', publish_to_owner);
        formData.append('publish_to_vendor', publish_to_vendor);
        formData.append('tenant_id', tenant_id);
        formData.append('vendor_id', vendor_id);
        formData.append('id', id);
        $.ajax({
            url:'/work-order-ajax',
            type: 'POST',
            data: formData, cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    if(hidden == 1){
                    toastr.success(response.message);

                    window.location.href = window.location.origin + '/VendorPortal/Vendor/WorkOrderVendor';
                        onTop(true);
                    }

                    else if(hidden == 0){
                        window.location.href = window.location.href;
                    }
                } else if (response.message == 'Validation Errors!' && response.code == 400) {
                    toastr.warning('Email already exits!');
                } else if (response.message == 'Validation Errors!' && response.code == 400) {
                    toastr.warning('Email already exits!');
                } else if (response.code == 500) {
                    toastr.warning(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
    }




});
