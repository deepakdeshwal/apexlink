onLoadView();
function onLoadView()
{
    $.ajax({
        url:'/VendorPortal/Communication/ComposeVendorMailAjax',
        method: 'post',
        data: {
            class: "composeVendorMailAjax",
            action: "getRecievedEmail",
        },
        success: function (data) {
            info =  JSON.parse(data);
            if(info.status=="success"){
                var res = info.data;
                var html = '';
                $(res).each(function (key,value) {
                    html += '<li >\n' +
                        '   <div class="col-sm-6">\n' +
                        '      <div class="inbox-lt">\n' +
                        '         <label>'+value.created_at+'</label>\n' +
                        '         <span class="mail_subject" data_id="'+value.id+'">'+value.email_subject+'</span>\n' +
                        '      </div>\n' +
                        '   </div>\n' +
                        '   <div class="col-sm-6">\n' +
                        '      <div class="inbox-rt pull-right">\n' +
                        '         <label>Posted by</label>\n' +
                        '         <span>'+value.name+'</span>\n' +
                        '      </div>\n' +
                        '   </div>\n' +
                        '</li>';
                });

                $('#communication_inbox').html(html);
                $('.detailed_inbox').html('');
            }
        },
    });
}
$(document).on('click','#communication_inbox .mail_subject',function () {
    var id = $(this).attr('data_id');
    if(!id)
    {
        return false;
    }
    $.ajax({
        url:'/VendorPortal/Communication/ComposeVendorMailAjax',
        method: 'post',
        data: {
            class: "composeVendorMailAjax",
            action: "getRecievedEmailByID",
            id: id,
        },
        success: function (data) {
            var info =  JSON.parse(data);
            if(info.status=="success"){
                var res = info.data;
                var html = '<h4 style="text-decoration: underline;text-transform: capitalize;font-weight:700;width: 100%;margin-bottom: 10px;">'+res[0].email_subject+'</h4> ';

                $(res).each(function (key,value) {
                    html += '<li data_mid="'+value.id+'">\n' +
                        '   <div class="col-sm-12">\n' +
                        '      <div class="inbox-lt">\n' +
                        '         <label style="width:100%;border-color: lightgrey;\n' +
                        '    padding-bottom: 10px;">Date: <span style="float: none;\n' +
                        '    color: grey;\n' +
                        '    font-weight: 100;">'+value.created_at+'</span></label>\n' +
                        '         <span style="float: left;\n' +
                        '    width: 100%;\n' +
                        '    margin-bottom: 10px; font-weight:100;text-transform: capitalize;">'+value.email_message+'</span><br>\n' +
                        '      </div>\n' +
                        '   </div>\n' +
                        '</li>';
                    '';
                });

                var detailed_html = '<hr/><form id="comment_reply_section"><div class="comment_block"><h5>Leave a comment...</h5></div><textarea class="form-control summernote" name="mesgbody"></textarea>   <div class="form-outer2">\n' +
                    '                                        <div class="col-sm-12 mb-15">\n' +
                    '                                            <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>\n' +
                    '                                            <input id="file_library" type="file" name="file_library[]" multiple style="display: none;">\n' +
                    '                                            <button type="button" class="orange-btn" id="remove_library_file">Remove All Files</button>\n' +
                    '                                        </div>\n' +
                    '                                        <div class="col-sm-12">\n' +
                    '                                            <div class="row" id="file_library_uploads">\n' +
                    '                                            </div>\n' +
                    '                                        </div>' +
                    '' +
                    '' +
                    '<div class="col-sm-12">' +
                    '<input type="hidden" name="reply_id" value="'+res[0].id+'">' +
                    '<button type="button" id="cancel_receive_mail" class="blue-btn">Cancel</button>' +
                    '<button type="submit" id="reply_receive_mail" class="blue-btn" style="float: right;">Reply</button>' +
                    '</div></form>';

                $('#communication_inbox').html(html);
                $('.detailed_inbox').html(detailed_html);
                summerNote();
            }
        },

    });
});


//Summernote Js Starts
function summerNote()
{

    $('.summernote').summernote({
        addclass: {
            debug: false,
            classTags: [{title:"Button","value":"btn btn-success"},"jumbotron", "lead","img-rounded","img-circle", "img-responsive","btn", "btn btn-success","btn btn-danger","text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
        },
        width: '100%',
        height: '300px',
        //margin-left: '15px',
        toolbar: [
            // [groupName, [list of button]]
            ['img', ['picture']],
            ['style', ['style', 'addclass', 'clear']],
            ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
            ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
            ['extra', ['video', 'table', 'height']],
            ['misc', ['undo', 'redo', 'codeview', 'help']]
        ]
    });

}


$(document).on('submit','#comment_reply_section',function (e) {
    e.preventDefault();
    var form = $('#comment_reply_section')[0];
    var formData = new FormData(form);
    formData.append('action','replyMail');
    formData.append('class','replyMail');

    $.ajax({
        url:'/VendorPortal/Communication/ComposeVendorMailAjax',
        type: 'POST',
        data: formData,
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status == "success"){
                toastr.success('Message Sent Successfully');
                window.location.href = '/VendorPortal/Communication/InboxMails';
            } else if (response.status == 'error'){
                toastr.warning(response.message);
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

$(document).on('click','#cancel_receive_mail',function (e) {
    onLoadView();
});
//Summernote JS Ends