$(document).ready(function () {
    $(document).on("click",".cancelhistorynotes",function (event) {
        event.preventDefault();
        $(".vendor_note").hide(500);
    });

    $(document).on("click","#notesAndHistory",function (event) {
        var $grid = $("#vendor_table").setGridWidth($(window).width()-70);
    });
    var notestatus=$(".notestatus").val();

    if ($("#vendor_table")[0].grid) {
        $('#vendor_table').jqGrid('GridUnload');
    }
    jqGrid('All',true);


  $(document).on("submit", "#vendorNotes", function (e) {
        e.preventDefault();
        if ($('#vendorNotes').valid()) {
           
            var note_id = vendor_id;
            var note = $(".note").val();
            var type = $(".note_type").val();
            var tenantchargenotes=$(".tenantchargenotes").val();
            $.ajax({
                type: 'post',
                url: '/vendor-add-ajax',
                data: {
                    class: 'addVendor',
                    action: 'addvendorNotes',
                    id: note_id,
                    note:note,
                    type:type,
                    tenantchargenotes:tenantchargenotes
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        toastr.success(response.message);
                        $(".vendor_note").hide(500);
                        $("#vendor_table").trigger('reloadGrid');
                    } else if (response.status == 'success' && response.code == 200) {
                        toastr.success(response.message);
                        $(".vendor_note").hide(500);
                        $("#vendor_table").trigger('reloadGrid');
                    }

                    else if (response.status == 'error' && response.code == 503) {
                        toastr.warning(response.message);
                    }
                    setTimeout(function () {
                        jQuery('#vendor_table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('#vendor_table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                    }, 1000);
                }
            });
        }
    });





    /**
     * vendor notes listing
     * @param status
     */

    function jqGrid(status,actionstatus) {
        var id=vendor_id;
        var table = 'tenant_chargenote';
        var columns = ['Date','Time','Notes','Action'];
        var select_column = ['Edit', 'Delete'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [{column: 'user_id', value: id, condition: '='}];
        var columns_options = [
            {name:'Date',index:'created_at',align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Time',index:'created_at',searchoptions: {sopt: conditions},table:table},
            {name:'Notes',index:'note', align:"center",searchoptions: {sopt: conditions},table:table},
            {name: 'Action',hidden: actionstatus,index: 'select', title: false, align: "right", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table}
        ];
        var ignore_array = [];
        jQuery("#vendor_table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "tenant_chargenote",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                extra_where:extra_where,
                deleted_at:'no'
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: 'desc',
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Notes/History",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {},
            {},
            {},
            {top:200,left:200,drag:true,resize:false}
        );
    }

    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Active";
        else if(cellValue == '0')
            return "InActive";
        else
            return '';
    }

    $(document).on('click','#vendor_edit_notes',function (e) {
        e.preventDefault();
        $('#add_notes').show(500);
        $('#vendor_edit_notes').hide();
        $(".notestatus").val("1");


        if ($("#vendor_table")[0].grid) {
            $('#vendor_table').jqGrid('GridUnload');
        }
        jqGrid('All',false);

    });

    $(document).on('click',"#add_notes",function()
        {
            $('.vendor_note').show();
            $(".note_type").val('add');
            $("#vendorNotes").trigger('reset');
            $(".tenantchargenotes").val('');
            $('#vendorAdd').val('Save');
            $('#clearVendorNotes').show();
            $('#resetVendorNotes').hide();
            vendorPortalNote = $('#vendorNotes').serializeArray();
        }
    );



    /**  List Action Functions  */
    $(document).on('change', '#vendor_table .select_options', function () {
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        $(".tenantchargenotes").val(id);
        var row_num = $(this).parent().parent().index();
        if (opt == 'Edit' || opt == 'EDIT') {
            $("#vendorNotes").show(500);
            $('#vendor_table').find('.green_row_left, .green_row_right').each(function () {
                $(this).removeClass("green_row_left green_row_right");
            });

            $.ajax({
                type: 'post',
                url: '/vendor-add-ajax',
                data: {id: id, class: 'addVendor', action: 'getVendorNotes'},
                success: function (response) {
                    $(".vendor_note").show(500);
                    $('#vendorAdd').val('Update');
                    var data = JSON.parse(response);
                    if (data.code == 200) {
                        $("#ven_note_id").val(data.data.id);
                        $(".notes").val(data.data.note);
                        vendorPortalNote = $('#vendorNotes').serializeArray();
                        $('#clearVendorNotes').hide();
                        $('#resetVendorNotes').show();
                    } else if (data.code == 500) {
                        toastr.error(data.message);
                    } else {
                        toastr.error(data.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
            //$("#portfolio-table").trigger('reloadGrid');
        } else if (opt == 'Delete' || opt == 'DELETE') {
            opt = opt.toLowerCase();
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/vendor-add-ajax',
                            data: {id: id, class: 'addVendor', action: 'deleteVendorNotes'},
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                    $("#vendor_table").trigger('reloadGrid');
                                } else if (response.code == 500) {
                                    toastr.warning(response.message);
                                } else {
                                    toastr.error(response.message);
                                }

                            }
                        });
                    }

                }
            });
        }
        $('.select_options').prop('selectedIndex', 0);
    });
});