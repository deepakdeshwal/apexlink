//add property form client side validations
$("#addManagementFeeForm").validate({
    rules: {
        management_type:{
            required: true
        },
        management_value: {
            required: true
        }
    },
    messages: {
        portfolioName: {
            valueNotEquals: "Please select non-default value!"
        },
        countryCode: {
            valueNotEquals: "Please select non-default value!"
        },
        garageAvalible: {
            valueNotEquals: "Please select non-default value!"
        }
    }
});
