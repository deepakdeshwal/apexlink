
$("#ownerBasicPaymentDetails").validate({
    rules: {
        country: {
            required:true
        },
        account_type: {
            required:true,
        },
        account_number: {
            required:true,
            number:true,
            minlength:5,
            maxlength:20
        },
        routing_number: {
            required:true,
            maxlength: 12
        },
        city: {
            required:true
        },
        line1: {
            required:true
        },
        postal_code: {
            required:true
        },
        state: {
            required:true
        },
        email: {
            required:true,
            email:true
        },
        first_name: {
            required:true
        },
        last_name: {
            required:true
        },
        phone: {
            required:true
        },
        ssn_last: {
            required:true
        },
        day: {
            required:true
        },
        month: {
            required:true
        },
        year: {
            required:true
        }
    }
});


