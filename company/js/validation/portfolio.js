//add rule for select drop down
$.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg !== value;
}, "* This field is required.");

//add rule for number validation
$.validator.addMethod("checkNumber", function(value, element, arg){
    if(value == "") return true;
    var st = value.replace("-", "");
    if(isNaN(st.replace("-", ""))){
        return false;
    } else {
        return true;
    }
}, "Only number are allowed!");

//add property form client side validations
$("#addPortfolioForm").validate({
    rules: {
        portfolio_id: {
            required:true,
            maxlength: 100
        },
        portfolio_name: {
            required:true,
            maxlength: 500
        }
    }
});

