

// jQuery.validator.addMethod("specialCharachter", function(value, element) {
//     return this.optional(element) || /^[_A-z0-9]*((-|\s)*[_A-z0-9])*$/.test(value);
// }, "Name must contain only letters, number &  space");

$("#add_property_style").validate({
    rules: { property_style: {
            required: true,
        },
    },
    submitHandler: function () {
        // e.preventDefault();

        var formData = $('#add_property_style').serializeArray();

        var form_type = $("#form_type").val();
        var action = '';
        if(form_type == 'add'){
            action = 'insert'
            // console.log('action>>', action);
        }else{
            action = 'update'
        }
        $.ajax
        ({
            type: 'post',
            url: '/MasterData/PropertyStyle-Ajax',
            data: {
                class: "PropertyStyleAjax",
                action: action,
                form: formData,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    $("#addPropertystyleForm").hide(500);
                    // $("#propertstyle-table").trigger('reloadGrid');
                   // toastr.success(response.message);
                    localStorage.setItem("Message",response.message)
                    localStorage.setItem("rowcolor",true)
                    window.location.href = '/MasterData/AddPropertyStyle';
                } else if(response.status == 'error' && response.code == 400){
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.'+key).html(value);
                    });
                }else{
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});

$("#importPropertyForm").validate({
    rules: { import_file: {
            required: true
        },
    },
    submitHandler: function (form) {
        event.preventDefault();

        var formData = $('#importPropertyForm').serializeArray();
        var myFile = $('#import_file').prop('files');
        var myFiles = myFile[0];
        var formData = new FormData();

        formData.append('file', myFiles);
        formData.append('class', 'PropertyStyleAjax');
        formData.append('action', 'importExcel');
        $.ajax
        ({
            type: 'post',
            url: 'PropertyStyle-Ajax',
            processData: false,
            contentType: false,
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                // console.log(response); return false;
                if(response.status == 'success' && response.code == 200){
                    toastr.success(response.message);
                    setTimeout(function() { triggerReload(); },2000);
                    // triggerReload();
                    $("#ImportProperty").hide(500)
                    //window.location.href = '/MasterData/AddPropertyType';
                } else if(response.status == 'failed' && response.code == 503){
                    toastr.error(response.message);
                    // $('.error').html(response.message);
                    // $.each(response.message, function (key, value) {
                    //     $('.'+key).html(value);
                    // });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});
