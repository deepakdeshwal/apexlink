/* initialisation on load */

jQuery(document).ready(function () {

    //add announcement
    $("#add_property_groups_form").validate({
        rules: {
            group_name: {
                required: true
            }
        },
        submitHandler: function (form) {
            event.preventDefault();
            var is_default =  '';
            if ($('#is_default').is(":checked")) {
                is_default = '1';
            } else {
                is_default = '0';
            }
            var formData = $('#add_property_groups_form').serializeArray();
            formData.push({name: 'is_default', value: is_default});
            //formData.append('is_default', is_default);
            // formData = {
            //     'is_default': is_default
            // };
            var form_type = $("#form_type").val()
            var action = '';
            if(form_type == "add"){
                action = 'insert'
            }else{
                action = 'update'
            }
            $.ajax
                    ({
                        type: 'post',
                        url: '/propertygroups-ajax',
                        data: {
                            class: "PropertyGroupsAjax",
                            action: action,
                            form: formData,
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if(response.status == 'success' && response.code == 200){
                                localStorage.setItem("Message",response.message)
                                localStorage.setItem("rowcolor",true)
                               window.location.href = '/MasterData/AddPropertyGroup';
                            } else if(response.status == 'error' && response.code == 400){
                                $('.error').html('');
                                $.each(response.data, function (key, value) {
                                    $('.'+key).html(value);
                                });
                            }else{
                                toastr.error(response.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                // alert(key+value);
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
        }
    });





    $("#importPropertyGroupForm").validate({
        rules: { import_file: {
                required: true
            },
        },
        submitHandler: function (form) {
            event.preventDefault();

            var formData = $('#importPropertyGroupForm').serializeArray();
            var myFile = $('#import_file').prop('files');
            var myFiles = myFile[0];

            var formData = new FormData();
            formData.append('file', myFiles);
            formData.append('class', 'PropertyGroupsAjax');
            formData.append('action', 'importExcel');
            $.ajax
            ({
                type: 'post',
                url: '/propertygroups-ajax',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        $("#ImportPropertyGroups").hide(500)
                        triggerReload();
                        toastr.success(response.message);
                        // setTimeout(function() { triggerReload(); },2000);
                    } else if(response.status == 'failed' && response.code == 503){
                        toastr.error(response.message);
                        // $('.error').html(response.message);
                        // $.each(response.message, function (key, value) {
                        //     $('.'+key).html(value);
                        // });
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });




});