//add property form client side validations
$("#add_vendor_form").validate({
    rules: {
        vendor_random_id: {
            required:true
        },
        first_name: {
            required:true
        },
        last_name: {
            required:true
        }
    }
});


$("#update_vendor_address_form").validate({
    rules: {
        address1: {
            required:true
        },
        address2: {
            required:true
        },
        zipcode: {
            required:true
        }
    }
});
