//add rule for select drop down
$.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg !== value;
}, "* This field is required.");
//add rule for select drop down
$.validator.addMethod("checkNumber", function(value, element, arg){
    if(value == "") return true;
    var st = value.replace("-", "");
    if(isNaN(st.replace("-", ""))){
        return false;
    } else {
        return true;
    }
}, "Only number are allowed!");


//add propertySetup form client side validations
$("#addUserForm").validate({
    rules: {
        first_name: {
            maxlength: 20,
            required:true
        },
        mi: {
            //maxlength: 2
        },
        last_name: {
            maxlength: 20,
            required:true
        },
        maiden_name: {
            //maxlength: 20
        },
        nick_name: {
            //maxlength: 20
        },
        email: {
            required: true,
            email: true,
            maxlength: 50
        },
        status: {
            valueNotEquals: "default"
        },
        work_phone: {
            // checkNumber: 12
        },
        country: {
            // maxlength: 20
        },
        mobile_number: {
            required:true,
            checkNumber:12
        },
        fax: {
            // checkNumber: 12
        }
    }
});

$("#updateUserForm").validate({
    rules: {
        first_name: {
            maxlength: 20,
            required:true
        },
        last_name: {
            maxlength: 20,
            required:true
        },
        email: {
            required: true,
            email: true,
            maxlength: 50
        },
        status: {
            valueNotEquals: "default"
        },
        country: {
            // maxlength: 20
        },
        mobile_number: {
            required:true,
            checkNumber:12
        },
    }
});


