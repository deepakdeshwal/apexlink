/* initialisation on load */

jQuery(document).ready(function () {

    //add announcement
    $("#add_property_type").validate({
        rules: { property_type: {
                required: true
            },
        },
        messages: {
            property_type: {
                required: "* This field is required",
            }
        },
        submitHandler: function (form) {
            event.preventDefault();

            var formData = $('#add_property_type').serializeArray();

            var form_type = $("#form_type").val()
            var action = '';
            if(form_type == "add"){
                action = 'insert'
            }else{
                action = 'update'
            }
            $.ajax
                    ({
                        type: 'post',
                        url: '/propertytype-ajax',
                        data: {
                            class: "PropertyTypeAjax",
                            action: action,
                            form: formData,


                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if(response.status == 'success' && response.code == 200){

                                localStorage.setItem("Message",response.message)
                                localStorage.setItem("rowcolor",true)
                               window.location.href = '/MasterData/AddPropertyType';
                            } else if(response.status == 'error' && response.code == 400){
                                $('.error').html('');
                                $.each(response.data, function (key, value) {
                                    $('.'+key).html(value);
                                });
                            }else{
                                toastr.error(response.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                // alert(key+value);
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
        }
    });





    $("#importPropertyForm").validate({
        rules: { import_file: {
                required: true
            },
        },
        submitHandler: function (form) {
            event.preventDefault();

            var formData = $('#importPropertyForm').serializeArray();
            var myFile = $('#import_file').prop('files');
            var myFiles = myFile[0];

            var formData = new FormData();
            formData.append('file', myFiles);
            formData.append('class', 'PropertyTypeAjax');
            formData.append('action', 'importExcel');
            $.ajax
            ({
                type: 'post',
                url: '/propertytype-ajax',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        toastr.success(response.message);
                        setTimeout(function() { triggerReload(); },2000);
                       // triggerReload();
                        $("#ImportProperty").hide(500)
                        //window.location.href = '/MasterData/AddPropertyType';
                    } else if(response.status == 'failed' && response.code == 503){
                        toastr.error(response.message);
                        // $('.error').html(response.message);
                        // $.each(response.message, function (key, value) {
                        //     $('.'+key).html(value);
                        // });
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });





});