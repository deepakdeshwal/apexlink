/* initialisation on load */

jQuery(document).ready(function () {

    var id =  getParameterByName('id');
    /*initaiate jquery tables*/
    jqGridListBuilding('All', true, id);
    /*initaiate jquery tables*/
    fetchAllUnittypes(false);
    //for current time acc to timezone

    var status =  localStorage.getItem("active_inactive_status");
    $("#key_designator").val(default_name);

    if(status !== undefined) {
        if ($("#building-table")[0].grid) {
            $('#building-table').jqGrid('GridUnload');
        }
        //intializing jqGrid
        if(status == 'all'){
            jqGridListBuilding('All', true,id);
        }  else {
            jqGridListBuilding(status, true,id);
        }
        $('#jqGridStatus option[value='+status+']').attr("selected", "selected");

    }else{
        if ($("#properttype-table")[0].grid) {
            $('#properttype-table').jqGrid('GridUnload');
        }
        jqGridListBuilding('All', true,id);
    }


    //jqGrid status
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        $('#building-table').jqGrid('GridUnload');
        changeGridStatus(selected);
        jqGridListBuilding(selected, true,id);

    });


    getPropertyDetail(id)
    $("#property_id").val(id)

    function getParameterByName( name ){
        var regexS = "[\\?&]"+name+"=([^&#]*)",
            regex = new RegExp( regexS ),
            results = regex.exec( window.location.search );
        if( results == null ){
            return "";
        } else{
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }


    $(document).on("change", ".key_access_codes_list", function () {
        $(this).next('.key_access_codeclass').find('textarea').val('');

        var value = $(this).val();
        if (parseInt(value) >= 1) {
            $(this).parent().find('.key_access_codeclass').show();
            $(this).parent().find('.key_access_codeclass').val(' ');
        } else {
            $(this).parent().find('.key_access_codeclass').hide();
        }
    });

    $(document).on("click", "#NewkeyMinus", function () {
        $(this).parents('#key_optionsDiv').remove();
    });
    //jquery grid for property insurance
    function jqGridListBuilding(status, deleted_at,id) {
        // alert(id);
        var table = 'building_detail';
        var columns = ['Building Name', 'Building Id', 'No of Units', 'Status', 'Action'];
        var select_column = ['Edit', 'Delete'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [{column: 'property_id', value: id, condition: '='}];
        var extra_columns = [''];
        var columns_options = [
            {name: 'Building Name', index: 'building_name',title:false,width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter: buildingName,classes: 'pointer',attr:[{name:'flag',value:'building'}]},
            {name: 'Building Id', index: 'building_id', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,classes: 'pointer'},
            {name: 'No of Units', index: 'no_of_units', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,classes: 'pointer'},
            {name: 'Status', index: 'status', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:statusFmatter,classes: 'pointer'},
            {name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionBuildingFmatter,title:false}
        ];
        var ignore_array = [];
        jQuery("#building-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height:'100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "building_detail",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at: deleted_at,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname:'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Buildings",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}

            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }

    /**
     * jqGrid function to format status
     * @param status
     */
    function statusFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Active";
        else if(cellvalue == 0)
            return "InActive";
        else
            return '';
    }


    /**
     *  function to format property name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function buildingName(cellValue, options, rowObject) {
       // return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        if(rowObject !== undefined) {
            var flagValue = $(rowObject.Action).attr('flag');
            var id = $(rowObject.Action).attr('data_id');
            var flag = '';
            if (flagValue == 'yes') {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a id="flag" href="javascript:void(0);" data_url="/Property/PropertyView?id="' + id + '"><img src="'+window.location.origin+'/company/images/Flag.png"></a></span>';
            } else {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
            }

        }
    }

    /**
     * jqGrid function to format action column
     * @param status
     */

    function actionBuildingFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var editable = $(cellvalue).attr('editable');
            var select = '';
            if(rowObject.Status == 1)  select = ['<span><i class="fa fa-edit edit_building" data_id="' + rowObject.id + '" style="font-size:24px"></i></span>','<span><i data_id="' + rowObject.id + '" class="fa fa-window-close deactivate_building " style="font-size:24px" aria-hidden="true"></i></span>'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['<span><i data_id="' + rowObject.id + '" class="fa fa-edit edit_building" style="font-size:24px"></i></span>','<i data_id="' + rowObject.id + '" style="font-size:24px" class="fa fa-check activate_building" aria-hidden="true"></i>'];
            var data = '';
            if(select != '') {
                $.each(select, function (key, val) {
                    if(editable == '0' && (val == 'delete' || val == 'Delete')){
                        return true;
                    }
                    data += val
                });
            }
            return data;
        }
    }

    /*redirect to edit page */
    $(document).on("click",".edit_building",function(){
        var id = $(this).attr('data_id')
        window.location.href =  window.location.origin+'/Building/EditBuilding?id='+id;
    });

    /*ajax to deactivate building */

    $(document).on("click",".deactivate_building",function(){
        var id = $(this).attr('data_id')
        bootbox.confirm({
            message: "Do you want to deactivate the record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/building-ajax',
                        data: {class: 'buildingDetail', action: 'deactivate', id: id},
                        success : function(response){
                            var response =  JSON.parse(response);
                            if(response.status == 'success' && response.code == 200) {
                                toastr.success('Building deactivated successfully.');
                            }else if(response.status == 'error' && response.code == 503) {
                                toastr.error(response.message);
                            }else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                }
                triggerReload();
            }
        });

    });

    /*ajax to activate building */
    $(document).on("click",".activate_building",function(){
        var id = $(this).attr('data_id')
        bootbox.confirm({
            message: "Do you want to activate the record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/building-ajax',
                        data: {class: 'buildingDetail', action: 'activate', id: id},
                        success : function(response){
                            var response =  JSON.parse(response);
                            if(response.status == 'success' && response.code == 200) {
                                toastr.success('Building activated successfully.');
                            } else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                }
                triggerReload();
            }
        });
    });

    function triggerReload(){
        var grid = $("#building-table");
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    /**
     * Function for view Building on clicking row
     */
    $(document).on('click','#building-table tr td:not(:last-child)',function(){
        var id = $(this).closest('tr').attr('id')
        window.location.href = window.location.origin+'/Building/View?id='+id;
    })

    $("input").keyup(function () {
        $(this).css('textTransform', 'capitalize');
    });
    $("textarea").keyup(function () {
        $(this).css('textTransform', 'capitalize');
    });



    //add announcement
    $("#add_building").validate({
        rules: { building_id: {
                required: true
            },building_name: {
                required: true
            },address: {
                required: true
            },address: {
                required: true
            },total_keys: {
                number: true
            },no_of_units: {
                required: true,
                number: true,
                min: 1
            },unit_type: {
                required: true,
            },base_rent: {
                required: true,
                number: true,
            },market_rent: {
                required: true,
                number: true,
            },security_deposit: {
                required: true,
                number: true,
            },key_name: {
                required: true,
            },email: {
                email: true

            }

        },
        messages: {
            property_type: {
                required: "* This field is required",
            },
            no_of_units: {
                min: "* Minimum value is 1",
            }
        },
        submitHandler: function (form) {
            //event.preventDefault();
            var uploadform = new FormData();
            var formData = $('#add_building').serialize();
            var form_type = $("#form_type").val()
            var action = '';
            var custom_field = [];
            combine__data_photo_document_array=[];
            combine__data_photo_document_array=[];
            $(".custom_field_html input").each(function(){
                var data = {'name':$(this).attr('name'),'value':$(this).val(),'id':$(this).attr('data_id'),'is_required':$(this).attr('data_required'),'data_type':$(this).attr('data_type'),'default_value':$(this).attr('data_value')};
                custom_field.push(data);
            });
            var  combine_photo_document_array =[];
            var length = $('#photo_video_uploads > div').length;
            var length1 = $('#file_library_uploads > div').length;
            combine_photo_document_array =  $.merge( photo_videos, file_library );
            var dataaa = Array.from(new Set(combine_photo_document_array)); // #=> ["foo", "bar"]

            if(length > 0 || length1 > 0 ) {

                var data = convertSerializeDatatoArrayPhotos();
                var data1 = convertSerializeDatatoArray();
                var combine__data_photo_document_array =  $.merge( data, data1 );
                // var count = photo_videos.length;

                $.each(dataaa, function (key, value) {
                    if(compareArray(value,combine__data_photo_document_array) == 'true'){
                        uploadform.append(key, value);
                    }
                    // if(key+1 === count){
                    //     savePhotoVideos(uploadform);
                    // }
                });
            }
            uploadform.append('class', "buildingDetail");
            uploadform.append('action', "addBuilding");
            uploadform.append('custom_field', JSON.stringify(custom_field));
            uploadform.append('form', formData);

            $.ajax
            ({
                type: 'post',
                url: '/Building/AddBuildingDetail',
                processData: false,
                contentType: false,
                data: uploadform,
                beforeSend: function (xhr) {

                    var radioValue = $("input[name='@key_tracker']:checked").val();
                    if(radioValue == 'track_key'  && $("#track_key_tracker").is(":visible")) {
                        // checking portfolio validations
                        $(".customValidateTrackKey").each(function () {
                            var res = validations(this);
                            if (res === false) {
                                xhr.abort();
                                return false;
                            }
                        });
                    }
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    var property_id = $("#property_id_span").val();
                    if(response.status == 'success' && response.code == 200){
                        if(response.building_count_after_insertion < response.property_data.data.no_of_buildings){
                            bootbox.confirm("Do you want to Add More Buildings ?", function (result) {
                                if (result == true) {
                                    window.location.reload();
                                }else{
                                    bootbox.confirm("Do you want to Add Units ?", function (result) {
                                        if (result == true) {
                                            window.location.href = window.location.origin+'/Unit/AddUnit?id='+id;
                                        }else{
                                            var res = update_users_flag('general_property',id);
                                            localStorage.setItem('rowcolor', 'rowColor');
                                            console.log(res);
                                            if(res.code==200){
                                                setTimeout(function () {
                                                    window.location.href = '/Property/PropertyModules';
                                                },100)
                                            }
                                        }
                                    });
                                }
                            });
                        }else{
                            bootbox.confirm("Do you want to Add Units ?", function (result) {
                                if (result == true) {
                                    window.location.href = window.location.origin+'/Unit/AddUnit?id='+id;
                                }else{
                                    var res = update_users_flag('general_property',id);
                                    localStorage.setItem('rowcolor', 'rowColor');
                                    console.log(res);
                                    if(res.code==200){
                                        setTimeout(function () {
                                            window.location.href = '/Property/PropertyModules';
                                        },500)
                                    }
                                }
                            });
                        }
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }else{
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });

    function compareArray(data,compare){
        for(var i =0;i < compare.length;i++){
            if(compare[i].name == data['name'] && compare[i].size == data['size']){
                return 'true';
            }
        }
        return 'false';
    }

    $(".grey-btn").click(function () {
        let box = $(this).closest('.add-popup');
        $(this).closest('.add-popup').find('input[type=text]').val('');
        $(box).hide();


    });

    $(document).on("click","#addnewkey",function(){
        $("#add_key_tracker").show(500);
        $("#track_key_tracker").hide(500);
    });

    $(document).on("click","#addtrackkey",function(){
        $("#add_key_tracker").hide(500);
        $("#track_key_tracker").show(500);
    });

    $('input[type=radio]').change(function() {
        if (this.value == 'add_key') {
            $("#addnewkey").show();
            $("#addtrackkey").hide();
            $("#add_key_tracker").hide(500);
            $("#track_key_tracker").hide(500);
        }
        else if (this.value == 'track_key') {
            $("#addnewkey").hide();
            $("#addtrackkey").show();
            $("#add_key_tracker").hide(500);
            $("#track_key_tracker").hide(500);
        }
    });

    /*for multiple phone number textbox*/
    $(document).on("click", "#multiplephoneno", function () {
        var clone = $(".multiplephonediv:first").clone().find("input:text").val("").end();
        var divlength = $(".multiplephonediv").clone();
        if (divlength.length <= 2) {
            $(".multiplephonediv").last().after(clone);
            $(".multiplephonediv:not(:eq(0))  #multiplephoneno").remove();
            $(".multiplephonediv:not(:eq(0))  #multiplephonenocross").show();
        }
        if (divlength.length == 2) {
            $(this).hide();
        }
    });
    /*remove multiple phone number textbox*/
    $(document).on("click", "#multiplephonenocross", function () {
        $(this).parents('.multiplephonediv').remove();
        $("#multiplephoneno").show();

    });

    $(function() {
        var regExp = /[a-z]/i;
        $(document).on('keydown keyup','.p-number-format', function(e) {
            if((event.which >= 48 && event.which <= 57) && event.shiftKey) {
                event.preventDefault();
            }
            if((event.which >= 95 && event.which <= 105) && event.shiftKey) {
                event.preventDefault();
            }
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)&& (event.which < 96 || event.which > 105) && event.which != 8) {
                event.preventDefault();
            }
        });
    });

    /*for multiple fax number textbox*/
    $(document).on("click", "#multiplefaxno", function () {
        var clone = $(".multiplefaxdiv:first").clone().find("input:text").val("").end();;
        var divlength = $(".multiplefaxdiv").clone();
        if (divlength.length <= 2) {
            $(".multiplefaxdiv").last().after(clone);
            $(".multiplefaxdiv:not(:eq(0))  #multiplefaxno").remove();
            $(".multiplefaxdiv:not(:eq(0))  #multiplefaxnocross").show();
        }
        if (divlength.length == 2) {
            $(this).hide();
        }
    });
    /*remove multiple phone number textbox*/
    $(document).on("click", "#multiplefaxnocross", function () {
        $(this).parents('.multiplefaxdiv').remove();
        $("#multiplefaxno").show();

    });

    $("input[name='last_renovation_date']").datepicker({
        dateFormat: date_format,
        autoclose: true,
        changeMonth: true,
        changeYear: true
    });


    $("#pick_up_date").datepicker({dateFormat: date_format,
        changeYear: true,
        minDate: 0,
        yearRange: "-100:+20",
        changeMonth: true,
        onClose: function (selectedDate) {
            $("#return_date").datepicker("option", "minDate", selectedDate);
        }}).datepicker("setDate", new Date());;

    //$("#pick_up_time").datepicker({dateFormat: date_format});
    $("#pick_up_time").timepicker({ timeFormat: 'h:mm p',defaultTime: new Date()});
    $("#return_date").datepicker({dateFormat: date_format, changeYear: true,
        minDate: 0,
        yearRange: "-100:+20",
        changeMonth: true,
        onClose: function (selectedDate) {
            $("#return_date").datepicker("option", "maxDate", selectedDate);
        }}).datepicker("setDate", new Date());


    // $("#return_time").datepicker({dateFormat: datepicker});
    $("#return_time").timepicker({ timeFormat: 'h:mm p',defaultTime: new Date()});

    $("input[name='last_renovation_time']").timepicker({ timeFormat: 'h:mm p',defaultTime: new Date()});
    $("#building_id").val(Math.random().toString(36).slice(2).substr(0, 6).toUpperCase());
    $("#no_of_units").val(1);

    jQuery(document).on("keyup keydown",'.p-number-format',function() {
        $(this).val($(this).val().replace(/^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/, "$1-$2-$3"));
    });

    jQuery(document).on("click",'#no_seprate_unit_checkbox',function() {
        var no_of_units = $("#no_of_units").val();
        if (no_of_units == 1) {
            if ($(this).is(":checked"))
                $("#no_seprate_unit").show(500);
            else
                $("#no_seprate_unit").hide(500)
        } else {
            toastr.error("Number of units must be equal to 1");
            return false;
        }


    });

    jQuery(document).on("click",'#selectAllamennity input[type=checkbox]',function() {
        if ($(this).is(":checked") && $(this).val()== 'selectall')
            $('.inputAllamenity').prop('checked', true);
        else
            $('.inputAllamenity').prop('checked', false);

    });

    jQuery(document).on("keyup keydown",'#no_of_units',function() {
        if ($("#no_seprate_unit_checkbox").is(":checked") && $(this).val() > 1){
            $(this).val(1)
        }
    });

    $(document).on('blur','#base_rent', function(e) {
        var base_rent = parseInt($(this).val());
        $("#security_deposit").val(base_rent.toFixed(2));
        $("#base_rent").val(base_rent.toFixed(2));

    });

    $(document).on('blur','#security_deposit', function(e) {
        var security_deposit = parseInt($(this).val());
        $("#security_deposit").val(security_deposit.toFixed(2));

    });

    $(document).on('blur','#market_rent', function(e) {
        var market_rent = parseInt($(this).val());
        $("#market_rent").val(market_rent.toFixed(2));

    });


    //combo grid for owner_pre_vendor
    $('.owner_pre_vendor1').combogrid({
        panelWidth: 500,
        url: '/get-vendorsdata',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getVendorsdata',
            class: 'propertyDetail'
        },
        columns: [[
            {field: 'id', hidden: true},
            {field: 'name', title: 'Name', width: 60},
            {field: 'address1', title: 'Address', width: 120},
            {field: 'email', title: 'Email', width: 120},
        ]],
        onSelect: function (index, row) {
            var desc = row.address1;  // the product's description
            $("#vendor_address").val(desc);
            $("#vendor_name").val(row.name);
            $("#track_key_holder_id").val(row.name);
            $("#vendor_address").attr("disabled", false);
        }
    });
    $("#_easyui_textbox_input1").attr("placeholder", "Click here to pick a vendor");

    $(document).on("click","#unit_icon",function(){
        toastr.error("Please fill buildinginformation first");
    });
    $(document).on('click','#add_building_cancel',function(){
        bootbox.confirm("Do you want to cancel this action now ?", function (result) {
            if (result == true) {
                window.location.href = '/Building/BuildingModule/?id='+id;
            }
        });
    });







});
function fetchAllUnittypes(id) {
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllUnittypes'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#unit_type').html(res.data);
            if (id != false) {
                $('#unit_type').val(id);

            }
            $("#default_rent").val(response.default_rent);

        },
    });

}


function validateCustomField(data_value, data_required, arg, element, id) {
    var msg = '';
    if (data_value == "") {
        if (data_required == 1) {
            msg = 'This field is required';
            $('#' + id).after("<span>" + msg + "</span>");
            return false;
        }
    }
    if (arg == 'text') {
        if (data_value.length > 150) {
            msg = 'Please enter character less than 150';
            $('#' + id).next('.customError').text(msg);
            return false;
        }

    } else if (arg == 'number') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            $('#' + id).next('.customError').text(msg);
            return false;
        } else {
            if (data_value > 20) {
                msg = 'Please enter less than 20';
                $('#' + id).next('.customError').text(msg);
                return false;
            }
        }
    } else if (arg == 'currency') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            console.log(msg);
            $('#' + id).next('.customError').text(msg);
            return false;
        }

    } else if (arg == 'percentage') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            console.log(msg);
            $('#' + id).next('.customError').text(msg);
            return false;
        } else {
            if (data_value > 100) {
                msg = 'Maximum value is 100';
                $('#' + id).next('.customError').text(msg);
                return false;
            }
        }
    } else if (arg == 'url') {
        var expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm;
        var regex = new RegExp(expression);
        if (data_value.match(regex)) {
        } else {
            msg = 'Please enter a valid URL';
            $('#' + id).next('.customError').text(msg);
            return false;
        }
    }
    $('#' + id).next('.customError').text('');
    return true;
}

function getPropertyDetail(id) {
    $.ajax({
        type: 'post',
        url: '/get-property-detail ',
        data: {
            class: 'propertyDetail',
            action: 'gerPropertyDetail',
            id:id
        },
        success: function (response) {
            var res = JSON.parse(response);
            if(res) {
                var building_address = $.trim(res.property_detail.address1 +' '+res.property_detail.address2+' '+res.property_detail.address3+' '+res.property_detail.address4);
                $("#legal_name , #building_name").val(res.property_detail.property_name);
                $("#property_name_span").html(res.property_detail.property_name);
                $("#property_id_span").html(res.property_detail.property_id);
                $("#address").val(building_address);
                var school_district_muncipiality_data = res.school_district_muncipiality_data;
                if(school_district_muncipiality_data == ''){
                    $("#school_district_muncipiality").css("display","block");
                }

                $.each(school_district_muncipiality_data, function(i, value){
                    $("#school_district_html").append(
                        '<div class=" row col-sm-12">'+
                        '<div class="col-sm-2">'+
                        '<label>School District</label>'+
                        '<input class="form-control"  value="'+value.school_district+'" type="text" disabled />'+
                        '</div>'+
                        '<div class="col-sm-2">'+
                        '<label>Code</label>'+
                        '<input class="form-control"  value="'+value.code+'" type="text" disabled />'+
                        '</div>'+
                        '<div class="col-sm-2">'+
                        '<label>Notes</label>'+
                        '<input class="form-control" value="'+value.notes+'" type="text" disabled />'+
                        '</div>'+
                        '</div>'
                    );
                });
                if(res.property_detail.smoking_allowed == 0 ){
                    $('#smoking_allowed option[value=0]').attr("selected", "selected");
                    $('#smoking_allowed').attr("disabled", "disabled");
                }
                if(res.property_detail.pet_friendly == 1 ){
                    setTimeout(function(){
                        $('#pet_options option[value=1]').attr("selected", "selected");
                        $('#pet_options').attr("disabled", "disabled");
                    }, 500);


                }
                if(res.default_settings.data.default_rent !=''){
                    $("#market_rent").val(res.default_settings.data.default_rent);
                }
            }
        },
    });

}

