//add property form client side validations
$("#payment_setting_form").validate({
    rules: {
        tax_payer_name: {
            required:true
        },
        tax_payer_id: {
            required:true
        },
        consolidate_checks: {
            valueNotEquals:'default'
        },
        default_GL: {
            valueNotEquals:'default'
        },
        order_limit: {
            valueNotEquals:'default'
        },
        account_name: {
            required:true
        },
        bank_account_number: {
            required:true
        },
        routing_number: {
            required:true
        },
        eligible_for_1099: {
            valueNotEquals:'default'
        }
    }
});

