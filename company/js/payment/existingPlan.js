$(document).ready(function () {
    jqGrid();
    fetchAllPlans(false);
    function jqGrid(status) {
        var table = 'plans_history';
        var columns = ['Trial Account','Start Date','End Date','Number of units','Status'];
        var select_column = [];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var columns_options = [
            { name:'Trial Account',index:'trial_acc', width:80, align:"center", searchoptions: {sopt: conditions},table:table,formatter:trialAccFmatter},
            { name:'Start Date',index:'start_date', width:80,align:"center", searchoptions: {sopt: conditions},table:table,change_type:'date'},
            { name:'End Date',index:'end_date', width:80,align:"center", searchoptions: {sopt: conditions},table:table,change_type:'date'},
            { name:'Number of units',index:'no_of_units', width:80,align:"center", searchoptions: {sopt: conditions},table:table},
            { name:'Status',index:'status', width:80,align:"center", searchoptions: {sopt: conditions},table:table,formatter:statusFmatter}
        ];
        var ignore_array = [ ];
        jQuery("#existPlan-table").jqGrid({
            url: '/Companies/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                /*status: status,*/
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns
            },
            viewrecords: true,
            sortname: 'id',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: 10,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "User Plans",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            },
            // onSelectRow: function (id) {
            //     console.log('res>>>>', id);
            //     window.location.href = '/Plans/ViewPlan/'+id;
            // },
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:200,drag:true,resize:false} // search options
        );
    }

    function statusFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "ACTIVATED";
        else if(cellvalue == '0')
            return "INACTIVE";
        else
            return '';
    }

    function trialAccFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Yes";
        else if(cellvalue == '0')
            return "No";
        else
            return '';
    }




    fetchAllPlan();
    function fetchAllPlan() {
        $.ajax({
            type: 'post',
            url: '/payment-stripe-ajax',
            data: {
                class: 'paymentStripe',
                action: 'fetchAllPlanAjax'},
            success: function (response) {
                var res = JSON.parse(response);
                if (res.status == 'success' && res.code == 200) {
                    setTimeout(function(){
                        $(".renewPaymentType").val(res.Result.term_plan);
                        $(".renewPlanName").val(res.planDetail.id);
                        $(".renewNoFUnits").val(res.planDetail.number_of_units);
                        $(".renewPlanPrice").val(res.Result.plan_price);
                        $(".renewPayPlanPrice").val(res.Result.pay_plan_price);

                        if(res.Result.term_plan == 1){
                            $("#lblSubscriptionType").html('<b>MONTH TO MONTH</b>')
                        }else{
                            $("#lblSubscriptionType").html('<b>YEARLY TO YEARLY</b>');
                        }

                        if(res.GetSourceDetail != '') {
                            if (res.GetSourceDetail == 'yes') {
                                $("#credit_card").prop('checked', true);
                                $("#ach").prop('checked', false);
                            } else {
                                $("#ach").prop('checked', true);
                                $("#credit_card").prop('checked', false);
                            }
                        }
                        fetchAllPlans(res.dataupgrade.subscription_plan);
                        $(".UpgradeNewPlanName").val(res.dataupgrade.subscription_plan);
                        $(".UpgradeNewTermPlan").val(res.dataupgrade.term_plan);
                        $(".UpgradeNewPlanUnits").val(res.dataupgrade.no_of_units);
                        $(".planTermAmmout").html('<b>'+default_currency_symbol+res.dataupgrade.pay_plan_price+'</b>');
                        $(".spnPlanDollar").html('<b>'+default_currency_symbol+res.dataupgrade.pay_plan_price+'</b>');
                        $(".lblStartDate").html('<b>'+res.dataupgrade.start_date+'</b>');
                        $(".lblExpireDate").html('<b>'+res.dataupgrade.end_date+'</b>');

                        $(".UpgradeNewPlanPrice").val(res.dataupgrade.plan_price);
                        $(".UpgradeNewPayPlanPrice").val(res.dataupgrade.pay_plan_price);

                        }, 500);
                }

            },
        });
    }


    $(document).on("click",".SavePlans",function (e) {
        e.preventDefault();
            var formData = $('#form_renew_plan_data').serializeArray();
            $.ajax({
                type: 'post',
                url: '/payment-stripe-ajax',
                data: {
                    form: formData,
                    class: 'paymentStripe',
                    action: 'addRenewPlan'
                },
                success: function (result) {
                    var response = JSON.parse(result);
                    if ((response.status == 'success') && (response.code == 200)) {
                        toastr.success(response.message);
                        location.reload();
                    }
                }, error: function (jqXHR, status, err) {
                    console.log(err);
                },
            });

    });

    $(document).on("change",".UpgradeNewPlanName",function(e){
        e.preventDefault();
       var Plan=$(this).find('option:selected').attr('units');
        var Price=$(this).find('option:selected').attr('price');
       $('.UpgradeNewPlanUnits').val(Plan);
       $(".UpgradeNewTermPlan").attr('price',Price);
        var discount=$("#UsersdiscountPrice").val();

        var totalPricemonth=parseInt(Price) - discount;

        var totalPriceyear=parseInt(Price) * 12 ;
        var totaldiscount2= totalPriceyear - discount;
        var id=$('.UpgradeNewTermPlan').val();
        if(id == 1){
            $(".UpgradeNewPlanPrice").val(Price);
            $(".UpgradeNewPayPlanPrice").val(totalPricemonth);
        }else{
            $(".UpgradeNewPlanPrice").val(totalPriceyear);
            $(".UpgradeNewPayPlanPrice").val(totaldiscount2);
        }
    });
    $(document).on("change",".UpgradeNewTermPlan",function(e){
        e.preventDefault();
        var id=$(this).val();
        var price=$(this).attr('price');
        var discount=$("#UsersdiscountPrice").val();

        var totalPricemonth=parseInt(price) - discount;

        var totalPriceyear=parseInt(price) * 12 ;
        var totaldiscount2= totalPriceyear - discount;

        if(id == 1){
            $(".UpgradeNewPlanPrice").val(price);
            $(".UpgradeNewPayPlanPrice").val(totalPricemonth);
        }else{
            $(".UpgradeNewPlanPrice").val(totalPriceyear);
            $(".UpgradeNewPayPlanPrice").val(totaldiscount2);
        }



    });
    $("#form_upgrade_plan_data").validate({
        rules: {
            subscription_plan: {
                required: true
            },
            term_plan: {
                required: true
            },
            plan_price :{
                required: true
            },
            pay_plan_price: {
                required: true
            }
        }
    });
    $(document).on("submit","#form_upgrade_plan_data",function (e) {
        e.preventDefault();
        if ($('#form_upgrade_plan_data').valid()) {
        var formData = $('#form_upgrade_plan_data').serializeArray();
        var card=$("input[name='card']:checked").val();
        $.ajax({
            type: 'post',
            url: '/payment-stripe-ajax',
            data: {
                form: formData,
                class: 'paymentStripe',
                action: 'addUpgradePlan',
                card:card

            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success') && (response.code == 200)) {
                    toastr.success(response.message);
                    location.reload();
                }
                else if (response.code == '500') {
                    toastr.warning(response.message);
                }
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
    });
});

function fetchAllPlans(id) {
    $.ajax({
        type: 'post',
        url: '/payment-stripe-ajax',
        data: {
            class: 'paymentStripe',
            action: 'fetchPlanAjax',
            id:id },
        success: function (response) {
            var res = JSON.parse(response);
            $(".UpgradeNewPlanName ").html(res.html);
            $("#UsersdiscountPrice").val(res.discount);

        },
    });

}


