$(document).ready(function() {
    // $(document).on('mouseover','.tooltipgridclass',function(){
    //     $(this).closest('td').css("overflow", "unset");
    // });
    // $(document).on('mouseover','.tooltipgridclass',function(){
    //     var last = $(this).closest('.table').find('tr:eq(1)').attr('id');
    //     var closest = $(this).closest('tr').attr('id');
    //     if(last != closest){
    //         console.log('0');
    //         $(this).closest('td').css("overflow", "unset");
    //         $(this).closest('td').find('span').removeClass('tooltiptextclass').addClass('tooltiptextbotclass');
    //     } else {
    //         $(this).closest('td').css("overflow", "unset");
    //         $(this).closest('td').find('span').removeClass('tooltiptextbotclass').addClass('tooltiptextclass');
    //     }
    // });



    $(document).on('mouseover','.tooltipgridclass',function(){
        $(this).closest('td').css("overflow", "unset");
        // $(this).closest('td').find('span').removeClass('tooltiptextclass').addClass('tooltiptextbotclass');

    });

    $(document).on('mouseout','.tooltipgridclass',function(){
        $(this).closest('td').css("overflow", "hidden");
    });
    $("#people_top").addClass("active");

    jqGridTenantListing('All','',pagination);

    /*  var base_url = window.location.origin;*/
    if(localStorage.getItem("rowcolorTenant"))
    {
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(9)').addClass("green_row_right");
            localStorage.removeItem('rowcolorTenant');
        }, 2000);
    }

    $(document).on("change", "#jqGridStatus", function (e) {
        var status = $(this).val();
        $('#employee-table').jqGrid('GridUnload');
        $('#userListings').jqGrid('GridUnload');
        $('#vendorListingTable').jqGrid('GridUnload');
        $('#owner_listings').jqGrid('GridUnload');
        $('#tenant_listings').jqGrid('GridUnload');

        jqGrid(status,true);
        jqGridUsers(status);
        jqGridvendorListing(status);
        jqGridOwnerListing(status);
        jqGridTenantListing(status,'',pagination);
        jqGridOtherUsers(status);

    });
    $(document).on('click','#AZ',function(){
        $('.AZ').hide();
        $('#apex-alphafilter').show();
    });

    $(document).on('click','#allAlphabet',function(){
        var grid = $("#employee-table");
        $('.AZ').show();
        $('#apex-alphafilter').hide();
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        //$('#companyUser-table').trigger( 'reloadGrid' );
    });
    $(document).on('click','.getAlphabet',function(){
        var grid = $("#employee-table"),f = [];
        var value = $(this).attr('data_id');
        var search = $(this).text();
        if(value != '0'){
            f.push({field:"name",op:"bw",data:search});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
        }
    })
    alphabeticSearch();

    function alphabeticSearch(){
        $.ajax({
            type: 'post',
            url:'/Companies/List/jqgrid',
            data: {class: 'jqGrid',
                action: "alphabeticSearch",
                table: 'users',
                column: 'name',
                where: [{column:'user_type',value:'5',condition:'=',table:'users'}]},
            success : function(response){
                var response = JSON.parse(response);
                if(response.code == 200){
                    var html = '';

                    $.each(response.data, function(key,val) {
                        var color = '#05A0E4';
                        if(val == 0) color = '#c5c5c5';
                        html += '<span class="getAlphabet" style="color:'+color+'" data_id="'+val+'">'+key+'</span>';
                    });
                    $('.AtoZ').html(html);
                }
            }
        });
    }



    /**
     *
     * count Number of tenants owner others vendors
     */
    getCount()
    function getCount(){
        // var id = $(".apxpg-main #contact_id").val();


        $.ajax({
            type: 'post',
            url: '/contactListAjax',
            data: {
                class: "ContactListAjax",
                action: "getcount"
                // id:id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var data1 = response.data;
                if (response.code == 200) {
                    $("#tenantCountNo").text(data1.tenant.count);
                    $("#ownersCountNo").text(data1.owners.count);
                    $("#vendorCountNo").text(data1.vendors.count);
                    $("#userCountNo").text(data1.users.count);
                    $("#otherCountNo").html(data1.others.count);

                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on('click','#employee-table tr td',function(e){
        e.preventDefault();
        var base_url = window.location.origin;
        var id = $(this).closest("tr").attr('id');
        if ($(this).index() == 0 || $(this).index() == 6 ) {
          return false;
        }else{
            window.location.href = base_url + '/Contact/View?id='+id;
        }


        /* ('tr td:not(:last-child)')*/
    });

    $(document).on('click','.classFlagRedirect',function(e){
        event.preventDefault();
        var base_url = window.location.origin;
        localStorage.setItem("AccordionHref",'#flag_bank');
        var url = $(this).attr('data_url');
        window.location.href = url;

    });



    /*  $('tr td:not(:last-child)');*/
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');
    function jqGrid(status, deleted_at) {

        if(jqgridNewOrUpdated == 'true')
        {
            var sortOrder = 'desc';
            var sortColumn = 'users.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }
        var table = 'users';
        var columns = ['Contact Name','Phone','Phone_Note', 'Email','Created Date','Status','Action'];
        var select_column = ['Edit','Email','Email History','TEXT','TEXT History','In-Touch History','Flag Bank','File Library','Notes & History','Add In-touch','Run Background Check','Archive Employee','Delete Employee','Print Envelope'];
        var joins = [{table:'users',column:'id',primary:'user_id',on_table:'employee_details'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [{column:'user_type',value:'5',condition:'=',table:'users'}];
        var columns_options = [

            {name:'Contact Name',index:'name',title:false, width:90,align:"center",searchoptions: {sopt: conditions},table:table,formatter: employeeName,classes: 'pointer',attr:[{name:'flag',value:'contact'}]},
            // {name: 'Phone', index: 'id',title:false, width: 100, searchoptions: {sopt: conditions}, table: table, classes: 'pointer',change_type:'line_multiple',index2:'phone_number',join:{table: 'users', column: 'id', primary: 'user_id', on_table: 'tenant_phone'},name_id:'id',formatter:addToolTip},
            {name: 'Phone', index: 'id',title:false, width: 80,searchoptions: {sopt: conditions}, table: table, classes: 'cursor',change_type:'line_multiple',index2:'phone_number',index3:'other_work_phone_extension',join:{table: 'users', column: 'id', primary: 'user_id', on_table: 'tenant_phone'},name_id:'id',formatter:addToolTip,cellattr:cellAttri}, /**cellattr:cellAttrdata**/
            {name: 'Phone_Note', index: 'phone_number_note', hidden:true, width: 80, searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer'},
            {name:'Email',index:'email', width:100,class:"pointer", align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Created Date',index:'created_at', width:100, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Status',index:'status', width:100,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Action',index:'', title: false, width:100,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},

        ];
        var ignore_array = [];
        jQuery("#employee-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "users",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Contacts",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }


    /**
     * jqGrid function to format tooltip
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function cellAttri(cellValue, options, rowObject) {
        if(rowObject !== undefined){

            if(rowObject.Phone_Note != ''){
                console.log('aaaaa>>',rowObject.Phone_Note);
                return 'title = " "';
            }
        }
    }

    /**
     * jqGrid function to format tooltip
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function addToolTip(cellValue, options, rowObject) {
        if(rowObject !== undefined){
            console.log(rowObject);
            if(rowObject.Phone_Note == ''){
                return cellValue;
            } else {
                console.log(rowObject.Phone_Note);
                return '<div class="tooltipgridclass">'+cellValue+'<span class="tooltiptextbotclass">'+rowObject.Phone_Note+'</span></div>';
            }
        }
    }


    function jqGridTenantListing(status, deleted_at,pagination) {
        if(jqgridNewOrUpdated == 'true')
        {
            var sortOrder = 'desc';
            var sortColumn = 'users.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }
        var table = 'users';
        var rentCurrSymbol = "Rent ("+currencySymbol+")";
        var balanceCurrSymbol = "Balance ("+currencySymbol+")";
        var columns = ['Tenant Name','Phone', 'Email', 'Created At'];
        // var columns = ['Tenant Name','Phone', 'Email', 'Property Name','Unit Number',rentCurrSymbol,balanceCurrSymbol,'Days Remaining','Status','Action'];
        //if(status===0) {
        var select_column = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Tenant Portal','Send Password Activation Mail','Deactivate Tenant account','HOA Violation Tracking','Print Envelope','Delete Tenant'];
        //}
        var joins = [{table:'users',column:'id',primary:'user_id',on_table:'tenant_property'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_details'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_lease_details'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_phone'},{table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},{table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        // var extra_where = [{column:'user_type',value:'2',condition:'=',table:'users'},{column:'record_status',value:'1',condition:'=',table:'tenant_details'}];
        var extra_where = [{column:'user_type',value:'2',condition:'=',table:'users'},{column:'record_status',value:'1',condition:'=',table:'tenant_details'},{column:'record_status',value:'1',condition:'=',table:'tenant_lease_details'}];
        // var pagination=[];
        var columns_options = [
            {name:'Tenant Name',index:'id', width:330,align:"left",searchoptions: {sopt: conditions},table:table,change_type: 'tenantname1', extra_columns: ['id'],original_index: 'id'},
            // {name:'Tenant Name',index:'id', width:330,align:"left",searchoptions: {sopt: conditions},table:table,formatter:titleCase},
            {name:'Phone',index:'phone_number', width:330,searchoptions: {sopt: conditions},table:'tenant_phone',cellattr: function () { return '  data-toggle="tooltip" data-placement="top" class="phonenotehover" data-original-title="Tooltip on top">'; }},
            {name:'Email',index:'email', width:330, align:"left",searchoptions: {sopt: conditions},table:table},
            {name:'Created At',index:'created_at', width:325, align:"left",searchoptions: {sopt: conditions},table:table},
            // {name:'Property Name',index:'property_name', width:80, align:"left",searchoptions: {sopt: conditions},table:'general_property'},
            // {name:'Unit Number',index:'unit_id', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_property',formatter:getUnitDetails},
            // {name:'Rent (USh)',index:'rent_amount', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details'},
            // {name:'Balance (USh)',index:'balance', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details',formatter:statusFormatter3},
            // {name:'Days Remaining',index:'days_remaining', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details',formatter:statusFormatter2},
            // {name:'Status',index:'status', width:80,align:"left",searchoptions: {sopt: conditions},table:'tenant_details',formatter:statusFormatter},
            // {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},

        ];
        var ignore_array = [];
        jQuery("#tenant_listings").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "users",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Tenants",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:200,left:200,drag:true,resize:false} // search options
        );
    }


    /**
     *  function to format employee name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function employeeName(cellValue, options, rowObject) {
        // return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';

        if(rowObject !== undefined) {

            var flagValue = $(rowObject.Action).attr('flag');

            var id = $(rowObject.Action).attr('data_id');

            var flag = '';

            if (flagValue == 'yes') {

                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold; text-transform: capitalize;">' + cellValue + '<a id="flag" class="classFlagRedirect" data_url="/Contact/View?id='+id+'"  href="javascript:void(0);" data_url="/Employee/View?id="' + id + '"><img src="/company/images/Flag.png"></a></span>';

            } else {

                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold; text-transform: capitalize;">' + cellValue + '</span>';

            }

        }
    }


    function nameFmatter(cellValue, options, rowObject){
        return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
    }



    function statusFormatter (cellValue, options, rowObject) {

        if (cellValue == 0)
            return "Archive";
        else if (cellValue == '1')
            return "Active";
        else
            return '';
    }


    function statusFmatter1 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            if(rowObject['Status'] == '0')  select = ['Email History','Run Background Check','Activate Contact','Delete Contact','Print Envelope']
            if(rowObject['Status'] == '1')  select = ['Edit','Email','Email History','TEXT','TEXT History','In-Touch History','Flag Bank','File Library','Notes & History','Add In-touch','Run Background Check','Convert To Tenant','Archive Contact','Delete Contact','Print Envelope']
            var data = '';

            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '" data_email="' + rowObject.Email + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }

            return data;
        }
    }
    $(document).on('click','#import_employee',function () {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $('#import_tenant_type_div').show(500);
    });
    $(document).on("click", "#import_tenant_cancel_btn", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#import_tenant_type_div").hide(500);
                }
            }
        });
    });
    $("#importTenantTypeFormId").validate({
        rules: { import_file: {
                required: true
            },
        },
        submitHandler: function (form) {
            event.preventDefault();

            var formData = $('#importPropertyForm').serializeArray();
            var myFile = $('#import_file').prop('files');
            var myFiles = myFile[0];
            var formData = new FormData();
            formData.append('file', myFiles);
            formData.append('class', 'EmployeeListAjax');
            formData.append('action', 'importExcel');
            $.ajax({
                type: 'post',
                url: '/contactListAjax',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        toastr.success(response.message);

                        $("#import_tenant_type_div").hide(500)
                        $('#employee-table').trigger('reloadGrid');
                    } else if(response.status == 'failed' && response.code == 503){
                        toastr.error(response.message);
                        // $('.error').html(response.message);
                        // $.each(response.message, function (key, value) {
                        //     $('.'+key).html(value);
                        // });
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });


    function deleteContact(id) {
        $.ajax({
            type: 'post',
            url: '/contactListAjax',
            data: {
                class: "ContactListAjax",
                action: "deleteContact",
                user_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#employee-table').trigger( 'reloadGrid' );
                    alphabeticSearch();
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    function changeStatus(id,status) {
        $.ajax({
            type: 'post',
            url: '/contactListAjax',
            data: {
                class: "ContactListAjax",
                action: "changeStatus",
                user_id: id,
                status:status
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#employee-table').trigger( 'reloadGrid' );
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    /** Export unit type excel  */
    $(document).on("click",'#export_tenant',function(){
        var base_url = window.location.origin;
        var status = $("#jqGridStatus option:selected").val();
        var table = 'users';
        var table1 = 'tenant_phone';
        var table2 = 'employee_details';
        window.location.href = base_url+"/contactListAjax?status="+status+"&&table="+table+"&&table1="+table1+"&&table2="+table2+"&&action=exportExcel";
        $(this).off('click');
        return false;
    });


    $(document).on("change", "#employee-table .select_options", function (e) {

        $('#employee-table').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        var row_num = $(this).parent().parent().index() ;
        jQuery('#employee-table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('#employee-table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");

        e.preventDefault();
        var base_url = window.location.origin;
        var action = this.value;
        var id = $(this).attr('data_id');
        var email = $(this).attr('data_email');
        switch (action) {
            case "Delete Contact":
                bootbox.confirm("Are you sure you want to delete this entry?", function (result) {
                    if (result == true) {
                        deleteContact(id);
                    }
                });
                break;
            case "Archive Contact":
                bootbox.confirm("Are you sure you want to archive the current record?", function (result) {
                    if (result == true) {
                        var status = 0;
                        changeStatus(id,status);
                    }
                });
                break;
            case "Activate Contact":
                bootbox.confirm("Are you sure you want to activate the current record?", function (result) {
                    if (result == true) {
                        var status = 1;
                        changeStatus(id,status);
                    }
                });
                break;
            case "Edit":
                 window.location.href = base_url + '/Contact/Edit?id='+id;
                break;
            case "Flag Bank":
                localStorage.setItem("AccordionHref",'#flag_bank');
                window.location.href = base_url+'/Contact/View?id='+id;
                break;
            case "File Library":
                localStorage.setItem("AccordionHref",'#filelibraryss');
                window.location.href = base_url+'/Contact/View?id='+id;
                break;
            case "Notes & History":
                localStorage.setItem("AccordionHref",'#noteshistory');
                window.location.href = base_url+'/Contact/View?id='+id;
                break;
            case "Work Order":
                window.location.href = base_url + '/WorkOrder/WorkOrders?WorkOrder='+id;
                break;
            case "Add In-touch":
                window.location.href = "/Communication/NewInTouch?tid="+id+"&category=Person";
                //window.location.href = base_url + '/Communication/InTouch';
                break;
            case "In-Touch History":
                var propertyname=$('#'+id).find('td:first').text();
                localStorage.setItem("propertyname",propertyname);
                window.location.href = base_url + '/Communication/InTouch';
                break;
            case "Email History":
                window.location.href = base_url + '/Communication/SentEmails';
                break;
            case "Email":

                localStorage.setItem('table_green_id',id);
                localStorage.setItem('table_green_url','/People/ContactListt');
                localStorage.setItem('predefined_mail',email);
                localStorage.setItem('table_green_tableid',"#employee-table");
                window.location.href = base_url + '/Communication/ComposeEmail';
                break;
            case "TEXT":

                var urlemail='/People/ContactListt';
                localStorage.setItem('predefined_text',email);
                localStorage.setItem('table_green_tableid', '#employee-table');
                localStorage.setItem('table_green_id',id);
                localStorage.setItem('table_green_url',urlemail);

                // window.location.href = base_url + '/Communication/AddTextMessage';
                //
                // localStorage.setItem('table_green_id',id);
                // localStorage.setItem('table_green_url','/People/ContactListt');
                // localStorage.setItem('predefined_mail',email);
                // localStorage.setItem('table_green_tableid',"#employee-table");
                window.location.href = base_url + '/Communication/AddTextMessage';
                break;
            case "TEXT History":
                window.location.href = base_url + '/Communication/TextMessage';
                break;
            case "Convert To Tenant":
                window.location.href = '/Tenantlisting/add';
                break;
            case "CONVERT TO TENANT":
                window.location.href = '/Tenantlisting/add';
                break;
            case "Run Background Check":
               // alert("pdf generation");
                $('#backGroundCheckPop').modal('show');
                break;
            case "Print Envelope":
                $.ajax({
                    type: 'post',
                    url: '/employeeListAjax',
                    data: {class: 'EmployeeListAjax', action: 'getEmployeeData','employee_id':id},
                    success : function(response){
                        var response =  JSON.parse(response);
                        if(response.status == 'success' && response.code == 200) {
                            $("#PrintEmployeeEnvelope").modal('show');
                            $("#user_company_name").text(response.data.data.company_name)
                            $("#user_address1").text(response.data.data.address1)
                            $("#user_address2").text(response.data.data.address2)
                            $("#user_address3").text(response.data.data.address3)
                            $("#user_address4").text(response.data.data.address4)
                            $(".city").text(response.data.data.city)
                            if(response.data.data.state != '') {
                                $(".state").text(', ' + response.data.data.state)
                            }
                            $(".postal_code").text(response.data.data.zipcode)
                            $("#employee_first_name").text(response.employee.data.first_name)
                            $("#employee_last_name").text(response.employee.data.last_name)
                            $("#employee_middle_name").text(response.employee.data.middle_name)
                            $("#employee_address1").text(response.employee.data.address1)
                            $("#employee_address2").text(response.employee.data.address2)
                            $("#employee_address3").text(response.employee.data.address3)
                            $("#employee_address4").text(response.employee.data.address4)
                            $(".employee_city").text(response.employee.data.city)
                            if(response.employee.data.state != '') {
                                $(".employee_state").text(', ' + response.employee.data.state)
                            }
                            $(".employee_postal_code").text(response.employee.data.zipcode)
                        }else {
                            toastr.warning('Record not updated due to technical issue.');
                        }
                    }
                });
                break;
            default:
                window.location.href = base_url + '/Setting/ApexNewUser';
        }
        $('.select_options').prop('selectedIndex',0);
    });



});

/*function to print element by id */
function PrintElem(elem)
{
    Popup($(elem).html());
}
/*function to print element by id */
function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.body).html( '<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if(mywindow.close()){

    }
    $("#print_complaint").modal('hide');
    $("#building-complaints").trigger('reloadGrid');
    return true;
}




/*nidhi code */









$(document).on('click','#tenant_listings tr td:not(:last-child)',function(e){
    e.preventDefault();
    var base_url = window.location.origin;
    var id = $(this).closest("tr").attr('id');
    window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
    /* ('tr td:not(:last-child)')*/
});

/** Tenant Listing Grid **/

//jqGridTenantListing('All');

function titleCase( cellvalue, options, rowObject) {
    if (cellvalue !== undefined) {

        var string = "";
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/getUserNameById',
            data: {
                class: "TenantAjax",
                action: "getUserNameById",
                id: cellvalue
            },
            async: false,
            success: function (response) {
                var res = JSON.parse(response);
                if (res.status == "success") {
                    string = res.data;
                }
            }
        });
        return string;
    }
}
function getUnitDetails( cellvalue, options, rowObject){
    var string = "";
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getUnitById',
        data: {
            class: "TenantAjax",
            action: "getUnitById",
            id: cellvalue
        },
        async: false,
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == "success") {
                string = res.data.unit_prefix+"-"+res.data.unit_no;
            }
        }
    });
    return string;
}

function statusFormatter (cellValue, options, rowObject){

    if (cellValue == 0)
        return "Inactive";
    else if(cellValue == '1')
        return "Active";
    else if(cellValue == '2')
        return "Evicting";
    else if(cellValue == '3')
        return "In-Collection";
    else if(cellValue == '4')
        return "Bankruptcy";
    else if(cellValue == '5')
        return "Evicted";
    else
        return '';

}
function statusFormatter2 (cellValue, options, rowObject){
    today=new Date();
    dt1 = new Date(today);
    dt2 = new Date(cellValue);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
}
function statusFormatter3 (cellValue, options, rowObject){
    return 1000;
}
function statusFmatter1 (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';
        if(rowObject['Status'] == '0')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Tenant Portal','Send Password Activation Mail','Deactivate Tenant account','HOA Violation Tracking','Print Envelope','Delete Tenant'];
        if(rowObject['Status'] == '1')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Tenant Portal','Send Password Activation Mail','Deactivate Tenant account','HOA Violation Tracking','Print Envelope','Delete Tenant'];
        if(rowObject['Status'] == '2')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Tenant Portal','Send Password Activation Mail','Deactivate Tenant account','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
        if(rowObject['Status'] == '3')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
        if(rowObject['Status'] == '4')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Tenant Portal','Send Password Activation Mail','Deactivate Tenant account','HOA Violation Tracking','Print Envelope','Delete Tenant'];
        if(rowObject['Status'] == '5')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
        if(rowObject['Status'] == '')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
        var data = '';

        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }

        return data;
    }
}


/*Owner Listing*/
$(document).on('click','#owner_listings tr td:not(:last-child)',function(e){
    e.preventDefault();
    var base_url = window.location.origin;
    var id = $(this).closest("tr").attr('id');
    window.location.href = base_url + '/People/ViewOwner?id='+id;
});
/**
 * jqGrid Initialization function
 * @param status
 */
jqGridOwnerListing(1);
function jqGridOwnerListing(status, deleted_at) {
    if(jqgridNewOrUpdated == 'true')
    {
        var sortOrder = 'desc';
        var sortColumn = 'users.updated_at';
    } else {
        var sortOrder = 'asc';
        var sortColumn = 'users.name';
    }
    var table = 'users';
    var columns = ['Owner Name','Phone', 'Email','Date Created'];
    var select_column = ['Edit','Email','Text','Work Order','Add In-touch','In-Touch History','Owner Draw','Owner Contribution','Owner statement','Owner Notes','File Library','Flag Bank','Email History','Text History','Run Background Check','Owner Portal','Send Password Activation Email','Owner Complaints','Archive Owner','Resign Owner','DeActivate Account','Print Envelope','Delete Owner'];

    var joins = [{table:'users',column:'id',primary:'user_id',on_table:'owner_details'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'user_type',value:'4',condition:'=',table:'users'}];
    var columns_options = [
        {name:'Owner Name',index:'name',title:false, width:320,align:"center",searchoptions: {sopt: conditions},table:table,formatter: ownerName,classes: 'pointer',attr:[{name:'flag',value:'owner'}]},
        // {name:'Owner Name',index:'name', width:90,align:"left",searchoptions: {sopt: conditions},table:table, classes:'pointer'},
        //  {name:'Company',index:'company_name', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Phone',index:'phone_number', width:330,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'phone_number_format'},
        {name:'Email',index:'email', width:330, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Date Created',index:'created_at', width:330, align:"center",searchoptions: {sopt: conditions},table:table},
        //   {name:'Owner\'s Portal',index:'owners_portal', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:ownersPortal},
        // {name:'Status',index:'status', width:80,align:"left",searchoptions: {sopt: conditions},table:'owner_details',formatter:statusFormatterr},
        // {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatters1},

    ];
    var ignore_array = [];

    jQuery("#owner_listings").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "users",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:true,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: sortColumn,
        sortorder: sortOrder,
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: 10,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Owners",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {top:200,left:200,drag:true,resize:false}
    );
}

// function statusFormatterr (cellValue, options, rowObject){
//
//     if (cellValue == '0' || cellValue == 0)
//         return "Inactive";
//     else if(cellValue == '1')
//         return "Active";
//     else if(cellValue == '2')
//         return "Archive";
//     else if(cellValue == '3')
//         return "Delete";
//     else if(cellValue == '4')
//         return "Past";
//     else
//         return '';
//
// }
/**
 * Function to format owner name
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function ownerName(cellValue, options, rowObject) {
    if(rowObject !== undefined) {
        var flagValue = $(rowObject.Action).attr('flag');
        var id = $(rowObject.Action).attr('data_id');
        var flag = '';
        if (flagValue == 'yes') {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a id="flag" href="javascript:void(0);" data_url="/Property/PropertyView?id="' + id + '"><img src="/company/images/Flag.png"></a></span>';
        } else {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }
}

function ownersPortal (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        if(cellvalue == '1') {
            return 'Yes';
        }else if(cellvalue == '2')
        {
            return 'Disabled';
        } else {
            return 'No';
        }
    }
}
function statusFmatters1 (cellvalue, options, rowObject){
    if(rowObject !== undefined) {

        var select = '';
        if(rowObject['Status'] == '1')
            var $owners = "Owner's Portal";
        if(rowObject[$owners] == '2'){
            select = ['Edit','Email','Text','Work Order','Add In-touch','In-Touch History','Owner Draw','Owner Contribution','Owner statement','Owner Notes','File Library','Flag Bank','Email History','Text History','Run Background Check','Owner Portal','Send Password Activation Email','Owner Complaints','Archive Owner','Resign Owner','Activate Account','Print Envelope','Delete Owner'];
        }else {
            select = ['Edit','Email','Text','Work Order','Add In-touch','In-Touch History','Owner Draw','Owner Contribution','Owner statement','Owner Notes','File Library','Flag Bank','Email History','Text History','Run Background Check','Owner Portal','Send Password Activation Email','Owner Complaints','Archive Owner','Resign Owner','DeActivate Account','Print Envelope','Delete Owner'];
        }
        if(rowObject['Status'] == '2')  select = ['Run Background Check','Activate this Account','Print Envelope','Delete Owner','Online Payment'];
        if(rowObject['Status'] == '4')  select = ['Run Background Check','Reactivate this Owner','Print Envelope','Delete Owner','Online Payment'];

        var data = '';

        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }

        return data;
    }
}
// $(document).on('click','#import_owner',function () {
//     $("#import_file-error").text('');
//     $("#import_file").val('');
//     $('#import_owner_div').show(500);
// });

/*Vendor Listing Starts*/

/**
 * jqGrid Initialization function
 * @param status
 */
jqGridvendorListing('1');
function jqGridvendorListing(status) {
    if(jqgridNewOrUpdated == 'true')
    {
        var sortOrder = 'desc';
        var sortColumn = 'users.updated_at';
    } else {
        var sortOrder = 'asc';
        var sortColumn = 'users.name';
    }
    var table = 'vendor_additional_detail';
    var columns = ['Vendor Name','Phone', 'Open Work Orders', 'YTD Payment', 'id','Vendor Status','Type', 'Email'];
    var select_column = ['Edit', 'Pay vendor', 'Work orders', 'New Bill', 'Email', 'Email History', 'Text', 'Text History', 'Add InTouch', 'InTouchHistory','File History','Notes & History','Flag Bank','Run Background Check','Send Password Activation Mail', 'Archive Vendor', 'Resign Vendor','Vendor Rating', 'Print Envelope','Delete Vendor','Online Payment'];
    var joins = [{table: 'vendor_additional_detail', column: 'vendor_id', primary: 'id', on_table: 'users'},{table: 'vendor_additional_detail', column: 'vendor_type_id', primary: 'id', on_table: 'company_vendor_type'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = ['vendor_additional_detail.vendor_id'];
    var extra_where = [];
    var columns_options = [
        {name: 'Vendor Name', title:true, index: 'name', width: 230, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer',name_id:'vendor_id',formatter:vendorName},
        {name: 'Phone', index: 'id', width: 220, searchoptions: {sopt: conditions}, table: table, classes: 'pointer',change_type:'line_multiple',index2:'phone_number',join:{table: 'vendor_additional_detail', column: 'vendor_id', primary: 'user_id', on_table: 'tenant_phone'},name_id:'vendor_id'},
        {name: 'Open Work Orders', index: 'phone_type', width: 220, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer',formatter:workOrderFormatter},
        {name: 'YTD Payment', index: 'phone_type', width: 220, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer',formatter:paymentFormatter},

        {name: 'vendor_id', hidden:true,title:true, index: 'vendor_id', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
        {name: 'vendor_status', hidden:true,title:true, index: 'status', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},

        {name: 'Type', index: 'vendor_type', width: 220, align: "left", searchoptions: {sopt: conditions}, table: 'company_vendor_type', classes: 'pointer'},
        //{name: 'Rate', index: 'vendor_rate', width: 80, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:rateFormatter},
       /* {name: 'Rating', index: 'rating', width: 170, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:starFormatter},*/
        {name: 'Email', index: 'email', width: 200, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer'},
        //{name: 'Status', index: 'status', width: 80, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',attr:[{name:'vendor_id',value:'vendor_id'},{name:'rating',value:'rating'},{name:'flag',value:'vendor',optional_id:'vendor_id'}],formatter: propertyStatus},
        // {name: 'Action', index: 'select', title: false, width: 80, align: "right",formatter: 'select', sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, edittype: 'select', search: false, table: table,formatter:actionFmatterss}
    ];
    var ignore_array = [];
    jQuery("#vendorListingTable").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: sortColumn,
        sortorder: sortOrder,
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Vendors",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 200, drag: true, resize: false} // search options
    );
}

function vendorName(cellValue, options, rowObject) {
    if(rowObject !== undefined) {
        var flagValue = $(rowObject.Action).attr('flag');
        var id = $(rowObject.Action).attr('data_id');
        //  console.log(id);
        // console.log(rowObject);
        var flag = '';
        if (flagValue == 'yes') {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id='+id+'" ><img src="/company/images/Flag.png"></a></span>';
        } else {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }
}




function starFormatter(cellValue, options, rowObject){
    var html = '';
    switch (cellValue) {
        case '1':
            html = '<form><fieldset class="starability-growRotate"><input type="radio" name="rating" value="1" /><label  title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label class="faded-star" title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label class="faded-star" title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label class="faded-star" title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label class="faded-star" title="Amazing">5 star</label> </fieldset> </form>';
            break;
        case '2':
            html = '<form><fieldset class="starability-growRotate"><input type="radio" name="rating" value="1" /><label  title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label class="faded-star" title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label class="faded-star" title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label class="faded-star" title="Amazing">5 star</label> </fieldset> </form>';
            break;
        case '3':
            html = '<form><fieldset class="starability-growRotate"><input type="radio" name="rating" value="1" /><label  title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label class="faded-star" title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label class="faded-star" title="Amazing">5 star</label> </fieldset> </form>';
            break;
        case '4':
            html = '<form><fieldset class="starability-growRotate"><input type="radio" name="rating" value="1" /><label  title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label class="faded-star" title="Amazing">5 star</label> </fieldset> </form>';
            break;
        case '5':
            html = '<form><fieldset class="starability-growRotate"><input type="radio" name="rating" value="1" /><label  title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label title="Amazing">5 star</label> </fieldset> </form>';
            break;
        default:
            html = '<form><fieldset class="starability-growRotate"><input type="radio" name="rating" value="1" /><label class="faded-star" title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label class="faded-star" title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label class="faded-star" title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label class="faded-star" title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label class="faded-star" title="Amazing">5 star</label> </fieldset> </form>';
    }
    return html;
}

/**
 * jqGrid function to format status
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */

function flagFormatter(cellValue, options, rowObject){
    if(rowObject !== undefined){
        console.log(rowObject.Action);
        var flagValue = $(rowObject.Action).attr('flag');
        var flag = '';
        if(flagValue == 'yes'){

        } else {

        }
    }
}



function workOrderFormatter(cellValue, options, rowObject){
    return '0';
}

function paymentFormatter(cellValue, options, rowObject){
    return '0.00';
}

/**
 * jqGrid function to format is_default column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function isDefaultFormatter(cellValue, options, rowObject) {
    if (cellValue == '1')
        return "Yes";
    else if (cellValue == '0')
        return "No";
    else
        return '';
}
/**  User Listing **/

jqGridUsers(1);
/**
 * jqGrid Initialization function
 * @param status
 */
function jqGridUsers(status) {

    if(jqgridNewOrUpdated == 'true')
    {
        var sortOrder = 'desc';
        var sortColumn = 'users.updated_at';
    } else {
        var sortOrder = 'asc';
        var sortColumn = 'users.name';
    }
    var table = 'users';
    var columns = ['Name', 'Email','Phone', 'Created Date', 'Status'];
    var extra_dropdown = [];
    var select_column = ['Edit','status','Delete'];
    var ignore_array = [{column:'users.id',value:'1'}];
    var joins = [{table:'users',column:'role',primary:'id',on_table:'company_user_roles'}];
    var conditions = ["eq","bw","ew","cn","in"];

    // if(selectedRoles){
    //     var extra_where = [{column:'user_type',value:1,condition:'='},{column:'role', value : selectedRoles,condition:'IN'}];
    // } else {
    var extra_where = [{column:'user_type',value:1,condition:'='}];
    // }
    var extra_columns = ['users.user_type', 'users.updated_at'];
    var columns_options = [
        {name:'Name',index:'name', width:280,align:"center",class:'pointer',searchoptions: {sopt: conditions},table:table,formatter: ContactName,change_type:'username'},
        {name:'Email',index:'email', width:280,align:"center",class:'pointer',searchoptions: {sopt: conditions},table:table},
        {name:'Phone',index:'mobile_number', width:280,class:'pointer',align:"center",searchoptions: {sopt: conditions},table:table},
        //  {name:'Role',index:'role_name', width:200, align:"center",searchoptions: {sopt: conditions},table:'company_user_roles'},
        {name:'Created At',index:'created_at', width:240,class:'pointer',align:"right",searchoptions: {sopt: conditions},table:table},
        {name:'Status',index:'status', width:230,align:"center",class:'pointer',searchoptions: {sopt: conditions},table:table,formatter:userStatusFormatter },
        // {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table}
    ];
    jQuery("#userListings").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            extra_dropdown:extra_dropdown,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'true',
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: sortColumn,
        sortorder: sortOrder,
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Manage User",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {top:10,left:200,drag:true,resize:false}
    );
}



/**  User Listing **/

jqGridOtherUsers(1);
/**
 * jqGrid Initialization function
 * @param status
 */
function jqGridOtherUsers(status) {

    if(jqgridNewOrUpdated == 'true')
    {
        var sortOrder = 'desc';
        var sortColumn = 'users.updated_at';
    } else {
        var sortOrder = 'asc';
        var sortColumn = 'users.name';
    }
    var table = 'users';
    var columns = ['Name', 'Email','Phone', 'Created Date', 'Type'];
    var extra_dropdown = [];
    var select_column = ['Edit','status','Delete'];
    var ignore_array = [{column:'users.id',value:'1'}];
    var joins = [{table:'users',column:'role',primary:'id',on_table:'company_user_roles'}];
    var conditions = ["eq","bw","ew","cn","in"];

    // if(selectedRoles){
    //     var extra_where = [{column:'user_type',value:1,condition:'='},{column:'role', value : selectedRoles,condition:'IN'}];
    // } else {
    var extra_where = [{column:'user_type',value:'("5","6","7")',condition:'IN'}];
    // }
    var extra_columns = ['users.user_type', 'users.updated_at'];
    var columns_options = [
        {name:'Name',index:'name', width:280,align:"center",class:'pointer',searchoptions: {sopt: conditions},table:table,formatter: ContactName,change_type:'username'},
        {name:'Email',index:'email', width:280,align:"center",class:'pointer',searchoptions: {sopt: conditions},table:table},
        {name:'Phone',index:'mobile_number', width:280,class:'pointer',align:"center",searchoptions: {sopt: conditions},table:table},
        //  {name:'Role',index:'role_name', width:200, align:"center",searchoptions: {sopt: conditions},table:'company_user_roles'},
        {name:'Created At',index:'created_at', width:240,class:'pointer',align:"right",searchoptions: {sopt: conditions},table:table},
        {name:'UserType',index:'user_type', width:230,align:"center",class:'pointer',searchoptions: {sopt: conditions},table:table,formatter:otherUserTypeFormatter },
        // {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table}
    ];
    jQuery("#otheruserListings").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            extra_dropdown:extra_dropdown,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'true',
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: sortColumn,
        sortorder: sortOrder,
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Manage User",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {top:10,left:200,drag:true,resize:false}
    );
}

/**
 * jqGrid function to format status
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function userStatusFormatter (cellValue, options, rowObject){
    if (cellValue == 1)
        return "Active";
    else if(cellValue == '0')
        return "InActive";
    else
        return '';
}

/**
 * jqGrid function to format status
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function otherUserTypeFormatter (cellValue, options, rowObject){
    if (cellValue == 5)
        return "Contact";
    else if(cellValue == '6')
        return "Prospect";
    else if(cellValue == '7')
        return "Applicant";
    else
        return '';
}

$(document).on('click','#userListings tr td:not(:last-child)',function(e){
    e.preventDefault();
    var base_url = window.location.origin;
    var id = $(this).closest("tr").attr('id');
    window.location.href = base_url + '/User/ManageUser?id='+id;
});
function ContactName(cellValue, options, rowObject) {
    if(rowObject !== undefined) {
        var flagValue = $(rowObject.Action).attr('flag');
        var id = $(rowObject.Action).attr('data_id');
        var flag = '';
        if (flagValue == 'yes') {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id='+id+'" ><img src="/company/images/Flag.png"></a></span>';
        } else {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }

}
$(document).on("click","#tenant_link,#owner_link,#vendor_link,#user_link,#other_link",function(){
    $("#contact_grid").hide();
});
$(document).on('click','.background_check_open',function () {
    if( $(this).is(':checked') )
    {
        $(this).prop('checked', false);
        // $('#backGroundCheckPop').modal('hide');
        // $('#backgroundReport').modal('show');
        window.open('https://www.victig.com', '_blank');
    }
});

$(document).on('click', '#vendorListingTable tr td:not(td:last-child)', function () {

    var id = $(this).closest('tr').find('td:eq(4)').text();
    var status = $(this).closest('tr').find('td:eq(5)').text();

    if (status == 0 || status == 9 || status == '3' || status == '2') {
        return false;
    } else {
        window.location.href = '/Vendor/ViewVendor?id='+id;
    }

});







