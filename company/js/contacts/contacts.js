
$(document).on('click','.contact_cancel',function(){
    bootbox.confirm("Do you want to cancel this action now ?", function (result) {
        if (result == true) {
            window.location.href = window.location.origin+'/People/ContactListt';
        }
    });
});
$("#people_top").addClass("active");
setTimeout(function(e){
    $('#additional_country option[value=220]').attr('selected', 'selected');
},500)
setTimeout( function(){
    $(".emergency_additional_countryCode").val(220);
},2000);
getCompanyDefaultData();
/*validation for add employee

 */

/*$(document).on('change','.additional_phoneType ',function(){
    var phone_type = $(this).val();
    if(phone_type == '2' || phone_type == '5')
    {
        $('#work_phone_extension-error').text('');
        $(this).closest('div').next().show();
        $(this).closest('div').next().find('#work_phone_extension').removeAttr("disabled");
    } else {
        $(this).closest('div').next().hide();
        $(this).closest('div').next().find('#work_phone_extension').prop('disabled', true);
    }
});*/

$(document).on('change','.additional_phoneType',function(){
    var phone_type = $(this).val();

    if(phone_type == '2' || phone_type == '5')
    {
        $('#work_phone_extension-error').text('');
        console.log( $(this).closest('div'));
        $(this).parent().next('div').next('div').show();
    } else {
        $(this).parent().next('div').next('div').hide();
    }
});



$("#addEmployee").validate({
    rules: {
        firstname: {
            required:true
        },
        lastname: {
            required:true
        },
        parking_keys: {
            number:true
        },
        'phoneNumber[]': {
            required: true,
        },
        referralSource: {
            required:true
        },
        additional_firstname: {
            required:true
        }
    }
});

/*submit halder for add employee

 */
$("#addEmployee").on('submit',function(event){
        event.preventDefault();
    $(".customadditionalEmailValidation").each(function () {
        res = validations(this);
    });
    $(".customValidationCarrier").each(function () {
        res = validations(this);
    });

    $(".customPhonenumbervalidations").each(function () {
        res = validations(this);
    });

    $(".customOtherPhonevalidations").each(function () {
        res = validations(this);
    });


    if($("#addEmployee").valid()){
        var employee_image = $('.employee_image').html();
        var employeeImage = JSON.stringify(employee_image);
        var form = $('#addEmployee')[0];
        var formData = new FormData(form);
        formData.append('action','insert');
        formData.append('class','EmployeeAjax');
        formData.append('employee_image',employeeImage);
        action = 'insert';
        var custom_field = [];
        $(".custom_field_html input").each(function(){
            var data = {'name':$(this).attr('name'),'value':$(this).val(),'id':$(this).attr('data_id'),'is_required':$(this).attr('data_required'),'data_type':$(this).attr('data_type'),'default_value':$(this).attr('data_value')};
            custom_field.push(data);
        });

        var length1 = $('#file_library_uploads > div').length;
        combine_photo_document_array =  file_library;
        var dataaa = Array.from(new Set(combine_photo_document_array)); // #=> ["foo", "bar"]

        if(length1 > 0 ) {
            var data1 = convertSerializeDatatoArray();
            var combine__data_photo_document_array =   data1;
            $.each(dataaa, function (key, value) {
                if(compareArray(value,combine__data_photo_document_array) == 'true'){
                    formData.append(key, value);
                }
            });
        }
        var a =  JSON.stringify(custom_field);
        formData.append('custom_field',a);
        $.ajax({
            url:'/ContactAjax',
            type: 'POST',
            data: formData,cache: false,
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                var emailRes = true;
                var customRes = true;
                var carrierRes = true;
                var phoneRes = true;
                var otherPhone = true;
                // checking portfolio validations
                $(".customadditionalEmailValidation").each(function() {
                    emailRes = validations(this);

                });
                $(".customValidationCarrier").each(function() {
                    carrierRes = validations(this);

                });
                $(".customPhonenumbervalidations").each(function () {
                    phoneRes = validations(this);
                });
                // checking custom field validations
                $(".custom_field_html input").each(function () {
                    customRes = validateCustomField($(this).val(), $(this).attr('data_required'), $(this).attr('data_type'), this, this.id);
                });

                $(".customOtherPhonevalidations input").each(function () {
                    otherPhone =  validations(this);
                });

                if (emailRes === false || customRes === false || carrierRes === false || phoneRes === false || otherPhone === false) {
                    xhr.abort();
                    return false;
                }

            },
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == "success" && response.table === "all"){
                    localStorage.setItem("Message", "Contact Added Successfully");
                    localStorage.setItem("contactrowcolor",true)
                    window.location.href = window.location.origin+'/People/ContactListt';
                }else{
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
    }
});



/* Getting Building Property And Unit*/

$(document).on("change","#building",function (e) {
    e.preventDefault();
    getUnitsByBuildingsID($("#property").val() , $(this).val());
    return false;
});

$(document).on("change","#property",function (e) {
    e.preventDefault();
    getBuildingsByPropertyID($(this).val());
    return false;
});





/*custom validation for email

 */
$(document).on('keyup','.customadditionalEmailValidation',function(){
    validations(this);
});

/*custom validation for carrier
 */
$(document).on('change','.customValidationCarrier',function(){
    validations(this);
});

/*custom validation for additional phone
 */
$(document).on('keyup','.customPhonenumbervalidations',function(){
    validations(this);
});

/* jquery for phone number format

 */
jQuery('.phone_format').mask('000-000-0000', {reverse: true});
/*For Cloning vehicle*/
$(document).on("click",".copyVehicle",function(){
    var clone = $(".property_vehicle:first").clone();
    clone.find('input[type=text]').val('');
    $(".property_vehicle").last().after(clone);
    $(".property_vehicle:not(:eq(0))  .removeVehicle").show();
    $(".property_vehicle:not(:eq(0))  .copyVehicle").remove();
    clone.find('.vehicle_date_purchased').each(function () {
        $(this).removeAttr('id').removeClass('hasDatepicker'); // added the removeClass part.
        $('.vehicle_date_purchased').datepicker({dateFormat: date_format,changeYear: true,
            yearRange: "-100:+20",
            changeMonth: true,});
    });
});

/*Jquery for remove vehicle

 */
$(document).on("click",".removeVehicle",function(){
    var phoneRowLenght = $(".property_vehicle");
    $(this).parents(".property_vehicle").remove();
});


/*Jquery for gender gender according to salutation

 */
$(document).on("change","#salutation",function(){
    var value = $(this).val();
    if(value == 1){
        $('#gender').prop('selectedIndex',0);
        $('.hidemaiden').hide();
    }
    if(value==2)
    {
        $("#gender").val("1");
        $('.hidemaiden').hide();
    }
    if(value==3)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    } if(value==4)
    {
        $('#gender').prop('selectedIndex',0);
        $('.hidemaiden').hide();
    } if(value==5)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    } if(value==6)
    {
        $("#gender").val("1");
        $('.hidemaiden').hide();
    } if(value==7)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    }
    if(value==8)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    } if(value==9)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    }if(value==11)
    {
        $("#gender").val("1");
        $('.hidemaiden').hide();
    }if(value==10)
    {
        $("#gender").val("1");
        $('.hidemaiden').hide();
    }


});

/*for multiple SSN textbox*/
$(document).on("click",".ssn-plus-sign",function(){
    var clone = $(".multipleSsn:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    $(".multipleSsn").last().after(clone);
    $(".multipleSsn:not(:eq(0))  .ssn-plus-sign").remove();
    $(".multipleSsn:not(:eq(0))  .ssn-remove-sign").show();

});
/* for multiple SSN textbox*/

/*remove ssn textbox*/
$(document).on("click",".ssn-remove-sign",function(){
    $(this).parents(".multipleSsn").remove();
});
/*remove ssn textbox*/


$(document).on("click",".additional_ssn-plus-sign",function(){
    var clone = $(".additional_multipleSsn:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    $(".additional_multipleSsn").first().after(clone);

    clone.find(".additional_ssn-plus-sign").remove();
    $(".additional_multipleSsn:not(:eq(0))  .additional_ssn-remove-sign").show();

});

$(document).on("click",".additional_ssn-remove-sign",function(){
    $(this).parents(".additional_multipleSsn").remove();
});

/*To show ethnicity popup

 */
$(document).on("click",'#additional_ethnicity',function(){
    $("#additional_ethnicityPopUp").show();
});


/*To hide ethicity popoup

 */
$(document).on("click",'#additional_ethncity_cancel',function(){
    $("#additional_ethnicityPopUp").hide();
    $("#additional_addThncity").val('');
});


/*To add datepicker for date of birth

 */

$("#date_of_birth").datepicker({dateFormat: date_format,
    changeYear: true,
    yearRange: "-100:+20",
    changeMonth: true,
    maxDate: new Date(),
});

/*
For zipcode functionality

 */
$(document).on('focusout','#zip_code',function(){
    getAddressInfoByZip($(this).val());
});

/*
For zipcode functionality

 */
$("#zipcode").keydown(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        getAddressInfoByZip($(this).val());
    }
});

/**
 *Get location on the basis of zipcode
 */
function getAddressInfoByZip(zip){
    if(zip.length >= 5 && typeof google != 'undefined'){
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                         console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    setTimeout(function(){
                        response(addr);
                    }, 2000);

                } else {
                    response({success:false});
                }
            } else {
                response({success:false});
            }
        });
    } else {
        response({success:false});
    }
}

/**
 * Set values in the city, state and country when focus loose from zipcode field.
 */
function response(obj){
    if(obj.success){
        $('#country').val(obj.country);
        $('#citys').val(obj.city);
        $('#states').val(obj.state);

    } else {
        $('#citys').val('');
        $('#states').val('');
        $('#country').val('');
    }
}




/*
To show additional marital status popup

 */

$(document).on("click",'#additional_maritalStatus',function(){
    $("#additional_maritalStatusPopUp").show();

});
/*
To cancel additional marital status

 */
$(document).on("click",'#additional_maritalStatus_cancel',function(){
    $("#additional_maritalStatusPopUp").hide();
    $("#additional_addMaritalStatus").val('');
});


/*
To show additional hobbies popup

 */
$(document).on("click",'#additional_hobbies',function(){
    $("#additional_hobbiesPopUp").show();
});

/*
To show additional  hobbies cancel popup

 */
$(document).on("click",'#additional_hobbies_cancel',function(){
    $("#additional_hobbiesPopUp").hide();
    $("#additional_addNewHobbies").val('');
});

/*
To show additional veteran status popup
 */
$(document).on("click",'#additional_veteranStatus',function(){
    $("#additional_veteranStatusPopUp").show();
});

/*
To show additional veteran status popup cancel
 */
$(document).on("click",'#additional_veteranStatus_cancel',function(){
    $("#additional_veteranStatusPopUp").hide();
    $("#additional_addNewVeteranStatus").val('');
});


/*
To show additional phone row
*/

$(document).on("click",".additional-phonerow-plus-sign",function(){
    var clone = $(".additional_phone-row:first").clone();
    clone.find('input[type=text]').val('');
    $(".additional_phone-row").first().after(clone);
    $(".additional_phone-row:not(:eq(0))  .fa-plus-circle").hide();
    $(".additional_phone-row:not(:eq(0))  .additional-phonerow-remove-sign .fa-times-circle").show();
    var phoneRowLenght = $("#addTenant .additional_phone-row");
    clone.find('.phone_format').mask('000-000-0000', {reverse: true});
    if (phoneRowLenght.length == 2) {
        clone.find(".fa-plus-circle").hide();
    }else if (phoneRowLenght.length == 3) {
        clone.find(".fa-plus-circle").hide();
        $(".additional_phone-row:eq(0) .fa-plus-circle").hide();
    }else{
        $(".additional_phone-row:not(:eq(0)) .fa-plus-circle").show();
    }
});


/*
To remove phone row
*/

$(document).on("click",".additional-phonerow-remove-sign",function(){
    var phoneRowLenght = $(".additional_phone-row");
    if (phoneRowLenght.length == 2) {
        $(".additional_phone-row:eq(0) .fa-plus-circle").show();
    }else if (phoneRowLenght.length == 3) {
        $(".additional_phone-row:eq(0) .fa-plus-circle").hide();
    }else{
        $(".additional_phone-row:not(:eq(0)) .fa-plus-circle").show();
    }
    $(this).parents(".additional_phone-row").remove();

});


/*
To clone employee notes
*/
$(document).on("click",".add-employee-notes",function(){
    var clone = $(".employee_notes:first").clone();
    clone.find('textarea').val('');
    $(".employee_notes").last().after(clone);
    $(".employee_notes:not(:eq(0))  .fa-plus-circle").hide();
    $(".employee_notes:not(:eq(0))  .remove-employee-notes").show();
    var employeeNotesLenght = $("#addEmployee .employee_notes");
    if (employeeNotesLenght.length == 2) {
        clone.find(".fa-plus-circle").hide();
    }else if (employeeNotesLenght.length == 3) {
        clone.find(".fa-plus-circle").hide();
        $(".employee_notes:eq(0) .fa-plus-circle").hide();
    }else{
        $(".employee_notes:not(:eq(0)) .fa-plus-circle").show();
    }
});



/*
To remove employee notes
*/
$(document).on("click",".remove-employee-notes",function(){
    var employeeNotesLenght = $(".employee_notes");
    if (employeeNotesLenght.length == 2) {
        $(".employee_notes:eq(0) .fa-plus-circle").show();
    }else if (employeeNotesLenght.length == 3) {
        $(".employee_notes:eq(0) .fa-plus-circle").show();
    }else{
        $(".employee_notes:not(:eq(0)) .fa-plus-circle").hide();
    }
    $(this).parents(".employee_notes").remove();

});



/*
To clone email
*/
$(document).on("click",".additional_email-plus-sign",function(){
    var emailRowLenght = $(".additional_multipleEmail");

    if (emailRowLenght.length == 2) {
        $(".additional_email-plus-sign").hide();
    }else{
        $(".additional_email-plus-sign").show();
    }
    var clone = $(".additional_multipleEmail:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    $(".additional_multipleEmail").last().after(clone);
    $(".additional_multipleEmail:not(:eq(0))  .additional_email-plus-sign").remove();
    $(".additional_multipleEmail:not(:eq(0))  .additional_email-remove-sign").show();

});



/*
To remove email
*/

$(document).on("click",".additional_email-remove-sign",function(){
    var emailRowLenght = $(".additional_multipleEmail");

    if (emailRowLenght.length == 3 || emailRowLenght.length == 2) {
        $(".additional_email-plus-sign").show();
    }else{
        $(".additional_email-plus-sign").hide();
    }
    $(this).parents(".additional_multipleEmail").remove();
});




/*phone row form1 remove*/
$(document).on("click",".guarantor-phonerow-form1-remove-sign",function(){

    $(this).parents(".guarantor-form1-phone-row").remove();

});



$(document).on("click",".multipleEmail-form1 .email-form1-plus-sign .fa-plus-circle",function(){
    var emailRowLenght = $(".multipleEmail-form1");
    if (emailRowLenght.length == 3) {
        $(".email-form1-plus-sign").hide();
    }
    var clone = $(".multipleEmail-form1:first").clone();
    clone.find('input[type=text]').val('');
    $(".multipleEmail-form1").first().after(clone);
    $(".multipleEmail-form1:not(:eq(0))  .email-form1-plus-sign .fa-plus-circle").remove();
    $(".multipleEmail-form1:not(:eq(0))  .email-form1-remove-sign .fa-times-circle").show();

});

$(document).on("click",".email-form1-remove-sign",function(){
    var emailRowLenght = $(".multipleEmail-form1");
    if (emailRowLenght.length == 3) {
        $(".email-form1-plus-sign").show();
    }
    $(this).parents(".multipleEmail-form1").remove();
});


$(document).on("click",".email-form2-plus-sign",function(){

    var clone = $(".multipleEmail-form2:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    $(this).parents(".multipleEmail-form2").after(clone);
    clone.find(".email-form2-plus-sign").hide();
    clone.find(".email-form2-remove-sign").show();
});


$(document).on("click",".email-form2-remove-sign",function(){
    $(this).parents(".multipleEmail-form2").remove();
});


$(document).on("click",".closeimagepopupicon",function () {
    $(".popup-bg").hide();
    $(this).parent().hide();
});


/*
To cropit image change
 */
$(document).on("change",".cropit-image-input",function(){
    photo_videos = [];
    var fileData = this.files;
    var type= fileData.type;
    var elem = $(this);
    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
        var uploadType = 'image';
        var validSize = '1030';
        var arrayType = validImageTypes;

        if ($.inArray(type, arrayType) < 0) {
            toastr.warning('Please select file with valid extension only!');
        } else {

            if (parseInt(size) > validSize) {
                var validMb =  1;
                toastr.warning('Please select documents less than '+validMb+' mb!');
            } else {
                size = isa_convert_bytes_to_specified(value['size'], 'k') + 'kb';
                photo_videos.push(value);
                var src = '';
                var reader = new FileReader();
                $('#photo_video_uploads').html('');


                $(".popup-bg").show();
                elem.next().show();

            }

        }

    });

});

$(document).on("click",".primary-tenant-phone-row .fa-plus-circle",function(){
    var clone = $(".primary-tenant-phone-row:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    $(".primary-tenant-phone-row").first().after(clone);
    clone.find('.work_extension_div').hide();
    clone.addClass('clone-row')
    clone.find('.additional_phoneType ').val('1');
    $(".primary-tenant-phone-row:not(:eq(0))  .fa-plus-circle").hide();
    $(".primary-tenant-phone-row:not(:eq(0))  .fa-times-circle").show();
    var phoneRowLenght = $("#addEmployee .primary-tenant-phone-row");
    clone.find('.phone_format').mask('000-000-0000', {reverse: true});
    if (phoneRowLenght.length == 2) {
        clone.find(".fa-plus-circle").hide();
    }else if (phoneRowLenght.length == 3) {
        clone.find(".fa-plus-circle").hide();
        $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").hide();
    }else{
        $(".primary-tenant-phone-row:not(:eq(0)) .fa-plus-circle").show();
    }
});

$(document).on("click",".primary-tenant-phone-row .fa-times-circle",function(){
    var phoneRowLenght = $(".primary-tenant-phone-row");
    if (phoneRowLenght.length == 2) {
        $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").show();
    }else if (phoneRowLenght.length == 3) {
        $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").show();
        // $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").hide();
    }else{
        $(".primary-tenant-phone-row:not(:eq(0)) .fa-plus-circle").show();
    }

    $(this).parents(".primary-tenant-phone-row").remove();
});


/*notice period section copy*/
$(document).on("click",".add-notice-period",function(){
    var clone = $(".tenant-credentials-control:first").clone();
    clone.find('input[type=text]').val('');
    clone.find('select[name="credentialType[]"]').val('1');
    $(".tenant-credentials-control").last().after(clone);
    var length = $(".tenant-credentials-control").length;
    clone.find('#acquireDateid')
        .removeClass('hasDatepicker')
        .removeData('datepicker')
        .removeAttr('id')
        .attr('id','acquireDateid_'+length)
        .datepicker({yearRange: '+0:+100', changeMonth: true, changeYear: true, dateFormat: date_format})
        .datepicker("setDate", new Date());
    clone.find('#expirationDateid')
        .removeClass('hasDatepicker')
        .removeData('datepicker')
        .removeAttr('id')
        .attr('id', 'expirationDateid_' + length)
        .datepicker({yearRange: '+0:+100', changeMonth: true, changeYear: true, dateFormat: date_format})
        .datepicker("setDate", new Date());

    clone.find(".add-notice-period").hide();
    clone.find(".remove-notice-period").show();
});

$(".acquireDateclass").datepicker({dateFormat: date_format,
    changeYear: true,
    yearRange: "-100:+20",
    changeMonth: true,
   }).datepicker("setDate", new Date());

$(".expirationDateclass").datepicker({dateFormat: date_format,
    changeYear: true,
    yearRange: "-100:+20",
    changeMonth: true}).datepicker("setDate", new Date());

$(document).on("change", ".acquireDateclass", function () {
    var dynclass= $(this).attr('id');
    var dynmid = $("#"+dynclass).closest('div').next().find('.expirationDateclass').attr('id');
    var valuedatepicker=$(this).val();
    $("#"+dynmid).datepicker({dateFormat: date_format,
        minDate: valuedatepicker,
    });
});
$(document).on("change", ".expirationDateclass", function () {
    var dynclass = $(this).attr('id');
    var dynmid = $("#" + dynclass).closest('div').prev().find('.acquireDateclass').attr('id');
    var valuedatepicker = $(this).val();
    $("#"+dynmid).datepicker({dateFormat: date_format,
        maxDate: valuedatepicker,
    });
});


$(document).on("click",".remove-notice-period",function(){
    $(this).parents(".tenant-credentials-control").remove();
});

/*
date picker for veichle purchase date
 */
$('.vehicle_date_purchased').datepicker({dateFormat: date_format,
    changeYear: true,
    yearRange: "-100:+20",
    changeMonth: true,
});



function isa_convert_bytes_to_specified(bytes, to) {
    var formulas =[];
    formulas['k']= (bytes / 1024).toFixed(1);
    formulas['M']= (bytes / 1048576).toFixed(1);
    formulas['G']= (bytes / 1073741824).toFixed(1);
    return formulas[to];
}




$(document).ready(function(){

    var redirect=$(".id_from_other").val();

    if(redirect!="")
    {
        getRedirectionData(redirect);
    }
//     var cropper = $('.image-editor').cropit({
//         imageState: {
//             src: subdomain_url+'500/400',
//         },
//     });
//     // Handle rotation
//     document.getElementById('rotate-ccw').addEventListener('click', function() {
//         $('.image-editor').cropit('rotateCCW')
//     });
//
//     document.getElementById('rotate-cw').addEventListener('click', function() {
//         $('.image-editor').cropit('rotateCW')
//     });
//
//
//     $(document).on("click",'.export',function(){
//         $(this).parents('.cropItData').hide();
//         $(this).parent().prev().val('');
//         var dataVal = $(this).attr("data-val");
//         var imageData = $(this).parents('.image-editor').cropit('export');
//         var image = new Image();
//         image.src = imageData;
//         $(".popup-bg").hide();
//         $(this).parent().parent().parent().prev().find('div').html(image);
//     });
//
    $(document).on('click','#add_libraray_file',function(){
        $('#file_library').val('');
        $('#file_library').trigger('click');
    });

});


/*show and hide radio button content*/
$(document).on("click",'.select_property_vehicle',function(){

    if( $(this).is(":checked") ){
        var val = $(this).val();
        if(val=='1')
        {
            $('.property_vehicle').show();
        }
        else
        {
            $('.property_vehicle').hide();
        }
    }

});


$(document).on("click",".add-emergency-contant",function(){

    var clone = $(".tenant-emergency-contact:first").clone();
    clone.find('input[type=text]').val('');
    clone.find('input[name="emergency_phone[]"]').mask('000-000-0000', {reverse: true});;
    $(".tenant-emergency-contact").first().after(clone);
    clone.find(".add-emergency-contant").hide();
    clone.find(".remove-emergency-contant").show();

});

$(document).on("click",".remove-emergency-contant",function(){
    $(this).parents(".tenant-emergency-contact").remove();
});

$(document).on("click",".additional-add-emergency-contant",function(){

    var clone = $(".additional-tenant-emergency-contact:first").clone();
    clone.find('input[type=text]').val('');
    clone.find('.phone_format').mask('000-000-0000', {reverse: true});
    $(".additional-tenant-emergency-contact").last().after(clone);
    clone.find(".additional-add-emergency-contant").hide();
    clone.find(".additional-remove-emergency-contant").show();

});

$(document).on("click",".additional-remove-emergency-contant",function(){
    $(this).parents(".additional-tenant-emergency-contact").remove();
});

/*To reset form */

$(document).on("click",".employee_reset",function(){
    bootbox.confirm({
        message: "Do you want to clear this form  ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                window.location.href = window.location.origin+'/People/AddContact';
            }
        }
    });

});


/*
function to get default company data
 */
function getCompanyDefaultData() {
    $.ajax({
        type: 'post',
        url: '/get-company-default-data',
        data: {
            class: 'commonPopup',
            action: 'getCompanyDefaultData'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#citys').val(res.record.city);
            $('#states').val(res.record.state);
            $('#country').val(res.record.country);
            $('#zip_code').val(res.record.zip_code);
        },
    });

}

/*
go back function
 */
function goBack() {
    window.location.href='/People/GetEmployeeList';
    //window.history.back();
}





function getBuildingsByPropertyID(propertyID){
    $.ajax({
        type: 'post',
        url: '/ContactAjax',
        data: {
            class: "contactPopup",
            action: "getContactBuildingDetail",
            propertyID: propertyID
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                if (data.data.length > 0){
                    var buildingOption = "<option value='0'>Select</option>";
                    $.each(data.data, function (key, value) {
                        buildingOption += "<option value='"+value.id+"' data-id='"+value.building_id+"'>"+value.building_name+"</option>";
                    });
                    $('#building').html(buildingOption);
                }
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}


function getUnitsByBuildingsID(propertyID, buildingID){
    $.ajax({
        type: 'post',
        url: '/ContactAjax',
        data: {
            class: "contactPopup",
            action: "getContactUnitDetail",
            propertyID: propertyID,
            buildingID: buildingID
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                if (data.data.length > 0){
                    var unitOption = "<option value=''>Undecided</option>";

                    $.each(data.data, function (key, value) {
                        if(value.unit_prefix != '' && value.unit_prefix != undefined){
                         var  unitPrefixwithhipen = value.unit_prefix+"-";
                        } else{
                            var  unitPrefixwithhipen = '';
                        }
                        var uniName = unitPrefixwithhipen+value.unit_no;
                        unitOption += "<option value='"+value.id+"' data-id='"+value.unit_prefix+"'>"+uniName+"</option>";
                    });
                    $('#unit').html(unitOption);
                }
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

}
function getRedirectionData(id) {
    $.ajax({
        type: 'post',
        url: '/RentalApplication/Ajax',
        data: {
            class: "RenatlApplicationAjax",
            action: "getRedirectionData",
            id:id
        },
        success: function (response) {
            var response = JSON.parse(response);
            $("#property").val(response.data2.prop_id).trigger("change");
            setTimeout(function () {
                $("#building").val(response.data2.build_id).trigger("change");
            },500);
            setTimeout(function () {
                $("#unit").val(response.data2.unit_id);
            },600);

            $("#salutation").val(response.data.salutation);
            $("#firstname").val(response.data.first_name);
            $("#lastname").val(response.data.last_name);
            $("#maidenname").val(response.data.maiden_name);
            $("#nickname").val(response.data.nick_name);
            $("#middlename").val(response.data.mi);
            $("#phoneType12").val(response.data.phone_type);
            $("#carrierCheck").val(response.data.carrier);
            $("#countryCodeCheck").val(response.data.country);
            $("#phoneNumberCheck").val(response.data.phone_number);
            $("#emailCheck").val(response.data.email);
            $("#zip_code").val(response.data.zipcode);
            $("#country").val(response.data.country);
            $("#states").val(response.data.state);
            $("#citys").val(response.data.city);
            $("#address1").val(response.data.address1);
            $("#address2").val(response.data.address2);
            $("#address3").val(response.data.address3);
            $("#address4").val(response.data.address4);
            $("#referralSourceCheck").val(response.data.referral_source);
            var dateParts = response.data2.exp_move_in.split("-");
            var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
            var exp_move_in= $.datepicker.formatDate(jsDateFomat, jsDate);
            $("#moveInDate").val(exp_move_in);
            $("#ethncityCheck").val(response.data.ethnicity);
            $("#maritalStatusCheck").val(response.data.maritial_status);
            $("#veteranStatusCheck").val(response.data.veteran_status);
            $("#ssn").val(response.data.ssn_sin_id);
            $("#emergency").val(response.data3.emergency_contact_name);
            $("#relationship").val(response.data3.emergency_relation);
            $("#emergency_countryCheck").val(response.data3.emergency_country_code);
            $("#phoneNumber").val(response.data3.emergency_phone_number);
            $("#email1").val(response.data3.emergency_email);



        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });



}


