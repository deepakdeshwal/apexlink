//property combo grid
// $(document).on('click','.textbox-text',function(){
//    // console.log('sdfdsf');
//     $(this).parents('combo_div').find('input').combogrid('showPanel');
// });
$(document).on('click','.textbox-text',function(){
    if($('#property').val() !== undefined) {
        $('#property').combogrid('showPanel');
    }

    if($('#tenant').val() !== undefined) {
        $('#tenant').combogrid('showPanel');
    }

    if($('#owner').val() !== undefined) {
        $('#owner').combogrid('showPanel');
    }
    if($('#vendor').val() !== undefined) {
        $('#vendor').combogrid('showPanel');
    }
    if($('#contact').val() !== undefined) {
        //intiate contact combogrid
        $('#contact').combogrid('showPanel');
    }
    if($('#employee').val() !== undefined) {
        //intiate employee combogrid
        $('#employee').combogrid('showPanel');
    }
});


function initateComboGrid() {
    //instantiate property combo grid
   // console.log($('#owner').val());
    if($('#property').val() !== undefined) {
        $('#property').combogrid({
            placeholder: "Select...",
            panelWidth: 310,
            url: '/combo-grid-global-ajax',
            idField: 'name',
            textField: 'name',
            mode: 'remote',
            id: 'fsdgsdg',
            fitColumns: true,
            queryParams: {
                action: 'getPropertiesdata',
                class: 'GlobalComboGrid',
            },
            columns: [[
                {field: 'id', title: 'Id', width: 8},
                {field: 'name', title: 'Property', width: 40}
            ]],
            onSelect: function (index, row) {
                $('#object_id').val(row.id);
                initateBuildingCombo(row.id);
            }
        });
    }

    if($('#tenant').val() !== undefined) {
        //intiate tenant combogrid
        $('#tenant').combogrid({
            placeholder: "Select...",
            panelWidth: 310,
            url: '/combo-grid-global-ajax',
            idField: 'name',
            textField: 'name',
            mode: 'remote',
            fitColumns: true,
            queryParams: {
                action: 'getUserdata',
                class: 'GlobalComboGrid',
                user_type: '2'
            },
            columns: [[
                {field: 'id', title: 'Id', width: 8},
                {field: 'name', title: 'Name', width: 40}
            ]],
            onSelect: function (index, row) {
                $('#object_id').val(row.id);
            }
        });
    }

    if($('#owner').val() !== undefined) {
        //intiate owner combogrid
        $('#owner').combogrid({
            placeholder: "Select...",
            panelWidth: 310,
            url: '/combo-grid-global-ajax',
            idField: 'name',
            textField: 'name',
            mode: 'remote',
            fitColumns: true,
            queryParams: {
                action: 'getUserdata',
                class: 'GlobalComboGrid',
                user_type: '4'
            },
            columns: [[
                {field: 'id', title: 'Id', width: 8},
                {field: 'name', title: 'Name', width: 40}
            ]],
            onSelect: function (index, row) {
                $('#object_id').val(row.id);
            }
        });
    }
    if($('#vendor').val() !== undefined) {
        //intiate vendor combogrid
        $('#vendor').combogrid({
            placeholder: "Select...",
            panelWidth: 310,
            url: '/combo-grid-global-ajax',
            idField: 'name',
            textField: 'name',
            mode: 'remote',
            fitColumns: true,
            queryParams: {
                action: 'getUserdata',
                class: 'GlobalComboGrid',
                user_type: '3'
            },
            columns: [[
                {field: 'id', title: 'Id', width: 8},
                {field: 'name', title: 'Name', width: 40}
            ]],
            onSelect: function (index, row) {
                $('#object_id').val(row.id);
            }
        });
    }
    if($('#contact').val() !== undefined) {
        //intiate contact combogrid
        $('#contact').combogrid({
            placeholder: "Select...",
            panelWidth: 310,
            url: '/combo-grid-global-ajax',
            idField: 'name',
            textField: 'name',
            mode: 'remote',
            fitColumns: true,
            queryParams: {
                action: 'getUserdata',
                class: 'GlobalComboGrid',
                user_type: '5'
            },
            columns: [[
                {field: 'id', title: 'Id', width: 8},
                {field: 'name', title: 'Name', width: 40}
            ]],
            onSelect: function (index, row) {
                $('#object_id').val(row.id);
            }
        });
    }
    if($('#employee').val() !== undefined) {
        //intiate employee combogrid
        $('#employee').combogrid({
            placeholder: "Select...",
            panelWidth: 310,
            url: '/combo-grid-global-ajax',
            idField: 'name',
            textField: 'name',
            mode: 'remote',
            fitColumns: true,
            queryParams: {
                action: 'getUserdata',
                class: 'GlobalComboGrid',
                user_type: '8'
            },
            columns: [[
                {field: 'id', title: 'Id', width: 8},
                {field: 'name', title: 'Name', width: 40}
            ]],
            onSelect: function (index, row) {
                $('#object_id').val(row.id);
            }
        });
    }
}

function initateBuildingCombo(id){
    if($('#building_combo_input').val() !== undefined) {
        $('#building_combo_input').combogrid({
            placeholder: "Select...",
            panelWidth: 310,
            url: '/combo-grid-global-ajax',
            idField: 'name',
            textField: 'name',
            mode: 'remote',
            fitColumns: true,
            queryParams: {
                action: 'getBuildingdata',
                class: 'GlobalComboGrid',
                property_id: id
            },
            columns: [[
                {field: 'id', title: 'Id', width: 8},
                {field: 'name', title: 'Building Name', width: 40}
            ]],
            onSelect: function (index, row) {
                $('#object_id').val(row.id);
                initateUnitCombo(row.id);
            }
        });
    }
}

function initateUnitCombo(id){
    if($('#unit_combo_input').val() !== undefined) {
        $('#unit_combo_input').combogrid({
            placeholder: "Select...",
            panelWidth: 310,
            url: '/combo-grid-global-ajax',
            idField: 'name',
            textField: 'name',
            mode: 'remote',
            fitColumns: true,
            queryParams: {
                action: 'getUnitdata',
                class: 'GlobalComboGrid',
                building_id: id
            },
            columns: [[
                {field: 'id', title: 'Id', width: 8},
                {field: 'name', title: 'Building Name', width: 40}
            ]],
            onSelect: function (index, row) {
                $('#object_id').val(row.id);
            }
        });
    }
}



