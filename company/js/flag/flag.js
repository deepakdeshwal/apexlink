// $("#portfolio_id").keypress(function(e){
//     var keyCode = e.which;
//     /*
//       8 - (backspace)
//       32 - (space)
//       48-57 - (0-9)Numbers
//     */
//     // Not allow special
//     if ( !( (keyCode >= 48 && keyCode <= 57)
//         ||(keyCode >= 65 && keyCode <= 90)
//         || (keyCode >= 97 && keyCode <= 122) )
//         && keyCode != 8 && keyCode != 32) {
//         e.preventDefault();
//     }
// });

//display add portfolio div
function getParameterByName( name ){
    var regexS = "[\\?&]"+name+"=([^&#]*)",
        regex = new RegExp( regexS ),
        results = regex.exec( window.location.search );
    if( results == null ){
        return "";
    } else{
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}
var prop_id =  getParameterByName('id');
$(document).ready(function () {
    if(localStorage.getItem("rowcolor")){
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(12)').addClass("green_row_right");
            localStorage.removeItem('rowcolorTenant');
        }, 2000);
    }
});
$(document).on('click','#new_flag',function () {
    $('#flagFormDiv').show(500);
    $('#flag_id').val('');
    $('#flagSaveBtnId').text('Save');
    $('#flagForm').trigger("reset");
    $('#flagged_for').val($('#property_name').val());
    $('#flag_flag_name').val('');
    var validator = $( "#flagForm" ).validate();
    validator.resetForm();
});

$("#flag_flag_date").datepicker({dateFormat: datepicker});

//on flag cancel
$(document).on('click','#flagCancel',function(){
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $('#flag_id').val('');
                $('#flagFormDiv').hide(500);
            }
        }
    });
});

//adding custom fields
$('#flagForm').on('submit',function(event) {
    event.preventDefault();
    //checking custom field validation
    if ($('#flagForm').valid()) {
        var formData = $('#flagForm').serializeArray();
        $.ajax({
            type: 'post',
            url: '/flag-ajax',
            data: {form: formData,class:'Flag',action:'create_flag',object_id:$("#property_editunique_id").val(),object_type:'property'},
            success: function (response) {
                var response = JSON.parse(response);
                if(response.code == 200){
                    getFlagCount();
                    $('#flagFormDiv').hide(500);
                    $('#propertyFlag-table').trigger('reloadGrid');
                    localStorage.setItem("Message",response.message);
                    localStorage.setItem('rowcolor', 'rowColor');
                    window.location.href = '/Property/PropertyModules';
                  //  toastr.success(response.message);
                } else if(response.code == 200) {
                    toastr.error(response.message);
                }
                
                setTimeout(function(){
                jQuery('#propertyFlag-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                jQuery('#propertyFlag-table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                }, 1000);
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
});

jqGridFlag('All');
/**
 * jqGridFlag Initialization function
 * @param status
 */
function jqGridFlag(status) {
    var table = 'flags';
    var columns = ['Date','Flag Name', 'Phone Number', 'Flag Reason', 'Completed', 'Note', 'Action'];
    var select_column = ['Edit','Delete','Completed'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'object_id',value:$("#property_editunique_id").val(),condition:'='},{column:'object_type',value:'property',condition:'='}];
    var extra_columns = ['flags.deleted_at'];
    var columns_options = [
        {name:'Date',index:'date',align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Flag Name',index:'flag_name',searchoptions: {sopt: conditions},table:table},
        {name:'Phone Number',index:'flag_phone_number',align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Flag Reason',index:'flag_reason', width:180,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Completed',index:'completed',width:180, align:"center",searchoptions: {sopt: conditions},table:table,formatter: completedFormatter},
        {name:'Note',index:'flag_note',width:200, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'select',width:200, title: false,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter: actionFmatter}
    ];
    var ignore_array = [];
    
    jQuery("#propertyFlag-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Flags",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}

/**
 * function to change completed format
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function completedFormatter (cellValue, options, rowObject){
    if (cellValue == 1)
        return "True";
    else if(cellValue == 0)
        return "False";
    else
        return '';
}

/**
 * function to change action column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function actionFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';
        if(rowObject.Completed == 1) select = ['Edit','Delete'];
        if(rowObject.Completed == 0) select = ['Edit','Delete','Completed'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }

}

/**  List Action Functions  */
$(document).on('change', '#propertyFlag-table .select_options', function() {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    var row_num = $(this).parent().parent().index() ;
    if (opt == 'Edit' || opt == 'EDIT') {
        var validator = $( "#flagForm" ).validate();
        validator.resetForm();
        $('#propertyFlag-table').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        jQuery('#propertyFlag-table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('#propertyFlag-table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
        $.ajax({
            type: 'post',
            url: '/flag-ajax',
            data: {id: id,class:'Flag',action:'get_flags'},
            success: function (response) {
                $("#flagFormDiv").show(500);
                $('#flagged_for').val($('#property_name').val());
                $('#flagSaveBtnId').text('Update');
                var data = $.parseJSON(response);
                if (data.code == 200) {
                    getFlagCount();
                    $("#flag_id").val(data.data.id);
                    edit_date_time(data.data.updated_at);
                    $.each(data.data, function (key, value) {
                        $('#flag_' + key ).val(value);
                    });

                    defaultFormData = $('#flagForm').serializeArray();
                } else if (data.code == 500){
                    toastr.error(data.message);
                } else{
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        //$("#portfolio-table").trigger('reloadGrid');
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/flag-ajax',
                        data: {id: id,class:'Flag',action:'deleteFlag'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                getFlagCount();
                                var returnRes = update_users_flag('general_property',prop_id);
                                if (returnRes.status == "success" && returnRes.code == 200){
                                    localStorage.setItem("Message", response.message);
                                    localStorage.setItem('rowcolor', 'rowColor');
                                    setTimeout(function () {
                                        window.location.href = '/Property/PropertyModules';
                                    },1000);
                                }
                                $('#propertyFlag-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }

            }
        });
    } else if (opt == 'Completed' || opt == 'COMPLETED') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to complete this flag ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/flag-ajax',
                        data: {id: id,class:'Flag',action:'flagCompleted'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                getFlagCount();
                                toastr.success(response.message);
                                var returnRes = update_users_flag('general_property',prop_id);
                                if (returnRes.status == "success" && returnRes.code == 200){
                                    localStorage.setItem("Message", response.message);
                                    localStorage.setItem('rowcolor', 'rowColor');
                                    setTimeout(function () {
                                        window.location.href = '/Property/PropertyModules';
                                    },1000);
                                }
                                $('#propertyFlag-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }

            }
        });
    }
    $('.select_options').prop('selectedIndex',0);
});
