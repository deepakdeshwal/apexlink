$(document).on("click",".cancelXml",function () {
    bootbox.confirm("Do you want to remove all files?", function (result) {
        if (result == true) {
            window.location.href="/User/AccountSetup";
        }
    });
})
$(document).on("click",".clearXml",function () {
    $("#xmlForm").trigger("reset");
})

$("#xmlForm").validate({
    rules: {
        first_name: {
            required: true
        },
        last_name: {
            required: true
        },
        email: {
            required: true,
            email:true
        },
        phone_number: {
            required: true
        },
        number_of_units	: {
            required: true
        },
        number_of_vacancies: {
            required: true
        },
        property_location: {
            required: true
        },
        xml_link: {
            required: true
        }
    }
});

$("#xmlForm").on("submit", function (e) {
    e.preventDefault();
    if ($('#xmlForm').valid()) {
        var formData = $('#xmlForm').serializeArray();
        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {
                form: formData,
                class: 'propertyDetail',
                action: 'addRentalRequest'
            },
            success: function (result) {
                var response = JSON.parse(result);
                if (response.status == 'success' && response.code == 200) {
                    $("#xmlModal").modal("hide");
                    toastr.success(response.message);

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    toastr.warning(response.message);

                }
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }

});
getPmdetails();
function getPmdetails() {
    var upload_url = window.location.origin+"/company/upload/"+domain_name_user+'.xml';
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: "propertyDetail",
            action: "getPmdetails"
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $("#xmlForm input[name='first_name']").val(response.data.first_name);
                $("#xmlForm input[name='last_name']").val(response.data.last_name);
                $("#xmlForm input[name='email']").val(response.data.email);
                $("#xmlForm input[name='phone_number']").val(response.data.phone_number);
                $("#xmlForm input[name='number_of_units']").val(response.totalUnits);
                $("#xmlForm input[name='number_of_vacancies']").val(response.vaccantUnits);
                $("#xmlForm input[name='url']").val(upload_url);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
