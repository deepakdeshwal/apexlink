$(document).ready(function () {

    $(document).on('click', '#AddNewUnitTypeModalPlus3', function (e) {
        e.preventDefault();
        $("#NewunitPopup2").show();
    });

    $("#unit_type").keypress(function(e){
        var keyCode = e.which;
        /*
          8 - (backspace)
          32 - (space)
          48-57 - (0-9)Numbers
        */
        /* Not allow special*/
        if ( !( (keyCode >= 48 && keyCode <= 57)
            ||(keyCode >= 65 && keyCode <= 90)
            || (keyCode >= 97 && keyCode <= 122) )
            && keyCode != 8 && keyCode != 32) {
            e.preventDefault();
        }
    });
    var base_url = window.location.origin;


    /** Show add new AccountChargeCode div on add new button click */
    $(document).on('click','#addChargeCodeButton',function () {
        $('#charge_charge_code').val('').prop('disabled', false);
        $('#charge_description').val('');
        $("#charge_charge_code_id").val('');
        $('#charge_code-error').text('');
        $('#charge_codeErr').text('');
        $('#charge_credit_account-error').text('');
        $('#credit_accountErr').text('');
        $('#charge_debit_account-error').text('');
        $('#charge_debit_accountErr').text('');
        $('#charge_status').val('');
        $('#status-error').text('');
        creditAccountList();
        debitAccountList();
        $('#add_charge_code_form').trigger("reset");
        var validator = $( "#add_charge_code_form" ).validate();
        validator.resetForm();
        $('#saveBtnId').val('Save');
        $('#add_charge_code_div').show(500);
    });

    function creditAccountList(){
        $.ajax({
            type: 'post',
            url: '/ChargeCode-Ajax',
            data: {
                class: 'ChargeCodeAjax',
                action: 'getAllCreditList',
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#credit_account').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#credit_account').append($("<option value = "+value.id+">"+value.credit_accounts+"</option>"));
                    })
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    function debitAccountList(){
        $.ajax({
            type: 'post',
            url: '/ChargeCode-Ajax',
            data: {
                class: 'ChargeCodeAjax',
                action: 'getAllDebitList',
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#debit_account').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#debit_account').append($("<option value = "+value.id+">"+value.debit_accounts+"</option>"));
                    })
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    chargeCodeList();
    function chargeCodeList(selectedData){
        $.ajax({
            type: 'post',
            url: '/ChargeCode-Ajax',
            data: {
                class: 'ChargeCodeAjax',
                action: 'getAllChargeCodeList',
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#chargeCodeSelect').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        var selected = '';
                        if((selectedData == 'true') && (key ==response.data.length-1)) selected = 'selected';
                        $('#chargeCodeSelect').append($("<option "+selected+" value = "+value.id+">"+value.charge_code+"</option>"));
                    })
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }



    /** Hide add new AccountChargeCode div on cancel button click */
    $(document).on("click", "#cancelChargeCode", function (e) {
        e.preventDefault();
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#chargeCodeModel").modal('hide');
                }
            }
        });
    });

    /** Add/Edit new AccountChargeCode */
    $("#add_charge_code_form").validate({
        rules: {
            charge_code: {
                required: true,
            },
            credit_account: {
                required:true
            },
            debit_account: {
                required:true
            },
            status: {
                required:true
            }
        },
        messages: {
            charge_code : {
                required: "* This field is required",
            }
        },
        submitHandler: function () {
            var charge_code = $('#charge_charge_code').val();
            var description = $('#charge_description').val();
            var credit_account = $('#charge_credit_account').val();
            var debit_account = $('#charge_debit_account').val();
            var status = $("#charge_status").val();
            var charge_code_id = $("#charge_charge_code_id").val();
            var is_default;
            if ($('#charge_is_default').is(":checked")) {
                is_default = '1';
            } else {
                is_default = '0';
            }
            var formData = {
                'charge_code': charge_code,
                'description': description,
                'credit_account': credit_account,
                'debit_account': debit_account,
                'is_default': is_default,
                'status':status,
                'charge_code_id': charge_code_id
            };

            var action;
            if (charge_code_id) {
                action = 'update';
            } else {
                action = 'insert';
            }
            $.ajax({
                type: 'post',
                url: '/ChargeCode-Ajax',
                data: {
                    class: 'ChargeCodeAjax',
                    action: action,
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        chargeCodeList('true');
                        $("#chargeCodeModel").modal('hide');
                        toastr.success(response.message);
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('#' + key).text(value);
                        });
                    } else if (response.status == 'error' && response.code == 503) {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });

    fetchAllUnittypes2(false);
    function fetchAllUnittypes2(id) {
        var propertyEditid = $("#property_editunique_id").val();

        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {
                propertyEditid: propertyEditid,
                class: 'propertyDetail',
                action: 'fetchAllManageUnittypes'},
            success: function (response) {
                var res = JSON.parse(response);
                var ele = $("#unit_type2");
                // debugger
                ele.html(res.data).multiselect("destroy").multiselect({
                    includeSelectAllOption: true,
                    nonSelectedText: 'Select Unit Type',
                    maxHeight: 200
                });
                if(id) {
                    ele.multiselect('select', id);
                }
            },
        });
    }

    $(document).on('click', '#NewunitPopupSave2', function (e) {
        e.preventDefault();
        var formData = $('#NewunitPopup2 :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addUnittype'},
            beforeSend: function (xhr) {
                // checking portfolio validations
                $(".customValidateUnitType2").each(function () {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewunitPopup2').hide(500);
                    toastr.success(response.message);
                    fetchAllUnittypes2(response.lastid);
                } else if (response.status == 'error' && response.code == 500) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });

    $('.unitSelect').on('change',function(){
       var unitSelect = $(this).prop('checked');
       if($(this).prop('checked')){
            if($(this).val() == '1'){
                $('#unitTypePropertyDiv').show();
                $('#typeForProperty').hide();
            } else if($(this).val() == '0') {
                $('#unitTypePropertyDiv').hide();
                $('#typeForProperty').show();
            }
       }
    });

    //adding custom fields
    $('#manageCharge').on('submit',function(event) {
        event.preventDefault();
        //checking custom field validation
        if ($('#manageCharge').valid()) {
            var formData = $('#manageCharge').serializeArray();
            var property_id = ($("#property_editunique_id").val() !== undefined)?$("#property_editunique_id").val():property_unique_id;
            $.ajax({
                type: 'post',
                url: '/manageCharge-ajax',
                data: {form: formData,class:'ManageCharges',action:'createChargeCode',property_id:property_id},
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.code == 200){
                         toastr.success('Record Added Successfully!');
                        $('#manageCharge')[0].reset();
                       $('#manageCharge .unitTypePropertyDiv select').multiselect('refresh');
                        getManageCharges();
                        $("#14collapseFourteen").trigger('click');
                    } else if(response.code == 403){
                        toastr.success(response.message);
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    });
});

$(document).on('change','#manageCharge select', function() {

    var total = 0,
        selected = this.selectedOptions,
        l = selected.length;

    for (var i = 0; i < l; i++) {
        total += parseInt(selected[i].value, 10);
    }

    var Ftotal = parseInt($('#manageCharge li').length) - parseInt(1);
    $('.multiselect-selected-text').text(total+' of' +Ftotal+ 'selected');

});
$(document).on('change','#manageCharge select', function() {
    var Stotal = $('#manageCharge li.active').length;
    if(Stotal >= 1){
        Stotal = parseInt(Stotal);
    } else {
        Stotal  = 1;
    }

    var Ftotal = $('#manageCharge li').length;
    if(Ftotal){
        Ftotal = parseInt(Ftotal) - parseInt(1);
    }

    setTimeout(function(){
        $('#manageCharge .multiselect-selected-text').text(Stotal+' of ' +Ftotal+ ' selected');
    },100);
});
function jqGridUnitCharge(status,obj) {
    var columns = ['<input type="checkbox" id="select_all_unitCharge_checkbox">','Charge Code','Checked','Is_editable','Unit Type','Frequency','Amount'];
    var conditions = ["eq","bw","ew","cn","in"];
    var columns_options = [
        {name:'id',label:'id',width:250,align:"center",sortable:false,formatter:actionCheckboxFmatter},
        {name:'charge_code',label:'Charge Code',width:260,align:"center"},
        {name:'is_editable',label:'Is_editable',hidden:true,width:260,align:"center"},
        {name:'checked',label:'Checked',hidden:true,width:260,align:"center"},
        {name:'unit_type',label:'Unit Type',width:260,align:"center"},
        {name:'frequency',label:'Frequency',width:255,align:"center"},
        {name:'amount',label:'Amount',width:255,align:"center",formatter:amountInput}
    ];
    var ignore_array = [];
    jQuery("#unitCharge-table").jqGrid({
        datatype: 'local',
        data:obj,
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: false,
        sortname: 'updated_at',
        mtype: "POST",
        sortorder: "asc",
        sortIconsBeforeText: true,
        headertitles: true,
        caption: "Charges : Unit",
        pgbuttons : true,
        viewrecords: true,
        navOptions: {
            edit:false,
            add:false,
            del:false,
            search:true,
            filterable: false,
            refreshtext: "Refresh",

        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,refresh:true
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

function jqGridPropertyCharge(status,obj) {
    var columns = ['<input type="checkbox" id="select_all_propertyCharge_checkbox">','Charge Code','Checked','Is_editable','Frequency','Amount'];
    var conditions = ["eq","bw","ew","cn","in"];
    var columns_options = [
        {name:'id',label:'id',width:250,align:"center",sortable:false,formatter:actionCheckboxFmatterProperty},
        {name:'charge_code',label:'Charge Code',width:345,align:"center"},
        {name:'is_editable',label:'Is_editable',hidden:true,width:260,align:"center"},
        {name:'checked',label:'Checked',hidden:true,width:260,align:"center"},
        {name:'frequency',label:'Frequency',width:340,align:"center"},
        {name:'amount',label:'Amount',width:340,align:"center",formatter:amountInputProperty}
    ];
    var ignore_array = [];
    jQuery("#propertyCharge-table").jqGrid({
        datatype: 'local',
        data:obj,
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: false,
        sortname: 'updated_at',
        mtype: "POST",
        sortorder: "asc",
        sortIconsBeforeText: true,
        headertitles: true,
        caption: "Charges : Property",
        pgbuttons : true,
        viewrecords: true,
        navOptions: {
            edit:false,
            add:false,
            del:false,
            search:true,
            filterable: false,
            refreshtext: "Refresh",
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,refresh:true
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

function getManageCharges(param){
    var property_id = ($("#property_editunique_id").val() !== undefined)?$("#property_editunique_id").val():property_unique_id;
    $.ajax({
        type: 'post',
        url: '/manageCharge-ajax',
        data: {class:'ManageCharges',action:'getManageCharges',property_id:property_id},
        success: function (response) {
            var response = JSON.parse(response);
            if(param == 'unit') {
                if ($("#unitCharge-table")[0].grid) {
                    $('#unitCharge-table').jqGrid('GridUnload');
                }
                jqGridUnitCharge('all', response.unitTypeCharge);
            } else if(param == 'property') {
                if ($("#propertyCharge-table")[0].grid) {
                    $('#propertyCharge-table').jqGrid('GridUnload');
                }
                jqGridPropertyCharge('all', response.propertyTypeCharge);
            } else {
                if ($("#unitCharge-table")[0].grid) {
                    $('#unitCharge-table').jqGrid('GridUnload');
                }
                jqGridUnitCharge('all', response.unitTypeCharge);
                if ($("#propertyCharge-table")[0].grid) {
                    $('#propertyCharge-table').jqGrid('GridUnload');
                }
                jqGridPropertyCharge('all', response.propertyTypeCharge);
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function getChargePrefrences(){
    var property_id = ($("#property_editunique_id").val() !== undefined)?$("#property_editunique_id").val():property_unique_id;
    $.ajax({
        type: 'post',
        url: '/manageCharge-ajax',
        data: {class:'ManageCharges',action:'getChargePrefrences',property_id:property_id},
        success: function (response) {
            var response = JSON.parse(response);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

$(document).on('click','#saveUnitCharges',function(){
    saveManageCharges();
});

$(document).on('click','#propertyChargeCodeSave',function(){
    saveManageChargesProperty();
});



/**
 * function save manage charges unit
 */
function saveManageCharges(){
    var unitData = [];
    var property_id = ($("#property_editunique_id").val() !== undefined)?$("#property_editunique_id").val():property_unique_id;
    $(".unitCharge_checkbox").each(function() {
        var checkedUnit = $(this).prop('checked')?'1':'0';
        var amount = $('#unitSpanAmount_'+$(this).attr('data_id')).text();
        unitData.push({'id':$(this).attr('data_id'),'status':checkedUnit,'amount':amount});
    });
    $.ajax({
        type: 'post',
        url: '/manageCharge-ajax',
        data: {class:'ManageCharges',action:'saveManageCharges',property_id:property_id,data:unitData},
        success: function (response) {
            var response = JSON.parse(response);
            if(response.code == 200){
              //  if ($("#unitCharge-table")[0].grid) {
                  //  $('#unitCharge-table').jqGrid('GridUnload');
              //  }
              toastr.success('Record saved successfully.');
                getManageCharges('unit');
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}

/**
 * function save manage charges property
 */
function saveManageChargesProperty(){
    var propertyData = [];
    var property_id = ($("#property_editunique_id").val() !== undefined)?$("#property_editunique_id").val():property_unique_id;
    $(".propertyCharge_checkbox").each(function() {
        var checkedUnit = $(this).prop('checked')?'1':'0';
        var amount = $('#propertySpanAmount_'+$(this).attr('data_id')).text();
        propertyData.push({'id':$(this).attr('data_id'),'status':checkedUnit,'amount':amount});
    });
    $.ajax({
        type: 'post',
        url: '/manageCharge-ajax',
        data: {class:'ManageCharges',action:'saveManageCharges',property_id:property_id,data:propertyData},
        success: function (response) {
            var response = JSON.parse(response);
            if(response.code == 200){
                getManageCharges('property');
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}



function actionCheckboxFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var data = '';
        var checked = (rowObject.checked=='1')?'checked':'';
        var checkedclass = (rowObject.is_editable=='1')?'checkboxchecked':'';
        var readonly = (rowObject.is_editable=='0')?'onclick="return false"':'';
        var data = '<input type="checkbox" name="unitCharge_checkbox[]" '+checked+' '+readonly+' class="unitCharge_checkbox '+checkedclass+'" id="unitCharge_checkbox_'+rowObject.id+'" data_id="' + rowObject.id + '"/>';
        return data;
    }
}

function amountInput (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var data = '';
        var amountCell = (cellvalue !== undefined && cellvalue !== null)?cellvalue:'0.00';
        var data = '<span id="unitSpanAmount_'+rowObject.id+'">'+amountCell+'</span><input style="display:none;"  maxlength="10" type="text" name="unitCharge_input[]" class="unitCharge_input amount number_only number_only_C" id="unitCharge_input_'+rowObject.id+'" data_id="' + rowObject.id + '"/> <i class="cursor editUnitCharge fa fa-edit"></i>';
        return data;
    }
}
$(document).on('keydown','.number_only_C',function () {
    var amount = $(this).val();
    amount = Number(amount.replace(/[^0-9.-]+/g,""));
    // amount = amount.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    $(this).val(amount);
});


function actionCheckboxFmatterProperty (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var data = '';
        var checked = (rowObject.checked=='1')?'checked':'';
        var checkedclass = (rowObject.is_editable=='1')?'checkboxcheckedproperty':'';
        var readonly = (rowObject.is_editable=='0')?'onclick="return false"':'';
        var data = '<input type="checkbox" name="propertyCharge_checkbox[]" '+checked+' '+readonly+' class="propertyCharge_checkbox '+checkedclass+'" id="propertyCharge_checkbox_'+rowObject.id+'" data_id="' + rowObject.id + '"/>';
        return data;
    }
}

function amountInputProperty (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var data = '';
        var amountCell = (cellvalue !== undefined && cellvalue !== null)?cellvalue:'0.00';
        var data = '<span id="propertySpanAmount_'+rowObject.id+'">'+amountCell+'</span><input style="display:none;"  maxlength="10" type="text" name="propertyCharge_input[]" class="propertyCharge_input amount number_only" id="propertyCharge_input_'+rowObject.id+'" data_id="' + rowObject.id + '"/> <i class="cursor editPropertyCharge fa fa-edit"></i>';
        return data;
    }
}


$(document).on('click','.editUnitCharge',function(){
    $(this).siblings('.unitCharge_input').show().focus();
    $(this).siblings('span').hide();
    $(this).hide();
});

$(document).on('focusout','.unitCharge_input',function(){
    var dataValue = ($(this).val() == '')? '0.00':$(this).val();
    $(this).siblings('.editUnitCharge').show();
    $(this).siblings('span').text(dataValue).show();
    $(this).hide();
});

$(document).on('click','.editPropertyCharge',function(){
    $(this).siblings('.propertyCharge_input').show().focus();
    $(this).siblings('span').hide();
    $(this).hide();
});

$(document).on('focusout','.propertyCharge_input',function(){
    var dataValue = ($(this).val() == '')? '0.00':$(this).val();
    $(this).siblings('.editPropertyCharge').show();
    $(this).siblings('span').text(dataValue).show();
    $(this).hide();
});

$(document).on('click','#select_all_unitCharge_checkbox',function(){
    if($(this).prop('checked')){
        $('.checkboxchecked').prop('checked',true);
    } else {
        $('.checkboxchecked').prop('checked',false);
    }
});

$(document).on('click','#select_all_propertyCharge_checkbox',function(){
    if($(this).prop('checked')){
        $('.checkboxcheckedproperty').prop('checked',true);
    } else {
        $('.checkboxcheckedproperty').prop('checked',false);
    }
});