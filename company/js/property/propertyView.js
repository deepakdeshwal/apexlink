fetchGeneralDetails();
fetchSchoolDistrict();
/**
 * function to retrive general detail data for view
 */
function fetchGeneralDetails() {
    $.ajax({
        type: 'post',
        url: '/propertyView-ajax',
        data: {class: 'propertyView', action: "getPropertyDetail", property_id: property_unique_id},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200) {
                var garage;
                $.each(response.data, function (key, value) {
                    if(key == 'phone_number'){
                        $('#prop_phone_number').text(value);
                    }else if(key=='key_access_codes_info'){
                        $('#key_access_codes_info').html(response.key_access_codes_info_string)
                    }else if(key=='garage_avail'){
                         garage = value;                       
                    }else if(key=='property_price' && value== 'N/A' ){
                        $("#property_symbol").html('');
                        $('#' + key).text(value);
                    } else {
                        $('#' + key).text(value);
                    }
                });
                if(garage ==  'Yes')
                {
                    //alert(value);
                           $(".garage_yes").show();
                           $(".garage_other").hide();
                } else {
                          $(".garage_other").show();  
                          $(".garage_yes").hide();
                }
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function fetchSchoolDistrict() {
    $.ajax({
        type: 'post',
        url: '/propertyView-ajax',
        data: {class: 'propertyView', action: "getSchoolDistrictInfo", property_id: property_unique_id},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200) {
                jqGridSchoolDistrict('All', response.data);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
fetchLinkOwner();
function fetchLinkOwner() {
    $.ajax({
        type: 'post',
        url: '/propertyView-ajax',
        data: {class: 'propertyView', action: "getLinkOwnerInfo", property_id: property_unique_id},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200) {
                $.each(response.data, function (key, value) {
                    $('#' + key).text(value);
                });
                jqGridLinkOwner('All', response.ownedData);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
fetchLateFeeManagement();
function fetchLateFeeManagement() {
    $.ajax({
        type: 'post',
        url: '/propertyView-ajax',
        data: {class: 'propertyView', action: "getLateFeeManagement", property_id: property_unique_id},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200) {
                $.each(response.data, function (key, value) {
                    $('#' + key).text(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
fetchManagementFee();
function fetchManagementFee() {
    $.ajax({
        type: 'post',
        url: '/propertyView-ajax',
        data: {class: 'propertyView', action: "getManagementFees", property_id: property_unique_id},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200) {
                $.each(response.data, function (key, value) {
                    if(key == 'management_type'){
                        if(value == 'Flat'){
                            $('#manage_fee_type').text();
                        } else if(value == 'Percent'){
                            $('#manage_fee_type').text('%');
                        } else if(value == 'Percentage and a min. set fee'){
                            $('.minimum_management_fee').show();
                            $('#manage_fee_type').text('%');
                            $('#minimum_management_fee').text(value);
                        }
                        $('#management_type').text(value);
                    } else {
                        $('#' + key).text(value);
                    }
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

fetchManagementInfo();
function fetchManagementInfo() {
    $.ajax({
        type: 'post',
        url: '/propertyView-ajax',
        data: {class: 'propertyView', action: "getManagementInfo", property_id: property_unique_id},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200) {
                $.each(response.data, function (key, value) {
                    $('#management_' + key).text(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
fetchMaintenanceInfo();
function fetchMaintenanceInfo() {
    $.ajax({
        type: 'post',
        url: '/propertyView-ajax',
        data: {class: 'propertyView', action: "getMaintenanceInfo", property_id: property_unique_id},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200) {
                $.each(response.data, function (key, value) {
                    $('#maintenance_' + key).text(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

fetchMortgageInfo();
function fetchMortgageInfo() {
    $.ajax({
        type: 'post',
        url: '/propertyView-ajax',
        data: {class: 'propertyView', action: "getMortgageInfo", property_id: property_unique_id},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200) {
                $.each(response.data, function (key, value) {
                    $('#mortgage_' + key).text(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
fetchPropertyLoan();
function fetchPropertyLoan() {
    $.ajax({
        type: 'post',
        url: '/propertyView-ajax',
        data: {class: 'propertyView', action: "getPropertyLoan", property_id: property_unique_id},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200) {
                $.each(response.data, function (key, value) {
                    $('#loan_' + key).text(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
fetchCustomFields();
function fetchCustomFields() {
    $.ajax({
        type: 'post',
        url: '/propertyView-ajax',
        data: {class: 'propertyView', action: "getCustomFields", property_id: property_unique_id},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200) {
                getCustomFieldForViewMode(response.data);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

fetchDetails();
function fetchDetails() {
    $.ajax({
        type: 'post',
        url: '/propertyView-ajax',
        data: {class: 'propertyView', action: "getDetails", property_id: property_unique_id},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200) {
                $.each(response.data, function (key, value) {
                    $('#detail_' + key).text(value);
                });
                $.each(response.imageData, function(index, img) {
                    var image=  upload_url+'/company/'+img.file_location
                    $('.owl-carousel').trigger('add.owl.carousel', ['<div class="item"><img src="'+image+'" alt=""></div>'])
                        .trigger('refresh.owl.carousel');
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

jqGridRenovationDetail('All');
function jqGridRenovationDetail(status) {
    var table = 'renovation_details';
    var columns = ['Last Renovation Date','Last Renovation Time','Last Renovation Description'];
    var select_column = [];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'object_id',value:property_unique_id,condition:'='},{column:'object_type',value:'property',condition:'='}];
    var columns_options = [
        {name:'Last Renovation Date',index:'last_renovation_date',width:400,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Last Renovation Time',index:'last_renovation_time',width:450,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'time'},
        {name:'Last Renovation Description',index:'last_renovation_description',width:450,align:"center",searchoptions: {sopt: conditions},table:table}
    ];
    var ignore_array = [];
    jQuery("#renovationDetail-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Renovation Detail",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

function jqGridSchoolDistrict(status,obj) {
    var table = 'general_property';
    var columns = ['School District','Code','Notes'];
    var conditions = ["eq","bw","ew","cn","in"];
    var columns_options = [
        {name:'school_district_municipality',label:'School District',width:400,align:"center"},
        {name:'school_district_code',label:'Code',width:450,align:"center"},
        {name:'school_district_notes',label:'Notes',width:450,align:"center"}
    ];
    var ignore_array = [];
    jQuery("#schoolDistrict-table").jqGrid({
        datatype: 'local',
        data:obj,
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        caption: "School District Info",
        pgbuttons : true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        viewrecords: true,
        navOptions: {
            edit:false,
            add:false,
            del:false,
            search:true,
            filterable: false,
            refreshtext: "Refresh",

        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,refresh:true
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

function jqGridLinkOwner(status,obj) {
    var table = 'general_property';
    var columns = ['Owner','Percent Owned'];
    var conditions = ["eq","bw","ew","cn","in"];
    var columns_options = [
        {name:'name',label:'Owner',width:400,align:"left",searchoptions: {sopt: conditions}},
        {name:'owner_percentowned',label:'Percent Owned',width:450,align:"left",searchoptions: {sopt: conditions}},
    ];
    var ignore_array = [];
    jQuery("#linkOwner-table").jqGrid({
        datatype: 'local',
        data:obj,
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        caption: "List of Property Owners/Landlords",
        pgbuttons : true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        viewrecords: true,
        navOptions: {
            edit:false,
            add:false,
            del:false,
            search:true,
            filterable: false,
            refreshtext: "Refresh"
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,refresh:true
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}
jqGridKeyTrackerDetail('All');
function jqGridKeyTrackerDetail(status) {
    var table = 'key_tracker';
    var columns = ['Key Tag','Description','Available'];
    var select_column = [];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'property_id',value:property_unique_id,condition:'='}];
    var columns_options = [
        {name:'Key Tag',index:'key_tag',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Description',index:'description',width:450,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Available',index:'total_keys',width:450,align:"center",searchoptions: {sopt: conditions},table:table}
    ];
    var ignore_array = [];
    jQuery("#keys-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Keys",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}
jqGridPropertyBankDetail('All');
function jqGridPropertyBankDetail(status) {
    var table = 'property_bank_details';
    var columns = ['Account Name','Chart of Account','Account Type','Bank Name','Bank Routing','Bank Account','SDB Account','Minimum Reservation Account'];
    var select_column = [];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'property_id',value:property_unique_id,condition:'='}];
    var columns_options = [
        {name:'Account Name',index:'account_name',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Chart of Account',index:'chart_of_account',width:450,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Account Type',index:'account_type',width:450,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Bank Name',index:'bank_name',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Bank Routing',index:'bank_routing',width:450,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Bank Account',index:'bank_account',width:450,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'SDB Account',index:'default_security_deposit',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Minimum Reservation Account',index:'bank_account_min_reserve_amount',width:450,align:"center",searchoptions: {sopt: conditions},table:table}
    ];
    var ignore_array = [];
    jQuery("#propertyBankAccount-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Property Accounts",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

jqGridPropertyInsurance('All');
function jqGridPropertyInsurance(status) {
    var table = 'property_insurance';
    var columns = ['Insurance Company Name','Insurance Type Name','Phone Number','Insurance Agent Name','Policy#','Policy Type Name','Coverage Amount','Renewal Date','Annual Premium (USh)','Status'];
    var select_column = [];
    var joins = [{table: 'property_insurance', column: 'company_insurance_type', primary: 'id', on_table: 'company_property_insurance'},{table: 'property_insurance', column: 'policy_name', primary: 'id', on_table: 'company_property_policy'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'property_id',value:property_unique_id,condition:'='}];
    var columns_options = [
        {name:'Insurance Company Name',index:'insurance_type',width:400,align:"center",searchoptions: {sopt: conditions},table:'company_property_insurance'},
        {name:'Insurance Type Name',index:'company_insurance_type',width:450,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Phone Number',index:'phone_number',width:450,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Insurance Agent Name',index:'agent_name',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Policy#',index:'policy',width:450,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Policy Type Name',index:'policy_type',width:450,align:"center",searchoptions: {sopt: conditions},table:'company_property_policy'},
        {name:'Coverage Amount',index:'coverage_amount',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Renewal Date',index:'renewal_date',width:450,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Annual Premium (USh)',index:'annual_premium',width:450,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Status',index:'status',width:450,align:"center",searchoptions: {sopt: conditions},table:table}
    ];
    var ignore_array = [];
    jQuery("#propertyInsurance-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'property_insurance.updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Property Insurance",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

jqGridWarrantyInformation('All');
function jqGridWarrantyInformation(status) {
    var table = 'warranty_information';
    var columns = ['Key fixture Name','Fixture Type','Maintenance Reminder','Assessed Age','Model','Warrenty Expiration','Insurance Expiration','Condition','Status'];
    var select_column = [];
    var joins = [{table: 'warranty_information', column: 'fixture_type', primary: 'id', on_table: 'fixture_types'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'property_id',value:property_unique_id,condition:'='}];
    var columns_options = [
        {name:'Key fixture Name',index:'key_fixture_name',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Fixture Type',index:'fixture_type',width:450,align:"center",searchoptions: {sopt: conditions},table:'fixture_types'},
        {name:'Maintenance Reminder',index:'maintenance_reminder',width:450,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Assessed Age',index:'assessed_age',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Model',index:'model',width:450,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Warrenty Expiration',index:'warranty_expiration_date',width:450,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Insurance Expiration',index:'insurance_expiration_date',width:400,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Condition',index:'status',width:450,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Status',index:'status',width:450,align:"center",searchoptions: {sopt: conditions},table:table}
    ];
    var ignore_array = [];
    jQuery("#warrantyInformation-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'warranty_information.updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Property Fixture",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

jqGridNotes('All');
function jqGridNotes(status) {
    var table = 'property_notes';
    var columns = ['Property Notes','Date','Status'];
    var select_column = [];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = ['created_at'];
    var extra_where = [{column:'property_id',value:property_unique_id,condition:'='}];
    var columns_options = [
        {name:'Property Notes',index:'notes',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Date',index:'created_at',width:450,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Status',index:'status',width:450,align:"center",searchoptions: {sopt: conditions},table:table},
    ];
    var ignore_array = [];
    jQuery("#notes-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'property_notes.updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Property Notes",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

/**
 * jqGrid Intialization function
 * @param status
 */
jqGridFileLibrary('All');
function jqGridFileLibrary(status) {
    var table = 'property_file_uploads';
    var columns = ['Name','Preview','Location','Action'];
    var select_column = ['Delete'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'file_type',value:'2',condition:'='},{column:'property_id',value:property_unique_id,condition:'='}];
    var columns_options = [
        {name:'Name',index:'file_name',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'file_extension',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
        {name:'Location',index:'file_location',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,hidden:true},
        {name:'Action',index:'select',width:430,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,hidden:true, formatter: 'select', edittype: 'select',search:false,table:table,title:false}
    ];
    var ignore_array = [];
    jQuery("#propertFileLibrary-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of File Library",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

/**
 * jqGrid Intialization function
 * @param status
 */
jqGridPhotovideos('All');
function jqGridPhotovideos(status) {
    var table = 'property_file_uploads';
    var columns = ['Name','Preview','Action'];
    var select_column = ['Email','Delete'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = ['property_file_uploads.codec'];
    var extra_where = [{column:'file_type',value:'("1","3")',condition:'IN'},{column:'property_id',value:property_unique_id,condition:'='}];
    var columns_options = [
        {name:'Name',index:'file_name',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'file_location',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:photoFormatter},
        {name:'Action',index:'select',width:430,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, hidden:true,formatter: 'select', edittype: 'select',search:false,table:table,title:false}
    ];
    var ignore_array = [];
    jQuery("#propertPhotovideos-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Photos/Virtual Tour Videos",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

/**
 * function to format document
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function imageFormatter(cellvalue, options, rowObject){

    if(rowObject !== undefined) {
        var path = upload_url+'company/'+rowObject.Location;
        var src = '';
        if (rowObject.Preview == 'xlsx') {
            src = upload_url + 'company/images/excel.png';
        } else if (rowObject.Preview == 'pdf') {
            src = upload_url + 'company/images/pdf.png';
        } else if (rowObject.Preview == 'docx') {
            src = upload_url + 'company/images/word_doc_icon.jpg';
        }else if (rowObject.Preview == 'txt') {
            src = upload_url + 'company/images/notepad.jpg';
        }
        return '<img class="img-upload-tab open_file_location" data-location="'+path+'" width=40 height=40 src="' + src + '">';
    }
}

$(document).on('click',".open_file_location",function(){
    var location =    $(this).attr("data-location");
    window.open(location, '_blank');
});

/**
 * function to format photos and videos
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function photoFormatter(cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var path = upload_url+'company/'+rowObject.Preview;
        console.log(path);
        var select = rowObject.Action;
        var type = $(select).attr('type');
        if(type.startsWith("image")){
            var appendType = '<a href="' + path + '" target="_blank"><img width=40 height=40 src="' + path + '"></a>';
        } else if(type.startsWith("video")){
            var appendType = '<video width=240 height=140 controls>' +
                '<source src="'+path+'" type="'+type+'">' +
                '</video>';
        }
        return appendType;
    }
}

/**
 * function to show custom field
 * @param editCustomFieldData
 */
function getCustomFieldForViewMode(editCustomFieldData){
    var module = ($('#customFieldModule').val() !== undefined)?$('#customFieldModule').val():'';
    $.ajax({
        type: 'POST',
        url: '/CustomField/get',
        data:{module:module},
        success: function (response) {
            var response = JSON.parse(response);
            var custom_array = [];
            if(response.code == 200 && response.message == 'Record retrieved successfully'){
                $('.custom_field_html_view_mode').html('');
                $('.custom_field_msg').html('');
                var html ='';
                $.each(response.data, function (key, value) {
                    var default_val = '';
                    if(editCustomFieldData) {
                        default_val = (value.default_value != '' && value.default_value != null) ? value.default_value : '';
                        $.each(editCustomFieldData.custom_field, function (key1, value1) {
                            if (value1.id == value.id) {
                                default_val = value1.value;
                            }
                        });

                    } else {
                        default_val = (value.default_value != '' && value.default_value != null) ? value.default_value : '';
                    }

                    var required = (value.is_required == '1')?' <em class="red-star">*</em>':'';
                    var name = value.field_name.replace(/ /g, "_");
                    var currency_class = (value.data_type == 'currency')?' amount':'';
                    if(value.data_type == 'date') {
                        custom_array.push(name);
                    }
                    var readonly = 'readonly';
                    html += '<div class="row custom_field_class">';
                    html += '<div class=col-sm-6>';
                    html += '<label>'+value.field_name+required+'</label>';
                    html += '<input type="text" readonly data_type="'+value.data_type+'" data_value="'+value.default_value+'" '+readonly+' data_id="'+value.id+'" data_required="'+value.is_required+'" name="'+name+'" id="'+name+'" value="'+default_val+'" class="form-control changeCustom add-input'+currency_class+'">';
                    //html += '<span class="customError required"></span>';
                    //html += '</div>';
                    if(value.is_editable == 1 || value.is_deletable == 1) {
                        //html += '<div class="col-sm-6">';
                        if (value.is_editable == 1) {
                            html += '<span id="' + value.id + '" class="editCustomField"><i class="fa fa-edit"></i></span>';
                        }
                        if (value.is_deletable == 1) {
                            html += '<span id="' + value.id + '" class="deleteCustomField"><i class="fa fa-times-circle""></i></span>';
                        }
                        //html += '</div>';
                    }
                    html += '<span class="customError required"></span>';
                    html += '</div>';
                    html += '</div>';

                });
                $(html).prependTo('.custom_field_html_view_mode');
            } else {
                $('.custom_field_html_view_mode').html('No Custom Fields');
            }
            triggerDatepicker(custom_array)
        },
        error: function (data) {
            console.log(data);
        }
    });
}

/**
 * function to trigger datepicker for custom fields
 * @param custom_array
 */
function triggerDatepicker(custom_array){
    $.each(custom_array, function (key, value) {
        $('#customField'+value).datepicker({
            dateFormat: datepicker
        });
    });
}
jqGridPropertyComplaints('All');
function jqGridPropertyComplaints(status) {
    var table = 'complaints';
    var columns = ['Complaint Id','Complaint','Complaint Type','Notes','Date','Other'];
    var select_column = ['Email','Delete'];
    var joins = [{table: 'complaints', column: 'complaint_type_id', primary: 'id', on_table: 'complaint_types'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'module_type',value:'property',condition:'='},{column:'object_id',value:property_unique_id,condition:'='}];
    var columns_options = [
        {name:'Complaint Id',index:'complaint_id',width:200,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Complaint',index:'complaint_name',width:250,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Complaint Type',index:'complaint_type',width:200,align:"center",searchoptions: {sopt: conditions},table:'complaint_types'},
        {name:'Notes',index:'complaint_note',width:250,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Date',index:'complaint_date',width:200,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Other',index:'other_notes',width:200,align:"center",searchoptions: {sopt: conditions},table:table}
    ];
    var ignore_array = [];
    jQuery("#propertComplaints-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'complaints.updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Complaints",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

jqGridWatingList('All');
/**
 * jqGridFlag Initialization function
 * @param status
 */
function jqGridWatingList(status) {
    var table = 'property_wating_list';
    var columns = ['Lead Name','Date', 'Phone Number', 'Email ID', 'Preferred', 'Pets', 'Notes','Property Name','Building','Unit'];
    var select_column = [];
    var joins = [{table:'property_wating_list',column:'building_id',primary:'id',on_table:'building_detail'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'property_id',value:property_unique_id,condition:'='}];
    var extra_columns = ['property_wating_list.deleted_at'];
    var columns_options = [
        {name:'Lead Name',index:'first_name',align:"center",searchoptions: {sopt: conditions},table:table,change_type:'name'},
        {name:'Date',index:'created_at',searchoptions: {sopt: conditions},table:table},
        {name:'Phone Number',index:'phone',align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Email ID',index:'email', width:110,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Preferred',index:'preferred',width:100, align:"center",searchoptions: {sopt: conditions},table:table,formatter:preferredFormater },
        {name:'Pets',index:'pets',width:100, align:"center",searchoptions: {sopt: conditions},table:table,formatter:petFormater},
        {name:'Notes',index:'note',width:110, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Property Name',index:'property_name',width:110, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Building',index:'building_name',width:110, align:"center",searchoptions: {sopt: conditions},table:'building_detail'},
        {name:'Unit',index:'unit',width:110, align:"center",searchoptions: {sopt: conditions},table:table}
    ];
    var ignore_array = [];
    jQuery("#propertWatingList-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'property_wating_list.updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Names",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

/**
 * function to change prefered format
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function preferredFormater (cellValue, options, rowObject){
    if (cellValue == 1)
        return "1stBR";
    else if(cellValue == 2)
        return "2ndBR";
    else if(cellValue == 3)
        return "3rdBR";
    else if(cellValue == 4)
        return "4thBR";
    else if(cellValue == 5)
        return "5thBR";
    else if(cellValue == 6)
        return "6thBR";
    else if(cellValue == 7)
        return "Commercial";
    else if(cellValue == 8)
        return "House";
    else if(cellValue == 9)
        return "Single/Family";
    else if(cellValue == 10)
        return "Multi Family";
    else if(cellValue == 11)
        return "Studio";
    else if(cellValue == 12)
        return "Office";
    else if(cellValue == 13)
        return "Other";
    else
        return '';
}

/**
 * function to change pet format
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function petFormater (cellValue, options, rowObject){
    if (cellValue == '1')
        return "Yes";
    else if(cellValue == '2')
        return "No";
    else if(cellValue == '3')
        return "Others";
    else
        return '';
}

jqGridFlag('All');
/**
 * jqGridFlag Initialization function
 * @param status
 */
function jqGridFlag(status) {
    var table = 'flags';
    var columns = ['Date','Flag Name', 'Phone Number', 'Flag Reason', 'Completed', 'Note'];
    var select_column = [];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'object_id',value:property_unique_id,condition:'='},{column:'object_type',value:'property',condition:'='}];
    var extra_columns = ['flags.deleted_at'];
    var columns_options = [
        {name:'Date',index:'date',align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Flag Name',index:'flag_name',searchoptions: {sopt: conditions},table:table},
        {name:'Phone Number',index:'flag_phone_number',align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Flag Reason',index:'flag_reason', width:180,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Completed',index:'completed',width:180, align:"center",searchoptions: {sopt: conditions},table:table,formatter: completedFormatter},
        {name:'Note',index:'flag_note',width:200, align:"center",searchoptions: {sopt: conditions},table:table}
    ];
    var ignore_array = [];
    jQuery("#propertyFlagBank-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Flags",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

/**
 * function to change completed format
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function completedFormatter (cellValue, options, rowObject){
    if (cellValue == 1)
        return "True";
    else if(cellValue == 0)
        return "False";
    else
        return '';
}

jqGridPropertyCharge('all');
/**
 * jqGridFlag Initialization function
 * @param status
 */
function jqGridPropertyCharge(status) {
    var table = 'property_manage_charge';
    var columns = ['<input type="checkbox" id="select_all_propertyCharge_checkbox">','Charge Code','Checked','Is_editable','Frequency','Amount'];
    var select_column = [];
    var joins = [{table:'property_manage_charge',column:'charge_code',primary:'id',on_table:'company_accounting_charge_code'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'checked',value:'1',condition:'='},{column:'charge_code_type',value:'0',condition:'='},{column:'property_id',value:property_unique_id,condition:'='}];
    var extra_columns = [];
    var columns_options = [
        {name:'id',index:'id',width:250,align:"center",searchoptions: {sopt: conditions},sortable:false,table:table,formatter:actionCheckboxFmatterProperty,search:false},
        {name:'charge_code',index:'charge_code',searchoptions: {sopt: conditions},width:345,align:"center",table:'company_accounting_charge_code'},
        {name:'is_editable',index:'is_editable',hidden:true,width:260,align:"center",table:table},
        {name:'checked',index:'checked',searchoptions: {sopt: conditions},hidden:true,width:260,align:"center",table:table},
        {name:'frequency',index:'frequency',searchoptions: {sopt: conditions},width:340,align:"center",table:table,formatter:frequencyFormater},
        {name:'amount',index:'amount',searchoptions: {sopt: conditions},width:340,align:"center",table:table,formatter:amountInputProperty}
    ];
    var ignore_array = [];
    jQuery("#propertyCharges-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where,
            deleted_at:'no'
        },
        viewrecords: true,
        sortname: 'property_manage_charge.updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Charges Property",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

/**
 * function ActionCheckbox Formater property
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function actionCheckboxFmatterProperty (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var data = '';
        var checked = (rowObject.checked=='1')?'checked':'';
        var checkedclass = (rowObject.is_editable=='1')?'checkboxcheckedproperty':'';
        var readonly = 'onclick="return false"';
        var data = '<input type="checkbox" name="propertyCharge_checkbox[]" '+checked+' '+readonly+' class="propertyCharge_checkbox '+checkedclass+'" id="propertyCharge_checkbox_'+rowObject.id+'" data_id="' + rowObject.id + '"/>';
        return data;
    }
}

/**
 * function action formater property
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function amountInputProperty (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var amountCell = (cellvalue != '')?cellvalue:'0.00';
        return amountCell;
    }
}

jqGridUnitCharge('all');
/**
 * function to show jqrid unit charges
 * @param status
 */
function jqGridUnitCharge(status) {
    var table = 'property_manage_charge';
    var columns = ['<input type="checkbox" id="select_all_unitCharge_checkbox">','Charge Code','Checked','Is_editable','Unit Type','Frequency','Amount'];
    var select_column = [];
    var joins = [{table:'property_manage_charge',column:'charge_code',primary:'id',on_table:'company_accounting_charge_code'},{table:'property_manage_charge',column:'unit_type_id',primary:'id',on_table:'company_unit_type'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'checked',value:'1',condition:'='},{column:'charge_code_type',value:'1',condition:'='},{column:'property_id',value:property_unique_id,condition:'='}];
    var extra_columns = [];
    var columns_options = [
        {name:'id',index:'id',width:250,align:"center",searchoptions: {sopt: conditions},sortable:false,table:table,formatter:actionCheckboxFmatter,search:false},
        {name:'charge_code',index:'charge_code',searchoptions: {sopt: conditions},width:260,align:"center",table:'company_accounting_charge_code'},
        {name:'is_editable',index:'is_editable',searchoptions: {sopt: conditions},hidden:true,width:260,align:"center",table:table},
        {name:'checked',index:'checked',hidden:true,width:260,align:"center",table:table},
        {name:'unit_type',index:'unit_type',searchoptions: {sopt: conditions},width:260,align:"center",table:'company_unit_type'},
        {name:'frequency',index:'frequency',searchoptions: {sopt: conditions},width:255,align:"center",table:table,formatter:frequencyFormater},
        {name:'amount',index:'amount',searchoptions: {sopt: conditions},width:255,align:"center",table:table,formatter:amountInput}
    ];
    var ignore_array = [];
    jQuery("#propertyUnits-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where,
            deleted_at:'no'
        },
        viewrecords: true,
        sortname: 'property_manage_charge.updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Charges Unit",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}


/**
 * function checkbox formater for unit charge
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function actionCheckboxFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var data = '';
        var checked = (rowObject.checked=='1')?'checked':'';
        var checkedclass = (rowObject.is_editable=='1')?'checkboxchecked':'';
        var readonly = (rowObject.is_editable=='0')?'onclick="return false"':'';
        var data = '<input type="checkbox" name="unitCharge_checkbox[]" '+checked+' '+readonly+' class="unitCharge_checkbox '+checkedclass+'" id="unitCharge_checkbox_'+rowObject.id+'" data_id="' + rowObject.id + '"/>';
        return data;
    }
}

/**
 * function amount formater for unit charge
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function amountInput (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var amountCell = (cellvalue != '')?cellvalue:'0.00';
        return amountCell;
    }
}

/**
 * function amount formater for unit charge
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function frequencyFormater (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var frequency = (cellvalue == '1')?'One Time':'Monthly';
        return frequency;
    }
}

jqGridOwnerVendor('all');
/**
 * function to show jqrid unit charges
 * @param status
 */
function jqGridOwnerVendor(status) {
    var table = 'owner_blacklist_vendors';
    var columns = ['Vendor Name','Address'];
    var select_column = [];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'status',value:'1',condition:'='},{column:'property_id',value:property_unique_id,condition:'='}];
    var extra_columns = [];
    var columns_options = [
        {name:'Vendor Name',index:'vendor_name',width:600,align:"center",table:table},
        {name:'Address',index:'vendor_address',width:600,align:"center",table:table},
    ];
    var ignore_array = [];
    jQuery("#propertyOwnerVendor-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where,
            deleted_at:'no'
        },
        viewrecords: true,
        sortname: 'owner_blacklist_vendors.updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Owner Preferred Vendors",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

jqGridBlacklistVendor('all');
/**
 * function to show jqrid unit charges
 * @param status
 */
function jqGridBlacklistVendor(status) {
    var table = 'owner_blacklist_vendors';
    var columns = ['Vendor Name','Address','Date','Reason'];
    var select_column = [];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'status',value:'0',condition:'='},{column:'property_id',value:property_unique_id,condition:'='}];
    var extra_columns = [];
    var columns_options = [
        {name:'Vendor Name',index:'vendor_name',width:600,align:"center",table:table},
        {name:'Address',index:'vendor_address',width:600,align:"center",table:table},
        {name:'Date',index:'blacklist_date',width:600,align:"center",table:table,change_type:'date'},
        {name:'Reason',index:'blacklist_reason',width:600,align:"center",table:table}
    ];
    var ignore_array = [];
    jQuery("#propertyBlacklistVendors-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where,
            deleted_at:'no'
        },
        viewrecords: true,
        sortname: 'owner_blacklist_vendors.updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Blacklisted Vendors",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

$(document).on('click','.viewEditClass',function(){
    var href = $(this).attr('data_href');
    localStorage.setItem("AccordionHref",href);
    window.location.href = "/Property/EditProperty?id=" + property_unique_id;
});

$(".mainPropertytab").each(function () {
    var data = $(this).attr('data');
    var id = property_unique_id;
    if(data == 'property') $(this).attr('href','/Property/PropertyView?id='+id);
    if(data == 'building') $(this).attr('href','/Building/BuildingModule?id='+id);
    if(data == 'unit') $(this).attr('href','/Unit/UnitModule?id='+id);
});

