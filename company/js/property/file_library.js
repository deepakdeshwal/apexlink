$(document).on('click','#add_libraray_file',function(){
    $('#file_library').val('');
    $('#file_library').trigger('click');
});

var file_library = [];
var imgArray = [];
$(document).on('change','#file_library',function(){
   // file_library = [];
    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        if(size > 4097) {
            toastr.warning('Please select documents less than 4 mb!');
        } else {
            size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
            if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                  if($.inArray(value['name'], imgArray) === -1)
                {
                    file_library.push(value);
                }
                var src = '';
                var reader = new FileReader();
              //  $('#file_library_uploads').html('');
                reader.onload = function (e) {
                    if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                        src = upload_url + 'company/images/excel.png';
                    } else if (type == 'application/pdf') {
                        src = upload_url + 'company/images/pdf.png';
                    } else if (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                        src = upload_url + 'company/images/word_doc_icon.jpg';
                    } else if (type == 'text/plain') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else if (type == 'text/xml') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else {
                        src = e.target.result;
                    }
                    console.log(imgArray);
                   if($.inArray(value['name'], imgArray) === -1)
                    {
                        
                    $("#file_library_uploads").append(
                        '<div class="row" style="margin:20px">' +
                        '<div class="col-sm-12 img-upload-library-div">' +
                        '<div class="col-sm-3"><img class="img-upload-tab' + key + '" width=80 height=88 src=' + src + '></div>' +
                        '<div style="margin-top: 36px;" class="col-sm-3 center_img show-library-list-imgs-name ' + key + '">' + value['name'] + '</div>' +
                        '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                        '<div  style="    margin-top: 36px;" class="col-sm-3 center_img show-library-list-imgs-size' + key + '">' + size + '</div>' +
                        '<div class="col-sm-3 center_img"><span id=' + key + ' class="delete_pro_img cursor"><button class="orange-btn">Delete</button></span></div></div></div>');
                        imgArray.push(value['name']);
                    } else {
                        toastr.warning('File already exists!');
                    }
                };
                reader.readAsDataURL(value);
            } else {
                toastr.warning('Please select file with .xlsx | .pdf | .docx | .txt | .xml extension only!');
            }
        }
    });
});

$('#saveLibraryFiles').on('click',function(){
    var length = $('#file_library_uploads > div').length;
    if(length > 0) {
        var data = convertSerializeDatatoArray();
        var uploadform = new FormData();
        var property_id = ($("#property_editunique_id").val() !== undefined)?$("#property_editunique_id").val():property_unique_id;
        uploadform.append('class', 'propertyFilelibrary');
        uploadform.append('action', 'file_library');
        uploadform.append('property_id', property_id);
        var count = file_library.length;
        $.each(file_library, function (key, value) {
             if(compareArray(value,data) == 'true'){
                 uploadform.append(key, value);
             }
            if(key+1 === count){
                saveLibraryFiles(uploadform);
            }
        });
    } else {

    }
});

$(document).on('click','.delete_pro_img',function(){
    toastr.success('The record deleted successfully.');
    var deleted_img = $(this).parent().parent().parent('.row').find('.show-library-list-imgs-name').text();
    $(this).parent().parent().parent('.row').remove();
    imgArray = jQuery.grep(imgArray, function(value) {
        return value != deleted_img;
    });
});

$(document).on('click','#remove_library_file',function(){
    bootbox.confirm("Do you want to remove all files?", function (result) {
        if (result == true) {
            toastr.success('The record deleted successfully.');
            $('#file_library_uploads').html('');
            $('#file_library').val('');
            $('#remove_library_file').attr('disabled',true);
            $('#file_library_uploads').attr('disabled',true);
            imgArray = [];
        }
    });

});

function saveLibraryFiles(uploadform){
    $.ajax({
        type: 'post',
        url: '/property/file_library',
        data:uploadform,
        processData: false,
        contentType: false,
        success: function (response) {
            var response = JSON.parse(response);
            if(response.code == 200){

                $('#file_library_uploads').html('');
                $('#propertFileLibrary-table').trigger('reloadGrid');
                toastr.success('Files uploaded successfully.');
            } else if(response.code == 500){

                toastr.warning(response.message);
            } else {

                toastr.success('Error while uploading files.');
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function convertSerializeDatatoArray(){
    var newData = [];
    $(".fileLibraryInput").each(function( index ) {
        var name = $(this).val();
        var size = $(this).attr('data_id');
        newData.push({'name':name,'size':size});
    });
    return newData;
}

function compareArray(data,compare){
    for(var i =0;i < compare.length;i++){
        if(compare[i].name == data['name'] && compare[i].size == data['size']){
            return 'true';
        }
    }
    return 'false';
}

function isa_convert_bytes_to_specified(bytes, to) {
    var formulas =[];
    formulas['k']= (bytes / 1024).toFixed(1);
    formulas['M']= (bytes / 1048576).toFixed(1);
    formulas['G']= (bytes / 1073741824).toFixed(1);
    return formulas[to];
}

/**
 * jqGrid Intialization function
 * @param status
 */

function jqGridFileLibrary(status) {
    var property_id = ($("#property_editunique_id").val() !== undefined)?$("#property_editunique_id").val():property_unique_id;
    var table = 'property_file_uploads';
    var columns = ['Name','Preview','Location','Action'];
    var select_column = ['Email','Delete'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'file_type',value:'2',condition:'='},{column:'property_id',value:property_id,condition:'='}];
    var columns_options = [
        {name:'Name',index:'file_name',width:400,align:"left",searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'file_extension',width:450,align:"left",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
        {name:'Location',index:'file_location',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,hidden:true},
        {name:'Action',index:'select',width:430,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, edittype: 'select',search:false,table:table,title:false,formatter:fileFormatter}
    ];
    var ignore_array = [];
    jQuery("#propertFileLibrary-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Property Document",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}


/**
 * function to format document
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function imageFormatter(cellvalue, options, rowObject){

    if(rowObject !== undefined) {
        var path = upload_url+'company/'+rowObject.Location;
        var src = '';
        if (rowObject.Preview == 'xlsx') {
            src = upload_url + 'company/images/excel.png';
        } else if (rowObject.Preview == 'pdf') {
            src = upload_url + 'company/images/pdf.png';
        } else if (rowObject.Preview == 'docx') {
            src = upload_url + 'company/images/word_doc_icon.jpg';
        }else if (rowObject.Preview == 'txt') {
            src = upload_url + 'company/images/notepad.jpg';
        }
        return '<img class="img-upload-tab open_file_location" data-location="'+path+'" width=30 height=30 src="' + src + '">';
    }
}

$(document).on('click',".open_file_location",function(){
    var location =    $(this).attr("data-location");
    window.open(location, '_blank');
});

$(document).on('change', '#propertFileLibrary-table .file_select_options', function() {
    var opt = $(this).val();
    var id = $(this).attr('data-id');
    var url=$(this).attr('data-path');
    var src=$(this).attr('data-src');
    var row_num = $(this).parent().parent().index() ;
    if (opt == 'Send' || opt == 'Send') {
        console.log('url',src)

        file_upload_email('users','email',id,1,url,src);

    } else if (opt == 'Delete' || opt == 'Delete') {

        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/property/file_library',
                        data: {class: 'propertyFilelibrary', action: "deleteFile", id: id},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                }
                $('#propertFileLibrary-table').trigger('reloadGrid');
            }
        });
    }
});

function fileFormatter(cellValue, options, rowObject)
{

    if(rowObject !== undefined) {
        var file_type =  rowObject.View;
        var location = rowObject.Location;
        var path = upload_url+'company/'+location;

        var imageData = '';
        var src = '';
        if(file_type == '1'){

            imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
        } else {
            if (rowObject.Preview == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            } else if (rowObject.Preview == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            } else if (rowObject.Preview == 'docx' || rowObject.Preview == 'doc') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            }else if (rowObject.Preview == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            }
        }


        var html = "<select editable='1' class='form-control file_select_options' data-id='"+rowObject.id+"' data-path = '"+path+"' data-src= '"+src+"'>"

        html +=  "<option value=''>Select</option>";
        html +=  "<option value='Delete'>Delete</option>";
        html +=  "<option value='Send'>SEND</option>";

        html +="</select>";







        return html;


        return imageData;
    }
}