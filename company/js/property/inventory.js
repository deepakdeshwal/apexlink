$(document).on('click','#addInventoryDiv',function(){
    console.log(upload_url);
    $('#inventoryDiv').show();
    $('#propertyInventory').trigger('reset');
    $('#image_id1').html('<img id="inventory_img1" src="'+upload_url+'company/images/dummy-img.jpg">');
    $('#image_id2').html('<img id="inventory_img2" src="'+upload_url+'company/images/dummy-img.jpg">');
    $('#image_id3').html('<img id="inventory_img3" src="'+upload_url+'company/images/dummy-img.jpg">');
    triggerDatePicker();
    jQuery('.phone_format_inventory').mask('000-000-0000', {reverse: true});
    $('#inventory-table').trigger('reloadGrid');
    $('#propertyInventoryId').val('');
});


/*
To cropit image change
 */
$(document).on("change",".cropit-image-input",function(){
    photo_videos = [];
    var fileData = this.files[0];
    var type= fileData.type;
    var elem = $(this);
    // $.each(this.files, function (key, value) {
        var type = fileData['type'];
        var size = isa_convert_bytes_to_specified(fileData['size'], 'k');
        var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
        var uploadType = 'image';
        var validSize = '1030';
        var arrayType = validImageTypes;

        if ($.inArray(type, arrayType) < 0) {
            toastr.warning('Please select file with valid extension only!');
        } else {

            if (parseInt(size) > validSize) {
                var validMb =  1;
                toastr.warning('Please select documents less than '+validMb+' mb!');
            } else {
                size = isa_convert_bytes_to_specified(fileData['size'], 'k') + 'kb';
                photo_videos.push(fileData);
                var src = '';
                var reader = new FileReader();
                $('#photo_video_uploads').html('');
                $(".popup-bg").show();
                elem.next().show();
            }
        }
   // });
});

$(document).ready(function() {
    var cropper = $('.image-editor').cropit({
        imageState: {
            src: subdomain_url+'500/400',
        },
    });
    // Handle rotation
    $(document).on("click",'.rotate-ccw',function(){
        $('.image-editor').cropit('rotateCCW');
    });

    $(document).on("click",'.rotate-cw',function(){
        $('.image-editor').cropit('rotateCW');
    });


    $(document).on("click", '.export', function () {
        $(this).parents('.cropItData').hide();
        $(this).parent().prev().val('');
        var dataVal = $(this).attr("data-val");
        var imageData = $(this).parents('.image-editor').cropit('export');
        var image = new Image();
        image.src = imageData;
        $(".popup-bg").hide();
        $('#'+dataVal).attr('src',imageData);
    });
});
getSublocation();
function getSublocation(){
    $.ajax({
        url:'/Property-inventory-ajax',
        type: 'POST',
        data: {
            "action": 'getSublocation',
            "class": 'PropertyInventory'
        },
        success: function (response) {
            console.log(response);
            var response = JSON.parse(response);
            if(response.status=='success') {
                var html = '<option value="0">Select</option>';
                $.each(response.data, function (key, value) {
                    html += "<option value='"+value.id+"'>"+value.sublocation+"</option>";
                });
                $('#sublocation').html(html);
            }
        }
    });
}



$(document).on('change','#selectType',function(){
   var selectValue = $('#selectType option:selected').val();
   console.log("selected",selectValue);
   if (selectValue == '0'){
      $('.building_div_select').hide();
      $('.unit_div_select').hide();
   } else if(selectValue == '1'){
       $('.building_div_select').show();
       $('.unit_div_select').hide();
   } else if(selectValue == '2'){
       $('.building_div_select').show();
       $('.unit_div_select').show();
   }
});

getBuilding();
function getBuilding(){
    $.ajax({
        url:'/Property-inventory-ajax',
        type: 'POST',
        data: {
            "action": 'getBuilding',
            "class": 'PropertyInventory',
            "property_id": $('#property_editunique_id').val()
        },
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status=='success') {
                var html = '<option value="0">Select</option>';
                $.each(response.data, function (key, value) {
                    html += "<option value='"+value.id+"'>"+value.building_name+"</option>";
                });
                $('#selectBuilding').html(html);
            }
        }
    });
}

function getUnit(building_id){
    $.ajax({
        url:'/Property-inventory-ajax',
        type: 'POST',
        data: {
            "action": 'getUnit',
            "class": 'PropertyInventory',
            "building_id": building_id
        },
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status=='success') {
                var html = '<option value="0">Select</option>';
                $.each(response.data, function (key, value) {
                    html += "<option value='"+value.id+"'>"+value.unit_no+"</option>";
                });
                $('#selectUnit').html(html);
            }
        }
    });
}

$(document).on('change','#selectBuilding',function(){
    var dataValue = $('#selectBuilding option:selected').val();
    getUnit(dataValue);
});

getCategory();
function getCategory(){
    $.ajax({
        url:'/Property-inventory-ajax',
        type: 'POST',
        data: {
            "action": 'getCategory',
            "class": 'PropertyInventory'
        },
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status=='success') {
                var html = '<option value="0">Select</option>';
                $.each(response.data, function (key, value) {
                    html += "<option value='"+value.id+"'>"+value.category_name+"</option>";
                });
                $('#inventoryCategory').html(html);
            }
        }
    });
}

$(document).on('change','#inventoryCategory',function(){
    var dataValue = $('#inventoryCategory option:selected').val();
    getSubCategory(dataValue);
});

function getSubCategory(category_id){
    $.ajax({
        url:'/Property-inventory-ajax',
        type: 'POST',
        data: {
            "action": 'getCategory',
            "class": 'PropertyInventory',
            "category_id":category_id
        },
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status=='success') {
                var html = '<option value="0">Select</option>';
                $.each(response.data, function (key, value) {
                    html += "<option value='"+value.id+"'>"+value.subcategory+"</option>";
                });
                $('#inventorySubcategory').html(html);
            }
        }
    });
}
triggerDatePicker();
function triggerDatePicker() {
    $("#warranty_from").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
    $("#warranty_to").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
    $("#installation_date").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
    $("#replacement_date").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
}



$('#propertyInventory').on('submit',function(e){
    e.preventDefault();
    var img1 = $('.inventory_img1').html();
    var img2 = $('.inventory_img2').html();
    var img3 = $('.inventory_img3').html();
    var inventory_image1 = JSON.stringify(img1);
    var inventory_image2 = JSON.stringify(img2);
    var inventory_image3 = JSON.stringify(img3);
    var photo_array = [$(img1).attr('src'),$(img2).attr('src'),$(img3).attr('src')];
    var pic_data = 'no';
    $.each(photo_array,function(key,value){
        console.log('value',value.indexOf("http://"));
        if(value.indexOf("http://") != 0){
            pic_data = 'yes';
            return;
        }
    });
    var form = $('#propertyInventory')[0];
    var formData = new FormData(form);
    formData.append('action', 'addInventory');
    formData.append('class', 'PropertyInventory');
    formData.append('inventory_image1', inventory_image1);
    formData.append('inventory_image2', inventory_image2);
    formData.append('inventory_image3', inventory_image3);
    formData.append('property_id', $('#property_editunique_id').val());
    formData.append('pic_data', pic_data);
    $.ajax({
        url: '/Property-inventory-ajax',
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                toastr.success(response.message)
                $('#inventoryDiv').hide();
                $('#propertyInventory').trigger('reset');
                $('#propertyInventoryId').val('');
                setTimeout(function(){
                    jQuery('#inventory-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('#inventory-table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                }, 500);
                $('#inventory-table').trigger('reloadGrid');
            } else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
        }

    });
});


/**
 * jqGrid Initialization function
 * @param status
 */
jqGridInventory('all');
function jqGridInventory(status) {
    var table = 'property_inventory';
    var columns = [' ','Building','Unit', 'Warranty Date', 'Warranty_From', 'Photo_Status', 'Guarrantee', 'Replacement Date', 'Inventory Image/Photo','Action'];
    var select_column = ['Edit','Delete'];
    var joins = [{table: 'property_inventory', column: 'building_id', primary: 'id', on_table: 'building_detail'},{table: 'property_inventory', column: 'unit_id', primary: 'id', on_table: 'unit_details'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = [];
    var extra_where = [{column:'property_id',value:$('#property_editunique_id').val(),condition:'='}];
    var columns_options = [
        {name: ' ', index: 'id', align: "center", editoptions: {value: "True:False"}, editrules: {required: true},
            formatter: function (cellvalue, options, rowObject) {
                return '<input type="checkbox" class="checkboxgrid propertyInventoryCheckbox"  value=' + cellvalue + ' >';
            },
            formatoptions: {disabled: false}, editable: true, searchoptions: {sopt: conditions}, table: table,search:false},
        {name: 'Building' ,index: 'building_name', align: "left", searchoptions: {sopt: conditions}, table: 'building_detail', classes: 'pointer'},
        {name: 'Unit', index: 'unit_no', searchoptions: {sopt: conditions}, table: 'unit_details', classes: 'pointer'},
        {name: 'Warranty Date',width:'200', index: 'warranty_to', align: "center", searchoptions: {sopt: conditions}, table: table, change_type:'date',classes: 'pointer',formatter:warrantyFormatter},
        {name: 'Warranty_From', index: 'warranty_from',hidden:true,align: "center", searchoptions: {sopt: conditions}, table: table, change_type:'date',classes: 'pointer'},
        {name: 'Photo_Status', index: 'photo_uploaded',hidden:true,align: "center", searchoptions: {sopt: conditions}, table: table,classes: 'pointer'},
        {name: 'Guarrantee', index: 'guarantee', align: "center", searchoptions: {sopt: conditions}, table: table,classes: 'pointer',formatter:guaranteeFormatter},
        {name: 'Replacement Date', index: 'replacement_date', align: "center", searchoptions: {sopt: conditions}, table: table, change_type:'date',classes: 'pointer'},
        {name: 'Inventory Image/Photo',width:'160', index: 'id', align: "center", searchoptions: {sopt: conditions}, table: table,search: false, classes: 'pointer',formatter:photoFormatter},
        {name: 'Action', index: 'select', title: false, align: "right", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table}
    ];
    var ignore_array = [];
    jQuery("#inventory-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        width: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'property_inventory.updated_at',
        sortorder: 'desc',
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Inventories",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 400, drag: true, resize: false} // search options
    );
}

$(document).on('change', '#inventory-table .select_options', function() {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    var row_num = $(this).parent().parent().index();
    console.log(row_num);
    if (opt == 'Edit' || opt == 'EDIT') {
        $.ajax({
            type: 'post',
            url: '/Property-inventory-ajax',
            data: {class: 'PropertyInventory', action: "getInventory", id: id},
            success: function (response) {
                var response = JSON.parse(response);
                console.log(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#addInventoryDiv').trigger('click');
                    $('#propertyInventoryId').val(response.data.id);
                    $.each(response.data,function(key,value){
                        if(key != 'select_type' && key != 'category' && key != 'sub_category' && key != 'inventory_img1' && key != 'inventory_img2' && key != 'inventory_img3'){
                            if(value !== null) {
                                $('#propertyInventory [name=' + key + ']').val(value);
                            }
                        }
                    });
                    if(response.data.select_type !== null) {
                        $('#selectType').val(response.data.select_type);
                        $('#selectType').trigger('change');
                    }
                    if(response.data.building_id !== null) {
                        $('#propertyInventory [name="building_id"]').val(response.data.building_id);
                        $('#propertyInventory [name="building_id"]').trigger('change');
                    }
                    if(response.data.unit_id !== null) {
                        setTimeout(function () {
                            $('#propertyInventory [name="unit_id"]').val(response.data.unit_id);
                        }, 1000);
                    }
                    $('#image_id1').html(response.data.inventory_img1);
                    $('#image_id2').html(response.data.inventory_img2);
                    $('#image_id3').html(response.data.inventory_img3);
                    if(response.data.category !== null) {
                        $('#propertyInventory [name="category"]').val(response.data.category);
                        $('#propertyInventory [name="category"]').trigger('change');
                        setTimeout(function () {
                            $('#propertyInventory [name="sub_category"]').val(response.data.sub_category);
                        },2000);
                    }
                    $('#inventory-table').trigger('reloadGrid');
                    setTimeout(function(){
                            jQuery('#inventory-table').find('tr:eq(row_num)').find('td:eq(0)').addClass("green_row_left");
                            jQuery('#inventory-table').find('tr:eq(row_num)').find('td').last().addClass("green_row_right");
                    }, 2000);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/Property-inventory-ajax',
                        data: {class: 'PropertyInventory', action: "deleteInventory", id: id},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                $('#inventory-table').trigger('reloadGrid');
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                }
            }
        });
    }
});

/**
 * function to format guarantee table
 * @param cellvalue
 * @param options
 * @param rowObject
 */
function guaranteeFormatter(cellvalue, options, rowObject){
    if(rowObject !== undefined){
        if(cellvalue != 'Lifetime' && cellvalue != ''){
            return cellvalue+' Years';
        } else {
            return cellvalue;
        }
    }
}

/**
 *
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {*}
 */
function warrantyFormatter(cellvalue, options, rowObject){
    if(rowObject !== undefined){
        return rowObject.Warranty_From+' To '+cellvalue;
    }
}
/**
 *
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {*}
 */
function photoFormatter(cellvalue, options, rowObject){
    if(rowObject !== undefined){
        if(rowObject.Photo_Status == '1') {
            return '<span data_id="' + rowObject.id + '" style="color: #05A0E4;text-decoration: underline;cursor: pointer;font-size: 13px;font-weight: bold;">Inventory Images/Photo</span>';
        } else {
            return '<span data_id="' + rowObject.id + '" style="color: Red;cursor: pointer;font-size: 13px;font-weight: bold;">No Images Uploaded</span>';
        }
    }
}

/** Email Functionality **/
/** print Functionality Start**/
$(document).on("click","#inventory_preview",function(){
    var user_array = [];
    $('.propertyInventoryCheckbox').each(function(key,value){
        if($(value).prop('checked') === true){
            user_array.push($(value).val());
        }
    });
    if(user_array.length === 0){
        bootbox.alert('Please select record from listing.');
    } else {
        $.ajax({
            type: 'post',
            url: '/Property-inventory-ajax',
            data: {
                class: "PropertyInventory",
                action: "getPropertyInventoryPrintData",
                data: user_array,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    // update other modal elements here too
                    $('#inventory_content').html(response.data);
                    // show modal
                    $('#inventoryPrintModal').modal('show');
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});

/*
Function to print envelope
*/
function PrintElem(elem)
{
    Popup($(elem).html());
}

function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
        'font-weight:bold;} .right-detail{\n' +
        '        position:relative;\n' +
        '        left:+400px;\n' +
        '    }</style>');
    $(mywindow.document.body).html( '<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if(mywindow.close()){

    }
    $("#PrintEnvelope").modal('hide');
    return true;
}
/** print Functionality End**/

