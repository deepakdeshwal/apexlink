$(document).ready(function () {
    $('#clone_div_icon').on('click',function(){
       $(this).addClass('active');
    });
    var ownerId = '';
    getAllOwners();
//combo grid for owner_pre_vendor

    $(document).on('click','#_easyui_textbox_input1',function(){
        $('.all_properties').combogrid('showPanel');
    })

    $('.all_properties').combogrid({
        placeholder: "Select...",
        panelWidth: 500,
        url: '/property-ajax',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getPropertiesdata',
            class: 'propertyDetail'
        },
        columns: [[
            {field: 'id', hidden: true},
            {field: 'name', title: 'Property', width: 60},
            {field: 'address1', title: 'Address', width: 120},
        ]],
        onSelect: function (index, row) {
            $("#property_clone_id").val("yes");
            $("#property_clone_status").val(row.id);
            $.ajax({
                type: 'post',
                url: '/property-ajax',
                data: {
                    class: 'propertyDetail',
                    action: 'getCloneproperty',
                    id:row.id,
                },
                success: function (response) {
                    getAllOwners();
                    var response = JSON.parse(response);
                    $.each(response.data, function (key, value) {
                        $('#' + key).val(value);
                        if(key == 'property_type'){
                            $("#property_type_options").val(value);
                        }
                        if(key == 'property_type'){
                            $("#property_style_options").val(value);
                        }
                        if(key == 'property_subtype'){
                            $("#property_subtype_options").val(value);
                        }
                        if(key == 'pet_friendly'){
                            $("#pet_options").val(value);
                        }
                        if(key == 'property_name'){
                            $("#property_name").val(value+'_copy');
                        }
                        if(key == 'legal_name'){
                            $("#legal_name").val(value+'_copy');
                        }
                        if(key == 'property_for_sale'){
                            $("#property_for_sale").val(value);
                        }
                        if(key == 'online_listing'){
                            $("#chkOnlineListing").prop('checked', true);
                        }
                        if(key == 'online_application'){
                            $("#chkOnlineApplication").prop('checked', true);
                        }
                        if(key == 'is_property_with_no_building'){
                            $(".details").show();
                            $("#default_building_unit").prop('checked', true);
                        }
                        if(key == 'disable_sync'){
                            $("#chkDisableSyndication").prop('checked', true);
                        }
                        if(key == 'school_district_municipalityID'){
                            $(".hidden_school_fields").show();
                            $.each(response.school_district_municipalityID, function (keysch, valuesch) {
                                $("#school_district_municipalityID").val(valuesch);
                            });
                        }
                        if(key == 'key_options'){
                            $(".key_access_codeclass").show();
                            $.each(response.key_options, function (keysoption, valuesoption) {
                                $("#key_options").val(valuesoption);
                            });
                        }
                        if(key == 'school_district_code'){
                            $.each(response.school_district_code, function (keyscode, valuescode) {
                                $("#school_district_code").val(valuescode);
                            });
                        }
                        if(key == 'school_district_notes') {
                            $.each(response.school_district_notes, function (keysnotes, valuesnotes) {
                                $("#school_district_notes").val(valuesnotes);
                            });
                        }
                        if(key == 'key_access_code') {
                            $.each(response.key_access_code_desc, function (keyaccesscode, valueacessscode) {
                                $("#key_access_code").val(valueacessscode);
                            });
                        }
                        if(key == 'amenities') {
                            $.each(response.amenties, function (key1, value1) {
                                $(":checkbox[value='" + value1 + "']").prop("checked", "true");
                            });
                        }
                        if(key == 'property_tax_id'){
                            $(".clonepropertyTaxhidden").html('');
                            var propertytax='';
                            $.each(response.property_tax_ids, function (keytax, valuetax) {
                                propertytax +='<div class="col-xs-12 col-sm-4 col-md-3 multiplepropertydiv" id="multiplepropertydivId">';
                                propertytax +='<label>Property Tax ID </label>';
                                propertytax +='<input  class="form-control add-input capital" type="text"  maxlength="20" name="property_tax_ids" placeholder="Property Tax" value="'+valuetax+'"/>';
                                propertytax +='<span class="property_tax_idsErr error"></span>';
                               if(keytax == 0){
                                   propertytax +='<a id= \'multiplepropertytax\' class="add-icon" href="javascript:;">';
                                   propertytax +='<i class="fa fa-plus-circle" aria-hidden="true"></i></a>';
                                   propertytax +='<a style=\'display: none;\' id= \'multiplepropertytaxcross\' class="add-icon" href="javascript:;">';
                                   propertytax +='<i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                               }else{
                                   propertytax +='<a id=\'multiplepropertytaxcross\' class="add-icon"  href="javascript:;">';
                                   propertytax +='<i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                               }
                                propertytax +='</div>';
                            });
                            $(".clonepropertyTaxhidden").html(propertytax);
                        }
                        if(key == 'phone_number'){
                            $(".clonemultiplephonediv").html('');
                            var  phonehtml = ''
                            $.each(response.phone_number, function (keyphone, valuephone) {
                                 phonehtml +='  <div class="col-xs-12 col-sm-4 col-md-3  multiplephonediv" id="multiplephoneIDD"><label>Phone Number</label>';
                                 phonehtml +='<input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control add-input phone_number_general" name="phone_number" value="'+valuephone+'" autocomplete="off">';

                                if(keyphone == 0){
                                    phonehtml +='<a id=\'multiplephoneno\' class="add-icon" href="javascript:;">';
                                    phonehtml +='<i class="fa fa-plus-circle" aria-hidden="true"></i></a>';
                                    phonehtml +='<a id=\'multiplephonenocross\' class="add-icon" style=\'display: none;\' href="javascript:;">';
                                    phonehtml +='<i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                                }else{
                                    phonehtml +='<a id=\'multiplephonenocross\' class="add-icon"  href="javascript:;">';
                                    phonehtml +='<i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                                }
                                phonehtml +='</div>';
                            });
                            $(".clonemultiplephonediv").html(phonehtml);

                        }
                        if(key =='garage_options'){
                            if (value == "1") {
                                $('.garageDesc').hide();
                                $('.garageVehicle').hide();

                            } else if (value == "2") {
                                $('.garageDesc').hide();
                                $('.garageVehicle').show();
                                $('.garage_veh').val('');
                            }
                        }
                        if(key == 'owner_percentownedcopy'){
                            if(value != null) {
                                var count = response.owner_percentowned_copy;
                                $.each(response.owner_percentowned_copy, function (keyowner, valueowner) {
                                    if (keyowner % 2 != 0) {
                                        if(keyowner == 1){
                                            $("#add_owners_div").find('.percentage_owned').val(valueowner);
                                        }
                                        if(keyowner != 1) {
                                            var newnode = $("#add_owners_div").clone().insertAfter("div.owners_div:last");
                                            $(".grey-box-add-inner:not(:eq(0))  #multiplegreybox").remove();
                                            $(".grey-box-add-inner:not(:eq(0))  #multiplegreycross").show();
                                            newnode.find('.percentage_owned').next().html('');
                                            newnode.find('.percentage_owned').val(valueowner);
                                        }
                                    }
                                });

                               // var ownerhtml = '';
                                // $.each(response.owner_percentowned, function (keyowner, valueowner) {
                                //     if (keyowner % 2 != 0) { // index is even
                                //
                                //         ownerhtml += '<div id="add_owners_div" class="grey-box-add owners_div"><div class="grey-box-add-inner"><div class="grey-box-add-lt"><div>';
                                //         ownerhtml += '<div class="col-xs-12 col-sm-4 col-md-3"><label>Owner/Landlord</label><select id="ownerSelectid" name="owner_percentowned" class="form-control ownerSelectclass" ></select></div>';
                                //         ownerhtml += '<div class="col-xs-12 col-sm-4 col-md-3"><label>Percent Owned (%) </label><input value ="' + valueowner + '" type="text" name="owner_percentowned"  max="100" placeholder="Eg:50" value="100" class="form-control number_only percentage_owned"></div>';
                                //         ownerhtml += ' </div></div><div class="grey-box-add-rt">';
                                //         if (keyowner == 1) {
                                //             ownerhtml += '<a id="multiplegreybox" class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a><a style="display:none" id="multiplegreycross" style="display: none;" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                                //         } else {
                                //             ownerhtml += '<a  id="multiplegreycross"  href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                                //         }
                                //         ownerhtml += '</div></div></div>';
                                //     }
                                // });
                                // $(".cloneOwners_div").html(ownerhtml);
                            }
                        }
                        if(key == 'owner_id'){
                            ownerId = response.owner_id;
                        }
                        if(key =='management_start_date'){
                            $("#management_start_date").val(response.management_start_date);
                        }
                        if(key =='management_end_date'){
                            $("#management_end_date").val(response.management_end_date);
                        }
                        if(key =='InsuranceExpiration') {
                            $("#InsuranceExpiration").val(response.InsuranceExpiration);
                        }
                        if(key =='HomeWarrantyExpiration') {
                            $("#HomeWarrantyExpiration").val(response.HomeWarrantyExpiration);
                        }
                        if(key =='mortgage_start_date') {
                            $("#mortgage_start_date").val(response.mortgage_start_date);
                        }
                        if(key == 'property_idrandom'){
                            $(".property_idrandom").val(value);
                        }
                        if(key == 'property_description'){
                            $(".property_description").val(value);
                        }
                        if(key == 'manager_id'){
                            $.each(response.manager_id, function (keymanager, valuemanager) {
                                var element = $("#select_mangers_options");
                                element.multiselect('select', valuemanager);
                            });

                        }
                        if(key == 'attach_groups'){
                            $.each(response.attach_groups, function (keygrp, valuegrp) {
                                var element = $("#select_group_options");
                                element.multiselect('select', valuegrp);
                            });

                        }
                        if(key == 'grace_period'){
                            $('#grace_period').attr('disabled', false);
                        }
                        if(key == 'one_time_flat_fee_desc'){
                            if(value == null){
                            $('#one_time_flat_fee_desc').attr('disabled', true);
                        }else{
                                $('#one_time_flat_fee_desc').attr('disabled', false);
                            }
                        }
                        if(key == 'one_time_monthly_fee_desc'){
                            if(value == null){
                                $('#one_time_monthly_fee_desc').attr('disabled', true);
                            }else {
                                $('#one_time_monthly_fee_desc').attr('disabled', false);
                            }
                        }
                        if(key == 'daily_flat_fee_desc'){
                            if(value == null){
                                $('#daily_flat_fee').attr('disabled', true);
                            }else {
                                $('#daily_flat_fee').attr('disabled', false);
                            }
                        }
                        if(key == 'daily_monthly_fee_desc'){
                            if(value == null){
                                $('#daily_flat_fee_desc').attr('disabled', true);
                            }else {
                                $('#daily_flat_fee_desc').attr('disabled', false);
                            }
                        }
                        if(key == 'applycheckbox'){
                            if(value == 1){
                                $("#apply_one_time_id").prop('checked', true);
                                $("#apply_daily_id").prop('checked', false);
                            }else if(value == 2){
                                $("#apply_daily_id").prop('checked', true);
                                $("#apply_one_time_id").prop('checked', false);
                            }
                        }
                    });
                },
                complete: function (data) {
                    var response = JSON.parse(data.responseText);
                    setTimeout(function () {
                        if(response.owner_id_copy != ''){
                            $('.ownerSelectclass').each(function(i, obj) {
                                console.log('response console',response.owner_id_copy[i]);
                                $(this).val(response.owner_id_copy[i]);
                            });
                        }
                    }, 1000);

                    if(response.vendor_1099_payer != ''){
                        $('#vendor_1099_payer').val(response.vendor_1099_payer);
                    }

                }
            });

        }
    });
    $("#_easyui_textbox_input1").attr("placeholder", "Click here to pick a Property");
    $("#_easyui_textbox_input1").addClass("form-control");
    $("#_easyui_textbox_input1").css("width",'100%');
    $("#_easyui_textbox_input1").css("height",'34px');
    $(document).on('click', '#clone_icon', function () {
        $(".all_propertiesDiv").show();
    });

});
function getAllOwners() {
    var property_id = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-owners',
        data: {
            class: 'propertyDetail',
            action: 'fetchOwners',
            property_id: property_id,
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('.ownerSelectclass').html(res.data);
        },
    });
}

