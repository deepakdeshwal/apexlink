$(document).on('mouseover','.tooltipgridclass',function(){
    $(this).closest('td').css("overflow", "unset");
});
$(document).on('mouseout','.tooltipgridclass',function(){
    $(this).closest('td').css("overflow", "hidden");
});

$(document).on('click','.add_new_property',function(){
    var checkPermissionData = checkPermissions(permissions,module,'Add Property');
    if(checkPermissionData == 'Access Denied') {
        return false;
    }
    window.location.href = '/Property/AddProperty';
});


$(document).ready(function () {
    createXml();
    //jqGrid status
    $('#jqGridStatus').on('change', function () {
        var selected = this.value;
        var grid = $("#property-table"),f = [];
        if(selected == '8'){
            grid[0].p.search = true;
            selected = 'all';
            var joins = [{table: 'general_property', column: 'property_type', primary: 'id', on_table: 'company_property_type'}];
            f.push({field: "general_property.is_short_term_rental", op: "eq", data: '1',int:'true'});
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:selected,joins:joins});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
        } else if(selected == '5'){
            var joins = [{table: 'general_property', column: 'property_type', primary: 'id', on_table: 'company_property_type'},{table: 'general_property', column: 'id', primary: 'property_id', on_table: 'tenant_property',type:'JOIN'}];
            grid[0].p.search = false;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all',joins:joins});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
        }  else if(selected == '7'){
            grid[0].p.search = true;
            selected = 'all';
            var joins = [{table: 'general_property', column: 'property_type', primary: 'id', on_table: 'company_property_type'}];
            f.push({field: "general_property.vacant", op: "eq", data: '0'});
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:selected,joins:joins});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
        } else {
            grid[0].p.search = false;
            var joins = [{table: 'general_property', column: 'property_type', primary: 'id', on_table: 'company_property_type'}];
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:selected,joins:joins});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
        }
    });

    $(document).on('change', '#property-table .select_options', function (e) {
        e.preventDefault();
        setTimeout(function(){ $(".select_options").val("default"); }, 200);
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        //New code start
        switch (opt) {
            case "Edit":
                var checkPermissionData = checkPermissions(permissions,module,'Edit Property');
                if(checkPermissionData == 'Access Denied') {
                    $('.select_options').prop('selectedIndex', 0);
                    return false;
                }
                window.location.href = "/Property/EditProperty?id=" + id;
                break;
            case "Active Tenants":
                localStorage.setItem('type','regular');
                window.location.href = "/Tenantlisting/ActiveTenant?id=" + id;
                break;
            case "Active Guests":
                localStorage.setItem('type','short');
                window.location.href = "/Tenantlisting/ActiveTenant?id=" + id;
                // window.location.href = "/GuestCard/ListGuestCard";
                break;
            case "Book Now":
                $("#shortTermRental").modal(show);
                //window.location.href = "/GuestCard/ListGuestCard";
                break;
            case "Tenant Living":
                localStorage.setItem('property_id',id);
                window.location.href = "/Tenantlisting/Tenantlisting";
                break;
            case "Add New Tenant":
                localStorage.setItem('property_id',id);
                localStorage.setItem('generateLease','genrateLease');
                setTimeout(function () {
                    window.location.href = "/Tenantlisting/add";
                },50);

                break;
            case "Work Order":
                localStorage.setItem('redirection_module','property_module');
                window.location.href = "/WorkOrder/AddWorkOrder?id="+id;
                break;
            case "Application":
                // window.location.href = "/WorkOrder/AddWorkOrder?id="+id;
                window.location.href = "/RentalApplication/RentalApplication?id="+id;
                break;
            case "Vacancy":
                window.location.href = "/Unit/UnitModule?id=" + id;
                break;
            case "Unit Directory":
                window.location.href = "/Unit/UnitModule?id=" + id;
                break;
            case "Inspection":
                localStorage.setItem("inspection_value", "add_inspection");
                window.location.href = "/Property/PropertyInspection?id=" + id;
                break;
            case "General Ledger":
                window.location.href = "/Property/GeneralLedger?id=" + id;
                break;
            case "File Library":
                localStorage.setItem("scrollDown",'#propertyFileLibrary');
                window.location.href = "/Property/PropertyView?id=" + id;
                break;
            case "Add Inventory":
                localStorage.setItem("AccordionHref",'#collapseTwenty12');
                window.location.href = "/Property/EditProperty?id=" + id;
                break;
            case "Waiting List":
                localStorage.setItem("scrollDown",'#propertyWatingList');
                window.location.href = "/Property/PropertyView?id=" + id;
                break;
            case "FlagBank":
                localStorage.setItem("scrollDown",'#propertyFlag');
                window.location.href = "/Property/PropertyView?id=" + id;
                break;
            case "Add In-Touch":
                window.location.href = "/Communication/NewInTouch?tid="+id+"&category=Property";
                break;
            case "Notes & History":
                localStorage.setItem("scrollDown",'#notesHistory');
                window.location.href = "/Property/PropertyView?id=" + id;
                break;
            case "In Touch History":
                var propertyname=$('#'+id).find('td:first').text();
                localStorage.setItem("propertyname",propertyname);
                window.location.href = "/Communication/InTouch";
                break;
            case "Archive Property":
                bootbox.confirm({
                    message: "Do you want to Archive the current record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/property-ajax',
                                data: {class: 'propertyDetail', action: "archiveProperty", id: id},
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                    triggerReload();
                                }
                            });
                        }
                    }
                });
                break;
            case "Resign This Property":
                bootbox.confirm({
                    message: "Do you want to Resign the current record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/property-ajax',
                                data: {class: 'propertyDetail', action: "resignProperty", id: id},
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                    triggerReload();
                                }
                            });
                        }
                    }
                });
                break;
            case "Print Envelope":
                $.ajax({
                    type: 'post',
                    url: '/property-ajax',
                    data: {class: 'propertyDetail', action: 'getCompanyData','building_id':id},
                    success : function(response){
                        var response =  JSON.parse(response);
                        if(response.status == 'success' && response.code == 200) {
                            $("#PrintEnvelope").modal('show');
                            $("#company_name").text(response.data.data.company_name)
                            $("#address1").text(response.data.data.address1)
                            $("#address2").text(response.data.data.address2)
                            $("#address3").text(response.data.data.address3)
                            $("#address4").text(response.data.data.address4)
                            $(".city").text(response.data.data.city)
                            $(".state").text(response.data.data.state)
                            $(".postal_code").text(response.data.data.zipcode)
                            $("#building_name").text(response.building.data.building_name)
                            $("#building_address").text(response.building.data.address)
                        }else {
                            toastr.warning('Record not updated due to technical issue.');
                        }
                    }
                });
                break;
            case "Activate This Property":
                var checkPermissionData = checkPermissions(permissions,module,'Deactivate/Activate Property');
                if(checkPermissionData == 'Access Denied') {
                    $('.select_options').prop('selectedIndex', 0);
                    return false;
                }
                bootbox.confirm({
                    message: "Do you want to Activate the current record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/property-ajax',
                                data: {class: 'propertyDetail', action: "activateProperty", id: id},
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                    triggerReload();
                                }
                            });
                        }
                    }
                });
                break;
            case "Delete This Property":
                bootbox.confirm({
                    message: "Do you want to delete this record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            bootbox.confirm({
                                message: "Deleting this entry will take it out of the system completely and no longer be recovered.Do you want to continue to delete this entry?",
                                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                                callback: function (result) {
                                    if (result == true) {
                                        $.ajax({
                                            type: 'post',
                                            url: '/property-ajax',
                                            data: {class: 'propertyDetail', action: "deleteProperty", id: id},
                                            success: function (response) {
                                                var response = JSON.parse(response);
                                                if (response.status == 'success' && response.code == 200) {
                                                    toastr.success(response.message);
                                                } else {
                                                    toastr.error(response.message);
                                                }
                                                triggerReload();
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
                break;
            default:
                $('#property-table .select_options').prop('selectedIndex',0);
        }
        //New code end
    });


    /**
     * jqGrid Initialization function
     * @param status
     */



    jqGrid('all');
    function jqGrid(status) {
        if(jqgridNewOrUpdated == 'true')
        {
            var sortOrder = 'desc';
            var sortColumn = 'general_property.update_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'general_property.property_name';
        }
        var checkbox = '<input type="checkbox" id="select_all_complaint_checkbox">';
        var table = 'general_property';
        var columns = ['Rentpath','Property Name','clone_status','Property Address', 'Buildings', 'Units', 'Type', 'total_unit', 'Vacant', 'Property Manager', 'Status', 'Owner', 'Short-Term Rental', 'Action'];
        var select_column = ['Edit', 'Tenant Living', 'Add New Tenant', 'Work orders', 'Application', 'Vacancy', 'Unit Directory', 'Inspection', 'General Ledger', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'Add In-Touch', 'In TouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
        var joins = [{table: 'general_property', column: 'property_type', primary: 'id', on_table: 'company_property_type'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['general_property.deleted_at', 'general_property.address1', 'general_property.address2', 'general_property.address3', 'general_property.address4', 'general_property.manager_id', 'general_property.owner_id'];
        var extra_where = [];
        var columns_options = [
            {name:'is_xml',index:'is_xml', width:80, sortable: false, searchoptions: {sopt: conditions},search: false,table:table,formatter:actionCheckboxFmatterComplaint},
            {name: 'Property Name', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'clone_status', title:false,hidden:true ,index: 'clone_status', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Property Address', index: 'address_list', width: 100, searchoptions: {sopt: conditions}, table: table, change_type: 'combine_column_line', extra_columns: ['address1', 'address2', 'address3', 'address4'], update_column: 'address_list',original_index: 'address1', classes: 'pointer',type:'line'},
            {name: 'Buildings', index: 'building_exist', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyUnitBuilding, classes: 'pointer'},
            {name: 'Units', index: 'unit_exist', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyUnitBuilding, classes: 'pointer'},
            {name: 'Type', index: 'property_type', width: 80, align: "center", searchoptions: {sopt: conditions}, table: 'company_property_type', classes: 'pointer'},
            {name: 'total_unit', index: 'no_of_units',hidden:true, width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Vacant', index: 'id', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',change_type: 'unit_vacant_count'},
            {name: 'Property Manager', index: 'manager_list', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'serialize', secondTable: 'users', index2: 'name', update_column: 'manager_list', original_index: 'manager_id', classes: 'pointer',type:'line'},
            {name: 'Status', index: 'status', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyStatus, classes: 'pointer'},
            {name: 'Owner', index: 'owner_list', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'serialize', secondTable: 'users', index2: 'name', update_column: 'owner_list', original_index: 'owner_id', classes: 'pointer',type:'line',attr:[{name:'flag',value:'property'}]},
            {name: 'Short-Term Rental', index: 'is_short_term_rental', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyShortTermRental, classes: 'pointer'},
            {name: 'Action', index: 'select', title: false, width: 80, align: "right", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table,formatter:actionFmatter}
        ];
        var ignore_array = [];
        jQuery("#property-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Properties",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
        );
    }

    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter(cellValue, options, rowObject) {
        if (cellValue == 1)
            return "Active";
        else if (cellValue == '0')
            return "InActive";
        else
            return '';
    }

    function flagFormatter(cellValue, options, rowObject){
        if(rowObject !== undefined){
            console.log(rowObject.Action);
            var flagValue = $(rowObject.Action).attr('flag');
            var flag = '';
            if(flagValue == 'yes'){

            } else {

            }
        }
    }

    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isDefaultFormatter(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "Yes";
        else if (cellValue == '0')
            return "No";
        else
            return '';
    }

    function actionFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = ['Edit', 'Active Tenants', 'Add New Tenant', 'Vacancy', 'Unit Directory', 'Inspection','Notes & History', 'File Library', 'Waiting List', 'FlagBank', 'Add In-Touch', 'In Touch History', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            //   if(rowObject.Status == '1')  select = ['Edit', 'Tenant Living', 'Add New Tenant', 'Work orders', 'Application', 'Vacancy', 'Unit Directory', 'Inspection', 'General Ledger','Notes & History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            //  if(rowObject.Status == '2')  select = ['Edit', 'Tenant Living', 'Add New Tenant', 'Work orders', 'Application', 'Vacancy', 'Unit Directory', 'Inspection', 'General Ledger','Notes & History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            if(rowObject.Status == '3')  select = ['Activate This Property', 'Print Envelope','Delete This Property'];
            //   if(rowObject.Status == '4')  select = ['Edit', 'Tenant Living', 'Add New Tenant', 'Work orders', 'Application', 'Vacancy', 'Unit Directory', 'Inspection', 'General Ledger','Notes & History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            //  if(rowObject.Status == '5')  select = ['Edit', 'Tenant Living', 'Add New Tenant', 'Work orders', 'Application', 'Vacancy', 'Unit Directory', 'Inspection', 'General Ledger','Notes & History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            if(rowObject.Status == '6')  select = ['Print Envelope','Delete This Property'];
            if(rowObject.Vacant == '1')  select = ['Edit', 'Active Tenants','Add New Tenant','Work Order','Application','Vacancy', 'Unit Directory', 'Inspection','General Ledger','Notes & History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'Add In-Touch', 'In-Touch History', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            if(rowObject['Short-Term Rental'] == '1')  select = ['Edit','Active Guests','Add New Tenant','Work Order','Application','Vacancy', 'Unit Directory', 'Inspection','General Ledger','Notes & History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank','Book Now', 'Add In-Touch', 'In-Touch History', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }

    }

    alphabeticSearch();
    function alphabeticSearch() {
        $.ajax({
            type: 'post',
            url: '/List/jqgrid',
            data: {class: 'jqGrid',
                action: "alphabeticSearch",
                table: 'general_property',
                column: 'property_name'},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.code == 200) {
                    var html = '';

                    $.each(response.data, function (key, val) {
                        var color = '#05A0E4'
                        if (val == 0)
                            color = '#c5c5c5';
                        html += '<span class="getAlphabet" style="color:' + color + '" data_id="' + val + '">' + key + '</span>';
                    });
                    $('.AtoZ').html(html);
                }
            }
        });
    }

    $(document).on('click', '#AZ', function () {
        $('.AZ').hide();
        $('#apex-alphafilter').show();
    });

    $(document).on('click', '#allAlphabet', function () {
        var grid = $("#property-table");
        $('.AZ').show();
        $('#apex-alphafilter').hide();
        grid[0].p.search = false;
        $.extend(grid[0].p.postData, {filters: ""});
        grid.trigger("reloadGrid", [{page: 1, current: true}]);
        //$('#companyUser-table').trigger( 'reloadGrid' );
    });

    $(document).on('click', '.getAlphabet', function () {
        var grid = $("#property-table"), f = [];
        var value = $(this).attr('data_id');
        var search = $(this).text();
        if (value != '0') {
            f.push({field: "property_name", op: "bw", data: search});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData, {filters: JSON.stringify(f)});
            grid.trigger("reloadGrid", [{page: 1, current: true}]);
        }
    });


    /**
     * Function for view company on clicking row
     */
    $(document).on('click', '#property-table tr td', function () {
        var id = $(this).closest('tr').attr('id');

        if ($(this).index() == 1 || $(this).index() == 13) {
            return false;
        }else if($(this).index() == 0 ){
            $(document).on("click",".is_xml",function () {
                var id= $(this).attr('data_id');
                if($("#xml_"+id).is(':checked')){
                    $("#xml_"+id).prop('checked',true);
                }else{
                    $("#xml_"+id).prop('checked',false);
                }
            });
        } else if($(this).index() == 3) {
            var checkPermissionData = checkPermissions(permissions,module,'View Property');
            if(checkPermissionData == 'Access Denied') {
                return false;
            }
            var index =$(this).index();
            var td = $(this).closest('tr').find('td:eq('+index+')').html();
            td = $(td).text();
            if(td != '0') {
                window.location.href = '/Building/BuildingModule?id=' + id
            }
        } else if($(this).index() == 4){
            var checkPermissionData = checkPermissions(permissions,module,'View Property');
            if(checkPermissionData == 'Access Denied') {
                return false;
            }
            var index =$(this).index();
            var td = $(this).closest('tr').find('td:eq('+index+')').html();
            td = $(td).text();
            if(td != '0') {
                window.location.href = '/Unit/UnitModule?id=' + id;
            }
        } else {
            var checkPermissionData = checkPermissions(permissions,module,'View Property');
            if(checkPermissionData == 'Access Denied') {
                return false;
            }
            window.location.href = '/Property/PropertyView?id=' + id;
        }
    });

    /**
     * download sample function
     */
    var base_url = window.location.origin;
    $(document).on("click", '#export_sample_property_button', function() {
        window.location.href = base_url + "/propertyView-ajax?status=" + "&&action=exportSampleExcel";
    });

    /**
     * export function
     */
    $(document).on("click", '#export_property_button', function() {
        var status = $("#jqGridStatus option:selected").val();
        var table = 'general_property';
        window.location.href = base_url + "/propertyView-ajax?status=" + status + "&&table=" + table + "&&action=exportExcel";
    });

    /**
     * Import function
     */
    $(document).on('click', '#import_property_button', function(e) {
        var checkPermissionData = checkPermissions(permissions,module,'Import Property');
        if(checkPermissionData == 'Access Denied') {
            return false;
        }
        $("#import_file-error").text('');
        $("#import_file").val('');
        $("#ImportProperty").show(500);
        return false;
    });

    /** Import unit type excel  */
    $("#importPropertyForm").validate({
        rules: { import_file: {
                required: true
            },
        },
        submitHandler: function () {
            var myFile = $('#import_file').prop('files');
            var myFiles = myFile[0];
            var formData = new FormData();
            formData.append('file', myFiles);
            formData.append('class', 'propertyView');
            formData.append('action', 'importExcel');
            $.ajax
            ({
                type: 'post',
                url: '/propertyView-ajax',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    $('#ImportProperty').hide();
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        toastr.success(response.message);
                        $("#ImportProperty").hide(500);
                        triggerReload();
                    } else if(response.status == 'failed' && response.code == 503){
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });

    $(document).on("click", "#import_property_cancel", function(e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {
                confirm: {
                    label: 'Yes'
                },
                cancel: {
                    label: 'No'
                }
            },
            callback: function(result) {
                if (result == true) {
                    $("#ImportProperty").hide(500);
                }
            }
        });
    });

    /**
     * function to format short term rental
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyShortTermRental(cellValue, options, rowObject) {
        if (cellValue == 0)
            return "No";
        else if (cellValue == 1)
            return "Yes";
    }

    /**
     * function to format property status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyStatus(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "Active";
        else if (cellValue == '2')
            return "Active";
        else if (cellValue == '3')
            return "Archive";
        else if (cellValue == '4')
            return "Active";
        else if (cellValue == '5')
            return "Active";
        else if (cellValue == '6')
            return "Resign";
        else if (cellValue == '7')
            return "Active";
        else if (cellValue == '8')
            return "Active";
        else
            return '';
    }

    /**
     *  function to format property name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            var flagValue = $(rowObject.Action).attr('flag');
            var id = $(rowObject.Action).attr('data_id');
            var clone = rowObject.clone_status;
            var flag = '';
            if(rowObject.Status == '4'){
                if (flagValue == 'yes') {
                    if (clone == 1) {
                        if (cellValue.indexOf("_copy") >= 0) {
                            var cellValueData = cellValue.replace("_copy", "");
                            return '<div class="tooltipgridclass"><span><span style="color:#FF1A1A;text-decoration:underline;font-weight: bold;">' + cellValueData + '</span><span style="color:red;">_copy</span><a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id=' + id + '" ><img src="/company/images/Flag.png"></a></span><span class="tooltiptextbotclass">THIS PROPERTY IS ON THE MARKET FOR SALE</span></div>';
                        } else {
                            return '<div class="tooltipgridclass"><div><span style="color:#FF1A1A;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id=' + id + '" ><img src="/company/images/Flag.png"></a></span><span class="tooltiptextbotclass">THIS PROPERTY IS ON THE MARKET FOR SALE</span></div>';
                        }
                    } else {
                        return '<div class="tooltipgridclass"><span style="color:#FF1A1A;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id=' + id + '" ><img src="/company/images/Flag.png"></a></span><span class="tooltiptextbotclass">THIS PROPERTY IS ON THE MARKET FOR SALE</span></div>';
                    }
                } else {
                    if (clone == 1) {
                        if (cellValue.indexOf("_copy") >= 0) {
                            var cellValueData = cellValue.replace("_copy", "");
                            return '<div class="tooltipgridclass"><span><span style="color:#FF1A1A;text-decoration:underline;font-weight: bold;">' + cellValueData + '</span><span style="color:red;">_copy</span></span><span class="tooltiptextbotclass">THIS PROPERTY IS ON THE MARKET FOR SALE</span></div>';
                        } else {
                            return '<div class="tooltipgridclass"><span><span style="color:#FF1A1A;text-decoration:underline;font-weight: bold;">' + cellValue + '</span><span class="tooltiptextbotclass">THIS PROPERTY IS ON THE MARKET FOR SALE</span></div>';
                        }
                    } else {
                        return '<div class="tooltipgridclass"><span style="color:#FF1A1A;text-decoration:underline;font-weight: bold;">' + cellValue + '</span><span class="tooltiptextbotclass">THIS PROPERTY IS ON THE MARKET FOR SALE</span></div>';
                    }
                }
            } else {
                if (flagValue == 'yes') {
                    if (clone == 1) {
                        if (cellValue.indexOf("_copy") >= 0) {
                            var cellValueData = cellValue.replace("_copy", "");
                            return '<span> <span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValueData + '</span><span style="color:red;">_copy</span><a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id=' + id + '" ><img src="/company/images/Flag.png"></a></span>';
                        } else {
                            return '<span> <span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id=' + id + '" ><img src="/company/images/Flag.png"></a></span>';
                        }
                    } else {
                        return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id=' + id + '" ><img src="/company/images/Flag.png"></a></span>';
                    }
                } else {
                    if (clone == 1) {
                        if (cellValue.indexOf("_copy") >= 0) {
                            var cellValueData = cellValue.replace("_copy", "");
                            return '<span><span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValueData + '</span><span style="color:red;">_copy</span></span>';
                        } else {
                            return '<span><span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
                        }
                    } else {
                        return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
                    }

                }
            }
        }

    }

    /**
     * function to format property unit and building
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyUnitBuilding(cellValue, options, rowObject) {
        if(cellValue == ''){
            return '<span data_redirect="no" style="text-decoration: underline;">0</span>';
        } else {
            return '<span data_redirect="yes" style="text-decoration: underline;">' + cellValue + '</span>';
        }
    }

    /**
     * function to trigger Reload
     */
    function triggerReload() {
        var grid = $("#property-table");
        grid[0].p.search = false;
        $.extend(grid[0].p.postData, {filters: ""});
        grid.trigger("reloadGrid", [{page: 1, current: true}]);
    }



    $(document).on('click','.classFlagRedirect',function(e){
        event.preventDefault();
        localStorage.setItem("scrollDown",$(this).attr('data_href'));
        var url = $(this).attr('data_url');
        window.location.href = url;
    });
});


function PrintElem(elem)
{
    Popup($(elem).html());
}

function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
        'font-weight:bold;} .right-detail{\n' +
        '        position:relative;\n' +
        '        left:+400px;\n' +
        '    }</style>');
    $(mywindow.document.body).html( '<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if(mywindow.close()){

    }
    $("#PrintEnvelope").modal('hide');
    return true;
}
function actionCheckboxFmatterComplaint(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {
        var checkedData = (rowObject.is_xml == '0') ? '' : 'checked';
        var data = '';
        var data = '<input type="checkbox" name="is_xml[]" class="is_xml"  id="xml_' + rowObject.id + '" data_id="' + rowObject.id + '" '+checkedData+'/>';
        return data;
    }
}
$(document).on('click', '#rentalUpload', function () {
    favorite = [];
    var uncheckedData = [];
    // if($("#select_all_complaint_checkbox").is(':checked')){
    //     favorite=[];
    //     $.each($("input[name='is_xml[]']:checked"), function () {
    //         favorite.push($(this).attr('data_id'));
    //     });
    //     uncheckedData=[];
    //     $.each($("input[name='is_xml[]']:unchecked"), function () {
    //         uncheckedData.push($(this).attr('data_id'));
    //     });
    // }else{
    favorite=[];
    $.each($("input[name='is_xml[]']:checked"), function () {
        favorite.push($(this).attr('data_id'));
    });
    console.log('favorite',favorite);
    uncheckedData=[];
    $.each($("input[name='is_xml[]']:unchecked"), function () {
        uncheckedData.push($(this).attr('data_id'));
    });


    // }
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {class: 'propertyDetail', action: "updateXmlStatus", id: favorite,uncheck:uncheckedData},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                toastr.success(response.message);
                // $("#uploadXmlModal").modal("show");
                // var upd = window.location.origin+'/company/upload/tony.xml';
                //
                // $("#xml_link").val(upd);
            } else if(response.status == 'error' && response.code == 500){
                toastr.error(response.message);
            } else {
                toastr.error(response.message);
            }
            $("#property-table").trigger("reload");
        }
    });
})

$(document).on('click', '#select_all_complaint_checkbox', function () {
    if($(this).is(':checked')){
        $(".is_xml").prop('checked',true);
    }else{
        $(".is_xml").prop('checked',false);
    }

});


$(document).on("click",".cancelXml",function () {
    $("#uploadXmlModal").modal("hide");
})

$(document).on("click","#uploadXmlForm",function(){
    $("#uploadXmlModal").modal("hide");
})

function createXml(){
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {class: 'propertyDetail', action: "createXmlAction"},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {

            } else {
                toastr.error(response.message);
            }
        }
    });
}