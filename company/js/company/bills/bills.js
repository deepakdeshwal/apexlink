$(document).on('click','#addNewRow',function(){
    addBillItems();
});

function addBillItems(){
    var cloneRow = $('.tableData');
    // console.log(cloneRow);return false;
    if(cloneRow.length == 0) {
        var html = '<tr class="tableData">';
        html += '<td width="20%"><select class="form-control building_select customBuildingValidation" name="building[]" data_required="true"><option value="">Select</option></select><span class="error red-star customError"></span></td>';
        html += '<td width="10%"><select class="form-control unit_select customUnitValidation" name="unit[]" data_required="true"><option value="">Select</option></select><span class="error red-star customError"></span></td>';
        html += '<td width="30%"><select class="form-control account_select customAccountValidation" name="account[]" data_required="true"><option value="">Select</option></select><span class="error red-star customError"></span></td>';
        html += '<td width="15%"><input placeholder="0.00" type="text" class="form-control customItemAmountValidation amount_num number_only item_amount" id="item_amount" name="item_amount[]" data_required="true"><span class="error red-star customError"></span></td>';
        html += '<td width="15%"><input placeholder="Description" type="text" class="form-control customItemDescriptionValidation item_description" name="item_description[]" data_required="true"><span class="error red-star customError"></span></td>';
        html += '<td width="10%"><i class="fa fa-times cursor crossRow" aria-hidden="true"></i></td>';
        html += '</tr>';
        $('#appendTable').append(html);
        fetchAllBuildings($('#property_id').val());
        fetchAllAccounts();
    } else {
        var cloneData = cloneRow.clone();
        cloneData.find('Select').val('');
        cloneData.find('input').val('');
        $('#appendTable').append(cloneData[0]);
    }
}

$(document).on('click','.crossRow',function(){
    $(this).parent().parent().remove();
});

$(document).on("click", ".vendor-notes-plus", function () {
    var length = $(".vendor-notes-clone-divclass").length;
    if (length <= 2) {
        var newnode = $("#vendor-notes-clone-div").clone().find(".notes").val("").end().insertAfter("div.vendor-notes-clone-divclass:last");
        $(".vendor-notes-clone-divclass:not(:eq(0))  .vendor-notes-plus").remove();
        $(".vendor-notes-clone-divclass:not(:eq(0))  .vendor-notes-minus").show();
        if (length == 2) {
            $('.vendor-notes-plus').hide();
        }
    } else {
        $('.vendor-notes-plus').hide();
    }
    newnode.find('.notes').on('keydown', function(event) {
        if (this.selectionStart == 0 && event.keyCode >= 65 && event.keyCode <= 90 && !(event.shiftKey) && !(event.ctrlKey) && !(event.metaKey) && !(event.altKey)) {
            var $t = $(this);
            event.preventDefault();
            var char = String.fromCharCode(event.keyCode);
            $t.val(char + $t.val().slice(this.selectionEnd));
            this.setSelectionRange(1,1);
        }
    });
});

$(document).on('click', '.vendor-notes-minus', function () {
    $(this).parent().parent().remove();
    $('.vendor-notes-plus').show();
});

$("#billDate").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
$("#dueDate").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
fetchAllPortfolio();

function fetchAllPortfolio(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-portfolioname',
        data: {
            propertyEditid: '',
            class: 'propertyDetail',
            action: 'fetchPortfolioname'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#portfolio_id').html(res.data);
        },
    });
}
setTimeout(function(){
    fetchAllProperty($('#portfolio_id').val());
}, 500);
function fetchAllProperty(portfolio_id,id) {
    $.ajax({
        type: 'post',
        url: '/global-get-module-ajax',
        data: {
            class: 'GlobalGetModule',
            action: 'getAllProperties',
            portfolio_id: portfolio_id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200) {
                console.log('property_id',id);
                var html = '<option value="">Select</option>';
                $.each(res.data,function(key,value){
                    if(id == value.id) {
                        html += '<option value="' + value.id + '" selected>' + value.property_name + '</option>';
                    } else {
                        html += '<option value="' + value.id + '">' + value.property_name + '</option>';
                    }
                });
                $('#property_id').html(html);
            }
        },
    });
}
fetchAllWorkOrder();
function fetchAllWorkOrder() {
    $.ajax({
        type: 'post',
        url: '/global-get-module-ajax',
        data: {
            class: 'GlobalGetModule',
            action: 'getAllWorkOrder'},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200) {
                var html = '';
                $.each(res.data,function(key,value){
                    html += '<option value="'+value.id+'">'+value.work_order_number+'</option>';
                });
                $('#work_order').html(html);

                $('#work_order').multiselect('destroy').multiselect({
                    includeSelectAllOption: true,
                    nonSelectedText: 'Select'
                });
            }
        },
    });
}
$(document).on('change','#portfolio_id',function(){
    fetchAllProperty($(this).val());
});

$(document).on('change','#property_id',function(){
    fetchAllBuildings($(this).val());
});

$(document).on('change','.building_select',function(){
    var element = $(this).parent().next().find('select');
    fetchAllUnits($(this).val(),element);
});

function fetchAllBuildings(property_id) {
    $.ajax({
        type: 'post',
        url: '/global-get-module-ajax',
        data: {
            class: 'GlobalGetModule',
            action: 'getAllBuildings',
            property_id: property_id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200) {
                var html = '<option value="">Select</option>';
                $.each(res.data,function(key,value){
                    html += '<option value="'+value.id+'">'+value.building_name+'</option>';
                });
                $('.building_select').html(html);
            }
        },
    });
}

function fetchAllUnits(building_id ,element) {
    $.ajax({
        type: 'post',
        url: '/global-get-module-ajax',
        data: {
            class: 'GlobalGetModule',
            action: 'getAllUnits',
            building_id : building_id },
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200) {
                var html = '<option value="">Select</option>';
                $.each(res.data,function(key,value){
                    html += '<option value="'+value.id+'">'+value.unit_no+'</option>';
                });
                $(element).html(html);
            }
        },
    });
}
fetchAllAccounts();
function fetchAllAccounts() {
    $.ajax({
        type: 'post',
        url: '/global-get-module-ajax',
        data: {
            class: 'GlobalGetModule',
            action: 'getAllChartsOfAccounts'},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200) {
                var html = '<option value="">Select</option>';
                $.each(res.data,function(key,value){
                    html += '<option value="'+value.id+'">'+value.account_code+'-'+value.account_name+'</option>';
                });
                $('.account_select').html(html);
            }
        },
    });
}

//  vendor combo box js
$(document).on('click','#_easyui_textbox_input1',function(){
    $('.vendorSelect').combogrid('showPanel');
});

$(document).on('focusout','#_easyui_textbox_input1',function(){
    if($(this).val() == ''){
        $('#vendorAddress').html('');
        $('#EditVendorAddress').hide();
        $('#EditVendorAddress').attr('data_id','');
        $('#vendor_id').val('');
    }
});

$( "#vendor_zip_code" ).focusout(function() {
    getZipCode('#vendor_zip_code',$('#vendor_zip_code').val(),'#vendor_city','#vendor_state','#vendor_country','','');
});

$(document).on('click','#EditVendorAddress',function(){
    var id = $(this).attr('data_id');
    var validator = $( "#update_vendor_address_form" ).validate();
    validator.resetForm();
    fetchUserAddress(id);
    $('#updateVendorAddress').modal('show');
});


$(document).on('click','.cancel_address_update',function(){
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $('#updateVendorAddress').modal('hide');
        }
    });
});

intiateComboGrid();
function intiateComboGrid() {
    $('.vendorSelect').combogrid({
        placeholder: "Select...",
        panelWidth: '780px',
        panelHeight: 'auto',
        url: '/combo-grid-global-ajax',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getUserdata',
            class: 'GlobalComboGrid',
            user_type: '3'
        },
        columns: [[
            {field: 'name', title: 'Vendor Name', width: '173px'},
            {field: 'address', title: 'Address', width: '216px'},
            {field: 'email', title: 'Email', width: '186px'},
            {field: 'vendor_type', title: 'Vendor Type', width: '100px'},
            {field: 'vendor_rate', title: 'Rate', width: '100px'}
        ]],
        onSelect: function (index, row) {
            $('#vendorAddress').val(row.addressFormat);
            $('#EditVendorAddress').show();
            $('#EditVendorAddress').attr('data_id', row.id);
            $('#vendor_id').val(row.id);
        }
    });
    $('.textbox').css('width','70%');
    $('.textbox-addon').css('display','none');
}

function fetchUserAddress(id) {
    $.ajax({
        type: 'post',
        url: '/global-user-module-ajax',
        data: {
            class: 'GlobalUserModule',
            action: 'getUser',
            id:id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200){
                $('#vendorAddress1').val(res.data.address1);
                $('#vendorAddress2').val(res.data.address2);
                $('#vendorAddress3').val(res.data.address3);
                $('#vendor_zip_code').val(res.data.zipcode);
                $('#vendor_country').val(res.data.country);
                $('#vendor_state').val(res.data.state);
                $('#vendor_city').val(res.data.city);
                $('#vendor_address_id').val(res.data.id);
            }
        },
    });
}

$(document).on('submit','#update_vendor_address_form',function(e){
    e.preventDefault();
    if($('#update_vendor_address_form').valid()){
        $.ajax({
            type: 'post',
            url: '/global-user-module-ajax',
            data: {
                class: 'GlobalUserModule',
                action: 'updateUserAddress',
                data:$('#update_vendor_address_form').serializeArray()},
            success: function (response) {
                var res = JSON.parse(response);
                if(res.code == 200){
                    $('#vendorAddress').val(res.address);
                    $('#updateVendorAddress').modal('hide');
                    toastr.success(res.message);
                }
            },
        });
    }
});

//building validation
$(document).on('change','.customBuildingValidation',function(){
    validations(this);
});

//unit validation
$(document).on('change','.customUnitValidation',function(){
    validations(this);
});

//account validation
$(document).on('change','.customAccountValidation',function(){
    validations(this);
});

//amount validation
$(document).on('keyup','.customItemAmountValidation',function(){
    validations(this);
});

//description validation
$(document).on('keyup','.customItemDescriptionValidation',function(){
    validations(this);
});



$("#saveNewBill").on('submit',function(e){
    e.preventDefault();
    var original = $('#billAmount').val().replace(',','');
    var totalAm = 0;
    $( ".item_amount" ).each(function( index ) {
        totalAm += parseFloat($(this).val().replace(',',''));
    });
    if(totalAm > original){
        bootbox.alert("Property Total amount can not be greater than Bill Amount");
        return false;
    }
    if($('#saveNewBill').valid()){
        var form = $('#saveNewBill')[0];
        var formData = new FormData(form);
        var data = convertSerializeDatatoArray();
        $.each(file_library, function (key, value) {
            if (compareArray(value, data) == 'true') {
                formData.append(key, value);
            }
        });
        formData.append('action', 'create_new_bill');
        formData.append('class', 'billsAjax');
        $.ajax({
            url: '/newBill-ajax',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                var buildingRes = true;
                var unitRes = true;
                var accountRes = true;
                var itemAmountRes = true;
                var descriptionRes = true;
                //building validation
                $(".customBuildingValidation").each(function () {
                    buildingRes = validations(this);
                });
                //unit validation
                $(".customUnitValidation").each(function () {
                    unitRes = validations(this);
                });
                //account validation
                $(".customAccountValidation").each(function () {
                    accountRes = validations(this);
                });
                //amount validation
                $(".customItemAmountValidation").each(function () {
                    itemAmountRes = validations(this);
                });
                //description validation
                $(".customItemDescriptionValidation").each(function () {
                    descriptionRes = validations(this);
                });

                if (buildingRes === false || unitRes === false || accountRes === false || itemAmountRes === false || descriptionRes === false) {
                    xhr.abort();
                    return false;
                }
            },
            success: function (response) {
                var response = JSON.parse(response)
                $('#loadingmessage').hide();
                if(response.code == 200){
                    window.location.href = '/Vendor/Vendor';
                }
            },
            error: function (data) {
                $('#loadingmessage').hide();
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});

$(document).on('focusout','#billAmount',function(){
     $('.item_amount').val($(this).val());
});

$(document).on('click','#cancelNewBill',function(){
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            window.location.href = '/Vendor/Vendor';
        }
    });
});

