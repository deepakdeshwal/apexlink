$(document).ready(function(){

    fetchAllPortfolio();
    fetchAllAccounts();
    intiateComboGrid();
    var billID = $('#bill_id').val();
    if(billID !='undefined' && billID != ''){
        get_RecurringTransactionInfo(billID);
    }else{

         setTimeout(function(){
        fetchAllProperty($('#portfolio_id').val());
    }, 500);
    }

    $("#billDate").datepicker({
        dateFormat: jsDateFomat,
        changeMonth: true,
        changeYear: true
    }).datepicker("setDate", new Date());


    $("#dueDate").datepicker({
        dateFormat: jsDateFomat,
        changeMonth: true,
        changeYear: true
    }).datepicker("setDate", new Date());

  //  $("#billDate").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
  //  $("#dueDate").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());

    $(document).on('change','#portfolio_id',function(){
        fetchAllProperty($(this).val());
    });

    $(document).on('change','#propertyid',function(){
        fetchAllBuildings($(this).val());
    });

    $(document).on('change','.building_select',function(){
        var element = $(this).parent().next().find('select');
        fetchAllUnits($(this).val(),element);
    });

    //  vendor combo box js
    $(document).on('click','#_easyui_textbox_input1',function(){
        $('.vendorSelect').combogrid('showPanel');
    });




    function fetchAllPortfolio() {
        $.ajax({
            type: 'post',
            url: '/fetch-portfolioname',
            data: {
                propertyEditid: '',
                class: 'propertyDetail',
                action: 'fetchPortfolioname'},
            success: function (response) {
                var res = JSON.parse(response);
                $('#portfolio_id').html(res.data);
            },
        });
    }



    $(document).on('click','#EditVendorAddress',function(){
        var id = $(this).attr('data_id');
        var validator = $( "#update_vendor_address_form" ).validate();
        validator.resetForm();
        fetchUserAddress(id);
        $('#updateVendorAddress').modal('show');
    });
});

$('.cancel_address_update').click(function () {
$('#updateVendorAddress').modal('hide');
});

function fetchAllProperty(portfolio_id) {
    $.ajax({
        type: 'post',
        url: '/global-get-module-ajax',
        async: false,
        data: {
            class: 'GlobalGetModule',
            action: 'getAllProperties',
            portfolio_id: portfolio_id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200) {
                //   console.log('property_id',id);
                var html = '<option value="">Select</option>';
                $.each(res.data,function(key,value){
                    //   if(id == value.id) {
                    html += '<option value="' + value.id + '" >' + value.property_name + '</option>';
                    //     } else {
                    //      html += '<option value="' + value.id + '">' + value.property_name + '</option>';
                    //    }
                });
                $('#propertyid').html(html);
            }
        },
    });
}

function fetchUserAddress(id) {
    $.ajax({
        type: 'post',
        url: '/global-user-module-ajax',
        data: {
            class: 'GlobalUserModule',
            action: 'getUser',
            id:id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200){
                $('#vendorAddress1').val(res.data.address1);
                $('#vendorAddress2').val(res.data.address2);
                $('#vendorAddress3').val(res.data.address3);
                $('#vendor_zip_code').val(res.data.zipcode);
                $('#vendor_country').val(res.data.country);
                $('#vendor_state').val(res.data.state);
                $('#vendor_city').val(res.data.city);
                $('#vendor_address_id').val(res.data.id);
            }
        },
    });
}


$(document).on('submit','#update_vendor_address_form',function(e){
    e.preventDefault();
    if($('#update_vendor_address_form').valid()){
        $.ajax({
            type: 'post',
            url: '/global-user-module-ajax',
            data: {
                class: 'GlobalUserModule',
                action: 'updateUserAddress',
                data:$('#update_vendor_address_form').serializeArray()},
            success: function (response) {
                var res = JSON.parse(response);
                if(res.code == 200){
                    $('#vendorAddress').val(res.address);
                    $('#updateVendorAddress').modal('hide');
                    toastr.success(res.message);
                }
            },
        });
    }
});

$(document).on('focusout','#billAmount',function(){
    $('.item_amount').val($(this).val());
});

function intiateComboGrid() {
    $('.vendorSelect').combogrid({
        placeholder: "Select...",
        panelWidth: '780px',
        panelHeight: 'auto',
        url: '/combo-grid-global-ajax',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getUserdata',
            class: 'GlobalComboGrid',
            user_type: '3'
        },
        columns: [[
            {field: 'name', title: 'Vendor Name', width: '173px'},
            {field: 'address', title: 'Address', width: '216px'},
            {field: 'email', title: 'Email', width: '186px'},
            {field: 'vendor_type', title: 'Vendor Type', width: '100px'},
            {field: 'vendor_rate', title: 'Rate', width: '100px'}
        ]],
        onSelect: function (index, row) {
            $('#vendorAddress').val(row.addressFormat);
            $('#EditVendorAddress').show();
            $('#EditVendorAddress').attr('data_id', row.id);
            $('#vendor_id').val(row.id);
        }
    });
    $('.textbox').css('width','70%');
    $('.textbox-addon').css('display','none');
    $('.textbox-text').css('width','131%');
}


function fetchAllBuildings(property_id) {
    $.ajax({
        type: 'post',
        url: '/global-get-module-ajax',
        data: {
            class: 'GlobalGetModule',
            action: 'getAllBuildings',
            property_id: property_id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200) {
                var html = '<option value="">Select</option>';
                $.each(res.data,function(key,value){
                    html += '<option value="'+value.id+'">'+value.building_name+'</option>';
                });
                $('.building_select').html(html);
            }
        },
    });
}

function fetchAllUnits(building_id ,element) {
    $.ajax({
        type: 'post',
        url: '/global-get-module-ajax',
        data: {
            class: 'GlobalGetModule',
            action: 'getAllUnits',
            building_id : building_id },
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200) {
                var html = '<option value="">Select</option>';
                $.each(res.data,function(key,value){
                    html += '<option value="'+value.id+'">'+value.unit_no+'</option>';
                });
                $(element).html(html);
            }
        },
    });
}

function fetchAllAccounts() {
    $.ajax({
        type: 'post',
        url: '/global-get-module-ajax',
        data: {
            class: 'GlobalGetModule',
            action: 'getAllChartsOfAccounts'},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200) {
                var html = '<option value="">Select</option>';
                $.each(res.data,function(key,value){
                    html += '<option value="'+value.id+'">'+value.account_code+'-'+value.account_name+'</option>';
                });
                $('.account_select').html(html);
            }
        },
    });
}




$("#saveNewBill").on('submit',function(e){
    e.preventDefault();
    var original = $('#billAmount').val().replace(',','');
    var totalAm = 0;
    $( ".item_amount" ).each(function( index ) {
        totalAm += parseFloat($(this).val().replace(',',''));
    });
    if(totalAm > original){
        bootbox.alert("Property Total amount can not be greater than Bill Amount");
        return false;
    }
    if($('#saveNewBill').valid()){
        var form = $('#saveNewBill')[0];
        var formData = new FormData(form);
       var data = convertSerializeDatatoArray();
        $.each(file_library, function (key, value) {
            if (compareArray(value, data) == 'true') {
                formData.append(key, value);
            }
        });
        formData.append('action', 'save_Recurring_bill');
        formData.append('class', 'billsAjax');
        $.ajax({
            url: '/newBill-ajax',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                var buildingRes = true;
                var unitRes = true;
                var accountRes = true;
                var itemAmountRes = true;
                var descriptionRes = true;
                //building validation
                $(".customBuildingValidation").each(function () {
                    buildingRes = validations(this);
                });
                //unit validation
                $(".customUnitValidation").each(function () {
                    unitRes = validations(this);
                });
                //account validation
                $(".customAccountValidation").each(function () {
                    accountRes = validations(this);
                });
                //amount validation
                $(".customItemAmountValidation").each(function () {
                    itemAmountRes = validations(this);
                });
                //description validation
                $(".customItemDescriptionValidation").each(function () {
                    descriptionRes = validations(this);
                });

                if (buildingRes === false || unitRes === false || accountRes === false || itemAmountRes === false || descriptionRes === false) {
                    xhr.abort();
                    return false;
                }
            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);
                localStorage.setItem("Message", response.message);
                localStorage.setItem("rowcolor",true)
                window.location.href = '/Accounting/RecurringBill';
            },
            error: function (data) {
                $('#loadingmessage').hide();
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});

function get_RecurringTransactionInfo(ID) {
   //            intiateComboGrid();
    $.ajax({
        type: 'post',
        url: '/newBill-ajax',
        data: {
            class: 'billsAjax',
            action: 'get_RecurringTransactionInfo',
            id: ID
        },
        success: function(response) {

              var response = JSON.parse(response);
              if (response.status == 'success' && response.code == 200) {
                  var billdata = response.data.BillInfo;
                  var Billnotes = response.data.BillNotes;
                  var Billitems = response.data.BillItems;
                  var Billfiles = response.data.BillFiles;
                  $('#_easyui_textbox_input1').val(billdata.name);
                  $('#vendor_id').val(billdata.vendor_id);

                  $("#billDate").datepicker({
                      dateFormat: jsDateFomat,
                      changeMonth: true,
                      changeYear: true
                  }).datepicker("setDate", new Date(billdata.bill_date));

                  $('#billAmount').val(billdata.amount);
                  $('#frequency option[value=' + billdata.frequency + ']').prop('selected,true');

                  if (billdata.change_to != '' && billdata.change_to != 'undefined') {
                      $("input:radio[value=" + billdata.change_to + "]").prop('checked', true);
                      $("input:radio[value=" + billdata.change_to + "]").trigger('click');
                      $('#change_to_div').show();
                      if (billdata.change_to == '1') {
                          $('#tenant_div_box').show();
                          $('#other_div_box').hide();
                          showTenantGrid();
                      } else {
                          $('#tenant_div_box').hide();
                          $('#other_div_box').show();
                          $('#change_to_other').val(billdata.change_to_other);
                      }
                      $('#_easyui_textbox_input2').val(billdata.tenant_name);
                  }

                  $("#dueDate").datepicker({
                      dateFormat: jsDateFomat,
                      changeMonth: true,
                      changeYear: true
                  }).datepicker("setDate", new Date(billdata.due_date));

                //  $('.vendor-notes-plus').trigger('click');
                  $('#duration').val(billdata.duration);
                  $('#ref_num').val(billdata.refrence_number);
                  $('#vendorAddress').html(billdata.address);
                  $('#memo').html(billdata.memo);
                  $('#portfolio_id option[value=' + billdata.portfolio_id + ']').prop('selected,true');
                  fetchAllProperty(billdata.portfolio_id);
                  $('#propertyid').val(billdata.property_id);
                  $('#appendTable').html(Billitems);

                  $('#file_library_uploads').html(Billfiles);
                  $('#term option[value="' + billdata.term + '"]').prop('selected', true);
                  if (Billnotes) {
                    Recurringbillnote(Billnotes);
                  } else {
                      toastr.error(response.message);
                      onTop(true);
                  }
                  edit_date_time(billdata.updated_at);
              }
            $('.textbox-text').css('width','131%');
        }
    });
}

function Recurringbillnote(Billnotes) {
    if (Billnotes.length > 0) {
        console.log( 'Billnotes',Billnotes);
        $.each(Billnotes, function (key, notes) {
            if(key!=0) {
                console.log( '1111',key);
                console.log( '1111',notes);
                var clone = $("#vendor-notes-clone-div").clone().insertAfter("div.vendor-notes-clone-div:last");

                var clone = $("#vendor-notes-clone-div").clone().find(".notes").val(notes.note).end().insertAfter("div.vendor-notes-clone-divclass:last");
                // $(".vendor-notes-clone-divclass:not(:eq(0))  .vendor-notes-plus").hide();
                // $(".vendor-notes-clone-divclass:not(:eq(0))  .vendor-notes-minus").show();


                // clone.find('.notes').val(notes.note);
                clone.find(".vendor-notes-plus").hide();
                clone.find(".vendor-notes-minus").show();

                var phoneRowLenght = $(".vendor-notes-clone-divclass");
                console.log( 'phoneRowLenght',phoneRowLenght);

                if (phoneRowLenght.length == 2) {
                    clone.find(".vendor-notes-plus").hide();
                } else if (phoneRowLenght.length == 3) {
                    clone.find(".vendor-notes-plus").hide();
                    $(".vendor-notes-clone-divclass:eq(0) .vendor-notes-plus").hide();
                } else {
                    $(".vendor-notes-clone-divclass:not(:eq(0)) .vendor-notes-plus").show();
                    // console.log($(".vendor-notes-clone-divclass:not(:eq(0)) .fa-plus-circle").show());
                }
            } else {
                $('div#vendor-notes-clone-div').find('.notes').val(notes.note);
            }
        });
    }
}

$(document).on('click','#addNewRow',function(){
    addBillItems();
});

function addBillItems(){
    var cloneRow = $('.tableData');
    // console.log(cloneRow);return false;
    if(cloneRow.length == 0) {
        var html = '<tr class="tableData">';
        html += '<td width="20%"><select class="form-control building_select customBuildingValidation" name="building[]" data_required="true"><option value="">Select</option></select><span class="error red-star customError"></span></td>';
        html += '<td width="10%"><select class="form-control unit_select customUnitValidation" name="unit[]" data_required="true"><option value="">Select</option></select><span class="error red-star customError"></span></td>';
        html += '<td width="30%"><select class="form-control account_select customAccountValidation" name="account[]" data_required="true"><option value="">Select</option></select><span class="error red-star customError"></span></td>';
        html += '<td width="15%"><input placeholder="0.00" type="text" class="form-control customItemAmountValidation amount_num number_only item_amount" id="item_amount" name="item_amount[]" data_required="true"><span class="error red-star customError"></span></td>';
        html += '<td width="15%"><input placeholder="Description" type="text" class="form-control customItemDescriptionValidation item_description" name="item_description[]" data_required="true"><span class="error red-star customError"></span></td>';
        html += '<td width="10%"><i class="fa fa-times cursor crossRow" aria-hidden="true"></i></td>';
        html += '</tr>';
        $('#appendTable').append(html);
        fetchAllBuildings($('#property_id').val());
        fetchAllAccounts();
    } else {
        var cloneData = cloneRow.clone();
        cloneData.find('Select').val('');
        cloneData.find('input').val('');
        $('#appendTable').append(cloneData[0]);
    }
}

$(document).on('click','.crossRow',function(){
    $(this).parent().parent().remove();
});

$(document).on("click", ".vendor-notes-plus", function () {
    var length = $(".vendor-notes-clone-divclass").length;
    if (length <= 2) {
        var newnode = $("#vendor-notes-clone-div").clone().find(".notes").val("").end().insertAfter("div.vendor-notes-clone-divclass:last");
        $(".vendor-notes-clone-divclass:not(:eq(0))  .vendor-notes-plus").remove();
        $(".vendor-notes-clone-divclass:not(:eq(0))  .vendor-notes-minus").show();
        if (length == 2) {
            $('.vendor-notes-plus').hide();
        }
    } else {
        $('.vendor-notes-plus').hide();
    }
    newnode.find('.notes').on('keydown', function(event) {
        if (this.selectionStart == 0 && event.keyCode >= 65 && event.keyCode <= 90 && !(event.shiftKey) && !(event.ctrlKey) && !(event.metaKey) && !(event.altKey)) {
            var $t = $(this);
            event.preventDefault();
            var char = String.fromCharCode(event.keyCode);
            $t.val(char + $t.val().slice(this.selectionEnd));
            this.setSelectionRange(1,1);
        }
    });

});

$(document).on('click', '.vendor-notes-minus', function () {
    $(this).parent().parent().remove();
    $('.vendor-notes-plus').show();
});

$(document).on('click','#cancelRecurringBill',function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
        window.location.href = '/Accounting/RecurringBill';
        }
    });
});


$(document).on('click','.change_to',function(){
     $('#change_to_div').show();
    $('#saveNewBill [name="change_to_other"]').val('');
    if($('input[name=change_to]:checked').val() == '1'){
        $('#tenant_div_box').show();
        $('#other_div_box').hide();
        showTenantGrid();
    } else {
        $('#tenant_div_box').hide();
        $('#other_div_box').show();
    }
});


function showTenantGrid(){
   if(coboGridIntiated == '0') {
        $('#change_to_other_tenant').combogrid({
            placeholder: "Select...",
            panelWidth: '780px',
            panelHeight: 'auto',
            url: '/combo-grid-global-ajax',
            idField: 'name',
            textField: 'name',
            mode: 'remote',
            fitColumns: true,
            queryParams: {
                action: 'getUserdata',
                class: 'GlobalComboGrid',
                user_type: '2'
            },
            columns: [[
                {field: 'name', title: 'Tenant Name', width: '173px'},
                {field: 'email', title: 'Email', width: '216px'},
                {field: 'property', title: 'Property', width: '186px'},
                {field: 'building', title: 'Building', width: '100px'},
                {field: 'unit', title: 'Unit', width: '100px'}
            ]],
            onSelect: function (index, row) {
                console.log(row);
                $('#tenant_id').val(row.id);
                $('#propertyid').val(row.property_id);
            }
        });
        $('.textbox').css('width', '70%');
        $('.textbox-addon').css('display', 'none');
      $('.textbox-text').css('width','272px');
        coboGridIntiated = '1';
    }
}

$(document).on('click','.clearFormReset',function() {
        bootbox.confirm("Do you want to clear this form?", function (result) {
            if (result == true) {
                $('#saveNewBill')[0].reset();
            }
        });
});


$(document).on('click','.resetrFormReset',function() {
    var id = $('#bill_id').val();
    bootbox.confirm("Do you want to reset this form?", function (result) {
        if (result == true) {
            window.location.href = base_url+"/Accounting/EditRecurringBill?id="+id;
        }
    });
});

$(document).on('click','.cancelNewBill',function() {
    bootbox.confirm("Do you want to close this form ?", function (result) {
        if (result == true) {
            window.location.href = base_url+"/Accounting/RecurringBill";
        }
    });
});
