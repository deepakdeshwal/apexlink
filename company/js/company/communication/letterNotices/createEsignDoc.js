$(document).ready(function () {

    //jqGrid status
    $('#jqgridOptions').on('change', function () {
        var selected = this.value;
        $('#e-Sign-Documents-table').jqGrid('GridUnload');
        jqGridEsign(selected);
    });
    jqGridEsign('All');
    var protocol = location.port;
    if (protocol != "") {
        var webpath = location.protocol+"//"+location.hostname+":"+protocol;
    }else{
        var webpath = location.protocol+"//"+location.hostname;
    }
    /**
     * jqGrid Initialization function
     * @param status
     */
    function jqGridEsign(status) {
        var table = 'letters_notices_template';
        var columns = ['Letter Name','Description','editable','Action'];
        var select_column = [];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['letters_notices_template.deleted_at, letters_notices_template.updated_at, letters_notices_template.is_editable'];
        var extra_where = [{column:'letter_type',value:'4',condition:'=',table:'letters_notices_template'}];
        var columns_options = [
            {name: 'Letter Name', index: 'letter_name', width: 455,  align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer openPopupSearch'},
            {name: 'Description', index: 'description', width: 450,  align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            /*{name: 'Action', index: 'select', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:'actions',editable:true},*/
            {name:'editable',index:'is_editable', hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'id',classes: 'textCenterTd', title: false, width:450,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: imageRedirect, edittype: 'select',search:false,table:table},

        ];
        var ignore_array = [];
        jQuery("#e-Sign-Documents-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            //sortname: 'updated_at',
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns:extra_columns,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: 'desc',
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of E-Sign Documents",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:400,drag:true,resize:false} // search options
        );
    }

    function imageRedirect(cellValue, options, rowObject){

        if(rowObject !== undefined){
            // console.log(rowObject['editable']);
            var html = "";
            if(rowObject['editable'] == '0'){
                html += "<span class='getLeaseTemplateId' data_id='"+rowObject.id+"'><a><img src='"+webpath+"/company/images/edit-icon.png'  class='edit_doc_icon' title='edit' alt='my image' style='cursor: pointer; '/></a></span>";
            }else if (rowObject['editable'] == '1'){
                html += "<span class='getLeaseTemplateId' data_id='"+rowObject.id+"'><a><img src='"+webpath+"/company/images/edit-icon.png'  class='edit_doc_icon' title='edit' alt='my image' style='cursor: pointer; '/></a></span><span data_id='"+rowObject.id+"'><a><img src='"+webpath+"/company/images/red_x_icon.png'  class='delete_doc_icon' title='delete' alt='my image' style='cursor: pointer; '/></a></span>";
            }else {
                html += "fdsfdsfsdfsdfds";
            }
            return html;
        }

    }

    $(document).on('click','.openPopupSearch',function (e) {
        e.preventDefault();
        var id = $(this).closest("tr").attr('id');
       var esign =  $("#eSignId").val(id);
        $('#eSignFilter').modal('show');
    });

    $(document).on('click','#eSignDivData #ddlRecipeint',function (e) {
        e.preventDefault();
        var id = $("#eSignId").val();
        var getValueData = $("#eSignDivData #ddlRecipeint").val();
        dataTypeDone(id,getValueData);
        /*$('#eSignFilter').modal('show');*/
    });

    function dataTypeDone(id,getValueData) {
        $(document).on('click', '#DoneButtn', function (e) {
            if(getValueData == 'Tenant') {
                console.log("tenant >>");
                $("#searchboxDataEsign").show();
                $("#searchboxDataOwner").hide();
                $("#e-Sign-Documents").hide();
                $("#searchboxDataRental").hide();
                $("#searchboxDataGuest").hide();
                docTemplateDataDetails(id);
            }else if(getValueData == 'Owner') {
                console.log("Owner >>");
                $("#searchboxDataOwner").show();
                $("#searchboxData1").hide();
                $("#e-Sign-Documents").hide();
                $("#searchboxDataGuest").hide();
                $("#searchboxDataEsign").hide();
                docTemplateDataDetails(id);
            }else if(getValueData == 'GuestCard') {
                console.log("GuestCard >>");
                $("#searchboxDataGuest").show();
                $("#searchboxDataOwner").hide();
                $("#searchboxData1").hide();
                $("#e-Sign-Documents").hide();
                $("#searchboxDataEsign").hide();
                docTemplateDataDetails(id);
            }else if(getValueData == 'Rental') {
                console.log("GuestCard >>");
                $("#searchboxDataRental").show();
                $("#searchboxDataGuest").hide();
                $("#searchboxDataOwner").hide();
                $("#searchboxData1").hide();
                $("#e-Sign-Documents").hide();
                $("#searchboxDataEsign").hide();
                docTemplateDataDetails(id);
            }
            $('#eSignFilter').modal('hide');
        });
    }

    function docTemplateDataDetails(id){
        $.ajax({
            type: 'POST',
            url: '/LettersNoticesDoc-Ajax',
            data: {
                class: 'lettersNoticesAjax',
                action: 'docTemplateDataDetails',
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                // console.log(response.data);
                var html ="";
                //console.log("------------------");
                if (response.code == 200) {
                    $("#create_new_letter_div #first_name").val(response.data.templateTitle.letter_name);
                    $("#create_new_letter_div .summernote").summernote("code", response.data.templateTitle.template_html);
                    $("#template_id").val(response.data.templateTitle.id);
                    $.each(response.data.templateTags, function (key, value) {
                        $('#tag_name').append($("<option value = " + value.tag_name + ">" + value.tag_name + "</option>"));
                    });
                    $("#create_new_letter_div #letter_type").val(response.data.templateTitle.letter_type);
                    $("#create_new_letter_div #middle_name").val(response.data.templateTitle.description);
                    $.each(response.data.templateTags, function (key, value) {
                        $('#tag_name').append($("<option value = " + value.tag_name + ">" + value.tag_name + "</option>"));
                    });

                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }





    $(document).on('click','#e-Sign-Documents-table tr td:not(:last-child)',function(e){
        e.preventDefault();
        var id = $(this).closest("tr").attr('id');
        $("#searchTemplateId").val(id);
    });

    $(document).on('click', '.active_tab', function () {
        // /$("#searchboxDataOwner").hide();
        var active_tab = $("#active_tabs li.active a").attr('href');
        $(active_tab+"-table").trigger('reloadGrid');
        console.log('active_tabs>>', active_tab);
        $("#searchboxDataOwner").hide();
        $("#searchboxDataRental").hide();
        $("#searchboxDataGuest").hide();
        $("#searchboxDataEsign").hide();
        $('#active_tab-table').jqGrid('GridUnload');
        $(active_tab).show();
        var $grid = $("#e-Sign-Documents-table"),
            newWidth = $grid.closest(".ui-jqgrid").parent().width();
        $grid.jqGrid("setGridWidth", newWidth, true);
    });
    $(document).on('click', '#searchboxDataOwner .back-building', function () {
        var active_tab = $("#active_tabs li.active a").attr('href');
        console.log('active_tabs>>', active_tab);
        $("#searchboxDataOwner").hide();
        $(active_tab).show();
    });

    /*owner search listing*/
    getOwnerList();
    function getOwnerList(){
        $.ajax({
            type: 'POST',
            url: '/LettersNoticesDoc-Ajax',
            data: {
                class: 'lettersNoticesAjax',
                action: 'getOwnerList',
                // data :window.location.pathname
            },
            success: function (response) {
                var html="";
                var response = JSON.parse(response);
                //console.log(response.data.first);
               // console.log("++++stop++++++");
                for (var i = 0; i < response.data.length; i++) {
                    html += ' <option class="abc" value="' + response.data[i].id + '" data-id="' + response.data[i].id + '">' + response.data[i].first_name +' '+response.data[i].last_name + '</option>';
                    $("#owner_listing_search").html("<option value='all'>Select</option>" + html);
                    //getInspectionBuildingDetails $("#propertyListAll").html("<option value='all'>Select</option>" + html);
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    /*owner search listing*/
    $(document).on("change","#searchboxDataOwner #owner_listing_search",function(){
        var conceptName = $('#searchboxDataOwner #owner_listing_search').find(":selected").text();
        var conceptId = $('#searchboxDataOwner #owner_listing_search').find(":selected").attr("data-id");
        getPropOwnerName(conceptName,conceptId);
    });

    function getPropOwnerName(conceptName,conceptId) {
        $.ajax({
            type: 'POST',
            url: '/LettersNoticesDoc-Ajax',
            data: {
                class: "lettersNoticesAjax",
                action: "getOwnerPropertyList",
                id:conceptId
                // data :window.location.pathname
            },
            success: function (response) {
                var html = ""
                var response = JSON.parse(response);
                console.log(response.data);
                if(response.data.length > 0) {
                    for (var i = 0; i < response.data.length; i++) {
                        html += ' <option value="' + response.data[i].id + '">' + response.data[i].property_name + '</option>';
                    }
                    $("#ownerPropertyListAll").html("<option value='0'>Select</option>" + html);
                }else {
                    $("#ownerPropertyListAll").html("<option value='0'>This Owner has no Property</option>");
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
    }


    /**
     * jqGrid status changes
     */
    $(document).on('change','.common_ddl',function(){
        var grid = $("#letterNoticesOwner-table"),f = [];
        var owner_list = $('#owner_listing_search').val();
        var ownerPropertyListAll = $('#ownerPropertyListAll').val();
        // console.log(ownerPropertyListAll);debugger;
        // var status = $('#jqGridStatus').val();
        f.push({field: "owner_name.id", op: "eq", data: owner_list},{field: "property_name.id", op: "eq", data: ownerPropertyListAll});

        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    });



    //jqGrid status
    $('#jqgridOptions').on('change', function () {
        var selected = this.value;
        $('#letterNoticesOwner-table').jqGrid('GridUnload');
        jqGridOwner(selected);
    });

    jqGridOwner('All');
    function jqGridOwner(status) {

        var table = 'owner_property_owned';
        var checkbox = '<input type="checkbox" id="checkAllletterLandLord" name="checkAllletterLandLord" checked="checked">';
        var columns = ['Landlord Name','Property Name','Tenant','Action',checkbox];
        var select_column = [];
        /* var joins = [{table:'owner_property_owned',column:'user_id',primary:'id',on_table:'users',as:'owner_name'},{table:'owner_property_owned',column:'property_id',primary:'id',on_table:'general_property',as:'property_name'},{table:'tenant_property',column:'user_id',primary:'id',on_table:'users',as:'tenant_name'}];*/
        var joins = [{table:'owner_property_owned',column:'user_id',primary:'id',on_table:'users',as:'owner_name'},{table:'owner_property_owned',column:'property_id',primary:'id',on_table:'general_property',as:'property_name'},{table:'owner_property_owned',column:'property_id',primary:'property_id',on_table:'tenant_property',as:'tenant_property'},{table:'tenant_property',column:'user_id',primary:'id',on_table:'users',as:'tenant_property_name'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [];
        var columns_options = [
            {name:'Landlord Name',index:'name',title:false, width:300,align:"center",searchoptions: {sopt: conditions},table:'owner_name',formatter: ownerName,alias:'landlord_name',classes: 'pointer',attr:[{name:'flag',value:'owner'}]},
            {name:'Property Name',index:'property_name', width:300,align:"center",searchoptions: {sopt: conditions},table:'property_name', classes: 'cursor',alias:'property_name'},
            {name:'Tenant',index:'name', width:255,align:"center",searchoptions: {sopt: conditions},table:'tenant_property_name',change_type: 'tenantname1', extra_columns: ['id'],original_index: 'id',alias:'tenant_id'},
            {name:'Action',index:'id', title: false, width:250,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: pdfRedirect, edittype: 'select',search:true,table:table},
            {name: 'Id', index: 'id', width: 250, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatter},

        ];
        var ignore_array = [];

        jQuery("#letterNoticesOwner-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns:extra_columns,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'owner_property_owned.updated_at',
            sortorder: 'desc',
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Owners",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
            // {top:200,left:200,drag:true,resize:false}
        );
    }

    /**
     * Function to format owner name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function ownerName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            var flagValue = $(rowObject.Action).attr('flag');
            var id = $(rowObject.Action).attr('data_id');
            var flag = '';
            if (flagValue == 'yes') {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a id="flag" href="javascript:void(0);" data_url="/Property/PropertyView?id="' + id + '"><img src="/company/images/Flag.png"></a></span>';
            } else {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
            }
        }
    }

    function actionCheckboxFmatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            return '<input type="checkbox" name="checkLetter[]" class="checkLetter" id="checkLetter_' + rowObject.id + '" data_id="' + rowObject.id + '" checked="checked"/>';
        }
    }

    function pdfRedirect(cellValue, options, rowObject){

        if(rowObject !== undefined){
            // console.log(rowObject['editable']);
            var html = "";
            html += "<span data_id='"+rowObject.id+"' temp_id=''>" +
                "<a class='clsDownloadPDFFromSearch' data_type='download' href='javaScript:void(0);'>" +
                "<img src='"+webpath+"/company/images/pdfDownload.png'  class='pdfDownload_doc_icon' title='download' alt='my image' style='cursor: pointer; width: 18px;height: 18px;'/></a>" +
                "</span>" +
                "<span class='tempalateIdLetter' data_id='"+rowObject.id+"' temp_id=''>" +
                "<a class='clsDownloadPDFFromSearch' data_type='view' href='javascript:void(0);'>" +
                "<img src='"+webpath+"/company/images/PDF-Icon.png'  class='delete_doc_icon' title='preview' alt='my image' style='cursor: pointer; width: 18px;height: 18px;'/></a></span>";

            return html;
        }
    }

    $(document).on("click","#e-Sign-Documents-table .delete_doc_icon",function () {
        var id = $(this).parent().parent().attr('data_id');
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/LettersNoticesDoc-Ajax',
                        data: {id: id,class:'lettersNoticesAjax',action:'deleteLettersNotices'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                $('#e-Sign-Documents-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }

            }
        });
    });

    $(document).on('click','#e-Sign-Documents-table tr',function(e){
        e.preventDefault();
        var id = $(this).closest("tr").attr('id');
        $("#searchTemplateIdOwner").val(id);
    });

    $(document).on('click','#letterNoticesOwner-table tr td .clsDownloadPDFFromSearch',function(e){

        var tenant_id = $(this).parents("tr").attr('id');
        var dataType = $(this).attr("data_type");
        var template_id = $("#searchTemplateIdOwner").val();
        getHtmlPdfConverter(tenant_id,template_id, dataType);
    });

    function getHtmlPdfConverter(tenant_id,template_id, dataType){
        $.ajax({
            type: 'post',
            url: '/LettersNoticesDoc-Ajax',
            data: {
                tenant_id: tenant_id,
                template_id:template_id,
                class:'lettersNoticesAjax',
                action:'getHtmlPdfConverter'
            },
            success: function (response) {
                var response = JSON.parse(response);


                if (response.status == 'success' && response.code == 200) {
                    console.log("here");
                    console.log(response);
                    if (dataType == "download"){
                        var splitName = response.data.split("/");
                        var fileName = splitName.reverse();
                        var link=document.createElement('a');
                        document.body.appendChild(link);
                        link.target="_blank";
                        link.download=fileName[0];
                        link.href=response.data;
                        link.click();
                    }else{
                        window.open(response.data, "_blank");
                    }

                    setTimeout(function () {
                    }, 2000);
                } else if(response.code == 500) {
                    toastr.warning(response.message);
                } else {
                    toastr.error(response.message);
                }

            }
        });
    }
    $("#checkAllletterLandLord").click(function () {
        $(".checkLetter").prop('checked', $(this).prop('checked'));
    });

    $(document).on("click", '#sendMailLetterLandLord', function () {
        var searchTemplateId = $(".searchbox_ownerPropertyAll #searchTemplateIdOwner").val();
        // alert(searchsad);debugger;
        favorite = [];
        var no_of_checked = $('[name="checkLetter[]"]:checked').length

        if (no_of_checked == 0) {
            toastr.error('Please select atleast one LandLord.');
            return false;
        }
        $.each($("input[name='checkLetter[]']:checked"), function () {
            favorite.push($(this).attr('data_id'));
        });
        $.ajax({
            type: 'post',
            url: '/LettersNoticesDoc-Ajax',
            data: {class: 'lettersNoticesAjax', action: 'getTemplateDataEmail', 'tenantTemplate_ids': favorite,'searchTemplateId':searchTemplateId},
            success: function (response) {
                var response = JSON.parse(response);
                /* if (response.status == 'success' && response.code == 200) {
                     $("#print_hoa").modal('show');
                     $("#modal-body-hoa").html(response.html)
                 } else {
                     toastr.warning('Record not updated due to technical issue.');
                 }*/
            }
        });
    });


    /*Get Property/Building Details Name List*/
    getPropertyList();
    function getPropertyList(){
        $.ajax({
            type: 'POST',
            url: '/LettersNoticesDoc-Ajax',
            data: {
                class: 'lettersNoticesAjax',
                action: 'getPropertyList',
                // data :window.location.pathname
            },
            success: function (response) {
                var html="";
                var response = JSON.parse(response);
                /*console.log(response);*/
                for (var i = 0; i < response.data.length; i++) {
                    html += ' <option class="abc" value="' + response.data[i].id + '" data-id="' + response.data[i].id + '">' + response.data[i].property_name + '</option>';
                    $("#propertyListAll2,#propertyListAll3,#propertyListAllEsign").html("<option value='all'>Select</option>" + html);
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /*Search Properties*/
    /*Search Properties*/
    $(document).on('change', '.tab-content select', function () {

        var grid = $("#letterNoticesGuest-table"), f = [];
        var value = $(this).val();
        var search = $(this).text();
        if (value != '0') {
            f.push({field: "general_property.id", op: "eq", data: value});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData, {filters: JSON.stringify(f)});
            grid.trigger("reloadGrid", [{page: 1, current: true}]);
        }
    });
    /*Search Properties*/
    /*Search Properties*/

    $('#jqgridOptions').on('change', function () {
        var selected = this.value;
        $('#letterNoticesGuest-table').jqGrid('GridUnload');
        jqGrid(selected);
    });

    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGridGuest('All');

    function jqGridGuest(status) {
        if(jqgridNewOrUpdated == 'true'){
            var sortOrder = 'desc';
            var sortColumn = 'u1.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'u1.name';
        }
        var table = 'lease_guest_card';
        //var rentCurrSymbol = "Rent ("+currencySymbol+")";
        var checkbox = '<input type="checkbox" id="checkAllletter" class="checkAllletter" name="checkAllletter" checked="checked">';
        var columns = ['Prospect Name', 'Property Name','user_id', 'Guest Card Number', ' Move In Date', 'Status','Unit_no', 'Building Unit ID','Action',checkbox];
        //if(status===0) {
        var select_column = [];
        //}
        var joins = [{table: 'lease_guest_card', column: 'user_id', primary: 'id', on_table: 'users',as:'u1'},
            {table: 'lease_guest_card', column: 'unit_id', primary: 'id', on_table: 'unit_details'},
            {table: 'unit_details', column: 'property_id', primary: 'id', on_table: 'general_property'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [{column: 'user_type', value: '6', condition: '=', table: 'u1'}];
        var columns_options = [

            {name:'Prospect Name',index:'name', width:200,align:"left",searchoptions: {sopt: conditions},table:'u1'},
            {name:'Property Name',index:'property_name', width:187,searchoptions: {sopt: conditions},table:'general_property'},
            {name:'user_id',index:'user_id', width:150,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'Guest Card Number',index:'id', width:150, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Move In Date',index:'expected_move_in',searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Status',index:'status', width:150,align:"center",searchoptions: {sopt: conditions},table:table,formatter: statusFormatter},
            {name:'Unit_no',index:'unit_no',hidden:true, width:150,align:"center",searchoptions: {sopt: conditions},table:'unit_details'},
            /*{name:'Building Unit ID',index:'id', width:80,align:"center",searchoptions: {sopt: conditions},table:'unit_details',formatter:getUnitDetails,alias:'unit_data_id'},*/
            {name:'Building Unit ID',index:'unit_prefix', width:150, align:"left",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
            {name:'Action',index:'id', title: false, width:200,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: pdfRedirect, edittype: 'select',search:true,table:table},
            //{name:'Checkbox',index:'id', width:200, align:"center",sortable:false,searchoptions: {sopt: conditions},table:table,formatter: myPrintCheckboxFormatter,search:true},
            {name: 'Id', index: 'id', width: 170, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatter},
        ];
        var ignore_array = [];
        jQuery("#letterNoticesGuest-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Guest",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:400,drag:true,resize:false} // search options
        );
    }

    function statusFormatter(cellValue, options, rowObject) {
        if (cellValue == 1)
            return "Active";
        else if (cellValue == '0')
            return "Inactive";
        else if (cellValue == '2')
            return "Archived";
        else if (cellValue == '3')
            return "View Scheduled";
        else if (cellValue == '4')
            return "Application Generated";
        else if (cellValue == '5')
            return "Lease Generated";
        else
            return '';
    }

    function statusFormatter1(cellValue, options, rowObject) {
        console.log(cellValue);

        if (cellValue == '1')
            return "Active";
        else if (cellValue == '2')
            return "Archived";
        else if (cellValue == '3')
            return "Lease Generated";
        else if (cellValue == '4')
            return "Approved";
        else if (cellValue == '5')
            return "Declined";
        else
            return '';
    }

    $(document).on('click','#letterNoticesGuest-table tr td .clsDownloadPDFFromSearch',function(e){
        var tenant_id = $(this).parents("tr").attr('id');
        var dataType = $(this).attr("data_type");
        var template_id = $("#searchTemplateId").val();
        //alert(template_id);
        getHtmlPdfConverter(tenant_id,template_id, dataType);
    });

    $(document).on("click", '#sendMailLetterGuest', function () {
        var searchTemplateId = $("#searchboxDataGuest .searchboxPropertyAll #searchTemplateId").val();
         alert(searchsad);debugger;
        favorite = [];
        var no_of_checked = $('[name="checkLetter[]"]:checked').length

        if (no_of_checked == 0) {
            toastr.error('Please select atleast one LandLord.');
            return false;
        }
        $.each($("input[name='checkLetter[]']:checked"), function () {
            favorite.push($(this).attr('data_id'));
        });
        $.ajax({
            type: 'post',
            url: '/LettersNoticesDoc-Ajax',
            data: {class: 'lettersNoticesAjax', action: 'getTemplateDataEmail', 'tenantTemplate_ids': favorite,'searchTemplateId':searchTemplateId},
            success: function (response) {
                var response = JSON.parse(response);
                /* if (response.status == 'success' && response.code == 200) {
                     $("#print_hoa").modal('show');
                     $("#modal-body-hoa").html(response.html)
                 } else {
                     toastr.warning('Record not updated due to technical issue.');
                 }*/
            }
        });
    });

    /*Search Properties*/
    $(document).on('change', '.tab-content select', function () {

        var grid = $("#letterNoticesRental-table"), f = [];
        var value = $(this).val();
        var search = $(this).text();
        if (value != '0') {
            f.push({field: "general_property.id", op: "eq", data: value});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData, {filters: JSON.stringify(f)});
            grid.trigger("reloadGrid", [{page: 1, current: true}]);
        }
    });
    /*Search Properties*/
    jqGridRental('All');
    function jqGridRental(status, deleted_at) {
        if(jqgridNewOrUpdated == 'true'){
            var sortOrder = 'desc';
            var sortColumn = 'users.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }
        var table = 'users';
        var checkbox = '<input type="checkbox" id="checkAllletter" class="checkAllletter" name="checkAllletter" checked="checked">';
        var columns = ['Applicant Name', 'Property Name', 'Unit', ' Move In Date', 'Status', 'Action',checkbox];
        var select_column = [];
        var joins = [{table: 'users', column: 'id', primary: 'user_id', on_table: 'company_rental_applications'},{table: 'company_rental_applications', column: 'prop_id', primary: 'id', on_table: 'general_property'},{table: 'company_rental_applications', column: 'unit_id', primary: 'id', on_table: 'unit_details'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['unit_details.unit_no'];

            var extra_where = [{column: 'user_type', value: '10', condition: '=', table: 'users'},{column: 'record_status', value: '1', condition: '=', table: 'users'}];

        // var pagination =[];

        var columns_options = [

            /*{name:'Prospect Name',index:'name', width:90,align:"center",searchoptions: {sopt: conditions},table:'users',},*/
            {name:'Applicant Name',index:'name', width:200,align:"left",searchoptions: {sopt: conditions},table:table},
            {name:'Property Name',index:'property_name', width:200,searchoptions: {sopt: conditions},table:'general_property'},
            {name:'Unit',index:'unit_prefix', width:200, align:"center",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
            {name:'Move In Date',width:200,index:'exp_move_in',searchoptions: {sopt: conditions},table:'company_rental_applications',change_type:'date'},
            {name:'Status',index:'status', width:185,align:"center",searchoptions: {sopt: conditions},table:'company_rental_applications',formatter: statusFormatter1},
            {name:'Action',index:'id', title: false, width:200,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: pdfRedirect, edittype: 'select',search:true,table:table},
            //{name:'Checkbox',index:'id', width:200, align:"center",sortable:false,searchoptions: {sopt: conditions},table:table,formatter: myPrintCheckboxFormatter,search:true},
            {name: 'Id', index: 'id', width: 170, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatter},

        ];
        var ignore_array = [];
        jQuery("#letterNoticesRental-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "users",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Rental Applications",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }

    $(document).on('click','#letterNoticesRental-table tr td .clsDownloadPDFFromSearch',function(e){
        var tenant_id = $(this).parents("tr").attr('id');
        var dataType = $(this).attr("data_type");
        var template_id = $("#searchTemplateId").val();
        //alert(template_id);
        getHtmlPdfConverter(tenant_id,template_id, dataType);
    });

    /*tenant grid*/
    jqGridEsignTenant('All');

    function jqGridEsignTenant(status) {
        if(jqgridNewOrUpdated == 'true'){
            var sortOrder = 'desc';
            var sortColumn = 'users.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }
        var table = 'users';
        //var rentCurrSymbol = "Rent ("+currencySymbol+")";
        var checkbox = '<input type="checkbox" id="checkAllletter" class="checkAllletter" name="checkAllletter" checked="checked">';
        var columns = ['Tenant Name','Phone','Email','Action',checkbox];
        var select_column = [];
        var joins = [{table:'users',column:'id',primary:'user_id',on_table:'tenant_property'},
            {table:'users',column:'id',primary:'user_id',on_table:'tenant_details'},
            {table:'users',column:'id',primary:'user_id',on_table:'tenant_lease_details'},
            {table:'users',column:'id',primary:'user_id',on_table:'tenant_phone'},
            {table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},
            {table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [{column:'user_type',value:'2',condition:'=',table:'users'},{column:'record_status',value:'1',condition:'=',table:'tenant_details'},{column:'record_status',value:'1',condition:'=',table:'tenant_lease_details'}];
        //var pagination=[];
        var columns_options = [

            /*{name:'Tenant Name',index:'id', width:90,align:"left",searchoptions: {sopt: conditions},table:table,formatter:titleCase},*/
            {name:'Tenant Name',index:'id', width:300,align:"center",searchoptions: {sopt: conditions},table:table,change_type: 'tenantname1', extra_columns: ['id'],original_index: 'id'},
            {name:'Phone',index:'phone_number', width:300,searchoptions: {sopt: conditions},table:'tenant_phone'},
            {name:'Email',index:'email', width:340, align:"left",searchoptions: {sopt: conditions},table:table},
            /* {name:'Property Name',index:'property_name', width:250, align:"center",searchoptions: {sopt: conditions},table:'general_property'},*/
            /*{name:'Unit Number',index:'unit_id', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_property',formatter:getUnitDetails},*/
            /* {name:'Unit Number',index:'unit_prefix', width:250, align:"center",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
             {name:'Rent (USh)',index:'rent_amount', width:205, align:"center",searchoptions: {sopt: conditions},table:'tenant_lease_details'},*/
            /* {name:'Lease ID',index:'user_id', width:100,searchoptions: {sopt: conditions},table:'tenant_lease_details'},
             {name:'Days Remaining',index:'days_remaining', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details',formatter:statusFormatter2},
             {name:'Status',index:'status', width:80,align:"left",searchoptions: {sopt: conditions},table:'tenant_details',formatter:statusFormatter},*/
            {name:'Action',index:'id', title: false, width:247,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: pdfRedirect, edittype: 'select',search:true,table:table},
            //{name:'Checkbox',index:'id', width:200, align:"center",sortable:false,searchoptions: {sopt: conditions},table:table,formatter: myPrintCheckboxFormatter,search:true},
            {name: 'Id', index: 'id', width: 170, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatter},
        ];
        var ignore_array = [];
        jQuery("#letterNoticesTenantEsign-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "users",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Tenant",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:400,drag:true,resize:false} // search options
        );
    }

    function pdfRedirect(cellValue, options, rowObject){

        if(rowObject !== undefined){
            // console.log(rowObject['editable']);
            var html = "";
            html += "<span data_id='"+rowObject.id+"' temp_id=''><a class='clsDownloadPDFFromSearch' data_type='download' href='javaScript:void(0);'><img src='"+webpath+"/company/images/pdfDownload.png'  class='pdfDownload_doc_icon' title='Download' alt='my image' style='cursor: pointer; width: 18px;height: 18px;'/></a></span><span class='tempalateIdLetter' data_id='"+rowObject.id+"' temp_id=''><a class='clsDownloadPDFFromSearch' data_type='view' href='javascript:void(0);'><img src='"+webpath+"/company/images/PDF-Icon.png'  class='delete_doc_icon' title='Preview' alt='my image' style='cursor: pointer; width: 18px;height: 18px;'/></a></span>";

            return html;
        }

    }

    function actionCheckboxFmatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            return '<input type="checkbox" name="checkLetter[]" class="checkLetter" id="checkLetter_' + rowObject.id + '" data_id="' + rowObject.id + '" checked="checked"/>';
        }
    }

    $(document).on('change', '.tab-content select', function () {

        var grid = $("#letterNoticesTenantEsign-table"), f = [];
        var value = $(this).val();
        var search = $(this).text();
        if (value != '0') {
            f.push({field: "general_property.id", op: "eq", data: value});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData, {filters: JSON.stringify(f)});
            grid.trigger("reloadGrid", [{page: 1, current: true}]);
        }
    });
    $(".checkAllletter").click(function () {
        $(".checkLetter").prop('checked', $(this).prop('checked'));
    });

    $(document).on('click','#letterNoticesTenantEsign-table tr td .clsDownloadPDFFromSearch',function(e){
        var tenant_id = $(this).parents("tr").attr('id');
        var dataType = $(this).attr("data_type");
        var template_id = $("#searchTemplateId").val();
        //alert(template_id);
        getHtmlPdfConverter(tenant_id,template_id, dataType);
    });

    var testClass;
    $(document).on('click','#active_tabs .active_tab',function(e){
        testClass = $(this).hasClass("esignBtn");
    });

    $(document).on("click", '#sendMailLetterEsignDoc', function () {

        $(".loader").show();
        var searchTemplateId = $(".searchboxPropertyAll #searchTemplateId").val();
        // alert(searchsad);debugger;
        favorite = [];
        var no_of_checked = $('#letterNoticesTenantEsign-table [name="checkLetter[]"]:checked').length

        if (no_of_checked == 0) {
            toastr.error('Please select atleast one Lease.');
            return false;
        }
        $.each($("#letterNoticesTenantEsign-table input[name='checkLetter[]']:checked"), function () {
            favorite.push($(this).attr('data_id'));
        });
        if (testClass){
            var actionName = 'getTemplateDataEmail2';
        }else{
            var actionName = 'getTemplateDataEmail';
        }
        $.ajax({
            type: 'post',
            url: '/LettersNoticesDoc-Ajax',
            data: {
                class: 'lettersNoticesAjax',
                action: actionName,
                'tenantTemplate_ids': favorite,
                'searchTemplateId': searchTemplateId,
                'esignClass':testClass
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log("tenantEmail >>",response);
                if (response.status == 'success' && response.code == 200) {
                    $(".loader").hide();
                    toastr.success("Email sent successfully.");
                } else {
                    toastr.warning('Mail is not send due to technical issue.');
                }
            }
        });
    });


    $(document).on('click','#searchboxDataEsign .back-link-tenant,#searchboxDataEsign .back-link-guest,#searchboxDataEsign .back-link-rental',function () {
            $('#searchboxDataEsign').hide();
            $('#e-Sign-Documents').show();
    });


});