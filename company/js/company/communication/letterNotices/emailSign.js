$(document).ready(function () {
    function getParameterByName(name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

        getsignatureonreload();

    function getsignatureonreload() {
        var templateId = getParameterByName('templateId');
        $.ajax({

            type: 'Post',
            url: '/EsignUser/saveEsignImage',
            data: {
                class: "lettersNoticesAjax",
                action: "getsignatureonreload",
                templateId:templateId
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                  var html=$(".orignatorUser").html();
                  if(data.originator_data!=false) {
                      setTimeout(function () {
                          $(".orignatorUser").html('Originator Signature:<img style="width: 100%;" src="' + data.originator_data.signature_image + '">');
                          $(".orignatorUser").css('font-weight', 'bold');
                          $(".orignatorUserText").html('Originator Entered Text:' + data.originator_data.signature_text + '');
                          $(".orignatorUserText").css('font-weight', 'bold');
                      }, 1000)
                  }
                    if(data.signer_data!=false) {
                        setTimeout(function () {
                            $(".signerUser").html('Signer Signature:<img style="width: 100%;" src="' + data.signer_data.signature_image + '">');
                            $(".signerUser").css('font-weight', 'bold');
                            $(".signerUserText").html('Signer Entered Text:' + data.signer_data.signature_text + '');
                            $(".signerUserText").css('font-weight', 'bold');
                        }, 1000)
                    }
                   }else{
                    toastr.error(data.message);
                }
            }
        });

    }

    $(document).on("click","#btnDownload",function () {
        window.print();
        return false;
    });

    //getSignatureInfo();
    docTemplateDataDetails();
    function docTemplateDataDetails(){
        var id = $(".templateEmailData").attr('data-id');
        var user_id = $(".templateUserId").attr('data-id');
        $.ajax({
            type: 'POST',
            url: '/LettersNoticesDoc-Ajax',
            data: {
                class: 'lettersNoticesAjax',
                action: 'docTemplateDataDetails',
                id: id,
                userId:user_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var userId = user_id;
                console.log("dataHereYou r>>",response.data);
                /*$("#currUserData").val(response.data.currentLoggedIn);*/
                var html = response.data.templateTitle.template_html;
                var userType =response.data.user_type;
                $("#currUserType").val(userType);
                if(response.data.get_signature != "") {
                    var url = response.data.get_signature.signature_image;
                    var img = '<img style="width: 100%;" src="'+url+'">';
                    var textFeild = response.data.get_signature.signature_text;
                    var spanData = '<span class="graggalbeElem" style="'+response.data.get_signature.style+'">'+textFeild+'</span>';
                    if (userType != "PM") {
                        var replaceText = html.replace('[Originator_Signature]', '<span class="orignatorUser">[Originator_Signature]<span>', html);
                        if (url != "") {
                            replaceText = replaceText.replace('[Signer_Signature]', '<b>Signer Signature:</b><span class="graggalbeElem">' + img + '<span>', replaceText);
                        } else {
                            replaceText = replaceText.replace('[Signer_Signature]', '<span class="esignatureDoc" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Signer_Signature]<span>', replaceText);
                        }
                        if (textFeild != "") {
                            replaceText = replaceText.replace('[Signer_Entered_Text]', '<b>Signer Text Signature:</b>' + spanData, replaceText);
                        } else {
                            replaceText = replaceText.replace('[Signer_Entered_Text]', '<span class="esignatureDoc" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Signer_Entered_Text]<span>', replaceText);
                        }
                        replaceText = replaceText.replace('[Signer_Entered_Date]', '<span class="esignatureDoc" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Signer_Entered_Date]<span>', replaceText);
                        replaceText = replaceText.replace('[Signer_Checkbox]', '<span class="" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Signer_Checkbox]<span>', replaceText);
                        replaceText = replaceText.replace('[Originator_Entered_Text]', '<span class="orignatorUserText" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Originator_Entered_Text]<span>', replaceText);
                        replaceText = replaceText.replace('[Originator_Entered_Date]', '<span class="" data-id="" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Originator_Entered_Date]<span>', replaceText);
                    }else{
                        var replaceText = html.replace('[Signer_Signature]', '<span class="signerUser">[Signer_Signature]<span>', html);
                        if (url != "") {
                            replaceText = replaceText.replace('[Originator_Signature]', '<b>Originator Signature:</b><span class="graggalbeElem">' + img + '<span>', replaceText);
                        } else {
                            replaceText = replaceText.replace('[Originator_Signature]', '<span class="esignatureDoc" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Originator_Signature]<span>', replaceText);
                        }
                        if (textFeild != "") {
                            replaceText = replaceText.replace('[Originator_Entered_Text]', '<b>Originator Text Signature:</b>' + spanData, replaceText);
                        } else {
                            replaceText = replaceText.replace('[Originator_Entered_Text]', '<span class="esignatureDoc" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Originator_Entered_Text]<span>', replaceText);
                        }
                        replaceText = replaceText.replace('[Signer_Entered_Text]', '<span class="signerUserText" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Signer_Entered_Text]<span>', replaceText);
                        replaceText = replaceText.replace('[Signer_Entered_Date]', '<span class="" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Signer_Entered_Date]<span>', replaceText);
                        replaceText = replaceText.replace('[Signer_Checkbox]', '<span class="" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Signer_Checkbox]<span>', replaceText);
                        replaceText = replaceText.replace('[Originator_Entered_Date]', '<span class="" data-id="" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Originator_Entered_Date]<span>', replaceText);
                    }

                 }else {

                    if (userType == "PM") {
                        var replaceText = html.replace('[Originator_Signature]', '<span class="esignatureDoc" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Originator_Signature]<span>', html);
                        replaceText = replaceText.replace('[Signer_Signature]', '<span class="" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Signer_Signature]<span>', replaceText);
                        replaceText = replaceText.replace('[Signer_Entered_Text]', '<span class="" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Signer_Entered_Text]<span>', replaceText);
                        replaceText = replaceText.replace('[Signer_Entered_Date]', '<span class="" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Signer_Entered_Date]<span>', replaceText);
                        replaceText = replaceText.replace('[Signer_Checkbox]', '<span class="" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Signer_Checkbox]<span>', replaceText);
                        replaceText = replaceText.replace('[Originator_Entered_Text]', '<span class="esignatureDoc" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Originator_Entered_Text]<span>', replaceText);
                        replaceText = replaceText.replace('[Originator_Entered_Date]', '<span class="" data-id="" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Originator_Entered_Date]<span>', replaceText);
                    } else {
                        var replaceText = html.replace('[Originator_Signature]', '<span class="" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Originator_Signature]<span>', html);
                        replaceText = replaceText.replace('[Signer_Signature]', '<span class="esignatureDoc" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Signer_Signature]<span>', replaceText);
                        replaceText = replaceText.replace('[Signer_Entered_Text]', '<span class="esignatureDoc" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Signer_Entered_Text]<span>', replaceText);
                        replaceText = replaceText.replace('[Signer_Entered_Date]', '<span class="esignatureDoc" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Signer_Entered_Date]<span>', replaceText);
                        replaceText = replaceText.replace('[Signer_Checkbox]', '<span class="" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Signer_Checkbox]<span>', replaceText);
                        replaceText = replaceText.replace('[Originator_Entered_Text]', '<span class="" data-id="' + userId + '" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Originator_Entered_Text]<span>', replaceText);
                        replaceText = replaceText.replace('[Originator_Entered_Date]', '<span class="" data-id="" id="' + Math.floor(100000000 + Math.random() * 900000000) + '">[Originator_Entered_Date]<span>', replaceText);
                    }
                }


                $("#eSignDataTemplate").append(replaceText);
                $(".graggalbeElem").draggable();
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $('#redrawSignature').signature({disabled: true});
    $('#redrawSignature canvas').prop('id',"canvas2");

    function getParameters(name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.search);
        return (results !== null) ? results[1] || 0 : false;
    }

    $(document).on("change","input[name='typing']",function () {
        if($(this).val() == "t"){
            $(".sketchingarea").hide();
            $(".typingarea").show();
        }else{
            $(".sketchingarea").show();
            $(".typingarea").hide();
        }
    });
    $('input[name="selectfonts"]').on('change', function() {
        var value = $(this).val();
        applyFont(value,".enterplaceholder,.typingtext");
    });
    function applyFont(font, classname) {
        font = font.replace(/\+/g, ' ');
        font = font.split(':');
        var fontFamily = font[0];
        var fontWeight = font[1] || 400;
        $(classname).css({fontFamily:"'"+fontFamily+"'", fontWeight:fontWeight});
        $(".typingtext").attr("data-font",fontFamily);
    }

    $(document).on("click","#eSignDataTemplate .esignatureDoc", function () {
        var tempId = getParameters('templateId');
        var userId = getParameters('userId');
        var elem = $(this).attr("id");
        var textData = $(this).text();

        if(textData == '[Originator_Signature]' || textData == '[Signer_Signature]'){
            $("#eSignModalLetter .submitsign a").attr("data-user",userId);
            $("#eSignModalLetter .submitsign a").attr("data-temp",tempId);
           // $("#eSignModalLetter .submitsign a").attr("data-type",'T');
            $("#eSignModalLetter").modal('show');
            $(".typingSketching ").click();
            $( ".typingData" ).prop( "disabled", true );
            $('#defaultSignature').signature({syncField: '#signatureJSON'});
            $('#defaultSignature').signature('option', 'syncFormat', 'PNG');
        }else if(textData == '[Originator_Entered_Text]' || textData == '[Signer_Entered_Text]'){
            $("#eSignModalLetter .submitsign a").attr("data-user",userId);
            $("#eSignModalLetter .submitsign a").attr("data-temp",tempId);

           // $("#eSignModalLetter .submitsign a").attr("data-type",'T');
            $("#eSignModalLetter").modal('show');
            $(".typingData ").click();
            $( ".typingSketching" ).prop( "disabled", true );
            $('#defaultSignature').signature({syncField: '#signatureJSON'});
            $('#defaultSignature').signature('option', 'syncFormat', 'PNG');
        }else{
            $("#eSignModalLetter .submitsign a").attr("data-user",userId);
            $("#eSignModalLetter .submitsign a").attr("data-temp",tempId);
           // $("#eSignModalLetter .submitsign a").attr("data-type",'T');
            $("#eSignModalLetter").modal('show');
            $( ".typingSketching" ).prop( "disabled", false );
            $( ".typingData" ).prop( "disabled", false );
            $('#defaultSignature').signature({syncField: '#signatureJSON'});
            $('#defaultSignature').signature('option', 'syncFormat', 'PNG');
        }

        $("#eSignModalLetter .submitsign a").attr("data-elem",elem);

    });
    /*Clear Signature*/
    $(document).on("click",'.clearSignature',function() {
        $('#defaultSignature').signature('clear');
    });
    /*Clear Signature*/
    $(document).on("click",".submitsign a",function(e){
        e.preventDefault();
        $('#redrawSignature').signature('enable').signature('draw', $('#signatureJSON').val());
        var dataUser = $(this).attr("data-user");
        var dataType = $(this).attr("data-type");
        var tempId = $(this).attr("data-temp");
        var dataElem = $(this).attr("data-elem");
        var fontText = $(".typingtext").val();
        var fontfamily = $(".typingtext").attr("data-font");

        setTimeout(function () {
            var can = document.getElementById("canvas2");
            var pngUrl = can.toDataURL();

            saveSignatureImage(pngUrl,dataUser,dataType,tempId,fontText,fontfamily,dataElem);
        },1000);
    });

    function saveSignatureImage(pngUrl,dataUser,dataType,tempId,fontText,fontfamily,dataElem) {
        var inputVal = $("input[name='typing']:checked").val();
        var styleAttr = $(".typingarea .typingtext").attr("style");

        $.ajax({
            type: 'post',
            url: '/EsignUser/saveEsignImage',
            data: {
                class: "lettersNoticesAjax",
                action: "saveSignatureImage",
                id: dataUser,
               // dataType: dataType,
                tempId: tempId,
                inputVal: inputVal,
                img: pngUrl,
                fontText: fontText,
                fontfamily: fontfamily,
                styleAttr: styleAttr
            },
            success: function (response) {
                var data = $.parseJSON(response);
                var url = data.data.signature_image;
                var img = '<img src="'+url+'">';
                var textFeild = data.data.signature_text;
                var spanData = '<span style="'+data.data.style+'">'+textFeild+'</span>';
                if (data.status == "success") {
                if(url != '') {
                    //$("#clearSignature").trigger('click');
                    $("#eSignModalLetter").modal('hide');
                    $(".graggalbeElem").draggable();

                    $("#" + dataElem).html(img);
                }
                if(textFeild != ""){
                    $("#eSignModalLetter").modal('hide');
                    $("#"+dataElem).html(spanData);
                }

                    toastr.success(data.message);
                    return false;
                }else{
                    toastr.error(data.message);
                }
            }
        });

        return false;
    }
    /*submit sign start PM/Other*/
    $(document).on('click','#btnSaveTemplate',function(event) {
        var tempId = $(".templateEmailData").attr('data-id');
        var user_id = $(".templateUserId").attr('data-id');
        var htmlTemplate = $("#eSignDataTemplate").html();
        var currLoggUserid = $("#currUserData").val();
        var currUserType = $("#currUserType").val();
        //console.log('currUserType>>>',currLoggUserid);
        $.ajax({
            type: 'Post',
            url: '/EsignUser/saveEsignImage',
            data: {
                class: "lettersNoticesAjax",
                action: "updateColmnHtmlData",
                tempId: tempId,
                user_id :user_id,
                htmlTemplate :htmlTemplate,
                currLoggUserid:currLoggUserid
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    toastr.success(data.message);
                    return false;
                }else{
                    toastr.error(data.message);
                }
            }
        });

    });
    /*submit sign end PM/Other*/

});