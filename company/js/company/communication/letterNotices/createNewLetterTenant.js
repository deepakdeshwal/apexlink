$(document).ready(function () {

    $(document).on('click','#searchboxDataEsign .back-link-tenantF',function () {
        $('#searchboxData1').hide();
        $('#Tenant-Documents').show();
    });
    // $(window).on("resize", function () {
    //     var $grid = $("#Tenant-Documents-table"),
    //         newWidth = $grid.closest(".ui-jqgrid").parent().width();
    //     $grid.jqGrid("setGridWidth", newWidth, true);
    // });
    $(window).on("resize", function () {
        var $grid = $("#Tenant-Documents-table"),
            newWidth = $grid.closest(".ui-jqgrid").parent().width();
        $grid.jqGrid("setGridWidth", newWidth, true);
    });

    //jqGrid status
    $('#jqgridOptions').on('change', function () {
        var selected = this.value;
        $('#Tenant-Documents-table').jqGrid('GridUnload');
        jqGrid1(selected);
    });



    jqGrid1('All');
    var protocol = location.port;
    if (protocol != "") {
        var webpath = location.protocol+"//"+location.hostname+":"+protocol;
    }else{
        var webpath = location.protocol+"//"+location.hostname;
    }
    /**
     * jqGrid Initialization function
     * @param status
     */
    function jqGrid1(status) {
        /*if(jqgridNewOrUpdated == 'true')
      {
           var sortOrder = 'desc';
           var sortColumn = 'letters_notices_template.updated_at';
       } else {
           var sortOrder = 'asc';
           var sortColumn = 'letters_notices_template.letter_name';
       }*/
        var table = 'letters_notices_template';
        var columns = ['Letter Name','Description','editable','Action'];
        var select_column = [];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['letters_notices_template.deleted_at, letters_notices_template.updated_at, letters_notices_template.is_editable'];
        var extra_where = [{column:'letter_type',value:'2',condition:'=',table:'letters_notices_template'}];
        var columns_options = [
            {name: 'Letter Name', index: 'letter_name', width: 455,  align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer letterNotice'},
            {name: 'Description', index: 'description', width: 450,  align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            /*{name: 'Action', index: 'select', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:'actions',editable:true},*/
            {name:'editable',index:'is_editable', hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'id',classes: 'textCenterTd', title: false, width:450,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: imageRedirect, edittype: 'select',search:false,table:table},

        ];
        var ignore_array = [];
        jQuery("#Tenant-Documents-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            //sortname: 'updated_at',
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns:extra_columns,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: 'desc',
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Tenant Documents",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:400,drag:true,resize:false} // search options
        );
    }

    function imageRedirect(cellValue, options, rowObject){

        if(rowObject !== undefined){
            // console.log(rowObject['editable']);
            var html = "";
            if(rowObject['editable'] == '0'){
                html += "<span class='getLeaseTemplateId' data_id='"+rowObject.id+"'><a><img src='"+webpath+"/company/images/edit-icon.png'  class='edit_doc_icon' title='edit' alt='my image' style='cursor: pointer; '/></a></span>";
            }else if (rowObject['editable'] == '1'){
                html += "<span class='getLeaseTemplateId' data_id='"+rowObject.id+"'><a><img src='"+webpath+"/company/images/edit-icon.png'  class='edit_doc_icon' title='edit' alt='my image' style='cursor: pointer; '/></a></span><span data_id='"+rowObject.id+"'><a><img src='"+webpath+"/company/images/red_x_icon.png'  class='delete_doc_icon' title='delete' alt='my image' style='cursor: pointer; '/></a></span>";
            }else {
                html += "<span data_id='"+rowObject.id+"'><a><img src='"+webpath+"/company/images/edit-icon.png'  class='nonEditablePdf' title='edit' alt='my image' style='cursor: pointer; background: gray;'/></a></span>";
            }
            return html;
        }

    }
    $(document).on('click','#Tenant-Documents tr td:not(:last-child)',function(e){
        e.preventDefault();
        var id = $(this).closest("tr").attr('id');
        // alert(id);
        docTemplateDataDetails(id);

    });
    function docTemplateDataDetails(id){
        $.ajax({
            type: 'POST',
            url: '/LettersNoticesDoc-Ajax',
            data: {
                class: 'lettersNoticesAjax',
                action: 'docTemplateDataDetails',
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var html ="";
                //console.log("------------------");
                if (response.code == 200) {
                        $("#create_new_letter_div #first_name").val(response.data.templateTitle.letter_name);
                        $("#create_new_letter_div .summernote").summernote("code", response.data.templateTitle.template_html);
                        $("#template_id").val(response.data.templateTitle.id);
                        $.each(response.data.templateTags, function (key, value) {
                            $('#tag_name').append($("<option value = " + value.tag_name + ">" + value.tag_name + "</option>"));
                        });
                        $("#create_new_letter_div #letter_type").val(response.data.templateTitle.letter_type);
                        $("#create_new_letter_div #middle_name").val(response.data.templateTitle.description);
                        $.each(response.data.templateTags, function (key, value) {
                            $('#tag_name').append($("<option value = " + value.tag_name + ">" + value.tag_name + "</option>"));
                        });

                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    $(document).on("click","#Tenant-Documents-table .delete_doc_icon",function () {
        var id = $(this).parent().parent().attr('data_id');
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/LettersNoticesDoc-Ajax',
                        data: {id: id,class:'lettersNoticesAjax',action:'deleteLettersNotices'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                $('#Tenant-Documents-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }

            }
        });
    });

    $(document).on('change', '.tab-content select', function () {

        var grid = $("#letterNoticesPropertyTenant-table"), f = [];
        var value = $(this).val();
        var search = $(this).text();
        if (value != '0') {
            f.push({field: "general_property.id", op: "eq", data: value});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData, {filters: JSON.stringify(f)});
            grid.trigger("reloadGrid", [{page: 1, current: true}]);
        }
    });
    /*Search Properties*/
    /*Get Property/Building Details Name List*/
    getPropertyList();
    function getPropertyList(){
        $.ajax({
            type: 'POST',
            url: '/LettersNoticesDoc-Ajax',
            data: {
                class: 'lettersNoticesAjax',
                action: 'getPropertyList',
                // data :window.location.pathname
            },
            success: function (response) {
                var html="";
                var response = JSON.parse(response);
                /*console.log(response);*/
                for (var i = 0; i < response.data.length; i++) {
                    html += ' <option class="abc" value="' + response.data[i].id + '" data-id="' + response.data[i].id + '">' + response.data[i].property_name + '</option>';
                    $("#propertyListAll1").html("<option value='all'>Select</option>" + html);
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $('#jqgridOptions').on('change', function () {
        var selected = this.value;
        $('#letterNoticesPropertyTenant-table').jqGrid('GridUnload');
        jqGrid(selected);
    });

    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGridProperty('All');

    function jqGridProperty(status) {
        if(jqgridNewOrUpdated == 'true'){
            var sortOrder = 'desc';
            var sortColumn = 'users.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }
        var table = 'users';
        //var rentCurrSymbol = "Rent ("+currencySymbol+")";
        var checkbox = '<input type="checkbox" id="checkAllletter" class="checkAllletter" name="checkAllletter" checked="checked">';
        var columns = ['Tenant Name','Phone','Email','Action',checkbox];
        var select_column = [];
        var joins = [{table:'users',column:'id',primary:'user_id',on_table:'tenant_property'},
            {table:'users',column:'id',primary:'user_id',on_table:'tenant_details'},
            {table:'users',column:'id',primary:'user_id',on_table:'tenant_lease_details'},
            {table:'users',column:'id',primary:'user_id',on_table:'tenant_phone'},
            {table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},
            {table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [{column:'user_type',value:'2',condition:'=',table:'users'},{column:'record_status',value:'1',condition:'=',table:'tenant_details'},{column:'record_status',value:'1',condition:'=',table:'tenant_lease_details'}];
        //var pagination=[];
        var columns_options = [

            /*{name:'Tenant Name',index:'id', width:90,align:"left",searchoptions: {sopt: conditions},table:table,formatter:titleCase},*/
            {name:'Tenant Name',index:'id', width:300,align:"center",searchoptions: {sopt: conditions},table:table,change_type: 'tenantname1', extra_columns: ['id'],original_index: 'id'},
            {name:'Phone',index:'phone_number', width:300,searchoptions: {sopt: conditions},table:'tenant_phone'},
            {name:'Email',index:'email', width:340, align:"left",searchoptions: {sopt: conditions},table:table},
           /* {name:'Property Name',index:'property_name', width:250, align:"center",searchoptions: {sopt: conditions},table:'general_property'},*/
            /*{name:'Unit Number',index:'unit_id', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_property',formatter:getUnitDetails},*/
           /* {name:'Unit Number',index:'unit_prefix', width:250, align:"center",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
            {name:'Rent (USh)',index:'rent_amount', width:205, align:"center",searchoptions: {sopt: conditions},table:'tenant_lease_details'},*/
            /* {name:'Lease ID',index:'user_id', width:100,searchoptions: {sopt: conditions},table:'tenant_lease_details'},
             {name:'Days Remaining',index:'days_remaining', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details',formatter:statusFormatter2},
             {name:'Status',index:'status', width:80,align:"left",searchoptions: {sopt: conditions},table:'tenant_details',formatter:statusFormatter},*/
            {name:'Action',index:'id', title: false, width:247,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: pdfRedirect, edittype: 'select',search:true,table:table},
            //{name:'Checkbox',index:'id', width:200, align:"center",sortable:false,searchoptions: {sopt: conditions},table:table,formatter: myPrintCheckboxFormatter,search:true},
            {name: 'Id', index: 'id', width: 170, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatter},
        ];
        var ignore_array = [];
        jQuery("#letterNoticesPropertyTenant-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "users",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Tenant",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:400,drag:true,resize:false} // search options
        );
    }


    function pdfRedirect(cellValue, options, rowObject){

        if(rowObject !== undefined){
            // console.log(rowObject['editable']);
            var html = "";
            html += "<span data_id='"+rowObject.id+"' temp_id=''>" +
                "<a class='clsDownloadPDFFromSearch' data_type='download' href='javaScript:void(0);'>" +
                "<img src='"+webpath+"/company/images/pdfDownload.png'  class='pdfDownload_doc_icon' title='Download' alt='my image' style='cursor: pointer; width: 18px;height: 18px;'/>" +
                "</a></span>" +
                "<span class='tempalateIdLetter' data_id='"+rowObject.id+"' temp_id=''><a class='clsDownloadPDFFromSearch' data_type='view' href='javascript:void(0);'>" +
                "<img src='"+webpath+"/company/images/PDF-Icon.png'  class='delete_doc_icon' title='Preview' alt='my image' style='cursor: pointer; width: 18px;height: 18px;'/>" +
                "</a></span>";

            return html;
        }

    }

    function actionCheckboxFmatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            return '<input type="checkbox" name="checkLetter[]" class="checkLetter" id="checkLetter_' + rowObject.id + '" data_id="' + rowObject.id + '" checked="checked"/>';
        }
    }


    $(".checkAllletter").click(function () {
        $(".checkLetter").prop('checked', $(this).prop('checked'));
    });


    $(document).on('click','#letterNoticesPropertyTenant-table tr td .clsDownloadPDFFromSearch',function(e){
        var tenant_id = $(this).parents("tr").attr('id');
        var dataType = $(this).attr("data_type");
        var template_id = $("#searchTemplateId").val();
        //alert(template_id);
        getHtmlPdfConverter(tenant_id,template_id, dataType);
    });

    function getHtmlPdfConverter(tenant_id,template_id, dataType){
        $.ajax({
            type: 'post',
            url: '/LettersNoticesDoc-Ajax',
            data: {
                tenant_id: tenant_id,
                template_id:template_id,
                class:'lettersNoticesAjax',
                action:'getHtmlPdfConverter'
            },
            success: function (response) {
                var response = JSON.parse(response);


                if (response.status == 'success' && response.code == 200) {
                   /* console.log("here");
                    console.log(response);*/
                    if (dataType == "download"){
                        var splitName = response.data.split("/");
                        var fileName = splitName.reverse();
                        var link=document.createElement('a');
                        document.body.appendChild(link);
                        link.target="_blank";
                        link.download=fileName[0];
                        link.href=response.data;
                        link.click();
                    }else{
                        window.open(response.data, "_blank");
                    }

                    setTimeout(function () {
                    }, 2000);
                } else if(response.code == 500) {
                    toastr.warning(response.message);
                } else {
                    toastr.error(response.message);
                }

            }
        });
    }
    /*Get Property/Building Details Name List*/
    var testClass;
    $(document).on('click','#active_tabs .active_tab',function(e){
        var $grid = $("#Tenant-Documents-table"),
            newWidth = $grid.closest(".ui-jqgrid").parent().width();
        $grid.jqGrid("setGridWidth", newWidth, true);
        testClass = $(this).hasClass("esignBtn");
        //testUser(testClass);
    });

    //function testUser(testClass) {

        $(document).on("click", '#sendMailLetter1', function () {

            $(".loader").show();
            var searchTemplateId = $(".searchboxPropertyAll #searchTemplateId").val();
            // alert(searchsad);debugger;
            favorite = [];
            var no_of_checked = $('#letterNoticesPropertyTenant-table [name="checkLetter[]"]:checked').length

            if (no_of_checked == 0) {
                toastr.error('Please select atleast one Lease.');
                return false;
            }
            $.each($("#letterNoticesPropertyTenant-table input[name='checkLetter[]']:checked"), function () {
                favorite.push($(this).attr('data_id'));
            });
            if (testClass){
                var actionName = 'getTemplateDataEmail2';
            }else{
                var actionName = 'getTemplateDataEmail';
            }
            $.ajax({
                type: 'post',
                url: '/LettersNoticesDoc-Ajax',
                data: {
                    class: 'lettersNoticesAjax',
                    action: actionName,
                    'tenantTemplate_ids': favorite,
                    'searchTemplateId': searchTemplateId,
                    'esignClass':testClass
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    console.log("tenantEmail >>",response);
                    if (response.status == 'success' && response.code == 200) {
                        $(".loader").hide();
                        toastr.success("Email sent successfully.");
                    } else {
                        toastr.warning('Mail is not send due to technical issue.');
                    }
                }
            });
        });

   // }

    $(document).on('click','#Tenant-Documents-table tr .letterNotice',function(e){
        $("#searchboxData1").show();
        $("#Tenant-Documents").hide();
    });

    $(document).on('click','#Tenant-Documents-table tr td:not(:last-child)',function(e){
        e.preventDefault();
        var id = $(this).closest("tr").attr('id');
        $("#searchTemplateId").val(id);
    });


});