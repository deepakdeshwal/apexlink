$(document).ready(function() {
    setTimeout(function () {
        $("#communication-one .parcelRandomData").val(Math.random().toString(36).slice(2).substr(0, 6).toUpperCase());
        getUserList();
    }, 1000);

    /*Cancel reason button*/
    $(".packageCancelButton").click(function () {
        $("#selectpackageTracker").hide();
    });

    $(document).on("click",".propPackageicon",function(){
        $("#selectpackageTracker").show();
    });

    $(document).on("click",".add_package", function () {
        var tableName = $(this).attr("data-table");
        var colName = $(this).attr("data-cell");
        var className = $(this).attr("data-class");
        var selectName = $(this).attr("data-name");
        var val = $(this).parent().prev().find("."+className).val();
        if (val == ""){
            $(this).parent().prev().find("."+className).next().text("Enter the value");
            return false;
        }else{
            $(this).parent().prev().find("."+className).next().text("");
        }
        savePopUpData(tableName, colName, val, selectName);
        var val = $(this).parent().prev().find("input[type='text']").val('');
    });

    getPackageTrackerDetails();
    function getPackageTrackerDetails() {
        $.ajax({
            type: 'post',
            url: '/PackageTracker-Ajax',
            data: {
                class: "packageTrackerAjax",
                action: "getPackageTrackerDetails"
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    if (response.data.length > 0){
                        var reasonOption = "";
                        for(var i=0; i<response.data.length; i++){
                            var html='<option value="'+ response.data[i].id +'">'+ response.data[i].carrierName +'</option>';
                            $('select[name="carrierName_id"]').append(html);
                        }
                    }
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    function  savePopUpData(tableName, colName, val, selectName) {
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/savePopUpData',
            data: {
                class: "TenantAjax",
                action: "savePopUpData",
                tableName:tableName,
                colName: colName,
                val:val
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                    $("select[name='"+selectName+"']").append(option);
                    if (selectName == "hobbies[]" || selectName == "additional_hobbies[]"){
                        $("select[name='"+selectName+"']").multiselect('destroy');
                        $("select[name='"+selectName+"']").multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                        $("select[name='"+selectName+"']").parent().parent().next().hide();
                    }else{
                        $("select[name='"+selectName+"']").next().hide();
                    }
                    toastr.success(data.message);

                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /*Cancel reason button*/
    /*Cancel reason button*/
    $(".locationCancelButton").click(function () {
        $("#selectLocationStore").hide();
    });

    $(document).on("click",".propLocationicon",function(){
        $("#selectLocationStore").show();
    });

    $(document).on("click",".add_Location", function () {
        var tableName = $(this).attr("data-table");
        var colName = $(this).attr("data-cell");
        var className = $(this).attr("data-class");
        var selectName = $(this).attr("data-name");
        var val = $(this).parent().prev().find("."+className).val();
        if (val == ""){
            $(this).parent().prev().find("."+className).next().text("Enter the value");
            return false;
        }else{
            $(this).parent().prev().find("."+className).next().text("");
        }
        savePopUpData(tableName, colName, val, selectName);
        var val = $(this).parent().prev().find("input[type='text']").val('');
    });

    getLocationStoredDetails();
    function getLocationStoredDetails() {
        $.ajax({
            type: 'post',
            url: '/PackageTracker-Ajax',
            data: {
                class: "packageTrackerAjax",
                action: "getLocationStoredDetails"
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    if (response.data.length > 0){
                        var reasonOption = "";
                        for(var i=0; i<response.data.length; i++){
                            var html='<option value="'+ response.data[i].id +'">'+ response.data[i].locationName +'</option>';
                            $('select[name="locationName_id"]').append(html);
                            $('select[name="locationName_id"]').val("1");

                        }
                    }
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    function  savePopUpData(tableName, colName, val, selectName) {
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/savePopUpData',
            data: {
                class: "TenantAjax",
                action: "savePopUpData",
                tableName:tableName,
                colName: colName,
                val:val
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                    $("select[name='"+selectName+"']").append(option);
                    if (selectName == "hobbies[]" || selectName == "additional_hobbies[]"){
                        $("select[name='"+selectName+"']").multiselect('destroy');
                        $("select[name='"+selectName+"']").multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                        $("select[name='"+selectName+"']").parent().parent().next().hide();
                    }else{
                        $("select[name='"+selectName+"']").next().hide();
                    }
                    toastr.success(data.message);

                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /*Cancel reason button*/

    function getUserList() {
        $.ajax({
            type: 'post',
            url: '/PackageTracker-Ajax',
            data: {
                class: "packageTrackerAjax",
                action: "getUsersListname"
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log(response);
                console.log(response.data1);
                console.log("+++++++++++++++");

                if (response.status == 'success' && response.code == 200) {
                    $('#receivedByPackage').html(response.data);
                    setTimeout(function () {
                        $('#receivedByPackage').val(response.data1);
                    }, 1000);
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on('change','#pcktrackList',function(){
        var owner_Type = $('#pcktrackList').val();

        setTimeout(function () {
            getOwnerTypeList(owner_Type);
        }, 1000);
    });


    function getOwnerTypeList(owner_type) {
        $.ajax({
            type: 'post',
            url: '/PackageTracker-Ajax',
            data: {
                class: "packageTrackerAjax",
                action: "getOwnerTypeListname"
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log(response.data);
                if (response.status == 'success' && response.code == 200) {
                    var html = '';
                    if(owner_type == 1){
                        $('#packageOwnerName').html('');
                        $('#packageOwnerName').show();
                        $('#packageAddOwnerName').hide();
                        if (response.data.tenant.length > 0){
                            $('#packageOwnerName').append("<option value='0'>Select Owner Name</option>");
                            for(var i=0; i<response.data.tenant.length; i++){
                                var html='<option value="'+ response.data.tenant[i].id +'">'+ response.data.tenant[i].tenantName +'</option>';
                                $('#packageOwnerName').append(html);
                            }

                        }
                    }else if (owner_type == 2) {
                        $('#packageOwnerName').html('');
                        $('#packageOwnerName').show();
                        $('#packageAddOwnerName').hide();

                        if (response.data.owner.length > 0){
                            $('#packageOwnerName').append("<option value='0'>Select Owner Name</option>");
                            for(var i=0; i<response.data.owner.length; i++){
                                var html='<option value="'+ response.data.owner[i].id +'">'+ response.data.owner[i].ownerName +'</option>';
                                $('#packageOwnerName').append(html);
                            }
                        }
                    }else if (owner_type == 3){

                        $('#packageOwnerName').html('');
                        $('#packageOwnerName').show();
                        $('#packageAddOwnerName').hide();
                        if (response.data.vendor.length > 0){
                            $('#packageOwnerName').append("<option value='0'>Select Owner Name</option>");
                            for(var i=0; i<response.data.vendor.length; i++){
                                var html='<option value="'+ response.data.vendor[i].id +'">'+ response.data.vendor[i].vendorName +'</option>';
                                $('#packageOwnerName').append(html);
                            }
                        }
                    }else if (owner_type == 4){

                        $('#packageOwnerName').html('');
                        $('#packageOwnerName').show();
                        $('#packageAddOwnerName').hide();
                        if (response.data.employee.length > 0){
                            $('#packageOwnerName').append("<option value='0'>Select Owner Name</option>");
                            for(var i=0; i<response.data.employee.length; i++){
                                var html='<option value="'+ response.data.employee[i].id +'">'+ response.data.employee[i].employeeName +'</option>';
                                $('#packageOwnerName').append(html);
                            }
                        }
                    }else{
                        if (response.data.other.length > 0){
                            $('#packageOwnerName').hide();
                            $('#packageAddOwnerName').show();
                        }
                    }
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on('change','#packageOwnerName',function(){
        var idOwnerType = $('#packageOwnerName').val();
        getOwnerDetails(idOwnerType);
    });

    function getOwnerDetails(idOwnerType) {
        $.ajax({
            type: 'post',
            url: '/PackageTracker-Ajax',
            data: {
                class: "packageTrackerAjax",
                action: "getOwnerDetails",
                id:idOwnerType
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    if(response.data.email != null){
                        $("input[name='ownerTypeEmail']").val(response.data.email);
                    }
                    if(response.data.phone_number != null){
                        $("input[name='ownerType_phone_number']").val(response.data.phone_number);
                    }
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("click",".CancelBtnPackage",function(){
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    window.location.href = window.location.origin+'/Package/Packages';
                }
            }
        });

    });

    $(document).on("click",".packageData",function(){

        if ($('#packageTracker_form').valid()){
            saveData("Save");
        }
        return false;
    });

    $(document).on("click",".saveSendEmailText",function(){
        if ($('#packageTracker_form').valid()){
        saveData("Email");
        }
        return false;
    });

    function saveData(value){
        var editData = $("#packageTrackerUpdate").val();
        var editId = $("#editPackageData").val();
        var form = $('#packageTracker_form').serializeArray();
        /*e.preventDefault();
        var formData = $('#NewportfolioPopup :input').serializeArray();*/
        $.ajax({
            type: 'post',
            url: '/PackageTracker-Ajax',
            data: {
                form: form,
                class: 'packageTrackerAjax',
                action: 'insert',
                update:editData,
                updateId:editId
            },
            success: function (response) {
                var response = JSON.parse(response);
                var tempId = response.id;
                console.log("here is your response",tempId);
                if (response.status == 'success' && response.code == 200) {
                    if(value == "Email"){
                        if(tempId != ''){
                            $('#packageData_model .custom_field_form').append("<input type='hidden' id='userId' value='"+tempId+"'>");
                        }
                        $("#myModalPackage").modal("show");
                        toastr.success(response.message);
                        $("#myModalPackage .phonePackageData").val(response.data.ownerType_phone_number);
                        $("#myModalPackage .emailPackageData").val(response.data.ownerTypeEmail);
                    }
                    else{
                        window.location.href = '/Package/Packages';
                        localStorage.setItem("Message", response.message);
                        localStorage.setItem("rowcolor", true)
                        $("#packageTracker-table").trigger('reloadGrid');
                    }
                } else if (response.status == 'failed' && response.code == 503) {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        }

    //$(document).on("submit","#packageTracker_form",function(e) {
    $("#packageTracker_form").validate({
        rules: {

            packageOwnerName: {
                required: true
            },
            packageOwnerName: {
                required: true
            },
            ownerTypeEmail: {
                required: true
            },
            country_code: {
                required: true
            },
            carrierName: {
                required: true
            },
            locationName: {
                required: true
            },
            carrierName_id: {
                required: true
            },
            pcktrackList: {
                required: true
            },
            locationName_id: {
                required: true
            }
        },
    });


    /*Jqgrid table*/
        //jqGrid status
    $('#jqgridOptions').on('change', function () {
        var selected = this.value;
        $('#packageTracker-table').jqGrid('GridUnload');
        jqGrid1(selected);
    });

    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid1('All');
    function jqGrid1(status) {
        var table = 'packageTracker_table';
        var columns = ['Package Owner','Others','Owner Type','Parcel Number','Quantity of Package','Package Received Date','Carrier','Package Size','Location Stored','Pick-Up Date/Time','Picked Up By','Received By','Pick-Up Notes','IsPackageDelivered','Action'];
        var select_column = ['Edit','Delete','Package Pickup'];
        var joins = [];
        var joins = [{table:'packageTracker_table',column:'packageOwnerName',primary:'id',on_table:'users'},
            {table:'packageTracker_table',column:'receivedByPackage',primary:'id',on_table:'users',as:'user2'},
            {table:'packageTracker_table',column:'carrierName_id',primary:'id',on_table:'packageTrackerCarrier_table'},
            {table:'packageTracker_table',column:'locationName_id',primary:'id',on_table:'locationStored_table'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['packageTracker_table.deleted_at'];
        //var extra_where = [{column:'letter_type',value:'1',condition:'=',table:'letters_notices_template'}];
        var columns_options = [
            {name: 'Package Owner', index: 'name', width: 135, align: "center", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer',formatter:checkNameFormatter},
            {name: 'Others', index: 'othersData', hidden:true, width: 135, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Owner Type', index: 'pcktrackList', width: 135, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:statusFormatterOwnerListType},
            {name: 'Parcel Number', index: 'parcelNumber', width: 135, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Quantity of Package', index: 'quantityOfPackages', width: 135, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Package Received Date', index: 'trackerDatePackage', width: 135, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',change_type:'date'},
            {name: 'Carrier', index: 'carrierName', width: 135, align: "center", searchoptions: {sopt: conditions}, table: 'packageTrackerCarrier_table', classes: 'pointer'},
            {name: 'Package Size', index: 'packageSize', width: 135, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Location Stored', index: 'locationName', width: 135, align: "center", searchoptions: {sopt: conditions}, table: 'locationStored_table', classes: 'pointer'},
            {name: 'Pick-Up Date/Time', index: 'pickUpDate', width: 135, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',change_type:'date'},
            {name: 'Picked Up By', index: 'PickedUpBy', width: 135, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Received By', index: 'name', width: 135, align: "center", searchoptions: {sopt: conditions}, table: 'user2', classes: 'pointer',alias:'received_by'},
            {name: 'Pick-Up Notes', index: 'notesPackages', width: 135,  align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'IsPackageDelivered', index: 'IsPackageDelivered', width: 135,  align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name:'Action',index:'select',width:200, title: false,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFmatter}
        ];
        var ignore_array = [];
        jQuery("#packageTracker-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            sortname: 'packageTracker_table.updated_at',
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns:extra_columns,
               // extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'packageTracker_table.updated_at',
            sortorder: 'desc',
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Packages",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:400,drag:true,resize:false} // search options
        );
    }

    function actionFmatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var select = '';
            if(rowObject['IsPackageDelivered'] == 'True')  select = ['Edit','Delete'];
            if(rowObject['IsPackageDelivered'] == 'False')  select = ['Edit','Delete','Package Pickup'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '" data-user_id="' + rowObject.user_id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    function checkNameFormatter(cellvalue, options, rowObject) {
            if(rowObject != undefined && rowObject['Others'] != ""){
                return rowObject['Others'];
            }
            return cellvalue;
    }

    function titleCase( cellvalue, options, rowObject) {
        if (cellvalue !== undefined) {

            var string = "";
            $.ajax({
                type: 'post',
                url: '/Tenantlisting/getUserNameById',
                data: {
                    class: "TenantAjax",
                    action: "getUserNameById",
                    id: cellvalue
                },
                async: false,
                success: function (response) {
                    var res = JSON.parse(response);
                    if (res.status == "success") {
                        string = res.data;
                    }
                }
            });
            return string;
        }
    }

    function statusFormatterOwnerListType (cellValue, options, rowObject){
        if (cellValue == '1')
            return "Tenant";
        else if(cellValue == '2')
            return "Owner";
        else if(cellValue == '3')
            return "Vendor";
        else if(cellValue == '4')
            return "Employee";
        else if(cellValue == '5')
            return "Other";
        else
            return '';
    }

    /*Delete row data from select option*/

    $(document).on('change', '#packageTracker-table .select_options', function() {
        var action = this.value;
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index() ;
        if (opt == 'Delete' || opt == 'DELETE') {
            opt = opt.toLowerCase();
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/PackageTracker-Ajax',
                            data: {
                                class: "packageTrackerAjax",
                                action: "deletePackage",
                                id: id
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    setTimeout(function () {
                                        toastr.success(response.message);
                                        onTop(true);
                                        $('#packageTracker-table').trigger('reloadGrid');
                                    }, 200);
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                    $('#packageTracker-table').trigger('reloadGrid');
                }
            });
        }else if(opt == 'EDIT' || opt == 'Edit') {
            $('#communication-one #packageData').val('Update');
            $('#communication-one .saveSendEmailText').val('Update & SendEmail');
            window.location.href = '/Package/AddPackages?id='+id+'&edit=1';

        }else if(opt == 'PACKAGE PICKUP' || opt == 'Package Pickup') {
            $('#myModalPackagePickUp').modal("show");
            $("#myModalPackagePickUp #idpickup").val(id);
            getUsersname(id);
        }
    });
    /*Jqgrid table*/

    function getUsersname(id) {
        $.ajax({
            type: 'POST',
            url: '/PackageTracker-Ajax',
            data: {
                class: "packageTrackerAjax",
                action: "getUsersname",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#pickUpByPackage').val(response.data);
                    //toastr.success(response.message);
                    // onTop(true);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }


    function getParameters(name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.search);
        return (results !== null) ? results[1] || 0 : false;
    }

    var editDisplay = getParameters('edit');
    var id = getParameters('id');


    /*Edit Package Data*/
    if(editDisplay == '1' && id != ""){
        editPackageData();
        function editPackageData() {
           var id = $("#editPackageData").val();
           $.ajax({
                type: 'POST',
                url: '/PackageTracker-Ajax',
                data: {
                    class: "packageTrackerAjax",
                    action: "editPackageData",
                    id: id
                },
                success: function (response) {
                    var response = JSON.parse(response);

                    if (response.status == 'success' && response.code == 200) {
                        $("#pcktrackList").val(response.data.pcktrackList).trigger('change');
                        $("#ownerTypeEmail").val(response.data.ownerTypeEmail);
                        $("#country_code").val(response.data.country_code);
                        $(".ownerType_phone_number").val(response.data.ownerType_phone_number);
                        $("#parcelRandomData").val(response.data.parcelNumber);
                        $("#quantityOfPackages").val(response.data.quantityOfPackages);
                        $("#carrierName_id").val(response.data.carrierName_id);
                        $("#packageSize").val(response.data.packageSize);
                        $("#locationName_id").val(response.data.locationName_id);
                        $("#notesPackages").val(response.data.notesPackages);
                        $("#packageAddOwnerName").val(response.data.othersData);
                        setTimeout(function () {
                            $("#receivedByPackage").val(response.data.receivedByPackage);
                            $("#packageOwnerName").val(response.data.packageOwnerName);
                        }, 2000);
                        edit_date_time(response.data.updated_at);
                        //toastr.success(response.message);
                       // onTop(true);
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }

    }


/*    $(document).on("click","#myModalPackage #doneEmailTextMessage",function(){

            if($('input[name="checkAll"]').prop("checked") == true){
               var phonenumber =  $('input[name="phoneNumberTracker"]').val();
                var email = $('input[name="emailSendTracker"]').val();

            }
            else if($('input[name="phoneNumberTracker"]').prop("checked") == true && $('input[name="checkAll"]').prop("checked") == false){

                var phonenumber = $('input[name="phoneNumberTracker"]').val();
                var email = $('input[name="emailSendTracker"]').val("");

            }else if($('input[name="emailSendTracker"]').prop("checked") == true && $('input[name="checkAll"]').prop("checked") == false){

                var email = $('input[name="emailSendTracker"]').val();
                var phonenumber = $('input[name="phoneNumberTracker"]').val("");

            }

       userEMAIL(phonenumber,email);
    });*/


    $(document).on("click","#myModalPackage #doneEmailTextMessage",function(){
        var favorite = [];
        $.each($("input[type='checkbox']:checked"), function(){
            favorite.push($(this).val());
        });
        userEMAIL(favorite);
    });


    function userEMAIL(favorite) {
        var jsonString = JSON.stringify(favorite);
        var userIdData = $("#packageData_model #userId").val();

        var userId = '';
        if(userIdData == 'undefined'){
             userId = $("#editPackageData").val();
        }else{
             userId = $("#packageData_model #userId").val();
        }

      //  console.log(userId);debugger;

            $.ajax({
                type: 'post',
                url: '/PackageTracker-Ajax',
                data: {
                    class: "packageTrackerAjax",
                    action: "sendMail",
                    id: userId,
                    favorite:favorite
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    console.log(response);
                    if (response.status == 'success' && response.code == 200) {

                        toastr.success(response.message);
                        onTop(true);
                        localStorage.setItem("Message", response.message);
                        localStorage.setItem("rowcolor", true)
                        window.location.href = '/Package/Packages';
                    } else {
                        toastr.error(response.message);
                    }
                }
            });

        $('#packageTracker-table').trigger('reloadGrid');
    }

    $("#pickUpPackage_model").validate({
        rules: {
            pickUpByPackage: {
                required: true
            }
        }
    });

    $(document).on("click","#myModalPackagePickUp #doneEmailText",function(){
        if ($('#pickUpPackage_model').valid()) {
            var idPickup = $("#idpickup").val();
            var form = $('#pickUpPackage_model').serializeArray();
            $.ajax({
                type: 'post',
                url: '/PackageTracker-Ajax',
                data: {
                    form: form,
                    class: 'packageTrackerAjax',
                    action: 'UpdatePickedUp',
                    id: idPickup
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        //window.location.href = '/Package/Packages';
                        $('#myModalPackagePickUp').modal("hide");
                        toastr.success(response.message);
                        $("#packageTracker-table").trigger('reloadGrid');
                        onTop(true);
                    } else if (response.status == 'failed' && response.code == 503) {
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });

    $("input[name='pickUpDatePackage']").datepicker({
        dateFormat: date_format,
        autoclose: true,
        changeMonth: true,
        changeYear: true
    }).datepicker("setDate", new Date());

    $('.pickUpTimePackage').timepicker({
        timeFormat: 'h:mm p',
        interval: 5,
        defaultTime: '8',
        startTime: '8:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $(document).on('click','#packageTracker-table tr td:not(:last-child)',function(e){
        e.preventDefault();
        var base_url = window.location.origin;
        var id = $(this).closest("tr").attr('id');
        window.location.href = '/Package/AddPackages?id='+id+'&edit=1';
    });

    $("#CheckProp").click(function () {
        $(".chkList").prop('checked', $(this).prop('checked'));
    });

    /*country code*/
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getInitialData',
        data: {
            class: "TenantAjax",
            action: "getIntialData"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if (data.data.state.state != ""){
                    $("#driverProvince").val(data.data.state.state);
                }

                if (data.data.country.length > 0){
                    var countryOption = "<option value='0'>Select</option>";
                    $.each(data.data.country, function (key, value) {
                        if( value.id == "220"){
                            countryOption += "<option value='"+value.id+"' data-id='"+value.code+"' selected>"+value.name+" ("+value.code+")"+"</option>";
                        }else{
                            countryOption += "<option value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")"+"</option>";
                        }
                    });
                    $('#communication-one .countycodediv select').html(countryOption);
                }
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

});