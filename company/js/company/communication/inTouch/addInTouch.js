$(document).ready(function () {
    $(document).on("click",".add_intouch_cancel",function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = '/Communication/InTouch';
            }
        });

    });
    fetchAllTouchType();
    fetchAllAssignedUser();

    $(document).on("click", ".typePopupAdd", function () {
        $(".typePopup").show();
        $('#add_intouch_form').find('textarea').val('');
    });
    $(document).on("click", ".cancelPopup", function () {
        $(".typePopup").hide();
    });
    $(document).on("click", ".addTypePopup", function (e) {
        e.preventDefault();
        var formData = $('#add_intouch_form :input').serializeArray();

        $.ajax({
            type: 'post',
            url: '/in-touch-ajax',
            data: {form: formData,
                class: 'InTouch',
                action: 'addType'},
            beforeSend: function (xhr) {
                var res = true;
                // checking portfolio validations
                $(".customValidateType").each(function () {
                    res = validations(this);
                });

                if (res === false) {
                    xhr.abort();
                    return false;
                }
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('.typePopup').hide(500);
                    toastr.success(response.message);
                    fetchAllTouchType(response.lastid);
                } else if (response.status == 'error' && response.code == 500) {
                    toastr.warning(response.message);
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });
})
//in touch all data save ajax
$("#formIn-touch-data-save").validate({
    rules: {
        type: {
            required: true
        },
        category: {
            required: true
        },
        assigned_for :{
            required: true
        },
        subject: {
            required: true
        }
    }
});
$("#formIn-touch-data-save").on("submit", function (e) {
    e.preventDefault();
if ($('#formIn-touch-data-save').valid()) {
    $('#loadingmessage').show();
    var formData = $('#formIn-touch-data-save').serializeArray();
    var remindercheck= '0';
    if ($('.remindercheck').is(":checked")) {
        var remindercheck = '1';
    }
    var completedMarkcheck='0';
    if ($('.completedMark-check').is(":checked")) {
        var completedMarkcheck = '1';
    }
    var intouchId=$("#edit-in-touch-id").val();
    $.ajax({
        type: 'post',
        url: '/in-touch-ajax',
        data: {form: formData,
            class: 'InTouch',
            action: 'addInTouchFormData',
            remindercheck:remindercheck,
            completedMarkcheck:completedMarkcheck,
            intouchId:intouchId
        },
        success: function (result) {
            var response = JSON.parse(result);
            if (response.status == 'success' && response.code == 200) {
                localStorage.setItem("Message", response.message);
                localStorage.setItem('rowcolor', 'rowColor');
                $('#loadingmessage').hide();
                window.location.href = window.location.origin + '/Communication/InTouch';
            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                toastr.warning(response.message);
            }
        }, error: function (jqXHR, status, err) {
            console.log(err);
        },
    });
}
});

function fetchAllTouchType(id) {
    $.ajax({
        type: 'post',
        url: '/in-touch-ajax',
        data: {
            class: 'InTouch',
            action: 'fetchAllTouchType'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#ddlType').html(res.data);
            if (id !== false) {
                $('#ddlType').val(id);
            }
        },
    });
}

function fetchAllAssignedUser() {
    $.ajax({
        type: 'post',
        url: '/in-touch-ajax',
        data: {
            class: 'InTouch',
            action: 'fetchAllAssignedUser'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#ddlassigned_user').html(res.data);
             var element = $("#subscription_user");
             element.html(res.data).multiselect("destroy").multiselect({
                includeSelectAllOption: true,
                nonSelectedText: 'Select'
            });
        },
    });
}
$(document).on("click","#cleardate-picker",function(){
$("#optional_due_date").val('');
})

$(document).on("click","#AddSubUser",function(){
      $(".subscribeduserTableTR").html('');
   var datauser= $("#subscription_user").val();
    $.ajax({
        type: 'post',
        url: '/in-touch-ajax',
        data: {
            class: 'InTouch',
            action: 'getAddUserDetail',
            datauser:datauser
        },
        success: function (response) {
            var result = JSON.parse(response);
            if (result.status == 'success') {
                $('#subscribeduserTable').show();
                $.each(result.res, function (key, value) {
                    var trHtml='<tr></tr><td>'+value.name+'</td><td><a style="" rel="'+value.id+'" class="add-icon deleteUser" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a></td></tr>';

                    $(".subscribeduserTableTR").append(trHtml);
                });
            }
        },
    });
})
$(document).on("click",".deleteUser",function(){
    // alert($(this).attr('rel'));
    $(this).parent().parent().remove();
})
getIntouchDetail();
function getIntouchDetail(){
    var userid=$("#edit-in-touch-id").val();
    $.ajax({
        type: 'post',
        url: '/in-touch-ajax',
        data: {
            class: 'InTouch',
            action: 'getIntouchDetail',
            id:userid
        },
        success: function (response) {
            var result = JSON.parse(response);
            if (result.status == 'success') {
                    $(".AppendNotesDiv").html(result.html);
                    $('#subscribeduserTable').show();
                    $(".subscribeduserTableTR").append(result.trHtml);

                setTimeout(function(){
                    $("#subscription_user").multiselect('select',result.subscribed_user);
                    $('#ddlType option[value='+result.data.type+']').attr('selected', 'selected');
                    $('#ddlCategory option[value='+result.data.category+']').attr('selected', 'selected');
                    $('#ddlassigned_user option[value='+result.data.assigned_user+']').attr('selected', 'selected');
                    if(result.data.assigned_for != null) {
                        $("#assigned_for_id").val(result.data.assigned_for);
                    }
                    $("#subject_id").val(result.data.subject);

                    $('#remind_days option[value='+result.data.remind_days+']').attr('selected', 'selected');
                    $('#remind_due_hours option[value='+result.data.remind_due_hours+']').attr('selected', 'selected');
                    $('#remind_overdue_hours option[value='+result.data.remind_overdue_hours+']').attr('selected', 'selected');
                    $("#optional_due_date").val(result.optional_due_date);
                   if(result.data.send_reminder == 1){
                       $(".remindercheckClass").prop("checked", true);
                   }
                }, 2000);

            }
        },
    });
}
