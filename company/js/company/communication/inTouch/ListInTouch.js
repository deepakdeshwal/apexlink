$(document).ready(function () {


    $('#jqGridStatus').on('change', function () {
        var selected = this.value;
        var deleted_at = true;
        $('#in-touch-table').jqGrid('GridUnload');
        // if (selected == 4)
        //     deleted_at = false;
        jqGrid(selected);
    });


    jqGrid('all',Byname);
    function jqGrid(status) {

        var table = 'in_touch_detail';
        var columns = ['In-Touch Id','Status','Subject', 'Type', 'Category','By', 'Assigned To', 'For', 'Created', 'Last Modified','Action'];
        var select_column = ['Edit', 'Tenant Living', 'Add New Tenant', 'Work orders', 'Application', 'Vacancy', 'Unit Directory', 'Inspection', 'General Ledger', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
        var joins = [{table: 'in_touch_detail', column: 'type', primary: 'id', on_table: 'in_touch_type'},{table: 'in_touch_detail', column: 'assigned_user', primary: 'id', on_table: 'users'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [];
        var columns_options = [
            {name: 'In-Touch Id', title:false, index: 'id', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Status', title:false,index: 'status', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter: StatusInTouch},
            {name: 'Subject', index: 'subject', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Type', index: 'in_touch_type', width: 80, align: "center", searchoptions: {sopt: conditions}, table: 'in_touch_type', classes: 'pointer'},
            {name: 'Category', index: 'category', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},

            {name: 'By', index: 'assigned_for', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:loggedinnameFormatter},

            {name: 'Assigned To', index: 'name', width: 80, align: "center", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer'},
            {name: 'For', index: 'assigned_for', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Created', index: 'created_at', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table,classes: 'pointer'},
            {name: 'Last Modified', index: 'updated_at', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Action', index: 'select', title: false, width: 80, align: "right", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true,formatter:actionFormatter, edittype: 'select', search: false, table: table}
        ];
        var ignore_array = [];
        jQuery("#in-touch-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            hoverrows:false,
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname:'updated_at',
            sortorder:'desc',
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "All In-Touch",
            pginput: true,
            pgbuttons: true,
            // loadComplete : function () {
            //     if(localStorage.getItem("propertyname") !== undefined && localStorage.getItem("propertyname") != '' ){
            //         searchFilters3(localStorage.getItem("propertyname"));
            //         localStorage.removeItem("propertyname");
            //     }
            // },
            rowattr: function (rd) {
                if(rd.Status == '0'){
                    return {"style": "background-color:#FFFF99;"};
                }else if(rd.Status == '1'){
                    return {"style": "background-color:#99FF99;"};
                }else if(rd.Status == '2'){
                    return {"style": "background-color:red;"};
                }
                else if(rd.Status == '3'){
                    return {"style": "background-color:#FFC69E;"};
                }
            },
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            },
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
        );
    }

    $(document).on("click","#transactionid",function(){
        $("#transactionHistoryModal").modal("show");
        var trans= $(this).attr('rel');
        var hid=$("#historyT_id").val(trans);
        searchFilters2(trans);
    });
    $(document).on("click",".searchInTouch",function(){
        var searchinput= $(".searchinput").val();
        searchFilters(searchinput);
    });
    var propertyname=localStorage.getItem("propertyname");
    if(localStorage.getItem("propertyname") !== undefined && localStorage.getItem("propertyname") != '' ){
        setTimeout(function () {
            searchFilters3(propertyname);
            localStorage.removeItem("propertyname");
        }, 1000);
    }

    function searchFilters3(value){
        var grid = $("#in-touch-table"),f = [];
        var type = value;
        f.push({field: "in_touch_detail.assigned_for", op: "cn", data: type});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }


    function searchFilters(value){
        console.log("sdsdsd",value);
        var grid = $("#in-touch-table"),f = [];
        var type =value;
        f.push({field: "in_touch_detail.category", op: "cn", data: type, con:'OR'},{field: "in_touch_detail.assigned_for", op: "cn", data: type, con:'OR'});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function searchFilters2(value){
        var grid = $("#in-touch-transaction"),f = [];
        var type =value;
        f.push({field: "in_touch_logs.in_touch_id", op: "eq", data: type,int:'int'});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }


    jqGridtransaction('all');
    function jqGridtransaction(status) {
        var table = 'in_touch_logs';
        var columns = ['User Name','Action','Action Time', 'Description'];
        var select_column = [];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [];
        var columns_options = [
            {name: 'User Name', title:false, index: 'username', width: 150, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Action Name', title:false,index: 'action', width: 150, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Action Time', index: 'action_time', width: 150, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',change_type:'date'},
            {name: 'Description', index: 'description', width: 150, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
             ];
        var ignore_array = [];
        jQuery("#in-touch-transaction").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where:extra_where,
                deleted_at:'no'
            },
            viewrecords: true,
            sortname:'username',
            sortorder:'desc',
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
        );
    }
});
function loggedinnameFormatter(){
    return Byname;
}
function StatusInTouch(cellValue, options, rowObject) {

    if(rowObject !== undefined) {
        if(rowObject.Status == 0){
            return 'Pending';
        }else if(rowObject.Status == 1){
            return 'Complete';
        }else if(rowObject.Status == 2){
            return 'Cancel';
        }else if(rowObject.Status == 3){
            return 'OverDue';
        }

    }

}
function actionFormatter (cellValue, options, rowObject){
    var protocol = location.port;
    if (protocol != "") {
        var webpath = location.protocol+"//"+location.hostname+":"+protocol;
    }else{
        var webpath = location.protocol+"//"+location.hostname;
    }
    if(rowObject !== undefined) {
        var status = rowObject.Status;
        var select = '<span>Notes</span><i data-id='+rowObject.id+' style="font-size: 16px;padding-right: 10px;" class="fa fa-sort-desc notesHistory" aria-hidden="true"></i><img src="'+webpath+'/company/images/transaction.png" id="transactionid" rel="'+rowObject.id+'">';
        if(status != 1 && status != 2)  select = "<span style='cursor: pointer;'>Notes</span><i data-id="+rowObject.id+" style='font-size: 16px;padding-right: 10px;cursor: pointer;' title='Notes'  class='fa fa-sort-desc notesHistory' aria-hidden='true'></i><img src='"+webpath+"/company/images/transaction.png' rel='"+rowObject.id+"' id='transactionid'><div><i id='Complete' data-id='"+rowObject.id+"' title='Complete' style='font-size: 16px;padding-right: 10px;' class='fa fa-check-circle' aria-hidden='true'></i><i data-id='"+rowObject.id+"' title='Cancel' id='cross' style='font-size: 16px;' class='fa fa-times' aria-hidden='true'></i></div><div><i title='Delete' data-id='"+rowObject.id+"' id='delete' style='font-size: 16px;padding-right: 10px;' class='fa fa-trash' aria-hidden='true'></i><i data-id='"+rowObject.id+"' title='Edit' id='edit-in-touch' style='font-size: 16px;' class='fa fa-pencil-square-o' aria-hidden='true'></i></div>";
        var data = '';
        if(select != '') {
            data = select;
        }
        return data;
    }
}

$(document).on("click","#edit-in-touch",function(){
    var id=$(this).attr('data-id');
    window.location.href = '/Communication/editInTouch?id=' + id;
});
$(document).on("click","#delete",function(){
    var id=$(this).attr('data-id');
                bootbox.confirm({
                    message: "Do you want to delete this record?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/in-touch-ajax-list',
                                data: {class: 'InTouchList', action: "delete", id: id},
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                    $('#in-touch-table').trigger( 'reloadGrid' );
                                }
                            });
                        }
                    }
                });

})
$(document).on("click","#Complete",function(){
    var id=$(this).attr('data-id');
    bootbox.confirm({
        message: "Do you want to change the status to Complete?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $.ajax({
                    type: 'post',
                    url: '/in-touch-ajax-list',
                    data: {class: 'InTouchList', action: "updateStatus", id: id,status:'1'},
                    success: function (response) {
                        var response = JSON.parse(response);
                        if (response.status == 'success' && response.code == 200) {
                            toastr.success(response.message);
                        } else {
                            toastr.error(response.message);
                        }
                        $('#in-touch-table').trigger( 'reloadGrid' );
                    }
                });
            }
        }
    });
});
$(document).on("click","#cross",function(){
    var id=$(this).attr('data-id');
    bootbox.confirm({
        message: "Do you want to change the status to Cancel?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $.ajax({
                    type: 'post',
                    url: '/in-touch-ajax-list',
                    data: {class: 'InTouchList', action: "updateStatus", id: id,status:'2'},
                    success: function (response) {
                        var response = JSON.parse(response);
                        if (response.status == 'success' && response.code == 200) {
                            toastr.success(response.message);
                        } else {
                            toastr.error(response.message);
                        }
                        $('#in-touch-table').trigger( 'reloadGrid' );
                    }
                });
            }
        }
    });
});

$(document).on("click",".notesHistory",function(){
    var id=$(this).attr('data-id');
    $("#Notesedit").attr('rel',id);
    $("#notesHistoryModal").modal("show");
    $.ajax({
        type: 'post',
        url: '/in-touch-ajax-list',
        data: {class: 'InTouchList', action: "NotesHistory", id: id},
        success: function (response) {
            var response = JSON.parse(response);
            $(".historyhtml").html(response.data);
        }
    });
});
$(document).on("click","#Notesedit",function(){
    var noteeditid=$(this).attr('rel');
    window.location.href = '/Communication/editInTouch?id='+noteeditid;

});
