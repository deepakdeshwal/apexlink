$(function() {
    $('.nav-tabs').responsiveTabs();
});
$('.selectSubRecepient').selectpicker();
<!--- Main Nav Responsive -->
$("#show").click(function(){
    $("#bs-example-navbar-collapse-2").show();
});
$("#close").click(function(){
    $("#bs-example-navbar-collapse-2").hide();
});
<!--- Main Nav Responsive -->


$(document).ready(function(){
    $(".slide-toggle").click(function(){
        $(".box").animate({
            width: "toggle"
        });
    });
});

$(document).ready(function(){
    $(".slide-toggle2").click(function(){
        $(".box2").animate({
            width: "toggle"
        });
    });
    $("#communication_top").addClass("active");
});


$(document).on('change','.selectSubRecepient', function() {
    // var Stotal = $('.selectSubRecepient li.selected').length;
    //
    // if(Stotal >= 1){
    //     Stotal = parseInt(Stotal);
    // } else {
    //     Stotal  = 1;
    // }
    // var Ftotal = $('.selectSubRecepient li').length;
    // if(Ftotal){
    //     Ftotal = (parseInt(Ftotal) - parseInt(1))+parseInt(1);
    // }
    // setTimeout(function(){
    //     $('.selectSubRecepient .filter-option').text(Stotal+' of ' +Ftotal+ ' selected');
    // },100);
});

$(document).on('change','.selectRecepient',function () {
    var id = $(this).val();
    selectDropdownRecepient(id,'');

});

function selectDropdownRecepient(id,editData)
{
    $.ajax({
        url:'/Communication/GroupMessageAjax',
        method: 'post',
        data: {
            class: "GroupMessageAjax",
            action: "getUserList",
            'id': id,
        },
        success: function (data) {
            var res = jQuery.parseJSON(data);

            if(res)
            {
                var dropdown='    <option value="All">Select All</option>\n';
                // $.each( res, function( key, value ) {
                //     dropdown += '<option value="'+value.id+'">'+value.name+'</option>';
                // });
                if(editData)
                {
                    $.each(res, function (key,value) {
                        var id = value.id;
                        var name = value.name;
                        if (jQuery.inArray(name, editData) !== -1){
                            console.log('YES==='+name);
                            dropdown += "<option selected value='" + id + "'>" + name + "</option>";
                        } else {
                            dropdown += "<option value='" + id + "'>" + name + "</option>";
                        }
                    });

                }else{
                    $.each(res, function (key,value) {
                        var id = value.id;
                        var name = value.name;
                        dropdown += "<option value='"+id+"'>"+name+"</option>";
                    });

                }

                $('select.selectSubRecepient').html(dropdown);
                $('.selectSubRecepient').selectpicker('refresh');


            }


        },

    });
}


$('.selectSubRecepient').selectpicker().change(function(){toggleSelectAll($(this));}).trigger('change');
$(document).on('change','.selectType',function () {
    var emailType = $(this).val();
    if(emailType == '1') {
        $('.email_container').show();
        $('.text_container').hide();
        $('.eTextContainer').show();
        $('.text_container textarea').attr('disabled',true);
        $('.email_container textarea,.email_container .subject').attr('disabled',false);
    } else if(emailType == '2') {
        $('.email_container').hide();
        $('.text_container').show();
        $('.eTextContainer').show();
        $('.text_container textarea').attr('disabled',false);
        $('.email_container textarea,.email_container .subject').attr('disabled',true);
    } else{
        $('.email_container').hide();
        $('.text_container').hide();
        $('.eTextContainer').hide();
        $('.text_container textarea').attr('disabled',true);
        $('.email_container textarea,.email_container .subject').attr('disabled',true);
    }
});
$('.selectSubRecepient').hide();
$(document).on('change','.selectRecepient',function () {
    var emailType = $(this).val();
    if(emailType == 'all')
    {
        $('.selectSubRecepient').hide();
        $('.selectSubRecepient').attr('disabled',true);
    } else {
        $('.selectSubRecepient').show();
        $('.selectSubRecepient').attr('disabled',false);
    }
});


//Summernote Js Starts
$(document).ready(function(){
    $('.summernote').summernote({
        addclass: {
            debug: false,
            classTags: [{title:"Button","value":"btn btn-success"},"jumbotron", "lead","img-rounded","img-circle", "img-responsive","btn", "btn btn-success","btn btn-danger","text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
        },
        width: '100%',
        height: '300px',
        //margin-left: '15px',
        toolbar: [
            // [groupName, [list of button]]
            ['img', ['picture']],
            ['style', ['style', 'addclass', 'clear']],
            ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
            ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
            ['extra', ['video', 'table', 'height']],
            ['misc', ['undo', 'redo', 'codeview', 'help']]
        ]
    });


$('.note-editable').on('keyup keydown keypress',function(e){
    var getCharacters = $('.summernote').summernote('code');
    console.log('dasdsadsa');
    var charCount = getCharacters.length;
    if((charCount==8) || (charCount==1)){
        $('.note-editable').addClass('caps');
    }
    var rx = /INPUT|SELECT|TEXTAREA/i;

    if( e.which == 8 ){ // 8 == backspace
        if(!rx.test(e.target.tagName) || e.target.disabled || e.target.readOnly ){
            $('.note-editable p').text(function (_,getCharacters) {
                return getCharacters.slice(0, -1);
            });
            e.preventDefault();
        }
    }

});
});
//Summernote JS Ends

$("#sendEmail").validate({
    ignore: ".mesgbody",
    rules: {
        recepient: {
            required:true
        },
        subject: {
            required:true
        },
        mesgbody: {
            required:true
        },
        tmesgbody: {
            required:true
        },
        subrecepient: {
            required:true
        },
        gtype: {
            required:true
        },

    }, submitHandler: function (e) {

        var tenant_id = $(".tenant_id").val();
        var form = $('#sendEmail')[0];
        console.log(form);
        var formData = new FormData(form);
        var to = $(".to").val();
        formData.append('to_users',to);
        formData.append('action','saveEmail');
        formData.append('class','saveEmail');
        $.ajax({
            url:'/Communication/GroupMessageAjax',
            type: 'POST',
            data: formData,
            success: function (data) {
                info =  JSON.parse(data);
                if(info.status=="success"){
                    var mail_type = $('.mail_type').val();
                    var select_type = $('.selectType').val();
                    if(mail_type == 'draft') {
                        if(select_type == '1') {
                            toastr.success("Group Email Draft Saved Successfully");
                        } else {
                            localStorage.setItem('gEmailTabSelecttion',true);
                            toastr.success("Group Message Draft Saved Successfully");
                        }

                        setTimeout(function () {
                            window.location.href='/Communication/DraftedGroupMessages';
                        },1000);
                    } else {
                        if(select_type == '1') {
                            toastr.success("Group Email Sent Successfully");
                        } else {
                            localStorage.setItem('gEmailTabSelecttion',true);
                            toastr.success("Group Message Sent Successfully");
                        }
                        setTimeout(function () {
                            window.location.href='/Communication/GroupMessage';
                        },1000);
                    }
                //    localStorage.setItem("Message", 'Record added successfully.');
                   localStorage.setItem('rowcolorgroup', 'rowColor');
                } else{
                    if(info.code=="503" && info.code=="error"){

                    } else {
                        toastr.warning(info.message);
                    }
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});

$(document).on("click",".compose-email-btn",function(){
    $('.mail_type').val('send');
    $('#sendEmail').trigger('submit');
    $('label#mesgbody-error').remove().insertAfter('.note-editor.note-frame.panel');
    $('label#to-error').remove().insertAfter('.to_field .bootstrap-tagsinput');

});

$(document).on("click",".compose-email-save-btn",function(){
    $('.mail_type').val('draft');
    $('#sendEmail').trigger('submit');
    $('label#mesgbody-error').remove().insertAfter('.note-editor.note-frame.panel');
    $('label#to-error').remove().insertAfter('.to_field .bootstrap-tagsinput');
});


function toggleSelectAll(control) {

    var allOptionIsSelected = (control.val() || []).indexOf("All") > -1;
    function valuesOf(elements) {
        return $.map(elements, function(element) {
            return element.value;
        });
    }

    if (control.data('allOptionIsSelected') != allOptionIsSelected) {
        // User clicked 'All' option
        if (allOptionIsSelected) {
            // Can't use .selectpicker('selectAll') because multiple "change" events will be triggered
            control.selectpicker('val', valuesOf(control.find('option')));
        } else {
            control.selectpicker('val', []);
        }
    } else {
        // User clicked other option
        if (allOptionIsSelected && control.val().length != control.find('option').length) {
            // All options were selected, user deselected one option
            // => unselect 'All' option
            control.selectpicker('val', valuesOf(control.find('option:selected[value!=All]')));
            allOptionIsSelected = false;
        } else if (!allOptionIsSelected && control.val().length == control.find('option').length - 1) {
            // Not all options were selected, user selected all options except 'All' option
            // => select 'All' option too
            control.selectpicker('val', valuesOf(control.find('option')));
            allOptionIsSelected = true;
        }
    }
    control.data('allOptionIsSelected', allOptionIsSelected);
}

getView();

function getView()
{
    var compose_mail_id = localStorage.getItem("composer_mail_id");
    var composer_mail_type = localStorage.getItem("composer_mail_type");

    $.ajax({
        url:'/Communication/ComposeMailAjax',
        method: 'post',
        data: {
            class: "PropertyUnitAjax",
            action: "getComposeForView",
            'id': compose_mail_id,
        },
        success: function (data) {
            info =  JSON.parse(data);
            if(info.status=="success"){
                var res = info.data;
                if(composer_mail_type == 'draft' || composer_mail_type == 'forward')
                {

                 if(res.gtype == 'E')
                 {
                     $('.selectType').val('1');
                     $('.email_container').show();
                     $('.email_container textarea,.email_container .subject').attr('disabled',false);
                     $('.text_container textarea').attr('disabled',true);
                 }
                 if(res.gtype == 'T')
                 {
                     $('.selectType').val('2');
                     $('.text_container').show();
                     $('.text_container textarea').attr('disabled',false);
                     $('.email_container textarea,.email_container .subject').attr('disabled',true);
                 }
                if(res.selected_user_type != 'All')
                {
                    $('.selectSubRecepient').show();
                    $('.selectRecepient').val(res.selected_user_type);
                } else {
                    $('.selectRecepient').val('all');

                }


                }
                if(composer_mail_type == 'draft')
                {
                    $('#compose_mail_id').val(res.id);
                }

                if(composer_mail_type == 'forward')
                {
                    localStorage.removeItem('composer_mail_id')
                }

                $('.subject').val(res.email_subject);
                $('.summernote').summernote('code', res.email_message);

                $('textarea[name="tmesgbody"]').text(res.email_message);


                var mail_to = res.user_name;
                if(mail_to)
                {
                    mail_to = mail_to.split(',');
                    selectDropdownRecepient(res.selected_user_type,mail_to);
                }


            }
        },

    });
}
$(document).on('click','#cancel_email',function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            window.location.href = '/Communication/GroupMessage';
        }
    });
});

