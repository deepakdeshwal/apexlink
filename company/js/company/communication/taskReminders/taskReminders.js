$(document).ready(function () {
    // alert('dfsf');


    var unit_popup = false;
    $(document).on("click",".task_reminder_cancel",function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = '/Communication/NewTaskAndReminders';
            }
        });
    });

    $(document).on("click",".blue-btn",function(){
        //  $('input[name="unit_checkbox[]"]').removeAttr('checked');
        var valu = '';
        var data = '';
        var unit_ids_array = [];
        $('input[name="unit_checkbox[]"]:checked').each(function(index,val) {
            var $this = $(this);
            //  val = index + 2;
            valu = $this.attr("data-label");
            data +=   valu +',';
            unit_ids_array.push($this.attr("value"));
        });
        data  = data.replace(/,\s*$/, "");
        $("#unit_ids").val(unit_ids_array);
        $("#unit").val(data);
        $(".unit,#unit-error").removeClass("error");
        $("#unit-error").html("");
        $("#myModal").modal("hide");

    });

    $(document).on("click",".unit_chekbox",function(){
        //   var total=$(this).find('input[name="unit_checkbox[][]"]:checked').length;
        var total_checkbox = $('input[name="unit_checkbox[]"]').length;
        var checkedBoxes = $('input[name="unit_checkbox[]"]:checked').length;
        if(total_checkbox == checkedBoxes){
            $('.select_all_checkbox').attr('checked',true);
        }
    });


    $(document).on("click", "#unit", function () {
        if(unit_popup == true) {
            $('input[name="unit_checkbox[]"]:checked').each(function(index,val) {
                var $this = $(this);
                $this.removeAttr("checked");
            });
            $("#myModal").modal("show");
        }
    });


    $(document).on("click",".close,.cancel_unit",function(){
        $("#myModal").modal("hide");
    });
    $('#product-options').modal('hide');


    $.ajax({
        type: 'post',
        url: '/taskReminder-Ajax',
        data: {
            class: "taskReminderAjax",
            action: "fetchPropertymanagers"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if (data.data.length > 0){
                    // var assigned_to = "<option value=''>Select</option>";
                    // $.each(data.data, function (key, value) {
                    //     propertyOption += "<option value='"+value.id+"' data-id='"+value.id+"'>"+value.property_name+"</option>";
                    // });
                    $('.assigned_to').html(data.data);
                }

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        }
    });

    $.ajax({
        type: 'post',
        url: '/taskReminder-Ajax',
        data: {
            class: "taskReminderAjax",
            action: "getInitialData"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if (data.data.propertyList.length > 0){
                    $(".selectpicker").selectpicker('refresh');
                    var propertyOption = "";
                    $.each(data.data.propertyList, function (key, value) {
                        propertyOption += "<option data-tokens="+value.property_name+" value='"+value.id+"' data-id='"+value.property_id+"'>"+value.property_name+"</option>";
                    });
                    $(".selectpicker").append(propertyOption);
                    //$('.property').html(propertyOption);
                }
                $(".selectpicker").selectpicker('refresh');

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        }
    });
    $('select[name="user_name[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});

    $(document).on("change",".property",function (e) {
        e.preventDefault();
        $('.building').empty();
        $('.unit').empty();
        if($(this).val() != '') {

            getDataByID($(this).val(), $(this).val(), 'property');
            unit_popup = true;
        } else {
            $('.building').html("<option value=''>Select</option>");
            $('.unit').html("<option value=''>Select</option>");
        }


        // getDataByID($('.announcement_for').val(),$(this).val(),'users');
        // $('.user_name').empty();
        // return false;
    });

    $(document).on("change",".building",function (e) {
        e.preventDefault();
        selected = $('.selectpicker').val()
        getDataByID($(this).val(),selected,'building');
        return false;
    });

    $(document).on("change",".announcement_for",function (e) {
        e.preventDefault();
        getDataByID($(this).val(),$('.property').val(),'users');
        return false;
    });


    function getDataByID(data_id, prev_id, data_type){
        if (data_id == '' || data_id == 'null'){
            return;
        }
        var newvar = [];
        $.ajax({
            type: 'post',
            url: '/taskReminder-Ajax',
            data: {
                class: "taskReminderAjax",
                action: "getDataByID",
                id: data_id,
                prev_id: prev_id,
                data_type : data_type
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {

                    if(data_type == 'property') {
                        var buildingOption = "<option value=''>Select</option>";
                        if (data.data.length > 0) {
                            $.each(data.data, function (key, value) {
                                buildingOption += "<option value='" + value.id + "' data-id='" + value.building_id + "'>" + value.building_name + "</option>";
                            });
                        }

                        if(data.property_manager_id.length == 1){
                            $('.assigned_to').val(data.property_manager_id[0]);
                        }
                        $('.building').html(buildingOption);
                    } else if(data_type == 'building') {
                        var concatunit_prefix = '';
                        var unitOption = "<option value=''>Select</option>";
                        var unitCheckboxHtml ="";
                        if (data.data.length > 0) {
                            $('.unit_select_all_checkbox_html').css({"display":"block"});
                            $.each(data.data, function (key, value) {
                                concatunit_prefix =  concatePrefix(value.unit_prefix,value.unit_no)
                                unitOption += "<option value='" + value.id + "' data-id='" + value.id + "'>" + concatunit_prefix + "</option>";
                                unitCheckboxHtml += '<div class="check-outer"><input type="checkbox" name="unit_checkbox[]" class="unit_chekbox" data-label="'+ concatunit_prefix+'" value="'+ value.id+'"/><label>'+ concatunit_prefix +'</label></div>';
                            });
                        }else{
                            var unitCheckboxHtml = "<span>No Unit Present</span>";
                            $("#unit").val('');
                            $('.unit_select_all_checkbox_html').css({"display":"none"});
                        }
                        $('.unit').html(unitOption);
                        $('.unit_checkbox_html').html(unitCheckboxHtml);
                    } else if(data_type == 'users'){
                        var usersOption = "";
                        if (data.data.length > 0){
                            $.each(data.data, function (key, value) {
                                usersOption += "<option value='"+value.id+"'>"+value.name+"</option>";
                            });
                            $('select[name="user_name[]"]').html(usersOption);
                            $('select[name="user_name[]"]').multiselect('destroy');
                            $('select[name="user_name[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                            if(data.data)
                            {
                                newvar = data.data;
                            }


                        } else {
                            $('select[name="user_name[]"]').html('');
                            $('select[name="user_name[]"]').multiselect('destroy');
                            $('select[name="user_name[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});

                        }

                    }

                } else if (data.status == "error") {

                    toastr.error(data.message);
                } else {

                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });


    }

    function concatePrefix(unitprefix,unitno) {
        return (unitprefix ? unitprefix + '-'+ unitno : unitno);
    }
    $('#due_date').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
        onSelect: function (selectedDate) {
            $("#add_announcement_div .end_date").datepicker("option", "minDate", selectedDate);
        }
    }).datepicker("setDate", new Date());

    $(document).on("change","#frequency",function(index,value){
        var $this = $(this);
        var frequency = $this.val();
        var jsDate = $('#due_date').datepicker('getDate');
        var date= '';

        if(frequency == 1){
            date = jsDate.addDays(1)
            $('#next_date').datepicker("setDate", date);
        }else if(frequency == 2){
            date = jsDate.addWeeks(1)
            $('#next_date').datepicker("setDate", date);
        }else if(frequency == 3){
            date = jsDate.addMonths(1)
            $('#next_date').datepicker("setDate", date);
        }else if(frequency == 4){
            date = jsDate.addYears(1)
            $('#next_date').datepicker("setDate", date);
        }else if(frequency == 5){
            date = jsDate.addMonths(6)
            $('#next_date').datepicker("setDate", date);
        }


    });

    $(document).on("click","#reccuring_task_checkbox",function(){
        if($(this).prop("checked") == true){
            $("#recurring_div").css("display","block");
        }
        else if($(this).prop("checked") == false){
            $("#recurring_div").css("display","none");
        }
    });
    $('#next_date').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
    }).datepicker("setDate", Date.today().addDays(1) );


    //add announcement
    $("#add_task_reminder_form").validate({
        rules: {
            title: {
                required: true
            },
            property: {
                required: true
            },
            building: {
                required: true
            },
            unit: {
                required: true
            },
            details: {
                required: true
            }
        },
        submitHandler: function (form) {
            event.preventDefault();
            var form = $('#add_task_reminder_form')[0];
            var formData = new FormData(form);
            var data = convertSerializeDatatoArray();
            formData.append('class', 'taskReminderAjax');
            formData.append('action', 'addTaskReminder');
            $.ajax({
                url: '/taskReminder-Ajax',
                type: 'post',
                data:  formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        localStorage.setItem("Message","Record created successfully.")
                        localStorage.setItem("rowcolor",true)
                        // toastr.success("Announcement Created Successfully.");
                        window.location.href = '/Communication/NewTaskAndReminders';
                    }else if(response.status == 'error' && response.code == 502){
                        toastr.error(response.message);
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                }
            });
        }
    });

    function getParameterByName( name ){
        var regexS = "[\\?&]"+name+"=([^&#]*)",
            regex = new RegExp( regexS ),
            results = regex.exec( window.location.search );
        if( results == null ){
            return "";
        } else{
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }
    var edit_id =  getParameterByName('id');
    if(edit_id){
        console.log('id>>', edit_id);
        $.ajax({
            type: 'post',
            url: '/Announcement-Ajax',
            data: {
                class: "announcementAjax",
                action: "getAnnouncementById",
                id: edit_id,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    var res = response.data;
                    getDataByID(res.property, '', 'property');       //Building listing
                    getDataByID(res.property,res.building,'building');       //Unit listing
                    var userData = getDataByID(res.announcement_for,res.property,'users');  //Users listing

                    console.log('getAnnouncementById>>', response);
                    console.log('userData>>', userData);
                    setTimeout(function () {
                        $.each(res, function (key, value) {
                            $('#edit_announcement_div .'+key).val(value);
                            if (key == 'start_time') {
                                $('#edit_announcement_div .start_time').timepicker('setTime', value);
                            }
                            if (key == 'end_time') {
                                $('#edit_announcement_div .end_time').timepicker('setTime', value);
                            }
                            else if(key == 'user_name')
                            {
                                var userDataInitial = [];
                                $('#edit_announcement_div .user_name option').each(function (key,value) {
                                    userDataInitial.push({'id':$(this).val(),'text':$(this).text()});
                                });
                                selectUserNames(userDataInitial,value);
                            }
                        });

                    }, 300);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }


    function selectUserNames(userDataInitial,value) {

        if (userDataInitial.length > 0){
            var nameOption = "";

            $.each(userDataInitial, function (nameKey, nameData) {
                if($.inArray(nameData.id, value) !== -1) {
                    nameOption += "<option value='"+nameData.id+"' selected>"+nameData.text+"</option>";
                } else {
                    nameOption += "<option value='"+nameData.id+"'>"+nameData.text+"</option>";
                }
            });
            setTimeout(function () {
                $("#user_name_e").multiselect("clearSelection");
                $('#user_name_e').html(nameOption);

                $('#user_name_e').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                $("#user_name_e").multiselect( 'refresh' );
            },400);
        }
    }


$(document).on('click','.select_all_checkbox',function () {
    if($(this).is(":checked")==true) {
            $('.unit_chekbox').prop('checked',true);
    } else {
        $('.unit_chekbox').prop('checked',false);
    }
});




    /*** <h3>Edit Announcement Div JS Ends</h3> ***/

});