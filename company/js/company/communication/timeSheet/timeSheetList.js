getEmployees();
/**
 * function to get the list of employees
 */
function getEmployees(){
    $.ajax({
        type: 'post',
        url: '/timeSheet-Ajax',
        data: {
            class: 'TimeSheetAjax',
            action: 'getEmployees'
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200){
                var html = '<option value="all">Select Employee</option>';
                $.each(response.data, function (key, value) {
                    html += '<option value="'+value.id+'" data_email="'+value.email+'" data_phone="'+value.mobile_number+'" data_login="'+value.last_login+'">'+value.name+'</option>';
                });
                $('#TimeSheetUsers').html(html);
            }
        }
    });
}

/**
 * function to select search filter
 */
$(document).on('click','.filter_class',function(){
    var checked_value = $('input[name=filter_search]:checked').val()
    if(checked_value == '0'){
        $('#ByWeek').show();
        $('#ByDate').hide();
    } else if(checked_value == '1') {
        $('#ByDate').show();
        $('#ByWeek').hide();
    }
});


$(document).ready(function () {


    $(document).on('click','#ByWeekFilter',function(){
        if($('#ByWeekFilter option:selected').val() == 'Monthly'){
            $('#ByMonth').show();
        } else {
            $('#ByMonth').hide();
        }
    });

    $('.from123').Monthpicker();
    //jqGrid status
    $(document).on('change','.common_ddl',function(){
        searchFilters();
    });

    $(document).on('change', '#timeSheetTable .select_options', function () {
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        switch (opt) {
            case "Edit":
                window.location.href = "/Communication/EditTimeSheet?id="+id;
                break;
            case "Delete":
                bootbox.confirm({
                    message: "Do you want to Delete this record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/timeSheet-Ajax',
                                data: {
                                    class: 'TimeSheetAjax',
                                    action: 'delete',
                                    id: id
                                },
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.code == 200) {
                                        toastr.success(response.message);
                                    }
                                    $('#timeSheetTable').trigger('reloadGrid');
                                }


                            });
                        }
                    }
                });
                break;
            default:
                $('#timeSheetTable .select_options').prop('selectedIndex',0);
        }
        $('#timeSheetTable').trigger('reloadGrid');

        $('#timeSheetTable .select_options').prop('selectedIndex',0);
    });


    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('all');
    function jqGrid(status) {
        if(jqgridNewOrUpdated == 'true') {
            var sortOrder = 'desc';
            var sortColumn = 'time_sheet.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }
        var table = 'time_sheet';
        var columns = ['Employee Name','Position','Email', 'Phone', 'Date', 'Last Login', 'Time In','Time Out','Time', 'Action'];
        var select_column = ['Edit', 'Delete'];
        var joins = [{table: 'time_sheet', column: 'employee_id', primary: 'id', on_table: 'users'},{table: 'time_sheet', column: 'position', primary: 'id', on_table: 'company_user_roles'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [];
        var columns_options = [
            {name: 'Employee Name', title:true, index: 'name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer'},
            {name: 'Position', title:true, index: 'role_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: 'company_user_roles',formatter: roleFormater},
            {name: 'Email', index: 'email',title:false, width: 80,searchoptions: {sopt: conditions}, table: table}, /**cellattr:cellAttrdata**/
            {name: 'Phone', index: 'mobile_number', width: 80, align: "left", searchoptions: {sopt: conditions}, table: table},
            {name: 'Date', index: 'date', width: 80, align: "left", searchoptions: {sopt: conditions}, table: table,change_type:'date'},
            {name: 'Last Login', index: 'last_login', width: 80, align: "left", searchoptions: {sopt: conditions}, table: table,change_type:'time'},
            {name: 'Time In', index: 'time_in', width: 80, align: "left", searchoptions: {sopt: conditions}, table: table,change_type:'time'},
            {name: 'Time Out', index: 'time_out', width: 100, align: "left", searchoptions: {sopt: conditions}, table: table,change_type:'time'},
            {name: 'Time', index: 'total_hours', width: 80, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:timeFormater},
            {name: 'Action', index: 'select', title: false, width: 80, align: "right",formatter: 'select', sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, edittype: 'select', search: false, table: table}
        ];
        var ignore_array = [];
        jQuery("#timeSheetTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            width: '100%',
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Employee Time Sheet",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
        );
    }

});

$('#start_date').datepicker({
    yearRange: '-0:+100',
    changeMonth: true,
    changeYear: true,
    dateFormat: datepicker,
    maxDate:new Date()
}).datepicker("setDate", new Date());

$('#end_date').datepicker({
    yearRange: '-0:+100',
    changeMonth: true,
    changeYear: true,
    dateFormat: datepicker,
    maxDate:new Date()
}).datepicker("setDate", new Date());

$(document).on('click','.callLogSearch',function(){
    searchFilters();
});

function searchFilters(){
    var grid = $("#timeSheetTable"),f = [];
    var type = $('#TimeSheetUsers option:selected').val();
    var checked_value = $('input[name=filter_search]:checked').val()
    if(checked_value == '0'){
        var data = $('#ByWeekFilter option:selected').val();
        if(data == 'Monthly'){
            var monthly = $('.monthpicker_input').text() != 'Select Month'? $('.monthpicker_input').text() :'all';
            f.push({field: "time_sheet.employee_id", op: "eq", data: type,int:'yes'},{field: "time_sheet.date", op: "dateByString", data: monthly});
        } else {
            f.push({field: "time_sheet.employee_id", op: "eq", data: type,int:'yes'},{field: "time_sheet.date", op: "dateByString", data: data});
        }

    } else if(checked_value == '1') {
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        f.push({field: "time_sheet.employee_id", op: "eq", data: type,int:'yes'},{field: "time_sheet.date", op: "dateBetween", data: start_date, data2:end_date});
    }
    grid[0].p.search = true;
    $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}

$(document).on("click","#print_preview",function(){
    $.ajax({
        type: 'post',
        url: '/timeSheet-Ajax',
        data: {
            class: "TimeSheetAjax",
            action: "getTimeSheetPrintData"
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                // update other modal elements here too
                $('#inventory_content').html(response.data);
                // show modal
                $('#PhoneCallLogModal').modal('show');
            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
});

/*
Function to print envelope
*/
function PrintElem(elem)
{
    Popup($(elem).html());
}

function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
        'font-weight:bold;} .right-detail{\n' +
        '        position:relative;\n' +
        '        left:+400px;\n' +
        '    }</style>');
    $(mywindow.document.body).html( '<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if(mywindow.close()){

    }
    $("#PrintEnvelope").modal('hide');
    return true;
}

function roleFormater(cellValue, options, rowObject){
    if(rowObject !== undefined){
        if(rowObject.Position == ''){
            return 'Employee';
        } else {
            return cellValue;
        }
    }
}

function timeFormater(cellValue, options, rowObject){
    if(rowObject !== undefined){
        if(rowObject.Time == ''){
            return cellValue;
        } else {
            return cellValue+' Hours';
        }
    }
}

