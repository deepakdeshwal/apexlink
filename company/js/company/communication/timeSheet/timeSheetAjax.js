getEmployees();
getPosition();
var timeSring = '1';
jQuery('#employeePhone').mask('000-000-0000', {reverse: true});

/**
 * function to get the list of employees
 */
function getEmployees(){
    $.ajax({
        type: 'post',
        url: '/timeSheet-Ajax',
        data: {
            class: 'TimeSheetAjax',
            action: 'getEmployees'
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200){
                var html = '<option value="default">Select Employee</option>';
                $.each(response.data, function (key, value) {
                    var role = value.role != ''?value.role:'0';
                    html += '<option value="'+value.id+'" data_email="'+value.email+'" data_phone="'+value.phone_number+'" data_login="'+value.last_login+'" data_role="'+role+'">'+value.name+'</option>';
                });
                $('#employee_id').html(html);
            }
        }
    });
}

/**
 * function to get position of employees
 */
function getPosition(){
    $.ajax({
        type: 'post',
        url: '/timeSheet-Ajax',
        data: {
            class: 'TimeSheetAjax',
            action: 'getPosition'
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200){
                var html = '<option value="default">Select Position</option>';
                $.each(response.data, function (key, value) {
                    html += '<option value="'+value.id+'" >'+value.role_name+'</option>';
                });
                html += '<option value="0">Employee</option>';
                $('#position').html(html);
            }
        }
    });
}

$(document).on('change','#employee_id',function(){
   var email = $('#employee_id option:selected').attr('data_email') != 'null'?$('#employee_id option:selected').attr('data_email'):'';
   var mobile_number = $('#employee_id option:selected').attr('data_phone') != 'null'?$('#employee_id option:selected').attr('data_phone'):'';
   var last_login = $('#employee_id option:selected').attr('data_login') != 'null'?$('#employee_id option:selected').attr('data_login'):'';
   var role = $('#employee_id option:selected').attr('data_role') != 'null'?$('#employee_id option:selected').attr('data_role'):'0';
   if($(this).val() != 'default') {
       $('#employeeEmail').val(email).attr('readonly', true);
       $('#employeePhone').val(mobile_number).attr('readonly', true);
       $('#employeeLogin').val(last_login).attr('readonly', true);
       $('#position').val(role).attr('readonly', true);
   } else{
       $('#position').val('default');
   }
});
$("#employeeDate").datepicker({dateFormat: datepicker,
    changeYear: true,
    yearRange: "-100:+20",
    changeMonth: true,
    maxDate: new Date(),
});

$('#time_in_employee').timepicker({
    timeFormat: 'h:mm p',
    interval: 5,
    minTime: '8',
    maxTime: '08:00pm',
    defaultTime: '08',
    startTime: '08:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true,
    change:getTimeDiffrence
});

$('#time_out_for_meal').timepicker({
    timeFormat: 'h:mm p',
    interval: 5,
    minTime: '10',
    maxTime: '09:55pm',
    defaultTime: '10',
    startTime: '10:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true,
    change:getTimeDiffrence
});


$('#time_in_after_meal').timepicker({
    timeFormat: 'h:mm p',
    interval: 5,
    minTime: '8',
    maxTime: '08:00pm',
    defaultTime: '08',
    startTime: '08:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true,
    change:getTimeDiffrence
});
$('#time_out').timepicker({
    timeFormat: 'h:mm p',
    interval: 5,
    minTime: '10',
    maxTime: '09:55pm',
    defaultTime: '10',
    startTime: '10:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true,
    change:getTimeDiffrence
});

function getTimeDiffrence(val){
    if(timeSring != '1') {
        checkTimeValidation();
    }
    setTimeout(function(){ timeSring = '2' }, 1000);
}



function checkTimeValidation(){
    $.ajax({
        type: 'post',
        url: '/timeSheet-Ajax',
        data: {
            class:"TimeSheetAjax",
            action:'calculateTimeDiff',
            time_in: $('#time_in_employee').val(),
            time_out_for_meal: $('#time_out_for_meal').val(),
            time_in_after_meal: $('#time_in_after_meal').val(),
            time_out: $('#time_out').val()
        },
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status == 'success' && response.code == 200){
                var time = parseFloat(response.data.replace(":", "."));
                $('#total_hours').val(time);
            } else if(response.code == 400){
                toastr.warning(response.data);
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}

$('#time_sheet_form').on('submit',function(event){
    event.preventDefault();
    if($('#time_sheet_form').valid()){
        var formData = $('#time_sheet_form').serializeArray();
        $.ajax({
            type: 'post',
            url: '/timeSheet-Ajax',
            data: {
                class:"TimeSheetAjax",
                action:'saveTimeSheet',
                form: formData,
                time_in: $('#time_in_employee').val(),
                time_out_for_meal: $('#time_out_for_meal').val(),
                time_in_after_meal: $('#time_in_after_meal').val(),
                time_out: $('#time_out').val()
            },
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    localStorage.setItem("rowcolor",'green');
                    localStorage.setItem("Message",response.message);
                    window.location.href = '/Communication/TimeSheet';
                } else if(response.status == 'error' && response.code == 500) {
                    toastr.warning(response.message);
                } else if(response.status == 'warning' && response.code == 400){
                    toastr.warning(response.data);
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
});

$(document).on("click","li#time_out_for_meal",function(){
    var startTime = $('#time_in_employee').val();
    var endTime   = $('#time_out_for_meal').val();
    st = minFromMidnight(startTime);
    et = minFromMidnight(endTime);
    if(st>et){
        alert("End time must be greater than start time");
    }
})

function minFromMidnight(tm){
    var ampm= tm.substr(-2)
    var clk = tm.substr(0, 5);
    var m  = parseInt(clk.match(/\d+$/)[0], 10);
    var h  = parseInt(clk.match(/^\d+/)[0], 10);
    h += (ampm.match(/pm/i))? 12: 0;
    return h*60+m;
}

$(document).on('click','.cancel_timeSheet',function(){
    bootbox.confirm("Are you sure you want to do this action?", function (result) {
        if (result == true) {
            window.location.href = '/Communication/TimeSheet';
        }
    });
});

