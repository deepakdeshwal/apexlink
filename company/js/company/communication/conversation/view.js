$(document).on("click","#add_tenant_conversation_btn",function(){
    $('.add_tenant_conversation_div').find('textarea').val('');
    var validator = $("#add_tenant_conversation_form_id").validate();
    validator.resetForm();
    fetchAllProblemCategory();
    fetchAllUsers('2');
    $('.tenant_conversation_div').hide();
    $('.add_tenant_conversation_div').show();
});
$(document).on("click","#tenant_tab",function(){
    $('.tenant_conversation_div').show();
    $('.add_tenant_conversation_div').hide();
    showTabViseData("tenant_tab");
});
$(document).on("click","#cancel_add_tenant_conversation",function(){
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $('.tenant_conversation_div').show();
            $('.add_tenant_conversation_div').hide();
        }
    });
});

$(document).on("click","#add_owner_conversation_btn",function(){
    $('.add_owner_conversation_div').find('textarea').val('');
    var validator = $("#add_owner_conversation_form_id").validate();
    validator.resetForm();
    fetchAllProblemCategory();
    fetchAllUsers('4');
    $('.owner_conversation_div').hide();
    $('.add_owner_conversation_div').show();
});
$(document).on("click","#owner_tab",function(){
    $('.owner_conversation_div').show();
    $('.add_owner_conversation_div').hide();
    showTabViseData("owner_tab");
});
$(document).on("click","#cancel_add_owner_conversation",function(){
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $('.owner_conversation_div').show();
            $('.add_owner_conversation_div').hide();
        }
    });
});

$(document).on("click","#add_vendor_conversation_btn",function(){
    var validator = $("#add_vendor_conversation_form_id").validate();
    validator.resetForm();
    $('.add_vendor_conversation_div').find('textarea').val('');
    fetchAllProblemCategory();
    fetchAllUsers('3');
    $('.vendor_conversation_div').hide();
    $('.add_vendor_conversation_div').show();
});
$(document).on("click","#vendor_tab",function(){
    $('.vendor_conversation_div').show();
    $('.add_vendor_conversation_div').hide();
    showTabViseData("vendor_tab");
});
$(document).on("click","#cancel_add_vendor_conversation",function(){
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $('.vendor_conversation_div').show();
            $('.add_vendor_conversation_div').hide();
        }
    });
});

var active_tab = $('.conversation-tabList li.active a').attr('id');

setTimeout(function () {
    var sideNavTab = localStorage.getItem('side-nav');
    if (sideNavTab == 'new-conversation'){
        $('.tenant_conversation_div').hide();
        $('.add_tenant_conversation_div').show();
        showTabViseData();
        fetchAllProblemCategory();
        localStorage.removeItem('side-nav');
    } else {
        showTabViseData(active_tab);
    }
}, 200);

function showTabViseData(active_tab) {
    if (active_tab == 'tenant_tab') {
        fetchAllManagers();
        fetchAllUsers('2');
        fetchAllConversations('','');
    } else if (active_tab == 'owner_tab'){
        fetchAllManagers();
        fetchAllUsers('4');
        fetchAllConversations('','');
    } else {
        fetchAllManagers();
        fetchAllUsers('3');
        fetchAllConversations('','');
    }
}

fetchTotalUsersInConversation();
function fetchTotalUsersInConversation() {
    $.ajax({
        type: 'post',
        url: '/Conversation-Ajax',
        data: {
            class: 'conversationAjax',
            action: 'grtTotalUsersInConversation',
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('#tenant_tab').text('Tenant ('+res.data.tenantCount+')');
            $('#owner_tab').text('Owner ('+res.data.ownerCount+')');
            $('#vendor_tab').text('Vendor ('+res.data.vendorCount+')');
        },
    });
}

function fetchAllManagers() {
    $.ajax({
        type: 'post',
        url: '/fetch-managers',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllPropertyManagers'},
        success: function (response) {
            var res = JSON.parse(response);
            $('.select_mangers_option').html(res.data);
        },
    });
}
function fetchAllUsers(user_type) {
    $.ajax({
        type: 'post',
        url: '/Conversation-Ajax',
        data: {
            class: 'conversationAjax',
            action: 'getInitialData',
            user_type: user_type
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('.user_id').html(res.data);
            $('.selected_user_id').html(res.data);
            // $('#tenant_tab').text('Tenant ('+res.totalTenant.total_tenant+')');
            // $('#owner_tab').text('Owner ('+res.totalOwner.total_owner+')');
            // $('#vendor_tab').text('Vendor ('+res.totalVendor.total_vendor+')');
        },
    });
}

function fetchAllProblemCategory(){
    $.ajax({
        type: 'post',
        url: '/Conversation-Ajax',
        data: {
            class: 'conversationAjax',
            action: 'getAllProblemCategory',
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('.problem_category').html(res.data);
        },
    });
}

/**** Save problem category popup data starts ****/

$(document).on("click",".pop-add-icon",function () {
    $(".add-popup-body input[type='text']").val('');
    var text_id = $(this).closest('div').find('.add-popup').attr('id');
    $("#"+text_id+" .required").text('');
    $(this).closest('div').find('.add-popup').show();
});

$(document).on("click",".add-popup .grey-btn",function(){
    var box = $(this).closest('.add-popup');
    $(box).hide();
});

$(document).on("click",".add_single1", function () {
    var tableName = $(this).attr("data-table");
    var colName = $(this).attr("data-cell");
    var className = $(this).attr("data-class");
    var selectName = $(this).attr("data-name");
    var val = $(this).parent().prev().find("."+className).val();

    if (val == ""){
        $(this).parent().prev().find("."+className).next().text("* This field is required");
        return false;
    }else{
        $(this).parent().prev().find("."+className).next().text("");
    }

    savePopUpData(tableName, colName, val, selectName);
    var val = $(this).parent().prev().find("input[type='text']").val('');
});

function  savePopUpData(tableName, colName, val, selectName) {
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/savePopUpData',
        data: {
            class: "TenantAjax",
            action: "savePopUpData",
            tableName:tableName,
            colName: colName,
            val:val
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                $("select[name='"+selectName+"']").append(option);
                $("select[name='"+selectName+"']").next().hide();
                toastr.success(data.message);
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
/**** Save problem category popup data ends here****/


$("#add_tenant_conversation_form_id").validate({
    rules: {
        selected_user_id: {
            required: true
        },
        problem_category: {
            required: true
        },
        description: {
            required: true
        }
    },
    submitHandler: function (form) {
        event.preventDefault();
        var active_tab = $('.conversation-tabList li.active a').attr('id');

        var form = $('#add_tenant_conversation_form_id')[0];
        var formData = new FormData(form);


        formData.append('class', 'conversationAjax');
        formData.append('action', 'addConversation');
        formData.append('active_tab', active_tab);
        console.log('formData>>', formData);

        $.ajax({
            url: '/Conversation-Ajax',
            type: 'post',
            data:  formData,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    $('.tenant_conversation_div').show();
                    $('.add_tenant_conversation_div').hide();
                    fetchTotalUsersInConversation();
                    toastr.success("Record created successfully.");
                    fetchAllConversations('','');
                }else if(response.status == 'error' && response.code == 502){
                    toastr.error(response.message);
                } else if(response.status == 'error' && response.code == 400){
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.'+key).html(value);
                    });
                }
            }
        });
    }
});

$("#add_owner_conversation_form_id").validate({
    rules: {
        selected_user_id: {
            required: true
        },
        problem_category: {
            required: true
        },
        description: {
            required: true
        }
    },
    submitHandler: function (form) {
        event.preventDefault();
        var active_tab = $('.conversation-tabList li.active a').attr('id');

        var form = $('#add_owner_conversation_form_id')[0];
        var formData = new FormData(form);


        formData.append('class', 'conversationAjax');
        formData.append('action', 'addConversation');
        formData.append('active_tab', active_tab);
        console.log('formData>>', formData);

        $.ajax({
            url: '/Conversation-Ajax',
            type: 'post',
            data:  formData,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    $('.owner_conversation_div').show();
                    $('.add_owner_conversation_div').hide();
                    fetchTotalUsersInConversation();
                    fetchAllConversations('','');
                    toastr.success("Record created successfully.");
                }else if(response.status == 'error' && response.code == 502){
                    toastr.error(response.message);
                } else if(response.status == 'error' && response.code == 400){
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.'+key).html(value);
                    });
                }
            }
        });
    }
});

$("#add_vendor_conversation_form_id").validate({
    rules: {
        selected_user_id: {
            required: true
        },
        problem_category: {
            required: true
        },
        description: {
            required: true
        }
    },
    submitHandler: function (form) {
        event.preventDefault();
        var active_tab = $('.conversation-tabList li.active a').attr('id');

        var form = $('#add_vendor_conversation_form_id')[0];
        var formData = new FormData(form);

        formData.append('class', 'conversationAjax');
        formData.append('action', 'addConversation');
        formData.append('active_tab', active_tab);
        $.ajax({
            url: '/Conversation-Ajax',
            type: 'post',
            data:  formData,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    $('.vendor_conversation_div').show();
                    $('.add_vendor_conversation_div').hide();
                    fetchTotalUsersInConversation();
                    fetchAllConversations('','');
                    toastr.success("Record created successfully.");
                }else if(response.status == 'error' && response.code == 502){
                    toastr.error(response.message);
                } else if(response.status == 'error' && response.code == 400){
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.'+key).html(value);
                    });
                }
            }
        });
    }
});

$(document).on("change",".select_mangers_option",function(){
    var selected_manager = $(this).val();
    var selected_user = $(this).parent().next().find('.user_id').val();

    fetchAllConversations(selected_manager,selected_user);
});

$(document).on("change",".user_id",function(){
    var selected_user = $(this).val();
    var selected_manager = $(this).parent().prev().find('.select_mangers_option').val();
    fetchAllConversations(selected_manager,selected_user);
});

function fetchAllConversations(selected_manager,selected_user){
    var active_tab = $('.conversation-tabList li.active a').attr('id');
    $.ajax({
        type: 'post',
        url: '/Conversation-Ajax',
        data: {
            class: 'conversationAjax',
            action: 'getAllConversationByType',
            active_tab : active_tab,
            selected_manager : selected_manager,
            selected_user : selected_user,
        },
        success: function (response) {
            var res = JSON.parse(response);
            var html = '';
            $.each(res.data, function (key, val) {
                var user_type = '';

                if (val.comment_by == 'yes'){
                    user_type = val.user_type;
                } else {
                    if (val.user_type == 1) {
                        user_type = 'Tenant'
                    } else if (val.user_type == 2) {
                        user_type = 'Owner'
                    } else {
                        user_type = 'Vendor'
                    }
                }

                var user_image = '<img src='+default_Image+'>';
                if(val.selected_user_image){
                    if (val.selected_user_image.owner_image && val.selected_user_image.owner_image !='undefined') {
                        user_image = val.selected_user_image.owner_image;
                    }
                    if (val.selected_user_image.tenant_image && val.selected_user_image.tenant_image !='undefined') {
                        user_image = val.selected_user_image.tenant_image;
                    }
                    if (val.selected_user_image.image && val.selected_user_image.image !='undefined') {
                        user_image = val.selected_user_image.image;
                    }
                }

                html += '<li >\n' +
                    '<div class="row">\n' +
                    '<div class="col-sm-1 conversation-lt">\n' +
                    '<div class="img-outer">\n' +user_image+
                    '</div>\n' +
                    '</div>\n' +
                    '<div class="col-sm-10 pad-none conversation-rt">\n' +
                    '<span class="dark_name">'+val.login_user_name+'('+val.login_user_role+') :</span>\n' +
                    '<span class="light_name">'+val.selected_user_name+'('+user_type+')</span>\n' +
                    '<span class="dark_name">'+val.problem_category_name+'</span>\n' +
                    '<span class="black_text" >'+val.description+'</span>\n' +
                    '<div class="conversation-time clear">\n' +
                    '<span class="black_text" >'+val.created_date+'-</span>\n' +
                    '<a class="light_name comment_on_conversation" conversation_id="'+val.id+'">Comment</a>\n' +
                    '</div>\n' +
                    '<div class="viewConversationCommentDiv">\n' +
                    '</div>'+
                    '<div class="textConversationCommentDiv">\n' +
                    '</div>'+
                    '</div>\n' +
                    '<div class="col-sm-1 pull-right">\n' +
                    '<span class="viewAllComment" conversation_id="'+val.id+'" ><i class="fa fa-plus-circle" aria-hidden="true"></i></span> \n' +
                    '</div>\n' +
                    '</div>\n' +
                    '</li>';
            });
            $('.conversation_list').html(html);
        },
    });
}
$(document).on("click",".viewAllComment",function () {
    var active_tab = $('.conversation-tabList li.active a').attr('id');
    var conversation_id = $(this).attr('conversation_id');
    var viewCommentHtml = $(this).parent().prev().find('.viewConversationCommentDiv');
    var viewCommentFindActive = $(this).parent().prev().find('.viewConversationCommentDiv').hasClass('active');
    if(viewCommentFindActive) {
        viewCommentHtml.html('');
        viewCommentHtml.removeClass('active');
    } else {
        $.ajax({
            type: 'post',
            url: '/Conversation-Ajax',
            data: {
                class: 'conversationAjax',
                action: 'getAllConversationByType',
                conversation_id: conversation_id,
                active_tab : active_tab
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    var comment_html = '';
                    $.each(response.data, function (key, val) {
                        var user_image = '<img src='+default_Image+'>';
                        if(val.selected_user_image){
                            if (val.selected_user_image.owner_image && val.selected_user_image.owner_image !='undefined') {
                                user_image = val.selected_user_image.owner_image;
                            }
                            if (val.selected_user_image.tenant_image && val.selected_user_image.tenant_image !='undefined') {
                                user_image = val.selected_user_image.tenant_image;
                            }
                            if (val.selected_user_image.image && val.selected_user_image.image !='undefined') {
                                user_image = val.selected_user_image.image;
                            }
                        }


                        comment_html += '<div style="" id="ConversationOwnerComment_0">\n' +
                            '<div class="conversation-comment-grey removeClass">\n' +
                            '<div class="conversation-arrow"></div>\n' +
                            '<div class="col-sm-1 conversation-lt">\n' +
                            '<div class="img-outer">\n' +user_image+
                            '</div>\n' +
                            '</div>\n' +
                            '<div class="col-sm-10 conversation-rt">\n' +
                            '<span class="dark_name">' + val.login_user_name + '(' + val.login_user_role + ') :</span>\n' +
                            '<span class="black_text" >' + val.description + '</span>\n' +
                            '<div class="conversation-comment-time">' + val.created_date + '</div>\n' +
                            '</div>\n' +
                            '</div>\n' +
                            '</div>'
                    });
                    viewCommentHtml.append(comment_html);
                    viewCommentHtml.addClass('active');
                } else if (response.status == 'error' && response.code == 502) {
                    toastr.error(response.message);
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            }
        });
    }
});

$(document).on("click",".comment_on_conversation",function () {
    var conversation_id = $(this).attr('conversation_id');
    $('.textConversationCommentDiv').html('');
    var commentHtml = '<form id="sendConversationCommentForm">\n' +
        '<input type="hidden" name="conversation_id" value="'+conversation_id+'">\n' +
        '<textarea class="form-control capital" name="comment_text" rows="6"></textarea>\n' +
        '<div class="btn-outer top-marginCls">\n' +
        '<button class="blue-btn sendConversationCommentBtn">Send</button>\n' +
        '<button  type="button" class="grey-btn cancelConversationCommentDiv">Cancel</button>\n' +
        '</div>\n' +
        '</form>\n';
    $(this).parent().next().next().html(commentHtml);
});
$(document).on("click",".cancelConversationCommentDiv",function () {
    console.log('cancel>>>',  $(this).parent());
    $(this).parents('.textConversationCommentDiv').html('');
});

$(document).on('submit','#sendConversationCommentForm',function(e) {
    e.preventDefault();
    var active_tab = $('.conversation-tabList li.active a').attr('id');
    var form = $('#sendConversationCommentForm')[0];
    var formData = new FormData(form);

    formData.append('class', 'conversationAjax');
    formData.append('action', 'sendCommentOnConversation');
    formData.append('active_tab', active_tab);

    $.ajax({
        url: '/Conversation-Ajax',
        type: 'post',
        data:  formData,
        contentType: false,
        processData: false,
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status == 'success' && response.code == 200){
                fetchAllConversations('','');
                toastr.success("Record created successfully.");
            }else if(response.status == 'error' && response.code == 502){
                toastr.error(response.message);
            } else if(response.status == 'error' && response.code == 400){
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.'+key).html(value);
                });
            }
        }
    });
});
