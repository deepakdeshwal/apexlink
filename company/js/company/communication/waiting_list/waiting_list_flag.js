$(document).ready(function () {


  var namewaiting=  localStorage.getItem("waitingname");
  $("input[name='object_name']").val(namewaiting);
    $('.flag-container #flagform input[name="date"]').datepicker({
        dateFormat: jsDateFomat
    });
    var currentDate = $.datepicker.formatDate(jsDateFomat, new Date());
    $('.flag-container #flagform input[name="date"]').val(currentDate);

    jQuery('.phone_format').mask('000-000-0000', {reverse: true});

    $(document).on("click",".flag-container .addbtnflag",function(){
        $(this).hide();

        $(".flag-container #flag_reason").val("");

        $(".flag-container #flagform .saveflag").text("Save");
        $(".flag-container #flagform").show();
        var tenantId = $(".tenant_id").val();
        //  console.log('dd',tenantId);
        getFlagInfo(tenantId);
    });

    $(document).on("click",".flag-container #flagform .grey-btn",function(){
        $('.flag-container .addbtnflag').show();
        $(".flag-container #flagform").hide();
    });

    $(document).on("click",".flag-container #flagform .saveflag",function(e){
        e.preventDefault();
        $(".flag-container #flagform").trigger('submit');
        return false;
    });



    $(document).on("submit",".flag-container #flagform", function(e){

        e.preventDefault();
        saveFlagInfo();
        return false;
    });
    jqGridFlags('All');
    $(document).on("change", "#flaglistingtable .select_options", function (e) {
        e.preventDefault();
        var tenantid = $(".tenant_id").val();
        var base_url = window.location.origin;
        var action = this.value;
        var id = $(this).attr('data_id');
        switch (action) {
            case "Edit":
                flagChangeOption('getRecord',id);
               setTimeout(function () {
                   $("input[name='object_name']").val(namewaiting);
               },50) ;
                $('#flaglistingtable').trigger( 'reloadGrid' );

                break;
            case "Delete":
                bootbox.confirm("Are you sure you want to delete this Flag?", function (result) {
                    if (result == true) {
                        flagChangeOption('deleted_at',id);
                        $('#flaglistingtable').trigger( 'reloadGrid' );
                    }else{
                        $('#flaglistingtable').trigger( 'reloadGrid' );
                    }
                });
                break;
            case "Completed":
                flagChangeOption('completed',id);
                $('#flaglistingtable').trigger( 'reloadGrid' );
                break;
            default:
                window.location.href = window.location.href;
        }
    });
});

function flagChangeOption(fieldType,id) {
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/flagChangeOption',
        data: {
            class: "TenantAjax",
            action: "flagChangeOption",
            recordid: id,
            fieldType: fieldType
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                if (fieldType == "getRecord"){
                    if (response.data.country.length > 0){
                        var cnt = response.data.country;
                        var countryOption = "<option value='0'>Select</option>";
                        $.each(cnt, function (key, value) {
                            if (value.id == response.data.record.country_code){
                                countryOption += "<option value='"+value.id+"' data-id='"+value.code+"' selected>"+value.name+" ("+value.code+")"+"</option>";
                            }else{
                                countryOption += "<option value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")"+"</option>";
                            }
                        });
                        $(".flag-container #flagform select[name='country_code']").html(countryOption);
                    }

                    var rcrd = response.data.record;
                    $(".flag-container .addbtnflag").hide();
                    $(".flag-container #flagform").trigger('reset');
                    var name=$(".hidden_name_val").val();
                    var name2=$("#first_name").val();
                    var last2=$("#last_name_user").val();
                    var last3=$("#last_name").val();
                    $(".flag_name342").val(name2+' '+last2);
                    $(".flag_name3424").val(name2+' '+last3);
                    $(".flag-container #flagform input[name='flag_name']").val(rcrd.flag_name);
                    $(".flag-container #flagform input[name='date']").val(rcrd.date);
                    $(".flag-container #flagform input[name='object_name']").val(rcrd.object_name);
                   $(".flag-container #flagform input[name='flag_phone_number']").val(rcrd.flag_phone_number);
                    $(".flag-container #flagform input[name='flag_reason']").val(rcrd.flag_reason);
                    $(".flag-container #flagform select[name='status']").val(rcrd.completed);
                    $(".flag-container #flagform textarea[name='flag_note']").val(rcrd.flag_note);
                    $(".flag-container #flagform input[name='id']").val(id);
                    $(".flag-container #flagform .saveflag").text("Update");
                    $(".flag-container #flagform .clearFormReset6").val("Reset");
                    $(".flag-container #flagform .clearFormReset6").addClass("FormReset");
                    $(".flag-container #flagform .FormReset").removeClass("clearFormReset6");
                    $(".flag-container #flagform").show();
                }else{
                    toastr.success(response.message);
                    $('#flaglistingtable').trigger( 'reloadGrid' );
                    if (fieldType != "deleted_at"){
                        onTop(true,'flaglistingtable');
                    }
                }

            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}



function getFlagInfo(tenantId){
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getFlagInfo',
        data: {
            class: "TenantAjax",
            action: "getFlagInfo",
            id: tenantId
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var currentDate = $.datepicker.formatDate(jsDateFomat, new Date());
                $('.flag-container #flagform input[name="date"]').val(currentDate);
                //$(".flag-container #flagform input[name='flagged_by_name']").val(data.data.propid.fullname);
                if (data.data.country.length > 0){
                    var countryOption = "<option value='0'>Select</option>";
                    $.each(data.data.country, function (key, value) {
                        if (value.id == '220'){
                            countryOption += "<option value='"+value.id+"' data-id='"+value.code+"' selected>"+value.name+" ("+value.code+")"+"</option>";
                        }else{
                            countryOption += "<option value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")"+"</option>";
                        }
                    });
                    $(".flag-container #flagform select[name='country_code']").html(countryOption);
                }
                var contactNumber = "";
                if (data.data.propid.phone_number != ""){
                    contactNumber = data.data.propid.phone_number;
                }else if(data.data.propid.mobile_number != ""){
                    contactNumber = data.data.propid.mobile_number;
                }else{
                    contactNumber = "";
                }
               // $(".flag-container #flagform input[name='phone_number']").val(contactNumber);

            } else if (data.status == "error") {

            } else {

            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
function getParameterByName( name ){
    var regexS = "[\\?&]"+name+"=([^&#]*)",
        regex = new RegExp( regexS ),
        results = regex.exec( window.location.search );
    if( results == null ){
        return "";
    } else{
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}
function saveFlagInfo() {
    var id =getParameterByName('id');
    var form = $(".flag-container #flagform").serializeArray();
    $.ajax({
        type: 'post',
        url: '/flag-ajax',
        data: {form: form,class:'Flag',action:'create_flag',object_id:id,object_type:'waiting_list'},
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                $(".flag-container #flagform .grey-btn").trigger("click");
                toastr.success(data.message);
                $('#flaglistingtable').trigger('reloadGrid');
                onTop(true,'flaglistingtable');
                setTimeout(function() {
                    jQuery('#flaglistingtable').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                }, 300);
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

}

function executeHtml(){

}
var idd = getParameterByName('id');
function getParameterByName(name) {
    var regexS = "[\\?&]" + name + "=([^&#]*)",
        regex = new RegExp(regexS),
        results = regex.exec(window.location.search);
    if (results == null) {
        return "";
    } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}


function jqGridFlags(status) {
    var table = 'flags';
    var columns = ['Date','Flag Name', 'Phone Number', 'Flag Reason','Completed','Note','Action'];
    var select_column = ['Edit','Delete','Completed'];
    //var joins = [{table:'users',column:'id',primary:'user_id',on_table:'tenant_property'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_details'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_lease_details'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_phone'},{table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},{table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'object_type',value:'waiting_list',condition:'='},{column:'object_id',value:idd,condition:'='}];
    var pagination=[];
    var columns_options = [
        {name:'Date',index:'date', width:180,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Flag Name',index:'flag_name', width:180,searchoptions: {sopt: conditions},table:table},
        {name:'Phone Number',index:'flag_phone_number', width:180, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Flag Reason',index:'flag_reason', width:180, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Completed',index:'completed', width:180, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
        {name:'Note',index:'flag_note', width:180, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'', title: false, width:180,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table},

    ];
    var ignore_array = [];
    jQuery("#flaglistingtable").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "flags",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:true,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Flags",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}
function statusFormatter (cellValue, options, rowObject){
    console.log("hereiam");
    if (cellValue == 0)
        return "False";
    else if(cellValue == '1')
        return "True";
    else
        return 'No';

}

function dateFormatter (cellValue, options, rowObject){
    if (cellValue == 0)
        return "No";
    else if(cellValue == '1')
        return "Yes";
    else
        return 'No';
}
$(document).on('click','.FormReset',function () {
    var id = $(".flag-container #flagform input[name='record_id']").val();
    flagChangeOption('getRecord',id);
});
