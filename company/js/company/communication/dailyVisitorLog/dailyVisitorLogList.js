$(document).ready(function () {
    $('.from123').Monthpicker();


$("#_easyui_textbox_input1").attr("placeholder", "Click here to Select a Visitor");

    //jqGrid status
    $(document).on('change','.common_ddl',function(){
        searchFilters();
    });

    $(document).on('change', '#timeSheetTable .select_options', function () {
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        switch (opt) {
            case "Edit":
                $('#addCaller').modal('show');
                $('#saveDailyVisitor').text('Update');
                $.ajax({
                    type: 'post',
                    url: '/dailyVisitorLog-Ajax',
                    data: {
                        class: 'DailyVisitorLogAjax',
                        action: 'getVisitorDetail',
                        id: id
                    },
                    success: function (response) {
                        var response = JSON.parse(response);
                        if (response.code == 200){
                            edit_date_time(response.data.updated_at);
                            $.each(response.data, function (key, value) {
                                console.log(key,value);
                                if(key == 'reason_for_visit'){
                                    $("#reasonsData").val(value);
                                } else if(key == 'id') {
                                    $("#editDailyVisitorId").val(value);
                                } else if(key == 'follow_up') {
                                    if(value == '1'){
                                        $('#followYes').prop('checked',true);
                                    } else {
                                        $('#followNo').prop('checked',true);
                                    }
                                }else if(key == 'note'){
                                    $("#daily_note").text(value);
                                } else {
                                    $("input[name=" + key + "]").val(value);
                                }
                            });
                        }
                    }
                });
                break;
            case "Print":
                $.ajax({
                    type: 'post',
                    url: '/dailyVisitorLog-Ajax',
                    data: {
                        class: "DailyVisitorLogAjax",
                        action: "getDailyVisitorLog",
                        id:id
                    },
                    success: function (response) {
                        var response = JSON.parse(response);
                        if (response.status == 'success' && response.code == 200) {
                            // update other modal elements here too
                            $('#phone_call_log_content').html(response.data);
                            // show modal
                            $('#PhoneCallLogModal').modal('show');
                        } else if (response.status == 'error' && response.code == 400) {
                            $('.error').html('');
                            $.each(response.data, function (key, value) {
                                $('.' + key).html(value);
                            });
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                break;
            case "Delete":
                bootbox.confirm("Are you sure you want to do this action?", function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/dailyVisitorLog-Ajax',
                            data: {
                                class: 'DailyVisitorLogAjax',
                                action: 'delete',
                                id:id
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.code == 200){
                                    toastr.success(response.message);
                                }
                            }
                        });
                    }
                    $('#timeSheetTable').trigger('reloadGrid');
                });

                break;
            default:
                $('#timeSheetTable .select_options').prop('selectedIndex',0);
        }
        $('#timeSheetTable').trigger('reloadGrid');
        $('#timeSheetTable .select_options').prop('selectedIndex',0);
    });


    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('all');
    function jqGrid(status) {
        var table = 'daily_visitor_log';
        var columns = ['Visitor Name','Phone Number','Date', 'Reason for Visits', 'Length of Visit', 'Time In','Time Out','Action Taken', 'Follow-Up Needed', 'Action'];
        var select_column = ['Edit', 'Delete', 'Print'];
        var joins = [{table:'daily_visitor_log',column:'reason_for_visit',primary:'id',on_table:'reasons'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [];
        var columns_options = [
            {name: 'Visitor Name', title:true, index: 'visitor', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Phone Number', title:true, index: 'phone', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table},
            {name: 'Date', index: 'date',title:true, width: 80,searchoptions: {sopt: conditions}, table: table,change_type:'date'}, /**cellattr:cellAttrdata**/
            {name: 'Reason for Visits', index: 'reason', width: 80, align: "left", searchoptions: {sopt: conditions}, table: 'reasons'},
            {name: 'Length of Visit', index: 'length_of_visit', width: 80, align: "left", searchoptions: {sopt: conditions}, table: table,formatter: listOfHours},
            {name: 'Time In', index: 'time_in', width: 80, align: "left", searchoptions: {sopt: conditions}, table: table,change_type:'time'},
            {name: 'Time Out', index: 'time_out', width: 100, align: "left", searchoptions: {sopt: conditions}, table: table,change_type:'time'},
            {name: 'Action Taken', index: 'action_taken', width: 80, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Follow-Up Needed', index: 'follow_up', width: 80, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:followUpNeeded},
            {name: 'Action', index: 'select', title: false, width: 80, align: "right",formatter: 'select', sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, edittype: 'select', search: false, table: table}
        ];
        var ignore_array = [];
        jQuery("#timeSheetTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            width: '100%',
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'daily_visitor_log.updated_at',
            sortorder: 'desc',
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Daily Visitors",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
        );
    }

    var base_url = window.location.origin;
    /**
     * export function
     */
    $(document).on("click", '#export_property_button', function() {
        var table = 'daily_visitor_log';
        window.location.href = base_url + "/dailyVisitorLog-Ajax?table=" + table + "&&action=exportExcel";
    });

    $(document).on('click','.genratePdf',function(){
        getHtmlPdfConverter();
    });

    function listOfHours(cellValue, options, rowObject) {
        if(rowObject !== undefined){
            var dataRow = rowObject;
            if(dataRow['Length of Visit'] == ''){
                return cellValue;
            } else {
                return cellValue+' Hours';
            }
        }
    }

    function followUpNeeded(cellValue, options, rowObject) {
        if(rowObject !== undefined){
            var dataRow = rowObject;
            if(dataRow['Follow-Up Needed'] == '1'){
                return 'Yes';
            } else {
                return 'No';
            }
        }
    }

    function getHtmlPdfConverter(){
        $.ajax({
            url:'/dailyVisitorLog-Ajax',
            type: 'POST',
            data: {
                class: "DailyVisitorLogAjax",
                action: "createDailyLogPdf" 
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log(response);
                if (response.status == 'success' && response.code == 200) {
                    var link=document.createElement('a');
                    document.body.appendChild(link);
                    link.target="_blank";
                    link.download="DailyVisitor.pdf";
                    link.href=response.data;
                    link.click();
                } else if(response.code == 500) {
                    //toastr.warning(response.message);
                } else {
                    //toastr.error(response.message);
                }

            }
        });
    }
});

$(document).on('click','.cancel_phone_call_log',function(){
    bootbox.confirm("Are you sure you want to do this action?", function (result) {
        if (result == true) {
            $('#addCaller').modal('hide');
            $('#editDailyVisitorId').val('');
        }
    });
});

