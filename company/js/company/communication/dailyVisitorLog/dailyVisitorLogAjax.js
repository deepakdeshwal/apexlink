getReasons();
var timeSring = '1';
$(document).on('click','#_easyui_textbox_input1',function(){
    $('.selectComboGrid').combogrid('showPanel');
})

$('.selectComboGrid').combogrid({
    placeholder: "Select...",
    panelWidth: 500,
    url: '/dailyVisitorLog-Ajax',
    idField: 'name',
    textField: 'name',
    mode: 'remote',
    fitColumns: true,
    queryParams: {
        action: 'getUsersData',
        class: 'DailyVisitorLogAjax'
    },
    columns: [[
        {field: 'id', hidden: true},
        {field: 'user_type', title: 'User Role', width: 60},
        {field: 'name', title: 'Visitor Name', width: 120},
        {field: 'address1', title: 'Address', width: 120},
        {field: 'email', title: 'Email', width: 120},
    ]],
    onSelect: function (index, row) {
        console.log('row',row,index);
        $('#visitor').val(row.name);
        $('#date').val(row.date);
        $('#company').val(row.company_name);
        $('#visit_phone_number').val(row.phone_number);
    }
});

/**
 * function to get the list of employees
 */
function getReasons(id){
    $.ajax({
        type: 'post',
        url: '/dailyVisitorLog-Ajax',
        data: {
            class: 'DailyVisitorLogAjax',
            action: 'getReasons'
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200){
                var html = '';
                $.each(response.data, function (key, value) {
                    if(id == value.id){
                        html += '<option value="' + value.id + '" selected>' + value.reason + '</option>';
                    } else {
                        html += '<option value="' + value.id + '">' + value.reason + '</option>';
                    }
                });
                $('#reasonsData').html(html);
            }
        }
    });
}
$(document).on('click','.addVisitor',function(){
    var dvalue = $('#_easyui_textbox_input1').val();
    if(dvalue == ''){
        $('#visitor').val('');
        $('#company').val('');
        $('.phone_number_format').val('');
    }else{
        $('#visitor').val(dvalue);
    }


    $('#daily_note').val('');
    // $('#_easyui_textbox_input1').val('');
    $('.textbox-value').val('');
    $('#saveDailyVisitor').text('Save');
    $('#editDailyVisitorId').val('');
});

/**
 * To show pop up reason div
 */
$(document).on('click','.addReasonsDiv',function(){
    $('#addReasonsDiv').show();
    $('#new_reasons').val('');
});
/**
 * To hide pop up reason div
 */
$(document).on('click','.cancelAddReasons',function(){
    $("#AddVisitorsForm")[0].reset();
    $('#addReasonsDiv').hide();
    $('#editTimeSheetId').val('');
});



$("#date").datepicker({dateFormat: datepicker, changeMonth: true, changeYear: true,minDate: new Date(),});
$('#time_in').timepicker({
    timeFormat: 'h:mm p',
    interval: 5,
    minTime: '8',
    maxTime: '08:00pm',
    defaultTime: '08',
    startTime: '08:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true,
    change:getTimeDiffrence
});
$('#time_out').timepicker({
    timeFormat: 'h:mm p',
    interval: 5,
    minTime: '10',
    maxTime: '09:55pm',
    defaultTime: '10',
    startTime: '10:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true,
    change:getTimeDiffrence
});

function getTimeDiffrence(val){
    if(timeSring != '1') {
        checkTimeValidation();
    }
    setTimeout(function(){ timeSring = '2' }, 1000);
}

function checkTimeValidation(){
    $.ajax({
        type: 'post',
        url: '/dailyVisitorLog-Ajax',
        data: {
            class:"DailyVisitorLogAjax",
            action:'calculateTimeDiff',
            time_in: $('#time_in').val(),
            time_out: $('#time_out').val()
        },
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status == 'success' && response.code == 200){
                var time = parseFloat(response.data.replace(":", "."));
                $('#total_hours').val(time);
            } else if(response.code == 400){
                toastr.warning(response.data);
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}



$('#AddVisitorsForm').on('submit',function(event){
    event.preventDefault();

    if($('#AddVisitorsForm').valid()){
        $('#loadingmessage').show();
        var formData = $('#AddVisitorsForm').serializeArray();
        $.ajax({
            type: 'post',
            url: '/dailyVisitorLog-Ajax',
            data: {
                class:"DailyVisitorLogAjax",
                action:'saveVisitorLog',
                form: formData
            },
            success: function (response) {
                var response = JSON.parse(response);
                $('#loadingmessage').hide();
                if(response.status == 'success' && response.code == 200){
                    toastr.success(response.message);

                    $('#addCaller').modal('hide');
                    $('#timeSheetTable').trigger('reloadGrid');
                    setTimeout(function(){
                        jQuery('#timeSheetTable').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('#timeSheetTable').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                    }, 1000);
                } else if(response.status == 'error' && response.code == 500){
                    toastr.warning(response.message);
                } else if(response.status == 'error' && response.code == 400){
                    $.each(response.data, function (key, value) {
                        if(key == 'emailErr'){
                            toastr.error(value);
                        } else {
                            $('#' + key).text(value);
                        }
                    });
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
});

$(document).on('click', '#addReasonsPopUpSave', function (e) {
    e.preventDefault();
    var reasons = $('#new_reasons').val();
    $.ajax({
        type: 'post',
        url: '/dailyVisitorLog-Ajax',
        data: {
            class: 'DailyVisitorLogAjax',
            action: 'saveReasonsData',
            reasons: reasons },
        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidateReasons").each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                toastr.success(response.message);
                getReasons(response.inserted_id);
                $('#addReasonsDiv').hide();
            }
            if (response.message == 'Reason already exists!')
                toastr.warning(response.message);
            return false;
        }
    });
});

/**
 * To change the format of phone number
 */
// $(document).on('keydown', '.phone_number_format', function (e) {
//     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which !=9) {
//         return false;
//     }
//     $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
// });

$(document).on("click",".preview_button",function(){
    $.ajax({
        type: 'post',
        url: '/dailyVisitorLog-Ajax',
        data: {
            class: "DailyVisitorLogAjax",
            action: "getDailyVisitorLog"
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                // update other modal elements here too
                $('#phone_call_log_content').html(response.data);
                // show modal
                $('#PhoneCallLogModal').modal('show');
            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
});

/*
Function to print envelope
*/
function PrintElem(elem)
{
    Popup($(elem).html());
}

function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
        'font-weight:bold;} .right-detail{\n' +
        '        position:relative;\n' +
        '        left:+400px;\n' +
        '    }</style>');
    $(mywindow.document.body).html( '<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if(mywindow.close()){

    }
    $("#PrintEnvelope").modal('hide');
    return true;
}

/**
 * function to select search filter
 */
$(document).on('click','.filter_class',function(){
    var checked_value = $('input[name=filter_search]:checked').val()
    if(checked_value == '0'){
        $('#ByWeek').show();
        $('#ByDate').hide();
    } else if(checked_value == '1') {
        $('#ByDate').show();
        $('#ByWeek').hide();
    }
});

$(document).on('click','.callLogSearch',function(){
    searchFilters();
});

$(document).on('click','#ByWeekFilter',function(){
    if($('#ByWeekFilter option:selected').val() == 'Monthly'){
        $('#ByMonth').show();
    } else {
        $('#ByMonth').hide();
    }
});

function searchFilters(){
    var grid = $("#timeSheetTable"),f = [];
    var type = $('#_easyui_textbox_input1').val() != ''?$('#_easyui_textbox_input1').val():'all';
    var checked_value = $('input[name=filter_search]:checked').val()
    if(checked_value == '0'){
        var data = $('#ByWeekFilter option:selected').val();
        if(data == 'Monthly'){
            var monthly = $('.monthpicker_input').text() != 'Select Month'? $('.monthpicker_input').text() :'all';
            f.push({field: "daily_visitor_log.visitor", op: "cn", data: type},{field: "daily_visitor_log.date", op: "dateByString", data: monthly});
        } else {
            f.push({field: "daily_visitor_log.visitor", op: "cn", data: type},{field: "daily_visitor_log.date", op: "dateByString", data: data});
        }
    } else if(checked_value == '1') {
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        f.push({field: "daily_visitor_log.visitor", op: "cn", data: type},{field: "daily_visitor_log.date", op: "dateBetween", data: start_date, data2:end_date});
    }
    grid[0].p.search = true;
    $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}

$('#start_date').datepicker({
    yearRange: '-0:+100',
    changeMonth: true,
    changeYear: true,
    dateFormat: datepicker,
    maxDate:new Date()
}).datepicker("setDate", new Date());

$('#end_date').datepicker({
    yearRange: '-0:+100',
    changeMonth: true,
    changeYear: true,
    dateFormat: datepicker,
    maxDate:new Date()
}).datepicker("setDate", new Date());

$('#month_date').datepicker( {
    changeMonth: true,
    changeYear: true,
    showButtonPanel: true,
    dateFormat: 'MM yy',
    maxDate:new Date()
}).datepicker("setDate", new Date());




