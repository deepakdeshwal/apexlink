jqGrid('All');
function jqGrid(status) {

    var table = 'communication_email';
    var columns = ['Name','To', 'User', 'Subject', 'Date', 'Actions'];
    var select_column = ['Delete','Open'];
    var joins = [{table:'communication_email',column:'email_to',primary:'email',on_table:'users'}];
    //var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = ['communication_email.updated_at'];
    var extra_where = [{column: 'type', value: 'E', condition: '='}];
    var status=2;
    var columns_options = [
        {name:'Name',index:'name', width:90,align:"center",searchoptions: {sopt: conditions},table:'users', classes: 'pointer'},
        {name:'To',index:'email_to', align:"center", width:100,searchoptions: {sopt: conditions},table:table, classes: 'pointer'},
        {name:'User',index:'user_type', width:80, align:"center",searchoptions: {sopt: conditions},table:'users',classes: 'pointer',formatter: userType},
        {name:'Subject',index:'email_subject', width:80, align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer'},
        {name:'Date',index:'created_at', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table}
    ];
    var ignore_array = [];
    jQuery("#communication-draft-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where,
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Drafts",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false}
    );
}
function userType(cellValue, options, rowObject) {
    if(rowObject !== undefined) {
        var user_type = rowObject.User;
        var user;
        switch (user_type) {
            case '2':
                user = "Tenant";
                break;
            case '4':
                user = "Owner";
                break;
            case '3':
                user = "Vendor";
                break;
            case '5':
                user = "Other Contacts";
                break;
            case '6':
                user = "Guest Card";
                break;
            case '7':
                user = "Rental Application";
            case '8':
                user = "Employee";
                break;
            default:
                user = "";
        }
        return user;
    }

}


jQuery(document).on('change','.select_options',function () {
    var select_options = $(this).val();
    var data_id = $(this).attr('data_id');
    if(select_options == 'Delete') {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        url: "/Communication/ComposeMailAjax",
                        method: 'post',
                        data: {
                            class: "PropertyUnitAjax",
                            action: "delete",
                            'id': data_id,
                        },
                        success : function(response){
                            var response =  JSON.parse(response);
                            if(response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                $('#communication-draft-table').trigger('reloadGrid');
                            } else if(response.status == 'error' && response.code == 503) {
                                toastr.error(response.message);
                            }else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                }
            }
        });
    } else if(select_options == 'Open')
    {
        localStorage.setItem('composer_mail_id',data_id);
        localStorage.setItem('composer_mail_type','draft');
        window.location.href='/Communication/ComposeEmail'
    }
    $('.select_options').prop('selectedIndex',0);
});