var table_green_id  = localStorage.getItem('table_green_id');
var table_green_tableid  = localStorage.getItem('table_green_tableid');
localStorage.removeItem('table_green_id');
localStorage.removeItem('table_green_tableid');
$('.to,.cc,.bcc').tagsinput({
    // typeahead: {
    //     source: function(query,process) {
    //         var query = {
    //             "search": query,
    //             "action": 'getUsersSearch',
    //             "class": 'getUsersSearch'
    //         };
    //         // return $.get('/Communication/ComposeMailAjax',query, function (data) {
    //         //     console.log(data);
    //         //     data = $.parseJSON(data);
    //         //     return process(data);
    //         // })
    //
    //         $.ajax({
    //             url:'/Communication/ComposeMailAjax',
    //             type: 'POST',
    //             data: query,
    //             async:false,
    //             success: function (response) {
    //                 return response;
    //             }
    //         });
    //     }
    //
    // },
    freeInput: true
});
function process(data)
{
    console.log(data)
    return data;
}
$('.to,.cc,.bcc').on('itemAdded', function(event) {
    setTimeout(function(){
        $(">input[type=text]",".bootstrap-tagsinput").val("");
    }, 1);
});

$(".popup_to,.popup_cc,.popup_bcc").on('itemRemoved', function(event) {
    autoRemoveEmailCheck(event.item);
});

$('.to').on('itemRemoved', function(event) {
    setTimeout(function () {
        var to = $('.to').val();
        $(".popup_to").tagsinput('remove', event.item);
    },200);
});

$('.cc').on('itemRemoved', function(event) {
    setTimeout(function () {
        var to = $('.cc').val();
        $(".popup_cc").tagsinput('remove', event.item);
    },200);
});

$('.bcc').on('itemRemoved', function(event) {
    setTimeout(function () {
        var to = $('.bcc').val();
        $(".popup_bcc").tagsinput('remove', event.item);
    },200);
});

$('.popup_to').tagsinput();
$('.popup_cc').tagsinput();
$('.popup_bcc').tagsinput();
$('.bootstrap-tagsinput input').removeAttr('style');
$('.bootstrap-tagsinput').css('width','100%');
$('input.form-control.subject').css('margin-bottom','10px');
getView();

setTimeout(function () {
    $('.to_field .bootstrap-tagsinput input').focus();
},500);

function getView()
{
    var compose_mail_id = localStorage.getItem("composer_mail_id");
    var composer_mail_type = localStorage.getItem("composer_mail_type");

    $.ajax({
        url:'/Communication/ComposeMailAjax',
        method: 'post',
        data: {
            class: "PropertyUnitAjax",
            action: "getComposeForView",
            'id': compose_mail_id,
        },
        success: function (data) {
            info =  JSON.parse(data);
            if(info.status=="success"){
                var res = info.data;
                if(composer_mail_type == 'draft')
                {
                    if(res.email_to) {
                        $('.to').tagsinput('add', res.email_to);
                        $('.to').tagsinput('add', res.email_to);
                    }

                    if(res.email_cc) {
                        $('.cc').tagsinput('add', res.email_cc);
                        $('.cc').tagsinput('add', res.email_cc);
                    }

                    if(res.email_bcc) {
                        $('.bcc').tagsinput('add', res.email_bcc);
                        $('.bcc').tagsinput('add', res.email_bcc);
                    }
                }


                $('.subject').val(res.email_subject);
                $('.summernote').summernote('code', res.email_message);
                $('#compose_mail_id').val(res.id);
            }
        },

    });
}

$(document).ready(function () {
    $(".note-editable p").css('line-height','8px !important');
    $(".note-editable").css('line-height','8px !important');
  /*  $(document).on("keyup",".note-editable",function () {
    $(".note-editable").removeClass("capsOn");
    });*/
    $(".slide-toggle").click(function () {
        $(".box").animate({
            width: "toggle"
        });
    });
});

$(document).ready(function () {
    function capitalize(s) {
        return s[0].toUpperCase() + s.slice(1);
    }
    setTimeout(function () {
        $(document).on("keypress",".onlyFirstCaps", function(){
            var value = $(this).text();
            if(value.length > 0){
                capitalize(value);
                //$(this).removeClass("capsOn");
                // $(this).text(value);
            }else{
                capitalize(value);
            }
        });

    },1000);
    $(".slide-toggle2").click(function () {
        $(".box2").animate({
            width: "toggle"
        });
    });
});

//Summernote Js Starts
var ii = 0;
$(document).ready(function(){
    $('.summernote').summernote({
        addclass: {
            debug: false,
            classTags: [{title:"Button","value":"btn btn-success"},"jumbotron", "lead","img-rounded","img-circle", "img-responsive","btn", "btn btn-success","btn btn-danger","text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
        },
        /**callbacks: { keypress: function(e) {
            if(e.keyCode == 8){
                var getCharacters = $('#summernote').summernote('code');
				console.log('getCharacters>>>',charCount);
				var charCount = getCharacters.length;
				console.log('charCount>>>',charCount);
				if((charCount==8) || (charCount==1)){
					$('.note-editable').addClass('caps');
				}
				var rx = /INPUT|SELECT|TEXTAREA/i;
				if( e.which == 8 ){ // 8 == backspace
				if(!rx.test(e.target.tagName) || e.target.disabled || e.target.readOnly ){
					$('.note-editable p').text(function (_,getCharacters) {
						return getCharacters.slice(0, -1);
					});
						e.preventDefault();
					}
				}
            }

        }},*/
        width: '100%',
        height: '300px',
        //margin-left: '15px',
        toolbar: [
            // [groupName, [list of button]]
            ['img', ['picture']],
            ['style', ['style', 'addclass', 'clear']],
            ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
            ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
            ['extra', ['video', 'table', 'height']],
            ['misc', ['undo', 'redo', 'codeview', 'help']]
        ]
    });
  //  $('#summernote').summernote({ callbacks: { onKeyup: function(e) { $('#summernote').summernote('formatPara'); } } });
});
//Summernote JS Ends


$("#sendEmail").validate({
    ignore: ".mesgbody",
    rules: {
        to: {
            required:true
        },
        subject: {
            required:true
        },
        mesgbody: {
            required:true
        }

    },
    submitHandler: function (e) {
        var composer_mail_type = localStorage.getItem("composer_mail_type");
        if(composer_mail_type == "forward"){
            $("#compose_mail_id").val("");
        }
        console.log('dd',$("#compose_mail_id").val(),composer_mail_type);
        var tenant_id = $(".tenant_id").val();
        var form = $('#sendEmail')[0];
        var formData = new FormData(form);
        var to = $(".to").val();
        formData.append('to_users',to);
        formData.append('action','saveEmail');
        formData.append('class','saveEmail');
        $.ajax({
            url:'/Communication/ComposeMailAjax',
            type: 'POST',
            data: formData,
            success: function (data) {
                info =  JSON.parse(data);
                if(info.status=="success"){

                    var mail_type = $('.mail_type').val();
                    localStorage.removeItem('composer_mail_id');
                    localStorage.removeItem('composer_mail_type');
                    localStorage.removeItem('predefined_text');
                    localStorage.removeItem('predefined_mail');
                    if(mail_type == 'draft')
                    {
                        toastr.success("You Email saved successfully");
                        setTimeout(function () {
                            window.location.href='/Communication/DraftedMails';
                        },1000);
                    } else {
                        toastr.success(" The Email was sent successfully");
                        setTimeout(function () {
                            window.location.href='/Communication/SentEmails';
                        },1000);
                    }
                    localStorage.setItem("Message", 'Record added successfully.');
                    localStorage.setItem('rowcolor', 'rowColor');
                } else{
                    toastr.warning(info.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});

$(document).on("click",".compose-email-btn",function(){
    $('.mail_type').val('send');
    $('#sendEmail').trigger('submit');
    $('label#mesgbody-error').remove().insertAfter('.note-editor.note-frame.panel');
    $('label#to-error').remove().insertAfter('.to_field .bootstrap-tagsinput');

});

$(document).on("click",".compose-email-save-btn",function(){
    $('.mail_type').val('draft');
    $("#compose_mail_id").val("");
    $('#sendEmail').trigger('submit');
    $('label#mesgbody-error').remove().insertAfter('.note-editor.note-frame.panel');
    $('label#to-error').remove().insertAfter('.to_field .bootstrap-tagsinput');

});

$(document).on("click",".addToRecepent",function(){
    $("#tousers").trigger('reset');
    $('#torecepents').modal('show');
});

// getUsers(2);
$(document).on("change",".selectUsers",function(){
    var type = $(this).val();

    getUsers(type);
    autoSelectEmailCheck('popup_to');

    if(type == 'Select'){
        $('.popup_values').hide();
    }else{
        $('.popup_values').show();
    }
});

function autoSelectEmailCheck(fieldClassName,) {
    var fieldClassName = '.'+fieldClassName;
    var popup_to = $(fieldClassName).val();
    popup_to = popup_to.split(',');
    if(popup_to)
    {
        $.each(popup_to,function (key,value) {
            setTimeout(function () {
                $("input[data-email='"+value+"']").prop( "checked", true );
            },200)
        });
    }
}

function autoRemoveEmailCheck(fieldClassName,) {
    $("input[data-email='"+fieldClassName+"']").prop( "checked", false );
}

function getUsers(type)
{
    $.ajax({
        url:'/Communication/ComposeMailAjax',
        type: 'POST',
        data: {
            "type": type,
            "action": 'getUsers',
            "class": 'EditOwnerAjax'
        },
        success: function (response) {
            console.log("response >>",response);
            /*debugger*/
            if(!response || response == 'false')
            {

                $(".userBccDetails").html(' <table class="table" border="1px"><tbody><tr>\n' +
                    '                                            <th>id</th>\n' +
                    '                                            <th>Name</th>\n' +
                    '                                            <th>Email</th>\n' +
                    '                                        </tr><tr><td colspan="3" align="center"  bgcolor="#f7f7f7">\n' +
                    '                                                No Record Found\n' +
                    '                                            </td></tr></tbody></table>');
            }else{
                $(".userDetails").html(response);
            }
        }
    });
}

$(document).on("click",".getEmails",function(){
    if(this.checked)
    {
        var email = $(this).attr('data-email');
        $('.popup_to').tagsinput('add', email);
    }
    else
    {
        var email = $(this).attr('data-email');
        $('.popup_to').tagsinput('remove', email);
    }
});

$(document).on("click","#SendselectToUsers",function(){
    var check_data = [];
    $('.getEmails:checked').each(function () {
        $('.popup_to').tagsinput('add', $(this).attr('data-email'));
    });
    var hidden_to = $('.popup_to').val();

    if(!hidden_to)
    {
        toastr.warning('Please select atleast one recipient');
        return false;
    }

    var popup_to = $('.popup_to').val();
    $('.to').tagsinput('add','');
    $('.to').tagsinput('add',hidden_to);
    $('#torecepents').modal('hide');

    // $('.getEmails').prop('checked', false);
});

$(document).on('click','.delete_pro_img',function(){
    $(this).parent().parent().parent('.row').remove();
});

$(document).on("click",".addCcRecepent",function(){
    $("#ccusers").trigger('reset');
    $('#ccrecepents').modal('show');
});
// getCCUsers(2);
$(document).on("change",".selectCcUsers",function(){
    var type = $(this).val();
    getCCUsers(type);
    autoSelectEmailCheck('popup_cc');
    if(type == 'Select'){
        $('.popup_values').hide();
    }else{
        $('.popup_values').show();
    }
});

function getCCUsers(type)
{
    $.ajax({
        url:'/Communication/ComposeMailAjax',
        type: 'POST',
        data: {
            "type": type,
            "action": 'getCCUsers',
            "class": 'EditOwnerAjax'
        },
        success: function (response) {

            if(response.status == 'failed')
            {

                $(".userBccDetails").html(' <table class="table" border="1px"><tbody><tr>\n' +
                    '                                            <th>id</th>\n' +
                    '                                            <th>Name</th>\n' +
                    '                                            <th>Email</th>\n' +
                    '                                        </tr><tr><td colspan="3" align="center"  bgcolor="#f7f7f7">\n' +
                    '                                                No Record Found\n' +
                    '                                            </td></tr></tbody></table>');
            }else{
                $(".userCcDetails").html(response);
            }
        }
    });
}

$(document).on("click","#SendselectCcUsers",function(){
    var check_data = [];
    $('.getCCEmails:checked').each(function () {
        $('.popup_cc').tagsinput('add', $(this).attr('data-email'));
    });
    var hidden_cc = $('.popup_cc').val();
    if(!hidden_cc)
    {
        toastr.warning('Please select atleast one recipient');
        return false;
    }
    $('.cc').tagsinput('add','');

    $('.cc').tagsinput('add',hidden_cc);
    $('#ccrecepents').modal('hide');
});


$(document).on("click",".getCCEmails",function(){
    if(this.checked)
    {
        var email = $(this).attr('data-email');
        $('.popup_cc').tagsinput('add', email);
    }
    else
    {
        var email = $(this).attr('data-email');
        $('.popup_cc').tagsinput('remove', email);
    }


});

$(document).on("click",".addBccRecepent",function(){
    $('#bccrecepents').modal('show');

});
// getBccUsers(2);
$(document).on("change",".selectBccUsers",function(){
    var type = $(this).val();
    getBccUsers(type);
    autoSelectEmailCheck('popup_bcc');
    if(type == 'Select'){
        $('.popup_values').hide();
    }else{
        $('.popup_values').show();
    }

});
function getBccUsers(type)
{
    $.ajax({
        url:'/Communication/ComposeMailAjax',
        type: 'POST',
        data: {
            "type": type,
            "action": 'getBCCUsers',
            "class": 'EditOwnerAjax'
        },
        success: function (response) {
            if(response.status == 'failed')
            {

                $(".userBccDetails").html(' <table class="table" border="1px"><tbody><tr>\n' +
                    '                                            <th>id</th>\n' +
                    '                                            <th>Name</th>\n' +
                    '                                            <th>Email</th>\n' +
                    '                                        </tr><tr><td colspan="3" align="center"  bgcolor="#f7f7f7">\n' +
                    '                                                No Record Found\n' +
                    '                                            </td></tr></tbody></table>');
            }else{
                $(".userBccDetails").html(response);
            }

        }
    });
}

$(document).on("click",".getBCCEmails",function(){
    if(this.checked)
    {
        var email = $(this).attr('data-email');
        $('.popup_bcc').tagsinput('add', email);
    }
    else
    {
        var email = $(this).attr('data-email');
        $('.popup_bcc').tagsinput('remove', email);
    }
});
$(document).on("click","#SendselectBccUsers",function(){
    var check_data = [];
    $('.getBCCEmails:checked').each(function () {
        $('.popup_bcc').tagsinput('add', $(this).attr('data-email'));
    });
    var hidden_bcc = $('.popup_bcc').val();
    //  var getEmails = $('.getBCCEmails:checked').length ;
    if(!hidden_bcc)
    {
        toastr.warning('Please select atleast one recipient');
        return false;
    }
    $('.bcc').tagsinput('add','');

    $('.bcc').tagsinput('add',hidden_bcc);
    $('#bccrecepents').modal('hide');
});

$(document).on('input','#toSearch,#bccSearch,#ccSearch',function () {
    var search = $(this).val();
    var search_len = search.length;
    var user_type = '';
    var id_type = $(this).prop('id');
    if(id_type == 'bccSearch')
    {
        user_type = $('.selectBccUsers').val();
    } else if(id_type == 'ccSearch')
    {
        user_type = $('.selectCcUsers').val();
    } else if(id_type == 'toSearch')
    {
        user_type = $('.selectUsers').val();
    }

    if(search_len >= 1)
    {
        $.ajax({
            url:'/Communication/ComposeMailAjax',
            type: 'POST',
            data: {
                "search": search,
                "id_type": id_type,
                "user_type": user_type,
                "action": 'getSearchUsers',
                "class": 'EditOwnerAjax'
            },
            success: function (response) {
                if(response)
                {
                    if(id_type == 'bccSearch')
                    {
                        $(".userBccDetails").html(response);
                    } else if(id_type == 'ccSearch')
                    {
                        $(".userCcDetails").html(response);
                    } else if(id_type == 'toSearch')
                    {
                        $(".userDetails").html(response);
                    }
                }

            }
        });
    }
});
$(document).on('click','#cancel_email',function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            var last_url = localStorage.getItem('table_green_url');
            if(last_url)
            {
                localStorage.removeItem('table_green_url');
                localStorage.setItem('cancelRecord',true);
                localStorage.setItem('rowcolor',true);
                localStorage.setItem('table_green_tableid',table_green_tableid);
                updateUserTable(table_green_id);
                debugger
                window.location.href = last_url;
            }else {
                window.location.href = '/Communication/SentEmails';
            }

        }
    });
});

setTimeout(function () {
    var predefined_email = localStorage.getItem('predefined_mail');
    if(predefined_email)
    {
        $('input.form-control.to').removeAttr('autofocus');
        $('input.form-control.subject').attr('autofocus',true);
        $('input.form-control.subject').focus();
        $('.to').tagsinput('add', predefined_email);
        localStorage.removeItem('composer_mail_id');
        localStorage.removeItem('composer_mail_type');
        localStorage.removeItem('predefined_text');
        localStorage.removeItem('predefined_mail');
    }
},500);

function updateUserTable(userId) {
    $.ajax({
        url:'/Communication/ComposeMailAjax',
        type: 'POST',
        data: {
            "userId": userId,
            "action": 'updateUserTable',
            "class": 'updateUserTable'
        },
        success: function (response) {
            response =JSON.parse(response);
        }
    });
}