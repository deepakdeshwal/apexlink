
function convertSerializeDatatoArray(){
    var newData = [];
    $(".fileLibraryInput").each(function( index ) {
        var name = $(this).val();
        var size = $(this).attr('data_id');
        newData.push({'name':name,'size':size});
    });
    return newData;
}

function compareArray(data,compare){
    for(var i =0;i < compare.length;i++){
        if(compare[i].name == data['name'] && compare[i].size == data['size']){
            return 'true';
        }
    }
    return 'false';
}

function isa_convert_bytes_to_specified(bytes, to) {
    var formulas =[];
    formulas['k']= (bytes / 1024).toFixed(1);
    formulas['M']= (bytes / 1048576).toFixed(1);
    formulas['G']= (bytes / 1073741824).toFixed(1);
    return formulas[to];
}

$(document).on('click','.delete_doc_icon',function(){
    var file_id = $(this).attr('data_id');
    console.log('delete_file_by_id',file_id);
    bootbox.confirm("Do you want to delete this record?", function (result) {
        if (result == true) {
            $.ajax({
                type: 'post',
                url: '/Announcement-Ajax',
                data: {
                    class: "announcementAjax",
                    action: "updateStatus",
                    id: file_id,
                    status_type : 'delete_file_by_id'
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        var res = response.data;
                        console.log('res>>', res);
                        toastr.success(response.message);
                        $('#announcementFileLibrary-table').trigger( 'reloadGrid' );
                        $('#announcementFileLibraryCopy-table').trigger( 'reloadGrid' );
                        $('#announcementFileLibraryPublish-table').trigger( 'reloadGrid' );
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }
    });
});

$(document).on('click','#add_libraray_file',function(){
    $('#addFileLibrary').val('');
    $('#addFileLibrary').trigger('click');
    // console.log('sasasa');
});
var edit_active =  localStorage.getItem("edit_active");
if(edit_active){

}
var fileLibrary = [];
var imgArray = [];
$(document).on('change','#addFileLibrary',function(){
   // fileLibrary = [];
    $.each(this.files, function (key, value) {
        var type = value['type'];
        console.log('++++++++++++++++++++',value);
        // return false;
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        if(size > 4097) {
            toastr.warning('Please select documents less than 4 mb!');
        } else {
            size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';

            if (type == 'application/msword' || type == 'image/jpeg' || type == 'image/jpg' || type == 'image/png' || type == 'image/gif' || type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                if($.inArray(value['name'], imgArray) === -1)
                {
                    fileLibrary.push(value);
                }
                var src = '';
                var reader = new FileReader();
                //  $('#file_library_uploads').html('');
                reader.onload = function (e) {
                    if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                        src = upload_url + 'company/images/excel.png';
                    } else if (type == 'application/pdf') {
                        src = upload_url + 'company/images/pdf.png';
                    } else if (type == 'application/msword' || type == 'doc' || type == 'docx' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                        src = upload_url + 'company/images/word_doc_icon.jpg';
                    } else if (type == 'text/plain') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else if (type == 'text/xml') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else {
                        src = e.target.result;
                    }
                    if($.inArray(value['name'], imgArray) === -1)
                    {
                        $("#add_file_library_uploads").append(
                            '<div class="row" style="margin:0 20px">' +
                            '<div class="col-sm-12 img-upload-library-div" style="margin: 0">' +
                            '<div class="col-sm-4"><img class="img-upload-tab' + key + '" width=40 height=40 src=' + src + '></div>' +
                            '<div style="margin-top: 10px;" class="col-sm-4 center_img show-library-list-imgs-name ' + key + '">' + value['name'] + '</div>' +
                            '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                            '<div  style="    margin-top: 10px;" class="col-sm-4 center_img show-library-list-imgs-size' + key + '">' + size + '</div>' +
                            '</div></div>');
                        imgArray.push(value['name']);
                    } else {
                        toastr.warning('File already exists!');
                    }
                };
                reader.readAsDataURL(value);
            } else {
                toastr.warning('Please select file with .jpg | .png | .jpeg | .gif | .xlsx | .pdf | .docx | .txt | .xml extension only!');
            }
        }
    });
});

$(document).on('click','#remove_library_file_add',function(){
    bootbox.confirm("Do you want to remove all files?", function (result) {
        if (result == true) {
            $('#add_file_library_uploads').html('');
            toastr.success('The record deleted successfully.');
            $('#addFileLibrary').val('');
            imgArray = [];
        }
    });
});

/** Edit File library**/
$(document).on('click','#edit_libraray_file',function(){
    $('#editFileLibrary').val('');
    $('#editFileLibrary').trigger('click');
    // console.log('sasasa');
});

$(document).on('change','#editFileLibrary',function(){
    // fileLibrary = [];
    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        if(size > 4097) {
            toastr.warning('Please select documents less than 4 mb!');
        } else {
            size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
            if (type == 'application/msword' || type == 'image/jpeg' || type == 'image/jpg' || type == 'image/png' || type == 'image/gif' || type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                if($.inArray(value['name'], imgArray) === -1)
                {
                    fileLibrary.push(value);
                }
                var src = '';
                var reader = new FileReader();
                //  $('#file_library_uploads').html('');
                reader.onload = function (e) {
                    if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                        src = upload_url + 'company/images/excel.png';
                    } else if (type == 'application/pdf') {
                        src = upload_url + 'company/images/pdf.png';
                    } else if (type == 'application/msword' || type == 'doc' || type == 'docx' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                        src = upload_url + 'company/images/word_doc_icon.jpg';
                    } else if (type == 'text/plain') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else if (type == 'text/xml') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else {
                        src = e.target.result;
                    }
                    if($.inArray(value['name'], imgArray) === -1)
                    {
                        $("#edit_file_library_uploads").append(
                            '<div class="row" style="margin:0 20px">' +
                            '<div class="col-sm-12 img-upload-library-div" style="margin: 0">' +
                            '<div class="col-sm-4"><img class="img-upload-tab' + key + '" width=40 height=40 src=' + src + '></div>' +
                            '<div style="margin-top: 10px;" class="col-sm-4 center_img show-library-list-imgs-name ' + key + '">' + value['name'] + '</div>' +
                            '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                            '<div  style="    margin-top: 10px;" class="col-sm-4 center_img show-library-list-imgs-size' + key + '">' + size + '</div>' +
                            '</div></div>');
                        imgArray.push(value['name']);
                    } else {
                        toastr.warning('File already exists!');
                    }
                };
                reader.readAsDataURL(value);
            } else {
                toastr.warning('Please select file with .jpg | .png | .jpeg | .gif | .xlsx | .pdf | .docx | .txt | .xml extension only!');
            }
        }
    });
});

$(document).on('click','#remove_library_file_edit',function(){
    bootbox.confirm("Do you want to remove all files?", function (result) {
        if (result == true) {
            $('#edit_file_library_uploads').html('');
            toastr.success('The record deleted successfully.');
            $('#editFileLibrary').val('');
            imgArray = [];
        }
    });
});
/** Edit File library**/

/** Copy File library**/
$(document).on('click','#copy_libraray_file',function(){
    $('#copyFileLibrary').val('');
    $('#copyFileLibrary').trigger('click');
    // console.log('sasasa');
});

$(document).on('change','#copyFileLibrary',function(){
    // fileLibrary = [];
    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        if(size > 4097) {
            toastr.warning('Please select documents less than 4 mb!');
        } else {
            size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
            if (type == 'application/msword' || type == 'image/jpeg' || type == 'image/jpg' || type == 'image/png' || type == 'image/gif' || type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                if($.inArray(value['name'], imgArray) === -1)
                {
                    fileLibrary.push(value);
                }
                var src = '';
                var reader = new FileReader();
                //  $('#file_library_uploads').html('');
                reader.onload = function (e) {
                    if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                        src = upload_url + 'company/images/excel.png';
                    } else if (type == 'application/pdf') {
                        src = upload_url + 'company/images/pdf.png';
                    } else if (type == 'application/msword' || type == 'doc' || type == 'docx' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                        src = upload_url + 'company/images/word_doc_icon.jpg';
                    } else if (type == 'text/plain') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else if (type == 'text/xml') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else {
                        src = e.target.result;
                    }
                    if($.inArray(value['name'], imgArray) === -1)
                    {
                        $("#copy_file_library_uploads").append(
                            '<div class="row" style="margin:0 20px">' +
                            '<div class="col-sm-12 img-upload-library-div" style="margin: 0">' +
                            '<div class="col-sm-4"><img class="img-upload-tab' + key + '" width=40 height=40 src=' + src + '></div>' +
                            '<div style="margin-top: 10px;" class="col-sm-4 center_img show-library-list-imgs-name ' + key + '">' + value['name'] + '</div>' +
                            '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                            '<div  style="    margin-top: 10px;" class="col-sm-4 center_img show-library-list-imgs-size' + key + '">' + size + '</div>' +
                            '</div></div>');
                        imgArray.push(value['name']);
                    } else {
                        toastr.warning('File already exists!');
                    }
                };
                reader.readAsDataURL(value);
            } else {
                toastr.warning('Please select file with .jpg | .png | .jpeg | .gif | .xlsx | .pdf | .docx | .txt | .xml extension only!');
            }
        }
    });
});

$(document).on('click','#remove_library_file_copy',function(){
    bootbox.confirm("Do you want to remove all files?", function (result) {
        if (result == true) {
            $('#copy_file_library_uploads').html('');
            toastr.success('The record deleted successfully.');
            $('#copyFileLibrary').val('');
            imgArray = [];
        }
    });
});
/** Copy File library**/


/** Publish File library**/
$(document).on('click','#publish_libraray_file',function(){
    $('#publishFileLibrary').val('');
    $('#publishFileLibrary').trigger('click');
    // console.log('sasasa');
});

$(document).on('change','#publishFileLibrary',function(){
    // fileLibrary = [];
    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        if(size > 4097) {
            toastr.warning('Please select documents less than 4 mb!');
        } else {
            size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
            if (type == 'application/msword' || type == 'image/jpeg' || type == 'image/jpg' || type == 'image/png' || type == 'image/gif' || type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                if($.inArray(value['name'], imgArray) === -1)
                {
                    fileLibrary.push(value);
                }
                var src = '';
                var reader = new FileReader();
                //  $('#file_library_uploads').html('');
                reader.onload = function (e) {
                    if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                        src = upload_url + 'company/images/excel.png';
                    } else if (type == 'application/pdf') {
                        src = upload_url + 'company/images/pdf.png';
                    } else if (type == 'application/msword' || type == 'doc' || type == 'docx' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                        src = upload_url + 'company/images/word_doc_icon.jpg';
                    } else if (type == 'text/plain') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else if (type == 'text/xml') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else {
                        src = e.target.result;
                    }
                    if($.inArray(value['name'], imgArray) === -1)
                    {
                        $("#publish_file_library_uploads").append(
                            '<div class="row" style="margin:0 20px">' +
                            '<div class="col-sm-12 img-upload-library-div" style="margin: 0">' +
                            '<div class="col-sm-4"><img class="img-upload-tab' + key + '" width=40 height=40 src=' + src + '></div>' +
                            '<div style="margin-top: 10px;" class="col-sm-4 center_img show-library-list-imgs-name ' + key + '">' + value['name'] + '</div>' +
                            '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                            '<div  style="    margin-top: 10px;" class="col-sm-4 center_img show-library-list-imgs-size' + key + '">' + size + '</div>' +
                            '</div></div>');
                        imgArray.push(value['name']);
                    } else {
                        toastr.warning('File already exists!');
                    }
                };
                reader.readAsDataURL(value);
            } else {
                toastr.warning('Please select file with .jpg | .png | .jpeg | .gif | .xlsx | .pdf | .docx | .txt | .xml extension only!');
            }
        }
    });
});

$(document).on('click','#publish_library_file_edit',function(){
    bootbox.confirm("Do you want to remove all files?", function (result) {
        if (result == true) {
            $('#publish_file_library_uploads').html('');
            toastr.success('The record deleted successfully.');
            $('#publishFileLibrary').val('');
            imgArray = [];
        }
    });
});
/** Publish File library**/
