
function statusFmatter (cellvalue, options, rowObject){
    if (cellvalue == 1)
        return "Active";
    else if(cellvalue == '0')
        return "InActive";
    else if(cellvalue == '2')
        return "Draft";
    else
        return '';
}

function announcementFor_formatter (cellvalue, options, rowObject){
    if (cellvalue == 'all')
        return "All";
    else if(cellvalue == '2')
        return "Tenant";
    else if(cellvalue == '3')
        return "Vendor";
    else if(cellvalue == '4')
        return "Owner";
    else
        return '';
}

function actionFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';
        if(rowObject.Status == 1) select = ['Edit','Copy','Delete'];
        if(rowObject.Status == 0) select = ['Copy','Delete'];
        if(rowObject.Status == 2) select = ['Edit','Copy','Delete','Publish'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}
$(document).ready(function () {
    $('#jqGridStatus').val('All');
    announcementsTable('All');
});

//jqGrid status
$('#jqGridStatus').on('change',function(){
    var selected = this.value;
    $('#announcements-table').jqGrid('GridUnload');
    announcementsTable(selected);
});

function announcementsTable(status) {
    var table = 'announcements';
    var columns = ['Announcement Title','Announcement For','Start Date', 'Start Time', 'End Date','End Time','Status','Action'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = ['announcements.status'];
    var extra_where = [];
    // var extra_where = [{column: 'object_id', value: id, condition: '='},{column: 'object_type', value: "owner", condition: '='}];
    var columns_options = [
        {name:'Announcement Title',index:'title', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Announcement For',index:'announcement_for', width:90,align:"center",searchoptions: {sopt: conditions},table:table, formatter:announcementFor_formatter},
        {name:'Start Date',index:'start_date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Start Time',index:'start_time', width:80, align:"right",searchoptions: {sopt: conditions},table:table,change_type:'time'},
        {name:'End Date',index:'end_date', width:80, align:"right",searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'End Time',index:'end_time', width:80,align:"right",searchoptions: {sopt: conditions},table:table,change_type:'time'},
        {name:'Status',index:'status', width:80,align:"right",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter},
        {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, edittype: 'select',search:false,table:table,formatter:actionFmatter},
    ];
    var ignore_array = [];
    jQuery("#announcements-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Announcements",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {top: 10, left: 400, drag: true, resize: false}
    );
}

localStorage.removeItem('edit_active_announcement');
localStorage.removeItem('copy_announcement_active');
localStorage.removeItem('publish_announcement_active');

$(document).on("change", "#announcements-table .select_options", function (e) {
    e.preventDefault();
    setTimeout(function(){ $(".select_options").val("default"); }, 200);
    var base_url = window.location.origin;
    var action = this.value;
    var id = $(this).attr('data_id');
    switch (action) {
        case "Edit":
            localStorage.setItem('edit_active_announcement','true');
            window.location.href = base_url + '/Announcement/AddAnnouncement?id='+id;
            break;
        case "Delete":
            bootbox.confirm("Are you sure you want to delete this record?", function (result) {
                if (result == true) {
                    status_announcement(id,'delete_announcement');
                }
            });
            break;
        case "Copy":
            localStorage.setItem('copy_announcement_active','true');
            window.location.href = base_url + '/Announcement/AddAnnouncement?id='+id;
            break;
        case "Publish":
            localStorage.setItem('publish_announcement_active','true');
            window.location.href = base_url + '/Announcement/AddAnnouncement?id='+id;
            break;
        default:
    }
});

function status_announcement(id,key) {

    $.ajax({
        type: 'post',
        url: '/Announcement-Ajax',
        data: {
            class       : "announcementAjax",
            action      : "updateStatus",
            id          : id,
            status_type : key,
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                toastr.success(response.message);
                $('#announcements-table').trigger( 'reloadGrid' );
            } else if (response.status == 'error' && response.code == 504) {
                toastr.error(response.message);
            } else {
                toastr.error(response.message);
            }
        }
    });
}

