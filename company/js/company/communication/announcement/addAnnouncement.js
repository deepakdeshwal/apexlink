
function getDataByID(data_id, prev_id, data_type){
    if (data_id == '' || data_id == 'null'){
        return;
    }
    $.ajax({
        type: 'post',
        url: '/Announcement-Ajax',
        data: {
            class: "announcementAjax",
            action: "getDataByID",
            id: data_id,
            prev_id: prev_id,
            data_type : data_type
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if(data_type == 'property') {
                    var buildingOption = "<option value=''>Select</option>";
                    $('.unit').html(buildingOption);
                    if (data.data.length > 0) {
                        $.each(data.data, function (key, value) {
                            buildingOption += "<option value='" + value.id + "'>" + value.building_name + "</option>";
                        });
                    }
                    $('.building').html(buildingOption);
                } else if(data_type == 'building') {
                    var buildingOption = "<option value=''>Select</option>";
                    if (data.data.length > 0) {
                     //   console.log('data.data>>>', data.data);
                        $.each(data.data, function (key, value) {
                            buildingOption += "<option value='" + value.id + "'>" + value.unit_prefix +" - " + value.unit_no +"</option>";
                        });
                    }
                    $('.unit').html(buildingOption);
                } else if(data_type == 'users'){
                    var usersOption = "";
                    if (data.data.length > 0){
                        $.each(data.data, function (key, value) {
                            usersOption += "<option value='"+value.id+"'>"+value.name+"</option>";
                        });
                        $('select[name="user_name[]"]').html(usersOption);
                        $('select[name="user_name[]"]').multiselect('destroy');
                        $('select[name="user_name[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                    } else {
                       // alert('a,an');
                        $('select[name="user_name[]"]').html('');
                        $('select[name="user_name[]"]').multiselect('destroy');
                        $('select[name="user_name[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});

                    }

                }

            } else if (data.status == "error") {

                toastr.error(data.message);
            } else {

                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });


}


/**** Clone - Add Announcement div****/
$(document).on("click",".add_more_div_icon",function() {
    var announcementDivLength = $('.add_announcement_div_outer_class').length;
    // console.log('announcementDivLength', announcementDivLength);
    if (announcementDivLength <= 2) {
        var clone = $("#add_announcement_div_outer_id").clone().insertAfter("div.add_announcement_div_outer_class:last");
        // clone.find('#property_owned_id').val('');
        // clone.find('#property_owned_id').attr('id','property_owned_id'+announcementDivLength);
        // clone.find('#property_percent_owned').val('100');
        // clone.find('#property_percent_owned').attr('id','property_percent_owned'+announcementDivLength);
        $(".add_announcement_div_outer_class:not(:eq(0))  .add_more_div_icon").remove();
        $(".add_announcement_div_outer_class:not(:eq(0))  .remove_more_div_icon").show();
        if (announcementDivLength == 2) {
            $('.add_more_div_icon').hide();
        }
    } else {
        $('.add_more_div_icon').hide();
    }
});
/**** Remove Clone - Add Announcement div****/
$(document).on("click",".remove_more_div_icon",function(){
    $(this).parents(".add_announcement_div_outer_class").remove();
    $(".add_announcement_div_outer_class .add_more_div_icon").show();
});





/*** <h3>Add Announcement Div JS Starts</h3> ***/
$(document).ready(function () {

    $(document).on("click",".add_announcement_cancel",function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = '/Announcement/Announcements';
            }
        });

    });

    $.ajax({
        type: 'post',
        url: '/Announcement-Ajax',
        data: {
            class: "announcementAjax",
            action: "getInitialData"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if (data.data.propertyList.length > 0){
                    var propertyOption = "<option value=''>Select</option>";
                    $.each(data.data.propertyList, function (key, value) {
                        propertyOption += "<option value='"+value.id+"' data-id='"+value.property_id+"'>"+value.property_name+"</option>";
                    });
                    $('.property').html(propertyOption);
                }

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        }
    });
    $('select[name="user_name[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});

    $(document).on("change",".property",function (e) {
        e.preventDefault();
        $('.building').empty();
        $('.unit').empty();
        console.log($(this).val());
        if($(this).val() != '') {
           // console.log('aaaa', $(this).val());
            getDataByID($(this).val(), '', 'property');
        } else {
            $('.uilding').html("<option value=''>Select</option>");
            $('.unit').html("<option value=''>Select</option>");
        }

        getDataByID($('.announcement_for').val(),$(this).val(),'users');
        $('.user_name').empty();
        return false;
    });

    $(document).on("change",".building",function (e) {
        e.preventDefault();
        getDataByID($(this).val(),$('.property').val(),'building');
        return false;
    });

    $(document).on("change",".announcement_for",function (e) {
        e.preventDefault();
        getDataByID($(this).val(),$('.property').val(),'users');
        return false;
    });


    $('#add_announcement_div .start_date').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
        onSelect: function (selectedDate) {
            $("#add_announcement_div .end_date").datepicker("option", "minDate", selectedDate);
        }
    }).datepicker("setDate", new Date());

    $('#add_announcement_div .end_date').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
        onSelect: function (selectedDate) {
            $("#add_announcement_div .start_date").datepicker("option", "maxDate", selectedDate);
        }
    }).datepicker("setDate", new Date());

    $('.start_time').timepicker({
        timeFormat: 'h:mm p',
        interval: 5,
        defaultTime: '8',
        startTime: '8:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('.end_time').timepicker({
        timeFormat: 'h:mm p',
        interval: 5,
        defaultTime: '10',
        startTime: '10:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });



    $(document).on("click",".add_form_submit_btn",function(){
        console.log('add_form_submit_btn>>', $(this).attr('id'));
        var btn_value = $(this).attr('id');
        $("#add_announcement_form").validate({
            rules: {
                announcement_for: {
                    required: true
                },
                'user_name[]': {
                    required: true
                },
                title: {
                    required: true
                },
                start_date: {
                    required: true
                },
                start_time: {
                    required: true
                },
                end_date: {
                    required: true
                },
                end_time: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            submitHandler: function (form) {
                event.preventDefault();
                $('#loadingmessage').show();
                var timezone_offset_minutes = new Date().getTimezoneOffset();
                timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;

                var form = $('#add_announcement_form')[0];
                var formData = new FormData(form);
                var data = convertSerializeDatatoArray();


                $.each(fileLibrary, function (key, value) {
                    if (compareArray(value, data) == 'true') {
                        formData.append(key, value);
                    }
                });

                formData.append('class', 'announcementAjax');
                formData.append('action', 'addAnnouncement');
                formData.append('timezone', timezone_offset_minutes);
                formData.append('btn_value', btn_value);
                console.log('formData>>', formData);

                $.ajax({
                    url: '/Announcement-Ajax',
                    type: 'post',
                    data:  formData,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        var response = JSON.parse(response);
                        $('#loadingmessage').hide();
                        if(response.status == 'success' && response.code == 200){
                            localStorage.setItem("Message","Record created successfully.")
                            localStorage.setItem("rowcolor",true)
                            // toastr.success("Announcement Created Successfully.");
                            window.location.href = '/Announcement/Announcements';
                        }else if(response.status == 'error' && response.code == 502){
                            toastr.error(response.message);
                        } else if(response.status == 'error' && response.code == 400){
                            $('.error').html('');
                            $.each(response.data, function (key, value) {
                                $('.'+key).html(value);
                            });
                        }
                    }
                });
            }
        });
    });
});
/*** <h3>Add Announcement Div JS End</h3> ***/


/*** <h3>Edit Announcement Div JS Starts</h3> ***/
$(document).ready(function () {
    function getParameterByName( name ){
        var regexS = "[\\?&]"+name+"=([^&#]*)",
            regex = new RegExp( regexS ),
            results = regex.exec( window.location.search );
        if( results == null ){
            return "";
        } else{
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }
    var edit_id =  getParameterByName('id');
    var edit_active_announcement =  localStorage.getItem("edit_active_announcement");
    console.log('edit_active_announcement>>>', edit_active_announcement);

    if(edit_active_announcement){
        console.log('edit_acfgdftive>>>', edit_active_announcement);
        $('#add_announcement_div').hide();
        $('#copy_announcement_div').hide();
        $('#edit_announcement_div').show();
        $('.add_breadcrumb').hide();
        $('.copy_breadcrumb').hide();
        $('.edit_breadcrumb').show();

        jqGridFileLibrary();
        function jqGridFileLibrary(status) {
            console.log('edit_id>>>>>>>', edit_id);
            var table = 'announcement_file_library';
            var columns = ['Name','Preview','Location','Action'];
            var select_column = ['Email','Delete'];
            var joins = [];
            var conditions = ["eq","bw","ew","cn","in"];
            var extra_columns = [];
            var extra_where = [{column:'announcement_id',value:edit_id,condition:'='}];
            var columns_options = [
                {name:'Name',index:'filename',width:400,align:"left",searchoptions: {sopt: conditions},table:table},
                {name:'Preview',index:'file_extension',width:450,align:"left",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
                {name:'Location',index:'file_location',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,hidden:true},
                {name:'Action',index:'select',width:430,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: imageRedirect, edittype: 'select',search:false,table:table,title:false}
            ];
            var ignore_array = [];
            jQuery("#announcementFileLibrary-table").jqGrid({
                url: '/Companies/List/jqgrid',
                datatype: "json",
                autowidth: true,
                colNames: columns,
                colModel: columns_options,
                pager: true,
                sortname: 'updated_at',
                mtype: "POST",
                postData: {
                    q: 1,
                    class: 'jqGrid',
                    action: "listing_ajax",
                    table: table,
                    select: select_column,
                    columns_options: columns_options,
                    status: status,
                    ignore:ignore_array,
                    joins:joins,
                    extra_columns:extra_columns,
                    extra_where:extra_where
                },
                viewrecords: true,
                sortorder: "desc",
                sortIconsBeforeText: true,
                headertitles: true,
                rowNum: pagination,
                rowList: [5, 10, 20, 30, 50, 100, 200],
                caption: "List of Files",
                pginput: true,
                pgbuttons: true,
                navOptions: {
                    edit: false,
                    add: false,
                    del: false,
                    search: true,
                    filterable: true,
                    refreshtext: "Refresh",
                    reloadGridOptions: {fromServer: true}
                }
            }).jqGrid("navGrid",
                {
                    edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
                },
                {top:10,left:200,drag:true,resize:false} // search options
            );
        }
    }
   /* else {
        $('#edit_announcement_div').hide();
        $('#copy_announcement_div').hide();
        $('#add_announcement_div').show();
        $('.add_breadcrumb').show();
        $('.edit_breadcrumb').hide();
    }
*/

    function imageRedirect(cellValue, options, rowObject){
        if(rowObject !== undefined){
            return "<span data_id='"+rowObject.id+"'><a><img src='"+upload_url+"company/images/trash.png' data_id='"+rowObject.id+"' class='delete_doc_icon' title='Delete' alt='my image' style='cursor: pointer; '/></a></span>";
        }
    }

    /**
     * function to format document
     * @param cellvalue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function imageFormatter(cellvalue, options, rowObject){

        if(rowObject !== undefined) {
            var path = upload_url+'company/'+rowObject.Location;
            var src = '';
            if (rowObject.Preview == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
            } else if (rowObject.Preview == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
            } else if (rowObject.Preview == 'doc'||rowObject.Preview == 'docx') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
            }else if (rowObject.Preview == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
            } else {
                src = path;
            }
            return '<img class="img-upload-tab open_file_location" data-location="'+path+'" width=30 height=30 src="' + src + '">';
        }
    }

    if(edit_id){
        $.ajax({
            type: 'post',
            url: '/Announcement-Ajax',
            data: {
                class: "announcementAjax",
                action: "getAnnouncementById",
                id: edit_id,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    var res = response.data;
                    getDataByID(res.property, '', 'property');       //Building listing
                    getDataByID(res.property,res.building,'building');       //Unit listing
                    getDataByID(res.announcement_for,res.property,'users');  //Users listing
                    edit_date_time(res.updated_at);

                    setTimeout(function () {
                        $.each(res, function (key, value) {
                            $('#edit_announcement_div .'+key).val(value);
                            if (key == 'start_time') {
                                $('#edit_announcement_div .start_time').timepicker('setTime', value);
                            }
                            if (key == 'end_time') {
                                $('#edit_announcement_div .end_time').timepicker('setTime', value);
                            }
                            else if(key == 'user_name')
                            {
                                var userDataInitial = [];
                                $('#edit_announcement_div .user_name option').each(function (key,value) {
                                    userDataInitial.push({'id':$(this).val(),'text':$(this).text()});
                                });
                                selectUserNames(userDataInitial,value);
                            }
                        });
                    }, 300);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }


    function selectUserNames(userDataInitial,value) {
        if (userDataInitial.length > 0){
            var nameOption = "";

            $.each(userDataInitial, function (nameKey, nameData) {
                if($.inArray(nameData.id, value) !== -1) {
                    nameOption += "<option value='"+nameData.id+"' selected>"+nameData.text+"</option>";
                } else {
                    nameOption += "<option value='"+nameData.id+"'>"+nameData.text+"</option>";
                }
            });
            setTimeout(function () {
                $("#user_name_e").multiselect("clearSelection");
                $('#user_name_e').html(nameOption);

                $('#user_name_e').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                $("#user_name_e").multiselect( 'refresh' );
            },400);
        }
    }

    $('#start_date_edit').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
        onSelect: function (selectedDate) {
            $("#end_date_edit").datepicker("option", "minDate", selectedDate);
            $("#start_date_edit").val(selectedDate);
        }
    });

    $('#end_date_edit').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
        onSelect: function (selectedDate) {
            $("#start_date_edit").datepicker("option", "maxDate", selectedDate);
            $("#end_date_edit").val(selectedDate);
        }
    });

    //edit announcement
    $("#edit_announcement_form").validate({
        rules: {
            announcement_for: {
                required: true
            },
            'user_name[]': {
                required: true
            },
            title: {
                required: true
            },
            start_date: {
                required: true
            },
            start_time: {
                required: true
            },
            end_date: {
                required: true
            },
            end_time: {
                required: true
            },
            description: {
                required: true
            }
        },
        submitHandler: function (form) {

            event.preventDefault();

            var timezone_offset_minutes = new Date().getTimezoneOffset();
            timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;

            var form = $('#edit_announcement_form')[0];
            var formData = new FormData(form);
            var data = convertSerializeDatatoArray();


            $.each(fileLibrary, function (key, value) {
                if (compareArray(value, data) == 'true') {
                    formData.append(key, value);
                }
            });
            formData.append('edit_id', edit_id);

            formData.append('class', 'announcementAjax');

            formData.append('action', 'addAnnouncement');
            formData.append('timezone', timezone_offset_minutes);
            console.log('formData>>', formData);

            $.ajax({
                url: '/Announcement-Ajax',
                type: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        localStorage.setItem("Message", "Record updated successfully.")
                        localStorage.setItem("rowcolor", true);
                        window.location.href = '/Announcement/Announcements';

                    } else if (response.status == 'error' && response.code == 502) {
                        toastr.error(response.message);
                    } else if (response.status == 'warning' && response.code == 500) {
                        toastr.warning(response.message);
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.' + key).html(value);
                        });
                    }
                }
            });
        }
    });
});
/*** <h3>Edit Announcement Div JS Ends</h3> ***/

/*** <h3>Copy Announcement Div JS Starts</h3> ***/
$(document).ready(function () {
    $('#jqGridStatus').val('All');
    function getParameterByName( name ){
        var regexS = "[\\?&]"+name+"=([^&#]*)",
            regex = new RegExp( regexS ),
            results = regex.exec( window.location.search );
        if( results == null ){
            return "";
        } else{
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }
    var copy_id =  getParameterByName('id');
    var copy_announcement_active =  localStorage.getItem("copy_announcement_active");
    if(copy_announcement_active){

        $('#add_announcement_div').hide();
        $('#edit_announcement_div').hide();
        $('#copy_announcement_div').show();
        $('.add_breadcrumb').hide();
        $('.edit_breadcrumb').hide();
        $('.copy_breadcrumb').show();

        jqGridFileLibraryCopy();
        function jqGridFileLibraryCopy(status) {
            console.log('edit_id>>>>>>>', copy_id);
            var table = 'announcement_file_library';
            var columns = ['Name','Preview','Location','Action'];
            var columns = ['Name','Preview','Location','Action'];
            var select_column = ['Email','Delete'];
            var joins = [];
            var conditions = ["eq","bw","ew","cn","in"];
            var extra_columns = [];
            var extra_where = [{column:'announcement_id',value:copy_id,condition:'='}];
            var columns_options = [
                {name:'Name',index:'filename',width:400,align:"left",searchoptions: {sopt: conditions},table:table},
                {name:'Preview',index:'file_extension',width:450,align:"left",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
                {name:'Location',index:'file_location',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,hidden:true},
                {name:'Action',index:'select',width:430,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: imageRedirect, edittype: 'select',search:false,table:table,title:false}
            ];
            var ignore_array = [];
            jQuery("#announcementFileLibraryCopy-table").jqGrid({
                url: '/Companies/List/jqgrid',
                datatype: "json",
                autowidth: true,
                colNames: columns,
                colModel: columns_options,
                pager: true,
                sortname: 'updated_at',
                mtype: "POST",
                postData: {
                    q: 1,
                    class: 'jqGrid',
                    action: "listing_ajax",
                    table: table,
                    select: select_column,
                    columns_options: columns_options,
                    status: status,
                    ignore:ignore_array,
                    joins:joins,
                    extra_columns:extra_columns,
                    extra_where:extra_where
                },
                viewrecords: true,
                sortorder: "desc",
                sortIconsBeforeText: true,
                headertitles: true,
                rowNum: pagination,
                rowList: [5, 10, 20, 30, 50, 100, 200],
                caption: "List of Files",
                pginput: true,
                pgbuttons: true,
                navOptions: {
                    edit: false,
                    add: false,
                    del: false,
                    search: true,
                    filterable: true,
                    refreshtext: "Refresh",
                    reloadGridOptions: {fromServer: true}
                }
            }).jqGrid("navGrid",
                {
                    edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
                },
                {top:10,left:200,drag:true,resize:false} // search options
            );
        }

        if(copy_id){
            $.ajax({
                type: 'post',
                url: '/Announcement-Ajax',
                data: {
                    class: "announcementAjax",
                    action: "getAnnouncementById",
                    id: copy_id,
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        var res = response.data;
                        getDataByID(res.property, '', 'property');       //Building listing
                        getDataByID(res.property,res.building,'building');       //Unit listing
                        getDataByID(res.announcement_for,res.property,'users');  //Users listing
                        edit_date_time(res.updated_at);

                        setTimeout(function () {
                            $.each(res, function (key, value) {
                                $('#copy_announcement_div .'+key).val(value);
                                if (key == 'start_time') {
                                    $('#copy_announcement_div .start_time').timepicker('setTime', value);
                                }
                                if (key == 'end_time') {
                                    $('#copy_announcement_div .end_time').timepicker('setTime', value);
                                }
                                else if(key == 'user_name')
                                {
                                    var userDataInitial = [];
                                    $('#copy_announcement_div .user_name option').each(function (key,value) {
                                        userDataInitial.push({'id':$(this).val(),'text':$(this).text()});
                                    });

                                    selectUserNames(userDataInitial,value);
                                }
                            });
                        }, 300);
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }
    }

    function imageRedirect(cellValue, options, rowObject){
        if(rowObject !== undefined){
            return "<span data_id='"+rowObject.id+"'><a><img src='"+upload_url+"company/images/trash.png' data_id='"+rowObject.id+"' class='delete_doc_icon' title='Delete' alt='my image' style='cursor: pointer; '/></a></span>";
        }
    }

    /**
     * function to format document
     * @param cellvalue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function imageFormatter(cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var path = upload_url+'company/'+rowObject.Location;
            var src = '';
            if (rowObject.Preview == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
            } else if (rowObject.Preview == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
            } else if (rowObject.Preview == 'docx') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
            }else if (rowObject.Preview == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
            } else {
                src = path;
            }
            return '<img class="img-upload-tab open_file_location" data-location="'+path+'" width=30 height=30 src="' + src + '">';
        }
    }

    function selectUserNames(userDataInitial,value) {

        if (userDataInitial.length > 0){
            var nameOption = "";

            $.each(userDataInitial, function (nameKey, nameData) {
                if($.inArray(nameData.id, value) !== -1) {
                    nameOption += "<option value='"+nameData.id+"' selected>"+nameData.text+"</option>";
                } else {
                    nameOption += "<option value='"+nameData.id+"'>"+nameData.text+"</option>";
                }
            });
            setTimeout(function () {
                $("#user_name_copy").multiselect("clearSelection");
                $('#user_name_copy').html(nameOption);

                $('#user_name_copy').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                $("#user_name_copy").multiselect( 'refresh' );
            },400);
        }
    }

    $('#start_date_copy').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
        onSelect: function (selectedDate) {
            $("#end_date_copy").datepicker("option", "minDate", selectedDate);
            $("#start_date_copy").val(selectedDate);
        }
    });

    $('#end_date_copy').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
        onSelect: function (selectedDate) {
            $("#start_date_copy").datepicker("option", "maxDate", selectedDate);
            $("#end_date_copy").val(selectedDate);
        }
    });

    //copy announcement
    $("#copy_announcement_form").validate({
        rules: {
            announcement_for: {
                required: true
            },
            'user_name[]': {
                required: true
            },
            title: {
                required: true
            },
            start_date: {
                required: true
            },
            start_time: {
                required: true
            },
            end_date: {
                required: true
            },
            end_time: {
                required: true
            },
            description: {
                required: true
            }
        },
        submitHandler: function (form) {

            event.preventDefault();

            var timezone_offset_minutes = new Date().getTimezoneOffset();
            timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;

            var form = $('#copy_announcement_form')[0];
            var formData = new FormData(form);
            var data = convertSerializeDatatoArray();


            $.each(fileLibrary, function (key, value) {
                if (compareArray(value, data) == 'true') {
                    formData.append(key, value);
                }
            });
            formData.append('copy_id', copy_id);

            formData.append('class', 'announcementAjax');

            formData.append('action', 'addAnnouncement');
            formData.append('timezone', timezone_offset_minutes);
            console.log('formData>>', formData);

            $.ajax({
                url: '/Announcement-Ajax',
                type: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        localStorage.setItem("Message", "Record created successfully.")
                        localStorage.setItem("rowcolor", true);
                        window.location.href = '/Announcement/Announcements';
                        $('#jqGridStatus').val('All');
                    } else if (response.status == 'error' && response.code == 502) {
                        toastr.error(response.message);
                    } else if (response.status == 'warning' && response.code == 500) {
                        toastr.warning(response.message);
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.' + key).html(value);
                        });
                    }
                }
            });
        }
    });


});
/*** <h3>Copy Announcement Div JS Ends</h3> ***/

/*** <h3>Publish Announcement Div JS Starts</h3> ***/
$(document).ready(function () {
    function getParameterByName( name ){
        var regexS = "[\\?&]"+name+"=([^&#]*)",
            regex = new RegExp( regexS ),
            results = regex.exec( window.location.search );
        if( results == null ){
            return "";
        } else{
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }
    var publish_id =  getParameterByName('id');
    var publish_announcement_active =  localStorage.getItem("publish_announcement_active");
    if(publish_announcement_active){
        $('#add_announcement_div').hide();
        $('#edit_announcement_div').hide();
        $('#copy_announcement_div').hide();
        $('#publish_announcement_div').show();
        $('.add_breadcrumb').hide();
        $('.edit_breadcrumb').hide();
        $('.copy_breadcrumb').hide();
        $('.publish_breadcrumb').show();

        jqGridFileLibraryPublish();
        function jqGridFileLibraryPublish(status) {
            console.log('publish_id>>>>>>>', publish_id);
            var table = 'announcement_file_library';
            var columns = ['Name','Preview','Location','Action'];
            var select_column = ['Email','Delete'];
            var joins = [];
            var conditions = ["eq","bw","ew","cn","in"];
            var extra_columns = [];
            var extra_where = [{column:'announcement_id',value:publish_id,condition:'='}];
            var columns_options = [
                {name:'Name',index:'filename',width:400,align:"left",searchoptions: {sopt: conditions},table:table},
                {name:'Preview',index:'file_extension',width:450,align:"left",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
                {name:'Location',index:'file_location',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,hidden:true},
                {name:'Action',index:'select',width:430,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: imageRedirect, edittype: 'select',search:false,table:table,title:false}
            ];
            var ignore_array = [];
            jQuery("#announcementFileLibraryPublish-table").jqGrid({
                url: '/Companies/List/jqgrid',
                datatype: "json",
                autowidth: true,
                colNames: columns,
                colModel: columns_options,
                pager: true,
                sortname: 'updated_at',
                mtype: "POST",
                postData: {
                    q: 1,
                    class: 'jqGrid',
                    action: "listing_ajax",
                    table: table,
                    select: select_column,
                    columns_options: columns_options,
                    status: status,
                    ignore:ignore_array,
                    joins:joins,
                    extra_columns:extra_columns,
                    extra_where:extra_where
                },
                viewrecords: true,
                sortorder: "desc",
                sortIconsBeforeText: true,
                headertitles: true,
                rowNum: pagination,
                rowList: [5, 10, 20, 30, 50, 100, 200],
                caption: "List of Files",
                pginput: true,
                pgbuttons: true,
                navOptions: {
                    edit: false,
                    add: false,
                    del: false,
                    search: true,
                    filterable: true,
                    refreshtext: "Refresh",
                    reloadGridOptions: {fromServer: true}
                }
            }).jqGrid("navGrid",
                {
                    edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
                },
                {top:10,left:200,drag:true,resize:false} // search options
            );
        }

        if(publish_id){
            console.log('id>>', publish_id);
            $.ajax({
                type: 'post',
                url: '/Announcement-Ajax',
                data: {
                    class: "announcementAjax",
                    action: "getAnnouncementById",
                    id: publish_id,
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        var res = response.data;
                        getDataByID(res.property, '', 'property');       //Building listing
                        getDataByID(res.property,res.building,'building');       //Unit listing
                        getDataByID(res.announcement_for,res.property,'users');  //Users listing

                        setTimeout(function () {
                            $.each(res, function (key, value) {
                                $('#publish_announcement_div .'+key).val(value);
                                if (key == 'start_time') {
                                    $('#publish_announcement_div .start_time').timepicker('setTime', value);
                                }
                                if (key == 'end_time') {
                                    $('#publish_announcement_div .end_time').timepicker('setTime', value);
                                }
                                else if(key == 'user_name')
                                {
                                    var userDataInitial = [];
                                    $('#publish_announcement_div .user_name option').each(function (key,value) {
                                        userDataInitial.push({'id':$(this).val(),'text':$(this).text()});
                                    });

                                    selectUserNames(userDataInitial,value);
                                }

                            });

                        }, 300);
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }
    }

    function imageRedirect(cellValue, options, rowObject){
        if(rowObject !== undefined){
            return "<span data_id='"+rowObject.id+"'><a><img src='"+upload_url+"company/images/trash.png' data_id='"+rowObject.id+"' class='delete_doc_icon' title='Delete' alt='my image' style='cursor: pointer; '/></a></span>";
        }
    }

    /**
     * function to format document
     * @param cellvalue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function imageFormatter(cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var path = upload_url+'company/'+rowObject.Location;
            var src = '';
            if (rowObject.Preview == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
            } else if (rowObject.Preview == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
            } else if (rowObject.Preview == 'docx') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
            }else if (rowObject.Preview == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
            } else {
                src = path;
            }
            return '<img class="img-upload-tab open_file_location" data-location="'+path+'" width=30 height=30 src="' + src + '">';
        }
    }

    function selectUserNames(userDataInitial,value) {
        if (userDataInitial.length > 0){
            var nameOption = "";

            $.each(userDataInitial, function (nameKey, nameData) {
                if($.inArray(nameData.id, value) !== -1) {
                    nameOption += "<option value='"+nameData.id+"' selected>"+nameData.text+"</option>";
                } else {
                    nameOption += "<option value='"+nameData.id+"'>"+nameData.text+"</option>";
                }
            });
            setTimeout(function () {
                $("#user_name_publish").multiselect("clearSelection");
                $('#user_name_publish').html(nameOption);

                $('#user_name_publish').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                $("#user_name_publish").multiselect( 'refresh' );
            },400);
        }
    }

    $('#start_date_publish').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
        onSelect: function (selectedDate) {
            $("#end_date_publish").datepicker("option", "minDate", selectedDate);
            $("#start_date_publish").val(selectedDate);
        }
    });

    $('#end_date_publish').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
        onSelect: function (selectedDate) {
            $("#start_date_publish").datepicker("option", "maxDate", selectedDate);
            $("#end_date_publish").val(selectedDate);
        }
    });

    //Publish announcement
    $(document).on("click",".publish_form_submit_btn",function() {
        console.log('add_form_submit_btn>>', $(this).attr('id'));
        var btn_value = $(this).attr('id');
        $("#publish_announcement_form").validate({
            rules: {
                announcement_for: {
                    required: true
                },
                'user_name[]': {
                    required: true
                },
                title: {
                    required: true
                },
                start_date: {
                    required: true
                },
                start_time: {
                    required: true
                },
                end_date: {
                    required: true
                },
                end_time: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            submitHandler: function (form) {

                event.preventDefault();

                var timezone_offset_minutes = new Date().getTimezoneOffset();
                timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;

                var form = $('#publish_announcement_form')[0];
                var formData = new FormData(form);
                var data = convertSerializeDatatoArray();


                $.each(fileLibrary, function (key, value) {
                    if (compareArray(value, data) == 'true') {
                        formData.append(key, value);
                    }
                });
                formData.append('publish_id', publish_id);

                formData.append('class', 'announcementAjax');

                formData.append('action', 'addAnnouncement');
                formData.append('timezone', timezone_offset_minutes);
                formData.append('btn_value', btn_value);
                console.log('formData>>', formData);

                $.ajax({
                    url: '/Announcement-Ajax',
                    type: 'post',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        var response = JSON.parse(response);
                        if (response.status == 'success' && response.code == 200) {
                            localStorage.setItem("Message", "Record updated successfully.")
                            localStorage.setItem("rowcolor", true);
                            window.location.href = '/Announcement/Announcements';
                            $('#jqGridStatus').val('All');
                        } else if (response.status == 'error' && response.code == 502) {
                            toastr.error(response.message);
                        } else if (response.status == 'warning' && response.code == 500) {
                            toastr.warning(response.message);
                        } else if (response.status == 'error' && response.code == 400) {
                            $('.error').html('');
                            $.each(response.data, function (key, value) {
                                $('.' + key).html(value);
                            });
                        }
                    }
                });
            }
        });
    });

});
/*** <h3>Publish Announcement Div JS Ends</h3> ***/