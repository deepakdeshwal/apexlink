$(document).ready(function () {

    $(document).on("change", "#select_all_complaint_checkbox", function () {
        if ($(this).is(":checked")) {
            $(".maintenance_checkbox").prop("checked", true);
        } else {
            $(".maintenance_checkbox").prop("checked", false);
        }

    });

    $(document).on("change", ".maintenance_checkbox", function () {
        if ($(this).is(":checked")) {
            //$(".maintenance_checkbox").prop("checked",true);
        } else {
            $("#select_all_complaint_checkbox").prop("checked", false);
        }

    });

    addCalanderDate();

    $('.image-editor').cropit({
        imageState: {
            // src: subdomain_url+'500/400',
            src: subdomain_url+'500/400',
        },
    });

    $(document).on("click", '.export', function () {
        $(this).parents('.cropItData').hide();
        $(this).parent().prev().val('');
        var dataVal = $(this).attr("data-val");
        var imageData = $(this).parents('.image-editor').cropit('export');
        var image = new Image();
        image.src = imageData;
        $(".popup-bg").hide();
        $(this).parent().parent().prev().find('div').html(image);
    });

    inventoryHandTable();
    inventoryHandTable2();

    $(document).on('click', '#import_tenant', function () {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $('#import_tenant_type_div').show(500);
    });
    $(document).on("click", "#import_tenant_cancel_btn", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#import_tenant_type_div").hide(500);
                }
            }
        });
    });

    $(document).on("click", ".inventory_cancel", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    window.location.href = window.location.href;
                }
            }
        });
    });

    $(document).on("click", ".cancel_invenentory", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#import_tenant_type_div").hide(500);
                }
            }
        });
    });

    $("#importTenantTypeFormId").validate({
        rules: {
            import_file: {
                required: true
            },
        },
        submitHandler: function (form) {
            event.preventDefault();

            var formData = $('#importTenantTypeForm').serializeArray();
            var myFile = $('#import_file').prop('files');
            var myFiles = myFile[0];
            var formData = new FormData();
            formData.append('file', myFiles);
            formData.append('class', 'Maintenance');
            formData.append('action', 'importInventoryTracker');
            $.ajax({
                type: 'post',
                url: '/maintenance',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        toastr.success(response.message);
                        $("#import_tenant_type_div").hide(500)
                        $('#inventory_hand_table2').trigger('reloadGrid');
                    } else if (response.status == 'failed' && response.code == 503) {
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });

    $(document).on("blur", "input[name='nameOfItemPurchased']", function () {
        var cate=$("#inventoryCategory").val();
        var sub_cat=$("#subcategory").val();
        if(cate !=="" && cate !==undefined && sub_cat !== "" && sub_cat !==undefined){
        var value = parseInt($(this).val());
        var cat = $("select[name='inventoryCategory'] option:selected").text();
        var subcat = $("select[name='subcategory'] option:selected").text();
        var returnHtml = createEnterCodeHtml(value, cat, subcat);
        $(".item_codes").html(returnHtml);
        var perItem = $("input[name='costPerItem']").val();
        if (perItem != "" && perItem !== undefined) {
            var total = value * parseInt(perItem);
            $("input[name='purchaseCost']").val(total);
        }
        }
        else{
            toastr.warning("Please fill the Category and Sub Category");
            $("input[name='nameOfItemPurchased']").val("");
        }
    });

    $(document).on("blur", "input[name='costPerItem']", function () {
        var value = parseInt($(this).val());
        var numberItems = parseInt($("input[name='nameOfItemPurchased']").val());
        var total = value * numberItems;
        $("input[name='purchaseCost']").val(total);
    });

});

function createEnterCodeHtml(num, cat, subcat) {
    var html = "";
    if (num == "" || num == "") {
        return html;
    }
    html += '<div class="panel panel-default">';
    html += '<div class="form-outer">';
    html += '<div class="form-hdr">';
    html += '<h3>Enter Item Code</h3>';
    html += '</div>';
    html += '<div class="form-data">';
    html += '<div class="">';
    html += '<table class="table" style="width:100%;">';
    html += '<thead><tr><th width= "33%" class="text-center" style="font-weight:bold;">Category</th><th  width= "33%" class="text-center" style="font-weight:bold;">Subcategory</th><th width= "33%" class="text-center" style="font-weight:bold;">Item Code</th></tr></thead>';
    html += '<tbody>';
    for (var i = 0; i < num; i++) {
        html += "<tr><td class='text-center'>" + cat + "</td><td class='text-center'>" + subcat + "</td><td class='text-center'><input type='text' name='item_code[]' class='form-control' style='width:300px; display:inline-block;'></td></tr>";
    }
    html += '</tbody>';
    html += '</table>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    return html;

}


function inventoryHandTable() {
    var table = 'maintenance_inventory';
    var columns = ['Property', 'Building', 'Inventory Sublocation', 'Category', 'Subcategory', 'Quantity', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [
        {table: 'maintenance_inventory', column: 'property_id', primary: 'id', on_table: 'general_property'},
        {table: 'maintenance_inventory', column: 'building_id', primary: 'id', on_table: 'building_detail'},
        {
            table: 'maintenance_inventory',
            column: 'sub_location_id',
            primary: 'id',
            on_table: 'maintenance_inventory_sublocation'
        },
        {
            table: 'maintenance_inventory',
            column: 'category_id',
            primary: 'id',
            on_table: 'company_maintenance_subcategory',
            as: 'cat'
        },
        {
            table: 'maintenance_inventory',
            column: 'sub_category_id',
            primary: 'id',
            on_table: 'company_maintenance_subcategory',
            as: 'subcat'
        }
    ];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    //var extra_where = [{column:'user_id',value:tenantId,condition:'=',table:'hoa_violation'}];
    var extra_where = [];
    var extra_columns = [];
    var columns_options = [
        {
            name: 'Property',
            index: 'property_name',
            width: 170,
            align: "center",
            searchoptions: {sopt: conditions},
            table: 'general_property'
        },
        {
            name: 'Building',
            index: 'building_name',
            align: "center",
            width: 170,
            searchoptions: {sopt: conditions},
            table: 'building_detail'
        },
        {
            name: 'Inventory Sublocation',
            index: 'sublocation',
            width: 80,
            align: "left",
            searchoptions: {sopt: conditions},
            table: 'maintenance_inventory_sublocation'
        },
        {
            name: 'Category',
            index: 'category',
            width: 170,
            align: "center",
            searchoptions: {sopt: conditions},
            table: 'cat'
        },
        {
            name: 'Subcategory',
            index: 'sub_category',
            width: 170,
            align: "center",
            searchoptions: {sopt: conditions},
            table: 'subcat'
        },
        {
            name: 'Quantity',
            index: 'item_purchased',
            width: 170,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Action',
            index: '',
            title: false,
            width: 135,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            formatter: 'select',
            edittype: 'select',
            search: false,
            table: table
        }
    ];
    var ignore_array = [];
    jQuery("#inventory_hand_table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,

            ignore: ignore_array,
            joins: joins,
            extra_where: extra_where,
            extra_columns: extra_columns,
            deleted_at: true
        },
        viewrecords: true,
        sortname: 'maintenance_inventory.updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Inventory on Hand",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 0, left: 400, drag: true, resize: false} // search options
    );
}


function inventoryHandTable2(inventory_status = 'All') {

    if (inventory_status == 'All') {
        var delete_at = true;
    } else {
        var delete_at = false;
    }

    var table = 'maintenance_inventory';
    var columns = ['<input type="checkbox" id="select_all_complaint_checkbox">', 'Property', 'Building', 'Inventory Sublocation', 'Category', 'Subcategory', 'Quantity', 'Cost/unit', 'Low Stock Recorder Level', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [
        {table: 'maintenance_inventory', column: 'property_id', primary: 'id', on_table: 'general_property'},
        {table: 'maintenance_inventory', column: 'building_id', primary: 'id', on_table: 'building_detail'},
        {
            table: 'maintenance_inventory',
            column: 'sub_location_id',
            primary: 'id',
            on_table: 'maintenance_inventory_sublocation'
        },
        {
            table: 'maintenance_inventory',
            column: 'category_id',
            primary: 'id',
            on_table: 'company_maintenance_subcategory',
            as: 'cat'
        },
        {
            table: 'maintenance_inventory',
            column: 'sub_category_id',
            primary: 'id',
            on_table: 'company_maintenance_subcategory',
            as: 'subcat'
        }
    ];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    //var extra_where = [{column:'user_id',value:tenantId,condition:'=',table:'hoa_violation'}];
    var extra_where = [];
    var extra_columns = [];
    var columns_options = [
        {
            name: 'Id',
            index: 'id',
            width: 150,
            align: "center",
            sortable: false,
            searchoptions: {sopt: conditions},
            table: table,
            search: false,
            formatter: actionCheckboxFmatterComplaint
        },
        {
            name: 'Property',
            index: 'property_name',
            width: 150,
            align: "center",
            searchoptions: {sopt: conditions},
            table: 'general_property'
        },
        {
            name: 'Building',
            index: 'building_name',
            align: "center",
            width: 170,
            searchoptions: {sopt: conditions},
            table: 'building_detail'
        },
        {
            name: 'Inventory Sublocation',
            index: 'sublocation',
            width: 80,
            align: "left",
            searchoptions: {sopt: conditions},
            table: 'maintenance_inventory_sublocation'
        },
        {
            name: 'Category',
            index: 'category',
            width: 150,
            align: "center",
            searchoptions: {sopt: conditions},
            table: 'cat'
        },
        {
            name: 'Subcategory',
            index: 'sub_category',
            width: 150,
            align: "center",
            searchoptions: {sopt: conditions},
            table: 'subcat'
        },
        {
            name: 'Quantity',
            index: 'item_purchased',
            width: 150,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Cost/unit',
            index: 'cost_per_item',
            width: 150,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Low Stock Recorder Level',
            index: 'stock_reorder_level',
            width: 200,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Action',
            index: '',
            title: false,
            width: 135,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            formatter: 'select',
            edittype: 'select',
            search: false,
            table: table
        }
    ];
    var ignore_array = [];
    jQuery("#inventory_hand_table2").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,

            ignore: ignore_array,
            joins: joins,
            extra_where: extra_where,
            extra_columns: extra_columns,
            deleted_at: delete_at
        },
        viewrecords: true,
        sortname: 'maintenance_inventory.updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Inventory on Hand",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 0, left: 400, drag: true, resize: false} // search options
    );
}


$(document).on("click", ".searchInventory", function () {

    inventoryFilters();


});


function inventoryFilters() {
    var grid = $("#inventory_hand_table2"), f = [];
    var seachsample = ($('#seachsample').val().length == 0) ? 'all' : $('#seachsample').val();

    f.push({
        field: "general_property.property_name",
        op: "in",
        data: seachsample,
        con: 'OR'
    }, {
        field: "building_detail.building_name",
        op: "eq",
        data: seachsample,
        con: 'OR'
    }, {
        field: "maintenance_inventory_sublocation.sublocation",
        op: "cn",
        data: seachsample,
        con: 'OR'
    }, {field: "cat.category", op: "cn", data: seachsample, con: 'OR'}, {
        field: "subcat.sub_category",
        op: "cn",
        data: seachsample
    });
    grid[0].p.search = true;
    $.extend(grid[0].p.postData, {filters: JSON.stringify(f), status: 'all'});
    grid.trigger("reloadGrid", [{page: 1, current: true}]);
}


$(document).on("click", ".lostSearch", function () {
    lostFilters();
});
function getParameterByName(name) {
    var regexS = "[\\?&]" + name + "=([^&#]*)",
        regex = new RegExp(regexS),
        results = regex.exec(window.location.search);
    if (results == null) {
        return "";
    } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}
var inventory_id = getParameterByName('id');
if(inventory_id){
    setTimeout(function(){
        editTrackerData(inventory_id);
    },1000);
}

$(document).on("change", ".inventoryTableTwo table .select_options", function () {


    var value = $(this).val();
    var id = $(this).attr('data_id');
    if (value == 'Edit') {

        var inventory_id = $(this).attr('data_id');
        window.location.href = window.location.origin + '/Maintenance/Inventory?id=' + inventory_id;
        $(".type").val('edit');
        $(".inventory_id").val(inventory_id);
        var idd = getParameterByName('id');



        $.ajax({
            url: '/maintenance',
            type: 'POST',
            data: {

                "action": 'getInventory',
                "class": 'Maintenance',
                "inventory_id": inventory_id
            },
            success: function (response) {
                var info = JSON.parse(response);
                setTimeout(function () {
                    $(".inventoryCategory ").val(info.category_id);
                    $(".inventoryCategory").trigger('change');

                    setTimeout(function () {
                        $(".subcategory").val(info.sub_category_id);
                        $(".subcategory").trigger('change');

                        setTimeout(function () {
                            $(".brand").val(info.brand_id);

                            $(".brand").trigger('change');
                            setTimeout(function () {
                                $(".supplier").val(info.supplier_id);
                            }, 200);

                        }, 200);


                    }, 200)
                }, 200);

                setTimeout(function () {
                    $(".property").val(info.property_id);
                    $(".property").trigger('change');

                    $(".building").val(info.building_id);
                }, 200);


                $.each(info, function (key, value) {
                    if (value != "") {

                        $('.' + key).val(value);


                    }
                });
            }
        });


    } else if (value == 'Delete') {

        deleteTableData('maintenance_inventory', id);
        $('#inventory_hand_table2').trigger('reloadGrid');

    }

});


$(document).on("change", ".inventoryTableOne table .select_options", function () {

    var value = $(this).val();
    var id = $(this).attr('data_id');
    if (value == 'Edit') {
        var inventory_id = $(this).attr('data_id');
        $(".type").val('edit');
        $(".inventory_id").val(inventory_id);
        $('input.clear-btn[type="button"]').val('Reset');
        $('input.clear-btn[type="button"]').addClass('formreset');
        $('input.clear-btn[type="button"]').removeClass('clearFormReset');
     InventoryData(inventory_id);


    } else if (value == 'Delete') {

        deleteTableData('maintenance_inventory', id);
        $('#inventory_hand_table').trigger('reloadGrid');

    }

});
function InventoryData(inventory_id){
    $.ajax({
        url: '/maintenance',
        type: 'POST',
        data: {

            "action": 'getInventory',
            "class": 'Maintenance',
            "inventory_id": inventory_id
        },
        success: function (response) {
            var info = JSON.parse(response);

            setTimeout(function () {

                $(".inventoryCategory ").val(info.category_id);
                var triggerCategory = $(".inventoryCategory").trigger('change');


                setTimeout(function () {
                    $(".subcategory").val(info.sub_category_id);
                    var triggersubcategory = $(".subcategory").trigger('change');

                    setTimeout(function () {
                        $(".brand").val(info.brand_id);

                        var triggerBrand = $(".brand").trigger('change');
                        setTimeout(function () {
                            $(".supplier").val(info.supplier_id);
                        }, 200);

                    }, 200);


                }, 200)
            }, 200);

            setTimeout(function () {
                $(".property").val(info.property_id);
                var triggerCategory = $(".property").trigger('change');

                $(".building").val(info.building_id);
            }, 200);


            $.each(info, function (key, value) {
                if (value != "") {

                    $('.' + key).val(value);


                }
            });
            defaultFormData = $("#addInventory").serializeArray();
        }
    });
}
var defaultFormData='';
$(document).on('click','.formreset',function () {
    var id = $('.inventory_id').val();
   // $('.item_codes').hide();
    InventoryData(id);
    resetEditForm("#addInventory",['inventoryCategory','subcategory','brand','supplier'],false,defaultFormData,[]);
});
$(document).on("change", ".inventory_status", function () {

    $('#inventory_hand_table2').jqGrid('GridUnload');
    var inventory_status = $(this).val();
    inventoryHandTable2(inventory_status);

})


function actionCheckboxFmatterComplaint(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {

        var data = '';
        var data = '<input type="checkbox" name="maintenance_checkbox[]" class="maintenance_checkbox" id="maintenance_checkbox' + rowObject.id + '" data_id="' + rowObject.id + '"/>';
        return data;
    }
}

$("#select_all_complaint_checkbox").click(function () {
    $(".complaint_checkbox").prop('checked', $(this).prop('checked'));
});


$(document).on("click", ".downloadpdf", function () {
    var sList = [];
    $('.maintenance_checkbox').each(function () {
        if (this.checked) {
            var checkboxId = $(this).attr("data_id");
            sList.push(checkboxId);
        }
    });
    if (sList.length !== 0) {
        getHtmlPdfConverter(sList, 'pdf');
    } else {
        toastr.warning("Please select atleast one Inventory Ticket.");
    }
});

$(document).on("click", ".downloadexcel", function () {
    var sList = [];
    $('.maintenance_checkbox').each(function () {
        if (this.checked) {
            var checkboxId = $(this).attr("data_id");
            sList.push(checkboxId);
        }
    });
    if (sList.length !== 0) {
        getTableHtml(sList, 'excel');
    } else {
        toastr.warning("Please select atleast one Inventory Ticket..");
    }
});

$(document).on("click", ".downloadexcelAll", function () {
    downloadExcelAll();
});

function getHtmlPdfConverter(dataArray, datatypes) {
    $.ajax({
        url: '/maintenance',
        type: 'POST',
        data: {
            class: "Maintenance",
            action: "getPdfContent",
            dataArray: dataArray,
            type: datatypes
        },
        success: function (response) {
            var response = JSON.parse(response);

            if (response.status == 'success' && response.code == 200) {
                var link = document.createElement('a');
                document.body.appendChild(link);
                link.target = "_blank";
                link.download = "Inventory.pdf";
                link.href = response.data.record;
                link.click();
            } else if (response.code == 500) {
                //toastr.warning(response.message);
            } else {
                //toastr.error(response.message);
            }

        }
    });
}

function downloadExcelAll() {
    $.ajax({
        url: '/maintenance',
        type: 'POST',
        data: {
            class: "Maintenance",
            action: "getAllExcelContent"
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                var htmls = response.data;
                var tablename = response.tablename;
                $("body").append(htmls);
                exportTableToExcel(tablename);
                $("#" + tablename).remove();
            } else if (response.code == 500) {
                //toastr.warning(response.message);
            } else {
                //toastr.error(response.message);
            }

        }
    });
}

function getTableHtml(dataArray, datatypes) {
    $.ajax({
        url: '/maintenance',
        type: 'POST',
        data: {
            class: "Maintenance",
            action: "getExcelContent",
            dataArray: dataArray,
            type: datatypes
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                var htmls = response.data;
                //purchaseorderexcel
                $("body").append(htmls);
                var tablename = response.tablename;
                exportTableToExcel(tablename);
                $("#" + tablename).remove();
            } else if (response.code == 500) {
                //toastr.warning(response.message);
            } else {
                //toastr.error(response.message);
            }

        }
    });
}

function exportTableToExcel(tableID) {
    var downloadLink;
    var filename = "inventory_hand_table2";
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename ? filename + '.xls' : 'excel_data.xls';

    // Create download link element
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob(blob, filename);
    } else {
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

        // Setting the file name
        downloadLink.download = filename;

        //triggering the function
        downloadLink.click();
    }
}


$(document).on("change", ".cropit-image-input", function () {
    photo_videos = [];
    var fileData = this.files;
    var type = fileData.type;
    var elem = $(this);
    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
        var uploadType = 'image';
        var validSize = '1030';
        var arrayType = validImageTypes;

        if ($.inArray(type, arrayType) < 0) {
            toastr.warning('Please select file with valid extension only!');
        } else {

            if (parseInt(size) > validSize) {
                var validMb = 1;
                toastr.warning('Please select documents less than ' + validMb + ' mb!');
            } else {
                size = isa_convert_bytes_to_specified(value['size'], 'k') + 'kb';
                photo_videos.push(value);
                var src = '';
                var reader = new FileReader();
                $('#photo_video_uploads').html('');


                $(".popup-bg").show();
                elem.next().show();

            }

        }

    });

});


function isa_convert_bytes_to_specified(bytes, to) {
    var formulas = [];
    formulas['k'] = (bytes / 1024).toFixed(1);
    formulas['M'] = (bytes / 1048576).toFixed(1);
    formulas['G'] = (bytes / 1073741824).toFixed(1);
    return formulas[to];
}


$(document).on('click', '.cancelPopup', function () {
    $(this).parents('.add-popup').hide();
});


$(document).on('click', '.pop-add-icon', function () {
    var popup = $(this).attr('data-popup');
    $("#" + popup).show();
});


function addCalanderDate() {
    $(".calander").datepicker({
        dateFormat: jsDateFomat,
    }).datepicker("setDate", new Date());


}


$(document).on("click", ".addSingleData", function () {

    var value = $(this).attr('data-value');
    var fieldValue = $("#" + value).val();
    var table = $(this).attr('data-table');
    var column = $(this).attr('data-cell');
    var select = $(this).attr('data-select');
    var hide = $(this).attr('data-hide');

    var error = $(this).attr('data-error');
    if (fieldValue == "") {

        $("." + error).html('Please fill required field');
    } else {

        $.ajax({
            url: '/maintenance',
            type: 'POST',
            data: {

                "action": 'addSingleData',
                "class": 'Maintenance',
                "table": table,
                "column": column,
                "fieldValue": fieldValue,
            },
            success: function (response) {
                var info = JSON.parse(response);
                if(info.status =='false'){
                    toastr.warning('It is already exist');
                } else {
                    var option = "<option value=" + info.id + ">" + info.value + "</option>";

                    $("#" + select).append(option);
                    $("#" + value).val('');
                    $(this).parents('.add-popup').hide();
                    $("." + hide).hide();
                    $("#" + select).val(info.id);
                    toastr.success('Data Added Successfully.');
                }
            }
        });


    }


});


$(document).on("click", ".addSubcategoryData", function () {

    var value = $(this).attr('data-value');
    var value2 = $(".inventoryCategory").val();
    var fieldValue = $("#" + value).val();
    var table = $(this).attr('data-table');
    var column = $(this).attr('data-cell');
    var column2 = $(this).attr('data-extracell1');
    var select = $(this).attr('data-select');
    var hide = $(this).attr('data-hide');

    var error = $(this).attr('data-error');
    if (fieldValue == "") {

        $("." + error).html('Please fill required field');
    } else {

        $.ajax({
            url: '/maintenance',
            type: 'POST',
            data: {

                "action": 'addDoubleData',
                "class": 'Maintenance',
                "table": table,
                "column": column,
                "column2": column2,
                "fieldValue": fieldValue,
                "value2": value2
            },
            success: function (response) {
                var info = JSON.parse(response);
                var option = "<option value=" + info.id + ">" + info.value + "</option>";

                $("#" + select).append(option);
                $("#" + value).val('');


                $(this).parents('.add-popup').hide();
                $("." + hide).hide();
                $("#" + select).val(info.id);

                toastr.success('Data Added Successfully.');


            }
        });


    }


});


$(document).on("click", ".addbrandData", function () {

    var value = $(this).attr('data-value');
    var value2 = $(".inventoryCategory").val();
    var value3 = $(".subcategory").val();
    var fieldValue = $("#" + value).val();
    var table = $(this).attr('data-table');
    var column = $(this).attr('data-cell');
    var column2 = $(this).attr('data-extracell1');
    var column3 = $(this).attr('data-extracell2');
    var select = $(this).attr('data-select');
    var hide = $(this).attr('data-hide');

    var error = $(this).attr('data-error');
    if (fieldValue == "") {

        $("." + error).html('Please fill required field');
    } else {

        $.ajax({
            url: '/maintenance',
            type: 'POST',
            data: {

                "action": 'addtrippleData',
                "class": 'Maintenance',
                "table": table,
                "column": column,
                "column2": column2,
                "column3": column3,
                "fieldValue": fieldValue,
                "value2": value2,
                "value3": value3
            },
            success: function (response) {
                var info = JSON.parse(response);
                var option = "<option value=" + info.id + ">" + info.value + "</option>";

                $("#" + select).append(option);
                $("#" + value).val('');


                $(this).parents('.add-popup').hide();
                $("." + hide).hide();
                $("#" + select).val(info.id);

                toastr.success('Data Added Successfully.');


            }
        });


    }


});


$(document).on("click", ".addSupplierData", function () {

    var value = $(this).attr('data-value');
    var value2 = $(".inventoryCategory").val();
    var value3 = $(".subcategory").val();
    var value4 = $(".brand").val();
    var fieldValue = $("#" + value).val();
    var table = $(this).attr('data-table');
    var column = $(this).attr('data-cell');
    var column2 = $(this).attr('data-extracell1');
    var column3 = $(this).attr('data-extracell2');
    var column4 = $(this).attr('data-extracell3');
    var select = $(this).attr('data-select');
    var hide = $(this).attr('data-hide');

    var error = $(this).attr('data-error');
    if (fieldValue == "") {

        $("." + error).html('Please fill required field');
    } else {

        $.ajax({
            url: '/maintenance',
            type: 'POST',
            data: {

                "action": 'addquardData',
                "class": 'Maintenance',
                "table": table,
                "column": column,
                "column2": column2,
                "column3": column3,
                "column4": column4,
                "fieldValue": fieldValue,
                "value2": value2,
                "value3": value3,
                "value4": value4
            },
            success: function (response) {
                var info = JSON.parse(response);
                var option = "<option value=" + info.id + ">" + info.value + "</option>";

                $("#" + select).append(option);
                $("#" + value).val('');


                $(this).parents('.add-popup').hide();
                $("." + hide).hide();
                $("#" + select).val(info.id);

                toastr.success('Data Added Successfully.');


            }
        });


    }


});


getInventoryData();

function getInventoryData() {

    $.ajax({
        url: '/maintenance',
        type: 'POST',
        data: {
            "action": 'getInventoryData',
            "class": 'Maintenance'
        },
        success: function (response) {
            var info = JSON.parse(response);
            $('.inventoryCategory').append(info.categoryHtml);
            $('.volume').append(info.volumeHtml);
            $('.property').append(info.property);
            $('.inventorySublocation').append(info.sublocationHtml);
        }
    });

}


$(document).on("change", ".inventoryCategory", function () {

    $(".brand").html('');
    var cat_id = $(this).val();
    $.ajax({
        url: '/maintenance',
        type: 'POST',
        data: {

            "action": 'getSubcategories',
            "class": 'Maintenance',
            "cat_id": cat_id
        },
        success: function (response) {
            var info = JSON.parse(response);

            $('.subcategory').html(info);


        }
    });

});


$(document).on("change", ".subcategory", function () {
    var subCat_id = $(this).val();
    $.ajax({
        url: '/maintenance',
        type: 'POST',
        data: {

            "action": 'getBrands',
            "class": 'Maintenance',
            "subCat_id": subCat_id
        },
        success: function (response) {
            var info = JSON.parse(response);

            $('.brand').html(info);


        }
    });

});


$(document).on("change", ".brand", function () {
    var brand_id = $(this).val();
    $.ajax({
        url: '/maintenance',
        type: 'POST',
        data: {

            "action": 'getSuppliers',
            "class": 'Maintenance',
            "brand_id": brand_id
        },
        success: function (response) {
            var info = JSON.parse(response);

            $('.supplier').html(info);


        }
    });

});


$(document).on("change", ".property", function () {
    var property_id = $(this).val();
    $.ajax({
        url: '/maintenance',
        type: 'POST',
        data: {

            "action": 'getBuilding',
            "class": 'Maintenance',
            "property_id": property_id
        },
        success: function (response) {
            var info = JSON.parse(response);

            $('.building').html(info);


        }
    });

});


$("#addInventory").validate({
    rules: {
        inventoryCategory: {
            required: true
        },
        subcategory: {
            required: true
        },
        brand: {
            required: true
        },
        supplier: {
            required: true
        },
        volume: {
            required: true
        },
        inventorySublocation: {
            required: true
        },
        size: {
            required: true,
            number: true
        },

        property: {
            required: true
        },
        building: {
            required: true
        },
        'item_code[]': {
            required: true
        },
    },
    submitHandler: function (e) {

        var inventory_image = $('.inventory_image').html();
        var inventoryImage = JSON.stringify(inventory_image);
        var form = $('#addInventory')[0];
        var formData = new FormData(form);
        formData.append('action', 'addInventoryData');
        formData.append('class', 'Maintenance');
        formData.append('inventory_image', inventoryImage);

        var custom_field = [];
        $(".custom_field_html input").each(function () {
            var data = {
                'name': $(this).attr('name'),
                'value': $(this).val(),
                'id': $(this).attr('data_id'),
                'is_required': $(this).attr('data_required'),
                'data_type': $(this).attr('data_type'),
                'default_value': $(this).attr('data_value')
            };
            custom_field.push(data);
        });
        var a = JSON.stringify(custom_field);


        formData.append('custom_field', a);


        $.ajax({
            url: '/maintenance',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == "success") {
                    toastr.success('Record Added Successfully');

                    setTimeout(function () {
                        window.location = window.location.origin + "/Maintenance/Inventory";
                    }, 400);

                }
                else
                {
                    toastr.warning(response.message); 
                    return false;
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});


function deleteTableData(tablename, id) {

    $.ajax({
        url: '/editTenant?action=deleteRecords&class=EditTenant&id=' + id + '&tablename=' + tablename,
        type: 'GET',
        success: function (data) {
            var info = JSON.parse(data);
            toastr.success(info.message);

        },
    });

}










          
function editTrackerData(inventory_id){
    $('input.clear-btn[type="button"]').val('Reset');
    $('input.clear-btn[type="button"]').addClass('formreset');
    $('input.clear-btn[type="button"]').removeClass('clearFormReset');
    $.ajax({
        url: '/maintenance',
        type: 'POST',
        data: {

            "action": 'getInventory',
            "class": 'Maintenance',
            "inventory_id": inventory_id
        },
        success: function (response) {
            var info = JSON.parse(response);

            setTimeout(function () {

                $(".inventoryCategory ").val(info.category_id);
                $(".inventoryCategory").trigger('change');


                setTimeout(function () {
                    $(".subcategory").val(info.sub_category_id);
                    $(".subcategory").trigger('change');

                    setTimeout(function () {
                        $(".brand").val(info.brand_id);

                        $(".brand").trigger('change');
                        setTimeout(function () {
                            $(".supplier").val(info.supplier_id);
                        }, 250);

                    }, 200);


                }, 150);
            }, 100);

            setTimeout(function () {
                $(".property").val(info.property_id);
                $(".property").trigger('change');

                $(".building").val(info.building_id);
            }, 200);


            $.each(info, function (key, value) {
                if (value != "") {

                    $('.' + key).val(value);


                }
            });
            defaultFormData = $("#addInventory").serializeArray();
        }
    });
}

function getParameterByName(name) {
    var regexS = "[\\?&]" + name + "=([^&#]*)",
        regex = new RegExp(regexS),
        results = regex.exec(window.location.search);
    if (results == null) {
        return "";
    } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}
jqGridInventory();
function jqGridInventory(status, deleted_at) {
    var id = getParameterByName('id');
    var table = 'warranty_information';
    var columns = ['Key Fixture Name', 'Fixture Type', 'Maintenance Reminder', 'Assessed Age', 'Model', 'Warranty Expiration', 'Insurance Expiration', 'Condition', 'Status', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [{table: 'warranty_information', column: 'fixture_type', primary: 'id', on_table: 'fixture_types'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'property_id', value: property, condition: '='}];
    var extra_columns = ['warranty_information.deleted_at', 'warranty_information.updated_at'];
    var columns_options = [
        {name: 'Key Fixture Name', index: 'key_fixture_name', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Fixture Type', index: 'fixture_type', width: 200, align: "center", searchoptions: {sopt: conditions}, table: 'fixture_types'},
        {name: 'Maintenance Reminder', index: 'maintenance_reminder', company_insurance_typewidth: 200, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'date'},
        {name: 'Assessed Age', index: 'assessed_age', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Model', index: 'model', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Warranty Expiration', index: 'warranty_expiration_date', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'date'},
        {name: 'Insurance Expiration', index: 'insurance_expiration_date', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'date'},
        {name: 'Condition', index: 'condition_fixture', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Status', index: 'status', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: statusFormatter},
        {name: 'Action', index: 'select', title: false, width: 200, align: "center", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table}
    ];
    var ignore_array = [];
    jQuery("#inventory-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            deleted_at: deleted_at,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'warranty_information.updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Fixtures",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 400, drag: true, resize: false} // search options
    );
}