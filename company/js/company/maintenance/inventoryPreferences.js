$(document).ready(function(){

    /*setTimeout(function () {
        $(".disable_edit").trigger('change');
    }, 1000);*/
    function onTop1(rowdata){
        if(rowdata){
            setTimeout(function(){
                jQuery('.table1').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                jQuery('.table1').find('tr:eq(1)').find('td').last().addClass("green_row_right");
            }, 300);
        }
    }

    function onTop2(rowdata){
        if(rowdata){
            setTimeout(function(){
                jQuery('#category_table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                jQuery('#category_table').find('tr:eq(1)').find('td:eq(2)').addClass("green_row_right");
            }, 300);
        }
    }
    function onTop3(rowdata){
        if(rowdata){
            setTimeout(function(){
                jQuery('#category_table1').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                jQuery('#category_table1').find('tr:eq(1)').find('td:eq(3)').addClass("green_row_right");
            }, 300);
        }
    }
    function onTop4(rowdata){
        if(rowdata){
            setTimeout(function(){
                jQuery('#category_table3').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                jQuery('#category_table3').find('tr:eq(1)').find('td:eq(3)').addClass("green_row_right");
            }, 300);
        }
    }
    function onTop7(rowdata){
        if(rowdata){
            setTimeout(function(){
                jQuery('#subLocationTable').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                jQuery('#subLocationTable').find('tr:eq(1)').find('td:eq(3)').addClass("green_row_right");
            }, 300);
        }
    }
    /*  insert into preferences type*/


    $(document).on("click","#save_preferences", function(e) {

        e.preventDefault();

        var inventory_item = 0;
        var add_suppliers = 0;
        var upload_photos = 0;
        if ($('#inventory_item').is(":checked")) {
            inventory_item = '1';   // it is checked
        }
        if ($('#add_suppliers').is(":checked")) {
            add_suppliers = '1';   // it is checked
        }

        if ($('#upload_images').is(":checked")) {
            upload_photos = '1';   // it is checked
        }
        $.ajax({
            type: 'post',
            url: '/MasterData/InventoryTracker-Ajax',
            data: {
                class: 'MaintenanceInventoryTracker',
                action: "insert",
                inventory_item: inventory_item,
                add_suppliers: add_suppliers,
                upload_photos:upload_photos
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {


                    localStorage.setItem("Message", "Preferences Updated Successfully")
                    localStorage.setItem("rowcolor", true)
                    window.location.reload();


                }  else if(response.status == 'error' && response.code == 400){
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#'+key).html(value);
                    });
                }
            },
            error: function (data) {
            }
        });
    });
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');
    function jqGrid(status, deleted_at) {
        var table = 'company_maintenance_subcategory';
        var columns = ['Category','Sub Category', 'Action','editable'];
        var select_column = ['Edit'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_where = [{column:'parent_id',value:'1',condition:'IS NULL'}];
        var extra_columns = [];
        var columns_options = [

            {name:'Category',index:'category', width:370,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Sub Category',index:'category', width:370,searchoptions: {sopt: conditions},table:table,change_type:'line',index2:'sub_category'},
            {name:'Action',index:'', title: false, width:350,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter2},
            {name:'editable',index:'is_editable', hidden:true,searchoptions: {sopt: conditions},table:table},
        ];
        var ignore_array = [];
        jQuery("#category_table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_maintenance_subcategory",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Categories",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:200,left:200,drag:true,resize:false} // search options
        );
    }

    $(document).ready(function(){
        $(document).on("click","#add_unit_cancel_btn",function(){
            $("#add_subcategory_type").hide();
            $("#addUnitTypeButton").show();
        });

        $(document).on("click","#addUnitTypeButton",function(){
            $(this).hide();
            $("#addUnitTypeButton").hide();
            $("#add_subcategory_type").show();
            $("#saveBtns").val("Save");
        });
    });

    /*for multiple property tax id textbox*/
    $(document).on("click", "#multiplsubcategory", function () {
        var clone = $(".mulSubCat:first").clone();
        clone.find("input[name='sub_category']").val('');
        $(".mulSubCat").first().after(clone);
        clone.find("#multiplsubcategory").hide();
        clone.find("#multiplsubcategory1").show();


    });
    /*remove multiple property tax id textbox*/
    $(document).on("click", "#multiplsubcategory1", function () {
        $(this).parents('.mulSubCat').remove();

    });
    /*  insert into category type*/
    $("#add_subcategory_type").validate({
        rules: {
            category: {
                required: true
            },
            sub_category: {
                required: true
            }
        }
    });
    $("#add_supplier_type1").validate({
        rules: {
            sub_category_supp: {
                required: true
            },
            supplier: {
                required: true
            }
        }
    });
    $("#add_subcategory_type4").validate({
        rules: {
            sub_category_brand: {
                required: true
            },
            main_brand: {
                required: true
            }
        }
    });
    $("#add_sublocation_type").validate({
        rules: {
            subLoc_property: {
                required: true
            },
            sub_building: {
                required: true
            },
            sub_location: {
                required: true
            }
        }
    });



    $(document).on("click","#saveBtns", function(e) {
        e.preventDefault();

        /*  var category = $("input[name='category']").val();
          var sub_category = $("input[name='sub_category[]']").val();
  */
        $("#addUnitTypeButton").show();
        if ($('#add_subcategory_type').valid()) {

        var form_data = $('#add_subcategory_type').serializeArray();
        var user_id_hidden10 = $("input[name='user_id_hidden10']").val();
        $.ajax({
            type: 'post',
            url: '/MasterData/InventoryTracker-Ajax',
            data: {
                class: 'MaintenanceInventoryTracker',
                action: "insertcategory",
                form_data: form_data,
                user_id_hidden10: user_id_hidden10
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#category_table').trigger('reloadGrid');
                    $("#add_subcategory_type").trigger("reset");
                    $("#add_subcategory_type").hide();
                    toastr.success(response.message);
                     localStorage.setItem("rowcolor", 'add colour');
                    onTop2(true);


                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).html(value);
                    });
                }
            },
            error: function (data) {
            }
        });
    }

    });
    /*  insert into Supplier type*/

    $(document).on("click","#saveBtnSupplier", function(e){
        e.preventDefault();
        if ($('#add_supplier_type1').valid()) {
            var supplier = $("#add_supplier_type1 input[name='supplier']").val();
            // var sub_category_id = $("#add_supplier_type1 select[name='sub_category_supp']").val();
            var sub_category_id = $('.abcd').val();
            var category_id = $("#add_supplier_type1 select[name='category']").val();
            var user_id_hidden = $("input[name='user_id_hiddens']").val();
            $.ajax({
                type: 'post',
                url: '/MasterData/InventoryTracker-Ajax',
                data: {
                    class: 'MaintenanceInventoryTracker',
                    action: "insertsupplier",
                    supplier: supplier,
                    category_id: category_id,
                    sub_category_id: sub_category_id,
                    user_id_hidden: user_id_hidden


                },
                success: function (response) {

                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {

                        $('#category_table').trigger('reloadGrid');
                        $("#add_supplier_type1").hide();
                        toastr.success("Supplier added successfully.");
                        $('#category_table1').trigger('reloadGrid');
                        $("#addUnitTypeButton2").show();

                        // localStorage.setItem("rowcolor", 'add colour');

                        onTop3(true);

                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('#' + key).html(value);
                        });
                    }
                },
                error: function (data) {
                }
            });
        }

    });
    $(document).on("change", ".form-control .select_options", function (e) {
        e.preventDefault();
        var action = this.value;
        var id = $(this).attr('data_id');

        switch(action) {

            case "Edit":
                //Deactivate or Activate manage user
                $("#add_subcategory_type").show();
                getsubcategoryType(id);
                break;
            case "Deactivate":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                    if (result == true) {
                        update_priority_status(id,action);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            case "Activate":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                    if (result == true) {
                        update_priority_status(id,action);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            case "Delete":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to "+action+" this record?", function (result) {
                    if (result == true) {
                        deletepriorityType(id);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            default:
                window.location.href = base_url+'/Setting/ApexNewUser';
        }

        return false;
    });
    function getcategoryType(id){
        $.ajax({
            type: 'post',
            url: '/MasterData/InventoryTracker-Ajax',
            data: {
                class: "InventoryTracker",
                action: "getpriorityType",
                cuser_id: id
            },
            success: function (response) {

                var response = JSON.parse(response);
                $("#category input[name='priority_type']").val(response.priority);
                $("#sub_category textarea[name='description']").val(response.description);
                localStorage.setItem("Message","priority Updated Successfully")
                localStorage.setItem("rowcolor",true)

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $(document).ready(function(){
        $(document).on("click","#add_unit_cancel_btn1",function(){
            $("#add_supplier_type1").hide();
            $("#addUnitTypeButton2").show();
        });

        $(document).on("click","#addUnitTypeButton2",function(){
            $(this).hide();
            $("#add_supplier_type1").show();
        });
    });

    $(document).ready(function(){
        $(document).on("click","#add_brand_cancel_btn",function(){
            $("#add_subcategory_type4").hide();
            $("#addUnitTypeButton4").show();
        });
    });
    $(document).ready(function(){
        $(document).on("click","#add_unit_cancel_btn2",function(){
            $("#add_sublocation_type").hide();
            $("#addUnitTypeButton3").show();
        });


        $(document).on("click","#addUnitTypeButton3",function(){
            $(this).hide();
            $("#add_sublocation_type").show();
            getPropertyDetails();
        });
        $(document).on("click","#addUnitTypeButton4",function(){
            $(this).hide();
            $("#add_subcategory_type4").show();
        });
    });


    $(document).ready(function(){
        $(document).on("click","#add_unit_cancel_btn",function(){
            $("#add_subcategory_type6").hide();
            $("#addUnitTypeButton6").show();
        });

        $(document).on("click","#addUnitTypeButton6",function(){
            $(this).hide();
            $('#volume').val('');
            $('#user_id_hidden2').val('');
            $('#saveBtnsCat').val('Add')
            $("#add_subcategory_type6").show();
        });
    });
    $(document).ready(function(){
        $(document).on("click","#add_unit_cancel_btn",function(){
            $("#add_subcategory_type7").hide();
            $("#addUnitTypeButton7").show();
        });

        $(document).on("click","#addUnitTypeButton7",function(){
            $(this).hide();
            $("#add_subcategory_type7").show();
        });
    });
    getInventoryData();
    function getInventoryData(){
        $.ajax({
            type: 'post',
            url: '/MasterData/InventoryTracker-Ajax',
            data: {
                class: "InventoryTracker",
                action: "getInventoryData",

            },
            success: function (response) {

                var response = JSON.parse(response);
                var check = response.data.property_type_data;

                if (check.inventory_item == 1) {
                    $("#inventory_item").attr("checked",true);
                }
                if (check.add_suppliers ==1) {
                    $("#add_suppliers").attr("checked",true);
                }

                if (check.upload_photos == 1) {
                    $("#upload_images").attr("checked",true);
                }


            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    /*Manage Volume Tab Starts*/
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid1 ('All')
    function jqGrid1(status) {
        var table = 'list_of_volumes';
        var columns = ['Volume', 'Action'];
        var select_column = ['Edit','Delete'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var columns_options = [
            {name:'Volume',index:'volume', width:690,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'', title: false, width:400,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table},
        ];
        var ignore_array = [];
        jQuery("#manage_volume").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "list_of_volumes",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,

            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Volumes",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:200,left:200,drag:true,resize:false} // search options
        );
    }

    $(document).on("click","#saveBtnsCat", function(e){
        e.preventDefault();
        var volume = $("input[name='volume']").val();
        var user_id_hidden2 = $("input[name='user_id_hidden2']").val();
        $.ajax({
            type: 'post',
            url: '/MasterData/InventoryTracker-Ajax',
            data: {
                class: 'InventoryTracker',
                action: "insertvolume",
                volume: volume,
                user_id_hidden2 : user_id_hidden2


            },
            success: function (response) {

                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    $("#add_subcategory_type6").hide();
                    $("#addUnitTypeButton6").show();
                    $('#manage_volume').trigger( 'reloadGrid' );

                    /*toastr.success("This record saved successfully.");*/
                    toastr.success(response.message);
                    // localStorage.setItem("rowcolor", 'add colour');

                    onTop(true);


                }  else if(response.status == 'error' && response.code == 400){
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#'+key).html(value);
                    });
                }
            },
            error: function (data) {
            }
        });


    });

    /* $(document).on("click","#saveBtnSupplier", function(e){
         e.preventDefault();

         var sub_category = $("input[name='sub_category']").val();






         $.ajax({
             type: 'post',
             url: '/MasterData/InventoryTracker-Ajax',
             data: {
                 class: 'InventoryTracker',
                 action: "updateCategorySupplier",
                 sub_category:sub_category
             },
             success: function (response) {

                 var response = JSON.parse(response);
                 if(response.status == 'success' && response.code == 200){



                 } else if(response.status == 'error' && response.code == 400){

                 }
             },
             error: function (data) {
             }
         });


     });*/

    $(document).on("change", "#manage_volume .select_options", function (e) {
        e.preventDefault();
        var action = this.value;
        var id = $(this).attr('data_id');


        switch(action) {

            case "Edit":
                //Deactivate or Activate manage user
                $("#add_subcategory_type6 #user_id_hidden2").val(id);
                $("#add_subcategory_type6").show();
                $("#addUnitTypeButton6").hide();
                $("#saveBtnsCat").val('Update');
                getVolume(id);
                break;

            case "Delete":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to "+action+" this record?", function (result) {
                    if (result == true) {
                        deletevolume(id);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            default:
                window.location.href = base_url+'/Setting/ApexNewUser';
        }
        $('#manage_volume').trigger('reloadGrid');

        return false;
    });

    function deletevolume(id) {
        $.ajax({
            type: 'post',
            url: '/MasterData/InventoryTracker-Ajax',
            data: {
                class: "InventoryTracker",
                action: "deletevolume",
                user_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);


                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The record deleted successfully.");
                    $('#manage_volume').trigger( 'reloadGrid' );

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    /*    Manage Volume Tab ends*/


    /*Mangage Reason Tab Starts*/
    function statusFmatter1 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            if(rowObject['editable'] == '0')  select = ['Edit'];
            if(rowObject['editable'] == '1')  select = [];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }
    function statusFmatter2 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            if(rowObject['editable'] == '1')  select = ['Edit'];
            if(rowObject['editable'] == '0')  select = [];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid2 ('All')
    function jqGrid2(status) {
        var table = 'list_of_reason';
        var columns = ['Reason', 'Action','editable'];
        var select_column = ['Edit'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var columns_options = [

            {name:'Reason',index:'reason', width:690,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'', title: false, width:400,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},
            {name:'editable',index:'is_editable', hidden:true,searchoptions: {sopt: conditions},table:table}
        ];
        var ignore_array = [];
        jQuery("#manage_reason").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "list_of_reason",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,

            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Reasons",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:200,left:200,drag:true,resize:false} // search options
        );
    }



    $(document).on("click","#saveBtnsMan", function(e){
       
        e.preventDefault();


        var reason = $("input[name='reason']").val();
        var user_id_hidden1 = $("input[name='user_id_hidden1']").val();


        $.ajax({
            type: 'post',
            url: '/MasterData/InventoryTracker-Ajax',
            data: {
                class: 'InventoryTracker',
                action: "insertreason",
                reason: reason,
                user_id_hidden1 : user_id_hidden1


            },
            success: function (response) {

                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 600){
                    toastr.warning(response.message);
                }
                if(response.status == 'success' && response.code == 200){
                    $("#add_subcategory_type7").hide();
                    $("#addUnitTypeButton7").show();
                    $('#manage_reason').trigger( 'reloadGrid' );
                    $('#reason').val('');
                    toastr.success(response.message);
                    // localStorage.setItem("rowcolor", 'add colour');
                    onTop1(true);


                }  else if(response.status == 'error' && response.code == 400){
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#'+key).html(value);
                    });
                }
            },
            error: function (data) {
            }
        });


    });



    $(document).on("change", "#category_table .select_options", function (e) {

        e.preventDefault();
        var action = this.value;
        var id = $(this).attr('data_id');


        switch(action) {

            case "Edit":
                //Deactivate or Activate manage user
                $("#add_subcategory_type #user_id_hidden10").val(id);

                $("#add_subcategory_type").show();
                $("#addUnitTypeButton").hide();
                $("#saveBtns").val('Update');
                $('.clearCategoryForm').hide();
                $('.reset_frm').show();
                getsubcategoryType(id);
                break;

            /*     case "Delete":
                     //Deactivate or Activate manage user
                     bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                         if (result == true) {
                             deleteseverityType(id);
                         } else {
                             window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                         }
                     });
                     break;*/
            default:
                window.location.href = base_url+'/Setting/ApexNewUser';
        }

        return false;
    });
    $(document).on("change", "#manage_reason .select_options", function (e) {

        e.preventDefault();
        var action = this.value;
        var id = $(this).attr('data_id');


        switch(action) {

            case "Edit":
                //Deactivate or Activate manage user
                $("#add_subcategory_type7 #user_id_hidden1").val(id);

                $("#add_subcategory_type7").show();
                $("#saveBtnsMan").val('Update');
                $("#addUnitTypeButton7").hide();

                getReason(id);
                break;

            /*     case "Delete":
                     //Deactivate or Activate manage user
                     bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                         if (result == true) {
                             deleteseverityType(id);
                         } else {
                             window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                         }
                     });
                     break;*/
            default:
                window.location.href = base_url+'/Setting/ApexNewUser';
        }

        return false;
    });
    /*Mangage Reason Tab Ends*/

    /*    Manage supplier Tab starts*/

    $(document).on("click","#addUnitTypeButton2 ", function(e){
        getCategoryies();
    });

    $(document).on("change", "#category.sub_cat_id", function (e) {


        e.preventDefault();
        var id = $(this).val();
            $.ajax({
                type: 'post',
                url: '/MasterData/InventoryTracker-Ajax',
                data: {
                    class: "MaintenanceInventoryTracker",
                    action: "getsupplierSub",
                    id:id


                },
                success: function (response) {
                    var response = JSON.parse(response);
                    var count=response.length;

                    var html="";

                    for(var i=0 ; i<count;i++) {


                        html += ' <option value="' + response[i].id + '">' + response[i].sub_category + '</option>';

                    }

                    $("#add_supplier_type1 #sub_category").html(html);
                    $('select[name="sub_category_supp"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});

                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });

    });

    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid4('All');
    function jqGrid4(status, deleted_at) {
        var table = 'company_maintenance_supplier';
        var columns = ['Category','Sub Category','Supplier','Action','editable'];
        var select_column = ['Edit'];
        var joins = [{table:'company_maintenance_supplier',column:'category_id',primary:'id',on_table:'company_maintenance_subcategory',as:'first'},{table:'company_maintenance_supplier',column:'sub_category_id',primary:'id',on_table:'company_maintenance_subcategory',as:'second'}];//[{table:'users',column:'subscription_plan',primary:'id',on_table:'plans'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [{column:'parent_id',value:'1',condition:'IS NULL'}];
        var columns_options = [

            {name:'Category',index:'category', width:300,align:"center",searchoptions: {sopt: conditions},table:'first'},
            {name:'Sub Category',index:'sub_category', width:300,searchoptions: {sopt: conditions},table:'second'},
            {name:'Supplier',index:'supplier', width:290,searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'', title: false, width:200,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter2},
            {name:'editable',index:'is_editable', hidden:true,searchoptions: {sopt: conditions},table:table},
        ];
        var ignore_array = [];
        jQuery("#category_table1").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_maintenance_supplier",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'company_maintenance_supplier.updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Suppliers",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, /* edit options */
            {}, /* add options */
            {}, /* del options */
            {top:200,left:200,drag:true,resize:false} // search options
        );
    }


    $(document).on("change", "#category.sub_cat_id2 ", function (e) {
        e.preventDefault();
        var id = $(this).val();
        var sub_cat_id = $(this).attr('data_id');

        $.ajax({
            type: 'post',
            url: '/MasterData/InventoryTracker-Ajax',
            data: {
                class: "MaintenanceInventoryTracker",
                action: "getsupplierSub",
                id:id,
                sub_cat_id:sub_cat_id


            },
            success: function (response) {
                var response = JSON.parse(response);
                var responseData = response.data;
                var count = responseData.all.length;
                var selectedIds = responseData.selected;
                var selectedArray = [];
                for(var j = 0; j < selectedIds.length; j++){
                    selectedArray.push(selectedIds[j].sub_category_id);
                }

                var subCatHtml = "";
                for(var i=0 ;i<count ;i++) {

                    if (responseData.all.sub_category != "") {
                        var sub_category_name = responseData.all[i].sub_category;
                        var sub_category_id = responseData.all[i].id;
                        var sub_categoryid = sub_category_id.toString();
                        if (sub_cat_id != ""){
                            if (jQuery.inArray( sub_categoryid, selectedArray ) !== -1 ){
                                subCatHtml += "<option value='" + sub_category_id + "' selected>" + sub_category_name + "</option>";
                            }else{
                                subCatHtml += "<option value='" + sub_category_id + "'>" + sub_category_name + "</option>";
                            }
                        } else{
                            subCatHtml += "<option value='" + sub_category_id + "'>" + sub_category_name + "</option>";
                        }
                    }
                }
                $("#add_supplier_type1 #sub_category_supp").multiselect('destroy');
                $("#add_supplier_type1 #sub_category_supp").attr('multiple','multiple');
                $("#add_supplier_type1 #sub_category_supp").html(subCatHtml);
                $("#add_supplier_type1 #sub_category_supp").multiselect('refresh');

                $(".multiselect-native-select .dropdown-menu").find("li").eq(0).remove();
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });
    $(document).on("change", " #category_table1 .select_options", function (e) {

        e.preventDefault();
        var action = this.value;
        var id = $(this).attr('data_id');

        switch(action) {

            case "Edit":
                getCategoryies();
                $("#add_supplier_type1 #user_id_hiddens").val(id);
                $("#add_supplier_type1").show();

                $("#addUnitTypeButton2").hide();
                $("#saveBtnSupplier").val('Upadate');
                getsupplierType(id);
                break;
            case "Deactivate":
                /*Deactivate or Activate manage user*/
                bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                    if (result == true) {
                        update_priority_status(id,action);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            case "Activate":
                bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                    if (result == true) {
                        update_priority_status(id,action);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            case "Delete":
                bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                    if (result == true) {
                        deletepriorityType(id);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            default:
                window.location.href = base_url+'/Setting/ApexNewUser';
        }

        return false;
    });

    /*    Manage supplier Tab ends*/
    /*Manage Brand Tab Starts*/
    $(document).on("click","#addUnitTypeButton4 ", function(e){

        e.preventDefault();
        $.ajax({
            type: 'post',
            url: '/MasterData/InventoryTracker-Ajax',
            data: {
                class: "MaintenanceInventoryTracker",
                action: "getbrand",

            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log("sdghdg >>> ",response);
                var count=response.length;

                var html="";

                for(var i=0 ; i<count; i++) {
                    if(response[i].sub_category == "") {
                        html += '<option value="' + response[i].id + '">' + response[i].category + '</option>';
                    }
                }
                $("#add_subcategory_type4 #category").html(html).trigger("change");
                $('select[name="sub_category_brand"]').multiselect({
                    includeSelectAllOption: true,
                    nonSelectedText: 'Select'
                });
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });
    $(document).on("change", "#category.subcatbrand ", function (e) {


        e.preventDefault();
        var id = $(this).val();

        $("#category.subcatbrand").attr("data_id",id);
        var sub_cat_id = $(this).attr('data_id');

        $.ajax({
            type: 'post',
            url: '/MasterData/InventoryTracker-Ajax',
            data: {
                class: "MaintenanceInventoryTracker",
                action: "getsupplierBrn",
                id:id,
                sub_cat_id:sub_cat_id


            },
            success: function (response) {
                var response = JSON.parse(response);
                var responseData = response.data;
                var count = responseData.all.length;
                var selectedIds = responseData.selected;
                var selectedArray = [];
                for(var j = 0; j < selectedIds.length; j++){
                    selectedArray.push(selectedIds[j].sub_category_id);
                }

                var subCatHtml = "";
                for(var i=0 ;i<count ;i++) {

                    if (responseData.all.sub_category != "") {
                        var sub_category_name = responseData.all[i].sub_category;
                        var sub_category_id = responseData.all[i].id;
                        var sub_categoryid = sub_category_id.toString();
                        if (sub_cat_id != ""){
                            if (jQuery.inArray( sub_categoryid, selectedArray ) !== -1 ){
                                console.log("shrey");
                                subCatHtml += "<option value='" + sub_category_id + "' selected>" + sub_category_name + "</option>";
                            }else{
                                console.log("hello");
                                subCatHtml += "<option value='" + sub_category_id + "'>" + sub_category_name + "</option>";
                            }
                        } else{
                            subCatHtml += "<option value='" + sub_category_id + "'>" + sub_category_name + "</option>";
                        }
                    }
                }


                $("#add_subcategory_type4 #sub_category_brand").multiselect('destroy');
                $("#add_subcategory_type4 #sub_category_brand").html(subCatHtml);
                $("#add_subcategory_type4 #sub_category_brand").multiselect('refresh');
                $(".multiselect-native-select .dropdown-menu").find("li").eq(0).remove();
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });
    $(document).on("click","#saveBtnsbrand", function(e){
        e.preventDefault();

        if($('#add_subcategory_type4').valid()) {
           // alert('sjbhdjkds');
            var brand = $("#add_subcategory_type4 input[name='main_brand']").val();
            // var sub_category_id = $("#add_supplier_type1 select[name='sub_category_supp']").val();
            var sub_category_id = $('.subcatbrn').val();
            var category_id = $("#add_subcategory_type4 select[name='category']").val();
            var user_id_hidden = $("input[name='user_id_hiddensss2']").val();
            $.ajax({
                type: 'post',
                url: '/MasterData/InventoryTracker-Ajax',
                data: {
                    class: 'MaintenanceInventoryTracker',
                    action: "insertbrand",
                    brand: brand,
                    category_id: category_id,
                    sub_category_id: sub_category_id,
                    user_id_hidden: user_id_hidden


                },
                success: function (response) {

                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {

                        $('#category_table3').trigger('reloadGrid');
                        $("#add_subcategory_type4").hide();
                        toastr.success("Brand added successfully.");
                        $('#addUnitTypeButton4').show();
                        //$('#category_table1').trigger( 'reloadGrid' );
                        // localStorage.setItem("rowcolor", 'add colour');

                        onTop4(true);

                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.' + key).html(value);
                        });
                    }
                },
                error: function (data) {
                }
            });
        }

    });



    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid5('All');
    function jqGrid5(status, deleted_at) {
        var table = 'company_maintenance_brand';
        var columns = ['Category','Sub Category','Brand','Action','editable'];
        var select_column = ['Edit'];
        var joins = [{table:'company_maintenance_brand',column:'category_id',primary:'id',on_table:'company_maintenance_subcategory',as:'first'},{table:'company_maintenance_brand',column:'sub_category_id',primary:'id',on_table:'company_maintenance_subcategory',as:'second'}];//[{table:'users',column:'subscription_plan',primary:'id',on_table:'plans'}];//[{table:'users',column:'subscription_plan',primary:'id',on_table:'plans'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [{column:'parent_id',value:'1',condition:'IS NULL'}];
        var columns_options = [

            {name:'Category',index:'category', width:280,align:"center",searchoptions: {sopt: conditions},table:'first'},
            {name:'Sub Category',index:'sub_category', width:280, searchoptions: {sopt: conditions},table:'second'},
            {name:'Brand',index:'brand', width:280,searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'', title: false, width:250,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter2},
            {name:'editable',index:'is_editable', hidden:true,searchoptions: {sopt: conditions},table:table},
        ];
        var ignore_array = [];
        jQuery("#category_table3").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_maintenance_brand",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'company_maintenance_brand.updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Brands",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, /* edit options */
            {}, /* add options */
            {}, /* del options */
            {top:200,left:200,drag:true,resize:false} // search options
        );
    }


    $(document).on("change", " #category_table3 .select_options", function (e) {

        e.preventDefault();
        var action = this.value;
        var id = $(this).attr('data_id');

        switch(action) {

            case "Edit":
                getCategoryies2();
                $("#add_subcategory_type4 #user_id_hiddensss2").val(id);
                $("#add_subcategory_type4").show();
                $("#addUnitTypeButton4").hide();
                $("#saveBtnsbrand").val('Update');
                getsupplierType2(id);
                break;
            case "Deactivate":
                /*Deactivate or Activate manage user*/
                bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                    if (result == true) {
                        update_priority_status(id,action);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            case "Activate":
                bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                    if (result == true) {
                        update_priority_status(id,action);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            case "Delete":
                bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                    if (result == true) {
                        deletepriorityType(id);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            default:
                window.location.href = base_url+'/Setting/ApexNewUser';
        }

        return false;
    });
    /*Manage Brand Tab Ends*/


function getCategoryies(){

    $.ajax({
        type: 'post',
        url: '/MasterData/InventoryTracker-Ajax',
        data: {
            class: "MaintenanceInventoryTracker",
            action: "getsupplier",

        },
        success: function (response) {
            var response = JSON.parse(response);
            console.log(response);
            var count=response.length;
            var html="";

            for(var i=0 ; i<count;i++) {

                if(response[i].sub_category == "") {
                    html += ' <option value="' + response[i].id + '">' + response[i].category + '</option>';
                }
            }
            $("#add_supplier_type1 #category").html(html);

          //  $('.sub_cat_id2').trigger('change');
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
function getCategoryies2(){

    $.ajax({
        type: 'post',
        url: '/MasterData/InventoryTracker-Ajax',
        data: {
            class: "MaintenanceInventoryTracker",
            action: "getsupplier",

        },
        success: function (response) {
            var response = JSON.parse(response);
            var count=response.length;
            var html="";

            for(var i=0 ; i<count;i++) {

                if(response[i].sub_category == "") {
                    html += ' <option value="' + response[i].id + '">' + response[i].category + '</option>';
                }
            }
            $("#add_subcategory_type4 #category").html(html);
            $('.subcatbrn').trigger('change');
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
function getsupplierType2(id){
    $("#category.subcatbrand").attr("data_id",id);
    $.ajax({
        type: 'post',
        url: '/MasterData/InventoryTracker-Ajax',
        data: {
            class: "MaintenanceInventoryTracker",
            action: "getsupplierType2",
            id: id

        },
        success: function (response) {

            var response = JSON.parse(response);
            var responseData = response.data;
           console.log('responseData>>', responseData.selected);
            $('.brand_frm').hide();
            $('.resetbrandform').show();
            var count=(responseData.all.length);
            var selectedArray  = responseData.selected;
            var subCatHtml = "";
            for(var i=0 ;i<count ;i++) {
                if (responseData.selected[i] != undefined && responseData.selected[i].category_id != "") {
                    var cat_id = responseData.selected[i].category_id;
                    $("#add_subcategory_type4 select[name='sub_category_brand'] option[value='" + cat_id + "']").attr('selected', 'selected');
                    $('.subcatbrand').trigger('change');
                    $("#add_subcategory_type4 input[name='sub_category_brand']").val(responseData.selected[i].brand);
                }
            }

            /*       if(responseData.all[i].sub_category != "") {

                        var sub_category_id=responseData.all[i];
                        var sub_category=responseData.selected[i];
                        var sub_category_name=responseData.all[i].sub_category;


                        var parent_id=responseData.all[i].parent_id;

                        if(jQuery.inArray("sub_category_id", selectedArray) != -1) {
                            console.log("111");
                            subCatHtml += "<option value='"+sub_category+"' selected>"+sub_category_name+"</option>";
                        } else {
                            console.log("222");
                            subCatHtml += "<option value='"+sub_category_id+"'>"+sub_category_name+"</option>";
                        }

                    }*/

            /*       $("#sub_category_supp").html(subCatHtml);
                   $("#sub_category_supp").multiselect("refresh");
   */


        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

}
$(document).on("change", "#category.subcatbrand ", function (e) {


    e.preventDefault();
    var id = $(this).val();
    var sub_cat_id = $(this).attr('data_id');

    $.ajax({
        type: 'post',
        url: '/MasterData/InventoryTracker-Ajax',
        data: {
            class: "MaintenanceInventoryTracker",
            action: "getsupplierSub2",
            sub_cat_id:sub_cat_id,
            id:id


        },
        success: function (response) {
            var response = JSON.parse(response);
            var responseData = response.data;
            var count = responseData.all.length;
            var selectedIds = responseData.selected;
            var selectedArray = [];
            for(var j = 0; j < selectedIds.length; j++){
                selectedArray.push(selectedIds[j].sub_category_id);
            }

            var subCatHtml = "";
            for(var i=0 ;i<count ;i++) {

                if (responseData.all.sub_category != "") {
                    var sub_category_name = responseData.all[i].sub_category;
                    var sub_category_id = responseData.all[i].id;
                    var sub_categoryid = sub_category_id.toString();
                    if (sub_cat_id != ""){
                        if (jQuery.inArray( sub_categoryid, selectedArray ) !== -1 ){
                            subCatHtml += "<option value='" + sub_category_id + "' selected>" + sub_category_name + "</option>";
                        }else{
                            subCatHtml += "<option value='" + sub_category_id + "'>" + sub_category_name + "</option>";
                        }
                    } else{
                        subCatHtml += "<option value='" + sub_category_id + "'>" + sub_category_name + "</option>";
                    }
                }
            }
            $("#add_subcategory_type4 #sub_category_brand").multiselect('destroy');
            $("#add_subcategory_type4 #sub_category_brand").html(subCatHtml);
            $("#add_subcategory_type4 #sub_category_brand").multiselect('refresh');
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

});

$(document).on("click",".clearForm",function(){
    var formID = $(this).attr('formid');
    resetFormClear('#'+formID,[], 'form',false);

    if(formID=='add_supplier_type1' || formID == 'add_subcategory_type4'){
        setTimeout(function () {
            $(".disable_edit").trigger('change');
        }, 600);

    }


});

$(document).on('click','.resetcategoryfrm',function () {
    var id =  $("#add_subcategory_type  #user_id_hidden10").val();
    getsubcategoryType(id);
});

$(document).on('click','.ResetSupplierForm',function () {
    var id =  $("#add_supplier_type1  #user_id_hiddens").val();
    getsupplierType(id);
});

$(document).on('click','.resetbrandform',function () {
  var id =  $("#add_subcategory_type4 #user_id_hiddensss2").val();
    getsupplierType2(id);
});

$(document).on('click','.reset_vol_frm',function () {
    var id =  $("#add_subcategory_type6 #user_id_hidden2").val();
    getVolume(id);
});

    $(document).on('click','.reset_subCate_frm',function () {
        var id =  $("#add_sublocation_type #user_id_hiddenLoction").val();
        getSubLoc(id);
    });


$(document).on('click','.resetreason_frm',function () {
    var id =  $("#add_subcategory_type7 #user_id_hidden1").val();
    getReason(id);
});

function getsubcategoryType(id){
    $.ajax({
        type: 'post',
        url: '/MasterData/InventoryTracker-Ajax',
        data: {
            class: "MaintenanceInventoryTracker",
            action: "getsubcategoryType",
            cuser_id: id
        },
        success: function (response) {
            $("#add_subcategory_type .mulSubCatClone").remove();
            var response = JSON.parse(response);
            console.log(response);
            var n=response.length;
            $('.catfrm').hide();
            $('.resetcategoryfrm').show();
            for(var i=0 ; i<n ; i++){
                if(i==0) {
                    $("#add_subcategory_type input[name='category']").val(response[i].category);
                    if(response[i].sub_category == ''){
                        $("#add_subcategory_type input[name='sub_category']").val('');
                    }else{
                        $("#add_subcategory_type input[name='sub_category']").val(response[i].sub_category);
                    }
                }else{
                    if(i== 1){
                        $("#add_subcategory_type input[name='sub_category']").val(response[i].sub_category);
                        $("#add_subcategory_type input[name='sub_cat_id']").val(response[i].id);
                    }else{
                        var html='<div class="col-xs-12 col-sm-4 mulSubCat mulSubCatClone">';
                        html +='<label>Sub Category<a class="pop-add-icon" id="multiplsubcategory" name="multiplsubcategory" href="javascript:;" style="display: none;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>';
                        html +='<a class="pop-minus-icon" id="multiplsubcategory1" name="multiplsubcategory1" href="javascript:;" style="display: inline;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                        html +='</label>';
                        html +='<input name="sub_category" id="sub_category" value="'+response[i].sub_category+'" maxlength="500" class="form-control">';
                        html +='<input type="hidden" id="sub_cat_id" name="sub_cat_id" value="'+response[i].id+'">';
                        html +='</div> ';
                        $("#add_subcategory_type .mulSubCat").last().after(html);
                    }


                }
            }

            //$("#add_subcategory_type input[name='category']").val(response[0].category);

            // $("#add_subcategory_type input[name='multiplsubcategory1']").val(response.multiplsubcategory1);
            // $("#add_subcategory_type input[name='multiplsubcategory1']").val(response.multiplsubcategory1);


        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function getsupplierType(id){
    $("#category.sub_cat_id2").attr("data_id",id);
    $.ajax({
        type: 'post',
        url: '/MasterData/InventoryTracker-Ajax',
        data: {
            class: "MaintenanceInventoryTracker",
            action: "getsupplierType",
            id: id

        },
        success: function (response) {

            var response = JSON.parse(response);
            var responseData = response.data;
            console.log('responseData>>', responseData);

            $('.supplier_frm').hide();
            $('.ResetSupplierForm').show();
            var count=(responseData.all.length);
            var selectedArray  = responseData.selected;
            var subCatHtml = "";
            for(var i=0 ;i<count ;i++) {


                if (responseData.selected[i] != undefined && responseData.selected[i].category_id != "") {
                    var cat_id = responseData.selected[i].category_id;
                    $("#add_supplier_type1 select[name='category'] option[value='" + cat_id + "']").attr('selected', 'selected');
                    $('.sub_cat_id2').trigger('change');
                    $("#add_supplier_type1 input[name='supplier']").val(responseData.selected[i].supplier);

                }
            }




            /*       if(responseData.all[i].sub_category != "") {

                        var sub_category_id=responseData.all[i];
                        var sub_category=responseData.selected[i];
                        var sub_category_name=responseData.all[i].sub_category;


                        var parent_id=responseData.all[i].parent_id;

                        if(jQuery.inArray("sub_category_id", selectedArray) != -1) {
                            console.log("111");
                            subCatHtml += "<option value='"+sub_category+"' selected>"+sub_category_name+"</option>";
                        } else {
                            console.log("222");
                            subCatHtml += "<option value='"+sub_category_id+"'>"+sub_category_name+"</option>";
                        }

                    }*/

            /*       $("#sub_category_supp").html(subCatHtml);
                   $("#sub_category_supp").multiselect("refresh");
   */


        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

}

function getVolume(id){
    $.ajax({
        type: 'post',
        url: '/MasterData/InventoryTracker-Ajax',
        data: {
            class: "InventoryTracker",
            action: "getVolume",
            cuser_id: id
        },
        success: function (response) {

            var response = JSON.parse(response);
            $('.reset_vol_frm').show();
            $('.volform').hide();
            $("#add_subcategory_type6 input[name='volume']").val(response.volume);
            // localStorage.setItem("Message","Volume Updated Successfully")
            // localStorage.setItem("rowcolor",true)

        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
function getReason(id){
    $.ajax({
        type: 'post',
        url: '/MasterData/InventoryTracker-Ajax',
        data: {
            class: "InventoryTracker",
            action: "getReason",
            cuser_id: id
        },
        success: function (response) {

            var response = JSON.parse(response);
            $('.resetreason_frm').show();
            $('.reasonform').hide();
            $("#add_subcategory_type7 input[name='reason']").val(response.reason);

            // localStorage.setItem("Message","Volume Updated Successfully")
            // localStorage.setItem("rowcolor",true)

        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

    $(document).on('click','.clearPreferences',function () {
        bootbox.confirm("Do you want to clear this form?", function (result) {
            if (result == true) {
                location.reload();
            }
        });
    });

    $(document).on("click","#saveBtnSubLoc", function(e) {
        e.preventDefault();
        if ($('#add_sublocation_type').valid()) {

                var form_data = $('#add_sublocation_type').serializeArray();
                var user_id_hiddenLoction = $("input[name='user_id_hiddenLoction']").val();


                $.ajax({
                    type: 'post',
                    url: '/MasterData/InventoryTracker-Ajax',
                    data: {
                        class: 'MaintenanceInventoryTracker',
                        action: "insertLocation",
                        form_data: form_data,
                        user_id_hiddenLoction: user_id_hiddenLoction


                    },
                    success: function (response) {

                        var response = JSON.parse(response);
                        if (response.status == 'success' && response.code == 200) {

                            $('#subLocationTable').trigger('reloadGrid');
                           $("#add_sublocation_type").hide();
                           $("#addUnitTypeButton3").show();
                            toastr.success(response.message);
                            // localStorage.setItem("rowcolor", 'add colour');
                            onTop7(true);

                        } else if (response.status == 'error' && response.code == 400) {
                            $('.error').html('');
                            $.each(response.data, function (key, value) {
                                $('#' + key).html(value);
                            });
                        }
                    },
                    error: function (data) {
                    }
                });

        }
    });

    function getPropertyDetails() {
        $.ajax({
            type: 'GET',
            url: '/MasterData/InventoryTracker-Ajax',
            data: {
                class: "InventoryTracker",
                action:"getPropertyListing"
            },
            success: function (response) {
                var html="";
                var response = JSON.parse(response);
                for (var i = 0; i < response.data.propertyDetails.length; i++) {
                    html += ' <option value="' + response.data.propertyDetails[i].id + '" data-id="' + response.data.propertyDetails[i].id + '">' + response.data.propertyDetails[i].property_name + '</option>';
                }
                $("#add_sublocation_type #subLoc_property").html("<option value=''>Select</option>" + html);
                /*$("#selectNewPopup #property_list_inspection").html("<option value=''>Select</option>" + html);*/
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("change","#add_sublocation_type #subLoc_property",function(){
        var conceptName = $('#add_sublocation_type #subLoc_property').find(":selected").text();
        var conceptId = $('#add_sublocation_type #subLoc_property').find(":selected").attr("data-id");
        getBuildName(conceptName,conceptId);
    });

    function getBuildName(conceptName,conceptId) {
        var property_id = conceptId;

        $.ajax({
            type: 'POST',
            url: '/MasterData/InventoryTracker-Ajax',
            data: {
                class: "InventoryTracker",
                action: "getBuildinglisting",
                id:property_id
            },
            success: function (response) {
                var html = ""
                var response = JSON.parse(response);
                if(response.data.length > 0) {
                    for (var i = 0; i < response.data.length; i++) {
                        html += ' <option value="' + response.data[i].id + '" data-id="' + response.data[i].id + '">' + response.data[i].building_name + '</option>';
                    }
                    $("#add_sublocation_type #sub_building").html("<option value=''>Select</option>" + html);
                }else {
                    $("#add_sublocation_type #sub_building").html("<option value=''>Select</option>");
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    /*jqGrid status*/
    $(document).on("change", "#collapseFour #jqgridOptions", function (e) {
        var status = $(this).val();
        $('#subLocationTable').jqGrid('GridUnload');
        jqGridSubLoc(status,true);

    });
/* jqGrid Initialization function
    * @param status
    */
    jqGridSubLoc('All');
    function jqGridSubLoc(status,deleted_at) {
        var table = 'maintainence_sub_loc';
        var columns = ['Property','Building','SubLocation','Action','Status'];
        /*var select_column = ['Edit','Deactivate','Delete'];*/
        if (status === 0) {
            var select_column = ['Edit','Deactivate','Delete'];
        }else {
            var select_column = ['Edit','Activate','Delete'];
        }

        var joins = [{table:'maintainence_sub_loc',column:'property_id',primary:'id',on_table:'general_property'},{table:'maintainence_sub_loc',column:'building_id',primary:'id',on_table:'building_detail'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['maintainence_sub_loc.deleted_at','maintainence_sub_loc.updated_at'];
       // var extra_where = [];
        var columns_options = [

            {name:'Property',index:'property_name', width:300,align:"center",searchoptions: {sopt: conditions},table:'general_property'},
            {name:'Building',index:'building_name', width:300, searchoptions: {sopt: conditions},table:'building_detail'},
            {name:'SubLocation',index:'sub_location', width:290,searchoptions: {sopt: conditions},table:table},
            /*{name:'Action',index:'',width:200,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, hidden:false,formatter: 'select', edittype: 'select',search:false,table:table,title:false},*/
            {name:'Action',index:'', title: false, width:200,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter78},
            {name:'Status',index:'status', hidden:true,searchoptions: {sopt: conditions},table:table},
        ];
        var ignore_array = [];
        jQuery("#subLocationTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of SubLocation",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, /* edit options*/
            {}, /* add options*/
            {}, /*del options*/
            {top: 200, left: 400, drag: true, resize: false} /* search options*/
        );
    }


    $(document).on("change", "#subLocationTable .select_options", function () {
        var id = $(this).attr('data_id');
        var action = $(this).val();
        if (action == "Delete") {
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        deleteTableData(id);
                        $('#subLocationTable').trigger('reloadGrid');

                    }

                }
            });
        }else if(action == "Deactivate") {
            bootbox.confirm("Do you want to "+action+" this record?", function (result) {
                if (result == true) {
                    update_subloc_status(id,action);
                }
            });
        }else if(action == "Activate"){
            bootbox.confirm("Do you want to "+action+" this record?", function (result) {
                if (result == true) {
                    update_subloc_status(id,action);
                }
            });
        }
        else if(action == "Edit") {
            $("#add_sublocation_type #user_id_hiddenLoction").val(id);
            $("#add_sublocation_type").show();
            $("#saveBtnSubLoc").val("Update");
               getSubLoc(id);
               getPropertyDetails();
        }

    });

    function deleteTableData(id) {

        $.ajax({
            type: 'POST',
            url: '/MasterData/InventoryTracker-Ajax',
            data: {
                class: "InventoryTracker",
                action: 'deletesubLoc',
                id: id
            },
            success : function(response){
                var response =  JSON.parse(response);
                var tagKeyData = response.data;
                console.log(response);
                if(response.status == 'success' && response.code == 200) {
                    toastr.success('Record Deleted Successfully');
                }else if(response.status == 'error' && response.code == 503) {
                    toastr.error(response.message);
                }else {

                }
            }
        });


    }

    function update_subloc_status(id,action) {
        $.ajax({
            type: 'post',
            url: '/MasterData/InventoryTracker-Ajax',
            data: {
                class: "InventoryTracker",
                action: "updateSublocStatus",
                user_id: id,
                status_type: action,

            },
            success: function (response) {
                var response = JSON.parse(response);
                var responseData = response.data;

                if (response.status == 'success' && response.code == 200) {

                    if(responseData.status == '1') {
                        toastr.success("Record Activated Successfully.");
                    }
                    else{
                        toastr.success("Record Deactivated Successfully.");
                    }
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                else if(response.status == 'error' && response.code == 503){
                    toastr.error("A default value cannot be deactivated");
                }
                localStorage.setItem("Message","Record Updated Successfully")
                localStorage.setItem("rowcolor",true)
                $('#subLocationTable').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    function statusFormatterSubloc (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Active";
        else if(cellValue == '0')
            return "Inactive";
        else
            return '';
    }

        function statusFmatter78 (cellvalue, options, rowObject){
            if(rowObject !== undefined) {
                var select = '';

                if(rowObject['Status'] == '1')  select = ['Edit','Deactivate','Delete'];
                if(rowObject['Status'] == '0')  select = ['Edit','Activate','Delete'];

                var data = '';
                if(select != '') {
                    var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                    $.each(select, function (key, val) {
                        data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                    });
                    data += '</select>';
                }
                return data;
            }
        }


    function getSubLoc(id){
        $.ajax({
            type: 'post',
            url: '/MasterData/InventoryTracker-Ajax',
            data: {
                class: "InventoryTracker",
                action: "getSubLoc",
                cuser_id: id
            },
            success: function (response) {

                var response = JSON.parse(response);
                $('.reset_subCate_frm').show();
                $('.subCateform').hide();
                setTimeout(function(){
                    $("#subLoc_property").val(response.property_id);
                    var conceptName = $('#add_sublocation_type #subLoc_property').find(":selected").text();
                    var conceptId = $('#add_sublocation_type #subLoc_property').find(":selected").attr("data-id");
                    getBuildName(conceptName,conceptId);
                    $("#sub_building").val(response.building_id);
                    $("#sub_location").val(response.sub_location);
                    }, 1000);

 
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

});