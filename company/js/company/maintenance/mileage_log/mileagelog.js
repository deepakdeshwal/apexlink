
  $(document).ready(function () {

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
     var y = today.getFullYear();
      var year = '';
      for (var i = y; i > y-101; i--)
      {
          year += "<option value='"+i+"'>"+i+"</option>";
      }

      $("select[name='year_vehicle']").html(year);


    vehiclelog();
    function vehiclelog() {

    var d=today;
    var table = 'mielage_log';
    var columns = ['Vehicle Name', 'Date', 'Driver','start_location','final_reading','Journey Details (To - From)','Odometer Reading (Start - Stop Reading)','Number of Miles','Action'];
    var select_column = ['Edit','Delete',];
    var conditions = ["eq","bw","ew","cn","in"];
     var extra_where = [{column:'type', value :'C', condition:'='}];
    var extra_columns = [];
    var joins = [];
    var columns_options = [
        {name:'Vehicle Name',index:'vehicle_name', width:100,hidden:false,searchoptions: {sopt: conditions},table:table},
        {name:'Date',index:'date', width:100,hidden:false,searchoptions: {sopt: conditions},table:table},

        {name:'Driver',index:'driver', width:100,searchoptions: {sopt: conditions},table:table},
        {name:'start_location',index:'start_location',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:'mielage_log'},
        {name:'final_reading',index:'final_reading',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:'mielage_log'},
        {name:'Journey Details (To - From)',index:'end_location', width:100,hidden:false,searchoptions: {sopt: conditions},table:table,change_type: 'combine_column_hyphen2', extra_columns: ['end_location', 'start_location'],original_index: 'end_location'},
        {name:'Odometer Reading (Start - Stop Reading)',index:'initial_reading', width:300,searchoptions: {sopt: conditions},table:table,change_type: 'combine_column_hyphen2', extra_columns: ['initial_reading', 'final_reading'],original_index: 'initial_reading'},
        {name:'Number of Miles',index:'mileage', width:100,hidden:false,searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},

    ];

    var ignore_array = [];
    jQuery("#vehicle_log").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:'true'

        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Vehicle Mileage Log",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
};


    function vehiclelog1(idd = "", searchValue = "") {

        var d=today;
        var hidden_date=$('#hidden-date').val();
        console.log("test="+idd);
        var radio=$("#raddioo").serializeArray();
        var radio_check=radio[0].value;

        var table = 'mielage_log';
        var columns = ['Vehicle Name', 'Date', 'Driver','start_location','final_reading','Journey Details (To - From)','Odometer Reading (Start - Stop Reading)','Number of Miles','Action'];
        var select_column = ['Edit','Delete',];
        var conditions = ["eq","bw","ew","cn","in"];
       // var extra_where = [{column:'type', value :'C', condition:'='}];


        if((radio_check=="week")) {
            if (idd == "") {
                if (searchValue == "0" || searchValue == "1") {
                    var extra_where = [{column: 'type', value: 'C', condition: '='}];
                } else {
                    if (searchValue == "2") {
                        console.log("here");

                        var d1 = $.datepicker.parseDate('mm/dd/yy', hidden_date);
                        var mysqlDate1 =d1.getFullYear()+"-"+(d1.getMonth()+1)+"-"+d1.getDate();

                        var d2 = $.datepicker.parseDate('mm/dd/yy', hidden_date);
                        var mysqlDate2 =d2.getFullYear()+"-"+(d2.getMonth()+1)+"-"+(d2.getDate()-15);

                        console.log(mysqlDate1, mysqlDate2);
                        var extra_where = [{column: 'type', value: 'C', condition: '='}, {column: 'date', value: mysqlDate1, condition: '<'}, {column: 'date', value: mysqlDate2, condition: '>'}];
                    } else if(searchValue == "3"){

                        var monthyear=$(".calander1").val();
                        var chnged_date1 = monthyear+"-"+"1";
                        var chnged_date2 = monthyear+"-"+"31";
                        var extra_where = [{column: 'type', value: 'C', condition: '='}, {column: 'date', value: chnged_date1, condition: '>'}, {column: 'date', value: chnged_date2, condition: '<='}];
                    }
                }
            } else {
                if (idd !== "" && searchValue == "0") {
                    var extra_where = [{column: 'type', value: 'C', condition: '='}, {column: 'user_id', value: idd, condition: '='}];
                } else if (idd !== "" && searchValue == "1") {
                    var extra_where = [{column: 'type', value: 'C', condition: '='}, {column: 'user_id', value: idd, condition: '='}];
                } else if (idd !== "" && searchValue == "2") {

                    var d1 = $.datepicker.parseDate('mm/dd/yy', hidden_date);
                    var mysqlDate1 =d1.getFullYear()+"-"+(d1.getMonth()+1)+"-"+d1.getDate();

                    var d2 = $.datepicker.parseDate('mm/dd/yy', hidden_date);
                    var mysqlDate2 =d2.getFullYear()+"-"+(d2.getMonth()+1)+"-"+(d2.getDate()-15);

                    console.log(mysqlDate1, mysqlDate2);
                    var extra_where = [{column: 'type', value: 'C', condition: '='}, {column: 'user_id', value: idd, condition: '='}, {column: 'date', value: mysqlDate1, condition: '<'}, {column: 'date', value: mysqlDate2, condition: '>'}];
                } else if (idd != "" && searchValue == "3") {
                    var monthyear=$(".calander1").val();
                    var chnged_date1 = monthyear+"-"+"1";
                    var chnged_date2 = monthyear+"-"+"31";
                    var extra_where = [{column: 'type', value: 'C', condition: '='},{column: 'user_id', value: idd, condition: '='}, {column: 'date', value: chnged_date1, condition: '>'}, {column: 'date', value: chnged_date2, condition: '<='}];

                }
            }
        }
        else if(radio_check=="date"){
            var date1=$('#date-range1').val();
            var d1 = $.datepicker.parseDate('mm/dd/yy', date1);
            var mysqlDate1 =d1.getFullYear()+"-"+(d1.getMonth()+1)+"-"+d1.getDate();
            console.log(mysqlDate1);

            var date2=$('#date-range2').val();
            var d2 = $.datepicker.parseDate('mm/dd/yy', date2);
            var mysqlDate2 =d2.getFullYear()+"-"+(d2.getMonth()+1)+"-"+d2.getDate();
            if (idd == ""){
            var extra_where = [{column:'type', value :'C', condition:'='},{column:'date', value :mysqlDate1, condition:'>'},{column:'date', value :mysqlDate2, condition:'<'}];
            }

            else if (idd != ""){

                var extra_where = [{column:'type', value :'C', condition:'='},{column: 'user_id', value: idd, condition: '='},{column:'date', value :mysqlDate1, condition:'>'},{column:'date', value :mysqlDate2, condition:'<'}];

            }

        }


        var extra_columns = [];
        var joins = [];
        var columns_options = [
            {name:'Vehicle Name',index:'vehicle_name', width:100,hidden:false,searchoptions: {sopt: conditions},table:table},
            {name:'Date',index:'date', width:100,hidden:false,searchoptions: {sopt: conditions},table:table},
            {name:'Driver',index:'driver', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'start_location',index:'start_location',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:'mielage_log'},
            {name:'final_reading',index:'final_reading',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:'mielage_log'},
            {name:'Journey Details (To - From)',index:'end_location', width:100,hidden:false,searchoptions: {sopt: conditions},table:table,change_type: 'combine_column_hyphen2', extra_columns: ['end_location', 'start_location'],original_index: 'end_location'},
            {name:'Odometer Reading (Start - Stop Reading)',index:'initial_reading', width:300,hidden:false,searchoptions: {sopt: conditions},table:table,change_type: 'combine_column_hyphen2', extra_columns: ['initial_reading', 'final_reading'],original_index: 'initial_reading'},
            {name:'Number of Miles',index:'mileage', width:100,hidden:false,searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},

        ];

        var ignore_array = [];
        jQuery("#vehicle_log").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'true'

            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Vehicle Mileage Log",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:200,drag:true,resize:false} // search options
        );
    };



function statusFmatter1 (cellvalue, options, rowObject){
    if(rowObject !== undefined) {

        var select = ['Edit','Delete'];

        var data = '';

        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }

        return data;
    }
}

//
    emplog();
   function emplog() {
       var d=today;
    var table = 'mielage_log';
    var columns = ['Employee Full Name', 'Employee ID', 'Vehicle','start_location','final_reading','Journey Details (To - From)','Date','Odometer Reading (Start - Stop Reading)','Number of Miles','Action'];
    var select_column = ['Edit','Delete'];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'type', value :'E', condition:'='}];
    var extra_columns = [];
    var joins = [];
    var columns_options = [
        {name:'Employee Full Name',index:'emp_name', width:100,hidden:false,searchoptions: {sopt: conditions},table:table},
        {name:'Employee ID',index:'user_id', width:100,searchoptions: {sopt: conditions},table:table},
        {name:'Vehicle',index:'vehicle_name', width:100,searchoptions: {sopt: conditions},table:table,},
        {name:'start_location',index:'start_location',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:'mielage_log'},
        {name:'final_reading',index:'final_reading',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:'mielage_log'},
        {name:'Journey Details (To - From)',index:'end_location', width:300,hidden:false,searchoptions: {sopt: conditions},table:table, change_type: 'combine_column_hyphen2', extra_columns: ['end_location', 'start_location'],original_index: 'end_location'},
        {name:'Date',index:'date', width:200,hidden:false,searchoptions: {sopt: conditions},table:table},
        {name:'Odometer Reading (Start - Stop Reading)',index:'initial_reading', width:300,hidden:false,searchoptions: {sopt: conditions},table:table,change_type: 'combine_column_hyphen2', extra_columns: ['initial_reading', 'final_reading'],original_index: 'initial_reading'},
        {name:'Number of Miles',index:'mileage', width:100,hidden:false,searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'', title: false, width:100,align:"right",sortable:false,cellEdit: true, editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},
    ];

    var ignore_array = [];
    jQuery("#employee_log").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Employee Mileage Log",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}



    function emplog1(idd = "", searchValue = "") {
     var hidden_date=$('#hidden-date').val() ;
       console.log("test="+idd);
       var radio=$("#radioo").serializeArray();
       var radio_check=radio[0].value;

        var d=today;
        var table = 'mielage_log';
        var columns = ['Employee Full Name', 'Employee ID', 'Vehicle','start_location','final_reading','Journey Details (To - From)','Date','Odometer Reading (Start - Stop Reading)','Number of Miles','Action'];
        var select_column = ['Edit','Delete'];
        var conditions = ["eq","bw","ew","cn","in"];
if((radio_check=="weekk")) {
    if (idd == "") {
        if (searchValue == "0" || searchValue == "1") {
            var extra_where = [{column: 'type', value: 'E', condition: '='}];
        } else {
            if (searchValue == "2"){
                console.log("here");

                var d1 = $.datepicker.parseDate('mm/dd/yy', hidden_date);
                var mysqlDate1 =d1.getFullYear()+"-"+(d1.getMonth()+1)+"-"+d1.getDate();

                var d2 = $.datepicker.parseDate('mm/dd/yy', hidden_date);
                var mysqlDate2 =d2.getFullYear()+"-"+(d2.getMonth()+1)+"-"+(d2.getDate()-15);

                console.log(mysqlDate1, mysqlDate2);
                var extra_where = [{column: 'type', value: 'E', condition: '='}, {column: 'date', value: mysqlDate1, condition: '<'}, {column: 'date', value: mysqlDate2, condition: '>'}];
            }  if(searchValue == "3"){

                var monthyear=$(".calander2").val();
                var chnged_date1 = monthyear+"-"+"1";
                var chnged_date2 = monthyear+"-"+"31";
                console.log('dddd',chnged_date2);
                var extra_where = [{column: 'type', value: 'E', condition: '='}, {column: 'date', value: chnged_date1, condition: '>'}, {column: 'date', value: chnged_date2, condition: '<='}];
            }
        }
    } else {
        if (idd != "" && searchValue == "0") {
            var extra_where = [{column: 'type', value: 'E', condition: '='}, {column: 'user_id', value: idd, condition: '='}];
        } else if (idd !== "" && searchValue == "1") {
            var extra_where = [{column: 'type', value: 'E', condition: '='}, {column: 'user_id', value: idd, condition: '='}];
        } else if (idd !== "" && searchValue == "2") {

            var d1 = $.datepicker.parseDate('mm/dd/yy', hidden_date);
            var mysqlDate1 =d1.getFullYear()+"-"+(d1.getMonth()+1)+"-"+d1.getDate();


            var d2 = $.datepicker.parseDate('mm/dd/yy', hidden_date);
            var mysqlDate2 =d2.getFullYear()+"-"+(d2.getMonth()+1)+"-"+(d2.getDate()-15);

            console.log(mysqlDate1, mysqlDate2);;
            var extra_where = [{column: 'type', value: 'E', condition: '='}, {column: 'user_id', value: idd, condition: '='}, {column: 'date', value: mysqlDate1, condition: '<'}, {column: 'date', value: mysqlDate2, condition: '>'}];
        } else if (idd != "" && searchValue == "3") {
            var monthyear=$(".calander2").val();
            var chnged_date1 = monthyear+"-"+"1";
            var chnged_date2 = monthyear+"-"+"31";
            var extra_where = [{column: 'type', value: 'E', condition: '='},{column: 'user_id', value: idd, condition: '='}, {column: 'date', value: chnged_date1, condition: '>'}, {column: 'date', value: chnged_date2, condition: '<='}];

        }
    }
}
 else if(radio_check=="datee"){
    var date1=$('#date-range1').val();
    var d1 = $.datepicker.parseDate('mm/dd/yy', date1);
    var mysqlDate1 =d1.getFullYear()+"-"+(d1.getMonth()+1)+"-"+d1.getDate();
    console.log(mysqlDate1);

    var date2=$('#date-range2').val();
    var d2 = $.datepicker.parseDate('mm/dd/yy', date2);
    var mysqlDate2 =d2.getFullYear()+"-"+(d2.getMonth()+1)+"-"+d2.getDate();

    if (idd == ""){
        var extra_where = [{column:'type', value :'E', condition:'='},{column:'date', value :mysqlDate1, condition:'>'},{column:'date', value :mysqlDate2, condition:'<'}];
    }

    else if (idd !== ""){

        var extra_where = [{column:'type', value :'E', condition:'='},{column: 'user_id', value: idd, condition: '='},{column:'date', value :mysqlDate1, condition:'>'},{column:'date', value :mysqlDate2, condition:'<'}];

    }


}
        var extra_columns = [];

        //var extra_where = [{column:'type', value :'E', condition:'='},{column:'date', value :date1, condition:'<'},{column:'date', value :date2, condition:'>'}];extra_columns = [];
        var joins = [];
        var columns_options = [
            {name:'Employee Full Name',index:'emp_name', width:100,hidden:false,searchoptions: {sopt: conditions},table:table},
            {name:'Employee ID',index:'user_id', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Vehicle',index:'vehicle_name', width:100,searchoptions: {sopt: conditions},table:table,},
            {name:'start_location',index:'start_location',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:'mielage_log'},
            {name:'final_reading',index:'final_reading',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:'mielage_log'},
            {name:'Journey Details (To - From)',index:'end_location', width:300,hidden:false,searchoptions: {sopt: conditions},table:table, change_type: 'combine_column_hyphen2', extra_columns: ['end_location', 'start_location'],original_index: 'end_location'},
            {name:'Date',index:'date', width:200,hidden:false,searchoptions: {sopt: conditions},table:table},
            {name:'Odometer Reading (Start - Stop Reading)',index:'initial_reading', width:300,hidden:false,searchoptions: {sopt: conditions},table:table,change_type: 'combine_column_hyphen2', extra_columns: ['initial_reading', 'final_reading'],original_index: 'initial_reading'},
            {name:'Number of Miles',index:'mileage', width:100,hidden:false,searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'', title: false, width:100,align:"right",sortable:false,cellEdit: true, editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},
        ];

        var ignore_array = [];
        jQuery("#employee_log").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'true'
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Employee Mileage Log",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:200,drag:true,resize:false} // search options
        );
    }

      compname();
      function compname() {
          $.ajax({
              type: 'post',
              url: '/mileage-log-ajax',
              data: {
                  class: "mileagelog",
                  action: "getuserdata",

              },
              success: function (response) {
                  var data = JSON.parse(response);
                  if (data.status == 'success') {


                      $('#pm_name').val(data.data1[0].company_name);
                      $('#compp_name').val(data.data1[0].company_name);
                      $('#company_name').val(data.data1[0].company_name);
console.log('nnn',data);
                  }
              },

          });
      }

    $(document).on('click','#filter2',function () {
        var searchVal=$('#searchvalues').val();
        $('#employee_log').jqGrid('GridUnload');
        var idd= $("#search_id").val();
        emplog1(idd, searchVal);

    });

    $(document).on('click','#mymodalcheck',function () {
        var searchVal=$('#searchvaluess').val();
        $('#vehicle_log').jqGrid('GridUnload');
        var idd= $("#search_idd").val();
        vehiclelog1(idd, searchVal);

    });




//delete log for company vehicle
    $(document).on('change', '#vehicle_log .select_options', function(e) {

        e.preventDefault();
        setTimeout(function(){ $(".select_options").val("default"); }, 200);

        var opt = $(this).val();
        var id = $(this).attr('data_id');
        console.log('idd',id);
        var user_id = $(this).attr('data-user_id');
        var row_num = $(this).parent().parent().index() ;
        var status = $(this).attr('status');
        var is_default = $(this).attr('is_default');
        if (opt == 'Edit' || opt == 'EDIT') {
            $("#update_vidd").val(id);
            $("#edit_vehiclelog").modal('show');
            getvehicledataedit(id);

        }

        if (opt == 'Delete' || opt == 'DELETE') {
            bootbox.confirm("Are you sure you want to delete this entry?", function (result) {
                if (result == true) {
                    deletelog1(id);

                } else {
                    $('#vehicle_log ').trigger( 'reloadGrid' );
                }
            });
        }

    });
    $(document).on('click','.cancel_edit__Popup',function () {

        $("#edit_vehiclelog").modal('hide');
    });

      $(document).on('click','#update_vehiclelog',function () {
          var id= $("#update_vidd").val();
          editcompanylog(id);

      });


//delete log for employee
    $(document).on('change', '#employee_log .select_options', function(e) {

        e.preventDefault();
        setTimeout(function(){ $(".select_options").val("default"); }, 200);

        var opt = $(this).val();
        var id = $(this).attr('data_id');

        console.log('iddd',id);

        if (opt == 'Edit' || opt == 'EDIT') {
            $("#update_idd").val(id);
            $("#date_purchased").val("");
            $("#edit_companylog").modal('show');
            getempdata(id);
            setTimeout(function () {
            $("#date_purchased").val("");
            },600);
            $('#vehicle_id').attr("readonly", false);


        }

        if (opt == 'Delete' || opt == 'DELETE') {
            bootbox.confirm("Are you sure you want to delete this entry?", function (result) {
                if (result == true) {
                    deletelog2(id);

                } else {
                    $('#employee_log ').trigger( 'reloadGrid' );
                }
            });
        }

    });
    $(document).on('click','.cancel_Popup',function () {

        $("#edit_companylog").modal('hide');
    });
      $(document).on('click','#update_emp_log',function () {
         var idd=  $("#update_idd").val();
          editemplog(idd);

      });

//delete log for vehicle

    function deletelog1(id) {
        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: "mileagelog",
                action: "deletelog",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#vehicle_log').trigger('reloadGrid');

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


//delete log for employee
    function deletelog2(id) {
        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: "mileagelog",
                action: "deletelog",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);

                    $('#employee_log').trigger('reloadGrid');

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    $(document).on('change','#searchvalues',function(){
        if($('#searchvalues').val() == '3' ){
            $('.monthly').show();
        }
        else{
                $('.monthly').hide();
        }
    });

    $(document).on('change','#searchvaluess',function(){
        console.log($("input[name=alt]").val());
        if($('#searchvaluess').val() == '3'){
            $('.monthlyy').show();
        }
        else if(($("input[name=alt]").val()=="date")){
            $('.monthlyy').hide();
        }
        else{
            $('.monthlyy').hide();
        }
    });

      $("#company_name").val($("#pm_name").val());

    $(".addvehicle").click(function() {
        $("#comp_employeelogg").trigger('reset');
        var date = $.datepicker.formatDate(jsDateFomat, new Date());
        $('.calander3').val(date);
        $("#compp_name").val($("#pm_name").val());
        $("#comp_name").val($("#pm_name").val());

        //$(".calander3").val(today);
        $("#add_vehiclelog").modal('show');
    });
    $(".cancel_Popup").click(function() {
        $("#add_vehiclelog").modal('hide');
    });

//popup for employee
    $(".addemployee").click(function() {
        $("#addmileagelogg").trigger('reset');
        var date = $.datepicker.formatDate(jsDateFomat, new Date());
        $('.calander3').val(date);
        $("").val($("#pm_name").val());
        $("#compp_name").val($("#pm_name").val());
        $("#comp_name").val($("#pm_name").val());
        $('#vehicle_no').val(getRandomNumber(5));
      //  $("#addmileagelogg").trigger('reset');
        $("#addmileagelog").modal('show');
    });

    $(".cancelPopup").click(function() {
        $("#addmileagelog").modal('hide');
    });

//radio buttons for company
    $('input[type=radio][name=alt]').change(function() {
        if (this.value == 'week') {
            $("#byweek").show();
            $("#bydate").hide();
        }
        else if (this.value == 'date') {
            $("#byweek").hide();
            $("#bydate").show();
            $(".monthlyy").hide();
        }
    });

//radio buttons for employee
    $('input[type=radio][name=case]').change(function() {
        if (this.value == 'weekk') {
            $("#week").show();
            $("#datte").hide();
        }
        else if (this.value == 'datee') {
            $("#datte").show();
            $("#week").hide();
            $(".monthly").hide();
        }
    });
  // $ (function() {
  //       $("#year_vehicle").datepicker({
  //           changeYear:true,
  //           yearRange: "2005:2015"
  //       });
  //   });
    $( "#year_vehicle" ).datepicker(
        {
            dateFormat: 'yy',
            changeMonth: false,
            changeYear: true,
            yearRange: "1900:-13"
        }
    ).on('change', function() {
        $(this).valid();  // triggers the validation test
        // '$(this)' refers to '$("#datepicker")'
    });

    //vehicle data

    getvehicledata();
    function getvehicledata(){
        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: 'mileagelog',
                action: 'getvehicledata',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#select_vehicle').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {

                        $('#select_vehicle').append("<option value = "+value.vehicle_name+">"+value.vehicle_name+"</option>");

                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }
    getvehicledata1();
    function getvehicledata1(){
        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: 'mileagelog',
                action: 'getvehicledata',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#vehicle_namee').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {

                        $('#vehicle_namee').append("<option value = "+value.vehicle_name+">"+value.vehicle_name+"</option>");

                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }


//employee detail for addition
    getuserdetail();
    function getuserdetail(){
        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: 'mileagelog',
                action: 'getuserdata',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#full_name').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {

                        $('#full_name').append("<option value = "+value.id+">"+value.name+"</option>");

                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }
    $(document).on('change','#full_name',function(){

        if($('#full_name').val()!=='null' || $('#full_name').val()!==' ' ){

            var idd= $('#full_name').val();
            $("#emp_id").val(idd);
        }
        setTimeout(function () {
            getvehicledetails($("#emp_id").val());
        },100);

    });

    function getvehicledetails(id) {
        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: 'mileagelog',
                action: 'getvehicledetails',
                id:id,
            },
            success: function (response) {
                var data = JSON.parse(response);
                var len=data.data.length;
                len=len-parseInt(1);
                console.log(data);
          $(".vehicle_name").val(data.data[len].vehicle_name);
          $("#vehicle_no").val(data.data[len].vehicle);
          $("#vehicle_type").val(data.data[len].vehicle_type);
          $("#vehicle_make").val(data.data[len].make);
          $(".model").val(data.data[len].model);
          $("input[name=vin]").val(data.data[len].vin);
          $("#registration_no").val(data.data[len].registration);
          $("#license_no").val(data.data[len].plate_number);
          $(".color").val(data.data[len].color);
          $("#year_vehicle").val(data.data[len].year_of_vehicle);
            }
        });
    }
    //vehicle detail for addition
    getuserdetail1();
    function getuserdetail1(){
        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: 'mileagelog',
                action: 'getuserdata',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#full_namee').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {

                        $('#full_namee').append("<option value = "+value.id+">"+value.name+"</option>");

                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    $(document).on('change','#full_namee',function() {

        if ($('#full_name').val() !== 'null' || $('#full_namee').val() !== ' ') {

            var idd = $('#full_namee').val();
            $("#emp_idd").val(idd);
        }

    });
//emolyee log

    $(document).on('blur','#stop_reading',function(){
       // alert('gjbhj');
        $('#start_reading').attr("readonly", false);
            var val = $(this).val() - $('#start_reading').val();
            console.log('mileage', val);
       if($(this).val() > $('#start_reading').val() || val > 0){
            $('#mileage').val(val);
       }
       else if($(this).val()=="" && $('#start_reading').val()==""){
           $('#mileage').val("");
       }
      else if(val <= 0){
        $('#mileage').val(' ');
         toastr.error('Start reading cannot be greater than stop reading')

      }
    });

    $(document).on('blur','#start_reading', function(){
        var val= $('#stop_reading').val()-$(this).val();
        console.log('mileage',val);
        if(($('#stop_reading').val() > $(this).val() && val > 0) || ($('#stop_reading').val()=='' && $(this).val() !='')){
            if(($('#stop_reading').val()=='' && $(this).val() !='')){
            $('#mileage').val('');
             }
            else if(($('#stop_reading').val() !=='' && $(this).val() !=='')){
                $('#mileage').val(val);
            }
            else{
                $('#mileage').val(' ');
                toastr.error('Start reading cannot be greater than stop reading')
            }
        }
        else if($(this).val()=="" && $('#stop_reading').val()==""){
            $('#mileage').val("");
        }
        else if(val <= 0){
            $('#mileage').val(' ');
            $('#start_reading').val('');
            toastr.error('Start reading cannot be greater than stop reading')
        }
    });

    //company log
      $(document).on('input','#stop_reading',function(){
        var startreading = $('#start_reading').val();
        if(startreading == ''){
            toastr.error('Please Fill Start reading first.')
            $(this).val('');
        }

      });
    $(document).on('blur','#stop_readingg',function(){
     // $('#start_readingg').attr("readonly", false);
        var val=$(this).val() - $('#start_readingg').val();
        console.log('miltteage', val);
        if($(this).val() > $('#start_readingg').val() || val > 0){
            $('#mileagee').val(val);
        }
        else if($(this).val()=="" && $('#start_readingg').val()==""){
            $('#mileagee').val("");
        }
        else if( val <= 0){
            $('#mileagee').val(' ');
            toastr.error('Start reading cannot be greater than stop reading')

        }
    });

    $(document).on('blur','#start_readingg',function(){
        var val= ($('#stop_readingg').val())-($(this).val());
        console.log('mileagee',val);
        if((($('#stop_readingg').val() > $(this).val()) && val > 0) || ($('#stop_readingg').val()=='' && $(this).val()!='')){
            if(($('#stop_readingg').val()=='' && $(this).val() !=='')){
                $('#mileagee').val('');
            }
            else if($('#stop_readingg').val() !=='' && $(this).val() !==''){
                $('#mileagee').val(val);
            }
            else{
                $('#mileagee').val(' ');
                toastr.error('Start reading cannot be greater than stop reading')
            }
        }
        else if(($(this).val())=="" && ($('#stop_readingg').val())==""){
            $('#mileagee').val("");
        }
        else if(val <= 0){
            $('#mileagee').val(' ');
            toastr.error('Start reading cannot be greater than stop reading')
            $('#start_reading').val('');
        }
    });
//add employee log
        $(document).on("click","#savelog",function() {
            if($('#addmileagelogg').valid()){
                addlog();
            }
    });

function addlog(){
    var base_url = window.location.origin;
    var formData = $('#addmileagelogg :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/mileage-log-ajax',
        data: {
            class: 'mileagelog',
            action: 'addlog',
            formData : formData,
        },
        success: function (response) {
            var data = JSON.parse(response);
            if (data.status == "success") {
                toastr.success("This record saved successfully");
                //onTop(true);
                setTimeout(function () {
                    $('#addmileagelog').modal('hide');
                },600);
                $('#employee_log').trigger('reloadGrid');
                setTimeout(function () {
                    jQuery('#employee_log').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('#employee_log').find('tr:eq(1)').find('td:eq(9)').addClass("green_row_right");
                },600);
                console.log('dd',data);
                localStorage.setItem('last_'+data.last_id1,data.last_id);
                setTimeout(function () {
                    $("#employee_log tr#"+data.last_id1).find("td:last").attr('idd',data.last_id);
                },500);
            }else if(data.code=400){
                     toastr.warning(data.message);
            }
            else {
                toastr.error(response.message);
            }
        }
    });
}

//add vehicle log
    $(document).on("click","#save_vehiclelog",function() {
        if($('#comp_employeelogg').valid()){
        addlog1();
        }
    });

    function addlog1(){
        var base_url = window.location.origin;
        var formData = $('#comp_employeelogg :input').serializeArray();
      //  console.log('compadd',formData);
        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: 'mileagelog',
                action: 'addlog1',
                formData : formData,
            },
            success: function (response) {
                var data = JSON.parse(response);

                if (data.status == "success") {

                    toastr.success("This record saved successfully");


                 //   onTop(true);
                    setTimeout(function () {

                        $('#add_vehiclelog').modal('hide');

                    },600);

                    $('#vehicle_log ').trigger( 'reloadGrid' );
                    jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('.table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                }
                else {
                    toastr.error(response.message);
                }
            }
        });
    }

    function getRandomNumber(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

//search input for vehicle log

    $("#enterlog").click(function() {

        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: 'mileagelog',
                action: 'getmileagedata',

            },
            success: function (response) {
                var data = JSON.parse(response);

                if (data.status == "success") {

         alert(data.data.id);
         console.log(data.data);
                    var html = "";

$.each(data.data,function (key,value) {

//console.log('name', value.vehicle_name);

    html =  '<thead>'+
        '<tr style="border:1px solid #C9C9C9;">'+
        '<th scope="col" >'+'Vehicle Name'+'</th>'+
        '<th scope="col" >'+'Vehicle Number'+' </th>'+
        '<th scope="col" >'+'Model'+'</th>'+
        '<th scope="col" >'+'Starting Mileage'+'</th>'+
        '</tr>'+
        '</thead>';

    html += '<tr style="border-left:1px solid #afafaf; border-right:1px solid #afafaf;">'+
        '<td class="col-sm-3">'+ data.data.vehicle_name+'</td>'+
        '<td class="col-sm-3" >'+data.data.vin_no+'</td>'+
        '<td class="col-sm-3" >'+ data.data.model+'</td>'+
        '<td class="col-sm-3" >'+ data.data.mileage+ '</td>'+
        '</tr>';

});
                    $("#enterlog :input").html(html);
                }
                else {
                    toastr.error(response.message);
                }
            }
        });

    });

//edit section

//employee detail for addition
    getempdetail();
    function getempdetail(){
        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: 'mileagelog',
                action: 'getuserdata',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('.emppp_name').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {

                        $('.emppp_name').append("<option value = "+value.id+">"+value.name+"</option>");

                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }
    $(document).on('change','.emppp_name',function(){

        if($('.emppp_name').val()!=='null' || $('.emppp_name').val()!==' ' ){

            var idd= $('.emppp_name').val();
            $('.user_id').val(idd);
        }

    });

//edit emp log
    function getempdata(id){

        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: 'mileagelog',
                action: 'geteditdata',
                id:id,
            },
            success: function (response) {
                var data = JSON.parse(response);

                if (data.status == "success") {

                    if (data.status == 'success' && data.code == 200) {
                        $('#em_name').val(data.data[0].emp_id);
                        $('#miileage').val(data.data[0].mileage);
                        console.log('co',data.data1[0].company_name);
                        $('#company_namee').val(data.data1[0].company_name);
                        // $('#emp_name').append("<option value = "+data.data[0].id+">"+data.data[0].emp_name+"</option>").attr("selected",true);
                        $.each(data.data[0], function(key, value) {

                            console.log(value);
                            $('.'+key).val(value);

                        });
                        defaultFormData=$("#editmileagelogg").serializeArray();
                    } else {
                        toastr.error(response.message);
                    }
                }
                else {
                    toastr.error(response.message);
                }
            }
        });
    }

    $(document).on('blur','#final_reading',function(){
        $('#initial_reading').attr("readonly", false);

        var val = $('#final_reading').val() - $('#initial_reading').val();

        if($('#final_reading').val() > $('#initial_reading').val() && val > 0)
        {
            $('.mileage').val(val);
        }
        else if(($(this).val())=="" && ($('#initial_reading').val())==""){
            $('#mileage').val("");
        }
        else{
            $('.mileage').val(' ');
            toastr.error('Start reading cannot be greater than stop reading')
        }
    });

    $(document).on('blur','#initial_reading',function(){

        var val= $('#final_reading').val()-$('#initial_reading').val();

        console.log('mileagee',val);
        if(($('#final_reading').val() > $('#initial_reading').val() && val > 0)|| ($('#final_reading').val()=='' && $(this).val()!='')){
            if(($('#final_reading').val()=='' && $(this).val() !='')){
                $('.mileage').val('');
            }
            else if(($('#final_reading').val() !=='' && $(this).val() !=='') && ($('#final_reading').val() > $('#initial_reading').val() && val > 0)){
                $('.mileage').val(val);
            }
            else{
                $('.mileage').val(' ');
                toastr.error('Start reading cannot be greater than stop reading')
            }
        }
        else if(($(this).val())=="" && ($('#final_reading').val())==""){
            $('#mileage').val("");
        }
        else{
            $('.mileage').val(' ');
            toastr.error('Start reading cannot be greater than stop reading')
        }
    });

    function editemplog(id){
         var man_id=localStorage.getItem('last_'+id);
         console.log(man_id);
        // return false;
        var base_url = window.location.origin;
        var formData = $('#editmileagelogg :input').serializeArray();
        // localStorage.removeItem('last_'+id);
        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: 'mileagelog',
                action: 'edit_emplog',
                formData : formData,
                id:id,
                'man_id':man_id

            },

            success: function (response) {
                var data = JSON.parse(response);

                if (data.status == "success") {

                    toastr.success("This record Updated successfully");
                    onTop(true);
                    setTimeout(function () {

                        $('#edit_companylog').modal('hide');

                    },600);

                    $('#employee_log ').trigger( 'reloadGrid' )

                }
                else {
                    toastr.error(response.message);
                }
            }
        });
    }

//VEHICLE-Company detail for edit
    getempimfo();
    function getempimfo(){
        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: 'mileagelog',
                action: 'getuserdata',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#emp_namme').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {

                        $('#emp_namme').append("<option value = "+value.id+">"+value.name+"</option>");

                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }
    $(document).on('change','#emp_namme',function(){

        if($('#emp_namme').val()!=='null' || $('#emp_namme').val()!==' ' ){

            var idd= $('#emp_namme').val();
            $('#user_id').val(idd);
        }

    });

//edit vehicle log
    function getvehicledataedit(id){

        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: 'mileagelog',
                action: 'geteditdata',
                id:id,
            },
            success: function (response) {
                var data = JSON.parse(response);

                if (data.status == "success") {

                    if (data.status == 'success' && data.code == 200) {

                        $('#emp_namme').val(data.data[0].emp_id);
                        $('#vehicle_namee').val(data.data[0].vehicle_name);
                        $('.mileage').val(data.data[0].mileage);
                       // console.log('co0',data.data1[0].company_name);
                        setTimeout(function () {
                        $("input[name=comp_namee]").val($("#pm_name").val());
                        },100);
                        $('#company_name').val(data.data1[0].company_name);
                        // $('#emp_name').append("<option value = "+data.data[0].id+">"+data.data[0].emp_name+"</option>").attr("selected",true);
                        $.each(data.data[0], function(key, value) {

                            console.log(value);
                            $('#'+key).val(value);

                        });
                        //defaultFormData=$("#editmileagelogg").serializeArray();
                        defaultFormData=$("#edit_companyvehiclelog").serializeArray();
                    } else {
                        toastr.error(response.message);
                    }
                }
                else {
                    toastr.error(response.message);
                }
            }
        });
    }

    $(document).on('blur','.final_reading',function(){
        $('.initial_reading').attr("readonly", false);

        var val = $('.final_reading').val() - $('.initial_reading').val();

        if($('.final_reading').val() > $('.final_reading').val() && val > 0)
        {
            $('#miileage').val(val);
        }
        else if(($(this).val())=="" && ($('#initial_reading').val())==""){
            $('#miileage').val("");
        }
        else{
            $('#miileage').val(' ');
            toastr.error('Start reading cannot be greater than stop reading')
        }
    });

    $(document).on('blur','.initial_reading',function(){

        var val= $('.final_reading').val()-$('.initial_reading').val();

        console.log('mileagee',val);
        if(($('.final_reading').val() > $('.initial_reading').val() && val > 0) || ($('.final_reading').val()=='' && $(this).val() !=='')){
            if(($('.final_reading').val()=='' && $(this).val() !='')){
                $('#miileage').val('');
            }
            else if(($('.final_reading').val() !=='' && $(this).val() !=='')){
                $('#miileage').val(val);
            }
            else{
                $('#miileage').val(' ');
                toastr.error('Start reading cannot be greater than stop reading')
            }
        }
        else if(($(this).val())=="" && ($('#final_reading').val())==""){
            $('#miileage').val("");
        }
        else{
            $('#miileage').val(' ');
            toastr.error('Start reading cannot be greater than stop reading')
        }
    });

    function editcompanylog(id){
        var base_url = window.location.origin;
        var formData = $('#edit_companyvehiclelog :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: 'mileagelog',
                action: 'edit_emplog1',
                formData : formData,
                id:id,
            },
            success: function (response) {
                var data = JSON.parse(response);

                if (data.status == "success") {

                    toastr.success("This record saved successfully");
                   onTop(true);
                    setTimeout(function () {

                        $('#edit_vehiclelog').modal('hide');

                    },600);

                    $('#vehicle_log ').trigger( 'reloadGrid' )

                }
                else {
                    toastr.error(response.message);
                }
            }
        });
    }

    $("#comp_employeelogg").validate({
        rules: {
            'full_namee': {required: true},
            'end_loca': {required: true},
            'start_loca': {required: true},
            'start_readingg': {required: true},
            'stop_readingg': {required: true},

        }
    });  $("#comp_employeelogg").validate({
        rules: {
            'full_namee': {required: true},
            'end_loca': {required: true},
            'start_loca': {required: true},
            'start_readingg': {required: true},
            'stop_readingg': {required: true},

        }
    });
    $("#addmileagelogg").validate({
        rules: {
            'full_name': {required: true},
            'vehicle_name': {required: true},
            'end_loc': {required: true},
            'start_loc': {required: true},
            'start_reading': {required: true},
            'stop_reading': {required: true},

        }
    });

    $(document).on("click","input[name='search']", function(){
        getuserList();
    });

    $(document).on("click","input[name='search_vehicle']", function(){
        getuserList1();
    });

    $(document).on("click",".getMileageList", function(){
       var id= $(this).attr('data-id');
        var name = $(this).text();
        $("input[name='search']").val(name);

        if($("input[name='search']").val() != ""){
        $('#search_id').val(id);
        }
        if($("input[name='search']").val() == ""){
            $('#search_id').val("");
        }

        $('.vendor_list_container').removeClass("in");

    });

    $(document).on("blur","#search", function(){

        if($("input[name='search']").val() == ""){
            $('#search_id').val("");
        }

    });

    $(document).on("click",".getMileageList2", function(){
        var id= $(this).attr('data-id');

        var name = $(this).text();
        $("input[name='search_vehicle']").val(name);

        if($("input[name='search_vehicle']").val() != ""){
          var d=  $('#search_idd').val(id);

        }

        if($("input[name='search_vehicle']").val() == ""){
            $('#search_idd').val("");
        }

        $('.list_container').removeClass("in");

    });

    $(document).on("blur","#search_vehicle", function(){

        if($("input[name='search_vehicle']").val() == ""){
            $('#search_idd').val("");
        }

    });

    function getuserList(){
        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: "mileagelog",
                action: "getuserdata"
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    if (data.data.length > 0){
                        console.log(data.data.name);
                        var tableRow = '';
                        $.each(data.data, function (key, value) {
                            tableRow += "<tr style='cursor: pointer;'>";
                            tableRow +=     "<td style='display: none;'>"+value.id+"</td>";
                            tableRow +=     "<td class='data_name'><a href='#' class='getMileageList' data-id="+value.id+" data-name="+value.name+">"+value.name+"</a></td>";
                            tableRow +=     "<td>"+value.id+"</td>";
                            tableRow +=     "<td>"+value.phone_number+"</td>";
                            tableRow +=     "<td>"+value.email+"</td>";
                        });

                        $('#vendorlist tbody').html(tableRow);
                        $('.vendor_list_container').addClass("in");
                        // $('#vendorlist1 tbody').html(tableRow);
                        // $('.list_container').addClass("in");
                    }
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    //toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function getuserList1(){
        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: "mileagelog",
                action: "getuserdata"
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    if (data.data.length > 0){
                        var tableRow = '';
                        $.each(data.data, function (key, value) {
                            tableRow += "<tr style='cursor: pointer;'>";
                            tableRow +=     "<td style='display: none;'>"+value.id+"</td>";
                            tableRow +=     "<td ><a href='#' class='getMileageList2' data-id="+value.id+" data-name="+value.name+">"+value.name+"</a></td>";
                            tableRow +=     "<td>"+value.id+"</td>";
                            tableRow +=     "<td>"+value.phone_number+"</td>";
                            tableRow +=     "<td>"+value.email+"</td>";
                        });

                        $('#vendorlist1 tbody').html(tableRow);
                        $('.list_container').addClass("in");
                    }
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    //toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("keyup","#search", function(){
   var a=$('#search').val();

   if(a.length >= 2){

        $.ajax({
            type: 'post',
            url: '/mileage-log-ajax',
            data: {
                class: "mileagelog",
                action: "getuserdata1",
                a:a,
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    if (data.data.length > 0){
                        var tableRow = '';
                        $.each(data.data, function (key, value) {
                            tableRow += "<tr style='cursor: pointer;'>";
                            tableRow +=     "<td style='display: none;'>"+value.id+"</td>";
                            tableRow +=     "<td ><a href='#' class='getMileageList' data-id="+value.id+" data-name="+value.name+">"+value.name+"</a></td>";
                            tableRow +=     "<td>"+value.id+"</td>";
                            tableRow +=     "<td>"+value.phone_number+"</td>";
                            tableRow +=     "<td>"+value.email+"</td>";
                        });

                        $('#vendorlist tbody').html(tableRow);
                        $('.vendor_list_container').addClass("in");
                    }
                }
            },

        });

      }
        if(a.length < 2){

            $.ajax({
                type: 'post',
                url: '/mileage-log-ajax',
                data: {
                    class: "mileagelog",
                    action: "getuserdata",

                },
                success: function (response) {
                    var data = $.parseJSON(response);
                    if (data.status == "success") {
                        if (data.data.length > 0){
                            var tableRow = '';
                            $.each(data.data, function (key, value) {
                                tableRow += "<tr style='cursor: pointer;'>";
                                tableRow +=     "<td style='display: none;'>"+value.id+"</td>";
                                tableRow +=     "<td ><a href='#' class='getMileageList' data-id="+value.id+" data-name="+value.name+">"+value.name+"</a></td>";
                                tableRow +=     "<td>"+value.id+"</td>";
                                tableRow +=     "<td>"+value.phone_number+"</td>";
                                tableRow +=     "<td>"+value.email+"</td>";
                            });

                            $('#vendorlist tbody').html(tableRow);
                            $('.vendor_list_container').addClass("in");
                        }
                    }
                },

            });

        }

    });

      $(document).on("keyup","#search_vehicle", function(){
          var a=$('#search_vehicle').val();

          if(a.length >= 2){
              $.ajax({
                  type: 'post',
                  url: '/mileage-log-ajax',
                  data: {
                      class: "mileagelog",
                      action: "getuserdata1",
                      a:a,
                  },
                  success: function (response) {
                      var data = $.parseJSON(response);
                      if (data.status == "success") {
                          if (data.data.length > 0){
                              var tableRow = '';
                              $.each(data.data, function (key, value) {
                                  tableRow += "<tr style='cursor: pointer;'>";
                                  tableRow +=     "<td style='display: none;'>"+value.id+"</td>";
                                  tableRow +=     "<td ><a href='#' class='getMileageList' data-id="+value.id+" data-name="+value.name+">"+value.name+"</a></td>";
                                  tableRow +=     "<td>"+value.id+"</td>";
                                  tableRow +=     "<td>"+value.phone_number+"</td>";
                                  tableRow +=     "<td>"+value.email+"</td>";
                              });

                              $('#vendorlist1 tbody').html(tableRow);
                              $('.list_container').addClass("in");
                          }
                      }
                  },

              });

          }
          if(a.length < 2){

              $.ajax({
                  type: 'post',
                  url: '/mileage-log-ajax',
                  data: {
                      class: "mileagelog",
                      action: "getuserdata",

                  },
                  success: function (response) {
                      var data = $.parseJSON(response);
                      if (data.status == "success") {
                          if (data.data.length > 0){
                              var tableRow = '';
                              $.each(data.data, function (key, value) {
                                  tableRow += "<tr style='cursor: pointer;'>";
                                  tableRow +=     "<td style='display: none;'>"+value.id+"</td>";
                                  tableRow +=     "<td ><a href='#' class='getMileageList' data-id="+value.id+" data-name="+value.name+">"+value.name+"</a></td>";
                                  tableRow +=     "<td>"+value.id+"</td>";
                                  tableRow +=     "<td>"+value.phone_number+"</td>";
                                  tableRow +=     "<td>"+value.email+"</td>";
                              });

                              $('#vendorlist1 tbody').html(tableRow);
                              $('.list_container').addClass("in");
                          }
                      }
                  },

              });

          }

      });

      $(document.body).click( function(e) {
          var container = $("#tabledropdown1");
          // if the target of the click isn't the container nor a descendant of the container
          if (!container.is(e.target) && container.has(e.target).length === 0)
          {
              $('.list_container').removeClass("in");
          }

      });

      $(document.body).click( function(e) {
          var container = $("#tabledropdown2");
          // if the target of the click isn't the container nor a descendant of the container
          if (!container.is(e.target) && container.has(e.target).length === 0)
          {
              $('.vendor_list_container').removeClass("in");
          }

      });

      // $(document).on('click','#end', function () {
   //
   //      google.maps.event.trigger(this, 'focus', {});
   //      google.maps.event.trigger(this, 'keydown', {
   //          keyCode: 13
   //      });
   //  });

    //
    // var map;
    //   initMap()
    // function initMap() {
    //     map = new google.maps.Map(document.getElementById('end'), {
    //         center: {lat: -34.397, lng: 150.644},
    //         zoom: 8
    //     });
    // }

      $(document).on('blur','#start_readingg',function () {
      var vehicle =   $('#comp_employeelogg  #select_vehicle').val();
      if(vehicle == ''){
          $('#start_readingg').val('');
          toastr.warning('Please select atleast one vehicle');
      }
      });
});




