 
  

   getMaintenance();

  function getMaintenance(status) {
    var table = 'tenant_maintenance';
    var columns = ['<input type="checkbox" id="select_all_complaint_checkbox">','Tenant Name','Property','Unit','Ticket No.','Ticket Type', 'Category', 'Created Date', 'Image/Photo(s)', 'Status', 'Action'];
    var select_column = ['Edit','Delete','Cancel'];
    var joins = [{table:'tenant_maintenance',column:'user_id',primary:'id',on_table:'users',as :'tp'},
    {table:'tenant_maintenance',column:'unit_id',primary:'id',on_table:'unit_details',as :'tp1'},
    {table:'tenant_maintenance',column:'property_id',primary:'id',on_table:'general_property',as :'tp3'},];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var columns_options = [
        {name: 'Id', index: 'id', width: 190, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatterComplaint},
        {name:'Tenant Name',index:'first_name', width:90,align:"left",searchoptions: {sopt: conditions},table:'tp'},
        {name:'Property',index:'property_name', width:150,align:"center",searchoptions: {sopt: conditions},table:'tp3'},
        {name:'Unit',index:'unit_no', width:150,searchoptions: {sopt: conditions},table:'tp1'},
        {name:'Ticket No.',index:'ticket_number', width:108,align:"center",hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'Ticket Type',index:'ticket_type', width:108,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Category',index:'category', width:108,searchoptions: {sopt: conditions},table:table},
        {name:'Created Date',index:'created_at', width:108,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Image/Photo(s)',index:'ticket_type', width:108,searchoptions: {sopt: conditions},table:table,formatter:maintenanceImageFormatter},
        {name:'Status',index:'status', width:108,searchoptions: {sopt: conditions},table:table,formatter:getstatusFormatter},
        {name:'Action',index:'status', width:300,searchoptions: {sopt: conditions},table:table,formatter:selectMainTenanceFormatter},
    ];
    var ignore_array = [];
    jQuery("#TenantMaintenance-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'created_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Maintenance Details",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}



function actionCheckboxFmatterComplaint(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {

        var data = '';
        var data = '<input type="checkbox" name="maintenance_checkbox[]" class="maintenance_checkbox" id="maintenance_checkbox' + rowObject.id + '" data_id="' + rowObject.id + '"/>';
        return data;
    }
}

$("#select_all_complaint_checkbox").click(function () {
    $(".complaint_checkbox").prop('checked', $(this).prop('checked'));
});




function getstatusFormatter(cellValue, options, rowObject)
{
    if(rowObject!==undefined)
    {
        console.log(rowObject);

        var status =  rowObject.Status;
        if(status==1)
        {
            return "Open";
        }
        else
        {
             return "Cancel";
        }


    } 
}




  function maintenanceImageFormatter (cellValue, options, rowObject){

    
    if(rowObject!==undefined)
    {
        var image1 =  rowObject.image1;
        var image2 =  rowObject.image2;
        var image3 =  rowObject.image3;
        if(image3=="" && image2=="" && image1=="")
        {
            return "No Image Uploaded";
        }
        else
        {
    
       return "<a href='javascript:void(0);' data-id='"+rowObject.id+"' class='getImagesByTable' data-tablename='tenant_maintenance'>Images/Photo</a>";        }

        }


    }


function selectMainTenanceFormatter(cellValue, options, rowObject)
{
  
   if(rowObject !== undefined) {
    var html = "<select editable='1' class='form-control select_options' data_id='"+rowObject.Id+"'>";
    console.log(rowObject.status);

    if(rowObject.Status==1)
    {
   html +=  "<option value=''>Select</option>";
   html +=  "<option value='Delete'>Delete</option>";
   html +=  "<option value='Cancel'>Cancel</option>";

     }
     else
     {
   html +=  "<option value=''>Select</option>";
   html +=  "<option value='View'>View</option>";


     }
   
 html +="</select>";
    

        



   return html;


      
   }
}



     $(document).on("click",".getImagesByTable",function(){
      tablename = $(this).attr('data-tablename');
      id = $(this).attr('data-id');
      
         $.ajax({
        url:'/editTenant?action=getImageByTable&class=EditTenant&id='+id+'&tablename='+tablename,
        type: 'GET',
        async:false,

        success: function (data) {
             var image = JSON.parse(data);

            setTimeout(function(){
                $('#imageModel').modal('show');
                $('#getImage1').html(image.img1);
                $('#getImage2').html(image.img2);
                $('#getImage3').html(image.img3);
            }, 400);

            
              },
  
    });  
  });



    $(document).on("click",'#print_email_button',function(){    

        favorite=[];
        var no_of_checked =    $('[name="maintenance_checkbox[]"]:checked').length
        if(no_of_checked == 0){
            toastr.error('Please select atleast one Complaint.');
            return false;
        }
        $.each($("input[name='maintenance_checkbox[]']:checked"), function(){
            favorite.push($(this).attr('data_id'));
        });

        $.ajax({
            type: 'post',
            url: '/editTenant',
            data: {class: 'EditTenant', action: 'getMaintenanceData','complaint_ids':favorite,'id':tenant_id},
            success : function(response){
                var response =  JSON.parse(response);
                if(response.status == 'success' && response.code == 200) {
                    $("#print_complaint").modal('show');
                    $("#modal-body-complaints").html(response.html)
                }else {
                    toastr.warning('Record not updated due to technical issue.');
                }
            }
        });
    });    
