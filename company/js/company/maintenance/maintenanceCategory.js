/**
 * jqGrid function to format status
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function statusFormatter (cellValue, options, rowObject){
    if (cellValue == 1)
        return "Active";
    else if(cellValue == '0')
        return "Inactive";
    else
        return '';
}
function statusFormatter1 (cellValue, options, rowObject){
    if (cellValue == 0)
        return "No";
    else if(cellValue == '1')
        return "Yes";
    else
        return '';
}
function statusFmatter1 (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';

        if(rowObject['Status'] == '1' && rowObject['editable'] == '0')  select = ['Edit','Deactivate','Delete'];
        if(rowObject['Status'] == '1' && rowObject['editable'] == '1')  select = ['Edit','Deactivate'];
        if((rowObject['Status'] == '0' || rowObject.Status == '' )&& rowObject['editable'] == '0')  select = ['Edit','Activate','Delete'];
        if((rowObject['Status'] == '0' || rowObject.Status == '' ) && rowObject['editable'] == '1')  select = ['Edit','Activate'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}
function statusFmatter2 (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';
        if(rowObject['is_editable'] == '1')  select = ['Edit','Deactivate'];
        if(rowObject['is_editable'] == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}
var base_url = window.location.origin;
$(document).on("change", ".category_type_status #jqGridStatus", function (e) {
    var status = $(this).val();
    $('#categoryType-table').jqGrid('GridUnload');

    jqGrid(status,true);

});

/**
 * jqGrid Initialization function
 * @param status
 */
jqGrid('All');
function jqGrid(status, deleted_at) {
    var table = 'company_category_type';
    var columns = ['Category','Description', 'Set as default', 'Status','editable', 'Action'];
    if (status===0) {
        var select_column = ['Edit','Deactivate','Delete'];

        var select_column = ['Edit','Activate','Delete'];

    }
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];

    var columns_options = [

        {name:'Category',index:'category', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Description',index:'description', width:100,searchoptions: {sopt: conditions},table:table},
        {name:'Set as default',index:'is_default', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter1},

        {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
        {name:'editable',index:'is_editable', hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},

    ];
    var ignore_array = [];
    jQuery("#categoryType-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "company_category_type",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:deleted_at
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Maintenance Category",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:200,left:200,drag:true,resize:false} // search options
    );
}

$(document).ready(function () {
    var base_url = window.location.origin;

    var status = localStorage.getItem("active_inactive_status");



    /** Show add new unit type div on add new button click */
    $(document).on('click', '#addUnitTypeButton', function () {
        $('#category_type').prop('disabled', false);
        $('#saveBtns').val('Save');
        $('#unit_type').val('').prop('disabled', false);
        $('#category_type').val('');
        $('.Reset_button').hide();
        $('.categoryFormReset ').show();
        $("#description").val('');
        $("#user_id_hidden").val('');
        $("#categoryErr").text('');
        $("#descriptionErr").text('');

        headerDiv.innerText = "Add category Type";
        $('#is_default').prop('checked', false);
        $('#saveBtnId').val('Save');
        $('#add_unit_type_div').show(500);
    });

    /** Show import excel div on import excel button click */
    $(document).on('click', '#importUnitTypeButton', function () {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $('#import_unit_type_div').show(500);
    });

    /** Hide add new unit type div on cancel button click */
    $(document).on("click", "#add_unit_cancel_btn", function (e) {
        e.preventDefault();
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $("#add_unit_type_div").hide(500);
            } else {
            }
        });
    });


    /** Hide import excel div on cancel button click */
    $(document).on("click", "#import_unit_cancel_btn", function (e) {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $("#import_unit_type_div").hide(500);
            } else {
            }
        });
    });


    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter(cellValue, options, rowObject) {
        if (cellValue == 1)
            return "Active";
        else if (cellValue == '0')
            return "Inactive";
        else
            return '';
    }

    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isDefaultFormatter(cellValue, options, rowObject) {
        if (cellValue == 1)
            return "Yes";
        else if (cellValue == '0')
            return "No";
        else
            return '';
    }


    /*change status category type*/
    function changeStatuscategoryType(id, action) {
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "ApexNewUserAjax",
                action: "changeStatuscategoryType",
                apexnewuser_id: id,
                status_type: action,
            },
            success: function (response) {

                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The status has been changed.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#categoryType-table').trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    /** Export sample unit type excel */
    $(document).on("click", '#exportSampleExcel', function () {
        window.location.href = base_url + "/UnitType-Ajax?action=exportSampleExcel";
    })

    /** Export unit type excel  */
    $(document).on("click", '#exportUnitTypeButton', function () {
        var status = $("#jqGridStatus option:selected").val();
        var table = 'company_unit_type';
        window.location.href = base_url + "/UnitType-Ajax?status=" + status + "&&table=" + table + "&&action=exportExcel";
    });
    /*  insert into category type*/

   /* $(document).on("click", "#saveBtns", function (e) {

        e.preventDefault();




    });*/

    function saveCategory(){
        var category_type = $("input[name='category_type']").val();
        var desc = $("textarea[name='description']").val();
        var user_id_hidden = $("input[name='user_id_hidden']").val();
        var is_default = '0';
        if ($('#is_default').is(":checked")) {
            is_default = '1';   // it is checked
        }

        $.ajax({
            type: 'post',
            url: '/MasterData/category-Ajax',
            data: {
                class: 'CategoryTypeAjax',
                action: "insert",
                category_type: category_type,
                desc: desc,
                user_id_hidden:user_id_hidden,
                is_default:is_default
            },
            success: function (response) {

                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 600){
                    toastr.warning(response.message);
                }
                if (response.status == 'success' && response.code == 200) {

                    $('#categoryType-table').trigger('reloadGrid');
                    $("#add_category_type").trigger("reset");
                    $("#add_unit_type_div").hide(500);


                    toastr.success(response.message);
                    onTop(true);

                }  else if(response.status == 'error' && response.code == 400){
                  //  $('.error').html('*This field is required');
                    $.each(response.data, function (key, value) {
                        $("#"+key).html(value);
                    });
                }
                else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Category already exists!')
                        toastr.warning(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text(value);
                    });
                }
                else if (response.status == 'error' && response.code == 503) {
                    toastr.warning(response.message);
                }

            },
            error: function (data) {
            }
        });
    }

    function getcategoryType(id) {
        $.ajax({
            type: 'post',
            url: '/MasterData/category-Ajax',
            data: {
                class: "CategoryTypeAjax",
                action: "getcategoryType",
                cuser_id: id
            },
            success: function (response) {

                var response = JSON.parse(response);

                $("#add_category_type input[name='category_type']").val(response.category);
                $("#add_category_type textarea[name='description']").val(response.description);
                if (response.is_default==1) {
                    $('#is_default').prop('checked', true);
                 }
                else {
                    $("#is_default").prop('checked', false);
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("click", "#saveBtns", function (e) {
        e.preventDefault();
       /* var catName = $("input[name='category_type']").val();

        $.ajax({
            type: 'post',
            url: '/MasterData/category-Ajax',
            data: {
                class: "CategoryTypeAjax",
                action: "getbootbox",

            },
            success: function (response) {

                var response = JSON.parse(response);
                if(response.length>0){
                    var alertMessage ="Currently " +response[0].category+" is set as default. Are you sure to change "+catName+" as default?";

                    bootbox.confirm(alertMessage, function (result) {
                        if (result == true) {
                            saveCategory();
                            console.log("true");
                        } else {
                            console.log("false");

                            //window.location.href = window.location.origin + '/Setting/ApexNewUser'
                        }

                    });

                }
                else {*/
                    saveCategory();
      /*          }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });*/
    });


    function update_category_status(id, action) {
        $.ajax({
            type: 'post',
            url: '/MasterData/category-Ajax',
            data: {
                class: "CategoryTypeAjax",
                action: "update_category_status",
                user_id: id,
                status_type: action,

            },
            success: function (response) {

                var response = JSON.parse(response);
                responseData = response.data;
                if (response.status == 'success' && response.code == 200) {

                if(responseData.status==1) {
                    toastr.success("Record Activated Successfully.");
                }
                else{
                    toastr.success("Record Deactivated Successfully.");
                }
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                else if(response.status == 'error' && response.code == 503){
                    toastr.error("A default value cannot be deactivated");
                }
                localStorage.setItem("Message", "Category Updated Successfully")
                localStorage.setItem("rowcolor", true)
                $('#categoryType-table').trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    function deletecategoryType(id) {
        $.ajax({
            type: 'post',
            url: '/MasterData/category-Ajax',
            data: {
                class: "CategoryTypeAjax",
                action: "deletecategoryType",
                user_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The record deleted successfully.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                else if(response.status == 'error' && response.code == 503){
                    toastr.error("A default value cannot be deleted");
                }
                $('#categoryType-table').trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("change", "#categoryType-table .select_options", function (e) {
        e.preventDefault();
        var action = this.value;
        var id = $(this).attr('data_id');
        var set_as_default= this.value;
        console.log(set_as_default);
        console.log("=============");
        var checkDelete = $('option[value="Delete"]', this).length;

        if (checkDelete == "0") {
            $("input[name='category_type']").attr("disabled", true);
        } else {
            $("input[name='category_type']").attr("disabled", false);
        }



        switch (action) {

            case "Edit":
                //Deactivate or Activate manage user
                headerDiv.innerText = "Edit Category Type";
                $('#categoryErr').text('');
                $('#descriptionErr').text('');
                $('#saveBtns').val('Update');
                $('.Reset_button').show();
                $('.categoryFormReset').hide();
                $("#add_category_type #user_id_hidden").val(id);
                $("#add_unit_type_div").show();
                $('#categoryType-table').trigger('reloadGrid');
                $('#eventType-table').trigger('reloadGrid');
                getcategoryType(id);

                break;
            case "Deactivate":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to " + action + " this record?", function (result) {
                    if (result == true) {
                        update_category_status(id, action);
                    } else {
                        window.location.href = window.location.origin + '/Maintenance/MaintenanceCategory'
                    }
                });
                break;
            case "Activate":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to " + action + " this record?", function (result) {
                    if (result == true) {
                        update_category_status(id, action);
                    } else {

                        window.location.href = window.location.origin + '/Maintenance/MaintenanceCategory'

                    }
                });
                break;
            case "Delete":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to " + action + " this record?", function (result) {
                    if (result == true) {
                        deletecategoryType(id);
                    } else {

                        window.location.href = window.location.origin + '/Maintenance/MaintenanceCategory'
                    }
                });
                break;
            default:

                window.location.href = base_url + '/Maintenance/MaintenanceCategory';

        }

        return false;
    });

    $(document).on('click','.categoryFormReset',function () {
        $('#add_category_type')[0].reset();
    });

    $(document).on('click','.Reset_button',function () {
       var id = $("#add_category_type #user_id_hidden").val();
        getcategoryType(id);
    });
});