$(document).on('click', '.cancelPopup', function () {
	$(this).parents('.add-popup').hide();
});


$(document).on('click', '.pop-add-icon', function () {
	var popup = $(this).attr('data-popup');
	$("#"+popup).show();
});


getInitialData();

function getInitialData()
{

	$.ajax({
		url:'/maintenance',
		type: 'POST',
		data: {

			"action": 'getInitialData',
			"class": 'Maintenance'
		},
		success: function (response) {
			var info = JSON.parse(response);
			$('.category').append(info.category);
			$('.color').append(info.colors);
			$('.item').append(info.items);




		}
	});



}


$(document).on('click',".userData",function(){
	var user_type =  $('input[name=userInfo]:checked').val();
	$.ajax({
		url:'/maintenance',
		type: 'POST',
		data: {

			"action": 'getUserData',
			"class": 'Maintenance',
			"user_type": user_type
		},
		success: function (response) {
			$(".getUserData").html(response);
		}
	});
});

$(document).on('click',".userInfouserInfo",function(){
	var user_type =  $('input[name=userInfo]:checked').val();
	$(".getUserData").html('');
});

$(document).on("click",".addSingleData",function(){

	var value = $(this).attr('data-value');
	var fieldValue = $("#"+value).val();
	var table = $(this).attr('data-table');
	var column = $(this).attr('data-cell');
	var select = $(this).attr('data-select');
	var hide = $(this).attr('data-hide');

	var error = $(this).attr('data-error');
	if(fieldValue=="")
	{

		$("."+error).html('Please fill required field');
	}
	else
	{

		$.ajax({
			url:'/maintenance',
			type: 'POST',
			data: {

				"action": 'addSingleData',
				"class": 'Maintenance',
				"table": table,
				"column": column,
				"fieldValue": fieldValue,
			},
			success: function (response) {
				var info = JSON.parse(response);
				var option = "<option value="+info.id+">"+info.value+"</option>";

				$("#"+select).append(option);
				$("#"+value).val('');


				$(this).parents('.add-popup').hide();
				$("."+hide).hide();
				$("#"+select).val(info.id);

				toastr.success('Data Added Successfully.');




			}
		});


	}


});



$(document).on("keyup",".customValidatePortfoio",function(){
	$(".customErrorCategory").html('');

});

$(document).on("click",".getUserInfo",function(){
	var user_id = $(this).attr('data-user_id');
	var user_type = $(this).attr('data-user_type');
	$.ajax({
		url:'/maintenance',
		type: 'POST',
		data: {
			"action": 'getUserInfo',
			"class": 'Maintenance',
			"user_type": user_type,
			"user_id": user_id
		},
		success: function (response) {
			var info = JSON.parse(response);
			console.log(info);
			$(".full_name,.userData").val(info.user_name);
			$(".user_address").val(info.address);
			$(".user_email").val(info.email);
			$(".user_phone").val(info.phone_number);

			//$(".user_property").val(info.property_id);
			$(".user_id").val(user_id);
			if(user_type=='2')
			{
				$(".user_property").val(info.property_id);
				$(".user_unit").val(info.unit_number);
				$(".property_id").val(info.property_id);
			}
			else if(user_type=='4')
			{
				$(".user_property").val(info.property_id);
			}
			else if(user_type=='1')
			{
				$(".user_property").val("");
			}
			$(".getUserData").html('');
		}
	});
});

$(document).ready(function(){
	var type =  $(".type").val();
	if(type="F")
	{
		$(".found_label").html("Found");
	}
	addCalanderDate();
	getItemCounts();
	getItemLists();
	getLostItemList('All');
	getFindItemList('All');
	getClaimedItemList('All');
	getUnclaimedItemList('All');
	getProperties();
	getMaintenanceFiles('All');
	getNotesData();

	var type = $(".type").val();

	$(".readOnlyProperty").prop("disabled", true);

	if(type=='F')
	{

		$(".markedAsMatched").attr("disabled", true);
	}


	$('.readOnlyProperty').attr('readonly', true);
	$('.itemNumberProperty').attr('readonly', true);


	$('.calander').datepicker({
		yearRange: '1919:2030',
		changeMonth: true,
		changeYear: true,
		dateFormat: jsDateFomat
	});





	$(document).on('click','#add_libraray_file',function(){
		$('#file_library').val('');
		$('#file_library').trigger('click');
	});




	function getProperties()
	{
		$.ajax({
			url:'/maintenance',
			type: 'POST',
			data: {

				"action": 'getProperties',
				"class": 'Maintenance'
			},
			success: function (response) {
				var info = JSON.parse(response);
				var htmlData = "";

				$.each(info.property, function (key, value) {
					htmlData += "<option value="+value.id+">"+value.property_name+"</option>";
				});
				$('.user_property').html(htmlData);
				setTimeout(function(){
					$('.lostItemProperty').html(htmlData);
					$('.lostItemProperty').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
					$('.dashboardProperty').html(htmlData);
					$('.dashboardProperty').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
				},500);
			}
		});
	}

	var file_library = [];
	$(document).on('change','#file_library',function(){
		file_library = [];
		$.each(this.files, function (key, value) {
			var type = value['type'];
			var size = isa_convert_bytes_to_specified(value['size'], 'k');
			if(size > 1030) {
				toastr.warning('Please select documents less than 1 mb!');
			} else {
				size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
				if (type == 'image/jpeg' || type == 'image/jpg' || type == 'image/png') {
					file_library.push(value);
					var src = '';
					var reader = new FileReader();
					reader.onload = function (e) {
						if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
							src = upload_url + 'company/images/excel.png';
						} else if (type == 'application/pdf') {
							src = upload_url + 'company/images/pdf.png';
						} else if (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
							src = upload_url + 'company/images/word_doc_icon.jpg';
						} else if (type == 'text/plain') {
							src = upload_url + 'company/images/notepad.jpg';
						} else if (type == 'text/xml') {
							src = upload_url + 'company/images/notepad.jpg';
						} else {
							src = e.target.result;
						}
						$("#file_library_uploads").append(
							'<div class="row" style="margin:20px">' +
							'<div class="col-sm-12 img-upload-library-div">' +
							'<div class="col-sm-3"><img class="img-upload-tab' + key + '" width=100 height=100 src=' + src + '></div>' +
							'<div class="col-sm-3 show-library-list-imgs-name' + key + '">' + value['name'] + '</div>' +
							'<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data-id="' + value['size'] + '">' +
							'<div class="col-sm-3 show-library-list-imgs-size' + key + '">' + size + '</div>' +
							'<div class="col-sm-3"><span id=' + key + ' class="delete_pro_img cursor"><button type="button" class="btn-warning">Delete</button></span></div></div></div>'
						);
					};
					reader.readAsDataURL(value);
				} else {
					toastr.warning('Please select file with .jpg | .jpeg | .png extension only!');
				}
			}
		});
	});


	function isa_convert_bytes_to_specified(bytes, to) {
		var formulas =[];
		formulas['k']= (bytes / 1024).toFixed(1);
		formulas['M']= (bytes / 1048576).toFixed(1);
		formulas['G']= (bytes / 1073741824).toFixed(1);
		return formulas[to];
	}



	$(document).on('click','#remove_library_file',function(){
		bootbox.confirm("Do you want to remove all files?", function (result) {
			if (result == true) {
				toastr.success('The record deleted successfully.');
				$('#file_library_uploads').html('');
				$('#file_library').val('');
				imgArray = [];
			}
		});
	});

	$('.setinput_readonly').removeClass('capital');
	var itemtype = $('#itemtype').val();
	if(itemtype != '' && itemtype != undefined){
		pageRedirect(itemtype);
	}
});

function addCalanderDate(){
	$(".calander").datepicker({
		dateFormat: jsDateFomat,
	}).datepicker("setDate", new Date());
}

$('input[type="submit"]').click(function () {
	var a=$(this).attr('id');
	$('.onsubmit').val(a);

});

$(document).on("click","#cancel_add",function(){
	bootbox.confirm("Do you want to cancel this action now?", function (result) {
		if (result == true) {
			window.location = window.origin+"/LostAndFound/LostAndFound";
		}
	});
});

$("#addLostFoundData").validate({
	rules: {
		lost_item_name: {
			required:true
		},
		loation_lost: {
			required:true
		},
		brand_description: {
			required:true
		}
		,
		item: {
			required:true
		},
		category: {
			required:true
		} ,
		user_email: {
			required:true
		},
		user_property: {
			required:true
		},
		lost_date: {
			required:true
		}
	},
	submitHandler: function (e) {
		var form = $('#addLostFoundData')[0];
		var formData = new FormData(form);
		formData.append('action','addLostFound');
		formData.append('class','Maintenance');
		$.ajax({
			url:'/maintenance',
			type: 'POST',
			data: formData,
			success: function (response) {
				var response = JSON.parse(response);
				if(response.status == "success"){
					var sub=$('.onsubmit').val();
					if(sub=='save_close'){
						toastr.success('Record saved successfully');
						var	type = response.type;

						window.location = window.origin+"/LostAndFound/LostAndFound?type="+type;
					}else if(sub=='save_add_another'){
						toastr.success('Record saved successfully');
						setTimeout(function(){
							window.location.href=window.location.href;
						}, 1500);
					}
				}
			},
			cache: false,
			contentType: false,
			processData: false
		});
	}
});
function pageRedirect(type){
	$('.dashboardtabs').find('.dashbrd').removeClass('active');
	$('.dashboardtabs').find('.items').addClass('active');

	$('#lostfound-one').hide();
	$('#lostfound-two').show();
	if(type == 'F'){
		$('.difftabs').find('.lost').removeClass('active');
		$('#lostfound-two .tab-content').find('.tab-pane').removeClass('active');
		$('.difftabs').find('.found').addClass('active');
		$('#lostfound-sub-two').addClass('active');
		var table_id = 'findItem-table';
		$('#lostfound-sub-two').trigger('click');
	} else{
		var table_id = 'lostItem-table';

		//$('#lostfound-sub-two').hide();

		//$('#collapseTwo').hide();
	}
	setTimeout(function(){
		jQuery('#'+table_id).find('tr:eq(1)').find('td:eq(1)').addClass("green_row_left");
		jQuery('#'+table_id).find('tr:eq(1)').find('td').last().addClass("green_row_right");
	}, 2000);
}
$("#editLostFoundData").validate({
	rules: {
		lost_item_name: {
			required:true
		},
		loation_lost: {
			required:true
		},
		brand_description: {
			required:true
		}
		,
		item: {
			required:true
		},
		category: {
			required:true
		} ,
		user_email: {
			required:true
		},
		user_property: {
			required:true
		}
	},
	submitHandler: function (e) {
		var form = $('#editLostFoundData')[0];
		var formData = new FormData(form);
		formData.append('action','editLostFound');
		formData.append('class','Maintenance');

		if($(".markedAsMatched").prop("checked")==true)
		{
			if($(".matchedCalander").val()=='')
			{
				toastr.error('Please fill matched Date');
				return false;
			}
		}
		$.ajax({
			url:'/maintenance',
			type: 'POST',
			data: formData,
			success: function (response) {
				var response = JSON.parse(response);
				var type = $('.type').val();
				if(response.status=='false')
				{
					toastr.warning(response.message);
					return false;
				}
				else
				{
					toastr.success(response.message);
				}
				setTimeout(function(){
					window.location = window.origin+"/LostAndFound/LostAndFound?type="+type;
				//	window.location = window.origin+"/LostAndFound/LostAndFound"
				}, 400);
			},
			cache: false,
			contentType: false,
			processData: false
		});
	}
});

$(document).on('click','.delete_pro_img',function(){
	$(this).parent().parent().parent('.row').remove();
});

$.ajax({
	url:'/maintenance',
	type: 'POST',
	data: {
		"action": 'getProperty',
		"class": 'Maintenance'
	},
	success: function (response) {
		//$('.getPropertyData').html(response);
		$('.user_property').html(response);
	}
});


/*$(document).on("change",".user_property",function(){

    $.ajax({
        url:'/maintenance',
        type: 'POST',
        data: {
            "action": 'getProperty',
            "class": 'Maintenance'
        },
        success: function (response) {
            $('.getPropertyData').html(response);

        }
    });

});*/


$(document).on("click",".getPropertyId",function(){
	var name =  $(this).attr('data-name');
	var id =  $(this).attr('data-id');

	$(".user_property").val(name);
	$(".property_id").val(id);
	$(".getPropertyData").html("");
	$(".user_unit").val();
});

function getItemCounts()
{
	$.ajax({
		url:'/maintenance',
		type: 'POST',
		data: {
			"action": 'getItemCounts',
			"class": 'Maintenance'
		},
		success: function (response) {
			var info = JSON.parse(response);
			$(".lostItemCount").html(info.lost);
			$(".foundItemCount").html(info.find);
			$(".claimedItemCount").html(info.claimed);
			$(".unclaimedItemCount").html(info.unclaimed);
		}
	});
}

function getItemLists()
{
	$.ajax({
		url:'/maintenance',
		type: 'POST',
		data: {
			"action": 'getItemLists',
			"class": 'Maintenance'
		},
		success: function (response) {
			var info = JSON.parse(response);
			var path = upload_url;
			var htmldata = '';
			var htmlData2 = '';
			$.each(info.lost, function (key, value) {

				htmldata += "<tr><td>"+value.lost_date+"</td><td>"+value.category_name+"</td><td>"+value.property_name+"</td><td><a href=editLostFoundItems?id="+value.id+"><img src="+path+"company/images/right-arrow.png></a></td></tr>";
			});

			$.each(info.find, function (key, value) {

				htmlData2 += "<tr><td>"+value.created_date+"</td><td>"+value.category_name+"</td><td>"+value.property_name+"</td><td><a href=editLostFoundItems?id="+value.id+"><img src="+path+"company/images/right-arrow.png></a></td></tr>";
			});
			$(".getLostItemList").html(htmldata);
			$(".getFindItemList").html(htmlData2);
		}
	});

}

function getLostItemList(status) {
	var sortby = 'maintenance_lost_found.updated_at';
	var date = $.datepicker.formatDate(jsDateFomat, new Date());
	var d1 = $.datepicker.parseDate('mm/dd/yy', date);
	var today =d1.getFullYear()+"-"+(d1.getMonth()+1)+"-"+d1.getDate();

	var table = 'maintenance_lost_found';
	var columns = ['id','Item Number','Description','Category','Lost Date','lost_date','lost_time','Expiration Date','Status','Full View'];
	var select_column = ['Edit','Delete','Cancel'];
	var joins = [{table:'maintenance_lost_found',column:'category_id',primary:'id',on_table:'maintenance_category',as :'tp'}];
	var conditions = ["eq","bw","ew","cn","in"];
	var extra_where = [{column:'type', value :'L',condition:'='},{column:'matched_status', value :'False',condition:'='}];
	var extra_columns = [];
	var columns_options = [
		{name:'id',index:'id', width:90,align:"left",hidden:true,searchoptions: {sopt: conditions},table:table},
		{name:'Item Number',index:'item_number', width:200,align:"left",searchoptions: {sopt: conditions},table:table},
		{name:'Description',index:'description', width:250,align:"left",searchoptions: {sopt: conditions},table:table},
		{name:'Category',index:'category_name', width:150,align:"center",searchoptions: {sopt: conditions},table:'tp'},
		{name:'Lost Date',index:'lost_date', width:150,change_type:'date',searchoptions: {sopt: conditions},table:table,formatter:getLostDateFormatter},
		{name:'lost_date',index:'lost_date',hidden:true, width:180,change_type:'date',searchoptions: {sopt: conditions},table:table},
		{name:'lost_time',index:'lost_date',hidden:true, width:180,change_type:'time',searchoptions: {sopt: conditions},table:table},
	    {name:'Expiration Date',index:'expiration_date', width:200,change_type:'date',searchoptions: {sopt: conditions},table:table},
		{name:'Status',index:'status', width:200,align:"center",searchoptions: {sopt: conditions},table:table,formatter:getstatusFormatter},
		{name:'Full View',index:'status', width:200,searchoptions: {sopt: conditions},table:table,formatter:getFullViewFormatter},

	];
	var ignore_array = [];
	jQuery("#lostItem-table").jqGrid({
		url: '/List/jqgrid',
		datatype: "json",
		height: '100%',
		autowidth: true,
		colNames: columns,
		colModel: columns_options,
		pager: true,
		mtype: "POST",
		postData: {
			q: 1,
			class: 'jqGrid',
			action: "listing_ajax",
			table: table,
			select: select_column,
			columns_options: columns_options,
			status: status,
			ignore:ignore_array,
			joins:joins,
			extra_columns:extra_columns,
			extra_where:extra_where,
			deleted_at:'true'
		},
		viewrecords: true,
		sortname: sortby,
		sortorder: "desc",
		sorttype:'date',
		sortIconsBeforeText: true,
		headertitles: true,
		rowNum: '5',
		rowList: [5, 10, 20, 30, 50, 100, 200],
		caption: "List of Lost Items",
		pginput: true,
		pgbuttons: true,
		navOptions: {
			edit: false,
			add: false,
			del: false,
			search: true,
			filterable: true,
			refreshtext: "Refresh",
			reloadGridOptions: {fromServer: true}
		}
	}).jqGrid("navGrid",
		{
			edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
		},
		{}, // edit options
		{}, // add options
		{}, //del options
		{top:10,left:200,drag:true,resize:false} // search options
	);

}

// $(document).on('change','.common_lost',function(){
//     searchFilters();
// });


function lostFilters(){
	var grid = $("#lostItem-table"),f = [];
	var lost_property_common = ($('#lost_property_common').val().length == 0)?'all':$('#lost_property_common').val();
	if(lost_property_common != 'all'){
		var property = '';
		$.each(lost_property_common, function (key, value) {
			property += value+',';
		});
		lost_property_common = property.replace(/,\s*$/, "");
	}
	var status = $('#lost_status_common').val();
	var field_common = ($('#lost_common_text').val() == '')?'all':$('#lost_common_text').val();
	f.push({field: "maintenance_lost_found.property_id", op: "in", data: lost_property_common},{field: "maintenance_lost_found.status", op: "eq", data: status},{field: "tp.category_name", op: "cn", data: field_common, con:'OR'},{field: "maintenance_lost_found.description", op: "cn", data: field_common, con:'OR'},{field: "maintenance_lost_found.item_number", op: "cn", data: field_common});
	grid[0].p.search = true;
	$.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
	grid.trigger("reloadGrid",[{page:1,current:true}]);
}


$(document).on("click",".lostSearch",function(){
	lostFilters();
});





function foundFilters(){
	var grid = $("#findItem-table"),f = [];
	var found_property_common = ($('#found_property_common').val().length == 0)?'all':$('#found_property_common').val();
	if(found_property_common != 'all'){
		var property = '';
		$.each(found_property_common, function (key, value) {
			property += value+',';
		});
		found_property_common = property.replace(/,\s*$/, "");
	}
	var status = $('#found_status_common').val();
	var field_common = ($('#found_common_text').val() == '')?'all':$('#found_common_text').val();
	f.push({field: "maintenance_lost_found.property_id", op: "in", data: found_property_common},{field: "maintenance_lost_found.status", op: "eq", data: status},{field: "tp.category_name", op: "cn", data: field_common, con:'OR'},{field: "maintenance_lost_found.description", op: "cn", data: field_common, con:'OR'},{field: "maintenance_lost_found.item_number", op: "cn", data: field_common});
	grid[0].p.search = true;
	$.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
	grid.trigger("reloadGrid",[{page:1,current:true}]);
}


$(document).on("click",".foundSearch",function(){
	foundFilters();
});




function claimedFilters(){
	var grid = $("#claimedItem-table"),f = [];
	var claimed_property_common = ($('#claimed_property_common').val().length == 0)?'all':$('#claimed_property_common').val();
	if(claimed_property_common != 'all'){
		var property = '';
		$.each(claimed_property_common, function (key, value) {
			property += value+',';
		});
		claimed_property_common = property.replace(/,\s*$/, "");
	}
	var status = $('#claimed_status_common').val();
	var field_common = ($('#claimed_common_text').val() == '')?'all':$('#claimed_common_text').val();
	f.push({field: "maintenance_lost_found.property_id", op: "in", data: claimed_property_common},{field: "maintenance_lost_found.status", op: "eq", data: status},{field: "tp.category_name", op: "cn", data: field_common, con:'OR'},{field: "maintenance_lost_found.description", op: "cn", data: field_common, con:'OR'},{field: "maintenance_lost_found.item_number", op: "cn", data: field_common});
	grid[0].p.search = true;
	$.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
	grid.trigger("reloadGrid",[{page:1,current:true}]);
}


$(document).on("click",".claimedSearch",function(){
	claimedFilters();
});




function unclaimedFilters(){
	var grid = $("#unclaimedItem-table"),f = [];
	var unclaimed_property_common = ($('#unclaimed_property_common').val().length == 0)?'all':$('#unclaimed_property_common').val();
	if(unclaimed_property_common != 'all'){
		var property = '';
		$.each(unclaimed_property_common, function (key, value) {
			property += value+',';
		});
		found_property_common = property.replace(/,\s*$/, "");
	}
	var status = $('#unclaimed_status_common').val();
	var field_common = ($('#unclaimed_common_text').val() == '')?'all':$('#unclaimed_common_text').val();
	f.push({field: "maintenance_lost_found.property_id", op: "in", data: unclaimed_property_common},{field: "maintenance_lost_found.status", op: "eq", data: status},{field: "tp.category_name", op: "cn", data: field_common, con:'OR'},{field: "maintenance_lost_found.description", op: "cn", data: field_common, con:'OR'},{field: "maintenance_lost_found.item_number", op: "cn", data: field_common});
	grid[0].p.search = true;
	$.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
	grid.trigger("reloadGrid",[{page:1,current:true}]);
}


$(document).on("click",".unclaimedSearch",function(){
	unclaimedFilters();
});

function getFindItemList(status) {

	var date = $.datepicker.formatDate(jsDateFomat, new Date());
	var d1 = $.datepicker.parseDate('mm/dd/yy', date);
	var today =d1.getFullYear()+"-"+(d1.getMonth()+1)+"-"+d1.getDate();

	var table = 'maintenance_lost_found';
	var columns = ['id','Item Number','Description','Category','Lost Date','lost_date','lost_time','Expiration Date','Status','Full View'];
	var select_column = ['Edit','Delete','Cancel'];
	var joins = [{table:'maintenance_lost_found',column:'category_id',primary:'id',on_table:'maintenance_category',as :'tp'}];
	var conditions = ["eq","bw","ew","cn","in"];
	var extra_where = [{column:'type', value :'F',condition:'='},{column:'matched_status', value :'False',condition:'='}];
	var extra_columns = [];
	var columns_options = [
		{name:'id',index:'id', width:120,align:"left",hidden:true,searchoptions: {sopt: conditions},table:table},
		{name:'Item Number',index:'item_number', width:180,align:"left",searchoptions: {sopt: conditions},table:table},
		{name:'Description',index:'description', width:250,align:"left",searchoptions: {sopt: conditions},table:table},
		{name:'Category',index:'category_name', width:180,align:"center",searchoptions: {sopt: conditions},table:'tp'},
		{name:'Lost Date',index:'lost_date', width:180,change_type:'date',searchoptions: {sopt: conditions},table:table,formatter:getLostDateFormatter},
		{name:'lost_date',index:'lost_date',hidden:true, width:180,change_type:'date',searchoptions: {sopt: conditions},table:table},
		{name:'lost_time',index:'lost_date',hidden:true, width:180,change_type:'time',searchoptions: {sopt: conditions},table:table},
		{name:'Expiration Date',index:'expiration_date', width:180,change_type:'date',searchoptions: {sopt: conditions},table:table},
		{name:'Status',index:'status', width:150,align:"center",searchoptions: {sopt: conditions},table:table,formatter:getstatusFormatter},
		{name:'Full View',index:'status', width:200,searchoptions: {sopt: conditions},table:table,formatter:getFullViewFormatter},
	];
	var ignore_array = [];
	jQuery("#findItem-table").jqGrid({
		url: '/List/jqgrid',
		datatype: "json",
		height: '100%',
		autowidth: true,
		colNames: columns,
		colModel: columns_options,
		pager: true,
		mtype: "POST",
		postData: {
			q: 1,
			class: 'jqGrid',
			action: "listing_ajax",
			table: table,
			select: select_column,
			columns_options: columns_options,
			status: status,
			ignore:ignore_array,
			joins:joins,
			extra_columns:extra_columns,
			extra_where:extra_where,
			deleted_at:'true'
		},
		viewrecords: true,
		sortname: 'maintenance_lost_found.updated_at',
		sortorder: "desc",
		sorttype:'date',
		sortIconsBeforeText: true,
		headertitles: true,
		rowNum: '5',
		rowList: [5, 10, 20, 30, 50, 100, 200],
		caption: "List of Found Items",
		pginput: true,
		pgbuttons: true,
		navOptions: {
			edit: false,
			add: false,
			del: false,
			search: true,
			filterable: true,
			refreshtext: "Refresh",
			reloadGridOptions: {fromServer: true}
		}
	}).jqGrid("navGrid",
		{
			edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
		},
		{}, // edit options
		{}, // add options
		{}, //del options
		{top:10,left:200,drag:true,resize:false} // search options
	);

}


function getLostDateFormatter(cellValue, options, rowObject)
{
	if(rowObject!==undefined)
	 {
       var date =  rowObject.lost_date+' '+rowObject.lost_time;
       return date;
	 }

}

function getFullViewFormatter(cellValue, options, rowObject)
{
	if(rowObject!==undefined)
	{
		var path = upload_url;
		return "<a href=editLostFoundItems?id="+rowObject.id+"><img src="+path+"company/images/expand.png></a>";


	}
}

function getstatusFormatter(cellValue, options, rowObject)
{
	if(rowObject!==undefined)
	{
		var status =  rowObject.Status;
		if(status==1)
		{
			return "Active";
		}
		else
		{
			return "Inactive";
		}
	}
}

function getClaimedItemList(status) {
	var table = 'maintenance_lost_found';
	var columns = ['id','Item Number','Description','Category','Lost Date','lost_date','lost_time','Found Date','Marked as Matched','Match Date','Matched and Returned','Returned Date','Status','Full View'];
	var select_column = ['Edit','Delete','Cancel'];
	var joins = [{table:'maintenance_lost_found',column:'category_id',primary:'id',on_table:'maintenance_category',as :'tp'}];
	var conditions = ["eq","bw","ew","cn","in"];
	var extra_where = [{column:'type', value :'F',condition:'='},{column:'matched_status', value :'True',condition:'='}];
	var extra_columns = [];
	var columns_options = [
		{name:'id',index:'id', width:90,align:"left",hidden:true,searchoptions: {sopt: conditions},table:table},
		{name:'Item Number',index:'item_number', width:120,align:"left",searchoptions: {sopt: conditions},table:table},
		{name:'Description',index:'description', width:200,align:"left",searchoptions: {sopt: conditions},table:table},
		{name:'Category',index:'category_name', width:90,align:"center",searchoptions: {sopt: conditions},table:'tp'},
		{name:'Lost Date',index:'lost_date', width:120,change_type:'date',searchoptions: {sopt: conditions},table:table,formatter:getLostDateFormatter},
		{name:'lost_date',index:'lost_date',hidden:true, width:180,change_type:'date',searchoptions: {sopt: conditions},table:table},
		{name:'lost_time',index:'lost_date',hidden:true, width:180,change_type:'time',searchoptions: {sopt: conditions},table:table},
        {name:'Found Date',index:'created_at', width:120,change_type:'date',searchoptions: {sopt: conditions},table:table},
		{name:'Marked as Matched',index:'matched_status', width:120,align:"left",searchoptions: {sopt: conditions},table:table},
		{name:'Match Date',index:'match_date', width:120,change_type:'date',searchoptions: {sopt: conditions},table:table},
		{name:'Matched and Returned',index:'matched_returned', width:120,searchoptions: {sopt: conditions},table:table},
		{name:'Returned Date',index:'return_date', width:120,change_type:'date',searchoptions: {sopt: conditions},table:table},
		{name:'Status',index:'status', width:90,align:"center",searchoptions: {sopt: conditions},table:table,formatter:getstatusFormatter},
		{name:'Full View',index:'status', width:120,searchoptions: {sopt: conditions},table:table,formatter:getFullViewFormatter},

	];
	var ignore_array = [];
	jQuery("#claimedItem-table").jqGrid({
		url: '/List/jqgrid',
		datatype: "json",
		height: '100%',
		autowidth: true,
		colNames: columns,
		colModel: columns_options,
		pager: true,
		mtype: "POST",
		postData: {
			q: 1,
			class: 'jqGrid',
			action: "listing_ajax",
			table: table,
			select: select_column,
			columns_options: columns_options,
			status: status,
			ignore:ignore_array,
			joins:joins,
			extra_columns:extra_columns,
			extra_where:extra_where,
			deleted_at:'true'
		},
		viewrecords: true,
		sortname: 'maintenance_lost_found.updated_at',
		sortorder: "desc",
		sorttype:'date',
		sortIconsBeforeText: true,
		headertitles: true,
		rowNum: '5',
		rowList: [5, 10, 20, 30, 50, 100, 200],
		caption: "List of Claimed Items",
		pginput: true,
		pgbuttons: true,
		navOptions: {
			edit: false,
			add: false,
			del: false,
			search: true,
			filterable: true,
			refreshtext: "Refresh",
			reloadGridOptions: {fromServer: true}
		}
	}).jqGrid("navGrid",
		{
			edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
		},
		{}, // edit options
		{}, // add options
		{}, //del options
		{top:10,left:200,drag:true,resize:false} // search options
	);

}

function getUnclaimedItemList(status) {

	var date = $.datepicker.formatDate(jsDateFomat, new Date());
	var d1 = $.datepicker.parseDate('mm/dd/yy', date);
	var today =d1.getFullYear()+"-"+(d1.getMonth()+1)+"-"+d1.getDate();

	var table = 'maintenance_lost_found';
	var columns = ['id','Item Number','Description','Category','Lost Date','lost_date','lost_time','Found Date','Release Date','Status','Release Method','Full View'];
	var select_column = ['Edit','Delete','Cancel'];
	var joins = [{table:'maintenance_lost_found',column:'category_id',primary:'id',on_table:'maintenance_category',as :'tp'}];
	var conditions = ["eq","bw","ew","cn","in"];
	var extra_where = [{column:'type', value :'F',condition:'='},{column:'matched_status', value :'False',condition:'='},{column:'expiration_date', value :today,condition:'<'}];
	var extra_columns = [];
	var columns_options = [
		{name:'id',index:'id', width:90,align:"left",hidden:true,searchoptions: {sopt: conditions},table:table},
		{name:'Item Number',index:'item_number', width:120,align:"left",searchoptions: {sopt: conditions},table:table},
		{name:'Description',index:'description', width:200,align:"left",searchoptions: {sopt: conditions},table:table},
		{name:'Category',index:'category_name', width:120,align:"center",searchoptions: {sopt: conditions},table:'tp'},
		{name:'Lost Date',index:'lost_date', width:120,change_type:'date',searchoptions: {sopt: conditions},table:table,formatter:getLostDateFormatter},
		{name:'lost_date',index:'lost_date',hidden:true, width:180,change_type:'date',searchoptions: {sopt: conditions},table:table},
		{name:'lost_time',index:'lost_date',hidden:true, width:180,change_type:'time',searchoptions: {sopt: conditions},table:table},
        {name:'Found Date',index:'created_at', width:120,change_type:'date',searchoptions: {sopt: conditions},table:table},
		{name:'Release Date',index:'release_date', width:120,align:"left",searchoptions: {sopt: conditions},table:table},
		{name:'Status',index:'status', width:120,align:"center",searchoptions: {sopt: conditions},table:table,formatter:getstatusFormatter},
		{name:'Release Method',index:'release_method', width:200,searchoptions: {sopt: conditions},table:table},
		{name:'Full View',index:'status', width:120,searchoptions: {sopt: conditions},table:table,formatter:getFullViewFormatter},
	];
	var ignore_array = [];
	jQuery("#unclaimedItem-table").jqGrid({
		url: '/List/jqgrid',
		datatype: "json",
		height: '100%',
		autowidth: true,
		colNames: columns,
		colModel: columns_options,
		pager: true,
		mtype: "POST",
		postData: {
			q: 1,
			class: 'jqGrid',
			action: "listing_ajax",
			table: table,
			select: select_column,
			columns_options: columns_options,
			status: status,
			ignore:ignore_array,
			joins:joins,
			extra_columns:extra_columns,
			extra_where:extra_where,
			deleted_at:'true'
		},
		viewrecords: true,
		sortname: 'maintenance_lost_found.updated_at',
		sortorder: "desc",
		sorttype:'date',
		sortIconsBeforeText: true,
		headertitles: true,
		rowNum: '5',
		rowList: [5, 10, 20, 30, 50, 100, 200],
		caption: "List of Unclaimed Items",
		pginput: true,
		pgbuttons: true,
		navOptions: {
			edit: false,
			add: false,
			del: false,
			search: true,
			filterable: true,
			refreshtext: "Refresh",
			reloadGridOptions: {fromServer: true}
		}
	}).jqGrid("navGrid",
		{
			edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
		},
		{}, // edit options
		{}, // add options
		{}, //del options
		{top:10,left:200,drag:true,resize:false} // search options
	);

}

function getitemDetails()
{
	var item_id = $(".item_id").val();
	$('.name').prop('readonly',true);
	$.ajax({
		url:'/maintenance',
		type: 'POST',
		data: {
			"action": 'getitemDetails',
			"class": 'Maintenance',
			"item_id":item_id
		},
		success: function (response) {
			var info = JSON.parse(response);


			$.each(info, function (key, value) {

				if(key=='type'){
					if(value=='L'){
						$('.found_label').html('Lost');
					}
				}
				if(value!="")
				{
					$('.' + key).val(value);
					if(key=='lost_date')
					{
						setTimeout(function(){
							$('.lost_date').val(value);


						},500);

					}
				}

			});



			if(info.matched_status=="False")
			{
				$('.markedAsMatched').prop('checked', false);
			}
			else if(info.matched_status=="True")
			{

				$('.markedAsMatched').prop('checked', true);
			}



			if(info.matched_returned=="False")
			{
				$('.matchedAsReturn').prop('checked', false);

			}
			else
			{

				$('.matchedAsReturn').prop('disabled',false);
				$('.matchedAsReturn').prop('checked', true);

			}










		}
	});




}

$(document).on("change",".markedAsMatched",function(){
	if(this.checked)
	{
		if($(".markedReleased").prop("checked")==true){
			toastr.warning('Please untick the Mark as Released');
			$('.markedAsMatched').prop('checked',false);
			return;
		}
		$('.matchedCalander').datepicker({
			yearRange: '1919:2030',
			changeMonth: true,
			changeYear: true,
			dateFormat: jsDateFomat
		});


		$('.matchWithF').prop('disabled',false);
		$('.matchedCalander').prop('disabled',false);

		$('.matchedAsReturn').prop('disabled',false);



		$('.returnDate').datepicker({
			yearRange: '1919:2030',
			changeMonth: true,
			changeYear: true,
			dateFormat: jsDateFomat
		});

	}
	else
	{


		$('.matchedCalander').val('');
		$('.matchedCalander').prop('disabled',true);
		$('.matchWithF').prop('disabled',true);
		$('.matchedAsReturn').prop('disabled',true);
		$('.returnDate').val('');
		$('.returnDate').prop('disabled',true);



	}

});


$(document).on("change",".matchedAsReturn",function(){

	if(this.checked)
	{
		$('.returnDate').prop('disabled',false);
	}
	else
	{
		$('.returnDate').prop('disabled',true);
	}
});

$(document).on("change",".editItem",function(){
	if(this.checked)
	{
		$(".readOnlyProperty").attr("readonly", false);
		$(".readOnlyProperty").prop("disabled", false);
		$('.lost_date').addClass('calander');

		$('.calander').datepicker({
			yearRange: '1919:2030',
			changeMonth: true,
			changeYear: true,
			dateFormat: jsDateFomat
		});
	}
	else
	{
		$(".readOnlyProperty").attr('disable',true);
		$(".readOnlyProperty").prop("disabled", true);
	}

});









function getMaintenanceFiles(status) {

	var item_id = $(".item_id").val();


	var table = 'maintenance_files';
	var columns = ['Id', 'Name', 'View','File_location','File_extension','type','Action'];
	var select_column = ['Edit','Delete','Send'];
	var conditions = ["eq","bw","ew","cn","in"];
	var extra_where = [{column:'record_id', value :item_id,condition:'='}];
	var extra_columns = [];
	var joins = [];
	var columns_options = [
		{name:'Id',index:'id', width:300,hidden:true,searchoptions: {sopt: conditions},table:table},
		{name:'Name',index:'filename', width:300,searchoptions: {sopt: conditions},table:table},
		{name:'View',index:'file_type', width:300,searchoptions: {sopt: conditions},table:table,formatter:fileFormatter},
		{name:'File_location',index:'file_location', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
		{name:'File_extension',index:'file_extension', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
		{name:'type',index:'file_extension', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
		{name:'Action',index:'file_type', width:300,searchoptions: {sopt: conditions},table:table,formatter:selectMaintanenceFormatter},
	];


	var ignore_array = [];
	jQuery("#itemFiles-table").jqGrid({
		url: '/List/jqgrid',
		datatype: "json",
		height: '100%',
		autowidth: true,
		colNames: columns,
		colModel: columns_options,
		pager: true,
		mtype: "POST",
		postData: {
			q: 1,
			class: 'jqGrid',
			action: "listing_ajax",
			table: table,
			select: select_column,
			columns_options: columns_options,
			status: status,
			ignore:ignore_array,
			joins:joins,
			extra_where:extra_where,
			extra_columns:extra_columns,
			deleted_at:'true'
		},
		viewrecords: true,
		sortname: 'updated_at',
		sortorder: "desc",
		sorttype:'date',
		sortIconsBeforeText: true,
		headertitles: true,
		rowNum: '5',
		rowList: [5, 10, 20, 30, 50, 100, 200],
		caption: "File Library",
		pginput: true,
		pgbuttons: true,
		navOptions: {
			edit: false,
			add: false,
			del: false,
			search: true,
			filterable: true,
			refreshtext: "Refresh",
			reloadGridOptions: {fromServer: true}
		}
	}).jqGrid("navGrid",
		{
			edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
		},
		{}, // edit options
		{}, // add options
		{}, //del options
		{top:0,left:400,drag:true,resize:false} // search options
	);
}



function fileFormatter(cellValue, options, rowObject)
{

	if(rowObject !== undefined) {
		var file_type =  rowObject.View;
		var location = rowObject.File_location;
		var path = upload_url+'company/'+location;
		var imageData = '';
		if(file_type == '1'){

			imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
		} else {
			if (rowObject.File_extension == 'xlsx') {
				src = upload_url + 'company/images/excel.png';
				imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
			} else if (rowObject.File_extension == 'pdf') {
				src = upload_url + 'company/images/pdf.png';
				imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
			} else if (rowObject.File_extension == 'docx') {
				src = upload_url + 'company/images/word_doc_icon.jpg';
				imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
			}else if (rowObject.File_extension == 'txt') {
				src = upload_url + 'company/images/notepad.jpg';
				imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
			}
		}
		return imageData;
	}
}




function selectMaintanenceFormatter(cellValue, options, rowObject)
{
	if(rowObject !== undefined) {
		/*	var file_type =  rowObject.View;
            var location = rowObject.File_location;
            var path = upload_url+'company/'+location;
            console.log(file_type);
            var imageData = '';
            if(file_type == '1'){

                imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
            } else {
                if (rowObject.File_extension == 'xlsx') {
                    src = upload_url + 'company/images/excel.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'pdf') {
                    src = upload_url + 'company/images/pdf.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'docx') {
                    src = upload_url + 'company/images/word_doc_icon.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }else if (rowObject.File_extension == 'txt') {
                    src = upload_url + 'company/images/notepad.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }
            }*/


		var html = "<select editable='1' class='form-control select_options' data-id='" + rowObject.Id + "'>"

		html += "<option value=''>Select</option>";
		html += "<option value='Delete'>Delete</option>";
		html += "<option value='Send'>Email</option>";


		html += "</select>";


	}
	return html;
}





$(document).on("change",".select_options",function(){


	var id = $(this).attr('data-id');
	var action =  $(this).val();

	if(action=="Delete")
	{
		bootbox.confirm({
			message: "Do you want to delete this record ?",
			buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
			callback: function (result) {
				if (result == true) {
					deleteTableData('maintenance_files',id);
					$('#itemFiles-table').trigger('reloadGrid');

				}

			}
		});



	}
	else if(action=="Send")
	{
		$('#sendMailModal').modal('toggle');
		var src = $(this).attr("data-src");
		var path = $(this).attr("data-path");

		var imageFile = '<a href="'+path+'" class="attachments"><img class="img-upload-tab" width="100" height="100" src="'+src+'"></a>';
		$(".attachmentFile").html(imageFile);
	}
});

function deleteTableData(tablename,id)
{
	$.ajax({
		url:'/editTenant?action=deleteRecords&class=EditTenant&id='+id+'&tablename='+tablename,
		type: 'GET',
		success: function (data) {
			var info = JSON.parse(data);
			toastr.success(info.message);
		},
	});
}
$(document).on('click','.addNoteForm',function() {
	$('.notes-timer-outer').show();
});

$(document).on('click','.addNoteButton',function(){
	var note = $(".itemNotes").val();
	if(note=='')
	{
		toastr.error("Please added note for item");
		return false;
	}
	var item_id = $(".item_id").val();
	$.ajax({
		url:'/maintenance',
		type: 'POST',
		data: {

			"action": 'addItemNote',
			"class": 'Maintenance',
			"note":note,
			"item_id":item_id
		},
		success: function (response) {
			toastr.success("Record Added Successfully");
			$(".itemNotes").val('');
			$('.notes-timer-outer').hide();
			$(".getNotesData").html(response);
		}
	});
});

$(document).on('click','.deleteNote',function(){


	var note_id = $(this).attr('data-id');
	var item_id = $(".item_id").val();
	var action = $(this).val();
	if(action == 'delete') {
		$.ajax({
			url: '/maintenance',
			type: 'POST',
			data: {

				"action": 'deleteItemNote',
				"class": 'Maintenance',
				"note_id": note_id,
				"item_id": item_id
			},
			success: function (response) {
				$(".getNotesData").html(response);
			}
		});
	}

});



$(document).on('click','.addNoteForm',function(){
	$(".noteDataForm").show();
});

function goBack() {
	window.history.back();
}

$(document).on("click","#showItems2",function(){

	$('#lost1').click();

	setTimeout(function(){
		$('#lost2').click();
	},500)

});

$(document).on("click","#showItems3",function(){

	$('#lost1').click();

	setTimeout(function(){
		$('#lost3').click();
	},500)

});

$(document).on("click","#showItems4",function(){

	$('#lost1').click();

	setTimeout(function(){
		$('#lost4').click();
	},500)

});

$(document).on("click",".addToRecepent",function(){
	$('#torecepents').modal('show');

});


$(document).on("change",".selectUsers",function(){
	var type = $(this).val();
	$.ajax({
		url:'/tenantPortal',
		type: 'POST',
		data: {
			"type": type,
			"action": 'getUsers',
			"class": 'TenantPortal'
		},
		success: function (response) {
			$(".userDetails").html(response);

		}
	});
});

$(document).on("click",".getEmails",function(){
	if(this.checked)
	{


		var email = $(this).attr('data-email');
		$('.to').tagsinput('add', email);

	}
	else
	{
		var email = $(this).attr('data-email');
		$('.to').tagsinput('remove', email);
	}


});


$(document).on('click','.delete_pro_img',function(){
	$(this).parent().parent().parent('.row').remove();
});




$(document).on("click",".addCcRecepent",function(){
	$('#ccrecepents').modal('show');

});

$(document).on("change",".selectCcUsers",function(){
	var type = $(this).val();
	$.ajax({
		url:'/tenantPortal',
		type: 'POST',
		data: {
			"type": type,
			"action": 'getCcUsers',
			"class": 'TenantPortal'
		},
		success: function (response) {
			$(".userCcDetails").html(response);

		}
	});



});




$(document).on("click",".getCcEmails",function(){
	if(this.checked)
	{


		var email = $(this).attr('data-email');
		$('.cc').tagsinput('add', email);

	}
	else
	{
		var email = $(this).attr('data-email');
		$('.cc').tagsinput('remove', email);
	}


});




$(document).on("click",".addBccRecepent",function(){
	$('#bccrecepents').modal('show');

});

$(document).on("change",".selectBccUsers",function(){
	var type = $(this).val();
	$.ajax({
		url:'/tenantPortal',
		type: 'POST',
		data: {
			"type": type,
			"action": 'getBccUsers',
			"class": 'TenantPortal'
		},
		success: function (response) {
			$(".userBccDetails").html(response);

		}
	});
});


$(document).on("click",".getBccEmails",function(){
	if(this.checked)
	{

		var email = $(this).attr('data-email');
		$('.bcc').tagsinput('add', email);

	}
	else
	{
		var email = $(this).attr('data-email');
		$('.bcc').tagsinput('remove', email);
	}


});

$("#sendEmail").validate({
	rules: {
		to: {
			required:true
		},
		cc: {
			required:true
		},
		body: {
			required:true
		},
	},
	submitHandler: function (e) {

		var tenant_id = $(".tenant_id").val();
		var form = $('#sendEmail')[0];
		var formData = new FormData(form);
		var path = $(".attachments").attr('href');
		var to = $(".to").val();
		var cc = $(".cc").val();
		var bcc = $(".bcc").val();



		formData.append('to_users',to);
		formData.append('cc_users',cc);
		formData.append('bcc_users',bcc);
		formData.append('action','sendMail');
		formData.append('class','TenantPortal');
		formData.append('path', path);


		$.ajax({
			url:'/tenantPortal',
			type: 'POST',
			data: formData,
			success: function (data) {
				info =  JSON.parse(data);
				if(info.status=="success"){

					toastr.success("Email has been sent successfully");
					setTimeout(function(){
						$('#sendMailModal').modal('toggle');
					},700);
				}
			},
			cache: false,
			contentType: false,
			processData: false
		});
	}
});

function getNotesData()
{
	var item_id = $(".item_id").val();
	$.ajax({
		url:'/maintenance',
		type: 'POST',
		data: {
			"action": 'getNotesData',
			"class": 'Maintenance',
			"item_id":item_id
		},
		success: function (response) {
			$(".getNotesData").html(response);
		}
	});
}

$(document).on('click','.markedReleased',function () {
	if($(".markedReleased").prop("checked")==true){
		$('.release_method').attr('disabled',false);
	} else {
		$('.release_method').attr('disabled',true);
	}
});

$(document).on('click','.editItem',function () {
	if($(".editItem").prop("checked")==true){
		$('.setinput_readonly').attr('readonly',false);
	} else {
		$('.setinput_readonly').attr('readonly',true);
	}
});

$(document).on("change",".matchedCalander",function() {
	var matchdate = $('.matchedCalander').val();
	console.log(matchdate);
	var	returndate = new Date(matchdate);
	$('.returnDate').datepicker({
		minDate: returndate,
	});
});

$(document).on('click','.dashbrd',function () {
	getItemLists();
	$('#lostfound-one').show();
	$('#lostfound-two').hide();
});

$(document).on('click','.items',function () {
	$('#lostfound-one').hide();
	$('#lostfound-two').show();
});

/*
$(document).on('click','#lost1',function () {
	$('#lostfound-sub-one').show();
});
*/










