
    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Active";
        else if(cellValue == '0')
            return "Inactive";
        else
            return '';
    }
    function statusFormatter1 (cellValue, options, rowObject){
        if (cellValue == 0)
            return "No";
        else if(cellValue == '1')
            return "Yes";
        else
            return '';
    }
    function statusFmatter1 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';

            if(rowObject['Status'] == '1' && rowObject['editable'] == '0')  select = ['Edit','Deactivate','Delete'];
            if(rowObject['Status'] == '1' && rowObject['editable'] == '1')  select = ['Edit','Deactivate'];
            if((rowObject['Status'] == '0' || rowObject.Status == '' )&& rowObject['editable'] == '0')  select = ['Edit','Activate','Delete'];
            if((rowObject['Status'] == '0' || rowObject.Status == '' ) && rowObject['editable'] == '1')  select = ['Edit','Activate'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }
    function statusFmatter2 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            if(rowObject['is_editable'] == '1')  select = ['Edit','Deactivate'];
            if(rowObject['is_editable'] == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    var base_url = window.location.origin;
    $(document).on("change", "#jqGridStatus", function (e) {
        var status = $(this).val();
        $('#priorityType-table').jqGrid('GridUnload');

        jqGrid(status,true);

    });


    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');
    function jqGrid(status, deleted_at) {
        var table = 'company_priority_type';
        var columns = ['Priority','Description', 'Set as default', 'Status','editable', 'Action'];
        if (status===0) {
            var select_column = ['Edit','Deactivate','Delete'];
            console.log(status);
            console.log("-------");
        }else {
            var select_column = ['Edit','Activate','Delete'];
            console.log(status);
            console.log("========");
        }
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];

        var columns_options = [

            {name:'Priority',index:'priority', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Description',index:'description', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Set as default',index:'is_default', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter1},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'editable',index:'is_editable', hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},

        ];
        var ignore_array = [];
        jQuery("#priorityType-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_priority_type",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Maintenance Priority",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:200,left:200,drag:true,resize:false} // search options
        );
    }

    $(document).ready(function () {
        var base_url = window.location.origin;

        var status =  localStorage.getItem("active_inactive_status");
        console.log(status);


        /** Show add new unit type div on add new button click */
        $(document).on('click','#addUnitTypeButton',function () {
            $('#priority_type').prop('disabled', false);
            $('#saveBtns').val('Save');
            $("#user_id_hidden").val('');
            $('#unit_type').val('').prop('disabled', false);
            $('#description').val('');
            $("#priority_type").val('');
            $('#priorityErr').text('');
            $('#descriptionErr').text('');
            headerDiv.innerText = "Add Priority Type";
            $('#is_default').prop('checked', false);
            $('#saveBtnId').val('Save');
            $('#add_unit_type_div').show(500);
        });

        /** Show import excel div on import excel button click */
        $(document).on('click','#importUnitTypeButton',function () {
            $("#import_file-error").text('');
            $("#import_file").val('');
            $('#import_unit_type_div').show(500);
        });

        /** Hide add new unit type div on cancel button click */
        $(document).on("click", "#add_unit_cancel_btn", function (e) {
            e.preventDefault();
            bootbox.confirm("Do you want to cancel this action now?", function (result) {
                if (result == true) {
                    $("#add_unit_type_div").hide(500);
                } else {
                }
            });
        });



        /** Hide import excel div on cancel button click */
        $(document).on("click", "#import_unit_cancel_btn", function (e) {
            bootbox.confirm("Do you want to cancel this action now?", function (result) {
                if (result == true) {
                    $("#import_unit_type_div").hide(500);
                } else {
                }
            });
        });


        /**
         * jqGrid function to format status
         * @param cellValue
         * @param options
         * @param rowObject
         * @returns {string}
         */
        function statusFormatter (cellValue, options, rowObject){
            if (cellValue == 1)
                return "Active";
            else if(cellValue == '0')
                return "Inactive";
            else
                return '';
        }

        /**
         * jqGrid function to format is_default column
         * @param cellValue
         * @param options
         * @param rowObject
         * @returns {string}
         */
        function isDefaultFormatter (cellValue, options, rowObject){
            if (cellValue == 1)
                return "Yes";
            else if(cellValue == '0')
                return "No";
            else
                return '';
        }


        /*change status priority type*/
        function changeStatuspriorityType(id,action) {
            $.ajax({
                type: 'post',
                url: '/user-ajax',
                data: {
                    class: "ApexNewUserAjax",
                    action: "changeStatuspriorityType",
                    apexnewuser_id: id,
                    status_type: action,
                },
                success: function (response) {

                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        toastr.success("The status has been changed.");
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.' + key).html(value);
                        });
                    }
                    $('#priorityType-table').trigger( 'reloadGrid' );
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }





        /** Export sample unit type excel */
        $(document).on("click",'#exportSampleExcel',function(){
            window.location.href = base_url+"/UnitType-Ajax?action=exportSampleExcel";
        })

        /** Export unit type excel  */
        $(document).on("click",'#exportUnitTypeButton',function(){
            var status = $("#jqGridStatus option:selected").val();
            var table =  'company_unit_type';
            window.location.href = base_url+"/UnitType-Ajax?status="+status+"&&table="+table+"&&action=exportExcel";
        });
        /*  insert into priority type*/

        function onTop(rowdata){
            if(rowdata){
                setTimeout(function(){
                    jQuery('#priorityType-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('#priorityType-table').find('tr:eq(1)').find('td:eq(5)').addClass("green_row_right");
                }, 300);
            }
        }
            function saveCategory() {


                var priority_type = $("input[name='priority_type']").val();
                var desc = $("textarea[name='description']").val();
                var user_id_hidden = $("input[name='user_id_hidden']").val();
                var is_default = '0';
                if ($('#is_default').is(":checked")) {
                    is_default = '1';   // it is checked
                }


                // console.log(priority_type);


                $.ajax({
                    type: 'post',
                    url: '/MasterData/maintenance-Ajax',
                    data: {
                        class: 'MaintenanceTypeAjax',
                        action: "insertPriority",
                        priority_type: priority_type,
                        desc: desc,
                        user_id_hidden: user_id_hidden,
                        is_default: is_default

                    },
                    success: function (response) {
                        var response = JSON.parse(response);
                        if (response.status == 'success' && response.code == 200) {
                            $('#priorityType-table').trigger('reloadGrid');
                            $("#add_unit_type_div").hide(500);
                            $("#add_priority_type").trigger("reset");
                            toastr.success(response.message);
                            onTop(true);

                        }  else if(response.status == 'error' && response.code == 400){
                            $('.error').html('*This field is required');
                            $.each(response.data, function (key, value) {
                                $('*This field is required'+key).html(value);
                            });
                        }
                        else if (response.status == 'error' && response.code == 503) {
                            toastr.warning(response.message);
                        }
                    },
                    error: function (data) {
                    }
                });

            };


        function getpriorityType(id){
            $.ajax({
                type: 'post',
                url: '/MasterData/maintenance-Ajax',
                data: {
                    class: "MaintenanceTypeAjax",
                    action: "getpriorityType",
                    cuser_id: id
                },
                success: function (response) {

                    var response = JSON.parse(response);
                    $("#add_priority_type input[name='priority_type']").val(response.priority);
                    $("#add_priority_type textarea[name='description']").val(response.description);
                    if (response.is_default==1) {
                        $('#is_default').prop('checked', true);
                    }
                    else {
                        $("#is_default").prop('checked', false);
                    }


                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }

        function update_priority_status(id,action) {
            $.ajax({
                type: 'post',
                url: '/MasterData/maintenance-Ajax',
                data: {
                    class: "MaintenanceTypeAjax",
                    action: "update_priority_status",
                    user_id: id,
                    status_type: action,

                },
                success: function (response) {

                    var response = JSON.parse(response);
                    responseData = response.data;
                    if (response.status == 'success' && response.code == 200) {

                        if(responseData.status==1) {
                            toastr.success("Record Activated Successfully.");
                        }
                        else{
                            toastr.success("Record Deactivated Successfully.");
                        }
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.' + key).html(value);
                        });
                    }
                    else if(response.status == 'error' && response.code == 503){
                        toastr.error("A default value cannot be deactivated");
                    }
                    localStorage.setItem("Message","Priority Updated Successfully")
                    localStorage.setItem("rowcolor",true)
                    $('#priorityType-table').trigger( 'reloadGrid' );
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }



        function deletepriorityType(id) {
            $.ajax({
                type: 'post',
                url: '/MasterData/maintenance-Ajax',
                data: {
                    class: "MaintenanceTypeAjax",
                    action: "deletepriorityType",
                    user_id: id
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        toastr.success("The record deleted successfully.");
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.' + key).html(value);
                        });
                    }
                    else if(response.status == 'error' && response.code == 503){
                        toastr.error("A default value cannot be deleted");
                    }
                    $('#priorityType-table').trigger( 'reloadGrid' );
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
        $(document).on("change", "#priorityType-table .select_options", function (e) {
            e.preventDefault();
            var action = this.value;
            var id = $(this).attr('data_id');
            var checkDelete = $('option[value="Delete"]',this).length;

            if (checkDelete == "0"){
                $("input[name='priority_type']").attr("disabled",true);
            }else{
                $("input[name='priority_type']").attr("disabled",false);
            }

            console.log('-----');

            switch(action) {

                case "Edit":
                    //Deactivate or Activate manage user

                    $('#priorityErr').text('');
                    headerDiv.innerText = "Edit Priority Type";
                    $('#descriptionErr').text('');
                    $('#saveBtns').val('Update');
                    $('.Resetbtn').show();
                    $('.clearFormPriority').hide();
                    $("#add_priority_type #user_id_hidden").val(id);
                    $("#add_unit_type_div").show();
                    $('#priorityType-table').trigger('reloadGrid');
                    getpriorityType(id);
                    break;
                case "Deactivate":
                    //Deactivate or Activate manage user
                    bootbox.confirm("Do you want to "+action+" this record?", function (result) {
                        if (result == true) {
                            update_priority_status(id,action);
                        } else {
                            window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                        }
                    });
                    break;
                case "Activate":
                    //Deactivate or Activate manage user
                    bootbox.confirm("Do you want to "+action+" this record?", function (result) {
                        if (result == true) {
                            update_priority_status(id,action);
                        } else {
                            window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                        }
                    });
                    break;
                case "Delete":
                    //Deactivate or Activate manage user
                    bootbox.confirm("Do you want to "+action+" this record?", function (result) {
                        if (result == true) {
                            deletepriorityType(id);
                        } else {
                            window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                        }
                    });
                    break;
                default:
                    window.location.href = base_url+'/Setting/ApexNewUser';
            }

            return false;
        });




      /*  jQuery.validator.addMethod("noSpace", function(value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "Space are not allowed");*/
        $("#priority_type,#description").keypress(function(e) {
            if (e.which === 32 && !this.value.length) {
                e.preventDefault();
            }
            var inputValue = e.charCode;
            if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
                e.preventDefault();
            }
        });

        jQuery.validator.addMethod("nameRegex", function(value, element) {
            return this.optional(element) || /^[a-z\\s]+$/i.test(value);
        }, "Name must contain only letters. No Space Allowed");

        $("#add_priority_type").validate({
            errorLabelContainer: $("#error"),
            rules: {
                priority_type: {
                    required: true,
                    nameRegex:true
                },
                description: {
                    required: true,
                    nameRegex:true
                }
            },
            messages: {
                priority_type: { required: 'This field is required' },
                description: { required: 'This field is required' }
            }
        });

        $(document).on("click", "#add_unit_type_div #saveBtns", function (e) {
            var valid = $("#add_priority_type").valid();
            if(valid) {
                saveCategory();
            }

        });



        $(document).on("click", ".clearFormPriority", function (e) {
            $('#add_priority_type')[0].reset();
        });

        $(document).on('click','.Resetbtn',function () {
            var id =  $("#add_priority_type #user_id_hidden").val();
            getpriorityType(id);
        });
    });
