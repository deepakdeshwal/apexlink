  $(document).ready(function () {
      $.ajax({
          type: 'post',
          url: '/calendar/ajax',
          async:false,
          data: {
              class: "calendar",
              action: "checkloggedinuser",

          },
          success: function (response) {
              var res = JSON.parse(response);
              if (res.status == "success") {
                var ids=res.cal_ids;
              if(ids.includes(1)){
                  ids=ids;
              }else{
                  ids.push(1);
              }
              var arrayy=[0,1,2,3,4,5,6];
                  $.each(arrayy,function (k,v) {
                      if(ids.includes(v)){
                          $("input[name='cal_grp'][value='"+v+"']").attr("disabled",false);
                          $("input[name='cal_grp1'][value='"+v+"']").attr("disabled",false);
                          $("input[name='cal_grp1'][value='"+v+"']").attr("checked",true);
                      }else{
                          $("input[name='cal_grp'][value='"+v+"']").attr("disabled",true);
                          $("input[name='cal_grp1'][value='"+v+"']").attr("disabled",true);
                      }
                  });
              }
          }
      });


      $.ajax({
          type: 'post',
          url: '/calendar/ajax',
          async:false,
          data: {
              class: "calendar",
              action: "checkcalpermissions",
          },
          success: function (response) {
              var res = JSON.parse(response);
              if (res.status == "success") {
                console.log('cal-data',res.cal_data);
              }
          }
      });
            $("#daily_skip").val(1);
            $("#weekly_recurr").val(1);
            $("#day_of_month").val(1);
            $("#skip_month").val(1);
            $("#skip_month1").val(1);
            $("#recurr_every_yearly").val(1);
            $("#end_freq_yearly").val(1);
            $("#collapse1").toggle();
      var ch_div=localStorage.getItem('cal_div');
     console.log(ch_div);
      if(ch_div !==null && ch_div !=="" && ch_div !==undefined){
      cal_show();
      }else {
          $("#month-cal").show();
          $("#day-cal").hide();
          $("#week-cal").hide();
      }
            function cal_show(){
            var all_elem=["#month-cal","#day-cal","#week-cal"];
            var to_show="";
            var to_hide=[];
                $.each(all_elem,function (k,v) {
                if(ch_div==v){
                    var l=v.length;
                    v = v.substr(1,l);
                    to_show=v
                }else{
                    var ll=v.length;
                    v=v.substr(1,ll);
                    to_hide.push(v);
                }
               setTimeout(function () {
                $.each(to_hide,function (ka,va) {
                $("#"+va).hide();
              });
                 $("#"+to_show).show();
             },1500);
                localStorage.removeItem('cal_div');
            });
            }
            $(".fc-today-button").hide();
            $("#day_view").click(function () {
                check_div("#day-cal");
                $("#month-cal").hide();
                $("#day-cal").show();
                $("#week-cal").hide();
                $(".fc-timeGridWeek-button").hide();$(".fc-timeGridDay-button").hide();$(".fc-today-button").hide();
            });
            $("#week_view").click(function () {
                check_div("#week-cal");
                $("#month-cal").hide();
                $("#day-cal").hide();
                $("#week-cal").show();
                $(".fc-timeGridWeek-button").hide();$(".fc-timeGridDay-button").hide();$(".fc-today-button").hide();
            });
            $("#month_view").click(function () {
                check_div("#month-cal");
                $("#month-cal").show();
                $("#day-cal").hide();
                $("#week-cal").hide();$(".fc-today-button").hide();
            });


            $("#new_entry").click(function () {
             //   $("#meeting_form").trigger('reset');
                $("#del_meeting").val("0");
                $("#location").val("Office");
                $("#newentry").modal('show');
             });

            $(document).on('change','.all_sub',function (){
                if($(this).val()=="Other"){
                   $("#other_sub").show();
                }else{
                    $("#other_sub").hide();
                }
            });
            $(document).on('change','.start_time',function (){
                var val=$(this).val();
                $(".end_time").val(val);
            });
           // var event_val=[];
            $(document).on('click','.close_modal',function (){
                bootbox.confirm("save changes?", function (result) {
                    if (result == true) {
                        var event_val = [];
                        event_val = savecaldata();
                        $("#events").val(JSON.stringify(event_val));
                        localStorage.setItem('events',JSON.stringify(event_val));
                        console.log('fdf',$("#events").val());
                        setTimeout(function () {
                            if(event_val !== []) {
                                setTimeout(function () {
                                    window.location.reload();
                                },1000);
                            }
                        },100);
                    }else{
                        $("#newentry").modal('hide');
                    }
                });
            });

            $(document).on('click','#save_schedule',function (e){
                e.preventDefault();
                var event_val = [];
                event_val = savecaldata();
                $("#events").val(JSON.stringify(event_val));
                localStorage.setItem('events',JSON.stringify(event_val));
                console.log('fdf',$("#events").val());
                setTimeout(function () {
                    if(event_val !== []) {
                        setTimeout(function () {
                            window.location.reload();
                        },1000);
                    }
                },100);

            });
            $(document).on('click','#delete_schedule',function (e){
                e.preventDefault();
                var event_val = [];
                event_val = deletecaldata();
                $("#events").val(JSON.stringify(event_val));
                localStorage.setItem('events',JSON.stringify(event_val));
                console.log('fdf',$("#events").val());
                setTimeout(function () {
                if(event_val !== []) {
                        setTimeout(function () {
                            window.location.reload();
                        },2000);
                    }
                },100);
            });
          var data={};
          var event_data=[];
          var i;
          function savecaldata(){
              var grop_arr=[];
              $("input[name=cal_grp1]").each(function () {
                  if($(this).is(":checked")){
                      grop_arr.push($(this).val());
                  }
              });
              var subject= $('.all_sub :selected').val();
              var location= $('#location').val();
              var start_time= $('#start_time').val();
              var end_time= $('#end_time').val();
              var time = $("#st_time").val();
              var reminder=$("#remm").val();
              var idd=$("#del_meeting").val();
              var hours = Number(time.match(/^(\d+)/)[1]);
              var minutes = Number(time.match(/:(\d+)/)[1]);
              var AMPM = time.match(/\s(.*)$/)[1];
              if(AMPM == "PM" && hours<12) hours = hours+12;
              if(AMPM == "AM" && hours==12) hours = hours-12;
              var sHours = hours.toString();
              var sMinutes = minutes.toString();
              if(hours<10) sHours = "0" + sHours;
              if(minutes<10) sMinutes = "0" + sMinutes;
              var st_time=sHours + ":" + sMinutes;
              var timee = $("#e_time").val();
              var hours1 = Number(timee.match(/^(\d+)/)[1]);
              var minutes1 = Number(timee.match(/:(\d+)/)[1]);
              var AMPM1 = timee.match(/\s(.*)$/)[1];
              if(AMPM1 == "PM" && hours1<12) hours1 = hours1+12;
              if(AMPM1 == "AM" && hours1==12) hours1 = hours1-12;
              var sHours1 = hours1.toString();
              var sMinutes1 = minutes1.toString();
              if(hours1<10) sHours1 = "0" + sHours1;
              if(minutes1<10) sMinutes1 = "0" + sHours1;
              var e_time=sHours1 + ":" + sMinutes1;
              var private="";
              ($("#private_meeting").hasClass("not_private")) ? private="0":private="1";
              $.ajax({
                  type: 'post',
                  url: '/calendar/ajax',
                  async: false,
                  data: {
                      class: "calendar",
                      action: "savecalendardata",
                      subject:subject,
                      location:location,
                      start_time:start_time,
                      end_time:end_time,
                      st_time:st_time,
                      e_time:e_time,
                      private:private,
                      reminder:reminder,
                      idd:idd,
                      calendar:grop_arr
                  },
                  success: function (response) {
                      var res = JSON.parse(response);
                      if (res.status == "success") {
                          $("#newentry").modal('hide');
                          toastr.success(res.message);
                          console.log(res);
                          var arr=[];
                          for (i = 0; i < res.count.total; i++) {
                              if(res.data2[i].private=="0"){
                              arr=[];
                              }else{
                                  arr.push("fa");
                                  arr.push("fa-lock");
                              }
                                data = {
                                    id:res.data2[i].id,
                                    title: res.data2[i].subject,
                                    start: res.data2[i].start_date+'T'+res.data2[i].start_time,
                                    end:res.data2[i].end_date+'T'+res.data2[i].end_time,
                                    allDay : false,
                                    displayEventTime:true,
                                    classNames:arr
                              };
                              event_data.push(data);
                          }
                          console.log(event_data);
                      }
                  }
              });
              return event_data;
          }


          function deletecaldata(){
            var id=$("#del_meeting").val();
              $.ajax({
                  type: 'post',
                  url: '/calendar/ajax',
                  async: false,
                  data: {
                      class: "calendar",
                      action: "deletecalendardata",
                      id:id

                  },
                  success: function (response) {
                      var res = JSON.parse(response);
                      if (res.status == "success") {
                       //   $("#newentry").modal('hide');
                          toastr.success(res.message);
                          var grop_arr=[];
                          $("input[name=cal_grp1]").each(function () {
                              if($(this).is(":checked")){
                                  grop_arr.push($(this).val());
                              }
                          });
                          $.ajax({
                              type: 'post',
                              url: '/calendar/ajax',
                              async: false,
                              data: {
                                  class: "calendar",
                                  action: "getdataafterdel",
                                  calendar:grop_arr
                              },
                              success: function (response) {
                                  var res = JSON.parse(response);
                                  if (res.status == "success") {
                                      $("#newentry").modal('hide');

                                      for (i = 0; i < res.count.total; i++) {
                                          var arr=[];
                                              if(res.data2[i].private=="0"){
                                                  arr=[];
                                              }else{
                                                  arr.push("fa");
                                                  arr.push("fa-lock");
                                              }
                                          data = {

                                              id:res.data2[i].id,
                                              title: res.data2[i].subject,
                                              start: res.data2[i].start_date+'T'+res.data2[i].start_time,
                                              end:res.data2[i].end_date+'T'+res.data2[i].end_time,
                                              allDay : false,
                                              displayEventTime:true,
                                              classNames:arr
                                          };
                                          event_data.push(data);
                                      }
                                  }
                              }
                          });
                      }
                  }
              });
              return event_data;
          }
      weekatgalance();
      function weekatgalance(){
          var today=new Date();
          var day=today.getDate();
          var day2=today.getDate()+7;
          var month=today.getMonth()+1;
          var valAsString1 = day.toString();
          var valAsString2 = month.toString();
          var valAsString3 = day2.toString();
          console.log('sd',day,day2);
          if(valAsString1.length===1){
              day="0"+day;
          }else{
              day=day;
          }
          if(valAsString3.length===1){
              day2="0"+day2;
          }else{
              day2=day2;
          }
          if(valAsString2.length===1){
              month="0"+month;
          }else{
              month=month;
          }
          var date=today.getFullYear()+"-"+month+"-"+day;
          var next_date=new Date();
          next_date.setDate(next_date.getDate() + 7);
          var day3=next_date.getDate();
          var month3=next_date.getMonth()+1;
          var valAsString4 = month3.toString();
          var valAsString5 = day3.toString();
          if(valAsString4.length===1){
              month3="0"+month3;
          }else{
              month3=month3;
          }
          if(valAsString5.length===1){
              day3="0"+day3;
          }else{
              day3=day3;
          }

         next_date=next_date.getFullYear()+"-"+month3+"-"+day3;
          $.ajax({
              type: 'post',
              url: '/calendar/ajax',
              async: false,
              data: {
                  class: "calendar",
                  action: "compareweekdata",
                  date:date,
                  next_date:next_date

              },
              success: function (response) {
                  var res = JSON.parse(response);
                  if (res.status == "success") {
                      console.log(res);
                      var dates="";
            var html="";
            $.each(res.data,function (key,value){
                    if(value.start_date===date){
                        dates="Today";
                    }else{
                        var datee=new Date(value.start_date);
                        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
                        var day=datee.getDay();
                        day=days[day];
                        dates=day;
                    }
                html +=
                    "                                <h5>"+dates+"</h5>\n" +
                    "                                <div class=\"row glance-meeting\">\n" +
                    "                                    <div class=\"col-sm-3\">\n" +
                    "                                        <p class=\"glance-time\">\n" +
                    "                                            "+value.start_time+"\n" +
                    "                                        </p>\n" +
                    "                                    </div>\n" +
                    "                                    <div class=\"col-sm-9\">\n" +
                    "                                         <p class=\"glance-task bg-skyblue\">\n" +
                    "                                            "+value.subject+"\n" +
                    "                                        </p>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n"

            });

             $("#week_schedule").append(html);
                     console.log(res);
                  }
              }
          });
      }

      $(document).on('click','input[name=freq]',function () {
       var val=$(this).val();
      if(val=='1'){
          $(".weekly").hide();
          $(".daily").show();
          $(".monthly").hide();
          $(".yearly").hide();
      }else if(val=='2'){
          $(".weekly").show();
          $(".daily").hide();
          $(".monthly").hide();
          $(".yearly").hide();
      }else if(val=='3'){
          $(".weekly").hide();
          $(".daily").hide();
          $(".monthly").show();
          $(".yearly").hide();
      }else if(val=='4'){
          $(".weekly").hide();
          $(".daily").hide();
          $(".monthly").hide();
          $(".yearly").show();
      }
      });
      // $('.myCalendar table').remove();
      // $('.myCalendar .month-head').remove();
      // $('.myCalendar .year-month').remove();
      var n_date=new Date();
      n_date.setMonth(n_date.getMonth()+1);
      var d5=n_date.getDate();
      $('.myCalendar').calendar({
          date: new Date(),
          autoSelect: false, // false by default
          select: function (result) {
              console.log(result);
          },
          toggle: function (y, m) {
              setTimeout(function () {
                  $("td[data-date='"+d5+"']").css('background-color','rgb(167, 232, 255)');
              },1000);
          }
      });
 $('.myCalendar2').calendar({
          date: new Date(),
          autoSelect: false, // false by default
          select: function (result) {
              console.log(result);
          },
          toggle: function (y, m) {
              setTimeout(function () {
                  $("td[data-date='"+d5+"']").css('background-color','rgb(167, 232, 255)');
              },1000);
          }
      });
 $('.myCalendar3').calendar({
          date: new Date(),
          autoSelect: false, // false by default
          select: function (result) {
              console.log(result);
          },
          toggle: function (y, m) {
              setTimeout(function () {
                  $("td[data-date='"+d5+"']").css('background-color','rgb(167, 232, 255)');
              },1000);
          }
      });

      $('.myCalendar1').calendar({
          date: n_date,
          autoSelect: false, // false by default
          select: function (result) {
              console.log(result);
          },
          toggle: function (y, m) {
              setTimeout(function () {
                  $("td[data-date='"+d5+"']").css('background-color','rgb(167, 232, 255)');
              },1000);
          }
      });
      setTimeout(function () {
          $("td[data-date='"+d5+"']").css('background-color','rgb(167, 232, 255)');
      },1000);

      $(document).on('click','.myCalendar .ic-arrow-angle-right',function () {
          $(".fc-next-button").trigger('click');
          // $(".myCalendar1 .ic-arrow-angle-right").trigger('click');

      });
      $(document).on('click','.myCalendar .ic-arrow-angle-left',function () {
          $(".fc-prev-button").trigger('click');
          // $(".myCalendar1 .ic-arrow-angle-right").trigger('click');

      });
      $(document).on('click','.myCalendar1 .ic-arrow-angle-right',function () {
          $(".fc-next-button").trigger('click');
      });
      $(document).on('click','.myCalendar1 .ic-arrow-angle-left',function () {
          $(".fc-prev-button").trigger('click');
      });
      $(document).on('click','#share_calendar',function () {
          $("#calendar-share").modal('show');
      });
      $(document).on('click','#print_calendar',function () {
          $("#calendar-print").modal('show');
      });
      $(document).on('click','#print_btn',function () {
          $("#calendar-print1").modal('show');
      });
      $(document).on('click','#send_invitation',function () {
          $("#sendInvitation").modal('show');
      });
      $(document).on('click','#reccur_pattern',function () {
          $("#calendar-recurring").modal('show');
      });
      $(document).on('click','#cal_group',function () {
          $("#calendar-group").modal('show');
      });
      $(document).on('click','#search_cal',function () {
          $("#search_cal").hide();
          $("#search_cal_val").show();
      });
      $(document).on('click','#btnShowAddUserPopup',function () {
          $("#user-names").modal('show');

      });
      $(document).on('keyup', '#search_cal_input', function () {
          var grid = $("#search_calendar_grid"),f = [];
          var val = $(this).val();
          if(val !==""){
          $("#main_sec").hide();
          $("#search_cal_outer").show();
          f.push({field: "subject", op: "bw", data: val,con:'OR'},{field: "location", op: "bw", data: val});
          grid[0].p.search = true;
          $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
          grid.trigger("reloadGrid",[{page:1,current:true}]);
          }else if(val ==""){
              $("#search_cal_outer").hide();
              $("#main_sec").show();
          }
       });

      $(document).on('click','#accept_reoccurence',function () {

       var count=$("input[name=count]:checked").val();
       var freq=$("input[name=freq]:checked").val();
       var tot_count="";
       if(count==0){
           tot_count=20;
       }else if(count==1){
           tot_count=$("#end_by").val();
       }else if(count==2){
           var recurr_end=$("#recurr_end_by").val();
           var recurr_start_by=$("#recurr_start_by").val();
           if(freq==1){
               var timeDiff  = (new Date(recurr_end)) - (new Date(recurr_start_by));
               tot_count =Math.round( timeDiff / (1000 * 60 * 60 * 24));
                console.log(tot_count);
           }else if(freq==2){
               var timeDiff  = (new Date(recurr_end)) - (new Date(recurr_start_by));
               tot_count =Math.round( timeDiff /(7 * 24 * 60 * 60 * 1000));
               console.log(tot_count);
           }else if(freq==3){
               var timeDiff  = (new Date(recurr_end)) - (new Date(recurr_start_by));
               tot_count =Math.round( timeDiff / (1000 * 60 * 60 * 24));
           }else if(freq==4){
               tot_count=10;
           }
       }
       if(freq==1){
           var daily_freq= $("input[name=daily]:checked").val();
           if(daily_freq==0){
               var daily_start_date1=new Date($(".start_date_recurr").val());
               var skip_days=$("#daily_skip").val();
               var all_dates_daily=[];
               var m;
               for(m=0;m<tot_count;m++){
                   if(m===0){
                       daily_start_date1.setDate(daily_start_date1.getDate());
                   }else{
                       daily_start_date1.setDate(daily_start_date1.getDate()+parseInt(skip_days));
                   }
                   var days_mon=daily_start_date1.getMonth()+1;
                   var days_day=daily_start_date1.getDate();
                   if(days_mon.toLocaleString().length===1){
                       days_mon="0"+days_mon
                   }else {
                       days_mon=days_mon;
                   }
                   if(days_day.toLocaleString().length===1){
                       days_day="0"+days_day;
                   }else {
                       days_day=days_day;
                   }
                   var l_daily=daily_start_date1.getFullYear()+"-"+days_mon+"-"+days_day;
                   all_dates_daily.push(l_daily);
               }
               recurring_meetings(all_dates_daily);
               console.log(all_dates_daily);
           }else if(daily_freq==1){
               var n;
               var all_dates_daily1=[];
               console.log(tot_count);
               for(n=0;n<tot_count;n++){
                   var daily_start_date2=new Date($(".start_date_recurr").val());
                   daily_start_date2.setDate(daily_start_date2.getDate()+parseInt(n));
                   console.log(daily_start_date2,daily_start_date2.getDay());
                   var day=daily_start_date2.getDay();
                   var new_date;
                   var mon=daily_start_date2.getMonth()+1;
                   var datee=daily_start_date2.getDate();
                       if(mon.toLocaleString().length===1){
                           mon="0"+mon;
                       } else{
                           mon=mon;
                       }
                       if(datee.toLocaleString().length===1){
                           datee="0"+datee;
                       } else{
                           datee=datee;
                       }
                   if(day !==0 && day !==6){
                       new_date=daily_start_date2.getFullYear()+"-"+mon+"-"+datee;
                       all_dates_daily1.push(new_date);
                     }else{
                       tot_count=parseInt(tot_count)+parseInt(1);
                   }
               }
               recurring_meetings(all_dates_daily1);
               console.log(all_dates_daily1);
           }

       }else if(freq==2){
           var weekly_start_date=new Date($(".start_date_recurr").val());
           var weekly_recurr=$("#weekly_recurr").val();
           var p;
           var arrr=[];
          $("input[name=weekly]").each(function () {
            if($(this).is(':checked')){
            arrr.push($(this).val());
            }
          });
          var week_dates=[];
          console.log(arrr,weekly_recurr);
           var week_no=weekly_start_date.getWeek();
          for(p=0;p<tot_count;p++){
                if(p==0){
                    console.log('dd',week_no);
                    weekly_start_date.setWeek(week_no);
                }else {
                    console.log('ddd',parseInt(week_no)+parseInt(weekly_recurr)*parseInt(p));
                    weekly_start_date.setWeek(parseInt(week_no)+parseInt(weekly_recurr)*parseInt(p));
                }
                var t;
                console.log(weekly_start_date);
                for(t=0;t<7;t++){
                    var st_datee=new Date(weekly_start_date);
                    st_datee.setDate(st_datee.getDate()+parseInt(t));
                    var week_day1=st_datee.getDay();
                    var week_date1=st_datee.getDate();
                    var week_mon1=st_datee.getMonth()+parseInt(1);
                    $.each(arrr,function (k,v) {
                    console.log(v,week_day1);
                     if(v==week_day1){
                     var fin_week_date=st_datee.getFullYear()+"-"+week_mon1+"-"+week_date1;
                     week_dates.push(fin_week_date);
                     }
                    });
                    console.log(st_datee);
                }
          }
          console.log(week_dates);
           recurring_meetings(week_dates);
       }else if(freq==3){
         var mon_freq= $("input[name=monthly_freq]:checked").val();
            if(mon_freq==0){
              var mnth_day=$("#day_of_month").val();
              var skip_month=$("#skip_month").val();
              var month_dates=[];
              var monthly_start_date=new Date($(".start_date_recurr").val());
              var valtoStr=mnth_day.length;
                if(valtoStr===1){
                    mnth_day="0"+mnth_day
                }else{
                    mnth_day=mnth_day;
                }
                var k;
            for(k=0;k<tot_count;k++){
                if(k==0){
                    monthly_start_date.setMonth(monthly_start_date.getMonth());
                }else {
                    monthly_start_date.setMonth(monthly_start_date.getMonth()+parseInt(skip_month));
                }
                var monn=monthly_start_date.getMonth();
                var valtostrmon=monn.toLocaleString().length;
                    if(valtostrmon===1){
                        monn="0"+monn;
                    }else {
                        monn=monn;
                    }
            var final_monthly_date=monthly_start_date.getFullYear()+"-"+monn+"-"+mnth_day;
            month_dates.push(final_monthly_date);
            }
             console.log(month_dates);
                recurring_meetings(month_dates);
            }else if(mon_freq==1){
                var monthly_start_date1=new Date($(".start_date_recurr").val());
                var skip_month1=$("#skip_month1").val();
                monthly_start_date1.setMonth(monthly_start_date1.getMonth());
                var number_monthly=parseInt($("#day_number_month").val());
                var num_monthly=parseInt($("#weekdays_monthly").val());
                var all_dates_monthly=[];
                var l;
                for(l=0;l<tot_count;l++){
                    if(l===0){
                        monthly_start_date1.setMonth(monthly_start_date1.getMonth());
                    }else{
                        monthly_start_date1.setMonth(monthly_start_date1.getMonth()+parseInt(skip_month1));
                    }
                    console.log(monthly_start_date1);
                    var mm=nthWeekdayOfMonth(num_monthly,number_monthly,monthly_start_date1);
                    console.log(mm);
                    var monthh_mon=mm.getMonth()+1;
                    var monthh_day=mm.getDate();
                        if(monthh_mon.toLocaleString().length===1){
                        monthh_mon="0"+monthh_mon
                        }else {
                            monthh_mon=monthh_mon;
                        }
                        if(monthh_day.toLocaleString().length===1){
                            monthh_day="0"+monthh_day;
                        }else {
                            monthh_day=monthh_day;
                        }
                    var l_date_month=mm.getFullYear()+"-"+monthh_mon+"-"+monthh_day;
                    all_dates_monthly.push(l_date_month);
                    console.log(l_date_month);
                }
                console.log(all_dates_monthly);
                recurring_meetings(all_dates_monthly);
            }
       }else if(freq==4){
        var yearly_rad_val=$("input[name=yearly_freq]:checked").val();
        if(yearly_rad_val==0){
        var start_date=new Date($(".start_date_recurr").val());
        var recurr_every=$("#recurr_every_yearly").val();
        var i;
        var all_dates=[];
        for(i=0;i<tot_count;i++){
            if(i==0){
                start_date.setFullYear(start_date.getFullYear());
            }else{
                start_date.setFullYear(start_date.getFullYear()+parseInt(recurr_every));
            }
            var year=start_date.getFullYear();
            var dd=$("#end_freq_yearly").val();
            var month=$("#yearly_months").val();
            var valtostr=dd.length;
            if(valtostr===1){
                dd="0"+dd;
            }else{
                dd=dd;
            }
            var fin_date=year+'-'+month+'-'+dd;
            all_dates.push(fin_date);
            console.log(fin_date);
        }
            console.log(all_dates);
            recurring_meetings(all_dates);
        }else if(yearly_rad_val==1){
            var date=new Date($(".start_date_recurr").val());
            var recurr_years=$("#month_yearly").val();
            var recurr_every1=$("#recurr_every_yearly").val();
            date.setMonth(recurr_years-parseInt(1));
            var number=$("#weekdays_yearly").val();
            var num=$("#recurr_number").val();
            var all_dates_yearly=[];
            var j;
            for(j=0;j<tot_count;j++){
                if(j===0){
                    date.setFullYear(date.getFullYear());
                }else{
                    date.setFullYear(date.getFullYear()+parseInt(recurr_every1));
                }
                var aa=nthWeekdayOfMonth(parseInt(number),parseInt(num),date);
                var monthh=aa.getMonth()+1;
                var l_date=aa.getFullYear()+"-"+monthh+"-"+aa.getDate();
                all_dates_yearly.push(l_date);
            }
            console.log(all_dates_yearly);
            recurring_meetings(all_dates_yearly);
        }
       }
      });
      function timeTo12HrFormat(time)
      {   // Take a time in 24 hour format and format it in 12 hour format
          var time_part_array = time.split(":");
          var ampm = 'AM';

          if (time_part_array[0] >= 12) {
              ampm = 'PM';
          }

          if (time_part_array[0] > 12) {
              time_part_array[0] = time_part_array[0] - 12;
          }

          formatted_time = time_part_array[0] + ':' + time_part_array[1] + ':' + time_part_array[2] + ' ' + ampm;

          return formatted_time;
      }
      function nthWeekdayOfMonth(weekday, n, date) {
          var count = 0,
              idate = new Date(date.getFullYear(), date.getMonth(), 1);
          while (true) {
              if (idate.getDay() === weekday) {
                  if (++count == n) {
                      break;
                  }
              }
              idate.setDate(idate.getDate() + 1);
          }
          return idate;
      }

        function recurring_meetings(stt_datee) {
            var gropp_arr=[];
            $("input[name=cal_grp]").each(function () {
                if($(this).is(":checked")){
                    gropp_arr.push($(this).val());
                }
            });
            var subject= $('.all_sub :selected').val();
            var location= $('#location').val();
            var time = $("#start_recurr_time").val();
            var hours = Number(time.match(/^(\d+)/)[1]);
            var minutes = Number(time.match(/:(\d+)/)[1]);
            var AMPM = time.match(/\s(.*)$/)[1];
            if(AMPM == "PM" && hours<12) hours = hours+12;
            if(AMPM == "AM" && hours==12) hours = hours-12;
            var sHours = hours.toString();
            var sMinutes = minutes.toString();
            if(hours<10) sHours = "0" + sHours;
            if(minutes<10) sMinutes = "0" + sMinutes;
            var st_time=sHours + ":" + sMinutes;
            var timee = $("#end_recurr_time").val();
            var hours1 = Number(timee.match(/^(\d+)/)[1]);
            var minutes1 = Number(timee.match(/:(\d+)/)[1]);
            var AMPM1 = timee.match(/\s(.*)$/)[1];
            if(AMPM1 == "PM" && hours1<12) hours1 = hours1+12;
            if(AMPM1 == "AM" && hours1==12) hours1 = hours1-12;
            var sHours1 = hours1.toString();
            var sMinutes1 = minutes1.toString();
            if(hours1<10) sHours1 = "0" + sHours1;
            if(minutes1<10) sMinutes1 = "0" + sHours1;
            var e_time=sHours1 + ":" + sMinutes1;
            $.ajax({
                type: 'post',
                url: '/calendar/ajax',
                async: false,
                data: {
                    class: "calendar",
                    action: "savecalendardatarecurring",
                    subject:subject,
                    location:location,
                    dates:stt_datee,
                    st_time:st_time,
                    e_time:e_time,
                    calendar:gropp_arr
                },
                success: function (response) {
                    var res = JSON.parse(response);
                    if (res.status == "success") {
                        toastr.success(res.message);
                        console.log(res);
                        var grop_arr=[];
                        $("input[name=cal_grp1]").each(function () {
                            if($(this).is(":checked")){
                                grop_arr.push($(this).val());
                            }
                        });
                        $.ajax({
                            type: 'post',
                            url: '/calendar/ajax',
                            async: false,
                            data: {
                                class: "calendar",
                                action: "getdataafterdel",
                                calendar:grop_arr
                            },
                            success: function (response) {
                                var res = JSON.parse(response);
                                if (res.status == "success") {
                                    $("#newentry").modal('hide');
                                    for (i = 0; i < res.count.total; i++) {
                                        data = {
                                            id:res.data2[i].id,
                                            title: res.data2[i].subject,
                                            start: res.data2[i].start_date+'T'+res.data2[i].start_time,
                                            end:res.data2[i].end_date+'T'+res.data2[i].end_time,
                                            allDay : false,
                                            displayEventTime:true
                                        };

                                        event_data.push(data);
                                    }
                                    console.log(event_data);
                                    var event_val = [];
                                    event_val = event_data;
                                    localStorage.setItem('events',JSON.stringify(event_val));
                                    console.log('fdf',$("#events").val());
                                    setTimeout(function () {
                                        if(event_val !== []) {
                                            setTimeout(function () {
                                                window.location.reload();
                                            },2000);
                                        }
                                    },100);

                                }

                            }
                        });


                    }
                }
            });
            return event_data;
        }

      calsearch();
      function calsearch() {
          var table = 'calendar_schedule';
          var columns = ['Subject','Location','Start','End','st','endd'];
          var conditions = ["eq","bw","ew","cn","in"];
          var extra_where = [];
          var extra_columns = [];
          var joins = [];
          var columns_options = [
              {name:'subject',index:'subject',hidden:false,width:300,searchoptions: {sopt: conditions},table:table},
              {name:'location',index:'location',hidden:false,width:300,searchoptions: {sopt: conditions},table:table},
              {name:'start',index:'start_date',hidden:false,width:300,searchoptions: {sopt: conditions},table:table,formatter:date_form_start},
              {name:'end',index:'end_date',width:350,hidden:false,searchoptions: {sopt: conditions},table:table,formatter:date_form_end},
              {name:'st',index:'start_time',width:350,hidden:true,searchoptions: {sopt: conditions},table:table},
              {name:'endd',index:'end_time',width:350,hidden:true,searchoptions: {sopt: conditions},table:table},
          ];
          var ignore_array = [];
          jQuery("#search_calendar_grid").jqGrid({
              url: '/List/jqgrid',
              datatype: "json",
              height: '100%',
              autowidth: true,
              colNames: columns,
              colModel: columns_options,
              pager: true,
              mtype: "POST",
              postData: {
                  q: 1,
                  class: 'jqGrid',
                  action: "listing_ajax",
                  table: table,
                  columns_options: columns_options,
                  ignore:ignore_array,
                  joins:joins,
                  extra_where:extra_where,
                  extra_columns:extra_columns,
                  deleted_at:'true'
              },
              viewrecords: true,
              sortname: 'calendar_schedule.updated_at',
              sortorder: "desc",
              sorttype:'date',
              sortIconsBeforeText: true,
              headertitles: true,
              rowNum: '5',
              rowList: [5, 10, 20, 30, 50, 100, 200],
              caption: "Calendar List",
              pginput: true,
              pgbuttons: true,
              navOptions: {
                  edit: false,
                  add: false,
                  del: false,
                  search: true,
                  filterable: true,
                  refreshtext: "Refresh",
                  reloadGridOptions: {fromServer: true}
              }
          }).jqGrid("navGrid",
              {
                  edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
              },
              {}, // edit options
              {}, // add options
              {}, //del options
              {top:10,left:200,drag:true,resize:false} // search options
          );
      }
      var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
      function date_form_start (cellValue, options, rowObject){
          var comb="";
          if (cellValue != undefined){
              var date=new Date(rowObject.start);
              var week=(date.getDay());
              week=(days[week]).substr(0,3);
              date= (date.getMonth()+1)+'/'+date.getDate()+'/'+date.getFullYear()+' '+'('+week+'.'+')';
              var time=rowObject.st;
              time=timeTo12HrFormat(time);
              comb=date+" "+time;
          }
          return comb;
      }
      function date_form_end (cellValue, options, rowObject){
          var combb="";
          if (cellValue != undefined){
              var date=new Date(rowObject.start);
              var week=(date.getDay());
              week=(days[week]).substr(0,3);
              date= (date.getMonth()+1)+'/'+date.getDate()+'/'+date.getFullYear()+' '+'('+week+'.'+')';
              var time=rowObject.st;
              time=timeTo12HrFormat(time);
              combb=date+" "+time;
          }
          return combb;
      }

      function check_div(elem) {
          localStorage.setItem('cal_div',elem);
      }
      $(document).on('click','#private_meeting',function () {
        if($(this).hasClass("not_private")){
            $(this).removeClass("not_private");
            $(this).css('background-color','white');
        }else{
            $(this).addClass("not_private");
            $(this).css('background-color','initial');
        }
      });
     function sendInviteemail() {
         $.ajax({
             type: 'post',
             url: '/calendar/ajax',
             async: false,
             data: {
                 class: "calendar",
                 action: "sendInvite",

             },
             success: function (response) {
                 var res = JSON.parse(response);
                 console.log(res);
             }
         });
     }

      function getSecondPart(str) {
          console.log("fgsadfg",str);
          var count= str.split(';').length;
          console.log('count',count);
          return str.split(';')[count-1];
      }
      $(document).on("keyup",".invitation_name",function () {
          $('.emails_users').show();
          var val1=$(this).val();
          if(val1.includes(';')){
            var val = getSecondPart(val1);
          }else{
             var val = val1;
          }
          $.ajax({
              type: 'post',
              url: '/calendar/ajax',
              async: false,
              data: {
                  class: "calendar",
                  action: "GetuserEmail",
                  val:val

              },
              success: function (response) {
                  $(".emails_users").html(response);
              }
          });
      });

      $(document).on("click",".dataEmail",function(){
          var val= $(this).text();
          var val_id=$(this).attr('data-id');
          var exist_val_id=$(".email_id").val();
          var exist_value=$('.invitation_name').val();
          if(exist_value!=''){
              $('.invitation_name').val(exist_value+' '+val+';');
              $('.email_id').val(exist_val_id+' '+val_id);
              $('.emails_users').hide();
          }else{
              $('.invitation_name').val(val+';');
              $('.emails_users').hide();
              $('.email_id').val(val_id);
          }
      });
      $(document).on('click','#send_invitation',function () {
          $(".meeting_invitation").text($(".all_sub").val());
          $(".Office_invitaion").text($("#location").val());
          $(".time_invitaion").text($("#start_time").val()+' '+$("#st_time").val());
          $(".name_invitaion").text(domain);

      });
      $(document).on('click','#btnShowAddUserPopup',function () {
          $.ajax({
              type: 'post',
              url: '/User/ManageGroupsAjax',
              data: {
                  class: "manageGroups",
                  action: "usersData",
              },
              success: function (response) {
                  var response = JSON.parse(response);
                  if(response.status == 'success' && response.code == 200){
                      var html="";
                      $.each(response.data,function (key,value) {
                          html +="<tr data-val='"+value.id+"'><td data-val='"+value.id+"'><input type='checkbox' value='"+value.id+"' class='check_user_single'/></td>" +
                              "<td>"+value.name+"</td>" +
                              "<td>"+value.email+"</td>"+
                              "<td>"+value.phone_number+"</td></tr>"
                      });
                      console.log('html',html);
                      $("#user_data_table").html('').append(html);
                  }
              }
          });
      });
      $(document).on('click','.check_user_mul',function () {
          if($(".check_user_mul").prop("checked")==true){
              $(".check_user_single").prop("checked",true);
          }else if($(".check_user_mul").prop("checked")==false){
              $(".check_user_single").prop("checked",false);
          }
      });
      $(document).on('click','#add_userss',function () {
          $(".check_user_single").each(function () {
              if($(this).is(":checked")){
                  $("#user_data").append("<tr>"+$(this).closest("tr").html()+"</tr>");
                  $("#user-names").modal('hide');
                  console.log($(this).closest("tr").html());
              }
          })
      });
      $(document).on('click','#view_access_perm',function () {
          if($("#view_access_perm").prop("checked")==false){
              $(".view_acess_class").attr("disabled",false);
              $("#permission_changes").attr("disabled",false);
          }else if($("#view_access_perm").prop("checked")==true){
              $(".view_acess_class").prop("checked",false);
              $(".view_acess_class").attr("disabled",true);
          }
      });
      $(document).on('click','#permission_changes',function () {
          if($("#permission_changes").prop("checked")==false){
              $(".changes_class").attr("disabled",false);
          }else if($("#permission_changes").prop("checked")==true){
              $(".changes_class").prop("checked",false);
              $(".changes_class").attr("disabled",true);
          }
      });
      $(document).on('click','#free_busy_time',function () {
          if($("#free_busy_time").is(":checked")){
              $(this).val("1");
          }else {
              $(this).val("0");
          }
      });
      $(document).on('click','#full_access',function () {
          if($("#full_access").is(":checked")){
              $(this).val("1");
          }else {
              $(this).val("0");
          }
      });
      $(document).on('click','#move_change',function () {
          if($("#move_change").is(":checked")){
              $(this).val("1");
          }else {
              $(this).val("0");
          }
      });
      $(document).on('click','#schedule_create',function () {
          if($("#schedule_create").is(":checked")){
              $(this).val("1");
          }else {
              $(this).val("0");
          }
      });
      $(document).on('click','#accept_permissions',function () {
          var grp_arr=[];
          $("input[name=cal_grp]").each(function () {
            if($(this).is(":checked")){
                grp_arr.push($(this).val());
            }
         });
          bootbox.confirm("Are you sure you want to implement the changes?", function (result) {
              if (result == true){
                  var id_arr=[];
              $("#user_data tr").each(function () {
                  id_arr.push($(this).find("td:eq(0)").attr("data-val"));
              });
                  cal_share_permissions(id_arr,grp_arr);
              }else{
                  $("#newentry").modal('hide');
              }
          });
      });
      function cal_share_permissions(ids,calendar) {
          var free_busy=$("#free_busy_time").val();
          var full=$("#full_access").val();
          var create=$("#schedule_create").val();
          var move=$("#move_change").val();
          $.ajax({
              type: 'post',
              url: '/calendar/ajax',
              data: {
                  class: "calendar",
                  action: "cal_share_permissions",
                  ids:ids,
                  calendar:calendar,
                  move:move,
                  create:create,
                  full:full,
                  free_busy:free_busy
              },
              success: function (response) {
                toastr.success("response.message");
              }
          });
      }
      $(document).on("click",".sendInvitation",function () {
          var send_to=$(".invitation_name").val();
          var message=$(".message_invite").val();
          var id=$('.email_id').val();
          var start_date=$('.start_time').val();
          var start_time=$('#st_time').val();
          $.ajax({
              type: 'post',
              url: '/calendar/ajax',
              async: false,
              data: {
                  class: "calendar",
                  action: "sendInvitation",
                  message:message,
                  send_to:send_to,
                  id:id,
                  start_date:start_date,
                  start_time:start_time

              },
              success: function (response) {
                toastr.success(response);
                $("#sendInvitation").modal('hide');
                  var event_val = savecaldata();
                  $("#events").val(JSON.stringify(event_val));
                  localStorage.setItem('events',JSON.stringify(event_val));
                  // var res = JSON.parse(response);

              }
          });
      });
  });