$(document).ready(function(){
    var currentPagePath = window.location.pathname;

    jQuery.validator.setDefaults({
        debug: true,
        success: "valid"
    });

// function to validate first password
    $.validator.addMethod("pwcheck", function(value, element, error) {
        if (error) {
            $.validator.messages.pwcheck = '';
            return false;
        }
        return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(value)
    },'please select ');

    $.validator.addMethod("password_regex", function(value, element) {
        var password_regex = this.optional(element) || /^(?=.*[a-zA-Z])(?=.*[0-9])/.test(value);
        return password_regex;
    }, "* Atleast one aphanumeric and a numeric character required");

    $( "#change_pass_form" ).validate({
        rules: {
            password:{
                required:true,
                password_regex : true
            },
            newpassword:{
                required:true,
                minlength:8,
                password_regex : true
            },
            confirmpassword:{
                required:true,
                minlength:8,
                password_regex : true,
                equalTo :'#newpassword2'
            }
        },
        messages: {
            newpassword: {
                minlength: "* Minimum 8 characters allowed",
            },
            confirmpassword: {
                equalTo: "* Fields do not match",
                minlength: "* Minimum 8 characters allowed",
            },

        }
    });

    $('#change_pass_form').on('submit',function (e) {
        e.preventDefault();
        var password = $("#newpassword2").val();
        var cpassword = $("input[name='confirmpassword']").val();
        var cur = $("input[name='password']").val();
        if ($("#change_pass_form").valid()) {
            changeAdminPassword(password, cpassword, cur);
        }
        return false;
    });


        function changeAdminPassword(password, cpassword, cur) {
        $.ajax({
            type: 'post',
            url: '/company-ajax',
            data: {
                class: "changePassword",
                action: "changePassword",
                nPassword: password,
                cPassword: cpassword,
                currentPass: cur,
                id: '',
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("Password changed successfully.");
                    $("#current_password").val('');
                    $("#newpassword2").val('');
                    $("#newpassword3").val('');
                    $("#strength").empty();
                    if ($('#result').hasClass('good')){
                        $("#password_checker span").removeClass("good");
                    }
                    if ($('#result').hasClass('short')){
                        $("#password_checker span").removeClass("short");
                    }
                    if ($('#result').hasClass('weak')){
                        $("#password_checker span").removeClass("weak");
                    }
                    if ($('#result').hasClass('strong')){
                        $("#password_checker span").removeClass("strong");
                    }
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

});