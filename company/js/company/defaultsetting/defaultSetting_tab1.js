loadCurrency();
loadData();
$(document).on('focusout','.amount',function(){
    var id = this.id;
    if($('#' + id).val() != '' && $('#' + id).val().indexOf(".") == -1) {
        var bef = $('#' + id).val().replace(/,/g, '');
        var value = numberWithCommas(bef) + '.00';
        $('#' + id).val(value);
    } else {
        var bef = $('#' + id).val().replace(/,/g, '');
        $('#' + id).val(numberWithCommas(bef));
    }
});


$(document).on('click','#default_setting_cancel',function(){
    bootbox.confirm({
        message: "Do you want to cancel this action now ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                window.location.href = '/User/AccountSetup';
            }
        }
    });
});

/**
 * If the letter is not digit in fax then don't type anything.
 */
$(".number_only").keydown(function (e) {
    if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
        return false;
    }
});

/**
 * Auto fill the city, state and country when focus loose on zipcode field.
 */
$("#zip_code").focusout(function () {
   // getAddressInfoByZip($(this).val(),false);
    getZipCode('#zip_code',$(this).val(),'#city2','#state2','#country','','#zip_code_validate');
});

function loadCurrency(){
    $.ajax({
        type: 'post',
        url: '/settings-ajax',
        data: {class: 'DefaultSettingsAjax', action: "fetchCountries"},
        success: function (response) {
            var response = JSON.parse(response);
            if(response.code == 200){
                var html = '';
                $.each(response.data, function (key, value) {
                    html += '<option value="'+value.id+'">'+value.currency+'('+value.symbol+')</option>';
                });
                $('#currency').html(html);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function loadData(){
    $.ajax({
        type: 'post',
        url: '/settings-ajax',
        data: {class: 'DefaultSettingsAjax', action: "getDefaultSettings"},
        success: function (response) {
            var response = JSON.parse(response);
            if(response.code == 200){
               $('#default_rent').val(response.data.default_rent);
               $('#application_fees').val(response.data.application_fees);
               $('#zip_code').val(response.data.zip_code);
               $('#country').val(response.data.country);
               $('#state2').val(response.data.state);
               $('#city2').val(response.data.city);
               $('#currency').val(response.data.currency_id);
               $('#payment_method').val(response.data.payment_method);
               $('#property_size').val(response.data.property_size);
               $('#timeout').val(response.data.timeout);
               $('#page_size').val(response.data.page_size);
               $('#notice_period').val(response.data.notice_period);
               $('#refund_time_out').val(response.data.refund_time_out);
               $('.currency_symbol').html(response.data.symbol);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

$('#default_settings').on('submit',function(event){
    event.preventDefault();
    var formData = $('#default_settings').serializeArray();
    if($('#default_settings').valid()){
        $.ajax({
            type: 'post',
            url: '/settings-ajax',
            data: {class: 'DefaultSettingsAjax', action: "updateDefaultSettings", form: formData},
            success: function (response) {
                var response = JSON.parse(response);
                if(response.code == 200){
                    toastr.success(response.message);
                    loadData();
                }else if(response.code == 400){

                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});



/*------------------------------Zip_Code_Master_start-----------------------------------------*/

/**
 * Auto fill the city, state and country when focus loose on zipcode field.
 */
$("#zip_code_master").focusout(function () {
    getZipCode('#zip_code_master',$(this).val(),'#city_master','#state_master','#country_master','county_master','#zip_code_master_validate');
});

$(document).on('click','#add_zip_code',function(){
    $('#zip_code_master_div').show(500);
    headerDiv.innerText = "Add Zip Code";
    $('#saveBtnId').val('Save');
    $('#zip_code_master_form').trigger("reset");
    $('#zip_code_id').val('');
    jQuery('.table').find('tr:eq(1)').find('td:eq(0)').removeClass("green_row_left");
    jQuery('.table').find('tr:eq(1)').find('td:eq(7)').removeClass("green_row_right");
});

$(document).on('click','#zip_code_master_cancel_btn',function(){
    bootbox.confirm({
        message: "Do you want to cancel this action now ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $('#zip_code_id').val('');
                $('#zip_code_master_div').hide(500);
            }
        }
    });
});

//adding custom fields
$('#zip_code_master_form').on('submit',function(event) {
    event.preventDefault();
    //checking custom field validation
    if ($('#zip_code_master_form').valid()) {
        var formData = $('#zip_code_master_form').serializeArray();
        $.ajax({
            type: 'post',
            url: '/settings-ajax',
            data: {class: 'DefaultSettingsAjax', action: "addZipCodeMaster", form: formData},
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    $("#zip_code_master_div").hide(500);
                    toastr.success(response.message);
                } else if(response.status == 'error' && response.code == 500){
                    if(response.message == 'Zip Code allready exists!') bootbox.alert(response.message); return false;
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#'+key).text(value);
                    });
                } else if(response.status == 'error' && response.code == 400){
                    toastr.warning(response.message);
                }
                $("#zip_code_master_table").trigger('reloadGrid');
                setTimeout(function(){
                    jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('.table').find('tr:eq(1)').find('td:eq(7)').addClass("green_row_right");
                }, 300);
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
});

jqGridZipCode();
//zip code master jqgrid
/**
 * jqGrid Intialization function
 * @param status
 */
function jqGridZipCode(status) {
    var table = 'zip_code_master';
    var columns = ['Zip/PostalCode', 'Latitude','Longitude','City','State/Province','County','Country','Action'];
    var select_column = ['Edit','Delete'];
    var joins = [];//[{table:'users',column:'subscription_plan',primary:'id',on_table:'plans'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = ['zip_code_master.deleted_at'];
    var columns_options = [
        {name:'Zip/PostalCode',index:'zip_code' , width:150,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Latitude',index:'latitude', width:135,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Longitude',index:'longitude',align:"center", width:135,searchoptions: {sopt: conditions},table:table},
        {name:'City',index:'city',align:"center", width:150,searchoptions: {sopt: conditions},table:table},
        {name:'State/Province',index:'state',align:"center", width:150,searchoptions: {sopt: conditions},table:table},
        {name:'County',index:'county',align:"center", width:150,searchoptions: {sopt: conditions},table:table},
        {name:'Country',index:'country', align:"center",width:150,searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'select', width:100,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false, editoptions: { dataInit: function( elem ){}}}
    ];
    var ignore_array = [];
    jQuery("#zip_code_master_table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Zip Code Master",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid");
}

//zip code master listing
/**  List Action Functions  */
$(document).on('change', '.select_options', function() {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    var row_num = $(this).parent().parent().index() ;
    if (opt == 'Edit' || opt == 'EDIT') {
        $('.table').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        jQuery('.table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('.table').find('tr:eq('+row_num+')').find('td:eq(7)').addClass("green_row_right");
        $(".clear-btn").text('Reset');
        $(".clear-btn").addClass('formreset');
        $(".clear-btn").removeClass('clearFormReset');
        $.ajax({
            type: 'post',
            url: '/settings-ajax',
            data: {class: 'DefaultSettingsAjax', action: "getZipCodeMaster", id: id},
            success: function (response) {
                $("#zip_code_master_div").show(500);
                headerDiv.innerText = "Edit Zipcode";
                $('#saveBtnId').val('Update');
                var data = $.parseJSON(response);
                if (data.code == 200) {
                    $("#zip_code_master").val(data.data.zip_code);
                    $("#latitude").val(data.data.latitude);
                    $("#longitude").val(data.data.longitude);
                    $("#city_master").val(data.data.city);
                    $("#state_master").val(data.data.state);
                    $("#county_master").val(data.data.county);
                    $("#country_master").val(data.data.country);
                    $('#zip_code_id').val(data.data.id);
                    defaultFormData = $('#zip_code_master_form').serializeArray();
                } else if (data.status == "error"){
                    toastr.error(data.message);
                } else{
                    toastr.error(data.message);
                }
               // $("#zip_code_master_table").trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        $('.select_options').prop('selectedIndex',0);

    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/settings-ajax',
                        data: {class: 'DefaultSettingsAjax', action: "deleteZipCodeMaster", id: id},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                }
                $('#zip_code_master_table').trigger('reloadGrid');
            }
        });
    }
});


function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

