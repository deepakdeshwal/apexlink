$(document).ready(function () {
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');

    /**
     * jqGrid Initialization function
     * @param status
     */
    function jqGrid(status,selectedRoles) {
        var protocol = location.port;
        if (protocol != "") {
            var webpath = location.protocol+"//"+location.hostname+":"+protocol;
        }else{
            var webpath = location.protocol+"//"+location.hostname;
        }
        var table = 'users';
        var columns = ['Name', 'Email','Phone', 'Last Login', 'Role', 'Status', 'Action'];
        var extra_dropdown = [];
        var select_column = ['Edit','status','Delete'];
        var ignore_array = [{column:'users.id',value:'1'}];
        var joins = [{table:'users',column:'role',primary:'id',on_table:'company_user_roles'}];
        var conditions = ["eq","bw","ew","cn","in"];
        if(selectedRoles){
            var extra_where = [{column:'role', value : selectedRoles,condition:'IN'}];
        } else {
            var extra_where = [];
        }
        var extra_columns = ['users.user_type', 'users.updated_at'];
        var columns_options = [
            {name:'Name',index:'name', width:150,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'username'},
            {name:'Email',index:'email', width:150,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Phone',index:'mobile_number', width:80,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Last Login',index:'last_login', width:80,align:"right",searchoptions: {sopt: conditions},table:table,change_type:'last_login'},
            {name:'Role',index:'role_name', width:80, align:"center",searchoptions: {sopt: conditions},table:'company_user_roles'},
            {name:'Status',index:'status', width:50,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Action',index:'', title: false, width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: function () {
                    return "<img src='"+webpath+"/company/images/icon6.png' id='restoreManageUser' title='Restore' alt='my image'/>";
                }, edittype: 'select',search:false,table:table}
        ];
        jQuery("#manager_id").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                extra_dropdown:extra_dropdown,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at: false,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Manage User",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {top:10,left:200,drag:true,resize:false}
        );
    }
    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == '1')
            return "InActive";
        else if(cellValue == '0')
            return "InActive";
        else
            return '';
    }
    function restoreManageUser(id){
        $.ajax({
            type: 'post',
            url: '/ManageUser-Ajax',
            data: {
                class: "manageUsers",
                action: "restoreManageUser",
                cuser_id: id

            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);
                $("#manager_id").trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    }

    $(document).on("click", "#restoreManageUser", function (e) {
        e.preventDefault();
        var id = $(this).parents("tr").attr('id');
        console.log(id);
        bootbox.confirm("Do you want to restore this record?", function (result) {
            if (result == true) {
                restoreManageUser(id);
            } else {
                window.location.href =  window.location.origin+'/TrashBin/ManageUser'
            }
        });
        /* var action = this.value;
         var id = $(this).attr('data_id');
     */
    });

});



