$(document).ready(function () {
    var base_url = window.location.origin;
    var login_user_id =  localStorage.getItem("login_user_id");
    var status =  localStorage.getItem("active_inactive_status");

    if(status !== undefined) {
        if ($("#propertstyle-table")[0].grid) {
            $('#propertstyle-table').jqGrid('GridUnload');
        }
        //intializing jqGrid
        if(status == 'all'){
            jqGrid('All');
        }  else {
            jqGrid(status);
        }
        $('#jqGridStatus option[value='+status+']').attr("selected", "selected");

    }else{
        if ($("#propertstyle-table")[0].grid) {
            $('#propertstyle-table').jqGrid('GridUnload');
        }
        jqGrid('All');
    }

    //jqGrid status
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        $('#propertstyle-table').jqGrid('GridUnload');
        changeGridStatus(selected);
        jqGrid(selected);
        $("#addPropertystyleForm").hide(500);
    });

    /**
     * jqGrid Intialization function
     * @param status
     */
    function jqGrid(status) {
        var protocol = location.port;
        if (protocol != "") {
            var webpath = location.protocol+"//"+location.hostname+":"+protocol;
        }else{
            var webpath = location.protocol+"//"+location.hostname;
        }
        var table = 'company_property_style';
        var columns = ['Property Style','Description','Status','Action'];
        var select_column = ['Edit','Deactivate','Delete'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['company_property_style.status','company_property_style.is_editable'];
        var columns_options = [
            {name:'Property Style',index:'property_style', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Description',index:'description', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter},
            //{name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false}
            {name:'Action',index:'', title: false, width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: function () {
                    return "<img src='"+webpath+"/company/images/icon6.png' id='restoreProperty'  title='Restore' alt='my image'/>";
                }, edittype: 'select',search:false,table:table},        ];
        var ignore_array = [];
        jQuery("#propertstyle-table").jqGrid({
            url: '/Companies/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            sortname: 'updated_at',
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_property_style",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:false
            },
            viewrecords: true,
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Property Style",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:200,drag:true,resize:false} // search options
        );
    }

    $(document).on("click",'#export_property_style_button',function(){
        var status = $("#jqGridStatus option:selected").val();
        var table =  'company_property_style';
        window.location.href = base_url+"/export-style-excel?status="+status+"&&table="+table+"&&action=exportExcel";
    });

    $(document).on("click",'#export_sample_property_style_button',function(){
        window.location.href = base_url+"/export-style-excel?status="+"&&action=exportSampleExcel";
    });

    /**
     * jqGrid function to format status
     * @param status
     */
    function statusFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Inactive";
        else if(cellvalue == '0')
            return "Inactive";
        else
            return '';
    }
    /**
     * jqGrid function to format is_default column
     * @param status
     */
    function isDefaultFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Yes";
        else if(cellvalue == '0')
            return "No";
        else
            return '';
    }



    /**
     * jqGrid function to format action column
     * @param status
     */

    function actionFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var editable = $(cellvalue).attr('editable');
            var select = '';
            if(rowObject.Status == 1)  select = ['Edit','Deactivate','Delete'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete'];
            if(rowObject.Status == '0' && rowObject.is_editable == '0')  select = ['Edit','Activate'];
            if(rowObject.Status == '0' && rowObject.is_editable == '1')  select = ['Edit','Activate','Delete'];
            if(rowObject.Status == '1' && rowObject.is_editable == '0')  select = ['Edit','Activate'];
            if(rowObject.Status == '1' && rowObject.is_editable == '1')  select = ['Edit','Activate','Delete'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    if(editable == '0' && (val == 'delete' || val == 'Delete')){
                        return true;
                    }
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    /**
     *change function to perform various actions(edit ,activate,deactivate)
     * @param status
     */
    jQuery(document).on('change','.select_options',function () {
        var select_options = $(this).val();
        var data_id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index() ;
        if(select_options == 'Edit')
        {
            $('.table').find('.green_row_left, .green_row_right').each(function(){
                $(this).removeClass("green_row_left green_row_right");
            });
            jQuery('.table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
            var validator = $( "#add_property_style" ).validate();
            validator.resetForm();
            $("#save_property_style").val('Update');
            $.ajax
            ({
                type: 'post',
                url: '/MasterData/PropertyStyle-Ajax',
                data: {
                    class: "PropertyStyleAjax",
                    action: "getPropertyStyleData",
                    id:data_id,
                },
                success: function (response) {
                    $("#addPropertystyleForm").show(500);
                    var response = $.parseJSON(response);
                    var property_style_data = response['data']['property_style_data'];
                    if(property_style_data)
                    {
                        $('#form_type').val('edit');
                        $('#property_style_id').val(data_id);
                        $.each(property_style_data, function (key, value) {
                            $('#' + key).val(value);
                            //$('.' + key).text(value);
                            if(key == 'is_default' && value == 1){
                                $('#' + key).prop("checked",true);

                            }else{
                                $('#' + key).prop("checked",false);
                            }
                            if(key == 'is_editable' && value == 0){
                                // alert()
                                jQuery("#property_style").prop("readonly", true);
                                // $('#' + key).attr('readonly', true);

                            }
                        });
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }else if(select_options == 'Deactivate') {
            bootbox.confirm({
                message: "Do you want to deactivate the record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/MasterData/PropertyStyle-Ajax',
                            data: {class: 'PropertyStyleAjax', action: 'deactivate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Property Style Deactivated successfully');
                                }  else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                }else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                }
            });
        }else if(select_options == 'Activate') {
            bootbox.confirm({
                message: "Do you want to activate the record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/MasterData/PropertyStyle-Ajax',
                            data: {class: 'PropertyStyleAjax', action: 'activate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Property Style activated successfully');
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                }
            });
        }else if(select_options == 'Delete') {
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/MasterData/PropertyStyle-Ajax',
                            data: {class: 'PropertyStyleAjax', action: 'delete', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Property Style deleted successfully');
                                }else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                }
            });
        }
        $('.select_options').prop('selectedIndex',0);
    });

    $(document).on('click','#add_property_style_button',function (e) {
        $('#description').html('');
        $('#save_property_style').val('Save');
        $("#addPropertystyleForm").show(500)
    });

    $(document).on('click','#import_property_style_button',function (e) {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $("#ImportProperty").show(500);
        return false;
    });



    $(document).on("click", "#property_style_cancel", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#ImportProperty").hide(500);
                }
            }
        });
    });


    $(document).on("click", "#import_type_cancel", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#ImportProperty").hide(500);
                }
            }
        });
    });


});

function triggerReload(){
    var grid = $("#propertstyle-table");
    grid[0].p.search = false;
    $.extend(grid[0].p.postData,{filters:""});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}
function restorepropertyType(id){
    $.ajax({
        type: 'post',
        url: '/MasterData/PropertyStyle-Ajax',
        data: {
            class: "PropertyStyleAjax",
            action: "restorepropertyType",
            cuser_id: id

        },
        success: function (response) {

            var response = JSON.parse(response);

                    toastr.success(response.message);

                    $("#propertstyle-table").trigger('reloadGrid');
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

}

$(document).on("click", "#restoreProperty", function (e) {
    e.preventDefault();
    var id = $(this).parents("tr").attr('id');
    console.log(id);
    bootbox.confirm("Do you want to restore this record?", function (result) {
        if (result == true) {
            restorepropertyType(id);
        } else {
            window.location.href =  window.location.origin+'/TrashBin/PropertyStyle'
        }
    });
    /* var action = this.value;
     var id = $(this).attr('data_id');
 */
});


