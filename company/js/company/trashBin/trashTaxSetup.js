$(document).ready(function () {
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');

    /**
     * jqGrid Initialization function
     * @param status
     */
    function jqGrid(status, deleted_at) {
        var protocol = location.port;
        if (protocol != "") {
            var webpath = location.protocol+"//"+location.hostname+":"+protocol;
        }else{
            var webpath = location.protocol+"//"+location.hostname;
        }
        var table = 'company_accounting_tax_setup';
        var columns = ['Tax Name','Type', 'Value', 'Status', 'Action'];
        var select_column = ['Edit','Deactivate','Delete'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var pagination;
        var columns_options = [
            {name:'Tax Name',index:'tax_name', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Type',index:'tax_type', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Value',index:'value', width:80,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Status',index:'status', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Action',index:'', title: false, width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: function () {
                    return "<img src='"+webpath+"/company/images/icon6.png' id='restoreTaxSetup' title='Restore' alt='my image'/>";
                }, edittype: 'select',search:false,table:table}
        ];
        var ignore_array = [];
        jQuery("#taxsetup_id").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_accounting_tax_setup",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                deleted_at:false
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Tax",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {top:200,left:200,drag:true,resize:false}
        );
    }

    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "InActive";
        else if(cellValue == '0')
            return "InActive";
        else
            return '';
    }
    function restoreTaxSetup(id){
        $.ajax({
            type: 'post',
            url: '/MasterData/Tax-Ajax',
            data: {
                class: "TaxTypeAjax",
                action: "restoreTaxSetup",
                cuser_id: id

            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);
                $("#taxsetup_id").trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    }

    $(document).on("click", "#restoreTaxSetup", function (e) {
        e.preventDefault();
        var id = $(this).parents("tr").attr('id');
        console.log(id);
        bootbox.confirm("Do you want to restore this record?", function (result) {
            if (result == true) {
                restoreTaxSetup(id);
            } else {
                window.location.href =  window.location.origin+'/TrashBin/TaxSetup  '
            }
        });
        /* var action = this.value;
         var id = $(this).attr('data_id');
     */
    });

});