$(document).ready(function () {
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');

    //jquery grid for property insurance
    function jqGrid(status, deleted_at) {
        var protocol = location.port;
        if (protocol != "") {
            var webpath = location.protocol+"//"+location.hostname+":"+protocol;
        }else{
            var webpath = location.protocol+"//"+location.hostname;
        }
        var table = 'building_detail';
        var columns = ['Building Name', 'Building Id', 'No of Units','Linked Units','Status', 'Action'];
        var select_column = ['Edit', 'Delete'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [''];
        var columns_options = [
            {name: 'Building Name', index: 'building_name', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Building Id', index: 'building_id', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'No of Units', index: 'no_of_units', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Linked Units', index: 'linked_units', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Status', index: 'status', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: statusFormatter},
            {name:'Action',index:'', title: false, width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: function () {
                    return "<img src='"+webpath+"/company/images/icon6.png' id='restoreBuilding' title='Restore' alt='my image'/>";
                }, edittype: 'select',search:false,table:table}
        ];
        var ignore_array = [];
        jQuery("#building_id").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height:'100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "building_detail",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at: false
            },
            viewrecords: true,
            sortname:'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Buildings",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}

            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:200,drag:true,resize:false} // search options
        );
    }

    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == '1')
            return "InActive";
        else if(cellValue == '0')
            return "InActive";
        else
            return '';
    }
    function restoreBuilding(id){
        $.ajax({
            type: 'post',
            url: '/building-ajax',
            data: {
                class: "buildingDetail",
                action: "restoreBuilding",
                cuser_id: id

            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);
                $("#building_id").trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    }

    $(document).on("click", "#restoreBuilding", function (e) {
        e.preventDefault();
        var id = $(this).parents("tr").attr('id');
        console.log(id);
        bootbox.confirm("Do you want to restore this record?", function (result) {
            if (result == true) {
                restoreBuilding(id);
            } else {
                window.location.href =  window.location.origin+'/TrashBin/Building'
            }
        });
        /* var action = this.value;
         var id = $(this).attr('data_id');
     */
    });


});