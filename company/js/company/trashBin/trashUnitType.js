$(document).ready(function () {
    var base_url = window.location.origin;

    var status =  localStorage.getItem("active_inactive_status");
    console.log(status);
    if(status !== undefined) {
        if ($("#UnitType-table")[0].grid) {
            $('#UnitType-table').jqGrid('GridUnload');
        }
        //intializing jqGrid
        if(status == 'all'){
            jqGrid('All');
        }  else {
            jqGrid(status);
        }
        $('#jqGridStatus option[value='+status+']').attr("selected", "selected");

    }else{
        if ($("#UnitType-table")[0].grid) {
            $('#UnitType-table').jqGrid('GridUnload');
        }
        jqGrid('All');
    }

    /** Show add new unit type div on add new button click */
    $(document).on('click','#addUnitTypeButton',function () {
        $('#unit_type').val('').prop('disabled', false);
        $('#description').val('');
        $("#unit_type_id").val('');
        $('#unit_typeErr').text('');
        headerDiv.innerText = "Add Unit Type";
        $('#is_default').prop('checked', false);
        $('#saveBtnId').val('Save');
        $('#add_unit_type_div').show(500);
    });

    /** Show import excel div on import excel button click */
    $(document).on('click','#importUnitTypeButton',function () {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $('#import_unit_type_div').show(500);
    });

    /** Hide add new unit type div on cancel button click */
    $(document).on("click", "#add_unit_cancel_btn", function (e) {
        e.preventDefault();
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#add_unit_type_div").hide(500);
                }
            }
        });
    });

    /** Hide import excel div on cancel button click */
    $(document).on("click", "#import_unit_cancel_btn", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#import_unit_type_div").hide(500);
                }
            }
        });
    });

    /** jqGrid status */
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        $('#UnitType-table').jqGrid('GridUnload');
        $('#add_unit_type_div').hide(500);
        changeGridStatus(selected);
        jqGrid(selected, false);
    });

    /**
     * jqGrid Initialization function
     * @param status
     */
    function jqGrid(status) {
        var protocol = location.port;
        if (protocol != "") {
            var webpath = location.protocol+"//"+location.hostname+":"+protocol;
        }else{
            var webpath = location.protocol+"//"+location.hostname;
        }
        var table = 'company_unit_type';
        var columns = ['Unit Type','Description', 'Status', 'Action'];
        var select_column = ['Recycle'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['company_unit_type.status', 'company_unit_type.deleted_at','company_unit_type.is_editable'];
        var columns_options = [
            {name:'Unit Type',index:'unit_type', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Description',index:'description', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Action',index:'', title: false, width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: function () {
                    return "<img src='"+webpath+"/company/images/icon6.png' id='restoreProperty'  title='Restore' alt='my image'/>";
                }, edittype: 'select',search:false,table:table},];
        var ignore_array = [];
        jQuery("#UnitType-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_unit_type",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:false
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Unit Types",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:200,left:200,drag:true,resize:false} // search options
        );
    }

    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Inactive";
        else if(cellValue == '0')
            return "Inactive";
        else
            return '';
    }

    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isDefaultFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Yes";
        else if(cellValue == '0')
            return "No";
        else
            return '';
    }

    /**
     * jqGrid function to format action column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function actionFormatter (cellValue, options, rowObject){
        if(rowObject !== undefined) {
            var is_default = rowObject;
            var editable = $(cellValue).attr('editable');
            var select = '';
            if(rowObject.Status == 1)  select = ['Edit','Deactivate','Delete'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete'];
            var data = '';
            if(select != '') {
                var data = '<select ' +
                    ' class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    if(editable == '0' && (val == 'delete' || val == 'Delete')){
                        return true;
                    }
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }


    /** Add/Edit new unit type */
    // $('#add_unit_type_form').on('submit',function(e){
    $("#add_unit_type_form").validate({
        rules: {
            unit_type: {
                required: true
            }
        },
        submitHandler: function () {
            var unit_type = $('#unit_type').val();
            var description = $('#description').val();
            var unit_type_id = $("#unit_type_id").val();
            var is_default;
            if ($('#is_default').is(":checked")) {
                is_default = '1';   // it is checked
            } else {
                is_default = '0';
            }
            var formData = {
                'unit_type': unit_type,
                'description': description,
                'is_default': is_default,
                'unit_type_id': unit_type_id,
            };

            var action;
            if (unit_type_id) {
                action = 'update';
            } else {
                action = 'insert';
            }
            $.ajax({
                type: 'post',
                url: '/UnitType-Ajax',
                data: {
                    class: 'UnitTypeAjax',
                    action: action,
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        $("#add_unit_type_div").hide(500);
                        $("#UnitType-table").trigger('reloadGrid');
                        toastr.success(response.message);
                        onTop(true);
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('#' + key).text(value);
                        });
                    } else if (response.status == 'error' && response.code == 503) {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });

    /**  List Action Functions  */
    $(document).on('change', '.select_options', function() {
        setTimeout(function(){ $(".select_options").val("default"); }, 200);

        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index() ;
        var status = $(this).attr('status');
        if (opt == 'Edit' || opt == 'EDIT') {

            $("#add_unit_type_div").show(500);
            headerDiv.innerText = "Edit Unit Type";
            $('#saveBtnId').val('Update');
            $('#unit_typeErr').text('');
            $('.table').find('.green_row_left, .green_row_right').each(function(){
                $(this).removeClass("green_row_left green_row_right");
            });
            $('.table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
            $('.table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");


            $.ajax
            ({
                type: 'post',
                url: '/UnitType-Ajax',
                data: {
                    class: "UnitTypeAjax",
                    action: "view",
                    id : id,
                },
                success: function (response) {
                    var data = $.parseJSON(response);
                    if (data.status == "success")
                    {
                        $("#unit_type").val(data.data.unit_type);
                        $("#description").val(data.data.description);
                        $("#unit_type_id").val(data.data.id);

                        if(data.data.is_editable == 0) {
                            $('#unit_type').prop('disabled', true);
                        } else {
                            $('#unit_type').prop('disabled', false);
                        }
                        if (data.data.is_default == 1) {
                            $('#is_default').prop('checked', true);
                        } else {
                            $('#is_default').prop('checked', false);
                        }

                    }else if(data.status == "error"){
                        toastr.error(data.message);
                    } else{
                        toastr.error(data.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });


        } else if (opt == 'Deactivate' || opt == 'DEACTIVATE' || opt == 'Activate' || opt == 'ACTIVATE') {
            opt = opt.toLowerCase();
            bootbox.confirm({
                message: "Do you want to " + opt + " this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        var status = opt == 'activate' ? '1' : '0';
                        $.ajax({
                            type: 'post',
                            url: '/UnitType-Ajax',
                            data: {
                                class: 'UnitTypeAjax',
                                action: 'updateStatus',
                                status: status,
                                id: id
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                                localStorage.setItem("rowcolor", 'add colour');
                            }
                        });
                    }
                    $('#UnitType-table').trigger('reloadGrid');
                }
            });
        } else if (opt == 'Delete' || opt == 'DELETE') {
            opt = opt.toLowerCase();
            if (status == '1') {
                toastr.warning('A default set value cannot be deleted.');
            } else {
                bootbox.confirm({
                    message: "Do you want to delete this record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/UnitType-Ajax',
                                data: {
                                    class: 'UnitTypeAjax',
                                    action: 'delete',
                                    id: id
                                },
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                }
                            });
                        }
                        $('#UnitType-table').trigger('reloadGrid');
                    }
                });
            }
        }
    });

    /** Export sample unit type excel */
    $(document).on("click",'#exportSampleExcel',function(){
        window.location.href = base_url+"/UnitType-Ajax?action=exportSampleExcel";
    })

    /** Export unit type excel  */
    $(document).on("click",'#exportUnitTypeButton',function(){
        var status = $("#jqGridStatus option:selected").val();
        var table =  'company_unit_type';
        window.location.href = base_url+"/UnitType-Ajax?status="+status+"&&table="+table+"&&action=exportExcel";
    });

    /** Import unit type excel  */
    $("#importUnitTypeFormId").validate({
        rules: { import_file: {
                required: true
            },
        },
        submitHandler: function (form) {
            event.preventDefault();

            var formData = $('#importPropertyForm').serializeArray();
            var myFile = $('#import_file').prop('files');
            var myFiles = myFile[0];
            var formData = new FormData();
            formData.append('file', myFiles);
            formData.append('class', 'UnitTypeAjax');
            formData.append('action', 'importExcel');
            $.ajax
            ({
                type: 'post',
                url: '/UnitType-Ajax',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        toastr.success(response.message);

                        $("#import_unit_type_div").hide(500)
                        $('#UnitType-table').trigger('reloadGrid');
                    } else if(response.status == 'failed' && response.code == 503){
                        toastr.error(response.message);
                        // $('.error').html(response.message);
                        // $.each(response.message, function (key, value) {
                        //     $('.'+key).html(value);
                        // });
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });
    function restorepropertyType(id){
        $.ajax({
            type: 'post',
            url: '/UnitType-Ajax',
            data: {
                class: "UnitTypeAjax",
                action: "restorepropertyType",
                cuser_id: id

            },
            success: function (response) {

                var response = JSON.parse(response);
                toastr.success(response.message);

                $("#UnitType-table").trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    }

    $(document).on("click", "#restoreProperty", function (e) {
        e.preventDefault();
        var id = $(this).parents("tr").attr('id');
        console.log(id);
        bootbox.confirm("Do you want to restore this record?", function (result) {
            if (result == true) {
                restorepropertyType(id);
            } else {
                window.location.href =  window.location.origin+'/TrashBin/UnitType'
            }
        });
        /* var action = this.value;
         var id = $(this).attr('data_id');
     */
    });
});