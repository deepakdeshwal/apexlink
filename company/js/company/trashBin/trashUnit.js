$(document).ready(function () {

    function getParameterByName( name ){
        var regexS = "[\\?&]"+name+"=([^&#]*)",
            regex = new RegExp( regexS ),
            results = regex.exec( window.location.search );
        if( results == null ){
            return "";
        } else{
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

    var id =  getParameterByName('id');
    console.log('id>>', id);
    //jqGrid status
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        var deleted_at = true;
        $('#property-unit-table').jqGrid('GridUnload');
        if(selected == 4) deleted_at = false;
        jqGrid(selected, id);
    });

    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All',id);
    function jqGrid(status,id) {
        var protocol = location.port;
        if (protocol != "") {
            var webpath = location.protocol+"//"+location.hostname+":"+protocol;
        }else{
            var webpath = location.protocol+"//"+location.hostname;
        }
        var table = 'unit_details';
        var columns = ['Floor #','Unit #', 'Building Name & ID', 'Building Name', 'Building ID', 'Status','# of Bedrooms','# of Bathrooms','Actions'];
        var select_column = ['Edit','Flag Bank','Inspection','Add In-Touch','In-Touch History','Deactivate','Activate','Print Envelope','Delete'];
        var joins = [{table:'unit_details',column:'building_id',primary:'id',on_table:'building_detail'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['unit_details.deleted_at', 'unit_details.updated_at'];
        // var extra_where = [{column: 'property_id', value: id, condition: '='}];
        var columns_options = [
            {name:'Floor #',index:'floor_no', width:90,align:"center",searchoptions: {sopt: conditions},table:table, classes: 'pointer'},
            {name:'Unit #',index:'unit_check', align:"center", width:100,searchoptions: {sopt: conditions},table:table, classes: 'pointer'},
            {name:'Building Name & ID',index:'building_name_and_id', width:80,align:"center",searchoptions: {sopt: conditions},table:table, change_type: 'combine_column_hyphen', extra_columns: ['building_name', 'building_id'], update_column: 'building_name_and_id',original_index: 'building_name'},
            {name:'Building ID',index:'building_id', width:80,hidden:true, align:"center",searchoptions: {sopt: conditions},table:'building_detail',attr:{name:'flag',value:'unit'}},
            {name:'Building Name',index:'building_name', hidden: true, width:80,align:"center",searchoptions: {sopt: conditions},table:'building_detail'},
            {name:'status',index:'building_unit_status', width:80, align:"center",searchoptions: {sopt: conditions},table:table, formatter: statusFormatter},
            {name:'# of Bedrooms',index:'bedrooms_no', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'# of Bathrooms',index:'bathrooms_no', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'', title: false, width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: function () {
                    return "<img src='"+webpath+"/company/images/icon6.png' id='restoreUnit' title='Restore' alt='my image'/>";
                }, edittype: 'select',search:false,table:table}
        ];
        var ignore_array = [];
        jQuery("#unit_id").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at: false
                // extra_where:extra_where,
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Units",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {top:10,left:200,drag:true,resize:false}
        );
    }

    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == '1')
            return "InActive";
        else if(cellValue == '0')
            return "InActive";
        else
            return '';
    }
    function restoreUnit(id){
        $.ajax({
            type: 'post',
            url: '/propertyUnitAjax',
            data: {
                class: "PropertyUnitAjax",
                action: "restoreUnit",
                cuser_id: id

            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);
                $("#unit_id").trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    }

    $(document).on("click", "#restoreUnit", function (e) {
        e.preventDefault();
        var id = $(this).parents("tr").attr('id');
        console.log(id);
        bootbox.confirm("Do you want to restore this record?", function (result) {
            if (result == true) {
                restoreUnit(id);
            } else {
                window.location.href =  window.location.origin+'/TrashBin/Unit'
            }
        });
        /* var action = this.value;
         var id = $(this).attr('data_id');
     */
    });

});