$(document).ready(function () {
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');
    function jqGrid(status, deleted_at) {
        var protocol = location.port;
        if (protocol != "") {
            var webpath = location.protocol+"//"+location.hostname+":"+protocol;
        }else{
            var webpath = location.protocol+"//"+location.hostname;
        }
        var table = 'company_accounting_charge_code';
        var columns = ['Charge Code', 'Credit Account', 'Debit Account', 'Description', 'Action'];
        var select_column = ['Edit', 'Deactivate', 'Delete'];
        var joins = [{
            table: 'company_accounting_charge_code',
            column: 'credit_account',
            primary: 'id',
            on_table: 'company_credit_accounts'
        }, {
            table: 'company_accounting_charge_code',
            column: 'debit_account',
            primary: 'id',
            on_table: 'company_debit_accounts'
        }];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['company_accounting_charge_code.status', 'company_accounting_charge_code.deleted_at', 'company_accounting_charge_code.is_editable', 'company_accounting_charge_code.updated_at'];
        var columns_options = [
            {
                name: 'Charge Code',
                index: 'charge_code',
                width: 90,
                align: "center",
                searchoptions: {sopt: conditions},
                table: table
            },
            {
                name: 'Credit Account',
                index: 'credit_accounts',
                width: 100,
                searchoptions: {sopt: conditions},
                table: 'company_credit_accounts'
            },
            {
                name: 'Debit Account',
                index: 'debit_accounts',
                width: 100,
                searchoptions: {sopt: conditions},
                table: 'company_debit_accounts'
            },
            {name: 'Description', index: 'description', width: 100, searchoptions: {sopt: conditions}, table: table},

            {name:'Action',index:'', title: false, width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: function () {
                    return "<img src='"+webpath+"/company/images/icon6.png' id='restoreChargeCode' title='Restore' alt='my image'/>";
                }, edittype: 'select',search:false,table:table},

        ];
        var ignore_array = [];
        jQuery("#charge_id").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_accounting_charge_code",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                deleted_at: false
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Charge Code",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {},
            {},
            {},
            {top: 200, left: 200, drag: true, resize: false}
        );
    }

    function restoreChargeCode(id){
        $.ajax({
            type: 'post',
            url: '/ChargeCode-Ajax',
            data: {
                class: "ChargeCodeAjax",
                action: "restorechargeCode",
                cuser_id: id

            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);
                $("#charge_id").trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    }

    $(document).on("click", "#restoreChargeCode", function (e) {
        e.preventDefault();
        var id = $(this).parents("tr").attr('id');
        console.log(id);
        bootbox.confirm("Do you want to restore this record?", function (result) {
            if (result == true) {
                restoreChargeCode(id);
            } else {
                window.location.href =  window.location.origin+'/TrashBin/ChargeCode'
            }
        });
        /* var action = this.value;
         var id = $(this).attr('data_id');
     */
    });
});