$(document).ready(function () {
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');

    function jqGrid(status, deleted_at) {
        var protocol = location.port;
        if (protocol != "") {
            var webpath = location.protocol+"//"+location.hostname+":"+protocol;
        }else{
            var webpath = location.protocol+"//"+location.hostname;
        }
        var table = 'company_accounting_charge_code_preferences';
        var columns = ['Charge Code','Applicable','UnitTypeId','Set as Default', 'Frequency', 'Status', 'Action'];
        var select_column = ['Edit','Deactivate','Delete'];
        var joins = [{table:'company_accounting_charge_code_preferences',column:'charge_code',primary:'id',on_table:'company_accounting_charge_code'}, {table:'company_accounting_charge_code_preferences',column:'unit_type_id',primary:'id',on_table:'company_unit_type'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['company_accounting_charge_code_preferences.deleted_at', 'company_accounting_charge_code_preferences.updated_at'];
        var extra_where = [];
        var columns_options = [
            {name:'Charge Code',index:'charge_code', width:90,align:"center",searchoptions: {sopt: conditions},table:'company_accounting_charge_code'},
            {name:'Applicable',index:'charge_code_type', width:100,align:"center",searchoptions: {sopt: conditions},table:table, formatter:applicableFormatter},
            {name:'UnitTypeId',index:'unit_type', width:100,align:"center", hidden: true, searchoptions: {sopt: conditions},table:'company_unit_type'},
            {name:'Set as Default',index:'is_default', width:100,align:"center", hidden: true, searchoptions: {sopt: conditions},table:table},
            {name:'Frequency',index:'frequency', width:100,align:"center",searchoptions: {sopt: conditions},table:table, formatter:frequencyFormatter},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Action',index:'', title: false, width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: function () {
                    return "<img src='"+webpath+"/company/images/icon6.png' id='restoreChargeCodePreferences' title='Restore' alt='my image'/>";
                }, edittype: 'select',search:false,table:table},
        ];
        var ignore_array = [];
        jQuery("#chargeCodePreference-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_accounting_charge_code_preferences",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:false,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Charge Code Preferences",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {top:200,left:200,drag:true,resize:false}
        );
    }

    /**
     * jqGrid function to format status
     * @param status
     */
    function statusFormatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Inactive";
        else if(cellvalue == '0')
            return "Inactive";
        else
            return '';
    }

    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function frequencyFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "One Time";
        else if(cellValue == 2)
            return "Monthly";
        else
            return '';
    }


    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function applicableFormatter (cellValue, options, rowObject){
        if (cellValue == 1){
            var unitType = rowObject;
            if(unitType.UnitTypeId != undefined) {
                var unitTypeName = unitType.UnitTypeId;
                unitTypeName = "Unit - " + unitTypeName;
                return unitTypeName;
            } else {
                return 'Unit';
            }
        } else if(cellValue == '0'){
            return "Property";
        } else {
            return '';
        }
    }

    function restoreChargeCodePrefer(id){
        $.ajax({
            type: 'post',
            url: '/ChargeCodePreferences-Ajax',
            data: {
                class: "ChargeCodePreferencesAjax",
                action: "restoreChargeCodePreference",
                data_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    toastr.success(response.message);
                    $("#chargeCodePreference-table").trigger('reloadGrid');
                }
            }
        });

    }

    $(document).on("click", "#restoreChargeCodePreferences", function (e) {
        e.preventDefault();
        var id = $(this).parents("tr").attr('id');
        console.log(id);
        bootbox.confirm("Do you want to restore this record?", function (result) {
            if (result == true) {
                restoreChargeCodePrefer(id);
            }
        });
    });
});