$(document).ready(function () {
    var base_url = window.location.origin;
    var login_user_id =  localStorage.getItem("login_user_id");
    var status =  localStorage.getItem("active_inactive_status");

    if(status !== undefined) {
        if ($("#properttype-table")[0].grid) {
            $('#properttype-table').jqGrid('GridUnload');
        }
        //intializing jqGrid
        if(status == 'all'){
            jqGrid('All');
        }  else {
            jqGrid(status);
        }
        $('#jqGridStatus option[value='+status+']').attr("selected", "selected");

    }else{
        if ($("#properttype-table")[0].grid) {
            $('#properttype-table').jqGrid('GridUnload');
        }
        jqGrid('All');
    }

    //jqGrid status
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        $("#addPropertyTypeForm").hide(500);
        $('#properttype-table').jqGrid('GridUnload');
        changeGridStatus(selected);
        jqGrid(selected);

    });

    /**
     * jqGrid Intialization function
     * @param status
     */
    function jqGrid(status) {
        var protocol = location.port;
        if (protocol != "") {
            var webpath = location.protocol+"//"+location.hostname+":"+protocol;
        }else{
            var webpath = location.protocol+"//"+location.hostname;
        }
        var table = 'company_property_type';
        var columns = ['Property Type','Description', 'Status','Action'];
        var select_column = ['Recycle'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['company_property_type.status','company_property_type.is_editable'];
        var columns_options = [
            {name:'Property Type',index:'property_type', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Description',index:'description', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter},
            //{name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false}
            {name:'Action',index:'', title: false, width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: function () {
                    return "<img src='"+webpath+"/company/images/icon6.png' id='restoreProperty'  title='Restore' alt='my image'/>";
                }, edittype: 'select',search:false,table:table},        ];
        var ignore_array = [];
        jQuery("#properttype-table").jqGrid({
            url: '/Companies/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_property_type",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:'false',

            },
            viewrecords: true,
            sortname:'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Property Types",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:200,drag:true,resize:false} // search options
        );
    }

    $(document).on("click",'#export_property_type_button',function(){
        var status = $("#jqGridStatus option:selected").val();
        var table =  'company_property_type';
        window.location.href = base_url+"/export-excel?status="+status+"&&table="+table+"&&action=exportExcel";


    });

    $(document).on("click",'#export_sample_property_type_button',function(){
        console.log('>>>>>>>>>>>', base_url+"/export-excel?status="+"&&action=exportSampleExcel");
        window.location.href = base_url+"/export-excel?status="+"&&action=exportSampleExcel";
    });

    /**
     * jqGrid function to format status
     * @param status
     */
    function statusFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Inactive";
        else if(cellvalue == '0')
            return "Inactive";
        else
            return '';
    }
    /**
     * jqGrid function to format is_default column
     * @param status
     */
    function isDefaultFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Yes";
        else if(cellvalue == '0')
            return "No";
        else
            return '';
    }






    $(document).on('click','#add_property_type_button',function (e) {
        $('#property_type_span').html('Add Property Type')
        $('#description').html('');
        $('#save_property_type').val('Save');
        $("#addPropertyTypeForm").show(500);
        var validator = $( "#add_property_type" ).validate();
        validator.resetForm();
    })

    $(document).on('click','#import_property_type_button',function (e) {
        $("#import_file").val('');
        $("#import_file-error").text('');
        $("#ImportProperty").show(500);
        return false;
    })



    $(document).on("click", "#property_type_cancel", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#addPropertyTypeForm").hide(500);
                }
            }
        });
    });


    $(document).on("click", "#import_type_cancel", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#ImportProperty").hide(500);
                }
            }
        });
    });

    $(document).on("click", "#create_stripe_accounr", function (e) {
        $.ajax({
            type: 'post',
            url: '/propertytype-ajax',
            data: {
                class: "PropertyTypeAjax",
                action: "enableAccount",
            },
            success: function (response) {
                var response =  JSON.parse(response);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });


});

function restorepropertyType(id){
    $.ajax({
        type: 'post',
        url: '/propertytype-ajax',
        data: {
            class: "ManageVehicleAjax",
            action: "restorepropertyType",
            cuser_id: id

        },
        success: function (response) {

            var response = JSON.parse(response);

                    toastr.success(response.message);
                    $("#properttype-table").trigger('reloadGrid');
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

}

$(document).on("click", "#restoreProperty", function (e) {
    e.preventDefault();
    var id = $(this).parents("tr").attr('id');
    console.log(id);
    bootbox.confirm("Do you want to restore this record?", function (result) {
        if (result == true) {
            restorepropertyType(id);
        } else {
            window.location.href =  window.location.origin+'/TrashBin/PropertyType'
        }
    });
   /* var action = this.value;
    var id = $(this).attr('data_id');
*/
});


function triggerReload(){
    var grid = $("#properttype-table");
    grid[0].p.search = false;
    $.extend(grid[0].p.postData,{filters:""});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}



