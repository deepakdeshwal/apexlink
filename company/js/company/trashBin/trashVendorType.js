/**
 * jqGrid function to format status
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function statusFormatter (cellValue, options, rowObject){
    if (cellValue == 1)
        return "Inactive";
    else if(cellValue == '0')
        return "Inactive";
    else
        return '';
}
function statusFormatter1 (cellValue, options, rowObject){
    if (cellValue == 1)
        return "False";
    else if(cellValue == '0')
        return "True";
    else
        return '';
}
function statusFmatter1 (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';

        if(rowObject['Status'] == '1' && rowObject['editable'] == '0')  select = ['Edit','Deactivate','Delete'];
        if(rowObject['Status'] == '1' && rowObject['editable'] == '1')  select = ['Edit','Deactivate'];
        if((rowObject['Status'] == '0' || rowObject.Status == '' )&& rowObject['editable'] == '0')  select = ['Edit','Activate','Delete'];
        if((rowObject['Status'] == '0' || rowObject.Status == '' ) && rowObject['editable'] == '1')  select = ['Edit','Activate'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}
function statusFmatter2 (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';
        if(rowObject['is_editable'] == '1')  select = ['Edit','Deactivate'];
        if(rowObject['is_editable'] == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}




/**
 * jqGrid Initialization function
 * @param status
 */
jqGrid('All')
function jqGrid(status) {
    var protocol = location.port;
    if (protocol != "") {
        var webpath = location.protocol+"//"+location.hostname+":"+protocol;
    }else{
        var webpath = location.protocol+"//"+location.hostname;
    }
    var table = 'company_vendor_type';
    var columns = ['Vendor type','Vendor Description', 'Status', 'Action','editable'];
    if (status===0) {
        var select_column = ['Edit','Deactivate','Delete'];
        console.log(status);
        console.log("-------");
    }else {
        var select_column = ['Edit','Activate','Delete'];
        console.log(status);
        console.log("========");
    }
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];

    var columns_options = [

        {name:'Vendor type',index:'vendor_type', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Vendor Description',index:'description', width:100,searchoptions: {sopt: conditions},table:table},
        {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
        {name:'Action',index:'', title: false, width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: function () {
                return "<img src='"+webpath+"/company/images/icon6.png' id='restoreProperty'  title='Restore' alt='my image'/>";
            }, edittype: 'select',search:false,table:table},
        {name:'editable',index:'is_editable', hidden:true,searchoptions: {sopt: conditions},table:table},
    ];
    var ignore_array = [];
    jQuery("#VendorType-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "company_vendor_type",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:false
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Vendor Types",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:200,left:200,drag:true,resize:false} // search options
    );
}

$(document).ready(function () {
    var base_url = window.location.origin;

    var status =  localStorage.getItem("active_inactive_status");
    console.log(status);



    /** Show add new unit type div on add new button click */
    $(document).on('click','#addUnitTypeButton',function () {
        $('#unit_type').val('').prop('disabled', false);
        $('#description').val('');
        $("#unit_type_id").val('');
        $('#unit_typeErr').text('');
        headerDiv.innerText = "Add Vendor Type";
        $('#is_default').prop('checked', false);
        $('#saveBtnId').val('Save');
        $('#add_unit_type_div').show(500);
    });

    /** Show import excel div on import excel button click */
    $(document).on('click','#importUnitTypeButton',function () {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $('#import_unit_type_div').show(500);
    });

    /** Hide add new unit type div on cancel button click */
    $(document).on("click", "#add_unit_cancel_btn", function (e) {
        e.preventDefault();
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $("#add_unit_type_div").hide(500);
            } else {
            }
        });
    });



    /** Hide import excel div on cancel button click */
    $(document).on("click", "#import_unit_cancel_btn", function (e) {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $("#import_unit_type_div").hide(500);
            } else {
            }
        });
    });


    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Active";
        else if(cellValue == '0')
            return "Inactive";
        else
            return '';
    }

    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isDefaultFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Yes";
        else if(cellValue == '0')
            return "No";
        else
            return '';
    }


    /*change status vendor type*/
    function changeStatusVendorType(id,action) {
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "ApexNewUserAjax",
                action: "changeStatusVendorType",
                apexnewuser_id: id,
                status_type: action,
            },
            success: function (response) {

                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The status has been changed.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#VendorType-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }





    /** Export sample unit type excel */
    $(document).on("click",'#exportSampleExcel',function(){
        window.location.href = base_url+"/UnitType-Ajax?action=exportSampleExcel";
    })

    /** Export unit type excel  */
    $(document).on("click",'#exportUnitTypeButton',function(){
        var status = $("#jqGridStatus option:selected").val();
        var table =  'company_unit_type';
        window.location.href = base_url+"/UnitType-Ajax?status="+status+"&&table="+table+"&&action=exportExcel";
    });
    /*  insert into vendor type*/

    $(document).on("click","#saveBtns", function(e){
        e.preventDefault();

        var vendor_type = $("input[name='vendor_type']").val();
        var desc = $("textarea[name='description']").val();



        // console.log(vendor_type);


        $.ajax({
            type: 'post',
            url: '/MasterData/VendorType-Ajax',
            data: {
                class: 'VendorTypeAjax',
                action: "insert",
                vendor_type: vendor_type,
                desc: desc

            },
            success: function (response) {
                console.log('response>>>', response);
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){

                    $('#VendorType-table').trigger( 'reloadGrid' );
                    $("#add_vendor_type").trigger("reset");
                    $("#add_unit_type_div").hide(500);
                    window.location.reload();


                } else if(response.status == 'error' && response.code == 400){
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.'+key).html(value);
                    });
                }
            },
            error: function (data) {
            }
        });


    });

    function getVendorType(id){
        $.ajax({
            type: 'post',
            url: '/MasterData/VendorType-Ajax',
            data: {
                class: "getVendorType",
                action: "getVendorType",
                cuser_id: id
            },
            success: function (response) {

                var response = JSON.parse(response);
                $("#add_vendor_type input[name='vendor_type']").val(response.vendor_type);
                $("#add_vendor_type textarea[name='description']").val(response.description);
                localStorage.setItem("Message","Vendor Updated Successfully")
                localStorage.setItem("rowcolor",true)

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function update_vendor_status(id,action) {
        $.ajax({
            type: 'post',
            url: '/MasterData/VendorType-Ajax',
            data: {
                class: "VendorTypeAjax",
                action: "update_vendor_status",
                user_id: id,
                status_type: action,

            },
            success: function (response) {

                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {

                    toastr.success("The status has been changed.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                localStorage.setItem("Message","Vendor Updated Successfully")
                localStorage.setItem("rowcolor",true)
                $('#VendorType-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



    function deleteVendorType(id) {
        $.ajax({
            type: 'post',
            url: '/MasterData/VendorType-Ajax',
            data: {
                class: "VendorTypeAjax",
                action: "deleteVendorType",
                user_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The record deleted successfully.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#VendorType-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $(document).on("change", "#VendorType-table .select_options", function (e) {
        e.preventDefault();
        var action = this.value;
        var id = $(this).attr('data_id');
        var checkDelete = $('option[value="Delete"]',this).length;

        if (checkDelete == "0"){
            $("input[name='vendor_type']").attr("disabled",true);
        }else{
            $("input[name='vendor_type']").attr("disabled",false);
        }

        console.log('-----');

        switch(action) {

            case "Edit":
                //Deactivate or Activate manage user
                $("#add_unit_type_div").show();
                getVendorType(id);
                break;
            case "Deactivate":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to "+action+" this record?", function (result) {
                    if (result == true) {
                        update_vendor_status(id,action);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            case "Activate":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to "+action+" this record?", function (result) {
                    if (result == true) {
                        update_vendor_status(id,action);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            case "Delete":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                    if (result == true) {
                        deleteVendorType(id);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            default:
                window.location.href = base_url+'/Setting/ApexNewUser';
        }

        return false;
    });
    function restorepropertyType(id){
        $.ajax({
            type: 'post',
            url: '/MasterData/VendorType-Ajax',
            data: {
                class: "VendorTypeAjax",
                action: "restorepropertyType",
                cuser_id: id

            },
            success: function (response) {

                var response = JSON.parse(response);

                        toastr.success(response.message);
                        $("#VendorType-table").trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    }

    $(document).on("click", "#restoreProperty", function (e) {
        e.preventDefault();
        var id = $(this).parents("tr").attr('id');
        console.log(id);
        bootbox.confirm("Do you want to restore this record?", function (result) {
            if (result == true) {
                restorepropertyType(id);
            } else {
                window.location.href =  window.location.origin+'/MasterData/VendorType'
            }
        });
        /* var action = this.value;
         var id = $(this).attr('data_id');
     */
    });


});