$(document).ready(function() {
    /**
     * jquery to redirect for edits
     */
    $(document).on('click', '.edit_redirection', function () {
        var redirection_data = $(this).attr('redirection_data');
        localStorage.setItem("AccordionHref",'#'+redirection_data);
        window.location.href = window.location.origin+'/Unit/EditUnit?id='+id;
    });
    /**
     * Get Parameters by id
     * @param status
     */
    function getParameterByName(name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

    var id = getParameterByName('id');
    var base_url = window.location.origin;
    // getPropertyDetail(id);
    function getPropertyDetail(id) {

        $.ajax({
            type: 'post',
            url: '/get-property-detail ',
            data: {
                class: 'propertyDetail',
                action: 'gerPropertyDetail',
                id:id
            },
            success: function (response) {
                var res = JSON.parse(response);
                if(res) {
                    $("#property_link_name" ).html(res.property_detail.property_name);
                    $("#property_link_name" ).attr('href',base_url+'/Property/PropertyModules');
                    $("#property_tab_link" ).attr('href',base_url+'/Property/PropertyModules');
                    $("#building_tab_link" ).attr('href',base_url+'/Building/BuildingModule?id='+id);
                    $("#new_property_unit_list_href").attr("href", base_url+"/Unit/UnitModule?id="+id);
                    $("#new_unit_href").attr("href", base_url+"/Unit/AddUnit?id="+id);
                    $("#property_inspection_link").prop("href", base_url+"/Property/PropertyInspection?id="+id);
                    $("#unit_tab_link").prop("href", base_url+"/Unit/UnitModule?id="+id);
                }
            },
        });

    }
    getUnitDetailForView();
    function getUnitDetailForView() {
        var base_url = window.location.origin;
        $.ajax({
            url: "/propertyUnitAjax",
            method: 'post',
            data: {
                class: "PropertyUnitAjax",
                action: "getUnitDetailForView",
                'id': id,
            },
            success: function (response) {
                var response = JSON.parse(response);

                if(response.code=='200')
                {
                    getPropertyDetail(response.data.property_id);
                    $(".renovation_detail").html(response.renovation_detail);
                    $(".school_district_html").html(response.school_district_municipality_data);

                    getCustomFieldForViewMode(response.custom_data);
                    $.each(response.images_data, function (index, img) {
                        var image = base_url + '/company/' + img.file_location
                        $('.owl-carousel').trigger('add.owl.carousel', ['<div class="item"><img src="' + image + '" alt=""></div>'])
                            .trigger('refresh.owl.carousel');
                    });
                    if (response.code == 200) {
                        $.each(response.data, function (key, value) {
                            $('.' + key).html(value);
                        });
                        $("#unit_property_name").html(response.data.property_name);
                        $("#property_link").prop("href", base_url + "/Property/PropertyView?id="+response.data.property_id);
                        $("#building_span_name").html(response.data.building_name);
                        $("#building_link").prop("href", base_url + "/Building/View?id="+response.data.building_id);
                        $("#unit_span_name").html(response.data.unit_name);
                        $("#building_property_id").val(response.data.building_property_id);
                        $("#new_building_href").prop("href", base_url + "/Building/AddBuilding?id=" + response.data.building_property_id);
                        $("#new_unit_href").prop("href", base_url + "/Unit/AddUnit?id=" + response.data.id);

                        if(response.data.key_unit == '2')
                        {
                            unitTagKeys();
                        }else{
                            unitKeysJqgrid();
                        }
                } else {

                }

                } else {
                    window.history.back();
                }
            }
        });
    }
    function actionKeysFormatter(cellValue, options, rowObject) {
        if(rowObject !== undefined){
            console.log(rowObject);
            var protocol = location.port;
            if (protocol != "") {
                var webpath = location.protocol+"//"+location.hostname+":"+protocol;
            }else{
                var webpath = location.protocol+"//"+location.hostname;
            }
            if(rowObject !== undefined) {
                var status = rowObject['Unit Status'];
                var select = '';
                select = "<img src='"+webpath+"/company/images/edit-icon.png' class='editUnit' rowid='"+rowObject.id+"' title='Edit' alt='my image'/>  <img src='"+webpath+"/company/images/cross-icon.png' rowid='"+rowObject.id+"'  class='deactivateUnit'  title='Deactivate' alt='my image'/>";
                var data = '';
                if(select != '') {
                    data = select;
                }
                return data;
            }
        }
    }
    function unitTagKeys(status) {

        var table = 'unit_track_key';
        var columns = ['Name', 'Email', 'CompanyName','Phone#','PickUp Date','PickUp Time','Key#','Key Quantity','PickUpReturn Date','Return Time','ApplicantName',];

        //var select_column = ['Edit', 'Flag Bank', 'Inspection', 'Add In-Touch', 'In-Touch History', 'Deactivate', 'Activate', 'Print Envelope', 'Delete'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['unit_track_key.deleted_at', 'unit_track_key.updated_at'];
        var extra_where = [{column: 'unit_id', value: id, condition: '='}];
        var columns_options = [
            {name: 'Name',index: 'key_name',width: 200,align: "center",searchoptions: {sopt: conditions},table: table},
            {name: 'Email', index: 'email', width: 350, searchoptions: {sopt: conditions}, table: table},
            // {name: 'Total Keys', index: 'total_keys', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:totalkeyFormatter},
            {name: 'CompanyName', index: 'company_name', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Phone#', index: 'phone', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'PickUp Date', index: 'pick_up_date', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'PickUp Time', index: 'pick_up_time', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Key#', index: 'key_number', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Key Quantity', index: 'key_quality', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'PickUpReturn Date', index: 'pick_up_date', width: 300, align: "center",searchoptions: {sopt: conditions}, table: table},
            {name: 'Return Time', index: 'pick_up_time', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'ApplicantName', index: 'key_designator', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
            // {name: 'Status', index: 'status', width: 300, align: "center", hidden:true,searchoptions: {sopt: conditions}, table: table},
            // {name: 'Action', index: 'select', title: false, width: 200, align: "center", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table, formatter:actionKeysFormatter},
        ];
        var ignore_array = [];
        jQuery("#unit-track-keys").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Keys",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }
    function getCustomFieldForViewMode(editCustomFieldData){
        var module = 'unit';
        $.ajax({
            type: 'post',
            url: '/CustomField/get',
            data:{module:module},
            success: function (response) {
                var response = JSON.parse(response);

                var custom_array = [];
                if(response.code == 200 && response.message == 'Record retrieved successfully'){
                    $('.custom_field_html_view_mode').html('');
                    $('.custom_field_msg').html('');
                    var html ='';
                    $.each(response.data, function (key, value) {
                        var default_val = '';
                        if(editCustomFieldData) {
                            default_val = (value.default_value != '' && value.default_value != null) ? value.default_value : '';
                            $.each(editCustomFieldData, function (key1, value1) {
                                if (value1.id == value.id) {
                                    default_val = value1.value;
                                }
                            });
                        } else {
                            default_val = (value.default_value != '' && value.default_value != null) ? value.default_value : '';
                        }
                        var required = (value.is_required == '1')?' <em class="red-star">*</em>':'';
                        var name = value.field_name.replace(/ /g, "_");
                        var currency_class = (value.data_type == 'currency')?' amount':'';
                        if(value.data_type == 'date') {
                            custom_array.push(name);
                        }
                        var readonly = (value.data_type == 'date') ? 'readonly' : '';
                        html += '<div class="row custom_field_class">';
                        html += '<div class=col-sm-6>';
                        html += '<label>'+value.field_name+required+'</label>';
                        html += '<input type="text" readonly data_type="'+value.data_type+'" data_value="'+value.default_value+'" '+readonly+' data_id="'+value.id+'" data_required="'+value.is_required+'" name="'+name+'" id="'+name+'" value="'+default_val+'" class="form-control changeCustom add-input'+currency_class+'">';
                        if(value.is_editable == 1 || value.is_deletable == 1) {
                            if (value.is_editable == 1) {
                                html += '<span id="' + value.id + '" class="editCustomField"><i class="fa fa-edit"></i></span>';
                            }
                            if (value.is_deletable == 1) {
                                html += '<span id="' + value.id + '" class="deleteCustomField"><i class="fa fa-times-circle""></i></span>';
                            }
                        }
                        html += '<span class="customError required"></span>';
                        html += '</div>';
                        html += '</div>';
                    });
                    $(html).prependTo('.custom_field_html_view_mode');
                } else {
                    $('.custom_field_msg').html('No Custom Fields');
                    $('.custom_field_html_view_mode').html('');
                }
                triggerDatepicker(custom_array)
            },
            error: function (data) {
            }
        });
    }

    function triggerDatepicker(custom_array){
        $.each(custom_array, function (key, value) {
            $('#customField'+value).datepicker({
                dateFormat: datepicker
            });
        });
    }
    /**
     * jqGrid unit keys list function
     * @param status
     */

    function unitKeysJqgrid(status) {
        var table = 'unit_keys';
        var columns = ['Key Tag', 'Description','Available Keys'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['unit_keys.available_keys'];
        var extra_where = [{column: 'unit_id', value: id, condition: '='}];
        var columns_options = [
            {name: 'Key Tag',index: 'key_id',width: 100,align: "center",searchoptions: {sopt: conditions},table: table},
            {name: 'Description', index: 'key_description', width: 100, searchoptions: {sopt: conditions}, table: table},
            {name: 'AvailableKeys', index: 'available_keys', width: 100, align: "center",searchoptions: {sopt: conditions}, table: table},
        ];
        var ignore_array = [];
        jQuery("#UnitKeys-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Keys",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: false, refresh:false, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 200, drag: true, resize: false}
        );
    }

    /**
     * jqGrid Building Keys function
     * @param status
     */
    fileLibrary();
    function fileLibrary(status) {
        var table = 'unit_file_uploads';
        var columns = ['File Name', 'File', 'Action'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [{column: 'unit_id', value: id, condition: '='},{column: 'file_type', value: "2", condition: '='}];
        var columns_options = [
            {name: 'File Name',index: 'file_name',width: 100,align: "center",searchoptions: {sopt: conditions},table: table},
            {name:'Preview',index:'file_extension',width:100,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
            {name:'Action',index:'select', width:100,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionLibraryFmatter,title:false}
        ];
        var ignore_array = [];
        jQuery("#file-library").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Files",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 200, drag: true, resize: false}
        );
    }

    /**
     * jqGrid function to format file extension
     * @param status
     */
    function imageFormatter(cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var src = '';
            if (rowObject.Preview == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
            } else if (rowObject.Preview == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
            } else if (rowObject.Preview == 'docx') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
            }else if (rowObject.Preview == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
            }
            return '<img class="img-upload-tab" width=30 height=30 src="' + src + '">';
        }
    }

    /**
     * jqGrid function to format action column of file library of unit
     * @param cellvalue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function actionLibraryFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            select = ['<span><i class="fa fa-trash delete_file_library pointer" data_id="' + rowObject.id + '" style="font-size:24px"></i></span>'];
            var data = '';
            if(select != '') {
                $.each(select, function (key, val) {
                    data += val
                });
            }
            return data;
        }
    }

    $(document).on("click",".delete_file_library",function(){
        var data_id = $(this).attr("data_id");
        bootbox.confirm({
            message: "Are you sure you want to delete this file ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/propertyUnitAjax',
                        data: {class: 'PropertyUnitAjax', action: 'deleteFile', id: data_id},
                        success : function(response){
                            var response =  JSON.parse(response);
                            if(response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                triggerFileReload();
                            }else if(response.status == 'error' && response.code == 503) {
                                toastr.error(response.message);
                            }else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                }
            }
        });
    });

    function triggerFileReload(){
        var grid = $("#file-library");
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    /**
     * jqGrid Building Keys function
     * @param status
     */
    unitFlags();
    function unitFlags(status) {
        var id = getParameterByName('id');
        var table = 'flags';
        var columns = ['Date', 'Flag Name', 'Phone Number','Flag Reason','Completed','Note'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [{column: 'object_id', value: id, condition: '='},{column: 'object_type', value: "unit", condition: '='}];
        var columns_options = [
            {name:'Date',index:'date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Flag Name',index:'flag_name',width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Phone',index:'flag_phone_number', width:100,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,table:table,formatter:phoneNumberFormat},
            {name:'Flag Reason',index:'flag_reason',width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Completed',index:'completed', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,table:table,formatter:isCompletedFormatter},
            {name:'Note',index:'flag_note',width:100,align:"center",searchoptions: {sopt: conditions},table:table},
        ];
        var ignore_array = [];
        jQuery("#unitFlags-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Flags",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 200, drag: true, resize: false}
        );
    }
    /**
     * jqGrid function to format completed column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function phoneNumberFormat(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            if(cellValue !== undefined || cellValue != '' ){
                return cellValue.replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
            }
            else {
                return '';
            }
        }
    }

    /**
     * jqGrid function to format completed column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isCompletedFormatter(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "True";
        else if (cellValue == '0')
            return "False";
        else
            return '';
    }

    /**
     * jqGrid Building Keys function
     * @param status
     */
    buildingComplaints();

    function buildingComplaints(status) {
        var id = getParameterByName('id');
        var table = 'complaints';
        var columns = ['Complaint Id', 'Complaint', 'Complaint Type','Notes','Date','Other'];
        var joins = [{table: 'complaints', column: 'complaint_type_id', primary: 'id', on_table: 'complaint_types'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [{column: 'object_id', value: id, condition: '='},{column: 'module_type', value: "unit", condition: '='}];
        var columns_options = [
            {name:'Complaint Id',index:'complaint_id', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Complaint',index:'complaint_name', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Complaint Type',index:'complaint_type', width:100,align:"center",searchoptions: {sopt: conditions},table:"complaint_types"},
            {name:'Notes',index:'complaint_note', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Date',index:'complaint_date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Other',index:'other_notes',width:100,align:"center",searchoptions: {sopt: conditions},search:false,table:table},
        ];
        var ignore_array = [];
        jQuery("#unitComplaints-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'complaints.updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Complaints",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 200, drag: true, resize: false}
        );
    }

   // viewData();
    function viewData() {

        $.ajax({
            type: 'post',
            url: '/OwnerAjax',
            data: {
                class: "OwnerAjax",
                action: "getviewData"
            },
            success: function (response) {

                var res = $.parseJSON(response);
                if (res.status == "success") {

                    //if (data.data.state.state != ""){
                    //     $("#driverProvince").val(data.data.state.state);

                    // $('.owner_address').html(countryOption);
                    $('.owner_phone').text(res.data.user_detail.phone_number);
                    $('.owner_note_for_Phone').text(res.data.user_detail.phone_number_note);
                    $('.owner_Email').text(res.data.user_detail.email);
                    $('.owner_SSN').text(res.data.user_detail.ssn_sin_id);
                    $('.owner_ethnicity').text(res.data.user_detail.ethnicity);
                    $('.owner_hobbies').text(res.data.user_detail.hobbies);
                    $('.owner_marital_status').text(res.data.user_detail.maritial_status);
                    $('.owner_vetran_status').text(res.data.user_detail.veteran_status);
                    $('.owner_dob').text(res.data.user_detail.dob);

                    // $('.owner_note_for_Phone').val(res.data.phone_note);
                    // $('.owner_Email').val(res.data.phone_note);
                    // $('.owner_SSN').val(res.data.ssn);
                    // $('.owner_ethnicity').val(res.res.ethnicity);
                    // $('.owner_hobbies').val(res.data.hobbies);
                    // $('.owner_marital_status').val(res.data.marital);
                    // $('.owner_vetran_status').val(res.data.veteran);

                    //}

                   // if (data.data.country.length > 0){
                        //var countryOption = "<option value='0'>Select</option>";
                        //$.each(data.data.country, function (key, value) {
                         //   countryOption += "<option value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")"+"</option>";
                       // });
                        //$('.countycodediv select').html(countryOption);
                        //('.emergencycountry').html(countryOption);
                   // }

                    // if (data.data.property_list.length > 0){
                    //     var propertyOption = "<option value=''>Select</option>";
                    //     $.each(data.data.property_list, function (key, value) {
                    //         propertyOption += "<option value='"+value.id+"' data-id='"+value.property_id+"'>"+value.property_name+"</option>";
                    //     });
                    //     $('#property_owned_id').html(propertyOption);
                    // }

                    // if (data.data.phone_type.length > 0){
                    //     var phoneOption = "";
                    //     $.each(data.data.phone_type, function (key, value) {
                    //         phoneOption += "<option value='"+value.id+"'>"+value.type+"</option>";
                    //     });
                    //     $('.primary-owner-phone-row select[name="phoneType[]"]').html(phoneOption);
                    //     $('.addition_owner_block select[name="additional_phoneType"]').html(phoneOption);
                    // }

                    // if (data.data.carrier.length > 0){
                    //     var carrierOption = "<option value=''>Select</option>";
                    //     $.each(data.data.carrier, function (key, value) {
                    //         carrierOption += "<option value='"+value.id+"'>"+value.carrier+"</option>";
                    //     });
                    //     $('.primary-owner-phone-row select[name="carrier[]"]').html(carrierOption);
                    //     $('.addition_owner_block select[name="additional_carrier"]').html(carrierOption);
                    //     $('.property_guarantor_form1 select[name="guarantor_carrier_1[]"]').html(carrierOption);
                    //     $('.additional_carrier').html(carrierOption);
                    // }

                    // if (data.data.referral.length > 0){
                    //     var referralOption = "";
                    //     $.each(data.data.referral, function (key, value) {
                    //         referralOption += "<option value='"+value.id+"'>"+value.referral+"</option>";
                    //     });
                    //
                    //     $('select[name="referral_Source"]').html(referralOption);
                    //     $('select[name="additional_referralSource"]').html(referralOption);
                    // }

                    // if (data.data.ethnicity.length > 0){
                    //     var ethnicityOption = "";
                    //     $.each(data.data.ethnicity, function (key, value) {
                    //         ethnicityOption += "<option value='"+value.id+"'>"+value.title+"</option>";
                    //     });
                    //     $('select[name="ethncity"]').html(ethnicityOption);
                    //     $('.addition_owner_block select[name="additional_ethncity"]').html(ethnicityOption);
                    // }

                    // if (data.data.marital.length > 0){
                    //     var maritalOption = "";
                    //     $.each(data.data.marital, function (key, value) {
                    //         maritalOption += "<option value='"+value.id+"'>"+value.marital+"</option>";
                    //     });
                    //
                    //     $('select[name="maritalStatus"]').html(maritalOption);
                    //     $('.marital_status').html(maritalOption);
                    // }

                    // if (data.data.hobbies.length > 0){
                    //     var hobbyOption = "";
                    //     $.each(data.data.hobbies, function (key, value) {
                    //         hobbyOption += "<option value='"+value.id+"'>"+value.hobby+"</option>";
                    //     });
                    //     $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').html(hobbyOption);
                    //     $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                    // }

                    // if (data.data.veteran.length > 0){
                    //     var veteranOption = "";
                    //     $.each(data.data.veteran, function (key, value) {
                    //         veteranOption += "<option value='"+value.id+"'>"+value.veteran+"</option>";
                    //     });
                    //     $('select[name="veteran_status"]').html(veteranOption);
                    //     $('.veteran_status').html(veteranOption);
                    // }

                    // if (data.data.collection_reason.length > 0){
                    //     var reasonOption = "";
                    //     $.each(data.data.collection_reason, function (key, value) {
                    //         reasonOption += "<option value='"+value.id+"'>"+value.reason+"</option>";
                    //     });
                    //     $('.property_collection select[name="collection_reason"]').html(reasonOption);
                    // }

                    // if (data.data.credential_type.length > 0){
                    //     var typeOption = "";
                    //     $.each(data.data.credential_type, function (key, value) {
                    //         typeOption += "<option value='"+value.id+"'>"+value.credential_type+"</option>";
                    //     });
                    //     $('.owner-credentials-control select[name="credentialType[]"]').html(typeOption);
                    // }
                    // if (data.data.account_name.length > 0){
                    //     var typeOption = "<option value=''>Select</option>";
                    //     $.each(data.data.account_name, function (key, value) {
                    //         typeOption += "<option value='"+value.id+"'>"+value.account_name+"</option>";
                    //     });
                    //     $('select[name="account_name[]"]').html(typeOption);
                    // }
                    // if (data.data.chart_accounts.length > 0){
                    //     var typeOption = "<option value=''>Select</option>";
                    //     $.each(data.data.chart_accounts, function (key, value) {
                    //         typeOption += "<option value='"+value.id+"'>"+value.account_name+'-'+value.account_code+"</option>";
                    //     });
                    //     $('select[name="chart_of_account[]"]').html(typeOption);
                    // }
                    // if (data.data.account_type.length > 0){
                    //     var typeOption = "<option value=''>Select</option>";
                    //     $.each(data.data.account_type, function (key, value) {
                    //         typeOption += "<option data-rangefrom="+value.range_from+" data-rangeto="+value.range_to+" value="+value.id+">"+value.account_type_name+"</option>";
                    //     });
                    //     $('select[name="account_type_id"]').html(typeOption);
                    // }

                } else if (res.status == "error") {
                    toastr.error(res.message);
                } else {
                    toastr.error(res.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }






});