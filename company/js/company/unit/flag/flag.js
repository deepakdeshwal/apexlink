// $("#portfolio_id").keypress(function(e){
//     var keyCode = e.which;
//     /*
//       8 - (backspace)
//       32 - (space)
//       48-57 - (0-9)Numbers
//     */
//     // Not allow special
//     if ( !( (keyCode >= 48 && keyCode <= 57)
//         ||(keyCode >= 65 && keyCode <= 90)
//         || (keyCode >= 97 && keyCode <= 122) )
//         && keyCode != 8 && keyCode != 32) {
//         e.preventDefault();
//     }
// });

//display add portfolio div

function getParameterByName( name ){
    var regexS = "[\\?&]"+name+"=([^&#]*)",
        regex = new RegExp( regexS ),
        results = regex.exec( window.location.search );
    if( results == null ){
        return "";
    } else{
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}
var idd =  getParameterByName('id');
$(document).ready(function () {
    if(localStorage.getItem("rowcolorTenant")){
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(9)').addClass("green_row_right");
            localStorage.removeItem('rowcolorTenant');
        }, 2000);
    }
});
$(document).on('click','#new_flag',function () {
    $('#flagFormDiv').show(500);
    $('#flagFormDiv .time_span').text('');
    var unit_prefix = $('#addUnitForm input[name="unit_prefix[]"]').val();
    var unit_no     = $('#addUnitForm input[name="unit_no[]"]').val();
    if (unit_prefix != ''){
        var unit_name = unit_prefix +'-'+unit_no;
    } else {
        var unit_name = unit_no;
    }

    $('#flagged_for').val(unit_name);
    $('#flag_id').val('');
    $('#flagSaveBtnId').text('Save');
    $('#clearAddFlagForm').text('Clear');
    /*date picker for complaint date */
    $("#flag_flag_date").datepicker({
        dateFormat: date_format,
        autoclose: true,
        changeMonth: true,
        changeYear: true
    }).datepicker("setDate", new Date());
    $("#flag_flag_name,#flag_flag_reason,#flag_note").val('');
    $('#flag_country_code').val(220);
    $('#completed').prop('selectedIndex',0);
    $("#flag_flag_by").val(default_login_user_name);
    $("#flag_phone_number").val(default_login_user_phone_number);
});

$(document).on("click","#clearAddFlagForm",function(){
    bootbox.confirm("Do you want to Clear this form?", function (result) {
        if (result == true) {
            var flag_id = $('#flag_id').val();
            if (flag_id != '') {
                getDataById(flag_id);
            } else {
                $('#flagFormDiv .time_span').text('');
                $('#flagSaveBtnId').text('Save');
                $('#clearAddFlagForm').text('Clear');
                /*date picker for complaint date */
                $("#flag_flag_date").datepicker({
                    dateFormat: date_format,
                    autoclose: true,
                    changeMonth: true,
                    changeYear: true
                }).datepicker("setDate", new Date());
                $("#flag_flag_name,#flag_flag_reason,#flag_note").val('');
                $('#flag_country_code').val(220);
                $('#completed').prop('selectedIndex', 0);
                $("#flag_flag_by").val(default_login_user_name);
                $("#flag_phone_number").val(default_login_user_phone_number);
            }
        }
    });
});
// $("#flag_flag_date").datepicker({dateFormat: datepicker});

//on flag cancel
$(document).on('click','#flagCancel',function(){
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $('#flag_id').val('');
                $('#flagFormDiv').hide(500);
            }
        }
    });
});

//adding custom fields
$(document).on('click','#flagSaveBtnId', function (e) {
    e.preventDefault();
    //checking custom field validation
    var formData = $('#flagFormDiv :input').serializeArray();
    var unit_id =  $("#edit_unit_id").val();
        $.ajax({
            type: 'post',
            url: '/flag-ajax',
            data: {form: formData,class:'Flag',action:'create_flag',object_id:unit_id,object_type:'unit'},
            success: function (response) {
                var response = JSON.parse(response);
                if(response.code == 200){
                    getFlagCount();
                    $('#flagFormDiv').hide(500);
                    $('#propertyFlag-table').trigger('reloadGrid');
                    jQuery('#propertyFlag-table').find('tr:eq(0)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('#propertyFlag-table').find('tr:eq(0)').find('td').last().addClass("green_row_right");
                    $('#flagFormDiv :input').val('');
                    toastr.success(response.message);
                    var returnRes = update_users_flag('unit_details',idd);
                    if (returnRes.status == "success" && returnRes.code == 200){
                        localStorage.setItem("Message", response.message);
                        localStorage.setItem('rowcolorTenant', 'rowColor');
                        var prop_id= localStorage.getItem('prop_id');
                        setTimeout(function () {
                            window.location.href = window.location.origin + '/Unit/UnitModule?id='+prop_id;
                            localStorage.removeItem('prop_id');
                        },1000);
                    }
                } else if(response.code == 200) {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
});

jqGridFlag('All');
/**
 * jqGridFlag Initialization function
 * @param status
 */
function jqGridFlag(status) {
    // alert($("#form_building_edit_id").val());
    var unit_id =  $("#edit_unit_id").val();
    var table = 'flags';
    var columns = ['Date','Flag Name', 'Phone Number', 'Flag Reason', 'Completed', 'Note', 'Action'];
    var select_column = ['Edit','Delete','Completed'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'object_id',value:unit_id,condition:'='},{column:'object_type',value:'unit',condition:'='}];
    var extra_columns = ['flags.deleted_at'];
    var columns_options = [
        {name:'Date',index:'date',align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date',editable:true,},
        {name:'Flag Name',index:'flag_name',searchoptions: {sopt: conditions},table:table},
        {name:'Phone',index:'flag_phone_number', width:200,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,table:table},
        {name:'Flag Reason',index:'flag_reason', width:180,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Completed',index:'completed',width:180, align:"center",searchoptions: {sopt: conditions},table:table,formatter: completedFormatter},
        {name:'Note',index:'flag_note',width:200, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'select',width:200, title: false,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter: actionFmatter}
    ];
    var ignore_array = [];
    jQuery("#propertyFlag-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'true',
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Flags",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}

/**
 * function to change completed format
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function completedFormatter (cellValue, options, rowObject){
    if (cellValue == 1)
        return "True";
    else if(cellValue == 0)
        return "False";
    else
        return '';
}

/**
 * function to change action column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function actionFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';
        if(rowObject.Completed == 1) select = ['Edit','Delete'];
        if(rowObject.Completed == 0) select = ['Edit','Delete','Completed'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }

}
function getDataById(id){
    $.ajax({
        type: 'post',
        url: '/flag-ajax',
        data: {id: id,class:'Flag',action:'get_flags'},
        success: function (response) {
            $("#flagFormDiv").show(500);
            $('#flagSaveBtnId').text('Update');
            var data = $.parseJSON(response);
            edit_date_time(data.data.updated_at);
            if (data.code == 200) {
                var unit_prefix = $('#addUnitForm input[name="unit_prefix[]"]').val();
                var unit_no     = $('#addUnitForm input[name="unit_no[]"]').val();
                if (unit_prefix != ''){
                    var unit_name = unit_prefix +'-'+unit_no;
                } else {
                    var unit_name = unit_no;
                }

                $('#flagged_for').val(unit_name);
                $('#clearAddFlagForm').text('Reset');
                $("#flag_id").val(data.data.id);
                $.each(data.data, function (key, value) {
                    if(key=='date')
                    {
                        $('#flag_flag_date').val(value);
                    } else if(key=='flag_phone_number'){
                        $('#flag_phone_number').val(value);
                    } else if(key=='flag_phone_number'){
                        $('#flag_phone_number').val(value);
                    }else if(key=='completed'){
                        if(value=='1')
                        {
                            $('#completed').prop('selectedIndex',1);
                        }else{
                            $('#completed').prop('selectedIndex',0);
                        }

                    } else if(key=='flag_note'){
                        $('#flag_note').val(value);
                    }
                    else {
                        $('#flag_' + key ).val(value);
                    }

                });

            } else if (data.code == 500){
                toastr.error(data.message);
            } else{
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

/**  List Action Functions  */
$(document).on('change', '#propertyFlag-table .select_options', function() {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    var row_num = $(this).parent().parent().index() ;
    if (opt == 'Edit' || opt == 'EDIT') {
        var validator = $( "#flagForm" ).validate();
        //validator.resetForm();
        $('#propertyFlag-table').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        jQuery('#propertyFlag-table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('#propertyFlag-table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
        getDataById(id);
        //$("#portfolio-table").trigger('reloadGrid');
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/flag-ajax',
                        data: {id: id,class:'Flag',action:'deleteFlag'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                getFlagCount();
                                toastr.success(response.message);
                                var returnRes = update_users_flag('unit_details',idd);
                                if (returnRes.status == "success" && returnRes.code == 200){
                                    localStorage.setItem("Message", response.message);
                                    localStorage.setItem('rowcolorTenant', 'rowColor');
                                    var prop_id= localStorage.getItem('prop_id');
                                    setTimeout(function () {
                                        window.location.href = window.location.origin + '/Unit/UnitModule?id='+prop_id;
                                        localStorage.removeItem('prop_id');
                                    },1000);
                                }
                                $('#propertyFlag-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }

            }
        });
    } else if (opt == 'Completed' || opt == 'COMPLETED') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to complete this flag ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/flag-ajax',
                        data: {id: id,class:'Flag',action:'flagCompleted'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                getFlagCount();
                                toastr.success(response.message);
                                var returnRes = update_users_flag('unit_details',idd);
                                if (returnRes.status == "success" && returnRes.code == 200){
                                    localStorage.setItem("Message", response.message);
                                    localStorage.setItem('rowcolorTenant', 'rowColor');
                                    var prop_id= localStorage.getItem('prop_id');
                                    setTimeout(function () {
                                        window.location.href = window.location.origin + '/Unit/UnitModule?id='+prop_id;
                                        localStorage.removeItem('prop_id');
                                    },1000);
                                }
                                $('#propertyFlag-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }

            }
        });
    }
    $('.select_options').prop('selectedIndex',0);
});
