/*Get Building details*/
var id =  getParameterByName('id');
fetchBuildingDetails();
unitComplaints();

function fetchBuildingDetails() {

    var editUnitId =  $('#editUnitId').val();
    $.ajax({
        type: 'post',
        url: '/propertyUnitAjax',
        data: {
            class: 'unitDetail',
            action: 'fetchUnitDetail',
            id:editUnitId
        },
        success: function (response) {
            var res = JSON.parse(response);
            if (res.code == '200') {
                var data = res.data;
                var updated_at = data.unitDetails.updated_at;
                edit_date_time(updated_at);
                if (data.building_disabled <= 0) {
                    $('.building_popup .pop-add-icon').addClass('disabled_building');
                    $('.building_popup .add-more-div').removeAttr('data-target');
                } else {
                    $('.building_popup .pop-add-icon').removeClass('disabled_building');
                    $('.building_popup .add-more-div').attr('data-target', '#AddNewBuildingModal');

                    getCustomfield(data.unitDetails.custom_field);
                    setTimeout(function () {
                        $(data.propertyAll).each(function (key, value) {
                            $('#property_id')
                                .append($("<option></option>")
                                    .attr("value", value.id)
                                    .text(value.property_name));
                        });
                    }, 1000);

                    setTimeout(function () {
                        $('#property_id').val(data.property_detail.id);
                    }, 2500);
                }


                //   $('#Pmarket_rent').val(market_rent);

                $('.propertyName').text(data.property_detail.property_name);
                $('.propertyID').text(data.property_detail.property_id);
                $('#property_unit_id').val(data.property_detail.id);

                var unitData = data.unitDetails;
                $('#editPropID').val(unitData.property_id);

                $(".PropertyUnitUrl").prop("href", "/Property/PropertyModules");
                $(".NewUnitUrl").prop("href", "/Unit/AddUnit?id=" + unitData.property_id);
                $(".PropertyInspectionUrl").prop("href", "/Property/PropertyInspection?id=" + unitData.property_id);

                if (unitData.key_unit == '1') {
                    $('.radiobuttonCls').prop("checked", true);
                    $('.keyFrstdiv').show();
                    $('.trackFrstdiv').hide();
                    $("#add_key_grid").show();
                    $("#track_key_grid").hide()
                    $('#edit_keyid_hidden').prop('disabled', 'disabled');
                    //Add Key Values
                    unitKeys();
                }

                if (unitData.key_unit == '2') {
                    $('.radiobuttonCls2').prop("checked", true);
                    $('.keyFrstdiv').hide();
                    $('.trackFrstdiv').show();
                    $("#add_key_grid").hide();
                    $("#track_key_grid").show();
                    unitTagKeys();
                }


                setTimeout(function () {

                    $.each(data.buildingAll, function (key, value) {
                        $('.unit_building')
                            .append($("<option></option>")
                                .attr("value", value.id)
                                .text(value.building_name));
                    });
                    $('select#unit_building').val(unitData.building_id);
                    $('select#unit_type').val(unitData.unit_type_id);
                    $('select#bedrooms_no').val(unitData.bedrooms_no);
                    $('select#bathrooms_no').val(unitData.bathrooms_no);
                    $('select#NonSmokingUnit').val(unitData.smoking_allowed);
                    if (unitData.smoking_allowed == '0' || unitData.smoking_allowed == 0 || unitData.smoking_allowed == 'No') {
                        $('select#NonSmokingUnit').attr('readonly', true);
                        $('select#NonSmokingUnit').addClass('disabled_field');
                    }

                    if (unitData.pet_friendly_id == '0' || unitData.pet_friendly_id == 0 || unitData.pet_friendly_id == 'No') {
                        $('select#pet_options').attr('readonly', true);
                        $('select#pet_options').addClass('disabled_field');
                    }
                    $('select#pet_options').val(unitData.pet_friendly_id);
                    $('select#ddlStatus').val(unitData.building_unit_status);

                    if (unitData.key_access_codes) {
                        var key_access_code = unitData.key_access_codes;
                        var key_access_codes_desc = unitData.key_access_codes_desc;
                        var key_access_code_length = key_access_code.length;
                        console.log(key_access_code_length);
                        if (key_access_code_length > 0) {
                            $('.key_access_codeclass').show();
                            var nth_key = parseInt(key_access_code_length) - parseInt(1);
                            for (var i = 0; i < nth_key; i++) {
                                var length = $('.key_optionsDivClass').length;
                                var nodeOne = $("#key_optionsDiv").clone();
                                nodeOne.find('.customValidateKeyAccessCode').attr('class', 'form-control keyAccessCode keyAccessCode' + i + ' key-access-input key_access_codes_list customValidateKeyAccessCode' + length);
                                nodeOne.find('.NewkeyPopupSave').attr('data_id', length);
                                nodeOne.find('#key_options').attr('id', 'key_options' + length);
                                nodeOne.insertAfter(".key_optionsDivClass:last");
                                nodeOne.find("#NewkeyPlus").remove();
                                nodeOne.find("#NewkeyMinus").show();
                                nodeOne.find("#key_options").val(' ');
                                nodeOne.find('.key_access_codeclass').hide();
                                $('.key_access_codeclass').show();
                            }


                            $('.key_optionsDivClass select').html(data.keyhtml);
                            for (var i = 0; i < key_access_code_length; i++) {
                                $('select#key_options' + i).val(key_access_code[i]);
                                $('#key_options' + i).next().find('textarea').val(key_access_codes_desc[i]);
                            }
                            $('#key_options').val(key_access_code[0]);
                            $('#key_access_code').val(key_access_codes_desc[0]);

                        }

                    }

                    if (unitData.amenities) {

                        var amenties = unitData.amenities;
                        $.each(amenties, function (i, val) {

                            $(".amenties_boxNew input[value='" + val + "']").prop('checked', true);
                        });
                    }


                }, 1000);


                $('input[name="floor_no[]"]').val(unitData.floor_no);
                $('input[name="unit_prefix[]"]').val(unitData.unit_prefix);
                $('input[name="unit_no[]"]').val(unitData.unit_no);
                $('input[name="square_ft"]').val(unitData.square_ft);
                //     $('input[name="baseRent"]').val(unitData.base_rent);
                //  $('input[name="market_rent"]').val(unitData.market_rent);
                //   $('input[name="securityDeposit"]').val(unitData.security_deposit);
                $('input[name="building_unit_note"]').val(unitData.building_unit_note);
                $('#txtNotes').text(unitData.building_unit_notes);
                $('#last_renovation_date').val(unitData.last_renovation);
                $('input[name="last_renovation_time"]').val(unitData.last_renovation_time);
                $('#last_renovation_description').val(unitData.last_renovation_description);
                $('input[name="building_description"]').val(unitData.building_description);
                $('input[name="school_district_code"]').val(unitData.school_district_code);
                $('input[name="school_district_municipality"]').val(unitData.school_district_municipality);
                $('input[name="school_district_notes"]').val(unitData.school_district_notes);
                $('input[name="phone_number[]"]').val(unitData.phone_number1);
                $('input[name="fax_number[]"]').val(unitData.fax_number1);
                $("#txtDescription").text(unitData.building_description);
                if (unitData.phone_number2) {
                    var clone = $(".primary-tenant-phone-row:first").clone();
                    $(".primary-tenant-phone-row").first().after(clone).next().find("input[type='text']").val(unitData.phone_number2);
                    $(".primary-tenant-phone-row:not(:eq(0)) .phone-add-remove-row .glyphicon-plus-sign").remove();
                    $(".primary-tenant-phone-row:not(:eq(0)) .phone-add-remove-row .glyphicon-remove-sign").show();
                }

                if (unitData.phone_number3) {
                    var clone = $(".primary-tenant-phone-row:last").clone();
                    $(".primary-tenant-phone-row").last().after(clone).next().find("input[type='text']").val(unitData.phone_number3);
                    $(".primary-tenant-phone-row:not(:eq(0)) .phone-add-remove-row .glyphicon-plus-sign").remove();
                    $(".primary-tenant-phone-row:not(:eq(0)) .phone-add-remove-row .glyphicon-remove-sign").show();

                }

                if (unitData.fax_number2) {
                    var clone = $(".primary-tenant-fax-row:first").clone();
                    $(".primary-tenant-fax-row").first().after(clone).next().find("input[type='text']").val(unitData.fax_number2);
                    $(".primary-tenant-fax-row:not(:eq(0)) .fax-add-remove-row .glyphicon-plus-sign").remove();
                    $(".primary-tenant-fax-row:not(:eq(0)) .fax-add-remove-row .glyphicon-remove-sign").show();
                }

                if (unitData.fax_number3) {
                    var clone = $(".primary-tenant-fax-row:last").clone();
                    $(".primary-tenant-fax-row").last().after(clone).next().find("input[type='text']").val(unitData.fax_number3);
                    $(".primary-tenant-fax-row:not(:eq(0)) .phone-add-remove-row .glyphicon-plus-sign").remove();
                    $(".primary-tenant-fax-row:not(:eq(0)) .phone-add-remove-row .glyphicon-remove-sign").show();

                }


                setTimeout(function () {
                    var market_rent = data.unitDetails.market_rent;
                    var base_rent = data.unitDetails.base_rent;
                    var security_rent = data.unitDetails.security_deposit;
                    $('#market_rent').val(market_rent);
                    $('#baseRent').val(base_rent);
                    $('#securityDeposit').val(security_rent);

                }, 1000);

                setTimeout(function () {
                    var market_rent = data.unitDetails.market_rent;
                    $('#market_rent').val(market_rent);
                }, 2000);
            } else {
                window.history.back();
            }
        },
    });

}

function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
}


function getParameterByName(name) {
    var regexS = "[\\?&]" + name + "=([^&#]*)",
        regex = new RegExp(regexS),
        results = regex.exec(window.location.search);
    if (results == null) {
        return "";
    } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}

$(document).on('click', '#clearRenovationField', function (e) {
    bootbox.confirm("Do you want to Clear this form?", function (result) {
        if (result == true) {
            $('#new_renovation_form #new_renovation_date').val('');
            $('#new_renovation_form #new_renovation_description').val('');
            $('#new_renovation_form label.error').text('');
            $("input[name='new_renovation_date']").datepicker({
                dateFormat: date_format,
                autoclose: true,
                changeMonth: true,
                changeYear: true
            });

            $("#new_renovation_timeModal").timepicker({timeFormat: 'h:mm p', defaultTime: new Date()});
            $('#new_renovation_timeModal').timepicker({zindex: 9999999});
        }
    });
});
$(document).on('click', '#newRenovation', function (e) {
    // $('#new_renovation_form')[0].reset();
    $("#renovationModal").modal('show');
    $('#new_renovation_form #new_renovation_date').val('');
    $('#new_renovation_form #new_renovation_description').val('');
    // $('#new_renovation_form #new_renovation_timeModal').val('');
    $('#new_renovation_form label.error').text('');
    $("input[name='new_renovation_date']").datepicker({
        dateFormat: date_format,
        autoclose: true,
        changeMonth: true,
        changeYear: true
    });

    $("#new_renovation_timeModal").timepicker({ timeFormat: 'h:mm p',defaultTime: new Date()});
    $('#new_renovation_timeModal').timepicker({ zindex: 9999999});
});
$("#new_renovation_timeModal").timepicker({ timeFormat: 'h:mm p',defaultTime: new Date(),zindex: 9999999});


//add announcement
// $(document).on('submit','#new_renovation_form',function (e) {
//     $(this).valid();
//     e.preventDefault();
// });

var req = $;
$("#new_renovation_form").validate({
    rules: {
        new_renovation_description  : 'required',
        new_renovation_date         : 'required',
        new_renovation_time         : 'required',
    },
    submitHandler: function () {
        var formData = $('#new_renovation_form').serializeArray();

        $.ajax({
            type: 'post',
            url: '/propertyUnitAjax',
            data: {form: formData,
                class: 'unitDetail',
                action: 'addUnitRenovationDetail',
                editRenovation:true
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $("#renovationModal").modal('hide');
                    $('#last_renovation_date').val(response.data.last_renovation_date);
                    $('#last_renovation_time').val(response.data.last_renovation_time);
                    $('.last_renovation_de').text(response.data.last_renovation_description);
                    $('.last_renovation_de').val(response.data.last_renovation_description);

                 //   window.location.reload();
                }   else if (response.status == 'error' && response.code == 500) {
                    toastr.error(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text(value);
                    });
                }else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});



/*ajax to delete file libraries*/
$(document).on("click",".delete_file_library",function(){
    var data_id = $(this).attr("data_id");

    bootbox.confirm({
        message: "Are you sure you want to delete this file ?",
        buttons: {confirm: {label: 'Ok'}, cancel: {label: 'Cancel'}},
        callback: function (result) {
            if (result == true) {
                $.ajax({
                    type: 'post',
                    url: '/propertyUnitAjax',
                    data: {class: 'unitDetail', action: 'deleteFile', id: data_id},
                    success : function(response){
                        var response =  JSON.parse(response);
                        if(response.status == 'success' && response.code == 200) {
                            toastr.success('The record deleted successfully');
                            triggerFileReload();
                            // window.location.href = '/MasterData/AddPropertyType';
                        }else if(response.status == 'error' && response.code == 503) {
                            toastr.error(response.message);
                            // window.location.href = '/MasterData/AddPropertyType';
                        }else {
                            toastr.warning('Record not updated due to technical issue.');
                        }
                    }
                });
            }
            triggerFileReload();
        }
    });
});

$(document).on("click",".deactivateUnit",function(){
    var id = $(this).attr('rowid');
    bootbox.confirm("Do you want to Delete this record?", function (result) {
        if (result == true) {
            $.ajax({
                type: 'post',
                url: '/propertyUnitAjax',
                data: {class: 'unitDetail', action: 'deleteTrackKey', id: id},
                success : function(response){
                    var response =  JSON.parse(response);
                    var tagKeyData = response.data;
                    console.log(response);
                    if(response.status == 'success' && response.code == 200) {
                        toastr.success('Record Deleted Successfully');
                        unitTagKeys();
                        triggerUnitTagKeys();
                    }else if(response.status == 'error' && response.code == 503) {
                        toastr.error(response.message);
                    }else {

                    }
                }
            });
        }
    });
});

$(document).on("focusout",".numberFormatAjax",function(){
    var elem = $(this);
    var elemId = $(this).attr('id');
    var number_data = $(this).val();
    number_data = Number(number_data.replace(/[^0-9.-]+/g,""))
     $.ajax({
        type: 'post',
        url: '/propertyUnitAjax',
        data: {class: 'unitDetail', action: 'numberFormatAjax', data: number_data},
        success : function(response){
          var response =  JSON.parse(response);
          elem.val(response.data);
          if(elemId == 'baseRent')
          {
             $('#securityDeposit').val(response.data);
          }
        }
    });
});


// $(document).on("focusout","#baseRent",function(){
//     var baseRent = $(this).val();

//     setTimeout(function(baseRent){ $('#securityDeposit').val(baseRent); }, 1000);

// });
/*ajax to delete images*/
$(document).on("click",".delete_image",function(){
    var data_id = $(this).attr("data_id");

    bootbox.confirm({
        message: "Are you sure you want to delete this file ?",
        buttons: {confirm: {label: 'Ok'}, cancel: {label: 'Cancel'}},
        callback: function (result) {
            if (result == true) {
                $.ajax({
                    type: 'post',
                    url: '/propertyUnitAjax',
                    data: {class: 'unitDetail', action: 'deleteFile', id: data_id},
                    success : function(response){
                        var response =  JSON.parse(response);
                        if(response.status == 'success' && response.code == 200) {
                            toastr.success(response.message);

                            triggerPhotoReload();
                            // window.location.href = '/MasterData/AddPropertyType';
                        }else if(response.status == 'error' && response.code == 503) {
                            toastr.error(response.message);
                            // window.location.href = '/MasterData/AddPropertyType';
                        }else {
                            toastr.warning('Record not updated due to technical issue.');
                        }
                    }
                });
            }
            triggerPhotoReload();
        }
    });
});


/*trigger to reload files jqgrid*/
function triggerFileReload(){
    var grid = $("#unitFileLibrary-table");
    grid[0].p.search = false;
    $.extend(grid[0].p.postData,{filters:""});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}

/*trigger to reload images jqgrid*/
function triggerPhotoReload(){
    var grid = $("#unitPhotovideos-table");
    grid[0].p.search = false;
    $.extend(grid[0].p.postData,{filters:""});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}

function triggerUnitTagKeys(){
    var grid = $("#unit-track-keys");
    grid[0].p.search = false;
    $.extend(grid[0].p.postData,{filters:""});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}

function triggerUnitKeys(){
    var grid = $("#unit-keys");
    grid[0].p.search = false;
    $.extend(grid[0].p.postData,{filters:""});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}

function unitKeys(status) {
    var table = 'unit_keys';
    var columns = ['Key Tag', 'Description', 'Total Keys','Available Keys','Action'];
    //var select_column = ['Edit', 'Flag Bank', 'Inspection', 'Add In-Touch', 'In-Touch History', 'Deactivate', 'Activate', 'Print Envelope', 'Delete'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = ['unit_keys.total_keys','unit_keys.available_keys','unit_keys.deleted_at', 'unit_keys.updated_at'];
    var extra_where = [{column: 'unit_id', value: id, condition: '='}];
    var columns_options = [
        {name: 'Key Tag',index: 'key_id',width: 200,align: "center",searchoptions: {sopt: conditions},table: table},
        {name: 'Description', index: 'key_description', width: 200,align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Total Keys', index: 'total_keys', width: 100, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:totalkeyFormatter},
        {name: 'AvailableKeys', index: 'available_keys', width: 100, align: "center", hidden:true,searchoptions: {sopt: conditions}, table: table},
        // {name: 'Status', index: 'status', width: 300, align: "center", hidden:true,searchoptions: {sopt: conditions}, table: table},
        {name: 'Action', index: 'select', title: false, width: 50, align: "center", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table, formatter:actionKeysFormatter2},
    ];
    var ignore_array = [];
    jQuery("#unit-keys").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Keys",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 200, drag: true, resize: false} // search options
    );
}


function unitTagKeys(status) {

    var table = 'unit_track_key';
    var columns = ['Name', 'Email', 'CompanyName','Phone#','PickUp Date','PickUp Time','Key#','Key Quantity','PickUpReturn Date','Return Time','ApplicantName','Action'];
    //var select_column = ['Edit', 'Flag Bank', 'Inspection', 'Add In-Touch', 'In-Touch History', 'Deactivate', 'Activate', 'Print Envelope', 'Delete'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = ['unit_track_key.deleted_at', 'unit_track_key.updated_at'];
    var extra_where = [{column: 'unit_id', value: id, condition: '='}];
    var columns_options = [
        {name: 'Name',index: 'key_name',width: 200,align: "center",searchoptions: {sopt: conditions},table: table},
        {name: 'Email', index: 'email', width: 350, searchoptions: {sopt: conditions}, table: table},
        // {name: 'Total Keys', index: 'total_keys', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:totalkeyFormatter},
        {name: 'CompanyName', index: 'company_name', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Phone#', index: 'phone', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'PickUp Date', index: 'pick_up_date', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'PickUp Time', index: 'pick_up_time', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Key#', index: 'key_number', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Key Quantity', index: 'key_quality', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'PickUpReturn Date', index: 'pick_up_date', width: 300, align: "center",searchoptions: {sopt: conditions}, table: table},
        {name: 'Return Time', index: 'pick_up_time', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'ApplicantName', index: 'key_designator', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
        // {name: 'Status', index: 'status', width: 300, align: "center", hidden:true,searchoptions: {sopt: conditions}, table: table},
        {name: 'Action', index: 'select', title: false, width: 200, align: "center", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table, formatter:actionKeysFormatter},
    ];
    var ignore_array = [];
    jQuery("#unit-track-keys").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Keys",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 200, drag: true, resize: false} // search options
    );
}


function totalkeyFormatter(cellValue, options, rowObject){
    var totalkeys =''
    if(rowObject !== undefined){
        if(rowObject.AvailableKeys == ''){ var avail=0 }else{ var avail= rowObject.AvailableKeys}
        var totalkeys=avail +'/'+cellValue;
        return totalkeys;
    }
    return totalkeys;
}

function actionKeysFormatter2(cellValue, options, rowObject) {
    if(rowObject !== undefined){
        console.log(rowObject);
        if (rowObject.Status == 1) {
            var select = ['Edit', 'Delete', 'checkout','return'];
            var data = '';
            if (select != '') {
                var data = '<select ' +
                    ' class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
        else {
            var select = ['Edit', 'Delete', 'checkout'];
            var data = '';
            if (select != '') {
                var data = '<select ' +
                    ' class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }
}
function actionKeysFormatter(cellValue, options, rowObject) {
    if(rowObject !== undefined){
        console.log(rowObject);
        var protocol = location.port;
        if (protocol != "") {
            var webpath = location.protocol+"//"+location.hostname+":"+protocol;
        }else{
            var webpath = location.protocol+"//"+location.hostname;
        }
        if(rowObject !== undefined) {
            var status = rowObject['Unit Status'];
            var select = '';
            select = "<img src='"+webpath+"/company/images/edit-icon.png' class='editUnit' rowid='"+rowObject.id+"' title='Edit' alt='my image'/>  <img src='"+webpath+"/company/images/cross-icon.png' rowid='"+rowObject.id+"'  class='deactivateUnit'  title='Deactivate' alt='my image'/>";
            var data = '';
            if(select != '') {
                data = select;
            }
            return data;
        }
    }
}

$(document).on('click','.editUnit',function () {
    var id = $(this).attr('rowid');

    var key = $('.latefee-check-outer .latefee-check-hdr input[name="key"]:checked').val();

    if(key == 'keyUnit')
    {
        $('.divNewKey').show('300');
        $.ajax({
            type: 'post',
            url: '/propertyUnitAjax',
            data: {class: 'unitDetail', action: 'fetchKeyDetails', id: id},
            success : function(response){
                var response =  JSON.parse(response);
                var tagKeyData = response.data;
                console.log(response);
                if(response.status == 'success' && response.code == 200) {
                    $('input[name="key_tag"]').val(tagKeyData.key_id);
                    $('input[name="key_desc"]').val(tagKeyData.key_description);
                    $('input[name="total_keys"]').val(tagKeyData.total_keys);
                    $('input[name="key_id"]').val(tagKeyData.id);
                    $('#edit_id_hidden').prop('disabled',false);
                    $('.updateTagKey').val('Update');
                }else if(response.status == 'error' && response.code == 503) {
                    toastr.error(response.message);
                }else {

                }
            }
        });

    } else {
        $('.divNewtracker').show('300');
        $.ajax({
            type: 'post',
            url: '/propertyUnitAjax',
            data: {class: 'unitDetail', action: 'fetchTrackKeyDetails', id: id},
            success : function(response){
                var response =  JSON.parse(response);
                var tagKeyData = response.data;

                if(response.status == 'success' && response.code == 200) {
                    $('input[name="tag_key_name"]').val(tagKeyData.key_name);
                    $('input[name="tag_key_email"]').val(tagKeyData.email);
                    $('input[name="tag_key_company"]').val(tagKeyData.company_name);
                    $('input[name="tag_key_phone"]').val(tagKeyData.phone);
                    $('input[name="tag_key_address1"]').val(tagKeyData.Address1);
                    $('input[name="tag_key_address2"]').val(tagKeyData.Address2);
                    $('input[name="tag_key_address3"]').val(tagKeyData.Address3);
                    $('input[name="tag_key_address4"]').val(tagKeyData.Address4);
                    $('input[name="tag_key"]').val(tagKeyData.key_name);
                    $('input[name="tag_key_quantity"]').val(tagKeyData.key_quality);
                    $('input[name="tag_key_pick_date"]').val(tagKeyData.pick_up_date);
                    $('input[name="tag_key_pick_time"]').val(tagKeyData.pick_up_time);
                    $('input[name="tag_key_return_date"]').val(tagKeyData.return_date);
                    $('input[name="tag_key_return_time"]').val(tagKeyData.return_time);
                    $('input[name="tag_key_designator"]').val(tagKeyData.key_designator);
                    $('input[name="key_track_id"]').val(tagKeyData.id);
                    $('#edit_trackid_hidden').prop('disabled',false);
                    $('.updateTagKey').val('Update');
                }else if(response.status == 'error' && response.code == 503) {
                    toastr.error(response.message);
                }else {

                }
            }
        });
    }

});

$(document).on('click','#ancCreateNewtracker',function () {
    $('.divNewtracker').find('input').val('');
    $('#edit_trackid_hidden').prop('disabled','disabled');
    $('.updateTagKey').val('Save');

});

$(document).on('click','#ancCreateNewKey',function () {
    $('.divNewKey').find('input').val('');
    $('#edit_id_hidden').prop('disabled','disabled');
    $('.updateTagKey').val('Save');

});

$(document).on('click','.updateTagKey',function (e) {
    e.preventDefault();

    var unit_id =  $("#editUnitId").val();
    var key = $('.latefee-check-outer .latefee-check-hdr input[name="key"]:checked').val();

    if (key == 'keyUnit') {
        $action = 'editKeys';
        var formData = $('.divNewKey :input').serializeArray();
    } else{
        $action = 'editTrackKeys';
        var formData = $('.divNewtracker :input').serializeArray();
    }

    $.ajax({
        type: 'post',
        url: '/propertyUnitAjax',
        data: {form: formData,
            class: 'unitDetail',
            action: $action,
            edit_unit_id:unit_id
        },

        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#new_complaint').hide(500);
                $("#building-track-keys").trigger('reloadGrid');
                $("#track_key_tracker").hide();
                $(".divNewKey").hide(500);
                $('#track_key_tracker :input').val('');
                toastr.success(response.message);

                triggerUnitKeys();
                triggerUnitTagKeys();
                setTimeout(function () {
                    $('.divNewtracker').find('input').val('');
                    $('.divNewtracker').hide(300);
                },1000)

            }   else if (response.status == 'error' && response.code == 500) {
                toastr.error(response.message);
                return false;
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('#' + key).text(value);
                });
            }else if (response.status == 'error' && response.code == 400) {
                $.each(response.data, function (key, value) {
                    $('.' + key).text('*This field is required');
                });
            }

            onTop(true);

        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});

$(function() {
    $('.latefee-check-outer .latefee-check-hdr input[name="key"]').on('click', function() {
        if ($(this).val() == 'keyUnit') {
            $('#add_key_grid').show();
            $('#track_key_grid').hide();
            unitKeys();
        }
        if ($(this).val() == 'trackUnit') {
            $('#add_key_grid').hide();
            $('#track_key_grid').show();
            unitTagKeys();
        }
    });
});

jQuery(document).on('change','#unit-keys .select_options',function () {
    var select_options = $(this).val();
    var data_id = $(this).attr('data_id');
    if (select_options == 'checkout') {
        $('.checkout_div').show();
        $('#checkout_id_hidden').val(data_id);
        $('#checkout_id_hidden').attr('disabled',false);
        $('#chekoutSave').val('Save');
        $('.divNewKey').hide();

        $.ajax({
            type: 'post',
            url: '/propertyUnitAjax',
            data: {class: 'unitDetail', action: 'fetchCheckoutDetails', id: data_id},
            success : function(response){
                var response =  JSON.parse(response);
                var tagKeyData = response.data;
                console.log(response);
                if(response.status == 'success' && response.code == 200) {
                    $('input[name="key_tag"]').val(tagKeyData.key_id);
                    $('input[name="key_desc"]').val(tagKeyData.key_description);
                    $('input[name="total_keys"]').val(tagKeyData.total_keys);
                    $('input[name="key_id"]').val(tagKeyData.id);
                    $('#edit_id_hidden').prop('disabled',false);
                    $('.updateTagKey').val('Update');
                }else if(response.status == 'error' && response.code == 503) {
                    toastr.error(response.message);
                }else {

                }
            }
        });



    }
    else if(select_options == 'Edit'){
        $('.divNewKey').show();
        $('#track_key_grid').hide();
        $('.checkout_div').hide();

        $('.divNewKey').show('300');

        $.ajax({
            type: 'post',
            url: '/propertyUnitAjax',
            data: {class: 'unitDetail', action: 'fetchKeyDetails', id: data_id},
            success : function(response){
                var response =  JSON.parse(response);
                var tagKeyData = response.data;
                console.log(response);
                if(response.status == 'success' && response.code == 200) {
                    $('input[name="key_tag"]').val(tagKeyData.key_id);
                    $('input[name="key_desc"]').val(tagKeyData.key_description);
                    $('input[name="total_keys"]').val(tagKeyData.total_keys);
                    $('input[name="key_id"]').val(tagKeyData.id);
                    $('#edit_id_hidden').prop('disabled',false);
                    $('.updateTagKey').val('Update');
                }else if(response.status == 'error' && response.code == 503) {
                    toastr.error(response.message);
                }else {

                }
            }
        });

        unitKeys();
    }
    else if(select_options == 'Delete') {
        bootbox.confirm("Do you want to Delete this record?", function (result) {
            if (result == true) {
                $.ajax({
                    type: 'post',
                    url: '/propertyUnitAjax',
                    data: {class: 'unitDetail', action: 'deleteKey', id: data_id},
                    success : function(response){
                        var response =  JSON.parse(response);
                        var tagKeyData = response.data;
                        console.log(response);
                        if(response.status == 'success' && response.code == 200) {
                            toastr.success('Record Deleted Successfully');
                            unitKeys();
                            triggerUnitKeys();
                        }else if(response.status == 'error' && response.code == 503) {
                            toastr.error(response.message);
                        }else {

                        }
                    }
                });
            }
        });


    }
});
jQuery(document).on('click','#checkout_cancel',function () {
    $('.checkout_div').hide();
    $('.checkout_div').find('input').val('');
});

jQuery(document).on('click','#chekoutSave',function () {

    var formData = $('.checkout_div :input').serializeArray();
    var unit_id =  $("#editUnitId").val();
    $.ajax({
        type: 'post',
        url: '/propertyUnitAjax',
        data: {form: formData,
            class: 'unitDetail',
            action: 'addCheckoutDetail',
            unit_id:unit_id
        },

        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidateAmenities").each(function() {
                var res = validations(this);
                if(res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                unitKeys();
                triggerUnitKeys();
                setTimeout(function () {
                    $('.checkout_div').find('input').val('');
                    $('.checkout_div').hide(300);
                },1000)

            }   else if (response.status == 'error' && response.code == 500) {
                toastr.warning(response.message);
            } else
            {
                var errors = response.data;
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('.' + key).text('*This field is required');
                });
            }

            onTop(true);

        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});

/*jquery to open new complaint box */
$(document).on("click","#new_complaint_button",function(){
    $("#new_complaint").show(500);
    $("#new_complaint .time_span").text('');

    $("#complaint_save").html('Save');
    $('#new_complaint :input').val('');
    $('#edit_complaint_id').val('');
    $('#complaint_id').prop('readonly',false);
    $("#complaint_id").val(Math.random().toString(36).slice(2).substr(0, 6).toUpperCase());
    /*date picker for complaint date */
    $("#complaint_date").datepicker({
        dateFormat: date_format,
        autoclose: true,
        changeMonth: true,
        changeYear: true
    }).datepicker("setDate", new Date());

});

$(document).on("change","#complaint_type_options",function(){
    if ($(this).val() == '18'){
        $('#other_notes_div').show();
    } else {
        $('#other_notes_div').hide();
    }

});
$(document).on("click","#clear_complaint_save",function(){
    bootbox.confirm("Do you want to Clear this form?", function (result) {
        if (result == true) {
            var edit_complaint_id = $('#edit_complaint_id').val();
            if (edit_complaint_id != '') {
                getComplaintDataById(edit_complaint_id);
            } else {
                $("#clear_complaint_save").text('Clear');
                $("#new_complaint .time_span").text('');
                $('#new_complaint :input').val('');
                $('#edit_complaint_id').val('');
                $('#complaint_id').prop('readonly', false);
                $("#complaint_id").val(Math.random().toString(36).slice(2).substr(0, 6).toUpperCase());
                /*date picker for complaint date */
                $("#complaint_date").datepicker({
                    dateFormat: date_format,
                    autoclose: true,
                    changeMonth: true,
                    changeYear: true
                }).datepicker("setDate", new Date());
            }
        }
    });
});

function getComplaintDataById(data_id){
    $.ajax({
        type: 'post',
        url: '/propertyUnitAjax',
        data: {class: 'UnitDetail', action: 'getComplaintDetail', id: data_id},
        success : function(response){
            var response =  JSON.parse(response);
            if(response.status == 'success' && response.code == 200) {
                edit_date_time(response.data.updated_at);
                $.each(response.data.data, function (key, value) {
                    $('#'+key+ ':input').val(value);
                    if(key == 'complaint_type_id' && value!=''){
                        if (value == '18') {
                            $('#other_notes_div').show();
                        } else {
                            $('#other_notes_div').hide();
                        }
                        $('#complaint_type_options option[value='+value+']').attr('selected','selected');
                    }
                    if(key == 'complaint_type_id')
                    {
                        $('#complaint_type_options').val(value);
                    }

                });
            } else if(response.status == 'error' && response.code == 503) {
                toastr.error(response.message);
            }else {
                toastr.warning('Record not updated due to technical issue.');
            }
        }
    });
}
/**
 *change function to perform various actions(edit ,delete)
 * @param status
 */
jQuery(document).on('change','#unit-complaints .select_options',function () {
    var select_options = $(this).val();
    var data_id = $(this).attr('data_id');
    if(select_options == 'Edit')
    {
        $('#new_complaint :input').val('');
        $('#new_complaint').show(500);
        $('#edit_complaint_id').val(data_id);
        $('#complaint_save').html('Update');
        $('#complaint_id').prop('readonly','readonly');

        getComplaintDataById(data_id);
        $("#unit-complaints").trigger('reloadGrid');
        // window.location.href = '/MasterData/AddPropertyType';

    }else if (select_options == 'Print Envelope'){
        $.ajax({
            type: 'post',
            url: '/propertyUnitAjax',
            data: {class: 'unitDetail', action: 'getCompanyData','unit_id':data_id},
            success : function(response){
                var response =  JSON.parse(response);
                if(response.status == 'success' && response.code == 200) {
                    $("#PrintEnvelope").modal('show');
                    $("#company_name").text(response.data.data.company_name)
                    $("#address1").text(response.data.data.address1)
                    $("#address2").text(response.data.data.address2)
                    $("#address3").text(response.data.data.address3)
                    $("#address4").text(response.data.data.address4)
                    $(".city").text(response.data.data.city)
                    $(".state").text(response.data.data.state)
                    $(".postal_code").text(response.data.data.zipcode)
                    $("#building_name").text(response.building.data.building_name)
                    $("#building_address").text(response.building.data.address)


                }else {
                    toastr.warning('Record not updated due to technical issue.');
                }
            }
        });


    }else if(select_options == 'Delete') {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/propertyUnitAjax',
                        data: {class: 'unitDetail', action: 'deleteComplaint', id: data_id},
                        success : function(response){
                            var response =  JSON.parse(response);
                            if(response.status == 'success' && response.code == 200) {
                                toastr.success('Complaint deleted successfully.');
                            } else if(response.status == 'error' && response.code == 503) {
                                toastr.error(response.message);
                            }else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                }
                $("#unit-complaints").trigger('reloadGrid');
            }
        });
    }
    $('.select_options').prop('selectedIndex',0);
});

$(document).on('click','#complaint_save', function (e) {
    e.preventDefault();
    var formData = $('#new_complaint :input').serializeArray();
    var complaint_type_options = $('#complaint_type_options option:selected').val();
    // formData.push({name: 'complaint_type_options', value: complaint_type_options});
    var unit_id =  $("#editUnitId").val();

    $.ajax({
        type: 'post',
        url: '/propertyUnitAjax',
        data: {form: formData,
            class: 'unitDetail',
            action: 'addNewComplaint',
            edit_unit_id:unit_id
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#new_complaint').hide(500);
                $("#unit-complaints").trigger('reloadGrid');
                toastr.success(response.message);
            }   else if (response.status == 'error' && response.code == 500) {
                toastr.error(response.message);
                return false;
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('#' + key).text(value);
                });
            }else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});

/**
 * jqGrid Unit Keys function
 * @param status
 */

function unitComplaints(status) {

    var table = 'complaints';
    var columns = ['<input type="checkbox" id="select_all_complaint_checkbox">','Complaint Id', 'Complaint Type','Notes','Date','Other','Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [{table: 'complaints', column: 'complaint_type_id', primary: 'id', on_table: 'complaint_types'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = ['complaints.status'];
    var extra_where = [{column: 'object_id', value: id, condition: '='},{column: 'module_type', value: "unit", condition: '='}];
    var columns_options = [
        {name:'Id',index:'id', width:100,align:"center",sortable:false,searchoptions: {sopt: conditions},table:table,search:false,formatter:actionCheckboxFmatter},
        {name:'Complaint Id',index:'complaint_id', width:200,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Complaint Type',index:'complaint_type', width:250,align:"center",searchoptions: {sopt: conditions},table:"complaint_types"},
        {name:'Notes',index:'complaint_note', width:300,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Date',index:'complaint_date', width:200,searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Other',index:'other_notes',width:200,align:"center",searchoptions: {sopt: conditions},search:false,table:table},
        {name:'Action',index:'select', title: false, width:100,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionsFmatter}
    ];
    var ignore_array = [];
    jQuery("#unit-complaints").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'complaints.updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Complaints",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 200, drag: true, resize: false} // search options
    );
}

/**
 * jqGrid function to format checkbox column
 * @param status
 */

function actionCheckboxFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {

        var data = '';
        var data = '<input type="checkbox" name="complaint_checkbox[]" class="complaint_checkbox" id="complaint_checkbox_'+rowObject.id+'" data_id="' + rowObject.id + '"/>';
        return data;
    }
}

function actionsFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var editable = $(cellvalue).attr('editable');
        var select = '';
        select = ['Edit','Delete'];
        var data = '';

        if(select != '') {

            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                if(editable == '0' && (val == 'delete' || val == 'Delete')){
                    return true;
                }
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}

/*function to print element by id */
function PrintElem(elem)
{
    Popup($(elem).html());
}
/*function to print element by id */
function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.body).html( '<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if(mywindow.close()){

    }
    $("#print_complaint").modal('hide');
    return true;
}

$(document).on("click",'#editUnitCancel',function(){
    var id =  getParameterByName('id');
     bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
           window.location.href = '/Unit/UnitModule?id='+id;
        }
    });
});
/*jquery to open modal for complaint box to print */
$(document).on("click",'#print_email_button',function(){
    var unit_id =  $("#editUnitId").val();
    favorite=[];
    var no_of_checked =    $('[name="complaint_checkbox[]"]:checked').length
    if(no_of_checked == 0){
        toastr.error('Please select atleast one Complaint.');
        return false;
    }
    $.each($("input[name='complaint_checkbox[]']:checked"), function(){
        favorite.push($(this).attr('data_id'));
    });

    $.ajax({
        type: 'post',
        url: '/propertyUnitAjax',
        data: {class: 'unitDetail', action: 'getComplaintsData','complaint_ids':favorite,'unit_id':unit_id},
        success : function(response){
            var response =  JSON.parse(response);
            if(response.status == 'success' && response.code == 200) {
                $("#print_complaint").modal('show');
                $("#modal-body-complaints").html(response.html)
            }else {
                toastr.warning('Record not updated due to technical issue.');
            }
        }
    });
});


$("#select_all_complaint_checkbox").click(function () {
    $(".complaint_checkbox").prop('checked', $(this).prop('checked'));
});

/*function to print element by id */
function sendComplaintsEmail(elem)
{
    SendMail($(elem).html());
}


function SendMail(data){
    $.ajax({
        type: 'post',
        url: '/propertyUnitAjax',
        data: {class: 'unitDetail', action: 'SendComplaintMail', data_html: data},
        success : function(response){
            var response =  JSON.parse(response);
            if(response.status == 'success' && response.code == 200) {
                toastr.success('Mail send successfully.');
            }else {
                toastr.warning('Mail not send due to technical issue.');
            }
        }
    });
}
setTimeout(function(){
    if(localStorage.getItem("AccordionHref")) {
        var message = localStorage.getItem("AccordionHref");

        $('html, body').animate({
            'scrollTop': $(message).position().top
        });

        localStorage.removeItem('AccordionHref');
    }
}, 100);


$(document).on('click','.updateTagKeyCancel',function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $('.divNewKey').hide(300);
            $('.divNewtracker').hide(300);        }
    });
});

$(document).on('click','#complaint_cancel',function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $('#new_complaint').hide(300);
        }
    });
});

$(document).on('click',".open_file_location",function(){
    var location =    $(this).attr("data-location");
    window.open(location, '_blank');
});



$(document).on("click","#clearUpdateUnitForm",function(){
    bootbox.confirm("Do you want to Clear this form?", function (result) {
        if (result == true) {
            location.reload();
        }
    });
});

