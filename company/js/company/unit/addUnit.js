var photo_videos = [];
$(document).ready(function(){

    var id =  getParameterByName('id');
    var url_buiding_id =  getParameterByName('bid');
    fetchAllUnittypes(false);
    getPropertyDetail(id);
    /*Key Tracker*/
    $("#ancCreateNewKey").click(function(){
        $(".divNewKey").toggle();
    });

    $(document).on("click", ".Newkey", function () {
        $('.customValidateKeyAccessCode').val('');
        $(this).parents('.key_optionsDivClass').find(".NewkeyPopup").show();
        $(this).parents('.NewkeyPopup').find(".key-access-input").val(' ');
        $('#NewkeyPopupid span.error').text('');
    });


    $("#ancCreateNewtracker").click(function(){
        $(".divNewtracker").toggle();
    });

    $(function() {
        $('.latefee-check-outer .latefee-check-hdr input[name="key"]').on('click', function() {
            if ($(this).val() == 'keyUnit') {
                $('.keyFrstdiv').show();
                $('.trackFrstdiv').hide()   ;
                $(".divNewtracker").hide();

            }
            if ($(this).val() == 'trackUnit') {
                $('.trackFrstdiv').show();
                $('.keyFrstdiv').hide();
                $(".divNewKey").hide();
            }
        });
    });
    /*Key Tracker*/

    /*Add Unit As*/
    $(function() {
        $('.latefee-check-outer .check-outer input[name="unit_as"]').on('click', function() {
            if ($(this).val() == '1') {
                $('.unit-indval').show();
                $(".group-checkAva").hide();
                $('.floor_individual').show();
                $('.floor_group').hide();

                var group_building_length = $('.check-availability').length;
                for(var i=1; i<group_building_length; i++)
                {
                    $('#unit_groupfield_'+i).remove();
                }
            }
            if ($(this).val() == '2') {
                $('.group-checkAva').show();
                $(".unit-indval").hide();
                $('.floor_individual').hide();
                $('.floor_group').show();
            }
            $('.floor-group input').val('');
        });
    });
    /*Add Unit As*/

    /*image upload add unit module
    $(document).on('click','#photos_upload',function(){
        $('#unit_img').trigger('click');
    });

    $(document).on('click','#file_upload',function(){
        $('#unit_file').trigger('click');
    });

    function readURL(input,showto) {

        if (input.files && input.files[0]) {
            var type = input.files[0]['type'];
            var reader = new FileReader();
            if(type == 'image/png' || type == 'image/jpeg' || type == 'image/gif') {
                reader.onload = function (e) {
                    $('#' + showto).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            } else {
                toastr.warning('Please select file with .jpeg | .png | .gif extension only!');
            }
        }
    }
    image upload add unit module*/



    $(document).on("click", ".dp-upload-icon", function (e) {
        $('.file-upload-images').trigger('click');
    });

    $(document).on("click",".remove-all-files",function(e){
        $('.show-list-imgs').html("");
    });

    $(document).on("change",".file-upload-images",function(e){

        var FileLength = e.target.files.length;

        for(var i =0 ; i < FileLength; i++) {
            var fileName1 = e.target.files[i].name;
            $(".show-list-imgs").append('<div class="col-sm-12 img-upload-div">' +
                '<div class="col-sm-2"><img class="img-upload-tab'+ i+'" src=""></div>' +
                '<div class="top-marginCls col-sm-push-1 col-sm-3 show-list-imgs-name'+ i+'"></div>' +
                '<div class="top-marginCls col-sm-push-1 col-sm-3 show-list-imgs-size'+ i+'"></div>' +
                '<div class="top-marginCls col-sm-3 cancel-generte-files text-right"><span class="remove_cancel" data-id="'+i+'">Cancel</span>' +
                '<input type="hidden" id="'+i+'" name="fileshidden[]" value="'+fileName1+'" /></div></div>');
        }

        for(var j =0 ; j < FileLength; j++) {
            var fileName = e.target.files[j].name;
            var fileSize = e.target.files[j].size;
            var iSize = (fileSize / 1024);
            iSize = (Math.round(iSize * 100) / 100);
            var className = ".show-list-imgs .img-upload-tab"+j;
            readURL(this, className, j);
            $(".show-list-imgs .show-list-imgs-name"+j).text(fileName);
            $(".show-list-imgs .show-list-imgs-size"+j).text(iSize + " KB");
        }

        /* upload tenant photo start */
        function readURL(input,imgClass,j) {

            if (input.files && input.files[j]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(imgClass).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[j]);
            }
        }
    });


    /*Cancel button js*/
    jQuery(document).on("click",".img-upload-div span",function(){
        var dataid = $(this).attr("data-id");
        $("#"+dataid).remove();
        $(this).parent().parent().remove();
    });
    /*Cancel button js*/



    /*File upload add unit module*/



    $(document).on("click", ".dp-upload-icon2", function (e) {
        $('.file-upload-images2').trigger('click');
    });

    $(document).on("click",".remove-all-files2",function(e){
        $('.show-list-imgs2').html("");
    });

    $(document).on("change",".file-upload-images2",function(e){

        var FileLength = e.target.files.length;

        for(var i =0 ; i < FileLength; i++) {
            var fileName1 = e.target.files[i].name;
            $(".show-list-imgs2").append('<div class="col-sm-12 img-upload-div2">' +
                '<div class="col-sm-2"><img class="img-upload-tab2'+ i+'" src=""></div>' +
                '<div class="top-marginCls col-sm-push-1 col-sm-3 show-list-imgs-name2'+ i+'"></div>' +
                '<div class="top-marginCls col-sm-push-1 col-sm-3 show-list-imgs-size2'+ i+'"></div>' +
                '<div class="top-marginCls col-sm-3 cancel-generte-files2 text-right"><span class="remove_cancel2" data-id="'+i+'">Cancel</span>' +
                '<input type="hidden" id="'+i+'" name="fileshidden[]" value="'+fileName1+'" /></div></div>');
        }

        for(var j =0 ; j < FileLength; j++) {
            var fileName = e.target.files[j].name;
            var fileSize = e.target.files[j].size;
            var iSize = (fileSize / 1024);
            iSize = (Math.round(iSize * 100) / 100);
            var className = ".show-list-imgs2 .img-upload-tab2"+j;
            readURL(this, className, j);
            $(".show-list-imgs2 .show-list-imgs-name2"+j).text(fileName);
            $(".show-list-imgs2 .show-list-imgs-size2"+j).text(iSize + " KB");
        }

        /* upload tenant photo start */
        function readURL(input,imgClass,j) {

            if (input.files && input.files[j]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(imgClass).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[j]);
            }
        }
    });


    /*Cancel button js*/
    jQuery(document).on("click",".img-upload-div2 span",function(){
        var dataid = $(this).attr("data-id");
        $("#"+dataid).remove();
        $(this).parent().parent().remove();
    });
    /*Cancel button js*/




    /*For fax clone*/
    $(document).on("click", ".primary-tenant-fax-row .fax-add-remove-row > .glyphicon-plus-sign", function () {
        var phoneRowLenght = $(".primary-tenant-fax-row");
        var clone = $(".primary-tenant-fax-row:first").clone();
        if (phoneRowLenght.length == 2) {
            $(".primary-tenant-fax-row .glyphicon-plus-sign").hide();
            $(".primary-tenant-fax-row").first().next().after(clone).next().find('input').val('');
        } else {
            $(".primary-tenant-fax-row:not(:eq(0)) .fax-add-remove-row .glyphicon-plus-sign").show();
            $(".primary-tenant-fax-row").first().after(clone).next().find('input').val('');
        }



        $(".primary-tenant-fax-row:not(:eq(0)) .fax-add-remove-row .glyphicon-plus-sign").remove();
        $(".primary-tenant-fax-row:not(:eq(0)) .fax-add-remove-row .glyphicon-remove-sign").show();


        $('.primary-tenant-fax-row').each(function (key, value) {
            $(this).find('span.error').removeClass('fax_number_0Err');
            var i = parseInt(key);
            $(this).find('span.error').addClass('fax_number_'+i+'Err');
            $('span.fax_number_2Err').removeClass('fax_number_1Err');
        });

    });


    $(document).on("click", ".primary-tenant-fax-row .fax-add-remove-row .glyphicon-remove-sign", function () {
        var phoneRowLenght = $(".primary-tenant-fax-row");

        if (phoneRowLenght.length == 3 || phoneRowLenght.length == 2) {
            $(".primary-tenant-fax-row .glyphicon-plus-sign").show();
        } else {
            $(".primary-tenant-fax-row .glyphicon-plus-sign").hide();
        }
        $(this).parents(".primary-tenant-fax-row").remove();
        $('span.error.red-star.fax_number_2Err').addClass('fax_number_1Err');
        $('span.error.red-star.fax_number_2Err').removeClass('fax_number_2Err');

    });
    /*For fax clone*/

    /*For Phone Number clone*/
    $(document).on("click", ".primary-tenant-phone-row .phone-add-remove-row > .glyphicon-plus-sign", function () {
        var phoneRowLenght = $(".primary-tenant-phone-row");
        var clone = $(".primary-tenant-phone-row:first").clone();
        if (phoneRowLenght.length == 2) {
            $(".primary-tenant-phone-row .glyphicon-plus-sign").hide();
            $(".primary-tenant-phone-row").first().next().after(clone).next().find('input').val('');
        } else {
            $(".primary-tenant-phone-row:not(:eq(0)) .phone-add-remove-row .glyphicon-plus-sign").show();
            $(".primary-tenant-phone-row").first().after(clone).next().find('input').val('');
        }



        $(".primary-tenant-phone-row:not(:eq(0)) .phone-add-remove-row .glyphicon-plus-sign").remove();
        $(".primary-tenant-phone-row:not(:eq(0)) .phone-add-remove-row .glyphicon-remove-sign").show();

        $('.primary-tenant-phone-row').each(function (key, value) {
            $(this).find('.error').removeClass('phone_number_0Err');
            var i = parseInt(key);
            $(this).find('.error').addClass('phone_number_'+i+'Err');
            $('.phone_number_2Err').removeClass('phone_number_1Err');
        })
    });
    $(document).on("click", ".primary-tenant-phone-row .phone-add-remove-row .glyphicon-remove-sign", function () {
        var phoneRowLenght = $(".primary-tenant-phone-row");

        if (phoneRowLenght.length == 3 || phoneRowLenght.length == 2) {
            $(".primary-tenant-phone-row .glyphicon-plus-sign").show();
        } else {
            $(".primary-tenant-phone-row .glyphicon-plus-sign").hide();
        }
        $(this).parents(".primary-tenant-phone-row").remove();

        $('span.error.red-star.phone_number_2Err').addClass('phone_number_1Err');
        $('span.error.red-star.phone_number_2Err').removeClass('phone_number_2Err');

    });
    /*For Phone Number clone*/

    /*For Key/Access code clone*/
    $(document).on("click", ".primary-tenant-keyAccess-row .keyAccess-add-remove-row > .glyphicon-plus-sign", function () {
        var phoneRowLenght = $(".primary-tenant-keyAccess-row");

        if (phoneRowLenght.length == 2) {
            $(".primary-tenant-keyAccess-row .glyphicon-plus-sign").hide();
        } else {
            $(".primary-tenant-keyAccess-row:not(:eq(0)) .keyAccess-add-remove-row .glyphicon-plus-sign").show();
        }
        var clone = $(".primary-tenant-keyAccess-row:first").clone();
        $(".primary-tenant-keyAccess-row").first().after(clone);
        $(".primary-tenant-keyAccess-row:not(:eq(0)) .keyAccess-add-remove-row .glyphicon-plus-sign").remove();
        $(".primary-tenant-keyAccess-row:not(:eq(0)) .keyAccess-add-remove-row .glyphicon-remove-sign").show();

    });
    $(document).on("click", ".primary-tenant-keyAccess-row .keyAccess-add-remove-row .glyphicon-remove-sign", function () {
        var phoneRowLenght = $(".primary-tenant-keyAccess-row");

        if (phoneRowLenght.length == 3 || phoneRowLenght.length == 2) {
            $(".primary-tenant-keyAccess-row .glyphicon-plus-sign").show();
        } else {
            $(".primary-tenant-keyAccess-row .glyphicon-plus-sign").hide();
        }
        $(this).parents(".primary-tenant-keyAccess-row").remove();
    });
    /*For Key/Access code clone*/




    $(document).on('click', '.check-availability .add_building_div', function () {
        //alert("hii");

        var blueouter_length = $('.check-availability').length;
        blueouter_length = blueouter_length;
        //alert(blueouter_length);
        var html_data = '';

        html_data += '<div class="check-availability" id="unit_groupfield_' + blueouter_length + '">\n' +
            '                                                                            <div class="col-xs-12 col-sm-4 col-md-2">\n' +
            '                                                                                <label>Select Building <em class="red-star">*</em>\n' +
            '                                                                                    <a class="pop-add-icon" href="javascript:;">\n' +
            '                                                                                        <i class="add-more-div fa fa-plus-circle" data-toggle="modal" data-target="#AddNewBuildingModal" aria-hidden="true" data-backdrop="static" data-keyboard="false" ></i>\n' +
            '                                                                                    </a>\n' +
            '                                                                                </label>\n' +
            '                                                                                <select class="form-control unit_building" name="building_id[]"> <option value="">Select</option></select><span class="building_id_'+blueouter_length+'Err error red-star building_id_Err"></span>\n' +
            '                                                                            </div>\n' +
            '                                                                            <div class="floor-indval col-xs-12 col-sm-4 col-md-2">\n' +
            '                                                                                <label>Floors <em class="red-star">*</em></label>\n' +
            '                                                                                <input placeholder="Eg: 1" class="form-control numberInput" name="floor_no[]" type="text" /> <span class="floor_no_'+blueouter_length+'Err error red-star floor_no_Err"></span>\n' +
            '                                                                            </div>\n' +
            '                                                                            <div class="col-xs-12 col-sm-4 col-md-2">\n' +
            '                                                                                <label>Unit Prefix</label>\n' +
            '                                                                                <input placeholder="Eg: A"  name="unit_prefix[]" class="form-control" type="text" /><span class="unit_prefix_'+blueouter_length+'Err error red-star"></span>\n' +
            '                                                                            </div>\n' +
            '\n' +
            '                                                                            <div class="group-checkAva col-xs-12 col-sm-4 col-md-6">\n' +
            '                                                                                <div class="col-sm-4 group_unit">\n' +
            '                                                                                    <label>\n' +
            '                                                                                        From<span style="color: Red; float: none"> *</span>\n' +
            '                                                                                    </label>\n' +
            '                                                                                    <span>\n' +
            '                                                                                        <input type="text" class="form-control txtFrom numberInput" name="unit_from[]"  maxlength="7" value="" placeholder="Eg: 100">\n' +
            '                                                                                        <span class="unit_from_'+blueouter_length+'Err error red-star unit_from_Err"></span>' +
            '                                                                                    </span>\n' +
            '                                                                                </div>\n' +
            '                                                                                <div class="col-sm-4 group_unit">\n' +
            '                                                                                    <label>\n' +
            '                                                                                        To<span style="color: Red; float: none"> *</span>\n' +
            '                                                                                    </label>\n' +
            '                                                                                    <span>\n' +
            '                                                                                        <input type="text" class="form-control txtTo numberInput" name="unit_to[]" maxlength="7" value="" placeholder="Eg: 100">\n' +
            '                                                                                       <span class="unit_to_'+blueouter_length+'Err error red-star unit_to_Err"></span>\n' +
            '                                                                                    </span>\n' +
            '                                                                                </div>\n' +
            '                                                                                <div class="col-sm-4 group_unit btn-check-ava">\n' +
            '                                                                                    <span class="check_availability" style="color:#3c763d;padding-top: 15px;float: left;width: 100%;">\n' +
            '                                                                                        <i class="fa fa-check" aria-hidden="true"></i>\n' +
            '                                                                                        Check Availability\n' +
            '                                                                                    </span>\n' +
            '                                                                                    <br>\n' +
            '                                                                                    <span class="add_building_div" style="cursor: pointer;">\n' +
            '                                                                                        <i class="fa fa-plus-circle"></i> Add\n' +
            '                                                                                    </span> <br>\n' +
            '                                                                                    <span class="remove_building_div" style="color:#a94442;cursor: pointer;">\n' +
            '                                                                                        <i class="fa fa-minus"></i> Delete\n' +
            '                                                                                    </span>\n' +
            '                                                                                </div>\n' +
            '                                                                            </div>\n' +
            '                                                                        </div>';

        //$(html_data).insertBefore('#unit_blueouter_before');
        $(html_data).insertBefore('.unit_blueouter_before');

        fetchPropertyDetails(id);

    });
    $(document).on('click', '.remove_building_div', function () {
        var blueouter_length = $('.check-availability').length;
        if (blueouter_length > 1) {
            $(this).parents('.check-availability').remove();
        }
        $('.check-availability').each(function(key,value){
            var key1= parseInt(key)+parseInt(1);

            $(this).find('span').removeClass('building_id_'+key1+'Err');
            $(this).find('.building_id_Err').addClass('building_id_'+key+'Err');

            $(this).find('span').removeClass('floor_no_'+key1+'Err');
            $(this).find('.floor_no_Err').addClass('floor_no_'+key+'Err');

            $(this).find('span').removeClass('unit_from_'+key1+'Err');
            $(this).find('.unit_from_Err').addClass('unit_from_'+key+'Err');

            $(this).find('span').removeClass('unit_to_'+key1+'Err');
            $(this).find('.unit_to_Err').addClass('unit_to_'+key+'Err');
        });
    });


    $("input[name='last_renovation_date']").datepicker({
        dateFormat: date_format,
        autoclose: true,
        changeMonth: true,
        changeYear: true
    });

    $("input[name='tag_key_pick_date']").datepicker({
        dateFormat: date_format,
        autoclose: true,
        changeMonth: true,
        changeYear: true
    });

    $("input[name='tag_key_return_date']").datepicker({
        dateFormat: date_format,
        autoclose: true,
        changeMonth: true,
        changeYear: true
    });


    $("input[name='last_renovation_time']").timepicker({ timeFormat: 'h:mm p',defaultTime: new Date()});
    $("input[name='tag_key_pick_time']").timepicker({ timeFormat: 'h:mm p',defaultTime: new Date()});
    $("input[name='tag_key_return_time']").timepicker({ timeFormat: 'h:mm p',defaultTime: new Date()});


    $('#addUnitForm .btn-check-ava').click(function() {
        $(".group-checkAva #unit_from_1_errcustom").show();
    });

    //key access code
    $(document).on("click", "#NewkeyPopupCancel", function () {
        $(this).closest('.NewkeyPopup').hide();
    });
    //hide key access box
    $(document).on("click", "#NewkeyMinus", function () {
        $(this).parents('#key_optionsDiv').remove();
    });

    // check all amenities
    $(document).on("click", "#AddNewBuildingModal #selectAllamennity", function () {
        $(".inputAllamenity").prop('checked', true);
        if ($('.all').prop("checked") == false) {
            $(".inputAllamenity").prop('checked', false);
        }
    });
    $(document).on("click", "#amenties_box #selectAllamennity input", function () {
        if ($(this).is(':checked')) {
            $(".inputAllamenity").prop('checked', true);
        } else {
            $(".inputAllamenity").prop('checked', false);
        }
    });

    $(document).on("click",'#NewamenitiesPopupSaveOne,#NewamenitiesPopupSaveTwo', function (e) {
        e.preventDefault();
        var classCheck = $(this).attr('id');
        if(classCheck == 'NewamenitiesPopupSaveTwo') {
            var formData = $('#NewamenitiesPopupTwo :input').serializeArray();
        } else {
            var formData = $('#NewamenitiesPopupOne :input').serializeArray();
        }

        $.ajax({
            type: 'post',
            url: '/add-amenity-popup',
            data: {form: formData,
                class: 'commonPopup',
                action: 'addAmenityPopup'},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewamenitiesPopupOne').hide(500);
                    toastr.success(response.message);
                    var lastid = response.lastid;
                    fetchAllAmenities(response.lastid)

                    setTimeout(function(){
                        $("#"+lastid).attr("checked",true)
                    }, 1000);
                    $('#NewamenitiesPopupTwo').hide();
                    $('#NewamenitiesPopupOne').hide();

                }   else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        if (key == 'codeErr' && value == 'codeErr  is required.') {
                            $('#NewamenitiesPopupTwo #' + key).text('This field is required.');
                            $('#NewamenitiesPopupOne #' + key).text('This field is required.');
                        } else if (key == 'codeErr' && value == 'codeErr  is required.') {
                            $('#NewamenitiesPopupTwo #' + key).text('This field is required.');
                            $('#NewamenitiesPopupOne #' + key).text('This field is required.');
                        } else {
                            $('#NewamenitiesPopupTwo #' + key).text(value);
                            $('#NewamenitiesPopupOne #' + key).text(value);
                        }
                    });
                }else if (response.status == 'error' && response.code == 500) {
                    toastr.error(response.message);
                }

                onTop(true);
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    function fetchAllAmenities(id) {
        $.ajax({
            type: 'post',
            url: '/fetch-all-amenities',
            data: {
                class: 'commonPopup',
                action: 'fetchAllAmenitiesProperty'},
            success: function (response) {
                var res = JSON.parse(response);
                $('#amenties_boxBuilding').html(res.data);
            },
        });
    }

    function fetchAllAmenitiesBuilding()
    {
        $.ajax({
            type: 'post',
            url: '/fetch-all-amenities',
            data: {
                class: 'commonPopup',
                action: 'fetchAllAmenitiesProperty'},
            success: function (response) {
                var res = JSON.parse(response);
                $('#amenties_boxBuilding').html(res.data);
            },
        });
    }



    /**
     * If the letter is not digit in fax then don't type anything.
     */
    $(document).on('keypress','.fax_number',function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    /**
     * To change the format of fax
     */
    $(document).on('keypress','.fax_number',function (e) {
        $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
    });

    /**
     * If the letter is not digit in phone number then don't type anything.
     */
    $(document).on('keypress','.phone_number',function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    /**
     * To change the format of phone number
     */
    $(document).on('keydown','.phone_number',function (e) {
        $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
    });

    fetchPropertyDetails(id);

    $(".hidden_ids #building_id").val();
    var eme = $(".hidden_ids #property_id").val();
    function getParameterByName( name ){
        var regexS = "[\\?&]"+name+"=([^&#]*)",
            regex = new RegExp( regexS ),
            results = regex.exec( window.location.search );
        if( results == null ){
            return "";
        } else{
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

    function getPropertyDetail(id) {

        setTimeout(function(){
            var edit_unit_id = $('#edit_unit_id').val();

            if(edit_unit_id)
            {
                id = edit_unit_id;
            }
            $.ajax({
                type: 'post',
                url: '/propertyUnitAjax',
                data: {
                    class: 'propertyDetail',
                    action: 'gerPropertyDetail',
                    id:id
                },
                success: function (response) {
                    var res = JSON.parse(response);
                    if(res) {
                        var school_district_muncipiality_data = res.school_district_muncipiality_data;
                        $('.propertyName').text(res.property_detail.property_name);
                        $('.propertyID').text(res.property_detail.property_id);
                        var school_html = '';
                        if(school_district_muncipiality_data) {
                            // $('#school_district_municipality').val(school_district_muncipiality_data[0].school_district);
                            // $('#school_district_code').val(school_district_muncipiality_data[0].code);
                            // $('#school_district_notes').val(school_district_muncipiality_data[0].notes);
                            $.each(school_district_muncipiality_data, function(i, val) {
                                // console.log(val.school_district)
                                //debugger
                                school_html += '       <div class="col-xs-12 col-sm-4 col-md-3">\n' +
                                    '                                                                                <label>School District</label>\n' +
                                    '                                                                                <input type="text" value="'+val.school_district+'" readonly class="form-control disabled_field" id="school_district_municipality" />\n' +
                                    '                                                                            </div>\n' +
                                    '                                                                            <div class="col-xs-12 col-sm-4 col-md-3">\n' +
                                    '                                                                                <label>Code</label>\n' +
                                    '                                                                                <input type="text" readonly value="'+val.code+'"  class="form-control disabled_field" id="school_district_code" />\n' +
                                    '                                                                            </div>\n' +
                                    '                                                                            <div class="col-xs-12 col-sm-4 col-md-3">\n' +
                                    '                                                                                <label>Notes</label>\n' +
                                    '                                                                                <input type="text" readonly value="'+val.notes+'"   class="form-control disabled_field" id="school_district_notes" />\n' +
                                    '                                                                            </div><div class="col-xs-12 col-sm-4 col-md-3"></div>';
                            });
                            $('#schoolHTML').html(school_html);
                        }
                        if(res.property_detail.smoking_allowed == 0 ){
                            $('#smoking_allowed option[value=0]').attr("selected", "selected");
                            $('#smoking_allowed').attr("disabled", "disabled");
                        }

                        if(res.property_detail.pet_friendly == 1 ){
                            setTimeout(function(){
                                $('#pet_options option[value=1]').attr("selected", "selected");
                                $('#pet_options').attr("disabled", "disabled");
                            }, 500);
                        }

                        if(res.property_detail.amenities)
                        {
                            var total_amenties = $('#amenties_box input.inputAllamenity').length;
                            var amenties = res.property_detail.amenities;
                            var amenties_length = amenties.length;

                            if(total_amenties == amenties_length)
                            {
                                $('#amenties_box #selectAllamennity input').prop('checked', true);
                                $('#amenties_box1 #selectAllamennity input').prop('checked', true);
                            }
                            setTimeout(function(){
                                if(amenties)
                                {
                                    $.each(amenties, function(i, val){
                                        $("#amenties_box input[value='" + val + "']").prop('checked', true);
                                        $("#amenties_box1 input[value='" + val + "']").prop('checked', true);
                                    });
                                }
                            }, 500);
                        }
                    }
                },
            });
        },1000)

    }

    $("#building_id").val(Math.random().toString(36).slice(2).substr(0, 6).toUpperCase());
    /*Get property details*/
    $(document).on('click','#addNewBuildingModalPlus',function () {
        var prop_id = id;
        getPropertyDetails(id);
        $('#AddNewBuildingModal').modal('show');
        $('#NewamenitiesPopupTwo').css('display', 'none');
    });

    function getPropertyDetails(id){
        $.ajax({
            type: 'post',
            url: '/propertyUnitAjax',
            data: {
                class: 'propertyDetail',
                action: 'getpropertyDetails',
                id:id
            },
            success: function (response) {
                var data = $.parseJSON(response);

                if (data.status == "success"){
                    $("#AddNewBuildingModal #legal_name1").val(data.data.property_name);
                    $("#AddNewBuildingModal #building_name").val(data.data.property_name);
                    $("#AddNewBuildingModal #no_of_units").val(data.data.no_of_units);
                    $("#AddNewBuildingModal #address").val(data.data.address1);
                    //$("#AddNewBuildingModal #smoking_allowed option").val(data.data.smoking_allowed);
                    $("#property_id").append('<option value="'+data.data.id+'" selected>'+data.data.property_name+'</option>')
                    $("#AddNewBuildingModal #pet_allowed_id").val(data.data.pet_friendly);
                    if(data.data.smoking_allowed == 0 ){
                        $('#smoking_allowed option[value=0]').attr("selected", "selected");
                        $('#smoking_allowed').attr("disabled", "disabled");
                    }else{
                        $('#smoking_allowed option[value=1]').attr("selected", "selected");
                        $('#smoking_allowed').attr("disabled", "disabled");
                    }

                    if(data.data.pet_friendly == 1 ) {
                        setTimeout(function () {
                            $('#pet_allowed_id option[value=1]').attr("selected", "selected");
                            $('#pet_allowed_id').attr("disabled", "disabled");
                        }, 500);
                    }else {
                        setTimeout(function () {
                            $('#pet_allowed_id option[value=0]').attr("selected", "selected");
                            $('#pet_allowed_id').attr("disabled", "disabled");
                        }, 500);
                    }
                    $("#AddNewBuildingModal .commonCheckboxClass .inputAllamenity").val(data.data.amenities);
                    $("#AddNewBuildingModal #description").val(data.data.description);
                    $('#no_seprate_unit_checkbox').prop('checked', false);
                    $('#no_seprate_unit').hide();
                    $('#no_seprate_unit :input[type="text"]').val('');
                    $('#add_building_form label.error').text('');
                    fetchAllAmenitiesBuilding();
                    var result = data.data;
                    // $.each(result, function (key, value) {
                    //     $('#'+key).val(value);
                    // });
                }

            },
        });
    }
    //Add Building Details
    $(document).on('submit','#add_building_form',function( e ) {
        $(this).valid();

        // no form to actually submit
        e.preventDefault();
    });
    var req = $;
    $('#add_building_form').validate({
        rules: {
            address             : 'required',
            property_id         : 'required',
            building_id         : 'required',
            no_of_units         : 'required',
            building_name       : 'required',
            base_rent           : 'required',
            unit_type           : 'required',
            market_rent         : 'required',
            security_deposit    : 'required',
        },
        submitHandler: function () {
            var BaseRent = parseFloat($('#base_rent').val());
            var MarketRent = parseFloat($('#Pmarket_rent').val());
            if(BaseRent > MarketRent)
            {
                toastr.warning('Base rent should not greater then Market rent.');
                return false;
            }

            var uploadform = new FormData();
            var formData = $('#add_building_form').serialize();
            var legal_name = $('#legal_name1').val();
            var building_name = $('input[name="building_name"]').val();
            uploadform.append('class', "buildingDetail");
            uploadform.append('action', "addBuilding");
            uploadform.append('form', formData + '&legal_name=' + legal_name);
            uploadform.append('unit_module_send', true);
            req.ajax
            ({
                type: 'post',
                url: '/Building/AddBuildingDetail',
                processData: false,
                contentType: false,
                data: uploadform,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        toastr.success('Record Added Successfully.');
                        $('#AddNewBuildingModal').modal('toggle');
                        $('.unit_building')
                            .append($("<option></option>")
                                .attr("value",response.last_insert_id)
                                .text(building_name));
                        //   $('#unit_building option:last').attr("selected", "selected");
                    } else if(response.status == 'error' && response.code == 400){
                        req('.error').html('');
                        req.each(response.data, function (key, value) {
                            req('.'+key).html(value);
                        });
                    }else{
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    // var errors = req.parseJSON(data.responseText);
                    // req.each(errors, function (key, value) {
                    //     // alert(key+value);
                    //     req('#' + key + '_err').text(value);
                    // });
                }
            });
        }
    });




    jQuery(document).on("click",'#no_seprate_unit_checkbox',function() {
        var no_of_units = $("#no_of_units").val();
        if (no_of_units == 1) {
            if ($(this).is(":checked"))
                $("#no_seprate_unit").show(500);
            else
                $("#no_seprate_unit").hide(500)
        } else {
            toastr.error("Number of units must be equal to 1");
            return false;
        }


    });

    jQuery(document).on("keyup keydown",'#no_of_units',function() {
        if ($("#no_seprate_unit_checkbox").is(":checked") && $(this).val() > 1){
            $(this).val(1)
        }
    });


    $(document).on('click', '#AddNewUnitTypeModalPlus2', function (e) {
        e.preventDefault();
        $("#NewunitPopup").show();
        $("#NewunitPopup :input[type = 'text']").val('');
        $("#NewunitPopup span.error").text('');
    });

    $(document).on('click', '#NewunitPopupSave', function (e) {
        e.preventDefault();
        var formData = $('#NewunitPopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addUnittype'},
            beforeSend: function (xhr) {
                // checking portfolio validations
                $(".customValidateUnitType").each(function () {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewunitPopup').hide(500);
                    toastr.success(response.message);
                    fetchAllUnittypes(response.lastid);
                } else if (response.status == 'error' && response.code == 500) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    function fetchAllUnittypes(id) {
        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {
                class: 'propertyDetail',
                action: 'fetchAllUnittypes'},
            success: function (response) {
                var res = JSON.parse(response);
                $('#unit_type').html(res.data);
                $('#dynamic_unit_type1 #unit_type').html(res.data);
                console.log('fetchAllUnittypes>>', id);
                if (id != false) {
                    $('#unit_type option[value="'+id+'"]').attr('selected', true);
                }
                $("#default_rent").val(response.default_rent);
            },
        });

    }
    /*Get property details*/


    function fetchPropertyDetails(id) {
        var building_id =  $('#editBuildingId').val();

        var prop_id = id;
        $.ajax({
            type: 'post',
            url: '/propertyUnitAjax',
            data: {
                class: 'propertyDetail',
                action: 'fetchPropertyDetail',
                id:id,building_id:building_id},
            success: function (response) {
                var res = JSON.parse(response);
                var data = res.data;

                if(data.building_disabled <= 0)
                {
                    $('.building_popup .pop-add-icon').addClass('disabled_building');
                    $('.building_popup .add-more-div').removeAttr('data-target');
                } else {
                    $('.building_popup .pop-add-icon').removeClass('disabled_building');
                    $('.building_popup .add-more-div').attr('data-target','#AddNewBuildingModal');

                    setTimeout(function () {
                        $(data.propertyAll).each(function(key,value){
                            $('#property_id')
                                .append($("<option></option>")
                                    .attr("value",value.id)
                                    .text(value.property_name));
                            if(prop_id)
                            {
                                $('#property_id').val(prop_id);
                            }

                            $("#AddNewBuildingModal #property_input").val(data.property_id);

                        });
                    },1000);
                }

                setTimeout(function () {
                    if(data.countries)
                    {
                        $('#flag_country_code')
                            .append($("<option></option>")
                                .attr("value", '')
                                .text('Select Country Code'));
                        $(data.countries).each(function (key, value) {
                            $('#flag_country_code')
                                .append($("<option></option>")
                                    .attr("value", value.id)
                                    .text(value.name + '(' + value.code + ')'));
                        });
                    }
                    $('input[name="flag_by"]').val(data.user_name);
                },1000);

                $('.unit_building').html('');
                $('.unit_building')
                    .append($("<option></option>")
                        .attr("value",'')
                        .text('Select'));
                $(data.buildingAll).each(function(key,value){
                    $('.unit_building')
                        .append($("<option></option>")
                            .attr("value",value.id)
                            .text(value.building_name));
                });
                $('.unit_building').val(url_buiding_id);
                var market_rent = data.default_rent;
                //  market_rent = Number(market_rent.replace(/[^0-9.-]+/g,""));
                $('#market_rent').val(market_rent);
                $('#Pmarket_rent').val(market_rent);
            },
        });

    }

    $(document).on('click','.disabled_building',function(){
        toastr.warning('Building limit reached.');
    });

    $(document).on('click','#NewamenitiesOne',function(){
        $('div#NewamenitiesPopupOne').show();
        $('#NewamenitiesPopupOne :input[type = "text"]').val('');
        $('#NewamenitiesPopupOne span.error').text('');
    });
    $(document).on('click','#NewamenitiesTwo',function(){
        $('div#NewamenitiesPopupTwo').show();
        $('#NewamenitiesPopupTwo :input[type = "text"]').val('');
        $('#NewamenitiesPopupTwo span.error').text('');
    });

    $('#addUnitForm').on('submit',function (e) {
        e.preventDefault();
        var uploadform = new FormData();
        var formData = $('#addUnitForm').serialize();

        var market_rent = $('#market_rent').val();
        market_rent = Number(market_rent.replace(/[^0-9.-]+/g,""));

        var baseRent = $('#baseRent').val();
        baseRent = Number(baseRent.replace(/[^0-9.-]+/g,""));

        var securityDeposit = $('#securityDeposit').val();
        securityDeposit = Number(securityDeposit.replace(/[^0-9.-]+/g,""));

        var custom_field = [];
        combine__data_photo_document_array=[];
        combine__data_photo_document_array=[];
        $(".custom_field_html input").each(function(){
            var data = {'name':$(this).attr('name'),'value':$(this).val(),'id':$(this).attr('data_id'),'is_required':$(this).attr('data_required'),'data_type':$(this).attr('data_type'),'default_value':$(this).attr('data_value')};
            custom_field.push(data);
        });
        var  combine_photo_document_array =[];
        var length = $('#photo_video_uploads > div').length;
        var length1 = $('#file_library_uploads > div').length;

        combine_photo_document_array =  $.merge( photo_videos, file_library );
        var dataaa = Array.from(new Set(combine_photo_document_array)); // #=> ["foo", "bar"]

        if(length > 0 || length1 > 0) {

            var data = convertSerializeDatatoArrayPhotos();
            var data1 = convertSerializeDatatoArray();
            var combine__data_photo_document_array =  $.merge( data, data1 );

            $.each(dataaa, function (key, value) {
                if(compareArray(value,combine__data_photo_document_array) == 'true'){
                    uploadform.append(key, value);
                }
            });
        }
        var edit_id = $('#editUnitId').val();
        uploadform.append('market_rent', market_rent);
        uploadform.append('baseRent', baseRent);
        uploadform.append('securityDeposit', securityDeposit);
        uploadform.append('class', "propertyDetail");
        uploadform.append('custom_field', JSON.stringify(custom_field));
        if(edit_id)
        {
            uploadform.append('action', "updatePropertyUnit");
        }else{
            uploadform.append('action', "insertPropertyUnit");
        }

        uploadform.append('form', formData);

        $.ajax({
            type: 'post',
            url: '/propertyUnitAjax',
            cache: false,
            processData: false,
            contentType: false,
            data:uploadform,
            success: function (response) {
                var res = JSON.parse(response);
                // var data = res.data;

                if (res.status == 'success' && res.code == 200) {
                    toastr.success(res.message);
                    if(!edit_id)
                    {
                        setTimeout(function () {
                            window.location.href = '/Unit/UnitModule?id='+id;
                        },2000);
                    } else {
                        var property_id = $('#editPropID').val();
                        setTimeout(function () {
                            window.location.href = '/Unit/UnitModule?id='+property_id;
                        },2000);

                    }

                    localStorage.setItem('rowcolor', 'rowColor');

                } else if (res.status == 'error' && res.code == 400) {
                    $('.error').html('');
                    if(res.data == 'base_rent_error')
                    {
                        toastr.warning('Base rent should not greater then Market rent.');
                    } else if(res.data == 'property_id_error'){
                        toastr.warning('Incorrect Property. Please select again.');
                    } else if(res.data == 'unit_limit_reached'){
                        toastr.warning('Unit Limit Reached');
                    } else {
                        $.each(res.data, function (key, value) {

                            $('.' + key).html('* This field is required');
                        });
                        toastr.warning(res.message);
                    }


                } else if (res.status == 'warning' && res.code == '500' && res.message=="File allready exists.") {
                    toastr.warning('File allready exists.');
                }
            },
        });

    });





    $(document).on('keypress','.numberInput',function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    /*  $(document).on('change','.key_access_codes_info',function(){
          var key_access = $(this).val();
          if(key_access)
          {
              $(this).next().show();
          } else {
              $(this).next().hide();
          }
      });
  */
    $(document).on("change", ".key_access_codes_list", function () {
        $(this).next('.key_access_codeclass').find('textarea').val('');

        var value = $(this).val();
        if (parseInt(value) >= 1) {
            $(this).parent().find('.key_access_codeclass').show();
            $(this).parent().find('.key_access_codeclass').val(' ');
        } else {
            $(this).parent().find('.key_access_codeclass').hide();
        }
    });



    /*   setTimeout(function(){
           $('#key_options')
               .prepend($("<option></option>")
                   .attr("value",'')
                   .attr("selected",true)
                   .text('Select'));
       },1000);
   */
    $(document).on('focusout','.txtTo', function () {
        var txtTo  =  $(this).val();
        var textFrom = $(this).parents('.group_unit').prev('.group_unit').find('.txtFrom').val();
        if(textFrom > txtTo)
        {
            $(this).next().text('* Range to must be greater than Range from.');
            $(this).val('');
        } else {
            $(this).next().text('');
        }
    });

    $('input').on('input',function(){
        var textb = $(this).val();
        if(textb.length){
            $(this).siblings('.error').html('');
        }
    });

    $('select').on('change',function(){
        var textb = $(this).val();
        if(textb.length){
            $(this).siblings('.error').html('');
        }
    });

    //Check Availability
    $(document).on('click','.check_availability',function(){
        var formData = $('#addUnitForm').serialize();
        $.ajax({
            type: 'post',
            url: '/propertyUnitAjax',
            data: {class: 'propertyDetail', action: 'checkAvailability', form: formData},
            success: function (response) {
                var res = JSON.parse(response);
                // var data = res.data;

                if (res.status == 'success' && res.code == 200) {
                    toastr.success(res.message);

                } else if (res.status == 'error' && res.code == 400) {
                    debugger
                    $('.error').html('');
                    if(res.data == 'base_rent_error')
                    {
                        toastr.warning('Base rent should not greater then Market rent.');
                    } else if(res.data == 'unit_limit_reached'){
                        toastr.warning('Unit Limit Reached');
                    } else {
                        $.each(res.data, function (key, value) {

                            $('.' + key).html('* This field is required');
                        });
                        toastr.warning(res.message);
                    }


                }
            },
        });
    });
    //End Check Availability

    $(document).on('change','.unit_building',function () {
        var building_id = $(this).val();
        $('#NonSmokingUnit').prop('readonly',false);
        $('#NonSmokingUnit').removeClass('disabled_field');
        if(building_id) {
            getBuildingDetailById(building_id);
        } else {
            $('#NonSmokingUnit option:eq(0)').attr('selected','selected');
            $('#NonSmokingUnit').removeAttr('disabled');
            $('#NonSmokingUnit').removeAttr('readonly');
        }

    });
    // console.log('url_buiding_id', url_buiding_id);
    if (url_buiding_id != '') {
        console.log('url_buiding_id', url_buiding_id);
        getBuildingDetailById(url_buiding_id);
    }
    function getBuildingDetailById(building_id){
        $.ajax({
            type: 'post',
            url: '/building-ajax',
            async: true,
            data: {
                class: 'buildingDetail',
                action: "getBuildingDetail",
                id: building_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var smoking_allowed = response.data.smoking_allowed;
                var pet_friendly = response.data.pet_friendly;

                if(smoking_allowed=='No')
                {

                    $('#NonSmokingUnit option').filter(function() {
                        return ($(this).text() == 'No'); //To select Blue
                    }).prop('selected', true);
                    $('#NonSmokingUnit').attr('readonly',true);
                    $('#NonSmokingUnit').addClass('disabled_field');
                }else {
                    $('#NonSmokingUnit').prop('readonly',false);
                    $('#NonSmokingUnit').removeClass('disabled_field');
                    $('#NonSmokingUnit').removeAttr('readonly');
                }

                if(pet_friendly=='No')
                {
                    $('#pet_options option').filter(function() {
                        return ($(this).text() == 'No'); //To select Blue
                    }).prop('selected', true);
                    $('#pet_options').attr('readonly',true);
                    $('#pet_options').addClass('disabled_field');
                } else {
                    $('#pet_options').attr('readonly',false);
                    $('#pet_options').removeClass('disabled_field');
                }

            }
        });
    }

    getBuildingNameOfUnit();
    /** Get all building name & ids */
    function getBuildingNameOfUnit(){
        $.ajax({
            url: "/propertyUnitAjax",
            method: 'post',
            data: {
                class: "PropertyUnitAjax",
                action: "getBuildingNameOfUnit",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var data = response.data;
                $('#building_name_and_id_filter').empty().append('<option value="all">Building Name & ID</option>');
                $.each(data, function (key, value) {
                    $("#building_name_and_id_filter").append('<option value="' + value['building_name_and_id'] + '" >' + value['building_name_and_id'] + '</option>');
                });
            }
        });
    }

    /**
     * jqGrid status changes
     */
    $(document).on('change','.common_ddl',function(){
        var grid = $("#property-unit-table"),f = [];
        var building_unit_status = $('#building_unit_status').val();
        var building_name_and_id_filter = $('#building_name_and_id_filter').val();
        var status = $('#jqGridStatus').val();
        f.push({field: "building_unit_status", op: "eq", data: building_unit_status},{field: "building_name_and_id", op: "eq", data: building_name_and_id_filter},{field: "status", op: "eq", data: status});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    });

    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid(1, id);
    function jqGrid(status, id) {
        var table = 'unit_details';
        var columns = ['Floor #','Unit #', 'Building Name & ID', 'Building Name', 'Building ID', 'Status','Unit Status', 'Actions'];
        var select_column = ['Edit','Flag Bank','Inspection','Add In-Touch','In-Touch History','Deactivate','Activate','Print Envelope','Delete'];
        var joins = [{table:'unit_details',column:'building_id',primary:'id',on_table:'building_detail'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['unit_details.deleted_at', 'unit_details.updated_at'];
        var extra_where = [{column: 'property_id', value: id, condition: '='}];
        var columns_options = [
            {name:'Floor #',index:'floor_no', width:90,align:"center",searchoptions: {sopt: conditions},table:table, classes: 'pointer'},
            {name:'Unit #',index:'unit_check', align:"center", width:100,searchoptions: {sopt: conditions},table:table, classes: 'pointer'},
            {name:'Building Name & ID',index:'building_name_and_id', width:80,align:"center",searchoptions: {sopt: conditions},table:table, change_type: 'combine_column_hyphen', extra_columns: ['building_name', 'building_id'], update_column: 'building_name_and_id',original_index: 'building_name'},
            {name:'Building ID',index:'building_id', width:80,hidden:true, align:"center",searchoptions: {sopt: conditions},table:'building_detail'},
            {name:'Building Name',index:'building_name', hidden: true, width:80,align:"center",searchoptions: {sopt: conditions},table:'building_detail'},
            {name:'status',index:'building_unit_status', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:buildingStatusFormatter},
            {name:'Unit Status',index:'status', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter},
            {name:'Action',index:'', title: false, width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter:actionFormatter, edittype: 'select',search:false,table:table}
        ];
        var ignore_array = [];
        jQuery("#property-unit-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Units",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}, beforeRefresh: function () {
                    var grid = $("#property-unit-table"),f = [];
                    var status = $('#jqGridStatus').val();
                    grid[0].p.search = false;
                    $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:status});
                    grid.trigger("reloadGrid",[{page:1,current:true}]);
                }
            },
            {top:10,left:200,drag:true,resize:false,}
        );
    }

    /**
     * jqGrid function to format status
     * @param status
     */
    function buildingStatusFormatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Vacant Available";
        else if(cellvalue == 2)
            return "Unrentable";
        else if(cellvalue == 4)
            return "Occupied";
        else if(cellvalue == 5)
            return "Notice Available";
        else if(cellvalue == 6)
            return ">Vacant Rented";
        else if(cellvalue == 7)
            return "Notice Rented";
        else if(cellvalue == 8)
            return "Under Make Ready";
        else
            return '';
    }
    /**
     * jqGrid function to format status
     * @param status
     */
    function statusFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Active";
        else if(cellvalue == 0)
            return "InActive";
        else
            return '';
    }

    /**
     * jqGrid function to format action field
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function actionFormatter (cellValue, options, rowObject){
        var protocol = location.port;
        if (protocol != "") {
            var webpath = location.protocol+"//"+location.hostname+":"+protocol;
        }else{
            var webpath = location.protocol+"//"+location.hostname;
        }
        if(rowObject !== undefined) {
            var status = rowObject['Unit Status'];
            var select = '';
            if(status == 1)  select = "<img src='"+webpath+"/company/images/edit-icon.png' id='editUnit'  title='Edit' alt='my image'/>  <img src='"+webpath+"/company/images/cross-icon.png' id='deactivateUnit'  title='Deactivate' alt='my image'/>";
            if(status == 0 || rowObject.Status == '')  select = "<img src='"+webpath+"/company/images/edit-icon.png' id='editUnit'  title='Edit' alt='my image'/>  <img src='"+webpath+"/company/images/tick-icon.png' id='activateUnit'  title='Activate' alt='my image'/>";
            var data = '';
            if(select != '') {
                data = select;
            }
            return data;
        }
    }

    $(document).on("click", "#activateUnit", function (e) {
        e.preventDefault();
        var id = $(this).parents("tr").attr('id');
        console.log(id);
        bootbox.confirm("Do you want to activate this record?", function (result) {
            if (result == true) {
                updateUnitStatus(id, 'activate');
            }
        });
    });

    $(document).on("click", "#deactivateUnit", function (e) {
        e.preventDefault();
        var id = $(this).parents("tr").attr('id');
        console.log(id);
        bootbox.confirm("Do you want to deactivate this record?", function (result) {
            if (result == true) {
                updateUnitStatus(id, 'deactivate');
            }
        });
    });

    $(document).on("click", "#editUnit", function (e) {
        e.preventDefault();
        var id = $(this).parents("tr").attr('id');
        window.location.href = '/Unit/EditUnit?id='+id;
    });

    function updateUnitStatus(id, status){
        var updatestatus = (status == 'activate') ? '1' : '0';
        $.ajax({
            url: "/propertyUnitAjax",
            method: 'post',
            data: {
                class: "PropertyUnitAjax",
                action: "updateStatus",
                'id': id,
                'status': updatestatus,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#property-unit-table').trigger('reloadGrid');
                    onTop(true);
                } else if(response.status == 'error' && response.code == 503) {
                    toastr.error(response.message);
                } else {
                    toastr.warning('Record not updated due to technical issue.');
                }
            }
        });
    }
    /**
     *change function to perform various actions(edit ,activate,deactivate)
     * @param status
     */
    jQuery(document).on('change','.select_options',function () {
        var select_options = $(this).val();
        var data_id = $(this).attr('data_id');
        if(select_options == 'Edit')
        {
            // window.location.href = '/MasterData/AddPropertyType';

        }else if (select_options == 'Deactivate' || select_options == 'DEACTIVATE' || select_options == 'Activate' || select_options == 'ACTIVATE') {
            select_options = select_options.toLowerCase();
            bootbox.confirm({
                message: "Do you want to " + select_options + " this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        var status = select_options == 'activate' ? '1' : '0';
                        $.ajax({
                            url: "/propertyUnitAjax",
                            method: 'post',
                            data: {
                                class: "PropertyUnitAjax",
                                action: "updateStatus",
                                'id': data_id,
                                'status': status,
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                    $('#property-unit-table').trigger('reloadGrid');
                                    onTop(true);
                                } else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                }
            });
        }
        $('.select_options').prop('selectedIndex',0);
    });


    function currencyFormat(num) {
        num = parseFloat(num);
        return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    (function($){
        $.unserialize = function(serializedString){
            var str = decodeURI(serializedString);
            var pairs = str.split('&');
            var obj = {}, p, idx, val;
            for (var i=0, n=pairs.length; i < n; i++) {
                p = pairs[i].split('=');
                idx = p[0];

                if (idx.indexOf("[]") == (idx.length - 2)) {
                    // Eh um vetor
                    var ind = idx.substring(0, idx.length-2)
                    if (obj[ind] === undefined) {
                        obj[ind] = [];
                    }
                    obj[ind].push(p[1]);
                }
                else {
                    obj[idx] = p[1];
                }
            }
            return obj;
        };
    })(jQuery);


    function convertSerializeDatatoArrayPhotos(){
        var newData = [];
        $(".photoVideoInput").each(function( index ) {
            var name = $(this).val();
            var size = $(this).attr('data_id');
            var id = $("#checkbox"+size);
            var checked = id.prop('checked')?'1':'0';
            newData.push({'name':name,'size':size,'siteMain':checked});
        });
        return newData;
    }

    $(document).on("click",".add-popup .clear_single_field",function(){
        var modal_id = $(this).parents('.add-popup').attr('id');
        resetFormClear('#'+modal_id,[], 'div',false);
        $('#'+modal_id+ ' span.error').text('');
    });

    $(document).on("click","#clearAddBuildingForm",function(){
        bootbox.confirm("Do you want to Clear this form?", function (result) {
            if (result == true) {
                getPropertyDetails(id);
                $("#add_building_form #building_id").val(Math.random().toString(36).slice(2).substr(0, 6).toUpperCase());
            }
        });
    });

    $(document).on("click","#cancelAddBuildingForm",function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $('#AddNewBuildingModal').modal('hide');
            }
        });
    });

    $(document).on("click","#cancelAddUnitForm",function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = '/Unit/UnitModule?id='+id;
            }
        });
    });

    $(document).on("click","#clearAddUnitForm",function(){
        bootbox.confirm("Do you want to Clear this form?", function (result) {
            if (result == true) {
                location.reload();
            }
        });
    });

    $(document).on("click","#clearCustomFieldModal",function(){
        resetFormClear('#custom_field',['data_type'], 'form',false);
    });

    $(document).on("click","#clearCustomFieldModal",function(){
        resetFormClear('#custom_field',['data_type'], 'form',false);
    });

    $(document).on("click","#clearAddKeyForm",function(){
        bootbox.confirm("Do you want to Clear this form?", function (result) {
            if (result == true) {
                resetFormClear('#addNewKeyDiv',[], 'div',false);
                $('#addNewKeyDiv span.error').text('');
            }
        });
    });
  $(document).on("click","#clearAddTrackKey",function(){
        bootbox.confirm("Do you want to Clear this form?", function (result) {
            if (result == true) {
                resetFormClear('#addNewTrackKeyDiv',[], 'div',false);
                $('#addNewTrackKeyDiv span.error').text('');
            }
        });
    });

    $(document).on('keypress','.money',function(e){
        if(this.value.length==10) return false;
    });

    $(document).on('focusout','.money',function(){
        if($(this).val() != '' && $(this).val().indexOf(".") == -1) {
            var bef = $(this).val().replace(/,/g, '');
            var value = numberWithCommas(bef) + '.00';
            $(this).val(value);
        } else {
            var bef = $(this).val().replace(/,/g, '');
            $(this).val(numberWithCommas(bef));
        }
    });


});




