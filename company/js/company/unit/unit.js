$(document).ready(function(){
    var base_url = window.location.origin;


    $("#Buildings-table-view").keypress(function(e){
        var keyCode = e.which;
        /*
          8 - (backspace)
          32 - (space)
          48-57 - (0-9)Numbers
        */
        // Not allow special
        if ( !( (keyCode >= 48 && keyCode <= 57)
            ||(keyCode >= 65 && keyCode <= 90)
            || (keyCode >= 97 && keyCode <= 122) )
            && keyCode != 8 && keyCode != 32) {
            e.preventDefault();
        }
    });


    var status =  localStorage.getItem("active_inactive_status");
    console.log(status);
    if(status !== undefined) {
        if ($("#Buildings-table-view")[0].grid) {
            $('#Buildings-table-view').jqGrid('GridUnload');
        }
        //intializing jqGrid
        if(status == 'all'){
            jqGrid('All');
        }  else {
            jqGrid(status);
        }
        $('#jqGridStatus option[value='+status+']').attr("selected", "selected");

    }else{
        if ($("#Buildings-table-view")[0].grid) {
            $('#Buildings-table-view').jqGrid('GridUnload');
        }
        jqGrid('All');
    }


    /** jqGrid status */
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        $('#Buildings-table-view').jqGrid('GridUnload');
        changeGridStatus(selected);
        jqGrid(selected, true);
        $("#Buildings-table-view").hide(500);
    });

    /**
     * jqGrid Initialization function
     * @param status
     */
    function jqGrid(status, deleted_at) {
        var table = 'company_property_unit';
        var columns = ['Floor#','Unit#','Building Name & Id','Status','# of Bedrooms','# of Bathrooms','Unit Status','Action'];
        var select_column = [];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var pagination;
        var extra_columns = ['company_property_unit.deleted_at'];
        var columns_options = [
            { name:'Floor#',index:'floor', width:80, align:"center", searchoptions: {sopt: conditions},table:table},
            { name:'Unit#',index:'unit', width:80,align:"center", searchoptions: {sopt: conditions},table:table},
            { name:'Building Name & Id',index:'buildings_name', width:80,align:"center", searchoptions: {sopt: conditions},table:table},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            { name:'# of Bedrooms',index:'bedrooms', width:80, align:"center", searchoptions: {sopt: conditions},table:table},
            { name:'# of Bathrooms',index:'bathroom', width:80,align:"center", searchoptions: {sopt: conditions},table:table},
            { name:'Unit Status',index:'unit_status', width:80,align:"center", searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'select', title: false, width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFormatter}
        ];
        var ignore_array = [];
        jQuery("#Buildings-table-view").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_property_unit",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Units",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:200,drag:true,resize:false} // search options
        );
    }


    function actionFormatter (cellValue, options, rowObject){
        if(rowObject !== undefined) {
            var editable = $(cellValue).attr('editable');
            var select = '';
            if(rowObject.Status == 1)  select = ['Edit','Flag Bank','Inspection','Add In-Touch','Deactivate','Print Envelope','Delete'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['Edit','Flag Bank','Inspection','Add In-Touch','activate','Print Envelope','Delete'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    if(editable == '0' && (val == 'delete' || val == 'Delete')){
                        return true;
                    }
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }


    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject) {
        if (cellValue == 1)
            return "Active";
        else if (cellValue == '0')
            return "InActive";
        else
            return '';
    }

    });