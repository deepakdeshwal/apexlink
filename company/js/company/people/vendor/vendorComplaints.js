$(document).on('click','#newComplaint',function(){
   $('#vendorFormDiv').show();
    $('#vendorComplaints').trigger('reset');
   $('#complaint_id_vendor').val(getRandomNumber(6));
});

$(document).on('change','#complaint_type_options',function(){
   if($(this).val() == '18'){
       $('#other_notes_div').show();
   } else {
       $('#other_notes_div').hide();
   }
});

$("#complaint_date").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());

$(document).on('click','#complaint_cancel',function(){
    $('#vendorFormDiv').hide();
    $('#vendorComplaints').trigger('reset');
});