//fetch company_bills details
fetchBillDetails(bill_edit_id);
function fetchBillDetails(bill_edit_id) {
    $.ajax({
        type: 'post',
        url: '/newBill-ajax',
        data: {
            class: 'billsAjax',
            action: 'getBillDetail',
            id: bill_edit_id},
        success: function (response) {
            var res = JSON.parse(response);
            console.log(res);
            if(res.code == 200){
                $('#EditVendorAddress').show();
                $('#EditVendorAddress').attr('data_id',res.data.vendor_id);
                $('#vendorAddress').val(res.data.address);
                $('#_easyui_textbox_input1').val(res.data.name);
                $('#vendor_id').val(res.data.vendor_id);
                //$('#saveNewBill [name="change_to"]').val(res.data.change_to);
                if(res.data.change_to == '1'){
                    $('#change_to_div').show();
                    $('#change_to_tenant_radio').prop('checked',true);
                    $('#tenant_div_box').show();
                    $('#other_div_box').hide();
                    $('#tenant_id').val(res.data.tenant_id);
                    $('#change_to_other_tenant').val(res.data.tenant_name)
                    showTenantGrid();
                } else if(res.data.change_to == '2'){
                    $('#change_to_div').show();
                    $('#change_to_other_radio').prop('checked',true);
                    $('#tenant_div_box').hide();
                    $('#change_to_other').show();
                    $('#saveNewBill [name="change_to_other"]').val(res.data.change_to_other);
                }
                $('#saveNewBill [name="invoice_number"]').val(res.data.invoice_number);
                $('#saveNewBill [name="term"]').val(res.data.term);
                $('#saveNewBill [name="memo"]').val(res.data.memo);
                $('#saveNewBill [name="bill_date"]').val(res.data.bill_date);
                $('#saveNewBill [name="amount"]').val(res.data.amount);
                $('#saveNewBill [name="refrence_number"]').val(res.data.refrence_number);
                $('#saveNewBill [name="due_date"]').val(res.data.due_date);
                 $('#saveNewBill [name="work_order"]').val(res.data.work_order).multiselect('rebuild');
                setTimeout(function(){
                    $('#saveNewBill [name="portfolio_id"]').val(res.data.portfolio_id);
                    fetchAllProperty(res.data.portfolio_id,res.data.property_id)
                }, 1500);
            }
        },
    });
}

//fetch company item details
fetchBillItemDetails(bill_edit_id);
function fetchBillItemDetails(bill_edit_id) {
    $.ajax({
        type: 'post',
        url: '/newBill-ajax',
        data: {
            class: 'billsAjax',
            action: 'getBillItems',
            id:bill_edit_id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200){
                $('#appendTable').html(res.data);
            }
        },
    });
}

//fetch company item details
fetchBillNotes(bill_edit_id);
function fetchBillNotes(bill_edit_id) {
    $.ajax({
        type: 'post',
        url: '/newBill-ajax',
        data: {
            class: 'billsAjax',
            action: 'getBillNotes',
            id:bill_edit_id},
        success: function (response) {
            var res = JSON.parse(response);
            console.log('notes',res);
            if(res.code == 200){
                var html = '';
                $.each(res.data,function(key,value){
                    html += '<div class="form-outer vendor-notes-clone-divclass" id="vendor-notes-clone-div">';
                    html += '<div class="col-sm-4">';
                    html += '<textarea class="notes capital" placeholder="" name="note[]" >'+value.note+'</textarea>';
                    if(key > 0) {
                        html += '<a class="add-icon-textarea vendor-notes-plus" style="display:none;" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>';
                        html += '<a class="add-icon-textarea vendor-notes-minus" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                    } else if(res.data.length > 1) {
                        html += '<a class="add-icon-textarea vendor-notes-plus" style="display:none;" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>';
                        html += '<a class="add-icon-textarea vendor-notes-minus" style="display:none;" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                    } else {
                        html += '<a class="add-icon-textarea vendor-notes-plus"  href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>';
                        html += '<a class="add-icon-textarea vendor-notes-minus" style="display:none;" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                    }
                    html += '</div>';
                    html += '</div>';
                });
                if(html != '') {
                    $('#note_div').html(html);
                }
            }
        },
    });
}

/**
 * jqGrid Intialization function
 * @param status
 */
jqGridFileLibrary('All');
function jqGridFileLibrary(status) {
    var table = 'company_bill_files';
    var columns = ['Name','Path','File','Action'];
    var select_column = [];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'bill_id',value:bill_edit_id,condition:'='}];
    var columns_options = [
        {name:'Name',index:'name',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Path',index:'path',hidden:true,width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'extension',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
        {name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionLibraryFmatter,title:false}
    ];
    var ignore_array = [];
    jQuery("#billLibrary_table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of File Library",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}

/**
 * jqGrid function to format action column
 * @param status
 */

function actionLibraryFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';
        select = ['<span><i class="fa fa-trash delete_file_library pointer" data_id="' + rowObject.id + '" style="font-size:24px"></i></span>'];
        var data = '';
        if(select != '') {
            $.each(select, function (key, val) {
                data += val
            });
        }
        return data;
    }
}

function imageFormatter(cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var src = '';
        if (rowObject.Preview == 'xlsx') {
            src = upload_url + 'company/images/excel.png';
        } else if (rowObject.Preview == 'pdf') {
            src = upload_url + 'company/images/pdf.png';
        } else if (rowObject.Preview == 'docx') {
            src = upload_url + 'company/images/word_doc_icon.jpg';
        }else if (rowObject.Preview == 'txt') {
            src = upload_url + 'company/images/notepad.jpg';
        }
        return '<span class="downloadImage" data_name="'+rowObject.Name+'" data_href="'+document_root+'company/'+rowObject.Path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></span>';
    }
}

/**
 * Delete file library data
 */
$(document).on('click','.delete_file_library',function(){
     var data_id = $(this).attr('data_id');
    $.ajax({
        type: 'post',
        url: '/newBill-ajax',
        data: {
            class: 'billsAjax',
            action: 'deleteFile',
            id: data_id
        },
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200){
                toastr.success(res.message);
                $('#billLibrary_table').trigger('reloadGrid');
            }
        },
    });
});

$(document).on('click','.downloadImage',function(){
    downloadURI($(this).attr('data_href'), $(this).attr('data_name'))
});

function downloadURI(uri, name)
{
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    link.click();
}

$(document).on('click','#cancelEditBill',function(){
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            window.location.href = '/Accounting/paybills';
        }
    });
});


