$(document).ready(function () {

    fetchAllWorkOrders();
    function fetchAllWorkOrders() {
        $.ajax({
            type: 'post',
            url: '/global-get-module-ajax',
            data: {
                class: 'GlobalGetModule',
                action: 'getAllWorkOrderStatus'},
            success: function (response) {
                var res = JSON.parse(response);
                if(res.code == 200) {
                    var html = '<option value="all">Select</option>';
                    $.each(res.data,function(key,value){
                        html += '<option value="'+value.id+'">'+value.work_order_status+'</option>';
                    });
                    $('#ddlWorkOrderListingFrequency').html(html);
                }
            },
        });
    }

    $(document).on('click', '#ddlWorkOrderListingFrequency', function () {
        var grid = $("#workOrder-table"),f = [];
        var status = $(this).val();
        f.push({field: "work_order.status_id", op: "eq", data: status});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    });


    getcountrycode();
//    flag js
    $('.vendorTab').on('click',function(){
        var table_id = $(this).attr('data_tab');
        if(table_id !== undefined && table_id != '') {
            var $grid = $("#" + table_id).setGridWidth($(window).width()-70);
            //  $("#jqgrid")new_flag
            //  newWidth = $grid.closest(".ui-jqgrid").parent().width();
            // $grid.jqGrid("setGridWidth", newWidth, true);
        }
    });

    var idd=getParameterByName('id');
    function getParameterByName(name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }





    notes();
    //on flag cancel
    // $(document).on('click', '#flagCancel', function () {
    //     bootbox.confirm({
    //         message: "Do you want to cancel this action now?",
    //         buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
    //         callback: function (result) {
    //             if (result == true) {
    //                 $('#flag_id').val('');
    //                 $('#flagFormDiv').hide(500);
    //             }
    //         }
    //     });
    // });

    //adding custom fields
    $('#flagForm').on('submit', function (event) {
        event.preventDefault();
        //checking custom field validation
        if ($('#flagForm').valid()) {
            var formData = $('#flagForm').serializeArray();
            var vendor_id = $(".vendor_id").val();
            $.ajax({
                type: 'post',
                url: '/vendor-view-ajax',
                data: {form: formData, class: 'ViewVendor', action: 'create_flag', vendor_id: vendor_id},
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.code == 200) {
                        getFlagCount();
                        $('#flagFormDiv').hide(500);
                        $('#vendorFlag-table').trigger('reloadGrid');
                        localStorage.setItem("Message",response.message);
                        localStorage.setItem('rowcolor', 'rowColor');
                        window.location.href = '/Vendor/Vendor';
                        toastr.success(response.message);
                        setTimeout(function () {
                            jQuery('#vendorFlag-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                            jQuery('#vendorFlag-table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                        }, 1000);
                    } else if (response.code == 200) {
                        toastr.error(response.message);
                    }


                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    });

    jqGridFlag('All');
    /**
     * jqGridFlag Initialization function
     * @param status
     */
    function jqGridFlag(status) {
        var table = 'flags';
        var columns = ['Date', 'Flag Name', 'Phone Number', 'Flag Reason', 'Completed', 'Note', 'Action'];
        var select_column = ['Edit', 'Delete', 'Completed'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [{column: 'object_id', value: $(".vendor_id").val(), condition: '='}, {column: 'object_type', value: 'vendor', condition: '='}];
        var extra_columns = ['flags.deleted_at'];
        var columns_options = [
            {name: 'Date', index: 'date', align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'date'},
            {name: 'Flag Name', index: 'flag_name', searchoptions: {sopt: conditions}, table: table},
            {name: 'Phone Number', index: 'flag_phone_number', align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Flag Reason', index: 'flag_reason', width: 180, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Completed', index: 'completed', width: 180, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: completedFormatter},
            {name: 'Note', index: 'flag_note', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Action', index: 'select', width: 200, title: false, align: "right", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table, formatter: actionFmatter}
        ];
        var ignore_array = [];

        jQuery("#vendorFlag-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_where: extra_where,
                extra_columns: extra_columns,
                deleted_at: 'true'
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Flags",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
        );
    }

    /**
     * function to change completed format
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function completedFormatter(cellValue, options, rowObject) {
        if (cellValue == 1)
            return "True";
        else if (cellValue == 0)
            return "False";
        else
            return '';
    }

    /**
     * function to change action column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function actionFmatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var select = '';
            if (rowObject.Completed == 1)
                select = ['Edit', 'Delete'];
            if (rowObject.Completed == 0)
                select = ['Edit', 'Delete', 'Completed'];
            var data = '';
            if (select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }

    }

    /**  List Action Functions  */
    $(document).on('change', '#vendorFlag-table .select_options', function () {
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index();
        if (opt == 'Edit' || opt == 'EDIT') {
            var validator = $("#flagForm").validate();
            validator.resetForm();
            $('#vendorFlag-table').find('.green_row_left, .green_row_right').each(function () {
                $(this).removeClass("green_row_left green_row_right");
            });
            jQuery('#vendorFlag-table').find('tr:eq(' + row_num + ')').find('td:eq(0)').addClass("green_row_left");
            jQuery('#vendorFlag-table').find('tr:eq(' + row_num + ')').find('td').last().addClass("green_row_right");
            $.ajax({
                type: 'post',
                url: '/vendor-view-ajax',
                data: {id: id, class: 'ViewVendor', action: 'get_flags'},
                success: function (response) {
                    $("#flagFormDiv").show(500);
                    $('#flagSaveBtnId').text('Update');
                    var data = $.parseJSON(response);
                    if (data.code == 200) {
                        $("#flag_id").val(data.data.id);
                        $("#flag_note").val(data.data.flag_note);
                        $("#flag_flag_date").val(data.data.date);
                        $("#flag_flag_by").val(data.data.flag_by);
                        $("#flag_country_code").val(data.data.country_code);
                        $("#flag_phone_number").val(data.data.flag_phone_number);
                        $("#flag_flag_reason").val(data.data.flag_reason);
                        $("#completed").val(data.data.completed);
                        $("#flag_flag_name").val(data.data.flag_name);
                        vendorFlagData = $('#flagForm').serializeArray();
                        $('#resetFlagForm').show();
                        $('#clearFlagForm').hide();

                    } else if (data.code == 500) {
                        toastr.error(data.message);
                    } else {
                        toastr.error(data.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
            setTimeout(function () {
                jQuery('#vendorFlag-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                jQuery('#vendorFlag-table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
            }, 1000);
            //$("#portfolio-table").trigger('reloadGrid');
        } else if (opt == 'Delete' || opt == 'DELETE') {
            opt = opt.toLowerCase();
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/vendor-view-ajax',
                            data: {id: id, class: 'ViewVendor', action: 'deleteFlag'},
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    getFlagCount();
                                    toastr.success(response.message);
                                    var returnRes = update_users_flag('users',idd);
                                    if (returnRes.status == "success" && returnRes.code == 200){
                                        localStorage.setItem("Message", response.message);
                                        localStorage.setItem('rowcolorTenant', 'rowColor');
                                        setTimeout(function () {
                                            window.location.href = '/Vendor/Vendor';
                                        },1000);
                                    }
                                    setTimeout(function () {
                                        jQuery('#vendorFlag-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                                        jQuery('#vendorFlag-table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                                    }, 1000);
                                    $('#vendorFlag-table').trigger('reloadGrid');
                                } else if (response.code == 500) {
                                    toastr.warning(response.message);
                                } else {
                                    toastr.error(response.message);
                                }

                            }
                        });
                    }

                }
            });
        } else if (opt == 'Completed' || opt == 'COMPLETED') {
            opt = opt.toLowerCase();
            bootbox.confirm({
                message: "Do you want to complete this flag ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/flag-ajax',
                            data: {id: id, class: 'Flag', action: 'flagCompleted'},
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    getFlagCount();
                                    var returnRes = update_users_flag('users',idd);
                                    if (returnRes.status == "success" && returnRes.code == 200){
                                        localStorage.setItem("Message", response.message);
                                        localStorage.setItem('rowcolorTenant', 'rowColor');
                                        setTimeout(function () {
                                            window.location.href = '/Vendor/Vendor';
                                        },1000);
                                    }
                                    setTimeout(function () {
                                        jQuery('#vendorFlag-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                                        jQuery('#vendorFlag-table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                                    }, 1000);
                                    $('#vendorFlag-table').trigger('reloadGrid');
                                } else if (response.code == 500) {
                                    toastr.warning(response.message);
                                } else {
                                    toastr.error(response.message);
                                }

                            }
                        });
                    }

                }
            });
        }
        $('.select_options').prop('selectedIndex', 0);
    });

    /**
     * jquery to redirect for edits
     */
    $(document).on('click', '.edit_redirection', function () {
        var redirection_data = $(this).attr('redirection_data');
        localStorage.setItem("AccordionHref",'#'+redirection_data);
        window.location.href = window.location.origin+'/Unit/EditUnit?id='+id;
    });
    /**
     * Get Parameters by id
     * @param status
     */


    function notes(status) {
        var id = $(".vendor_id").val();
        var table = 'tenant_chargenote';
        var columns = ['Date', 'Time', 'Notes'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        // var extra_columns = ['tenant_chargenote.deleted_at'];
        var extra_columns = [];
        var extra_where = [{column: 'user_id', value: id, condition: '='}];
        var columns_options = [
            {name: 'Date', index: 'updated_at', width: 420, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Time', index: 'updated_at', width: 420, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'updated_at_time'},
            {name: 'Notes', index: 'note', width: 420, align: "center", searchoptions: {sopt: conditions}, table: table, },
        ];
        var ignore_array = [];
        jQuery("#notes-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_where: extra_where,
                deleted_at: 'no'
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Notes/History",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}

            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: false, refresh: false, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 400, drag: true, resize: false}
        );
    }
    fileLibrary();
    function fileLibrary(status) {

        var id = $(".vendor_id").val();

        var table = 'tenant_file_library';
        var columns = ['Name', 'Preview'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        // var extra_columns = []
        var extra_where = [{column: 'user_id', value: id, condition: '='}];
        var columns_options = [
            {name: 'Name', index: 'name', width: 600, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Preview', index: 'extension', width: 600, align: "center", searchoptions: {sopt: conditions}, search: false, table: table, formatter: imageFormatter},
        ];
        var ignore_array = [];
        jQuery("#file-library").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                // extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Vendor Files",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {},
            {},
            {},
            {top: 10, left: 400, drag: true, resize: false}
        );
    }


    /**
     * jqGrid function to format file extension
     * @param status
     */
    function imageFormatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var src = '';
            if (rowObject.Preview == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
            } else if (rowObject.Preview == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
            } else if (rowObject.Preview == 'docx') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
            } else if (rowObject.Preview == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
            }
            return '<img class="img-upload-tab" width=30 height=30 src="' + src + '">';
        }
    }

    /**
     * jqGrid function to format action column of file library of unit
     * @param cellvalue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function actionLibraryFmatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var select = '';
            select = ['<span><i class="fa fa-trash delete_file_library pointer" data_id="' + rowObject.id + '" style="font-size:24px"></i></span>'];
            var data = '';
            if (select != '') {
                $.each(select, function (key, val) {
                    data += val
                });
            }
            return data;
        }
    }


    VendorComplaints();
    function VendorComplaints(status) {
        var id=$(".vendor_id").val();
        var table = 'complaints';
        var columns = ['<input type="checkbox" class="Vendor_checkbox" id="select_all_complaint_checkbox">', 'Complaint Id', 'complaint', 'Complaint Type', 'Notes', 'Date', 'Other','Action'];
        var select_column = ['Edit', 'Delete'];
        var joins = [{table: 'complaints', column: 'complaint_type_id', primary: 'id', on_table: 'complaint_types'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        // var extra_columns = ['complaints.status'];
        var extra_where = [{column: 'object_id', value: id, condition: '='}];
        var columns_options = [
            {name: 'Id', index: 'id', width: 50, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatter},
            {name: 'Complaint Id', index: 'complaint_id', width: 170, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'complaint', index: 'complaint_by_about', width: 170, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Complaint Type', index: 'complaint_type', width: 170, align: "center", searchoptions: {sopt: conditions}, table: "complaint_types"},
            {name: 'Notes', index: 'complaint_note', width: 170, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Date', index: 'complaint_date', width: 170, searchoptions: {sopt: conditions}, table: table, change_type: 'date'},
            {name: 'Other', index: 'other_notes', width: 250, align: "center", searchoptions: {sopt: conditions}, search: false, table: table},
            {name: 'Action', index: 'select', title: false, width: 170, align: "right", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table, formatter:actionsFmatter}
        ];
        var ignore_array = [];
        jQuery("#vendor-complaints").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                // extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'complaints.updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Vendor Complaints",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
        );
    }
    function actionCheckboxFmatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {

            var data = '';
            var data = '<input type="checkbox" name="complaint_checkbox[]" class="complaint_checkbox" id="complaint_checkbox_' + rowObject.id + '" data_id="' + rowObject.id + '"/>';
            return data;
        }
    }
    function actionsFmatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var editable = $(cellvalue).attr('editable');
            var select = '';
            select = ['Edit', 'Delete'];
            var data = '';

            if (select != '') {

                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    if (editable == '0' && (val == 'delete' || val == 'Delete')) {
                        return true;
                    }
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    $(document).on("change","#select_all_complaint_checkbox",function(e){
        var inputs = $('#vendor-complaints input[type=checkbox]');
        if(e.originalEvent === undefined) {
            var allChecked = true;
            inputs.each(function(){
                allChecked = allChecked && this.checked;
            });
            this.checked = allChecked;
        } else {
            inputs.prop('checked', this.checked);
        }
    });

    $(document).on('change', '#vendor-complaints .complaint_checkbox input[type=checkbox]', function(){
        $('#select_all_complaint_checkbox').trigger('change');
    });



    
    $(document).on('change','#grid_status',function(){
        var grid = $("#vendor-complaints"), f = [];
        var value = $(this).val();
        if (value != 'all') {
            f.push({field: "complaints.complaint_by_about", op: "eq", data: value});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData, {filters: JSON.stringify(f)});
            grid.trigger("reloadGrid", [{page: 1, current: true}]);
        } else {
            grid[0].p.search = false;
            $.extend(grid[0].p.postData, {filters: ""});
            grid.trigger("reloadGrid", [{page: 1, current: true}]);
        }
    });


    $(document).on("click", "#new_complaint_button", function () {
        $("#new_complaint").show(500)
        $("#complaint_save").html('Save')
        $("#new_complaintform").trigger("reset");
        $('#edit_complaint_id').val('');
        $('#complaint_id').prop('readonly', false);
        $("#complaint_id").val(Math.random().toString(36).slice(2).substr(0, 6).toUpperCase());
        /*date picker for complaint date */
        $("#complaint_date").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
        vendorComplaintData = $('#new_complaintform').serializeArray();
        $('#clearNewComplaintform').show();
        $('#resetNewComplaintform').hide();
    });

    $(document).on("click", "#complaintype", function () {
        $("#ComplaintTypePopup").show();
        $('#NewvendorComplaintSave').val('Save');
        $('.cancelPopup').val('Cancel');
    });

    $(document).on('click', '.cancelPopup', function () {
        $("#ComplaintTypePopup").hide();
    });

    $(document).on('click', '#complaint_cancel', function () {
        $("#new_complaint").hide();
    });

    $(document).on('click', '#NewvendorComplaintSave', function (e) {
        e.preventDefault();
        var vendor_id=$(".vendor_id").val();
        var formData = $('#ComplaintTypePopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/vendor-view-ajax',
            data: {form: formData,
                vendor_id:vendor_id,
                class: 'ViewVendor',
                action: 'addComplaintType'},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#ComplaintTypePopup').hide(500);
                    toastr.success(response.message);
                    fetchAllcomplaintType(response.lastid);
                } else if (response.status == 'error' && response.code == 500) {
                    toastr.warning(response.message);
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    fetchAllcomplaintType();
    function fetchAllcomplaintType(id) {
        $.ajax({
            type: 'post',
            url: '/vendor-view-ajax',
            data: {
                class: 'ViewVendor',
                action: 'fetchAllcomplaintType'},
            success: function (response) {
                var res = JSON.parse(response);
                $('#complaint_type_options').html(res.data);
                if (id != false) {
                    $('#complaint_type_options').val(id);
                }
            },
        });
    }

    /*to show other notes text box for other notes */
    $(document).on("change", "#complaint_type_options", function () {
        var complaint_type = $("#complaint_type_options option:selected").text();
        if (complaint_type == "Other") {
            $("#other_notes_div").show();
        } else {
            $("#other_notes_div").hide();
        }
    });

    $(document).on('click', '#complaint_cancel', function () {
        $("#new_complaint").hide(500);
    });

    $(document).on('click', '#complaint_save', function (e) {

        e.preventDefault();
        var vendor_id=$(".vendor_id").val();
        var formData = $('#new_complaintform').serializeArray();
        var complaint_about = $('.clsVendorComplaints:checked').val();
        console.log('complaint_about',complaint_about);


        $.ajax({
            type: 'post',
            url: '/vendor-view-ajax',
            data: {form: formData,
                vendor_id:vendor_id,
                class: 'ViewVendor',
                action: 'addNewComplaint',
                complaint_about:complaint_about
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#new_complaint').hide(500);
                    $("#vendor-complaints").trigger('reloadGrid');
                    toastr.success(response.message);
                    setTimeout(function () {
                        jQuery('#vendor-complaints').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('#vendor-complaints').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                    },500);

                } else if (response.status == 'error' && response.code == 500) {
                    toastr.error(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });



    jQuery(document).on('change', '#vendor-complaints .select_options', function () {
        var select_options = $(this).val();
        var data_id = $(this).attr('data_id');
        console.log(data_id);
        if (select_options == 'Edit')
        {
            $('#new_complaint :input').val('');
            $('#new_complaint').show(500);
            $('#edit_complaint_id').val(data_id);
            $('#complaint_save').html('Update');
            $('#complaint_id').prop('readonly', 'readonly');

            $.ajax({
                type: 'post',
                url: '/vendor-view-ajax',
                data: {class: 'ViewVendor', action: 'getComplaintDetail', id: data_id},
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        $.each(response.data.data, function (key, value) {
                            $('#' + key + ':input').val(value);
                            if (key == 'complaint_type_id' && value != '') {

                                $('#complaint_type_options').val(value);



                            }
                        });
                        edit_date_time(response.data.data.updated_at);
                        vendorComplaintData = $('#new_complaintform').serializeArray();
                        $('#resetNewComplaintform').show();
                        $('#clearNewComplaintform').hide();
                    } else if (response.status == 'error' && response.code == 503) {
                        toastr.error(response.message);
                    } else {
                        toastr.warning('Record not updated due to technical issue.');
                    }
                }
            });
            // window.location.href = '/MasterData/AddPropertyType';

        }  else if (select_options == 'Delete') {
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/vendor-view-ajax',
                            data: {class: 'ViewVendor', action: 'deleteComplaint', id: data_id},
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success('Complaint deleted successfully.');
                                } else if (response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    $("#vendor-complaints").trigger('reloadGrid');
                }
            });
        }
        $('.select_options').prop('selectedIndex', 0);
    });



    /*jquery to open modal for complaint box to print */



    function SendMail(data, mail_length, vendor_email){
        var vendor_id=$(".vendor_id").val();
        $.ajax({
            type: 'post',
            url:'/vendor-view-ajax',
            data: {
                class: 'ViewVendor',
                action: 'SendComplaintMail',
                data_html: data,
                'complaint_ids': favorite,
                'vendor_id' : vendor_id,
                mail_length : mail_length,
                vendor_email : vendor_email,
            },
            success : function(response){
                var response =  JSON.parse(response);
                if(response.status == 'success' && response.code == 200) {
                    toastr.success('Mail sent successfully.');
                    $("#print_complaint").modal('hide');
                }else {
                    toastr.warning('Mail not sent due to technical issue.');
                }
            }
        });
    }


    // function sendComplaintsEmail(elem)
    // {
    //     var mail_length = $('#print_complaint #modal-body-complaints>table').length;
    //     var vendor_email = $('#email').val();
    //     SendMail($(elem).html(), mail_length, vendor_email);
    // }

    /*function to print element by id */
    function sendComplaintsEmail(elem)
    {
        var mail_length = $('#print_complaint #modal-body-complaints>table').length;

        var owner_email = $('#email').val();
        SendMail($(elem).html(), mail_length, owner_email);
    }



    function response1(obj) {
        var objres = JSON.parse(obj);
        if (objres.status == 'success') {
            $('.zipcity').val(objres.city);
            $('.zipstate').val(objres.state);
            $('.zipcountry').val(objres.country);

        }
    }


    viewData();
    function viewData() {
        var vendor_id=$(".vendor_id").val();

        $.ajax({
            type: 'post',
            url: '/vendor-view-ajax',
            data: {
                class: "ViewVendor",
                action: "getVendorDetail",
                vendor_id:vendor_id
            },
            success: function (response) {

                var res = $.parseJSON(response);
                if (res.status == "success") {
                    var vendor_detail_data = res.dataVendors.vendor_detail;
                    var vendor_detail_data1 = res.dataVendor.vendor_additional_detail;
                    var vendor_type_idd = res.dataVendor.vendor_additional_detail.vendor_typess;
                    console.log('klsdknsdllf',vendor_type_idd);
                    $('#vendor_type_options').val(vendor_type_idd);
                    var fullname = res.fullname;
                    if(vendor_detail_data.email != '') {
                        $('.autoVendoremail').val(vendor_detail_data.email).attr('readonly', true);

                    }

                    $(".viewEmergency").html(res.emergencyInfo);
                    $('.vendor_name').text(fullname);
                    $('.owner_note_for_Phone').text('N/A');
                    $('.owner_Email').text('N/A');
                    $('.vendor_id').text(vendor_detail_data1.vendor_random_id);
                    $('.Phone').text(vendor_detail_data.phone);
                    $('.middle_name').text(vendor_detail_data.middle_name);

                    $('.salutation').text(vendor_detail_data.salutation);


                    if(vendor_detail_data.salutation =="Dr." || vendor_detail_data.salutation =="Mr." || vendor_detail_data.salutation =="Mr. & Mrs." || vendor_detail_data.salutation =="Sir"){

                        $('label[for="maiden_name"]').hide();
                    }
                    else{

                        $('label[for="maiden_name"]').show();

                        $(".maiden_name").text(vendor_detail_data.maiden_name);
                    }

                    $('.Gender').text(vendor_detail_data.gender);
                    $('.company_name').text(vendor_detail_data.company_name);

                    $('.Hobbies').text(vendor_detail_data.hobbies);
                    $('.vendor_marital_status').text(vendor_detail_data.marital_status);
                    $('.vendor_vetran_status').text(vendor_detail_data.veteran_name);
                    $('.vendor_dob').text(res.vendor_detaildob);
                    $('.First_Name').text(vendor_detail_data.first_name);
                    $('.Last_Name').text(vendor_detail_data.last_name);
                    $('.Nickname').text(vendor_detail_data.nick_name);
                    $('.Name_on_Check').text(vendor_detail_data1.name_on_check);
                    $('.Notes_for_Phone').text(vendor_detail_data.phone_number_note);
                    $('.Vendor_Type').text(vendor_detail_data1.vendor_types);
                    $('.Vendor_Rate').text(vendor_detail_data1.vendor_rate);
                    $('.Ethnicity').text(vendor_detail_data.ethnicity_name);
                    $('.SSN').text(vendor_detail_data.ssn_sin_id);
                    $('.Postal_Code').text(vendor_detail_data.zipcode);
                    $('.State').text(vendor_detail_data.state);
                    $('.Address').text(vendor_detail_data.custom_address);



                    $('.Country').text(vendor_detail_data.country);
                    $('.City').text(vendor_detail_data.city);
                    $('.Website').text(vendor_detail_data.website);
                    $('.email').text(vendor_detail_data1.email);
                    $(".viewCredential").html(res.viewCredentialInfo);

                    //payment setting data

                    $(".Payer_Name").text(vendor_detail_data.tax_payer_name);
                    $(".Payer_ID").text(vendor_detail_data.tax_payer_id);

                    var consolidate_check = (vendor_detail_data1.consolidate_checks=='1') ? "Yes" : "No";

                    $(".checks").text(consolidate_check);
                    $(".Order_Limit").text(vendor_detail_data1.order_limit);

                    $(".Expense_Account").text(vendor_detail_data1.account_names);


                    $(".Account_Name").text(vendor_detail_data1.acc_name);
                    $(".Routing_Number").text(vendor_detail_data1.routing_number);
                    $(".Account_Number").text(vendor_detail_data1.bank_account_number);
                    $(".Comments").text(vendor_detail_data1.comment);

                    var Eligible = (vendor_detail_data1.eligible_for_1099=='1') ? "Yes" : "No";


                    $(".Eligible_1099").text(Eligible);


                    $('.vendor_image').html(vendor_detail_data1.image);



                } else if (res.status == "error") {
                    toastr.error(res.message);
                } else {
                    toastr.error(res.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }




});
function getcountrycode(){
    $.ajax({
        type: 'post',
        url: '/vendor-view-ajax',
        data: {class: "ViewVendor",
            action: "getCountrycode"},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                var countryOption = "<option value='0'>Select</option>";
                $.each(response.data, function (key, value) {
                    countryOption += "<option value='" + value.id + "' data-id='" + value.code + "'>" + value.name + " (" + value.code + ")" + "</option>";
                });
                $('#flag_country_code').html(countryOption);
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}

jQuery(document).ready(function($){
    if(localStorage.getItem("activeTab"))
    {
        var message = localStorage.getItem("activeTab");
        $(message).find('a').click();
        localStorage.removeItem('activeTab');
    }
    if(localStorage.getItem("scrollDown")) {
        var message = localStorage.getItem("scrollDown");
        $('html, body').animate({
            'scrollTop' : $(message).position().top
        });
        localStorage.removeItem('scrollDown');
    }
});

$(document).on('click','.edit_redirection',function(){
    var vendor_id = $(".vendor_id").val();
    var data = $(this).attr('data_id');
    var scroll = $(this).attr('data_scroll');
    localStorage.setItem("activeTab",data);
    localStorage.setItem("scrollDown",scroll);
    window.location.href = window.location.origin + '/Vendor/EditVendor?id='+vendor_id;
});

$("#sendEmailTemplate").validate({
    rules: {
        sendEmail: {
            required:true
        }
    },
    submitHandler: function (e) {
        var tenant_id = $(".vendor_id").val();
        var form = $('#sendEmailTemplate')[0];
        var formData = new FormData(form);
        formData.append('action','sendEmailTemplate');
        formData.append('class','VendorPortal');
        formData.append('tenant_id',tenant_id);
        formData.append('temp_key',"newVendorWelcome_Key");
        $.ajax({
            url:'/Vendor-portal-ajax',
            type: 'POST',
            data: formData,
            success: function (data) {
                var info = JSON.parse(data);
                if(info.status=='success')
                {
                    toastr.success(info.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});
showCustomFields();
function showCustomFields(){
    var vendor_id=$(".vendor_id").val();
    $.ajax({
        type: 'post',
        url: '/vendor-view-ajax',
        data: {id:vendor_id,
            class: 'ViewVendor',
            action: 'getCustomFieldData'},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                getCustomFieldForViewMode(response.data);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });
}


/**
 * function to show custom field
 * @param editCustomFieldData
 */
function getCustomFieldForViewMode(editCustomFieldData){
    var module = 'vendor';
    $.ajax({
        type: 'POST',
        url: '/CustomField/get',
        data:{module:module},
        success: function (response) {
            var response = JSON.parse(response);
            var custom_array = [];
            if(response.code == 200 && response.message == 'Record retrieved successfully'){
                $('.custom_field_html_view_mode').html('');
                $('.custom_field_msg').html('');
                var html ='';
                $.each(response.data, function (key, value) {
                    var default_val = '';
                    if(editCustomFieldData) {
                        default_val = (value.default_value != '' && value.default_value != null) ? value.default_value : '';
                        $.each(editCustomFieldData.custom_field, function (key1, value1) {
                            if (value1.id == value.id) {
                                default_val = value1.value;
                            }
                        });

                    } else {
                        default_val = (value.default_value != '' && value.default_value != null) ? value.default_value : '';
                    }

                    var required = (value.is_required == '1')?' <em class="red-star">*</em>':'';
                    var name = value.field_name.replace(/ /g, "_");
                    var currency_class = (value.data_type == 'currency')?' amount':'';
                    if(value.data_type == 'date') {
                        custom_array.push(name);
                    }
                    var readonly = 'readonly';
                    html += '<div class="row custom_field_class">';
                    html += '<div class=col-sm-6>';
                    html += '<label>'+value.field_name+required+'</label>';
                    html += '<input type="text" readonly data_type="'+value.data_type+'" data_value="'+value.default_value+'" '+readonly+' data_id="'+value.id+'" data_required="'+value.is_required+'" name="'+name+'" id="'+name+'" value="'+default_val+'" class="form-control changeCustom add-input'+currency_class+'">';
                    //html += '<span class="customError required"></span>';
                    //html += '</div>';
                    if(value.is_editable == 1 || value.is_deletable == 1) {
                        //html += '<div class="col-sm-6">';
                        if (value.is_editable == 1) {
                            html += '<span id="' + value.id + '" class="editCustomField"><i class="fa fa-edit"></i></span>';
                        }
                        if (value.is_deletable == 1) {
                            html += '<span id="' + value.id + '" class="deleteCustomField"><i class="fa fa-times-circle""></i></span>';
                        }
                        //html += '</div>';
                    }
                    html += '<span class="customError required"></span>';
                    html += '</div>';
                    html += '</div>';

                });
                $(html).prependTo('.custom_field_html_view_mode');
            } else {
                $('.custom_field_html_view_mode').html('No Custom Fields');
            }
            triggerDatepicker(custom_array)
        },
        error: function (data) {
            console.log(data);
        }
    });
}
$(document).on('click','.compVendorView',function () {
    var formid=$(this).attr('rel');
    var dataid=$(this).attr('data-id');
    var text=$("#"+dataid).text();
    if(text == 'Update'){
        resetEditForm("#"+formid,[],true,vendorComplaintData,[]);
    }else{
        resetFormClear('#'+formid,[],'form',false);
    }
});
$(document).on('click','.flagVendorView',function () {
    var formid=$(this).attr('rel');
    var dataid=$(this).attr('data-id');
    var text=$("#"+dataid).text();
    if(text == 'Update'){
        resetEditForm("#"+formid,[],true,vendorFlagData,[]);
    }else{
        resetFormClear('#'+formid,[],'form',false);
    }
});