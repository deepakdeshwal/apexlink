// $("#portfolio_id").keypress(function(e){
//     var keyCode = e.which;
//     /*
//       8 - (backspace)
//       32 - (space)
//       48-57 - (0-9)Numbers
//     */
//     // Not allow special
//     if ( !( (keyCode >= 48 && keyCode <= 57)
//         ||(keyCode >= 65 && keyCode <= 90)
//         || (keyCode >= 97 && keyCode <= 122) )
//         && keyCode != 8 && keyCode != 32) {
//         e.preventDefault();
//     }
// });

//display add portfolio div
$(document).on('click','#new_flag',function () {

    $('#flagFormDiv').show(500);
    $('#flag_id').val('');
    $('#flagSaveBtnId').text('Save');
    $('#flagForm').trigger("reset");
    var validator = $( "#flagForm" ).validate();
    validator.resetForm();
    var flag_view_name=$("#flag_view_name").val();
    $("#flag_flag_date").datepicker({dateFormat: datepicker,
            changeYear: true,
            yearRange: "-100:+20",
            changeMonth: true,
        }).datepicker("setDate", new Date());
    var flag_country_code=$("#flag_country_code").val();
    var flag_view_phone_number=$("#flag_view_phone_number").val();
    $('.flag_country_codeClass option[value='+flag_country_code+']').attr('selected', 'selected');
   // $(".editflagfname").val(flag_view_name);
    $("#flag_phone_number").val(flag_view_phone_number);
    vendorFlagData = $('#flagForm').serializeArray();
    $('#resetFlagForm').hide();
    console.log($('#Vendorname').text());
    $('#flagged_for').val($('#Vendorname').text());
    $('#clearFlagForm').show();

});



$("#flag_flag_date").datepicker({dateFormat: datepicker});

//on flag cancel
$(document).on('click','#flagCancel',function(){
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $('#flag_id').val('');
                $('#flagFormDiv').hide(500);
            }
        }
    });
});

//adding custom fields


jqGridFlag('All');
/**
 * jqGridFlag Initialization function
 * @param status
 */
function jqGridFlag(status) {
    var table = 'flags';
    var columns = ['Date','Flag Name', 'Phone Number', 'Flag Reason', 'Completed', 'Note', 'Action'];
    var select_column = ['Edit','Delete','Completed'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'object_id',value:$("#property_editunique_id").val(),condition:'='},{column:'object_type',value:'property',condition:'='}];
    var extra_columns = ['flags.deleted_at'];
    var columns_options = [
        {name:'Date',index:'date',align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Flag Name',index:'flag_name',searchoptions: {sopt: conditions},table:table},
        {name:'Phone Number',index:'flag_phone_number',align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Flag Reason',index:'flag_reason', width:180,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Completed',index:'completed',width:180, align:"center",searchoptions: {sopt: conditions},table:table,formatter: completedFormatter},
        {name:'Note',index:'flag_note',width:200, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'select',width:200, title: false,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter: actionFmatter}
    ];
    var ignore_array = [];
    
    jQuery("#propertyFlag-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Flags",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}

/**
 * function to change completed format
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function completedFormatter (cellValue, options, rowObject){
    if (cellValue == 1)
        return "True";
    else if(cellValue == 0)
        return "False";
    else
        return '';
}

/**
 * function to change action column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function actionFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';
        if(rowObject.Completed == 1) select = ['Edit','Delete'];
        if(rowObject.Completed == 0) select = ['Edit','Delete','Completed'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }

}

/**  List Action Functions  */

getcountrycode();
function getcountrycode(){
    $.ajax({
        type: 'post',
        url: '/vendor-view-ajax',
        data: {class: "ViewVendor",
            action: "getCountrycode"},
        success: function (response) {

            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                var countryOption = "<option value='0'>Select</option>";
                $.each(response.data, function (key, value) {
                    countryOption += "<option value='" + value.id + "' data-id='" + value.code + "'>" + value.name + " (" + value.code + ")" + "</option>";
                });
                $('.flag_country_codeClass').html(countryOption);
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}