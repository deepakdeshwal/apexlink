$(document).on('click','#clearNewvendortype',function(){
    resetFormClear('#Newvendortype',[],'div',false);
});

$(document).on('click','#clearEthnicity_options',function(){
    resetFormClear('#Newethnicity',[],'div',false);
});

$(document).on('click','#clearselectPropertyHobbies1',function(){
    resetFormClear('#selectPropertyHobbies1',[],'div',false);
});

$(document).on('click','#clearselectPropertyVeteranStatus1',function(){
    resetFormClear('#selectPropertyVeteranStatus1',[],'div',false);
});

$(document).on('click','#clearadditionalReferralResource1',function(){
    resetFormClear('#additionalReferralResource1',[],'div',false);
});

$(document).on('click','#clearNewaccountname',function(){
    resetFormClear('.Newaccountname',[],'div',false);
});

$(document).on('click','#clearChartsOfAccountsModal',function(){
    bootbox.confirm("Do you want to clear this form?", function (result) {
        if (result == true) {
            resetFormClear('#add_chart_account_form_id',[],'form',false);
            $('#is_default').prop('checked',false);
        }
    });
});

$(document).on('click','#clearNewCredentialType',function(){
    resetFormClear('#NewCredentialType',[],'div',false);
});

$(document).on('click','#clearVendor_note',function(){
    resetFormClear('#vendorNotes',[],'form',false);
});