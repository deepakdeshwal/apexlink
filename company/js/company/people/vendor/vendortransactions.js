
$(document).ready(function () {
    $(document).on('click','#search_ledger',function () {
        ten_ledger();
    });
    ten_ledger();
    function ten_ledger() {
        var  user_id= localStorage.getItem('ven_id');
        console.log(user_id);
        var a=$('.st_date').val();
        a = new Date(a);
        var yr=a.getFullYear();
        var mon=a.getMonth();
        var day=a.getDate();
        var st_date=yr+'-'+mon+'-'+day;
        var b= $('.end_date').val();
        b=new Date(b);
        var yr1=b.getFullYear();
        var mon1=b.getMonth() + 1;
        var end_date=yr1+'-'+mon1;
        console.log((st_date,end_date));
        var firstDay = new Date(yr, mon, 1);
        var lastDay = new Date(yr1, mon1, 0);
        st_date=firstDay.getFullYear()+'-'+(firstDay.getMonth()) +'-'+firstDay.getDate();
        end_date=lastDay.getFullYear()+'-'+(lastDay.getMonth()+1) +'-'+lastDay.getDate();
        $.ajax({
            url:'/vendorportal/transactions',
            type: 'POST',
            data: {
                "action": 'gettransactiondata',
                "class": 'VendorPortalAjax',
                "user_id":user_id,
                'st_date':st_date,
                'end_date':end_date
            },
            success: function (response) {
                var res=JSON.parse(response);
                console.log(res);
                var html="";
                for(var i=0;i<res.data.length;i++){
                    html +='<tr class="ledger_tr">' +
                        '                                                                    <td>'+res.dates[i]+'</td>' +
                        '                                                                    <td>'+res.data[i].transaction_id+'</td>' +
                        '                                                                    <td>'+res.data[i].payment_response+'</td>' +
                        '                                                                    <td>'+res.data[i].reference_no+'</td>' +
                        '                                                                    <td>'+currencySign+''+res.data[i].total_charge_amount+'</td>' +
                        '                                                                </tr>';
                }
                $("#vendor_ledger").html('').append(html);
            }
        });

    }

    $(document).on('click','#exportexcel',function () {
        exportTableToExcel();
    });
    $(document).on('click','#exportpdf',function () {
        exppdf();
    });
    $(document).on('click','#printsample',function () {
        exppdf();
    });
    function exportTableToExcel(tableID, filename = ''){
        var innerHTML=$("#ledger_table").html();
        var html = innerHTML;
        var blob = new Blob(['\ufeff', html], {
            type: 'application/vnd.ms-excel'
        });

        // Specify link url
        var url = 'data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent(html);

        // Specify file name
        filename = filename?filename+'.xls':'excel.xls';


        // Create download link element
        var downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);

        if(navigator.msSaveOrOpenBlob ){
            navigator.msSaveOrOpenBlob(blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = url;

            // Setting the file name
            downloadLink.download = filename;

            //triggering the function
            downloadLink.click();
        }

        document.body.removeChild(downloadLink);
    }


    function exppdf() {
        var htmls=$("#ledger_table").html();
        console.log('htmls',htmls);
        $.ajax({
            type: 'post',
            url: '/vendorportal/transactions',
            data: {
                class: "VendorPortalAjax",
                action: "getPdfContent",
                htmls: htmls
            },
            success: function (res) {
                var res= JSON.parse(res);
                console.log(res.data);
                if (res.code == 200) {
                    $('.reports-loader').css('visibility','hidden');
                    var link=document.createElement('a');
                    document.body.appendChild(link);
                    link.target="_blank";
                    link.download="Report.pdf";
                    link.href=res.data.record;
                    link.click();
                    $("#wait").css("display", "none");
                } else if(res.code == 500) {
                    //toastr.warning(response.message);
                } else {
                    //toastr.error(response.message);
                }
            }
        });
    }


});



































