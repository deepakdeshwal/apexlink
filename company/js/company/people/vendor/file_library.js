$(document).on('click','#add_libraray_file',function(){
    $('#file_library').val('');
    $('#file_library').trigger('click');
});
var file_library = [];
var imgArray = [];
$(document).on('change','#file_library',function(){
   // file_library = [];
    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        if(size > 4097) {
            toastr.warning('Please select documents less than 4 mb!');
        } else {
            size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
            if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                  if($.inArray(value['name'], imgArray) === -1)
                {
                    file_library.push(value);
                }
                var src = '';
                var reader = new FileReader();
              //  $('#file_library_uploads').html('');
                reader.onload = function (e) {
                    if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                        src = upload_url + 'company/images/excel.png';
                    } else if (type == 'application/pdf') {
                        src = upload_url + 'company/images/pdf.png';
                    } else if (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                        src = upload_url + 'company/images/word_doc_icon.jpg';
                    } else if (type == 'text/plain') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else if (type == 'text/xml') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else {
                        src = e.target.result;
                    }
                   if($.inArray(value['name'], imgArray) === -1)
                    {
                        
                    $("#file_library_uploads").append(
                        '<div class="row" style="margin:20px">' +
                        '<div class="col-sm-12 img-upload-library-div">' +
                        '<div class="col-sm-3"><img class="img-upload-tab' + key + '" width=80 height=88 src=' + src + '></div>' +
                        '<div style="margin-top: 36px;" class="col-sm-3 center_img show-library-list-imgs-name ' + key + '">' + value['name'] + '</div>' +
                        '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                        '<div  style="margin-top: 36px;" class="col-sm-3 center_img show-library-list-imgs-size' + key + '">' + size + '</div>' +
                        '</div></div>');
                        imgArray.push(value['name']);
                    } else {
                        toastr.warning('File already exists!');
                    }
                };
                reader.readAsDataURL(value);
            } else {
                toastr.warning('Please select file with .xlsx | .pdf | .docx | .txt | .xml extension only!');
            }
        }
    });
});


$(document).on('click','.delete_pro_img',function(){
    toastr.success('The record deleted successfully.');
    $(this).parent().parent().parent('.row').remove();

    imgArray = jQuery.grep(imgArray, function(value) {
        return value != deleted_img;
    });
});

$(document).on('click','#remove_library_file',function(){
    bootbox.confirm("Do you want to remove all files?", function (result) {
       /* if (result == true) {
            toastr.success('The record deleted successfully.');
            $('#file_library_uploads').html('');
            $('#file_library').val('');
            imgArray = [];
        }*/
    });

});

function convertSerializeDatatoArray(){
    var newData = [];
    $(".fileLibraryInput").each(function( index ) {
        var name = $(this).val();
        var size = $(this).attr('data_id');
        newData.push({'name':name,'size':size});
    });
    return newData;
}

function compareArray(data,compare){
    for(var i =0;i < compare.length;i++){
        if(compare[i].name == data['name'] && compare[i].size == data['size']){
            return 'true';
        }
    }
    return 'false';
}

function isa_convert_bytes_to_specified(bytes, to) {
    var formulas =[];
    formulas['k']= (bytes / 1024).toFixed(1);
    formulas['M']= (bytes / 1048576).toFixed(1);
    formulas['G']= (bytes / 1073741824).toFixed(1);
    return formulas[to];
}