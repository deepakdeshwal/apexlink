$(document).ready(function() {
  Recurring_Bill();
    $(document).on("click",'#RecurringPropertyAccountTable .UnitdetailAmount', function () {
        var id = $(this).attr('data-id');
        jqGridUnit(id);
    });
    $(document).on("click",'.viewEdit', function () {
        var id = $(this).attr('data-editid');
        window.location.href = base_url+"/Accounting/EditRecurringBill?id="+id;
    });
  //  alert( $default_symbol);

});
function Recurring_Bill() {
    var sortColumn = 'company_bills.updated_at';
    var table = 'company_bills';
    var columns = ['Vendor Name','vndr_name','Created Date','Reference Number', 'Original Amount('+default_symbol+')', 'Amount Due('+default_symbol+')', 'Last Generated Bill','Action'];
    var select_column = ['EDIT', 'DELETE','VIEW DETAILS','CANCEL RECURRENCE'];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [];
    var extra_columns = [];
    var joins = [{table: 'company_bills', column: 'vendor_id', primary: 'id', on_table: 'users'}
    ];

    var columns_options = [
        {name: 'Vendor Name', index: 'name', width: 100, searchoptions: {sopt: conditions}, table: 'users',formatter:VendorNameFormatter},
        {name: 'vndr_name', hidden:true ,index: 'name', width: 100, searchoptions: {sopt: conditions}, table: 'users'},
        {
            name: 'Created Date',
            index: 'created_at',
            width: 100,
            searchoptions: {sopt: conditions},
            table: table
        },
        {name: 'Reference Number', index: 'refrence_number', width: 100, searchoptions: {sopt: conditions}, table: table},
        {name: 'OriginalAmount', index: 'amount', width: 100, searchoptions: {sopt: conditions}, table: table,formatter:OriginalAmtFormatter},
        {name: 'AmountDue', index: 'amount', width: 100, searchoptions: {sopt: conditions}, table: table,formatter:AmtDueFormatter},
        {name: 'Last Generated Bill', index: 'created_at', width: 100, searchoptions: {sopt: conditions}, table: table},
        {
            name: 'Action',
            index: 'select',
            title: false,
            width: 80,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            edittype: 'select',
            search: false,
            table: table,
            formatter: actionFormatter
        },
    ];
    var ignore_array = [];
    jQuery("#RecurringDataTable").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            ignore: ignore_array,
            joins: joins,
            extra_where: extra_where,
            extra_columns: extra_columns,
            deleted_at: 'true'
        },
        viewrecords: true,
        sortname: sortColumn,
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Recurring bills",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 200, drag: true, resize: false} // search options
    );
}

function VendorNameFormatter(cellvalue, options, rowObject)
{
   // console.log(rowObject);
    if(rowObject!==undefined)
    {
        if(rowObject.vndr_name!='' && typeof rowObject.vndr_name != undefined )
        {
            console.log(rowObject.vndr_name);
           return '<a class="GoOnBillView" data-id="'+rowObject.id+'" style="color:#05A0E4 ! important;font-weight: bold"><u><strong>'+rowObject.vndr_name+'</strong></u></a>';
        } else{
            return '';
        }
    }
}

    function OriginalAmtFormatter(cellvalue, options, rowObject){
        if(rowObject!==undefined)
        {
            var symbol = default_symbol;
            if(rowObject.OriginalAmount!='')
            {
                var amount = changeToFloat(rowObject.OriginalAmount.toString());
                return symbol+''+amount;
            }
        }
    }
    function AmtDueFormatter(cellvalue, options, rowObject){
        if(rowObject!==undefined)
        {
            var symbol = default_symbol;
            if(rowObject.AmountDue!='')
            {
                var amountdue = changeToFloat(rowObject.AmountDue.toString());
                return '<b>'+symbol+''+amountdue+'</b>';
            }
        }
    }

$(document).on('click','.GoOnBillView',function () {
    var id =   $(this).attr('data-id');
    $('#RecurrTable').hide();
    $('#viewlisting').show();
    jqGridListOfProperties(id);
    jqGridNotes(id);
    jqGridFileLibrary(id);
    getInfo(id);

});

function actionFormatter(cellValue, options, rowObject) {

    if (rowObject !== undefined) {
     //   console.log('rowObject', rowObject);

        var editable = $(cellValue).attr('editable');
        var is_default = rowObject;
         //   console.log(rowObject);
        var select = '';

        select = ['EDIT', 'DELETE','VIEW DETAILS','CANCEL RECURRENCE'];

        var data = '';

        if (select != '') {

            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function(key, val) {
              /*  if (editable == '0' && (val == 'delete' || val == 'Delete')) {
                    return true;
                } */
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';

            });

            data += '</select>';

        }
     //   console.log( 'data', data);
        return data;

    }

}

/*
function isa_convert_bytes_to_specified(bytes, to) {
    var formulas =[];
    formulas['k']= (bytes / 1024).toFixed(1);
    formulas['M']= (bytes / 1048576).toFixed(1);
    formulas['G']= (bytes / 1073741824).toFixed(1);
    return formulas[to];
}*/

$(document).on('change', '.select_options', function() {
    setTimeout(function() {
        $(".select_options").val("default");
    }, 200);

    var opt = $(this).val();

    var id = $(this).attr('data_id');

    var row_num = $(this).parent().parent().index();

    if (opt == 'View Details' || opt == 'VIEW DETAILS') {

        $('#RecurrTable').hide();
        $('#viewlisting').show();

        jqGridListOfProperties(id);
        jqGridNotes(id);
        jqGridFileLibrary(id);
        getInfo(id);

    } else if (opt == 'Cancel Recurrence' || opt == 'CANCEL RECURRENCE') {
        $.ajax({
            type: 'post',
            url: '/newBill-ajax',
            data: {
                class: 'billsAjax',
                action: 'RecurringRecord_delete',
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                } else {
                    toastr.error(response.message);
                }
                $('#RecurringDataTable').trigger('reloadGrid');
            }
        });
    } else if (opt == 'Edit' || opt == 'EDIT') {

        opt = opt.toLowerCase();
            window.location.href = base_url+"/Accounting/EditRecurringBill?id="+id;

    } else if (opt == 'Delete' || opt == 'DELETE') {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/newBill-ajax',
                        data: {
                            class: 'billsAjax',
                            action: 'RecurringRecord_delete',
                            id: id
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                            $('#RecurringDataTable').trigger('reloadGrid');
                        }
                    });
                }
                $('#RecurringDataTable').trigger('reloadGrid');
            }
        });

    } else {

    }
});

function getInfo(id) {
    $.ajax ({
        type: 'post',
        url: '/newBill-ajax',
        data: {
            class: "billsAjax",
            action: "RecurringView",
            id: id,
        },
        success: function(response) {
            var data = $.parseJSON(response);

            $('.vndr_nme').html(data.data.BillInfo['name']);
            $('.billdate').html(data.data.BillInfo['bill_date']);
            var amt = data.data.BillInfo['amount'];
           var newamt =  changeToFloat(amt.toString());
            $('.amt').html(default_symbol+newamt);
            $('.terms').html(data.data.BillInfo['term']);
            $('.wrkorderno').html(data.data.BillInfo['work_order']);
            $('.duedate').html(data.data.BillInfo['due_date']);
            $('.reference').html(data.data.BillInfo['refrence_number']);
            $('.memo').html(data.data.BillInfo['memo']);
            $('.invoiceno').html(data.data.BillInfo['invoice_number']);
            $('.viewEdit').attr('data-editid',data.data.BillInfo['id']);

            var frequency = data.data.BillInfo['frequency'];
            var freval;
            if(frequency == 1){
                freval = 'Weekly';
            } else if(frequency == 2){
                freval='Bi-Weekly';
            } else if(frequency == 3){
                freval='Monthly';
            } else if(frequency == 4){
                freval='Annually';
            } else if(frequency == 5){
                freval='Quartely';
            } else if(frequency == 6){
                freval='Semi-Annually';
            } else if(frequency == 7){
                freval='Daily';
            } else if(frequency == 8){
                freval='Bi-Monthly';
            }
            $('.billrecurr').html(freval);
        },
        error: function(data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function(key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
    return false;
}

function jqGridListOfProperties(ID) {
    //alert(ID);
    var sortby = 'company_bill_items.updated_at';
    var table = 'company_bill_items';
    var columns = ['Property Name','Units','Account Type','Property Amount('+default_currency_symbol+')','Description','Charge('+default_currency_symbol+')','Actions'];
    var select_column = ['View details'];
    var joins = [{table: 'company_bill_items', column: 'account', primary: 'id', on_table: 'company_chart_of_accounts'},
        {table: 'company_bill_items', column: 'building_id', primary: 'id', on_table: 'building_detail'},
        {table: 'building_detail', column: 'property_id', primary: 'id', on_table: 'general_property'}
        ];

    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var pagination = [];
    var extra_where = [{column: "bill_id", condition: "=", value: ID}];
   // var extra_where = [{column:'object_id',value:property_unique_id,condition:'='},{column:'object_type',value:'property',condition:'='}];
    var columns_options = [
        {name:'Property Name',index:'property_name',width:300,align:"center",searchoptions: {sopt: conditions},table:'general_property'},
        {name:'Units',index:'no_of_units',width:150,align:"center",searchoptions: {sopt: conditions},table:'general_property'},
        {name:'Account Type',index:'account_name',width:250,align:"center",searchoptions: {sopt: conditions},table:'company_chart_of_accounts'},
        {name:'Property Amount('+default_currency_symbol+')',index:'amount',width:250,align:"center",searchoptions: {sopt: conditions},table:table,formatter:Amtformatter},
        {name:'Description',index:'description',width:250,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Charge('+default_currency_symbol+')',index:'amount',width:250,align:"center",searchoptions: {sopt: conditions},table:table,formatter:Amtformatter},
        {
            name: 'Action',
            index: 'select',
            title: false,
            width: 80,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            edittype: 'select',
            search: false,
            table: table,
            formatter: AccountactionFormatter
        },
       // {name:'Actions',index:'amount',width:450,align:"center",searchoptions: {sopt: conditions},table:table}
    ];
    var ignore_array = [];
    jQuery("#RecurringPropertyAccountTable").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: sortby,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
          //  status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Properties",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

function Amtformatter(cellValue, options, rowObject) {
    if (rowObject !== undefined) {
    var symbol = default_currency_symbol;
        var amt = changeToFloat(cellValue.toString());
   // console.log(cellValue);
        return symbol+amt;
    }
}
function AccountactionFormatter(cellValue, options, rowObject) {

    if (rowObject !== undefined) {
              var data = '<a data-id="'+rowObject.id+'" class="UnitdetailAmount" data-toggle="modal" data-target="#UnitModal">ViewDetails</a>';
//var data = 'View details';
        return data;
    }
}

function jqGridNotes(ID) {
    //alert(ID);
    var sortby = 'company_bill_notes.updated_at';
    var table = 'company_bill_notes';
    var columns = ['Date','Time','Notes'];
    var select_column = [];
    var joins = [];

    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var pagination = [];
    var extra_where = [{column: "bill_id", condition: "=", value: ID}];
    // var extra_where = [{column:'object_id',value:property_unique_id,condition:'='},{column:'object_type',value:'property',condition:'='}];
    var columns_options = [
        {name:'Date',index:'created_at',width:400,align:"center",searchoptions: {sopt: conditions},table:table,formatter:DateFormatter},
        {name:'Time',index:'created_at',width:450,align:"center",searchoptions: {sopt: conditions},table:table,formatter:TimeFormatter},
        {name:'Notes',index:'note',width:450,align:"center",searchoptions: {sopt: conditions},table:table}
    ];
    var ignore_array = [];
    jQuery("#RecurringNotesTable").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: sortby,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            //  status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Notes/History",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}
function DateFormatter(cellValue, options, rowObject) {
    if (rowObject !== undefined) {
        var dateTime = rowObject.Date;
        var d = new Date(dateTime);
       var date = d.toDateString();
        return date;
    }
}
function TimeFormatter(cellValue, options, rowObject) {
    if (rowObject !== undefined && rowObject !== 'NaN') {

        var dateTime = rowObject.Time;

        var d = new Date(dateTime);
        var time = d.toTimeString();
     ///   console.log(date);
        return time;
    }
}


function jqGridFileLibrary(ID) {
  //  alert('aman');
    var table = 'company_bill_files';
    var columns = ['Display Name','Preview'];
    var select_column = [];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column: "bill_id", condition: "=", value: ID}];
    var columns_options = [
        {name:'Display Name',index:'name',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'path',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
    ];
    var ignore_array = [];
    jQuery("#RecurringFilesTable").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
        //    status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
       rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of File Library",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}


function imageFormatter(cellvalue, options, rowObject){

    if(rowObject !== undefined) {

        console.log(upload_url);
        var path = upload_url+'company/'+rowObject.Preview;
        console.log(path);
        var imageurl = upload_url+'/company/images/pdf.png';
        var src = '';
        if (rowObject.Preview == 'xlsx') {
            src = upload_url + 'company/images/excel.png';
        } else if (rowObject.Preview == 'pdf') {
            src = upload_url + 'company/images/pdf.png';
        } else if (rowObject.Preview == 'docx') {
            src = upload_url + 'company/images/word_doc_icon.jpg';
        }else if (rowObject.Preview == 'txt') {
            src = upload_url + 'company/images/notepad.jpg';
        }
        return '<a href='+ path + '><img class="img-upload-tab open_file_location" data-location="'+path+'" width=100 height=100 src="'+imageurl+'"></a>';
    }
}


function jqGridUnit(ID) {
    var table = 'company_bill_items';
    var columns = ['Unit Name','Unit Amount','Charge('+default_currency_symbol+')'];
    var select_column = [];
    var joins = [{table: 'company_bill_items', column: 'unit_id', primary: 'id', on_table: 'unit_details'},
    ];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column: "id", condition: "=", value: ID}];
    var columns_options = [
        {name:'Unit Name',index:'unit_prefix',width:190,align:"center",searchoptions: {sopt: conditions},table:'unit_details'},
        {name:'Unit Amount',index:'amount',width:190,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:UnitAmtFormatter},
        {name:'Charge',index:'amount',width:190,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:UnitAmtFormatter},
    ];
    var ignore_array = [];
    jQuery("#UnitModal #UnitDataTable").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'company_bill_items.updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            //    status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
          rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Unit Detail",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}

function UnitAmtFormatter(cellvalue, options, rowObject) {
    if(rowObject!==undefined)
    {
        var symbol = default_symbol;
        if(rowObject.AmountDue!='')
        {
            var amountdue = changeToFloat(rowObject.Charge.toString());
            return symbol+''+amountdue;
        }
    }
}


