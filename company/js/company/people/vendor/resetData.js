if(localStorage.getItem('windowScroll') !== null){
    setTimeout(function(){
        $('html, body').animate({
            'scrollTop':  localStorage.getItem('windowScroll')
        });
        localStorage.removeItem('windowScroll');
    },300);
}

$(document).on('click','#resetNewvendortype',function(){
    bootbox.confirm("Do you want to reset this form?", function (result) {
        if (result == true) {
            localStorage.setItem('windowScroll',document.body.scrollHeight);
            window.location.reload();
        }
    });

});



$(document).on('click','#reset_payment_setting_form',function(){
    resetEditForm('#payment_setting_form',[],true,vendorPaymentFormData,[]);
});

$(document).on('click','#resetVendor_note',function(){
    resetEditForm('#vendorNotes',[],true,vendorNotesData,[]);
});

$(document).on('click','#clearVendor_note',function(){
    resetFormClear('#vendorNotes',[],'form',true);
});

$(document).on('click','#clearNewComplaintform',function(){
    resetFormClear('#new_complaintform',['complaint_id','complaint_date','complaint_type_options'],'form',true,vendorComplaintData,[]);
});

$(document).on('click','#resetNewComplaintform',function(){
    resetEditForm('#new_complaintform',[],true,vendorComplaintData,['complaint_by_about']);
});

$(document).on('click','#clearFlagForm',function(){
    resetFormClear('#flagForm',['flag_by','date','country_code','flag_phone_number','completed'],'form',true,vendorFlagData,[]);
});

$(document).on('click','#resetFlagForm',function(){
    resetEditForm('#flagForm',[],true,vendorFlagData,[]);
});

$(document).on('click','#clearComplaintTypePopup',function(){
    resetFormClear('#ComplaintTypePopup',[],'div',false);
});

$(document).on('click','#clearChartsOfAccountsModal',function(){
    bootbox.confirm("Do you want to clear this form?", function (result) {
        if (result == true) {
            resetFormClear('#add_chart_account_form_id',[],'form',false);
            $('#is_default').prop('checked',false);
        }
    });
});

$(document).on('click','#clearNewaccountname',function(){
    resetFormClear('.Newaccountname',[],'div',false);
});

$(document).on('click','#clearNewvendortype',function(){
    resetFormClear('#Newvendortype',[],'div',false);
});

$(document).on('click','#clearEthnicity_options',function(){
    resetFormClear('#Newethnicity',[],'div',false);
});

$(document).on('click','#clearselectPropertyHobbies1',function(){
    resetFormClear('#selectPropertyHobbies1',[],'div',false);
});

$(document).on('click','#clearselectPropertyVeteranStatus1',function(){
    resetFormClear('#selectPropertyVeteranStatus1',[],'div',false);
});

$(document).on('click','#clearadditionalReferralResource1',function(){
    resetFormClear('#additionalReferralResource1',[],'div',false);
});

$(document).on('click','.clearNewCredentialType',function(){
    resetFormClear('.NewCredentialType',[],'div',false);
});

$(document).on('click','#clearselectPropertyMaritalStatus1',function(){
    resetFormClear('#selectPropertyMaritalStatus1',[],'div',false);
});




// $(document).on('click','#clearChartsOfAccountsModal',function(){
//     bootbox.confirm("Do you want to clear this form?", function (result) {
//         if (result == true) {
//             resetFormClear('#add_chart_account_form_id',[],'form',false);
//             $('#is_default').prop('checked',false);
//         }
//     });
// });
