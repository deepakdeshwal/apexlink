$(document).ready(function () {

    $(document).on("click",".rotate-ccw",function(){
        $('.image-editor').cropit('rotateCCW')
    });

    $(document).on("click",".rotate-cw",function(){
        $('.image-editor').cropit('rotateCW')
    });

    jQuery('.phone_format').mask('000-000-0000', {reverse: true});

    $(document).on('click', '.edit_redirectionTab1', function (event) {
        event.preventDefault();
        $("#GenVendorOne").hide();
        $("#vendor_type_options").val(' ');
        setTimeout(function viewData() {
            var vendor_id=$(".vendor_id").val();

            $.ajax({
                type: 'post',
                url: '/vendor-view-ajax',
                data: {
                    class: "ViewVendor",
                    action: "getVendorDetail",
                    vendor_id:vendor_id
                },
                success: function (response) {

                    var res = $.parseJSON(response);
                    if (res.status == "success") {

                        var vendor_type_idd = res.dataVendor.vendor_additional_detail.vendor_typess;

                        $("select[name='vendor_type_id']").val(vendor_type_idd);
                    }
                },

            });
        },100);
        $("#editViewLinkTab1").show();
        $(".acquireDateclass,.expirationDateclass").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());




    });

    $(document).on('click', '.edit_redirectionTab2', function (event) {
        event.preventDefault();
        $(".paymentDivViewTab").hide();
        $("#paymentDivTab2").show();
    });

    $(document).on('click', '.btnSaveNewVendorCancel', function (event) {
        event.preventDefault();
        $("#GenVendorOne").show();
        $("#editViewLinkTab1").hide();
    });
    $(document).on('click', '.btnUpdateVendorCancel', function (event) {
        event.preventDefault();
        $(".paymentDivViewTab").show();
        $("#paymentDivTab2").hide();
    });
    $(document).on('change', '#phone_type_id', function () {
        var phone_type = $(this).val();

        if (phone_type == '2' || phone_type == '5')
        {
            $('#work_phone_extension-error').text('');
            $(this).closest('div').next().find('#work_phone_extension').val('');

            $(this).parent().next('div').next('div').show();
            $(this).closest('div').next().find('.phone_format').mask('000-000-0000', {reverse: true});
        } else {
            $(this).parent().next('div').next('div').hide();
        }

    });
    $(document).on('change', '#relationship', function () {
        var relation = $(this).val();

        if (relation == '9')
        {
            $(this).closest('div').next().find('#emergency_other_relation').val('');
            $(this).closest('div').next().show();
        } else {
            $(this).closest('div').next().hide();
        }
    });
    $(document).on("blur", ".address_field", function (e) {
        jQuery.validator.classRuleSettings.unique = {
            unique: true
        };

        jQuery.validator.addMethod("unique", function (value, element, params) {
            var prefix = params;
            var selector = jQuery.validator.format("[name!='{0}'][name^='{1}'][unique='{1}']", element.name, prefix);
            var matches = new Array();
            $(selector).each(function (index, item) {
                if (value == $(item).val()) {
                    matches.push(item);
                }
            });
            alert(matches.length);
            return matches.length == 0;
        }, "* Duplicate entry");

    });
    $(document).on("change","#salutation",function(){
        var value = $(this).val();
        if(value == 1){
            $('#gender').prop('selectedIndex',0);
            $('.hidemaiden').hide();
        }
        if(value==2)
        {
            $("#gender").val("1");
            $('.hidemaiden').hide();
        }
        if(value==3)
        {
            $("#gender").val("2");
            $('.hidemaiden').show();
        } if(value==4)
        {
            $('#gender').prop('selectedIndex',0);
            $('.hidemaiden').hide();
        } if(value==5)
        {
            $("#gender").val("2");
            $('.hidemaiden').show();
        } if(value==6)
        {
            $("#gender").val("1");
            $('.hidemaiden').hide();
        } if(value==7)
        {
            $("#gender").val("2");
            $('.hidemaiden').show();
        }
        if(value==8)
        {
            $("#gender").val("2");
            $('.hidemaiden').show();
        } if(value==9)
        {
            $("#gender").val("2");
            $('.hidemaiden').show();
        }if(value==10)
        {
            $("#gender").val("1");
            $('.hidemaiden').hide();
        }if(value==11)
        {
            $("#gender").val("1");
            $('.hidemaiden').hide();
        }


    });
    $(document).on('keydown', '.number_only', function (e) {
        if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            if (e.which == 190 || e.which == 110) {
                return true;
            }
            return false;
        }
    });
    $(".acquireDateclass,.expirationDateclass").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());



    $(document).on('focusout', '#zip_code', function () {
        getAddressInfoByZip($(this).val());
    });
    $(document).on('focusout','.czip_code', function () {
        getAddressInfoByZip($(this).val());
    });
    $("#zipcode").keydown(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            getAddressInfoByZip($(this).val());
        }
    });

    fetchAllethnicity(false);
    fetchAllVendorType(false);
    fetchAllAccountName(false);
    fetchAllcredentialtypes(false);
    fetchAllCarrier(false);
    fetchAllPhonetypes();
    fetchAllCarrier();
    fetchChartAccounts(false);
    accountTypeList();
    if($(".vendor_edit_id").val() != undefined){
        fetchVendorDetails();
    }
    $('.vendor_random_id').val(getRandomNumber(6));
    //add vendor type
    $(document).on("click", ".vendortypeplus", function () {
        $("#Newvendortype").show();
    });
    //add ethnicity
    $(document).on("click", ".ethnicityplus", function () {
        $("#Newethnicity").show();
    });
    //add mariatal status
    $(document).on("click", ".MaritalStatusplus", function () {
        $("#NewMaritalStatus").show();
    });
    //add hobbies
    $(document).on("click", ".hobbiesplus", function () {
        $("#Newhobbies").show();
    });
    //add hobbies
    $(document).on("click", ".hobbiesplus", function () {
        $("#Newhobbies").show();
    });
    //add Veteran Status 
    $(document).on("click", ".VeteranStatusplus", function () {
        $("#NewVeteranStatus").show();
    });
    //add ssn-sin 
    $(document).on("click", ".ssn-sin-plus", function () {
        var newnode = $("#ssn-sin-mainid").clone().insertAfter("div.ssn-sin-mainDiv:last");
        $(".ssn-sin-mainDiv:not(:eq(0))  .ssn-sin-plus").remove();
        $(".ssn-sin-mainDiv:not(:eq(0))  .ssn-sin-minus").show();
        newnode.find('.ssn_sin_id').val('');
    });
    //add ssn-sin 

    //add contact details

    $(document).on("click", ".phoneclassDivplus", function () {
        var clone = $(".primary-tenant-phone-row2:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        clone.find('#phone_type_id').val('1');
        $(".primary-tenant-phone-row2").last().after(clone);
        $(".primary-tenant-phone-row2:not(:eq(0)) .phoneclassDivplus").hide();
        $(".primary-tenant-phone-row2:not(:eq(0)) .phoneclassDivminus").show();
        var phoneRowLenght = $(".primary-tenant-phone-row2");
        clone.find(".work_extension_div").hide();
        if (phoneRowLenght.length == 2) {
            clone.find(".phoneclassDivplus").hide();
            clone.find(".phoneclassDivminus").show();
        } else if (phoneRowLenght.length == 3) {
            clone.find(".phoneclassDivminus").show();
            $(".primary-tenant-phone-row2:eq(0) .phoneclassDivplus").hide();
        } else {
            $(".primary-tenant-phone-row2:not(:eq(0)) .phoneclassDivplus").show();
        }
    });

    $(document).on("click", ".phoneclassDivminus", function () {

        var phoneRowLenght = $(".primary-tenant-phone-row2");
        if (phoneRowLenght.length == 2) {
            $(".primary-tenant-phone-row2:eq(0) .phoneclassDivplus").show();
        } else if (phoneRowLenght.length == 3) {
            $(".primary-tenant-phone-row2:eq(0) .phoneclassDivplus").hide();
        } else {
            $(".primary-tenant-phone-row2:not(:eq(0)) .phoneclassDivplus").show();
        }

        $(this).parents(".primary-tenant-phone-row2").remove();
    });

    //add Email Address details
    $(document).on("click", ".email-address-plus", function () {
        var emailDivLength = $('.emailaddressmainDiv').length;
        if (emailDivLength <= 2) {
            var newnode = $("#emailaddressmainDivId").clone().find("input:text").val("").end().insertAfter("div.emailaddressmainDiv:last");
            $(".emailaddressmainDiv:not(:eq(0))  .email-address-plus").remove();
            $(".emailaddressmainDiv:not(:eq(0))  .email-address-minus").show();
            if (emailDivLength == 2) {
                $('.email-address-plus').hide();
            }
        } else {
            $('.email-address-plus').hide();
        }
    });
    //remove email div
    $(document).on('click', '.email-address-minus', function () {
        $(this).parent().remove();
        $('.email-address-plus').show();
    });

    //remove email div
    $(document).on('click', '.email-address-minus', function () {
        $(this).parent().remove();
        $('.email-address-plus').show();
    });

    //add Email Address details
    $(document).on("click", ".newaccountnameplus", function () {
        $(".Newaccountname").show();
    });
    //add credential type details
    $(document).on("click", ".CredentialTypeplus", function () {
        $("#NewCredentialType").show();
    });

    $(document).on('click', '.credential-control-minus', function () {
        $(this).parent().parent().remove();
    });
    //add vendor credential control clone
    $(document).on("click", ".credential-control-plus", function () {
        var newnode = $("#vendor-credential-control-div").clone().find("input:text").val("").end().insertAfter("div.vendor-credential-control-class:last");
        $(".vendor-credential-control-class:not(:eq(0))  .credential-control-plus").remove();
        $(".vendor-credential-control-class:not(:eq(0))  .credential-control-minus").show();
        var length = $(".vendor-credential-control-class").length;


        newnode.find('#acquireDateid')
            .removeClass('hasDatepicker')
            .removeData('datepicker')
            .removeAttr('id')
            .attr('id','acquireDateid_'+length)
            .datepicker({yearRange: '+0:+100', changeMonth: true, changeYear: true, dateFormat: datepicker})
            .datepicker("setDate", new Date());

        newnode.find('#expirationDateid')
            .removeClass('hasDatepicker')
            .removeData('datepicker')
            .removeAttr('id')
            .attr('id', 'expirationDateid_' + length)
            .datepicker({yearRange: '+0:+100', changeMonth: true, changeYear: true, dateFormat: datepicker})
            .datepicker("setDate", new Date());



    });
    $(document).on("change", ".acquireDateclass", function () {
        var dynclass= $(this).attr('id');
        var dynmid = $("#"+dynclass).closest('div').next().find('.expirationDateclass').attr('id');
        var valuedatepicker=$(this).val();
        $("#"+dynmid).datepicker({dateFormat: datepicker,
            minDate: valuedatepicker,
        });


    });

    $(document).on("change", ".expirationDateclass", function () {
        var dynclass = $(this).attr('id');
        var dynmid = $("#" + dynclass).closest('div').prev().find('.acquireDateclass').attr('id');
        var valuedatepicker = $(this).val();
        $("#"+dynmid).datepicker({dateFormat: datepicker,
            maxDate: valuedatepicker,
        });
    });
    //add vendor notes clone
    $(document).on("click", ".vendor-notes-plus", function () {
        var length = $(".vendor-notes-clone-divclass").length;
        if (length <= 2) {
            var newnode = $("#vendor-notes-clone-div").clone().find(".notes").val("").end().insertAfter("div.vendor-notes-clone-divclass:last");
            $(".vendor-notes-clone-divclass:not(:eq(0))  .vendor-notes-plus").remove();
            $(".vendor-notes-clone-divclass:not(:eq(0))  .vendor-notes-minus").show();
            if (length == 2) {
                $('.vendor-notes-plus').hide();
            }
        } else {
            $('.vendor-notes-plus').hide();
        }
        newnode.find('.notes').on('keydown', function(event) {
            if (this.selectionStart == 0 && event.keyCode >= 65 && event.keyCode <= 90 && !(event.shiftKey) && !(event.ctrlKey) && !(event.metaKey) && !(event.altKey)) {
                var $t = $(this);
                event.preventDefault();
                var char = String.fromCharCode(event.keyCode);
                $t.val(char + $t.val().slice(this.selectionEnd));
                this.setSelectionRange(1,1);
            }
        });

    });

    $(document).on('click', '.vendor-notes-minus', function () {
        $(this).parent().parent().remove();
        $('.vendor-notes-plus').show();
    });
    //close SSN/SIN/ID multiple
    $(document).on("click", ".ssn-sin-minus", function () {
        $(this).parent().hide();
    });
    //close all popups
    $(document).on("click", ".cancelPopup", function () {
        $(this).parent().parent().parent().parent().hide();
    });
    //clear all popups 
    $(document).on('click', '.cancelPopup', function () {
        $(this).parent().parent().parent().find('input[type=text]').val('');
        $(this).parent().parent().parent().find('span').text('');
    });

    // add vendor type
    $(document).on('click', '#NewvendortypeSave', function (e) {
        e.preventDefault();
        var formData = $('#Newvendortype :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/vendor-add-ajax',
            data: {form: formData,
                class: 'addVendor',
                action: 'addVendorType'},
            beforeSend: function (xhr) {
                var res = true;
                // checking portfolio validations
                $(".customValidatePortfoio").each(function () {
                    res = validations(this);
                });

                if (res === false) {
                    xhr.abort();
                    return false;
                }
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#Newvendortype').hide(500);
                    toastr.success(response.message);
                    fetchAllVendorType(response.lastid);
                } else if (response.status == 'error' && response.code == 500) {
                    toastr.warning(response.message);
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });
    // add ethnicity type
    $(document).on('click', '#NewethnicitySave', function (e) {
        e.preventDefault();
        var formData = $('#Newethnicity :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/vendor-add-ajax',
            data: {form: formData,
                class: 'addVendor',
                action: 'addEthnicity'},
            beforeSend: function (xhr) {
                var res = true;
                // checking portfolio validations
                $(".customValidateEthnicity").each(function () {
                    res = validations(this);
                });
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#Newethnicity').hide(500);
                    toastr.success(response.message);
                    fetchAllethnicity(response.lastid);
                } else if (response.status == 'error' && response.code == 500) {
                    toastr.error(response.message);
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });
    $(document).on('click', ".NewaccountnameSave", function (e) {
        e.preventDefault();
        //var formData = $('.Newaccountname :input').serializeArray();
        var formData = $('.Newaccountname :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/vendor-add-ajax',
            data: {form: formData,
                class: 'addVendor',
                action: 'addaccountName'},
            beforeSend: function (xhr) {
                var res = true;
                $(".customValidateAccountName").each(function () {
                    console.log(validations(this));
                    res = validations(this);
                });
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    //$("#Newaccountname").hide();
                    $('.Newaccountname').hide(500);
                    fetchAllAccountName(response.lastid);
                    toastr.success(response.message);
                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Account Name already exists!')
                    toastr.warning(response.message);
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });

    $(document).on('click', "#NewCredentialTypeSave", function (e) {
        e.preventDefault();
        //var formData = $('.Newaccountname :input').serializeArray();
        var formData = $('#NewCredentialType :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/vendor-add-ajax',
            data: {form: formData,
                class: 'addVendor',
                action: 'addCredentialType'},
            beforeSend: function (xhr) {
                var res = true;
                // checking portfolio validations
                //$(".customValidateAccountName").each(function () {
                var validationClass = $(this).parent().parent().parent().parent().parent().find('.customValidatecredentialname :input')
                validationClass.each(function () {
                    res = validations(this);
                });
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    //$("#Newaccountname").hide();
                    $('#NewCredentialType').hide(500);
                    fetchAllcredentialtypes(response.lastid);
                    toastr.success(response.message);
                } else if (response.status == 'error' && response.code == 500) {

                    if(response.status == 'Credential Type already exists!')

                        toastr.warning(response.message);

                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });

    $(document).on('focusout', '.amount', function () {
        var id = this.id;
        if ($('#' + id).val() != '' && $('#' + id).val().indexOf(".") == -1) {
            var bef = $('#' + id).val().replace(/,/g, '');
            var value = numberWithCommas(bef) + '.00';
            $('#' + id).val(value);
        } else {
            var bef = $('#' + id).val().replace(/,/g, '');
            $('#' + id).val(numberWithCommas(bef));
        }
    });

    $('#first_name').on('blur', function () {

        $('#name_on_check').val(this.value);
        $('#tax_payer_name').val(this.value);
    });
    $('#last_name').on('blur', function () {
        var name = $('#name_on_check').val();
        var tax_payer_name = $('#tax_payer_name').val();
        $('#name_on_check').val(name + ' ' + this.value);
        $('#tax_payer_name').val(tax_payer_name + ' ' + this.value);
    });


    $("#birth_date").datepicker({dateFormat: datepicker,
        changeYear: true,
        yearRange: "-100:+20",
        changeMonth: true,
        maxDate: new Date(),
    });

    //cancel add vendor
    $(document).on("click", ".cancel-all", function (e) {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = '/Vendor/Vendor';
            }
        });
    });
    $("#FormSaveNewVendor").validate({
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            vendor_random_id :{
                required: true
            },
            vendor_rate: {
                maxAmount: 10
            },
            'additional_email[]': {
                email: true,
            },
            company_name: {
                required: function () {
                    if ($(".entity-company-checkbox").is(':checked')) {
                        return true;
                    } else {
                        return false;
                    }
                },
            }
        }
    });

    //Property subtype validation
    $(document).on('keyup','.customadditionalEmailValidation',function(){
        validations(this);
    });

//Property AccountType validation
    $(document).on('change','.customCarriervalidations',function(){
        validations(this);
    });

//Property AccountType validation
    $(document).on('keyup','.customPhonenumbervalidations',function(){
        validations(this);
    });

//add vendor ajax
    $("#FormSaveNewVendor").on('submit',function(event){
        event.preventDefault();
        $(".customadditionalEmailValidation").each(function () {
            res = validations(this);
        });
        $(".customCarriervalidations").each(function () {
            res = validations(this);
        });

        $(".customPhonenumbervalidations").each(function () {
            res = validations(this);
        });

        if($("#FormSaveNewVendor").valid()){
            $('#loadingmessage').show();
            // checking carrier field validations
            var vendor_image = $('.tenant_image').html();
            var vendorImage = JSON.stringify(vendor_image);
            var form = $('#FormSaveNewVendor')[0];
            var formData = new FormData(form);
            var data = convertSerializeDatatoArray();
            var count = file_library.length;
            var custom_field = [];
            $(".custom_field_html input").each(function () {
                var data = {'name': $(this).attr('name'), 'value': $(this).val(), 'id': $(this).attr('data_id'), 'is_required': $(this).attr('data_required'), 'data_type': $(this).attr('data_type'), 'default_value': $(this).attr('data_value')};
                custom_field.push(data);
            });
            var custom_field = JSON.stringify(custom_field);
            $.each(file_library, function (key, value) {
                if (compareArray(value, data) == 'true') {
                    formData.append(key, value);
                }
            });
            var is_pmc;
            if ($('.is_pmc').is(":checked")) {
                var is_pmc = '1';
            }
            var use_company_name;
            if ($('.use_company_name').is(":checked")) {
                var use_company_name = '1';
            }
            else{
                var use_company_name = '0';
            }
            formData.append('action', 'addVendor');
            formData.append('class', 'addVendor');
            formData.append('vendor_image', vendorImage);
            formData.append('custom_field', custom_field);
            formData.append('is_pmc', is_pmc);
            formData.append('use_company_name', use_company_name);
            formData.append('stripe_detail_form_data', stripe_detail_form_data);

            $.ajax({
                url: '/vendor-add-ajax',
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function (xhr) {
                    var emailRes = true;
                    var customRes = true;
                    var carrierRes = true;
                    var phoneRes = true;
                    // checking portfolio validations
                    $(".customadditionalEmailValidation").each(function () {
                        emailRes = validations(this);
                    });

                    // checking custom field validations
                    $(".custom_field_html input").each(function () {
                        customRes = validateCustomField($(this).val(), $(this).attr('data_required'), $(this).attr('data_type'), this, this.id);
                    });
                    // checking carrier field validations
                    $(".customCarriervalidations").each(function () {
                        carrierRes = validations(this);
                    });

                    $(".customPhonenumbervalidations").each(function () {
                        phoneRes = validations(this);
                    });

                    if (emailRes === false || customRes === false || carrierRes === false || phoneRes === false) {
                        $('#loadingmessage').hide();
                        xhr.abort();
                        return false;
                    }

                },
                success: function (response) {
                    var response = JSON.parse(response)
                    $('#loadingmessage').hide();
                    if (response.status == 'success' && response.code == 200) {
                        localStorage.setItem("Message", 'Record added successfully.');
                        localStorage.setItem('rowcolor_vendor', 'rowColor');
                        window.location.href = window.location.origin + '/Vendor/Vendor/';

                    } else if (response.message == 'Validation Errors!' && response.code == 400) {
                        $('#loadingmessage').hide();
                        toastr.warning('Email already exits!');
                    }  else if (response.message == 'Validation Errors!' && response.code == 400) {
                        $('#loadingmessage').hide();
                        toastr.warning('Email already exits!');
                    }
                    else if (response.status == 'error' && response.code == 400) {
                        $('#loadingmessage').hide();
                        toastr.warning(response.message);
                    }else if (response.code == 500) {
                        $('#loadingmessage').hide();
                        toastr.warning(response.message);
                    }
                },
                error: function (data) {
                    $('#loadingmessage').hide();
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }

            });
        }
    });



    //update vendor ajax
    $("#updateFormSaveNewVendor").on('submit',function(event){
        event.preventDefault();
        $(".customadditionalEmailValidation").each(function () {
            res = validations(this);
        });
        $(".customCarriervalidations").each(function () {
            res = validations(this);
        });

        $(".customPhonenumbervalidations").each(function () {
            res = validations(this);
        });

        if($("#updateFormSaveNewVendor").valid()){
            // checking carrier field validations


            var vendor_edit_id = $('.vendor_edit_id').val();
            var vendor_image = $('.tenant_image').html();
            var vendorImage = JSON.stringify(vendor_image);
            var form = $('#updateFormSaveNewVendor')[0];
            var formData = new FormData(form);
            var data = convertSerializeDatatoArray();
            var count = file_library.length;
            var custom_field = [];
            $(".custom_field_html input").each(function () {
                var data = {'name': $(this).attr('name'), 'value': $(this).val(), 'id': $(this).attr('data_id'), 'is_required': $(this).attr('data_required'), 'data_type': $(this).attr('data_type'), 'default_value': $(this).attr('data_value')};
                custom_field.push(data);
            });
            var custom_field = JSON.stringify(custom_field);
            $.each(file_library, function (key, value) {
                if (compareArray(value, data) == 'true') {
                    formData.append(key, value);
                }
            });
            var is_pmc;
            if ($('.is_pmc').is(":checked")) {
                var is_pmc = '1';
            }
            var use_company_name;
            if ($('.use_company_name').is(":checked")) {
                var use_company_name = '1';
            }
            else{
                var use_company_name = '0';
            }
            formData.append('action', 'updateVendor');
            formData.append('class', 'addVendor');
            formData.append('vendor_image', vendorImage);
            formData.append('custom_field', custom_field);
            formData.append('is_pmc', is_pmc);
            formData.append('use_company_name', use_company_name);
            formData.append('vendor_edit_id', vendor_edit_id);




            $.ajax({
                url: '/vendor-add-ajax',
                type: 'POST',
                data: formData, cache: false,
                contentType: false,
                processData: false,
                beforeSend: function (xhr) {
                    var emailRes = true;
                    var customRes = true;
                    var carrierRes = true;
                    var phoneRes = true;
                    // checking portfolio validations
                    $(".customadditionalEmailValidation").each(function () {
                        emailRes = validations(this);
                    });

                    // checking custom field validations
                    $(".custom_field_html input").each(function () {
                        customRes = validateCustomField($(this).val(), $(this).attr('data_required'), $(this).attr('data_type'), this, this.id);
                    });
                    // checking carrier field validations
                    $(".customCarriervalidations").each(function () {
                        carrierRes = validations(this);
                    });

                    $(".customPhonenumbervalidations").each(function () {
                        phoneRes = validations(this);
                    });

                    if (emailRes === false || customRes === false || carrierRes === false || phoneRes === false) {
                        xhr.abort();
                        return false;
                    }

                },
                success: function (response) {
                    var response = JSON.parse(response);
                    console.log(response);
                    if (response.status == 'success' && response.code == 200) {
                        localStorage.setItem("Message", 'Record updated successfully.');
                        localStorage.setItem('rowcolor_vendor', 'rowColor');
                        window.location.href = window.location.origin + '/Vendor/Vendor/';

                    } else if (response.message == 'Validation Errors!' && response.code == 400) {
                        toastr.warning('Email already exits!');
                    } else if (response.status == 'error' && response.code == 400) {
                        toastr.warning(response.message);
                    } else if (response.code == 500) {
                        toastr.warning(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }

            });
        }
    });
    //end of ajax
    $(document).on('change', '.entity-company-checkbox', function (e) {
        e.preventDefault();
        if (this.checked) {
            $(".entity-company-check-div").hide();
        } else {
            $(".entity-company-check-div").show();
        }
    });

    var cropper = $('.image-editor').cropit({
        imageState: {
            src: subdomain_url+'500/400',
        },
    });
    // Handle rotation

    $(document).on("click", '.export', function () {
        $(this).parents('.cropItData').hide();
        $(this).parent().prev().val('');
        var dataVal = $(this).attr("data-val");
        var imageData = $(this).parents('.image-editor').cropit('export');
        var image = new Image();
        image.src = imageData;
        $(".popup-bg").hide();
        $(this).parent().parent().prev().find('div').html(image);
    });
    $(document).on("click", ".closeimagepopupicon", function () {
        // $(".popup-bg").hide();
        $(this).parent().hide();
        $(".popup-bg").hide();
    });
    $(document).on("change", ".cropit-image-input", function () {
        photo_videos = [];
        var fileData = this.files;
        var type = fileData.type;
        var elem = $(this);
        $.each(this.files, function (key, value) {
            var type = value['type'];
            var size = isa_convert_bytes_to_specified(value['size'], 'k');
            var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
            var uploadType = 'image';
            var validSize = '1030';
            var arrayType = validImageTypes;

            if ($.inArray(type, arrayType) < 0) {
                toastr.warning('Please select file with valid extension only!');
            } else {

                if (parseInt(size) > validSize) {
                    var validMb = 1;
                    toastr.warning('Please select documents less than ' + validMb + ' mb!');
                } else {
                    size = isa_convert_bytes_to_specified(value['size'], 'k') + 'kb';
                    photo_videos.push(value);
                    var src = '';
                    var reader = new FileReader();
                    $('#photo_video_uploads').html('');


                    $(".popup-bg").show();
                    elem.next().show();

                }

            }

        });

    });

    $(document).on("click", ".additional-add-emergency-contant", function () {

        var clone = $(".additional-tenant-emergency-contact:first").clone();
        clone.find('input[type=text]').val('');
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        $(".additional-tenant-emergency-contact").last().after(clone);
        clone.find(".additional-add-emergency-contant").hide();
        clone.find(".additional-remove-emergency-contant").show();
        clone.find("#otherRelationDiv").hide();
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});

    });



    $(document).on("click", ".additional-remove-emergency-contant", function () {
        $(this).parents(".additional-tenant-emergency-contact").remove();
    });
    $(document).on('change', '#account_type_id', function () {
        var accountSubType = this.value;
        //  console.log('range>>>>', $('#account_type_id').find(':selected').attr('data-rangefrom'));
        accountSubTypeList(accountSubType);
    });

    $(document).on("submit", "#add_chart_account_form_id", function (e) {
        e.preventDefault();
        if ($('#add_chart_account_form_id').valid()) {
            var account_type_id = $('#account_type_id').val();
            var account_code = $('#account_code').val();
            var account_name = $("#com_account_name").val();
            var reporting_code = $("#reporting_code").val();
            var sub_account = $("#sub_account").val();
            var status = $("#status").val();
            var range_from = $('#account_type_id').find(':selected').attr('data-rangefrom');
            var range_to = $('#account_type_id').find(':selected').attr('data-rangeto');
            var posting_status = $("#posting_status").val();
            var chart_account_edit_id = $("#chart_account_edit_id").val();

            var formData = {
                'account_type_id': account_type_id,
                'account_code': account_code,
                'account_name': account_name,
                'reporting_code': reporting_code,
                'sub_account': sub_account,
                'posting_status': posting_status,
                'status': status,
                'range_from': range_from,
                'range_to': range_to,
                'chart_account_edit_id': chart_account_edit_id
            };
            $.ajax({
                type: 'post',
                url: '/property-ajax',
                data: {
                    class: 'propertyDetail',
                    action: 'insertChartofaccount',
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        $("#chartofaccountmodal").modal('hide');
                        $("#add_chart_account_form_id").trigger("reset");

                        fetchChartAccounts(response.lastid);
                        toastr.success(response.message);
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('#' + key).text(value);
                        });
                    } else if (response.status == 'error' && response.code == 503) {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });

    /*Online Payment*/
    $(document).on('click','.onlinePaymentInfo',function(e){
        e.preventDefault();
        var first_name = $('#first_name').val();
        var last_name = $('#last_name').val();
        var company_name = $('.company_name').val();
        var address1 = $('#vAddress1').val();
        var address2 = $('#vAddress2').val();
        var state = $('.states').val();                       																							$('#ffirst_name').val(first_name);
        var city = $('.citys').val();                       																							$('#ffirst_name').val(first_name);
        var zipcode = $('#zip_code').val();
        var phoneNumber = $('#p_number').val();


        $('#ffirst_name').val(first_name);
        $('#flast_name').val(last_name);
        $('#cCompany').val(company_name);
        $('#caddress1').val(address1);
        $('#caddress2').val(address2);
        $('#cphoneNumber').val(phoneNumber);
        $('#ccity').val(city);
        $('.cstate').val(state);
        $('.czip_code').val(zipcode);
    });

    $(document).on('click','#financial-info .basicDiv',function(){
        $(".basic-payment-detail").show();
        $(".apx-adformbox-content").hide();
        $(this).addClass("active");
        $(".paymentDiv").removeClass("active");
    });
    $(document).on('click','#financial-info .paymentDiv',function(){
        $(".basic-payment-detail").hide();
        $(".apx-adformbox-content").show();
        $(this).addClass("active");
        $(".basicDiv").removeClass("active");
    });
    /*Online Payment*/

});
function clearPopUps(id) {
    $(id).find('input[type=text]').val('');
    $(id).find('span').text('');
}
function fetchAllethnicity(id) {

    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {
            class: 'addVendor',
            action: 'fetchAllethnicity'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#ethnicity_options').html(res.data);
            $('.emergency_additional_countryCode option[value=220]').attr('selected', 'selected');
            $('#additional_country option[value=220]').attr('selected', 'selected');
            if (id != false) {
                $('#ethnicity_options').val(id);
            }
        },
    });
}




function fetchAllCarrier(id) {
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {
            class: 'addVendor',
            action: 'fetchAllCarrier'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#contact_carrier').html(res.data);
            if (id != false) {
                $('#contact_carrier').val(id);
            }
        },
    });
}


function fetchAllVendorType(id) {
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {
            class: 'addVendor',
            action: 'fetchAllVendorType'},
        success: function (response) {
            var res = JSON.parse(response);
            $("select[name='vendor_type_id']").html(res.data);
            if (id != false) {
                $('#vendor_type_options').val(id);
            }
        },
    });
}

function fetchAllAccountName(id) {
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {
            class: 'addVendor',
            action: 'fetchAllAccountName'},
        success: function (response) {
            var res = JSON.parse(response);
            $('.bankAccountName').html(res.data);
            if (id != false) {
                $('.bankAccountName').val(id);
            }

        },
    });

}
function fetchAllcredentialtypes(id) {
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {
            class: 'addVendor',
            action: 'fetchAllcredentialtypes'},
        success: function (response) {
            var res = JSON.parse(response);
            $('.credential-options').html(res.data);
            if (id != false) {
                $('.credential-options').val(id);
            }

        },
    });

}
function getRandomNumber(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function isa_convert_bytes_to_specified(bytes, to) {
    var formulas = [];
    formulas['k'] = (bytes / 1024).toFixed(1);
    formulas['M'] = (bytes / 1048576).toFixed(1);
    formulas['G'] = (bytes / 1073741824).toFixed(1);
    return formulas[to];
}
function getAddressInfoByZip(zip) {
    if (zip.length >= 5 && typeof google != 'undefined') {
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': zip}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++) {
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number") {
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment") {
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political") {
                            addr.city = results[0].address_components[ii].short_name;
                        }
                        if (types == "administrative_area_level_1,political") {
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code") {
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political") {
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                     console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    setTimeout(function () {
                        response(addr);
                    }, 2000);

                } else {
                    response({success: false});
                }
            } else {
                response({success: false});
            }
        });
    } else {
        response({success: false});
    }
}
function response(obj) {
    console.log(obj);
    if (obj.success) {

        $('#country').val(obj.country);
        $('.citys').val(obj.city);
        $('.states').val(obj.state);
        $(".cstate ").val(obj.state);
        $("#ccity").val(obj.city);

    } else {
        $('.citys').val('');
        $('.states').val('');
        $('#country').val('');
        $(".cstate ").val('');
        $("#ccity").val('');
    }
}
function fetchAllPhonetypes() {
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {
            class: 'addVendor',
            action: 'fetchAllPhonetypes'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#phone_type_id').html(res.data);

        },
    });

}
function fetchAllCarrier() {
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {
            class: 'addVendor',
            action: 'fetchAllCarrier'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#contact_carrier').html(res.data);

        },
    });

}
function fetchChartAccounts(id) {
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {
            class: 'addVendor',
            action: 'fetchChartAccounts'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#default_security_bank_account').html(res.data);
            if (id != false) {
                $('#default_security_bank_account').val(id);
            }

        },
    });

}
function accountTypeList() {
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {
            class: 'addVendor',
            action: 'getAllAccountType',
        },
        success: function (response) {
            var response = JSON.parse(response);

            if (response.status == 'success' && response.code == 200) {
                $('#account_type_id').empty().append("<option value=''>Select</option>");
                $.each(response.data, function (key, value) {

                    $('#account_type_id').append($("<option data-rangefrom=" + value.range_from + " data-rangeto =" + value.range_to + " value = " + value.id + ">" + value.account_type_name + "</option>"));
                });
            } else {
                toastr.error(response.message);
            }
        }
    });
}
function accountSubTypeList(accountSubType) {
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'getAllAccountSubType',
            id: accountSubType,
        },
        success: function (response) {
            var response = JSON.parse(response);

            if (response.status == 'success' && response.code == 200) {
                $('#sub_account').empty().append("<option value=''>Select</option>");
                $.each(response.data, function (key, value) {
                    $('#sub_account').append($("<option value = " + value.id + ">" + value.account_sub_type + "</option>"));
                })
            } else {
                toastr.error(response.message);
            }
        }
    });
}

function fetchVendorDetails() {
    var vendor_id =$(".vendor_edit_id").val();
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {class: 'addVendor', action: "getVendorDetail", vendor_id: vendor_id},
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == 'success' && res.code == 200) {

                // console.log('000000000000000',res.dataVendors.vendor_type_id);

                $('#Vendorname').text(res.fullname);
                $('#vendor_random_iddata').text(res.randomnumner);
                $.each(res.dataVendors, function (key, value) {
                    // console.log(key+' '+value);
                    $('#' + key).val(value);
                    if (key == 'tenant_image_id') {
                        $('#' + key).html(value);
                    }
                });
                $('#vendor_type_options').val(res.dataVendors.vendor_type_id);

                $("#flag_flag_name").val(res.fullname);
                $("#flag_view_name").val(res.fullname);
                $("#flag_view_phone_number").val(res.dataVendors.phone_number);
                $("#flag_country_code").val(res.dataVendors.country_codeRes);

                $("#phonemainIdDiv").html(res.phoneInfo);
                jQuery('.phone_format').mask('000-000-0000', {reverse: true});

                $(".emergencyDetailMainDiv").html(res.EmergencyInfo);
                $(".credentialMainDiv").html(res.CredentialInfo);
                $('.ssnMainVendor').html(res.gn_ssn);
                $(".emailGetDiv").html(res.gnEmail);
                $("#birth_date").val(res.vendorbirthdate);
                $('.states').val(res.dataVendors.states);
                $('.sendEmail').val(res.dataVendors.vendoremailView).attr('readonly',true);
                $('.citys').val(res.dataVendors.citys);
                if(res.dataVendors.use_company_name == 1){
                    $(".entity-company-check-div").hide();
                }else{
                    $(".entity-company-check-div").show();
                }
                if(res.dataVendors.is_pmc == 1){
                    $("#is_pmc").prop("checked", true);
                }
                getsalutationData(res.dataVendors.salutation);
                if(res.dataVendors.emergency_relation == 9){
                    $("#otherRelationDiv").show();
                }

                if(res.hobbies !='') {
                    $.each(res.hobbies, function (key, value) {
                        $("input[type='checkbox'][value='" + value + "']").prop('checked', true);
                    });
                    $(".multiselect-selected-text").html(res.hobbies.length+' selected')
                    $.each(res.hobbies, function(i,e){
                        $("#hobbies option[value='" + e + "']").prop("selected", true);
                    });
                }

                // alert(res.birthdate);

                var phone =[];
                var html = '';
                $.each(res.dataphone, function (key, value) {
                    phone += value.phone_number +',  ';
                    $('.phone_numbervendor').text(phone);
                });


                setTimeout(function(){
                    edit_date_time(res.updated_at);
                }, 2000);


                defaultFormData=$('#payment_setting_form').serializeArray();
                defaultFormData2=$('#updateFormSaveNewVendor').serializeArray();

            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
function getsalutationData(value){
    if(value == 1)
    {
        $('.hidemaiden').hide();
    }
    if(value==2)
    {
        $('.hidemaiden').hide();
    }
    if(value==3)
    {
        $('.hidemaiden').show();
    }
    if(value==4)
    {
        $('.hidemaiden').hide();
    }
    if(value==5)
    {
        $('.hidemaiden').show();
    }
    if(value==6)
    {
        $('.hidemaiden').hide();
    }
    if(value==7)
    {
        $('.hidemaiden').show();
    }
    if(value==8)
    {
        $('.hidemaiden').show();
    }
    if(value==9)
    {
        $('.hidemaiden').show();
    }
}


/**Online Payment**/
$(document).on('click','.onlinePaymentInfo',function(e){
    e.preventDefault();
    var first_name = $('#first_name').val();
    var last_name = $('#last_name').val();
    var company_name = $('.company_name').val();
    var address1 = $('#vAddress1').val();
    var address2 = $('#vAddress2').val();
    var state = $('.states').val();                                                                                                                   $('#ffirst_name').val(first_name);
    var city = $('.citys').val();                                                                                                                   $('#ffirst_name').val(first_name);
    var zipcode = $('#zip_code').val();
    var phoneNumber = $('#p_number').val();
    $('#ffirst_name').val(first_name);
    $('#flast_name').val(last_name);
    $('#cCompany').val(company_name);
    $('#faddress1').val(address1);
    $('#faddress2').val(address2);
    $('#financialInfo #fphoneNumber').val(phoneNumber);
    $('#financialInfo #f_city').val(city);
    $('#financialInfo #f_state').val(state);
    $('#financialInfo #f_zipCode').val(zipcode);
});
/**Online Payment**/

$(document).ready(function () {
    $(document).on('click','#search_ledger',function () {
        ten_ledger_search();
    });
    $("#ledgerTab").click(function () {
        ten_ledger();
    });
    function ten_ledger() {
        var  user_id= localStorage.getItem('ven_id');
        console.log(user_id);
        var a=$('.st_date').val();
        a = new Date(a);
        var yr=a.getFullYear();
        var mon=a.getMonth();
        var day=a.getDate();
        var st_date=yr+'-'+mon+'-'+day;
        var b= $('.end_date').val();
        b=new Date(b);
        var yr1=b.getFullYear();
        var mon1=b.getMonth() + 1;
        var end_date=yr1+'-'+mon1;
        console.log((st_date,end_date));
        var firstDay = new Date(yr, mon, 1);
        var lastDay = new Date(yr1, mon1, 0);
        st_date=firstDay.getFullYear()+'-'+(firstDay.getMonth()) +'-'+firstDay.getDate();
        end_date=lastDay.getFullYear()+'-'+(lastDay.getMonth()+1) +'-'+lastDay.getDate();
        $.ajax({
            url:'/vendor-add-ajax',
            type: 'POST',
            data: {
                "action": 'gettransactiondata',
                "class": 'addVendor',
                "user_id":user_id,
                'st_date':st_date,
                'end_date':end_date
            },
            success: function (response) {
                var res=JSON.parse(response);
                console.log(res);
                var html="";
                for(var i=0;i<res.data.length;i++){
                    html +='<tr class="ledger_tr">' +
                        '                                                                    <td>'+res.dates[i]+'</td>' +
                        '                                                                    <td>'+res.data[i].transaction_id+'</td>' +
                        '                                                                    <td>'+res.data[i].payment_response+'</td>' +
                        '                                                                    <td>'+res.data[i].reference_no+'</td>' +
                        '                                                                    <td>'+currencySign+''+res.data[i].total_charge_amount+'</td>' +
                        '                                                                </tr>';
                }
                $("#vendor_ledger").html('').append(html);
            }
        });

    }
    function ten_ledger_search() {
        var  user_id= localStorage.getItem('ven_id');
        console.log(user_id);
        var a=$('.st_date').val();
        a = new Date(a);
        var yr=a.getFullYear();
        var mon=a.getMonth();
        var day=a.getDate();
        var st_date=yr+'-'+mon+'-'+day;
        var b= $('.end_date').val();
        b=new Date(b);
        var yr1=b.getFullYear();
        var mon1=b.getMonth() + 1;
        var end_date=yr1+'-'+mon1;
        console.log((st_date,end_date));
        var firstDay = new Date(yr, mon, 1);
        var lastDay = new Date(yr1, mon1, 0);
        st_date=firstDay.getFullYear()+'-'+(firstDay.getMonth()) +'-'+firstDay.getDate();
        end_date=lastDay.getFullYear()+'-'+(lastDay.getMonth()+1) +'-'+lastDay.getDate();
        $.ajax({
            url:'/vendor-add-ajax',
            type: 'POST',
            data: {
                "action": 'gettransactiondatasearch',
                "class": 'addVendor',
                "user_id":user_id,
                'st_date':st_date,
                'end_date':end_date
            },
            success: function (response) {
                var res=JSON.parse(response);
                console.log(res);
                var html="";
                for(var i=0;i<res.data.length;i++){
                    html +='<tr class="ledger_tr">' +
                        '                                                                    <td>'+res.dates[i]+'</td>' +
                        '                                                                    <td>'+res.data[i].transaction_id+'</td>' +
                        '                                                                    <td>'+res.data[i].payment_response+'</td>' +
                        '                                                                    <td>'+res.data[i].reference_no+'</td>' +
                        '                                                                    <td>'+currencySign+''+res.data[i].total_charge_amount+'</td>' +
                        '                                                                </tr>';
                }
                $("#vendor_ledger").html('').append(html);
            }
        });

    }

    $(document).on('click','#exportexcel',function () {
        exportTableToExcel();
    });
    $(document).on('click','#exportpdf',function () {
        exppdf();
    });
    $(document).on('click','#printsample',function () {
        exppdf();
    });
    function exportTableToExcel(tableID, filename = ''){
        var innerHTML=$("#table_data").html();
        var html = innerHTML;
        var blob = new Blob(['\ufeff', html], {
            type: 'application/vnd.ms-excel'
        });

        // Specify link url
        var url = 'data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent(html);

        // Specify file name
        filename = filename?filename+'.xls':'excel.xls';


        // Create download link element
        var downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);

        if(navigator.msSaveOrOpenBlob ){
            navigator.msSaveOrOpenBlob(blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = url;

            // Setting the file name
            downloadLink.download = filename;

            //triggering the function
            downloadLink.click();
        }

        document.body.removeChild(downloadLink);
    }


    function exppdf() {
        var htmls=$("#ledger_table").html();
        console.log('htmls',htmls);
        $.ajax({
            type: 'post',
            url: '/vendorportal/transactions',
            data: {
                class: "VendorPortalAjax",
                action: "getPdfContent",
                htmls: htmls
            },
            success: function (res) {
                var res= JSON.parse(res);
                console.log(res.data);
                if (res.code == 200) {
                    $('.reports-loader').css('visibility','hidden');
                    var link=document.createElement('a');
                    document.body.appendChild(link);
                    link.target="_blank";
                    link.download="Report.pdf";
                    link.href=res.data.record;
                    link.click();
                    $("#wait").css("display", "none");
                } else if(res.code == 500) {
                    //toastr.warning(response.message);
                } else {
                    //toastr.error(response.message);
                }
            }
        });
    }


    function ten_ledger_search() {
        var  user_id= localStorage.getItem('ven_id');
        console.log(user_id);
        var a=$('.st_date').val();
        a = new Date(a);
        var yr=a.getFullYear();
        var mon=a.getMonth();
        var day=a.getDate();
        var st_date=yr+'-'+mon+'-'+day;
        var b= $('.end_date').val();
        b=new Date(b);
        var yr1=b.getFullYear();
        var mon1=b.getMonth() + 1;
        var end_date=yr1+'-'+mon1;
        console.log((st_date,end_date));
        var firstDay = new Date(yr, mon, 1);
        var lastDay = new Date(yr1, mon1, 0);
        st_date=firstDay.getFullYear()+'-'+(firstDay.getMonth()) +'-'+firstDay.getDate();
        end_date=lastDay.getFullYear()+'-'+(lastDay.getMonth()+1) +'-'+lastDay.getDate();
        $.ajax({
            url:'/vendor-add-ajax',
            type: 'POST',
            data: {
                "action": 'gettransactiondatasearch',
                "class": 'addVendor',
                "user_id":user_id,
                'st_date':st_date,
                'end_date':end_date
            },
            success: function (response) {
                var res=JSON.parse(response);
                console.log(res);
                var html="";
                for(var i=0;i<res.data.length;i++){
                    html +='<tr class="ledger_tr">' +
                        '                                                                    <td>'+res.dates[i]+'</td>' +
                        '                                                                    <td>'+res.data[i].transaction_id+'</td>' +
                        '                                                                    <td>'+res.data[i].payment_response+'</td>' +
                        '                                                                    <td>'+res.data[i].reference_no+'</td>' +
                        '                                                                    <td>'+currencySign+''+res.data[i].total_charge_amount+'</td>' +
                        '                                                                </tr>';
                }
                $("#vendor_ledger").html('').append(html);
            }
        });

    }

    $(document).on('click','#exportexcel',function () {
        exportTableToExcel();
    });
    $(document).on('click','#exportpdf',function () {
        exppdf();
    });
    $(document).on('click','#printsample',function () {
        exppdf();
    });
    function exportTableToExcel(tableID, filename = ''){
        var innerHTML=$("#table_data").html();
        var html = innerHTML;
        var blob = new Blob(['\ufeff', html], {
            type: 'application/vnd.ms-excel'
        });

        // Specify link url
        var url = 'data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent(html);

        // Specify file name
        filename = filename?filename+'.xls':'excel.xls';


        // Create download link element
        var downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);

        if(navigator.msSaveOrOpenBlob ){
            navigator.msSaveOrOpenBlob(blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = url;

            // Setting the file name
            downloadLink.download = filename;

            //triggering the function
            downloadLink.click();
        }

        document.body.removeChild(downloadLink);
    }


    function exppdf() {
        var htmls=$("#ledger_table").html();
        console.log('htmls',htmls);
        $.ajax({
            type: 'post',
            url: '/vendorportal/transactions',
            data: {
                class: "VendorPortalAjax",
                action: "getPdfContent",
                htmls: htmls
            },
            success: function (res) {
                var res= JSON.parse(res);
                console.log(res.data);
                if (res.code == 200) {
                    $('.reports-loader').css('visibility','hidden');
                    var link=document.createElement('a');
                    document.body.appendChild(link);
                    link.target="_blank";
                    link.download="Report.pdf";
                    link.href=res.data.record;
                    link.click();
                    $("#wait").css("display", "none");
                } else if(res.code == 500) {
                    //toastr.warning(response.message);
                } else {
                    //toastr.error(response.message);
                }
            }
        });
    }




$(document).on('click','.resetButtonVendor',function () {
    resetEditForm("#payment_setting_form",[],true,defaultFormData,[]);
});
$(document).on('click','.vendorGeneralInformation',function () {
    resetEditForm("#updateFormSaveNewVendor",[],true,defaultFormData2,[]);
});
$(document).on('click','.clearVendorcredentialForm',function(){
    var formid=$(this).attr('rel');
    resetFormClear('#'+formid,[],'div',false);
});


/**
 *Get location on the basis of zipcode
 */

function getAddressInfoByZip(zip){
    if(zip.length >= 5 && typeof google != 'undefined'){
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                         console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    setTimeout(function(){
                        response3(addr);
                    }, 2000);

                } else {
                    response3({success:false});
                }
            } else {
                response3({success:false});
            }
        });
    } else {
        response3({success:false});
    }
}

});

