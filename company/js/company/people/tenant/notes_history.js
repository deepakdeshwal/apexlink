$(document).ready(function () {
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');
    function jqGrid(status, deleted_at) {

        var table = 'tenant_notes';
        var columns = ['Date','Time','Notes','Type','Action'];
        var select_column = [];

        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [];
       // var pagination=[];
        var columns_options = [

            {name:'Date',index:'created_at', width:350,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Time',index:'created_at', width:350,searchoptions: {sopt: conditions},table:table},
            {name:'Notes',index:'notes', width:350, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Type',index:'type', width:350, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Action',index:'', title: false, width:270,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table}
        ];
        var ignore_array = [];
        jQuery("#notes_table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "tenant_notes",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Tenant Notes",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }
    function statusFormatter (cellValue, options, rowObject){

        if (cellValue == 1)
            return "Tenant";
        return "Tenant";

    }
    $(document).on('click','#edit_notes',function () {

        $('#add_notes').show(500);
        $('#edit_notes').hide();

    });
    $(document).on('click','#add_notes',function () {

        $('#add_notes').hide(500);
        $('.notes_grid').hide();
        $('.notes_div').show();


    });   $(document).on('click','#add_notes_history',function () {

        $('#add_notes').hide(500);
        $('.notes_grid').show();
        $('.notes_div').hide();
        $('#edit_notes').show();


    });
    $(document).on('click','#add_notes_history',function () {
        addNotes();


    });
    function addNotes() {
        var tenant_id = $("input[name='tenantapplytaxid']").val();
        var formData = $('#notes_his_form').serializeArray();
        $.ajax({
            type: 'post',
            url: '/tenantOccupantsTypeAjax',
            data: {
                class: "TenantOccupantsAjax",
                action: "addNotes",
                formData: formData,
                tenant_id:tenant_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#notes_table').trigger('reloadGrid');
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $(document).on("click","#notes_his_form .plus-circle",function(){
        var notesRowLenght = $("#notes_his_form  .clone_notes .col-sm-12");
        var clone = $("#notes_his_form  .clone_notes .col-sm-12:first").clone();
        clone.find('textarea[name=notes_text]').text('');
        $("#notes_his_form  .clone_notes .col-sm-12").first().after(clone);
        $("#notes_his_form  .clone_notes .col-sm-12:not(:eq(0)) .plus-circle").remove();
        $("#notes_his_form  .clone_notes .col-sm-12:not(:eq(0)) .minus-circle").show();
    });




    /*Remove Email Textbox*/
    $(document).on("click","#notes_his_form .fa-minus-circle",function(){
        var notesRowLenght = $("#notes_his_form  .clone_notes .col-sm-12");
        $(this).parents("#notes_his_form  .clone_notes .col-sm-12").remove();
    });
});
