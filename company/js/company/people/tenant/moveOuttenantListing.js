$(document).ready(function () {
    $('#jqgridOptions').on('change', function () {
        var selected = this.value;
        $('#MoveOuttenant_listing').jqGrid('GridUnload');
        jqGrid(selected);
    });

    /**
     * jqGrid Intialization function
     * @param status
     */
    jqGrid('All');
			function jqGrid(status) {
				if(jqgridNewOrUpdated == 'true')
			{
			   var sortOrder = 'desc';
			   var sortColumn = 'users.updated_at';
			} else {
			   var sortOrder = 'asc';
			   var sortColumn = 'users.name';
			}
            var table = 'moveouttenant';
            var columns = ['Tenant Name','unit_id','User Id','Property', 'Building Name', 'Unit No.','Status','Schedule Moveout Date','Actual Moveout Date','Unit Available Date','Action'];
            var select_column = ['Receive Payment','Undo Move Out'];
            var joins = [{table:'moveouttenant',column:'user_id',primary:'user_id',on_table:'tenant_property'},{table:'moveouttenant',column:'user_id',primary:'id',on_table:'users'},{table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},{table:'tenant_property',column:'building_id',primary:'id',on_table:'building_detail'},{table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}];
            var conditions = ["eq","bw","ew","cn","in"];
            var extra_where = [{column:'record_status',value:'1',condition:'=',table:'moveouttenant'}];
            //var pagination=[];
            var columns_options = [
                {name:'Tenant Name',index:'id',width:400,align:"center",searchoptions: {sopt: conditions},table:'users',classes: 'pointer',formatter:titleCase},
                {name:'unit_id',index:'unit_id',width:400,align:"center",hidden:true,classes: 'hidden-data-unit', searchoptions: {sopt: conditions},table:'tenant_property'},
                {name:'User Id',index:'user_id',width:400,align:"center",hidden:true,classes: 'hidden-data-userId', searchoptions: {sopt: conditions},table:'moveouttenant'},
                {name:'Property Name',index:'property_name', width:400,classes: 'pointer', align:"left",searchoptions: {sopt: conditions},table:'general_property'},
                {name:'Building Name',index:'building_name',width:400,align:"center",searchoptions: {sopt: conditions},table:'building_detail'},
                {name:'Unit No.',index:'unit_id',width:400,align:"center",searchoptions: {sopt: conditions},table:'tenant_property',formatter:getUnitDetails},
                {name:'Status',index: 'status', width: 400, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyStatus, classes: 'pointer'},
                {name:'Schedule Moveout Date',index:'scheduledMoveOutDate',width:400,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
                {name:'Actual Moveout Date',index:'actualMoveOutDate',width:400,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
                {name:'Unit Available Date',index:'unitAvailableDate',width:400,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date',attr:[{name:'unit_id',value:'unit_id'},{name:'user_id',value:'user_id'}]},
                {name:'Action',index:'select',width:430,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,formatter: 'select', edittype: 'select',search:false,table:table,title:true}
            ];
            var ignore_array = [];
            jQuery("#MoveOuttenant_listing").jqGrid({
                url: '/List/jqgrid',
                datatype: "json",
                height: '100%',
                autowidth: true,
                colNames: columns,
                colModel: columns_options,
                pager: true,
                //sortname: 'updated_at',
                mtype: "POST",
                postData: {
                    q: 1,
                    class: 'jqGrid',
                    action: "listing_ajax",
                    table: table,
                    select: select_column,
                    columns_options: columns_options,
                    status: status,
                    ignore: ignore_array,
                    joins: joins,
                  //  extra_columns:extra_columns,
                    extra_where:extra_where
                },
                viewrecords: true,
                sortname: sortColumn,
				sortorder: sortOrder,
                sorttype: 'date',
                sortIconsBeforeText: true,
                headertitles: true,
                rowNum: pagination,
                rowList: [5, 10, 20, 30, 50, 100, 200],
                caption: "Tenant Move Out Details",
                pginput: true,
                pgbuttons: true,
                navOptions: {
                    edit: false,
                    add: false,
                    del: false,
                    search: true,
                    filterable: true,
                    refreshtext: "Refresh",
                    reloadGridOptions: {fromServer: true}
                }
            }).jqGrid("navGrid",
                {
                    edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
                },
                {}, // edit options
                {}, // add options
                {}, //del options
                {top:0,left:400,drag:true,resize:false} // search options
            );
        }

    /**
     * function to format property status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyStatus(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "Notice Given";
        else if (cellValue == '2')
            return "MoveOut Recorded";
        else
            return '';
    }

    function titleCase( cellvalue, options, rowObject) {
        if (cellvalue !== undefined) {

            var string = "";
            $.ajax({
                type: 'post',
                url: '/Tenantlisting/getUserNameById',
                data: {
                    class: "TenantAjax",
                    action: "getUserNameById",
                    id: cellvalue
                },
                async: false,
                success: function (response) {
                    var res = JSON.parse(response);
                    if (res.status == "success") {
                        string = res.data;
                    }
                }
            });
            return string;
        }
    }

    function getUnitDetails( cellvalue, options, rowObject){
        var string = "";
        if (cellvalue != undefined) {
            $.ajax({
                type: 'post',
                url: '/Tenantlisting/getUnitById',
                data: {
                    class: "TenantAjax",
                    action: "getUnitById",
                    id: cellvalue
                },
                async: false,
                success: function (response) {
                    var res = JSON.parse(response);
                    if (res.status == "success") {
                        string = res.data.unit_prefix + "-" + res.data.unit_no;
                    }
                }
            });
        }
        return string;
    }

    function titleCase( cellvalue, options, rowObject) {
        if (cellvalue !== undefined) {

            var string = "";
            $.ajax({
                type: 'post',
                url: '/Tenantlisting/getUserNameById',
                data: {
                    class: "TenantAjax",
                    action: "getUserNameById",
                    id: cellvalue
                },
                async: false,
                success: function (response) {
                    var res = JSON.parse(response);
                    if (res.status == "success") {
                        string = res.data;
                    }
                }
            });
            return string;
        }
    }

    /*Work on dropdown list Start*/
    $(document).on("change", "#MoveOuttenant_listing .select_options", function (e) {
        e.preventDefault();
        var action = this.value;
        var idData = $(this).attr('data_id');
        var unit_id = $(this).attr('unit_id');
        var user_id = $(this).attr('user_id');

        var hidden_unitId = unit_id;

        switch(action) {

            case "Undo Move Out":
            checkDataUnitDetails(hidden_unitId, idData);
                //bootbox.alert("Property is not vacent!!");

                break;

            default:
                window.location.href = '/Tenant/MoveOut';
        }

        return false;
    });
    /*Work on dropdown list End*/

    function checkDataUnitDetails(hidden_unitId,idData) {
        var id = hidden_unitId;


        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "checkDataUnitDetails",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log(response.data.building_unit_status);
                if (response.status == 'success' && response.code == 200) {
                    if(response.data.building_unit_status == '1'){
                        bootbox.confirm("Do you sure you want to undo this move out?", function (result) {
                            if (result == true) {
                                undoMoveoutTenant(idData);
                                $('#MoveOuttenant_listing').trigger('reloadGrid');
                            }
                        });
                    }else{
                        toastr.warning("Cannot undo move out because move out date is in the past.");
                    }

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /*Move Out Tenant Undo Steps Start*/

    function undoMoveoutTenant(idData) {       // alert(idUser);

        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "undoMoveoutTenant",
                id: idData
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("Record Saved Successfully!!");
                    $('#MoveOuttenant_listing').trigger('reloadGrid');
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    /*Date lease Table*/

    function hhh(endDate){

        var todayDate = new Date();
        var endDate1 = $.datepicker.parseDate('mm/dd/yy', endDate);

        var result = todayDate - endDate;
        console.log(result);

    }
    function myDateFormatter(dateObject){
        var newdate = dateObject.getFullYear() + "-" + (dateObject.getMonth() + 1) + "-" + dateObject.getDate();
        var tmp = [];
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/getDateFormat',
            data: {class: "TenantAjax",action: "getDateformat",date: newdate,},
            async: false,
            global:false,
            success: function (response) {
                tmp = $.parseJSON(response);
            }
        });
        return tmp;
    }
    /*Date Lease Table*/


    /*Move Out Tenant Undo Steps End*/
    $(document).on('click','#MoveOuttenant_listing tr td:not(:last-child)',function(e){
        e.preventDefault();
        var base_url = window.location.origin;
        var id = $(this).closest("tr").attr('id');
        window.location.href = base_url + '/Tenant/MoveOutTenant?id='+id;

    });

});

