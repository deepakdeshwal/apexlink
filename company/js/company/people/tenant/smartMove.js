$(document).ready(function(){
    smartMove();
    $(document).on("change","input[name='bankrupters_check']",function(){
        if ($(this).is(":checked")){
            $("textarea[name='bankrupters_text']").attr("readonly",false);
        }else{
            $("textarea[name='bankrupters_text']").attr("readonly",true);
        }
    });

    $(document).on('click','#btnBackgroundSelection',function () {
        var tent_id = $(this).attr("tent_id");
        var smartValue = $("#backGroundCheckPopCondition input[name='background_type']:checked").val();
        if (smartValue == "1"){
            $("#backGroundCheckPopCondition").modal('hide');
            $("#sm-mainpage .landlord_account").attr('tent_id',tent_id);
            $("#sm-mainpage .landlord_account1").attr('tent_id',tent_id);
            $("#sm-mainpage").modal('show');
        }
        if (smartValue == "2"){
            $('#backGroundCheckPopCondition').modal('hide');
            $('#backGroundCheckPop').modal('show');
        }
    });

    $(document).on('click','.landlord_account',function () {
        var tent_id = $(this).attr("tent_id");
        $("#sm-mainpage").modal('hide');
        $("#smsignup-modal").modal('show');
        $("#smsignup-modal .saveAccountType").attr('tent_id',tent_id);
        //getLandLordEmail(tent_id,'#smsignup-modal input[name="email"]');
    });

    $(document).on('click','.saveAccountType',function () {
        if ($("#landlord_signup_form").valid()){
            var tent_id = $(this).attr("tent_id");
            $("#smsignup-modal").modal('hide');
            $("#newproperty-modal").modal('show');
            $("#newproperty-modal .save_property").attr('tent_id',tent_id);
            var landlord = $("select[name='landlord']").val();
            var email = $("input[name='email']").val();
            var accept_term = $("input[name='accept_term']").val();
            var tent_id = $(this).attr('tent_id');
            saveLandlordAccountInfo(landlord, email, accept_term, tent_id);
            return false;
        }
        return false;
    });

    $(document).on('click','.getlandlordinfo',function () {
        var tent_id = $(this).attr("tent_id");
        $("#smsignup-modal").modal('hide');
        $("#multistep-signup-modal").modal('show');
        $("#multistep-signup-modal .nextBtn").attr('tent_id',tent_id);
        getlandlordinfo(tent_id);
    });

    $(document).on('click','.confirm_submit_account',function () {
        var tent_id = $(this).attr("tent_id");
        $("#multistep-signup-modal").modal('hide');
        $("#newproperty-modal").modal('show');
        $("#newproperty-modal .save_property").attr('tent_id',tent_id);
        getPropertyDetailsByTenant(tent_id);
    });

    $(document).on("click",".save_property",function () {
        var textVal = $("#newproperty-modal textarea[name='bankrupters_text']").val();
        if (textVal > 12) {
            $("#newproperty-modal textarea[name='bankrupters_text']").val("");
            $("#bankrupters_text-error").show();
            return false;
        }else{
            $("#bankrupters_text-error").hide();
        }
        var tent_id = $(this).attr("tent_id");
        var property_id = $("#newproperty-modal #property_form input[name='property_id']").val();
        var address1 = $("#newproperty-modal #property_form input[name='address1']").val();
        var city = $("#newproperty-modal #property_form input[name='city']").val();
        var state = $("#newproperty-modal #property_form select[name='state']").val();
        var zipcode = $("#newproperty-modal #property_form input[name='zipcode']").val();
        var property_db_id = $("#newproperty-modal #property_form select[name='property_list']").val();
        var unit_db_id = $("#newproperty-modal #property_form select[name='unit_list']").val();
        var serverTime = getServerTime();
        var securityHeaderToken = getSecurityHeaders(serverTime);
        var propertyData = getPropertyAllDetails(tent_id);

        var checkSMPropertyExist = getSMPropertyById( serverTime, securityHeaderToken, property_db_id, unit_db_id );
        if (checkSMPropertyExist == "SUCCESS") {
            var smPropertyApiRes = createProperty( serverTime, securityHeaderToken, propertyData[0]);
            console.log("smPropertyApiRes",smPropertyApiRes);
            savePropertyData(tent_id, smPropertyApiRes);
        }else{
            toastr.warning("Property unit is already created!");
            return false;
        }

        $("#newproperty-modal").modal('hide');
        $("#verify-property").modal('show');
        $("#verify-property .property_id").text(property_id);
        $("#verify-property .address1").text(address1);
        $("#verify-property .city").text(city);
        $("#verify-property .state").text(state);
        $("#verify-property .zipcode").text(zipcode);
        $("#verify-property #verify-button2").attr('tent_id',tent_id);
    });

    $(document).on("click","#verify-button2",function () {
        var tent_id = $(this).attr('tent_id');
        $("#verify-property").modal('hide');
        $("#newproperty-modal").modal('hide');
        $("#newapp-property").modal('show');
        $("#newapp-property .save_application_btn").attr('tent_id',tent_id);
        getApplicationDetailsByTenant(tent_id);
    });

    $("#application_form").validate({
        rules: {'pay_by':{required:true}},
        messages: {"pay_by": "Please select Payee!"},
        errorLabelContainer: '.errorTxt'
    });

    $(document).on("click",".save_application_btn",function () {
        var tent_id = $(this).attr('tent_id');
        if($("#application_form").valid()){
            $("#newapp-property").modal('hide');
            var payBy = $("input[name='pay_by']:checked").val();
            console.log("payBy = "+payBy);
            var formData = $('#application_form').serializeArray();
            var getSMProperty = getSMPropertyId(tent_id, formData);
            var serverTime = getServerTime();
            var securityHeaderToken = getSecurityHeaders(serverTime);
            var createApplicationRes = createApplication(getSMProperty, serverTime, securityHeaderToken, payBy);
            var saveApplicationRes = saveApplication(tent_id, createApplicationRes);
            if (saveApplicationRes['code'] == 200) {
                toastr.success(saveApplicationRes['message']);
            }
            if (payBy == '2'){
                $("#pm_payment").modal('show');
                $("#pm_payment .pm_payment_btn").attr('tent_id',tent_id);
                sendEmailToRenter(tent_id);
            }else{
                sendEmailToRenter(tent_id);
            }
        }
        return false;
    });

    $(document).on("click","#pm_payment .pm_payment_btn", function(){
        var tent_id = $(this).attr('tent_id');
        sendEmailToRenter(tent_id);
        $("#pm_payment").modal('hide');
        toastr.success("Email sent to renter");
        return false;
    });

    $(document).on('click','.confirm_info_details',function () {
        var first_name = $("#multistep-signup-modal #smartmove_personal_info input[name='first_name']").val();
        var last_name = $("#multistep-signup-modal #smartmove_personal_info input[name='last_name']").val();
        var middle_name = $("#multistep-signup-modal #smartmove_personal_info input[name='middle_name']").val();
        var address1 = $("#multistep-signup-modal #smartmove_personal_info input[name='address1']").val();
        var address2 = $("#multistep-signup-modal #smartmove_personal_info input[name='address2']").val();
        var city = $("#multistep-signup-modal #smartmove_personal_info input[name='city']").val();
        var state = $("#multistep-signup-modal #smartmove_personal_info select[name='state']").val();
        var zipcode = $("#multistep-signup-modal #smartmove_personal_info input[name='zipcode']").val();
        var phone_number = $("#multistep-signup-modal #smartmove_personal_info input[name='phone_number']").val();
        var phone_number2 = $("#multistep-signup-modal #smartmove_personal_info input[name='phone_number2']").val();
        var phone_number3 = $("#multistep-signup-modal #smartmove_personal_info input[name='phone_number3']").val();
        var extension = $("#multistep-signup-modal #smartmove_personal_info input[name='extension']").val();
        var units = $("#multistep-signup-modal #smartmove_personal_info input[name='units']").val();
        var position = $("#multistep-signup-modal #smartmove_personal_info select[name='position']").val();
        var role = $("#multistep-signup-modal #smartmove_personal_info select[name='role']").val();
        var company_name = $("#multistep-signup-modal #smartmove_personal_info input[name='company_name']").val();
        var company_address1 = $("#multistep-signup-modal #smartmove_personal_info input[name='company_address1']").val();
        var company_address2 = $("#multistep-signup-modal #smartmove_personal_info input[name='company_address2']").val();
        var company_city = $("#multistep-signup-modal #smartmove_personal_info input[name='company_city']").val();
        var company_state = $("#multistep-signup-modal #smartmove_personal_info select[name='company_state']").val();
        var company_zipcode = $("#multistep-signup-modal #smartmove_personal_info input[name='company_zipcode']").val();

        $("#multistep-signup-modal #step-3 .sh_first_name").text(first_name);
        $("#multistep-signup-modal #step-3 .sh_last_name").text(last_name);
        $("#multistep-signup-modal #step-3 .sh_middle_name").text(middle_name);
        $("#multistep-signup-modal #step-3 .sh_address1").text(address1);
        $("#multistep-signup-modal #step-3 .sh_address2").text(address2);
        $("#multistep-signup-modal #step-3 .sh_city").text(city);
        $("#multistep-signup-modal #step-3 .sh_state").text(state);
        $("#multistep-signup-modal #step-3 .sh_zipcode").text(zipcode);
        $("#multistep-signup-modal #step-3 .sh_phone_number").text(phone_number);
        $("#multistep-signup-modal #step-3 .sh_phone_number2").text(phone_number2);
        $("#multistep-signup-modal #step-3 .sh_phone_number3").text(phone_number3);
        $("#multistep-signup-modal #step-3 .sh_extension").text(extension);
        $("#multistep-signup-modal #step-3 .sh_units").text(units);
        $("#multistep-signup-modal #step-3 .sh_position").text(position);
        $("#multistep-signup-modal #step-3 .sh_role").text(role);
        $("#multistep-signup-modal #step-3 .sh_company_name").text(company_name);
        $("#multistep-signup-modal #step-3 .sh_company_address1").text(company_address1);
        $("#multistep-signup-modal #step-3 .sh_company_address2").text(company_address2);
        $("#multistep-signup-modal #step-3 .sh_company_city").text(company_city);
        $("#multistep-signup-modal #step-3 .sh_company_state").text(company_state);
        $("#multistep-signup-modal #step-3 .sh_company_zipcode").text(company_zipcode);
    });

    var navListItems = $('div.setup-panel div a'),allWells = $('.setup-content'),allNextBtn = $('.nextBtn');
        allWells.hide();
        navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),$item = $(this);
        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),curStepBtn = curStep.attr("id"),nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),curInputs = curStep.find("input[type='text'],input[type='url']"),isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
    $('div.setup-panel div a.btn-primary').trigger('click');

    $("#landlord_signup_form").validate({
        rules: {'accept_term':{required:true}},
        messages: {"accept_term": "Accept term & conditions."}
    });

    $(document).on("blur","input[name='l_password']",function () {
        var value = $(this).val();
        var resp = CheckPassword(value);
        if (resp === false){
            $(".pass_error").text("8 to 15 character long including one capital letter, one lower case letter, and one number or special character");
        }else{
            $(".pass_error").text("");
        }
    });

    $(document).on("keyup","input[name='lc_password']",function () {
        var value = $(this).val();
        var password1 = $("input[name='l_password']").val();
        var resp = checkConfirmPassword(password1,value);
        $(".cpass_error").html(resp);
    });

    $(document).on("change","select[name='security_question_1']",function(){
        var currentvalue = $(this).val();
        var secondQuestion = $("select[name='security_question_2']").val();
        var thirdQuestion = $("select[name='security_question_3']").val();
        if (currentvalue != '' && (currentvalue == secondQuestion || currentvalue == thirdQuestion)){
            $(this).val("");
        }
    });
    $(document).on("change","select[name='security_question_2']",function(){
        var currentvalue = $(this).val();
        var secondQuestion = $("select[name='security_question_1']").val();
        var thirdQuestion = $("select[name='security_question_3']").val();
        if (currentvalue != '' && (currentvalue == secondQuestion || currentvalue == thirdQuestion)){
            $(this).val("");
        }
    });
    $(document).on("change","select[name='security_question_3']",function(){
        var currentvalue = $(this).val();
        var secondQuestion = $("select[name='security_question_2']").val();
        var thirdQuestion = $("select[name='security_question_1']").val();
        if (currentvalue != '' && (currentvalue == secondQuestion || currentvalue == thirdQuestion)){
            $(this).val("");
        }
    });

    $(document).on("click","#btn-circle1,#btn-circle2", function () {
        $("."+$(this).attr("id")).trigger("click");
    });
});

function CheckPassword(inputtxt){
    var decimal=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
    if(inputtxt.match(decimal)){
        return true;
    }else{
        return false;
    }
}

function checkConfirmPassword(password1,password2) {
    if (password1 == '')
        return "<span style='color: red;'>Please enter Password</span>";
    else if (password2 == '')
        return "<span style='color: red;'>Please enter confirm password</span>";
    else if (password1 != password2) {
        return "<span style='color: red;'>Password did not match: Please try again...</span>";
    }else{
        return "<span style='color: green;'>Password Matched!</span>";
    }
}

function sendEmailToRenter(tent_id){
    $.ajax({
        type: 'post',
        url: '/smartmove',
        data: {
            class: "SmartMove",
            action: "sendEmailToRenter",
            id:tent_id
        },
        success: function (data) {
            var res = JSON.parse(data);
            if(res.status == "success" && res.code == 200){
            }else{
                toastr.error(res.message);
            }
        },error: function (data) {}
    });
}

function getApplicationDetailsByTenant(tent_id){
    $.ajax({
        type: 'post',
        url: '/smartmove',
        data: {
            class: "SmartMove",
            action: "getApplicationDetailsByTenant",
            id:tent_id
        },
        success: function (data) {
            var res = JSON.parse(data);
            if(res[0].status == "success" && res[0].code == 200){
                var result = res[0].data;
                $("#application_form input[name='app_property_id']").val(result.property_id);

                var address = "";
                if (result.address1 != "" && result.address2 == "" && result.city == "" && result.state == "" && result.zipcode == ""){
                    address = result.address1;
                }else if(result.address1 != "" && result.address2 != "" && result.city == "" && result.state == "" && result.zipcode == ""){
                    address = result.address1+", "+result.address2;
                }else if(result.address1 != "" && result.address2 != "" && result.city != "" && result.state == "" && result.zipcode == ""){
                    address = result.address1+", "+result.address2+", "+result.city;
                }else if(result.address1 != "" && result.address2 != "" && result.city != "" && result.state != "" && result.zipcode == ""){
                    address = result.address1+", "+result.address2+", "+result.city+", "+result.state;
                }else if(result.address1 != "" && result.address2 != "" && result.city != "" && result.state != "" && result.zipcode != ""){
                    address = result.address1+", "+result.address2+", "+result.city+", "+result.state+" "+result.zipcode;
                }else{
                    address = result.address1+", "+result.address2+", "+result.city+", "+result.state+" "+result.zipcode;
                }
                $("#application_form textarea[name='property_address']").val(address);
                var unitNumber = "";
                if (result.unit_no != "" && result.unit_prefix == ""){
                    unitNumber = result.unit_no;
                }else if(result.unit_no == "" && result.unit_prefix != ""){
                    unitNumber = result.unit_prefix;
                }else if(result.unit_no != "" && result.unit_prefix != ""){
                    unitNumber = result.unit_prefix+"-"+result.unit_no;
                }else{
                    unitNumber = '';
                }
                var unitHtml = "<option value='"+result.unitId+"'>"+unitNumber+"</option>";
                $("#application_form select[name='unit']").html(unitHtml);
                $("#application_form input[name='rent_amount']").val(result.rent_amount);
                $("#application_form input[name='security_deposite']").val(result.security_deposite);
                var termHtml = "<option value='"+result.term+"'>"+result.term+"</option>";
                $("#application_form select[name='lease_term']").html(termHtml);
                $("#application_form input[name='tenant_email']").val(result.email);
                $("#application_form input[name='additional_tenants_emails']").val(result.occupants);
                $("#application_form input[name='guarantors_emails']").val(result.guarantors);
                $("#application_form .full_credit_report").text(" $35.00 X "+result.manager_id+" applicants ");
            }else{
                toastr.error(res[0].message);
            }
        },error: function (data) {}
    });
}

function getPropertyAllDetails(tent_id){
    var propertyObj = [];
    $.ajax({
        type: 'post',
        url: '/smartmove',
        async: false,
        data: {
            class: "SmartMove",
            action: "getTenantPorpertyInfo",
            tent_id:tent_id
        },
        success: function (data) {
            var res = JSON.parse(data);
            if(res.status == "success" && res.code == 200){
                propertyObj.push(res.data);
            }else{
                toastr.error(res.message);
            }
        },error: function (data) {}
    });
    return propertyObj;
}

function getPropertyDetailsByTenant(tent_id){
    $.ajax({
        type: 'post',
        url: '/smartmove',
        data: {
            class: "SmartMove",
            action: "getPropertyDetailsByTenant",
            id:tent_id
        },
        success: function (data) {
            var res = JSON.parse(data);
            if(res[0].status == "success" && res[0].code == 200){
                var result = res[0].data;
                var propertNameHtml = "<option value='"+result.property_name+"'>"+result.property_name+"</option>";
                $("#newproperty-modal #property_form select[name='property_list']").html(propertNameHtml);
                var unitNumber = "";
                if (result.unit_no != "" && result.unit_prefix == ""){
                    unitNumber = result.unit_no;
                }else if(result.unit_no == "" && result.unit_prefix != ""){
                    unitNumber = result.unit_prefix;
                }else if(result.unit_no != "" && result.unit_prefix != ""){
                    unitNumber = result.unit_prefix+"-"+result.unit_no;
                }else{
                    unitNumber = '';
                }
                var unitHtml = "<option value='"+unitNumber+"'>"+unitNumber+"</option>";
                $("#newproperty-modal #property_form select[name='unit_list']").html(unitHtml);
                $("#newproperty-modal #property_form input[name='property_id']").val(result.property_id);
                $("#newproperty-modal #property_form input[name='address1']").val(result.address1);
                $("#newproperty-modal #property_form input[name='address2']").val(result.address2);
                $("#newproperty-modal #property_form input[name='city']").val(result.city);
                var stateHtml = "<option value='"+result.state+"'>"+result.state+"</option>";
                $("#newproperty-modal #property_form select[name='state']").html(stateHtml);
                $("#newproperty-modal #property_form input[name='zipcode']").val(result.zipcode);
                $("#newproperty-modal .welcome_pm").text(result.manager_id);
            }else{
                toastr.error(res[0].message);
            }
        },error: function (data) {}
    });
}

function getlandlordinfo(landlordId){
    $.ajax({
        type: 'post',
        url: '/smartmove',
        data: {
            class: "SmartMove",
            action: "getlandlordinfo",
            id:landlordId
        },
        success: function (data) {
            var res = JSON.parse(data);
            if(res[0].status == "success" && res[0].code == 200){
                var result = res[0].data;
                $("#multistep-signup-modal #smartmove_personal_info input[name='first_name']").val(result.first_name);
                $("#multistep-signup-modal #smartmove_personal_info input[name='last_name']").val(result.last_name);
                $("#multistep-signup-modal #smartmove_personal_info input[name='middle_name']").val(result.middle_name);
                $("#multistep-signup-modal #smartmove_personal_info input[name='address1']").val(result.address1);
                $("#multistep-signup-modal #smartmove_personal_info input[name='address2']").val(result.address2);
                $("#multistep-signup-modal #smartmove_personal_info input[name='city']").val(result.city);
                var stateHtml = "<option value='"+result.state+"'>"+result.state+"</option>";
                $("#multistep-signup-modal #smartmove_personal_info select[name='state']").html(stateHtml);
                $("#multistep-signup-modal #smartmove_personal_info input[name='zipcode']").val(result.zipcode);
                $("#multistep-signup-modal #smartmove_personal_info input[name='phone_number']").val(result.phone_number);
                if (result.work_phone_extension != 0){
                    $("#multistep-signup-modal #smartmove_personal_info input[name='extension']").val(result.work_phone_extension);
                }
                if (result.number_of_units != ""){
                    var number_of_units = $.trim(result.number_of_units.split('-')[1]);
                    $("#multistep-signup-modal #smartmove_personal_info input[name='units']").val(number_of_units);
                }
                var postionHtml = "<option value='Property Manager'>Property Manager</option>";
                $("#multistep-signup-modal #smartmove_personal_info select[name='position']").html(postionHtml);
                $("#multistep-signup-modal #smartmove_personal_info input[name='company_name']").val(result.company_name);
                $("#multistep-signup-modal #smartmove_personal_info input[name='company_address1']").val(result.address1);
                $("#multistep-signup-modal #smartmove_personal_info input[name='company_address2']").val(result.address2);
                $("#multistep-signup-modal #smartmove_personal_info input[name='company_city']").val(result.city);
                $("#multistep-signup-modal #smartmove_personal_info select[name='company_state']").html(stateHtml);
                $("#multistep-signup-modal #smartmove_personal_info input[name='company_zipcode']").val(result.zipcode);
            }else{
                toastr.error(res[0].message);
            }
        },error: function (data) {}
    });
}

function smartMove() {
    $.ajax({
        type: 'post',
        url: '/smartmove',
        data: {
            class: "SmartMove",
            action: "getTenantById",
            id:40
        },
        success: function (data) {
            var res = JSON.parse(data);
            res = res[0];
            var renter = {
                'Email':"test1@yopmail.com",
                'FirstName':res.first_name,
                'MiddleName':res.middle_name,
                'LastName':res.last_name,
                'DateOfBirth':res.dob,
                'SocialSecurityNumber':res.ssn_sin_id,
                'StreetAddressLineOne':res.address1,
                'StreetAddressLineTwo':res.address2,
                'City':res.city,
                'State':res.state,
                'Zip':res.zipcode,
                'HomePhoneNumber':res.phone_number,
                'OfficePhoneNumber':res.phone_number,
                'OfficePhoneExtension':res.phone_number,
                'MobilePhoneNumber':res.phone_number,
                'Income':'120000',
                'IncomeFrequency':'Annual',
                'OtherIncome':100,
                'OtherIncomeFrequency':'Monthly',
                'AssetValue':35000,
                'FcraAgreementAccepted':true
            };
            //getServerTime();
        },error: function (data) {}
    });
}

function getServerTime(){
    var serverTime = "";
    $.ajax({
        url: 'https://cors-anywhere.herokuapp.com/https://smlegacygateway-integration.mysmartmove.com/LandlordApi/v3/ServerTime',
        type: 'get',
        async: false,
        success: function (response, status, xhr) {
            var serverTimes = response.split(".");
            serverTime = serverTimes[0];
        },
        error: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect. Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
        }
    });
    return serverTime;
}

function createProperty( serverTime, securityToken, propertyData) {
    var propertyAddress = "";
    if (propertyData['property_address1'] != "" && propertyData['property_address2'] == "") {
        propertyAddress = propertyData['property_address1'];
    }else if(propertyData['property_address1'] == "" && propertyData['property_address2'] != ""){
        propertyAddress = propertyData['property_address2'];
    }else if(propertyData['property_address1'] != "" && propertyData['property_address2'] != ""){
        propertyAddress = propertyData['property_address1']+" "+propertyData['property_address2'];
    }
    if (propertyData['property_phone_number'] == "") {
        propertyData['property_phone_number'] = "9999999999";
    }
    var unitNumber = "";
    if (propertyData['unit_no'] != "" && propertyData['unit_prefix'] == "") {
        unitNumber = propertyData['unit_no'];
    }else if (propertyData['unit_no'] == "" && propertyData['unit_prefix'] != "") {
        unitNumber = propertyData['unit_prefix'];
    }else if (propertyData['unit_no'] != "" && propertyData['unit_prefix'] != "") {
        unitNumber = propertyData['unit_no']+"-"+propertyData['unit_prefix'];
    }
    var propertyStateForm = getStateSortForm(propertyData['property_state']);
    if (propertyStateForm == undefined ) {
        propertyStateForm = "NY";
    }
    var landlordStateForm = getStateSortForm(propertyData['landlord']['state']);
    if (landlordStateForm == undefined ) {
        landlordStateForm = "NY";
    }
    
    var landlordAddress1 = addressFormat(propertyData['landlord']['address1']);
    var landlordAddress2 = addressFormat(propertyData['landlord']['address2']);
    console.log("propertyData",propertyData);
    var property = {
        "PropertyIdentifier": propertyData['property_name'],
        "OrganizationName": propertyData['company_name'],
        "OrganizationId": 0,
        "Active": "true",
        "Name": propertyData['legal_name'],
        "Street": propertyAddress,
        "City": propertyData['property_city'],
        "State": propertyStateForm, //-------- two character
        "Zip": propertyData['property_zipcode'],
        "Phone": propertyData['property_phone_number'],
        "PhoneExtension": "",
        "UnitNumber": unitNumber,
        "Landlord": {
            "FirstName": propertyData['landlord']['first_name'],
            "LastName": propertyData['landlord']['last_name'],
            "StreetAddressLineOne": landlordAddress1,
            "StreetAddressLineTwo": landlordAddress2,
            "City": propertyData['landlord']['city'],
            "State": landlordStateForm,
            "Zip": propertyData['landlord']['zipcode'],
            "PhoneNumber": propertyData['landlord']['phone_number'],
            "Email": propertyData['landlord']['email']
        },
        "Classification": "Conventional",
        "IR": "2",
        "IncludeMedicalCollections": true,
        "IncludeForeclosures": "true",
        "OpenBankruptcyWindow": "6",
        "IsFcraAgreementAccepted": "true",
        "DeclineForOpenBankruptcies": true
    };
    var createPorpertyResponse;
    var headerset = 'smartmovepartner partnerId="212", serverTime="'+serverTime+'", securityToken="'+securityToken+'"';
    $.ajax({
        url: 'https://cors-anywhere.herokuapp.com/https://smlegacygateway-integration.mysmartmove.com/LandlordApi/v3/Property',
        type: 'post',
        dataType: 'json',
        accepts: { json: 'application/json' },
        data: property,
        async: false,
        beforeSend: function (xhr) { xhr.setRequestHeader ("Authorization", headerset); },
        success: function (data) {
            createPorpertyResponse = data;
            //{"PropertyId":383007,"PropertyIdentifier":"New Enclave","OrganizationName":"Martyn","OrganizationId":367539,"Active":true,"Name":"New Enclave","Street":"#136 Seasia","City":"Anchorage","State":"AK","Zip":"99501","Phone":"9999999999","PhoneExtension":null,"UnitNumber":"2-S","IR":2.0,"IncludeMedicalCollections":true,"IncludeForeclosures":true,"OpenBankruptcyWindow":6,"IsFcraAgreementAccepted":true,"DeclineForOpenBankruptcies":true,"Landlord":{"FirstName":"Harry","LastName":"Bell","StreetAddressLineOne":"Strret 5","StreetAddressLineTwo":null,"City":"Arapahoe","State":"NY","Zip":"80010","PhoneNumber":"222222222222","Email":"adam@yopmail.com","IsCompany":false}}
        },error: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect1. Verify Network.1';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
        }
    });
    return createPorpertyResponse;
}

function getSMPropertyById( serverTime, securityToken, property_db_id, unit_db_id) {
    var getPorpertyResponse = "ERROR";
    var headerset = 'smartmovepartner partnerId="212", serverTime="'+serverTime+'", securityToken="'+securityToken+'"';
    $.ajax({
        type: 'post',
        url: '/smartmove',
        async: false,
        data: {
            class: "SmartMove",
            action: "getSMPropertyIdFromDB",
            property_db_id: property_db_id,
            unit_db_id: unit_db_id
        },
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == 'success' && res.code == 200) {
                if(res.data.sm_property_id != "" && res.data.sm_property_id != null){
                    getPorpertyResponse = "ERROR";
                    /*var smPropertyId = res.data.sm_property_id;
                    $.ajax({
                        url: 'https://cors-anywhere.herokuapp.com/https://smlegacygateway-integration.mysmartmove.com/LandlordApi/v3/Property/'+smPropertyId,
                        type: 'get',
                        dataType: 'json',
                        accepts: {
                            json: 'application/json'
                        },
                        async: false,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader ("Authorization", headerset);
                        },
                        success: function (data) {
                            getPorpertyResponse = data;
                            //{"PropertyId":383007,"PropertyIdentifier":"New Enclave","OrganizationName":"Martyn","OrganizationId":367539,"Active":true,"Name":"New Enclave","Street":"#136 Seasia","City":"Anchorage","State":"AK","Zip":"99501","Phone":"9999999999","PhoneExtension":null,"UnitNumber":"2-S","IR":2.0,"IncludeMedicalCollections":true,"IncludeForeclosures":true,"OpenBankruptcyWindow":6,"IsFcraAgreementAccepted":true,"DeclineForOpenBankruptcies":true,"Landlord":{"FirstName":"Harry","LastName":"Bell","StreetAddressLineOne":"Strret 5","StreetAddressLineTwo":null,"City":"Arapahoe","State":"NY","Zip":"80010","PhoneNumber":"222222222222","Email":"adam@yopmail.com","IsCompany":false}}
                        },error: function (jqXHR, exception) {
                            var msg = '';
                            if (jqXHR.status === 0) {
                                msg = 'Not connect1. Verify Network.1';
                            } else if (jqXHR.status == 404) {
                                msg = 'Requested page not found. [404]';
                            } else if (jqXHR.status == 500) {
                                msg = 'Internal Server Error [500].';
                            } else if (exception === 'parsererror') {
                                msg = 'Requested JSON parse failed.';
                            } else if (exception === 'timeout') {
                                msg = 'Time out error.';
                            } else if (exception === 'abort') {
                                msg = 'Ajax request aborted.';
                            } else {
                                msg = 'Uncaught Error.\n' + jqXHR.responseText;
                            }

                        }
                    });*/
                }else{
                    getPorpertyResponse = "SUCCESS";
                }
            }else if(res.status == 'error' && res.code == 503){
                getPorpertyResponse = "SUCCESS";
            }else{
                getPorpertyResponse = "ERROR";
            }
        }
    });
    return getPorpertyResponse;
}

function addressFormat(address){
    if (address !== null) {
        return address.replace("-"," ");
    }
    return "";
}

function getStateSortForm(state){
    var stateArray = {"AL":"Alabama","AK":"Alaska","AZ":"Arizona","AR":"Arkansas","CA":"California","CO":"Colorado","CT":"Connecticut","DE":"Delaware","FL":"Florida","GA":"Georgia","HI":"Hawaii","ID":"Idaho","IL":"Illinois","IN":"Indiana","IA":"Iowa","KS":"Kansas","KY":"Kentucky","LA":"Louisiana","ME":"Maine","MD":"Maryland","MA":"Massachusetts","MI":"Michigan","MN":"Minnesota","MS":"Mississippi","MO":"Missouri","MT":"Montana","NE":"Nebraska","NV":"Nevada","NH":"New Hampshire","NJ":"New Jersey","NM":"New Mexico","NY":"New York","NC":"North Carolina","ND":"North Dakota","OH":"Ohio","OK":"Oklahoma","OR":"Oregon","PA":"Pennsylvania","RI":"Rhode Island","SC":"South Carolina","SD":"South Dakota","TN":"Tennessee","TX":"Texas","UT":"Utah","VT":"Vermont","VA":"Virginia","WA":"Washington","WV":"West Virginia","WI":"Wisconsin","WY":"Wyoming","AS":"American Samoa","DC":"District of Columbia","FM":"Federated States of Micronesia","GU":"Guam","MH":"Marshall Islands","MP":"Northern Mariana Islands","PW":"Palau","PR":"Puerto Rico","VI":"Virgin Islands"};
    for (var prop in stateArray) { 
        if (stateArray.hasOwnProperty(prop)) { 
            if (stateArray[prop] === state) 
            return prop; 
        } 
    } 
}   

function getSecurityHeaders(serverTime){
    var securityKey = '387KmgTwB6eUZx8Uwqllp9aao5nNGtMNBRiEYGZc+wAX383gfieO/L5HiNvEE5Lf7DH448ujhItBuU7BJ8CvqA==';
    var securityHeaderToken = "";
    $.ajax({
        type: 'post',
        url: '/smartmove',
        async: false,
        data: {
            class: "SmartMove",
            action: "generateSmartMoveSecurityHeader",
            securityKey: securityKey,
            serverTime: serverTime
        },
        success: function (response) {
            var res = JSON.parse(response);
            securityHeaderToken = res.hash_key;
        }
    });
    return securityHeaderToken;
}

function saveLandlordAccountInfo(landlord, email, accept_term, tent_id){
    //getTenantPorpertyDetails(tent_id);
    $.ajax({
        type: 'post',
        url: '/smartmove',
        data: {
            class: "SmartMove",
            action: "saveLandlordAccountInfo",
            landlord: landlord,
            tent_id: tent_id,
            email: email,
            accept_term: accept_term
        },
        success: function (response) {
            var res = JSON.parse(response);
            getTenantPorpertyDetails(tent_id);
        }
    });
}

function getTenantPorpertyDetails(tenantId){
    $.ajax({
        type: 'post',
        url: '/smartmove',
        data: {
            class: "SmartMove",
            action: "getTenantPorpertyInfo",
            tent_id: tenantId
        },
        success: function (response) {
            var res = JSON.parse(response);
            if (res.code == 200) {
                var data = res.data;
                var unitname = "";
                if (data.unit_prefix == "" && data.unit_no != "") {
                    unitname = data.unit_no;
                }else if (data.unit_prefix != "" && data.unit_no == ""){
                    unitname = data.unit_prefix;
                }else if (data.unit_prefix != "" && data.unit_no != ""){
                    unitname = data.unit_prefix+ '-' +data.unit_no;
                }else{
                    unitname = '';
                }
                $("#property_form select[name='property_list']").html("<option value='"+data.propertyId+"'>"+data.property_name+"</option>");
                $("#property_form select[name='unit_list']").html("<option value='"+data.unitId+"'>"+unitname+"</option>");
                $("#property_form input[name='property_id']").val(data.property_id);
                $("#property_form input[name='address1']").val(data.property_address1);
                $("#property_form input[name='address2']").val(data.property_address2);
                $("#property_form input[name='city']").val(data.property_city);
                $("#property_form select[name='state']").html("<option value='"+data.property_state+"'>"+data.property_state+"</option>");
                $("#property_form input[name='zipcode']").val(data.property_zipcode);
                $("#property_form input[name='rent_amount']").val(data.rent_amount);
                $("#property_form input[name='deposite_amount']").val(data.security_deposite);
                if (data.landlord.last_name == undefined) {
                    $("#newproperty-modal .welcome_pm").text(data.landlord.first_name);
                }else{
                    $("#newproperty-modal .welcome_pm").text(data.landlord.first_name+" "+data.landlord.last_name);
                }
            }
            //getTenantPorpertyDetails(tent_id);
        }
    });
}

function savePropertyData(tenantId,smPropertyApiRes){
    var formData = $('#property_form').serializeArray();
    console.log("savePropertyData",formData);
    console.log("savePropertyData2",smPropertyApiRes);
    $.ajax({
        type: 'post',
        url: '/smartmove',
        data: {
            class: "SmartMove",
            action: "savePropertyData",
            form: formData,
            tenantId: tenantId,
            propertyReponse: smPropertyApiRes
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                toastr.success(data.message);
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
    return false;
}

function saveApplication(tenantId, saveApplicationRes){
    //{"ProductBundle":"PackageCorePlusEviction","Applicants":["john@yopmail.com"],"ApplicationId":291816,"LandlordPays":true,"PropertyId":383893,"Rent":200.0,"Deposit":200.0,"LeaseTermInMonths":12,"UnitNumber":"B-10"}    
    var saveApplicationResponse = false;
    $.ajax({
        type: 'post',
        url: '/smartmove',
        async: false,
        data: {
            class: "SmartMove",
            action: "saveApplication",
            tenantId: tenantId,
            form: saveApplicationRes
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                saveApplicationResponse = data.data;
                toastr.success(data.message);
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            saveApplicationResponse = false;
        }
    });
    return saveApplicationResponse;
}

function arrayManagementString(arr){
    var arr = [ "grahem@yopmail.com, struat@yopmail.com, davis@yopmail.com" ];
    var arr = arr[0].split(",");
    return arr.map(s => s.trim());
}

function createApplication(getSMProperty, serverTime, securityToken, payBy) {
    var emailArray = [];
    emailArray.push(getSMProperty['tenant_email']);
    if (getSMProperty['additional_tenants_emails'] != "") {
        var arr = getSMProperty['additional_tenants_emails'].split(",");
        var addTenantEmails = arr.map(s => s.trim());
        emailArray.concat(addTenantEmails);
    }
    if (getSMProperty['guarantors_emails'] != "") {
        var arr = getSMProperty['guarantors_emails'].split(",");
        var addTenantEmails = arr.map(s => s.trim());
        emailArray.concat(addTenantEmails);
    }
    var landlordPay = true;
    if (payBy == "1") {
        landlordPay = false;
    }

    var applicationObj = {
        "ApplicationId": 0,
        "LandlordPays": landlordPay,
        "PropertyId": getSMProperty['sm_property_id'],
        "Rent": getSMProperty['rent_amount'],
        "Deposit": getSMProperty['deposite_amount'],
        "LeaseTermInMonths": getSMProperty['termTenure'],
        "UnitNumber": getSMProperty['unit'],
        "ProductBundle": 'PackageCorePlusEviction',
        "Applicants": emailArray
    };
    var createApplicationResponse;
    var headerset = 'smartmovepartner partnerId="212", serverTime="'+serverTime+'", securityToken="'+securityToken+'"';
    $.ajax({
        url: 'https://cors-anywhere.herokuapp.com/https://smlegacygateway-integration.mysmartmove.com/LandlordApi/v3/Application',
        type: 'post',
        dataType: 'json',
        accepts: { json: 'application/json' },
        data: applicationObj,
        async: false,
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", headerset);
            /*xhr.setRequestHeader("Accept","application/json");
            xhr.setRequestHeader("Content-Type","application/json");*/
        },
        success: function (data) {
            createApplicationResponse = data;
            //{"ProductBundle":"PackageCorePlusEviction","Applicants":["john@yopmail.com"],"ApplicationId":291816,"LandlordPays":true,"PropertyId":383893,"Rent":200.0,"Deposit":200.0,"LeaseTermInMonths":12,"UnitNumber":"B-10"}
        },error: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect1. Verify Network.1';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
        }
    });
    return createApplicationResponse;
}

function getSMPropertyId(tent_id, formData){
    var returnResponse;
    $.ajax({
        type: 'post',
        url: '/smartmove',
        async: false,
        data: {
            class: "SmartMove",
            action: "getSMPropertyId",
            form: formData,
            tent_id: tent_id
        },
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == 'success' && res.code == 200) {
                returnResponse = res.data;
            }else{
                return "ERROR";
            }
        }
    });
    return returnResponse;
}
