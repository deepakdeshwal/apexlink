$(document).ready(function () {

    //$('.pet_date_given').
    $(document).on("click",".savetenant,.savetenantnext",function () {
        var movein_date=$("#moveInDate").val();
        localStorage.setItem("moveInDate", movein_date);
        return;
    });

    var prop_id=localStorage.getItem('property_id');
    if(prop_id !==undefined && prop_id !=="" && prop_id !==null){
        setTimeout(function () {
            $('select[name=property]').val(prop_id).trigger('change');
        },1550);
    }

    $(document).on('change','#additional_salutation',function () {
        var sal=$("#additional_salutation").val();
        if(sal=="Dr." || sal=="Mr." || sal=="Sir" || sal=="Father" || sal=="Brother" || sal=="Mr. & Mrs." || sal=="Select") {
            $(".maiden_name_add_hide").hide();
        }
        else{
                $(".maiden_name_add_hide").show();
            }
    });
    /*Changes on building popup 13-1-2020 Start*/

    $('#generalPropertyName').on('blur', function () {
        $('#generalLegalName').val(this.value);
    });
    $(document).on("blur",".phone_format,.phone_number", function(){
        var currentVal = $(this).val();
        var letters = /^[a-zA-Z]+$/;
        if(currentVal.match(letters)){
            $(this).val('');
            return false;
        }
    });

    $('.vendorTab').on('click',function(){
        var table_id = $(this).attr('data_tab');
        if(table_id !== undefined && table_id != '') {
            var $grid = $("#" + table_id).setGridWidth($(window).width()-100);
              //  $("#jqgrid")
              //  newWidth = $grid.closest(".ui-jqgrid").parent().width();
            // $grid.jqGrid("setGridWidth", newWidth, true);
        }
    });

    if(localStorage.getItem("Message")){
        var message = localStorage.getItem("Message");
        toastr.success(message);
        localStorage.removeItem('Message');
    }
    if(localStorage.getItem("rowcolor")){
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
            localStorage.removeItem('rowcolor');
        }, 700);
    }

    var rString = randomString(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    $("#collection_collectionId").val(rString);
    $("#AddNewPortfolioModal input[name='portfolio_id']").val(rString);


    $ (".countycodediv .countrycode_prefix").intlTelInput();

    var def_data_dial_code = $(".country-list li").eq(0).attr('data-dial-code');
    var def_country_code = $(".country-list li").eq(0).find(".country-name").text();
    var def_dial_code = $(".country-list li").eq(0).find(".dial-code").text();
    $('.countycodediv .countrycode_prefix').val(def_country_code+" "+def_dial_code);
    $('.countycodediv input[name="countryCode[]"]').val(def_data_dial_code);

    $('.countycodediv li.country').on("click",function(){
        var dial_code = $(this).attr('data-dial-code');
        var countryname = $(this).find(".country-name").text();
        var dialcode = $(this).find(".dial-code").text();

        $('.countycodediv .countrycode_prefix').val(countryname+" "+dialcode);
        $('.countycodediv input[name="countryCode[]"]').val(dial_code);
    });
    $(".countycodediv .countrycode_prefix").on('input',function(){
        var dial_code = $('li.country.active').attr('data-dial-code');
        $('.countycodediv input[name="countryCode[]"]').val(dial_code);
    });

    /* pet count4wry*/
    $ (".petcountycodediv .petcountrycode_prefix").intlTelInput();

    var def_data_dial_code = $(".petcountry-list li").eq(0).attr('data-dial-code');
    var def_country_code = $(".petcountry-list li").eq(0).find(".petcountry-name").text();
    var def_dial_code = $(".petcountry-list li").eq(0).find(".dial-code").text();
    $('.petcountycodediv .petcountrycode_prefix').val(def_country_code+" "+def_dial_code);
    $('.petcountycodediv input[name="pet_countryCode[]"]').val(def_data_dial_code);

    $('.petcountycodediv li.country').on("click",function(){
        var dial_code = $(this).attr('data-dial-code');
        var countryname = $(this).find(".petcountry-name").text();
        var dialcode = $(this).find(".dial-code").text();

        $('.petcountycodediv .petcountrycode_prefix').val(countryname+" "+dialcode);
        $('.petcountycodediv input[name="pet_countryCode[]"]').val(dial_code);
    });
    $(".petcountycodediv .petcountrycode_prefix").on('input',function(){
        var dial_code = $('li.country.active').attr('data-dial-code');
        $('.petcountycodediv input[name="pet_countryCode[]"]').val(dial_code);
    });

    /* service country*/
    $ (".servicecountycodediv .servicecountrycode_prefix").intlTelInput();

    var def_data_dial_code = $(".servicecountry-list li").eq(0).attr('data-dial-code');
    var def_country_code = $(".servicecountry-list li").eq(0).find(".servicecountry-name").text();
    var def_dial_code = $(".servicecountry-list li").eq(0).find(".dial-code").text();
    $('.servicecountycodediv .servicecountrycode_prefix').val(def_country_code+" "+def_dial_code);
    $('.servicecountycodediv input[name="service_countryCode[]"]').val(def_data_dial_code);

    $('.servicecountycodediv li.country').on("click",function(){
        var dial_code = $(this).attr('data-dial-code');
        var countryname = $(this).find(".servicecountry-name").text();
        var dialcode = $(this).find(".dial-code").text();

        $('.servicecountycodediv .servicecountrycode_prefix').val(countryname+" "+dialcode);
        $('.servicecountycodediv input[name="service_countryCode[]"]').val(dial_code);
    });
    $(".servicecountycodediv .servicecountrycode_prefix").on('input',function(){
        var dial_code = $('li.country.active').attr('data-dial-code');
        $('.servicecountycodediv input[name="service_countryCode[]"]').val(dial_code);
    });

    /* guarantor_country */
    $ (".guarantorcountycodediv .guarantorcountrycode_prefix").intlTelInput();

    var def_data_dial_code = $(".guarantorcountry-list li").eq(0).attr('data-dial-code');
    var def_country_code = $(".guarantorcountry-list li").eq(0).find(".guarantorcountry-name").text();
    var def_dial_code = $(".guarantorcountry-list li").eq(0).find(".dial-code").text();
    $('.guarantorcountycodediv .guarantorcountrycode_prefix').val(def_country_code+" "+def_dial_code);
    $('.guarantorcountycodediv input[name="guarantor_countryCode_1[]"]').val(def_data_dial_code);

    $('.guarantorcountycodediv li.country').on("click",function(){
        var dial_code = $(this).attr('data-dial-code');
        var countryname = $(this).find(".guarantorcountry-name").text();
        var dialcode = $(this).find(".dial-code").text();

        $('.guarantorcountycodediv .guarantorcountrycode_prefix').val(countryname+" "+dialcode);
        $('.guarantorcountycodediv input[name="guarantor_countryCode_1[]"]').val(dial_code);
    });
    $(".guarantorcountycodediv .guarantorcountrycode_prefix").on('input',function(){
        var dial_code = $('li.country.active').attr('data-dial-code');
        $('.guarantorcountycodediv input[name="guarantor_countryCode_1[]"]').val(dial_code);
    });


    /* emergency country */
    $ (".emergencycountycodediv .emergencycountrycode_prefix").intlTelInput();

    var def_data_dial_code = $(".emergencycountry-list li").eq(0).attr('data-dial-code');
    var def_country_code = $(".emergencycountry-list li").eq(0).find(".emergencycountry-name").text();
    var def_dial_code = $(".emergencycountry-list li").eq(0).find(".dial-code").text();
    $('.emergencycountycodediv .emergencycountrycode_prefix').val(def_country_code+" "+def_dial_code);
    $('.emergencycountycodediv input[name="countryCode1"]').val(def_data_dial_code);

    $('.emergencycountycodediv li.country').on("click",function(){
        var dial_code = $(this).attr('data-dial-code');
        var countryname = $(this).find(".emergencycountry-name").text();
        var dialcode = $(this).find(".dial-code").text();

        $('.emergencycountycodediv .emergencycountrycode_prefix').val(countryname+" "+dialcode);
        $('.emergencycountycodediv input[name="countryCode1"]').val(dial_code);
    });
    $(".emergencycountycodediv .emergencycountrycode_prefix").on('input',function(){
        var dial_code = $('li.country.active').attr('data-dial-code');
        $('.emergencycountycodediv input[name="countryCode1"]').val(dial_code);
    });

    /* additional tenant country */
    $ (".addcountycodediv .addcountrycode_prefix").intlTelInput();

    var def_data_dial_code = $(".addcountry-list li").eq(0).attr('data-dial-code');
    var def_country_code = $(".addcountry-list li").eq(0).find(".addcountry-name").text();
    var def_dial_code = $(".addcountry-list li").eq(0).find(".dial-code").text();
    $('.addcountycodediv .addcountrycode_prefix').val(def_country_code+" "+def_dial_code);
    $('.addcodediv input[name="additional_countryCode[]"]').val(def_data_dial_code);

    $('.addcountycodediv li.country').on("click",function(){
        var dial_code = $(this).attr('data-dial-code');
        var countryname = $(this).find(".addcountry-name").text();
        var dialcode = $(this).find(".dial-code").text();

        $('.addcountycodediv .addcountrycode_prefix').val(countryname+" "+dialcode);
        $('.addcountycodediv input[name="additional_countryCode[]"]').val(dial_code);
    });
    $(".addcountycodediv .addcountrycode_prefix").on('input',function(){
        var dial_code = $('li.country.active').attr('data-dial-code');
        $('.addcountycodediv input[name="additional_countryCode[]"]').val(dial_code);
    });

    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getInitialData',
        data: {
            class: "TenantAjax",
            action: "getIntialData"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if (data.data.state.state != ""){
                    $("#driverProvince").val(data.data.state.state);
                }

                if (data.data.petgender.length > 0){
                    var petGenderOption = "<option value='0'>Select</option>";
                    $.each(data.data.petgender, function (key, value) {
                        petGenderOption += "<option value='"+value.id+"'>"+value.gender+"</option>";
                    });
                    $('#addTenant #pet_gender').html(petGenderOption);
                }
                if (data.data.animalgender.length > 0){
                    var animalGenderOption = "<option value='0'>Select</option>";
                    $.each(data.data.animalgender, function (key, value) {
                        animalGenderOption += "<option value='"+value.id+"'>"+value.gender+"</option>";
                    });
                    $('#addTenant select[name="service_gender[]"]').html(animalGenderOption);
                }

                if (data.data.country.length > 0){
                    var countryOption = "<option value='0'>Select</option>";
                    $.each(data.data.country, function (key, value) {
                        if( value.id == "220"){
                            countryOption += "<option value='"+value.id+"' data-id='"+value.code+"' selected>"+value.name+" ("+value.code+")"+"</option>";
                        }else{
                            countryOption += "<option value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")"+"</option>";
                        }
                    });
                    $('#addTenant .countycodediv select').html(countryOption);
                    $('.emergencycountry').html(countryOption);
                }

                if (data.data.propertylist.length > 0){
                    var propertyOption = "<option value=''>Select</option>";
                    $.each(data.data.propertylist, function (key, value) {
                        propertyOption += "<option value='"+value.id+"' data-id='"+value.property_id+"'>"+value.property_name+"</option>";
                    });
                    $('#addTenant #property').html(propertyOption);
                }

                if (data.data.phone_type.length > 0){
                    var phoneOption = "";
                    $.each(data.data.phone_type, function (key, value) {
                        phoneOption += "<option value='"+value.id+"'>"+value.type+"</option>";
                    });
                    $('.primary-tenant-phone-row select[name="phoneType[]"]').html(phoneOption);
                    $('.addition_tenant_block select[name="additional_phoneType"]').html(phoneOption);
                }

                if (data.data.carrier.length > 0){

                    var carrierOption = "<option value=''>Select</option>";
                    $.each(data.data.carrier, function (key, value) {
                        carrierOption += "<option value='"+value.id+"'>"+value.carrier+"</option>";
                    });
                    $('.primary-tenant-phone-row select[name="carrier[]"]').html(carrierOption);
                }
                if (data.data.carrier.length > 0){

                    var carrierOption = "<option value='0'>Select</option>";
                    $.each(data.data.carrier, function (key, value) {
                        carrierOption += "<option value='"+value.id+"'>"+value.carrier+"</option>";
                    });
                    $('.addition_tenant_block select[name="additional_carrier"]').html(carrierOption);
                    $('.property_guarantor_form1 select[name="guarantor_carrier_1[]"]').html(carrierOption);
                    $('.additional_carrier').html(carrierOption);
                    $(' select[name="guarantor_form2_carrier_1[]"]').html(carrierOption);
                }

                if (data.data.referral.length > 0){
                    var referralOption = "<option value='0'>Select</option>";
                    $.each(data.data.referral, function (key, value) {
                        referralOption += "<option value='"+value.id+"'>"+value.referral+"</option>";
                    });
                    $('select[name="referralSource"]').html(referralOption);
                    $('.addition_tenant_block select[name="additional_referralSource"]').html(referralOption);
                }

                if (data.data.ethnicity.length > 0){
                    var ethnicityOption = "<option value=''>Select</option>";
                    $.each(data.data.ethnicity, function (key, value) {
                        ethnicityOption += "<option value='"+value.id+"'>"+value.title+"</option>";
                    });
                    $('select[name="ethncity"]').html(ethnicityOption);
                    $('.addition_tenant_block select[name="additional_ethncity"]').html(ethnicityOption);
                }

                if (data.data.marital.length > 0){
                    var maritalOption = "<option value='0'>Select</option>";
                    $.each(data.data.marital, function (key, value) {
                        maritalOption += "<option value='"+value.id+"'>"+value.marital+"</option>";
                    });
                    $('select[name="maritalStatus"]').html(maritalOption);
                    $('.addition_tenant_block select[name="maritalStatus"]').html(maritalOption);
                    $('.additional_maritalStatus').html(maritalOption);
                }

                if (data.data.hobbies.length > 0){
                    var hobbyOption = "";
                    $.each(data.data.hobbies, function (key, value) {
                        hobbyOption += "<option value='"+value.id+"'>"+value.hobby+"</option>";
                    });
                    $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').html(hobbyOption);
                    $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                }

                if (data.data.veteran.length > 0){
                    var abc = "";
                    var veteranOption = "<option value='0'>Select</option>";
                    $.each(data.data.veteran, function (key, value) {
                        veteranOption += "<option value='"+value.id+"'>"+value.veteran+"</option>";
                    });
                    $('select[name="veteranStatus"]').html(veteranOption);
                    $('.additional_veteranStatus').html(veteranOption);
                }

                if (data.data.collection_reason.length > 0){
                    var reasonOption = "";
                    $.each(data.data.collection_reason, function (key, value) {
                        reasonOption += "<option value='"+value.id+"'>"+value.reason+"</option>";
                    });
                    $('.property_collection select[name="collection_reason"]').html(reasonOption);
                }

                if (data.data.credential_type.length > 0){
                    var typeOption = "";
                    var typeOption = "<option value='0'>Select</option>";
                    $.each(data.data.credential_type, function (key, value) {
                        typeOption += "<option value='"+value.id+"'>"+value.credential_type+"</option>";
                    });
                    $('.tenant-credentials-control select[name="credentialType[]"]').html(typeOption);
                }


            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

    $(document).on("click", "#addAmenityCancelBtn", function (e) {
        e.preventDefault();
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#addAmenitiesDivId").hide(500);
                }
            }
        });
    });

    $(document).on("click",".main-tabs ul li a", function(e){
        //e.preventDefault();
        //changePage($(this).attr("data-tab"));
    });

    $(document).on("change","#addTenant #property",function (e) {
        e.preventDefault();
        getBuildingsByPropertyID($(this).val());
        return false;
    });

    $(document).on("change","#addTenant #building",function (e) {
        e.preventDefault();
        getUnitsByBuildingsID($("#addTenant #property").val() , $(this).val());
        return false;
    });

    $(document).on("click","#people-tenant .nav-tabs .rent_tab", function(e){
        e.preventDefault();
        getTenantRentInfo($(".tenant_id").val());
        return false;
    });

    $(document).on("click","#edit_rent_form .save_edit_rent", function(e){
        e.preventDefault();
        $(".edit_rent_user_id").val($(".tenant_id").val());
        $("#edit_rent_form").trigger('submit');
        return false;
    });

    $(document).on("submit","#edit_rent_form", function(e){
        e.preventDefault();
        saveTenantRentInfo();
        return false;
    });

    $(document).on("click",".select_property_popup",function () {
        setTimeout(function () {
            getPropertyPopUpRecord();
            fetchAllPortfolio(false);
            fetchAllPropertytype(false);
            fetchAllPropertystyle(false);
            fetchAllPropertysubtype(false);
        },500);

    });

    $(document).on("click",".select_building_popup",function () {
        var propertyId = $("#addTenant #property").val();
        var propertyName = $("#addTenant #property option:selected").text();
        setTimeout(function () {
            getBuildingPopUpRecord(propertyId, propertyName);
        },500);
    });

    $(document).on("click",".select_unit_popup",function (e) {
        e.preventDefault();
        var propertyId = $("#addTenant #property").val();
        var propertyName = $("#addTenant #property option:selected").text();
        var buildingId = $("#addTenant #building").val();
        var buildingName = $("#addTenant #building option:selected").text();

        getUnitPopUpRecord(propertyId, propertyName, buildingId, buildingName);
        return false;
    });

    $(document).on("click",".pop-add-icon",function () {
        setTimeout(function () {
            $(".modal.in .modal-body input").val('');
        },500);
    });

    $(document).on("click",".modal-dialog .add_single", function () {
        var tableName = $(this).attr("data-table");
        var colName = $(this).attr("data-cell");
        var className = $(this).attr("data-class");
        var selectName = $(this).attr("data-name");
        var val = $.trim($("."+className).val());
        if (val == ""){
            $("#"+className).text("Enter the value");
            return false;
        }else{
            $("#"+className).text("");
        }
        savePopUpData(tableName, colName, val, selectName);

    });

    $(document).on("click",".modal-dialog .add_double", function () {
        var tableName = $(this).attr("data-table");
        var colName = $(this).attr("data-cell");
        var className = $(this).attr("data-class");
        var selectName = $(this).attr("data-name");
        var splitClass = className.split(',');
        var arrayValue = [];
        for (var iniSplitClass = 0; iniSplitClass < splitClass.length; iniSplitClass++){
            var val = $.trim($("."+splitClass[iniSplitClass]).val());
            if (val == ""){
                $("#"+splitClass[iniSplitClass]).text("Enter the value");
                return false;
            }else{
                arrayValue.push(val);
                $("#"+splitClass[iniSplitClass]).text("");
            }
        }
        saveDoublePopUpData(tableName, colName, arrayValue, selectName);
    });

    $(document).on("click",".modal-dialog .add_double2", function () {
        var tableName = $(this).attr("data-table");
        var colName = $(this).attr("data-cell");
        var className = $(this).attr("data-class");
        var selectName = $(this).attr("data-name");
        var arrayValue = [];
        var firstName = $("#AddNewManagerModal input[name='first_name']").val();
        var lastName = $("#AddNewManagerModal input[name='last_name']").val();
        var email = $("#AddNewManagerModal input[name='email']").val();
        var flag = 1;

        if(firstName == ""){
            flag = 0;
            $("#AddNewManagerModal #first_name_err").show();
            return false;
        } else{
            $("#AddNewManagerModal #first_name_err").hide();
            flag = 1;
        }
        if(email == ""){
            flag = 0;
            $("#AddNewManagerModal #email_err").show();
            return false;
        } else{
            if (ValidateEmail(email) === false){
                flag = 0;
                $("#AddNewManagerModal #email_err").show();
                return false;
            }else{
                $("#AddNewManagerModal #email_err").hide();
                flag = 1;
            }
        }

        if (flag){
            $("#AddNewManagerModal .red-star").hide();
            arrayValue.push(firstName);
            arrayValue.push(lastName);
            arrayValue.push(email);
            arrayValue.push('9');
            arrayValue.push('1');
            saveDoublePopUpData2(tableName, colName, arrayValue, selectName);
        }else{
            $("#AddNewManagerModal .red-star").show();
            return false;
        }

    });

    $(document).on("click",".saveunitpopup",function(e){
        e.preventDefault();
        $('form#addUnitForm').submit();
        $(this).off('click');
        return false;
    });

    $(document).on("submit","#addUnitForm",function(e){
        e.preventDefault();
        var prefixName = $("#txtPrefix").val();
        var txtUnitNumber = $("#txtUnitNumber").val();
        var uploadform = new FormData();
        saveUnitPopUp(uploadform,prefixName,txtUnitNumber);
        return false;
    });

    /*Online Payment*/
    $(document).on('click','.onlinePaymentInfo',function(e){
        e.preventDefault();
        var first_name = $('#firstname').val();
        var last_name = $('#lastname').val();
        var phoneNumber = $('#phoneNumberCheck').val();


        $('#ffirst_name').val(first_name);
        $('#flast_name').val(last_name);
        $('#cphoneNumber').val(phoneNumber);
    });
    $(document).on('click','#financial-info .basicDiv',function(){
        $(".basic-payment-detail").show();
        $(".apx-adformbox-content").hide();
        $(this).addClass("active");
        $("#paymentDiv").removeClass("active");
    });
    $(document).on('click','#financial-info .paymentDiv',function(){
        $(".basic-payment-detail").hide();
        $(".apx-adformbox-content").show();
        $(this).addClass("active");
        $("#basicDiv").removeClass("active");
    });

    /*Online Payment*/

    $("#add_contact #add_contact_popup").validate({
        rules: {
            first_name: {required: true},
            last_name: {required: true},
            email: {email: true}
        }
    });

    $(document).on("change","#addTenant #contact", function(){
        var contactid = $(this).val();
        if(contactid!=undefined || contactid!='0')
        {
                     $.ajax({
            type: 'post',
            url: '/tenantAjax',
            data: {
            class: 'TenantAjax',
            action: 'getContantData',
            contact_id:contactid
            },
            success: function (response) {
            var response = JSON.parse(response);
            if(response.status=='true')
            {

              $("#salutation").val(response.salutation);
              $("#firstname").val(response.data.first_name);
              $("#middlename").val(response.data.middle_name);
              $("#lastname").val(response.data.last_name);
              $("#nickname").val(response.data.nick_name);
              $("#emailCheck").val(response.data.email);
              $("#phoneNumberCheck").val(response.data.phone_number);
              $("#ssn").val(response.ssn_id);
             }
            }

            });

         }
     });


    var redirect=$(".id_from_other").val();
    if(redirect!="" && redirect != undefined){
        getRedirectionData(redirect);
    }

    $(document).on("change","#AmenityList #select_all",function(){
        if ($(this).is(":checked")){
            $("#AmenityList #dynamic_amenity div input").prop("checked",true);
        }else{
            $("#AmenityList #dynamic_amenity div input").prop("checked",false);
        }
    });

    $(".only_letter").keypress(function(event){
        var inputValue = event.which;
        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)||inputValue == 91||inputValue == 93||inputValue == 94||inputValue == 95){
            event.preventDefault();
        }
    });

    $(document).on("click",".slide-toggle2",function () {
        $(".box2").animate({
            width: "toggle"
        });
    });
    $(document).on("click",".goback_func",function () {
        window.history.back();
    });

    $(document).on("click",".closeimagepopupicon",function () {
        $(".popup-bg").hide();
        $(this).parent().hide();
    });

    getContact();
    $(document).on("click","#add_contact_popUp", function () {
        $("#add_contact").modal(show);
    });
    $(document).on("click","#add_contact_popup .savecontactpopup", function () {
        if($("#add_contact #add_contact_popup").valid()) {
            AddContact();
        }

    });
    $(document).on('focusout', '.czip_code', function () {
      getAddressInfoByZip($(this).val());
    });

});

  function fetchAllPortfolio(id) {
        var propertyEditid = $("#property_editunique_id").val();
        $.ajax({
            type: 'post',
            url: '/fetch-portfolioname',
            data: {
                propertyEditid: propertyEditid,
                class: 'propertyDetail',
                action: 'fetchPortfolioname'},
            success: function (response) {
                var res = JSON.parse(response);

                $('select[name="portfolio_id"]').html(res.data);
                if (id != false) {
                    $('select[name="portfolio_id"]').val(id);

                }

            },
        });

    }

    function fetchAllPropertytype(id) {
        var propertyEditid = $("#property_editunique_id").val();
        $.ajax({
            type: 'post',
            url: '/fetch-propertytype',
            data: {
                propertyEditid: propertyEditid,
                class: 'propertyDetail',
                action: 'fetchPropertytype'},
            success: function (response) {
                var res = JSON.parse(response);
                $('select[name="property_type"]').html(res.data);
                if (id != false) {
                    $('select[name="property_type"]').val(id);
                }

            },
        });

    }
    function fetchAllPropertystyle(id) {
        var propertyEditid = $("#property_editunique_id").val();
        $.ajax({
            type: 'post',
            url: '/fetch-propertytype',
            data: {
                propertyEditid: propertyEditid,
                class: 'propertyDetail',
                action: 'fetchPropertystyle'},
            success: function (response) {
                var res = JSON.parse(response);
                $('#property_style_options').html(res.data);
                if (id != false) {
                    $('#property_style_options').val(id);
                }
            },
        });

    }
    function fetchAllPropertysubtype(id) {
        var propertyEditid = $("#property_editunique_id").val();
        $.ajax({
            type: 'post',
            url: '/fetch-propertysubtype',
            data: {
                propertyEditid: propertyEditid,
                class: 'propertyDetail',
                action: 'fetchPropertysubtype'},
            success: function (response) {
                var res = JSON.parse(response);
                $('#property_subtype_options').html(res.data);
                if (id != false) {
                    $('#property_subtype_options').val(id);
                }

            },
        });

    }


function ValidateEmail(mail){
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)){
        return (true);
    }
    alert("You have entered an invalid email address!");
    return (false);
}

function  savePopUpData(tableName, colName, val, selectName) {
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/savePopUpData',
        data: {
            class: "TenantAjax",
            action: "savePopUpData",
            tableName:tableName,
            colName: colName,
            val:val
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                $("select[name='"+selectName+"']").append(option);
                if (selectName == "hobbies[]" || selectName == "additional_hobbies[]"){
                    $("select[name='"+selectName+"']").multiselect('destroy');
                    $("select[name='"+selectName+"']").multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                }
                toastr.success(data.message);
                $(".add_single").parents(".modal.in").modal('hide');

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function  saveDoublePopUpData2(tableName, colName, val, selectName){
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/saveDoublePopUpData2',
        data: {
            class: "TenantAjax",
            action: "saveDoublePopUpData2",
            tableName:tableName,
            colName: colName,
            val:val
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                $("select[name='"+selectName+"']").append(option);
                if (selectName == "manager_id[]"){
                    $("select[name='"+selectName+"']").multiselect('destroy');
                    $("select[name='"+selectName+"']").multiselect();
                }
                toastr.success(data.message);
                $(".add_double2").parents(".modal.in").modal('hide');

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function  saveDoublePopUpData(tableName, colName, val, selectName){
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/saveDoublePopUpData',
        data: {
            class: "TenantAjax",
            action: "saveDoublePopUpData",
            tableName:tableName,
            colName: colName,
            val:val
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                $("select[name='"+selectName+"']").append(option);
                if (selectName == "attach_groups[]"){
                    $("select[name='"+selectName+"']").multiselect('destroy');
                    $("select[name='"+selectName+"']").multiselect();
                }
                toastr.success(data.message);
                $(".add_double").parents(".modal.in").modal('hide');

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function getUnitPopUpRecord(propertyId, propertyName, buildingId, buildingName){
   // $("#selectUnit #addUnitForm").trigger('reset');

    if (propertyId != "" || propertyId != "0"){
        $("#selectUnit select[name='property_id']").html('');
        $("#selectUnit select[name='property_id']").append("<option value='"+propertyId+"' selected>"+propertyName+"</option>");
        $("#selectUnit select[name='property_id']").attr("readonly",true);
    }
    if (buildingId != "" || buildingId != "0"){
        $("#selectUnit select[name='building_id[]']").html('');
        $("#selectUnit select[name='building_id[]']").append("<option value='"+buildingId+"' selected>"+buildingName+"</option>");
        $("#selectUnit select[name='building_id[]']").attr("readonly",true);
    }

    $(document).on('focusout','#txtBaseRent,#txtMarketRent,#txtSecurityDeposit',function(){

        var id = this.id;
        if($('#' + id).val() != '' && $('#' + id).val().indexOf(".") == -1) {
            var bef = $('#' + id).val().replace(/,/g, '');
            console.log(bef);
            var value = numberWithCommas(bef) + '.00';
            $('#' + id).val(value);
        } else {
            var bef = $('#' + id).val().replace(/,/g, '');
            $('#' + id).val(numberWithCommas(bef));
        }
    });

    $(document).on("blur","input[name='baseRent']",function (){
        var currentVal = $(this).val();
        currentVal=(Math.round(currentVal * 100) / 100).toFixed(2);
        if(currentVal !=='' && currentVal !==null && currentVal!== undefined && currentVal !=='0.00'){
            $("input[name='securityDeposit']").val(currentVal);
            $(this).next().show();
            $("input[name='securityDeposit']").next().show();
        }else{
            $("input[name='securityDeposit']").val(currentVal);
            $(this).next().hide();
            $("input[name='securityDeposit']").next().hide();
        }

    });
    $(document).on("blur","input[name='market_rent']",function (){
        var currentVal = $(this).val();
        currentVal=(Math.round(currentVal * 100) / 100).toFixed(2);
        if(currentVal !=='' && currentVal !==null && currentVal!== undefined && currentVal !=='0.00'){
            $(this).next().show();
        }else{
            $(this).next().hide();
        }

    });

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getUnitPopUpData',
        data: {
            class: "TenantAjax",
            action: "getUnitPopUpData",
            buildingId:buildingId
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                if (data.data.amenity.length > 0){
                    var amenityOption = "";
                    $.each(data.data.amenity, function (key, value) {
                        amenityOption += '<div class="col-sm-4 col-md-4" style="min-height:40px; padding: 0;"><input class="" type="checkbox" value="'+value.id+'" name="amenities[]"> '+value.name+'</div>';
                    });
                    $('#AmenityList #dynamic_amenity').html(amenityOption);
                }

                if (data.data.unitType.length > 0){
                    var unitTypeOption = "";
                    $.each(data.data.unitType, function (key, value) {
                        unitTypeOption += '<option value="'+value.id+'">'+value.unit_type+'</option>';
                    });
                    $('#addUnitForm #ddlUnitType2').html(unitTypeOption);
                }

                if (data.data.building !== false){
                    $("#NonSmokingUnit").val(data.data.building.smoking_allowed);
                    $("#pet_friendly_id").val(data.data.building.pet_friendly);
                }

                if (data.data.market !== false){
                    setTimeout(function(){
                        $("#txtBaseRent, #txtMarketRent").val(data.data.market);
                    },1000);
                }
            } else if (data.status == "error" && data.code == 504) {
                toastr.warning('Select Property & Building');
            } else if (data.status == "error" && data.code != 504) {
                toastr.warning(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

    $(document).on("change",".check-all-amenities",function(){
        if ($(this).is(":checked")){
            $("#dynamic_amenity div input").prop("checked",true);
        }else{
            $("#dynamic_amenity div input").prop("checked",false);
        }
    });

    $(document).on("change",".NoBuilding",function(){
        if ($(this).is(":checked")){
            $(this).parent().parent().next().show();
        }else{
            $(this).parent().parent().next().hide();
        }
    });

    $(document).on("change","#amenities_checkbox #select_all",function(){
        if ($(this).is(":checked")){
            $("#amenities_checkbox #dynamic_amenity div input").prop("checked",true);
        }else{
            $("#amenities_checkbox #dynamic_amenity div input").prop("checked",false);
        }
    });

    /*$("#addPropertyForm").validate({
        rules: {
            'property_id':{required:true},
            'property_name':{required:true},
            'portfolio_id':{required:true},
            'property_type':{required:true},
            'no_of_buildings':{required: true},
            'no_of_units':{required: true}
        },
        messages: {
            "property_id": "Enter property id.",
            "property_name": "Enter property name",
            "portfolio_id": "Select portfolio",
            "property_type": "Enter property type",
            "no_of_buildings": "Enter number of building",
            "no_of_units": "Enter number of unit",
        }
    });*/
}

function getBuildingPopUpRecord(propertyId, propertyName){
    $("#addBuildingForm select[name='property_id']").html('');
    $("#addBuildingForm select[name='property_id']").append("<option value='"+propertyId+"' selected>"+propertyName+"</option>");
    $("#addBuildingForm select[name='property_id']").attr("readonly",true);
    $("#selectBuilding #legal_name,#selectBuilding #building_name").val(propertyName);

    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getBuildingPopUpData',
        data: {
            class: "TenantAjax",
            action: "getBuildingPopUpData"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if (data.data.amenity.length > 0){
                    var amenityOption = "";
                    $.each(data.data.amenity, function (key, value) {
                        amenityOption += '<div class="col-sm-4 col-md-4" style="min-height:40px; padding: 0;"><input class="" type="checkbox" value="'+value.id+'" name="amenities[]"> '+value.name+'</div>';
                    });
                    $('#amenities_checkbox #dynamic_amenity').html(amenityOption);
                }

                var rString = randomString(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
                $("#addBuildingForm input[name='building_id']").val(rString);


            } else if (data.status == "error") {
                toastr.warning(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

    $(document).on("change",".check-all-amenities",function(){
        if ($(this).is(":checked")){
            $("#dynamic_amenity div input").prop("checked",true);
        }else{
            $("#dynamic_amenity div input").prop("checked",false);
        }
    });

    $(document).on("click",".savebuildingpopup",function(e){
        e.preventDefault();
        $("#addBuildingForm").trigger('submit');
        $(this).off('click');
        return false;
    });

    $(document).on("submit","#addBuildingForm",function(e){
        e.preventDefault();
        saveBuildingPopUp();
        return false;
    });

    $(document).on("change",".NoBuilding",function(){
        if ($(this).is(":checked")){
            $(this).parent().parent().next().show();
        }else{
            $(this).parent().parent().next().hide();
        }
    });

    $(document).on("change","#amenities_checkbox #select_all",function(){
        if ($(this).is(":checked")){
            $("#amenities_checkbox #dynamic_amenity div input").prop("checked",true);
        }else{
            $("#amenities_checkbox #dynamic_amenity div input").prop("checked",false);
        }
    });

    /*$("#addPropertyForm").validate({
        rules: {
            'property_id':{required:true},
            'property_name':{required:true},
            'portfolio_id':{required:true},
            'property_type':{required:true},
            'no_of_buildings':{required: true},
            'no_of_units':{required: true}
        },
        messages: {
            "property_id": "Enter property id.",
            "property_name": "Enter property name",
            "portfolio_id": "Select portfolio",
            "property_type": "Enter property type",
            "no_of_buildings": "Enter number of building",
            "no_of_units": "Enter number of unit",
        }
    });*/
}

function getPropertyPopUpRecord(){
    getZipCode('#selectProperty #propertyZipcode','90001','#selectProperty #propertyCity_name','#selectProperty #propertyState_name','#selectProperty #propertyCountry_name','','');
    $("#selectProperty #propertyZipcode").focusout(function () {
        getZipCode('#selectProperty #propertyZipcode',$(this).val(),'#selectProperty #propertyCity_name','#selectProperty #propertyState_name','#selectProperty #propertyCountry_name','','');
    });

    $(document).on("change","input[name='default_building_unit']",function(){
        if ($(this).is(":checked")){
            $(this).parent().parent().next().show();
        }else{
            $(this).parent().parent().next().hide();
        }
    });

    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getPropertyPopUpData',
        data: {
            class: "TenantAjax",
            action: "getPropertyPopUpData"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                if (data.data.portfolio.length > 0){
                    var portfolioOption = "<option value='0'>Select</option>";
                    $.each(data.data.portfolio, function (key, value) {
                        portfolioOption += "<option value='"+value.id+"'>"+value.portfolio_name+"</option>";
                    });
                    $('#selectProperty select[name="portfolio_id"]').html(portfolioOption);
                }

                if (data.data.property_type.length > 0){
                    var typeOption = "<option value='0'>Select</option>";
                    $.each(data.data.property_type, function (key, value) {
                        typeOption += "<option value='"+value.id+"'>"+value.property_type+"</option>";
                    });
                    $('#selectProperty select[name="property_type"]').html(typeOption);
                }

                if (data.data.style.length > 0){
                    var styleOption = "<option value='0'>Select</option>";
                    $.each(data.data.style, function (key, value) {
                        styleOption += "<option value='"+value.id+"'>"+value.property_style+"</option>";
                    });
                    $('#selectProperty #property_style_options').html(styleOption);
                }

                if (data.data.subtype.length > 0){
                    var subtypeOption = "<option value='0'>Select</option>";
                    $.each(data.data.subtype, function (key, value) {
                        subtypeOption += "<option value='"+value.id+"'>"+value.property_subtype+"</option>";
                    });
                    $('#selectProperty #property_subtype_options').html(subtypeOption);
                }

                if (data.data.manager.length > 0){
                    var managerOption = "<option value='0'>Select</option>";
                    $.each(data.data.manager, function (key, value) {
                        var name = value.first_name+' '+value.last_name;
                        managerOption += "<option value='"+value.id+"'>"+name+"</option>";
                    });
                    $('#selectProperty .select_manager').html(managerOption);
                    $('#selectProperty .select_manager').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                }

                if (data.data.group.length > 0){
                    var groupOption = "<option value='0'>Select</option>";
                    $.each(data.data.group, function (key, value) {
                        groupOption += "<option value='"+value.id+"'>"+value.group_name+"</option>";
                    });
                    $('#selectProperty .attach_groups').html(groupOption);
                    $('#selectProperty .attach_groups').multiselect({includeSelectAllOption: true,nonSelectedText: 'Group'});
                }

                if (data.data.amenity.length > 0){
                    var amenityOption = "";
                    $.each(data.data.amenity, function (key, value) {
                        amenityOption += '<div class="col-sm-4 col-md-4" style="min-height:40px; padding: 0;"><input class="" type="checkbox" value="'+value.id+'" name="amenities[]"> '+value.name+'</div>';
                    });
                    $('#selectProperty #dynamic_amenity').html(amenityOption);
                }

                var rString = randomString(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
                $("#selectProperty #autoGenProperty_id").val(rString);


            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

    $(document).on("change",".check-all-amenities",function(){
        if ($(this).is(":checked")){
            $("#dynamic_amenity div input").prop("checked",true);
        }else{
            $("#dynamic_amenity div input").prop("checked",false);
        }
    });

    $(document).on("click","#AddGeneralInformationButton",function(e){
        e.preventDefault();
        $("#addPropertyForm").trigger('submit');
        return false;
    });

    $(document).on("submit","#addPropertyForm",function(e){
        e.preventDefault();
        savePropertyPopUp();
        return false;
    });

    $("#addPropertyForm").validate({
        rules: {
            'property_id':{required:true},
            'property_name':{required:true},
            'portfolio_id':{required:true},
            'property_type':{required:true},
            'no_of_buildings':{required: true},
            'no_of_units':{required: true}
        },
        // messages: {
        //     "property_id": "Enter property id.",
        //     "property_name": "Enter property name",
        //     "portfolio_id": "Select portfolio",
        //     "property_type": "Enter property type",
        //     "no_of_buildings": "Enter number of building",
        //     "no_of_units": "Enter number of unit",
        // }
    });
}

function saveUnitPopUp(uploadform, prefixName,txtUnitNumber) {
    var formData = $('#addUnitForm').serialize();
    var sqFt = $("#addUnitForm #txtTotalSqFootage").val();
    var market_rent = $('#addUnitForm #txtMarketRent').val();
    market_rent = Number(market_rent.replace(/[^0-9.-]+/g,""));

    var baseRent = $('#addUnitForm #txtBaseRent').val();
    baseRent = Number(baseRent.replace(/[^0-9.-]+/g,""));

    var securityDeposit = $('#addUnitForm #txtSecurityDeposit').val();
    securityDeposit = Number(securityDeposit.replace(/[^0-9.-]+/g,""));

    uploadform.append('market_rent', market_rent);
    uploadform.append('baseRent', baseRent);
    uploadform.append('securityDeposit', securityDeposit);
    uploadform.append('unit_as', 1);
    uploadform.append('class', "propertyDetail");
    uploadform.append('action', "insertPropertyUnit");

    uploadform.append('form', formData+'&unit_as=1');

        $.ajax({
            type: 'post',
            url: '/propertyUnitAjax',
            processData: false,
            contentType: false,
            data:uploadform,
            success: function (response) {
                //var res = JSON.parse(response);
                var res = $.parseJSON(response);
                // var data = res.data;
                if (res.status == 'success' && res.code == 200) {

                    var lastId = res.data.last_insert_id;
                    if(prefixName == "" && txtUnitNumber != ""){
                        var unitValue =  txtUnitNumber;
                    }else if(prefixName != "" && txtUnitNumber == "") {
                        var unitValue =  prefixName;
                    }else{
                        var unitValue =  prefixName+"-"+txtUnitNumber;
                    }

                $("#addTenant #unit").append("<option value='"+lastId+"' selected>"+unitValue+"</option>");
                toastr.success(response.message);
                $("#selectUnit").modal('hide');
                $("#addUnitForm").trigger('reset');
                return false;
            } else if (res.status == 'error' && res.code == 400) {
                $('.error').html('');
                if(res.data == 'base_rent_error')
                {
                    toastr.warning('Base rent should not greater then Market rent.');
                } else if(res.data == 'property_id_error'){
                    toastr.warning('Incorrect Property. Please select again.');
                } else if(res.data == 'unit_limit_reached'){
                    toastr.warning('Unit Limit Reached');
                } else {
                    $.each(res.data, function (key, value) {

                        $('.' + key).html('* This field is required');
                    });
                    toastr.warning(res.message);
                }
                return false;
            }
        },
    });
    return false;
}

function onTop(rowdata,table){
    if(rowdata){
        setTimeout(function(){
            if (table == ""){
                jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                jQuery('.table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
            }else{
                jQuery('#'+table).find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                jQuery('#'+table).find('tr:eq(1)').find('td').last().addClass("green_row_right");
            }
        }, 300);
    }
}

function saveBuildingPopUp() {

    var formData = $('#addBuildingForm').serializeArray();
    var buildingName = $("#addBuildingForm #building_name").val();

    $.ajax({
        type: 'post',
        url: '/Building/AddBuildingDetail',
        data: {form: formData,
            class: 'buildingDetail',
            action: 'addBuilding'
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $("#addTenant #building").append("<option value='"+response.last_insert_id+"' selected>"+buildingName+"</option>");
                toastr.success(response.message);
                $("#selectBuilding").modal('hide');
            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
                toastr.warning(response.message);
            }else{
                toastr.warning(response.message);
            }
            //onTop(true);
        },
        error: function (response) {
        }
    });
}

function savePropertyPopUp() {
    var formData = $('#addPropertyForm').serializeArray();
    var propertyName = $("#addPropertyForm #generalPropertyName").val();
    $.ajax({
        type: 'post',
        url: '/add-property',
        data: {form: formData,
            class: 'propertyDetail',
            action: 'addProperty'},
        success: function (response) {

            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $("#addTenant #property").append("<option value='"+response.pId+"' selected>"+propertyName+"</option>");
                toastr.success(response.message);
                $("#selectProperty").modal('hide');
            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
                toastr.warning(response.message);
            }
            //onTop(true);
        },
        error: function (response) {
        }
    });
}

function getBuildingsByPropertyID(propertyID){
    if(propertyID == ""){
        propertyID = 0;
    }
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getBuildingByPropertyId',
        data: {
            class: "TenantAjax",
            action: "getBuilding",
            propertyID: propertyID
        },
        success: function (response) {
            var data = $.parseJSON(response);

            if (data.status == "success") {

                if (data.data.length > 0){
                    var buildingOption = "<option value='0'>Select</option>";
                    $.each(data.data, function (key, value) {
                        buildingOption += "<option value='"+value.id+"' data-id='"+value.building_id+"'>"+value.building_name+"</option>";
                    });
                    $('#addTenant #building').html(buildingOption);
                }
                else{
                    var buildingOption = "<option value='0'>Select</option>";
                    $('#addTenant #building').html(buildingOption);
                }

            } else if (data.status == "error") {

                toastr.error(data.message);
            } else {

                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function getUnitsByBuildingsID(propertyID, buildingID){
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getUnitByBuildingId',
        data: {
            class: "TenantAjax",
            action: "getUnits",
            propertyID: propertyID,
            buildingID: buildingID
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                if (data.data.length > 0){
                    var unitOption = "<option value=''>Select</option>";
                    $.each(data.data, function (key, value) {
                        var uniname = "";
                        if (value.unit_prefix == "" && value.unit_no != ""){
                            uniname = value.unit_no;
                        }else if (value.unit_prefix != "" && value.unit_no == ""){
                            uniname = value.unit_prefix;
                        }else if (value.unit_prefix != "" && value.unit_no != ""){
                            uniname = value.unit_prefix +"-"+value.unit_no;
                        }else{
                            uniname = "";
                        }
                        unitOption += "<option value='"+value.id+"' data-id='"+value.unit_prefix+"'>"+uniname+"</option>";
                    });
                    $('#addTenant #unit').html(unitOption);
                }else{
                    var unitOption = "<option value=''>Select</option>";
                    $('#addTenant #unit').html(unitOption);
                }
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

if(localStorage.getitem('genrateLeaseRe') == 'genrateLease'){
    changePage('generate');
}

function changePage(dataTab){
    if(dataTab == "lease"){
        $(".content-data").html("");
        $(".content-data").load("/Tenantlisting/getTenantLeasePage");
        setTimeout(function () {
            initializeLeasePage();
        },500);
    }else if(dataTab == "charges"){
        $(".content-data").html("");
        $(".content-data").load("/Tenantlisting/getTenantChargePage");
    }else if(dataTab == "generate"){
        $(".content-data").html("");
        $(".content-data").load("/Tenantlisting/getTenantGeneratePage");
        setTimeout(function () {
            initializeGeneratePage();
        },500);
    }
}

function initializeLeasePage(){

    var tenantId = $(".tenant_session_id").val();
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getRentInfo',
        data: {
            class: "TenantAjax",
            action: "getRentInfo",
            id: tenantId
        },
        success: function (response) {
            var ext2=0;
            ext2=(Math.round(ext2 * 100) / 100).toFixed(2);
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var rentData = data.data.unit.base_rent;
                var securityData = data.data.unit.security_deposit;
                if(rentData != '' && (rentData.toString().indexOf(".") === -1)) {
                    var bef = rentData.toString().replace(/,/g, '');
                    var value = numberWithCommas(bef);
                    value= (Math.round(value * 100) / 100).toFixed(2)
                    setTimeout(function () {
                        $("#rent_amount").val(value);
                        $("#rent_amount").next().show();
                    },1000);
                } else {
                    var bef = rentData.toString().replace(/,/g, '');
                    setTimeout(function () {
                        $("#rent_amount").val(numberWithCommas(bef));
                        $("#rent_amount").next().show();
                    },1000);
                }

                if(securityData != '' && (securityData.toString().indexOf(".") === -1)) {
                    var securityDatabef = securityData.toString().replace(/,/g, '');
                    var securityDatavalue = numberWithCommas(securityDatabef);
                    securityDatavalue= (Math.round(securityDatavalue * 100) / 100).toFixed(2);
                    setTimeout(function () {
                        $("#security_amount").val(securityDatavalue);
                        $("#security_amount").next().show();
                    },1000);
                } else {
                    var securityDatabef = securityData.toString().replace(/,/g, '');
                    setTimeout(function () {
                        $("#security_amount").val(numberWithCommas(securityDatabef));
                        $("#security_amount").next().show();
                    },1000);
                }
            }
        }
    });


    $(document).on("keyup","#rent_amount",function(){
        $(this).next().show();
    });$(document).on("keyup","#cam_amount",function(){
        $(this).next().show();
    });$(document).on("keyup","#security_amount",function(){
        $(this).next().show();
    });$("input[name=increase_amount]").keyup(function(){$(this).next().show();});

    $('.move_in_date,.move_out_date,.end_date,.notice_date').datepicker({dateFormat: jsDateFomat});
    $(".start_date").datepicker({
        minDate: 0,
        beforeShowDay: function (date) {
            if (date.getDate() == 1) {
                return [true, ''];
            }
            return [false, ''];
        },
        onSelect: function(dateText, instance) {
            onStartDateChange(dateText);
        }
    });

    $(document).on("change",".lease_tenure",function(){
        var value       = $(this).val();
        var time_duration = parseInt($(".lease_term").val(), 10);
        changeTenureTerm(value,time_duration);
    });

    $(document).on("blur",".lease_term",function(){
        var value           = parseInt($(this).val());
        var leaseTenure     = $(".lease_tenure").val();

        var value = $(this).val();
        var notice_period = $('.notice_period').val();
        if (notice_period == "60" && value <= 1 ){
            $(".notice_period,.notice_date").val('');
            bootbox.alert("Notice Period should not greater then lease term 30");
            return false;
        }

        if (notice_period == "90" && value == 2 ){
            $(".notice_period,.notice_date").val('');
            bootbox.alert("Notice Period should not greater then lease term 60");
            return false;
        }

        changeTenureTerm(leaseTenure,value);
    });

    $(document).on("change",".notice_period",function(e){
        e.preventDefault();
        var value = $(this).val();
        var lease_term = $('.lease_term').val();
        if (value == "60" && lease_term <= 1 ){
            bootbox.alert("Notice Period should not greater then lease term 30");
            return false;
        }

        if (value == "90" && lease_term == 2 ){
            bootbox.alert("Notice Period should not greater then lease term 60");
            return false;
        }

        var endDate = $("#end_date").val();
        var d = $.datepicker.parseDate('mm/dd/yy', endDate);
        if (endDate == "") {
            alert("Lease End Date is Blank !");
            $(this).val("");
            return false;
        }else{
            if (value == "30") {
                d.setDate(d.getDate() - 30);
                var newdate = myDateFormatter(d);
                $(".notice_date").val(newdate);
            }else if (value == "60") {
                d.setDate(d.getDate() - 60);
                var newdate = myDateFormatter(d);
                $(".notice_date").val( newdate);
            }else if (value == "90") {
                d.setDate(d.getDate() - 90);
                var newdate = myDateFormatter(d);
                $(".notice_date").val( newdate);
            }else{
                $(".notice_date").val("");
            }
        }
    });

    $('.number_only').keydown(function (e) {
        if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return false;
        }
    });

    $(document).on('focusout','#rent_amount,#cam_amount,#security_amount,#increase_amount,#tax_value',function(){
        var id = this.id;
        if($('#' + id).val() != '' && $('#' + id).val().indexOf(".") == -1) {
            var bef = $('#' + id).val().replace(/,/g, '');
            var value = numberWithCommas(bef) + '.00';
            $('#' + id).val(value);
        } else {
            var bef = $('#' + id).val().replace(/,/g, '');
            $('#' + id).val(numberWithCommas(bef));
        }
    });

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    $(document).on("change",".increased_by", function () {
        if ($(this).val() == "flat"){

            $(".label_amount").text("Amount ("+currencySign+")");
        }else{
            $(".label_amount").text("Percentage (%)");
        }
    });

    /* lease form validation */

    $(document).on("click",".save_lease_btn",function(e){
        e.preventDefault();
        var tenatntleaseuserid = $(".tenant_session_id").val();
        if (tenatntleaseuserid == "") {
            tenatntleaseuserid = $("input[name='tenant_session_id']").val();
        }
        $(".lease_user_id").val(tenatntleaseuserid);
        if ($('#save_lease_form').valid()){
            $("#save_lease_form").trigger("submit");
            $(".content-data").html("");
            $(".content-data").load("/Tenantlisting/getTenantChargePage");
        }
        return false;
    });

    $("#save_lease_form").on("submit",function(e){
        e.preventDefault();
        var formData = new FormData(this);
        saveTenantLease(formData);
        return false;
    });

    $("#save_lease_form").validate({
        rules: {
            'move_in_date':{required:true},
            'start_date1':{required:true},
            'lease_term':{required:true},
            'end_date1':{required:true},
            'notice_date':{required: true},
            'rent_amount':{required: true}
        },
        messages: {
            "move_in_date": "Select move in date",
            "start_date1": "Select start date",
            "lease_term": "Select lease term",
            "end_date1": "Enter end date",
            "notice_date": "Enter notice date",
            "rent_amount": "Enter rent amunt",
        }
    });

}

function initializeGeneratePage(){

    $(document).on("click",".lease_generation",function(){
        var userLeaseId = $('.tenant_session_id').val();
        //var userLeaseId = 2;
        var elem = $(this);
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/TenantLease_Generate_Pdf',
            data: {
                class: "TenantAjax",
                action: "downloadLeaseFile",
                id: userLeaseId
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    elem.parents("#generate_lease").modal('hide');
                    window.open(data.refurl);
                   /* setTimeout(function () {
                       window.location.reload();
                    },500);*/
                    bootbox.confirm("Do you want to move-in this tenant now?", function (result) {
                        if (result == true) {
                            $(".content-data").html("");
                            $(".content-data").load("/Tenantlisting/movein");
                            setTimeout(function () {
                                initializeMoveInPage();
                            },2000);
                        }else{
                            var baseUrl = window.location.origin;
                            window.location.href = baseUrl+'/Tenantlisting/Tenantlisting';
                        }
                    });

                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

        //var refurl = '/Tenantlisting/TenantLease_Generate_Pdf/?_token='+userLeaseId;

        return false;
    });
}

function CallPrint(elemId) {
    var prtContent = document.getElementById(elemId);
    var WinPrint = window.open('', '', 'width=800,height=650,scrollbars=1,menuBar=1');
    var str =  prtContent.innerHTML;
    WinPrint.document.write(str);
    WinPrint.document.close();
    WinPrint.focus();
}

function initializeMoveInPage(){
    $(".generatepage .nav-tabs li.active").removeClass("active");
    var tab = '<li class="active"><a href="#">Move-In</a></li>';
    $(".generatepage .nav-tabs").append(tab);
    $(".scheduled_move_in").datepicker({dateFormat: jsDateFomat});
    $(".actual_move_in").datepicker({dateFormat: jsDateFomat});

    var tenantId = $(".tenant_session_id").val();
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getTenantRentInfoMoveIn',
        data: {
            class: "TenantAjax",
            action: "getTenantRentInfoMoveIn",
            id: tenantId
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                $("#move_in_form input[name='scheduled_move_in']").val(data.data.move_in);
                $("#move_in_form input[name='actual_move_in']").val(data.data.actual_move_in);
                $("#move_in_form .mi_schedule_move_in").text(data.data.move_in);
                $("#move_in_form .mi_actual_move_in").text(data.data.actual_move_in);
                $("#move_in_form .mi_start_date").text(data.data.start_date);
                $("#move_in_form .mi_end_date").text(data.data.end_date);
                $("#move_in_form .mi_amount").text(data.data.rent_amount);
                //$("#move_in_form .pro_rent").val(data.data.pro_rent);
            }
        }
    });


    $(document).on("click",".save_move_in", function () {
        $(".movein_user_id").val($(".tenant_session_id").val());
        $("#move_in_form").trigger("submit");
    });

    $(document).on("submit","#move_in_form", function(e){
        e.preventDefault();
        saveTenantMoveIn();
        return false;
    });

}

function changeTenureTerm(value,time_duration) {
    var leaseStart  = $("#start_date").val();
    var noticePeriod  = $(".notice_period").val();
    var d = $.datepicker.parseDate('mm/dd/yy', leaseStart);

    if (value == "2") {
        d.setFullYear(d.getFullYear() + time_duration);
        d.setDate(d.getDate() - 1);
    }else{
        d.setMonth(d.getMonth() + time_duration);
        d.setDate(d.getDate() - 1);
    }
    var outdate = myDateFormatter(d);

    $('.move_out_date').datepicker('setDate', outdate);
    $(".end_date").datepicker("setDate", outdate);
    var slashEndDate = getSlashDateFormat(d);
    $("#end_date").val(slashEndDate);
    $("#move_out_date").val(slashEndDate);
    var endDate = $("#end_date").val();
    var d = $.datepicker.parseDate('mm/dd/yy', endDate);

    if (endDate == "") {
        alert("Lease End Date is Blank !");
        $(this).val("");
        return false;
    }else{
        if (noticePeriod == "30") {
            d.setDate(d.getDate() - 30);
            var newdate = myDateFormatter(d);
            $(".notice_date").datepicker('setDate', newdate);
        }else if (noticePeriod == "60") {
            d.setDate(d.getDate() - 60);
            var newdate = myDateFormatter(d);
            $(".notice_date").datepicker('setDate', newdate);
        }else if (noticePeriod == "90") {
            d.setDate(d.getDate() - 90);
            var newdate = myDateFormatter(d);
            $(".notice_date").val(newdate);
        }else{
            $(".notice_date").val("");
        }
    }
}
var d2 = new Date();
var month2 = d2.getMonth() ;
var year2=d2.getFullYear();
var firstDay = new Date(year2, month2, 1);
function onStartDateChange(dateText) {
    var d = $.datepicker.parseDate('mm/dd/yy', dateText);
    var currentDate = myDateFormatter(d);
    var time_tenure = $(".lease_tenure").val();
    var time_duration = parseInt($(".lease_term").val(), 10);
    if (time_tenure == "2") {
        d.setFullYear(d.getFullYear() + time_duration);
        d.setDate(d.getDate() - 1);
    }else{
        d.setMonth(d.getMonth() + time_duration);
        d.setDate(d.getDate() - 1);
    }

    var outdate = myDateFormatter(d);

    $('.move_out_date').datepicker('setDate', outdate);

    $(".end_date").attr("disabled", false);
    $(".end_date").datepicker("setDate", outdate);
    $("#end_date").val(getSlashDateFormat(d));
    $("#move_out_date").val(getSlashDateFormat(d));
    //$(".end_date").attr("disabled", true);

    if ($("#end_date").val() != "") {
        var leaseNoticePeriod = $(".notice_period").val();

        if (leaseNoticePeriod != "") {
            d.setDate(d.getDate() - parseInt(leaseNoticePeriod));
        }else{
            d.setDate(d.getDate());
        }

        var newdate = myDateFormatter(d);
        $(".notice_date").attr("readonly",false);
        //$(".notice_date").val(newdate).attr("readonly",true);
        $(".notice_date").val(newdate);
    }

    $('#start_date').val(dateText);
    $('.start_date').val(currentDate);
}

function myDateFormatter(dateObject){
    var newdate = dateObject.getFullYear() + "-" + (dateObject.getMonth() + 1) + "-" + dateObject.getDate();
    var tmp = [];
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getDateFormat',
        data: {class: "TenantAjax",action: "getDateformat",date: newdate,},
        async: false,
        global:false,
        success: function (response) {
            tmp = $.parseJSON(response);
        }
    });
    return tmp;
}

function getSlashDateFormat(dateObject){
    var newdate = (dateObject.getMonth() + 1) + "/" + dateObject.getDate() + "/" + dateObject.getFullYear();
    return newdate;
}

function saveTenantLease(){

    var formData = $('#save_lease_form').serializeArray();
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/saveTenantLeasePage',
        data: {
            class: "TenantAjax",
            action: "saveTenantLease",
            form: formData
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                toastr.success(data.message);
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function saveTenantMoveIn(){
    var formData = $('#move_in_form').serializeArray();
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/saveTenantMoveInPage',
        data: {
            class: "TenantAjax",
            action: "saveTenantMoveIn",
            form: formData
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                toastr.success(data.message);
                localStorage.setItem("Message", data.message);
                localStorage.setItem("rowcolorTenant",'green');
                window.location.href='/Tenantlisting/Tenantlisting';
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

function getZipCode(element,zipcode,city,state,country,county,validation){
    $(validation).val('1');
    $.ajax({
        type: 'post',
        url: '/common-ajax',
        data: {zip_code: zipcode,class: 'CommonAjax', action: 'getZipcode'},
        success: function (response) {
            var data = JSON.parse(response);
            if(data.code == 200){
                if(city !== undefined)$(city).val(data.data.city);
                if(state !== undefined)$(state).val(data.data.state);
                if(country !== undefined)$(country).val(data.data.country);
                if(county !== undefined)$(county).val(data.data.county);
            } else {
                getAddressInfoByZip(element,zipcode,city,state,country,county,validation);
            }
        },
        error: function (data) {}
    });
}


/**
 *Get location on the basis of zipcode
 */
function getAddressInfoByZip(element,zip,city,state,country,county,validation){
    var addr = {};
    addr.element = element;
    addr.elecity = city;
    addr.elestate = state;
    addr.elecountry = country;
    addr.elecounty = county;
    addr.validation = validation;
    if(zip.length >= 5 && typeof google != 'undefined'){
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    response(addr);
                } else {
                    addr.success = false;
                    response(addr);
                }
            } else {
                addr.success = false;
                response(addr);
            }
        });
    } else {
        addr.success = false;
        response(addr);
    }
}

/**
 * Set values in the city, state and country when focus loose from zipcode field.
 */
function response(obj){

    if(obj.success){
        if(obj.elecity !== undefined)$(obj.elecity).val(obj.city);
        if(obj.elestate !== undefined)$(obj.elestate).val(obj.state);
        if(obj.elecountry !== undefined)$(obj.elecountry).val(obj.country);
    } else {
        $(obj.validation).val('0');
        if(obj.elecity !== undefined)$(obj.elecity).val('');
        if(obj.elestate !== undefined)$(obj.elestate).val('');
        if(obj.elecountry !== undefined)$(obj.elecountry).val('');
        if(obj.elecounty !== undefined)$(obj.elecounty).val('');
        $(obj.element).valid();
    }
}

//custom validation
function validations(element){
    var required = $(element).attr('data_required');
    var number = $(element).attr('data_number');
    var only_number = $(element).attr('data_only_number');
    var url = $(element).attr('data_url');
    var max = $(element).attr('data_max');
    var min = $(element).attr('data_min');
    var email = $(element).attr('data_email');
    var value = $(element).val();
    var msg = '';
    if(required == "true"){
        if(value == ''){
            msg = '* This field is required.';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }
    if(max !== undefined){
        if(value.length > max){
            msg = '* Please enter less than '+max+' characters.';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }

    if(min !== undefined){
        if(value.length > min){
            msg = '* Minimum '+min+' character required.';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }

    if(only_number !== undefined) {
        if (isNaN(value)) {
            msg = '* Only number are allowed!';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }

    if(number !== undefined) {
        if (isNaN(value)) {
            msg = '* Only number are allowed!';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        } else {
            if(value > number){
                msg = '* please enter less than '+number;
                $(element).siblings('.customError').text(msg);
                $(element).focus();
                return false;
            }
        }
    }

    if(url !== undefined){
        var expression =  /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm;
        var regex = new RegExp(expression);
        if(value.match(regex)){
        } else {
            msg = '* Please enter a valid URL.';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }

    if(email !== undefined){
        var expression =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(expression.test(value)){

        } else {
            msg = '* Please enter a valid email address.';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }
    $(element).siblings('.customError').text('');
    return true;
}

function randomNumberString(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function getBuildings(property_id){

    $.ajax({
        type: 'post',
        url: '/common-ajax',
        data: {id: property_id, class: 'CommonAjax', action: 'getBuildings'},
        success: function (response) {
            var data = $.parseJSON(response);
            return data;
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

}
function getContact(){
    $.ajax({
        type: 'post',
        url: '/tenantAjax',
        data: {
            class: "tenantAjax",
            action: "getContact",

        },
        success: function (response) {
            var response = JSON.parse(response);
            var html="";
            html += ' <option value="' + '0' + '">' + 'Select'+ '</option>';
            for (var i = 0; i < response.data.length; i++)

            {
                html += ' <option value="' + response.data[i].id + '">' + response.data[i].name + '</option>';
            }
            $("#contact").html(html);

        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}


function AddContact(){
        var form_data = $("#add_contact_popup").serializeArray();

        $.ajax({
            type: 'post',
            url: '/tenantAjax',
            data: {
                class: "tenantAjax",
                action: "AddContact",
                form_data: form_data
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $("#add_contact").modal('hide');
                    getContact();
                    toastr.success("Record added successfully");
                    setTimeout(function () {
                        $(".contact_class").val(response.data);
                    }, 1000);
                }
                if (response.status == 'error' && response.code == 400) {
                    $("#first_nameErr").show();
                    toastr.warning('Record Already Exist')
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
}
function getRedirectionData(id) {
  $.ajax({
      type: 'post',
      url: '/RentalApplication/Ajax',
      data: {
          class: "RenatlApplicationAjax",
          action: "getRedirectionData",
          id:id
      },
      success: function (response) {
          var response = JSON.parse(response);
          $("#property").val(response.data2.prop_id).trigger("change");
          setTimeout(function () {
              $("#building").val(response.data2.build_id).trigger("change");
          },500);
          setTimeout(function () {
              $("#unit").val(response.data2.unit_id);
          },600);

          $("#salutation").val(response.data.salutation);
          $("#firstname").val(response.data.first_name);
          $("#lastname").val(response.data.last_name);

          if(response.data.salutation=="Mrs." || response.data.salutation=="Madam" || response.data.salutation=="Sister" || response.data.salutation=="Mother" || response.data.salutation=="Ms."){
              $(".maiden_name_hide").show();
          }
          $("#maidenname").val(response.data.maiden_name);
          $("#nickname").val(response.data.nick_name);
          $("#middlename").val(response.data.mi);
          $("#phoneType12").val(response.data.phone_type);
          $("#carrierCheck").val(response.data.carrier);
          $("#countryCodeCheck").val(response.data.country);
          $("#phoneNumberCheck").val(response.data.phone_number);
          $("#emailCheck").val(response.data.email);
          $("#referralSourceCheck").val(response.data.referral_source);
          var dateParts = response.data2.exp_move_in.split("-");
          var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
          var exp_move_in= $.datepicker.formatDate(jsDateFomat, jsDate);
          $("#moveInDate").val(exp_move_in);
          $("#ethncityCheck").val(response.data.ethnicity);
          $("#maritalStatusCheck").val(response.data.maritial_status);
          $("#veteranStatusCheck").val(response.data.veteran_status);
          $("#ssn").val(response.data.ssn_sin_id);
          $("#emergency").val(response.data3.emergency_contact_name);
          $("#relationship").val(response.data3.emergency_relation);
          $("#emergency_countryCheck").val(response.data3.emergency_country_code);
          $("#phoneNumber").val(response.data3.emergency_phone_number);
          $("#email1").val(response.data3.emergency_email);



      },
      error: function (data) {
          var errors = $.parseJSON(data.responseText);
          $.each(errors, function (key, value) {
              $('#' + key + '_err').text(value);
          });
      }
  });
}
