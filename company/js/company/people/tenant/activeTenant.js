
$(document).ready(function() {
    /*  $('tr td:not(:last-child)');*/
    $(document).on("hover", "#tenant_listing td:eq(2)", function () {
        var spanTag = "<span class='tooltiptext'>Tooltip text</span>";


    });

    function myCellFormatter(cellvalue, options, rowObject) {
        if (cellvalue != undefined) {
            var returnHtml = "<span class='tooltiptext'>" + cellvalue + "</span>";
            return returnHtml;
        }
    }

    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('all',property_id);
    function jqGrid(status,property_id) {
        if (jqgridNewOrUpdated == 'true') {
            var sortOrder = 'desc';
            var sortColumn = 'users.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }

        if(localStorage.getItem('type') == 'short') var isShortRental = 1;
        if(localStorage.getItem('type') == 'regular') var isShortRental = 0;

        var table = 'users';
        var rentCurrSymbol = "Rent (" + currencySymbol + ")";
        var balanceCurrSymbol = "Balance (" + currencySymbol + ")";
        var columns = ['Tenant Name','Unit Number', rentCurrSymbol,'Status','Lease StartDate','Lease EndDate','Phone', 'Email'];
        //if(status===0) {
        var select_column = [];
        //}
        var joins = [{table: 'users', column: 'id', primary: 'user_id', on_table: 'tenant_property'}, {
            table: 'users',
            column: 'id',
            primary: 'user_id',
            on_table: 'tenant_details'
        }, {
            table: 'users',
            column: 'id',
            primary: 'user_id',
            on_table: 'tenant_lease_details'
        }, {
            table: 'tenant_property',
            column: 'property_id',
            primary: 'id',
            on_table: 'general_property'
        }, {table: 'tenant_property', column: 'unit_id', primary: 'id', on_table: 'unit_details'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [{column: 'is_short_term_rental', value: isShortRental, condition: 'INT', table: 'general_property'},{column: 'user_type', value: '2', condition: '=', table: 'users'}, {column: 'record_status', value: '1', condition: '=', table: 'tenant_details'}, {column: 'record_status', value: '1', condition: '=', table: 'tenant_lease_details'}, {column: 'status', value: '1', condition: '=', table: 'tenant_details'},{column: "property_id", condition: "=", value: property_id,table: 'tenant_property'}];

        //var pagination=[];
        var columns_options = [
            {
                name: 'Tenant Name',
                index: 'name',
                align: "left",
                searchoptions: {sopt: conditions},
                table: table
            },
            {
                name: 'Unit Number',
                index: 'unit_prefix',
                align: "left",
                searchoptions: {sopt: conditions},
                table: 'unit_details',
                change_type: 'combine_column_hyphen2',
                extra_columns: ['unit_prefix', 'unit_no'],
                original_index: 'unit_prefix'
            },
            {
                name: rentCurrSymbol,
                index: 'rent_amount',
                align: "left",
                searchoptions: {sopt: conditions},
                table: 'tenant_lease_details',
                formatter: currencyFormatter
            },{
                name: 'Status',
                index: 'status',
                align: "left",
                searchoptions: {sopt: conditions},
                table: 'tenant_details',
                formatter: statusFormatter
            },{
                name: 'Lease StartDate',
                index: 'start_date',
                align: "left",
                searchoptions: {sopt: conditions},
                change_type: 'date',
                table: 'tenant_lease_details'
            },{
                name: 'Lease EndDate',
                index: 'end_date',
                align: "left",
                searchoptions: {sopt: conditions},
                change_type: 'date',
                table: 'tenant_lease_details'
            },{
                name: 'Phone',
                index: 'id',
                title: false,
                searchoptions: {sopt: conditions},
                table: table,
                classes: 'cursor',
                change_type: 'line_multiple',
                index2: 'phone_number',
                index3: 'other_work_phone_extension',
                join: {table: 'users', column: 'id', primary: 'user_id', on_table: 'tenant_phone'},
                name_id: 'id',
                formatter: addToolTip,
                cellattr: cellAttri
            },
            {name: 'Email', index: 'email', align: "left", searchoptions: {sopt: conditions}, table: table}
        ];
        var ignore_array = [];
        jQuery("#active_tenant_listing").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "users",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                deleted_at: true,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Tenants",
            pginput: true,
            pgbuttons: true,
            loadComplete : function (data) {
                if(localStorage.getItem('type') == 'short') var heading ='List Of Guests (Total Active Guests(s) : ';
                if(localStorage.getItem('type') == 'regular') var heading ='List Of Tenants (Total Active Tenant(s) : ';
                $('.ui-jqgrid-title').text(heading+data.records+')');

            },
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 0, left: 400, drag: true, resize: false} // search options
        );
    }

    function cellAttri(cellValue, options, rowObject) {
        if (rowObject !== undefined) {

            if (rowObject.Notes != '') {
                return 'title = " "';
            }
        }
    }

    function addToolTip(cellValue, options, rowObject) {
        if (rowObject !== undefined) {

            if (rowObject.Notes == '') {
                return cellValue;
            } else {

                return '<div class="tooltipgridclass">' + cellValue + '<span class="tooltiptextbotclass">' + rowObject.Notes + '</span></div>';
            }
        }
    }

    function getUnitDetails(cellvalue, options, rowObject) {
        var string = "";
        if (cellvalue != undefined) {
            $.ajax({
                type: 'post',
                url: '/Tenantlisting/getUnitById',
                data: {
                    class: "TenantAjax",
                    action: "getUnitById",
                    id: cellvalue
                },
                async: false,
                success: function (response) {
                    var res = JSON.parse(response);
                    if (res.status == "success") {
                        string = res.data.unit_prefix + "-" + res.data.unit_no;
                    }
                }
            });
        }
        return string;
    }

    function statusFormatter(cellValue, options, rowObject) {
        if (cellValue == 0)
            return "Inactive";
        else if (cellValue == '1')
            return "Active";
        else if (cellValue == '2')
            return "<span class='collection' style=color:red;>Evicting</span>";
        else if (cellValue == '3')
            return "<span class='collection' style=color:red;>In-Collection</span>";
        else if (cellValue == '4')
            return "<span class='collection' style=color:red;>Bankruptcy</span>";
        else if (cellValue == '5')
            return "<span class='collection' style=color:red;>Evicted</span>";
        else
            return '';

    }

    function statusFormatter2(cellValue, options, rowObject) {
        today = new Date();
        dt1 = new Date(today);
        dt2 = new Date(cellValue);
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
    }

    function currencyFormatter(cellValue, options, rowObject) {
        if (cellValue !== undefined && cellValue !== "") {
            return currencySymbol + '' + cellValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ".00";
        } else {
            return "";
        }
    }

    function statusFormatter3(cellValue, options, rowObject) {
        return currencySymbol + '' + '1,000.00';
    }

    function statusFmatter1(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var select = '';
            if (rowObject['Status'] == '0') select = ['Edit', 'Apply Payment', 'Email', 'TEXT', 'Work Order', 'Add In-touch', 'In-Touch History', 'Enter Credit', 'New Invoice', 'CAM Charge', 'File Library', 'Payment History', 'Email History', 'TEXT History', 'Notes & History', 'Flag Bank', 'Send Tenant Statement', 'Run Background Check', 'Transfer Tenant', 'Move Out Tenant', 'Tenant Portal', 'Send Password Activation Mail', 'Deactivate Tenant account', 'HOA Violation Tracking', 'Print Envelope', 'Delete Tenant', 'ONLINE PAYMENT'];
            if (rowObject['Status'] == '1') select = ['Edit', 'Apply Payment', 'Email', 'TEXT', 'Work Order', 'Add In-touch', 'In-Touch History', 'Enter Credit', 'New Invoice', 'CAM Charge', 'File Library', 'Payment History', 'Email History', 'TEXT History', 'Notes & History', 'Flag Bank', 'Send Tenant Statement', 'Run Background Check', 'Transfer Tenant', 'Move Out Tenant', 'Tenant Portal', 'Send Password Activation Mail', 'Deactivate Tenant account', 'HOA Violation Tracking', 'Print Envelope', 'Delete Tenant', 'ONLINE PAYMENT'];
            if (rowObject['Status'] == '2') select = ['Edit', 'Apply Payment', 'Email', 'TEXT', 'Work Order', 'Add In-touch', 'In-Touch History', 'Enter Credit', 'New Invoice', 'CAM Charge', 'File Library', 'Payment History', 'Email History', 'TEXT History', 'Notes & History', 'Flag Bank', 'Send Tenant Statement', 'Run Background Check', 'Transfer Tenant', 'Move Out Tenant', 'Tenant Portal', 'Send Password Activation Mail', 'Deactivate Tenant account', 'HOA Violation Tracking', 'Print Envelope', 'Delete Tenant', 'ONLINE PAYMENT'];
            if (rowObject['Status'] == '3') select = ['Edit', 'Apply Payment', 'Email', 'TEXT', 'Work Order', 'Add In-touch', 'In-Touch History', 'Enter Credit', 'New Invoice', 'CAM Charge', 'File Library', 'Payment History', 'Email History', 'TEXT History', 'Notes & History', 'Flag Bank', 'Send Tenant Statement', 'Run Background Check', 'Transfer Tenant', 'Move Out Tenant', 'HOA Violation Tracking', 'Print Envelope', 'Delete Tenant', 'ONLINE PAYMENT'];
            if (rowObject['Status'] == '4') select = ['Edit', 'Apply Payment', 'Email', 'TEXT', 'Work Order', 'Add In-touch', 'In-Touch History', 'Enter Credit', 'New Invoice', 'CAM Charge', 'File Library', 'Payment History', 'Email History', 'TEXT History', 'Notes & History', 'Flag Bank', 'Send Tenant Statement', 'Run Background Check', 'Transfer Tenant', 'Move Out Tenant', 'Tenant Portal', 'Send Password Activation Mail', 'Deactivate Tenant account', 'HOA Violation Tracking', 'Print Envelope', 'Delete Tenant', 'ONLINE PAYMENT'];
            if (rowObject['Status'] == '5') select = ['Edit', 'Apply Payment', 'Email', 'TEXT', 'Work Order', 'Add In-touch', 'In-Touch History', 'Enter Credit', 'New Invoice', 'CAM Charge', 'File Library', 'Payment History', 'Email History', 'TEXT History', 'Notes & History', 'Flag Bank', 'Send Tenant Statement', 'Run Background Check', 'Transfer Tenant', 'Move Out Tenant', 'HOA Violation Tracking', 'Print Envelope', 'Delete Tenant', 'ONLINE PAYMENT'];
            if (rowObject['Status'] == '') select = ['Edit', 'Apply Payment', 'Email', 'TEXT', 'Work Order', 'Add In-touch', 'In-Touch History', 'Enter Credit', 'New Invoice', 'CAM Charge', 'File Library', 'Payment History', 'Email History', 'TEXT History', 'Notes & History', 'Flag Bank', 'Send Tenant Statement', 'Run Background Check', 'Transfer Tenant', 'Move Out Tenant', 'HOA Violation Tracking', 'Print Envelope', 'Delete Tenant', 'ONLINE PAYMENT'];
            var data = '';

            if (select != '') {
                var data = '<select class="form-control select_options" data_email="' + rowObject.Email + '" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }

            return data;
        }
    }

});