    $(document).ready(function(){
        setTimeout(function(){
        viewMaintenanceInfo();
    },300);

    function viewMaintenanceInfo()
    {

        var ticket_id = $(".ticket_id").val();
        var tenant_id = $(".tenant_id").val();



        $.ajax({
            url:'/tenantPortal?action=getViewMaintenanceInfo&class=TenantPortal&tenant_id='+tenant_id+"&ticket_id="+ticket_id,
            type: 'GET',
            success: function (data) {

                var tenantInfo =  JSON.parse(data);



                $.each(tenantInfo, function (key, value) {
                    if(value!="")
                    {

                        $('.' + key).html(value);


                    }
                });


            },
        });

    }
    

    })

