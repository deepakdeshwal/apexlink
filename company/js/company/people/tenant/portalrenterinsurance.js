$(document).ready(function () {

    $(document).on("click",".renter-container .renter-edit-icon",function(){
        $(this).hide();
        $(".renter-container .renterlisting").hide();
        $(".renter-container .renterform").show();
        getRenterInfo();
    });

    $(document).on("click",".renter-container .clonerenteradd",function(){
        var clone = $(".renterformdata:first").clone();
        clone.find("input[name='renter_tenant_id']").val('');
        clone.find("input[name='expiration[]']").attr("id", "").removeClass('hasDatepicker').removeData('datepicker').unbind().datepicker({dateFormat: jsDateFomat});
        clone.find("input[name='renewal[]']").attr("id", "").removeClass('hasDatepicker').removeData('datepicker').unbind().datepicker({dateFormat: jsDateFomat});
        clone.find("input[name='effective[]']").attr("id", "").removeClass('hasDatepicker').removeData('datepicker').unbind().datepicker({dateFormat: jsDateFomat});
        var len = $(".renterformdata").length;
        clone.find(".clonerenteradd").remove();
        clone.find(".clonerenterremove").show();
        clone.find(".clonerenterremove .fa-minus-circle").show().css("display","block");
        $(".renterformdata").first().after(clone);
    });

    $(document).on("click",".renter-container .clonerenterremove",function(){
        $(this).parents(".renterformdata").remove();
    });

    $('.renter-container #renterformfield input[name="expiration[]"]').datepicker({
        dateFormat: jsDateFomat
    });
    $('.renter-container #renterformfield input[name="renewal[]"]').datepicker({
        dateFormat: jsDateFomat
    });
    $('.renter-container #renterformfield input[name="effective[]"]').datepicker({
        dateFormat: jsDateFomat
    });

    $(document).on("click",".renter-container #renterformfield .grey-btn",function(){
        $(".renter-container .renter-edit-icon").show();
        $(".renter-container .renterlisting").show();
        $(".renter-container .renterform").hide();
    });

    $(document).on("click",".renter-container #renterformfield .saverenter",function(e){
        $(".renter-container #renterformfield").trigger('submit');
        $(this).off("click");
        return false;
    });

    $(document).on("submit",".renter-container #renterformfield", function(e){
        e.preventDefault();
        saveRenterInfo();
        return false;
    });
    jqGrid1('All');
});

function getRenterInfo(){
    var tenantId = $(".tenant_id").val();
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getRenterInfo',
        data: {
            class: "TenantAjax",
            action: "getRenterInfo",
            tenantId:tenantId
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                if (data.data.renter.length > 0 ){
                    var renter = data.data.renter;
                    var tenantId = $(".tenant_id").val();
                    var html = '<input type="hidden" name="renter_tenant_id" value="'+tenantId+'">';
                    for(var i = 0; i < renter.length; i++ ){
                        html += renterHtml(renter[i], i);
                    }
                    html += '<div class="col-sm-12">';
                    html +=     '<div class="col-sm-12">';
                    html +=         '<div class="btn-outer">';
                    html +=             '<a class="blue-btn saverenter" href="javascript:void(0)">Update</a>';
                    html +=             '<a class="grey-btn" href="javascript:void(0)">Cancel</a>';
                    html +=         '</div>';
                    html +=     '</div>';
                    html +=  '</div>';
                    $(".renter-container #renterformfield").html(html);
                    $("#renterformfield input[type='text']").datepicker({dateFormat:jsDateFomat});
                }
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function renterHtml(data, initialVar){
    $html = '<div class="row renterformdata">';
    $html +=     '<div class="col-sm-11">';
    $html +=         '<div class="col-sm-12">';
    $html +=             '<div class="col-xs-12 col-sm-4 col-md-3">';
    $html +=                '<label>Policy Info:</label>';
    $html +=                 '<input class="form-control" name="policyinfo[]" type="text" value="'+data.policy_no+'">';
    $html +=                 '<input name="policyid[]" type="hidden" value="'+data.id+'">';
    $html +=             '</div>';
    $html +=             '<div class="col-xs-12 col-sm-4 col-md-3">';
    $html +=                '<label>Insurance Provider :</label>';
    $html +=                 '<input class="form-control" type="text" name="provider[]" value="'+data.provider+'">';
    $html +=             '</div>';
    $html +=             '<div class="col-xs-12 col-sm-4 col-md-3">';
    $html +=                '<label>Status</label>';
    $html +=                 '<select name="status[]" class="form-control">';
    if (data.status == "1"){
        $html +=                     '<option value="1" selected>Active</option>';
        $html +=                     '<option value="0">Inactive</option>';
    }else{
        $html +=                     '<option value="1">Active</option>';
        $html +=                     '<option value="0" selected>Inactive</option>';
    }
    $html +=                 '</select>';
    $html +=             '</div>';
    $html +=         '</div>';
    $html +=         '<div class="col-sm-12">';
    $html +=             '<div class="col-xs-12 col-sm-4 col-md-3">';
    $html +=                '<label>Policy Expiration Date :</label>';
    $html +=                 '<input class="form-control" type="text" name="expiration[]" value="'+data.expire_date+'">';
    $html +=             '</div>';
    $html +=             '<div class="col-xs-12 col-sm-4 col-md-3">';
    $html +=                '<label>Next Renewal Date :</label>';
    $html +=                 '<input class="form-control" type="text" name="renewal[]" value="'+data.renewel_date+'">';
    $html +=             '</div>';
    $html +=             '<div class="col-xs-12 col-sm-4 col-md-3">';
    $html +=                '<label>Effective Date :</label>';
    $html +=                 '<input class="form-control" type="text" name="effective[]" value="'+data.effective_date+'">';
    $html +=             '</div>';
    $html +=         '</div>';
    $html +=     '</div>';
    $html +=     '<div class="col-sm-1">';
    if(initialVar == 0){
        $html +=         '<a class="clonerenteradd pop-add-icon" href="javascript:;">';
        $html +=             '<i class="fa fa-plus-circle" aria-hidden="true"></i>';
        $html +=         '</a>';
        $html +=         '<a class="clonerenterremove pop-add-icon" href="javascript:;" style="display: none;">';
        $html +=             '<i class="fa fa-minus-circle" aria-hidden="true"></i>';
        $html +=         '</a>';
    }else{
        $html +=         '<a class="clonerenteradd pop-add-icon" href="javascript:;" style="display:none;">';
        $html +=             '<i class="fa fa-plus-circle" aria-hidden="true"></i>';
        $html +=         '</a>';
        $html +=         '<a class="clonerenterremove pop-add-icon" href="javascript:;">';
        $html +=             '<i class="fa fa-minus-circle" aria-hidden="true" style="display:block;"></i>';
        $html +=         '</a>';
    }
    $html +=     '</div>';
    $html += '</div>';
    return $html;
}

function saveRenterInfo() {
    var form = $(".renter-container #renterformfield").serializeArray();
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/saveRenterData',
        data: {
            class: "TenantAjax",
            action: "saveRenterData",
            form: form
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                $(".renter-container .renter-edit-icon").show();
                $(".renter-container .renterlisting").show();
                $(".renter-container .renterform").hide();
                toastr.success(data.message);
                $('#renterinsurancetable').trigger( 'reloadGrid' );
                onTop(true,'renterinsurancetable');
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

}


function jqGrid1(status,deleted_at) {
    var tenantId = $(".tenant_id").val();
    var table = 'tenant_renter_insurance';
    var columns = ['Policyholder','Policy Information','Provider','Status','Expiration Date','Effective Date','Next Renewal','Last Update'];
    var select_column = [];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'user_id',value:tenantId,condition:'=',table:'tenant_renter_insurance'}];
    var pagination=[];
    var columns_options = [
        {name:'Policyholder',index:'policy_holder', width:150,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Policy Information',index:'policy_no', width:150,searchoptions: {sopt: conditions},table:table},
        {name:'Provider',index:'provider', width:140, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Status',index:'status', width:110, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
        {name:'Expiration Date',index:'expire_date', width:110, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Effective Date',index:'effective_date', width:110, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Next Renewal',index:'renewel_date', width:110, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Last Update',index:'updated_at', width:110, align:"center",searchoptions: {sopt: conditions},table:table},
        /*{name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table},*/

    ];
    var ignore_array = [];
    jQuery("#renterinsurancetable").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "tenant_renter_insurance",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:deleted_at,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'tenant_renter_insurance.updated_at',
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Renter Insurance",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}
function statusFormatter (cellValue, options, rowObject){
    if (cellValue == 0)
        return "Inactive";
    else if(cellValue == '1')
        return "Active";
    else
        return 'Inactive';

}
