$(document).ready(function() {

    $(".closeimagepopupicon").click(function () {
        $(".cropItData").hide();
    });
    $(document).on("click",".booknowredirect",function () {
        $("#shortTermRental .nav-tabs li").removeClass('active');
    $("#account-paybill").hide();
    $("#account-receive").show();
    $(".booknowredirect").addClass('active');
    $("#account-paybill2").hide();
    });
    $(document).on("click",".booknowredirect1",function () {
        $("#shortTermRental .nav-tabs li").removeClass('active');
    $("#account-paybill").show();
    $(".booknowredirect1").addClass('active');
    $("#account-receive").hide();
    $("#account-paybill2").hide();
    });
    $(document).on("click",".booknowredirect2",function () {
        $("#shortTermRental .nav-tabs li").removeClass('active');
    $("#account-paybill").hide();
    $("#account-receive").hide();
    $("#account-paybill2").show();
    $(".booknowredirect2").addClass('active');
    });
    $(".current_zip").val("10001");
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    $(".calander").val(date);
    var shortTermRenat=localStorage.getItem('short_term_rental');
    if(shortTermRenat=='1'){
        $('#shortTermRental').modal("show");
        localStorage.setItem('short_term_rental','0');
    }
    $("#checkFreq1").prop('checked','true');
    function greencalander() {
    $('.myCalendar table').remove();
    $('.myCalendar .month-head').remove();
    $('.myCalendar .year-month').remove();
        var date1 = new Date();
        var result = new Date(date1);
        result.setDate(result.getDate() + 1);
        console.log("<<<<<<<<",result);
        $('.myCalendar').calendar({
            date: new Date(),
            autoSelect: false, // false by default
            select: function (result) {
                //console.log("chakko");
            },
            toggle: function (y, m) {
                //console.log('TOGGLE', y, m);
            }
        });
       // $('.month-head').prev().remove();
        //$('.month-body').prev().remove();
      //  $('.year-month').prev().remove();
    }
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

    jQuery('.phone_format').mask('000-000-0000', {reverse: true});

    $('input[name="from_date"]').datepicker({
        minDate: "dateToday",
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('input[name="start_from_reocc"]').datepicker({
        minDate: "dateToday",
         yearRange: '2019:2030',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('input[name="till_date"]').datepicker({
        minDate: "dateToday",
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $(document).on('change','input[name="from_date"]',function () {
        var date=$(this).val();
        $('input[name="till_date"]').val(date);
        $('input[name="till_date"]').datepicker({
            minDate: date,
            changeMonth: true,
            changeYear: true,
            dateFormat: jsDateFomat
    })
    });
    $(".booknowredirect1").removeClass('active');
    $(document).on('change', '.checkFreq', function () {
        var checkFrequency = $("#shortTermRentalform").serializeArray();
        if (checkFrequency[0].value == 2) {
            $("#frequency_2").show();
            $("#frequency_1").hide();
            $(".prop_short").html("<option value=''>Property</option>");
            $(".build_short").html("<option value=''>Building</option>");
            $(".unit_short").html("<option value=''>Unit</option>");
        } else {
            $("#frequency_2").hide();
            $("#frequency_1").show();
            $(".prop_short").html("<option value=''>Property</option>");
            $(".build_short").html("<option value=''>Building</option>");
            $(".unit_short").html("<option value=''>Unit</option>");
        }
    });
    $(document).on("click", "#shorttermrentals", function () {
        $("#shortTermRental").addClass("modalScroll");
        $("body").addClass("modalopen-hide");
        $("#shortTermRental").modal(show);
        $(".right-links-outer").removeClass("show-links").addClass("hide-links");
        $(".booknowredirect2").removeClass("active");
    });
    $(document).on('click', '.searching_shortTerm', function () {
        var reoccur=$(".reoccurance_pattern ").val();
        var fre=$("#shortTermRentalform").serializeArray();
        console.log(fre[0]['value']);
        if(fre[0]['value']=='2' && reoccur=='0') {
            toastr.error("Please select recurrence pattern first");
        }
       else if(fre[0]['value']=='2' && reoccur=='2'){
            var checkboxes = document.getElementsByName('weekdays');
            var selected_days = [];
            for (var i=0; i<checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    selected_days.push(checkboxes[i].value);
                }
            }
            if(selected_days.length=='0'){
                toastr.error("Please select at least one weekday");
            }else{
                searchingProp();
                greencalander();
                dateselected();
            }
            }
        else{
            searchingProp();
            greencalander();
            dateselected();
        }
    });
    $(document).on('click', '.ic-arrow-angle-left', function () {
        dateselected();
    });
    $(document).on('click', '.ic-arrow-angle-right', function () {
        dateselected();
    });
    function dateselected() {
        var from_date=$('.from_date').val();
        var to_date=$('.till_date').val();
        var frequency=$('.checkFreq').val();
        var reoccurance_pattern=$('.reoccurance_pattern').val();
        var formData3=$("#shortTermRentalform").serializeArray();
        var frequency1=formData3[0]['value'];
        var start_from_reocc=$('.start_from_reocc').val();
        var end_after_reocc=$('.end_after_reocc').val();
        var every_reocc=$('.every_reocc').val();
        var every_month_reocc=$('.every_month_reocc').val();
        var monthly_week=$('.monthly_week').val();
        var monthly_days=$('.monthly_days').val();
        var monthly_every=$('.monthly_every').val();
        var formData=$("#general_short_term").serializeArray();
        var formData2=$("#shortTermRentalform").serializeArray();
        var checkboxes = document.getElementsByName('weekdays');
        var selected_days = [];
        for (var i=0; i<checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                selected_days.push(checkboxes[i].value);
            }
        }
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "goGreen",
                from_date:from_date,
                to_date:to_date,
                frequency:frequency,
                frequency1:frequency1,
                reoccurance_pattern:reoccurance_pattern,
                start_from_reocc:start_from_reocc,
                end_after_reocc:end_after_reocc,
                every_reocc:every_reocc,
                every_month_reocc:every_month_reocc,
                monthly_week:monthly_week,
                monthly_days:monthly_days,
                monthly_every:monthly_every,
                formData:formData,
                formData2:formData2,
                selected_days:selected_days
            },
            success: function (response) {
                var data = $.parseJSON(response);
                var month_year= $('.ic-target').next().text();
                var monthNames = ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"
                ];
                var count=data.length;
               // console.log(count);
                for(var i=0;i<count;i++){
                    var date= new Date(data[i]);
                    var month_years=monthNames[date.getMonth()]+' '+date.getFullYear();
                    var day=date.getDate();
                    console.log(day);
                    if(month_year==month_years){
                        var days=[];
                        $('.myCalendar table').find('td.ripple-element').each(function(key,val) {
                           var attr=$(this).attr('data-date');
                           if(attr!==undefined){
                               if(attr==day){
                                  var class_name=$(this).attr('class');
                                 class_name=class_name.substr(class_name.indexOf(' ')+18);
                                 console.log(class_name);
                                   $('.'+class_name).addClass('today');
                               }
                           }
                        });
                    }
                }
            },
        });
        // $('#account-paybill').show();
        // $('#account-receive').hide();
        // $(".booknowredirect").removeClass('active');
        // $(".booknowredirect1").addClass('active');

    }
    function searchingProp() {
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "searchingProp",
            },
            success: function (response) {
                var html = "";
                var response = JSON.parse(response);
                html += ' <option value="' + '0' + '">' + 'Property' + '</option>';
                for (var i = 0; i < response.data.prop.length; i++) {
                    html += ' <option value="' + response.data.prop[i].id + '">' + response.data.prop[i].property_name + '</option>';
                }
                $(".prop_short").html(html);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on('change', '.prop_short', function () {
        var id = $(".prop_short").val();
        console.log(id);
        searchingBuilding(id);
    }); $(document).on('change', '.unit_short', function () {
        var from_date=$('.from_date').val();
        var to_date=$('.till_date').val();
        var frequency=$('.checkFreq').val();
        var reoccurance_pattern=$('.reoccurance_pattern').val();
        var formData3=$("#shortTermRentalform").serializeArray();
        var frequency1=formData3[0]['value'];
        var unit_id=$(".unit_short").val();
        var id = $(this).val();
        var start_from_reocc=$('.start_from_reocc').val();
        var end_after_reocc=$('.end_after_reocc').val();
        var every_reocc=$('.every_reocc').val();
        var every_month_reocc=$('.every_month_reocc').val();
        var prop_id=$('.prop_short').val();
        var build_id=$('.build_short').val();
        var monthly_week=$('.monthly_week').val();
        var monthly_days=$('.monthly_days').val();
        var monthly_every=$('.monthly_every').val();
        var formData=$("#general_short_term").serializeArray();
        var formData2=$("#shortTermRentalform").serializeArray();
        var checkboxes = document.getElementsByName('weekdays');
        var selected_days = [];
        for (var i=0; i<checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                selected_days.push(checkboxes[i].value);
            }
        }
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "checkbookingavail",
                from_date:from_date,
                to_date:to_date,
                frequency:frequency,
                unit_id:unit_id,
                frequency1:frequency1,
                reoccurance_pattern:reoccurance_pattern,
                start_from_reocc:start_from_reocc,
                end_after_reocc:end_after_reocc,
                every_reocc:every_reocc,
                every_month_reocc:every_month_reocc,
                prop_id:prop_id,
                build_id:build_id,
                monthly_week:monthly_week,
                monthly_days:monthly_days,
                monthly_every:monthly_every,
                formData:formData,
                formData2:formData2,
                selected_days:selected_days
            },
            success: function (response) {
                var data = $.parseJSON(response);
                //  console.log(data);
                if(data=="null"){
                    getunitrent(id);
                    //searchingProp();
                    greencalander();
                    dateselected();
                    //greencalander();
                    /* $('#account-paybill').show();
                     $('#account-receive').hide();
                     $(".booknowredirect").removeClass('active');
                     $(".booknowredirect1").addClass('active');*/
                }else{
                    bootbox.confirm({
                        message: "The unit selected is booked for "+data+" .Please select another available unit or date range to proceed or cancel this request.",
                        buttons: {confirm: {label: 'Ok'}, cancel: {label: 'Cancel'}},
                        callback: function (result) {
                            if (result == true) {
                                $('.unit_short').val('0');
                            }else{
                                $('.unit_short').val('0');
                            }
                        }
                    });
                    var month_year= $('.ic-target').next().text();
                    var monthNames = ["January", "February", "March", "April", "May", "June",
                        "July", "August", "September", "October", "November", "December"
                    ];
                    var count=data.length;
                    // console.log(count);
                    for(var i=0;i<count;i++){
                        var date= new Date(data[i]);
                        var month_years=monthNames[date.getMonth()]+' '+date.getFullYear();
                        var day=date.getDate();
                        console.log(day);
                        if(month_year==month_years){
                            var days=[];
                            $('.myCalendar table').find('td.ripple-element').each(function(key,val) {
                                var attr=$(this).attr('data-date');
                                if(attr!==undefined){
                                    if(attr==day){
                                        var class_name=$(this).attr('class');
                                        class_name=class_name.substr(class_name.indexOf(' ')+18);
                                        var newClass=class_name.replace(" ", ".");
                                        setTimeout(function(){
                                            $('.'+newClass).addClass('active');
                                        }, 400);
                                        setTimeout(function(){
                                            $('.'+newClass).removeClass('today');
                                        }, 500);
                                    }
                                }
                            });
                        }


                    }
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

        // $('#account-paybill').show();
        // $('#account-receive').hide();
        // $(".booknowredirect").removeClass('active');
        // $(".booknowredirect1").addClass('active');
    });
    $("#shortTermRentalform").validate({
        ignore: 'hidden',
        rules: {
            prop_shortt: {
                required: true
            },
            build_shortt: {
                required: true
            },

            unit_shortt: {
                required: true,
            },
        },
    });
    $(document).on('click', '.booknow1', function () {
        if ($("#shortTermRentalform").valid()) {
            console.log($('.prop_short').val());
          //  console.log("hlo");
             $('#account-paybill').show();
             $('#account-receive').hide();
             $(".booknowredirect").removeClass('active');
             $(".booknowredirect1").addClass('active');
        }
        });
    function searchingBuilding(id) {
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "searchingBuilding",
                id: id
            },
            success: function (response) {
                var html = "";
                var response = JSON.parse(response);
                html += ' <option value="' + '0' + '">' + 'Building' + '</option>';
                for (var i = 0; i < response.data.prop.length; i++) {
                    html += ' <option value="' + response.data.prop[i].id + '">' + response.data.prop[i].building_name + '</option>';
                }
                $(".build_short").html(html);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on('change', '.build_short', function () {
        var id = $(".build_short").val();
        searchingUnit(id);
    });

    function searchingUnit(id) {
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "searchingUnit",
                id: id
            },
            success: function (response) {
                var html = "";
                var response = JSON.parse(response);
                html += ' <option value="' + '' + '">' + 'Unit' + '</option>';
                for (var i = 0; i < response.data.prop.length; i++) {
                    html += ' <option value="' + response.data.prop[i].id + '">' + response.data.prop[i].unit_prefix + "-" + response.data.prop[i].unit_no + '</option>';
                }
                $(".unit_short").html(html);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    function getunitrent(id) {

        var from_date=$('.from_date').val();
        var to_date=$('.till_date').val();
        var frequency=$('.checkFreq').val();
        var reoccurance_pattern=$('.reoccurance_pattern').val();
        var formData3=$("#shortTermRentalform").serializeArray();
        var frequency1=formData3[0]['value'];
        var start_from_reocc=$('.start_from_reocc').val();
        var end_after_reocc=$('.end_after_reocc').val();
        var every_reocc=$('.every_reocc').val();
        var every_month_reocc=$('.every_month_reocc').val();
        var monthly_week=$('.monthly_week').val();
        var monthly_days=$('.monthly_days').val();
        var monthly_every=$('.monthly_every').val();
        var formData=$("#general_short_term").serializeArray();
        var formData2=$("#shortTermRentalform").serializeArray();
        var checkboxes = document.getElementsByName('weekdays');
        var selected_days = [];
        for (var i=0; i<checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                selected_days.push(checkboxes[i].value);
            }
        }
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "getunitamount",
                id: id,
                from_date:from_date,
                to_date:to_date,
                frequency:frequency,
                frequency1:frequency1,
                reoccurance_pattern:reoccurance_pattern,
                start_from_reocc:start_from_reocc,
                end_after_reocc:end_after_reocc,
                every_reocc:every_reocc,
                every_month_reocc:every_month_reocc,
                monthly_week:monthly_week,
                monthly_days:monthly_days,
                monthly_every:monthly_every,
                formData:formData,
                formData2:formData2,
                selected_days:selected_days
            },
            success: function (response) {
                //console.log(response);
                $(".unit_rent").text(currencySign + response + '.00');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on('change', '.entity_company', function () {
        var check = document.getElementById("entity_company").checked;
        if (check == true) {
            $("#company_hidden_div").hide();
            $("#company_hidden_field").show();
        } else {
            $("#company_hidden_div").show();
            $("#company_hidden_field").hide();

        }

    });
    $(document).on('change', '.entity_company1', function () {
        var check = document.getElementById("entity_company1").checked;
        if (check == true) {
            $("#company_hidden_div1").hide();
            $("#company_hidden_field1").show();
        } else {
            $("#company_hidden_div1").show();
            $("#company_hidden_field1").hide();

        }

    });

    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getInitialData',
        data: {
            class: "TenantAjax",
            action: "getIntialData"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if (data.data.state.state != "") {
                    $("#driverProvince").val(data.data.state.state);
                }

                if (data.data.country.length > 0) {
                    var countryOption = "<option value='0'>Select</option>";
                    $.each(data.data.country, function (key, value) {
                        countryOption += "<option value='" + value.id + "' data-id='" + value.code + "'>" + value.name + " (" + value.code + ")" + "</option>";
                    });
                    $('.country_shortTerm').html(countryOption);
                    // $('.emergencycountry').html(countryOption);
                }

                if (data.data.propertylist.length > 0) {
                    var propertyOption = "<option value=''>Select</option>";
                    $.each(data.data.propertylist, function (key, value) {
                        propertyOption += "<option value='" + value.id + "' data-id='" + value.property_id + "'>" + value.property_name + "</option>";
                    });
                    $('#addTenant #property').html(propertyOption);
                }

                if (data.data.phone_type.length > 0) {
                    var phoneOption = "";
                    $.each(data.data.phone_type, function (key, value) {
                        phoneOption += "<option value='" + value.id + "'>" + value.type + "</option>";
                    });
                    $('.phone_shortTerm').html(phoneOption);
                    //   $('.phone_shortTerm').html(phoneOption);
                }

                if (data.data.carrier.length > 0) {

                    var carrierOption = "<option value=''>Select</option>";
                    $.each(data.data.carrier, function (key, value) {
                        carrierOption += "<option value='" + value.id + "'>" + value.carrier + "</option>";
                    });
                    $('.carrier_shortTerm').html(carrierOption);
                }
                if (data.data.carrier.length > 0) {

                    var carrierOption = "<option value='0'>Select</option>";
                    $.each(data.data.carrier, function (key, value) {
                        carrierOption += "<option value='" + value.id + "'>" + value.carrier + "</option>";
                    });
                    $('.addition_tenant_block select[name="additional_carrier"]').html(carrierOption);
                    $('.property_guarantor_form1 select[name="guarantor_carrier_1[]"]').html(carrierOption);
                    $('.additional_carrier').html(carrierOption);
                    $(' select[name="guarantor_form2_carrier_1[]"]').html(carrierOption);
                }

                if (data.data.referral.length > 0) {
                    var referralOption = "";
                    $.each(data.data.referral, function (key, value) {
                        referralOption += "<option value='" + value.id + "'>" + value.referral + "</option>";
                    });
                    // $('select[name="referralSource"]').html(referralOption);
                    $('.referralShortTerm').html(referralOption);
                }

                if (data.data.ethnicity.length > 0) {
                    var ethnicityOption = "";
                    $.each(data.data.ethnicity, function (key, value) {
                        ethnicityOption += "<option value='" + value.id + "'>" + value.title + "</option>";
                    });
                    //$('select[name="ethncity"]').html(ethnicityOption);
                    $('.ethnicity_short').html(ethnicityOption);
                }

                if (data.data.marital.length > 0) {
                    var maritalOption = "";
                    $.each(data.data.marital, function (key, value) {
                        maritalOption += "<option value='" + value.id + "'>" + value.marital + "</option>";
                    });
                    // $('select[name="maritalStatus"]').html(maritalOption);
                    // $('.addition_tenant_block select[name="maritalStatus"]').html(maritalOption);
                    $('.martial_short').html(maritalOption);
                }

                if (data.data.hobbies.length > 0) {
                    var hobbyOption = "";
                    $.each(data.data.hobbies, function (key, value) {
                        hobbyOption += "<option value='" + value.id + "'>" + value.hobby + "</option>";
                    });
                    $('.hobbies_short').html(hobbyOption);
                    $('.hobbies_short').multiselect({includeSelectAllOption: true, nonSelectedText: 'Select'});
                }

                if (data.data.veteran.length > 0) {
                    var abc = "";
                    var veteranOption = "<option value='0'>Select</option>";
                    $.each(data.data.veteran, function (key, value) {
                        veteranOption += "<option value='" + value.id + "'>" + value.veteran + "</option>";
                    });
                    // $('select[name="veteranStatus"]').html(veteranOption);
                    $('.veteran_shortTerm').html(veteranOption);
                }

                if (data.data.collection_reason.length > 0) {
                    var reasonOption = "";
                    $.each(data.data.collection_reason, function (key, value) {
                        reasonOption += "<option value='" + value.id + "'>" + value.reason + "</option>";
                    });
                    $('.property_collection select[name="collection_reason"]').html(reasonOption);
                }

                if (data.data.credential_type.length > 0) {
                    var typeOption = "";
                    var typeOption = "<option value='0'>Select</option>";
                    $.each(data.data.credential_type, function (key, value) {
                        typeOption += "<option value='" + value.id + "'>" + value.credential_type + "</option>";
                    });
                    $('.tenant-credentials-control select[name="credentialType[]"]').html(typeOption);
                }


            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
    $(document).on("click", ".primary-tenant-phone-row-short .fa-plus-circle", function () {
        var clone = $(".primary-tenant-phone-row-short:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".primary-tenant-phone-row-short").first().after(clone);
        $(".primary-tenant-phone-row-short:not(:eq(0))  .fa-plus-circle").hide();
        $(".primary-tenant-phone-row-short:not(:eq(0))  .fa-minus-circle").show();
        var phoneRowLenght = $(".primary-tenant-phone-row-short");
        console.log(phoneRowLenght);
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        if (phoneRowLenght.length == 2) {
            clone.find(".fa-plus-circle").hide();
        } else if (phoneRowLenght.length == 3) {
            clone.find(".fa-plus-circle").hide();
            $(".primary-tenant-phone-row-short:eq(0) .fa-plus-circle").hide();
        } else {
            $(".primary-tenant-phone-row-short:not(:eq(0)) .fa-plus-circle").show();
        }
    });

    $(document).on("click", ".primary-tenant-phone-row-short .fa-minus-circle", function () {

        var phoneRowLenght = $(".primary-tenant-phone-row-short");
        if (phoneRowLenght.length == 2) {
            $(".primary-tenant-phone-row-short:eq(0) .fa-plus-circle").show();
        } else if (phoneRowLenght.length == 3) {
            $(".primary-tenant-phone-row-short:eq(0) .fa-plus-circle").hide();
        } else {
            $(".primary-tenant-phone-row-short:not(:eq(0)) .fa-plus-circle").show();
        }

        $(this).parents(".primary-tenant-phone-row-short").remove();
    });
    $(document).on("click", ".email-plus-sign-short-term", function () {
        var emailRowLenght = $(".multipleEmailShortTerm");
        if (emailRowLenght.length == 2) {

            $(".email-plus-sign-short-term").hide();
        }
        var clone = $(".multipleEmailShortTerm:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".multipleEmailShortTerm").first().after(clone);
        $(".multipleEmailShortTerm:not(:eq(0))  .email-plus-sign-short-term").remove();
        $(".multipleEmailShortTerm:not(:eq(0))  .fa-minus-circle-short-term").show();

    });

    /*for multiple email textbox*/


    /*Remove Email Textbox*/
    $(document).on("click", ".fa-minus-circle-short-term", function () {
        var emailRowLenght = $(".multipleEmailShortTerm");

        if (emailRowLenght.length == 3 || emailRowLenght.length == 2) {
            $(".email-plus-sign-short-term").show();
        }
        $(this).parents(".multipleEmailShortTerm").remove();
    });

    /*Remove Email Textbox*/

    $(document).on("click", ".ssn-plus-sign-short_term", function () {
        var clone = $(".multipleSsnShortTerm:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".multipleSsnShortTerm").first().after(clone);
        $(".multipleSsnShortTerm:not(:eq(0))  .ssn-plus-sign-short_term").remove();
        $(".multipleSsnShortTerm:not(:eq(0))  .ssn-remove-sign-short-term").show();

    });
    /* for multiple SSN textbox*/


    /*remove ssn textbox*/
    $(document).on("click", ".ssn-remove-sign-short-term", function () {
        $(this).parents(".multipleSsnShortTerm").remove();
    });


    /*remove ssn textbox*/
    $("#general_short_term").validate({
        ignore: 'hidden',
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            carrier: {
                required: true,
            },
            phone_number: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },

        },
       /* errorPlacement: function(error, element) {
            if (element.attr("name") == "email")
                error.insertAfter(".err-class");
        }*/
    });
    $(document).on("click", ".savePersonalShortTerm", function () {

        var rent=$(".unit_rent").html();
        var first_name=$(".first_name_shoort").val();
        var last_name=$(".short_term_last_name").val();
        $(".amountTobePaid").val(rent);
        $(".cardname-input").val(first_name+' '+last_name);

        $(".amountTobePaid").prop("readonly",true);
        if ($("#general_short_term").valid()) {   // test for validity
            $(".booknowredirect1").removeClass('active');
            $(".booknowredirect2").addClass('active');
            $('#account-paybill2').show();
            $('#account-paybill').hide();
          //  savePersonalInformation();
        } else {
           return false;
        }

     //   savePersonalInformation();
    });

    $(document).on("click", ".submit_pay", function () {
        savePersonalInformation();
    });
    function savePersonalInformation() {
        var frequency=$('.checkFreq').val();
        var formData3=$("#shortTermRentalform").serializeArray();
        var frequency1=formData3[0]['value'];
        var reoccurance_pattern=$(".reoccurance_pattern").val();
        var start_from_reocc=$('.start_from_reocc').val();
        var end_after_reocc=$('.end_after_reocc').val();
        var every_reocc=$('.every_reocc').val();
        var every_month_reocc=$('.every_month_reocc').val();
        var prop_id=$('.prop_short').val();
        var build_id=$('.build_short').val();
        var unit_id=$('.unit_short').val();
        var from_date=$('.from_date').val();
        var to_date=$('.till_date').val();
        var monthly_week=$('.monthly_week').val();
        var monthly_days=$('.monthly_days').val();
        var monthly_every=$('.monthly_every').val();
        var abc=$(".shortImage .img-outer").html();
        var image=$(abc).attr('src');
        var formData=$("#general_short_term").serializeArray();
        var formData2=$("#shortTermRentalform").serializeArray();
        var checkboxes = document.getElementsByName('weekdays');
        var selected_days = [];
        for (var i=0; i<checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                selected_days.push(checkboxes[i].value);
            }
        }
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "savePersonalInformation",
                formData:formData,
                image:image,
                formData2:formData2,
                unit_id:unit_id,
                prop_id:prop_id,
                build_id:build_id,
                from_date:from_date,
                to_date:to_date,
                frequency:frequency,
                frequency1:frequency1,
                reoccurance_pattern:reoccurance_pattern,
                start_from_reocc:start_from_reocc,
                end_after_reocc:end_after_reocc,
                every_reocc:every_reocc,
                every_month_reocc:every_month_reocc,
                selected_days:selected_days,
                monthly_week:monthly_week,
                monthly_days:monthly_days,
                monthly_every:monthly_every
            },
            success: function (response) {
                var response = JSON.parse(response);
                localStorage.setItem("rowcolorTenant",true);
                toastr.success(response.message);
                setTimeout(function(){
                    var base_url = window.location.origin;

                    window.location.href = base_url + '/ShortTermRental/RentersListing';
                }, 1000);
                },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $(document).on("change",".cropit-image-input",function(){
        photo_videos = [];
        var fileData = this.files;
        var type= fileData.type;
        var elem = $(this);
        $.each(this.files, function (key, value) {
            var type = value['type'];
            var size = isa_convert_bytes_to_specified(value['size'], 'k');
            var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
            var uploadType = 'image';
            var validSize = '1030';
            var arrayType = validImageTypes;

            if ($.inArray(type, arrayType) < 0) {
                toastr.warning('Please select file with valid extension only!');
            } else {

                if (parseInt(size) > validSize) {
                    var validMb =  1;
                    toastr.warning('Please select documents less than '+validMb+' mb!');
                } else {
                    size = isa_convert_bytes_to_specified(value['size'], 'k') + 'kb';
                    photo_videos.push(value);
                    var src = '';
                    var reader = new FileReader();
                    $('#photo_video_uploads').html('');


                    $(".popup-bg").show();
                    elem.next().show();

                }

            }

        });

    });

    function isa_convert_bytes_to_specified(bytes, to) {
        var formulas =[];
        formulas['k']= (bytes / 1024).toFixed(1);
        formulas['M']= (bytes / 1048576).toFixed(1);
        formulas['G']= (bytes / 1073741824).toFixed(1);
        return formulas[to];
    }


    $(document).ready(function(){
        $('.image-editor').cropit({
            imageState: {
                src: subdomain_url+'500/400',
            },
        });
        $(document).on("click",'.export',function(){
            $(this).parents('.cropItData').hide();
            $(this).parent().prev().val('');
            var dataVal = $(this).attr("data-val");
            var imageData = $(this).parents('.image-editor').cropit('export');
            var image = new Image();
            image.src = imageData;
            $(".popup-bg").hide();
            $(this).parent().parent().prev().find('div').html(image);


        });

    });
    $(document).on("change",".reoccurance_pattern",function () {
        $(".prop_short").html("<option value=''>Property</option>");
        $(".build_short").html("<option value=''>Building</option>");
        $(".unit_short").html("<option value=''>Unit</option>");
    var value=$(this).val();
    if(value=="1"){
        $('.occur1').show();
        $('.occur2').hide();
        $('.monthy-reoccuring').hide();
    }
    if(value=="2"){
        $('.occur1').hide();
        $('.monthy-reoccuring').hide();
        $('.occur2').show();
    }
    if(value=="3"){
        $('.monthy-reoccuring').show();
        $('.occur1').hide();
        $('.occur2').hide();
    }

    });
    $(document).on("click",".preview_short",function () {
    $("#account-receive").show();
    $("#account-paybill").show();
    $("#account-paybill2").hide();
    $(".searching_shortTerm").hide();
    $(".booknow1").hide();
    $("#shortTermRentalform select").attr("disabled","true");
    $("#shortTermRentalform input").attr("disabled","true");
    $("#general_short_term input").attr("disabled","true");
    $("#general_short_term select").attr("disabled","true");
    $(".multiselect").attr("disabled","true");
    $(".savePersonalShortTerm").hide();
    $(".ok_preview").show();
    $(".cancel_div").show();
    });
    $(document).on("click",".ok_preview",function () {
        $("#account-receive").hide();
        $("#account-paybill").hide();
        $("#account-paybill2").show();
        $(".searching_shortTerm").show();
        $(".booknow1").show();
        $("#shortTermRentalform select").removeAttr("disabled","true");
        $("#shortTermRentalform input").removeAttr("disabled","true");
        $("#general_short_term input").removeAttr("disabled","true");
        $("#general_short_term select").removeAttr("disabled","true");
        $(".multiselect").removeAttr("disabled","true");
        $(".savePersonalShortTerm").show();
        $(".ok_preview").hide();
        $(".cancel_div").hide();

    });
    $(document).on("click",".cancel_div",function () {
        bootbox.confirm({
            message: "Are you sure you want to cancel the existing booking? All the information filled will be lost.",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    localStorage.setItem('short_term_rental','1');
                    setTimeout(function () {
                        window.location.reload();
                    },1000);
                    /*setTimeout(function(){
                        $("#shortTermRental").modal("show");
                    }, 2000);*/
                }
            }
        });
    });
    $(document).on("click",".close_short_term",function () {
        bootbox.confirm({
            message: "Are you sure you want to close Book Now section? All the information filled will be lost.",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                        window.location.reload();
                }else{

                }
            }
        });
    });
    function getZipCode(element,zipcode,city,state,country,county,validation){
        $(validation).val('1');
        $.ajax({
            type: 'post',
            url: '/common-ajax',
            data: {zip_code: zipcode,class: 'CommonAjax', action: 'getZipcode'},
            success: function (response) {
                var data = JSON.parse(response);
                if(data.code == 200){
                    if(city !== undefined)$(city).val(data.data.city);
                    if(state !== undefined)$(state).val(data.data.state);
                    if(country !== undefined)$(country).val(data.data.country);
                    if(county !== undefined)$(county).val(data.data.county);
                } else {
                    getAddressInfoByZip(element,zipcode,city,state,country,county,validation);
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
    getZipCode('.current_zip','10001','.current_city','.current_state','.propertyCountry_name','','');
    $(".current_zip").focusout(function () {
        getZipCode('.current_zip ',$(this).val(),'.current_city','.current_state','.propertyCountry_name','','');
    });
    $("#financialCardInfo").submit(function( event ) {
        event.preventDefault();
        if ($('#financialCardInfo').valid()) {
            $('#loadingmessage').show();
            var formData = $('#financialCardInfo').serializeArray();
            $.ajax({
                type: 'post',
                url: '/payment-ajax',
                data: {
                    class: 'PaymentAjax',
                    action: "addCard",
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success'){
                        $('#loadingmessage').hide();
                        toastr.success(response.message);
                        if(response.stripe_account_id){
                            $('#billing-subscription').modal('toggle');
                            setTimeout(function(){
                                window.location.reload();
                            }, 3000);
                        }else{
                            $('#billing-subscription').modal('hide');
                            $(".basic-payment-detail").show();
                            $(".apx-adformbox-content").hide();
                            //$(".basicDiv").addClass("active");
                            $(".paymentDiv").removeClass("active");
                            setTimeout(function(){
                                window.location.reload();
                            }, 3000);
                        }

                    } else {
                        $('#loadingmessage').hide();
                        toastr.error(response.message);
                    }
                },
                error: function (data) {

                }
            });
        }
    });
    var stripe = Stripe('pk_test_jF5tXlX0qLcCR1YAX9WB8FaL');

// Create an instance of Elements.
    var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#32325d',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

// Create an instance of the card Element.
    var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

// Handle real-time validation errors from the card Element.
    card.addEventListener('change', function(event) {
        event.preventDefault();
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });
$(document).on("click",".submit_payment_class",function () {
    // general_short_term
    if($("#shortTermRentalform").valid()){
    if($("#general_short_term").valid()){
        checkpayment();
    }else{
       toastr.warning("Please fill all required fields in previous tabs");
    }
    }else{
        toastr.warning("Please fill all required fields in previous tabs");
    }
//checkpayment();
});
// Handle form submission.
   /* var form = document.getElementById('payment-form');
    form.addEventListener('submit', function(event) {*/
        // event.preventDefault();
        function checkpayment(){
        stripe.createToken(card).then(function(result) {
            if (result.error) {
                // Inform the user if there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                console.log(result);
                var token_id = result.token.id;
                makepayment(token_id);
            }
        });
    /*});*/
}
    function makepayment(token_id) {
        var currency = 'USD';
        var Amount = $(".unit_rent").html();
        var user_type = 'PM';
        var user_id = "<?php echo $_SESSION[SESSION_DOMAIN]['cuser_id']; ?>";
        // Insert the token ID into the form so it gets submitted to the server
        $.ajax({
            url: '/ShortTermRental-Ajax',
            type: 'POST',
            data: {
                "action": "makepayment",
                "class": "TenantShortTermAjax",
                "token_id":token_id,
                "currency":currency,
                "amount": Amount,
                "user_id":user_id,
                "user_type":user_type
            }, beforeSend: function() {
                // setting a timeout
                $('#loadingmessage').show();
            },
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status=='success')
                {
                    savePersonalInformation();
                }
                else if(response.status=='accountError')
                {
                toastr.error(response.message);
                }
                else
                {
                    toastr.error(response.message);
                }


            }
        });



    }

});
