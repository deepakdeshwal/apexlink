$(document).ready(function () {

    $(document).on("click",".hoa-container .print_hoa",function(){

    });

    $(document).on("click",".hoa-container #hoa_form .hoa_cancel",function(){
        $(".hoa-container #hoa_form").trigger('reset');
        $(".hoa-container .hoa_form_block").hide();
        $(".hoa-container .hoa_table_block").show();
        $(".hoa-container .addhoabtn_block").show();
        $(".hoa-container .print_hoa").show();
        $('.hoa-container .hoa_table_block #hoa_table').trigger( 'reloadGrid' );
    });

    $(document).on("click",".hoa-container .addhoabtn_block",function(){
        $(this).hide();
        $(".hoa-container .hoa_form_block").trigger('reset')
        $(".hoa-container .hoa_form_block").show();
        $(".hoa-container .hoa_table_block").hide();
        $(".hoa-container .print_hoa").hide();
        getHoaInfo();
    });

    $(document).on("change",".upload-edittenant-photo input[type='file']",function(){
        readURL(this);
    });

    /*$(document).on("click",".renter-container .renter-edit-icon",function(){
        $(this).hide();
        $(".renter-container .renterlisting").hide();
        $(".renter-container .renterform").show();
        getRenterInfo();
    });*/


    $(document).on("click",".hoa-container #hoa_form .hoa_save",function(e){
        e.preventDefault();
        $(".hoa-container #hoa_form").trigger('submit');
        return false;
    });

    $(document).on("submit",".hoa-container #hoa_form", function(e){
        e.preventDefault();
        saveHoaInfo();
        return false;
    });
    jqGridHoa('All');

    $("#select_all_hoa_checkbox").click(function () {
        $(".hoa_checkbox").prop('checked', $(this).prop('checked'));
    });
    
    $(document).on("click",".hoa_image_link",function () {
        var dataId = $(this).attr("data-id");
        if (dataId != ""){
            getHoaLinkImage(dataId);
        }
    });

    $(document).on("change","#hoa_table .select_options",function(){
        var id = $(this).attr('data_id');
        var tenant_id = $('.tenant_id').val(id);
        var action = $(this).val();
        if(action =="Edit"){
            $(".hoa-container .hoa_form_block").show();
            $(".hoa-container .hoa_form_block input[name='hoa_tenant']").val(id);
            $(".hoa-container .hoa_table_block").hide();
            $(".hoa-container .addhoabtn_block").hide();
            $(".hoa-container .print_hoa").hide();
            $(".hoa-container #hoa_form input[name='hoa_date']").datepicker({dateFormat:jsDateFomat});
            var currentDate = $.datepicker.formatDate(jsDateFomat, new Date());
            $(".hoa-container #hoa_form input[name='hoa_date']").val(currentDate);
            $(".hoa-container #hoa_form input[name='hoa_time']").timepicker({
                timeFormat: 'h:mm p'
            });

            $.ajax({
                type: 'post',
                url: '/Tenantlisting/getHoaData',
                async:false,
                data: {
                    class: "TenantAjax",
                    action: "getHoaData",
                    rowIds:id
                },
                success: function (response) {
                    var data = JSON.parse(response);

                    if (data.status == "success") {
                        var record = data.data;
                        $(".hoa-container #hoa_form input[name='hoa_id']").val(record.hoa_violation_id);
                        $(".hoa-container #hoa_form input[name='hoa_date']").val(record.date);
                        $(".hoa-container #hoa_form input[name='hoa_time']").val(record.time);
                        $(".hoa-container #hoa_form input[name='hoa_type']").val(record.hoa_type);
                        $(".hoa-container #hoa_form textarea[name='hoa_text']").text(record.hoa_description);
                        $(".hoa-container #hoa_form .hoa_photo1 .img-outer").html(record.upload_pic_1);
                        $(".hoa-container #hoa_form .hoa_photo2 .img-outer").html(record.upload_pic_2);
                        $(".hoa-container #hoa_form .hoa_photo3 .img-outer").html(record.upload_pic_3);
                        default_form_data = $("#hoa_form").serializeArray();
                        setTimeout(function () {
                            edit_date_time(record.updated_at);
                        },2000);
                    } else if (data.status == "error") {
                        toastr.error(data.message);
                    } else {
                        toastr.error(data.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }else if(action=="Delete"){
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        deleteTableData('hoa_violation',id);
                        setTimeout(function(){
                            $('#hoa_table').trigger('reloadGrid');
                        },500);

                    }

                }
            });
        }
    });



});

function deleteTableData(tablename,id){

    $.ajax({
        url:'/editTenant?action=deleteRecords&class=EditTenant&id='+id+'&tablename='+tablename,
        type: 'GET',
        success: function (data) {
            var info = JSON.parse(data);
            return toastr.success(info.message);

        },
        cache: false,
        contentType: false,
        processData: false
    });

}

function getHoaLinkImage(dataId){

    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getHoaData',
        async:false,
        data: {
            class: "TenantAjax",
            action: "getHoaData",
            rowIds: dataId
        },
        success: function (response) {
            var data = JSON.parse(response);

            if (data.status == "success") {
                var imageHtml = "";
                var path = data.data.path;
                if (data.data.upload_pic_1 != ""){
                    var imagePath1 = path+""+data.data.upload_pic_1;
                    imageHtml += "<div class='col-sm-3 col-md-3'><img src='"+imagePath1+"'></div>";
                }
                if (data.data.upload_pic_2 != ""){
                    var imagePath2 = path+""+data.data.upload_pic_2;
                    imageHtml += "<div class='col-sm-3 col-md-3'><img src='"+imagePath2+"'></div>";
                }
                if (data.data.upload_pic_3 != ""){
                    var imagePath3 = path+""+data.data.upload_pic_3;
                    imageHtml += "<div class='col-sm-3 col-md-3'><img src='"+imagePath3+"'></div>";
                }
                $("#hoa_image_popup .modal-body .col-sm-12").html(imageHtml);
                $("#hoa_image_popup").modal('show');
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(input).prev().prev().find("img").attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function getHoaInfo(){
    var tenantId = $(".tenant_id").val();
    var rString = randomString(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    $(".hoa-container #hoa_form input[name='hoa_id']").val(rString);
    $(".hoa-container #hoa_form input[name='hoa_date']").datepicker({dateFormat:jsDateFomat});
    var currentDate = $.datepicker.formatDate(jsDateFomat, new Date());
    $(".hoa-container #hoa_form input[name='hoa_date']").val(currentDate);
    $(".hoa-container #hoa_form input[name='hoa_time']").timepicker({
        timeFormat: 'h:mm p'
    });

    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getHoaInfo',
        data: {
            class: "TenantAjax",
            action: "getHoaInfo",
            tenantId:tenantId
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                if (data.data.length > 0 ){
                    var hoa = data.data;
                    var html = '';
                    for(var i = 0; i < hoa.length; i++ ){
                        html += '<option value="'+hoa[i].id+'">'+hoa[i].type+'</option>';
                    }
                    $(".hoa-container #hoa_form select[name='hoa_type']").html(html);
                    default_form_data = $("#hoa_form").serializeArray();
                }
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

function saveHoaInfo() {
    var tenantId = $(".formtenant_id").val();
    var form = $('#hoa_form')[0];
    var formData = new FormData(form);

    var hoa_image1 = [];
    $(".hoa_photo1").each(function(index) {
        var img1 = $(this).find(".img-outer").html();
        console.log(img1);
        hoa_image1.push(img1);

    });
    var hoaImg1 =  JSON.stringify(hoa_image1);

    var hoa_image2 = [];
    $(".hoa_photo2").each(function(index) {
        var img2 = $(this).find(".img-outer").html();
        hoa_image2.push(img2);
    });
    var hoaImg2 =  JSON.stringify(hoa_image2);

    var hoa_image3 = [];
    $(".hoa_photo3").each(function(index) {
        var img3 = $(this).find(".img-outer").html();
        hoa_image3.push(img3);
    });
    var hoaImg3 =  JSON.stringify(hoa_image3);

    formData.append('action','saveHoaData');
    formData.append('class','tenantAjax');
    formData.append('tenant_id',tenantId);
    formData.append('hoaImg1',hoaImg1);
    formData.append('hoaImg2',hoaImg2);
    formData.append('hoaImg3',hoaImg3);

    var form = $(".hoa-container #hoa_form").serializeArray();
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/saveHoaData',
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                $(".hoa-container .hoa_form_block").hide();
                $(".hoa-container .hoa_table_block").show();
                $(".hoa-container .addhoabtn_block").show();
                $(".hoa-container .print_hoa").show();
                $('.hoa-container .hoa_table_block #hoa_table').trigger( 'reloadGrid' );
                toastr.success(data.message);
                onTop(true);
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

}

function jqGridHoa(status, deleted_at) {
    var tenantId = $(".tenant_id").val();
    var table = 'hoa_violation';
    var columns = ['<input type="checkbox" id="select_all_hoa_checkbox">','HOA Violation ID','Date','Time','img1','img2','img3','HOA Type','Photo/Image Link','Description','Action'];
    var select_column = ['Edit','Delete'];
    var joins = [{table:'hoa_violation',column:'hoa_type',primary:'id',on_table:'hoa_violation_type'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'user_id',value:tenantId,condition:'=',table:'hoa_violation'}];
    var extra_columns = [];
    var columns_options = [
        {name: 'Id', index: 'id', width: 170, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatter},
        {name:'HOA Violation ID',index:'hoa_violation_id', width:170,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Date',index:'date',align:"center", width:170,searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Time',index:'time',align:"center", width:170,searchoptions: {sopt: conditions},table:table,change_type:'time'},
        {name:'img1',index:'upload_pic_1', width:170,align:"center",hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'img2',index:'upload_pic_2', width:170,align:"center",hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'img3',index:'upload_pic_3', width:170,align:"center",hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'HOA Type',index:'type', width:170,align:"center",searchoptions: {sopt: conditions},table:'hoa_violation_type',formatter:fmattr},
        {name:'Photo/Image Link',index:'id', width:170, align:"center",searchoptions: {sopt: conditions},table:table, formatter: checkImages},
        {name:'Description',index:'hoa_description', width:170,searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'select', title: false, width:135,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table}
    ];
    var ignore_array = [];
    jQuery("#hoa_table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'hoa_violation.updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "HOA Violations",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}



    $(document).on("click",".getImagesByTable",function(){
      tablename = $(this).attr('data-tablename');
      id = $(this).attr('data-id');
      
         $.ajax({
        url:'/editTenant?action=getImageByTable&class=EditTenant&id='+id+'&tablename='+tablename,
        type: 'GET',
        success: function (data) {
             var image = JSON.parse(data);

            setTimeout(function(){
                $('#imageModel').modal('show');
                $('#getImage1').html(image.img1);
                $('#getImage2').html(image.img2);
                $('#getImage3').html(image.img3);
            }, 400);

            
              },
        cache: false,
        contentType: false,
        processData: false
    });  
  });



function checkImages(cellvalue, options, rowObject) {

    if (rowObject != undefined) {
       var image1 = rowObject.img1;
       var image2 = rowObject.img2;
       var image3 = rowObject.img3;
        if(image3=="" && image2=="" && image1==""){
            return "No Image Uploaded";
        }else{
            return "<a href='javascript:void(0);' data-id='"+rowObject.id+"' class='getImagesByTable' data-tablename='hoa_violation'>Hoa Violations Image/Photo</a>";
        }
    }  
}



function actionCheckboxFmatter(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {
        return '<input type="checkbox" name="hoa_checkbox[]" class="hoa_checkbox" id="hoa_checkbox_' + rowObject.id + '" data_id="' + rowObject.id + '"/>';
    }
}
function fmattr(cellvalue, options, rowObject) {

    if(cellvalue == "Select")
    {
        return "";
    }
    else
    {
        return cellvalue;
    }
}


function statusFormatter (cellValue, options, rowObject){
    if (cellValue == 0)
        return "Inactive";
    else if(cellValue == '1')
        return "Active";
    else
        return 'Inactive';

}

/*jquery to open modal for complaint box to print */
$(document).on("click", '#tenantPrint', function () {
    favorite = [];
    var no_of_checked = $('[name="hoa_checkbox[]"]:checked').length
    if (no_of_checked == 0) {
        toastr.error('Please select atleast one HOA Violations.');
        return false;
    }
    $.each($("input[name='hoa_checkbox[]']:checked"), function () {
        favorite.push($(this).attr('data_id'));
    });

    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getInitialData',
        data: {class: 'TenantAjax', action: 'getHoaPrintData', 'complaint_ids': favorite},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $("#print_hoa").modal('show');
                $("#modal-body-hoa").html(response.html)
            } else {
                toastr.warning('Record not updated due to technical issue.');
            }
        }
    });
});



$(document).on("click",".resetHoa",function(){
  var defaultIgnoreArray = [];
  resetEditForm("#hoa_form",[],true,default_form_data,[]);

});





