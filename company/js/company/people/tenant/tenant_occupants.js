$(document).ready(function() {

    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid_Occupants('All');
    function jqGrid_Occupants(status, deleted_at) {
        var tenant_id = $("input[name='tenantapplytaxid']").val();
        // alert(tenant_id);
        // return false;
        var protocol = location.port;
        if (protocol != "") {
            var webpath = location.protocol+"//"+location.hostname+":"+protocol;
        }else{
            var webpath = location.protocol+"//"+location.hostname;
        }
        var table = 'tenant_additional_details';
        var columns = ['Name','SSN/SIN/ID', 'Ethnicity', 'Martial Status','Veteran Status','Relationship','Is Financial Responsible','Action'];
        //if(status===0) {
        var select_column = [''];
        //}
        var joins = [{table:'tenant_additional_details',column:'id',primary:'user_id',on_table:'tenant_ssn_id'},{table:'tenant_additional_details',column:'ethnicity',primary:'id',on_table:'tenant_ethnicity'},{table:'tenant_additional_details',column:'veteran_status',primary:'id',on_table:'tenant_veteran_status'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [{column:'user_id',value:tenant_id,condition:'=',table:'tenant_additional_details'}];
        var pagination=[];
        var columns_options = [

            {name:'Name',index:'first_name', width:195,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'name'},
            {name:'SSN/SIN/ID',index:'ssn',align:"center", width:195,searchoptions: {sopt: conditions},table:'tenant_ssn_id'},
            {name:'Ethnicity',index:'title', width:195, align:"center",searchoptions: {sopt: conditions},table:'tenant_ethnicity'},
            {name:'Martial Status',index:'marital_status', width:195, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter2 },
            {name:'Veteran Status',index:'veteran', width:200, align:"center",searchoptions: {sopt: conditions},table:'tenant_veteran_status'},
            {name:'Relationship',index:'relationship', width:200, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter3},
            {name:'Is Financial Responsible',index:'financial_responsible', width:200, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Action',index:'', title: false, width:135,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: function () {
                    return "<img src='"+webpath+"/company/images/edit-icon.png' id='editOccupant' title='Edit' alt='my image' style='cursor: pointer; '/>&nbsp;&nbsp;<img src='"+webpath+"/company/images/cross-icon.png' id='deactivateOccupant' title='Deactivate' alt='my image' style='cursor: pointer;'/>";
                }, edittype: 'select',search:false,table:table},
        ];
        var ignore_array = [];
        jQuery(".occcupants_table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "tenant_additional_details",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: '',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Occupants",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }
    function statusFormatter (cellValue, options, rowObject){

        if (cellValue == 0)
            return "No";
        else if(cellValue == '1')
            return "Yes";

    }
    function statusFormatter2 (cellValue, options, rowObject){

        if (cellValue == 1)
            return "Single";
        else if(cellValue == '2')
            return "Married";
            else
            return "";

    }
    function statusFormatter3 (cellValue, options, rowObject){
        if(cellValue == '1')
            return "Brother";
        if(cellValue == '2')
            return "Daughter"
        if(cellValue == '3')
            return "Employer"
        if(cellValue == '4')
            return "Father"
             if(cellValue == '5')
            return "Friend"
        if(cellValue == '6')
            return "Mentor";
        if(cellValue == '7')
            return "Mother";
        if(cellValue == '8')
            return "Neighbor";
        if(cellValue == '9')
            return "Nephew";
        if(cellValue == '10')
            return "Niece";
        if(cellValue =='11')
            return "Owner";
        if(cellValue =='12')
            return "Partner";
        if(cellValue =='13')
            return "Sister"; 
        if(cellValue =='14')
            return "Son";
        if(cellValue =='15')
            return "Spouse";
        if(cellValue =='16')
            return "Teacher";
        if(cellValue =='17')
            return "Other";
        if(cellValue =='')
            return "";

    }
    getEmergencyDetails();
    function getEmergencyDetails() {
        var tenant_id = $("input[name='tenantapplytaxid']").val();
        $.ajax({
            type: 'post',
            url: '/tenantOccupantsTypeAjax',
            data: {
                class: "TenantOccupantsAjax",
                action: "getEmergencyDetails",
                tenant_id:tenant_id

            },
            success: function (response) {
                var response = JSON.parse(response);
                for (var i = 0; i < response.data.user.length; i++)
                {
                    $("#emergency_detail").text(response.data.user[i].emergency_contact_name);
                    $("input[name='contact_emer_occupancy']").val(response.data.user[i].emergency_contact_name);
                    $("#country_code").text(response.data.user[i].emergency_country_code);
                    $("select[name='rela_emer_occupancy']").val(response.data.user[i].emergency_relation);
                    $("input[name='ph_emer_occupancy']").val(response.data.user[i].emergency_phone_number);
                    $("#emergency_contact").text(response.data.user[i].emergency_phone_number);
                    $("select[name='countryCode_emer_occupancy']").val(response.data.user[i].emergency_country_code);
                    $("#emergency_contact_email").text(response.data.user[i].emergency_email);
                    $("input[name='email_emer_occupancy']").val(response.data.user[i].emergency_email);
                    $("#relationship_contact").text(response.data.user[i].emergency_relation);
                }


            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $(document).on('click','#edit_occupants',function () {

        $('#add_occupant').show(500);
        $('#edit_occupants').hide();

    }); $(document).on('click','#editOccupant',function () {

        $('.occupancy_div').show(500);
        $('#add_occupant').hide();
        var elem = $(this).parent().parent().attr("id");
        $("input[name='hidden_id']").val(elem);
        getoccupants(elem);

    });
    $(document).on('click','#add_occupant',function () {

        $('.occupancy_div').show(500);
        /*  $('.add_occupancy_div').hide();*/

        $('#add_occupant').hide();


    }); /*$(document).on('click','#cancel_occupancy',function () {

        $('.occupancy_div').hide();
        $('.add_occupancy_div').show(500);
        $('#add_occupant').show();


    });*/
    $(document).on('click','#add_occupany_button',function (e) {
        addOccupant();
        return false;
    });
    $(document).on('click','#deactivateOccupant',function (e) {
        var tenant_id = $("input[name='tenantapplytaxid']").val();
        var elem = $(this).parent().parent().attr("id");
        bootbox.confirm("Do you want to delete this record?", function (result) {
            if (result == true) {
                decativeOccupant(elem);
            } else {

                window.location.href = window.location.origin + '/Tenant/ViewEditTenant?tenant_id='+tenant_id
            }
        });


    });
    function getoccupants(elem) {
        var id = elem;
        var tenant_id = $("input[name='tenantapplytaxid']").val();
        $.ajax({
            type: 'post',
            url: '/tenantOccupantsTypeAjax',
            data: {
                class: "TenantOccupantsAjax",
                action: "getoccupants",
                id:id,
                tenant_id:tenant_id,

            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log(response.data.user);
                if (response.status == 'success' && response.code == 200) {
                    $("input[name='firstname_occupancy']").val(response.data.user.first_name);
                    $("input[name='middlename_occupancy']").val(response.data.user.mi);
                    $("input[name='lastname_occupancy']").val(response.data.user.last_name);
                    $("input[name='medianName_occupancy']").val(response.data.user.maiden_name);
                    $("input[name='nickname_occupancy']").val(response.data.user.nick_name);

                    $("#email_occupancy1").val(response.data.user.email1);
                    $("select[name='salutation_occupancy']").val(response.data.user.salutation);
                    $("select[name='gender_occupancy']").val(response.data.user.gender);
                    $("select[name='ethncity_occupancy']").val(response.data.user.ethnicity);
                    $("select[name='maritalStatus_occupancy']").val(response.data.user.marital_status);
                    $("select[name='edit_general_veteran_occupancy']").val(response.data.user.veteran_status);
                    $("select[name='rela_occupancy']").val(response.data.user.relationship);
                    $("select[name='referal_occupancy']").val(response.data.user.referral_source);
                    $("input[name='sinn_occupancy']").val(response.data.ssn.ssn);
                    $("select[name='edit_general_hobby_occupancy']").val(response.data.hob.hobby);

                    if(response.data.user.financial_responsible != null)
                    {
                        if(response.data.user.financial_responsible=='1')
                        {
                            $("#responsible_occupants").attr("checked",true);
                        }
                    }
                    else
                    {
                        $("#responsible_occupants").attr("checked",false);
                    }


                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    function addOccupant() {
        var tenant_id = $("input[name='tenantapplytaxid']").val();

        var formData = $('#occupancy_Tenant').serializeArray();
        $.ajax({
            type: 'post',
            url: '/tenantOccupantsTypeAjax',
            data: {
                class: "TenantOccupantsAjax",
                action: "addOccupant",
                formData: formData,
                tenant_id:tenant_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    
                    $("select[name='salutation_occupancy']").val('Select');
                    $("select[name='gender_occupancy']").val('0');
                    $("input[name='firstname_occupancy']").val('');
                    $("input[name='middlename_occupancy']").val('');
                    $("input[name='lastname_occupancy']").val('');
                    $("input[name='medianName_occupancy']").val('');
                    $("input[name='nickname_occupancy']").val('');
                    $("input[name='sinn_occupancy']").val('');
                    $("input[name='contact_emer_occupancy']").val('');
                    $("select[name='ethncity_occupancy']").val('1');
                    $("select[name='maritalStatus_occupancy']").val('1');
                    $("select[name='edit_general_hobby_occupancy']").val('1');
                    $("select[name='edit_general_veteran_occupancy']").val('1');
                    $("select[name='referal_occupancy']").val('1');
                    $("select[name='rela_occupancy']").val('0');
                    $("select[name='rela_emer_occupancy']").val('0');
                    $("select[name='countryCode_emer_occupancy']").val('0');
                    $("input[name='ph_emer_occupancy']").val('');
                    $("input[name='email_emer_occupancy']").val('');

                    $('.occcupants_table').trigger('reloadGrid');
                    onTop(true);
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('*This field is required');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text('*This field is required');
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    function decativeOccupant(elem) {
        var id=elem;
        $.ajax({
            type: 'post',
            url: '/tenantOccupantsTypeAjax',
            data: {
                class: "TenantOccupantsAjax",
                action: "decativeOccupant",
                id:id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('.occcupants_table').trigger('reloadGrid');
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getInitialData',
        data: {
            class: "TenantAjax",
            action: "getIntialData"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {


                if (data.data.country.length > 0){
                    var countryOption = "<option value='0'>Select</option>";
                    $.each(data.data.country, function (key, value) {
                        countryOption += "<option value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")"+"</option>";
                    });
                    $('#countryCode_emer_occupancy .countryCode_emer_occupancy select').html(countryOption);
                }

                if (data.data.propertylist.length > 0){
                    var propertyOption = "<option value=''>Select</option>";
                    $.each(data.data.propertylist, function (key, value) {
                        propertyOption += "<option value='"+value.id+"' data-id='"+value.property_id+"'>"+value.property_name+"</option>";
                    });
                    $('#addTenant #property').html(propertyOption);
                }

                if (data.data.phone_type.length > 0){
                    var phoneOption = "";
                    $.each(data.data.phone_type, function (key, value) {
                        phoneOption += "<option value='"+value.id+"'>"+value.type+"</option>";
                    });
                    $('.primary-tenant-phone-row select[name="phoneType[]"]').html(phoneOption);
                    $('.addition_tenant_block select[name="additional_phoneType"]').html(phoneOption);
                }

                if (data.data.carrier.length > 0){
                    var carrierOption = "";
                    $.each(data.data.carrier, function (key, value) {
                        carrierOption += "<option value='"+value.id+"'>"+value.carrier+"</option>";
                    });
                    $('.primary-tenant-phone-row select[name="carrier[]"]').html(carrierOption);
                    $('.addition_tenant_block select[name="additional_carrier"]').html(carrierOption);
                    $('.property_guarantor_form1 select[name="guarantor_carrier_1[]"]').html(carrierOption);
                    $('.additional_carrier').html(carrierOption);
                }

                if (data.data.referral.length > 0){
                    var referralOption = "";
                    $.each(data.data.referral, function (key, value) {
                        referralOption += "<option value='"+value.id+"'>"+value.referral+"</option>";
                    });
                    $('select[name="referal_occupancy"]').html(referralOption);
                    $('.addition_tenant_block select[name="additional_referralSource"]').html(referralOption);
                }

                if (data.data.ethnicity.length > 0){
                    var ethnicityOption = "";
                    $.each(data.data.ethnicity, function (key, value) {
                        ethnicityOption += "<option value='"+value.id+"'>"+value.title+"</option>";
                    });
                    $('select[name="ethncity_occupancy"]').html(ethnicityOption);
                    $('.addition_tenant_block select[name="additional_ethncity"]').html(ethnicityOption);
                }

                if (data.data.marital.length > 0){
                    var maritalOption = "";
                    $.each(data.data.marital, function (key, value) {
                        maritalOption += "<option value='"+value.id+"'>"+value.marital+"</option>";
                    });
                    $('select[name="maritalStatus_occupancy"]').html(maritalOption);
                    $('.addition_tenant_block select[name="maritalStatus"]').html(maritalOption);
                    $('.additional_maritalStatus').html(maritalOption);
                }

                if (data.data.hobbies.length > 0){
                    var hobbyOption = "";
                    $.each(data.data.hobbies, function (key, value) {
                        hobbyOption += "<option value='"+value.id+"'>"+value.hobby+"</option>";
                    });
                    $('select[name="edit_general_hobby_occupancy"],select[name="additional_hobbies[]"]').html(hobbyOption);
                    $('select[name="hobbies"],select[name="additional_hobbies[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                }

                if (data.data.veteran.length > 0){
                    var veteranOption = "";
                    $.each(data.data.veteran, function (key, value) {
                        veteranOption += "<option value='"+value.id+"'>"+value.veteran+"</option>";
                    });
                    $('select[name="edit_general_veteran_occupancy"]').html(veteranOption);
                    $('.additional_veteranStatus').html(veteranOption);
                }

                if (data.data.collection_reason.length > 0){
                    var reasonOption = "";
                    $.each(data.data.collection_reason, function (key, value) {
                        reasonOption += "<option value='"+value.id+"'>"+value.reason+"</option>";
                    });
                    $('.property_collection select[name="collection_reason"]').html(reasonOption);
                }

                if (data.data.credential_type.length > 0){
                    var typeOption = "";
                    $.each(data.data.credential_type, function (key, value) {
                        typeOption += "<option value='"+value.id+"'>"+value.credential_type+"</option>";
                    });
                    $('.tenant-credentials-control select[name="credentialType[]"]').html(typeOption);
                }


            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
});