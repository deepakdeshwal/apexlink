/* pass id*/
$(document).on('click','.sectionContactEdit',function(e){
    $(".contactViewMode").hide();
    $(".contactEditMode").show();
        var id=$(".contact_id").val();

    //var first_name =$(".contact_first_name").val();
    //$('.contact_first_name').val(bla);
    //alert(id);

    $.ajax({
        type: 'POST',
        url: '/Ajax-People/EditViewContact',
        data: {
            class: 'ContactEdit',
            action: 'getContactEdit',
            id: id
        },
        success: function (response) {
           console.log(response);
            var info = JSON.parse(response);
            var salutation = info.data.users.salutation;
            var first_name = info.data.users.first_name;
            var middle_name = info.data.users.middle_name;
            var maiden_name = info.data.users.maiden_name;
            var nick_name = info.data.users.nick_name;
            var gender_contacts = info.data.users.gender;
            var birth = info.data.users.dob;
            var contactt_referal = info.data.users.referral_source;
            var contactt_address1 = info.data.users.address1;
            var contactt_address2 = info.data.users.address2
            var contactt_address3 = info.data.users.address3;
            var contactt_address4 = info.data.users.address4;
            var contact_zipcode = info.data.users.zipcode;
            var contactt_country = info.data.users.country;
            var contactt_state = info.data.users.state;
            var contactt_city = info.data.users.city;
            var note = info.data.users.phone_number_note;
            var ethnicity = info.data.users.ethnicity;
            var maritial = info.data.users.maritial;
            var contactt_ssn_sin_id= info.data.ssn;

            var contactt_phoneType= info.data.phoneInfo;
            var contactt_notes= info.data.contactt_notes;
            var contactt_hobbies = info.data.users.hobbies;
            var contactt_veteran = info.data.users.veteran_status;
            var tenant_credential = info.data.tenant_credential;
            var generalEmails = info.data.email;
            var emergencyContactINFO = info.data.emergencyContact;
            console.log(info.data.emergencyContact);
            //alert(contactt_ssn_sin_id);


            var last_name = info.data.users.last_name;
            $("#salutation").val(salutation);
            $("#first_name").val(first_name);
            $("#last_name").val(last_name);
            $("#middle_name").val(middle_name);
            $("#contact_maiden_name").val(maiden_name);
            $("#nick_name").val(nick_name);
            $("#gender_contacts").val(gender_contacts);
            $("#birth").val(birth);
            $("#contactt_referal").val(contactt_referal);
            $("#contactt_address1").val(contactt_address1);
            $("#contactt_address2").val(contactt_address2);
            $("#contactt_address3").val(contactt_address3);
            $("#contactt_address4").val(contactt_address4);
            $("#contact_zipcode").val(contact_zipcode);
            $("#contactt_country").val(contactt_country);
            $("#contactt_state").val(contactt_state);
            $("#contactt_city").val(contactt_city);
            $("#note").val(note);
            $("#ethnicity").val(ethnicity);
            $("#maritial").val(maritial);
            $(".editcontactssnclone").html(contactt_ssn_sin_id);
            $("#contactt_hobbies").val(contactt_hobbies);
            $("#contactt_veteran").val(contactt_veteran);
            $(".contactNotesClone").html(contactt_notes);
            $(".credentialsDetailClone").html(tenant_credential);
            $(".genaralEmail").html(generalEmails);
            $(".emergencyContactINFO").html(emergencyContactINFO);
            $(".phoneInfo").html(contactt_phoneType);
           // alert(contactt_notes);
           // alert(contact_country);
           // alert(middle_name);
            // alert(first_name);
           // console.log(info.data.users.salutation);
           // console.log(info.data.users.first_name);

           
        }
    });

    //update
    $("#editContact").validate({
        rules: {
            salutation: {
                required:true
            },
            firstname: {
                required:true
            }
        },
        submitHandler: function (e) {

            var id = $('.contact_id') .val();
            //alert(id);
            var contact_notes = $("#contactt_notes").val();
            var form = $('#editContact')[0];
            var formData = new FormData(form);
            formData.append('action','UpdateContactDetails');
            formData.append('class','ContactEdit');
            formData.append('id',id);
            // console.log("hello", contact_notes);
            // return;
            // if('.$contactnotesval.')
           // formData.append('contact_notes',contact_notes);
            formData.append('contact_notes',contact_notes);
           // formData.append('contact_notes',contact_notes);


           // action = 'update';


            $.ajax({
                url:'/Ajax-People/EditViewContact',
                type: 'POST',
                data: formData,
               // id:id,
                success: function (data) {

                    var info = JSON.parse(data);
                    if(info.status=='success')
                    {
                        localStorage.setItem("Message","Record updated successfully");
                        localStorage.setItem("rowcolorTenant",'green');
                        window.location.href = window.location.origin+"/People/ContactListt";
                    }


                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
});


$(document).on("click",".email-plus-sign",function(){
    var emailRowLenght = $(".multipleEmail");
    if (emailRowLenght.length == 2) {

        $(".email-plus-sign .fa-plus-circle").hide();
    }
    var clone = $(".multipleEmail:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    $(".multipleEmail").first().after(clone);
    $(".multipleEmail:not(:eq(0))  .email-plus-sign .fa-plus-circle").remove();
    $(".multipleEmail:not(:eq(0))  .email-remove-sign .fa-minus-circle").show();

});

/*for multiple email textbox*/


/*Remove Email Textbox*/
  $(document).on("click",".email-remove-sign",function(){
        var emailRowLenght = $(".multipleEmail");
        
        if (emailRowLenght.length == 3 || emailRowLenght.length == 2) {
           $(".email-plus-sign .fa-plus-circle").show();
        }
        $(this).parents(".multipleEmail").remove();
    });





  $(document).on("click",".add-emergency-contant",function(){
  
  var clone = $(".tenant-emergency-contact:first").clone();
  clone.find('input[type=text]').val('');
  clone.find('input[name="emergency_phone[]"]').mask('000-000-0000', {reverse: true});;
  $(".tenant-emergency-contact").first().after(clone);
  clone.find(".add-emergency-contant").hide();
    clone.find(".remove-emergency-contant").show();

}); 
    


    $(document).on("click",".remove-emergency-contant",function(){
    
  
 $(this).parents(".tenant-emergency-contact").remove();

});



$(document).on("click",".add-notice-period",function(){
    var clone = $(".tenant-credentials-control:first").clone();
    clone.find('input[type=text]').val('');
    clone.find('select[name="credentialType[]"]').val('1');
    $(".tenant-credentials-control").first().after(clone);
    clone.find(".add-notice-period").hide();
    clone.find(".remove-notice-period").show();
});


$(document).on("click",".remove-notice-period",function(){
    $(this).parents(".tenant-credentials-control").remove();
});

/*To reset form */

$(document).on("click",".cancel_contact",function(){
    bootbox.confirm({
        message: "Do you want to Reset ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                window.location.href = window.location.origin+'/People/ContactListt';
            }
        }
    });

})


/*Cancel*/

/*To reset form */

$(document).on("click",".reset_contact",function(){
    bootbox.confirm({
        message: "Do you want to Cancel ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                window.location.href = window.location.origin+'/People/ContactListt';
            }
        }
    });

});






