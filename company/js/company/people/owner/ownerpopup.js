$(document).ready(function () {

    $(document).on("click",".NewportfolioPopup",function(){
        $("#NewportfolioPopup").show();
        $(".add-popup-body #portfolio_name").val('');
        $(".add-popup-body .required").text('');
        var rString = randomString1(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
        $("#NewportfolioPopup #portfolio_id").val(rString);
    });
    $(document).on("click",".add-popup .grey-btn",function(){
        var box = $(this).closest('.add-popup');
        $(box).hide();
    });

    $(document).on("click",".add-popup .clear_single_field",function(){
        var modal_id = $(this).parents('.add-popup').attr('id');
        resetFormClear('#'+modal_id,[], 'div',false);
        $(this).parents('.add-popup span.required').html('');
    });

    $(document).on("click",".clearCustomFieldModal",function(){
        resetFormClear('#custom_field',['data_type'], 'form',false);
    });

    $(document).on("click",".clearChartOfAccountModal",function(){
        resetFormClear('#add_chart_account_form_id',[ ], 'form',false);
        $('#sub_account').html('<option value="">Select</option>');
        $('#defaultChart').prop('checked', false);
    });

    $(document).on("click","#clearPortfolioModal",function(){
        resetFormClear('#NewportfolioPopup',['@portfolio_id'], 'div',false,'');
    });

    /*$(document).on("click","#NewmanagerPopupSave",function(){
        resetFormClear('#NewmanagerPopup',[], 'div',false);
    });*/

    $(document).on("click","#NewportfolioPopupSave",function(e){
        e.preventDefault();
        var formData = $('#NewportfolioPopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-portfolio-popup',
            data: {
                form: formData,
                class: 'propertyDetail',
                action: 'addPortfolioPopup'
            },
            beforeSend: function(xhr) {
                // checking portfolio validations
                $(".customValidatePortfoio").each(function() {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewportfolioPopup').hide(500);
                    toastr.success(response.message);
                    fetchAllPortfolio(response.lastid);
                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Portfolio already exists!')
                        bootbox.alert(response.message);
                    return false;
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }
                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });

    $(document).on("click","#Newmanager",function(){
        $("#NewmanagerPopup").show();
        $("#NewmanagerPopup .add-popup-body input").val('');
        $(".required").text('');
    });
    $(document).on("click","#NewmanagerPopupSave",function(e){
        e.preventDefault();
        var formData = $('#NewmanagerPopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-manager-popup',
            data: {
                form: formData,
                class: 'propertyDetail',
                action: 'addManagerPopup'
            },
            beforeSend: function(xhr) {
                // checking portfolio validations
                $(".customValidateManager").each(function() {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {

                    $('#dynamic_manager').html('');
                    var optionHtml = '<select name="manager_id[]" class="form-control select_manager" multiple="multiple" id="select_mangers_options">';
                    $.each(response.getdata, function(key, value) {
                        if (response.selected == value.id) {
                            var selected = 'selected="selected"';
                        } else {
                            var selected = '';
                        }
                        if (value.last_name) {
                            optionHtml += '<option ' + selected + ' value="' + value.id + '">' + value.first_name + ' ' + value.last_name + '</option>';
                        } else {
                            optionHtml += '<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>';
                        }

                    });
                    $('#selectProperty #dynamic_manager').html(optionHtml);
                    $('.select_manager').multiselect({
                        includeSelectAllOption: true,
                        nonSelectedText: 'Manager Name'
                    });
                    $('#NewmanagerPopup').hide(500);
                    toastr.success(response.message);

                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Email already exists!')
                        bootbox.alert(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    $(document).on("click",".newAttachGroupPlus",function(){
        $("#newAttachGroupPopup").show();
        $("#newAttachGroupPopup .add-popup-body input").val('');
        $(".required").text('');
    });
    $(document).on("click","#NewattachPopupSave",function(e){
        e.preventDefault();
        var formData = $('#newAttachGroupPopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-attachgroup-popup',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addAttachgroupPopup'},
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#dynamic_groups').html('');
                    var optionHtml = '<select name="attach_groups[]" class="form-control attach_groups" multiple>';
                    $.each(response.getdata, function(key, value) {
                        if (response.selected == value.id) {
                            var selected = 'selected="selected"';
                        } else {
                            var selected = '';
                        }
                        optionHtml += '<option ' + selected + ' value="' + value.id + '">' + value.group_name + '</option>';

                    });
                    $('#dynamic_groups').html(optionHtml);
                    $('.attach_groups').multiselect({
                        includeSelectAllOption: true,
                        nonSelectedText: 'Property Groups'
                    });
                    $('#newAttachGroupPopup').hide(500);
                    toastr.success(response.message);
                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Group Name already exists!')
                        toastr.warning(response.message);
                    return false;
                } else if (response.status == 'error' && response.code == 400) {
                    $('.required').html('');
                    $.each(response.data, function(key, value) {
                        $('.' + key).text('* This field is required.');
                    });
                }

                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    $(document).on("click",".propertyTypePlus",function(){
        $("#NewtypePopup .add-popup-body input").val('');
        $(".required").text('');
        $("#NewtypePopup").show();
    });
    $(document).on("click","#NewtypePopupSave",function(e){
        e.preventDefault();
        var formData = $('#NewtypePopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-propertytype-popup',
            data: {
                form: formData,
                class: 'propertyDetail',
                action: 'addPropertytpePopup'
            },
            beforeSend: function(xhr) {
                // checking portfolio validations
                $(".customValidatePropertyType").each(function() {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewtypePopup').hide(500);
                    toastr.success(response.message);
                    fetchAllPropertyType(response.lastid);
                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Property Type already exists!')
                        bootbox.alert(response.message);
                    return false;
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });

    $(document).on("click",".propertyStylePlus",function(){
        $("#NewpstylePopup .add-popup-body input").val('');
        $(".required").text('');
        $("#NewpstylePopup").show();
    });
    $(document).on("click","#NewpstylePopupSave",function(e){
        e.preventDefault();
        var formData = $('#NewpstylePopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-propertystyle-popup',
            data: {
                form: formData,
                class: 'propertyDetail',
                action: 'addPropertystylePopup'
            },
            beforeSend: function(xhr) {
                // checking portfolio validations
                $(".customValidatePropertyStyle").each(function() {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewpstylePopup').hide(500);
                    toastr.success(response.message);
                    fetchAllPropertystyle(response.lastid);

                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Property Style already exists!')
                        bootbox.alert(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });

    $(document).on("click",".propertySubTypePlus",function(){
        $("#newSubTypePopup .add-popup-body input").val('');
        $(".required").text('');
        $("#newSubTypePopup").show();
    });
    $(document).on("click","#NewsubtypePopupSave",function(e){
        e.preventDefault();
        var formData = $('#newSubTypePopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-propertysubtype-popup',
            data: {
                form: formData,
                class: 'propertyDetail',
                action: 'addPropertysubtypePopup'
            },
            beforeSend: function(xhr) {
                // checking portfolio validations
                $(".customValidatePropertySubType").each(function() {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#newSubTypePopup').hide(500);
                    toastr.success(response.message);
                    fetchAllPropertysubtype(response.lastid);

                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Property Sub-Type already exists!')
                        bootbox.alert(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    $(document).on("click",".newAmenitiesPlus",function(){
        $("#newAmenitiesPopup .add-popup-body input").val('');
        $(".required").text('');
        $("#newAmenitiesPopup").show();
    });
    $(document).on("click","#NewamenitiesPopupSave",function(e){
        e.preventDefault();
        var formData = $('#newAmenitiesPopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-amenity-popup',
            data: {
                form: formData,
                class: 'commonPopup',
                action: 'addAmenityPopup'
            },
            beforeSend: function(xhr) {
                // checking portfolio validations
                $(".customValidateAmenities").each(function() {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#newAmenitiesPopup').hide(500);
                    toastr.success(response.message);
                    var lastid = response.lastid;
                    fetchAllAmenities(response.lastid)
                    //$("#"+lastid).attr('checked',true);
                    setTimeout(function() {
                        $("#" + lastid).attr("checked", true)
                    }, 1000);
                } else if (response.status == 'error' && response.code == 500) {
                    toastr.error(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    $(document).on("click",".newUnitTypeModalPlus2",function(){
        $("#newAmenitiesPopup .add-popup-body input").val('');
        $(".required").text('');
        $("#newUnitTypePopup").show();
    });
    $(document).on("click","#NewunitPopupSave",function(e){
        e.preventDefault();
        var formData = $('#newUnitTypePopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {
                form: formData,
                class: 'propertyDetail',
                action: 'addUnittype'
            },
            beforeSend: function(xhr) {
                // checking portfolio validations
                $(".customValidateUnitType").each(function() {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#newUnitTypePopup').hide(500);
                    toastr.success(response.message);
                    fetchAllUnittypes(response.lastid);
                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Portfolio already exists!')
                        bootbox.alert(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    $(document).on("click",".propreerralicon",function(){
        //$(this).parent().next().next().show();
        $("#selectPropertyReferralResource1").show();
    });

    $(document).on("click",".pop-add-icon",function () {
        $(".add-popup-body input[type='text']").val('');
        var text_id = $(this).closest('div').find('.add-popup').attr('id');
        console.log('text_id>>', text_id);
        $("#"+text_id+" .required").text('');
    });

    $(document).on("click",".add_single1", function () {
        var tableName = $(this).attr("data-table");
        var colName = $(this).attr("data-cell");
        var className = $(this).attr("data-class");
        var selectName = $(this).attr("data-name");
        var val = $(this).parent().prev().find("."+className).val();

        if (val == ""){
            $(this).parent().prev().find("."+className).next().text("* This field is required");
            return false;
        }else{
            $(this).parent().prev().find("."+className).next().text("");
        }

        savePopUpData(tableName, colName, val, selectName);
        var val = $(this).parent().prev().find("input[type='text']").val('');
    });

    $(document).on("click",".add_single3", function () {
        var tableName = $(this).attr("data-table");
        var colName = $(this).attr("data-cell");
        var className = $(this).attr("data-class");
        var selectName = $(this).attr("data-name");
        var val = $(this).parent().prev().find("."+className).val();
        if (val == ""){
            $(this).parent().prev().find("."+className).next().text("* This field is required   ");
            return false;
        }else{
            $(this).parent().prev().find("."+className).next().text("");
        }
        savePopUpData3(tableName, colName, val, selectName);
        var val = $(this).parent().prev().find("input[type='text']").val('');
    });

    $(document).on("click",".selectPropertyEthnicity",function(){
        $("#selectPropertyEthnicity1").show();
    });

    $(document).on("click",".selectPropertyHobbies",function(){
        $("#selectPropertyHobbies1").show();
    });

    $(document).on("click",".selectPropertyMaritalStatus",function(){
        $("#selectPropertyMaritalStatus1").show();
    });

    $(document).on("click",".selectPropertyVeteranStatus",function(){
        $("#selectPropertyVeteranStatus1").show();
    });

    $(document).on("click",".ownerCredentialType",function(){
        $("#ownerCredentialType1").show();
    });

    $(document).on("click",".additionalMaritalStatus",function(){
        $("#additionalMaritalStatus1").show();
    });

    $(document).on("click",".additionalHobbies",function(){
        $("#additionalHobbies1").show();
    });

    $(document).on("click",".additionalReferralResource",function(){
        $('.required').text('');
        $("#additionalReferralResource1").show();
    });

    $(document).on("click","#complaintPopupIcon",function(){
        $("#ComplaintTypePopup").show();
    });


});

function  savePopUpData(tableName, colName, val, selectName) {
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/savePopUpData',
        data: {
            class: "TenantAjax",
            action: "savePopUpData",
            tableName:tableName,
            colName: colName,
            val:val
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                $("select[name='"+selectName+"']").append(option);
                console.log('selectName', selectName);
                if (selectName == "hobbies[]" || selectName == "additional_hobbies[]" || selectName == "hobbies"){
                    $("select[name='"+selectName+"']").multiselect('destroy');
                    $("select[name='"+selectName+"']").multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                    $("#selectPropertyHobbies1").hide();
                }else{
                    $("select[name='"+selectName+"']").next().hide();
                }
                toastr.success(data.message);

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function  savePopUpData3(tableName, colName, val, selectName) {
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/savePopUpData3',
        data: {
            class: "TenantAjax",
            action: "savePopUpData3",
            tableName:tableName,
            colName: colName,
            val:val
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                $("select[name='"+selectName+"']").append(option);
                if (selectName == "hobbies[]" || selectName == "additional_hobbies[]"){
                    $("select[name='"+selectName+"']").multiselect('destroy');
                    $("select[name='"+selectName+"']").multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                    $("select[name='"+selectName+"']").parent().parent().next().hide();
                }else{
                    $("select[name='"+selectName+"']").next().hide();
                }
                toastr.success(data.message);

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function fetchAllUnittypes(id) {
    var propertyEditid = $("#property_editunique_id").val();

    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchAllUnittypes'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#unit_type').html(res.data);
            if (id != false) {
                $('#unit_type').val(id);

            }
            $("#default_rent").val(response.default_rent);

        },
    });
}

function fetchAllAmenities(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-all-amenities',
        data: {
            class: 'commonPopup',
            action: 'fetchAllAmenitiesOwner'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#dynamic_amenity').html(res.data);
            // $('#dynamic_amenity').html(res.data);
        },
    });

}


function randomString1(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

function validateCustomField(data_value, data_required, arg, element, id) {
    var msg = '';
    if (data_value == "") {
        if (data_required == 1) {
            msg = 'This field is required';
            $('#' + id).after("<span>" + msg + "</span>");
            return false;
        }
    }
    if (arg == 'text') {
        if (data_value.length > 150) {
            msg = 'Please enter character less than 150';
            $('#' + id).next('.customError').text(msg);
            return false;
        }

    } else if (arg == 'number') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            $('#' + id).next('.customError').text(msg);
            return false;
        } else {
            if (data_value > 20) {
                msg = 'Please enter less than 20';
                $('#' + id).next('.customError').text(msg);
                return false;
            }
        }
    } else if (arg == 'currency') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            $('#' + id).next('.customError').text(msg);
            return false;
        }

    } else if (arg == 'percentage') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            $('#' + id).next('.customError').text(msg);
            return false;
        } else {
            if (data_value > 100) {
                msg = 'Maximum value is 100';
                $('#' + id).next('.customError').text(msg);
                return false;
            }
        }
    } else if (arg == 'url') {
        var expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm;
        var regex = new RegExp(expression);
        if (data_value.match(regex)) {
        } else {
            msg = 'Please enter a valid URL';
            $('#' + id).next('.customError').text(msg);
            return false;
        }
    }
    $('#' + id).next('.customError').text('');
    return true;
}
function fetchAllPortfolio(id) {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-portfolioname',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchPortfolioname'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#addPropertyForm select[name="portfolio_id"]').html(res.data);
            if (id != false) {
                $('#addPropertyForm select[name="portfolio_id"]').val(id);
            }
        },
    });

}
function fetchAllPropertyType(id) {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-propertytype',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchPropertytype'},
        success: function (response) {
            var res = JSON.parse(response);
            console.log('res>>', res);
            $('#property_type_options ').html(res.data);
            if (id != false) {
                $('#property_type_options').val(id);
            }

        },
    });

}
function fetchAllPropertystyle(id) {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-propertytype',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchPropertystyle'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#property_style_options').html(res.data);
            if (id != false) {
                $('#property_style_options').val(id);
            }
        },
    });

}
function fetchAllPropertysubtype(id) {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-propertysubtype',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchPropertysubtype'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#property_subtype_options').html(res.data);
            if (id != false) {
                $('#property_subtype_options').val(id);
            }

        },
    });

}

function fetchAllgarage(id) {

    $.ajax({
        type: 'post',
        url: '/fetch-garagedata',
        data: {
            class: 'propertyDetail',
            action: 'fetchGaragedata'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#garage_options').html(res.data);
            if (id != false) {
                $('#garage_options').val(id);
            }

        },
    });

}
function fetchAllManagers(id) {

    $.ajax({
        type: 'post',
        url: '/fetch-managers',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllmanagers'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#select_mangers_options').html(res.data);
            $('#select_mangers_options').multiselect({
                columns: 1,
                placeholder: 'Select',
                selectAll: true
            });

            $('#select_mangers_options').change(function () {
//               alert('ddf');
            });


        },
    });

}
function fetchAllinsurancetypes(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-insurancetype',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllinsurancetypes'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#SelectInsuranceTypes').html(res.data);
            $('#SelectInsuranceTypes').val(id);
        },
    });
}

function fetchAllpolicytypes(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-policytype',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllpolicytypes'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#SelectPolicyTypes').html(res.data);
            $('#SelectPolicyTypes').val(id);
        },
    });
}
function fetchAllAttachgroups(id) {

    $.ajax({
        type: 'post',
        url: '/fetch-attachgroup',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllAttachgroups'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#select_group_options').html(res.data);
            $('#select_group_options').multiselect({
                columns: 1,
                placeholder: 'Select',
                selectAll: true});
        },
    });

}


//vendor values on owner select
function vendorValues(){
    var selectedowner = $(".ownerSelectclass option:selected").text();
    var count = $(".ownerSelectclass option:selected").length;
//         $(".vendor_1099_payer").val(selectedowner);
    var data = '';
    $('#vendor_id').val('');
    $(".ownerSelectclass").each(function () {
        if ($('option:selected', this).text() != 'Select') {
            if (data == '') {
                data += $('option:selected', this).text();
            } else {
                data += ", " + $('option:selected', this).text();
            }
        }
    });
    $('#vendor_id').val(data);



}
//fetch all owners
function getAllOwners() {
    var property_id = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-owners',
        data: {
            class: 'propertyDetail',
            action: 'fetchOwners',
            property_id: property_id,
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('.ownerSelectclass').html(res.data);
        },
    });
}
function fetchAllFixture(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-all-fixture',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllfixture'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#fixture_type').html(res.data);
            if (id != false) {
                $('#fixture_type').val(id);
            }

        },
    });
}

function fetchAllReason(id) {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchAllReason'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#selectreasondiv').html(res.data);
            if (id != false) {
                $('#selectreasondiv').val(id);
            }

        },
    });
}
function getcurrentdatetime() {
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'getcurrentdatetime'},
        success: function (response) {
            var response = JSON.parse(response);
            $('.last_renovation').val(response.timeZone);
            $(".last_renovation_time").val(response.renovationtime);
            $("#pick_up_date").val(response.timeZone);
            $("#pick_up_time").val(response.timeZone);
            $("#key_designator").val(default_name);
//            $("#management_start_date").val(response.timeZone);
            $("#loan_start_date").val(response.timeZone);
        }


    });
}
function getParameterByName(name) {
    var regexS = "[\\?&]" + name + "=([^&#]*)",
        regex = new RegExp(regexS),
        results = regex.exec(window.location.search);
    if (results == null) {
        return "";
    } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}

$(document).on("click",".add_owner_account_name",function(){
    $('.required').text('');
    $("#account_name").show();
    var className = $(this).parent().next('.account_name').attr('class');
    if(className)
    {
        className = className.split(' ');
        $('#ownerAccountNameBtn').attr("select_class",className[2]);
    }
});

function  savePopUpAccountNameData(tableName, colName, val, selectName,selectClass) {

    $.ajax({
        type: 'post',
        url: '/OwnerAjax',
        data: {
            class: "ownerDetails",
            action: "addAccountName",
            tableName:tableName,
            colName: colName,
            val:val
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                // $("select[name='"+selectName+"']").append(option);
                $("."+selectClass).append(option);
                if (selectName == "hobbies[]" || selectName == "additional_hobbies[]"){
                    $("select[name='"+selectName+"']").multiselect('destroy');
                    $("select[name='"+selectName+"']").multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                    $("select[name='"+selectName+"']").parent().parent().next().hide();
                }else{
                    $("select[name='"+selectName+"']").next().hide();
                }
                toastr.success(data.message);

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}


$(document).on("click","#ownerAccountNameBtn",function(e){
    var tableName = $(this).attr("data-table");
    var colName = $(this).attr("data-cell");
    var className = $(this).attr("data-class");
    var selectName = $(this).attr("data-name");
    var selectClass = $(this).attr("select_class");
    var val = $(this).parent().prev().find("."+className).val();
    if (val == ""){
        $(this).parent().prev().find("."+className).next().text("* This field is required");
        return false;
    }else{
        $(this).parent().prev().find("."+className).next().text("");
    }
    savePopUpAccountNameData(tableName, colName, val, selectName, selectClass);
    var val = $(this).parent().prev().find("input[type='text']").val('');
});
$("#add_chart_account_form_id").validate({
    rules: {
        account_type_id: {
            required: true
        },
        account_code: {
            required: true,
            number: true,
        },
        account_name: {
            required: true
        },
        reporting_code: {
            required: true,
        }
    },
    messages: {
        account_code: {
            number: "* Please add numerical values only",
        }
    }
});
$(document).on("click","#chartAccountId",function(e){
    e.preventDefault();
    $("#add_chart_account_form_id")[0].reset();
    var validator = $("#add_chart_account_form_id").validate();
    validator.resetForm();

});

$(document).on("submit", "#add_chart_account_form_id", function (e) {
    e.preventDefault();
    if ($('#add_chart_account_form_id').valid()) {
        var account_type_id = $('#account_type_id').val();
        var account_code = $('#account_code').val();
        var account_name = $("#com_account_name").val();
        var reporting_code = $("#reporting_code").val();
        var sub_account = $("#sub_account").val();
        var status = $("#status").val();
        var range_from = $('#account_type_id').find(':selected').attr('data-rangefrom');
        var range_to = $('#account_type_id').find(':selected').attr('data-rangeto');
        var posting_status = $("#posting_status").val();
        var chart_account_edit_id = $("#chart_account_edit_id").val();
        var defaultChart = $("#defaultChart").val();

        var formData = {
            'account_type_id': account_type_id,
            'account_code': account_code,
            'account_name': account_name,
            'reporting_code': reporting_code,
            'sub_account': sub_account,
            'posting_status': posting_status,
            'status': status,
            'range_from': range_from,
            'range_to': range_to,
            'chart_account_edit_id': chart_account_edit_id,
            'is_default':defaultChart
        };
        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {
                class: 'propertyDetail',
                action: 'insertChartofaccount',
                form: formData
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $("#chartofaccountmodal").modal('hide');
                    var option = "<option value='"+response.lastid+"' selected>"+response.data.account_name+'-'+response.data.account_code+"</option>";

                    $("select[name='account_for_transaction[]']").append(option);
                    toastr.success(response.message);
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text('* This field is required.');
                    });
                } else if (response.status == 'error' && response.code == 503) {
                    toastr.warning(response.message);
                }
            }
        });
    }
});

$(document).on("input", "input", function (e) {
    var textb = $(this).val();
    if(textb.length){
        $(this).siblings('.error').html('');
    }
});
//add new complaint type
$('#NewpetComplaintSave').on('click', function (e) {
    e.preventDefault();
    var formData = $('#ComplaintTypePopup :input').serializeArray();
    var complaint = $('#complaint_type_name').val();
    $.ajax({
        type: 'post',
        url: '/add-complaint-type-popup',
        data: {form: formData,
            class: 'commonPopup',
            action: 'addComplaintType'},
        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidateComplaint").each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#ComplaintTypePopup').hide(500);
                toastr.success(response.message);
                fetchAllComplaintType(response.lastid);
                // if(response.lastid){
                //     var option = "<option value='"+response.lastid+"' selected>"+complaint+"</option>";
                //     $("#complaint_type_options").append(option);
                // }

            } else if (response.status == 'error' && response.code == 500) {
                toastr.error(response.message);
                return false;
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('#' + key).text(value);
                });
            } else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});

function fetchAllComplaintType(id) {
    $.ajax({
        url: '/EditOwnerAjax',
        data: {
            class: "EditOwnerAjax",
            action: "fetchAllComplaintType",
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('#complaint_type_options').html(res.data);
            if (id != false) {
                $('#complaint_type_options').val(id);
            }


        },
    });

}

$(document).on('change', '#complaint_type_options', function () {
    if($(this).val() == '18'){
        $('#other_notes_div').show();
    } else {
        $('#other_notes_div').hide();
    }
});

/**
 *
  Create new property popup save data
 *
 **/
$(document).on("click","#ancCreateNewProperty",function () {
    var validator = $( "#addPropertyForm" ).validate();
    validator.resetForm();
    $("#addPropertyForm")[0].reset();
    $('.details').hide();
    $('#selectProperty #propertyZipcode').val(default_zipcode);
    getZipCode('#selectProperty #propertyZipcode',default_zipcode,'#selectProperty #propertyCity_name','#selectProperty #propertyState_name','#selectProperty #propertyCountry_name','');
    setTimeout(function () {
        getPropertyPopUpRecord();
    },500);

});
function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}
function getPropertyPopUpRecord () {
    $.ajax({
        type: 'post',
        url: '/OwnerAjax',
        data: {
            class: 'OwnerAjax',
            action: 'getPropertyPopUpData'
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var portfolioOption = "<option value=''>Select</option>";
                if (data.data.portfolio.length > 0) {
                    $.each(data.data.portfolio, function (key, value) {
                        if(value.is_default == '1'){
                            portfolioOption += "<option value='" + value.id + "' selected>" + value.portfolio_name + "</option>";
                        } else {
                            portfolioOption += "<option value='" + value.id + "'>" + value.portfolio_name + "</option>";
                        }

                    });
                    $('#selectProperty .portfolio_id').html(portfolioOption);
                } else {
                    $('#selectProperty .portfolio_id').html(portfolioOption);
                }

                var managerOption = "";
                if (data.data.manager.length > 0) {
                    $.each(data.data.manager, function (key, value) {
                        var name = value.first_name + ' ' + value.last_name;
                        managerOption += "<option value='" + value.id + "'>" + name + "</option>";
                    });
                    $('#selectProperty .select_manager').html(managerOption);
                    $('#selectProperty .select_manager').multiselect({
                        includeSelectAllOption: true,
                        nonSelectedText: 'Select'
                    });
                    $('#selectProperty .select_manager').multiselect("refresh");
                } else {
                    $('#selectProperty .select_manager').html(managerOption).removeAttr('multiple');
                }

                var groupOption = "";
                if (data.data.property_group.length > 0) {
                    $.each(data.data.property_group, function (key, value) {
                        groupOption += "<option value='" + value.id + "'>" + value.group_name + "</option>";
                    });
                    $('#selectProperty .attach_groups').html(groupOption);
                    $('#selectProperty .attach_groups').multiselect({
                        includeSelectAllOption: true,
                        nonSelectedText: 'Group'
                    });
                    $("#selectProperty .attach_groups").multiselect("refresh");
                } else {
                    $('#selectProperty .attach_groups').html(groupOption).removeAttr('multiple');
                }

                var typeOption = "<option value=''>Select</option>";
                if (data.data.property_type.length > 0) {
                    $.each(data.data.property_type, function (key, value) {
                        if(value.is_default == '1'){
                            typeOption += "<option value='" + value.id + "' selected>" + value.property_type + "</option>";
                        } else {
                            typeOption += "<option value='" + value.id + "'>" + value.property_type + "</option>";
                        }
                    });
                    $('#selectProperty .property_type').html(typeOption);
                } else {
                    $('#selectProperty .property_type').html(typeOption);
                }

                var styleOption = "<option value=''>Select</option>";
                if (data.data.property_style.length > 0) {
                    $.each(data.data.property_style, function (key, value) {
                        if(value.is_default == '1') {
                            styleOption += "<option value='" + value.id + "' selected>" + value.property_style + "</option>";
                        } else {
                            styleOption += "<option value='" + value.id + "'>" + value.property_style + "</option>";
                        }
                    });
                    $('#selectProperty .property_style').html(styleOption);
                } else {
                    $('#selectProperty .property_style').html(styleOption);
                }

                var subTypeOption = "<option value=''>Select</option>";
                if (data.data.property_sub_type.length > 0) {
                    $.each(data.data.property_sub_type, function (key, value) {
                        if(value.is_default == '1') {
                            subTypeOption += "<option value='" + value.id + "' selected>" + value.property_subtype + "</option>";
                        } else {
                            subTypeOption += "<option value='" + value.id + "'>" + value.property_subtype + "</option>";
                        }
                    });
                    $('#selectProperty .property_subtype').html(subTypeOption);
                } else {
                    $('#selectProperty .property_subtype').html(subTypeOption);
                }

                var unitTypeOption = "<option value=''>Select</option>";
                if (data.data.property_unit_type.length > 0) {
                    $.each(data.data.property_unit_type, function (key, value) {
                        if(value.is_default == '1') {
                            unitTypeOption += "<option value='" + value.id + "' selected>" + value.unit_type + "</option>";
                        } else {
                            unitTypeOption += "<option value='" + value.id + "'>" + value.unit_type + "</option>";
                        }
                    });
                    $('#selectProperty #dynamic_unit_type .unit_type').html(unitTypeOption);
                } else {
                    $('#selectProperty #dynamic_unit_type .unit_type').html(unitTypeOption);
                }

                if (data.data.year_built) {
                    var year_built =  '<option value="0">(Select Year Built)</option>';
                    for($i = data.data.year_built; $i >= data.data.year_built-120; $i--){
                        year_built +=  '<option value="'+$i+'">'+$i+'</option>';
                    }
                    $('#selectProperty .property_year').html(year_built);
                }

                var amenityOption = "";
                if (data.data.amenity.length > 0) {
                    $.each(data.data.amenity, function (key, value) {
                        amenityOption += '<div class="col-sm-4 col-md-4" style="min-height:40px; padding: 7px;"><input class="" type="checkbox" value="' + value.id + '" name="amenities[]"> ' + value.name + '</div>';
                    });
                    $('#selectProperty #dynamic_amenity').html(amenityOption);
                }

                var rString = randomString(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
                $("#selectProperty #autoGenProperty_id").val(rString);


            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
$("#selectProperty #generalPropertyName").focusout(function () {
    $("#selectProperty #generalLegalName").val($("#selectProperty #generalPropertyName").val());
});

/**
 * Auto fill the city, state and country when focus loose on zipcode field.
 */
$("#selectProperty #propertyZipcode").focusout(function () {
    // getAddressInfoByZip($(this).val(),false);
    getZipCode('#selectProperty #propertyZipcode',$(this).val(),'#selectProperty #propertyCity_name','#selectProperty #propertyState_name','#selectProperty #propertyCountry_name','');
});

$(document).on("change",".check-all-amenities",function(e){
    var inputs = $('#dynamic_amenity div input[type=checkbox]');
    if(e.originalEvent === undefined) {
        var allChecked = true;
        inputs.each(function(){
            allChecked = allChecked && this.checked;
        });
        this.checked = allChecked;
    } else {
        inputs.prop('checked', this.checked);
    }
});

$(document).on('change', '#dynamic_amenity div input[type=checkbox]', function(){
    $('.check-all-amenities').trigger('change');
});

$(document).on("change",".NoBuilding",function(){
    var no_of_buildings = $('#no_of_buildings').val();
    var no_of_units = $('#no_of_units').val();
    if(no_of_buildings != '1' || no_of_units != '1'){
        $('.NoBuilding').prop('checked',false);
        $('.details').hide();
        toastr.warning('Number of Building & Unit must be equal to 1.')
    } else {
        if ($(this).is(":checked")) {
            $('.details').show();
        } else {
            $('.details').hide();
        }
    }
});
$("#selectProperty #base_rent").focusout(function () {
    setTimeout(function () {
        $("#selectProperty #security_deposit").val($("#selectProperty #base_rent").val());
    },300);

});
$(document).on("click","#AddGeneralInformationButton",function(e){
    e.preventDefault();
    $("#addPropertyForm").trigger('submit');
    return false;
});

$(document).on("submit","#addPropertyForm",function(e){
    debugger
    e.preventDefault();
    savePropertyPopUp();
    return false;
});
//add rule for number validation
$.validator.addMethod("maxAmount", function(value, element, arg){
    var am = parseInt(value.replace(/,/g, ''));
    if(am > 9999999999){
        return false;
    }
    return true;
}, "* Please enter less than 10 digits.");

$("#addPropertyForm").validate({
    ignore: ":hidden",
    rules: {
        'property_id':{required:true},
        'property_name':{required:true},
        'portfolio_id':{required:true},
        'property_type':{required:true},
        'no_of_buildings':{required: true},
        'no_of_units':{required: true},
        'unit_type':{required: true},
        'property_squareFootage' : { number: true},
        'property_price' : {maxAmount:10},
        'base_rent' : {required: true, maxAmount:10},
        'market_rent' : {required: true,maxAmount:10},
        'security_deposit':{required: true, maxAmount:10},
    },
    messages: {
        "property_squareFootage": "* Please add numerical values only.",
    }
});

function savePropertyPopUp() {
    var formData = $('#addPropertyForm').serializeArray();
    var propertyName = $("#addPropertyForm #generalPropertyName").val();
    $.ajax({
        type: 'post',
        url: '/add-property',
        data: {
            class: 'propertyDetail',
            action: 'addProperty',
            form: formData
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $(".property_owned_name").append("<option value='"+response.pId+"' selected>"+propertyName+"</option>");
                toastr.success('Record added successfully.');
                $("#selectProperty").modal('hide');
            } else if (response.status == 'error' && response.code == 500) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
        }
    });
}



$(document).on('change', '#account_type_id', function () {
    var accountSubType = this.value;
    accountSubTypeList(accountSubType);
});
function accountSubTypeList(accountSubType) {

    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'getAllAccountSubType',
            id: accountSubType,
        },
        success: function (response) {
            var response = JSON.parse(response);

            if (response.status == 'success' && response.code == 200) {
                $('#sub_account').empty().append("<option value=''>Select</option>");
                $.each(response.data, function (key, value) {
                    $('#sub_account').append($("<option value = " + value.id + ">" + value.account_sub_type + "</option>"));
                })
            } else {
                toastr.error(response.message);
            }
        }
    });
}

$(document).on("click","#clearAddPropertyModal",function(){
    bootbox.confirm("Do you want Clear this form?", function(result) {
        if (result == true) {
            var validator = $("#addPropertyForm").validate();
            validator.resetForm();
            $("#addPropertyForm")[0].reset();
            $('.details').hide();
            $('#selectProperty #propertyZipcode').val(default_zipcode);
            getZipCode('#selectProperty #propertyZipcode', default_zipcode, '#selectProperty #propertyCity_name', '#selectProperty #propertyState_name', '#selectProperty #propertyCountry_name', '');
            setTimeout(function () {
                getPropertyPopUpRecord();
            }, 500);
        }
    });
});

/** Create new property popup save data **/