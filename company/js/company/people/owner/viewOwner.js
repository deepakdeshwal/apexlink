$(document).ready(function() {
    /**
     * Get Parameters by id
     * @param status
     */
    function getParameterByName(name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

    var id = getParameterByName('id');
    /**
     * jquery to redirect for edits
     */
    $(document).on('click', '.edit_redirection', function () {
        var redirection_data = $(this).attr('redirection_data');
        localStorage.setItem("AccordionHref",'#'+redirection_data);
        window.location.href = window.location.origin+'/People/EditOwners?id='+id;
    });

    setTimeout(function(){
        if(localStorage.getItem("AccordionHref")) {
            var message = localStorage.getItem("AccordionHref");
            console.log('message>>', message);
            $('html, body').animate({
                'scrollTop' : $(message).position().top
            });
            // localStorage.removeItem('AccordionHref');
        }
    }, 250);
    var base_url = window.location.origin;

    var id=$(".owner_id").val();

    bankinginfJqgrid();
    function bankinginfJqgrid(status) {
        var id = $(".owner_id").val();
        var table = 'owner_banking_information';
        var columns = ['Bank Acc. for ACH Transaction', 'Acc. Type', 'Account Name', 'Acc. No.', 'Bank Routing Transit Number', 'Bank Institution Name', 'Bank Fraction Number' ];
        var joins = [{table:'owner_banking_information',column:'account_for_transaction',primary:'id',on_table:'company_chart_of_accounts'},{table:'owner_banking_information',column:'account_name',primary:'id',on_table:'owner_account_name'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['owner_banking_information.deleted_at', 'owner_banking_information.updated_at'];
        var extra_where = [{column: 'user_id', value: id, condition: '='}];
        var columns_options = [
            {name: 'Bank Acc. for ACH Transaction',index: 'account_name',width: 100,searchoptions: {sopt: conditions},alias:'acc_name',table:'company_chart_of_accounts'},
            {name: 'Acc. Type', index: 'bank_account_type', width: 100, searchoptions: {sopt: conditions}, table: table, formatter:bankAccountTypeFormatter},
            {name: 'Account Name', index: 'account_name', width: 100, searchoptions: {sopt: conditions}, table:'owner_account_name'},
            {name: 'Acc. No.', index: 'bank_account_number', width: 100, searchoptions: {sopt: conditions}, table: table},
            {name: 'Bank Routing Transit Number', index: 'routing_transit_number', width: 100, searchoptions: {sopt: conditions}, table: table},
            {name: 'Bank Institution Name', index: 'bank_institution_name', width: 100, searchoptions: {sopt: conditions}, table: table},
            {name: 'Bank Fraction Number', index: 'bank_fraction_number', width: 100, searchoptions: {sopt: conditions}, table: table},
        ];
        var ignore_array = [];
        jQuery("#bankinginfo-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {

                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Banking Info",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: false, refresh:false, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 200, drag: true, resize: false}
        );
    }

    function bankAccountTypeFormatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return 'Business Checking Account';
        else if(cellvalue == 2)
            return 'Personal Checking Account';
        else if(cellvalue == 3)
            return 'Business Savings Account';
        else if(cellvalue == 4)
            return 'Personal Savings Account';
        else if(cellvalue == 5)
            return 'Credit';
        else if(cellvalue == 6)
            return 'Undeposited Funds';
        else if(cellvalue == 7)
            return 'Other';
        else
            return '';
    }

    // fetchLinkOwner();
    // function fetchLinkOwner() {
    //     $.ajax({
    //         type: 'post',
    //         url: '/propertyView-ajax',
    //         data: {class: 'propertyView', action: "getLinkOwnerInfo", property_id: 2},
    //         success: function (response) {
    //             var response = JSON.parse(response);
    //             if (response.code == 200) {
    //                 $.each(response.data, function (key, value) {
    //                     $('#' + key).text(value);
    //                 });
    //                 propertyown('All', response.ownedData);
    //             }
    //         },
    //         error: function (data) {
    //             var errors = $.parseJSON(data.responseText);
    //             $.each(errors, function (key, value) {
    //                 $('#' + key + '_err').text(value);
    //             });
    //         }
    //     });
    // }
    propertyown();
    function propertyown(status) {
        var id=$(".owner_id").val();
        var table = 'owner_property_owned';
        var columns = ['Property Name','Percent Owned'];
        var joins = [{table:'owner_property_owned',column:'property_id',primary:'id',on_table:'general_property'}];
        //var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['owner_property_owned.updated_at', 'general_property.property_name'];
        var extra_where = [{column: 'user_id', value: id, condition: '='}];
        var columns_options = [
            {name:'Property Name',index:'property_name',width:400,align:"left",classes: 'owner_property_name',searchoptions: {sopt: conditions},table:'general_property'},
            {name:'Percent Owned',index:'property_percent_owned',width:450,align:"left",searchoptions: {sopt: conditions},table:table},
        ];
        var ignore_array = [];
        jQuery("#property-own").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",

            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                //data:unserialize(),
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'owner_property_owned.updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Property",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: false, refresh:false, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 200, drag: true, resize: false}
        );
    }



    /**
     * jqGrid function to format file extension
     * @param status
     */
    function imageFormatter(cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var src = '';
            if (rowObject.Preview == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
            } else if (rowObject.Preview == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
            } else if (rowObject.Preview == 'docx' || rowObject.Preview == 'doc') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
            }else if (rowObject.Preview == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
            }
            return '<img class="img-upload-tab" width=30 height=30 src="' + src + '">';
        }
    }

    /**
     * jqGrid function to format action column of file library of unit
     * @param cellvalue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function actionLibraryFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            select = ['<span><i class="fa fa-trash delete_file_library pointer" data_id="' + rowObject.id + '" style="font-size:24px"></i></span>'];
            var data = '';
            if(select != '') {
                $.each(select, function (key, val) {
                    data += val
                });
            }
            return data;
        }
    }

    $(document).on("click",".delete_file_library",function(){
        var data_id = $(this).attr("data_id");
        bootbox.confirm({
            message: "Are you sure you want to delete this file ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/propertyUnitAjax',
                        data: {class: 'PropertyUnitAjax', action: 'deleteFile', id: data_id},
                        success : function(response){
                            var response =  JSON.parse(response);
                            if(response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                triggerFileReload();
                            }else if(response.status == 'error' && response.code == 503) {
                                toastr.error(response.message);
                            }else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                }
            }
        });
    });

    function triggerFileReload(){
        var grid = $("#file-library");
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    ownerFlags('All');
    function ownerFlags(status) {
        var table = 'flags';
        var columns = ['Date', 'Flag Name', 'Phone Number','Flag Reason','Completed','Note'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [{column: 'object_id', value: id, condition: '='},{column: 'object_type', value: "owner", condition: '='}];
        var columns_options = [
            {name:'Date',index:'date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Flag Name',index:'flag_name',width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Phone',index:'flag_phone_number', width:100,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,table:table,formatter:phoneNumberFormat},
            {name:'Flag Reason',index:'flag_reason',width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Completed',index:'completed', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,table:table,formatter:isCompletedFormatter},
            {name:'Note',index:'flag_note',width:100,align:"center",searchoptions: {sopt: conditions},table:table},
        ];
        var ignore_array = [];
        jQuery("#ownerFlags-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Flags",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 200, drag: true, resize: false}
        );
    }

    /**
     * jqGrid function to format completed column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function phoneNumberFormat(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            if(cellValue !== undefined || cellValue != '' ){
                return cellValue.replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
            }
            else {
                return '';
            }
        }
    }

    /**
     * jqGrid function to format completed column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isCompletedFormatter(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "True";
        else if (cellValue == '0')
            return "False";
        else
            return '';
    }


    ownerComplaintsForView();
    function ownerComplaintsForView(status) {
        var id=$(".owner_id").val();
        var table = 'complaints';
        // var columns = ['<input type="checkbox" id="select_all_complaint_checkbox">','Complaint Id', 'Complaint', 'Complaint Type','Notes','Date','Other'];
        var columns = ['Complaint Id', 'Complaint', 'Complaint Type','Notes','Date','Other'];
        var select_column = ['Edit', 'Delete'];
        var joins = [{table: 'complaints', column: 'complaint_type_id', primary: 'id', on_table: 'complaint_types'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['complaints.status'];
        var extra_where = [{column: 'object_id', value: id, condition: '='},{column: 'module_type', value: "owner", condition: '='}];
        var columns_options = [
            // {name:'Id',index:'id', width:100,align:"center",sortable:false,searchoptions: {sopt: conditions},table:table,search:false,formatter:actionCheckboxFmatter},
            {name:'Complaint Id',index:'complaint_id', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Complaint',index:'complaint_by_about', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Complaint Type',index:'complaint_type', width:100,align:"center",searchoptions: {sopt: conditions},table:"complaint_types"},
            {name:'Notes',index:'complaint_note', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Date',index:'complaint_date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Other',index:'other_notes',width:200,align:"center",searchoptions: {sopt: conditions},search:false,table:table},
        ];
        var ignore_array = [];
        jQuery("#ownerComplaintsView-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'complaints.updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Complaints",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }
    /**
     * jqGrid function to format checkbox column
     * @param status
     */

    function actionCheckboxFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {

            var data = '';
            var data = '<div class="owner_complaint_checkbox"><input type="checkbox" name="complaint_checkbox[]" class="complaint_checkbox" id="complaint_checkbox_'+rowObject.id+'" data_id="' + rowObject.id + '"/></div>';
            return data;
        }
    }

    notes();
    function notes(status) {
        var id=$(".owner_id").val();
        var table = 'tenant_chargenote';
        var columns = [ 'Notes' ];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        // var extra_columns = ['tenant_chargenote.deleted_at'];
        var extra_columns = [ ];
        var extra_where = [{column: 'user_id', value: id, condition: '='}];
        var columns_options = [
            {name: 'Notes',index: 'note',width: 100,align: "center",searchoptions: {sopt: conditions},table: table},

        ];
        var ignore_array = [];
        jQuery("#notes-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_where: extra_where,
                deleted_at: 'no'
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Notes",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: false, refresh:false, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 200, drag: true, resize: false}
        );
    }




    fileLibrary();
    function fileLibrary(status) {

        var id=$(".owner_id").val();
        var table = 'tenant_chargefiles';
        var columns = ['Name','Preview'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        // var extra_columns = [];
        var extra_where = [{column: 'user_id', value: id, condition: '='}];
        var columns_options = [
            {name: 'Name',index: 'filename',width: 100,align: "center",searchoptions: {sopt: conditions},table: table},
            {name:'Preview',index:'file_extension',width:100,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter}

        ];
        var ignore_array = [];
        jQuery("#file-library").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                //  extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Files",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 200, drag: true, resize: false}
        );
    }

    viewData();
    function viewData() {
        var owner_id=$(".owner_id").val();
        $.ajax({
            type: 'post',
            url: '/ViewOwnerAjax',
            data: {
                class: "ViewOwnerAjax",
                action: "getviewData",
                owner_id:owner_id
            },
            success: function (response) {

                var res = $.parseJSON(response);
                if (res.status == "success") {
                    getCustomFieldForViewMode(res.data.custom_data);

                    var user_detail_data = res.data.user_detail;
                    var owner_detail_data = res.data.owner_detail;
                    var emergency_detail_data = res.emergency_data;
                    var owner_credential_data = res.credential_data;

                    if (owner_detail_data.owner_image != '') {
                        $('.owner_image').html(owner_detail_data.owner_image);
                    }

                    if (user_detail_data.if_entity_name_display == '1') {
                        $('#ethnicity_to_dob_div').hide();
                    } else {
                        $('#ethnicity_to_dob_div').show();
                    }
                    $('.name').text(user_detail_data.name);
                    $('.owner_address').text(user_detail_data.owner_address);
                    $('.owner_phone').text(user_detail_data.phone_number);
                    $('.owner_note_for_Phone').text(user_detail_data.phone_number_note);
                    $('.owner_Email').text(owner_detail_data.email);
                    $('.owner_SSN').text(user_detail_data.ssn_sin_id);
                    $('.owner_ethnicity').text(user_detail_data.ethnicity_name);

                    $('.owner_hobbies').text(user_detail_data.hobbies);
                    $('.owner_marital_status').text(user_detail_data.marital_status);
                    $('.owner_vetran_status').text(user_detail_data.veteran_name);
                    $('.owner_dob').text(user_detail_data.dob);

                    var owners_portal = (user_detail_data.owners_portal == 1) ? 'Yes' : 'No'
                    $('.owners_portal').text(owners_portal);

                    var email_statement = (user_detail_data.email_financial_info == 1) ? 'Yes' : 'No'
                    $('.email_statement').text(email_statement);

                    $('.tax_payer_name').text(user_detail_data.tax_payer_name);
                    $('.tax_payer_id').text(user_detail_data.tax_payer_id);

                    var eligible_for_1099 = (user_detail_data.eligible_1099 == 1) ? 'Yes' : 'No'
                    $('.eligible_for_1099').text(eligible_for_1099);

                    var emergency_detail_data_html = '';
                    if (emergency_detail_data.length >0) {
                        $.each(emergency_detail_data, function (key, value) {
                            emergency_detail_data_html += "<div class=\"row\" style='margin-bottom: 10px;'>\n" +
                                "<div class=\"col-sm-6\">\n" +
                                "<div class=\"col-xs-12\">\n" +
                                "<label class=\"text-right\">Emergency Contact Name : </label>\n" +
                                "<span class=\"emeregency_name\">" + value.emergency_contact_name + "</span>\n" +
                                "</div>\n" +
                                "<div class=\"col-xs-12\">\n" +
                                "<label class=\"text-right\">Country Code :</label>\n" +
                                "<span class=\"country_code\"> " + value.emergency_country_code + "</span>\n" +
                                "</div>\n" +
                                "<div class=\"col-xs-12\">\n" +
                                "<label class=\"text-right\">Emergency Contact Phone : </label>\n" +
                                "<span class=\"emeregency_contact\">" + value.emergency_phone_number + "</span>\n" +
                                "\</div>\n" +
                                " </div>\n" +
                                "<div class=\"col-sm-6\">\n" +
                                "<div class=\"col-xs-12\">\n" +
                                "<label class=\"text-right\">Emergency Contact Email : </label>\n" +
                                "<span class=\"emeregency_email\">" + value.emergency_email + "</span>\n" +
                                "</div>\n" +
                                "<div class=\"col-xs-12\">\n" +
                                "<label class=\"text-right\">Emergency Contact Relation : </label>\n" +
                                "<span class=\"emeregency_relation\">" + value.emergency_relation + "</span>\n" +
                                "</div>\n" +
                                "</div>\n" +
                                "</div>"
                        });
                        $('.emergency_detail_data').html(emergency_detail_data_html);
                    }

                    var html = '';
                    if (owner_credential_data.length >0) {
                        $.each(owner_credential_data, function (key, value) {
                            html += "<div class=\"row\" style=\"margin-bottom: 10px;\">\n" +
                                "    <div class=\"col-sm-6\">\n" +
                                "        <div class=\"col-xs-12\">\n" +
                                "            <label class=\"text-right\">Credential Name :</label>\n" +
                                "            <span class=\"credential_name\">" + value.owner_credential_name + "</span>\n" +
                                "        </div>\n" +
                                "\n" +
                                "        <div class=\"col-xs-12\">\n" +
                                "            <label class=\"text-right\">Credential Type :</label>\n" +
                                "            <span class=\"credential_type\">" + value.owner_credential_type + "</span>\n" +
                                "        </div>\n" +
                                "        <div class=\"col-xs-12\">\n" +
                                "            <label class=\"text-right\">Acquire Date :</label>\n" +
                                "            <span class=\"acquire_date\">" + value.owner_credential_acquire_date + "</span>\n" +
                                "        </div>\n" +
                                "    </div>\n" +
                                "\n" +
                                "    <div class=\"col-sm-6\">\n" +
                                "        <div class=\"col-xs-12\">\n" +
                                "            <label class=\"text-right\">Expiraion Date :</label>\n" +
                                "            <span class=\"expiration_date\">" + value.owner_credential_expiration_date + "</span>\n" +
                                "        </div>\n" +
                                "\n" +
                                "        <div class=\"col-xs-12\">\n" +
                                "            <label class=\"text-right\">Notice Period :</label>\n" +
                                "            <span class=\"notice_period\">" + value.owner_credential_notice_period + "</span>\n" +
                                "        </div>\n" +
                                "\n" +
                                "    </div>\n" +
                                "\n" +
                                "</div>"
                        });
                        $('.owner_credentials').html(html);
                    }
                    if(owner_detail_data.status == 4)
                    {
                        $('.edit_redirection').hide();
                    } else {
                        $('.edit_redirection').show();
                    }

                } else if (res.status == "error") {
                    toastr.error('Owner does not exist.');
                    window.location.href="/People/Ownerlisting";
                } else {
                    toastr.error(res.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});

function getCustomFieldForViewMode(editCustomFieldData){
    var module = 'owner';
    $.ajax({
        type: 'post',
        url: '/CustomField/get',
        data:{module:module},
        success: function (response) {
            var response = JSON.parse(response);
            console.log('res>>>', response);
            var custom_array = [];
            if(response.code == 200 && response.message == 'Record retrieved successfully'){
                $('.custom_field_html_view_mode').html('');
                $('.custom_field_msg').html('');
                var html ='';
                $.each(response.data, function (key, value) {
                    var default_val = '';
                    if(editCustomFieldData) {
                        default_val = (value.default_value != '' && value.default_value != null) ? value.default_value : '';
                        $.each(editCustomFieldData, function (key1, value1) {
                            if (value1.id == value.id) {
                                default_val = value1.value;
                            }
                        });
                    } else {
                        default_val = (value.default_value != '' && value.default_value != null) ? value.default_value : '';
                    }
                    var required = (value.is_required == '1')?' <em class="red-star">*</em>':'';
                    var name = value.field_name.replace(/ /g, "_");
                    var currency_class = (value.data_type == 'currency')?' amount':'';
                    if(value.data_type == 'date') {
                        custom_array.push(name);
                    }
                    var readonly = (value.data_type == 'date') ? 'readonly' : '';
                    html += '<div class="row custom_field_class">';
                    html += '<div class=col-sm-6>';
                    html += '<label>'+value.field_name+required+'</label>';
                    html += '<input type="text" readonly data_type="'+value.data_type+'" data_value="'+value.default_value+'" '+readonly+' data_id="'+value.id+'" data_required="'+value.is_required+'" name="'+name+'" id="'+name+'" value="'+default_val+'" class="form-control changeCustom add-input'+currency_class+'">';
                    if(value.is_editable == 1 || value.is_deletable == 1) {
                        if (value.is_editable == 1) {
                            html += '<span id="' + value.id + '" class="editCustomField"><i class="fa fa-edit"></i></span>';
                        }
                        if (value.is_deletable == 1) {
                            html += '<span id="' + value.id + '" class="deleteCustomField"><i class="fa fa-times-circle""></i></span>';
                        }
                    }
                    html += '<span class="customError required"></span>';
                    html += '</div>';
                    html += '</div>';
                });
                $(html).prependTo('.custom_field_html_view_mode');
            } else {
                $('.custom_field_msg').html('No Custom Fields');
                $('.custom_field_html_view_mode').html('');
            }
            triggerDatepicker(custom_array)
        },
        error: function (data) {
        }
    });
}

function triggerDatepicker(custom_array){
    $.each(custom_array, function (key, value) {
        $('#customField'+value).datepicker({
            dateFormat: datepicker
        });
    });
}