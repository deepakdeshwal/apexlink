
/*vendor new connected and payment setup code starts here */


$(document).on('click','#financial-info .basicDiv',function(){
    // $("#fmcc,#fcmcc").val(6513);
    $("#financial-infotype").modal('show');

});


$(document).on("click","#savefinancialtype",function(){
    var user_id = $("#company_user_id").val();
    getAccountDetails(user_id);
});

function getAccountDetails(data_id){

    $('#financial-infotype').modal('hide');
    var radioValue = $("input[name='companytype']:checked").val();
    if(radioValue == 'individual') {
        $(".basic-payment-detail").show();
        $(".apx-adformbox-content").hide();
        $(".company-basic-payment-detail").hide();
        $(this).addClass("active");
        $(".paymentDiv").removeClass("active");
    }else{
        $(".company-basic-payment-detail").show();
        $(".basic-payment-detail").hide();
        $(".apx-adformbox-content").hide();
        $(this).addClass("active");
        $(".paymentDiv").removeClass("active");



    }

    $('.select_options').prop('selectedIndex',0);
}

$.ajax({
    type: 'post',
    url: '/payment-ajax',
    data: {
        class: 'PaymentAjax',
        action: "getMccTypes"
    },
    success: function (response) {
        var data = $.parseJSON(response);
        if (data.status == "success") {

            if (data.data.mcc_types.length > 0) {
                var mccOption = "<option value='0'>Select</option>";
                $.each(data.data.mcc_types, function (key, value) {
                    mccOption += "<option value='" + value.code + "' data-id='" + value.code + "'>" + value.name + " - " + value.code + " " + "</option>";
                });
                $('#fmcc,#fcmcc,#acc_mcc').html(mccOption);
            }

        } else if (data.status == "error") {
            toastr.error(data.message);
        } else {
            toastr.error(data.message);
        }
    },
    error: function (data) {
        var errors = $.parseJSON(data.responseText);
        $.each(errors, function (key, value) {
            $('#' + key + '_err').text(value);
        });
    }
});

setTimeout(function () {
    $("#fmcc,#fcmcc,#acc_mcc").val(6513);
    // $(".verification-message").html("<p class='verification-due-p'>Verification Due</p>");
}, 500);

$("#financialCompanyInfo").validate({
    rules: {
        fcountry: {
            required:true
        },
        fmcc: {
            required:true
        },fbusiness: {
            required:true
        },
        faccount_number: {
            required:true,
            number:true
        },
        frouting_number: {
            required:true,
            number:true
        },
        fccity: {
            required:true
        },
        fcaddress: {
            required:true
        },
        // address_line2: {
        //     required:true
        // },
        fczipcode: {
            required:true,
        },
        fcstate: {
            required:true
        },fcname: {
            required:true
        },
        fcphone_number: {
            required:true
        },fctax_id: {
            required:true
        },company_document_id: {
            required:true
        },
        femail: {
            required:true
        },
        ffirst_name: {
            required:true
        },
        fplast_name: {
            required:true
        },
        fphone_number: {
            required:true
        },
        fssn: {
            required:true,
            number:true
        },
        fday: {
            required:true
        },
        fmonth: {
            required:true
        },
        fyear: {
            required:true
        },furl: {
            required:true
        },faddress: {
            required:true
        },faddress2: {
            required:true
        },fzipcode: {
            required:true
        },fcity: {
            required:true
        },company_document:{
            required:true
        }
    },
    submitHandler: function (e) {
        var form = $('#financialCompanyInfo')[0];
        var formData = new FormData(form);
        stripe_detail_form_data =  $(form).serialize()
        formData.append('action', 'createCompanyConnectedAccount');
        formData.append('class', 'addVendor');
        // console.log(formData); return false;
        $('#loadingmessage').show();
        $.ajax({
            url: '/vendor-add-ajax',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {

            },
            success: function (response) {
                $('#loadingmessage').hide();
                var response = JSON.parse(response)

                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $("#hidden_account_id").val(response.account_data.id);
                    localStorage.setItem('stripe_acc_id', response.account_data.id);
                    var account_type = $("#fcbusiness").val();
                    $("#hidden_account_type").val(account_type);
                    $("#financial-info").modal("hide");

                } else {
                    $('#loadingmessage').hide();
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                $('#loadingmessage').hide();
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });


        //  var country= $("#fcountry").val();
        //  var mcc= $("#fcmcc").val();
        //  var url = $("#fcurl").val();

        //  var account_number= $("#fcaccount_number").val();
        //  var routing_number=  $("#fprouting_number").val();
        //  var city =  $("#fccity").val();
        //  var line1 = $("#fcaddress").val();
        //  var address_line2 = $("#fcaddress2").val();
        //  var postal_code= $("#fczipcode").val();
        //  var state = $("#fcstate").val();
        //  var company_name = $("#fcname").val();
        //  var phone = $("#fcphone_number").val();
        //  var tax_id = $("#fctax_id").val();
        //  var document_id = $("#company_document_id").val();
        //  var email= $("#fpemail").val();
        //  var first_name= $("#fpfirst_name").val();
        //  var last_name = $("#acc_last_name").val();
        //  var pphone = $("#fpphone_number").val();
        //  var ssn_last=  $("#fpssn").val();
        //  var paddress1 = $("#fpaddress").val();
        //  var paddress2 = $("#fpaddress2").val();
        //  var pzipcode = $("#fpzipcode").val();
        //  var pstate = $("#fpstate").val();
        //  var pcity = $("#fpcity").val();
        //  var day = $("#fppyear").val();
        //  var month = $("#fppmonth").val();
        //  var year = $("#fppyear").val();
        //
        //  detailCompanyArr.push({"country":country,"mcc":mcc,"url":url,"account_type":account_type,"account_number":account_number,"rounting_number":routing_number,"city":city,"line1":line1,"line2":address_line2,"postal_code":postal_code,"state":state,"company_name":company_name,"phone":phone,"tax_id":tax_id,"document_id":document_id,"email":email,"first_name":first_name,"last_name":last_name,"person_phone":pphone,"day":day,"month":month,"year":year,"last_4_ssn":ssn_last,"person_address1":paddress1,"person_address2":paddress2,"person_zipcode":pzipcode,"person_state":pstate,"person_city":pcity});
        //  console.log(detailCompanyArr);
        // // $("#hidden_acc_detail").val(detailCompanyArr);

    }
});



/*
For zipcode functionality

*/
$(document).on('focusout','#fczipcode',function(){
    getAddressInfoByZip($(this).val());
});

/*
For zipcode functionality

 */
$("#fczipcode").keydown(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        getAddressInfoByZip($(this).val());
    }
});

/**
 *Get location on the basis of zipcode
 */
function getAddressInfoByZip(zip){
    if(zip.length >= 5 && typeof google != 'undefined'){
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                         console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    setTimeout(function(){
                        response(addr);
                    }, 2000);

                } else {
                    response({success:false});
                }
            } else {
                response({success:false});
            }
        });
    } else {
        response({success:false});
    }
}

/**
 * Set values in the city, state and country when focus loose from zipcode field.
 */
function response(obj){
    if(obj.success){
        $('#fccity').val(obj.city);
        $('#fcstate').val(obj.state);

    } else {
        $('#ccity').val('');
        $('#cstate').val('');
    }
}


/*
For zipcode functionality

*/
$(document).on('focusout','#fpzipcode',function(){
    getAddressInfoByZip1($(this).val());
});

/*
For zipcode functionality

 */
$("#fpzipcode").keydown(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        getAddressInfoByZip1($(this).val());
    }
});

/**
 *Get location on the basis of zipcode
 */
function getAddressInfoByZip1(zip){
    if(zip.length >= 5 && typeof google != 'undefined'){
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                         console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    setTimeout(function(){
                        response1(addr);
                    }, 2000);

                } else {
                    response1({success:false});
                }
            } else {
                response1({success:false});
            }
        });
    } else {
        response1({success:false});
    }
}

/**
 * Set values in the city, state and country when focus loose from zipcode field.
 */
function response1(obj){
    if(obj.success){
        $('#fpcity').val(obj.city);
        $('#fpstate').val(obj.state);

    } else {
        $('#fpcity').val('');
        $('#fpstate').val('');
    }
}

$(function () {
    $("#company_document:file").change(function () {
        if (this.files && this.files[0]) {
            file = this.files[0];
            formdata = new FormData();
            formdata.append("company_document", file);
            formdata.append("action",'uploadCompanyDocument');
            formdata.append("class",'PaymentAjax');
            $.ajax({
                url: '/payment-ajax',
                enctype: 'multipart/form-data',
                data: formdata,
                type: "POST",
                processData: false,
                contentType: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        // $('#company_document').val('');

                        $("#company_document_id").val(response.document_id);
                        toastr.success(response.message);
                        $('#loadingmessage').hide();
                    } else {
                        $('#company_document').val('')
                        toastr.error(response.message);
                        $('#loadingmessage').hide();
                    }
                },
                error: function (response) {
                    // console.log(response);
                }
            });
        }
    });
});




var min = new Date().getFullYear(),
    max = min - 49,
    select = document.getElementById('fppyear');
for (var i = max; i<=min; i++){
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}


var min = 1,
    max =31,
    select = document.getElementById('fppday');

for (var i = min; i<=max; i++){
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}


var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var month = 0;
// var i = 1;
for (; month < monthNames.length; month++) {

    var new_month = month+1;
    $('#fppmonth').append('<option value='+new_month+'>' + monthNames[month] + '</option>');
}

/**
 * If the letter is not digit in phone number then don't type anything.
 */
$("#phone_number,#fphone_number,#fssn,#fpphone_number,#fpssn").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
});


/*vendor new connected and payment setup code ends here */



$(document).on('click','#financial-info .basicDiv',function(){
    $(".basic-payment-detail").show();
    $(".apx-adformbox-content").hide();
    $(this).addClass("active");
    $(".paymentDiv").removeClass("active");
});
$(document).on('click','#financial-info .paymentDiv',function(){
    $(".basic-payment-detail").hide();
    $(".apx-adformbox-content").show();
    $(this).addClass("active");
    $(".basicDiv").removeClass("active");
});
$(document).on('click','.background_check_open',function () {
    if( $(this).is(':checked') )
    {   $(this).prop('checked', false);
        // $('#backGroundCheckPop').modal('hide');
        // $('#backgroundReport').modal('show');
        window.open('https://www.victig.com', '_blank');
    }
});

$("#addVendorCardDetailsForm").validate({
    rules: {
        cfirst_name: {
            required:true
        },
        clast_name: {
            required:true
        },
        ccard_number: {
            required:true,
            number:true
        },
        cexpiry_year: {
            required:true
        },
        cexpiry_month: {
            required:true
        },
        ccvv: {
            required:true,
            number:true,
        },
        cphoneNumber: {
            required:true
        },
        caddress1: {
            required:true
        },
        caddress2: {
            required:true
        },
        ccity: {
            required:true
        },
        cstate: {
            required:true
        },
        czip_code: {
            required:true
        },
        ccountry: {
            required:true
        }
    },
    submitHandler: function (e) {
        $('#loadingmessage').show();
        var form = $('#addVendorCardDetailsForm')[0];
        var formData = new FormData(form);
        var vendor_id=$("#stripe_vendor_id").val();
        formData.append('action','addVendorCardDetails');
        formData.append('class','Stripe');
        formData.append('vendor_id',vendor_id);
        action = 'addAdditionalTenant';
        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                $('#loadingmessage').hide();
                if(response.status == 'success' && response.code == 200){
                    toastr.success(response.message);
                    $("#financial-info").modal("hide");
                } else if(response.status == 'failed'){
                    toastr.error(response.message);
                }else if(response.status == 'AccountDetailupdated'){
                    $('#loadingmessage').hide();
                    // console.log(response.enableAccount);
                    if(response.enableAccount.code == 400){
                        toastr.error(response.enableAccount.message);
                    }else if(response.enableAccount.code == 200){
                        toastr.success(response.enableAccount.message);
                        $("#financial-info").modal("hide");
                    }
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});



$("#createAccountIdForm").validate({
    rules: {
        country: {
            required:true
        },
        account_type: {
            required:true
        },
        account_number: {
            required:true,
            number:true
        },
        routing_number: {
            required:true,
            number:true
        },
        city: {
            required:true
        },
        line1: {
            required:true
        },
        address_line2: {
            required:true
        },
        postal_code: {
            required:true,
        },
        state: {
            required:true
        },
        email: {
            required:true
        },
        first_name: {
            required:true
        },
        last_name: {
            required:true
        },
        phone: {
            required:true,
            number:true
        },
        ssn_last: {
            required:true,
            number:true
        },
        dob: {
            required:true
        }
    },
    submitHandler: function (e) {
        var form = $('#createAccountIdForm')[0];
        var formData =new FormData(form);
        var vendor_id=$("#stripe_vendor_id").val();
        formData.append('action','addVendorAccountDetails');
        formData.append('class','Stripe');
        formData.append('user_id',vendor_id);
        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    toastr.success(response.message);
                    $("#financial-info").modal("hide");
                } else if(response.status == 'failed'){
                    toastr.error(response.message);
                }else if(response.status == 'AccountDetailupdated'){
                    // console.log(response.enableAccount);
                    if(response.enableAccount.code == 400){
                        toastr.error(response.enableAccount.message);
                    }else if(response.enableAccount.code == 200){
                        toastr.success(response.enableAccount.message);
                        $("#financial-info").modal("hide");
                    }
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});
// $(document).on("click","#basicDiv",function(){
//     var vendor_id=$("#stripe_vendor_id").val();
//     $("#createAccountIdForm").trigger("reset");
//     $.ajax({
//         type: 'post',
//         url:'/stripeCheckout',
//         data: {
//             class: 'Stripe',
//             action: 'getAccountDetail',
//             user_id: vendor_id},
//         success: function (response) {
//             var response = JSON.parse(response);
//             if(response.status == 'AccountDetail'){
//                 $.each(response.data, function (key, value) {
//                     console.log(key+' '+value);
//                     $('.' + key).val(value);
//                     if(key == 'dob'){
//                         $(".dob").val(response.dob);
//                     }
//
//                 });
//
//             } else if(response.code == 500){
//                 toastr.error('error');
//             }else if(response.message == 'pending'){
//                 $(".basic-payment-detail").hide();
//                 setTimeout(function(){
//                     // $(".verification-message").html("<p class='verification-due-p'>Verification Due</p>");
//                 },500);
//
//             }else if(response.message == 'verified'){
//                 $("#basicDiv").hide();
//                 $(".basic-payment-detail").hide();
//                 $('.payment_li').addClass('after-hide');
//             }
//         },
//     });
// });

function getTenantInfo(id)
{
    $.ajax({
        type: 'post',
        url: '/stripeCheckout',
        data: {
            class: 'Stripe',
            action: 'getTenantZipCode',
            id:id},
        success: function (response) {
            getZipCode('.czip_code', response, '#ccity', '.cstate', '.ccountry', null, null);


        },
    });


}

$(document).on("blur",".czip_code",function(){
    // var value = $(this).val();
    // getZipCode('.czip_code', value, '#ccity', '.cstate', '.ccountry', null, null);
    getAddressInfoByZip2(response);
});

function getZipCode(element,zipcode,city,state,country,county,validation){
    $(validation).val('1');
    $.ajax({
        type: 'post',
        url: '/common-ajax',
        data: {zip_code: zipcode,class: 'CommonAjax', action: 'getZipcode'},
        success: function (response) {
            var data = JSON.parse(response);
            if(data.code == 200){
                if(city !== undefined)$(city).val(data.data.city);
                if(state !== undefined)$(state).val(data.data.state);
                if(country !== undefined)$(country).val(data.data.country);
                if(county !== undefined)$(county).val(data.data.county);
            } else {
                getAddressInfoByZip(element,zipcode,city,state,country,county,validation);
            }
        },
        error: function (data) {
        }
    });
}




// function getAddressInfoByZip(element,zip,city,state,country,county,validation){
//     var addr = {};
//     addr.element = element;
//     addr.elecity = city;
//     addr.elestate = state;
//     addr.elecountry = country;
//     addr.elecounty = county;
//     addr.validation = validation;
//     if(zip.length >= 5 && typeof google != 'undefined'){
//         var geocoder = new google.maps.Geocoder();
//         geocoder.geocode({ 'address': zip }, function(results, status){
//             if (status == google.maps.GeocoderStatus.OK){
//                 if (results.length >= 1) {
//                     for (var ii = 0; ii < results[0].address_components.length; ii++){
//                         //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
//                         var types = results[0].address_components[ii].types.join(",");
//                         if (types == "street_number"){
//                             addr.street_number = results[0].address_components[ii].long_name;
//                         }
//                         if (types == "route" || types == "point_of_interest,establishment"){
//                             addr.route = results[0].address_components[ii].long_name;
//                         }
//                         if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
//                             addr.city =  results[0].address_components[ii].short_name ;
//                         }
//                         if (types == "administrative_area_level_1,political"){
//                             addr.state = results[0].address_components[ii].short_name;
//                         }
//                         if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
//                             addr.zipcode = results[0].address_components[ii].long_name;
//                         }
//                         if (types == "country,political"){
//                             addr.country = results[0].address_components[ii].long_name;
//                         }
//                     }
//                     addr.success = true;
//                     response(addr);
//                 } else {
//                     addr.success = false;
//                     response(addr);
//                 }
//             } else {
//                 addr.success = false;
//                 response(addr);
//             }
//         });
//     } else {
//         addr.success = false;
//         response(addr);
//     }
// }

$(document).on('focusout','#acc_postal_code',function(){
    //getZipCode('#acc_postal_code', $(this).val(), '#acc_city', '#acc_state', '#acc_country', '', '');
    getAddressInfoByZip2($(this).val());
});
// $("#addStep1Form").validate({
//     rules: {
//         cfirst_name: {
//             required:true
//         },
//         clast_name: {
//             required:true
//         },
//         ccard_number: {
//             required:true,
//             number:true
//         },
//         cexpiry_year: {
//             required:true
//         },
//         cexpiry_month: {
//             required:true
//         },
//         ccvv: {
//             required:true,
//             number:true,
//         },
//         cCompany: {
//             required:true
//         },
//         cphoneNumber: {
//             required:true
//         },
//         caddress1: {
//             required:true
//         },
//         // caddress2: {
//         //     required:true
//         // },
//         ccity: {
//             required:true
//         },
//         cstate: {
//             required:true
//         },
//         czip_code: {
//             required:true
//         },
//         ccountry: {
//             required:true
//         }
//     },
//     submitHandler: function (e) {
//         var ccard_number=$("#ccard_number").val();
//         var ccvv=$("#ccvv").val();
//         var cexpiry_month=$("#cexpiry_month").val();
//         var cexpiry_year=$("#cexpiry_year").val();
//
//         $("#hidden_ccard_number").val(ccard_number);
//         $("#hidden_ccvv").val(ccvv);
//         $("#hidden_cexpiry_month").val(cexpiry_month);
//         $("#hidden_cexpiry_year").val(cexpiry_year);
//
//         $("#financial-info").modal("hide");
//
//     }
// });


$("#addStep1Form").validate({
    rules: {
        cfirst_name: {
            required:true
        },
        clast_name: {
            required:true
        },
        ccard_number: {
            required:true,
            number:true
        },
        cexpiry_year: {
            required:true
        },
        cexpiry_month: {
            required:true
        },
        ccvv: {
            required:true,
            number:true,
        },
        // cCompany: {
        //     required:true
        // },
        // cphoneNumber: {
        //     required:true
        // },
        // caddress1: {
        //     required:true
        // },
        // caddress2: {
        //     required:true
        // },
        // ccity: {
        //     required:true
        // },
        // cstate: {
        //     required:true
        // },
        // czip_code: {
        //     required:true
        // },
        // ccountry: {
        //     required:true
        // }
    },
    submitHandler: function (e) {
        // var ccard_number=$("#ccard_number").val();
        // var ccvv=$("#ccvv").val();
        // var cexpiry_month=$("#cexpiry_month").val();
        // var cexpiry_year=$("#cexpiry_year").val();
        //
        // $("#hidden_ccard_number").val(ccard_number);
        // $("#hidden_ccvv").val(ccvv);
        // $("#hidden_cexpiry_month").val(cexpiry_month);
        // $("#hidden_cexpiry_year").val(cexpiry_year);
        $('#loadingmessage').show();
        var form = $('#addStep1Form')[0];
        var formData = new FormData(form);
        stripe_card_detail_form_data =  $(form).serialize();
        //var vendor_id=$("#stripe_vendor_id").val();
        formData.append('action','addNewVendorCardDetails');
        formData.append('class','Stripe');
        //formData.append('vendor_id',vendor_id);
        action = 'addAdditionalTenant';
        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                $('#loadingmessage').hide();
                if(response.status== 200)
                {
                    $('#loadingmessage').hide();
                    $("#hidden_customer_id").val(response.customer_id)
                    toastr.success(response.result);
                    localStorage.setItem('customer_id', response.customer_id);
                    $("#financial-info").modal("hide");
                    // window.location.reload();

                }
                else
                {
                    $('#loadingmessage').hide();
                    toastr.error(response.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });



    }
});

$("#acc_dob").datepicker({dateFormat: datepicker,
    changeYear: true,
    yearRange: "-100:+20",
    changeMonth: true,
});
$(document).ready(function(){

    var min = new Date().getFullYear(),
        max = min + 49,
        select = document.getElementById('cexpiry_year');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var min = 1,
        max =12,
        select = document.getElementById('cexpiry_month');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }



});

// $("#addStep2Form").validate({
//     rules: {
//         country: {
//             required:true
//         },
//         account_type: {
//             required:true
//         },
//         account_number: {
//             required:true,
//             number:true
//         },
//         routing_number: {
//             required:true,
//             number:true
//         },
//         city: {
//             required:true
//         },
//         line1: {
//             required:true
//         },
//         // address_line2: {
//         //     required:true
//         // },
//         postal_code: {
//             required:true,
//         },
//         state: {
//             required:true
//         },
//         email: {
//             required:true
//         },
//         first_name: {
//             required:true
//         },
//         last_name: {
//             required:true
//         },
//         phone: {
//             required:true,
//             number:true
//         },
//         ssn_last: {
//             required:true,
//             number:true
//         },
//         day: {
//             required:true
//         },
//         month: {
//             required:true
//         },
//         year: {
//             required:true
//         }
//     },
//     submitHandler: function (e) {
//
//        var country= $("#acc_country").val();
//        var account_type = $("#acc_account_type").val();
//        var account_number= $("#acc_account_number").val();
//        var routing_number=  $("#acc_routing_number").val();
//        var city =  $("#acc_city").val();
//        var line1 = $("#acc_line1").val();
//        var address_line2 = $("#acc_address_line2").val();
//        var postal_code= $("#acc_postal_code").val();
//        var state = $("#acc_state").val();
//        var email= $("#acc_email").val();
//        var first_name= $("#acc_first_name").val();
//        var last_name = $("#acc_last_name").val();
//        var phone = $("#acc_phone").val();
//        var ssn_last=  $("#acc_ssn_last").val();
//        var dob = $("#acc_dob").val();
//
//         var detailArr=[];
//         detailArr.push(country,account_type,account_number,routing_number,city,line1,address_line2,postal_code,state,email,first_name,last_name,phone,ssn_last,dob);
//         console.log(detailArr);
//         $("#hidden_acc_detail").val(detailArr);
//
//         $("#financial-info").modal("hide");
//
//     }
// });

$("#addStep2Form").validate({
    rules: {
        country: {
            required:true
        },
        mcc: {
            required:true
        },
        url: {
            required:true,
            url:true
        },
        account_type: {
            required:true
        },
        account_number: {
            required:true,
            number:true
        },
        routing_number: {
            required:true,
            number:true
        },
        city: {
            required:true
        },
        line1: {
            required:true
        },
        // address_line2: {
        //     required:true
        // },
        postal_code: {
            required:true,
        },
        state: {
            required:true
        },
        email: {
            required:true
        },
        first_name: {
            required:true
        },
        last_name: {
            required:true
        },
        phone: {
            required:true,
            number:true
        },
        ssn_last: {
            required:true,
            number:true
        },
        day: {
            required:true
        },
        month: {
            required:true
        },
        year: {
            required:true
        }
    },
    submitHandler: function (e) {

        var form = $('#addStep2Form')[0];
        var formData = new FormData(form);
        stripe_detail_form_data =  $(form).serialize()
        formData.append('action', 'createIndividualConnectedAccount');
        formData.append('class', 'addVendor');
        // console.log(formData); return false;
        $('#loadingmessage').show();
        $.ajax({
            url: '/vendor-add-ajax',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {

            },
            success: function (response) {
                $('#loadingmessage').hide();
                var response = JSON.parse(response)

                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $("#hidden_account_id").val(response.account_data.id);
                    localStorage.setItem('stripe_acc_id', response.account_data.id);
                    var account_type = $("#acc_account_type").val();
                    $("#hidden_account_type").val(account_type);
                    $("#financial-info").modal("hide");

                } else {
                    $('#loadingmessage').hide();
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                $('#loadingmessage').hide();
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        // var country= $("#acc_country").val();
        // var account_type = $("#acc_account_type").val();
        // var account_number= $("#acc_account_number").val();
        // var routing_number=  $("#acc_routing_number").val();
        // var city =  $("#acc_city").val();
        // var line1 = $("#acc_line1").val();
        // var address_line2 = $("#acc_address_line2").val();
        // var postal_code= $("#acc_postal_code").val();
        // var state = $("#acc_state").val();
        // var email= $("#acc_email").val();
        // var first_name= $("#acc_first_name").val();
        // var last_name = $("#acc_last_name").val();
        // var phone = $("#acc_phone").val();
        // var ssn_last=  $("#acc_ssn_last").val();
        // var dob = $("#acc_dob").val();
        //
        // var detailArr=[];
        // detailArr.push(country,account_type,account_number,routing_number,city,line1,address_line2,postal_code,state,email,first_name,last_name,phone,ssn_last,dob);
        // console.log(detailArr);
        // $("#hidden_acc_detail").val(detailArr);
        //
        // $("#financial-info").modal("hide");

    }
});


var min = 1,
    max =31,
    select = document.getElementById('fpday');

for (var i = min; i<=max; i++){
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}


var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var month = 0;
// var i = 1;
for (; month < monthNames.length; month++) {

    var new_month = month+1;
    $('#fpmonth').append('<option value='+new_month+'>' + monthNames[month] + '</option>');
}

var min = new Date().getFullYear(),
    max = min - 49,
    select = document.getElementById('fpyear');
for (var i = max; i<=min; i++){
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}


/**
 *Get location on the basis of zipcode
 */
function getAddressInfoByZip2(zip){
    if(zip.length >= 5 && typeof google != 'undefined'){
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                         console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    setTimeout(function(){
                        response2(addr);
                    }, 2000);

                } else {
                    response2({success:false});
                }
            } else {
                response2({success:false});
            }
        });
    } else {
        response2({success:false});
    }
}

/**
 * Set values in the city, state and country when focus loose from zipcode field.
 */
function response2(obj){
    if(obj.success){
        $('#acc_city').val(obj.city);
        $('#acc_state').val(obj.state);

    } else {
        $('#acc_city').val('');
        $('#acc_state').val('');
    }
}


    $(document).on("change",".setType",function(){
        var value = $(this).val();
        if(value=='1')
        {
            $(".accounts").hide();
            $(".cards").show();
        }
        else
        {
            $(".accounts").show();
            $(".cards").hide();
        }

    });