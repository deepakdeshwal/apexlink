$(document).ready(function() {
    /* new popup js starts here */
    $("#financialCardInfo").validate({
        rules: {
            cfirst_name: {
                required:true
            },
            clast_name: {
                required:true
            },
            ccard_number: {
                required:true,
                number:true
            },
            cexpiry_year: {
                required:true
            },
            cexpiry_month: {
                required:true
            },
            ccvv: {
                required:true,
                number:true,
            },
            cphoneNumber: {
                required:true
            },
            caddress1: {
                required:true
            },
            ccity: {
                required:true
            },
            cstate: {
                required:true
            },
            czip_code: {
                required:true
            },
            ccountry: {
                required:true
            }
        },
        submitHandler: function (e) {
            $('#loadingmessage').show();
            var form = $('#financialCardInfo')[0];
            var formData = new FormData(form);
            var vendor_id=$("#stripe_vendor_id").val();
            formData.append('action','addVendorCardDetails');
            formData.append('class','Stripe');
            formData.append('vendor_id',vendor_id);
            action = 'addAdditionalTenant';
            $.ajax({
                url:'/stripeCheckout',
                type: 'POST',
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    $('#loadingmessage').hide();
                    if(response.status== 200)
                    {
                        $('#loadingmessage').hide();
                        $("#billing-subscription").modal("hide");
                        localStorage.setItem("Message","Payment has been done successfully!");
                        localStorage.setItem('rowcolor_vendor', 'rowColor');
                        window.location.reload();

                    }
                    else
                    {
                        $('#loadingmessage').hide();
                        toastr.error(response.message);
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });

    $(document).on('click','#billing-subscription .paymentDiv',function(){
        $(".basic-payment-detail").hide();
        $(".apx-adformbox-content").show();
        $(this).addClass("active");
        $(".basicDiv").removeClass("active");
    });

    $(document).on('click','#billing-subscription .basicDiv',function(){
        $("#fmcc,#fcmcc").val(6513);
        setTimeout(function(){
            var account_type= $("#business_type").val();
            var verification_status = $("#account_verification").val();
            if(account_type == "company"){
                $(".basic-payment-detail").show();
            } else if(account_type == "individual"){
                $(".basic-user-payment-detail").show();
                $(".fmcc").show();
                $(".furl").show();
            } else {
                // $(".fmcc").hide();
                // $(".furl").hide();
                $('#billing-subscription').modal('hide');
                $('#financial-infotype').modal('show');
            }

            // if(verification_status == 'verified'){
            //     $(".basic-payment-detail").hide();
            //     $(".basic-user-payment-detail").hide();
            // }

            $(".apx-adformbox-content").hide();
            $(".basicDiv").addClass("active");
            $(".paymentDiv").removeClass("active");
        }, 500);
    });


    $.ajax({
        type: 'post',
        url: '/payment-ajax',
        data: {
            class: 'PaymentAjax',
            action: "getMccTypes"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if (data.data.mcc_types.length > 0) {
                    var mccOption = "<option value='0'>Select</option>";
                    $.each(data.data.mcc_types, function (key, value) {
                        mccOption += "<option value='" + value.code + "' data-id='" + value.code + "'>" + value.name + " - " + value.code + " " + "</option>";
                    });
                    $('#fmcc,#fcmcc').html(mccOption);
                }

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

    jQuery('.phone_format').mask('000-000-0000', {reverse: true});



    var min = new Date().getFullYear(),
        max = min - 49,
        select = document.getElementById('fyear');
    for (var i = max; i<=min; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var min = 1,
        max =31,
        select = document.getElementById('fday');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var month = 0;
// var i = 1;
    for (; month < monthNames.length; month++) {

        var new_month = month+1;
        $('#fmonth').append('<option value='+new_month+'>' + monthNames[month] + '</option>');
    }





    var min = new Date().getFullYear(),
        max = min - 49,
        select = document.getElementById('fpyear');
    for (var i = max; i<=min; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var min = 1,
        max =31,
        select = document.getElementById('fpday');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var month = 0;
// var i = 1;
    for (; month < monthNames.length; month++) {

        var new_month = month+1;
        $('#fpmonth').append('<option value='+new_month+'>' + monthNames[month] + '</option>');
    }

    /**
     * If the letter is not digit in phone number then don't type anything.
     */
    $("#phone_number,#fphone_number,#fssn,#fpphone_number,#fpssn").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });


    var min = new Date().getFullYear(),
        max = min + 49,
        select = document.getElementById('cexpiry_year');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var min = 1,
        max =12,
        select = document.getElementById('cexpiry_month');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    $(document).on('click','#savefinancial',function (e) {
        e.preventDefault();
        var financial_data = $( "#financialInfo" ).serializeArray();
        var company_id = $("#company_user_id").val();
        $.ajax
        ({
            type: 'post',
            url: '/payment-ajax',
            data: {
                class: "PaymentAjax",
                action: "validateFinancialData",
                data:financial_data,
                company_id:company_id
            },
            success: function (response) {
                var response = $.parseJSON(response);
                if(response.code == 400)
                {
                    $.each(response.data, function (key) {
                        $('.' + key).text('* This field is required');
                    });
                }else{
                    localStorage.setItem('financial_data',JSON.stringify(financial_data));


                    toastr.clear();
                    updateUserAccount(financial_data);

                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('.' + key + 'Err').text(value);
                });
            }
        });




    });





    $(document).on('click','#savecompanyfinancial',function (e) {
        e.preventDefault();
        var financial_data = $( "#financialCompanyInfo" ).serializeArray();
        console.log(financial_data);


        $.ajax
        ({
            type: 'post',
            url: '/payment-ajax',
            data: {
                class: "paymentAjax",
                action: "validateCompanyFinancialData",
                data:financial_data
            },
            success: function (response) {
                var response = $.parseJSON(response);
                if(response.code == 400)
                {
                    $.each(response.data, function (key) {
                        $('.' + key).text('* This field is required');
                    });
                }else{
                    localStorage.setItem('financial_data',JSON.stringify(financial_data));


                    toastr.clear();
                    updateCompanyAccount(financial_data);

                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('.' + key + 'Err').text(value);
                });
            }
        });




    });

    function updateUserAccount(financial_data){
        $('#loadingmessage').show();
        var company_id = $("#company_user_id").val();
        $.ajax({
            type: 'post',
            url: '/payment-ajax',
            data: {class: 'PaymentAjax', action: "updateUserConnectedAccount",financial_data:financial_data,"company_id":company_id },
            success : function(response){
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    $('#loadingmessage').hide();
                    localStorage.removeItem("financial_data");
                    $('#financial-info').modal('hide');
                    $('#billing-subscription').modal('hide');
                    toastr.success("Account Updated Successfully.");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);


                } else if(response.status == 'error' && response.code == 400){
                    if(response.message == 'online payment error')
                    {
                        toastr.error('Please fill all online payment required fields.');
                    }else{
                        console.log()
                    }
                    $('#loadingmessage').hide();
                    $('.error').html('');
                }
                else if(response.status == 'failed' && response.code == 400){
                    toastr.error(response.message);
                }
                $('#loadingmessage').hide();
                $('.error').html('');
            },
            error: function (response) {
                // console.log(response);
            }
        });
    }


    /* new popup js ends here */

    jQuery('#cphoneNumber').mask('000-000-0000', {reverse: true});
    jQuery('#cphoneNumber').mask('000-000-0000', {reverse: true});
    /*  var base_url = window.location.origin;*/
    if(localStorage.getItem("rowcolorTenant"))
    {
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(9)').addClass("green_row_right");
            localStorage.removeItem('rowcolorTenant');
        }, 2000);
    }

    $(document).on("change", ".tenant_type_status #jqgridOptions", function (e) {
        var status = $(this).val();
        $('#owner_listing').jqGrid('GridUnload');

        jqGrid(status,true);

    });
    $(document).on('click','#AZ',function(){
        $('.AZ').hide();
        $('#apex-alphafilter').show();
    });

    $(document).on('click','#allAlphabet',function(){
        var grid = $("#owner_listing");
        $('.AZ').show();
        $('#apex-alphafilter').hide();
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        //$('#companyUser-table').trigger( 'reloadGrid' );
    });
    $(document).on('click','.getAlphabet',function(){
        var grid = $("#owner_listing"),f = [];
        var value = $(this).attr('data_id');
        var search = $(this).text();
        if(value != '0'){
            f.push({field:"name",op:"bw",data:search});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
        }
    });
    alphabeticSearch();

    function alphabeticSearch(){
        $.ajax({
            type: 'post',
            url:'/Companies/List/jqgrid',
            data: {class: 'jqGrid',
                action: "alphabeticSearch",
                table: 'users',
                column: 'name',
                // where: [{column:'record_status',value:'0',condition:'=',table:'users'}]},
                where: [{column:'user_type',value:'4',condition:'=',table:'users'}]},
            success : function(response){
                var response = JSON.parse(response);
                if(response.code == 200){
                    var html = '';

                    $.each(response.data, function(key,val) {
                        var color = '#05A0E4';
                        if(val == 0) color = '#c5c5c5';
                        html += '<span class="getAlphabet" style="color:'+color+'" data_id="'+val+'">'+key+'</span>';
                    });
                    $('.AtoZ').html(html);
                }
            }
        });

    }
    $(document).on('click','#owner_listing tr td:not(:last-child)',function(e){
        e.preventDefault();
        var base_url = window.location.origin;
        var id = $(this).closest("tr").attr('id');
        window.location.href = base_url + '/People/ViewOwner?id='+id;
    });


    if(localStorage.getItem('ElasticSearch')){
        var elasticSearchData = localStorage.getItem('ElasticSearch');
        setTimeout(function(){
            var grid = $("#owner_listing"),f = [];
            f.push({field: "users.id", op: "eq", data: elasticSearchData,int:'true'});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
            localStorage.removeItem('ElasticSearch');
        },1500);
    }

    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid(1);
    function jqGrid(status, deleted_at) {
        if(jqgridNewOrUpdated == 'true')
        {
            var sortOrder = 'desc';
            var sortColumn = 'users.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }
        var table = 'users';
        var columns = ['Owner Name','Company','Phone', 'Phone_Note', 'Email','Date Created','Owner\'s Portal','Status','Action'];
        var select_column = ['Edit','Email','Text','Work Order','Add In-touch','In-Touch History','Owner Draw','Owner Contribution','Owner statement','Owner Notes','File Library','Flag Bank','Email History','Text History','Run Background Check','Owner Portal','Send Password Activation Email','Owner Complaints','Archive Owner','Resign Owner','DeActivate Account','Print Envelope','Delete Owner'];
        var joins = [{table:'users',column:'id',primary:'user_id',on_table:'owner_details'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [{column:'user_type',value:'4',condition:'=',table:'users'}];
        var columns_options = [
            {name:'Owner Name',index:'name',title:false, width:90,align:"center",searchoptions: {sopt: conditions},table:table,formatter: ownerName,classes: 'pointer',attr:[{name:'flag',value:'owner'}]},
            // {name:'Owner Name',index:'name', width:90,align:"left",searchoptions: {sopt: conditions},table:table, classes:'pointer'},
            {name:'Company',index:'company_name', width:90,align:"center",searchoptions: {sopt: conditions},table:table, classes: 'cursor'},
            // {name:'Phone',index:'phone_number', width:90,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'phone_number_format',formatter:addToolTip, classes: 'cursor',cellattr:cellAttri},
            {name: 'Phone', index: 'id',title:false, width: 80,searchoptions: {sopt: conditions}, table: table, classes: 'cursor',change_type:'line_multiple',index2:'phone_number',index3:'other_work_phone_extension',join:{table: 'users', column: 'id', primary: 'user_id', on_table: 'tenant_phone'},name_id:'id',formatter:addToolTip,cellattr:cellAttri}, /**cellattr:cellAttrdata**/
            {name:'Phone_Note', index: 'phone_number_note', hidden:true, width: 80, searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer'},
            {name:'Email',index:'email', width:80, align:"center",searchoptions: {sopt: conditions},table:table, classes: 'cursor'},
            {name:'Date Created',index:'created_at', width:80, align:"center",searchoptions: {sopt: conditions},table:table, classes: 'cursor'},
            {name:'Owner\'s Portal',index:'owners_portal', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:ownersPortal, classes: 'cursor'},
            {name:'Status',index:'status', width:80,align:"left",searchoptions: {sopt: conditions},table:'owner_details',formatter:statusFormatter, classes: 'cursor'},
            {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, edittype: 'select',search:false,table:table,formatter:statusFmatter1},

        ];
        var ignore_array = [];

        jQuery("#owner_listing").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "users",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Owners",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
            // {top:200,left:200,drag:true,resize:false}
        );
    }

    /**
     * jqGrid function to format tooltip
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function addToolTip(cellValue, options, rowObject) {
        if(rowObject !== undefined){
            if(rowObject.Phone_Note == ''){
                return cellValue;
            } else {
                return '<div class="tooltipgridclass">'+cellValue+'<span class="tooltiptextbotclass">'+rowObject.Phone_Note+'</span></div>';
            }
        }
    }

    /**
     * jqGrid function to format tooltip
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function cellAttri(cellValue, options, rowObject) {
        if(rowObject !== undefined){

            if(rowObject.Phone_Note != ''){
                return 'title = " "';
            }
        }
    }

    $(document).on('mouseover','.tooltipgridclass',function(){
        $(this).closest('td').css("overflow", "unset");
    });

    $(document).on('mouseout','.tooltipgridclass',function(){
        $(this).closest('td').css("overflow", "hidden");
    });
    function statusFormatter (cellValue, options, rowObject){

        if (cellValue == '0' || cellValue == 0)
            return "Inactive";
        else if(cellValue == '1')
            return "Active";
        else if(cellValue == '2')
            return "Archive";
        else if(cellValue == '3')
            return "Delete";
        else if(cellValue == '4')
            return "Past";
        else
            return '';

    }
    /**
     * Function to format owner name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function ownerName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            var flagValue = $(rowObject.Action).attr('flag');
            var id = $(rowObject.Action).attr('data_id');
            var flag = '';
            if (flagValue == 'yes') {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a id="flag" href="javascript:void(0);" data_url="/Property/PropertyView?id="' + id + '"><img src="/company/images/Flag.png"></a></span>';
            } else {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
            }
        }
    }

    function ownersPortal (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            if(cellvalue == '1') {
                return 'Yes';
            }else if(cellvalue == '2')
            {
                return 'Disabled';
            } else {
                return 'No';
            }
        }
    }
    function statusFmatter1 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {

            var select = '';
            var $owners = "Owner's Portal";
            if(rowObject[$owners] == '1'){
                select = ['Edit','Email','Text','Work Order','Add In-touch','In-Touch History','Owner Draw','Owner Contribution','Owner statement','Owner Notes','File Library','Flag Bank','Email History','Text History','Run Background Check','Owner Portal','Send Password Activation Email','Owner Complaints','Archive Owner','Resign Owner','DeActivate Account','Print Envelope','Delete Owner','Online Payment'];
            }else {
                select = ['Edit','Email','Text','Work Order','Add In-touch','In-Touch History','Owner Draw','Owner Contribution','Owner statement','Owner Notes','File Library','Flag Bank','Email History','Text History','Run Background Check','Send Password Activation Email','Owner Complaints','Archive Owner','Resign Owner','Activate Account','Print Envelope','Delete Owner'];
            }
            if(rowObject['Status'] == '2')  select = ['Run Background Check','Activate this Account','Print Envelope','Delete Owner','Online Payment'];
            if(rowObject['Status'] == '4')  select = ['Run Background Check','Reactivate this Owner','Print Envelope','Delete Owner','Online Payment'];

            var data = '';

            var rowObj = rowObject;
            if(select != '') {
                var data = '<select class="form-control select_options" owner_name="'+rowObj['Owner Name']+'" owner_phone="'+rowObject.Phone+'" owner_email="'+rowObject.Email+'" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }
    $(document).on('click','#import_owner',function () {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $('#import_owner_div').show(500);
    });
    $(document).on("click", "#import_owner_cancel_btn", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#import_owner_div").hide(500);
                }
            }
        });
    });
    $(document).on('click','#clearImportForm',function () {
        bootbox.confirm("Do you want Clear this?", function(result) {
            if (result == true) {
                $("#import_file-error").text('');
                $("#import_file").val('');
            }
        });
    });

    $("#importOwnerTypeFormId").validate({
        rules: { import_file: {
                required: true
            },
        },
        submitHandler: function (form) {
            event.preventDefault();

            var formData = $('#importPropertyForm').serializeArray();
            var myFile = $('#import_file').prop('files');
            var myFiles = myFile[0];
            var formData = new FormData();
            formData.append('file', myFiles);
            formData.append('class', 'OwnerAjax');
            formData.append('action', 'importExcel');
            $.ajax({
                type: 'post',
                url: '/OwnerAjax',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        toastr.success(response.message);

                        $("#import_owner_div").hide(500)
                        $('#owner_listing').trigger('reloadGrid');
                    } else if(response.status == 'failed' && response.code == 503){
                        toastr.error(response.message);
                    }else if(response.status == 'warning' && response.code == 503){
                        toastr.warning(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });


    function status_owner(id,key) {
        var owner_status = key;

        $.ajax({
            type: 'post',
            url:'/OwnerAjax',
            data: {
                class: "OwnerAjax",
                action: owner_status,
                user_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#reason_for_leaving_ownerId').val(id);
                    $('#reason_for_leaving').val('');
                    $('#reason_for_leaving-error').text('');
                    $('#reason_notes_div').hide();
                    if(owner_status == 'deactivate_owner')
                    {
                        $("#reasonForLeavingOwner").modal('show');
                    }
                    $('#owner_listing').trigger( 'reloadGrid' );
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    /** Export unit type excel  */
    $(document).on("click",'#export_owner',function(){
        var base_url = window.location.origin;
        var status = $("#jqGridStatus option:selected").val();
        var table = 'users';
        var table1 = 'general_property';
        var table2 = 'tenant_phone';
        var table3 = 'tenant_details';
        var table4 = 'tenant_lease_details';
        var table5 = 'unit_details';
        var table6 = 'building_detail';
        var table7 = 'tenant_property';
        window.location.href = base_url+"/OwnerAjax?status="+status+"&&table="+table+"&&table1="+table1+"&&table2="+table2+"&&table3="+table3+"&&table4="+table4+"&&table4="+table4+"&&table5="+table5+"&&table6="+table6+"&&table7="+table7+"&&action=exportExcel";
        $(this).off('click');
        return false;
    });


    $(document).on("change", "#owner_listing .select_options", function (e) {
        e.preventDefault();
        setTimeout(function(){ $(".select_options").val("default"); }, 200);

        $('#owner_listing').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });

        var row_num = $(this).parent().parent().index() ;
        jQuery('#owner_listing').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('#owner_listing').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");

        var base_url = window.location.origin;
        var action = this.value;
        var id = $(this).attr('data_id');
        var owner_email = $(this).attr('owner_email');
        var owner_phone = $(this).attr('owner_phone');
        switch (action) {
            case "Transfer Tenant":
                window.location.href = base_url + '/Tenant/TransferTenant?tenant_id='+id;
                break;
            case "Delete Owner":
                bootbox.confirm("Are you sure you want to delete this record?", function (result) {
                    if (result == true) {
                        status_owner(id,'delete_owner');
                    }
                });
                break;
            case "Edit":
                localStorage.setItem('edit_active','');
                var editActiveTab = "owner-detail-one";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/People/EditOwners?id='+id;
                break;
            case "Email":
                localStorage.setItem('edit_active','');
                var editActiveTab = "owner-detail-one";
                localStorage.setItem('edit_active',editActiveTab);
                localStorage.setItem('predefined_mail',owner_email);
                localStorage.setItem('table_green_id',id);
                localStorage.setItem('table_green_tableid','#gview_owner_listing');
                localStorage.setItem('table_green_url','/People/Ownerlisting');
                window.location.href = base_url + '/Communication/ComposeEmail';
                break;
            case "Text":
                localStorage.setItem('edit_active','');
                var editActiveTab = "owner-detail-one";
                localStorage.setItem('edit_active',editActiveTab);
                localStorage.setItem('predefined_text',owner_email);
                localStorage.setItem('table_green_id',id);
                localStorage.setItem('table_green_tableid','#gview_owner_listing');
                localStorage.setItem('table_green_url','/People/Ownerlisting');
                window.location.href = base_url + '/Communication/AddTextMessage';
                break;
            case "Work Order":
                localStorage.setItem('redirection_module', 'owner_module');
                window.location.href = base_url + '/WorkOrder/AddWorkOrder?id='+id;
                break;
            case "Add In-touch":
                /*localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-fifteen";
                localStorage.setItem('edit_active',editActiveTab);*/
                window.location.href = base_url + "/Communication/NewInTouch?tid="+id+"&category=Person";
                break;
            case "In-Touch History":
                /*localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-fifteen";
                localStorage.setItem('edit_active',editActiveTab);*/
                window.location.href = base_url + '/Communication/InTouch';
                break;
            case "Email History":
                window.location.href = base_url + '/Communication/SentEmails';
                break;
            case "Text History":
                window.location.href = base_url + '/Communication/TextMessage';
                break;
            case "Run Background Check":
                // $('#backGroundCheckPopCondition').modal('show');
                $('#backGroundCheckPop').modal('show');
                break;
            case "Print Envelope":
                $.ajax({
                    type: 'post',
                    url: '/employeeListAjax',
                    data: {class: 'EmployeeListAjax', action: 'getEmployeeData','employee_id':id},
                    success : function(response){
                        var response =  JSON.parse(response);
                        if(response.status == 'success' && response.code == 200) {
                            console.log('res>>', response);
                            $("#PrintOwnerEnvelope").modal('show');
                            $("#user_company_name").text(response.data.data.company_name)
                            $("#user_address1").text(response.data.data.address1)
                            $("#user_address2").text(response.data.data.address2)
                            $("#user_address3").text(response.data.data.address3)
                            $("#user_address4").text(response.data.data.address4)
                            $("#com_city").text(response.data.data.city)
                            $("#com_state").text(response.data.data.state)
                            $("#com_postal_code").text(response.data.data.zipcode)
                            $("#employee_first_name").text(response.employee.data.first_name)
                            $("#employee_last_name").text(response.employee.data.last_name)
                            $("#employee_address1").text(response.employee.data.address1)
                            $("#employee_address2").text(response.employee.data.address2)
                            $("#employee_address3").text(response.employee.data.address3)
                            $("#employee_address4").text(response.employee.data.address4)
                            $(".employee_city").text(response.employee.data.city)
                            $(".employee_state").text(response.employee.data.state)
                            $(".employee_postal_code").text(response.employee.data.zipcode)
                        }else {
                            toastr.warning('Record not updated due to technical issue.');
                        }
                    }
                });
                break;
            case "Owner Draw":
                var owner_name = $(this).attr('owner_name');
                localStorage.setItem('redirect_owner_name', owner_name);
                localStorage.setItem('redirect_owner_id', id);
                window.location.href = base_url + '/Accounting/GenerateOwnerDraw';
                break;
            case "Owner Contribution":
                var owner_name = $(this).attr('owner_name');
                localStorage.setItem('redirect_owner_name', owner_name);
                localStorage.setItem('redirect_owner_id', id);
                localStorage.removeItem('redirect_owner_id_portal');
                window.location.href = base_url + '/Accounting/GenerateOwnerContribution?id='+id;
                break;

            case "Owner statement":
                var date = new Date(), y = date.getFullYear(), m = date.getMonth();
                // var firstDay = new Date(y, m, 1);
                // var lastDay = new Date(y, m + 1, 0);
                $('#ownerStatementFilter #os_start_date').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat})
                    .datepicker("setDate", new Date(y, m, 1));
                $('#ownerStatementFilter #os_end_date').datepicker({yearRange: '-0:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat, minDate:  $('#start_date').val()})
                    .datepicker("setDate", new Date(y, m + 1, 0));
                $.ajax({
                    type: 'POST',
                    url: '/CommonReportsModal-Ajax',
                    data: {
                        class: 'commonReportsFunction',
                        action: 'fetchModalData',
                        id: id
                    },
                    success: function (response) {
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.portfolio_ddl);
                            $('#ownerStatementFilter #os_portfolio_id').append(data.portfolio_ddl);
                            $('#ownerStatementFilter #os_portfolio_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                            $('#ownerStatementFilter #os_property_id').append(data.property_ddl);
                            $('#ownerStatementFilter #os_property_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');

                            // $("#ownerStatementFilter #os_owner_id").multiselect("destroy");
                            $('#ownerStatementFilter #os_owner_id').html(data.owner_ddl);
                            $('#ownerStatementFilter #os_owner_id').multiselect({
                                includeSelectAllOption: true,
                                // allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    }
                });



                $('#ownerStatementFilter').modal({backdrop: 'static',keyboard: false});
                break;
            case "Owner Notes":
                localStorage.setItem("AccordionHref",'#OwnerNotes');
                window.location.href = base_url + '/People/ViewOwner?id='+id;
                break;
            case "File Library":
                localStorage.setItem("AccordionHref",'#filelibrary');
                window.location.href = base_url + '/People/ViewOwner?id='+id;
                break;
            case "Flag Bank":
                localStorage.setItem("AccordionHref",'#flags');
                window.location.href = base_url + '/People/ViewOwner?id='+id;
                break;
            case "Owner Portal":
                var owner_name = $(this).attr('owner_name');

                localStorage.setItem('redirect_owner_name', owner_name);
                localStorage.setItem('redirect_owner_id_portal', id);
                $.ajax({
                    type: 'post',
                    url: '/OwnerAjax',
                    data: {
                        class: 'OwnerAjax',
                        action: 'ownerPortalLogin',
                        id: id
                    },
                    async: false,
                    success: function (response) {
                        var res = JSON.parse(response);
                        if (res.code == 200 && res.status == "success") {

                            window.open(base_url+'/Owner/MyAccount/AccountInfo');
                        }
                    }
                });

                break;
            case "Send Password Activation Email":
                var email = $(this).attr('owner_email');
                var user_type = '4';
                $.ajax
                ({
                    type: 'post',
                    url: '/OwnerPortalLogin-Ajax',
                    data: {
                        class: "portalLoginAjax",
                        action: "resetPasswordEmailByAdmin",
                        email: email,
                        user_type : user_type
                    },
                    success: function (response) {
                        var data = $.parseJSON(response);
                        if(data.code == 200 && data.status == 'success') {
                            toastr.success('Mail sent successfully.');
                        } else {
                            toastr.warning(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                break;
            case "Owner Complaints":
                localStorage.setItem("AccordionHref",'#complaintInfoDivId');
                window.location.href = base_url + '/People/ViewOwner?id='+id;
                break;
            case "Archive Owner":
                bootbox.confirm("Do you want to Archive the current record?", function (result) {
                    if (result == true) {
                        status_owner(id,'archive_owner');
                    }
                });
                break;
            case "Resign Owner":
                bootbox.confirm("Do you want to Resign this record?", function (result) {
                    if (result == true) {
                        status_owner(id,'resign_owner');
                    }
                });
                break;
            case "DeActivate Account":
                bootbox.confirm("Do you want to Deactivate this record?", function (result) {
                    if (result == true) {
                        status_owner(id,'deactivate_owner');
                    }
                });
                break;
            case "Activate this Account":
                bootbox.confirm("Do you want to Activate this record?", function (result) {
                    if (result == true) {
                        status_owner(id,'reactivate_owner');
                    }
                });
                break;

            case "Activate Account":
                bootbox.confirm("Do you want to Activate this record?", function (result) {
                    if (result == true) {
                        status_owner(id,'activate_owner');
                    }
                });
                break;
            case "Online Payment":

                // fetchUserCardDetail(id);
                // $(".basic-payment-detail").hide();
                // $(".apx-adformbox-content").show();
                // $("#paymentDiv").addClass("active");
                // $(".basicDiv").removeClass("active");
                //
                // $("#stripe_vendor_id").val(id);
                // $(".company_user_id").val(id);
                //
                // getTenantInfo(id);
                // $("#addVendorCardDetailsForm").trigger('reset');
                // $("#financial-info").modal("show");
                // $('#payment_method').val('1');
                //
                //
                // step2Detail(id);

                $(".basic-user-payment-detail").hide();
                fetchCustomerCardDetail(id);
                cardLists(id);
                bankLists(id);

                $("#billing-subscription").modal('show');
                $("#stripe_vendor_id").val(id);
                $("#company_user_id").val(id);
                checkUserAccountVerification(id);
                $(".basic-payment-detail").hide();
                $(".apx-adformbox-content").show();
                $("#paymentDiv").addClass("active");
                $(".basicDiv").removeClass("active");

                //
                //  getTenantInfo(vendor_id);
                //  $("#financial-info").modal("show");
                //  $("#addVendorCardDetailsForm").trigger('reset');
                //  $('#payment_method').val('1');
                //  step2Detail(vendor_id);
                getTenantInfo(id);

                break;
            case "Reactivate this Owner":
                bootbox.confirm("Do you want to Reactivate this record?", function (result) {
                    if (result == true) {
                        status_owner(id,'reactivate_owner');
                    }
                });
                break;

            default:
            //window.location.href = base_url + '/Setting/ApexNewUser';
        }
    });


    function titleCase( cellvalue, options, rowObject) {
        if (cellvalue !== undefined) {

            var string = "";
            $.ajax({
                type: 'post',
                url: '/Tenantlisting/getUserNameById',
                data: {
                    class: "TenantAjax",
                    action: "getUserNameById",
                    id: cellvalue
                },
                async: false,
                success: function (response) {
                    var res = JSON.parse(response);
                    if (res.status == "success") {
                        string = res.data;
                    }
                }
            });
            return string;
        }
    }

    $(document).on('click', '#reason_for_leaving', function (e) {
        e.preventDefault();
        var val = $(this).val();
        if(val == 'Other'){
            $('#reason_notes_div').show();
        } else {
            $('#reason_notes_div').hide();
        }
    });

    $("#reason_for_leaving_form_id").validate({
        rules: {
            reason_for_leaving: {
                required: true
            },
        },
        submitHandler: function() {
            var reason_for_leaving = $('#reason_for_leaving').val();
            var reason_for_leaving_note = $('#reason_for_leaving_note').val();
            var reason_for_leaving_ownerId = $('#reason_for_leaving_ownerId').val();
            var formData = {
                'reason_for_leaving': reason_for_leaving,
                'reason_for_leaving_note': reason_for_leaving_note,
                'reason_for_leaving_ownerId' : reason_for_leaving_ownerId
            };
            $.ajax({
                type: 'post',
                url: '/OwnerAjax',
                data: {
                    class: 'OwnerAjax',
                    action: 'updateReasonForLeaving',
                    form: formData
                },
                success: function(response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        $("#reasonForLeavingOwner").modal('hide');
                    } else if (response.status == 'error' && response.code == 400) {

                    } else if (response.status == 'error' && response.code == 503) {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });

});
$(document).on("change", "#ownerStatementFilter #start_date", function () {
    var start_date = $(this).val();
    var end_date = $('#end_date').val();
    if (end_date != '' && end_date < start_date){
        $('#end_date').datepicker({ dateFormat: jsDateFomat}).datepicker("setDate", start_date);
    }
    $('#end_date').datepicker({dateFormat: jsDateFomat, minDate: start_date});
});


// $(document).on('click','#btnBackgroundSelection',function () {
//     $('#backGroundCheckPopCondition').modal('hide');
//     $('#backGroundCheckPop').modal('show');
// })

$(document).on('click','.background_check_open',function () {
    if( $(this).is(':checked') )
    {
        $(this).prop('checked', false);
        // $('#backGroundCheckPop').modal('hide');
        // $('#backgroundReport').modal('show');
        window.open('https://www.victig.com', '_blank');
    }
});

$(document).on('click','#financial-info .basicDiv',function(){
    $(".basic-payment-detail").show();
    $(".apx-adformbox-content").hide();
    $(this).addClass("active");
    $(".paymentDiv").removeClass("active");
});
$(document).on('click','#financial-info .paymentDiv',function(){
    $(".basic-payment-detail").hide();
    $(".apx-adformbox-content").show();
    $(this).addClass("active");
    $(".basicDiv").removeClass("active");
});
$(document).on('click','.background_check_open',function () {
    if( $(this).is(':checked') )
    {   $(this).prop('checked', false);
        // $('#backGroundCheckPop').modal('hide');
        // $('#backgroundReport').modal('show');
        window.open('https://www.victig.com', '_blank');
    }
});

$("#addVendorCardDetailsForm").validate({
    rules: {
        cfirst_name: {
            required:true
        },
        clast_name: {
            required:true
        },
        ccard_number: {
            required:true,
            number:true
        },
        cexpiry_year: {
            required:true
        },
        cexpiry_month: {
            required:true
        },
        ccvv: {
            required:true,
            number:true,
        },
        cCompany: {
            required:true
        },
        cphoneNumber: {
            required:true
        },
        caddress1: {
            required:true
        },
        // caddress2: {
        //     required:true
        // },
        ccity: {
            required:true
        },
        cstate: {
            required:true
        },
        czip_code: {
            required:true
        },
        ccountry: {
            required:true
        }
    },
    submitHandler: function (e) {
        $('#loadingmessage').show();
        var form = $('#addVendorCardDetailsForm')[0];
        var formData = new FormData(form);
        var owner_id=$("#stripe_vendor_id").val();
        formData.append('action','addOwnerCardDetails');
        formData.append('class','Stripe');
        formData.append('owner_id',owner_id);
        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response);
                $('#loadingmessage').hide();
                if(response.status=="true")
                {
                    $("#financial-info").modal("hide");
                    localStorage.setItem("Message",response.message);
                    localStorage.setItem("rowcolor","rowcolor");

                    window.location.reload();
                }
                else
                {
                    toastr.error(response.message);
                }
            }
        });
    }
});


$(document).ready(function(){
    var min = new Date().getFullYear(),
        max = min + 49,
        select = document.getElementById('cexpiry_year');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var min = 1,
        max =12,
        select = document.getElementById('cexpiry_month');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }

});

function getTenantInfo(id)
{
    $.ajax({
        type: 'post',
        url: '/stripeCheckout',
        data: {
            class: 'Stripe',
            action: 'getTenantZipCode',
            id:id},
        success: function (response) {
            var info = JSON.parse(response);
            // setTimeout(function () {
            var name = "("+info.name+")";
            $(".userName").html(name);
            // },100);
            //getZipCode('#czip_code', response, '#ccity', '#cstate', '#ccountry', null, null);

        },
    });


}

$(document).on("blur","#czip_code",function(){
    var value = $(this).val();
    getZipCode('#czip_code', value, '#ccity', '#cstate', '#ccountry', null, null);
});

function fetchUserCardDetail(id) {
    $.ajax({
        type: 'post',
        url: '/common-ajax',
        data: {
            class: 'CommonAjax',
            action: 'getCustomerDetails',
            id: id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200){
                $('#addVendorCardDetailsForm [name ="cfirst_name"]').val(res.data.data.first_name);
                $('#addVendorCardDetailsForm [name ="clast_name"]').val(res.data.data.last_name);
                $('#addVendorCardDetailsForm [name ="cCompany"]').val(res.data.data.company_name);
                $('#addVendorCardDetailsForm [name ="cphoneNumber"]').val(res.data.data.phone_number);
                $('#addVendorCardDetailsForm [name ="caddress1"]').val(res.data.data.address1);
                $('#addVendorCardDetailsForm [name ="caddress2"]').val(res.data.data.address2);
                $('#addVendorCardDetailsForm [name ="ccity"]').val(res.data.data.city);
                $('#addVendorCardDetailsForm [name ="cstate"]').val(res.data.data.state);
                $('#addVendorCardDetailsForm [name ="czip_code"]').val(res.data.data.zipcode);
                $('#addVendorCardDetailsForm [name ="ccountry"]').val(res.data.data.country);
            }
        },
    });
}

function checkUserAccountVerification(owner_id){
    $.ajax({
        type: 'post',
        url: '/payment-ajax',
        data: {
            class: 'PaymentAjax',
            action: "getUserAccountVerification",
            user_id : owner_id
        },
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status == 'success'){
                if(response.account_status == 'Verified'){
                    var message = 'Verified';

                    $("#business_type").val(response.account_data.business_type)
                    setTimeout(function() {
                        $("#account_verification").val('verified');


                    },2000);
                    // $('.account_verification_status').addClass("verified")
                    // $(".basic-payment-detail").hide();
                    $(".basic-user-payment-detail").hide();
                    // $("#basicDiv").css("display","none");
                    // $(".basicDiv_li").css("display","none");
                    // $(".basic_div_span").css("display","none");
                    // $('.payment_li').addClass('after-hide');
                    // $(".right_tick").css("display","block");
                    setTimeout(function() {
                        if(response.user_account_detail.data.business_type == 'individual') {
                            $("#financialInfo :input").prop("disabled", true);
                            $("#savefinancial").prop("disabled", true);
                            $("#account_verification").val('verified');
                            autofillIndividualData(response);

                        }
                        if(response.user_account_detail.data.business_type == 'company'){
                            $("#financialCompanyInfo :input").prop("disabled", true);
                            $("#financialCompanyInfo").prop("disabled", true);
                            $("#account_verification").val('verified');
                            autofillCompanyData(response);
                        }
                    },2000);


                }else if(response.account_status == 'not_exists'){
                    $("#business_type").val("");
                    setTimeout(function() {
                        $("#account_verification").val('not_exists');
                    },2000);

                }else{
                    $("#business_type").val(response.account_data.business_type)
                    setTimeout(function() {
                        if(response.user_account_detail.data.business_type == 'individual') {
                            $("#account_verification").val('notverified');
                            autofillIndividualData(response);
                        }
                        if(response.user_account_detail.data.business_type == 'company'){
                            $("#account_verification").val('notverified');
                            autofillCompanyData(response);
                        }
                    },2000);

                    $('.account_verification_status').addClass("not_verified");
                    var message = 'Not Verified .  To recieve payments submit account details';
                    $("#account_verification").val('');
                    $('.unverified-icon').css("display","block");
                }
                $('.account_verification_status').html(message);


            } else {
                toastr.error(response.message);
            }
        },
        error: function (data) {

        }
    });
}

function autofillCompanyData(resposnes){

    $('#financialCompanyInfo [name ="furl"]').val(resposnes.user_account_detail.data.url);
    $('#financialCompanyInfo [name ="fbusiness"]').val(resposnes.user_account_detail.data.business_type);
    $('#financialCompanyInfo [name ="faccount_number"]').val(resposnes.user_account_detail.data.account_number);
    $('#financialCompanyInfo [name ="frouting_number"]').val(resposnes.user_account_detail.data.routing_number);
    $('#financialCompanyInfo [name ="fccity"]').val(resposnes.user_account_detail.data.company_city);
    $('#financialCompanyInfo [name ="fcaddress"]').val(resposnes.user_account_detail.data.company_address1);
    $('#financialCompanyInfo [name ="fcaddress2"]').val(resposnes.user_account_detail.data.company_address2);
    $('#financialCompanyInfo [name ="fczipcode"]').val(resposnes.user_account_detail.data.company_postal_code);
    $('#financialCompanyInfo [name ="fcstate"]').val(resposnes.user_account_detail.data.company_state);
    $('#financialCompanyInfo [name ="fcname"]').val(resposnes.user_account_detail.data.company_name);
    $('#financialCompanyInfo [name ="fcphone_number"]').val(resposnes.user_account_detail.data.company_phone_number);
    $('#financialCompanyInfo [name ="fctax_id"]').val(resposnes.user_account_detail.data.tax_id);
    $("#company_document_id").val(resposnes.user_account_detail.data.company_document_id);
    $('#financialCompanyInfo [name ="fcity"]').val(resposnes.user_account_detail.data.city);
    $('#financialCompanyInfo [name ="faddress"]').val(resposnes.user_account_detail.data.address_1);
    $('#financialCompanyInfo [name ="faddress2"]').val(resposnes.user_account_detail.data.address_2);
    $('#financialCompanyInfo [name ="fzipcode"]').val(resposnes.user_account_detail.data.postal_code);
    $('#financialCompanyInfo [name ="fstate"]').val(resposnes.user_account_detail.data.state);
    $('#financialCompanyInfo [name ="fday"]').val(resposnes.user_account_detail.data.day);
    $('#financialCompanyInfo [name ="fmonth"]').val(resposnes.user_account_detail.data.month);
    $('#financialCompanyInfo [name ="fyear"]').val(resposnes.user_account_detail.data.year);
    $('#financialCompanyInfo [name ="femail"]').val(resposnes.user_account_detail.data.email);
    $('#financialCompanyInfo [name ="ffirst_name"]').val(resposnes.user_account_detail.data.first_name);
    $('#financialCompanyInfo [name ="flast_name"]').val(resposnes.user_account_detail.data.last_name);
    $('#financialCompanyInfo [name ="fphone_number"]').val(resposnes.user_account_detail.data.phone);
    $('#financialCompanyInfo [name ="fphone_number"]').val(resposnes.user_account_detail.data.phone);
    $('#financialCompanyInfo [name ="fssn"]').val(resposnes.user_account_detail.data.ssn_last);
}

function autofillIndividualData(resposnes){
    $('#financialInfo [name ="furl"]').val(resposnes.user_account_detail.data.url);
    $('#financialInfo [name ="fbusiness"]').val(resposnes.user_account_detail.data.business_type);
    $('#financialInfo [name ="faccount_number"]').val(resposnes.user_account_detail.data.account_number);
    $('#financialInfo [name ="frouting_number"]').val(resposnes.user_account_detail.data.routing_number);
    $('#financialInfo [name ="fcity"]').val(resposnes.user_account_detail.data.city);
    $('#financialInfo [name ="faddress"]').val(resposnes.user_account_detail.data.address_1);
    $('#financialInfo [name ="faddress2"]').val(resposnes.user_account_detail.data.address_2);
    $('#financialInfo [name ="fzipcode"]').val(resposnes.user_account_detail.data.postal_code);
    $('#financialInfo [name ="fstate"]').val(resposnes.user_account_detail.data.state);
    $('#financialInfo [name ="fday"]').val(resposnes.user_account_detail.data.day);
    $('#financialInfo [name ="fmonth"]').val(resposnes.user_account_detail.data.month);
    $('#financialInfo [name ="fyear"]').val(resposnes.user_account_detail.data.year);
    $('#financialInfo [name ="femail"]').val(resposnes.user_account_detail.data.email);
    $('#financialInfo [name ="ffirst_name"]').val(resposnes.user_account_detail.data.first_name);
    $('#financialInfo [name ="flast_name"]').val(resposnes.user_account_detail.data.last_name);
    $('#financialInfo [name ="fphone_number"]').val(resposnes.user_account_detail.data.phone);
    $('#financialInfo [name ="fphone_number"]').val(resposnes.user_account_detail.data.phone);
    $('#financialInfo [name ="fssn"]').val(resposnes.user_account_detail.data.ssn_last);

}



function cardLists(owner_id)
{

    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        data: {
            "action": 'getOwnerCardsOnList',
            "class": 'Stripe',
            "owner_id":owner_id
        },
        success: function (response) {


            $(".cardDetails").html(response);


        }
    });

}





function bankLists(owner_id)
{

    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        data: {
            "action": 'getOwnerBankOnList',
            "class": 'Stripe',
            "owner_id":owner_id
        },
        success: function (response) {


            $(".accountDetails").html(response);


        }
    });

}


$(document).on("change",".cardAction",function(){
    var cardAction = $(this).val();
    var card_id = $(this).attr('data-cardId');
    var owner_id = $("#company_user_id").val();
    if(cardAction=='delete')
    {
        $(".cardAction").val('');
        bootbox.confirm("Do you want to delete this card?", function (result) {

            if (result == true) {
        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
                "action": 'deleteOwnerCardOnList',
                "class": 'Stripe',
                "card_id":card_id,
                "owner_id":owner_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);

                cardLists(owner_id);
                bankLists(owner_id);




            }
        });

            }
        });
    }
    else if(cardAction=='default') {
        $(".cardAction").val('');
        bootbox.confirm("Do you want to set this method to default?", function (result) {
            if (result == true) {
                $.ajax({
                    url: '/stripeCheckout',
                    type: 'POST',
                    data: {
                        "action": 'defaultOwnerCardOnList',
                        "class": 'Stripe',
                        "card_id": card_id,
                        "owner_id":owner_id
                    },
                    success: function (response) {
                        var response = JSON.parse(response);
                        toastr.success(response.message);

                        cardLists(owner_id);
                        bankLists(owner_id);


                    }
                });

            }
        });
    }



});
