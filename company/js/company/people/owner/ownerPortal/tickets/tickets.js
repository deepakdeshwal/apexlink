 
  

   getMaintenance();

  function getMaintenance(status) {
    var table = 'tenant_maintenance';
    //var columns = ['<input type="checkbox" id="select_all_complaint_checkbox">','Tenant Name','email','Tenant_id','Property','Unit Number','Ticket No.','Ticket Type', 'Category', 'Created Date', 'Image/Photo(s)', 'Status', 'Action'];
    var columns = ['Ticket Number','Ticket Type','Tenant Name','Property','Building','Unit Number','Category','Created Date','Estimated Cost($)','Status','Approval'];
    var select_column = ['Edit','Delete','Cancel'];
    var joins = [{table:'tenant_maintenance',column:'user_id',primary:'id',on_table:'users',as :'tp'},
    {table:'tenant_maintenance',column:'unit_id',primary:'id',on_table:'unit_details',as :'tp1'},
    {table:'tenant_maintenance',column:'property_id',primary:'id',on_table:'general_property',as :'tp3'},];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var columns_options = [
     //   {name: 'Id', index: 'id', width: 90, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatterComplaint},
       // {name:'email',index:'email', width:90,align:"left",hidden:true,searchoptions: {sopt: conditions},table:'tp'},
       // {name:'Tenant_id',index:'user_id', width:90,align:"left",hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'Ticket Number.',index:'ticket_number', width:108,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Ticket Type',index:'ticket_type', width:108,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'TenantName',index:'first_name', width:90,align:"left",searchoptions: {sopt: conditions},table:'tp'},
        {name:'Property',index:'property_name', width:150,align:"center",searchoptions: {sopt: conditions},table:'tp3'},
        {name:'building',index:'category', width:108,searchoptions: {sopt: conditions},table:table},
        {name:'Unit Number',index:'unit_no', width:150,searchoptions: {sopt: conditions},table:'tp1'},
        {name:'Category',index:'category', width:108,searchoptions: {sopt: conditions},table:table},
        {name:'Created Date',index:'created_at', width:190,align:"center",searchoptions: {sopt: conditions},table:table},
        //{name:'Image/Photo(s)',index:'ticket_type', width:108,searchoptions: {sopt: conditions},table:table,formatter:maintenanceImageFormatter},
        {name:'Estimated Cost($)',index:'created_at', width:190,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Status',index:'status', width:108,searchoptions: {sopt: conditions},table:table,formatter:getstatusFormatter},
        {name:'Approval',index:'status', width:200,searchoptions: {sopt: conditions},table:table,formatter:selectMainTenanceFormatter},
    ];
    // var columns_options = [
    //     {name: 'Ticket Number', title:false, index: 'ticket_number', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
    //     {name: 'Ticket Type', title:false, index: 'ticket_type', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
    //     {name: 'Tenant Name', title:false, index: 'first_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
    //     {name: 'Property', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
    //     {name: 'Building', title:false, index: 'unit_no', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
    //     {name: 'Unit Number', title:false, index: 'unit_no', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
    //     {name: 'Category', title:false, index: 'category', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
    //     {name: 'Created Date', title:false, index: 'created_at', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
    //     {name: 'Estimated Cost($)', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
    //     {name: 'Status', title:false, index: 'status', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
    //     {name: 'Approval', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
    // ];
    var ignore_array = [];
    jQuery("#list_bills").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'created_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Tickets",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}


function deleteTableData(tablename,id)
{

      $.ajax({
        url:'/editTenant?action=deleteRecords&class=EditTenant&id='+id+'&tablename='+tablename,
        type: 'GET',
        success: function (data) {
            var info = JSON.parse(data);
             toastr.success(info.message);

              },
    });  

}

function TenantNameFormatter(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {
        var name ='';
        if (rowObject.TenantName != '') {
        name = '<a style="color:#05A0E4 ! important;font-weight: bold"><u><strong>' + rowObject.TenantName + '</strong></u></a>';
          }
        return name;
    }
}

function propertyFormatter(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {
        var property ='';
        if (rowObject.Property != '') {
            property = '<a style="color:#05A0E4 ! important;font-weight: bold"><u><strong>' + rowObject.Property + '</strong></u></a>';
        }
        return property;
    }
}

function actionCheckboxFmatterComplaint(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {

        var data = '';
        var data = '<input type="checkbox" name="maintenance_checkbox[]" class="maintenance_checkbox" id="maintenance_checkbox' + rowObject.id + '" data_id="' + rowObject.id + '"/>';
        return data;
    }
}

$("#select_all_complaint_checkbox").click(function () {
    if($("#select_all_complaint_checkbox").is(":checked")){
    $(".maintenance_checkbox").prop('checked', true);
    }
    else{
        $(".maintenance_checkbox").prop('checked', false);
    }
});




function getstatusFormatter(cellValue, options, rowObject)
{
    if(rowObject!==undefined)
    {
        console.log(rowObject);

        var status =  rowObject.Status;
        if(status==1)
        {
            return "Open";
        }
        else
        {
             return "Cancel";
        }


    } 
}




  function maintenanceImageFormatter (cellValue, options, rowObject){

    
    if(rowObject!==undefined)
    {
        var image1 =  rowObject.image1;
        var image2 =  rowObject.image2;
        var image3 =  rowObject.image3;
        if(image3=="" && image2=="" && image1=="")
        {
            return "No Image Uploaded";
        }
        else
        {
    
       return "<a href='javascript:void(0);' data-id='"+rowObject.id+"' class='getImagesByTable' data-tablename='tenant_maintenance'>Images/Photo</a>";        }

        }


    }


    function selectMainTenanceFormatter(cellValue, options, rowObject)
{
  
   if(rowObject !== undefined) {
    var html = "<select editable='1' class='form-control select_options' data_id='"+rowObject.Id+"' data_tenant_id='"+rowObject.Tenant_id+"' data-email = "+rowObject.email+">";
  
   html +=  "<option value=''>Select</option>";
   html +=  "<option value='View'>Approval</option>";
   html +=  "<option value='View'>Cancel</option>";

     
   
 
html +="</select>";
    html

        



   return html;


      
   }
}





     $(document).on("click",".getImagesByTable",function(){
      tablename = $(this).attr('data-tablename');
      id = $(this).attr('data-id');
      
         $.ajax({
        url:'/editTenant?action=getImageByTable&class=EditTenant&id='+id+'&tablename='+tablename,
        type: 'GET',
        async:false,

        success: function (data) {
             var image = JSON.parse(data);

            setTimeout(function(){
                $('#imageModel').modal('show');
                $('#getImage1').html(image.img1);
                $('#getImage2').html(image.img2);
                $('#getImage3').html(image.img3);
            }, 400);

            
              },
  
    });  
  });



    //      $(document).on("click",'#print_email_button',function(){
    //
    //
    //
    //     favorite=[];
    //     var no_of_checked =    $('[name="maintenance_checkbox[]"]:checked').length
    //     if(no_of_checked == 0){
    //         toastr.error('Please select atleast one Complaint.');
    //         return false;
    //     }
    //     $.each($("input[name='maintenance_checkbox[]']:checked"), function(){
    //         favorite.push($(this).attr('data_id'));
    //     });
    //
    //     $.ajax({
    //         type: 'post',
    //         url: '/maintenance',
    //         data: {class: 'Maintenance', action: 'getMaintenanceData','maintenanceIds':favorite},
    //         success : function(response){
    //
    //
    //
    //
    //                 var pdfData = $(".maintenancePdfData").html(response);
    //                 var pdfData1 = $(".maintenancePdfData").html();
    //
    //               var new_html = '<div id="content" style="color:red;"><h3>Hello, this is a H3 tag</h3> <p>A paragraph</p></div>';
    //
    //                 if(pdfData1!==undefined)
    //                 {
    //                     var a = "dfsdfshgfsgfsdgfjgdgfd";
    //                         var doc = new jsPDF();
    //                         var specialElementHandlers = {
    //                         '#editor': function (element, renderer) {
    //                         return true;
    //                         }
    //                         };
    //                         doc.fromHTML(new_html, 15, 15, {
    //                         'width': 170,
    //                         'elementHandlers': specialElementHandlers
    //                         });
    //                         doc.save('sample-file.pdf');
    //
    //                 }
    //
    //         }
    //     });
    // });





    $(document).on("change",".maintenance table .select_options",function(){


var base_url=window.location.origin;

      var email = $(this).attr('data-email');
      var id = $(this).attr('data_id');
      var tenant_id = $(this).attr('data_tenant_id');  
      var action =  $(this).val();
        if(action=="create_work_order")
        {
            localStorage.setItem('redirection_module','ticket_module');
            window.location = window.origin+"/WorkOrder/AddWorkOrder?tenant_id="+tenant_id+"&id="+id;
            console.log(id);
        }
     if(action=="Edit")
     {
       window.location = window.origin+"/TenantPortal/Tenant/TenantEditTicket?tenant_id="+tenant_id+"&ticket_id="+id
     }
      else if(action=="Delete")
      {
      bootbox.confirm({
      message: "Do you want to delete this record ?",
      buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
      callback: function (result) {
         if (result == true) {
            deleteTableData('tenant_maintenance',id);
            $('#TenantMaintenance-table').trigger('reloadGrid');
        
         }
       
      }
      });



      }
      else if(action=="Cancel")
      {
      bootbox.confirm({
      message: "Are you sure you want to cancel ticket?",
      buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
      callback: function (result) {
      if (result == true) {
      changeTicketStatus(id);
      $('#TenantMaintenance-table').trigger('reloadGrid');

      }

      }
      });

      }
      else if(action=="View")
      {
      window.location = window.origin+"/tickets/ViewTickets?tenant_id="+tenant_id+"&ticket_id="+id

      }
      else if(action=="Text")
      {
     localStorage.setItem('predefined_text',email);
          localStorage.setItem('table_green_id',id);
          localStorage.setItem('table_green_tableid', '#TenantMaintenance-table');
          localStorage.setItem('table_green_url','/ticket/tickets');
          window.location.href = base_url + '/Communication/AddTextMessage';


      }
      else if(action=="Email")
      {
     localStorage.setItem('predefined_mail',email);
          localStorage.setItem('table_green_id',id);
          localStorage.setItem('table_green_tableid', '#TenantMaintenance-table');
          localStorage.setItem('table_green_url','/ticket/tickets');

     window.location.href = base_url + '/Communication/ComposeEmail';

      }


     });

   if(localStorage.getItem("table_green_tableid")){
       setTimeout(function(){
           var tableId = localStorage.getItem("table_green_tableid")

           jQuery(tableId).find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
           jQuery(tableId).find('tr:eq(1)').find('td:eq(11)').addClass("green_row_right");
           localStorage.removeItem('table_green_id');
           localStorage.removeItem('table_green_tableid');
       }, 2000);
   }


        function changeTicketStatus(id)
        {

        $.ajax({
        url:'/tenantPortal',
        type: 'POST',
        data: {
        "ticket_id": id,
        "action": 'changeTicketStatus',
        "class": 'TenantPortal'
        },
        success: function (response) {
        var info = JSON.parse(response);
        if(info.status=='success'){

        toastr.success('Ticket Canceled Successfully.');
        }


        }
        });
        }