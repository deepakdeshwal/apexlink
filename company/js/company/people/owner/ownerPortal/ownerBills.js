$(document).ready(function () {

    $(document).on('click','#searchFilter',function(){
        searchFilter();
    });


    var formData = new FormData();
    formData.append('action','ownerPropertyList');
    formData.append('class','ownerBillsAjax');
    formData.append('owner_id',owner_id);
    $.ajax({
        url:'/OwnerBills-Ajax',
        type: 'POST',
        data: formData,
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status == "success"){
                $('#owner_property').html(response.data);
            } else if (response.status == 'error'){

            }
        },
        cache: false,
        contentType: false,
        processData: false
    });

    // $('#startDate').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat ,})
    //     .datepicker("setDate" , new Date());

    // $('#endDate').datepicker({yearRange: '-0:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat, minDate:  new Date(),})
    //     .datepicker("setDate" , new Date());

    $('#startDate').datepicker({
        yearRange: '-2:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        onSelect: function (selectedDate) {
            $("#endDate").datepicker("option", "minDate", selectedDate)
        }
    });

    $('#endDate').datepicker({
        yearRange: '-2:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        onSelect: function (selectedDate) {
            $("#startDate").datepicker("option", "maxDate", selectedDate);
        }
    });



// <--Custom 11-3-2020
    var  start_date = '';
    var  end_date = '';
    $(document).on('change', '#filter_by',function () {
        filterBy();
    });

    function filterBy(){
        var val = $("#filter_by").val();
        var date = new Date(), y = date.getFullYear(), m = date.getMonth();

        switch(val){
            case("1"):
                $("#startDate" ).val('').prop("disabled", true);
                $("#endDate" ).val('').prop("disabled", true);
                start_date =$.datepicker.formatDate(jsDateFomat, new Date(y, m-3, 1));
                end_date =$.datepicker.formatDate(jsDateFomat, new Date(y, m-1, 1));
                break;
            case("2"):
                $("#startDate" ).val('').prop("disabled", true);
                $("#endDate" ).val('').prop("disabled", true);
                start_date =$.datepicker.formatDate(jsDateFomat, new Date(y, m-1, 1));
                end_date = $.datepicker.formatDate(jsDateFomat, new Date(y, m , 0));
                break;
            case ("3"):
                $("#startDate" ).val('').prop("disabled", true);
                $("#endDate" ).val('').prop("disabled", true);
                var last_week_start = date.getDate() - date.getDay()-6;
                start_date =$.datepicker.formatDate(jsDateFomat, new Date(date.setDate(last_week_start)));
                var last_week_end = date.getDate() - date.getDay()+7;
                end_date =  $.datepicker.formatDate(jsDateFomat, new Date(date.setDate(last_week_end)));
                break;
            case("4"):
                $("#startDate" ).prop("disabled", false);
                $("#endDate" ).val('').prop("disabled", true);
                start_date = $("#startDate").val();
                end_date = $("#endDate").val();
                break;
            case("5"):
                $("#startDate" ).prop("disabled", false);
                $("#endDate" ).prop("disabled", false);
                start_date =  $("#startDate").val();
                end_date =   $("#endDate").val();
            default:
        }
    }


    $(document).on('change','.common_search_class',function(){
        searchFilter();
    });

    function searchFilter(){
        var grid = $("#list_bills"),f = [];
        var property_in = [];
        if($('#owner_property').val() == ''){
            $("#owner_property option").each(function(key,value) {
                if($(value).val() != '') {
                    property_in.push($(value).val());
                }
            });
        } else {
            property_in.push($('#owner_property').val());
        }
        var status   =  $('#status').val() == ''?'all':$('#status').val();
        f.push(
            {field: "general_property.id", op: "in", data: property_in},
            {field: "company_bills.status", op: "eq", data: status}
        );
        if($('#filter_by').val() != '' && $('#filter_by').val() < 4) {
            f.push({field: "company_bills.created_at", op: "dateBetween", data: start_date, data2: end_date});
        }
        if($('#filter_by').val() == '4' && $('#startDate').val() != ''){
            f.push({field: "company_bills.created_at", op: "date", data: $('#startDate').val()});
        }
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    ownerBillsListing(' ');
    function ownerBillsListing(status) {
        var table = 'company_bills';
        var columns = ['Bill #','Bill Date','Property','Status','Due Date','Payment Date','Amount('+default_currency_symbol+')','Amount Paid('+default_currency_symbol+')','Vendor'];
        var joins = [{table:'company_bills',column:'property_id',primary:'id',on_table:'general_property'},{table:'company_bills',column:'vendor_id',primary:'id',on_table:'users'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['owner_property_owned.updated_at', 'general_property.property_name','owner_property_owned.property_percent_owned','general_property.deleted_at', 'general_property.address1', 'general_property.address2', 'general_property.address3', 'general_property.address4', 'general_property.manager_id', 'general_property.owner_id','owner_property_owned.updated_at'];
        var columns_options = [
            {name: 'Bill #', title:false, index: 'refrence_number', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table},
            {name: 'Bill Date', title:false, index: 'created_at', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table},
            {name: 'Property', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: 'general_property'},
            {name: 'Status', title:false, index: 'status', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: statusFormatter},
            {name: 'Due Date', title:false, index: 'due_date', width: 90, align: "left", change_type : 'date', searchoptions: {sopt: conditions}, table: table},
            {name: 'Payment Date', title:false, index: 'user_type', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table},
            {name: 'Amount($)', title:false, index: 'amount', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table},
            {name: 'Amount Paid($)', title:false, index: 'user_type', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table},
            {name: 'Vendor', title:false, index: 'name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: 'users', formatter: vendorName, classes: 'pointer'},
        ];
        var ignore_array = [];
        jQuery("#list_bills").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                //extra_columns: extra_columns,
                //extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'company_bills.created_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Bills",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid", {
            edit: false,
            add: false,
            del: false,
            search: true,
            reloadGridOptions: {
                fromServer: true
            }
        }, {
            top: 200,
            left: 200,
            drag: true,
            resize: false
        });
    }

    /**
     *
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function vendorName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }

    }

    /**
     *
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            if (cellValue == '0')
                return "Due";
            else if (cellValue == '1')
                return "Paid";
            else if (cellValue == '2')
                return "Overdue";
            else if (cellValue == '3')
                return "OnHold";
            else
                return '';
        }
    }

});