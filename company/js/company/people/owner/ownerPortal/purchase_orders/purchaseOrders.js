$(document).ready(function () {
    $("#maintenance_top").addClass("active");
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    var rString = randomString(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    $("input[name='po_number']").val("PO:"+rString);
    $("input[name='current_date'],input[name='required_by']").val(date);

    $('input[name="required_by"]').datepicker({
        dateFormat: jsDateFomat,
        changeMonth: true,
        changeYear: true,
        yearRange: "1919:2050"
    });

    $(document).on("change","input[name='owner_approve']",function(){
        if($(this).prop("checked") == true){
            $(".owners_block").show();
        }else{
            $(".owners_block").hide();
        }
    });
    $(document).on("change","input[name='tenant_approve']",function(){
        if($(this).prop("checked") == true){
            $(".tenants_block").show();
        }else{
            $(".tenants_block").hide();
        }
    });

    $(document).on("click", ".add_more_list", function(){
        var clone = $("#amnt_table tbody tr:first").clone();
        clone.find('input[name="qty_number[]"]').val('1');
        clone.find('select[name="gl_account[]"]').val('0');
        clone.find('input[name="description[]"]').val('');
        clone.find('input[name="item_amount[]"]').val('0.00');
        clone.find('input[name="total_amount[]"]').val('0.00');
        $("#amnt_table tbody tr").last().after(clone);
        clone.find(".remove_clone .fa-minus-circle").show();
    });

    $(document).on("click","#amnt_table tbody tr .remove_clone",function(){
        var totalCellAmnt = $(this).parent().prev().find("input").val();
        var totalAmt = $(".totalamt").text();
        var subtractAmnt = parseInt(totalAmt) - parseInt(totalCellAmnt);
        subtractAmnt = amountFormatter(subtractAmnt);
        $(".totalamt").text(subtractAmnt);
        $(this).parent().parent().remove();
    });

    $(document).on("blur", "input[name='qty_number[]']", function () {
        var getCurrentValue = parseInt($(this).val());
        var itemAmount = $(this).parent().next().next().next().find("input[name='item_amount[]']").val();
        var totalCellAmnt = 0.00;
        if (itemAmount != "0.00" && itemAmount != ""){
            totalCellAmnt = parseInt(itemAmount) * getCurrentValue;
            var formatterAmnt = amountFormatter(totalCellAmnt);
            $(this).parent().next().next().next().next().find("input[name='total_amount[]']").val(formatterAmnt);
            var getTotalAmnt = getAllTotalAmt();
            $(".totalamt").text(getTotalAmnt);
        }
    });

    $(document).on("blur", "input[name='item_amount[]']", function () {
        var getCurrentValue = parseInt($(this).val());
        var itemQty = $(this).parent().prev().prev().prev().find("input[name='qty_number[]']").val();
        var totalCellAmnt = 0.00;
        if (itemQty != "0"){
            totalCellAmnt = parseInt(itemQty) * getCurrentValue;
            var formatterAmnt = amountFormatter(totalCellAmnt);
            $(this).parent().next().find("input[name='total_amount[]']").val(formatterAmnt);
            var getTotalAmnt = getAllTotalAmt();
            $(".totalamt").text(getTotalAmnt);
        }
    });

    $(document).on("change",".add_mainenance_section select[name='property']",function (e) {
        e.preventDefault();
        getBuildingsByPropertyID($(this).val());
        return false;
    });

    $(document).on("change",".add_mainenance_section select[name='building']",function (e) {
        e.preventDefault();
        getUnitsByBuildingsID($(".add_mainenance_section select[name='property']").val() , $(this).val());
        return false;
    });

    $(document).on("click","input[name='vendor']", function(){
        getVendorList();
    });

    $('body').click(function(evt){
        if(evt.target.nodeName == "TD" || evt.target.id == "vendor")
            return;
    });

    $(document).on("click","#vendorlist tbody tr td", function(){
        var parentTr = $(this).parents("tr");
        var id = parentTr.find("td:first-child").text();
        var name = parentTr.find("td:nth-child(2)").text();
        var phone_number = parentTr.find("td:nth-child(6)").text();
        var email = parentTr.find("td:nth-child(4)").text();
        var category = parentTr.find("td:nth-child(5)").text();
        var address = parentTr.find(".textareaadd").text();

        var tableRow = "<tr>";
        tableRow += "<td style='display: none'>"+id+"</td>";
        tableRow += "<td>"+name+"</td>";
        tableRow += "<td>"+category+"</td>";
        tableRow += "<td>"+email+"</td>";
        tableRow += "<td>"+phone_number+"</td>";
        tableRow += "<td style='display: none'>"+address+"</td>";
        tableRow += "</tr>";

        $("input[name='vendor']").val(name);
        $("input[name='vendorid']").val(id);
        $(".vendor_selected_table tbody").html(tableRow);
        $('.vendor_list_container').removeClass("in");
        $("textarea[name='address']").text(address);
    });

    $(document).on("click", ".save_purchase", function (){
        savePurchaseOrder();
        setTimeout(function(){
            localStorage.setItem("Message", 'Record saved successfully');
            localStorage.setItem("rowcolorPurchaseOrder",'green');
            onTop(true,'purchasetable');
            window.location.href = window.location.origin+"/PurchaseOrder/PurchaseOrderListing";
            return false;
        },3000);
    });

    $(document).on("click", ".save_add_purchase", function (){
        savePurchaseOrder();
        toastr.success("Record saved successfully");
        $("#purchaseOrderForm").trigger('reset');
        var rString = randomString(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
        $("input[name='po_number']").val("PO:"+rString);
        var date = $.datepicker.formatDate(jsDateFomat, new Date());
        $("input[name='current_date'],input[name='required_by']").val(date);
        $(".owners_block, .tenants_block").hide();
        $("textarea[name='address']").text('');
        $("textarea[name='address']").val('');
        $(".vendor_selected_table tbody").html('');
        $("#amnt_table tbody ").find("tr:gt(0)").remove();
        $(".totalamt").text(0.00);
        $("#remove_library_file").trigger('click');
        return false;
    });

    $(document).on("click", ".cancel_purchase", function (){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = window.location.origin + "/PurchaseOrder/PurchaseOrderListing";
                return false;
            }
        });
    });

    $(document).on('click','#add_libraray_file',function(){
        $('#file_library').val('');
        $('#file_library').trigger('click');
    });

    $(document).on('change','#file_library',function(){
        var file_library = [];
        $.each(this.files, function (key, value) {
            var type = value['type'];
            var size = isa_convert_bytes_to_specified(value['size'], 'k');
            if(size > 1030) {
                toastr.warning('Please select documents less than 1 mb!');
            } else {

                size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
                if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                    file_library.push(value);
                    var src = '';
                    var reader = new FileReader();
                    $('#file_library_uploads').html('');
                    reader.onload = function (e) {
                        var upload_url = window.location.origin+"/";
                        if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                            src = upload_url + 'company/images/excel.png';
                        } else if (type == 'application/pdf') {
                            src = upload_url + 'company/images/pdf.png';
                        } else if (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                            src = upload_url + 'company/images/word_doc_icon.jpg';
                        } else if (type == 'text/plain') {
                            src = upload_url + 'company/images/notepad.jpg';
                        } else if (type == 'text/xml') {
                            src = upload_url + 'company/images/notepad.jpg';
                        } else {
                            src = e.target.result;
                        }
                        $("#file_library_uploads").append(
                            '<div class="row" style="margin:20px">' +
                                '<div class="col-sm-12 img-upload-library-div">' +
                                    '<div class="col-sm-3">' +
                                        '<img class="img-upload-tab' + key + '" width=100 height=100 src=' + src + '>' +
                                    '</div>' +
                                    '<div class="col-sm-3 show-library-list-imgs-name' + key + '">' + value['name'] + '</div>' +
                                    '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                                    '<div class="col-sm-3 show-library-list-imgs-size' + key + '">' + size + '</div>' +
                                    '<div class="col-sm-3">' +
                                        '<span id=' + key + ' class="delete_pro_img cursor">' +
                                            '<button class="btn-warning">Delete</button>' +
                                        '</span>' +
                                    '</div>' +
                                '</div>' +
                            '</div>');
                    };
                    reader.readAsDataURL(value);
                } else {
                    toastr.warning('Please select file with .xlsx | .pdf | .docx | .txt | .xml extension only!');
                }
            }
        });
    });

    $('#saveLibraryFiles').on('click',function(){
        var length = $('#file_library_uploads > div').length;
        if(length > 0) {
            var data = convertSerializeDatatoArray();
            var uploadform = new FormData();
            uploadform.append('class', 'propertyFilelibrary');
            uploadform.append('action', 'file_library');
            uploadform.append('property_id', property_unique_id);
            var count = file_library.length;
            $.each(file_library, function (key, value) {
                if(compareArray(value,data) == 'true'){
                    uploadform.append(key, value);
                }
                if(key+1 === count){
                    saveLibraryFiles(uploadform);
                }
            });
        } else {

        }
    });

    $(document).on('click','.delete_pro_img',function(){
        $(this).parent().parent().parent('.row').remove();
    });

    $(document).on('click','#remove_library_file',function(){
        $('#file_library_uploads').html('');
        $('#file_library').val('');
    });

    function saveLibraryFiles(uploadform){
        $.ajax({
            type: 'post',
            url: '/property/file_library',
            data:uploadform,
            processData: false,
            contentType: false,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.code == 200){
                    $('#file_library_uploads').html('');
                    $('#propertFileLibrary-table').trigger('reloadGrid');
                    toastr.success('Files uploaded successfully.');
                } else if(response.code == 500){
                    toastr.warning(response.message);
                } else {
                    toastr.success('Error while uploading files.');
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function convertSerializeDatatoArray(){
        var newData = [];
        $(".fileLibraryInput").each(function( index ) {
            var name = $(this).val();
            var size = $(this).attr('data_id');
            newData.push({'name':name,'size':size});
        });
        return newData;
    }

    function compareArray(data,compare){
        for(var i =0;i < compare.length;i++){
            if(compare[i].name == data['name'] && compare[i].size == data['size']){
                return 'true';
            }
        }
        return 'false';
    }

    function isa_convert_bytes_to_specified(bytes, to) {
        var formulas =[];
        formulas['k']= (bytes / 1024).toFixed(1);
        formulas['M']= (bytes / 1048576).toFixed(1);
        formulas['G']= (bytes / 1073741824).toFixed(1);
        return formulas[to];
    }

    $(document).on("click",".saveChargeFile",function(){
        var tenant_id = getParameters('tenant_id');
        var form = $('#addChargeNote')[0];
        var formData = new FormData(form);
        formData.append('action','insertChargeFileNote');
        formData.append('class','tenantAjax');
        formData.append('tenant_id',tenant_id);

        $.each($("input[name='file_library[]']"), function(i, obj) {
            $.each(obj.files,function(j, file){
                formData.append('photo['+j+']', file);
            })
        });


        $.ajax({
            url:'/tenantAjax',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == "success"){
                    toastr.success(response.message);
                    setTimeout(function(){
                        $('#TenantFiles-table').trigger('reloadGrid');
                        $('#remove_library_file').trigger('click');
                    }, 400);

                }
            },
            cache: false,
            contentType: false,
            processData: false
        });

    });

    function deleteTableData(tablename,id){
        $.ajax({
            url:'/editTenant?action=deleteRecords&class=EditTenant&id='+id+'&tablename='+tablename,
            type: 'GET',
            success: function (data) {
                var info = JSON.parse(data);
                toastr.success(info.message);
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function isEmpty(mainArr,removeKeyArr) {
        for(var i = 0; i < mainArr.length; i++){
            if(mainArr[i].key == "key1"){
            }
        }
    }

    getInitialData();
    purchaseOrderList('1');

    $(document).on("click","#select_all_purchaseorder_checkbox",function () {
        $(".purchaseorder_checkbox").prop('checked', $(this).prop('checked'));
    });

    $(document).on("change", "#list_bills .select_options", function (e) {
        // alert("blbl");
        e.preventDefault();
        var base_url = window.location.origin;
        var action = this.value;
        var id = $(this).attr('data_id');
        switch (action) {

            // case "Create Bill":
            //     window.location.href = base_url + '/OwnerPortal/NewBill?PurchaseOrderId='+id;
            //     break;

            case "View":
                //changeStatusAjax(id,'purchaseOrder','status','3');
                // alert("Chaa ge");
                window.location.href = base_url + '/Owner/MyAccount/OwnerPurchaseOrderView?PurchaseOrderId='+id;
                break;


            case "Approve":
                // alert("approve");
                    changeStatusAjax(id,'purchaseOrder','status','3');
                    break;

            case "Reject":
                // alert("Reject");
                        changeStatusAjax(id,'purchaseOrder','status','3');
                        break;
            default:
                window.location.href = base_url + '/Setting/ApexNewUser';
        }
    });

    $(document).on("click",".downloadpdf", function () {
        var sList = [];
        $('.purchaseorder_checkbox').each(function () {
            if (this.checked ){
                var checkboxId = $(this).attr("data_id");
                sList.push(checkboxId);
            }
        });
        if (sList.length !== 0){
            getHtmlPdfConverter(sList,'pdf');
        }else{
            toastr.warning("Please select atleast one PurchaseOrder.");
        }
    });

    $(document).on("click",".downloadexcel", function () {
        var sList = [];
        $('.purchaseorder_checkbox').each(function () {
            if (this.checked ){
                var checkboxId = $(this).attr("data_id");
                sList.push(checkboxId);
            }
        });
        if (sList.length !== 0){
            getTableHtml(sList,'excel');
        }else{
            toastr.warning("Please select atleast one PurchaseOrder.");
        }
        //exportTableToExcel("")
    });

    $(document).on('click','#purchasetable tr td:not(:last-child):not(:first-child)',function(e){
        e.preventDefault();
        var base_url = window.location.origin;
        var id = $(this).closest("tr").attr('id');
        window.location.href = base_url + '/PurchaseOrder/PurchaseOrderView?PurchaseOrderId='+id;
    });

    viewPurchaseOrderListFiles();

    var purchaseOrderId = getParameters('PurchaseOrderId');
    if (purchaseOrderId != ""){
        loadViewPage(purchaseOrderId);
    }

    $(document).on('click','.sectionOneEdit',function(e){
        var base_url = window.location.origin;
        var purchaseOrderId = getParameters('PurchaseOrderId');
        window.location.href = base_url + '/PurchaseOrder/EditPurchaseOrder?PurchaseOrderId='+purchaseOrderId;
    });



});

function loadViewPage(purchaseOrderId) {
    $.ajax({
        url: '/PurchaseOrder/ajaxData',
        type: 'POST',
        data: {
            class: "PurchaseOrder",
            action: "getViewPageInformation",
            id: purchaseOrderId
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                var purchaseOrder = response.data.purchaseOrderData;
                var orderDetails = response.data.orderDetails;
                var vendorRecord = response.data.vendorRecord;
                var totalPrice = response.data.totalPrice;

                $(".view_mainenance_section .po_number").text(purchaseOrder.po_number);
                $(".view_mainenance_section .required_by").text(purchaseOrder.required_by);
                $(".view_mainenance_section .currentdate").text(purchaseOrder.currentdate);
                $(".view_mainenance_section .status").text(purchaseOrder.status);
                $(".view_mainenance_section textarea[name='vendor_instruction']").text(purchaseOrder.vendor_instruction).attr("disabled",true);
                $(".view_mainenance_section textarea[name='vendor_instruction']").val(purchaseOrder.vendor_instruction).attr("disabled",true);

                var trRow = "<tr><td>"+purchaseOrder.property+"</td><td>"+purchaseOrder.building+"</td><td>"+purchaseOrder.unit+"</td></tr>";
                $("#property_table tbody").html(trRow);

                var orderDetailsHtml = "";
                for(var i = 0; i < orderDetails.length; i++){
                    orderDetailsHtml +="<tr>";
                    orderDetailsHtml +=     '<td><input disabled class="form-control" type="text" name="qty_number[]" value="'+orderDetails[i].qty_number+'"></td>';
                    orderDetailsHtml +=     '<td><select disabled class="form-control" name="gl_account[]"><option value="'+orderDetails[i].gl_account+'" selected>'+orderDetails[i].gl_account+'</option></select></td>';
                    orderDetailsHtml +=     '<td><input disabled class="form-control" type="text" name="description[]" value="'+orderDetails[i].description+'"></td>';
                    orderDetailsHtml +=     '<td><input disabled class="form-control" type="text" name="item_amount[]" value="'+amountFormatter(orderDetails[i].item_amount)+'"></td>';
                    orderDetailsHtml +=     '<td><input disabled class="form-control" type="text" name="total_amount[]" value="'+amountFormatter(orderDetails[i].item_total)+'"></td>';
                    orderDetailsHtml +="</tr>";
                }
                $("#amnt_table tbody").html(orderDetailsHtml);

                $(".totalamt").text(amountFormatter(totalPrice));

                var vendorHtml = "<tr>";
                vendorHtml    +=    "<td>"+vendorRecord.name+"</td>";
                vendorHtml    +=    "<td>"+vendorRecord.vendor_type+"</td>";
                vendorHtml    +=    "<td>"+vendorRecord.email+"</td>";
                vendorHtml    +=    "<td>"+vendorRecord.phone_number+"</td>";
                vendorHtml    += "</tr>";
                $("#vendor_table tbody").html(vendorHtml);

            } else if(response.code == 500) {
                //toastr.warning(response.message);
            } else {
                //toastr.error(response.message);
            }

        }
    });

}

function viewPurchaseOrderListFiles() {

    var tenantId = $(".tenant_id").val();

    var table = 'maintenance_files';
    var columns = ['Image Name','Preview'];
    var select_column = [];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [];
    var extra_columns = [];
    var columns_options = [
        {name:'Image Name',index:'filename', width:170,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'file_location',align:"center", width:170,searchoptions: {sopt: conditions},table:table,formatter:file_image}
    ];
    var ignore_array = [];
    jQuery("#viewpurchaseordertable").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: 'All',
            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:true
        },
        viewrecords: true,
        sortname: 'maintenance_files.updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Files",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}

function file_image (cellValue, options, rowObject){
    if (cellValue != undefined){
        var explodeUrl = cellValue.split('.');
        var arrayReverse = explodeUrl.reverse()[0];
        var upload_url = window.location.origin+"/company/";
        var filePath = window.location.origin+"/company/"+cellValue;
        if (arrayReverse == 'xlsx') {
            src = upload_url + 'images/excel.png';
        } else if (arrayReverse== 'pdf') {
            src = upload_url + 'images/pdf.png';
        } else if (arrayReverse == 'docx') {
            src = upload_url + 'images/word_doc_icon.jpg';
        } else if (arrayReverse == 'txt') {
            src = upload_url + 'images/notepad.jpg';
        } else if (arrayReverse == 'txt') {
            src = upload_url + 'images/notepad.jpg';
        } else {
            src = upload_url + 'images/notepad.jpg';
        }
        return '<a href ="'+filePath+'" download target="_blank"><img src="'+src+'" style="width: 30px;"></a>';
    }
}

function getTableHtml(dataArray,datatypes){
    $.ajax({
        url: '/PurchaseOrder/ajaxData',
        type: 'POST',
        data: {
            class: "PurchaseOrder",
            action: "getExcelContent",
            dataArray: dataArray,
            type: datatypes
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                var htmls = response.data;
                //purchaseorderexcel
                $("body").append(htmls);
                exportTableToExcel("purchaseorderexcel");
            } else if(response.code == 500) {
                //toastr.warning(response.message);
            } else {
                //toastr.error(response.message);
            }

        }
    });
}

function getHtmlPdfConverter(dataArray,datatypes){
    $.ajax({
        url: '/PurchaseOrder/ajaxData',
        type: 'POST',
        data: {
            class: "PurchaseOrder",
            action: "getPdfContent",
            dataArray: dataArray,
            type: datatypes
        },
        success: function (response) {
            var response = JSON.parse(response);

            if (response.status == 'success' && response.code == 200) {
                var link=document.createElement('a');
                document.body.appendChild(link);
                link.target="_blank";
                link.download="PurchaseOrder.pdf";
                link.href=response.data.record;
                link.click();
            } else if(response.code == 500) {
                //toastr.warning(response.message);
            } else {
                //toastr.error(response.message);
            }

        }
    });
}

function deleteAjax(id,table,col){
    $.ajax({
        url: '/PurchaseOrder/ajaxData',
        type: 'POST',
        data: {
            class: "PurchaseOrder",
            action: "deleteFile",
            id: id,
            table: table,
            col: col
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == "success") {
                toastr.success(response.message);
                $('#purchasetable').trigger('reloadGrid');
            }
        }
    });
}

function changeStatusAjax(id,table,col,val){
    $.ajax({
        url: '/PurchaseOrder/ajaxData',
        type: 'POST',
        data: {
            class: "PurchaseOrder",
            action: "changeStatusAjax",
            id: id,
            table: table,
            col: col,
            val: val
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == "success") {
                toastr.success(response.message);
                $('#purchasetable').trigger('reloadGrid');
                onTop(true,'purchasetable');
            }
        }
    });
}

function actionCheckboxFmatter(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {
        return '<input type="checkbox" name="purchaseorder_checkbox[]" class="purchaseorder_checkbox" id="purchaseorder_checkbox' + rowObject.id + '" data_id="' + rowObject.id + '"/>';
    }
}

function purchaseOrderList(status) {
// alert(vendor_portal_id);
// alert("knknlk");
    var tenantId = $(".tenant_id").val();
    var table = 'purchaseOrder';
    //var columns = ['<input type="checkbox" id="select_all_purchaseorder_checkbox">','PO Number','Property','Building','Unit','Unit_no','Required By','Vendor','Work Orders','Approvers','Invoice Number','PO Status','Created By','Created Date','Action'];
    var columns = ['PO Number','Property','Building','Unit','Require By Date', 'Vendor','Work Orders','Approvers','Invoice Number','PO Status','Created By', 'Created Date','Action'];
    var select_column = [];
    var joins = [
        {table:'purchaseOrder',column:'property',primary:'id',on_table:'general_property'},
        {table:'purchaseOrder',column:'building',primary:'id',on_table:'building_detail'},
        {table:'purchaseOrder',column:'unit',primary:'id',on_table:'unit_details'},
        {table:'purchaseOrder',column:'owners',primary:'id',on_table:'users'}
    ];
    var conditions = ["eq","bw","ew","cn","in"];
   var extra_where = [{column:'owners',value:owner_id,condition:'=',table:'purchaseOrder'}];
    // var extra_where = [];
    var extra_columns = [];
    var columns_options = [
       // {name: 'Id', index: 'id', width: 170, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatter},
        {name:'PO Number',index:'po_number', width:170,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Property',index:'property_name',align:"center", width:170,searchoptions: {sopt: conditions},table:'general_property'},
        {name:'Building',index:'building_name',align:"center", width:170,searchoptions: {sopt: conditions},table:'building_detail'},
        {name:'Unit',index:'unit_prefix', width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
      //  {name:'Unit_no',index:'unit_no',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details'},
        {name:'Required By Date',index:'required_by', width:170,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Vendor',index:'vendor', width:170,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Work Orders',index:'work_order', width:170,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Approvers',index:'name', width:170, align:"center",searchoptions: {sopt: conditions},table:'users'},
        {name:'Invoice Number',index:'invoice_number', width:170,searchoptions: {sopt: conditions},table:table},
        {name:'PO Status',index:'status', width:170,searchoptions: {sopt: conditions},table:table, formatter:statusFormatter},
        {name:'Created By',index:'name', width:170,searchoptions: {sopt: conditions},table:'users'},
        {name:'Created Date',index:'currentdate', width:170,searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Action',index:'', title: false, width:135,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFmatter1}
    ];
    
    var ignore_array = [];
    jQuery("#list_bills").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,

            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:true
        },
        viewrecords: true,
        sortname: 'purchaseOrder.updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Purchase Orders",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}

function actionFmatter1(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {

        var select = '';

        select = ['View' , 'Approve','Reject',];


        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '" data-user_id="' + rowObject.user_id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}

function statusFormatter (cellValue, options, rowObject){
    if (cellValue != undefined){
        if (cellValue == 0)
            return "Rejected";
        else if(cellValue == '1')
            return "Created";
        else if(cellValue == '2')
            return "Approved";
        else
            return 'Completed';
    }
}

function getParameters(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.search);
    return (results !== null) ? results[1] || 0 : false;
}

function savePurchaseOrder() {
    var tenant_id = getParameters('tenant_id');
    var form = $('#purchaseOrderForm')[0];
    var formData = new FormData(form);
    formData.append('action','savePurchaseOrder');
    formData.append('class','PurchaseOrder');
    formData.append('tenant_id',tenant_id);
    var custom_field = [];
    $(".custom_field_html input").each(function(){
        var data = {'name':$(this).attr('name'),'value':$(this).val(),'id':$(this).attr('data_id'),'is_required':$(this).attr('data_required'),'data_type':$(this).attr('data_type'),'default_value':$(this).attr('data_value')};
        custom_field.push(data);
    });
    var a =  JSON.stringify(custom_field);
    formData.append('custom_field',a);

    $.ajax({
        url:'/PurchaseOrder/ajaxData',
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status == "success"){
            }
        }
    });
}

function getVendorList(){
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {
            class: "addVendor",
            action: "getAllVendors"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                if (data.data.length > 0){
                    var tableRow = '';
                    $.each(data.data, function (key, value) {
                        tableRow += "<tr style='cursor: pointer;'>";
                        tableRow +=     "<td style='display: none;'>"+value.id+"</td>";
                        tableRow +=     "<td>"+value.name+"</td>";
                        var address = "";
                        if (value.address1 != ""){
                            address += value.address1;
                        }
                        if (value.address2 != ""){
                            address += " ,"+value.address2;
                        }
                        if (value.address3 != ""){
                            address += " ,"+value.address3;
                        }
                        if (value.address4 != ""){
                            address += " ,"+value.address4;
                        }
                        tableRow +=     "<td>"+address+"</td>";
                        tableRow +=     "<td>"+value.email+"</td>";
                        tableRow +=     "<td>"+value.vendor_type+"</td>";
                        tableRow +=     "<td>"+value.phone_number+"</td>";
                        var salutation = getSalutation(value.salutation);
                        var textareaAddress = salutation+" "+value.name+' '+"<br>";
                            textareaAddress += address+' '+"<br>";
                            textareaAddress += value.city+ ' '+"<br>";
                            textareaAddress += value.state+", "+value.zipcode+"<br>";
                        tableRow +=     "<td class='textareaadd' style='display: none;'>"+textareaAddress+"</td>";
                        tableRow += "</tr>";
                    });

                    $('#vendorlist tbody').html(tableRow);
                    $('.vendor_list_container').addClass("in");
                }
            } else if (data.status == "error") {
                //toastr.error(data.message);
            } else {
                //toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function getSalutation(salutation){
    var salutation_value = '';
    if(salutation == "1"){
        salutation_value = "Dr.";
    }else if(salutation == "2"){
        salutation_value = "Mr.";
    }else if(salutation == "3"){
        salutation_value = "Mrs.";
    }else if(salutation == "4"){
        salutation_value = "Mr. & Mrs.";
    }else if(salutation == "5"){
        salutation_value = "Ms.";
    }else if(salutation == "6"){
        salutation_value = "Sir";
    }else if(salutation == "7"){
        salutation_value = "Madam";
    }
    return  salutation_value;
}

function getInitialData(){
    $.ajax({
        type: 'post',
        url: '/PurchaseOrder/ajaxData',
        data: {
            class: "PurchaseOrder",
            action: "getIntialData"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if (data.data.charts.length > 0){
                    var chartsOption = "<option value='0'>Select</option>";
                    $.each(data.data.charts, function (key, value) {
                        var fullName = value.account_code+"-"+value.account_name;
                        chartsOption += "<option value='"+value.id+"'>"+fullName+"</option>";
                    });
                    $('select[name="gl_account[]"]').html(chartsOption);
                }

                if (data.data.property.length > 0){
                    var propertyOption = "<option value='0'>Select</option>";
                    $.each(data.data.property, function (key, value) {
                        propertyOption += "<option value='"+value.id+"'>"+value.property_name+"</option>";
                    });
                    $('select[name="property"]').html(propertyOption);
                }

                if (data.data.tenants.length > 0){
                    var tenantOption = "<option value='0'>Select</option>";
                    $.each(data.data.tenants, function (key, value) {
                        tenantOption += "<option value='"+value.id+"'>"+value.name+"</option>";
                    });
                    $('select[name="tenants"]').html(tenantOption);
                }

                if (data.data.owners.length > 0){
                    var ownerOption = "<option value='0'>Select</option>";
                    $.each(data.data.owners, function (key, value) {
                        ownerOption += "<option value='"+value.id+"'>"+value.name+"</option>";
                    });
                    $('select[name="owners"]').html(ownerOption);
                }

                if (data.data.created_by != ''){
                    $('select[name="created_by"]').html(data.data.created_by);
                }


            } else if (data.status == "error") {
                //toastr.error(data.message);
            } else {
                //toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function getAllTotalAmt(){
    var total = 0;
    $("input[name='total_amount[]']").each(function(){
        total += parseInt($(this).val());
    });
    return amountFormatter(total);
}

function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

function amountFormatter(cellValue){
    if(cellValue!==undefined ){
        return cellValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+".00";
    } else {
        return "";
    }
}

function getBuildingsByPropertyID(propertyID){
    if(propertyID == ""){
        propertyID = 0;
    }
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getBuildingByPropertyId',
        data: {
            class: "TenantAjax",
            action: "getBuilding",
            propertyID: propertyID
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                if (data.data.length > 0){
                    var buildingOption = "<option value='0'>Select</option>";
                    $.each(data.data, function (key, value) {
                        buildingOption += "<option value='"+value.id+"' data-id='"+value.building_id+"'>"+value.building_name+"</option>";
                    });
                    $('.add_mainenance_section select[name="building"]').html(buildingOption);
                }else{
                    var buildingOption = "<option value='0'>Select</option>";
                    $('.add_mainenance_section select[name="building"]').html(buildingOption);
                    $('.add_mainenance_section select[name="unit"]').html(buildingOption);
                }
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function getUnitsByBuildingsID(propertyID, buildingID){
    $.ajax({
        url: '/PurchaseOrder/ajaxData',
        type: 'POST',
        data: {
            class: "PurchaseOrder",
            action: "getUnits",
            propertyID: propertyID,
            buildingID: buildingID
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                if (data.data.length > 0){
                    var unitOption = "<option value=''>Select</option>";
                    $.each(data.data, function (key, value) {
                        var uniName = "";
                        if (value.unit_prefix != "" && value.unit_no != "") {
                            uniName = value.unit_prefix+"-"+value.unit_no;
                        }else if(value.unit_prefix == "" && value.unit_no != ""){
                            uniName = value.unit_no;
                        }else if(value.unit_prefix != "" && value.unit_no == ""){
                            uniName = value.unit_prefix;
                        }else{
                            uniName = "";
                        }
                        unitOption += "<option value='"+value.id+"' data-id='"+value.unit_prefix+"'>"+uniName+"</option>";
                    });
                    $('.add_mainenance_section select[name="unit"]').html(unitOption);
                }else{
                    var unitOption = "<option value=''>Select</option>";
                    $('.add_mainenance_section select[name="unit"]').html(unitOption);
                }
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function exportTableToExcel(tableID){
    var downloadLink;
    var filename = "PurchaseOrder";
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';

    // Create download link element
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

        // Setting the file name
        downloadLink.download = filename;

        //triggering the function
        downloadLink.click();
    }
}