$(document).on("change",".setType",function(){
    var value = $(this).val();
    if(value=='1')
    {

        $(".accounts").hide();
        $(".cards").show();

    }
    else
    {
        var validator = $("#addCards").validate();
        validator.resetForm();
        $(".accounts").show();
        $(".cards").hide();
    }

});
$(document).ready(function(){

    cardLists();
    bankLists();
    checkAutoPay();
    $('.exp_date').datepicker({
        yearRange: '2019:2040',
        changeMonth: true,
        changeYear: true,
        dateFormat: 'mm/yy'
    });

});


$("#addCards").validate({
    rules: {
        card_number: {
            required:true,
            number:true,
            maxlength: 16
        },
        exp_date: {
            required: true,
        },
        cvc: {
            required: true,
            number:true
        },
        holder_name: {
            required: true,
        },
    },
    submitHandler: function (e) {
        var form = $('#addCards')[0];
        var formData = new FormData(form);
        formData.append('action','addOwnersCardDetails');
        formData.append('class','Stripe');
        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status=="true")
                {
                    toastr.success(response.message);
                    cardLists();
                    $("#addCards").trigger("reset");
                }
                else
                {
                    toastr.error(response.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});


function cardLists()
{

    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        data: {
            "action": 'getAllOwnerCards',
            "class": 'Stripe'
        },
        success: function (response) {


            $(".cardDetails").html(response);


        }
    });

}





function bankLists()
{

    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        data: {
            "action": 'getOwnerBanks',
            "class": 'Stripe'
        },
        success: function (response) {
            console.log(response);

            $(".accountDetails").html(response);


        }
    });

}



$(document).on("change",".cardAction",function(){
    var cardAction = $(this).val();
    var card_id = $(this).attr('data-cardId');
    if(cardAction=='delete')
    {
        $(".cardAction").val('');
        bootbox.confirm("Do you want to delete this card?", function (result) {

            if (result == true) {
        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
                "action": 'deleteOwnerCard',
                "class": 'Stripe',
                "card_id":card_id,
            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);

                cardLists();
                bankLists();




            }
        });

            }
        });
    }
    else if(cardAction=='default') {
        $(".cardAction").val('');
        bootbox.confirm("Do you want to set this method to default?", function (result) {
            if (result == true) {
                $.ajax({
                    url: '/stripeCheckout',
                    type: 'POST',
                    data: {
                        "action": 'defaultOwnerCard',
                        "class": 'Stripe',
                        "card_id": card_id,
                    },
                    success: function (response) {
                        var response = JSON.parse(response);
                        toastr.success(response.message);

                        cardLists();
                        bankLists();


                    }
                });

            }
        });
    }



});


function checkAutoPay()
{
    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        data: {
            "action": 'checkAutoPayOwner',
            "class": 'Stripe'
        },
        success: function (data_res) {
            var info = JSON.parse(data_res);
            if(info.data=="ON"){

                $('.auto_pay').prop('checked', true);
            }
            else

                $('.auto_pay').prop('checked', false);
        }

    });

}