viewData();
var setTime = 400;

function getParameterByName( name ){
    var regexS = "[\\?&]"+name+"=([^&#]*)",
        regex = new RegExp( regexS ),
        results = regex.exec( window.location.search );
    if( results == null ){
        return "";
    } else{
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}
var id =  getParameterByName('id');
function viewData() {

    var owner_id =  getParameterByName('id');
    $.ajax({
        type: 'post',
        url: '/EditOwnerAjax',
        data: {
            class: "EditOwnerAjax",
            action: "getviewData",
            owner_id:owner_id
        },
        success: function (response) {

            var res = $.parseJSON(response);

            if (res.status == "success") {
                var OwnerDataUser = res.data.user_detail;
                var OwnerDataDetail = res.data.owner_detail;
                var OwnerDataIntial = res.data.intial_data;
                var OwnerDataBanking = res.data.owner_banking_information;
                var ownerPropertyOwnedInfo = res.data.owner_property_owned_info;
                var ownerPhoneInfo = res.data.owner_phone_Info;
                var owner_property_owned_info = res.data.owner_property_owned_info;
                var tenant_chargenote = res.data.tenant_chargenote;
                var credential_data = res.credential_data;
                var emergency_data = res.emergency_data;
                getCustomfield(res.data.custom_data);
                //Get Initial Data
                console.log('OwnerDataUser',OwnerDataUser.updated_at);
                edit_date_time(OwnerDataUser.updated_at);
                if (OwnerDataIntial && OwnerDataIntial.ethnicity.length > 0){
                    var ethnicityOption = "<option value=''>Select</option>";
                    $.each(OwnerDataIntial.ethnicity, function (key, value) {
                        ethnicityOption += "<option value='"+value.id+"'>"+value.title+"</option>";
                    });
                    setTimeout(function () {
                        $('select[name="ethncity"]').html(ethnicityOption);
                        $('.addition_owner_block select[name="additional_ethncity"]').html(ethnicityOption);
                    },400);
                }

                if (OwnerDataIntial && OwnerDataIntial.marital.length > 0){
                    var maritalOption =  "<option value=''>Select</option>";
                    $.each(OwnerDataIntial.marital, function (key, value) {
                        maritalOption += "<option value='"+value.id+"'>"+value.marital+"</option>";
                    });
                    setTimeout(function () {
                        $('select[name="maritalStatus"]').html(maritalOption);
                        $('.marital_status').html(maritalOption);
                    },400);
                }

                if (OwnerDataIntial && OwnerDataIntial.veteran.length > 0){
                    var veteranOption = "<option value=''>Select</option>";
                    $.each(OwnerDataIntial.veteran, function (key, value) {
                        veteranOption += "<option value='"+value.id+"'>"+value.veteran+"</option>";
                    });
                    setTimeout(function () {
                        $('select[name="veteran_status"]').html(veteranOption);
                        $('.veteran_status').html(veteranOption);
                    },400);
                }
                //End Intial Data
                if(OwnerDataUser.if_entity_name_display == '1')
                {
                    $('#if_entity_name_display').prop('checked', true);
                    $('#ethnicity_to_ssn_div').hide();
                }
                setTimeout(function () {
                    $('input#if_entity_name_display').removeAttr('value');
                },600)
                if(OwnerDataUser.draw_payment_method == 'check')
                {
                    $("input[name='draw_payment_method'][value='check']").attr('checked', 'checked');
                } else{
                    $("input[name='draw_payment_method'][value='eft']").attr('checked', 'checked');
                }
                if(OwnerDataIntial)
                {
                    contactCarrier(OwnerDataIntial.carrier);
                }

                $('#flag_flag_date').val(OwnerDataUser.current_date);


                var myarray = ['owners_portal','draw_payment_method','send_owners_package','hold_owners_payments','email_financial_info','include_reports','eligible_1099','send_1099','company_name_as_tax_payer','ethncity','maritalStatus','hobbies','veteranStatus','referral_source'];

                $.each(OwnerDataUser, function( key, value ) {
                    //fix the conflict to remove the overidden issue
                    if(!(jQuery.inArray(key, myarray) !== -1))
                    {
                        $("input[name='"+key+"']").val(value);
                    }

                    if(key=='ethnicity')
                    {
                        setTimeout(function () {
                            $("select[name='ethncity']").val(value);
                        },400);
                    }else if(key=='salutation')
                    {
                        if(value == 'Mrs.' || value == 'Ms.' || value == 'Madam' || value == 'Sister' || value == 'Mother')
                        {
                            $('#maiden_name').parents('.col-sm-3').show();
                        } else {
                            $('#maiden_name').parents('.col-sm-3').hide();
                        }
                    }else if(key=='maritial_status')
                    {
                        setTimeout(function () {
                            $("select[name='maritalStatus']").val(value);
                        },400);
                    }else if(key=='hobbies')
                    {
                        selectHobbies(OwnerDataIntial,value);
                    } else if(key=='veteran_status')
                    {
                        setTimeout(function () {
                            $("select[name='veteranStatus']").val(value);
                        },400);
                    }else if(key=='ssn_sin_id')
                    {
                        if ( !value || value != 'N/A') {
                            selectSSN(value);
                        }
                    }else if(key=='phone_number_note')
                    {
                        (!value || value != 'N/A') ? $("textarea[name='phone_number_note']").val(value) : '';

                    }else if(key=='company_name')
                    {
                        // if(!value || value != 'N/A') {
                        //     $('.company_name_as_tax_payer_div').show();
                        // } else {
                        //     $('.company_name_as_tax_payer_div').hide();
                        // }
                        $('.company_name_as_tax_payer_div').show();
                        // }else if(key=='company_name_as_tax_payer')
                        // {
                        //     if(value != 1){
                        //         $('#company_name_as_tax_payer').prop('checked', true);
                        //         $('.company_name_as_tax_payer_div').show();
                        //     } else {
                        //         $('#company_name_as_tax_payer').prop('checked', false);
                        //         $('.company_name_as_tax_payer_div').hide();
                        //     }

                    }else if(key=='send_1099')
                    {
                        (value == 1) ? $('#send_1099').prop('checked', true) : $('#send_1099').prop('checked', false);

                    }else if(key=='hold_owners_payments')
                    {
                        (value == 1) ? $('#hold_owners_payments').prop('checked', true) : $('#hold_owners_payments').prop('checked', false);

                    }else if(key=='email_financial_info')
                    {
                        (value == 1) ? $('#email_financial_info').prop('checked', true) : $('#email_financial_info').prop('checked', false);

                    }else if(key=='owners_portal')
                    {
                        if(value == 1){
                            $('#owners_portal_Yes').prop('checked', true);
                            $('#owners_portal_No').prop('checked', false);
                        } else {
                            $('#owners_portal_Yes').prop('checked', false);
                            $('#owners_portal_No').prop('checked', true);
                        }
                    }else if(key=='send_owners_package')
                    {
                        if(value == 1){
                            $('#send_owners_package_Fax').prop('checked', true);
                            $('#send_owners_package_Email').prop('checked', false);
                        } else {
                            $('#send_owners_package_Fax').prop('checked', false);
                            $('#send_owners_package_Email').prop('checked', true);
                        }
                    }else if(key=='eligible_1099')
                    {
                        if(value == 1){
                            $('#eligible_1099_yes').prop('checked', true);
                            $('#eligible_1099_no').prop('checked', false);
                        } else {
                            $('#eligible_1099_yes').prop('checked', false);
                            $('#eligible_1099_no').prop('checked', true);
                        }
                    }else if(key=='send_1099')
                    {
                        if(value == 1){
                            $('#send_1099').prop('checked', true);
                        } else {
                            $('#send_1099').prop('checked', false);
                        }
                    }else if(key=='company_name_as_tax_payer')
                    {
                        if(value == 1){
                            $('#company_name_as_tax_payer').prop('checked', true);
                        } else {
                            $('#company_name_as_tax_payer').prop('checked', false);
                        }
                    }else if(key=='referral_source')
                    {
                        setTimeout(function () {
                            $("select[name='referral_source']").val(value);
                        },400);
                    }else if(key=='zipcode' || key=='country' || key=='state'  || key=='city')
                    {   setTimeout(function () {
                        $("input[name='"+key+"']").val(value);
                    },400);
                    }else {
                        if(!(jQuery.inArray(key, myarray) !== -1))
                        {
                            $("select[name='"+key+"']").val(value);
                        }

                    }
                    // },100);
                });

                if(OwnerDataDetail && OwnerDataDetail.owner_image)
                {
                    $('.owner_image.img-outer').html(OwnerDataDetail.owner_image);
                }

                if(OwnerDataDetail && OwnerDataDetail.contact_carrier_data && OwnerDataDetail.contact_carrier_data.length>=1)
                {
                    selectPhoneRow(OwnerDataDetail.contact_carrier_data,OwnerDataIntial.carrier);
                }

                if(OwnerDataIntial && OwnerDataDetail.email && OwnerDataDetail.email.length>=1)
                {
                    selectEmailRow(OwnerDataDetail.email);
                }
                if(OwnerDataBanking)
                {
                    selectOwnerBanking(OwnerDataBanking);
                }

                if(OwnerDataIntial && OwnerDataDetail.emergency_contact_name && OwnerDataDetail.emergency_contact_name.length>=1)
                {

                    selectEmergencyRow(OwnerDataDetail.emergency_contact_name,OwnerDataDetail.edit_emergency_code,OwnerDataDetail.emergency_email,OwnerDataDetail.emergency_phone_number,OwnerDataDetail.emergency_relation_new,OwnerDataDetail.emergency_other_relation);
                }

                if(owner_property_owned_info)
                {
                    selectOwnedProperty(owner_property_owned_info,OwnerDataIntial.property_list);
                }

                if(tenant_chargenote)
                {
                    selectTenantChargenote(tenant_chargenote);
                }

                if(credential_data)
                {
                    if (credential_data.length != 0) {
                        selectCredentialData(credential_data);
                    }

                }
            } else if (res.status == "error") {
                toastr.error('Owner does not exist.');
                window.location.href="/People/Ownerlisting";
            } else {  debugger
                toastr.error(res.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function selectOwnedProperty(OwnerPropertyData,property_list) {

    var property_id = [];
    $.each(OwnerPropertyData, function (key, value) {
        property_id.push(value.property_id);
    });

    $.ajax({
        url:'/EditOwnerAjax',
        type: 'POST',
        data: {action:'getPropertyListing',property_id:property_id},
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status == "success"){
                $.each(response.data,function (key,value) {
                    $("#property_owned_id").append(new Option(value.property_name, value.id));
                })
            } else if (response.code == 500){
                //  toastr.warning(response.message);
            }
        },
    });

    $.each(OwnerPropertyData, function (key, value) {
        if (key != 0) {
            var propertyDivLength = $('.owner_property_owned_outer').length;
            if (propertyDivLength <= 2) {
                var clone = $("#div_add_owner").clone().insertAfter("div.owner_property_owned_outer:last");
                clone.find('#property_owned_id').val('');
                clone.find('#property_owned_id').attr('id', 'property_owned_id' + propertyDivLength);
                clone.find('#property_percent_owned').val('100');
                clone.find('#property_percent_owned').attr('id', 'property_percent_owned' + propertyDivLength);
                $(".owner_property_owned_outer:not(:eq(0))  .add_property_div_plus").remove();
                $(".owner_property_owned_outer:not(:eq(0))  .remove_property_div_minus").show();
                if (propertyDivLength == 2) {
                    $('.add_property_div_plus').hide();
                }
            }
        }

        var propertyOption = "<option value=''>Select</option>";
        $.each(property_list, function (key, value) {
            if(value.property_percent_owned < 100){
                propertyOption += "<option value='"+value.id+"' property_percent_owned='"+value.property_percent_owned+"' data-id='"+value.property_id+"' >"+value.property_name+"</option>";
            }
        });
        $('#property_owned_id').html(propertyOption);
    });

    $('.owner_property_owned_outer').each(function (key,value) {
        $(this).find('select[name="property_id[]"]').addClass('property_owned_id'+key);
        $(this).find('input[name="property_percent_owned[]"]').addClass('property_percent_owned property_percent_owned'+key);
    });



    setTimeout(function () {
        $('.owner_property_owned_outer').each(function (key,value) {
            var keyD = parseInt(key);
            $('.property_owned_id'+keyD).val((OwnerPropertyData)?OwnerPropertyData[keyD]['property_id']:'');
            $('.property_percent_owned'+keyD).val((OwnerPropertyData)?OwnerPropertyData[keyD]['property_percent_owned']:'');
        },500);
    });
    setTimeout(function () {
        $('#property_owned_id').val((OwnerPropertyData) ? OwnerPropertyData[0]['property_id'] : '');
    },2000);
}
function selectOwnerBanking(OwnerDataBanking) {
    $.each(OwnerDataBanking, function (key, value) {
        if (key != 0) {
            var clone = $(".divBankAccount:first").clone();
            clone.find('input[type=text]').val('');
            clone.find(".remove-chart-bank").show();
            $(".divBankAccount").first().after(clone);
            $(".divBankAccount .add-chart-bank").hide();
        }
    });
    $('.divBankAccount').each(function (key,value) {
        $(this).find('select[name="account_name[]"]').addClass('account_name'+key);
        $(this).find('select[name="account_for_transaction[]"]').addClass('account_for_transaction'+key);
        $(this).find('select[name="bank_account_type[]"]').addClass('bank_account_type'+key);
        $(this).find('input[name="bank_account_number[]"]').addClass('bank_account_number'+key);
        $(this).find('input[name="routing_transit_number[]"]').addClass('routing_transit_number'+key);
        $(this).find('input[name="bank_institution_name[]"]').addClass('bank_institution_name'+key);
        $(this).find('input[name="bank_fraction_number[]"]').addClass('bank_fraction_number'+key);
    });

    setTimeout(function () {
        $('.owner-emergency-contact').each(function (key,value) {
            var keyD = parseInt(key);
            if(OwnerDataBanking[keyD])
            {
                $('.account_name'+keyD).val((OwnerDataBanking)?OwnerDataBanking[keyD]['account_name']:'');
                $('.account_for_transaction'+keyD).val((OwnerDataBanking)?OwnerDataBanking[keyD]['account_for_transaction']:'');
                $('.bank_account_type'+keyD).val((OwnerDataBanking)?OwnerDataBanking[keyD]['bank_account_type']:'');
                $('.bank_account_number'+keyD).val((OwnerDataBanking)?OwnerDataBanking[keyD]['bank_account_number']:'');
                $('.routing_transit_number'+keyD).val((OwnerDataBanking)? OwnerDataBanking[keyD]['routing_transit_number']:'');
                $('.bank_institution_name'+keyD).val((OwnerDataBanking)? OwnerDataBanking[keyD]['bank_institution_name']:'');
                $('.bank_fraction_number'+keyD).val((OwnerDataBanking)? OwnerDataBanking[keyD]['bank_fraction_number']:'');
            }
        });
    },500);
}

function selectEmergencyRow(emergency_contact_name,emergency_country_code,emergency_email,emergency_phone_number,emergency_relation, emergency_other_relation) {

    $.each(emergency_contact_name, function (key, value) {
        if (key != 0) {
            var clone = $("#owner-emergency-contact_div_id:first").clone().insertAfter("div.owner-emergency-contact:last");
            clone.find('input[type=text]').val('');
            clone.find('input[name="emergency_phone[]"]').mask('000-000-0000', {reverse: true});
            clone.find(".add-emergency-contant").hide();
            clone.find(".remove-emergency-contant").show();
        }
    });


    $('.owner-emergency-contact').each(function (key,value) {
        $(this).find('input[name="emergency_contact_name[]"]').addClass('emergency_contact_name'+key);
        $(this).find('select[name="emergency_relation[]"]').addClass('emergency_relation'+key);
        $(this).find('select[name="emergency_country[]"]').addClass('emergency_country'+key);
        $(this).find('input[name="emergency_phone[]"]').addClass('emergency_phone'+key);
        $(this).find('input[name="emergency_email[]"]').addClass('emergency_email'+key);
    });

    setTimeout(function () {
        $('.owner-emergency-contact').each(function (key,value) {
            var keyD = parseInt(key);

            $('.emergency_contact_name'+keyD).val((emergency_contact_name)?emergency_contact_name[keyD]:'');
            $('.emergency_relation'+keyD).val((emergency_relation)?emergency_relation[keyD]:'');
            // if(emergency_relation[keyD] == 'Other'){
            //     $('#otherRelationDiv').show();
            //     $('.emergency_relation'+keyD).val((emergency_other_relation)?emergency_other_relation[keyD]:'');
            // } else {
            //     $('#otherRelationDiv').hide();
            //     $('#emergency_other_relation').val('');
            // }

            $('.emergency_phone'+keyD).val((emergency_phone_number)?emergency_phone_number[keyD]:'');
            $('.emergency_email'+keyD).val((emergency_email)? emergency_email[keyD]:'');

            if(emergency_relation[keyD] == '9')
            {
                $('.emergency_relation'+keyD).parents('.owner-emergency-contact').find('#otherRelationDiv').show();
                $('.emergency_relation'+keyD).parents('.owner-emergency-contact').find('#emergency_other_relation').val(emergency_other_relation[keyD]);
                $('.emergency_relation'+keyD).parents('.owner-emergency-contact').find('input').attr('disabled',false);
            }

        },500);


        setTimeout(function () {
            $('.emergencycountry option').each(function(key,value){
                var country = $(this).attr('data-id');
                $(this).val(country);
            });

            $.each(emergency_country_code, function (key, value) {
                var keyD = parseInt(key);

                $('.emergency_country' + keyD).val((value) ? value : '');
            });
        },600);
    })
}

function selectCredentialData(credential_data) {
    $.each(credential_data, function (key, value) {
        if (key != 0) {
            var length = $('.credential-clone').length;
            var clone = $("#owner-credentials-control_divId").clone().insertAfter("div.owner-credentials-control:last");
            clone.find('input[type=text]').val('');
            clone.find('select[name="credentialType[]"]').val('');

            clone.find('#acquireDate')
                .removeClass('hasDatepicker')
                .removeData('datepicker')
                .removeAttr('id')
                .attr('id','acquireDate'+length)
                .unbind()
                .datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat})
                .datepicker("setDate", new Date());

            clone.find('#expirationDate')
                .removeClass('hasDatepicker')
                .removeData('datepicker')
                .removeAttr('id')
                .attr('id','expirationDate'+length)
                .unbind()
                .datepicker({yearRange: '+0:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat, minDate: new Date()})
                .datepicker("setDate", new Date());

            // $(".owner-credentials-control").first().after(clone);
            clone.find(".add-notice-period").hide();
            clone.find(".remove-notice-period").show();
        }
    });

    $('.owner-credentials-control').each(function (key,value) {
        $(this).find("input[name='credentialName[]']").addClass('credentialName'+key);
        $(this).find('select[name="credentialType[]"]').addClass('credentialType'+key);
        $(this).find("input[name='expirationDate[]']").addClass('expirationDate'+key);
        $(this).find('input[name="acquireDate[]"]').addClass('acquireDate'+key);
        $(this).find('select[name="noticePeriod[]"]').addClass('noticePeriod'+key);
    });

    setTimeout(function () {
        $('.owner-credentials-control').each(function (key,value) {
            var keyD = parseInt(key);
            $('.credentialName'+keyD).val((credential_data)?credential_data[keyD]['owner_credential_name']:'');
            $('.credentialType'+keyD).val((credential_data)?credential_data[keyD]['owner_credential_type']:'');
            $('.expirationDate'+keyD).val((credential_data)?credential_data[keyD]['owner_credential_expiration_date']:'');
            $('.acquireDate'+keyD).val((credential_data)?credential_data[keyD]['owner_credential_acquire_date']:'');
            $('.noticePeriod'+keyD).val((credential_data)?credential_data[keyD]['owner_credential_notice_period']:'');
        });
    },1000);
}

function selectEmailRow(OwnerDataDetail) {
    $.each(OwnerDataDetail, function (key, value) {
        if(key!=0)
        {
            var clone = $("#multiple_email_div_id").clone().insertAfter("div.multipleEmail:last");
            clone.find('input[type=text]').val(value);
            $(".multipleEmail:not(:eq(0))  .email-plus-sign .fa-plus-circle").remove();
            $(".multipleEmail:not(:eq(0))  .email-remove-sign .fa-times-circle").show();
            if(key == 2){
                $('.email-plus-sign').hide();
            }
        } else {
            $('#multiple_email_div_id input').val(value);
        }
    });
}

function selectTenantChargenote(tenant_chargenote) {

    if (tenant_chargenote.length > 0) {
        $.each(tenant_chargenote, function (key, notes) {
            if(key!=0) {


                var clone = $("#ownerTxtAreaDivId").clone().insertAfter("div.ownerTxtArea:last");

                clone.find('.notes').val(notes.note);
                clone.find(".add-icon-textarea").hide();
                clone.find(".remove-icon-textarea").show();

                var phoneRowLenght = $(".ownerTxtArea");
                if (phoneRowLenght.length == 2) {
                    clone.find(".fa-plus-circle").hide();
                } else if (phoneRowLenght.length == 3) {
                    clone.find(".fa-plus-circle").hide();
                    $(".ownerTxtArea:eq(0) .fa-plus-circle").hide();
                } else {
                    $(".ownerTxtArea:not(:eq(0)) .fa-plus-circle").show();
                }
            } else {
                $('div#ownerTxtAreaDivId').find('.notes').val(notes.note);
            }
        });
    }
}

function selectHobbies(OwnerDataIntial,value) {

    if (OwnerDataIntial.hobbies.length > 0){
        var hobbyOption = "";
        var result = value.split(',');
        $.each(OwnerDataIntial.hobbies, function (keyHobby, hobbyData) {

            if($.inArray(hobbyData.hobby, result) !== -1)
            {
                hobbyOption += "<option value='"+hobbyData.id+"' selected>"+hobbyData.hobby+"</option>";
            } else {
                hobbyOption += "<option value='"+hobbyData.id+"'>"+hobbyData.hobby+"</option>";
            }
        });
        setTimeout(function () {


            $("select[name='hobbies[]'],select[name='additional_hobbies[]']").multiselect("clearSelection");
            $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').html(hobbyOption);

            $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});


            $("select[name='hobbies[]']").multiselect( 'refresh' );


        },400);
    }
}

function selectSSN(SSNData) {
    if (SSNData && SSNData.length > 0){
        $.each(SSNData, function (keySSN, ssn_sin_id) {
            if(keySSN!='0')
            {
                var clone = $("#multiple_ssn_div_id").clone().insertAfter("div.multipleSsn:last");
                clone.find('input[type=text]').val(ssn_sin_id);
                $(".multipleSsn:not(:eq(0))  .ssn-plus-sign").remove();
                $(".multipleSsn:not(:eq(0))  .ssn-remove-sign").show();
            }else{
                $('input#ssn').val(ssn_sin_id);
            }

        });
    }
}

function selectPhoneRow(contact_carrier_data,contact_phone_type)
{
    var n = contact_carrier_data.length;
    for(i=0; i<n; i++){
        if(i!=0)
        {
            var clone = $("#primary-owner-phone-row-divId").clone().insertAfter("div.primary-owner-phone-row:last");
            clone.find('input[type=text]').val('');
            $(".primary-owner-phone-row:not(:eq(0))  .fa-plus-circle").hide();
            $(".primary-owner-phone-row:not(:eq(0))  .fa-times-circle").show();

            var phoneRowLenght = $(".primary-owner-phone-row");
            clone.find('.phone_format').mask('000-000-0000', {reverse: true});
            if (phoneRowLenght.length == 2) {
                clone.find(".fa-plus-circle").hide();
            }else if (phoneRowLenght.length == 3) {
                clone.find(".fa-plus-circle").hide();
                $(".primary-owner-phone-row:eq(0) .fa-plus-circle").hide();
            }else{
                $(".primary-owner-phone-row:not(:eq(0)) .fa-plus-circle").show();
            }
        }

    }

    $('.primary-owner-phone-row').each(function (key,value) {

        $(this).find('select[name="carrier[]"]').addClass('carrier_'+key);
        $(this).find('select[name="country_code[]"]').addClass('country_code_'+key);
        $(this).find('select[name="phone_type[]"]').addClass('phone_type_'+key);
        $(this).find('input[name="phone_number[]"]').addClass('phone_number_'+key);
    });

    setTimeout(function () {
        $('.primary-owner-phone-row').each(function (key,value) {
            var keyD = parseInt(key);
            $('.carrier_'+keyD).val((contact_carrier_data)?contact_carrier_data[keyD]['carrier']:'');
            $('.country_code_'+keyD).val((contact_carrier_data)?contact_carrier_data[keyD]['country_code']:'');
            $('.phone_type_'+keyD).val((contact_carrier_data)?contact_carrier_data[keyD]['phone_type']:'');
            $('.phone_number_'+keyD).val((contact_carrier_data)?contact_carrier_data[keyD]['phone_number']:'');

            if(contact_carrier_data[keyD]['phone_type'] == '5' || contact_carrier_data[keyD]['phone_type'] == '2')
            {
                $('.phone_type_'+keyD).parents('.primary-owner-phone-row').find('.work_extension_div').show();
                $('.phone_type_'+keyD).parents('.primary-owner-phone-row').find('#work_phone_extension').val(contact_carrier_data[keyD]['work_phone_extension']);
                $('.phone_type_'+keyD).parents('.primary-owner-phone-row').find('#other_work_phone_extension').val(contact_carrier_data[keyD]['other_work_phone_extension']);
                $('.phone_type_'+keyD).parents('.primary-owner-phone-row').find('input').attr('disabled',false);
            }
        });
    },500)
}

$(document).on('change','#salutation',function(){
    var salutation = $(this).val();

    if(salutation == 'Mrs.' || salutation == 'Ms.' || salutation == 'Madam' || salutation == 'Sister' || salutation == 'Mother')
    {
        $('#maiden_name').parents('.col-sm-3').show();
    } else {
        $('#maiden_name').parents('.col-sm-3').hide();
    }
});

function contactCarrier(contactCarrier)
{
    var carrierOption = "<option value=''>Select</option>";
    $.each(contactCarrier, function (key, value) {
        carrierOption += "<option value='"+value.id+"'>"+value.carrier+"</option>";
    });
    $('.primary-owner-phone-row select[name="carrier[]"]').html(carrierOption);
    $('.addition_owner_block select[name="additional_carrier"]').html(carrierOption);
    $('.property_guarantor_form1 select[name="guarantor_carrier_1[]"]').html(carrierOption);
    $('.additional_carrier').html(carrierOption);
}

/**
 * jqGrid Building Keys function
 * @param status
 */
//ownerFlags();
function ownerFlags(status) {
    var id =  getParameterByName('id');
    var table = 'flags';
    var columns = ['Date', 'Flag Name', 'Phone Number','Flag Reason','Completed','Note', 'Action'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    //var extra_columns = [];
    var extra_where = [{column: 'user_id', value: id, condition: '='}];
    var columns_options = [
        {name:'Date',index:'date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Flag Name',index:'flag_name',width:100,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Phone',index:'flag_phone_number', width:100,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,table:table,formatter:phoneNumberFormat},
        {name:'Flag Reason',index:'flag_reason',width:100,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Completed',index:'completed', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,table:table,formatter:isCompletedFormatter},
        {name:'Note',index:'flag_note',width:100,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'flag_note',width:100,align:"center",searchoptions: {sopt: conditions},table:table},
    ];
    var ignore_array = [];
    jQuery("#ownerFlags-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            // extra_columns: extra_columns,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Flags",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {top: 10, left: 200, drag: true, resize: false}
    );
}

/**
 * jqGrid function to format completed column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function phoneNumberFormat(cellValue, options, rowObject) {
    if(rowObject !== undefined) {
        if(cellValue !== undefined || cellValue != '' ){
            return cellValue.replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
        }
        else {
            return '';
        }
    }
}
/**
 * jqGrid function to format completed column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function isCompletedFormatter(cellValue, options, rowObject) {
    if (cellValue == '1')
        return "True";
    else if (cellValue == '0')
        return "False";
    else
        return '';
}

function triggerFileReload(){
    var grid = $("#file-library");
    grid[0].p.search = false;
    $.extend(grid[0].p.postData,{filters:""});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}
fileLibrary();
function fileLibrary(status) {

    var owner_id =  getParameterByName('id');
    var table = 'tenant_chargefiles';
    // var columns = ['Name','Preview','Action'];
    var columns = ['Id', 'Name', 'Preview','File_location','File_extension','Action']
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    // var extra_columns = [];
    var extra_where = [{column: 'user_id', value: id, condition: '='}];
    var columns_options = [
        // {name: 'Name',index: 'filename',width: 150,align: "center",searchoptions: {sopt: conditions},table: table},
        // {name:'Preview',index:'file_extension',width:150,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
        // {name:'Action',index:'', title: false, width:50,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:fileFormatter},


        {name:'Id',index:'id', width:150,hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'Name',index:'filename', width:150,searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'file_extension',width:150,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
        {name:'File_location',index:'file_location', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'File_extension',index:'file_extension', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'file_type', width:50,searchoptions: {sopt: conditions},table:table,formatter:fileFormatter},
    ];
    var ignore_array = [];
    jQuery("#file-library").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            //  extra_columns: extra_columns,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Files",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {top: 10, left: 200, drag: true, resize: false}
    );
}

/**
 * jqGrid function to format file extension
 * @param status
 */
function imageFormatter(cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var src = '';
        if (rowObject.Preview == 'xlsx') {
            src = upload_url + 'company/images/excel.png';
        } else if (rowObject.Preview == 'pdf') {
            src = upload_url + 'company/images/pdf.png';
        } else if (rowObject.Preview == 'docx' || rowObject.Preview == 'doc') {
            src = upload_url + 'company/images/word_doc_icon.jpg';
        }else if (rowObject.Preview == 'txt') {
            src = upload_url + 'company/images/notepad.jpg';
        }
        return '<img class="img-upload-tab" width=30 height=30 src="' + src + '">';
    }
}

function statusFmatter1 (cellvalue, options, rowObject){
    if(rowObject !== undefined) {

        var select = '';
        select = ['Email','Delete'];

        var data = '';

        if(select != '') {
            var data = '<select class="form-control file_select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }

        return data;
    }
}

$('#remove_library_file').attr('disabled',true);
$('#SaveAllimagesFileOwner').attr('disabled',true);

$(document).on('click','#SaveAllimagesFileOwner',function () {

    // var owner_image = $('.owner_image').html();
    // var ownerImage = JSON.stringify(owner_image);
    // var owner_id =  getParameterByName('id');

    var form = $('#addOwner')[0];
    var formData = new FormData(form);
    formData.append('action','ownerFileAdd');
    formData.append('class','EditOwnerAjax');

    $.ajax({
        url:'/EditOwnerAjax',
        type: 'POST',
        data: formData,
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status == "success"){
                localStorage.setItem("Message", 'Record added successfully.');
                $('#file_library_uploads').html('');
                $('#file-library').trigger( 'reloadGrid' );
                $('#remove_library_file').attr('disabled',true);
                $('#SaveAllimagesFileOwner').attr('disabled',true);
            } else if (response.code == 500){
                toastr.warning(response.message);
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

setTimeout(function(){
    if(localStorage.getItem("AccordionHref")) {
        var message = localStorage.getItem("AccordionHref");
        $('html, body').animate({
            'scrollTop': $(message).position().top
        });
        localStorage.removeItem('AccordionHref');
    }
}, 250);

$(document).on("click", "#reset_edit_owner_btn", function(e) {
    bootbox.confirm("Do you want Reset?", function(result) {
        if (result == true) {
            window.location.href = '/People/EditOwners?id='+id;
        }
    });
});

/**Send Email Scripts**/
$(document).on('change','.file_select_options ',function () {
    var selectVal = $(this).val();
    var selectID = $(this).attr('data-id');

    setTimeout(function(){ $(".file_select_options").val(""); }, 200);

    $('#file-library').find('.green_row_left, .green_row_right').each(function(){
        $(this).removeClass("green_row_left green_row_right");
    });

    var row_num = $(this).parent().parent().index() ;
    jQuery('#file-library').find('tr:eq('+row_num+')').find('td:eq(1)').addClass("green_row_left");
    jQuery('#file-library').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");

    if(selectVal == 'Send')
    {
        //$('#sendMailModal').modal('toggle');
        // var src = $(this).attr("data-src");
        // var path = $(this).attr("data-path");
        //
        // var imageFile = '<a href="'+path+'" class="attachments"><img class="img-upload-tab" width="100" height="100" src="'+src+'"></a>';
        // $(".attachmentFile").html(imageFile);
        //
        // $('.to').tagsinput('add',  $('.current_admin_email').val());
        file_upload_email('users','email',selectID,1);

    }else if(selectVal == 'Delete')
    {
        bootbox.confirm("Do you want to delete this record?", function(result) {
            if (result == true) {

                $.ajax({
                    type: 'post',
                    url:'/EditOwnerAjax',
                    data: {
                        class: "EditOwnerAjax",
                        action: 'ownerFileDelete',
                        file_id: selectID
                    },
                    success: function (response) {
                        var response = JSON.parse(response);
                        if (response.status == 'success' && response.code == 200) {
                            toastr.success(response.message);
                            $('#file-library').trigger( 'reloadGrid' );
                        } else if (response.status == 'error' && response.code == 400) {
                            $('.error').html('');
                            $.each(response.data, function (key, value) {
                                $('.' + key).html(value);
                            });
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            // alert(key+value);
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
            }
        });

    }else {

    }


});
//
// $("#sendEmail").validate({
//     rules: {
//         to: {
//             required:true
//         },
//         cc: {
//             required:true
//         },
//         body: {
//             required:true
//         },
//     },
//     submitHandler: function (e) {
//
//         var tenant_id = $(".tenant_id").val();
//         var form = $('#sendEmail')[0];
//         var formData = new FormData(form);
//         var path = $(".attachments").attr('href');
//         /*  alert(path);*/
//         var to = $(".to").val();
//         formData.append('to_users',to);
//         formData.append('action','sendMail');
//         formData.append('class','EditOwnerAjax');
//         formData.append('path', path);
//
//         $.ajax({
//             url:'/EditOwnerAjax',
//             type: 'POST',
//             data: formData,
//             success: function (data) {
//                 info =  JSON.parse(data);
//                 if(info.status=="success"){
//                     toastr.success("Email has been sent successfully");
//                 }
//             },
//             cache: false,
//             contentType: false,
//             processData: false
//         });
//     }
// });

$('.note-editable').keyup(function(){ $('.message_body').html('');  });

$('.bootstrap-tagsinput').keyup(function(){ $('.recipient_list').html('');  });

$("#sendEmail").validate({
    rules: {
        to: {
            required:true
        },
         body: {
            required:true
         },
        subject: {
            required:true
        },
    },
    submitHandler: function (e) {
        var tenant_id = $(".tenant_id").val();
        var form = $('#sendEmail')[0];
        var formData = new FormData(form);
        var path = $(".attachments").attr('href');
     var recipientlength =  $('.bootstrap-tagsinput .label-info').length;
     if(recipientlength < 1){
        $('.recipient_list').html('This field is required');
        return false;
     }
     var message = $('.note-editable p br').html();
    // console.log(message);
     if(message =='')
        {
            $('.message_body').html('This field is required');
            return false;
        }

        /*  alert(path);*/
        var to = $(".to").val();
        formData.append('to_users',to);
        formData.append('action','sendFileLibraryattachEmail');
        formData.append('class','TenantAjax');
        formData.append('path', path);

        $.ajax({
            url: '/Tenantlisting/getInitialData',
            type: 'POST',
            data: formData,
            success: function (data) {
                info =  JSON.parse(data);
                if(info.status=="success"){
                    $('#sendMailModal').modal('toggle');
                    toastr.success("Email has been sent successfully");
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});


$(document).on("click",".addToRecepent",function(){
    $('#torecepents').modal('show');
});


$(document).on("change",".selectUsers",function(){
    var type = $(this).val();
    $.ajax({
        url:'/EditOwnerAjax',
        type: 'POST',
        data: {
            "type": type,
            "action": 'getUsers',
            "class": 'EditOwnerAjax'
        },
        success: function (response) {
            $(".userDetails").html(response);

        }
    });



});

$(document).on("click",".getEmails",function(){
    if(this.checked)
    {
        var email = $(this).attr('data-email');
        $('.to').tagsinput('add', email);
    }
    else
    {
        var email = $(this).attr('data-email');
        $('.to').tagsinput('remove', email);
    }
});

$(document).on("click","#SendselectToUsers",function(){
    var check_data = [];
    $('.getEmails:checked').each(function () {
        $('.to').tagsinput('add', $(this).attr('data-email'));
    });
    $('#torecepents').modal('hide');
    $("#sendMailModal").addClass("modalScroll");
    $("body").addClass("modalopen-hide");
    // $('.getEmails').prop('checked', false);
});

$(document).on('click','.delete_pro_img',function(){
    $(this).parent().parent().parent('.row').remove();
});

$(document).on("click",".addCcRecepent",function(){
    $('#ccrecepents').modal('show');
});

$(document).on("change",".selectCcUsers",function(){
    var type = $(this).val();
    $.ajax({
        url:'/EditOwnerAjax',
        type: 'POST',
        data: {
            "type": type,
            "action": 'getCCUsers',
            "class": 'EditOwnerAjax'
        },
        success: function (response) {
            $(".userCcDetails").html(response);
        }
    });



});

$(document).on("click","#SendselectCcUsers",function(){
    var check_data = [];
    $('.getCCEmails:checked').each(function () {
        $('.cc').tagsinput('add', $(this).attr('data-email'));
    });
    $('#ccrecepents').modal('hide');
    $("#sendMailModal").addClass("modalScroll");
    $("body").addClass("modalopen-hide");
});


$(document).on("click",".getCCEmails",function(){

    if(this.checked)
    {
        var email = $(this).attr('data-email');
        $('.cc').tagsinput('add', email);
    }
    else
    {
        var email = $(this).attr('data-email');
        $('.cc').tagsinput('remove', email);
    }


});

$(document).on("click",".addBccRecepent",function(){
    $('#bccrecepents').modal('show');

});

$(document).on("change",".selectBccUsers",function(){
    var type = $(this).val();
    $.ajax({
        url:'/EditOwnerAjax',
        type: 'POST',
        data: {
            "type": type,
            "action": 'getBCCUsers',
            "class": 'EditOwnerAjax'
        },
        success: function (response) {
            $(".userBccDetails").html(response);
        }
    });
});


$(document).on("click",".getBCCEmails",function(){
    if(this.checked)
    {
        var email = $(this).attr('data-email');
        $('.bcc').tagsinput('add', email);
    }
    else
    {
        var email = $(this).attr('data-email');
        $('.bcc').tagsinput('remove', email);
    }


});
$(document).on("click","#SendselectBccUsers",function(){
    var check_data = [];
    $('.getBCCEmails:checked').each(function () {
        $('.bcc').tagsinput('add', $(this).attr('data-email'));
    });
    $('#bccrecepents').modal('hide');
    $("#sendMailModal").addClass("modalScroll");
    $("body").addClass("modalopen-hide");
});

function fileFormatter(cellValue, options, rowObject)
{

    if(rowObject !== undefined) {
        console.log( rowObject.File_extension);
        var file_type =  rowObject.View;

        var location = rowObject.File_location;
        var path = upload_url+'company/'+location;

        var imageData = '';
        var src = '';
        if(file_type == '1'){

            imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
        } else {
            if (rowObject.File_extension == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            } else if (rowObject.File_extension == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            } else if (rowObject.File_extension == 'docx' || rowObject.File_extension == 'doc') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            }else if (rowObject.File_extension == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            }
        }


        var html = "<select editable='1' class='form-control file_select_options' data-id='"+rowObject.Id+"' data-path = '"+path+"' data-src= '"+src+"'>"

        html +=  "<option value=''>Select</option>";
        html +=  "<option value='Delete'>Delete</option>";
        html +=  "<option value='Send'>SEND</option>";

        html +="</select>";







        return html;


        return imageData;
    }
}


// $(".to").tagsinput('items');

$(document).on('click','#remove_library_file',function (e) {
    e.preventDefault();
});

document.getElementById('rotate-ccw').addEventListener('click', function() {
    $('.image-editor').cropit('rotateCCW')
});
document.getElementById('rotate-cw').addEventListener('click', function() {
    $('.image-editor').cropit('rotateCW')
});
$(document).on("blur", '.property_percent_owned', function () {
    var percentage_limit = $(this).attr('percentage_limit');
    var prop_percent = $(this).val();
    if( parseFloat(prop_percent)> parseFloat(percentage_limit)) {
        $(this).val(percentage_limit);
        toastr.warning('Exceed the limit of remaining percentage of this property.');
    }
});


// $(document).on('click','#new_flag',function (e) {
//     e.preventDefault();
//     var phone = $('#flag_phone_number').val();
//
//     if(phone.length > 10)
//     {
//         phone = phone.slice(0,-1);
//     }
//     phone = phone.replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
//     $('#flag_phone_number').val('');
//     setTimeout(function () {
//         $('#flag_phone_number').val(phone);
//     },500)
// });
