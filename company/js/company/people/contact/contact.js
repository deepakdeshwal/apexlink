/***** Contact Listing Tabs Ends *****/
// $(document).ready(function(){
//     $(".abc").click(function(){
//         $(".contact-subtab1").addClass("active");
//     });
// });

$(document).ready(function(){
    $('.contact-main-tab-hide').on('click',function(){
        $('#contact-main').hide();
    });
    $('#contact-main-tab').on('click',function(){
        $('#contact-main').show();
    });

});
/***** Contact Listing Tabs Ends *****/

/*custom field button*/
$(document).on('click','#add_custom_field',function(){
    custom_field_validator.resetForm();
    $('#default_value').prop('readonly',false);
    $('#default_value').datepicker('destroy');
});

/*jqGrid status*/
$('#conteactTeanantListing').on('change',function(){
    var selected = this.value;
   // console.log(selected);
    if ($("#ContactListing-table")[0].grid) {
        $('#ContactListing-table').jqGrid('GridUnload');
        jqGridContact(selected, '')
    } else {
        jqGridContact(selected, '')
    }
});


$(function () {
    $('.nav-tabs').responsiveTabs();
});

<!---Main Nav Responsive-->

$("#show").click(function () {
    $("#bs-example-navbar-collapse-2").show();
});
$("#close").click(function () {
    $("#bs-example-navbar-collapse-2").hide();
});

<!---Main Nav Responsive-->


$(document).ready(function () {
    getZipCode('#contact_zipcode', '10001', '#contact_city', '#contact_state', '#contact_country', '', '');
    $('#contact_zipcode').val('10001');
    $(".slide-toggle").click(function () {
        $(".box").animate({
            width: "toggle"
        });
    });

    $(".slide-toggle2").click(function () {
        $(".box2").animate({
            width: "toggle"
        });
    });




    getContactDetail();
});





/* salutation */

$(document).on("change","#salutation",function(){

    var value = $(this).val();
    // alert(value);
    if(value=="1")
    {
        $("#gender_contacts").val("0");
    }
    if(value=="2")
    {
        $("#gender_contacts").val("1");
    }
    if(value=="3")
    {
        $("#gender_contacts").val("2");
    } if(value=="4")
    {
        $("#gender_contacts").val("0");
    } if(value=="5")
    {
        $("#gender_contacts").val("2");
    } if(value=="6")
    {
        $("#gender_contacts").val("1");
    } if(value=="7")
    {
        $("#gender_contacts").val("2");
    } if(value=="8")
    {
        $("#gender_contacts").val("1");
    } if(value=="9")
    {
        $("#gender_contacts").val("2");
    } if(value=="10")
    {
        $("#gender_contacts").val("1");
    }
    if(value=="11")
    {
        $("#gender_contacts").val("2");
    }
    // if(value=="Dr.")
    // {
    //     $("#gender_contacts").val("");
    // }
});

/**
 * linking on contact name
 */

$(document).on('click','#ContactListing-table tr td:not(:last-child)',function(e){
    e.preventDefault();
    var base_url = window.location.origin;
    var id = $(this).closest("tr").attr('id');
    window.location.href = base_url + '/People/ContactModule?id='+id;
    $(".hidden_edit").val(id);
});

/*Add Referral Pop Add*/
$(document).on("click",".additionalReferralResource",function(){
    $('.required').text('');
    $("#additionalReferralResource1").show();
});
$(document).on("click",".add-popup .grey-btn",function(){
    var box = $(this).closest('.add-popup');
    $(box).hide();
});
/*Add marital Status*/
$(document).on("click",".selectPropertyMaritalStatus",function(){
    $("#selectPropertyMaritalStatus1").show();
});
/*Add Veteran Status POPUP*/
$(document).on("click",".selectPropertyVeteranStatus",function(){
    $("#selectPropertyVeteranStatus1").show();
});
/*Add Ethinicity POPUP*/
$(document).on("click",".selectPropertyEthnicity",function(){
    $("#selectPropertyEthnicity1").show();
});
/*Add Hobbies POPUP*/
$(document).on("click",".additionalHobbies",function(){
    $("#additionalHobbies1").show();
});
/*common function to save popup data*/
$(document).on("click",".add_single1", function () {
    var tableName = $(this).attr("data-table");
    var colName = $(this).attr("data-cell");
    var className = $(this).attr("data-class");
    var selectName = $(this).attr("data-name");
    var val = $(this).parent().prev().find("."+className).val();
    if (val == ""){
        $(".referral_sources").show().text("* This field is required");
        $(".marital_s").show().text("* This field is required");
        $(".ethnicity_s").show().text("* This field is required");
        $(".veteran_s").show().text("* This field is required");
        $(".hobbies_s").show().text("* This field is required");
     /*   $(this).parent().prev().find("."+className).next().text("* This field is required");*/
        return false;
    }else{
        $(this).parent().prev().find("."+className).next().text("");
        $("#additionalReferralResource1").hide();
        $("#selectPropertyMaritalStatus1").hide();
        $("#selectPropertyEthnicity1").hide();
        $("#selectPropertyVeteranStatus1").hide();
        $("#additionalHobbies1").hide();
        // getIntialData();
    }

    savePopUpData(tableName, colName, val, selectName);
    var val = $(this).parent().prev().find("input[type='text']").val('');
});
/*Add Referral*/
// remove data after save from input box
$(document).on("click", ".modal-dialog .add_single", function () {
    var tableName = $(this).attr("data-table");
    var colName = $(this).attr("data-cell");
    var className = $(this).attr("data-class");
    var selectName = $(this).attr("data-name");
    var val = $.trim($("." + className).val());
    if (val == "") {
        $("#" + className).text("Enter the value");
        return false;
    } else {
        $("#" + className).text("");
    }
    savePopUpData(tableName, colName, val, selectName);
});


$(document).on("click", ".pop-add-icon", function () {
    setTimeout(function () {
        $(".modal.in .modal-body input").val('');
    }, 500);
});

//save pop up data
function savePopUpData(tableName, colName, val, selectName) {
    $.ajax({
        type: 'post',
        url: '/Ajax-People/AddContact',
        data: {
            class: "contactPopup",
            action: "savePopUpData",
            tableName: tableName,
            colName: colName,
            val: val
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var option = "<option value='" + data.data.last_insert_id + "' selected>" + data.data.col_val + "</option>";
                $("select[name='" + selectName + "']").append(option);
                toastr.success(data.message);
                $(".add_single").parents(".modal.in").modal('hide');
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
// display data in drop down
$.ajax({
    type: 'post',
    url: '/Ajax-People/AddContact',
    data: {
        class: "contactPopup",
        action: "getIntialData"
    },
    success: function (response) {
        var data = $.parseJSON(response);
        if (data.status == "success") {


            if (data.data.country.length > 0){
              //  console.log(data.data.country);
                var countryOption = "<option value='0'>Select</option>";
                $.each(data.data.country, function (key, value) {
                    countryOption += "<option value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")</option>";
                });
                $('select[name="insurance_country_code[]"]').html(countryOption);
                $('select[name="insurance_country_code[][]"]').html(countryOption);
            }

            if (data.data.propertylist.length > 0){
                var propertyOption = "<option value='0'>Select</option>";
                $.each(data.data.propertylist, function (key, value) {
                    propertyOption += "<option value='"+value.id+"' data-id='"+value.property_id+"'>"+value.property_name+"</option>";
                });
                $('#property').html(propertyOption);
            }

            if (data.data.phone_type.length > 0){
                var phoneOption = "";
                $.each(data.data.phone_type, function (key, value) {
                    phoneOption += "<option value='"+value.id+"'>"+value.type+"</option>";
                });
                $('select[name="phoneType[]"]').html(phoneOption);
            }

            if (data.data.carrier.length > 0) {
                var carrierOption = "";
                $.each(data.data.carrier, function (key, value) {
                    carrierOption += "<option value='" + value.id + "'>" + value.carrier + "</option>";
                });
                $('.primary-tenant-phone-row select[name="carrier[]"]').html(carrierOption);
                $('.addition_tenant_block select[name="additional_carrier"]').html(carrierOption);
                $('.property_guarantor_form1 select[name="guarantor_carrier_1[]"]').html(carrierOption);
                $('.additional_carrier').html(carrierOption);
            }

            if (data.data.referral.length > 0) {
                var referralOption = "";
                $.each(data.data.referral, function (key, value) {
                    referralOption += "<option value='" + value.id + "'>" + value.referral + "</option>";
                });
                $('select[name="referral"]').html(referralOption);
                $('.contact_block select[name="referral"]').html(referralOption);
            }

            if (data.data.ethnicity.length > 0) {
                var ethnicityOption = "";
                $.each(data.data.ethnicity, function (key, value) {
                    ethnicityOption += "<option value='" + value.id + "'>" + value.title + "</option>";
                });
                $('select[name="title"]').html(ethnicityOption);
                $('.contact_block select[name="title"]').html(ethnicityOption);
            }

            if (data.data.marital.length > 0) {
                var maritalOption = "";
                $.each(data.data.marital, function (key, value) {
                    maritalOption += "<option value='" + value.id + "'>" + value.marital + "</option>";
                });
                $('select[name="marital"]').html(maritalOption);
                $('.contact_block select[name="marital"]').html(maritalOption);
                //$('.additional_maritalStatus').html(maritalOption);
            }

            if (data.data.hobbies.length > 0) {
                var hobbyOption = "";
                $.each(data.data.hobbies, function (key, value) {
                    hobbyOption += "<option value='" + value.id + "'>" + value.hobby + "</option>";
                });

                $('select[name="hobbies[]"]').html(hobbyOption);
                $('select[name="hobbies[]"]').multiselect({
                    includeSelectAllOption: true,
                    nonSelectedText: 'Select'
                });

            }


            /*  $('.add_owner_hobby').multiselect({
                  includeSelectAllOption: true,
                  nonSelectedText: 'Hobbies'
              });
*/
            if (data.data.veteran.length > 0) {
                var veteranOption = "";
                $.each(data.data.veteran, function (key, value) {
                    veteranOption += "<option value='" + value.id + "'>" + value.veteran + "</option>";
                });
                $('select[name="veteran"]').html(veteranOption);
                $('.contact_block select[name="veteran"]').html(veteranOption);
                // $('.additional_veteran').html(veteranOption);
            }

            // if (data.data.collection_reason.length > 0){
            //     var reasonOption = "";
            //     $.each(data.data.collection_reason, function (key, value) {
            //         reasonOption += "<option value='"+value.id+"'>"+value.reason+"</option>";
            //     });
            //     $('.property_collection select[name="collection_reason"]').html(reasonOption);
            // }
            //
            if (data.data.credential_type.length > 0){
                var typeOption = "";
                $.each(data.data.credential_type, function (key, value) {
                    typeOption += "<option value='"+value.id+"'>"+value.credential_type+"</option>";
                });
                $('select[name="credential_type[]"]').html(typeOption);
                $('.contact_block select[name="credential_type[]"]').html(typeOption);
            }


        } else if (data.status == "error") {
            toastr.error(data.message);
        } else {
            toastr.error(data.message);
        }
    },
    error: function (data) {
        var errors = $.parseJSON(data.responseText);
        $.each(errors, function (key, value) {
            $('#' + key + '_err').text(value);
        });
    }
});
// $( document ).ready(function() {
//     getIntialData();
// });



/*ADD multiple SSN/SIN/ID * textbox*/

$(document).on("click", ".contact-ssn-row .ssn-add-remove-row > .glyphicon-plus-sign", function () {

    var clone = $(".contact-ssn-row:first").clone();
    clone.find("input[type='text']").val('');
    $(".contact-ssn-row").first().after(clone);
    $(".contact-ssn-row:not(:eq(0)) .ssn-add-remove-row .glyphicon-plus-sign").remove();
    $(".contact-ssn-row:not(:eq(0)) .ssn-add-remove-row .glyphicon-remove-sign").show();

});
/*remove multiple SSSN/SIN/ID * textbox*/

$(document).on("click", ".contact-ssn-row .ssn-add-remove-row .glyphicon-remove-sign", function () {

    $(this).parents(".contact-ssn-row").remove();
});

/* EMAIL JS CODE STARTS */

$(document).on("click", ".primary-email-row .email-add-remove-row > .glyphicon-plus-sign", function () {
    var EmailRowLenght = $(".primary-email-row");

    if (EmailRowLenght.length == 2) {
        $(".primary-email-row .glyphicon-plus-sign").hide();
    } else {
        $(".primary-email-row:not(:eq(0)) .email-add-remove-row .glyphicon-plus-sign").show();
    }
    var clone = $(".primary-email-row:first").clone();
    clone.find("input[type='text']").val('');
    $(".primary-email-row").first().after(clone);
    $(".primary-email-row:not(:eq(0)) .email-add-remove-row .glyphicon-plus-sign").remove();
    $(".primary-email-row:not(:eq(0)) .email-add-remove-row .glyphicon-remove-sign").show();

});
$(document).on("click", ".primary-email-row .email-add-remove-row .glyphicon-remove-sign", function () {
    var EmailRowLenght = $(".primary-email-row");

    if (EmailRowLenght.length == 3 || EmailRowLenght.length == 2) {
        $(".primary-email-row .glyphicon-plus-sign").show();
    } else {
        $(".primary-email-row .glyphicon-plus-sign").hide();
    }
    $(this).parents(".primary-email-row").remove();
});


//  Phone Number Full Div Clone

// $(document).on("click", ".primary-tenant-phone-row .fa-plus-circle", function () {
//     var phoneRowLenght = $(".primary-tenant-phone-row");
//
//     if (phoneRowLenght.length == 2) {
//         $(".primary-tenant-phone-row .fa-plus-circle").hide();
//     } else {
//         $(".primary-tenant-phone-row:not(:eq(0)) .fa-plus-circle").show();
//     }
//
//     var clone = $(".primary-tenant-phone-row:first").clone();
//     clone.find('input[type=text]').val('');
//     clone.find(".calander").
//     removeClass('hasDatepicker').
//     removeData('datepicker').
//     unbind().datepicker({
//         dateFormat: jsDateFomat,
//         setDate: $.datepicker.formatDate(jsDateFomat, new Date())
//     });
//     $(".primary-tenant-phone-row").first().after(clone);
//     $(".primary-tenant-phone-row:not(:eq(0))  .fa-plus-circle").remove();
//     $(".primary-tenant-phone-row:not(:eq(0))  .fa-minus-circle").show();
// });
//
// $(document).on("click", ".primary-tenant-phone-row .fa-minus-circle", function () {
//     var phoneRowLenght = $(".primary-tenant-phone-row");
//
//     if (phoneRowLenght.length == 3 || phoneRowLenght.length == 2) {
//         $(".primary-tenant-phone-row .fa-plus-circle").show();
//     } else {
//         $(".primary-tenant-phone-row .fa-minus-circle").hide();
//     }
//     $(this).parents(".primary-tenant-phone-row").remove();
// });

$(document).on("click",".primary-tenant-phone-row .fa-plus-circle",function(){
    var clone = $("#primary-tenant-phone-row-divId").clone().insertAfter("div.primary-tenant-phone-row:last");
    clone.find('input[type=text]').val('');
    clone.find('select').next().text('');
    clone.find('input[type=text]').next().text('');
    clone.find('select').val('');
    clone.find('.countycodediv').find('select').val(220);
    // var c = clone.find('.countycodediv').find('#country_code').val(220);

    $(".primary-tenant-phone-row:not(:eq(0))  .fa-plus-circle").hide();
    // $(".primary-tenant-phone-row:not(:eq(0))  .fa-times-circle").show();
    var phoneRowLenght = $(".primary-tenant-phone-row");
    clone.find(".fa-times-circle").show();
    console.log('phoneRowLenght>>>', phoneRowLenght);
    clone.find('.phone_format').mask('000-000-0000', {reverse: true});
    if (phoneRowLenght.length == 2) {
        // clone.find(".fa-times-circle").show();
        clone.find(".fa-plus-circle").hide();
    }else if (phoneRowLenght.length == 3) {
        clone.find(".fa-plus-circle").hide();
        $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").hide();
    }else{
        $(".primary-tenant-phone-row:not(:eq(0)) .fa-plus-circle").show();
    }
});

$(document).on("click",".primary-tenant-phone-row .fa-times-circle",function(){

    var phoneRowLenght = $(".primary-tenant-phone-row");
    if (phoneRowLenght.length >= 2) {
        $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").show();
    }else if (phoneRowLenght.length == 3) {
        $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").hide();
    }else{
        $(".primary-tenant-phone-row:not(:eq(0)) .fa-plus-circle").show();
    }

    $(this).parents(".primary-tenant-phone-row").remove();
});


//Emergency Contact Details Full Div Clone


$(document).on("click", ".primary-emergency-contact-row .fa-plus-circle", function () {
    var contactRowLength = $(".primary-emergency-contact-row");

    var clone = $(".primary-emergency-contact-row:first").clone();
    clone.find("#relation").remove();
    clone.find('input[type=text]').val('');
    $(".primary-emergency-contact-row").first().after(clone);
    $(".primary-emergency-contact-row:not(:eq(0))  .fa-plus-circle").remove();
    $(".primary-emergency-contact-row:not(:eq(0))  .fa-minus-circle").show();
});

$(document).on("click", ".primary-emergency-contact-row .fa-minus-circle", function () {
    var contactRowLength = $(".primary-emergency-contact-row");


    $(this).parents(".primary-emergency-contact-row").remove();
});

//Contact Credentials Details Full Div Clone


$(document).on("click", ".primary-contact-credentials-row .add-contact-icon", function () {
    var clone = $(".primary-contact-credentials-row:first").clone();
    clone.find('input[type=text]').val('');
    clone.find(".acquireDate").attr("id","").removeClass('hasDatepicker').removeData('datepicker').unbind().datepicker({
        dateFormat: jsDateFomat,
        setDate: $.datepicker.formatDate(jsDateFomat, new Date())
    });
    clone.find(".expirationDate").attr("id","").removeClass('hasDatepicker').removeData('datepicker').unbind().datepicker({
        dateFormat: jsDateFomat,
        setDate: $.datepicker.formatDate(jsDateFomat, new Date())
    });
    $(".primary-contact-credentials-row").first().after(clone);
    $(".primary-contact-credentials-row:not(:eq(0))  .add-contact-icon").remove();
    $(".primary-contact-credentials-row:not(:eq(0))  .minus-contact-icon").show();
});

$(document).on("click", ".primary-contact-credentials-row .minus-contact-icon", function () {
    $(this).parents(".primary-contact-credentials-row").remove();
});

// Notes Full Div Clone


$(document).on("click", ".primary-notes-row .fa-plus-circle", function () {
    var notesRowLenght = $(".primary-notes-row");

    if (notesRowLenght.length == 2) {
        $(".primary-notes-row .fa-plus-circle").hide();
    } else {
        $(".primary-notes-row:not(:eq(0)) .fa-plus-circle").show();
    }

    var clone = $(".primary-notes-row:first").clone();
    clone.find('input[type=text]').val('');
    $(".primary-notes-row").first().after(clone);
    $(".primary-notes-row:not(:eq(0))  .fa-plus-circle").remove();
    $(".primary-notes-row:not(:eq(0))  .fa-minus-circle").show();
});

$(document).on("click", ".primary-notes-row .fa-minus-circle", function () {
    var notesRowLenght = $(".primary-notes-row");

    if (notesRowLenght.length == 3 || notesRowLenght.length == 2) {
        $(".primary-notes-row .fa-plus-circle").show();
    } else {
        $(".primary-notes-row .fa-minus-circle").hide();
    }
    $(this).parents(".primary-notes-row").remove();
});

/**
 * Auto fill the city, state and country when focus loose on zipcode field.
 */


$("#contact_zipcode").focusout(function () {
    getZipCode('#contact_zipcode', $(this).val(), '#contact_city', '#contact_state', '#contact_country', '', '');
    // getAddressInfoByZip($(this).val());
});

/**
 * Auto fill the city, state and country when enter key is clicked.
 */
$("#contact_zipcode").keydown(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        getZipCode('#contact_zipcode', $(this).val(), '#contact_city', '#contact_state', '#contact_country', '', '');
    }
});


/**
 *
 *
 * tenant clone div
 */
//
//
// $(document).on("click",".primary-tenant-phone-row .fa-plus-circle",function(){
//     var clone = $(".primary-tenant-phone-row:first").clone();
//     clone.find('input[type=text]').val(''); //harjinder
//     $(".primary-tenant-phone-row").first().after(clone);
//     $(".primary-tenant-phone-row:not(:eq(0))  .fa-plus-circle").hide();
//     $(".primary-tenant-phone-row:not(:eq(0))  .fa-minus-circle").show();
//     var phoneRowLenght = $("#addTenant .primary-tenant-phone-row");
//     clone.find('.phone_format').mask('000-000-0000', {reverse: true});
//     if (phoneRowLenght.length == 2) {
//         clone.find(".fa-plus-circle").hide();
//     }else if (phoneRowLenght.length == 3) {
//         clone.find(".fa-plus-circle").hide();
//         $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").hide();
//     }else{
//         $(".primary-tenant-phone-row:not(:eq(0)) .fa-plus-circle").show();
//     }
// });
//
// $(document).on("click",".primary-tenant-phone-row .fa-minus-circle",function(){
//
//     var phoneRowLenght = $(".primary-tenant-phone-row");
//     if (phoneRowLenght.length == 2) {
//         $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").show();
//     }else if (phoneRowLenght.length == 3) {
//         $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").hide();
//     }else{
//         $(".primary-tenant-phone-row:not(:eq(0)) .fa-plus-circle").show();
//     }
//
//     $(this).parents(".primary-tenant-phone-row").remove();
// });

/**
 *
 * end tenant Clone
 */





$("#contactvalidations").validate({
    rules: {

        carrier: {
            required:true
        },
        salutation: {
            required:true
        },
        first_name: {
            required:true
        },
        last_name: {
            required:true
        },
        'email[]': {
            required: true,
            email: true,
        },
        'email1[]': {
            required: true,
            email: true,
        },
        'carrier[]': {
            required: true,
        },
        'phoneNumber[]': {
            required: true,
        },
        referral: {
            required:true
        },
        gender: {
            required:true
        }
    },
    submitHandler: function (e) {
   
        var form = $('#contactvalidations')[0];
        var formData = new FormData(form);
        var count = file_library.length;
        var custom_field = [];
        var data = convertSerializeDatatoArray();
        $(".custom_field_html input").each(function(){
            var data = {'name':$(this).attr('name'),'value':$(this).val(),'id':$(this).attr('data_id'),'is_required':$(this).attr('data_required'),'data_type':$(this).attr('data_type'),'default_value':$(this).attr('data_value')};
            custom_field.push(data);
        });
        var custom_field = JSON.stringify(custom_field);
        
        $.each(file_library, function (key, value) {
                if (compareArray(value, data) == 'true') {
                    formData.append(key, value);
                }
            });


        formData.append('action', 'InsertData');
        formData.append('class', 'contactPopup');
        formData.append('custom_field', custom_field);
        // console.log(count);return false;
        $.ajax({
            type: 'post',
            url: '/Ajax-People/AddContact',
            data: formData, 
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(xhr) {
                // checking custom field validations
                $(".custom_field_html input").each(function() {
                    var res = validateCustomField($(this).val(), $(this).attr('data_required'), $(this).attr('data_type'), this, this.id);
                    if(res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function (response) {
                // console.log('nggg');
                var response = JSON.parse(response);



                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    onTop(true);
                    //  $("select[name='first_name']").val(response.data.hob.hobby);
                    // $("#first_name".val('');
                    setTimeout(function () {
                        window.location.href = window.location.origin+"/People/ContactListt";

                    }, 500);

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 503) {
                    toastr.warning(response.message);
                }
            }
        });
    }
});

/*File Upload Code*/
 function addFileData()
 {

    var length = $('#photo_video_uploads > div').length;
    if(length > 0) {
        var data = convertSerializeDatatoArray();
        var uploadform = new FormData();
        uploadform.append('class', 'propertyFilelibrary');
        uploadform.append('action', 'file_library');
        uploadform.append('property_id', property_unique_id);
        var count = file_library.length;
     //   console.log(count);
        $.each(file_library, function (key, value) {
            if(compareArray(value,data) == 'true'){
                uploadform.append(key, value);
            }
            if(key+1 === count){
                saveLibraryFiles(uploadform);
            }
        });
    } else {

    }
}



function saveLibraryFiles(uploadform){
     
    $.ajax({
        type: 'post',
        url: '/property/file_library',
        data:uploadform,
        processData: false,
        contentType: false,
        success: function (response) {
            var response = JSON.parse(response);
            if(response.code == 200){
                $('#photo_video_uploads').html('');

              //  $('#propertFileLibrary-table').trigger('reloadGrid');
                //toastr.success('Files uploaded successfully.');
            } else if(response.code == 500){
                toastr.warning(response.message);
                onTop(true);
            } else {
                toastr.success('Error while uploading files.');
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}




/** Download sample ContactListing excel */

var base_url = window.location.origin;
$(document).on("click", '#exportSampleExcel', function () {
    window.location.href = base_url + "/Ajax-People/AddContact?action=exportSampleExcel";
});

/** Export AccountChargeCode type excel  */
$(document).on("click", '#exportContactButton', function () {
    var status = $("#jqGridStatus option:selected").val();
    var table = 'users';
    window.location.href = base_url + "/Ajax-People/AddContact?status=" + status + "&&table=" + table + "&&action=exportExcel";
});

/** Show import excel div on import excel button click */
$(document).on('click', '#importChargeCodeButton', function () {
    $("#import_file-error").text('');
    $("#import_file").val('');
    $('#import_contact_div').show(500);
});

/** Hide add new Contact div on cancel button click */
$(document).on("click", "#add_charge_code_cancel_btn", function (e) {
    e.preventDefault();
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $("#add_charge_code_div").hide(500);

            }
        }
    });
});
/**
 * submit button import
 */


/** Hide add new Contact div on cancel button click */
$(document).on("click", "#flagCancel", function (e) {
    e.preventDefault();
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $("#flagFormDivcontact").hide(500);
            }
        }
    });
});

/** Hide import excel div on cancel button click */
$(document).on("click", "#import_unit_cancel_btn", function (e) {
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $("#import_contact_div").hide(500);
            }
        }
    });
});


/** Importing ContactListing excel */

$("#importContactFormId").validate({
    rules: {
        import_file: {
            required: true
        },
    },

    messages: {

        import_file:{

            required: "*Please Choose File",

        }

    },
    submitHandler: function (form) {
        event.preventDefault();

        // var formData = $('#importPropertyForm').serializeArray();
        var myFile = $('#import_file').prop('files');
        var myFiles = myFile[0];
        var formData = new FormData();
        formData.append('file', myFiles);
        formData.append('class', 'contactPopup');
        formData.append('action', 'importExcel');
        $.ajax
        ({
            type: 'post',
            url: '/Ajax-People/AddContact',
            processData: false,
            contentType: false,
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    onTop(true);
                    $("#import_contact_div").hide(500);
                    $('#ContactListing-table').trigger('reloadGrid');
                } else if (response.status == 'failed' && response.code == 503) {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});

/**
 * jqGrid function to format status
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function statusFormatter (cellValue, options, rowObject){
    if (cellValue == '1')
        return "Active";
    else if(cellValue == '0')
        return "InActive";
    else
        return '';
}

/**
 * jqGrid function to format status
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function statusFmatter (cellvalue, options, rowObject){
    //  alert(rowObject);
    if (cellvalue == 1)
        return "Active";
    else
        return "Archive";
}


function statusFmatter1 (cellvalue, options, rowObject){
    //alert(cellvalue);
    // alert(rowObject);
    if(rowObject !== undefined) {
        var select = '';
        if($(cellvalue).attr('data_id') == '1' || $(cellvalue).attr('data_id') == '2' || $(cellvalue).attr('data_id') == '3') return select;
        if(rowObject['Status'] == '1')  select = ['Edit', 'Email', 'Email History', 'Text', 'Text Histroy', 'Add IN-Touch','In-Touch History', 'Flag Blank', 'File Library', 'Notes & History', 'Run Background Check', 'Convert To Tenant', 'Archive', 'Delete Contact', 'Print Envelope'];
        if(rowObject['Status'] == '0' || rowObject.Status == '')  select = ['Edit', 'Email', 'Email History', 'Text', 'Text Histroy', 'Add IN-Touch','In-Touch History', 'Flag Blank', 'File Library', 'Notes & History', 'Run Background Check', 'Convert To Tenant', 'Active', 'Delete Contact', 'Print Envelope'];;
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        // alert(data);
        return data;
    }
}

if(localStorage.getItem('ElasticSearch')){
    var elasticSearchData = localStorage.getItem('ElasticSearch');
    setTimeout(function(){
        var grid = $("#ContactListing-table"),f = [];
        f.push({field: "users.id", op: "eq", data: elasticSearchData,int:'true'});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        localStorage.removeItem('ElasticSearch');
    },1500);
}

/**
 * View Data
 * jqGrid Initialization function
 * @param status
 */
jqGridContact('All');
function jqGridContact(status, deleted_at) {
    var table = 'users';
    var columns = ['Contact Name', 'Phone', 'Email', 'Date Created', 'Status', 'Action'];
    //var select_column = ['Edit', 'Email', 'Email History', 'Text', 'Text Histroy', 'Add IN-Touch','In-Touch History', 'Flag Blank', 'File Library', 'Notes & History', 'Run Background Check', 'Convert To Tenant', 'Archive Contact', 'Delete Contact', 'Print Envelope'];
    var select_column = [];
    var joins = [];
    //var joins = [{table:'company_accounting_charge_code',column:'credit_account',primary:'id',on_table:'company_credit_accounts'},{table:'company_accounting_charge_code',column:'debit_account',primary:'id',on_table:'company_debit_accounts'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = [];
    var extra_where = [{column:'user_type',value:'5',condition:'=',table:'users'}];

    var pagination=[];
    var columns_options = [
        {
            name: 'Contact Name',
            index: 'name',
            width: 90,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table,
            formatter: propertyName,
            classes: 'pointer'
        },
        //{name:'Contact Name',index:'name', width:100,searchoptions: {sopt: conditions},table:table},
        {name: 'Phone', index: 'phone_number', width: 100, searchoptions: {sopt: conditions}, table: table},
        {name: 'Email', index: 'email', class:"pointer",width: 100, searchoptions: {sopt: conditions}, table: table},
        {name: 'Date Created', index: 'created_at', width: 100,class:"pointer" , searchoptions: {sopt: conditions}, table: table},
        {name:'Status',index:'status',class:"pointer", width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter},
        {name: 'Action', index: 'select', title: false,width: 80, align: "right", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true,formatter: statusFmatter1, edittype: 'select',search:false, editoptions: { dataInit: function( elem ){
                } }}
    ];
    var ignore_array = [];
    jQuery("#ContactListing-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "users",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where:extra_where,
            deleted_at: deleted_at
        },
        viewrecords: true,
        //sortname: 'updated_at',
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Contacts",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {},
        {},
        {},
        {top: 200, left: 200, drag: true, resize: false}
    );
}

    /*Delete row data from select option*/

$(document).on('change', '#ContactListing-table .select_options', function() {
    var action = this.value;
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    var row_num = $(this).parent().parent().index() ;
    if (opt == 'Delete Contact' || opt == 'DELETE CONTACT') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/Ajax-People/AddContact',
                        data: {class: 'contactPopup', action: "deleteDataContact", id: id},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                onTop(true);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                }
                $('#ContactListing-table').trigger('reloadGrid');
            }
        });
    }else if(opt == 'EDIT' || opt == 'Edit') {
        window.location.href = '/People/ContactModule?id='+id+'&edit=1';
    }else if(opt == 'EMAIL' || opt == 'Email'){
        window.location.href = '/Communication/ComposeEmail';
    }else if(opt == 'Email History' || opt == 'EMAIL HISTORY'){
        window.location.href = '/Communication/SentEmails';
    }else if(opt == 'TEXT' || opt == 'Text'){
        //window.location.href = '/Communication/AddTextMessage';
        window.location.href = '/Communication/TextMessage';
    }else if(opt == 'TEXT HISTROY' || opt == 'Text Histroy'){
        window.location.href = '/Communication/TextMessage';
    }else if(opt == 'ADD IN-TOUCH' || opt == 'Add IN-Touch'){
        window.location.href = '/Communication/InTouch';
    }else if(opt == 'IN-TOUCH HISTORY' || opt == 'In-Touch History'){
        window.location.href = '/Communication/InTouch';
    }else if(opt == 'FLAG BLANK' || opt == 'Flag Blank'){
        window.location.href = '/People/ContactModule';
    }else if(opt == 'FILE LIBRARY' || opt == 'File Library'){
        window.location.href = '/People/ContactModule';
    }else if(opt == 'NOTES &amp; HISTORY' || opt == 'Notes &amp; History'){
        window.location.href = '/People/ContactModule';
    }else if(opt == 'RUN BACKGROUND CHECK' || opt == 'Run Background Check'){
        alert("Run Background Check");
    }else if(opt == 'CONVERT TO TENANT' || opt == 'Convert To Tenant'){
        window.location.href = '/Tenantlisting/add';
    }else if(opt == 'ARCHIVE' || opt == 'Archive'){
        bootbox.confirm("Do you want to "+action+"  this user?", function (result) {
            if (result == true) {
                changeStatusApexNewUser(id,action);
            } else {
                window.location.href =  window.location.origin+'/People/ContactListt'
            }
        });
    }else if(opt == 'ACTIVE' || opt == 'Active'){
        bootbox.confirm("Do you want to "+action+"  this user?", function (result) {
            if (result == true) {
                changeStatusApexNewUser(id,action);
            } else {
                window.location.href =  window.location.origin+'/People/ContactListt'
            }
        });
    }else if(opt == 'PRINT ENVELOPE' || opt == 'Print Envelope'){
        alert("Print");
    }
});

/**/

/**/
function changeStatusApexNewUser(id,action) {
    //alert(action);
    $.ajax({
        type: 'post',
        url: '/Ajax-People/AddContact',
        data: {
            class: "contactPopup",
            action: "changeStatusApexNewUser",
            contactuser_id: id,
            status_type: action,
        },
        success: function (response) {
         //   alert("hghgh");
            var response = JSON.parse(response);
            // alert(response);
           // console.log(response);
            if (response.status == 'success' && response.code == 200) {
                toastr.success("The status has been changed.")
                onTop(true);
            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
            }
            $('#ContactListing-table').trigger( 'reloadGrid' );
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}


    /* View Contact Detail*/



    function getContactDetail (){
        var ids = $(".apxpg-main #contact_id").val();


        $.ajax({
            type: 'POST',
            url: '/Ajax-People/AddContact',
            data: {
                class: "contactPopup",
                action: "getContactDetail",
                id: ids
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.code == 200) {
                    if(response.data.salutation == 1){
                        var salutationValue = "Dr.";
                    }
                    else if(response.data.salutation == 2){
                        var salutationValue = "Mr.";
                    }
                    else if(response.data.salutation == 3){
                        var salutationValue = "Mrs.";
                    }
                    else if(response.data.salutation == 4){
                        var salutationValue = "Mr. & Mrs.";
                    }
                    else if(response.data.salutation == 5){
                        var salutationValue = "Mr. & Mrs.";
                    }
                    else if(response.data.salutation == 6){
                        var salutationValue = "Sir";
                    }
                    else if(response.data.salutation == 7){
                        var salutationValue = "Madam";
                    }
                    else if(response.data.salutation == 8){
                        var salutationValue = "Brother";
                    }
                    else if(response.data.salutation == 9){
                        var salutationValue = "Sister";
                    }
                    else if(response.data.salutation == 10){
                        var salutationValue = "Father";
                    }
                    else if(response.data.salutation == 9){
                        var salutationValue = "Mother";
                    }

                    $("#viewContactData #salulation_contact").html(salutationValue);
                    $("#viewContactData #contact_notes_phone").html(response.data.phone_number_note);
                    $("#viewContactData #contact_first_name").html(response.data.first_name);
                    $("#viewContactData #contact_address1").html(response.data.address1);
                    $("#viewContactData #contact_middle_name").html(response.data.middle_name);
                    $("#viewContactData #contact_address2").html(response.data.address2);
                    $("#viewContactData #contact_last_name").html(response.data.last_name);
                    $("#viewContactData #contact_address3").html(response.data.address3);
                    $("#viewContactData #contact_address4").html(response.data.address4);
                    $("#viewContactData #contact_madien_name").html(response.data.maiden_name);
                    $("#viewContactData #contact_nick_name").html(response.data.nick_name);
                    $("#viewContactData #contact_city").html(response.data.city);
                    if(response.data.gender == 1)
                    {
                        var genderValue ="Male";
                    }
                    else if(response.data.gender == 2)
                    {
                        var genderValue ="Female";
                    }
                    else if(response.data.gender == 3)
                    {
                        var genderValue ="Prefer Not To Say";
                    }
                    else if(response.data.gender == 4)
                    {
                        var genderValue ="Other";
                    }
                    $("#viewContactData #contact_gender").text(genderValue);
                    $("#viewContactData #contact_country").html(response.data.country);
                    $("#viewContactData #contact_ethnicity").html(response.data.ethnicity_name);
                    $("#viewContactData #contact_hobbies").html(response.data.hobbies);
                    $("#viewContactData #contact_marital_status").html(response.data.marital_status);
                    $("#viewContactData #contact_ssn").html(response.ssn);
                    $("#viewContactData #contact_zip").html(response.data.zipcode);
                    // if(response.data.veteran_status == 2){
                    //     var veteranSatus = "Newly Separated Veteran";
                    // }else if(response.data.veteran_status == 3){
                    //     var veteranSatus = "Other Protected Veterans";
                    // }else if(response.data.veteran_status == 4){
                    //     var veteranSatus = "Special Disabled Veteran";
                    // }else if(response.data.veteran_status == 5){
                    //     var veteranSatus = "Veteran";
                    // }else if(response.data.veteran_status == 6){
                    //     var veteranSatus = "Veteran of Vietnam Era";
                    // }else if(response.data.veteran_status == 7){
                    //     var veteranSatus = "dfs";
                    // }else if(response.data.veteran_status == 8){
                    //     var veteranSatus = "Veteran1";
                    // }
                    $("#viewContactData #contact_veteran").html(response.data.veteran_name);
                    $("#viewContactData #contact_dob").html(response.data.dob);

                    // console.log(response.data);
                    getCustomFieldForViewMode(response.data.custom_fields);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

/*To reset form */

$(document).on("click",".contact_reset",function(){
    bootbox.confirm({
        message: "Do you want to Reset ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                window.location.href = window.location.origin+'/People/AddContact';
            }
        }
    });

})


/*Cancel*/

/*To reset form */

$(document).on("click",".contact_cancel",function(){
    bootbox.confirm({
        message: "Do you want to Cancel ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                window.location.href = window.location.origin+'/People/ContactListt';
            }
        }
    });

});

    /* view Full detail Of Emergency Contact*/

    getEmergencyContactDetail ()
    function getEmergencyContactDetail (){
        var id = $(".apxpg-main #contact_id").val();
        // alert(id);

        $.ajax({
            type: 'POST',
            url: '/Ajax-People/AddContact',
            data: {
                class: "contactPopup",
                action: "getEmegencyContactDetail",
                id:id
                // data :window.location.pathname
            },

            success: function (response) {
                var response = JSON.parse(response);

                if (response.code == 200) {

                    $("#viewContactData #contact_name").html(response.data.emergency_contact_name);
                    $("#viewContactData #contact_email").html(response.data.emergency_email);
                    $("#viewContactData #contact_country_code").html(response.data.emergency_country_code);
                    $("#viewContactData #contact_phone").html(response.data.emergency_phone_number);
                    $("#viewContactData #contact_relation").html(response.data.emergency_relation);

                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /* view Full detail Of Crediental Detail*/

    getContactCredential()
    function getContactCredential(){
        var id = $(".apxpg-main #contact_id").val();
        // alert(id);

        $.ajax({
            type: 'POST',
            url: '/Ajax-People/AddContact',
            data: {
                class: "contactPopup",
                action: "getContactCredentialDetail",
                id:id
                // data :window.location.pathname
            },

            success: function (response) {
                var response = JSON.parse(response);
                //alert(id);


              //  console.log(response.data);
                if (response.code == 200) {
                    $("#viewContactData #contact_credential_name").html(response.data.credential_name);
                    $("#viewContactData #contact_credential_type").html(response.data.credential_type);
                    $("#viewContactData #contact_acquire_date").html(response.data.acquire_date);
                    $("#viewContactData #contact_expiration_date").html(response.data.expire_date);
                    $("#viewContactData #contact_notice_period").html(response.data.notice_period);
                    // $("#viewContactData #contact_relation").html(response.data.emergency_relation);
                    //
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /* Getting Building Property And Unit*/

    $(document).on("change","#contactvalidations #building",function (e) {
        e.preventDefault();
        getUnitsByBuildingsID($("#contactvalidations #property").val() , $(this).val());
        return false;
    });

    $(document).on("change","#contactvalidations #property",function (e) {
        e.preventDefault();
        getBuildingsByPropertyID($(this).val());
        return false;
    });




    function getBuildingsByPropertyID(propertyID){
        $.ajax({
            type: 'post',
            url: '/Ajax-People/AddContact',
            data: {
                class: "contactPopup",
                action: "getContactBuildingDetail",
                propertyID: propertyID
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    if (data.data.length > 0){
                        var buildingOption = "<option value='0'>Select</option>";
                        $.each(data.data, function (key, value) {
                            buildingOption += "<option value='"+value.id+"' data-id='"+value.building_id+"'>"+value.building_name+"</option>";
                        });
                        $('#contactvalidations #building').html(buildingOption);
                    }
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    function getUnitsByBuildingsID(propertyID, buildingID){
        $.ajax({
            type: 'post',
            url: '/Ajax-People/AddContact',
            data: {
                class: "contactPopup",
                action: "getContactUnitDetail",
                propertyID: propertyID,
                buildingID: buildingID
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    if (data.data.length > 0){
                        var unitOption = "<option value=''>Select</option>";
                        $.each(data.data, function (key, value) {
                            var uniName = value.unit_prefix+"-"+value.unit_no;
                            unitOption += "<option value='"+value.id+"' data-id='"+value.unit_prefix+"'>"+uniName+"</option>";
                        });
                        $('#contactvalidations #unit').html(unitOption);
                    }
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

        }


/**
 *
 * count Number of tenants owner others vendors
 */
getCount()
function getCount(){
     // var id = $(".apxpg-main #contact_id").val();


    $.ajax({
        type: 'POST',
        url: '/Ajax-People/AddContact',
        data: {
            class: "contactPopup",
            action: "getcount"

        },

        success: function (response) {

            var response = JSON.parse(response);
            var data1 = response.data;
            if (response.code == 200) {
                $("#viewCount #tenantCountNo").text(data1.tenant.count);
                $("#viewCount #ownersCountNo").text(data1.owners.count);
                $("#viewCount #vendorCountNo").text(data1.vendors.count);
                $("#viewCount #userCountNo").text(data1.users.count);
               $("#viewCount #otherCountNo").html(data1.others.count);

            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

/**
 * Ends count
 *
 */

/**
 * jqGrid Initialization function
 * @param status
 */

jqGrid("All");
function jqGrid(status) {
    var table = 'contact_notes';
    var contact_id=$(".contact_id").val();
    // var contact_id = property_unique_id;
//    alert(contact_id);
    var columns = ['contact_notes','created_at','record_status'];
    var select_column = ['Edit','Deactivate','Delete'];
    var joins = [];
    var extra_where = [{column:'user_id',value:contact_id,condition:'=',table:'contact_notes'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var columns_options = [
        {name:'Contact Notes',index:'contact_notes', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Date',index:'created_at', width:100,searchoptions: {sopt: conditions},table:table},
        {name:'Status',index:'record_status', width:100,searchoptions: {sopt: conditions},table:table},
    ];
    var ignore_array = [];
    jQuery("#notes-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "contact_notes",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at: false,
            extra_where:extra_where

        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: 'desc',
        sorttype:'',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Notes",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {},
        {},
        {},
        {top:200,left:200,drag:true,resize:false}
    );
}

/**
 *
 * jqgrid Files Library
 */

jqGridFiles("All");
function jqGridFiles(status) {
     var table = 'tenant_file_library';
    var contact_id=$(".contact_id").val();

    var columns = ['Name','Preview'];
    var select_column = ['Edit','Deactivate','Delete'];
    var joins = [];
    var extra_where = [{column:'user_id',value:contact_id,condition:'=',table:'tenant_file_library'}];

    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var columns_options = [
        {name:'Name',index:'name',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
       // {name:'Name',index:'name', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'extension',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
      //  {name:'Name',index:'name', width:90,align:"center",searchoptions: {sopt: conditions},table:table,formatter:imageFormatter},
       // {name:'Date',index:'created_at', width:100,searchoptions: {sopt: conditions},table:table},
        //{name:'Status',index:'status', width:100,searchoptions: {sopt: conditions},table:table},
    ];
    var ignore_array = [];
    jQuery("#contactFileLibrary-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "tenant_file_library",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:true,
            extra_where:extra_where

        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: 'desc',
        sorttype:'',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Contact Files",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {},
        {},
        {},
        {top:200,left:200,drag:true,resize:false}
    );
}


/**
 *
 *
 * Edit JQGrid File Library
 */
jqGridEditFilesLibrary("All");
function jqGridEditFilesLibrary(status) {
    var table = 'tenant_file_library';
    var contact_id=$(".contact_id").val();
   // var contact_id = property_unique_id;
    //alert(contact_id);
    var columns = ['Name','Preview'];
    var select_column = ['Edit','Deactivate','Delete'];
    var joins = [];
    var extra_where = [{column:'user_id',value:contact_id,condition:'=',table:'tenant_file_library'}];
    //alert(extra_where);
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var columns_options = [
        {name:'Name',index:'name',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        // {name:'Name',index:'name', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'extension',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
        //  {name:'Name',index:'name', width:90,align:"center",searchoptions: {sopt: conditions},table:table,formatter:imageFormatter},
        // {name:'Date',index:'created_at', width:100,searchoptions: {sopt: conditions},table:table},
        //{name:'Status',index:'status', width:100,searchoptions: {sopt: conditions},table:table},
    ];
    var ignore_array = [];
    jQuery("#contactEditFileLibrary-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "tenant_file_library",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:true,
            extra_where:extra_where

        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: 'desc',
        sorttype:'',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Contact Files",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {},
        {},
        {},
        {top:200,left:200,drag:true,resize:false}
    );
}

/**
 *
 * Flag Listing
 */

jqGridFlagListing('All');
function jqGridFlagListing(status) {
    var contact_id=$(".contact_id").val();
    // var contact_id = property_unique_id;
    var table = 'flags';
    var columns = ['Date','Flag Name', 'Phone Number', 'Flag Reason', 'Completed', 'Note', 'Action'];
    var select_column = ['Edit','Delete','Completed'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"]
    // var extra_where = [{column:'object_id',value:unit_id,condition:'='},{column:'object_type',value:'unit',condition:'='}];
    extra_where = [{column:'object_id',value:contact_id,condition:'='},{column:'object_type',value:'contact',condition:'='}];
    // var extra_where = [{column:'user_id',value:contact_id,condition:'=',table:'flags'}];
    // alert(extra_where);
    var extra_columns = ['flags.deleted_at'];
    var columns_options = [
        {name:'Date',index:'date',align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date',editable:true,},
        {name:'Flag Name',index:'flag_name',searchoptions: {sopt: conditions},table:table},
        {name:'Phone',index:'flag_phone_number', width:200,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,table:table},
        {name:'Flag Reason',index:'flag_reason', width:180,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Completed',index:'completed',width:180, align:"center",searchoptions: {sopt: conditions},table:table,formatter: completedFormatters},
        {name:'Note',index:'flag_note',width:200, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'select',width:200, title: false,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter: actionFmatters}
    ];
    var ignore_array = [];
    jQuery("#contactFlaglist-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'true',
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Flags",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}

function completedFormatters (cellValue, options, rowObject){
    if (cellValue == 1)
        return "True";
    else if(cellValue == 0)
        return "False";
    else
        return '';
}

function actionFmatters (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';
        if(rowObject.Completed == 1) select = ['Edit','Delete'];
        if(rowObject.Completed == 0) select = ['Edit','Delete','Completed'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }

}

$(document).on('change', '#contactFlaglist-table .select_options', function() {
    var opt = $(this).val();
    // var id =  $(".contact_id").val();
    var id = $(this).attr('data_id');
    var row_num = $(this).parent().parent().index() ;
    if (opt == 'Edit' || opt == 'EDIT') {
        var validator = $( "#flagsForm" ).validate();
        //validator.resetForm();
        $('#contactFlag-table').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        jQuery('#contactFlag-table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('#contactFlag-table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
        $.ajax({
            type: 'post',
            url: '/flag-ajax',
            data: {id: id,class:'Flag',action:'get_flags'},
            success: function (response) {
                $("#flagFormDivcontact").show(500);
                $('#flagsSaveBtnId').text('Update');
                var data = $.parseJSON(response);
                if (data.code == 200) {
                    $("#flag_id").val(data.data.id);

                    $.each(data.data, function (key, value) {
                        $('#flag_' + key ).val(value);
                    });
                } else if (data.code == 500){
                    toastr.error(data.message);
                } else{
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        //$("#portfolio-table").trigger('reloadGrid');
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/flag-ajax',
                        data: {id: id,class:'Flag',action:'deleteFlag'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                $('#propertyFlag-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }

            }
        });
    } else if (opt == 'Completed' || opt == 'COMPLETED') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to complete this flag ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/flag-ajax',
                        data: {id: id,class:'Flag',action:'flagCompleted'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                $('#contactFlag-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }

            }
        });
    }
    $('.select_options').prop('selectedIndex',0);
});

/**
 * jqGridFlag Initialization function
 * @param status
 */
jqGridFlag('All');
function jqGridFlag(status) {
    var contact_id=$(".contact_id").val();
    // var contact_id = property_unique_id;
    var table = 'flags';
    var columns = ['Date','Flag Name', 'Phone Number', 'Flag Reason', 'Completed', 'Note', 'Action'];
    var select_column = ['Edit','Delete','Completed'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"]
   // var extra_where = [{column:'object_id',value:unit_id,condition:'='},{column:'object_type',value:'unit',condition:'='}];
    extra_where = [{column:'object_id',value:contact_id,condition:'='},{column:'object_type',value:'contact',condition:'='}];
   // var extra_where = [{column:'user_id',value:contact_id,condition:'=',table:'flags'}];
   // alert(extra_where);
    var extra_columns = ['flags.deleted_at'];
    var columns_options = [
        {name:'Date',index:'date',align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date',editable:true,},
        {name:'Flag Name',index:'flag_name',searchoptions: {sopt: conditions},table:table},
        {name:'Phone',index:'flag_phone_number', width:200,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,table:table},
        {name:'Flag Reason',index:'flag_reason', width:180,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Completed',index:'completed',width:180, align:"center",searchoptions: {sopt: conditions},table:table,formatter: completedFormatter},
        {name:'Note',index:'flag_note',width:200, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'select',width:200, title: false,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter: actionFmatter}
    ];
    var ignore_array = [];
    jQuery("#contactFlag-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'true',
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Flags",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}

/**
 * Flag JqGrid Ends
 *
 *
 */

/**
 *  function to format property name
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function propertyName(cellValue, options, rowObject) {
    if(rowObject !== undefined) {
        var flagValue = $(rowObject.Action).attr('flag');
        var id = $(rowObject.Action).attr('data_id');
        var flag = '';
        if (flagValue == 'yes') {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id='+id+'" ><img src="/company/images/Flag.png"></a></span>';
        } else {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }

}

/**
 * function to format document
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function imageFormatter(cellvalue, options, rowObject){

    if(rowObject !== undefined) {
        var path = upload_url+'company/'+rowObject.Location;
        var src = '';
        if (rowObject.Preview == 'xlsx') {
            src = upload_url + 'company/images/excel.png';
        } else if (rowObject.Preview == 'pdf') {
            src = upload_url + 'company/images/pdf.png';
        } else if (rowObject.Preview == 'docx') {
            src = upload_url + 'company/images/word_doc_icon.jpg';
        }else if (rowObject.Preview == 'txt') {
            src = upload_url + 'company/images/notepad.jpg';
        }
        return '<img class="img-upload-tab open_file_location" data-location="'+path+'" width=30 height=30 src="' + src + '">';
    }
}


function completedFormatter (cellValue, options, rowObject){
    if (cellValue == 1)
        return "True";
    else if(cellValue == 0)
        return "False";
    else
        return '';
}

function actionFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';
        if(rowObject.Completed == 1) select = ['Edit','Delete'];
        if(rowObject.Completed == 0) select = ['Edit','Delete','Completed'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }

}

/**/
$(document).on('click','#flagsSaveBtnId', function (e) {
    e.preventDefault();
    //checking custom field validation
    var formData = $('#flagFormDivcontact :input').serializeArray();
    // alert(formData);
    var id =  $(".contact_id").val();

    $.ajax({
        type: 'post',
        url: '/flag-ajax',
        data: {form: formData,class:'Flag',action:'create_flag',object_id:id,object_type:'contact'},
        success: function (response) {
            var response = JSON.parse(response);

            if(response.code == 200){
                $('#flagFormDivcontact').hide(500);
                jQuery('#contactFlag-table').find('tr:eq(0)').find('td:eq(0)').addClass("green_row_left");
                jQuery('#contactFlag-table').find('tr:eq(0)').find('td').last().addClass("green_row_right");
                $('#contactFlag-table').trigger('reloadGrid');
                $('#flagFormDivcontact :input').val('');
                toastr.success(response.message);
            } else if(response.code == 200) {
                toastr.error(response.message);
            }
        },
        error: function (data) {
           // console.log(data);
        }
    });
});
/**/
$(document).on('click','#tenant_listing tr td:not(:last-child)',function(e){
    e.preventDefault();
    var base_url = window.location.origin;
    var id = $(this).closest("tr").attr('id');
    window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
    /* ('tr td:not(:last-child)')*/
});

/**  List Action Functions  */
$(document).on('change', '#contactFlag-table .select_options', function() {
    var opt = $(this).val();
   // var id =  $(".contact_id").val();
    var id = $(this).attr('data_id');
   // alert(id);
    var row_num = $(this).parent().parent().index() ;
    if (opt == 'Edit' || opt == 'EDIT') {
        var validator = $( "#flagsForm" ).validate();
        //validator.resetForm();
        $('#contactFlag-table').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        jQuery('#contactFlag-table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('#contactFlag-table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
        $.ajax({
            type: 'post',
            url: '/flag-ajax',
            data: {id: id,class:'Flag',action:'get_flags'},
            success: function (response) {
                $("#flagFormDivcontact").show(500);
                $('#flagsSaveBtnId').text('Update');
                var data = $.parseJSON(response);
                if (data.code == 200) {
                    $("#flag_id").val(data.data.id);

                    $.each(data.data, function (key, value) {
                        $('#flag_' + key ).val(value);
                    });
                } else if (data.code == 500){
                    toastr.error(data.message);
                } else{
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        //$("#portfolio-table").trigger('reloadGrid');
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/flag-ajax',
                        data: {id: id,class:'Flag',action:'deleteFlag'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                $('#propertyFlag-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }

            }
        });
    } else if (opt == 'Completed' || opt == 'COMPLETED') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to complete this flag ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/flag-ajax',
                        data: {id: id,class:'Flag',action:'flagCompleted'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                $('#contactFlag-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }

            }
        });
    }
    $('.select_options').prop('selectedIndex',0);
});

// var getUrlParameter = function getUrlParameter(sParam) {
//     var sPageURL = window.location.search.substring(1),
//         sURLVariables = sPageURL.split('&'),
//         sParameterName,
//         i;
//
//     for (i = 0; i < sURLVariables.length; i++) {
//         sParameterName = sURLVariables[i].split('=');
//
//         if (sParameterName[0] === sParam) {
//             return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
//         }
//     }
// };

// var edit = getUrlParameter('edit');
// if(edit == '1')
// {
//     $("#contactEditMode").show();
// }

