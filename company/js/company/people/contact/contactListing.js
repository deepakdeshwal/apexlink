$(document).on('click','#tenant_listings tr td:not(:last-child)',function(e){
    e.preventDefault();
    var base_url = window.location.origin;
    var id = $(this).closest("tr").attr('id');
    window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
    /* ('tr td:not(:last-child)')*/
});

/** Tenant Listing Grid **/

jqGridTenantListing('All');
function jqGridTenantListing(status, deleted_at) {
    var table = 'users';
    var rentCurrSymbol = "Rent ("+currencySymbol+")";
    var balanceCurrSymbol = "Balance ("+currencySymbol+")";
    var columns = ['Tenant Name','Phone', 'Email', 'Created At'];
   // var columns = ['Tenant Name','Phone', 'Email', 'Property Name','Unit Number',rentCurrSymbol,balanceCurrSymbol,'Days Remaining','Status','Action'];
    //if(status===0) {
    var select_column = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Tenant Portal','Send Password Activation Mail','Deactivate Tenant account','HOA Violation Tracking','Print Envelope','Delete Tenant'];
    //}
    var joins = [{table:'users',column:'id',primary:'user_id',on_table:'tenant_property'},
        {table:'users',column:'id',primary:'user_id',on_table:'tenant_details'},
        {table:'users',column:'id',primary:'user_id',on_table:'tenant_lease_details'},
        {table:'users',column:'id',primary:'user_id',on_table:'tenant_phone'},
        {table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},
        {table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    // var extra_where = [{column:'user_type',value:'2',condition:'=',table:'users'},
    //     {column:'record_status',value:'1',condition:'=',table:'tenant_details'}];

    var extra_where = [{column:'user_type',value:'2',condition:'=',table:'users'},{column:'record_status',value:'1',condition:'=',table:'tenant_details'},{column:'record_status',value:'1',condition:'=',table:'tenant_lease_details'}];
    var pagination=[];
    var columns_options = [

        {name:'Tenant Name',index:'id', width:320,align:"left",searchoptions: {sopt: conditions},table:table,formatter:titleCase},
        {name:'Phone',index:'phone_number', width:310,searchoptions: {sopt: conditions},table:'tenant_phone',cellattr: function () { return '  data-toggle="tooltip" data-placement="top" class="phonenotehover" data-original-title="Tooltip on top">'; }},
        {name:'Email',index:'email', width:300, align:"left",searchoptions: {sopt: conditions},table:table},
        {name:'Created At',index:'created_at', width:320, align:"left",searchoptions: {sopt: conditions},table:table},
       // {name:'Property Name',index:'property_name', width:80, align:"left",searchoptions: {sopt: conditions},table:'general_property'},
       // {name:'Unit Number',index:'unit_id', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_property',formatter:getUnitDetails},
       // {name:'Rent (USh)',index:'rent_amount', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details'},
       // {name:'Balance (USh)',index:'balance', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details',formatter:statusFormatter3},
       // {name:'Days Remaining',index:'days_remaining', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details',formatter:statusFormatter2},
       // {name:'Status',index:'status', width:80,align:"left",searchoptions: {sopt: conditions},table:'tenant_details',formatter:statusFormatter},
       // {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},

    ];
    var ignore_array = [];
    jQuery("#tenant_listings").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "users",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:true,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'users.updated_at',
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Tenants",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:200,left:200,drag:true,resize:false} // search options
    );
}
function titleCase( cellvalue, options, rowObject) {
    if (cellvalue !== undefined) {

        var string = "";
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/getUserNameById',
            data: {
                class: "TenantAjax",
                action: "getUserNameById",
                id: cellvalue
            },
            async: false,
            success: function (response) {
                var res = JSON.parse(response);
                if (res.status == "success") {
                    string = res.data;
                }
            }
        });
        return string;
    }
}
function getUnitDetails( cellvalue, options, rowObject){
    var string = "";
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getUnitById',
        data: {
            class: "TenantAjax",
            action: "getUnitById",
            id: cellvalue
        },
        async: false,
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == "success") {
                string = res.data.unit_prefix+"-"+res.data.unit_no;
            }
        }
    });
    return string;
}

function statusFormatter (cellValue, options, rowObject){

    if (cellValue == 0)
        return "Inactive";
    else if(cellValue == '1')
        return "Active";
    else if(cellValue == '2')
        return "Evicting";
    else if(cellValue == '3')
        return "In-Collection";
    else if(cellValue == '4')
        return "Bankruptcy";
    else if(cellValue == '5')
        return "Evicted";
    else
        return '';

}
function statusFormatter2 (cellValue, options, rowObject){
    today=new Date();
    dt1 = new Date(today);
    dt2 = new Date(cellValue);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
}
function statusFormatter3 (cellValue, options, rowObject){
    return 1000;
}
function statusFmatter1 (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';
        if(rowObject['Status'] == '0')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Tenant Portal','Send Password Activation Mail','Deactivate Tenant account','HOA Violation Tracking','Print Envelope','Delete Tenant'];
        if(rowObject['Status'] == '1')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Tenant Portal','Send Password Activation Mail','Deactivate Tenant account','HOA Violation Tracking','Print Envelope','Delete Tenant'];
        if(rowObject['Status'] == '2')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Tenant Portal','Send Password Activation Mail','Deactivate Tenant account','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
        if(rowObject['Status'] == '3')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
        if(rowObject['Status'] == '4')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Tenant Portal','Send Password Activation Mail','Deactivate Tenant account','HOA Violation Tracking','Print Envelope','Delete Tenant'];
        if(rowObject['Status'] == '5')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
        if(rowObject['Status'] == '')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
        var data = '';

        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }

        return data;
    }
}


/*Owner Listing*/
$(document).on('click','#owner_listings tr td:not(:last-child)',function(e){
    e.preventDefault();
    var base_url = window.location.origin;
    var id = $(this).closest("tr").attr('id');
    window.location.href = base_url + '/People/ViewOwner?id='+id;
});
/**
 * jqGrid Initialization function
 * @param status
 */
jqGridOwnerListing(1);
function jqGridOwnerListing(status, deleted_at) {
    var table = 'users';
    var columns = ['Owner Name','Phone', 'Email','Date Created'];
    var select_column = ['Edit','Email','Text','Work Order','Add In-touch','In-Touch History','Owner Draw','Owner Contribution','Owner statement','Owner Notes','File Library','Flag Bank','Email History','Text History','Run Background Check','Owner Portal','Send Password Activation Email','Owner Complaints','Archive Owner','Resign Owner','DeActivate Account','Print Envelope','Delete Owner'];

    var joins = [{table:'users',column:'id',primary:'user_id',on_table:'owner_details'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'user_type',value:'4',condition:'=',table:'users'}];
    var columns_options = [
        {name:'Owner Name',index:'name',title:false, width:310,align:"center",searchoptions: {sopt: conditions},table:table,formatter: ownerName,classes: 'pointer',attr:[{name:'flag',value:'owner'}]},
        // {name:'Owner Name',index:'name', width:90,align:"left",searchoptions: {sopt: conditions},table:table, classes:'pointer'},
      //  {name:'Company',index:'company_name', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Phone',index:'phone_number', width:320,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'phone_number_format'},
        {name:'Email',index:'email', width:310, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Date Created',index:'created_at', width:310, align:"center",searchoptions: {sopt: conditions},table:table},
     //   {name:'Owner\'s Portal',index:'owners_portal', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:ownersPortal},
       // {name:'Status',index:'status', width:80,align:"left",searchoptions: {sopt: conditions},table:'owner_details',formatter:statusFormatterr},
       // {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatters1},

    ];
    var ignore_array = [];

    jQuery("#owner_listings").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "users",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:true,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'users.updated_at',
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: 10,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Owners",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {top:200,left:200,drag:true,resize:false}
    );
}

// function statusFormatterr (cellValue, options, rowObject){
//
//     if (cellValue == '0' || cellValue == 0)
//         return "Inactive";
//     else if(cellValue == '1')
//         return "Active";
//     else if(cellValue == '2')
//         return "Archive";
//     else if(cellValue == '3')
//         return "Delete";
//     else if(cellValue == '4')
//         return "Past";
//     else
//         return '';
//
// }
/**
 * Function to format owner name
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function ownerName(cellValue, options, rowObject) {
    if(rowObject !== undefined) {
        var flagValue = $(rowObject.Action).attr('flag');
        var id = $(rowObject.Action).attr('data_id');
        var flag = '';
        if (flagValue == 'yes') {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a id="flag" href="javascript:void(0);" data_url="/Property/PropertyView?id="' + id + '"><img src="/company/images/Flag.png"></a></span>';
        } else {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }
}

function ownersPortal (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        if(cellvalue == '1') {
            return 'Yes';
        }else if(cellvalue == '2')
        {
            return 'Disabled';
        } else {
            return 'No';
        }
    }
}
function statusFmatters1 (cellvalue, options, rowObject){
    if(rowObject !== undefined) {

        var select = '';
        if(rowObject['Status'] == '1')
            var $owners = "Owner's Portal";
        if(rowObject[$owners] == '2'){
            select = ['Edit','Email','Text','Work Order','Add In-touch','In-Touch History','Owner Draw','Owner Contribution','Owner statement','Owner Notes','File Library','Flag Bank','Email History','Text History','Run Background Check','Owner Portal','Send Password Activation Email','Owner Complaints','Archive Owner','Resign Owner','Activate Account','Print Envelope','Delete Owner'];
        }else {
            select = ['Edit','Email','Text','Work Order','Add In-touch','In-Touch History','Owner Draw','Owner Contribution','Owner statement','Owner Notes','File Library','Flag Bank','Email History','Text History','Run Background Check','Owner Portal','Send Password Activation Email','Owner Complaints','Archive Owner','Resign Owner','DeActivate Account','Print Envelope','Delete Owner'];
        }
        if(rowObject['Status'] == '2')  select = ['Run Background Check','Activate this Account','Print Envelope','Delete Owner','Online Payment'];
        if(rowObject['Status'] == '4')  select = ['Run Background Check','Reactivate this Owner','Print Envelope','Delete Owner','Online Payment'];

        var data = '';

        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }

        return data;
    }
}
// $(document).on('click','#import_owner',function () {
//     $("#import_file-error").text('');
//     $("#import_file").val('');
//     $('#import_owner_div').show(500);
// });

/*Vendor Listing Starts*/

/**
 * jqGrid Initialization function
 * @param status
 */
jqGridvendorListing('1');
function jqGridvendorListing(status) {
    var table = 'vendor_additional_detail';
    var columns = ['Vendor Name','Phone', 'Open Work Orders', 'YTD Payment', 'Type', 'Rating', 'Email'];
    var select_column = ['Edit', 'Pay vendor', 'Work orders', 'New Bill', 'Email', 'Email History', 'Text', 'Text History', 'Add InTouch', 'InTouchHistory','File History','Notes & History','Flag Bank','Run Background Check','Send Password Activation Mail', 'Archive Vendor', 'Resign Vendor','Vendor Rating', 'Print Envelope','Delete Vendor','Online Payment'];
    var joins = [{table: 'vendor_additional_detail', column: 'vendor_id', primary: 'id', on_table: 'users'},{table: 'vendor_additional_detail', column: 'vendor_type_id', primary: 'id', on_table: 'company_vendor_type'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = ['vendor_additional_detail.vendor_id'];
    var extra_where = [];
    var columns_options = [
        {name: 'Vendor Name', title:true, index: 'name', width: 180, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer',name_id:'vendor_id',formatter:vendorName},
        {name: 'Phone', index: 'id', width: 180, searchoptions: {sopt: conditions}, table: table, classes: 'pointer',change_type:'line_multiple',index2:'phone_number',join:{table: 'vendor_additional_detail', column: 'vendor_id', primary: 'user_id', on_table: 'tenant_phone'},name_id:'vendor_id'},
        {name: 'Open Work Orders', index: 'phone_type', width: 180, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer',formatter:workOrderFormatter},
        {name: 'YTD Payment', index: 'phone_type', width: 180, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer',formatter:paymentFormatter},
        {name: 'Type', index: 'vendor_type', width: 180, align: "left", searchoptions: {sopt: conditions}, table: 'company_vendor_type', classes: 'pointer'},
        //{name: 'Rate', index: 'vendor_rate', width: 80, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:rateFormatter},
        {name: 'Rating', index: 'rating', width: 170, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:starFormatter},
        {name: 'Email', index: 'email', width: 180, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer'},
        //{name: 'Status', index: 'status', width: 80, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',attr:[{name:'vendor_id',value:'vendor_id'},{name:'rating',value:'rating'},{name:'flag',value:'vendor',optional_id:'vendor_id'}],formatter: propertyStatus},
       // {name: 'Action', index: 'select', title: false, width: 80, align: "right",formatter: 'select', sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, edittype: 'select', search: false, table: table,formatter:actionFmatterss}
    ];
    var ignore_array = [];
    jQuery("#vendorListingTable").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'vendor_additional_detail.updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Vendors",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 200, drag: true, resize: false} // search options
    );
}

function vendorName(cellValue, options, rowObject) {
    if(rowObject !== undefined) {
        var flagValue = $(rowObject.Action).attr('flag');
        var id = $(rowObject.Action).attr('data_id');
      //  console.log(id);
       // console.log(rowObject);
        var flag = '';
        if (flagValue == 'yes') {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id='+id+'" ><img src="/company/images/Flag.png"></a></span>';
        } else {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }
}




function starFormatter(cellValue, options, rowObject){
    var html = '';
    switch (cellValue) {
        case '1':
            html = '<form><fieldset class="starability-growRotate"><input type="radio" name="rating" value="1" /><label  title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label class="faded-star" title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label class="faded-star" title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label class="faded-star" title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label class="faded-star" title="Amazing">5 star</label> </fieldset> </form>';
            break;
        case '2':
            html = '<form><fieldset class="starability-growRotate"><input type="radio" name="rating" value="1" /><label  title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label class="faded-star" title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label class="faded-star" title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label class="faded-star" title="Amazing">5 star</label> </fieldset> </form>';
            break;
        case '3':
            html = '<form><fieldset class="starability-growRotate"><input type="radio" name="rating" value="1" /><label  title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label class="faded-star" title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label class="faded-star" title="Amazing">5 star</label> </fieldset> </form>';
            break;
        case '4':
            html = '<form><fieldset class="starability-growRotate"><input type="radio" name="rating" value="1" /><label  title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label class="faded-star" title="Amazing">5 star</label> </fieldset> </form>';
            break;
        case '5':
            html = '<form><fieldset class="starability-growRotate"><input type="radio" name="rating" value="1" /><label  title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label title="Amazing">5 star</label> </fieldset> </form>';
            break;
        default:
            html = '<form><fieldset class="starability-growRotate"><input type="radio" name="rating" value="1" /><label class="faded-star" title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label class="faded-star" title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label class="faded-star" title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label class="faded-star" title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label class="faded-star" title="Amazing">5 star</label> </fieldset> </form>';
    }
    return html;
}

/**
 * jqGrid function to format status
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */

function flagFormatter(cellValue, options, rowObject){
    if(rowObject !== undefined){
        console.log(rowObject.Action);
        var flagValue = $(rowObject.Action).attr('flag');
        var flag = '';
        if(flagValue == 'yes'){

        } else {

        }
    }
}

function workOrderFormatter(cellValue, options, rowObject){
    return '0';
}

function paymentFormatter(cellValue, options, rowObject){
    return '0.00';
}

/**
 * jqGrid function to format is_default column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function isDefaultFormatter(cellValue, options, rowObject) {
    if (cellValue == '1')
        return "Yes";
    else if (cellValue == '0')
        return "No";
    else
        return '';
}
/**  User Listing **/

jqGridUsers(1);
/**
 * jqGrid Initialization function
 * @param status
 */
function jqGridUsers(status) {
    var table = 'users';
    var columns = ['Name', 'Email','Phone', 'Created Date', 'Status'];
    var extra_dropdown = [];
    var select_column = ['Edit','status','Delete'];
    var ignore_array = [{column:'users.id',value:'1'}];
    var joins = [{table:'users',column:'role',primary:'id',on_table:'company_user_roles'}];
    var conditions = ["eq","bw","ew","cn","in"];

    // if(selectedRoles){
    //     var extra_where = [{column:'user_type',value:1,condition:'='},{column:'role', value : selectedRoles,condition:'IN'}];
    // } else {
        var extra_where = [{column:'user_type',value:1,condition:'='}];
    // }
    var extra_columns = ['users.user_type', 'users.updated_at'];
    var columns_options = [
        {name:'Name',index:'name', class:"pointer", width:260,align:"center",searchoptions: {sopt: conditions},table:table,formatter: ContactName,change_type:'username'},
        {name:'Email',index:'email',class:"pointer", width:260,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Phone',index:'mobile_number',class:"pointer", width:260,align:"center",searchoptions: {sopt: conditions},table:table},
      //  {name:'Role',index:'role_name', width:200, align:"center",searchoptions: {sopt: conditions},table:'company_user_roles'},
        {name:'Created At',index:'created_at',class:"pointer", width:240,align:"right",searchoptions: {sopt: conditions},table:table},
        {name:'Status',index:'status',class:"pointer", width:230,align:"center",searchoptions: {sopt: conditions},table:table,formatter:userStatusFormatter },
       // {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table}
    ];
    jQuery("#userListings").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            extra_dropdown:extra_dropdown,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'true',
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Manage User",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {top:10,left:200,drag:true,resize:false}
    );
}

/**
 * jqGrid function to format status
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function userStatusFormatter (cellValue, options, rowObject){
    if (cellValue == 1)
        return "Active";
    else if(cellValue == '0')
        return "InActive";
    else
        return '';
}
$(document).on('click','#userListings tr td:not(:last-child)',function(e){
    e.preventDefault();
    var base_url = window.location.origin;
    var id = $(this).closest("tr").attr('id');
    window.location.href = base_url + '/User/ManageUser?id='+id;
});
function ContactName(cellValue, options, rowObject) {
    if(rowObject !== undefined) {
        var flagValue = $(rowObject.Action).attr('flag');
        var id = $(rowObject.Action).attr('data_id');
        var flag = '';
        if (flagValue == 'yes') {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id='+id+'" ><img src="/company/images/Flag.png"></a></span>';
        } else {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }

}












