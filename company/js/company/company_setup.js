/**
 *
 */
$('#companySetup').on('submit',function(event){
    event.preventDefault();
    if($('#companySetup').valid()) {
        var uploadform = new FormData();
        var formData = $('#companySetup').serialize();
        uploadform.append('data', formData);
        $('.img_upload').each(function() {
            var name = this.id;
            var file = this.files[0];
            uploadform.append(name, file);
        });
        $.ajax({
            type: 'post',
            url: '/User/AccountSetup/update',
            data:uploadform,
            processData: false,
            contentType: false,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.code == 200){
                    window.location.reload();
                }else if(response.code == 400){

                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});

$(function () {

    $('#companySetup').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    /**
     * If the letter is not digit in fax then don't type anything.
     */
    $("#fax").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    /**
     * To change the format of fax
     */
    $("input[name='fax']").keydown(function () {
        $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
    });

    /**
     * If the letter is not digit in phone number then don't type anything.
     */
    $(".phone_number").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    /**
     * To change the format of phone number
     */
    $(".phone_number").keydown(function () {
        $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
    });

    /**
     * Auto fill the city, state and country when focus loose on zipcode field.
     */
    $("#zipcode").focusout(function () {
        getZipCode('#zipcode',$(this).val(),'#city','#state','#country','','#zip_code_validate');
        // getAddressInfoByZip($(this).val());
    });

    /**
     * Auto fill the city, state and country when enter key is clicked.
     */
    $("#zipcode").keydown(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            getZipCode('#zipcode',$(this).val(),'#city','#state','#country','','#zip_code_validate');
        }
    });
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

new AutoNumeric('#rent_amount', {
    allowDecimalPadding: true,
    maximumValue  : '9999999999',
});

new AutoNumeric('#application_fee', {
    allowDecimalPadding: true,
    maximumValue  : '9999999999',
});

var scroll_bottom = localStorage.getItem('scroll_bottom');
if(scroll_bottom == "true"){
    $('html, body').animate({scrollTop:300}, 'slow');
    localStorage.removeItem('scroll_bottom');
}
