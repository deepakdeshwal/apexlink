$(document).ready(function () {
    var id =  getParameterByName('id');
    getPropertyDetail(id);


    jqGridPhotovideos('all');


});
function getParameterByName( name ){
    var regexS = "[\\?&]"+name+"=([^&#]*)",
        regex = new RegExp( regexS ),
        results = regex.exec( window.location.search );
    if( results == null ){
        return "";
    } else{
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}


function getPropertyDetail(id){

    $.ajax({
        type: 'post',
        url: '/Marketing/viewEditAjax',
        data: {
            class: 'viewEdit',
            action: 'getProperty',
            id:id

        },
        success: function (result) {
            var response = JSON.parse(result);
            if ((response.status == 'success')) {

                $(".tenantTrData").html(response.getTenantDetail);
                $.each(response.data.amenties,function(key, value){
                    var val=value.name;
                    if (val.indexOf("Parking ") >= 0){
                      $(".parkingClass").html('Yes');
                    }else{
                        $(".parkingClass").html('No');
                    }
                    $('.amentiesDiv').append('<span class="AmentieswidthCss">'+value.name+'</span>');
                });
                $.each(response.data,function(key, value){
                    console.log(key,value);

                    if (key == 'property_for_sale'){
                        if(value == 'Yes'){
                            $('#property_for_sale').html('For Sale');
                        }else{
                            $('#property_for_sale').html('For Rent');
                        }
                    }
                    if(key == 'garage_available'){
                       if(value == '1'){
                          $(".parkingClass").html('No');
                       }else if(value == '2'){
                           $(".parkingClass").html('Yes');
                       }
                    }
                    $('.'+key).text(value);
                });
            }
        }, error: function (jqXHR, status, err) {
            console.log(err);
        },
    });
}
var id =  getParameterByName('id');
initialData('0');
function initialData(page) {
    var id =  getParameterByName('id');
    var pagination = page;
    var totalpages = '';
    $.ajax({
        type: 'post',
        url: '/Marketing/viewEditAjax',
        async: false,
        data: {
            class: "viewEdit",
            action: "onLoadUnitListing",
            pagination: page,
            id:id
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.code == 200){
                getUnitDetail(data.marketing_list_data,data.amndata);
                totalpages = data.total_pages;
                if(page == '0') {
                    paginationGrid(totalpages);
                }
            }
        }
    });
    return totalpages;
}
function getUnitDetail(data1,amndata) {
    var html = '';
    $.each(data1, function (key, value) {
        html += '<tr><td><input type="checkbox"/></td>';
        html += '<td><h4><a class="unithref" href="/Unit/UnitView?id=' + value.id + '">Unit No -' + value.unit_no + '</a></h4><span>Bed: ' + value.bedrooms_no + ',Bathroom: ' + value.bathrooms_no + '</span></td>';
        html += '<td>' + value.base_rent + '</td>';
        html += '<td>' + statusFormat(value.building_unit_status) + '</td>';
        html += '<td><ul class="amnDataclass"></ul></td>';
    })
    $(".unitLisitng").html(html);
    $.each(amndata, function (key1, value1) {
        $('.amnDataclass').append('<li>' + value1.name + '</li>');
    })
}
function paginationGrid(totalpages){
    window.pagObj = $('#pagination').twbsPagination({
        totalPages: totalpages,
        visiblePages: 3,
        onPageClick: function (event, page) {
            console.log(event);
            initialData(page);
        }
    }).on('page', function (event, page) {

    });
}
function statusFormat(value){
    var status='';
    if(value == 1){
        status='Vacant';
    }
    if(value == 2){
        status='Unrentable';
    }
    if(value == 4){
        status='Occupied';
    }
    if(value == 5){
        status='Notice Available';
    }
    if(value == 6){
        status='Vacant Rented';
    }
    if(value == 7){
        status='Notice Rented';
    }
    if(value == 8){
        status='Under Make Ready';
    }
    return status;
}
function jqGridPhotovideos(status) {
    var id =  getParameterByName('id');
    var property_id = id;

    var table = 'property_file_uploads';
    var columns = ['Name','Preview','Location'];
    var select_column = ['Email','Delete'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = ['property_file_uploads.codec'];
    var extra_where = [{column:'property_id',value:property_id,condition:'='}];
    var columns_options = [
        {name:'Name',index:'file_name',width:400,align:"left",searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'file_location',width:450,align:"left",searchoptions: {sopt: conditions},search:false,table:table,formatter: photoFormatter},
        {name:'Location',index:'file_extension',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,hidden:true},
     ];
    var ignore_array = [];
    jQuery("#propertPhotovideos-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Property Files",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

function photoFormatter(cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var path = upload_url + 'company/' + rowObject.Preview;
        var select = rowObject.Action;
        var type = $(select).attr('type');
        if (type.startsWith("image")) {
            var appendType = '<a href="' + path + '" target="_blank"><img width=40 height=40 src=' + path + '></a>';
        } else if (type.startsWith("video")) {
            var appendType = '<video width=240 height=140 controls>' +
                '<source src="' + path + '" type="' + type + '">' +
                '</video>';
        } else{
            var path = upload_url+'company/'+rowObject.Preview;
            var src = '';
            if (rowObject.Location == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
            } else if (rowObject.Location == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
            } else if (rowObject.Location == 'docx') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
            } else if (rowObject.Location == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
            }
            return '<img class="img-upload-tab open_file_location" data-location="' + path + '" width=30 height=30 src="' + src + '">';
        }
        return appendType;
    }
}
$(document).on("click",".edit-foot",function(){
    var id =  getParameterByName('id');
    window.location.href = '/Marketing/edit?id='+id;
})
