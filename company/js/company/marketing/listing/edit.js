$(document).ready(function () {
    var id =  getParameterByName('id');
    getPropertyDetail(id);
    fetchAllPropertystyle();
    fetchAllPropertytype();
    fetchAllAmenities();

    jqGridPhotovideos();

});
function getParameterByName( name ){
    var regexS = "[\\?&]"+name+"=([^&#]*)",
        regex = new RegExp( regexS ),
        results = regex.exec( window.location.search );
    if( results == null ){
        return "";
    } else{
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}


function getPropertyDetail(id){
    $.ajax({
        type: 'post',
        url: '/Marketing/editAjax',
        data: {
            class: 'edit',
            action: 'getProperty',
            id:id

        },
        success: function (result) {
            var response = JSON.parse(result);
            if ((response.status == 'success')) {
                setTimeout(function () {
                    $.each(response.amentiesData, function (key, value) {

                        $("#"+value).prop("checked", "true");
                    });
                }, 1000);
                $.each(response.data,function(key,value){

                    $('#updatePropertyPostData [name ="'+key+'"]').val(value);
                    if(key == 'pet_friendly'){
                        var checkedSgn= (value == '2') ? 'checked' : '';
                        var checkedSgn1= (value == '1') ? 'checked' : '';
                        var html_pet='';
                            html_pet='<label>Pet Friendly</label>\n' +
                               '<div class="check-outer">\n' +
                               '<input type="radio" name="pet_friendly"  value="2" '+checkedSgn+'/> <label>Yes </label>\n' +
                               '</div>\n' +
                               '<div class="check-outer">\n' +
                               '<input type="radio" name="pet_friendly"  value="1" '+checkedSgn1+'/> <label>No </label>\n' +
                               '</div>';
                           $(".pet_friendly_radio_btn").html(html_pet);
                    }
                    if(key == 'garage_available'){
                        var checkedSgn= (value == '2') ? 'checked' : '';
                        var checkedSgn1= (value == '1') ? 'checked' : '';
                        var html_parking='';
                        html_parking=' <label>Parking</label>\n' +
                            '<div class="check-outer">\n' +
                            '<input type="radio" name="garage_available" value="2" '+checkedSgn+'/> <label>Yes </label>\n' +
                            '</div>\n' +
                            '<div class="check-outer">\n' +
                            '<input type="radio"  name="garage_available" value="1" '+checkedSgn1+'/> <label>No </label>\n' +
                            '</div>';
                        $(".parking_radio_btn").html(html_parking);

                    }
                    if(key == 'property_style'){
                        setTimeout(function(){
                            $("#property_style_options").val(value);
                        }, 1000);

                    }
                    if(key == 'property_type'){
                        setTimeout(function(){
                            $("#property_type_options").val(value);
                        }, 1000);

                    }
                    if(key == 'is_short_term_rental'){
                       if(value == '1'){
                           $('.listing_type').val(1);
                       }
                    }
                    if(key == 'property_for_sale'){
                        if(value == 'Yes'){
                            $('.listing_type').val(2);
                        }
                    }



                });
            }
        }, error: function (jqXHR, status, err) {
            console.log(err);
        },
    });
}
function statusFormat(value){
    var status='';
    if(value == 1){
        status='Vacant';
    }
    if(value == 2){
        status='Unrentable';
    }
    if(value == 4){
        status='Occupied';
    }
    if(value == 5){
        status='Notice Available';
    }
    if(value == 6){
        status='Vacant Rented';
    }
    if(value == 7){
        status='Notice Rented';
    }
    if(value == 8){
        status='Under Make Ready';
    }
    return status;
}

function jqGridPhotovideos(status) {
    var id =  getParameterByName('id');
    var property_id = id;
    var table = 'property_file_uploads';
    var columns = ['Name','Preview','Location'];
    var select_column = ['Email','Delete'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = ['property_file_uploads.codec'];
    var extra_where = [{column:'property_id',value:property_id,condition:'='}];
    var columns_options = [
        {name:'Name',index:'file_name',width:400,align:"left",searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'file_location',width:450,align:"left",searchoptions: {sopt: conditions},search:false,table:table,formatter: photoFormatter},
        {name:'Location',index:'file_extension',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,hidden:true},
     ];
    var ignore_array = [];
    jQuery("#propertPhotovideos-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Property Files",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

function photoFormatter(cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var path = upload_url + 'company/' + rowObject.Preview;
        var select = rowObject.Action;
        var type = $(select).attr('type');
        if (type.startsWith("image")) {
            var appendType = '<a href="' + path + '" target="_blank"><img width=40 height=40 src=' + path + '></a>';
        } else if (type.startsWith("video")) {
            var appendType = '<video width=240 height=140 controls>' +
                '<source src="' + path + '" type="' + type + '">' +
                '</video>';
        } else{
            var path = upload_url+'company/'+rowObject.Preview;
            var src = '';
            if (rowObject.Location == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
            } else if (rowObject.Location == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
            } else if (rowObject.Location == 'docx') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
            } else if (rowObject.Location == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
            }
            return '<img class="img-upload-tab open_file_location" data-location="' + path + '" width=30 height=30 src="' + src + '">';
        }
        return appendType;
    }
}
$(document).on("click",".edit-foot",function(){
    var id =  getParameterByName('id');
    window.location.href = '/Marketing/edit?id='+id;
})
function fetchAllPropertytype() {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-propertytype',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchPropertytype'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#property_type_options').html(res.data);

        },
    });

}
function fetchAllPropertystyle() {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-propertytype',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchPropertystyle'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#property_style_options').html(res.data);
        },
    });

}

$(document).on("submit",'#updatePropertyPostData',function (e) {
    e.preventDefault();
    var id =  getParameterByName('id');

        var formData = $('#updatePropertyPostData :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/Marketing/editAjax',
            data: {form: formData,
                class: 'edit',
                action: 'updateProperty',
                id: id
            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success')) {
                        toastr.success(response.message);
                        window.location.href = "/Marketing/view?id="+id;
                }
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });

});
function fetchAllAmenities() {
    $.ajax({
        type: 'post',
        url:  '/Marketing/editAjax',
        data: {
            class: 'edit',
            action: 'fetchAllAmenities'},
        success: function (response) {
            var res = JSON.parse(response);
            //$('#amenties_box').html(res.data);
            $('#amenties_box1').html(res.data);
        },
    });

}
$(document).on("submit",'#updateAmentiesData',function (e) {
    e.preventDefault();
    var id =  getParameterByName('id');


    var formData =  $('#updateAmentiesData').find('input[name]').serializeArray();

    $.ajax({
        type: 'post',
        url: '/Marketing/editAjax',
        data: {form: formData,
            class: 'edit',
            action: 'updateAmenties',
            id: id
        },
        success: function (result) {
            var response = JSON.parse(result);
            if ((response.status == 'success')) {
                toastr.success(response.message);
                window.location.href = "/Marketing/view?id="+id;
            }
        }, error: function (jqXHR, status, err) {
            console.log(err);
        },
    });

});

//check all amenities
$(document).on("click", "#selectAllamennity", function () {
    $(".inputAllamenity").prop('checked', true);
    if ($('.all').prop("checked") == false) {
        $(".inputAllamenity").prop('checked', false);
    }
});

$(document).on("click", ".inputAllamenity", function () {

    if ($('.all').prop("checked") == true) {
        $(".all").prop('checked', false);
    }
});

$(document).on('click','.clearFormReset',function(){
    bootbox.confirm("Do you want to reset this form?", function (result) {
        if (result == true) {
            location.reload();
            return true;
        } else {
            bootbox.hideAll()
        }
        return false;
    });

});