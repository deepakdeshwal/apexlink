$(document).ready(function () {

    new AutoNumeric('.property_price', {
        allowDecimalPadding: true,
        maximumValue  : '10000000000',
    });
    // var returnData = initialData('0');
    initialData('0');
    function initialData(page) {
        var pagination = page;
        var totalpages = '';
        $.ajax({
            type: 'post',
            url: '/Marketing/listingAjax',
            async: false,
            data: {
                class: "listing",
                action: "onLoadPropertyListing",
                pagination: page
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.code == 200){
                    // console.log(data);
                    $('#property_id').html(data.ddl_list_html);
                    marketListHtml(data.marketing_list_data);
                    totalpages = data.total_pages;
                    console.log('data', page);
                    if(page == '0') {
                        console.log('page',page);
                        $('#pagination').show();
                        $('#pagination')
                            .empty()
                            .removeData("twbs-pagination")
                            .unbind("page");
                        paginationGrid(totalpages);
                    }
                }
            }
        });
        return totalpages;
    }

    function marketListHtml(data){

        var html = '';
        $.each(data, function (key, value) {
            console.log('23423423423',value);
            // var price = (value.property_price != ' ') ? default_currency_symbol + value.property_price  : '';
            var sqrfoot = (value.property_squareFootage != '' && value.property_squareFootage != null) ? value.property_squareFootage+'.00'  : '0.00';
            var price = '';
            if (value.property_base_rent != '' && value.property_base_rent != null){
                price = default_currency_symbol + value.property_base_rent;
            } else{
                price = (value.unit_base_rent != '' && value.unit_base_rent != null) ? default_currency_symbol + value.unit_base_rent  : default_currency_symbol+'0.00 ';
            }

            var is_published = value.property_post_data.is_published;
            var is_featured  = value.property_post_data.is_featured;
            var show_on_map  = value.property_post_data.show_on_map;

            var post_data = propertiesPostData(is_published, is_featured, show_on_map);

            var avail_vacant_unit = '';
            if(post_data.is_published == 'active' || post_data.is_featured == 'active' || post_data.show_on_map =='active'){
                avail_vacant_unit = 'checked';
            }

           var property_typeHtml =  (value.property_type == null) ? '' : value.property_type;

            html += '<div class="market-list">\n' +
                '<ul>\n' +
                '<li>\n' +
                '<div class="row">\n' +
                '<div class="col-sm-12">\n' +
                '<div class="check-outer">\n' +
                '<input disabled type="checkbox" '+avail_vacant_unit+' class="avail_vacant_unit"/> <label class="availLabel">Avail the Vacant units</label>\n' +
                '</div>\n' +
                '</div>\n' +
                '<div class="col-sm-4">\n' +
                '<div class="col-sm-12">\n' +
                '<h4>' + value.property_name + '</h4>\n' +
                '<p>' + value.address1 + ', ' + value.city + ', ' + value.state + '</p>\n' +
                '<p>' + value.country + '</p>\n' +
                '<p>Type - ' + property_typeHtml + '</p>\n' +
                '<p>IsShortTermRental - ' + value.is_short_term_rental + '</p>\n' +
                '<p>Total sqft - ' + sqrfoot + '</p>\n' +
                '</div>\n' +
                '</div>\n' +
                '<div class="col-sm-4 col-sm-offset-1">\n' +
                '<h4></h4>' +
                '<p>From: ' + price + '</p>\n' +
                '<p>&nbsp;</p>\n' +
                '<p>Status:' + value.vacant + ' Vacant</p>\n' +
                '<p>&nbsp;</p>\n' +
                '<div id="this_property_id" property_id_val="'+value.id+'">\n' +
                '<p>\n' +
                '<i class="fa fa-file-text post_icon '+post_data.is_published+'" id="publish" title="'+post_data.publish_title+'" aria-hidden="true"></i>\n' +
                '<i class="fa fa-star post_icon '+post_data.is_featured+'" id="feature" title="'+post_data.featured_title+'" aria-hidden="true"></i>\n' +
                '<i class="fa fa-map-marker post_icon '+post_data.show_on_map+'" id="show_on_map" title="'+post_data.show_on_map_title+'" aria-hidden="true"></i>\n' +
                '</p></div>\n' +
                '</div>\n' +
                '<div class="col-sm-3">';

                var upload_url1=upload_url+"company/images/img-home2.jpg";
                var upload_url2=upload_url+"company/images/img-home3.jpg";
                var upload_url3=upload_url+"company/images/img-home1.jpg";

            html +='<div id="myCarousel_'+value.id+'" class="carousel slide" data-ride="carousel">';

            html +='<div class="carousel-inner">';

            if (typeof value.property_images_data !== 'undefined' && value.property_images_data.length > 0) {
                $.each(value.property_images_data, function (key1, value1) {
                    var path = upload_url + 'company/' + value1.file_location;
                    var active = (key1 == 0) ? 'active' : '';

                    html += '<div class="item '+active+'">\n' +
                        '<img src="'+path+'" alt="Slide1">\n' +
                        '</div>\n';
                });
            } else {
                html += '<div class="item active">\n' +
                    '<img src="/company/images/img-nt-available.png" alt="Slide1">\n' +
                    '</div>\n';
            }

             html +='</div>\n' +
                '<a class="left carousel-control" href="#myCarousel_'+value.id+'" data-slide="prev">\n' +
                '<span class="glyphicon glyphicon-chevron-left"></span>\n' +
                '<span class="sr-only">Previous</span>\n' +
                '</a>\n' +
                '<a class="right carousel-control" href="#myCarousel_'+value.id+'" data-slide="next">\n' +
                '<span class="glyphicon glyphicon-chevron-right"></span>\n' +
                '<span class="sr-only">Next</span>\n' +
                '</a>\n' +
                '</div>';

                html +='</div><div class="col-sm-12">\n' +
                '<div class="btn-outer">\n' +
                '<a href="/Marketing/view?id=' + value.id + '" class="blue-btn">More Details</a>\n' +
                '<a  href="/Marketing/edit?id=' + value.id +'"  class="blue-btn">Edit</a>\n' +
                '</div>\n' +
                '</div>';
                html +='</div>\n' +
                    '<div class="list-footer">\n' +
                    '<p>Last 30 Days -'
                html  +=value.guest_card.guest_card+' Prospect,'+value.count_applicants.count_applicants+' Applicants</p>';
                html  +='</div>\n' +
                    '</li>\n' +
                    '</ul>\n' +
                    '</div>';
        });
        $('#market-list').html(html);
    }

    $(document).on('click','.post_icon', function () {
        var selected_icon = $(this).attr('id');
        var is_active = ($(this).hasClass('active') == true) ? '1' : '0';
        var property_id = $(this).parent().parent().attr('property_id_val');
        var post_icon_area = $(this).parent().parent();
        var avail_vacant_unit = $(this).parent().parent().parent().prev().prev().find('.avail_vacant_unit');

        $.ajax({
            type: 'post',
            url: '/Marketing/listingAjax',
            async: false,
            data: {
                class: "listing",
                action: "updatePostData",
                selected_icon: selected_icon,
                is_active:is_active,
                property_id: property_id
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.code == 200){
                    toastr.success('This record saved successfully.');

                    var is_published = data.data.is_published;
                    var is_featured  = data.data.is_featured;
                    var show_on_map  = data.data.show_on_map;

                    var post_data = propertiesPostData(is_published, is_featured, show_on_map);

                    if(post_data.is_published == 'active' || post_data.is_featured == 'active' || post_data.show_on_map =='active'){
                        avail_vacant_unit.prop('checked', true);
                    } else {
                        avail_vacant_unit.prop('checked', false);
                    }

                    var html = '<p>\n' +
                        '<i class="fa fa-file-text post_icon '+post_data.is_published+'" id="publish" title="'+post_data.publish_title+'" aria-hidden="true"></i>\n' +
                        '<i class="fa fa-star post_icon '+post_data.is_featured+'" id="feature" title="'+post_data.featured_title+'" aria-hidden="true"></i>\n' +
                        '<i class="fa fa-map-marker post_icon '+post_data.show_on_map+'" id="show_on_map" title="'+post_data.show_on_map_title+'" aria-hidden="true"></i>\n' +
                        '</p>\n';
                    post_icon_area.html(html);
                }
            }
        });
    });

    function propertiesPostData(is_published, is_featured, show_on_map){
        var return_data = {};
        is_published = (is_published == 'yes' && is_published != null) ? 'active' : '';
        var publish_title = (is_published == 'active') ? 'UnPublish' : 'Publish';

        is_featured = (is_featured == 'yes' && is_featured != null) ? 'active' : '';
        var featured_title = (is_featured == 'active') ? 'Remove From Feature List' : 'Add To Feature List';

        show_on_map = (show_on_map == 'yes' && show_on_map != null) ? 'active' : '';
        var show_on_map_title = (show_on_map == 'active') ? 'Hide On Map' : 'Show On Map';
        return  return_data = {
            'is_published'      :is_published,
            'publish_title'     :publish_title,
            'is_featured'       :is_featured,
            'featured_title'    :featured_title,
            'show_on_map'       :show_on_map,
            'show_on_map_title' :show_on_map_title,
        }
    }

    function paginationGrid(totalpages){
        console.log('totalpages',totalpages);
        window.pagObj = $('#pagination').twbsPagination({
            totalPages: totalpages,
            visiblePages: 3,
            onPageClick: function (event, page) {
                //
            }
        }).on('page', function (event, page) {
            initialData(page);
        });
    }

    $(document).on('click', '.search_property_btn', function () {

        var property_price = $('.property_price').val();
        var property_id = $('#property_id').val();
        console.log('property_price>>', property_price);
        console.log('property_id>>', property_id);
        if (property_id != '') {
            onSearchData(property_price, property_id);
        } else if(property_price != ''){
            onSearchData(property_price, property_id);
        }else {
            initialData('0');
        }
    });

    function onSearchData(property_price, property_id) {
        $.ajax({
            type: 'post',
            url: '/Marketing/listingAjax',
            async: false,
            data: {
                class: "listing",
                action: "onSearchPropertyListing",
                property_price: property_price,
                property_id: property_id
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.code == 200){
                    $('#pagination').twbsPagination('destroy');
                    // console.log(data);
                    if (property_id !=''){
                        $('#pagination').hide();
                    }
                    // console.log('property_price>>', data.marketing_list_data);
                    // if (property_price == '' && pr)
                    if (data.marketing_list_data.length == 0){
                        $('#market-list').html('');
                    } else {
                        if (property_price != '') {
                            $('#pagination').show();
                            $('#pagination')
                                .empty()
                                .removeData("twbs-pagination")
                                .unbind("page");
                            paginationGrid(data.total_pages);
                        }
                        marketListHtml(data.marketing_list_data);
                    }
                }
            }
        });
    }

});



$(document).on('click','.availLabel',function () {
    $('.postData').modal('show');
    $('#postDataForm').trigger('reset');
    var id = $(this).parent().parent().parent().parent().find('#this_property_id').attr('property_id_val');
    $('.postData').attr('id',id);
    var publish1 = ($(this).parent().parent().parent().parent().find('#this_property_id').find('#publish').hasClass('active') == true)
        ? $("#publish1").prop("checked","true") : '';
    var feature1 =($(this).parent().parent().parent().parent().find('#this_property_id').find('#feature').hasClass('active') == true)
        ? $("#feature1").prop("checked","true") : '';
    var show_on_map1 = ($(this).parent().parent().parent().parent().find('#this_property_id').find('#show_on_map').hasClass('active') == true)
        ? $("#show_on_map1").prop("checked","true") : '';
});

$(document).on('click','.postDataBtn',function () {
    var id=$('.postDataBtn').parents().parents().parents().parents().parents().parents().parents().attr('id');
    // alert(id);
    if ($('#publish1').is(":checked")) {
       $('#'+id).find('#publish').trigger('click');
        $('.postData').modal('hide');
    }
    if($('#publish1').is(":unchecked")){
        $('#publish').trigger('click');
        $('.postData').modal('hide');
    }

    if ($('#feature1').is(":checked")) {
        $('#feature').trigger('click');
        $('.postData').modal('hide');
    }
    if($('#publish1').is(":unchecked")){
        $('#feature').trigger('click');
        $('.postData').modal('hide');
    }

    if ($('#show_on_map1').is(":checked")) {
        $('#show_on_map').trigger('click');
        $('.postData').modal('hide');
    }
    if($('#publish1').is(":unchecked")){
        $('#show_on_map').trigger('click');
        $('.postData').modal('hide');
    }
});