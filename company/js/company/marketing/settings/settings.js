$(document).on('click', '.click-marketing-contact', function() {

    $( "#zip_code" ).focusout(function() {
        getZipCode('#zip_code',$('#zip_code').val(),'.citys','.state','#country','','');
    });
$(".view-marketing-contactdetails").hide();
$(".edit-marketing-contactdetails").show();
    getMarketingDetail('edit');

});
$(document).on('click', '.cancel_settings_btn', function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $(".view-marketing-contactdetails").show();
            $(".edit-marketing-contactdetails").hide();
        }
    });

});
$(document).on("submit","#edit-form-contactdetails",function (e) {
    e.preventDefault();
    var form = $('#edit-form-contactdetails')[0];
    var formData = new FormData(form);
    formData.append('action', 'addContactDetail');
    formData.append('class', 'settings');
    $.ajax({
        type: 'post',
        url: '/Marketing/settingsAjax',
        data:formData,
        processData: false,
        contentType: false,
        success: function (result) {
            var response = JSON.parse(result);
            if ((response.status == 'success') && (response.code == 200)) {
                toastr.success(response.message);
                $("#contact_detail_id").val(response.lastid);
                $(".view-marketing-contactdetails").show();
                $(".edit-marketing-contactdetails").hide();
                getMarketingDetail('view');

            }
            else if (response.code == '400') {
                toastr.warning(response.message);
            }
        }, error: function (jqXHR, status, err) {
            console.log(err);
        },
    });
});

$(document).ready(function () {


    getMarketingDetail('view');
    jQuery('.conatct_phone_number').mask('000-000-0000', {reverse: true});
});

function getMarketingDetail(get) {
    var upload_url = window.location.origin;
    var id= userId;
    $.ajax({
        type: 'post',
        url: '/Marketing/settingsAjax',
        data: {
            class: 'settings',
            action: 'getMarketingDetailAjax',
            id: id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200){

                if(get == 'edit'){
                    $('#edit-form-contactdetails [name ="company_name"]').val(res.data.company_name);

                    $('#edit-form-contactdetails [name ="phone_number"]').val(res.data.phone_number);
                    $('#edit-form-contactdetails [name ="address"]').val(res.data.address);
                    $('#edit-form-contactdetails [name ="email"]').val(res.data.email);
                    $('#edit-form-contactdetails [name ="zip_code"]').val(res.data.zip_code);
                    $('#edit-form-contactdetails [name ="country"]').val(res.data.country);
                    $('#edit-form-contactdetails [name ="state"]').val(res.data.state);
                    $('#edit-form-contactdetails [name ="city"]').val(res.data.city);
                    $('#edit-form-contactdetails [name ="website"]').val(res.data.website);
                    $('#edit-form-contactdetails [name ="hours_of_operation"]').val(res.data.hours_of_operation);
                    if(res.data.logo != null) {
                        var logourl = upload_url + '/company/' + res.data.logo;
                        $('.marketing_logo').attr("src", logourl);
                    }
                    $(".marketing_logo_class").val(logourl);
                    if(res.data.publish == '0'){
                        $('#edit-form-contactdetails [name ="publish"]').prop("checked", false);
                    }else{
                        $('#edit-form-contactdetails [name ="publish"]').prop("checked", true);
                    }

                }else if(get == 'view'){
                    $('.company_name_view').val(res.data.company_name);

                    $('.contact_info_phn').html(res.data.phone_number);
                    $('.contact_info_add').html(res.data.address);
                    $('.contact_info_email').html(res.data.email);
                    $('.contact_info_country').html(res.data.country);
                    $('.contact_info_state').html(res.data.state);
                    $('.contact_info_city').html(res.data.city);
                    $('.contact_info_Website').html(res.data.website);
                    $('.contact_info_ope').html(res.data.hours_of_operation);

                    if(res.data.logo != null) {
                        var logourl = upload_url + '/company/' + res.data.logo;
                        $('.marketing_logo').attr("src", logourl);
                    }
                    if(res.data.publish == '0'){
                        $(".marketing_setting_publish").prop("checked", false);
                    }else{
                        $(".marketing_setting_publish").prop("checked", true);
                    }

                }



            }
        },
    });
}

$(document).on('click','.clearFormReset',function(){
    bootbox.confirm("Do you want to reset this form?", function (result) {
        if (result == true) {
            getMarketingDetail('edit');
            return true;
        } else {
            bootbox.hideAll()
        }
        return false;
    });

});
