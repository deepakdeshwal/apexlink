$(document).ready(function () {
  var one=  localStorage.getItem('id');
  var two=  localStorage.getItem('id2');

    $("#"+one).hide();
    $("#"+two).show();

    getLogo();

});

$(document).on("click",".flyerStatus",function(){
   var thisvar=$(this);
   var Fclass=$(this).attr('rel');

    $.ajax({
        type: 'post',
        url: '/Marketing/flyer',
        data: {
            class: "flyer",
            action: "ChangeDefaultTemplate",
            Fclass: Fclass
        },
        success: function (response) {

            var data = $.parseJSON(response);
            if (data.code == 200){
                toastr.success('This Template is now set as Default Template.');

                thisvar.hide();
                thisvar.next('a').show();
                var id=thisvar.attr('id');
                var id2=thisvar.next('a').attr('id');

                location.reload();

                localStorage.setItem('id',id);
                localStorage.setItem('id2',id2);
            }

        }
    });
});
$("#flyerUpload").validate({
    rules: {
        logo: {
            required: true
        }
    }
});
$(document).on("submit","#flyerUpload",function (e) {
    e.preventDefault();
    var upload_url = window.location.origin;
    var form = $('#flyerUpload')[0];
    var formData = new FormData(form);
    formData.append('action', 'UploadLogo');
    formData.append('class', 'flyer');
    $.ajax({
        type: 'post',
        url:  '/Marketing/flyer',
        data:formData,
        processData: false,
        contentType: false,
        success: function (result) {
            var response = JSON.parse(result);
            if ((response.status == 'success') && (response.code == 200)) {
                toastr.success("Logo Uploaded Successfully");

                var logourl= upload_url+'/company/'+response.logo;
                var logoname= response.name;
                $('#imgMarketingLogo').attr("src",logourl);
                $("#spnMarketingLogoName").html(logoname);
                localStorage.setItem('name',logoname);
            }
            else if (response.code == '500') {
                toastr.warning(response.message);
            }
        }, error: function (jqXHR, status, err) {
            console.log(err);
        },
    });
});

function getLogo(){
    $.ajax({
        type: 'post',
        url: '/Marketing/flyer',
        data: {
            class: "flyer",
            action: "getLogo"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.code == 200){
                var upload_url = window.location.origin;
                var logourl= upload_url+'/company/'+data.data.logo;
                var logoname=localStorage.getItem('name');
                $('#imgMarketingLogo').attr("src",logourl);
                $("#spnMarketingLogoName").html(logoname);
            }

        }
    });
}
