var monthArrayData = ['Oct','Nov','Dec','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep'];
fetchAllPortfolio('');
function fetchAllPortfolio(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-portfolioname',
        data: {
            propertyEditid: '',
            class: 'propertyDetail',
            action: 'fetchPortfolioname'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#portfolio_id').html(res.data);
        },
    });
}
setTimeout(function(){
    fetchAllProperty($('#portfolio_id').val());
}, 500);

function fetchAllProperty(portfolio_id,id) {
    $.ajax({
        type: 'post',
        url: '/global-get-module-ajax',
        data: {
            class: 'GlobalGetModule',
            action: 'getAllProperties',
            portfolio_id: portfolio_id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200) {
                var html = '<option value="">Select</option>';
                $.each(res.data,function(key,value){
                    if(id == value.id) {
                        html += '<option value="' + value.id + '" selected>' + value.property_name + '</option>';
                    } else {
                        html += '<option value="' + value.id + '">' + value.property_name + '</option>';
                    }
                });
                $('#property_id').html(html);
            }
        },
    });
}

$(document).on('change','#portfolio_id',function(){
    fetchAllProperty($(this).val(),'');
});

$(document).on('change','#startingMonth',function(){
    var month = $(this).val();
    bootbox.confirm("You will lose the data.Do you want to continue?", function (result) {
        if (result == true) {
            makeDiscriptionHeading(month);
        }
    });
});

function makeDiscriptionHeading(month){
    var monthArray = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    month =  month-1;
    var array1 = monthArray.slice(0, month);
    var array2 = monthArray.slice(month, month + monthArray.length);
    var months = array2.concat(array1);
    monthArrayData = months;
    var months = months.concat(months);
    $(".yearDescription").each(function(key,value) {
        $(this).html(months[key]);
    });
    return;
}

function fetchAllProperty(portfolio_id,id) {
    $.ajax({
        type: 'post',
        url: '/global-get-module-ajax',
        data: {
            class: 'GlobalGetModule',
            action: 'getAllProperties',
            portfolio_id: portfolio_id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200) {
                var html = '<option value="">Select</option>';
                $.each(res.data,function(key,value){
                    if(id == value.id) {
                        html += '<option value="' + value.id + '" selected>' + value.property_name + '</option>';
                    } else {
                        html += '<option value="' + value.id + '">' + value.property_name + '</option>';
                    }
                });
                $('#property_id').html(html);
            }
        },
    });
}

$(document).on('change','#yearDesc',function(){
    if($(this).val() != '') {
        $('.yearDesc').html('Total - FY ' +$(this).val());
    }
});
// getAllBudget();
function getAllBudget(){
    $.ajax({
        type: 'post',
        url: '/budgeting-ajax',
        data: {
            class: 'Budgeting',
            action: 'getBudgetChartsOfAccounts'},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200) {
                var income = '<tr><td colspan="14" class="bg-grey">Income <em class="red-star">*</em> ('+default_currency_symbol+')</td></tr>';
                var expense = '<tr><td colspan="14" class="bg-grey">Expenses <em class="red-star">*</em> ('+default_currency_symbol+')</td></tr>';
                $.each(res.data,function(key,value){
                    if(value.account_type_id == '2') {
                        expense += '<tr id="'+value.id+'" data_type="expense">';
                        expense += '<td data_index="0" data_type="expense">'+value.account_code+'-'+value.account_name+'</td>';
                        expense += '<td data_index="1" data_type="expense"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        expense += '<td data_index="2" data_type="expense"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        expense += '<td data_index="3" data_type="expense"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        expense += '<td data_index="4" data_type="expense"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        expense += '<td data_index="5" data_type="expense"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        expense += '<td data_index="6" data_type="expense"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        expense += '<td data_index="7" data_type="expense"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        expense += '<td data_index="8" data_type="expense"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        expense += '<td data_index="9" data_type="expense"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        expense += '<td data_index="10" data_type="expense"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        expense += '<td data_index="11" data_type="expense"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        expense += '<td data_index="12" data_type="expense"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        expense += '<td data_index="13" data_type="expense"><input class="form-control number_only amount_num trAmountTotal" readonly type="text"/></td>'
                        expense += '</tr>';
                    } else if(value.account_type_id == '5') {
                        income += '<tr id="'+value.id+'" data_type="income">';
                        income += '<td data_index="0" data_type="income">'+value.account_code+'-'+value.account_name+'</td>';
                        income += '<td data_index="1" data_type="income"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        income += '<td data_index="2" data_type="income"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        income += '<td data_index="3" data_type="income"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        income += '<td data_index="4" data_type="income"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        income += '<td data_index="5" data_type="income"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        income += '<td data_index="6" data_type="income"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        income += '<td data_index="7" data_type="income"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        income += '<td data_index="8" data_type="income"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        income += '<td data_index="9" data_type="income"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        income += '<td data_index="10" data_type="income"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        income += '<td data_index="11" data_type="income"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        income += '<td data_index="12" data_type="income"><input class="form-control number_only amount_num trAmount" type="text"/></td>';
                        income += '<td data_index="13" data_type="income"><input class="form-control number_only amount_num trAmountTotal" readonly type="text"/></td>'
                        income += '</tr>';
                    }
                });
                $('#appendCOA').html(income).append(expense);
            }
        },
    });
}

/**
 *
 */
$(document).on('focusout','.trAmount',function(){
    $('#NetIncomeTotal').show();
    var amount = 0;
    var index = $(this).parent().attr('data_index');
    var type = $(this).parent().attr('data_type');
    var nodeParent = $(this).parent().parent().find('.trAmount');
    $.each(nodeParent,function(key,value){
        if($(value).val() != '') {
            var value = $(value).val().replace(/,/g,'');
            amount += parseFloat(value);
        }
    });
    amount = changeToFloat(amount.toString());
    $(this).parent().parent().find('.trAmountTotal').val(amount);
    setIncomeTable(index,changeToFloat($(this).val()),type);
});

function setIncomeTable(index,amount,type){
    if(type == 'income'){
        var idEle = 'incomeTrRow';
        var find = 'trIncome';
        var total = 'trIncomeTotal';
        var totalRowAmount = 0;
        $('#appendCOA tr').each(function(key,value) {
            if($(value).find("td:eq("+index+")").attr('data_type') == 'income'){
                totalRowAmount += $(value).find("td:eq("+index+")").find('input').val() != ''?parseFloat($(value).find("td:eq("+index+")").find('input').val().replace(/,/g,'')):0;
            };
        });

    } else if(type == 'expense'){
        var idEle = 'expenseTrRow';
        var find = 'trExpense';
        var total = 'trExpenseTotal';
        var totalRowAmount = 0;
        $('#appendCOA tr').each(function(key,value) {
            if($(value).find("td:eq("+index+")").attr('data_type') == 'expense'){
                totalRowAmount += $(value).find("td:eq("+index+")").find('input').val() != ''?parseFloat($(value).find("td:eq("+index+")").find('input').val().replace(/,/g,'')):0;
            };
        });
    }
    var element = $('#'+idEle);
    element.find("td:eq("+index+") input").val(changeToFloat(totalRowAmount.toString()));
    var nodeParent = element.find('.'+find);
    var dataAmount = 0;
    $.each(nodeParent,function(key,value){
        if($(value).val() != '') {
            var value = $(value).val().replace(/,/g,'');
            dataAmount += parseFloat(value);
        }
    });
    dataAmount = changeToFloat(dataAmount.toString());
    element.find('.'+total).val(dataAmount);
    getTotalFy();
}

function getTotalFy(){
    var incomeTotal = ($('.trIncomeTotal').val() != '')?parseFloat($('.trIncomeTotal').val().replace(/,/g,'')):0;
    var expenseTotal = ($('.trExpenseTotal').val() != '')?parseFloat($('.trExpenseTotal').val().replace(/,/g,'')):0;
    $('.grandTotal').val(changeToFloat((incomeTotal-expenseTotal).toString()));
}

//add property form client side validations
$("#newBudgetForm").validate({
    rules: {
        budget_name: {
            required:true
        },
        starting_year: {
            required:true
        },
        portfolio_id: {
            required:true
        },
        property_id: {
            required:true
        }
    }
});