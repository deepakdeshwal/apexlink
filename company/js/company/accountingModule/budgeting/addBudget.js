$(document).on('click','#budgetCancel',function(){
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            window.location.href = '/Accounting/Budgeting';
        }
    });
});

//submit form
$(document).on('click','#budgetSubmit',function(e){
    e.preventDefault();
    if($('#newBudgetForm').valid()){
        //adding income and expense data
        var budgetItemData = [];
        $('#appendCOA tr').each(function(key,value) {
            var budgetRowItems = [];
            var id = $(value).attr('id');
            var type = $(value).attr('data_type');
            var total = $(value).find('.trAmountTotal').val() != ''?$(value).find('.trAmountTotal').val():'0.00';
            if($(value).attr('data_type') !== undefined){
                var nodeTr = $(value).find('td');
                budgetRowItems.push({'charts_of_account_id':id});
                budgetRowItems.push({'type':type});
                budgetRowItems.push({'total_fy':total});
                $.each(nodeTr,function(key1,value1){
                    var inputValue = $(value1).find('input').val() != ''?$(value1).find('input').val():'0.00';
                    if(inputValue !== undefined) {
                        var keyd = monthArrayData[key1-1];
                        switch (keyd) {
                            case "Oct":
                                budgetRowItems.push({october: inputValue});
                                break;
                            case "Nov":
                                budgetRowItems.push({november: inputValue});
                                break;
                            case "Dec":
                                budgetRowItems.push({december: inputValue});
                                break;
                            case "Jan":
                                budgetRowItems.push({january: inputValue});
                                break;
                            case "Feb":
                                budgetRowItems.push({february: inputValue});
                                break;
                            case "Mar":
                                budgetRowItems.push({march: inputValue});
                                break;
                            case "Apr":
                                budgetRowItems.push({april: inputValue});
                                break;
                            case "May":
                                budgetRowItems.push({may: inputValue});
                                break;
                            case "Jun":
                                budgetRowItems.push({june: inputValue});
                                break;
                            case "Jul":
                                budgetRowItems.push({july: inputValue});
                                break;
                            case "Aug":
                                budgetRowItems.push({august: inputValue});
                                break;
                            case "Sep":
                                budgetRowItems.push({september: inputValue});
                                break;
                            default:

                        }
                    }
                });
                budgetItemData.push(budgetRowItems);
            };
        });
        $.ajax({
            type: 'post',
            url: '/budgeting-ajax',
            data: {
                class: 'Budgeting',
                action: 'saveBudget',
                data: $('#newBudgetForm').serializeArray(),
                income:$('.trIncomeTotal').val(),
                expense:$('.trExpenseTotal').val(),
                grandTotal:$('.grandTotal').val(),
                budgetItemData:budgetItemData},
            beforeSend: function(xhr) {
                // checking portfolio validations
                if (($('.trIncomeTotal').val() === undefined || $('.trIncomeTotal').val() == '') && ($('.trExpenseTotal').val() === undefined || $('.trExpenseTotal').val() == '')){
                    bootbox.alert('The Income and Expense accounts are empty.Please review.');
                    xhr.abort();
                    return false;
                }
            },
            success: function (response) {
                var res = JSON.parse(response);
                if(res.code == 200) {
                    localStorage.setItem("rowcolor",'fdsf');
                    localStorage.setItem("Message",res.message);
                    window.location.href = '/Accounting/Budgeting';
                }
            },
        });
    }
});