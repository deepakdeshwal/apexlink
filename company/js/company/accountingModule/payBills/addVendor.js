fetchAllVendorType(false);
var coboGridIntiated = '0';
$( "#zip_code" ).focusout(function() {
    getZipCode('#zip_code',$('#zip_code').val(),'.citys','#state','#country','','');
});

$(document).on('change','#salutation',function(){
    var value = $(this).val();
    if(value == '2'){
        $('#gender').val('1');
    } else if(value == '3' || value == '5' || value == '6' || value == '7' || value == '8' || value == '9'){
        $('#gender').val('2');
    } else {
        $('#gender').val('');
    }
});

function getRandomNumber(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

//open add vendor modal
$(document).on('click','.addVendorModal',function(){
    bootbox.confirm("Do you want to add a New Vendor?", function (result) {
        if (result == true) {
            var validator = $( "#add_vendor_form" ).validate();
            validator.resetForm();
            $('#add_vendor_form').trigger('reset');
            $('#addVendorModal').modal('show');
            $('.vendor_random_id').val(getRandomNumber(6));
        }
    });
});

$(document).on('click','.cancel_add_vendor',function(){
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $('#addVendorModal').modal('hide');
        }
    });
});

//add vendor type
function clearPopUps(id) {
    $(id).find('input[type=text]').val('');
    $(id).find('span').text('');
}

$(document).on("click", ".vendortypeplus", function () {
    $("#Newvendortype").show();
});

$(document).on("click", ".additionalReferralResource", function () {
    $("#additionalReferralResource1").show(200);
});

function fetchAllVendorType(id) {
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {
            class: 'addVendor',
            action: 'fetchAllVendorType'},
        success: function (response) {
            var res = JSON.parse(response);
            $("select[name='vendor_type_id']").html(res.data);
            if (id != false) {
                $('#vendor_type_options').val(id);
            }
        },
    });
}

// add vendor type
$(document).on('click', '#NewvendortypeSave', function (e) {
    e.preventDefault();
    var formData = $('#Newvendortype :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {form: formData,
            class: 'addVendor',
            action: 'addVendorType'},
        beforeSend: function (xhr) {
            var res = true;
            // checking portfolio validations
            $(".customValidatePortfoio").each(function () {
                res = validations(this);
            });

            if (res === false) {
                xhr.abort();
                return false;
            }
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#Newvendortype').hide(500);
                toastr.success(response.message);
                fetchAllVendorType(response.lastid);
            } else if (response.status == 'error' && response.code == 500) {
                toastr.warning(response.message);
            } else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });
});

$(document).on("click", ".cancelPopup", function () {
    $(this).parent().parent().parent().parent().hide();
    $(this).parent().parent().parent().find('input[type=text]').val('');
    $(this).parent().parent().parent().find('span').text('');
});

$('#add_vendor_form').on('submit',function(e){
    e.preventDefault();
    if($('#add_vendor_form').valid()){
        $.ajax({
            type: 'POST',
            url: '/global-user-module-ajax',
            data: {class: 'GlobalUserModule', action: "add_vendor",data:$('#add_vendor_form').serializeArray()},
            success: function (response) {
                var res = JSON.parse(response);
                if(res.code == 200){
                    $('#addVendorModal').modal('hide');
                    toastr.success(res.message);
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
});

$(document).on("click",".modal-dialog .add_single", function () {
    var tableName = $(this).attr("data-table");
    var colName = $(this).attr("data-cell");
    var className = $(this).attr("data-class");
    var selectName = $(this).attr("data-name");
    var val = $.trim($("."+className).val());
    if (val == ""){
        $("#"+className).text("Enter the value");
        return false;
    }else{
        $("#"+className).text("");
    }
    savePopUpData(tableName, colName, val, selectName);
});


function  savePopUpData(tableName, colName, val, selectName) {
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/savePopUpData',
        data: {
            class: "TenantAjax",
            action: "savePopUpData",
            tableName:tableName,
            colName: colName,
            val:val
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                $("select[name='"+selectName+"']").append(option);
                if (selectName == "hobbies[]" || selectName == "additional_hobbies[]"){
                    $("select[name='"+selectName+"']").multiselect('destroy');
                    $("select[name='"+selectName+"']").multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                }
                toastr.success(data.message);
                $('#additionalReferralResource1').hide();

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

$(document).on('click','.change_to',function(){
    $('#change_to_div').show();
    $('#saveNewBill [name="change_to_other"]').val('');
    if($('input[name=change_to]:checked').val() == '1'){
        $('#tenant_div_box').show();
        $('#other_div_box').hide();
        showTenantGrid();
    } else {
        // $('#change_to_other').combo('destroy');
        $('#tenant_div_box').hide();
        $('#other_div_box').show();
    }
});

function showTenantGrid(){
    if(coboGridIntiated == '0') {
        $('#change_to_other_tenant').combogrid({
            placeholder: "Select...",
            panelWidth: '780px',
            panelHeight: 'auto',
            url: '/combo-grid-global-ajax',
            idField: 'name',
            textField: 'name',
            mode: 'remote',
            fitColumns: true,
            queryParams: {
                action: 'getUserdata',
                class: 'GlobalComboGrid',
                user_type: '2'
            },
            columns: [[
                {field: 'name', title: 'Tenant Name', width: '173px'},
                {field: 'email', title: 'Email', width: '216px'},
                {field: 'property', title: 'Property', width: '186px'},
                {field: 'building', title: 'Building', width: '100px'},
                {field: 'unit', title: 'Unit', width: '100px'}
            ]],
            onSelect: function (index, row) {
                console.log(row);
                $('#tenant_id').val(row.id);
                $('#property_id').val(row.property_id);
            }
        });
        $('.textbox').css('width', '70%');
        $('.textbox-addon').css('display', 'none');
        coboGridIntiated = '1';
    }
}

$(document).on('click','#_easyui_textbox_input2',function(){
    $('#change_to_other_tenant').combogrid('showPanel');
});

$(document).ready(function(){
    var vendor_id = localStorage.getItem('ven_id');
    localStorage.removeItem('ven_id');
    if(vendor_id != undefined){
        setTimeout(function () {
            $.ajax({
                type: 'post',
                url: '/global-user-module-ajax',
                data: {
                    class: 'GlobalUserModule',
                    action: 'getUser',
                    id:vendor_id},
                success: function (response) {
                    var res = JSON.parse(response);
                    console.log('vendor_address',res);
                    if(res.code == 200){
                        $('#vendorAddress').val(res.addressFormated);
                        $('#EditVendorAddress').show();
                        $('#EditVendorAddress').attr('data_id', res.data.id);
                        $('#vendor_id').val(res.data.id);
                        $('#_easyui_textbox_input1').val(res.data.name);
                    }
                },
            });
        },500);

    }
    $(document).on('change','#salutation',function () {
        var value = $(this).val();
        if(value == 1){
            $('#gender').prop('selectedIndex',0);
            $('.hidemaiden').hide();
        }
        if(value==2)
        {
            $("#gender").val("1");
            $('.hidemaiden').hide();
        }
        if(value==3)
        {
            $("#gender").val("2");
            $('.hidemaiden').show();
        } if(value==4)
        {
            $('#gender').prop('selectedIndex',0);
            $('.hidemaiden').hide();
        } if(value==5)
        {
            $("#gender").val("2");
            $('.hidemaiden').show();
        } if(value==6)
        {
            $("#gender").val("1");
            $('.hidemaiden').hide();
        } if(value==7)
        {
            $("#gender").val("2");
            $('.hidemaiden').show();
        }
        if(value==8)
        {
            $("#gender").val("2");
            $('.hidemaiden').show();
        } if(value==9)
        {
            $("#gender").val("2");
            $('.hidemaiden').show();
        }


    });
});