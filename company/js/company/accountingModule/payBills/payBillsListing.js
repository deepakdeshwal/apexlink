$(document).on('change','.searchRadio',function(){
    if($(this).val() == '2'){
        $('#dateFilterDiv').show();
    } else {
        $('#dateFilterDiv').hide();
    }
});

$(document).on('click','#_easyui_textbox_input1',function(){
    $('.vendorSelect').combogrid('showPanel');
});

$(document).on('focusout','#_easyui_textbox_input1',function(){
     if($(this).val() == ''){
         $('#vendorAddress').html('');
         $('#editVendorDiv').hide();
         $('#EditVendorAddress').attr('data_id','');
         $('#vendor_search_id').val('');
     }
});

$( "#vendor_zip_code" ).focusout(function() {
    getZipCode('#vendor_zip_code',$('#vendor_zip_code').val(),'#vendor_city','#vendor_state','#vendor_country','','');
});

$(document).on('click','#EditVendorAddress',function(){
    var id = $(this).attr('data_id');
    var validator = $( "#update_vendor_address_form" ).validate();
    validator.resetForm();
    fetchUserAddress(id);
    $('#updateVendorAddress').modal('show');
});


$(document).on('click','.cancel_address_update',function(){
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $('#updateVendorAddress').modal('hide');
        }
    });
});
$(document).ready(function () {
    $('.vendorSelect').combogrid({
        placeholder: "Click here to pick a vendor",
        panelWidth: '780px',
        panelHeight: 'auto',
        url: '/combo-grid-global-ajax',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getUserdata',
            class: 'GlobalComboGrid',
            user_type: '3'
        },
        columns: [[
            {field: 'name', title: 'Vendor Name', width: '173px'},
            {field: 'address', title: 'Address', width: '216px'},
            {field: 'email', title: 'Email', width: '186px'},
            {field: 'vendor_type', title: 'Vendor Type', width: '100px'},
            {field: 'vendor_rate', title: 'Rate', width: '100px'}
        ]],
        onSelect: function (index, row) {
            $('#vendorAddress').html(row.addressFormat);
            $('#editVendorDiv').show();
            $('#EditVendorAddress').attr('data_id', row.id);
            $('#vendor_search_id').val(row.id);
        }

    });
    $("#_easyui_textbox_input1").attr("placeholder", "Click here to pick a vendor");
    $('.textbox').css('width','90%');
});

function fetchUserAddress(id) {
    $.ajax({
        type: 'post',
        url: '/global-user-module-ajax',
        data: {
            class: 'GlobalUserModule',
            action: 'getUser',
            id:id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200){
                $('#vendorAddress1').val(res.data.address1);
                $('#vendorAddress2').val(res.data.address2);
                $('#vendorAddress3').val(res.data.address3);
                $('#vendor_zip_code').val(res.data.zipcode);
                $('#vendor_country').val(res.data.country);
                $('#vendor_state').val(res.data.state);
                $('#vendor_city').val(res.data.city);
                $('#vendor_address_id').val(res.data.id);
            }
        },
    });
}

$(document).on('submit','#update_vendor_address_form',function(e){
    e.preventDefault();
    if($('#update_vendor_address_form').valid()){
        $.ajax({
            type: 'post',
            url: '/global-user-module-ajax',
            data: {
                class: 'GlobalUserModule',
                action: 'updateUserAddress',
                data:$('#update_vendor_address_form').serializeArray()},
            success: function (response) {
                var res = JSON.parse(response);
                if(res.code == 200){
                    $('#vendorAddress').html(res.address);
                    $('#updateVendorAddress').modal('hide');
                    toastr.success(res.message);
                }
            },
        });
    }
});

//jquery grid for property insurance
jqGrid('all');
function jqGrid(status) {
    var table = 'company_bills';
    var columns = [' ', 'Vendor Name', 'Vendor_id', 'Due Date', 'Reference Number', 'Original Amount('+amount_symbol+')','Discounted Amount('+amount_symbol+')','Terms','Amount Due After Discount('+amount_symbol+')','Status'];
    var select_column = [];
    var joins = [{table: 'company_bills', column: 'vendor_id', primary: 'id', on_table: 'users'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [];
    var extra_columns = [];
    var columns_options = [
        {name: ' ', index: 'id', width: 100, align: "center", editoptions: {value: "True:False"}, editrules: {required: true},
            formatter: function (cellvalue, options, rowObject) {
                if(rowObject !== undefined) {
                    if(rowObject.Status == '1'){
                        return '';
                    } else {
                        return '<input type="checkbox" class="checkboxgrid" data_value="' + rowObject.Vendor_id + '" value=' + cellvalue + ' data_status="' + rowObject.Status + '">';
                    }
                }
            },
            formatoptions: {disabled: false}, editable: true, searchoptions: {sopt: conditions}, table: table,search:false},
        {name: 'Vendor Name', index: 'name', width: 200, align: "center", searchoptions: {sopt: conditions}, table: 'users'},
        {name: 'Vendor_id', index: 'vendor_id',hidden:true, width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Due Date', index: 'due_date', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,change_type:'date'},
        {name: 'Reference Number', index: 'refrence_number', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:referenceFormatter},
        {name: 'Original Amount', index: 'amount', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:amountDiv},
        {name: 'Discounted Amount', index: 'memo', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:discountDiv},
        {name: 'Terms', index: 'term', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Amount Due After Discount', index: 'amount', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:afterDiscountDiv},
        {name: 'Status', index: 'status', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:statusFormater},
    ];
    var ignore_array = [];
    jQuery("#paybills_table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'company_bills.updated_at',
        sortorder: 'desc',
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Pay Bills",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 400, drag: true, resize: false} // search options
    );
}

/**
 *
 * @param cellValue
 * @param options
 * @param rowObject
 */
function discountDiv(cellValue, options, rowObject){
    if(rowObject !== undefined){
        var html = '<span class="span_discount cursor">'+amount_symbol+'0.00</span><input style="display: none;" class="amount number_only input_discount form-control" id="input_'+rowObject.id+'" name="discount[]" value="0.00">';
        return html;
    }
}

function referenceFormatter(cellValue, options, rowObject){
    if(rowObject !== undefined){
        return '<a target="_blank" style="text-decoration: underline;color:#551A8B !important;" href="/Vendor/EditBill?id='+rowObject.id+'">'+cellValue+'</a>';
    }
}

/**
 *
 * @param cellValue
 * @param options
 * @param rowObject
 */
function afterDiscountDiv(cellValue, options, rowObject){
    if(rowObject !== undefined){
        var amount = changeToFloat(cellValue.toString());
        return amount_symbol+amount;
    }
}

function creditCardNumber(cellValue, options, rowObject){
    return '';
}

function statusFormater(cellValue, options, rowObject){
    if(rowObject !== undefined){
        if(cellValue == '1'){
            return '<b>Paid</b>';
        }
        return '<b>Due</b>';
    }
}

/**
 *
 * @param cellValue
 * @param options
 * @param rowObject
 */
function amountDiv(cellValue, options, rowObject){
    if(rowObject !== undefined){
        var amount = changeToFloat(cellValue.toString());
        return '<span data_input="'+amount+'">'+amount_symbol+amount+'</span>';
    }
}

$(document).on('click','.span_discount',function(){
    $(this).hide();
    $(this).parent().find('input').css('display','block');
});

$(document).on('focusout','.input_discount',function(){
    var amount_value = $(this).val() != '' ? $(this).val() : '0.00';
    var value = amount_value.replace(/,/g, '');
    var amountValue = $(this).parent().prev().find('span').attr('data_input');
    amountValue = amountValue.replace(/,/g, '');
    if (parseFloat(value) > parseFloat(amountValue)) {
        bootbox.alert("Please Enter Valid Amount!");
        var span_amount = '0.00';
        $(this).val('0.00');
    } else {
        var realAmount = parseFloat(amountValue) - parseFloat(value);
        realAmount = changeToFloat(realAmount.toString());
        var span_amount = changeToFloat(value.toString());
        $(this).parent().next().next().text(amount_symbol + realAmount);
    }
});
$("#selectDate").datepicker({dateFormat: jsDateFomat}).datepicker("setDate", new Date());
$("#searchDate").datepicker({dateFormat: jsDateFomat}).datepicker("setDate", new Date());

$(document).on('click','#searchBillTable',function(){
    searchFilters();
});

function searchFilters(){
    var grid = $("#paybills_table"),f = [];
    var radio = $('.searchRadio:checked').val();
    var vendor_id = ($('#vendor_search_id').val() == '')?'all':$('#vendor_search_id').val();
    var search_date = formatDate($('#searchDate').val());
    if(radio == '1'){
        f.push({field: "company_bills.vendor_id", op: "eq", data: vendor_id});
    } else if(radio == '2') {
        f.push({field: "company_bills.vendor_id", op: "eq", data: vendor_id},{field: "company_bills.due_date", op: "lt", data: search_date});
    } else if(radio == '3') {
        f.push({field: "company_bills.vendor_id", op: "eq", data: '0'});
    }
    grid[0].p.search = true;
    $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

