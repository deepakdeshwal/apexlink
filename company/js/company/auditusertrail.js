$(document).ready(function(){
    var currentPagePath = window.location.pathname;

    $(document).on("click",".addapexlinknewbtn", function(){
        $(".add_apexlink_new").show();
        $(".apexlink_new_listing").hide();
    });

    $(document).on("click",".cancelapexnew", function(){
        $(".add_apexlink_new").hide();
        $(".apexlink_new_listing").show();
    });

    $(document).on("click",".saveapexbtn", function(e){
        e.preventDefault();
        var title = $("input[name='title']").val();
        var date = $("input[name='date']").val();
        var time = $("input[name='time']").val();
        var desc = $("textarea[name='description']").val();
        if ($("#addapexnewform").valid()) {
            $.ajax({
                type: 'post',
                url: '/user-ajax',
                data: {
                    class: 'UserAjax',
                    action: "createApexlinkList",
                    title: title,
                    date: date,
                    time: time,
                    desc: desc,
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        $(".add_apexlink_new").hide();
                        $("#addapexnewform")[0].reset();
                        $(".apexlink_new_listing").show();
                        $('#apexlink_new_table').trigger( 'reloadGrid' );

                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                },
                error: function (data) {
                }
            });
        }

    });

    $("input[name='date']").datepicker({dateFormat: date_format});
    $("input[name='time']").timepicker({ timeFormat: 'h:mm:ss' });

    function statusFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "True";
        else
            return "False";
    }

    function roleFmatter (cellvalue, options, rowObject){
        if (cellvalue == "1")
            return "Super Admin";
        else
            return "Super Admin";
    }


    //enable text fields
    $(document).ready(function () {
        $('#disable_remove').click(function () {
            $('.form-control').removeAttr("disabled")
            $(".hide_btn").show();
        });

    });


    $(document).on("change", ".select_options", function (e) {
        var action = this.value;
        var id = $(this).attr('data_id');

        switch(action) {
            case "View":
                $(".apexlink_new_listing").hide();
                $(".list_hide").show();
                $('#edit_apexlink_new .form-control').attr("disabled",true);
                $("#edit_apexlink_new #user_id_hidden").val(id);
                $('#btnSubmit').removeAttr("disabled");
                getUserDetails(id);
                break;

            case "Edit":
                $(".apexlink_new_listing").hide();
                $(".list_hide").show();
                $(".listing_users").hide();
                $(".add_users_list").show();
                $("#edit_apexlink_new #user_id_hidden").val(id);
                $('#edit_apexlink_new .form-control').removeAttr("disabled");
                $(".hide_btn").show();
                $("#disable_remove").hide();
                getUserDetails(id);
                break;

            case "Disable":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                    if (result == true) {
                        changeStatusApexNewUser(id,action);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            case "Delete":
                //delete manage user
                bootbox.confirm("Do you want to delete this user?", function (result) {
                    if (result == true) {
                        deleteApexNewUser(id);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;


            case "Change Password":
                //change manage user password
                $('#changepassword').modal({backdrop: 'static',keyboard: false})
                $("#changepassword").modal('show');
                $("#changepassword input[name='id']").val(id);
                break;
            default:
                window.location.href = base_url+'/Setting/ApexNewUser';
        }
    });


    /*Delete apexlink new apexlink what's new*/

    function deleteApexNewUser(id) {
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "ApexNewUserAjax",
                action: "deleteApexNewUser",
                apexnewuser_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The record deleted successfully.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#apexlink_new_table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



    function changeStatusApexNewUser(id,action) {
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "ApexNewUserAjax",
                action: "changeStatusApexNewUser",
                apexnewuser_id: id,
                status_type: action,
            },
            success: function (response) {

                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The status has been changed.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#apexlink_new_table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    //////////////////////////////////////////////


    $("#updateUserForm").submit(function( event ) {
        event.preventDefault();
        if ($('#updateUserForm').valid()) {
            var formData = $('#updateUserForm').serializeArray();
            $.ajax({
                type: 'post',
                url: '/user-ajax',
                data: {class: 'UserAjax', action: "updateUser", form: formData},
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        localStorage.setItem("rowcolor",true)
                        window.location.href = '/Setting/ManageUser';
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                },
                error: function (data) {
                }
            });
        }
    });
    /**
     * To change the format of fax
     */
    if (currentPagePath == "/Setting/AddManageUser" || currentPagePath == "/Setting/ManageUser" ) {
        toggleSidebarMenu(1);
    }

    function toggleSidebarMenu(value){
        if (value == 1 ) {
            $(".default-sidebar").removeClass("collapsed");
            $(".default-sidebar, #leftnav2").attr("aria-expanded",true);
            $("#leftnav2").addClass("in");
            $("#leftnav2").removeAttr("style");
        }else{
            $(".default-sidebar").addClass("collapsed");
            $(".default-sidebar, #leftnav2").attr("aria-expanded",false);
            $("#leftnav2").removeClass("in");
            $("#leftnav2").css("height","0px");
        }
    }



    function deleteManageUser(id) {
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "ManageUserAjax",
                action: "deleteManageUser",
                manageuser_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The record deleted successfully.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#users-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    /*getUserDetails*/
    function auditUserTrail(id){
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "auditUserTrail",
                action: "auditUserTrail",
                user_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                $('#audit_trail_list').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("click",".updateForm", function(e){
        e.preventDefault();

        var id=$("#edit_apexlink_new input[name='user_id_hidden']").val();
        var title=$("#edit_apexlink_new input[name='title']").val();
        var date=$("#edit_apexlink_new input[name='date']").val();
        var time=$("#edit_apexlink_new input[name='time']").val();
        var description=$("#edit_apexlink_new textarea[name='description']").val();



        update_apexlink_list(id,title,date,time,description);

    });

    function update_apexlink_list(id,title,date,time,description){
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "update_apexlink_list",
                action: "update_apexlink_list",
                id: id,
                title: title,
                date: date,
                time: time,
                description: description
            },
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    localStorage.setItem("rowcolor",true)
                    window.location.href = '/Setting/ApexLinkNew';
                } else if(response.status == 'error' && response.code == 400){
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.'+key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function changeStatusManageUser(id,action) {
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "ManageUserAjax",
                action: "changeStatusManageUser",
                manageuser_id: id,
                status_type: action,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The status has been changed.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#users-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    jQuery.validator.setDefaults({
        debug: true,
        success: "valid"
    });

// function to validate first password
    $.validator.addMethod("pwcheck", function(value, element, error) {
        if (error) {
            $.validator.messages.pwcheck = '';
            return false;
        }
        return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(value)
    });

    $( "#change_pass_form" ).validate({
        rules: {
            password:{
                required:true,
                minlength:8,
                /*//pwcheck:true
                remote: {
                    url: "/user-ajax",
                    type: "post",
                    data: {
                      password: function() {
                        return $( "input[name='password']" ).val();
                      },
                      action: function() {
                        return "checkCurrentPassword";
                      }
                    },
                }*/
            },
            newpassword:{
                required:true,
                minlength:8,
                //pwcheck:true
            },
            confirmpassword:{
                minlength:8,
                equalTo :'#newpassword2'
            }
        },
        /*messages: {
           password: {
             remote: "Incorrect Password!"
           }
         }*/
    });


    /* change password validation trigger */

    $(document).on("keyup","input[name='password']",function(){
        var password = $(this).val();
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "checkCurrentPassword",
                action: "checkCurrentPassword",
                password: password,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $("input[name='password'], #password-error").addClass("valid ");
                    $("input[name='password']").removeClass("error");
                    $("#password-error").text("");
                } else if (response.status == 'error' && response.code == 400) {
                    $("input[name='password']").addClass("error");
                    $("input[name='password'], #password-error").removeClass("valid");
                    $("#password-error").text(response.message);
                    return false;
                }
            }
        });

    });


});