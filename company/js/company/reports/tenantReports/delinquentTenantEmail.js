$(document).on('change','#delinquent_tenant_email #dl_tenant_email_portfolio_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'fetchModalBalanceDetailOnchange',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#delinquent_tenant_email #dl_tenant_email_property_id").multiselect("destroy");
            $('#delinquent_tenant_email #dl_tenant_email_property_id').html(data.property_ddl);
            setTimeout(function(){
                $('#delinquent_tenant_email #dl_tenant_email_property_id').trigger('change');
            },1000);
                $('#delinquent_tenant_email #dl_tenant_email_property_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});

$(document).on('change','#delinquent_tenant_email #dl_tenant_email_property_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'onchangePropertyTenant',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#delinquent_tenant_email #dl_tenant_email_tenant_id").multiselect("destroy");
            $('#delinquent_tenant_email #dl_tenant_email_tenant_id').html(data.tenant_ddl);
            $('#delinquent_tenant_email #dl_tenant_email_tenant_id').multiselect({
                includeSelectAllOption: true,
                allSelectedText: 'All Selected',
                enableFiltering: true,
                nonSelectedText: 'Select'
            }).multiselect('selectAll', false).multiselect('updateButtonText');  
            // console.log(data.tenant_ddl);
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});


$(document).on('change','#delinquent_tenant_email #dl_tenant_email_status', function(){
    var val = $(this).val();
    // alert(val);
    // console.log(val);
    $.ajax({
        type : 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'onChangeStatus',
            id : val
        },
        success : ( response )=>{
            var data = $.parseJSON(response);
            $("#delinquent_tenant_email #dl_tenant_email_tenant_id").multiselect("destroy");
            $('#delinquent_tenant_email #dl_tenant_email_tenant_id').html(data.tenant_ddl);
            $('#delinquent_tenant_email #dl_tenant_email_tenant_id').multiselect({
                includeSelectAllOption: true,
                allSelectedText: 'All Selected',
                enableFiltering: true,
                nonSelectedText: 'Select'
            }).multiselect('selectAll', false).multiselect('updateButtonText');  
            // console.log(data.tenant_ddl);
        },
        error : ( err )=> {
            console.log(err);
        }
    });
});
