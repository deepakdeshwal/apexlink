$(document).on('change','#tenant_online_payment #to_payment_portfolio_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'fetchModalBalanceDetailOnchange',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#tenant_online_payment #to_payment_property_id").multiselect("destroy");
            $('#tenant_online_payment #to_payment_property_id').html(data.property_ddl);
            setTimeout(function(){
                $('#tenant_online_payment #to_payment_property_id').trigger('change');
            },1000);
                $('#tenant_online_payment #to_payment_property_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});


$(document).on('change','#tenant_online_payment #to_payment_property_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'onchangePropertyTenant',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#tenant_online_payment #to_payment_tenant_id").multiselect("destroy");
            $('#tenant_online_payment #to_payment_tenant_id').html(data.tenant_ddl);
            $('#tenant_online_payment #to_payment_tenant_id').multiselect({
                includeSelectAllOption: true,
                allSelectedText: 'All Selected',
                enableFiltering: true,
                nonSelectedText: 'Select'
            }).multiselect('selectAll', false).multiselect('updateButtonText');  
            // console.log(data.tenant_ddl);
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});