$(document).ready(function () {
    $(".main-content").contents().filter(function(){
        return (this.nodeType == 3);
    }).remove();
    $("body").contents().filter(function(){
        return (this.nodeType == 3);
    }).remove();
    $('ul.doc_download li:eq(0)').click(function()
    {
        Export2Doc1();
        $(".save-document-list").hide();
    });
    $('ul.doc_download li:eq(1)').click(function()
    {
        exportTableToExcel1();
        $(".save-document-list").hide();
    });
    $('ul.doc_download li:eq(2)').click(function()
    {
        exportTableToPpt1();
        $(".save-document-list").hide();
    });
    $('ul.doc_download li:eq(3)').click(function()
    {
        //getHtmlPdfConverter();
        $('.reports-loader').css('visibility','visible');
        $(".save-document-list").hide();
    });
    $('ul.doc_download li:eq(4)').click(function()
    {
        exportTableToTiff1();
        $(".save-document-list").hide();
    });
    $('ul.doc_download li:eq(5)').click(function()
    {
        exportTableTomhtml();
        $(".save-document-list").hide();
    });
    $('ul.doc_download li:eq(6)').click(function()
    {
        exportTableToCsv1();
        $(".save-document-list").hide();
    });
    $('ul.doc_download li:eq(7)').click(function()
    {
        exportTableToXml1();
        $(".save-document-list").hide();
    });
    $(document).on("click", ".ddlList", function () {
        $(".save-document-list").show();
    });
    $(".container-fluid").click(function(e) {
        $(".save-document-list").hide();
        // This should not be used unless you do not want
        // any click events registering inside the div
    });
    // $(".print_class").click(function(e) {
    //     getHtmlPdfConverter();
    //  // This should not be used unless you do not want
    //                          // any click events registering inside the div
    // });
    function Export2Doc(element, filename = ''){
        var innerHTML=$(".htmldownload").html();
        var html = innerHTML;
        var blob = new Blob(['\ufeff', html], {
            type: 'application/msword'
        });

        // Specify link url
        var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);

        // Specify file name
        filename = filename?filename+'.doc':'document.doc';

        // Create download link element
        var downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);

        if(navigator.msSaveOrOpenBlob ){
            navigator.msSaveOrOpenBlob(blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = url;

            // Setting the file name
            downloadLink.download = filename;

            //triggering the function
            downloadLink.click();
        }

        document.body.removeChild(downloadLink);
    }



    function Export2Doc1(element, filename = ''){

        $.ajax({
            type: 'post',
            url: '/Reporting-Ajax-download',
            data: {
                class: "reportHtmlfull",
                action: "AllreportsAjax",

            },
            success: function (response) {
                // console.log('html',response);return;
                // var response = JSON.parse(response);
                //  console.log(response);return;

                var htmls=response;

              //  var innerHTML=$(".htmldownload").html();
                var html = htmls;
                var blob = new Blob(['\ufeff', html], {
                    type: 'application/msword'
                });

                // Specify link url
                var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);

                // Specify file name
                filename = filename?filename+'.doc':'document.doc';

                // Create download link element
                var downloadLink = document.createElement("a");

                document.body.appendChild(downloadLink);

                if(navigator.msSaveOrOpenBlob ){
                    navigator.msSaveOrOpenBlob(blob, filename);
                }else{
                    // Create a link to the file
                    downloadLink.href = url;

                    // Setting the file name
                    downloadLink.download = filename;

                    //triggering the function
                    downloadLink.click();
                }

                document.body.removeChild(downloadLink);


            }
        });




        //
        // var innerHTML=$(".htmldownload").html();
        // var html = innerHTML;
        // var blob = new Blob(['\ufeff', html], {
        //     type: 'application/msword'
        // });
        //
        // // Specify link url
        // var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);
        //
        // // Specify file name
        // filename = filename?filename+'.doc':'document.doc';
        //
        // // Create download link element
        // var downloadLink = document.createElement("a");
        //
        // document.body.appendChild(downloadLink);
        //
        // if(navigator.msSaveOrOpenBlob ){
        //     navigator.msSaveOrOpenBlob(blob, filename);
        // }else{
        //     // Create a link to the file
        //     downloadLink.href = url;
        //
        //     // Setting the file name
        //     downloadLink.download = filename;
        //
        //     //triggering the function
        //     downloadLink.click();
        // }
        //
        // document.body.removeChild(downloadLink);
    }



    function exportTableToExcel(tableID, filename = ''){
        var innerHTML=$(".htmldownload").html();
        var html = innerHTML;
        var blob = new Blob(['\ufeff', html], {
            type: 'application/msword'
        });

        // Specify link url
        var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);

        // Specify file name
        filename = filename?filename+'.xls':'document.xls';

        // Create download link element
        var downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);

        if(navigator.msSaveOrOpenBlob ){
            navigator.msSaveOrOpenBlob(blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = url;

            // Setting the file name
            downloadLink.download = filename;

            //triggering the function
            downloadLink.click();
        }

        document.body.removeChild(downloadLink);
    }

    function exportTableToExcel1(tableID, filename = ''){



        $.ajax({
            type: 'post',
            url: '/Reporting-Ajax-download',
            data: {
                class: "reportHtmlfull",
                action: "AllreportsAjax",

            },
            success: function (response) {
                // console.log('html',response);return;
                // var response = JSON.parse(response);
                //  console.log(response);return;

                var htmls=response;
                    console.log(htmls);
                //  var innerHTML=$(".htmldownload").html();
                var html = htmls;
                var blob = new Blob(['\ufeff', html], {
                    type: 'application/vnd.ms-excel'
                });

                // Specify link url
                var url = 'data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent(html);

                // Specify file name
                filename = filename?filename+'.xls':'excel.xls';

                // Create download link element
                var downloadLink = document.createElement("a");

                document.body.appendChild(downloadLink);

                if(navigator.msSaveOrOpenBlob ){
                    navigator.msSaveOrOpenBlob(blob, filename);
                }else{
                    // Create a link to the file
                    downloadLink.href = url;

                    // Setting the file name
                    downloadLink.download = filename;

                    //triggering the function
                    downloadLink.click();
                }
//work on this function
                document.body.removeChild(downloadLink);

            }
        });


    }




    function exportTableToPpt(tableID, filename = ''){
        var innerHTML=$(".htmldownload").html();
        var html = innerHTML;
        var blob = new Blob(['\ufeff', html], {
            type: 'application/msword'
        });

        // Specify link url
        var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);

        // Specify file name
        filename = filename?filename+'.pptx':'document.pptx';

        // Create download link element
        var downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);

        if(navigator.msSaveOrOpenBlob ){
            navigator.msSaveOrOpenBlob(blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = url;

            // Setting the file name
            downloadLink.download = filename;

            //triggering the function
            downloadLink.click();
        }

        document.body.removeChild(downloadLink);
    }



    function exportTableToPpt1(tableID, filename = ''){


        $.ajax({
            type: 'post',
            url: '/Reporting-Ajax-download',
            data: {
                class: "reportHtmlfull",
                action: "AllreportsAjax",

            },
            success: function (response) {
                // console.log('html',response);return;
                // var response = JSON.parse(response);
                //  console.log(response);return;

                var htmls=response;
                var html=htmls;

//                 var pptx = null;
//
 // Presentation 1:
//                 pptx = new PptxGenJS();
//                 pptx.addNewSlide().addText('html', {x:1, y:1});
//                 pptx.save('PptxGenJS-Presentation-1');

                //  var innerHTML=$(".htmldownload").html();
              //  var html = htmls;
              //  var PptxGenJS = require("pptxgenjs");
              //  var pptx = new PptxGenJS();

// STEP 2: Add a new Slide to the Presentation
                //var slide = pptx.addNewSlide();

// STEP 3: Add any objects to the Slide (charts, tables, shapes, images, etc.)
//                 slide.addText(
//                     'html',
//                     { x:0.0, y:0.25, w:'100%', h:1.5, align:'c', fontSize:24, color:'0088CC', fill:'F1F1F1' }
//                 );

// STEP 4: Send the PPTX Presentation to the user, using your choice of file name
               // pptx.save('PptxGenJs-Basic-Slide-Demo');


                var blob = new Blob(['\ufeff', html], {
                    type: 'application/ms-powerpoint'
                });

                // Specify link url
                var url = 'data:ms-powerpoint;charset=utf-8,' + btoa(html);


                // Specify file name
                filename = filename?filename+'.pptx':'document.pptx';
                console.log("filename >> "+ filename);

                // Create download link element
                var downloadLink = document.createElement("a");

                document.body.appendChild(downloadLink);

                if(navigator.msSaveOrOpenBlob ){
                    navigator.msSaveOrOpenBlob(blob, filename);
                }else{
                    // Create a link to the file
                    downloadLink.href = url;

                    // Setting the file name
                    downloadLink.download = filename;

                    //triggering the function
                    downloadLink.click();
                }

                document.body.removeChild(downloadLink);

            }
        });


    }






    // function getHtmlPdfConverter(dataArray,datatypes){
    //     //var html =$(".htmldownload").html();
    //     var html =document.getElementById("htmldownload").innerHTML;
    //     $.ajax({
    //         type: 'post',
    //         url: '/Reporting-Ajax',
    //         data: {
    //             class: "reportsPopupData",
    //             action: "getPdfContent",
    //             html:html,
    //         },
    //         success: function (response) {
    //             var response = JSON.parse(response);
    //             if (response.status == 'success' && response.code == 200) {
    //                 var link=document.createElement('a');
    //                 document.body.appendChild(link);
    //                 link.target="_blank";
    //                 link.download="Report.pdf";
    //                 link.href=response.data.record;
    //                 link.click();
    //             } else if(response.code == 500) {
    //                 //toastr.warning(response.message);
    //             } else {
    //                 //toastr.error(response.message);
    //             }
    //         }
    //     });
    // }
    function exportTableToTiff(){

        $.ajax({
            type: 'post',
            url: '/Reporting-Ajax-download',
            data: {
                class: "reportHtmlfull",
                action: "AllreportsAjax",

            },
            success: function (response) {
                // console.log('html',response);return;
                // var response = JSON.parse(response);
                //  console.log(response);return;

                var htmls = response;

                //  var innerHTML=$(".htmldownload").html();
                var html1 = htmls;
                html2canvas(document.querySelector(html1)).then(canvas => {
                    var a = document.createElement('a');
                    a.href = canvas.toDataURL("image/tiff");
                    a.download = 'image.tiff';
                    a.click();
                });
            }
        });



    }
 function exportTableToTiff1(){
        html2canvas(document.querySelector("#htmldownload")).then(canvas => {
            var a = document.createElement('a');
            a.href = canvas.toDataURL("image/tiff");
            a.download = 'image.tiff';
            a.click();
        });
    }

    function exportTableTomhtml(){


        $.ajax({
            type: 'post',
            url: '/Reporting-Ajax-download',
            data: {
                class: "reportHtmlfull",
                action: "AllreportsAjax",

            },
            success: function (response) {
                // console.log('html',response);return;`
                // var response = JSON.parse(response);
                //  console.log(response);return;

               var request = new XMLHttpRequest();
                request.open("GET",  document.URL);
                request.onreadystatechange = function() {
                    if (request.readyState === 4 && request.status === 200) {

//response handling code

                    }
                };
                request.send(null); // Send the request now

                var link=document.createElement('a');
                                document.body.appendChild(link);
                                link.target="_blank";
                                link.download="Report.mhtml";
                                link.href=   document.URL;
                                link.click();
        }
        });

    }

    function download_csv(csv, filename) {
        var csvFile;
        var downloadLink;

        // CSV FILE
        csvFile = new Blob([csv], {type: "text/csv"});

        // Download link
        downloadLink = document.createElement("a");

        // File name
        downloadLink.download = filename;

        // We have to create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);

        // Make sure that the link is not displayed
        downloadLink.style.display = "none";

        // Add the link to your DOM
        document.body.appendChild(downloadLink);

        // Lanzamos
        downloadLink.click();
    }

    function export_table_to_csv(html, filename) {
        var csv = [];
        var rows = document.querySelectorAll("#htmldownload tr");

        for (var i = 0; i < rows.length; i++) {
            var row = [], cols = rows[i].querySelectorAll("td, th");

            for (var j = 0; j < cols.length; j++)
                row.push(cols[j].innerText);

            csv.push(row.join(","));
        }

        // Download CSV
        download_csv(csv.join("\n"), filename);
    }
    function exportTableToCsv() {

        var html=$(".htmldownload .reports_table").html();
        export_table_to_csv(html, "table.csv");
    }

    function exportTableToCsv1() {
        $.ajax({
            type: 'post',
            url: '/Reporting-Ajax-download',
            data: {
                class: "reportHtmlfull",
                action: "AllreportsAjax",

            },
            success: function (response) {
                // console.log('html',response);return;
                // var response = JSON.parse(response);
                //  console.log(response);return;
                var html=response;
                export_table_to_csv(html, "table.csv");

                //  va innerHTML=$(".htmldownload").html();

                var blob = new Blob(['\ufeff', html], {
                    type: 'text/csv'
                });

                // Specify link url
                var url = 'data:text/csv;charset=utf-8,' + encodeURIComponent(html);

                // Specify file name
                filename = filename?filename+'.csv':'table.csv';

                // Create download link element
                var downloadLink = document.createElement("a");

                document.body.appendChild(downloadLink);

                if(navigator.msSaveOrOpenBlob ){
                    navigator.msSaveOrOpenBlob(blob, filename);
                }else{
                    // Create a link to the file
                    downloadLink.href = url;

                    // Setting the file name
                    downloadLink.download = filename;

                    //triggering the function
                    downloadLink.click();
                }

                document.body.removeChild(downloadLink);

            }
        });
    }

    function exportTableToXml() {

        var xml = '<?xml version="1.0"?><Root><Classes>';
        $(".htmldownload .reports_table tr").each(function () {
            var cells = $("td", this);
            if (cells.length > 0) {
                xml += "<Class name='" + cells.eq(0).text() + "'>\n";
                for (var i = 1; i < cells.length; ++i) {
                    xml += "\t<data>" + cells.eq(i).text() + "</data>\n";
                }
                xml += "</Class >\n";
            }
        });
        xml += '</Classes></Root>';
        var filename="file.xml";
        csvFile = new Blob([xml], {type: "text/plain"});
        // Download link
        downloadLink = document.createElement("a");
        // File name
        downloadLink.download = filename;
        // We have to create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);
        // Make sure that the link is not displayed
        downloadLink.style.display = "none";
        // Add the link to your DOM
        document.body.appendChild(downloadLink);
        // Lanzamos
        downloadLink.click();
        //here you can rewrite the xmlstring to a new document
        //or use the hidden control to store the xml text, call the text in code behind.
        //also, you can call ajax to excuet codebehind and sava the xml file
    }



    function exportTableToXml1() {

        $.ajax({
            type: 'post',
            url: '/Reporting-Ajax-download',
            data: {
                class: "reportHtmlfull",
                action: "AllreportsAjax",

            },
            success: function (response) {
                // console.log('html',response);return;
                // var response = JSON.parse(response);
                //  console.log(response);return;
                var html=response;
                var xml = '<?xml version="1.0"?><Root><Classes>';
                $(html).each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) {
                        xml += "<Class name='" + cells.eq(0).text() + "'>\n";
                        for (var i = 1; i < cells.length; ++i) {
                            xml += "\t<data>" + cells.eq(i).text() + "</data>\n";
                        }
                        xml += "</Class >\n";
                    }
                });
                xml += '</Classes></Root>';
                var filename="file.xml";
                csvFile = new Blob([xml], {type: "text/plain"});
                // Download link
                downloadLink = document.createElement("a");
                // File name
                downloadLink.download = filename;
                // We have to create a link to the file
                downloadLink.href = window.URL.createObjectURL(csvFile);
                // Make sure that the link is not displayed
                downloadLink.style.display = "none";
                // Add the link to your DOM
                document.body.appendChild(downloadLink);
                // Lanzamos
                downloadLink.click();
                //here you can rewrite the xmlstring to a new document
                //or use the hidden control to store the xml text, call the text in code behind.
                //also, you can call ajax to excuet codebehind and sava the xml file

            }
        });


    }




    $("#usr").keyup(function(){
        myFunction(event);
    });

    function myFunction(event) {
        var base_url = window.location.origin;
        var page=$("#usr").val();
        var total=$("#totalPages").text();
        var total_pages=total.replace(" ", "");
        var x = event.keyCode;
        if (x == 13) {
            if(page>'0' && page!=''){
                window.location.href = base_url + '/Reporting/RentRollConsolidatedReport?page='+page;
            }
            else{
                window.location.href = base_url + '/Reporting/RentRollConsolidatedReport?page=1';
            }
        }
    }
    $(document).on('click','.show_fliter',function () {


        // if(data.field_name != 'status' && data.field_name != 'vendor_type' && data.field_name != 'vendor' &&  data.field_name != 'work_category' ) {
            $("#modalDataClass input[name='tenant[]'], input[name='owner[]'], input[name='vendor[]'], input[name='property[]'], input[name='portfolio[]']").multiselect({
                includeSelectAllOption: true,
                allSelectedText: 'All Selected',
                enableFiltering: true,
                nonSelectedText: 'Select'
            }).multiselect('selectAll', false).multiselect( 'rebuild' ).multiselect('updateButtonText');
        // }

        $("input[name='current_date']").datepicker({
            dateFormat: date_format,
            autoclose: true,
            changeMonth: true,
            changeYear: true
        }).datepicker("setDate", new Date());

        var date = new Date(), y = date.getFullYear(), m = date.getMonth();
        var firstDay = new Date(y, m, 1);
        var lastDay = new Date(y, m + 1, 0);

        $("input[name='start_date']").datepicker({
            dateFormat: date_format,
            autoclose: true,
            changeMonth: true,
            changeYear: true
        }).datepicker("setDate", firstDay);

        $("input[name='end_date']").datepicker({
            dateFormat: date_format,
            autoclose: true,
            changeMonth: true,
            changeYear: true
        }).datepicker("setDate", lastDay);

        var year = '';
        for (var i = y; i > y-101; i--)
        {
            year += "<option value='"+i+"'>"+i+"</option>";
        }

        $("select[name='start_year']").html(year);
        $("select[name='last_year']").html(year);

        var tenant_insurance= "<option value='All'> All</option>" +
            "<option value='yes'>Yes</option>" +
            "<option value='no'>No</option>";
        $("select[name='envelope_size']").html(tenant_insurance);

        var print_envelope = "<option value='1'>41/8 * 91⁄2</option>" +
            "<option value='2'>5/4</option>";
        $("select[name='envelope_size']").html(print_envelope);

        var mailing_label_type = "<option value='1'> HP Avery 5260 3x10</option>" +
            "<option value='2'> HP Avery 5260 2x10</option>";
        $("select[name='mailing_label_type']").html(mailing_label_type);

        $("#vendorList").modal('show');

    });

    /*07-11-19*/
    $(document).on('click','#emailOpenModalBtn',function(){
        $('#sentMailModalReport').modal('show');
    });

    $('#sentMailModalReport .bootstrap-tagsinput').tagsinput({
        confirmKeys: [13, 44]
    });
    $(document).on("click","#sentMailModalReport",function(event){
       $("#sentMailModalReport .input-table").hide();
    });

   /* $(document).on("keyup","#sentMailModalReport .bootstrap-tagsinput input",function(event){
        var keyString = $(this).val();
        console.log(keyString);
       $("#testdataDivTo .input-table").show();

        $.ajax({
            type: 'post',
            url: '/Reporting-Ajax',
            data: {
                class: "reportsPopupData",
                action: "searchEmailForReport",
                keyValue: keyString
            },
            success: function (response) {
                var response = JSON.parse(response);
                //console.log(response);
                if (response.status == 'success' && response.code == 200) {
                    var html = "";
                    for(var i=0; i<response.data.length;i++){
                        html += '<tr><td>'+ response.data[i].email +'</td></tr>';
                        $(".input-table table tbody").html(html);
                    }

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#ContactListing-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });*/

    $(document).on("click","#emailOpenModalBtn",function(event){
        $.ajax({
            type: 'post',
            url: '/Reporting-Ajax',
            data: {
                class: "reportsPopupData",
                action: "searchEmailForReport"
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log("List of user >> below");
                console.log(response);
                if (response.status == 'success' && response.code == 200) {
                    var html = "";
                    for(var i=0; i<response.data.length;i++){
                        html +='<option>'+ response.data[i].email +'</option>';
                    }
                    $("#searchEmailReports").html(html);

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#ContactListing-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });

    $(document).on("click",".input-table table tr td", function(){
        var emailValue = $(this).text();
        var spanEmail = "<span class='tag label label-info'>" + emailValue + "<span data-role='remove'></span></span>";
        var getInput = $("#testdataDivTo .bootstrap-tagsinput input").before(spanEmail);
    });
    $(document).on("click",".label-info span", function(){
        $(this).parent().remove();
    });


    $("#testdataDiv .bootstrap-tagsinput input").css('width','100% !important');

    /*07-11-19*/

    $('#reportEmailSend .forward-emailReport-btn').click(function() {
        reportMailGetData();
    });

        function reportMailGetData() {
        var UserTo= $("#searchEmailReports").val();
       // var Todata= $("#searchEmailReports").val();
        var UserCc= $("#testdataDivCc .bootstrap-tagsinput").val();
        var UserBcc= $("#testdataDivBcc .bootstrap-tagsinput").val();
        var reportName = $('#reportTitleSub').val();


        $.ajax({
            type: 'post',
            url: '/Reporting-Ajax',
            data: {
                class: "reportsPopupData",
                action: "sendMail",
                //Todata: Todata,
                UserTo: UserTo == ''?null:UserTo,
                UserCc: UserCc,
                UserBcc: UserBcc,
                reportName:reportName
            },
            success: function (response) {
                var response = JSON.parse(response);
               // console.log("Here >>");
                //console.log(response.data);
               // debugger;
                if (response.code == 200) {
                    toastr.success("The Email was send successfully");
                    //localStorage.setItem("Message","The email was sent successfully.");
                    $('#sentMailModalReport').modal('hide');


                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on('click','#testdataDivCc .bootstrap-tagsinput,#testdataDivBcc .bootstrap-tagsinput',function () {
        $("#sentMailModalReport #input-table").hide();
    });


    $(document).on('click','.print_class,ul.doc_download li:eq(3)', function getHtmlPdfConverterr(dataArray,datatypes){
        //var html =$(".htmldownload").html();
        //   var html =document.getElementById("htmldownloadd").innerHTML;
        $('.reports-loader').css('visibility','visible');
        $("#wait").css("display", "block");
        $.ajax({
            type: 'post',
            url: '/Reporting-Ajax-download',
            data: {
                class: "reportHtmlfull",
                action: "AllreportsAjax",
            },
            success: function (response) {
                // console.log('html',response);return;
                // var response = JSON.parse(response);
                //  console.log(response);return;
                var htmls=response;
                $.ajax({
                    type: 'post',
                    url: '/Reporting-Ajax',
                    data: {
                        class: "reportsPopupData",
                        action: "getPdfContent",
                        htmls: htmls
                    },
                    success: function (res) {
                        console.log("test");
                        var res= JSON.parse(res);
                        console.log(res.data);
                        if (res.code == 200) {
                            $('.reports-loader').css('visibility','hidden');
                            var link=document.createElement('a');
                            document.body.appendChild(link);
                            link.target="_blank";
                            link.download="Report.pdf";
                            link.href=res.data.record;
                            link.click();
                            $("#wait").css("display", "none");
                        } else if(res.code == 500) {
                            //toastr.warning(response.message);
                        } else {
                            //toastr.error(response.message);
                        }
                    }
                });
            }
        });
    });

    /*$('#searchEmailReports').select2({tags: true});*/
    $('#searchEmailReports').select2({
        tags: true,
        tokenSeparators: [",", ""],
        createTag: function (tag) {
            var term = $.trim(tag.term);

            if (term === '') {
                return null;
            }

            return {
                id: tag.term,
                text: tag.term,
                isNew : true
            };
        }
    });

    //restartMultipleLib();
    $(document).on("click",".show_fliter", function () {
        console.log("working");
        //restartMultipleLib();
        var idsArray = selectedFields();
        if(idsArray.length > 0){
            makeSelectedById(idsArray);
        }
    });

    $(document).on("change",".multiselect-item.multiselect-all input", function () {
        if ($(this).is(":checked")) {
            $(".multiselect-container input").attr("checked",true);
        } else {
            $(".multiselect-container input").attr("checked",false);
        }
    });

});

function restartMultipleLib() {
    var multiselectClass = $(".multiSelectfield");
    if (multiselectClass.length > 0){
        $.each(multiselectClass, function() {
            var elemId = this.id;
            var selectfield = $("#"+elemId);
            var getMultiULHtml = $("#"+elemId).next().find("ul").html();
            var selectfield = $("#"+elemId).parent().replaceWith(selectfield);
            var selectfield = $("#"+elemId).removeClass("multiSelectfield");
            var selectfield = $("#"+elemId).removeClass("valid");
            var selectfield = $("#"+elemId).removeAttr("area-required");
            var selectfield = $("#"+elemId).multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
            /*if ($(this).hasClass("active")){
                $(".multiselect-container input").attr("checked",true);
            }else{
                $(".multiselect-container input").attr("checked",false);
            }*/
        });

    }
}

function selectedFields(){
    var idArray = [];
    $(".multiselect.dropdown-toggle.text-left").each(function(){
        var elem = $(this);
        var selectField = elem.parent().prev().attr("id");
        idArray.push(selectField);
    });
    return idArray;

}

// function makeSelectedById(idsArray){
//     for (var i = 0; i< idsArray.length; i++){
//         var titles = $("#"+idsArray[i]).next().find(".multiselect.dropdown-toggle.text-left").attr("title");
//         var newTitleArr = titles.split(",").map(function(item) { return item.trim(); });
//         console.log(newTitleArr);
//         for(var j=0; j < newTitleArr.length; j++){
//             var labelName = $("#"+idsArray[i]).next().find("ul.multiselect-container li a");
//             var x = labelName.find('label[title="'+newTitleArr[j]+'"]');
//             if(labelName.length > 0){
//                 x.find("input").attr("checked",true);
//                 var checkSelectAll = x.find("input[value='multiselect-all']").is(":checked");
//                 var checkSelectAll = $("#"+idsArray[i]).next().find("ul li.multiselect-all").hasClass("active");
//                 if(checkSelectAll){
//                     $("#"+idsArray[i]).next().find("ul li.multiselect-all a label input").attr("checked",true);
//                 }else{
//                     $("#"+idsArray[i]).next().find("ul li.multiselect-all a label input").attr("checked",false);
//                 }
//             }
//         }
//
//
//     }
// }

function makeSelectedById(idsArray){
    $('.btn-group').remove();
    for (var i = 0; i< idsArray.length; i++){
        var select = $('#'+idsArray[i]).wrap('<p>').parent().html();
        var parentMultiselect =  $('#'+idsArray[i]).parent();
        var parent =  $('#'+idsArray[i]).parent().parent();
        parent.append(select);
        parentMultiselect.remove();
        //$('.btn-group').remove();
        $('#'+idsArray[i]).multiselect('destroy').multiselect({
            includeSelectAllOption: true,
            allSelectedText: 'All Selected',
            enableFiltering: true,
            nonSelectedText: 'Select'
        }).multiselect('selectAll', false).multiselect('updateButtonText');
    }
}
