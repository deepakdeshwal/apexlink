$(document).ready(function () { 
    $(document).on('change','#gmf_collection #collection_portfolio_id',function(){
        var val=$(this).val();

        $.ajax({
            type: 'POST',
            url: '/CommonReportsModal-Ajax',
            data: {
                class: 'balanceSheetReport',
                action: 'fetchModalBalanceDetailOnchange',
                id : val
            },
            success: ( response )=>{
             
                var data = $.parseJSON(response);
                $("#collection_property_id").multiselect("destroy");
                $('#gmf_collection #collection_property_id').html(data.property_ddl);
                setTimeout(function(){
                    $('#gmf_collection #collection_property_id').trigger('change');
                },1000);
                    $('#gmf_collection #collection_property_id').multiselect({
                        includeSelectAllOption: true,
                        allSelectedText: 'All Selected',
                        enableFiltering: true,
                        nonSelectedText: 'Select'
                    }).multiselect('selectAll', false).multiselect('updateButtonText');
            },
            error: ( err )=>{
                console.log(err);
            } 
            
        });
    });

    //tenant on change

    // tenant on change
 $(document).on('change','#gmf_collection #collection_property_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'onchangePropertyTenant',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#gmf_collection #collection_tenant_id").multiselect("destroy");
            $('#gmf_collection #collection_tenant_id').html(data.tenant_ddl);
            $('#gmf_collection #collection_tenant_id').multiselect({
                includeSelectAllOption: true,
                allSelectedText: 'All Selected',
                enableFiltering: true,
                nonSelectedText: 'Select'
            }).multiselect('selectAll', false).multiselect('updateButtonText');  
            console.log(data.tenant_ddl);
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});
});
