$(document).on('change','#trial_balance #trial_balance_portfolio_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'fetchModalBalanceDetailOnchange',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#trial_balance #trial_balance_property_id").multiselect("destroy");
            $('#trial_balance #trial_balance_property_id').html(data.property_ddl);
                $('#trial_balance #trial_balance_property_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});