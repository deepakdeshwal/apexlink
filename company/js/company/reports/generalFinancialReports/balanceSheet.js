$(document).ready(function () {   
    $(document).on('change','#balance-sheet-detail #portfolio_id',function(){
        var val=$(this).val();
        $.ajax({
            type: 'POST',
            url: '/CommonReportsModal-Ajax',
            data: {
                class: 'balanceSheetReport',
                action: 'fetchModalBalanceDetailOnchange',
                id : val
            },
            success: ( response )=>{
                var data = $.parseJSON(response);
                console('nidhi');
                $("#balance_property_id").multiselect("destroy");
                $('#balance-sheet-detail #balance_property_id').html(data.property_ddl);
                    $('#balance-sheet-detail #balance_property_id').multiselect({
                        includeSelectAllOption: true,
                        allSelectedText: 'All Selected',
                        enableFiltering: true,
                        nonSelectedText: 'Select'
                    }).multiselect('selectAll', false).multiselect('updateButtonText');
            },
            error: ( err )=>{
                console.log(err);
            } 
            
        });
    });
});



