$(document).ready(function () { 
    $(document).on('change','#ps_unbill #unbill_portfolio_id',function(){
        var val=$(this).val();

        $.ajax({
            type: 'POST',
            url: '/CommonReportsModal-Ajax',
            data: {
                class: 'balanceSheetReport',
                action: 'fetchModalBalanceDetailOnchange',
                id : val
            },
            success: ( response )=>{
             
                var data = $.parseJSON(response);
                $("#unbill_property_id").multiselect("destroy");
                $('#ps_unbill #unbill_property_id').html(data.property_ddl);
                setTimeout(function(){
                $('#ps_unbill #unbill_property_id').trigger('change');
                },1000);
                    $('#ps_unbill #unbill_property_id').multiselect({
                        includeSelectAllOption: true,
                        allSelectedText: 'All Selected',
                        enableFiltering: true,
                        nonSelectedText: 'Select'
                    }).multiselect('selectAll', false).multiselect('updateButtonText');
            },
            error: ( err )=>{
                console.log(err);
            } 
            
        });
    });

    //tenant on change

    // tenant on change
  $(document).on('change','#ps_unbill #unbill_property_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'onchangePropertyVendor',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#unbill_vendor_id").multiselect("destroy");
            $('#ps_unbill #unbill_vendor_id').html(data.vendor_ddl);
                $('#ps_unbill #unbill_vendor_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            // console.log(data.vendor_ddl);
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});

  // on change status

  $(document).on('change','#ps_unbill #pr_unbill_id',function(){
   
    var val=$(this).val();
    //var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'onchangeStatusVendor',
            id : val
        },
        success: ( response )=>{
            var data = $.parseJSON(response);
            $("#unbill_vendor_id").multiselect("destroy");
            $('#ps_unbill #unbill_vendor_id').html(data.status_ddl);
                $('#ps_unbill #unbill_vendor_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            // console.log(data.vendor_ddl);
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});
});
