$(document).ready(function () { 
    $(document).on('change','#property_statement #properties_portfolio_id',function(){
        var val=$(this).val();

        $.ajax({
            type: 'POST',
            url: '/CommonReportsModal-Ajax',
            data: {
                class: 'balanceSheetReport',
                action: 'fetchModalBalanceDetailOnchange',
                id : val
            },
            success: ( response )=>{
             
                var data = $.parseJSON(response);
                $("#properties_property_id").multiselect("destroy");
                $('#property_statement #properties_property_id').html(data.property_ddl);
                    $('#property_statement #properties_property_id').multiselect({
                        includeSelectAllOption: true,
                        allSelectedText: 'All Selected',
                        enableFiltering: true,
                        nonSelectedText: 'Select'
                    }).multiselect('selectAll', false).multiselect('updateButtonText');
            },
            error: ( err )=>{
                console.log(err);
            } 
            
        });
    });

   
  // on change status

  $(document).on('change','#property_statement #prop_record_id',function(){
   
    var val=$(this).val();
    //var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'onchangeStatusProperty',
            id : val
        },
        success: ( response )=>{

            var data = $.parseJSON(response);
            $("#properties_property_id").multiselect("destroy");
            $('#property_statement #properties_property_id').html(data.propstatus_ddl);
                $('#property_statement #properties_property_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            // console.log(data.vendor_ddl);
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});
});
