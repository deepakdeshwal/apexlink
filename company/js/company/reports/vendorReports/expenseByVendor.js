$(document).on('change','#expense_by_vendor #eb_vendor_portfolio_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'fetchModalBalanceDetailOnchange',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#expense_by_vendor #eb_vendor_property_id").multiselect("destroy");
            $('#expense_by_vendor #eb_vendor_property_id').html(data.property_ddl);
            setTimeout(function(){
                $('#expense_by_vendor #eb_vendor_property_id').trigger('change');
            },1000);
                $('#expense_by_vendor #eb_vendor_property_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});


$(document).on('change','#expense_by_vendor #eb_vendor_property_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'onchangePropertyVendor',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#eb_vendor_vendor_id").multiselect("destroy");
            $('#expense_by_vendor #eb_vendor_vendor_id').html(data.vendor_ddl);
                $('#expense_by_vendor #eb_vendor_vendor_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            // console.log(data.tenant_ddl);
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});



$(document).on('change','#expense_by_vendor #eb_vendor_status',function(){
    //alert('fnkjnb');
   
    var val=$(this).val();
    //var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'onchangeStatusVendor',
            id : val
        },
        success: ( response )=>{
            var data = $.parseJSON(response);
            $("#eb_vendor_vendor_id").multiselect("destroy");
            $('#expense_by_vendor #eb_vendor_vendor_id').html(data.status_ddl);
                $('#expense_by_vendor #eb_vendor_vendor_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            // console.log(data.vendor_ddl);
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});
