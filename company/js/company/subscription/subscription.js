$(document).ready(function(){

    //Ajax to create a clock settings
    $("#financialCardInfo").submit(function( event ) {
        event.preventDefault();
        if ($('#financialCardInfo').valid()) {
            $('#loadingmessage').show();
            var formData = $('#financialCardInfo').serializeArray();
            $.ajax({
                type: 'post',
                url: '/payment-ajax',
                data: {
                    class: 'PaymentAjax',
                    action: "addCard",
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success'){
                        $('#loadingmessage').hide();
                        toastr.success(response.message);
                        if(response.stripe_account_id){
                            $('#billing-subscription').modal('toggle');
                            setTimeout(function(){
                                window.location.reload();
                            }, 3000);
                        }else{
                            $('#billing-subscription').modal('hide');
                            $(".basic-payment-detail").show();
                            $(".apx-adformbox-content").hide();
                            //$(".basicDiv").addClass("active");
                            $(".paymentDiv").removeClass("active");
                            setTimeout(function(){
                                window.location.reload();
                            }, 3000);
                        }

                    } else {
                        $('#loadingmessage').hide();
                        toastr.error(response.message);
                    }
                },
                error: function (data) {

                }
            });
        }


    });

    var min = new Date().getFullYear(),
        max = min + 49,
        select = document.getElementById('cexpiry_year');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var min = 1,
        max =12,
        select = document.getElementById('cexpiry_month');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }
    getCompanyDefaultData();


    /*
For zipcode functionality

 */
    $(document).on('focusout','#czip_code',function(){
        getAddressInfoByZip($(this).val());
    });

    /*
    For zipcode functionality

     */
    $("#czip_code").keydown(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            getAddressInfoByZip($(this).val());
        }
    });

    /**
     *Get location on the basis of zipcode
     */
    function getAddressInfoByZip(zip){
        if(zip.length >= 5 && typeof google != 'undefined'){
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': zip }, function(results, status){
                if (status == google.maps.GeocoderStatus.OK){
                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++){
                            //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                            var types = results[0].address_components[ii].types.join(",");
                            if (types == "street_number"){
                                addr.street_number = results[0].address_components[ii].long_name;
                            }
                            if (types == "route" || types == "point_of_interest,establishment"){
                                addr.route = results[0].address_components[ii].long_name;
                            }
                            if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                                addr.city =  results[0].address_components[ii].short_name ;
                            }
                            if (types == "administrative_area_level_1,political"){
                                addr.state = results[0].address_components[ii].short_name;
                            }
                            if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                                addr.zipcode = results[0].address_components[ii].long_name;
                            }
                            if (types == "country,political"){
                                addr.country = results[0].address_components[ii].long_name;
                            }
                        }
                        addr.success = true;
                        /* for (name in addr){
                             console.log('### google maps api ### ' + name + ': ' + addr[name] );
                         }*/
                        setTimeout(function(){
                            response(addr);
                        }, 2000);

                    } else {
                        response({success:false});
                    }
                } else {
                    response({success:false});
                }
            });
        } else {
            response({success:false});
        }
    }

    /**
     * Set values in the city, state and country when focus loose from zipcode field.
     */
    function response(obj){
        if(obj.success){
            $('#ccountry').val(obj.country);
            $('#ccity').val(obj.city);
            $('#cstate').val(obj.state);

        } else {
            $('#ccity').val('');
            $('#cstate').val('');
            $('#ccountry').val('');
        }
    }

    $.ajax({
        type: 'post',
        url: '/payment-ajax',
        data: {
            class: 'PaymentAjax',
            action: "getAccountVerification"
        },
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status == 'success'){
                if(response.account_status == 'Verified'){
                    var message = 'Verified';
                    $("#business_type").val(response.account_data.business_type)
                    $("#account_verification").val('verified');
                    $('.account_verification_status').addClass("verified")
                    $(".basic-payment-detail").hide();
                    $(".basic-user-payment-detail").hide();
                    $("#basicDiv").css("display","none");
                    $(".basicDiv_li").css("display","none");
                    $(".basic_div_span").css("display","none");
                    $('.payment_li').addClass('after-hide');
                    $(".right_tick").css("display","block");


                }else{
                    $("#business_type").val(response.account_data.business_type)
                    $("#account_verification").val('notverified');
                    $('.account_verification_status').addClass("not_verified");
                    var message = 'Not Verified .  To recieve payments submit account details';
                    $("#account_verification").val('');
                    $('.unverified-icon').css("display","block");
                }
                $('.account_verification_status').html(message);

            } else {
                toastr.error(response.message);
            }
        },
        error: function (data) {

        }
    });

    $.ajax({
        type: 'post',
        url: '/payment-ajax',
        data: {
            class: 'PaymentAjax',
            action: "getMccTypes"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if (data.data.mcc_types.length > 0) {
                    var mccOption = "<option value='0'>Select</option>";
                    $.each(data.data.mcc_types, function (key, value) {
                        mccOption += "<option value='" + value.code + "' data-id='" + value.code + "'>" + value.name + " - " + value.code + " " + "</option>";
                    });
                    $('#fmcc,#fcmcc').html(mccOption);
                }

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

    jQuery('.phone_format').mask('000-000-0000', {reverse: true});




    /*
For zipcode functionality

*/
    $(document).on('focusout','#fzipcode',function(){
        getAddressInfoByZip($(this).val());
    });

    /*
    For zipcode functionality

     */
    $("#fzipcode").keydown(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            getAddressInfoByZip($(this).val());
        }
    });

    /**
     *Get location on the basis of zipcode
     */
    function getAddressInfoByZip(zip){
        if(zip.length >= 5 && typeof google != 'undefined'){
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': zip }, function(results, status){
                if (status == google.maps.GeocoderStatus.OK){
                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++){
                            //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                            var types = results[0].address_components[ii].types.join(",");
                            if (types == "street_number"){
                                addr.street_number = results[0].address_components[ii].long_name;
                            }
                            if (types == "route" || types == "point_of_interest,establishment"){
                                addr.route = results[0].address_components[ii].long_name;
                            }
                            if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                                addr.city =  results[0].address_components[ii].short_name ;
                            }
                            if (types == "administrative_area_level_1,political"){
                                addr.state = results[0].address_components[ii].short_name;
                            }
                            if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                                addr.zipcode = results[0].address_components[ii].long_name;
                            }
                            if (types == "country,political"){
                                addr.country = results[0].address_components[ii].long_name;
                            }
                        }
                        addr.success = true;
                        /* for (name in addr){
                             console.log('### google maps api ### ' + name + ': ' + addr[name] );
                         }*/
                        setTimeout(function(){
                            response1(addr);
                        }, 2000);

                    } else {
                        response1({success:false});
                    }
                } else {
                    response1({success:false});
                }
            });
        } else {
            response1({success:false});
        }
    }

    /**
     * Set values in the city, state and country when focus loose from zipcode field.
     */
    function response1(obj){
        if(obj.success){
            $('#fcity').val(obj.city);
            $('#fstate').val(obj.state);

        } else {
            $('#fcity').val(obj.city);
            $('#fstate').val(obj.state);
        }
    }



    $(document).on("click","#basicDiv ",function(){
        $.ajax({
            type: 'post',
            url: '/payment-ajax',
            data: {
                class: 'PaymentAjax',
                action: "getConnectedAccount"
            },
            success: function (response) {
                var data = $.parseJSON(response);
                console.log(data);
                if (data.status == "success") {
                    if(data.data.data.business_type == "company"){
                        $(".fmcc").show();
                        $(".furl").show();
                        $("#fcmcc").val(data.data.data.mcc);
                        $("#fcurl").val(data.data.data.url);
                        $("#fcbusiness").val(data.data.data.business_type);
                        $("#fccity").val(data.data.data.company_city);
                        $("#fcaddress").val(data.data.data.company_address1);
                        $("#fcaddress2").val(data.data.data.company_address2);
                        $("#fczipcode").val(data.data.data.company_postal_code);
                        $("#fcstate").val(data.data.data.company_state);
                        $("#fcname").val(data.data.data.company_name);
                        $("#fcphone_number").val(data.data.data.company_phone_number);
                        $("#fctax_id").val(data.data.data.tax_id);
                        $("#fpfirst_name").val(data.data.data.first_name);
                        $("#fplast_name").val(data.data.data.last_name);
                        $("#fpemail").val(data.data.data.email);
                        $("#fpphone_number").val(data.data.data.phone);
                        $("#fpday").val(data.data.data.day);
                        $("#fpmonth").val(data.data.data.month);
                        $("#year").val(data.data.data.year);
                        $("#fpaddress").val(data.data.data.address_1);
                        $("#fpaddress2").val(data.data.data.address_2);
                        $("#fpzipcode").val(data.data.data.postal_code);
                        $("#fpstate").val(data.data.data.state);
                        $("#fpcity").val(data.data.data.state);
                    }else if(data.data.data.business_type == "individual"){
                        // $(".fmcc").hide();
                        // $(".furl").hide();
                        $("#furl").val(data.data.data.url);
                        $("#fmcc").val(data.data.data.mcc);
                        $("#fbusiness").val(data.data.data.business_type);
                        $("#ffirst_name").val(data.data.data.first_name);
                        $("#flast_name").val(data.data.data.last_name);
                        $("#femail").val(data.data.data.email);
                        $("#fphone_number").val(data.data.data.phone);
                        $("#fday").val(data.data.data.day);
                        $("#fmonth").val(data.data.data.month);
                        $("#year").val(data.data.data.year);
                        $("#faddress").val(data.data.data.address_1);
                        $("#faddress2").val(data.data.data.address_2);
                        $("#fzipcode").val(data.data.data.postal_code);
                        $("#fstate").val(data.data.data.state);
                        $("#fcity").val(data.data.data.state);
                    }
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });


    });

    $("#fbirth_date").datepicker({dateFormat: date_format,
        changeYear: true,
        yearRange: "-100:+20",
        changeMonth: true,
        maxDate: new Date(),
    });




    var min = new Date().getFullYear(),
        max = min - 49,
        select = document.getElementById('fyear');
    for (var i = max; i<=min; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var min = 1,
        max =31,
        select = document.getElementById('fday');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var month = 0;
    // var i = 1;
    for (; month < monthNames.length; month++) {

        var new_month = month+1;
        $('#fmonth').append('<option value='+new_month+'>' + monthNames[month] + '</option>');
    }





    var min = new Date().getFullYear(),
        max = min - 49,
        select = document.getElementById('fpyear');
    for (var i = max; i<=min; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var min = 1,
        max =31,
        select = document.getElementById('fpday');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var month = 0;
    // var i = 1;
    for (; month < monthNames.length; month++) {

        var new_month = month+1;
        $('#fpmonth').append('<option value='+new_month+'>' + monthNames[month] + '</option>');
    }

    /**
     * If the letter is not digit in phone number then don't type anything.
     */
    $("#phone_number,#fphone_number,#fssn,#fpphone_number,#fpssn").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });


    $(document).on('click','#savefinancial',function (e) {
        e.preventDefault();
        var financial_data = $( "#financialInfo" ).serializeArray();
        console.log(financial_data);
        $.ajax
        ({
            type: 'post',
            url: '/payment-ajax',
            data: {
                class: "PaymentAjax",
                action: "validateFinancialData",
                data:financial_data
            },
            success: function (response) {
                var response = $.parseJSON(response);
                if(response.code == 400)
                {
                    $.each(response.data, function (key) {
                        $('.' + key).text('* This field is required');
                    });
                }else{
                    localStorage.setItem('financial_data',JSON.stringify(financial_data));


                    toastr.clear();
                    updateUserAccount(financial_data);

                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('.' + key + 'Err').text(value);
                });
            }
        });




    });





    $(document).on('click','#savecompanyfinancial',function (e) {
        e.preventDefault();
        var financial_data = $( "#financialCompanyInfo" ).serializeArray();
        console.log(financial_data);


        $.ajax
        ({
            type: 'post',
            url: '/payment-ajax',
            data: {
                class: "paymentAjax",
                action: "validateCompanyFinancialData",
                data:financial_data
            },
            success: function (response) {
                var response = $.parseJSON(response);
                if(response.code == 400)
                {
                    $.each(response.data, function (key) {
                        $('.' + key).text('* This field is required');
                    });
                }else{
                    localStorage.setItem('financial_data',JSON.stringify(financial_data));


                    toastr.clear();
                    updateCompanyAccount(financial_data);

                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('.' + key + 'Err').text(value);
                });
            }
        });




    });

    $(document).on('click','#edit_payment_method',function(){

        localStorage.setItem('scroll_bottom',true);
        window.location.href = '/User/AccountSetup';

    });

    $('#billing-subscription').modal({backdrop: 'static', keyboard: false,show: false});


});
/*
function to get default company data
*/
function getCompanyDefaultData() {
    $.ajax({
        type: 'post',
        url: '/get-company-default-data',
        data: {
            class: 'commonPopup',
            action: 'getCompanyDefaultData'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#ccity').val(res.record.city);
            $('#cstate').val(res.record.state);
            $('#ccountry').val(res.record.country);
            $('#czip_code').val(res.record.zip_code);
        },
    });

}

function updateCompanyAccount(financial_data){
    $('#loadingmessage').show();
    var company_id = $("#hidden_company_id").val();
    $.ajax({
        type: 'post',
        url: '/payment-ajax',
        data: {class: 'PaymentAjax', action: "updateCompanyConnectedAccount",financial_data:financial_data,"company_id":company_id },
        success : function(response){
            var response = JSON.parse(response);
            if(response.status == 'success' && response.code == 200){
                $('#loadingmessage').hide();
                localStorage.removeItem("financial_data");
                $('#financial-company-info').modal('toggle');
                toastr.success("Account Updated Successfully.");
            } else if(response.status == 'error' && response.code == 400){
                if(response.message == 'online payment error')
                {
                    toastr.error('Please fill all online payment required fields.');
                }else{
                    console.log()
                }
                $('#loadingmessage').hide();
                $('.error').html('');
            }
            else if(response.status == 'failed' && response.code == 400){
                toastr.error(response.message);
            }
            $('#loadingmessage').hide();
            $('.error').html('');
        },
        error: function (response) {
            // console.log(response);
        }
    });
}


/* google zip code by  company */
$( "#fczipcode" ).focusout(function() {
    getAddressInfoByZipforCompanyFinance($(this).val());
});
/**
 * Auto fill the city, state and country when enter key is clicked.
 */
$( "#fczipcode" ).keydown(function(event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13') {
        getAddressCompanyInfoByZip($(this).val());
    }
});


function getAddressInfoByZipforCompanyFinance(zip){
    if(zip.length >= 5 && typeof google != 'undefined'){
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                         console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    responseCompanyFinance(addr);
                } else {
                    responseCompanyFinance({success:false});
                }
            } else {
                responseCompanyFinance({success:false});
            }
        });
    } else {
        responseCompanyFinance({success:false});
    }
}
/**
 * Set values in the city, state and country when focus loose from zipcode field.
 */
function responseCompanyFinance(obj){
    //console.log(obj);
    if(obj.success){
        $('#fccity').val(obj.city);
        $('#fcstate').val(obj.state);
        // $('#fcountry').val(obj.country);
    } else {
        $('#fccity').val('');
        $('#fcstate').val('');
        // $('#fcountry').val('');
    }
}

/**
 *Get location on the basis of zipcode
 */

function getAddressCompanyInfoByZip(zip){
    if(zip.length >= 5 && typeof google != 'undefined'){
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                         console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    responseCompanyFinance(addr);
                } else {
                    responseCompanyFinance({success:false});
                }
            } else {
                responseCompanyFinance({success:false});
            }
        });
    } else {
        responseCompanyFinance({success:false});
    }
}





/* google zip code by  person */
$( "#fpzipcode" ).focusout(function() {
    getAddressInfoByZipforPersonFinance($(this).val());
});
/**
 * Auto fill the city, state and country when enter key is clicked.
 */
$( "#fpzipcode" ).keydown(function(event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13') {
        getAddressPersonInfoByZip($(this).val());
    }
});


function getAddressInfoByZipforPersonFinance(zip){
    if(zip.length >= 5 && typeof google != 'undefined'){
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                         console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    responsePersonFinance(addr);
                } else {
                    responsePersonFinance({success:false});
                }
            } else {
                responsePersonFinance({success:false});
            }
        });
    } else {
        responsePersonFinance({success:false});
    }
}
/**
 * Set values in the city, state and country when focus loose from zipcode field.
 */
function responsePersonFinance(obj){
    //console.log(obj);
    if(obj.success){
        $('#fpcity').val(obj.city);
        $('#fpstate').val(obj.state);
        // $('#fcountry').val(obj.country);
    } else {
        $('#fpcity').val('');
        $('#fpstate').val('');
        // $('#fcountry').val('');
    }
}

/**
 *Get location on the basis of zipcode
 */

function getAddressPersonInfoByZip(zip){
    if(zip.length >= 5 && typeof google != 'undefined'){
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                         console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    responsePersonFinance(addr);
                } else {
                    responsePersonFinance({success:false});
                }
            } else {
                responsePersonFinance({success:false});
            }
        });
    } else {
        responsePersonFinance({success:false});
    }
}
// $("#myImg").hide();
$(function () {
    $(":file").change(function () {
        if (this.files && this.files[0]) {
            file = this.files[0];
            formdata = new FormData();
            formdata.append("company_document", file);
            formdata.append("action",'uploadCompanyDocument');
            formdata.append("class",'PaymentAjax');
            $.ajax({
                url: '/payment-ajax',
                enctype: 'multipart/form-data',
                data: formdata,
                type: "POST",
                processData: false,
                contentType: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        // $('#company_document').val('');

                        $("#company_document_id").val(response.document_id);
                        toastr.success(response.message);
                        $('#loadingmessage').hide();
                    } else {
                        $('#company_document').val('')
                        toastr.error(response.message);
                        $('#loadingmessage').hide();
                    }
                },
                error: function (response) {
                    // console.log(response);
                }
            });
        }
    });
});

$(document).ready(function(){
    $('input').on('input',function(){
        var textb = $(this).val();
        if(textb.length){
            $(this).siblings('.error').html('');
        }
    });
    $('#phone_number').on('input',function(){
        var textb = $(this).val();
        if(textb.length){
            $(this).parents('.intl-tel-input').siblings('.error').html('');
        }
    });
    $('select').on('change',function(){
        var textb = $(this).val();
        if(textb.length){
            $(this).siblings('.error').html('');
        }
    });
    $('#fphone_number').on('input',function(){
        var textb = $(this).val();
        if(textb.length){
            $(this).parents('.intl-tel-input').siblings('.error').html('');
        }
    });
    $('#fbirth_date').on('change',function(){
        var textb = $(this).val();
        if(textb.length){
            $(this).siblings('.error').html('');
        }
    });
    $('#faddress').on('input',function(){
        var textb = $(this).val();
        if(textb.length){
            $(this).siblings('.error').html('');
        }
    });

});

function updateUserAccount(financial_data){
    $('#loadingmessage').show();
    var company_id = $("#hidden_company_id").val();
    $.ajax({
        type: 'post',
        url: '/payment-ajax',
        data: {class: 'PaymentAjax', action: "updateUserConnectedAccount",financial_data:financial_data,"company_id":company_id },
        success : function(response){
            var response = JSON.parse(response);
            if(response.status == 'success' && response.code == 200){
                $('#loadingmessage').hide();
                localStorage.removeItem("financial_data");
                $('#financial-info').modal('hide');
                $('#billing-subscription').modal('hide');
                toastr.success("Account Updated Successfully.");
                setTimeout(function(){
                   window.location.reload();
                }, 3000);


            } else if(response.status == 'error' && response.code == 400){
                if(response.message == 'online payment error')
                {
                    toastr.error('Please fill all online payment required fields.');
                }else{
                    console.log()
                }
                $('#loadingmessage').hide();
                $('.error').html('');
            }
            else if(response.status == 'failed' && response.code == 400){
                toastr.error(response.message);
            }
            $('#loadingmessage').hide();
            $('.error').html('');
        },
        error: function (response) {
            // console.log(response);
        }
    });
}







