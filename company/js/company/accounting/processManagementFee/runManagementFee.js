$(document).ready(function () {

    /**
     * Shows datepicker for date filter in list of properties
     */
    var currentTime = new Date();
    var maxDate = new Date(currentTime.getFullYear(),currentTime.getMonth() +1,0);
    $('#process_management_fee_date').datepicker( {
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0",
        // showButtonPanel: true,
        dateFormat: 'mm-yy',
        maxDate: maxDate,
        onClose: function(dateText, inst) {
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }
    }).datepicker("setDate", new Date());

    /**
     * Function to get list of all properties for run management fee
     */
    // getInitialData('', '', 'all_properties_list_run_management', '');
    function getInitialData(data_id, prev_data, data_type, extra_array){
        var regex = /[,\s]/g;
        $.ajax({
            type: 'post',
            url: '/ProcessManagementFee-Ajax',
            async: false,
            data: {
                class: "processManagementFee",
                action: "getDataByID",
                id: data_id,
                prev_data: prev_data,
                data_type : data_type,
                extra_array : extra_array,
                async: false
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {

                    if(data_type == 'all_properties_list_run_management') {
                        if (data.code == 201){
                            $('#showTable').hide();
                            $('#showTotal').hide();
                            toastr.error(data.message);
                        } else {
                            var propertyOption = "";
                            var propList = data.data.property_listing;
                            var parsed_process_management_fee = 0;
                            if (propList.length > 0) {
                                var html = '';
                                var total = 0;
                                for (var i = 0; i < propList.length; i++) {
                                    // console.log('propList', propList);
                                    var management_type = (propList[i].management_type != null && propList[i].management_type != '') ? propList[i].management_type : '-';
                                    var management_value = (propList[i].management_value != null && propList[i].management_value != '') ? propList[i].management_value : '0.00';
                                    var minimum_management_fee = (propList[i].minimum_management_fee != null && propList[i].minimum_management_fee != '') ? propList[i].minimum_management_fee : '-';
                                    var process_management_fee = (propList[i].process_management_fee != null && propList[i].process_management_fee != '') ? propList[i].process_management_fee : '0.00';

                                    parsed_process_management_fee = process_management_fee.replace(regex, '');
                                    parsed_process_management_fee = parseFloat(parsed_process_management_fee);
                                    html += '<tr class="table-row">\n' +
                                        '    <td class="property_checkbox">\n' +
                                        '        <input type="checkbox" property_id="' + propList[i].gp_prop_id + '">\n' +
                                        '    </td>\n' +
                                        '    <td>\n' +
                                        '        <span>' + propList[i].property_name + '</span>\n' +
                                        '    </td>\n' +
                                        '    <td>\n' +
                                        '        <span >' + management_type + '</span>\n' +
                                        '    </td>\n' +
                                        '    <td>\n' +
                                        '        <span >' + management_value + '</span>\n' +
                                        '    </td>\n' +
                                        '    <td>\n' +
                                        '        <span >' + minimum_management_fee + '</span>\n' +
                                        '    </td>\n' +
                                        '    <td>\n' +
                                        '        <span >0.00</span>\n' +
                                        '    </td>\n' +
                                        '    <td>\n' +
                                        '        <span >' + process_management_fee + '</span>\n' +
                                        '    </td>\n' +
                                        '</tr>';
                                    total = parseFloat(total) + parsed_process_management_fee;
                                }

                                $('#properties_listing').html(html);
                                total = total.toFixed(2);
                                total = numberWithCommas(total);

                                $('.total_management_fee_amount').text(total);
                            } else {
                                var html = '<tr class="norecord"><td align="center" colspan="7">No records found</td></tr>'
                                $('#properties_listing').html(html);
                            }
                            $('#showTable').show();
                            $('#showTotal').show();
                        }
                    } else if (data_type == 'all_PMC_vendors'){
                        var vendorOption = "";
                        var vendor_list = data.data.vendor_list;

                        var vendorOption = "<option value=''>Select</option>";
                        if (vendor_list.length > 0) {
                            $.each(vendor_list, function (key, value) {
                                vendorOption += "<option value='" + value.id + "'>" + value.name +"</option>";
                            });
                        }
                        $('#vendor_id').html(vendorOption);
                    } else if (data_type == 'get_vendor_address'){
                        var vendor_data = data.data.vendor_data;
                        $('.vendor-name').text(vendor_data.name);
                        $('.address1').text(vendor_data.address1);
                        $('.address2').text(vendor_data.address2);
                        $('.address3').text(vendor_data.address3);
                        $('.address4').text(vendor_data.address4);
                        $('.city-state').text(vendor_data.city+', '+vendor_data.state+' '+vendor_data.country+' '+vendor_data.zipcode);

                        $('.address1').val(vendor_data.address1);
                        $('.address2').val(vendor_data.address2);
                        $('.address3').val(vendor_data.address3);
                        $('.address4').val(vendor_data.address4);
                        $('.city').val(vendor_data.city);
                        $('.state').val(vendor_data.state);
                        $('.country').val(vendor_data.country);
                        $('.zipcode').val(vendor_data.zipcode);
                        $('#user_id').val(vendor_data.id);
                    } else if (data_type == 'all_bank_accounts'){
                        var data_array = data.data;
                        var html = '';
                        $.each(data_array, function (key, val) {
                             html += '<tr>' +
                                '<td>'+val.bank_name+'</td>' +
                                '<td>'+default_currency_symbol+val.property_amount+'</td>' +
                                '<td></td>' +
                                '</tr> '
                        });

                        $('#bank_with_property_listing').html(html);

                    }
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /**
     * To show properties in  the run management table
     */
    $(document).on('click', '#run_management_fee_btn', function(){
        var process_management_fee_date = $('#process_management_fee_date').val();
        getInitialData('', process_management_fee_date, 'all_properties_list_run_management', '');
        $('#process_amount').val('');

    });

    /**
     * To refresh the unsaved changes in the run management table
     */
    $(document).on('click', '#refresh_btn', function(){
        $('#select_all_properties_checkbox').prop('checked', false);
        var process_management_fee_date = $('#process_management_fee_date').val();
        getInitialData('', process_management_fee_date, 'all_properties_list_run_management', '');
        $('#process_amount').val('');
    });

    /**
     * To change Process Management Fee value on checkbox checked
     */
    $(document).on('click', '#list_run_management_table .property_checkbox input[type=checkbox]', function(){
        var inputs = $('#list_run_management_table .property_checkbox input[type=checkbox]:checked');
        var process_amount = $('#process_amount').val();
        process_amount = (process_amount != '') ? process_amount : '0.00';
        $(this).parent().siblings(":last").find('span').text(process_amount);
        var regex = /[,\s]/g;
        // process_amount = process_amount.replace(regex, '');
        // var length = inputs.length;
        console.log('bbb>>>', process_amount);

        var total_fee = 0;
        setTimeout(function () {
            $('#list_run_management_table #properties_listing tr').each(function(){
                var fee = $(this).find('td').eq(6).find("span").text();
                console.log('fee>>>', fee);

                fee = fee.replace(regex, '');
                fee = parseFloat(fee);
                total_fee = total_fee +fee;
                console.log('bbb>>>', total_fee);
            });
        }, 100);

    });

    /**
     * Check/Uncheck all properties for update
     */
    $(document).on("change","#select_all_properties_checkbox",function(e){
        console.log('111');
        var regex = /[,\s]/g;
        var inputs = $('#list_run_management_table .property_checkbox input[type=checkbox]');
        var all_checked_inputs = $('#list_run_management_table .property_checkbox input[type=checkbox]:checked');

        var process_amount = $('#process_amount').val();
        process_amount = (process_amount != '') ? process_amount : '0.00';
        if(e.originalEvent === undefined) {

            var allChecked = true;
            inputs.each(function(){
                allChecked = allChecked && this.checked;
            });
            this.checked = allChecked;
            var total_fee = 0;
            setTimeout(function () {
                $('#list_run_management_table #properties_listing tr').each(function(){
                    var fee = $(this).find('td').eq(6).find("span").text();
                    fee = fee.replace(regex, '');
                    fee = parseFloat(fee);
                    total_fee = total_fee +fee;
                });
                total_fee = total_fee.toFixed(2);
                total_fee = numberWithCommas(total_fee);

                $('.total_management_fee_amount').text(total_fee);
            }, 100);
        } else {
            $('#list_run_management_table tbody tr td:last-child span').text(process_amount);
            var total_fee = 0;
            setTimeout(function () {
                $('#list_run_management_table #properties_listing tr').each(function(){
                    var fee = $(this).find('td').eq(6).find("span").text();
                    fee = fee.replace(regex, '');
                    fee = parseFloat(fee);
                    total_fee = total_fee +fee;
                });
                total_fee = total_fee.toFixed(2);
                total_fee = numberWithCommas(total_fee);

                $('.total_management_fee_amount').text(total_fee);
            }, 100);
            inputs.prop('checked', this.checked);
        }
    });

    /**
     * Trigger Check/Uncheck all properties for update
     */
    $(document).on('change', '#list_run_management_table .property_checkbox input[type=checkbox]', function(){
        $('#select_all_properties_checkbox').trigger('change');
    });

    /**
     * On blur process amount change the value of amount in the table
     */
    $(document).on('blur', '#process_amount', function(){
        var inputs = $('#list_run_management_table .property_checkbox input[type=checkbox]:checked');
        var process_amount = $('#process_amount').val();
        var regex = /[,\s]/g;
        process_amount = (process_amount != '') ? process_amount : '0.00';

        $(inputs).each(function() {
            $(this).parent().siblings(":last").find('span').text(process_amount);
        });

        var total_fee = 0;
        setTimeout(function () {
            $('#list_run_management_table #properties_listing tr').each(function(){
                var fee = $(this).find('td').eq(6).find("span").text();
                fee = fee.replace(regex, '');
                fee = parseFloat(fee);
                total_fee = total_fee +fee;
            });
            total_fee = total_fee.toFixed(2);
            total_fee = numberWithCommas(total_fee);

            $('.total_management_fee_amount').text(total_fee);
        }, 100);

        if ($('#process_amount').val() == ''){
            $('#process_amount').val('0.00');
        }
    });

    /**
     * On click save button, save the run management fee for property
     */
    $(document).on('click', '#save-run-management-btn', function(){
        saveUpdateFee();
    });

    function saveUpdateFee(){
        var dataArr = [];
        $('#list_run_management_table #properties_listing tr').each(function(){
            var property_id = $(this).eq(0).find('td').find("input").attr("property_id");
            var income = $(this).find('td').eq(5).find("span").text();
            var fee = $(this).find('td').eq(6).find("span").text();
            dataArr.push({"property_id": property_id, "income": income, "fee": fee});
        });
        var process_management_fee_date = $('#process_management_fee_date').val();

        $.ajax({
            type: 'post',
            url: '/ProcessManagementFee-Ajax',
            data: {
                class: "processManagementFee",
                action: "saveRunManagementFee",
                data: dataArr,
                process_management_fee_date: process_management_fee_date
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    $('#properties_listing').html('');
                    var process_management_fee_date = $('#process_management_fee_date').val();
                    $('#select_all_properties_checkbox').prop('checked', false);

                    getInitialData('', process_management_fee_date, 'all_properties_list_run_management', '');
                    toastr.success(data.message);
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /**
     * Pay Process Management Fee button show modal for payment
     */
    $(document).on('click', '#payProcessManagementFeeBtnId', function(){
        var propertyArr = [];
        var propertyAmountArr = [];
        $('#list_run_management_table #properties_listing tr').each(function(){
            var property_id = $(this).find('td').eq(0).find('input').attr('property_id');
            var property_amount = $(this).find('td').eq(6).find('span').text();
            propertyArr.push(property_id);
            propertyAmountArr.push(property_amount);
        });
        getInitialData('', '', 'all_PMC_vendors', '');
        getInitialData('', propertyAmountArr, 'all_bank_accounts', propertyArr);
        var amount = $('#total_management_fee_amount').text();
        $('#spnTotalAmtPay').text(amount);
        $('#payProcessManagementFeeModal').modal({backdrop: 'static',keyboard: true});
        $('#payProcessManagementFeeModal').modal('show');
    });

    /**
     * On change PMC vendor show address
     */
    $(document).on('change', '#vendor_id', function(){
        var vendor_id = $('#vendor_id').val();
        if (vendor_id != ''){
            showVendorAddress(vendor_id);
            checkForReceivePayment(vendor_id, '');
        } else {
            $(".address-textarea span").text('');
            $(".address-textarea span.vendor-name").text('Address');
        }
    });

    /**
     * Check payment mode is set for vendor or not
     */
    function checkForReceivePayment(vendor_id, dataArr) {
        $.ajax({
            type: 'post',
            url: '/ProcessManagementFee-Ajax',
            data: {
                class: 'processManagementFee',
                action: "checkForReceivePayment",
                "user_id": vendor_id,
                "data_array": dataArr
            },
            success: function (response) {
                var res = JSON.parse(response);
                if (res.status == 'false') {
                    toastr.error(res.message);
                }
            }
        });
    }

    function showVendorAddress(vendor_id){
        var amount = $('#total_management_fee_amount').text();
        if (vendor_id != '') {
            getInitialData(vendor_id, '', 'get_vendor_address', '');
            $('#ancChangePMCAddress').css('display', 'block');
        }
        $('#spnTotalAmtPay').text(amount);
        $('#payProcessManagementFeeModal').modal('show');
    }

    /**
     * Show modal to update PMC vendor address
     */
    $(document).on('click', '#ancChangePMCAddress', function(){
        var vendor_id = $('#vendor_id').val();
        getInitialData(vendor_id, '', 'get_vendor_address', '');
        $('#editVendorAddressModal').modal({backdrop: 'static',keyboard: true});
        $('#payProcessManagementFeeModal').modal('hide');
        $('#editVendorAddressModal').modal('show');
    });

    /**
     * Cancel/Cross update PMC vendor address modal
     */
    $(document).on('click', '#cancelEditPMCAddressForm, #crossEditPMCAddressForm', function(){
        var vendor_id = $('#user_id').val();
        getInitialData(vendor_id, '', 'get_vendor_address', '');
        $('#payProcessManagementFeeModal').modal({backdrop: 'static',keyboard: true});
        $('#editVendorAddressModal').modal('hide');
        $('#payProcessManagementFeeModal').modal('show');
    });


    /**
     * Reset update PMC vendor address modal
     */
    $(document).on('click', '#resetEditPMCAddressForm', function(){
        var vendor_id = $('#user_id').val();
        bootbox.confirm("Do you want to reset this form?", function (result) {
            if (result == true) {
                getInitialData(vendor_id, '', 'get_vendor_address', '');
            }
        });
    });

    /**
     * Update PMC vendor address on update btn click
     */
    $(document).on('click', '#updatePMCAddressBtn', function(event){
        event.preventDefault();
        var form = $('#updatePMCAddressFormId input').serializeArray();
        var user_id = $('#user_id').val();
        $.ajax({
            type: 'post',
            url: '/ProcessManagementFee-Ajax',
            data: {
                class: 'processManagementFee',
                action: 'updatePMCAddress',
                form: form
            },
            success: function (response) {
                var response = JSON.parse(response);
                if(response.code == 200){
                    showVendorAddress(user_id);
                    $('#editVendorAddressModal').modal('hide');
                    $('#payProcessManagementFeeModal').modal('show');
                    toastr.success(response.message);
                } else if(response.code == 200) {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    /**
     * Get city, state & country by zipcode
     */
    $(document).on('blur', '.zipcode ', function(){
        getZipCode('#updatePMCAddressFormId', $(this).val(), '.city', '.state', '.country', null, null);
    });

    /**
     * Pay Button to make final payment for PMC
     */
    $(document).on('click', '#pay_run_management_fee_btn', function(){
        var vendor_id = $('#vendor_id').val();
        // var payment_type = $('input[name="payment_type"]').val();
        var payment_type = $("input[name='payment_type']:checked").val();
        console.log('payment_type', payment_type);
        if (vendor_id == ''){
            $('#vendor_id_error').text('* This field is required.');
            return;
        } else {
            $('#vendor_id_error').text('');
            bootbox.confirm({
                message: "Click OK if wants to pay.",
                buttons: {confirm: {label: 'OK'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        confirmPaymentOnSubmit(payment_type, vendor_id);
                    }
                }
            });
        }

    });

    /**
     * Confirm final payment on OK button click after confirmation
     */
    function confirmPaymentOnSubmit(payment_type, vendor_id) {
        var dataArr = [];
        $('#list_run_management_table #properties_listing tr').each(function(){
            var property_id = $(this).eq(0).find('td').find("input").attr("property_id");
            var income = $(this).find('td').eq(5).find("span").text();
            var fee = $(this).find('td').eq(6).find("span").text();
            dataArr.push({"property_id": property_id, "income": income, "fee": fee});
        });
        var process_management_fee_date = $('#process_management_fee_date').val();
        var total_amount_to_pay = $('#spnTotalAmtPay').text();

        $.ajax({
            type: 'post',
            url: '/ProcessManagementFee-Ajax',
            data: {
                class: 'processManagementFee',
                action: "createPaymentOnSubmit",
                payment_type: payment_type,
                data: dataArr,
                vendor_id: vendor_id,
                total_amount_to_pay: total_amount_to_pay,
                process_management_fee_date: process_management_fee_date
            },
            success: function (response) {
                var res = JSON.parse(response);
                if (res.status == 'false') {
                    toastr.error(res.message);
                    // if (payment_type == 'ACH') {
                    //     // achTypeDiv(transaction_type);
                    // }
                    // if (payment_type == 'Check'){
                    //     $("#owner_draw_pay_btn").attr("disabled", true);
                    // }
                } else {
                    window.location.href = '/Accounting/RunManagementFee';
                }
            }
        });
    }

});