$(document).ready(function() {
    Recurring_Bill();
    getProperties();
    getUserNameByType(3);
    $('.address').prop('readonly',true);

   var editid = $('#check_id').val();

    if(editid !='' && typeof editid !='undefined'){
            editRecurringCheck(editid);
    }
    $(".calander").datepicker({
        dateFormat: date,
        changeMonth: true,
        changeYear: true
    }).datepicker("setDate", new Date());


    $('.number_only').removeClass('capital');
});

function Recurring_Bill() {
    var sortColumn = 'accounting_banking.updated_at';
    var table = 'accounting_banking';
    var columns = ['Payor Name','Payorname','OtherName', 'Created Date', 'Original Amount('+default_symbol+')', 'Type', 'Last Generated Recurring Check','Action'];
    var select_column = ['EDIT', 'DELETE','VIEW DETAILS','CANCEL RECURRENCE'];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [];
    var extra_columns = [];
    var joins = [{table: 'accounting_banking', column: 'user_id', primary: 'id', on_table: 'users'}
    ];

    var columns_options = [
        {name: 'Payor Name', index: 'name', width: 100, searchoptions: {sopt: conditions}, table: 'users',formatter:PayorNameFormatter},
        {name: 'Payorname', index: 'name', hidden:true,width: 100, searchoptions: {sopt: conditions}, table: 'users'},
        {name: 'OtherName', index: 'other_name',hidden:true, width: 100, searchoptions: {sopt: conditions}, table: table},
        {
            name: 'Created Date',
            index: 'created_at',
            width: 100,
            searchoptions: {sopt: conditions},
            table: table
        },
        {name: 'OriginalAmount', index: 'amount', width: 100, searchoptions: {sopt: conditions}, table: table,formatter:OriginalAmtFormatter},
        {name: 'Type', index: 'userType', width: 100, searchoptions: {sopt: conditions}, table: table,formatter:UserTypeFormatter},
        {name: 'Last Generated Recurring Check', index: 'created_at', width: 100, searchoptions: {sopt: conditions}, table: table},
        {
            name: 'Action',
            index: 'select',
            title: false,
            width: 80,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            edittype: 'select',
            search: false,
            table: table,
            formatter: actionFormatter
        },
    ];
    var ignore_array = [];
    jQuery("#RecurringChecksDataTable").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            ignore: ignore_array,
            joins: joins,
            extra_where: extra_where,
            extra_columns: extra_columns,
            deleted_at: 'true'
        },
        viewrecords: true,
        sortname: sortColumn,
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Recurring checks",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 200, drag: true, resize: false} // search options
    );
}

function OriginalAmtFormatter(cellvalue, options, rowObject){
    if(rowObject!==undefined)
    {
        var symbol = default_symbol;
        if(rowObject.OriginalAmount!='')
        {
            var amt = changeToFloat(rowObject.OriginalAmount.toString());
            return symbol+''+amt;
        }
    }
}

function UserTypeFormatter(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {
        var data = '';
        var user = "Other";
        if (rowObject.Type != '') {
            if (rowObject.Type == 2) {
                user = 'Tenant';

            } else if (rowObject.Type == 4) {
                user = 'Owner';
            } else if (rowObject.Type == 3) {
                user = 'Vendor';
            }
            data += '<span class="invoice_to_name">' + user + '</span>';
        }
        return data;
    }
}

function PayorNameFormatter(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {
         console.log(rowObject);
         var name;
         if(rowObject.OtherName != '' && typeof rowObject.OtherName != 'undefined'){
              name = '<a class="GoOnCheckView" data-id="'+rowObject.id+'" style="color:#05A0E4 ! important;font-weight: bold"><u><strong>'+rowObject.OtherName+'</strong></u></a>';
         }
         else{
             name = '<a class="GoOnCheckView" data-id="'+rowObject.id+'" style="color:#05A0E4 ! important;font-weight: bold"><u><strong>'+rowObject.Payorname+'</strong></u></a>';
         }
         return name;
        }
    }

$(document).on('click','.GoOnCheckView',function () {
    var id =   $(this).attr('data-id');
    $('#checkTable').hide();
    $('#viewlisting').show();
    ViewInfo(id);

    //  Accounting/ViewRecurringInvoice?id

});
function actionFormatter(cellValue, options, rowObject) {

    if (rowObject !== undefined) {
        //   console.log('rowObject', rowObject);

        var editable = $(cellValue).attr('editable');
        var is_default = rowObject;
        //   console.log(rowObject);
        var select = '';

        select = ['EDIT', 'DELETE','VIEW DETAILS','CANCEL RECURRENCE'];

        var data = '';

        if (select != '') {

            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function(key, val) {
                /*  if (editable == '0' && (val == 'delete' || val == 'Delete')) {
                      return true;
                  } */
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';

            });

            data += '</select>';

        }
        //   console.log( 'data', data);
        return data;

    }

}


$(document).on('change', '.select_options', function() {
    setTimeout(function() {
        $(".select_options").val("default");
    }, 200);

    var opt = $(this).val();

    var id = $(this).attr('data_id');

    var row_num = $(this).parent().parent().index();

    if (opt == 'View Details' || opt == 'VIEW DETAILS') {

        $('#checkTable').hide();
        $('#viewlisting').show();
        ViewInfo(id);

    } else if (opt == 'Cancel Recurrence' || opt == 'CANCEL RECURRENCE') {
        $.ajax({
            type: 'post',
            url: '/Accounting/recurring-check-ajax',
            data: {
                class: 'RecurringChecks',
                action: 'delete',
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                } else {
                    toastr.error(response.message);
                }
                $('#RecurringChecksDataTable').trigger('reloadGrid');
            }
        });
        $('#RecurringChecksDataTable').trigger('reloadGrid');

    } else if (opt == 'Edit' || opt == 'EDIT') {
        opt = opt.toLowerCase();
        window.location.href = base_url+"/Accounting/EditRecurringCheck?id="+id;

    } else if (opt == 'Delete' || opt == 'DELETE') {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/Accounting/recurring-check-ajax',
                        data: {
                            class: 'RecurringChecks',
                            action: 'delete',
                            id: id
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                            $('#RecurringChecksDataTable').trigger('reloadGrid');
                        }
                    });
                }
                $('#RecurringChecksDataTable').trigger('reloadGrid');
            }
        });

    } else {

    }

});
function ViewInfo(id){
    $.ajax ({
        type: 'post',
        url: '/Accounting/recurring-check-ajax',
        data: {
            class: "RecurringChecks",
            action: "view",
            id: id,
        },
        success: function(response) {
            //   console.log(response);
            var data = $.parseJSON(response);

            $('.propertnme').html(data.data['property_name']);
            $('.bank').html(data.data['bank_name']);
            var amt = data.data['amount'];
            $('.bankbal').html(default_currency_symbol+changeToFloat(amt,2));
            $('.date').html(data.data['created_at']);
            var frequency = data.data['frequency'];
            var freval;
            if(frequency == 1){
                freval = 'Weekly';
            } else if(frequency == 2){
                freval='Bi-Weekly';
            } else if(frequency == 3){
                freval='Monthly';
            } else if(frequency == 4){
                freval='Annually';
            } else if(frequency == 5){
                freval='Quartely';
            } else if(frequency == 6){
                freval='Semi-Annually';
            } else if(frequency == 7){
                freval='Daily';
            } else if(frequency == 8){
                freval='Bi-Monthly';
            }
            $('.frequency').html(freval);
            $('.duration').html(data.data['duration']);
            $('.chkamt').html(default_currency_symbol+changeToFloat(data.data['amount'],2));
            var amtinWords =  inWords(data.data['amount']);
            $('.amttoward').html(amtinWords);
            $('.name').html(data.data['name']);
            $('.memo').html(data.data['memo']);
            $('.address').html(data.data['other_address']);
            $('.viewEdit').attr('data-editid',data.data['id']);
        },
        error: function(data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function(key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
    return false;
}

$("#createCheckForm").validate({
    rules: {
        property_id: {
            required:true
        },
        building_id: {
            required: true
        },
        unit_id: {
            required:true
        },
        bank_id: {
            required:true
        },
        Date: {
            required:true
        },
        payOrderOff: {
            required:true
        },
        afn: {
            required:true
        },
        duration: {
            required:true
        },
        address: {
            required:true
        },
    }
});
/*
$.validator.addMethod("duration", function(value, element, arg){
    if(value == "") return true;
    var st = value.replace("-", "");
    if(isNaN(st.replace("-", ""))){
        return false;
    } else {
        return true;
    }
}, "Only number are allowed!");*/

$('#createCheckForm').submit(function () {
    if($("#createCheckForm").valid()){
        var form = $('#createCheckForm')[0];
        var formData = new FormData(form);
        formData.append('action','add');
        formData.append('class','RecurringChecks');
        $.ajax({
            type: 'post',
            url:'/Accounting/recurring-check-ajax',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == "success"){
                    toastr.success(response.message);
                    localStorage.setItem("Message", response.message);
                    localStorage.setItem("rowcolor",true)
                    window.location.href = base_url+'/Accounting/RecurringChecks';
                }else{
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
        return false;
    }
    return false;
});

var a = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
var b = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];

function inWords (num) {
    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';
    return str;
}
/*
document.getElementById('number').onkeyup = function () {
    document.getElementById('amttoward').innerHTML = inWords(document.getElementById('number').value);
};*/




$(document).on("change","#property",function(){

    var property_id = $(this).val();
    if(property_id=="")
    {
        var property_id = 0;
    }
    if(property_id!=0)
    {
        $.ajax({
            url:'/accountingReceivable',
            type: 'POST',
            data: {
                "action": 'getBuildings',
                "class": 'accounting',
                'property_id':property_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                $("#building").html(response.building);
            }
        });
    }
});

$(document).on("change","#building",function(){

    var building_id = $(this).val();
    if(building_id!='0')
    {
        $.ajax({
            url:'/accountingReceivable',
            type: 'POST',
            data: {
                "action": 'getUnits',
                "class": 'accounting',
                'building_id':building_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                $("#unit").html(response.unit);
            }
        });
    }
});


$(document).on("change","#property",function(){
    var property_id = $(this).val();
    $.ajax({
        url:'/accountingReceivable',
        type: 'POST',
        data: {

            "action": 'getBankListByProperty',
            "class": 'accounting',
            'property_id':property_id

        },
        success: function (response) {

            $("#selectBank").html(response);
            $("input[name=userType][value='3']").prop('checked', true);
            $("input[name=userType][value='3']").trigger( "click" );

        }
    });

});


function getProperties()
{
    $.ajax({
        url:'/accountingReceivable',
        type: 'POST',
        data: {
            "action": 'getProperties',
            "class": 'accounting'
        },
        success: function (response) {

            var response = JSON.parse(response);
            $("#property").html(response.property);



        }
    });
}
function getUserNameByType(type,property_id)
{
    $.ajax({
        url:'/accountingReceivable',
        type: 'POST',
        data: {

            "action": 'getUserNameByType',
            "class": 'accounting',
            'type':type,
            'property_id':property_id

        },
        success: function (response) {
            $(".payOrderOff").html(response);


        }
    });

}

$(document).on("click",".selectUser",function(){
    $('#EditVendorAddress').hide();
    var val = $(this).val();
    var property_id = $("#property").val();
    if(val==0)
    {
        $(".orderOff").html("<input type='text' name='other_name' class='payOrderOff form-control' >");
        $(".memo").attr("readonly", false);
        $('.address').prop('readonly',false);
    }
    else
    {


        $(".orderOff").html('<select name="payOrderOff" class="form-control payOrderOff"><option>Select</option></select>');
        if(val==3 || val==2 && (property_id!=0 || property_id!=''))
        {
            getUserNameByType(val,property_id);
            $('.address').prop('readonly',true);
        }
        else
        {
            getUserNameByType(val,0);
            $('.address').prop('readonly',true);
        }

        $(".memo").attr("readonly", true);
    }


});

//$(document).on('change','.payOrderOff',function () {
 /*  var id =  $(this).val();
$('#EditVendorAddress').attr('data-id',id);
    $('#EditVendorAddress').show();*/
//});
$(document).on('click','#EditVendorAddress',function () {
 var id = $(this).attr('data-id');
    $('#updateVendorAddress').modal('show');

   var type_id = $('.selectUser:checked').val();
    if(type_id == 2){
        var property = $('#property').val();
        GetTenantAddress(property);
    } else {
        fetchUserAddress(id);
     }
});

function GetTenantAddress(id){
    $.ajax({
        type: 'post',
        url: '/Accounting/recurring-check-ajax',
        data: {
            class: 'RecurringChecks',
            action: 'getTenantAddress',
            property_id: id
        },
        success: function (response) {
         //   console.log(response);
            var res = JSON.parse(response);
            $('#update_vendor_address_form #vendorAddress1').val(res.data.address1);
            $('#update_vendor_address_form #vendorAddress2').val(res.data.address2);
            $('#update_vendor_address_form #vendorAddress3').val(res.data.address3);
            $('#update_vendor_address_form #vendorAddress4').val(res.data.address4);
            $('#update_vendor_address_form #vendor_zip_code').val(res.data.zipcode);
            $('#update_vendor_address_form #vendor_state').val(res.data.state);
            $('#update_vendor_address_form #vendor_city').val(res.data.city);
        }
    });
    return false;
}


$("#update_vendor_address_form").validate({
    rules: {
        address1: {
            required:true
        },
        address2: {
            required:true
        },
        zipcode: {
            required:true
        }
    }
});
$(document).on('submit','#update_vendor_address_form',function(e){
    e.preventDefault();
    if($('#update_vendor_address_form').valid()){
        var type_id = $('.selectUser:checked').val();
        if(type_id == 2) {
        UpdateTenantAddress();
        } else {
            $.ajax({
                type: 'post',
                url: '/global-user-module-ajax',
                data: {
                    class: 'GlobalUserModule',
                    action: 'updateUserAddress',
                    data: $('#update_vendor_address_form').serializeArray()
                },
                success: function (response) {
                    var res = JSON.parse(response);
                    if (res.code == 200) {
                        $('#vendorAddress').val(res.address);
                        $('#updateVendorAddress').modal('hide');
                        toastr.success(res.message);
                    }
                },
            });
        }
    }
});

function  UpdateTenantAddress() {
    $.ajax({
        type: 'post',
        url: '/global-user-module-ajax',
        data: {
            class: 'GlobalUserModule',
            action: 'updateUserAddress',
            data: $('#update_vendor_address_form').serializeArray()
        },
        success: function (response) {
            var res = JSON.parse(response);
            if (res.code == 200) {
                $('#vendorAddress').val(res.address);
                $('#updateVendorAddress').modal('hide');
                toastr.success(res.message);
            }
        },
    });
}
function fetchUserAddress(id) {
    $.ajax({
        type: 'post',
        url: '/global-user-module-ajax',
        data: {
            class: 'GlobalUserModule',
            action: 'getUser',
            id:id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200){
                $('#vendorAddress1').val(res.data.address1);
                $('#vendorAddress2').val(res.data.address2);
                $('#vendorAddress3').val(res.data.address3);
                $('#vendor_zip_code').val(res.data.zipcode);
                $('#vendor_country').val(res.data.country);
                $('#vendor_state').val(res.data.state);
                $('#vendor_city').val(res.data.city);
                $('#vendor_address_id').val(res.data.id);
            }
        },
    });
}

$(document).on("change",".payOrderOff",function(){
    var user_id = $(this).val();
    var property_id = $("#property").val();
    var type = $("input[name='userType']:checked").val();
    $('#EditVendorAddress').hide();
    $.ajax({
        url:'/accountingReceivable',
        type: 'POST',
        data: {
            "action": 'getAddress',
            "class": 'accounting',
            'property_id':property_id,
            'type':type,
            'user_id':user_id,
        },
        success: function (response) {
        //    console.log(response);
            if (type == 2 && property_id =='') {
            var response = JSON.parse(response);
                if(response.status == 'false'){
                    toastr.warning('Please select property');
                }
            }
          // else {
                $(".address").html(response);
                $('#EditVendorAddress').attr('data-id', user_id);
                var res = response.trim();
                if (res != '' && typeof res != undefined) {
                    $('#EditVendorAddress').show();
            //    }
            }
        }
    });
});

function editRecurringCheck(id) {
    $.ajax ({
        type: 'post',
        url: '/Accounting/recurring-check-ajax',
        data: {
            class: "RecurringChecks",
            action: "view",
            id: id,
        },
        success: function (response) {
            var data = $.parseJSON(response);
             var checkdata = data.data;
            $('#property option[value="'+checkdata['property_id']+'"]').prop('selected',true);
            $('#building_id option[value="'+checkdata['building_id']+'"]').prop('selected',true);
            $('#property option[value="'+checkdata['property_id']+'"]').prop('selected',true);
            $('#property').val(checkdata['property_id']);
            $('.endingbalance ').val('dfd');
            $('#property').val(checkdata.property_id);
            $('#memo').val(checkdata.memo);

        }
    });
    return false;
}

$(document).on("click",'.viewEdit', function () {
    var id = $(this).attr('data-editid');
    window.location.href = base_url+"/Accounting/EditRecurringCheck?id="+id;
});

$(document).on('click','#cancel_frm',function () {
    bootbox.confirm({
        message: "Do you want to cancel this action ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                window.location.href = base_url + '/Accounting/RecurringChecks';
            }
        }
    });
});

$(document).on('click','.cancel_address_update',function () {
$('#updateVendorAddress').modal('hide');
});
$(document).on('click','#clear',function () {
    bootbox.confirm({
        message: "Do you want to clear this form ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $('#createCheckForm')[0].reset();
            }
        }
    });
});