$(document).ready(function() {
    /**
     * Get Parameters by id
     * @param status
     */
    function getParameterByName(name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

    var owner_id = getParameterByName('id');

    $('#start_date').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat});
    $('#end_date').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat});

    $(document).on("select", "#start_date", function () {
        var start_date = $(this).val();
        var end_date = $('#end_date').val();
        if (end_date != '' && end_date < start_date){
            $('#end_date').datepicker({ dateFormat: jsDateFomat}).datepicker("setDate", start_date);
        }
        $('#end_date').datepicker({dateFormat: jsDateFomat, minDate: start_date});
    });

    $(document).on('change','.common_search_class',function(){
        var grid = $("#owner_draw_listing"),f = [];
        // var value = $(this).attr('data_id');
        var owner_name_id = $('#owner_name_id').val()== ''?'all':$('#owner_name_id').val();
        var property_id   = $('#property_id').val()== ''?'all':$('#property_id').val();
        var start_date    = $('#start_date').val() == ''?'all':$('#start_date').val();
        var end_date      = $('#end_date').val() == ''?'all':$('#end_date').val();
        f.push(
            {field: "users.id", op: "eq", data: owner_name_id},
            {field: "general_property.id", op: "eq", data: property_id},
            {field: "owner_draw.created_at", op: "dateBetween", data: start_date, data2:end_date}
        );
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    });

    console.log('owner_id>>>>',owner_id);
    getDataByID('','', 'all_owner_listing');
    getDataByID('','', 'all_property_listing');
    function getDataByID(data_id, prev_id, data_type){
        $.ajax({
            type: 'post',
            url: '/OwnerDraw-Ajax',
            async: false,
            data: {
                class: "ownerDraw",
                action: "getDataByID",
                id: data_id,
                prev_id: prev_id,
                data_type : data_type
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    if(data_type == 'all_owner_listing') {
                        var ownerOption = "<option value=''>Select</option>";
                        if (data.data.owner_listing.length > 0) {
                            $.each(data.data.owner_listing, function (key, value) {
                                if (value.name != undefined && value.id != undefined) {
                                    ownerOption += "<option value='" + value.id + "'>" + value.name + "</option>";
                                }
                            });
                        }
                        $('#owner_name_id').html(ownerOption);
                    }
                    if(data_type == 'all_property_listing') {
                        var propertyOption = "";
                        if (data.data.property_listing.length > 0) {
                            propertyOption = "<option value=''>Select</option>";
                            $.each(data.data.property_listing, function (key, value) {
                                if (value.property_name != undefined && value.id != undefined) {
                                    propertyOption += "<option value='" + value.id + "'>" + value.property_name + "</option>";
                                }
                            });
                        }
                        $('#property_id').html(propertyOption);
                    }
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        // return returnElement;
    }





    listOwnerDrawJqGrid('', '');
    function listOwnerDrawJqGrid(selected_owner_id, deleted_at) {

        var sortOrder = 'desc';
        var sortColumn = 'owner_draw.created_at';

        var table = 'owner_draw';
        var select_column = '';
        var columns = ['Owner Name','Property Name','Account No.', 'transaction_type', 'account_number_id', 'created_at', /*'Account No.',*/ 'COA','Amount('+default_currency_symbol+')'];
        var joins = [
            {table: 'owner_draw', column: 'owner_id', primary: 'id', on_table: 'users'},
            {table: 'owner_draw', column: 'property_id', primary: 'id', on_table: 'general_property'},
            {table: 'owner_draw', column: 'chart_of_account_id', primary: 'id', on_table: 'company_chart_of_accounts'},
            {table: 'owner_draw', column: 'account_number_id', primary: 'id', on_table: 'property_bank_details'},
        ];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        if (selected_owner_id != '') {
            var extra_where = [
                {column: 'user_type', value: '4', condition: '=', table: 'users'},
                { column: 'owner_id', value: selected_owner_id, condition: '=', table: 'owner_draw'}
            ];
        } else {
            var extra_where = [{column: 'user_type', value: '4', condition: '=', table: 'users'}];
        }
        var columns_options = [
            {name: 'Owner Name', index: 'name', width: 60, align: "center", searchoptions: {sopt: conditions}, table: 'users', classes: 'cursor', formatter: ownerName },
            { name: 'Property Name', index: 'property_name', width: 60, align: "center", searchoptions: {sopt: conditions}, table: 'general_property', classes: 'pointer', formatter: propertyName},
            { name: 'Account No.', index: 'account_number_id', width: 60, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            /*hidden fields*/
            {name: 'transaction_type', index: 'transaction_type', hidden: true, alias: 'property_id', width: 60, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'account_number_id', index: 'bank_name', hidden: true, width: 130, align: "center", searchoptions: {sopt: conditions}, table: 'property_bank_details'},
            {name: 'created_at', index: 'created_at', hidden: true, width: 130, align: "center", searchoptions: {sopt: conditions}, table: table},
            /*hidden fields*/
            // {name: 'Account No.', index: 'bank_name', width: 130, align: "center", searchoptions: {sopt: conditions}, table: 'company_accounting_bank_account', formatter: accountNameNumber},
            {name: 'COA', index: 'account_name', width: 130, align: "center", searchoptions: {sopt: conditions}, table: 'company_chart_of_accounts'},
            {name: 'Amount', index: 'amount', width: 130, align: "center", searchoptions: {sopt: conditions}, table: table,  formatter: amount}
        ];
        var ignore_array = [];

        jQuery("#owner_draw_listing").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "owner_draw",
                select: select_column,
                columns_options: columns_options,
                // status: '',
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                deleted_at: true,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Owner Draw",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
            // {top:200,left:200,drag:true,resize:false}
        );
    }

    /**
     *  Function to format property name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyName(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }

    /**
     * Function to format owner name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function ownerName(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }

    function amount(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            return default_currency_symbol + cellValue;
        }
    }

    function accountNameNumber(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            var account_name = rowObject['Account No.'];
            var transation_type = rowObject['transation_type'];
            var bank_account_number = rowObject['bank_account_number'];
            if (transation_type == 'Check' || transation_type == 'Cash'){
                account_name = '';
            } else {
                account_name = account_name+' '+bank_account_number;
            }
            return  account_name;
        }
    }

});

