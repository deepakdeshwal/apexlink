$(document).ready(function(){
    getMultipaymentList();
    $(document).on('click','.clear_multipayment',function(){
        bootbox.confirm("Do you want to clear this form?", function (result) {
            if (result == true) {
                getMultipaymentList();
            }
        });
    });
});
function getMultipaymentList()
{
            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {
                    "action": 'getMultipaymentList',
                    "class": 'accounting'
                },
                success: function (response) {
                   $(".multipaymentList").html(response);
                  $(".disableAll").prop('disabled', true);
                }
            });
  }
  $(document).on("click",".payer_id",function(){
   var payer_id = $(this).attr('data-user_id');
   if(this.checked==true)
   {
     var currentOverPay = $(".overUnderPay_"+payer_id).val();
     $(".hiddenOverPay").val(currentOverPay);
     $(".checkedUserid").val(payer_id);
     $(".unchecked").prop("checked", false);
     $(this).prop("checked", true);
     $(".disableAll").prop('disabled', true);
     $(".enableDisable_"+payer_id).prop('disabled', false);
     $(".forNoAction").html('<input type="button" value="No Action" class="form-control disableAll enableDisable_8" disabled>');
     $(".actionbutton_"+payer_id).html("<input type='button' class='apply'  data-id='"+payer_id+"' value='Apply'>");
     $(".paymentStatus").val("notReadyForPay");
   }
   else
   {
      $(".checkedUserid").val(0);
      $(".enableDisable_"+payer_id).prop('disabled', true);
      $(".actionbutton_"+payer_id).html("<input type='button' class='apply'  data-id='"+payer_id+"' value='Apply'>");
      $(".paymentStatus").val('notReadyForPay');
   }
  });



$(document).on("click",".apply",function(){
 var id =  $(this).attr('data-id');
  bootbox.confirm("Are you sure you want to process this record for payment?", function (result) {
    if (result == true) {
    $(".actionbutton_"+id).html("<input type='button' class='Pending'  data-id='"+id+"' value='Pending'>");
    $(".paymentStatus").val('readyForPay');
    }
  });
 });


  $(document).on("click",".multipayButton",function(){
   var user_id = $(".checkedUserid").val();
   var paidAmount = $(".paidAmt_"+user_id).val().replace(/,/g, '');;
   var overPay = $(".overUnderPay_"+user_id).val().replace(/,/g, '');;
   var dueAmt = $(".balance_"+user_id).val().replace(/,/g, '');;
   var payment_type = $(".paymnt_type_"+user_id).val();
   var waiveAmt = $(".waive_of_on_total_"+user_id).val().replace(/,/g, '');;
   if(waiveAmt=="")
   {
    waiveAmt = 0;
   }
   if(overPay<0)
   {
      toastr.error("Currently you are paying less amount");
      return false;
   }
   var hiddenOverPay = $(".hiddenOverPay").val();
   var hiddenUnchecked =  $(".hiddenUncheckedAmt").val();

   if(paidAmount==0 && dueAmt==0 && waiveAmt==0)
   {
    toastr.error("Currently you have no charge to pay");
    return false;
   }

   if(paidAmount>=dueAmt && waiveAmt>0)
   {
    toastr.error("You can not waive, As your current amount is greater/equal to your balance amount");
    return false;
   }
   var paymentStatus = $(".paymentStatus").val();
   if(paymentStatus!="readyForPay")
   {
    toastr.warning("Please mark atleast on record as pending for processing.");
    return false;
   }
   var paymentType = $(".paymnt_type_"+user_id).val();
   var checkNumber = $(".check_"+user_id).val();
   var ref =         $(".ref_"+user_id).val();
            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {
                    "action": 'updateChargeByMultipay',
                    "class": 'accounting',
                    "paidAmount": paidAmount,
                    "payment_type": payment_type,
                    "payment_module": 'multipay',
                    "waiveAmt": waiveAmt,
                    "user_id": user_id,
                    'overPay':overPay,
                    'checkNumber':checkNumber,
                    'ref':ref
                    },
                success: function (response) {
                  var response = JSON.parse(response);
                  if(response.status=='true')
                  {
                    toastr.success(response.message);
                    setTimeout(function(){
                     location.reload();
                    },500)
                  }
                  else
                  {
                   toastr.error(response.message);
                  }
                }
            });
          });




  $(document).on("blur keyup",".paidAmt, .waive_of_on_total",function(){
  var user_id = $(".checkedUserid").val();
  var hiddenDueAmount = $('.balance_'+user_id).val().replace(/,/g, '');;
  var paidAmt = $(".paidAmt_"+user_id).val().replace(/,/g, '');;
  var hiddenOverPay = $(".hiddenOverPay").val().replace(/,/g, '');;
  var waiveAmt = $(".waive_of_on_total_"+user_id).val().replace(/,/g, '');;
  if(hiddenDueAmount=="")
  {
   hiddenDueAmount = 0;
  }
   if(paidAmt=="")
  {
   paidAmt = 0;
  }
   if(hiddenOverPay=="")
  {
   hiddenOverPay = 0;
  }
   if(waiveAmt=="")
  {
   waiveAmt = 0;
  }
  var overPayUnderPay = parseFloat(hiddenOverPay) + parseFloat(paidAmt) + parseFloat(waiveAmt) -parseFloat(hiddenDueAmount);
  var overPayUnderPay = overPayUnderPay.toString();
  var overPayUnderPay = changeToFloat(overPayUnderPay); 
  $(".overUnderPay_"+user_id).val(overPayUnderPay);
});
$(document).on('keypress keyup','.numberonly',function(){
     $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
});
$(document).on("change",".paymnt_type",function(){
  var type = $(this).val();
  var user_id = $(".checkedUserid").val();
  if(type=='card')
  {
                $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {
                    "action": 'checkForCard',
                    "class": 'accounting',
                    "type":type,
                    "user_id":user_id
                },
                success: function (response) {
                   var info = JSON.parse(response);
                   if(info.status=='false')
                   {
                   toastr.error(info.message);
                    $(".paymnt_type").val('check');
                   }
                }
            });
  }
  else if(type=='ACH')
  {
                $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {
                    "action": 'checkForACH',
                    "class": 'accounting',
                    "type":type,
                    "user_id":user_id
                },
                success: function (response) {
                   var info = JSON.parse(response);
                   if(info.status=='false')
                   {
                    toastr.error(info.message);
                     $(".paymnt_type").val('check');
                   }
                }
            });
  }
});



$(document).ready(function () {
   $("#search_input").click(function () {
       var b=$(".search_input").val();
       getsearchList(b);
   });
    function getsearchList(a)
    {
        $.ajax({
            url:'/accountingReceivable',
            type: 'POST',
            data: {
                "action": 'getMultipaymentList',
                "class": 'accounting',
                'a':a
            },
            success: function (response) {
                $(".multipaymentList").html(response);
                $(".disableAll").prop('disabled', true);
            }
        });
    }
});
