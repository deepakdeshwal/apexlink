$(document).ready(function() {

    var base_url = window.location.origin;
    var status = localStorage.getItem("active_inactive_status");
    if (status !== undefined) {
        if ($("#AccountType-table")[0].grid) {
            $('#AccountType-table').jqGrid('GridUnload');
        } /*intializing jqGrid*/
        if (status == 'all') {
            jqGrid('All');
        } else {
            jqGrid(status);
        }
        $('#jqGridStatus option[value=' + status + ']').attr("selected", "selected");
    } else {
        if ($("#AccountType-table")[0].grid) {
            $('#AccountType-table').jqGrid('GridUnload');
        }
        jqGrid('All');
    }

    /** Show add new account type div on add new button click */
    $(document).on('click', '#addAccountTypeButton', function() {
        headerDiv.innerText = "Add Account Type";
        var validator = $("#add_account_type_form").validate();
        validator.resetForm();
        $('#account_type_name').val('');
        $('#account_type_nameErr').text('');
        $('#range_from').val('');
        $('#range_fromErr').text('');
        $('#range_to').val('');
        $('#range_toErr').text('');
        $("#account_type_id").val('');
        $('#is_default').prop('checked', false);
        $('#saveBtnId').val('Save');
        $('#add_account_type_div').show(500);
    });

    /** Show import excel div on import excel button click */
    $(document).on('click', '#importAccountTypeButton', function() {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $('#import_account_type_div').show(500);
    });

    /** Hide add new account type div on cancel button click */
    $(document).on("click", "#add_account_type_cancel_btn", function(e) {
        e.preventDefault();
        bootbox.confirm("Do you want to cancel this action now?", function(result) {
            if (result == true) {
                $("#add_account_type_div").hide(500);
            } else {}
        });
    });

    /** Hide import excel div on cancel button click */
    $(document).on("click", "#import_account_cancel_btn", function(e) {
        bootbox.confirm("Do you want to cancel this action now?", function(result) {
            if (result == true) {
                $("#import_account_type_div").hide(500);
            } else {}
        });
    });

    /** jqGrid status */
    $('#jqGridStatus').on('change', function() {
        var selected = this.value;
        $('#AccountType-table').jqGrid('GridUnload');
        $("#import_account_type_div").hide(500);
        $("#add_account_type_div").hide(500);
        changeGridStatus(selected);
        jqGrid(selected, true);
    });

    /**     * jqGrid Initialization function     * @param status     */
    function jqGrid(status, deleted_at) {
        var table = 'company_account_type';
        var columns = ['Account Type', 'Range From', 'Range To', 'Status', 'Set as Default', 'Action'];
        var select_column = ['Edit', 'Deactivate', 'Delete'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['company_account_type.status', 'company_account_type.deleted_at'];
        var columns_options = [{
            name: 'Account Type',
            index: 'account_type_name',
            width: 90,
            align: "center",
            searchoptions: {
                sopt: conditions
            },
            table: table
        }, {
            name: 'Range From',
            index: 'range_from',
            width: 100,
            align: "center",
            searchoptions: {
                sopt: conditions
            },
            table: table
        }, {
            name: 'Range To',
            index: 'range_to',
            width: 100,
            align: "center",
            searchoptions: {
                sopt: conditions
            },
            table: table
        }, {
            name: 'Status',
            index: 'status',
            width: 80,
            align: "center",
            searchoptions: {
                sopt: conditions
            },
            table: table,
            formatter: statusFormatter
        }, {
            name: 'Set as Default',
            index: 'is_default',
            width: 80,
            align: "center",
            searchoptions: {
                sopt: conditions
            },
            table: table,
            formatter: isDefaultFormatter
        }, {
            name: 'Action',
            index: 'select',
            title: false,
            width: 80,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            formatter: 'select',
            edittype: 'select',
            search: false,
            table: table,
            formatter: actionFormatter
        }];
        var ignore_array = [];
        jQuery("#AccountType-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_account_type",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                deleted_at: deleted_at
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Account Types",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {
                    fromServer: true
                }
            }
        }).jqGrid("navGrid", {
            edit: false,
            add: false,
            del: false,
            search: true,
            reloadGridOptions: {
                fromServer: true
            }
        }, {
            top: 200,
            left: 200,
            drag: true,
            resize: false
        });
    }

    /**     * jqGrid function to format status     * @param cellValue     * @param options     * @param rowObject     * @returns {string}     */
    function statusFormatter(cellValue, options, rowObject) {
        if (cellValue == 1) return "Active";
        else if (cellValue == '0') return "Inactive";
        else return '';
    }

    /**     * jqGrid function to format is_default column     * @param cellValue     * @param options     * @param rowObject     * @returns {string}     */
    function isDefaultFormatter(cellValue, options, rowObject) {
        if (cellValue == 1) return "Yes";
        else if (cellValue == '0') return "No";
        else return '';
    }

    /**     * jqGrid function to format action column     * @param cellValue     * @param options     * @param rowObject     * @returns {string}     */
    function actionFormatter(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            var is_default = rowObject;

            var editable = $(cellValue).attr('editable');
            var select = '';
            if (rowObject.Status == 1) select = ['Edit', 'Deactivate'];
            if (rowObject.Status == '0' || rowObject.Status == '') select = ['Edit', 'Activate'];
            var data = '';
            if (select != '') {
                var data = '<select ' + ' class="form-control select_options" is_default="' + is_default['Set as Default'] + '" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function(key, val) {
                    if (editable == '0' && (val == 'delete' || val == 'Delete')) {
                        return true;
                    }
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }
    $.validator.addMethod('minValue', function(value, el, param) {
        return value > param;
    });

    /** Add/Edit new account type */
    $("#add_account_type_form").validate({
        rules: {
            account_type_name: {
                required: true
            },
            range_from: {
                required: true,
                number: true,
                minValue: 0
            },
            range_to: {
                required: true,
                number: true,
                minValue: 0
            },
        },
        messages: {
            account_type_name: {
                required: "* This field is required",
            },
            range_from: {
                required: "* This field is required",
                number: "* Please add numerical values only",
                minValue: "* Minimum value is 1",
            },
            range_to: {
                required: "* This field is required",
                number: "* Please add numerical values only",
                minValue: "* Minimum value is 1",
            },
        },
        submitHandler: function() {
            var account_type_name = $('#account_type_name').val();
            var range_from = $('#range_from').val();
            var range_to = $('#range_to').val();
            var account_type_id = $("#account_type_id").val();
            var is_default;
            if ($('#is_default').is(":checked")) {
                is_default = '1';
            } else {
                is_default = '0';
            }
            var formData = {
                'account_type_name': account_type_name,
                'range_from': range_from,
                'range_to': range_to,
                'is_default': is_default,
                'account_type_id': account_type_id,
            };
            var action;
            if (account_type_id) {
                action = 'update';
            } else {
                action = 'insert';
            }
            $.ajax({
                type: 'post',
                url: '/AccountType-Ajax',
                data: {
                    class: 'AccountTypeAjax',
                    action: action,
                    form: formData
                },
                success: function(response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        $("#add_account_type_div").hide(500);
                        $("#AccountType-table").trigger('reloadGrid');
                        toastr.success(response.message);
                        onTop(true);
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        console.log('response.data>>>', response.data);
                        $.each(response.data, function(key, value) {
                            $('#' + key).text('* ' + value);
                        });
                    } else if (response.status == 'error' && response.code == 503) {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });

    /** List Action Functions  */
    $(document).on('change', '.select_options', function() {
        setTimeout(function() {
            $(".select_options").val("default");
        }, 200);
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index();
        var status = $(this).attr('status');
        var is_default = $(this).attr('is_default');
        if (opt == 'Edit' || opt == 'EDIT') {
            $("#add_account_type_div").show(500);
            headerDiv.innerText = "Edit Account Type";
            $('#saveBtnId').val('Update');
            $('#account_type_nameErr').text('');
            $('.table').find('.green_row_left, .green_row_right').each(function() {
                $(this).removeClass("green_row_left green_row_right");
            });
            $('.table').find('tr:eq(' + row_num + ')').find('td:eq(0)').addClass("green_row_left");
            $('.table').find('tr:eq(' + row_num + ')').find('td').last().addClass("green_row_right");
            $(".clear-btn").text('Reset');
            $(".clear-btn").addClass('formreset');
            $(".clear-btn").removeClass('clearFormReset');
            $.ajax({
                type: 'post',
                url: '/AccountType-Ajax',
                data: {
                    class: "AccountTypeAjax",
                    action: "view",
                    id: id,
                },
                success: function(response) {
                    var data = $.parseJSON(response);
                    if (data.status == "success") {
                        $("#account_type_name").val(data.data.account_type_name);
                        $("#range_from").val(data.data.range_from);
                        $("#range_to").val(data.data.range_to);
                        $("#account_type_id").val(data.data.id);
                        if (data.data.is_editable == 0) {
                            $('#unit_type').prop('disabled', true);
                        } else {
                            $('#unit_type').prop('disabled', false);
                        }
                        if (data.data.is_default == 1) {
                            $('#is_default').prop('checked', true);
                        } else {
                            $('#is_default').prop('checked', false);
                        }
                        defaultFormData = $('#add_account_type_form').serializeArray();
                    } else if (data.status == "error") {
                        toastr.error(data.message);
                    } else {
                        toastr.error(data.message);
                    }
                },
                error: function(data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function(key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        } else if (opt == 'Deactivate' || opt == 'DEACTIVATE' || opt == 'Activate' || opt == 'ACTIVATE') {
            opt = opt.toLowerCase();
            if (is_default == 1) {
                toastr.error('A default value cannot be deactivated.');
            } else {
                bootbox.confirm("Do you want to " + opt + " this record ?", function(result) {
                    if (result == true) {
                        var status = opt == 'activate' ? '1' : '0';
                        $.ajax({
                            type: 'post',
                            url: '/AccountType-Ajax',
                            data: {
                                class: 'UnitTypeAjax',
                                action: 'updateStatus',
                                status: status,
                                id: id
                            },
                            success: function(response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                                $('#AccountType-table').trigger('reloadGrid');
                                localStorage.setItem("rowcolor", 'add colour');
                            }
                        });
                    } else {
                        $('#Plans-table').trigger('reloadGrid');
                    }
                });
            }
        } else if (opt == 'Delete' || opt == 'DELETE') {
            opt = opt.toLowerCase();
            if (status == '1') {
                toastr.warning('A default set value cannot be deleted.');
            } else {
                bootbox.confirm("Do you want to delete this record ?", function(result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/AccountType-Ajax',
                            data: {
                                class: 'UnitTypeAjax',
                                action: 'delete',
                                id: id
                            },
                            success: function(response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                                $('#AccountType-table').trigger('reloadGrid');
                            }
                        });
                    } else {
                        $('#AccountType-table').trigger('reloadGrid');
                    }
                });
            }
        } else {}
    });

    /** Export sample account type excel */
    $(document).on("click", '#exportSampleExcel', function() {
        window.location.href = base_url + "/AccountType-Ajax?action=exportSampleExcel";
    });


    /** Export account type excel  */
    $(document).on("click", '#exportAccountTypeButton', function() {
        var status = $("#jqGridStatus option:selected").val();
        var table = 'company_account_type';
        window.location.href = base_url + "/AccountType-Ajax?status=" + status + "&&table=" + table + "&&action=exportExcel";
    });

    /** Import unit type excel  */
    $("#importAccountTypeFormId").validate({
        rules: {
            import_file: {
                required: true
            },
        },
        submitHandler: function (form) {
            event.preventDefault();
            return;
        }
    });


});