var photo_videos = [];
var imgArray = [];
$(document).on('change','#photosVideos',function(){
    // photo_videos = [];
    var fileData = this.files;

    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
        var validVideoTypes = ['video/mp4','video/avi'];
        var uploadType = ($.inArray(type, validImageTypes) < 0)? 'video':'image';
        var validSize = (uploadType == 'video')? '5120':'1030';
        var arrayType = (uploadType == 'video')? validVideoTypes:validImageTypes;
        if(uploadType == 'video' && fileData.length > 1) {
            toastr.warning('Please select one Video to upload!');
            return false;
        }

        if ($.inArray(type, arrayType) < 0) {
            toastr.warning('Please select file with valid extension only!');
        } else {
            if (parseInt(size) > validSize) {
                var validMb = (validSize == 5120)? 5 : 1;
                toastr.warning('Please select documents less than '+validMb+' mb!');
            } else {
                size = isa_convert_bytes_to_specified(value['size'], 'k') + 'kb';
                if($.inArray(value['name'], imgArray) === -1)
                {
                    photo_videos.push(value);
                }

                var src = '';
                var reader = new FileReader();

                //$('#photo_video_uploads').html('');
                reader.onload = function (e) {
                    src = e.target.result;
                    if(uploadType == 'image'){
                        var appendType = '<img class="img-upload-tab' + key + '" width=100 height=100 src=' + src + '>';
                    } else {
                        var appendType = '<video class="img-upload-tab' + key + '" width=240 height=140 controls>' +
                                         '<source src="'+src+'" type="'+type+'">' +
                                         '</video>';
                    }

                    if($.inArray(value['name'], imgArray) === -1)
                    {
                        $("#photo_video_uploads").append(
                            '<div class="row" style="margin:20px">' +
                            '<div class="col-sm-12 img-upload-library-div">' +
                            '<div class="col-sm-3">'+appendType+'</div>' +
                            '<div style="margin-top: 36px;" class="col-sm-3 show-library-list-imgs-name' + key + '">' + value['name'] + '</div>' +
                            '<input type="hidden" class="photoVideoInput" name="photoVideoName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                            '<div style="margin-top: 36px;" class="col-sm-2 show-library-list-imgs-size' + key + '"><strong>' + size + '</strong></div>' +
                            '<div style="margin-top: 36px;" class="col-sm-2 show-library-list-imgs-checkboc' + key + '">' +
                            '<div  class="col-sm-2"><span id=' + key + ' class="delete_photo_video cursor"><button type="button" class="orange-btn">Delete</button></span></div></div></div>');
                        imgArray.push(value['name']);
                    } else {
                        toastr.warning('Image already exists!');
                    }
                    //console.log(imgArray);

                };
                reader.readAsDataURL(value);
            }
        }
    });
});

function appendFiles(){

}

$('#savePhotoVideo').on('click',function(){
    var length = $('#photo_video_uploads > div').length;
    if(length > 0) {
        var data = convertSerializeDatatoArrayPhotos();
        var uploadform = new FormData();
        uploadform.append('class', 'propertyPhotosvideos');
        uploadform.append('action', 'upload_photos_videos');
        var count = photo_videos.length;
        $.each(photo_videos, function (key, value) {
            if(compareArray(value,data) == 'true'){
                uploadform.append(key, value);
                var marketing_site = JSON.stringify(data);
                uploadform.append('marketing_site', marketing_site);
            }
            if(key+1 === count){
                savePhotoVideos(uploadform);
            }
        });
    }
});

$(document).on('click','.delete_photo_video',function(){
    $(this).parent().parent().parent().parent('.row').remove();
    toastr.success('The record deleted successfully.');
});

$(document).on('click','#removePhotoVideo',function(){
    $('#photo_video_uploads').html('');
    $('#photosVideos').val('');
});

function savePhotoVideos(uploadform){
    $.ajax({
        type: 'post',
        url: '/property/photos_videos',
        data:uploadform,
        processData: false,
        contentType: false,
        success: function (response) {
            var response = JSON.parse(response);
            if(response.code == 200){
                $('#photo_video_uploads').html('');
                $('#propertPhotovideos-table').trigger('reloadGrid');
                toastr.success('Files uploaded successfully.');
            } else if(response.code == 500){
                toastr.warning(response.message);
            } else {
                toastr.success('Error while uploading files.');
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function convertSerializeDatatoArrayPhotos(){
    var newData = [];
    $(".photoVideoInput").each(function( index ) {
        var name = $(this).val();
        var size = $(this).attr('data_id');
        var id = $("#checkbox"+size);
        var checked = id.prop('checked')?'1':'0';
        newData.push({'name':name,'size':size,'siteMain':checked});
    });
    return newData;
}

function compareArray(data,compare){
    for(var i =0;i < compare.length;i++){
        if(compare[i].name == data['name'] && compare[i].size == data['size']){
            return 'true';
        }
    }
    return 'false';
}

function isa_convert_bytes_to_specified(bytes, to) {
    var formulas =[];
    formulas['k']= (bytes / 1024).toFixed(1);
    formulas['M']= (bytes / 1048576).toFixed(1);
    formulas['G']= (bytes / 1073741824).toFixed(1);
    return formulas[to];
}

/**
 * jqGrid Intialization function
 * @param status
 */
jqGridPhotovideos('All');
function jqGridPhotovideos(status) {
   var building_id = $("#edit_building_id").val();
    var table = 'building_file_uploads';
    var columns = ['Name','Image','Action'];
    var select_column = ['Delete'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = ['building_file_uploads.codec'];
    var extra_where = [{column:'file_type',value:1,condition:'='},{column:'building_id',value:building_id,condition:'='}];
    var columns_options = [
        {name:'Name',index:'file_name',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'file_location',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter: photoFormatter},
        {name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionLibraryFmatter,title:false}
    ];
    var ignore_array = [];
    jQuery("#buildingPhotovideos-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Photos/Virtual Tour Videos",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}

/**
 * jqGrid function to format action column
 * @param status
 */

function actionLibraryFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {

        var select = '';
        select = ['<span><i class="fa fa-trash delete_file_library pointer" data_id="' + rowObject.id + '" style="font-size:24px"></i></span>'];
        var data = '';
        if(select != '') {
            $.each(select, function (key, val) {
                data += val
            });
        }
        return data;
    }
}

function photoFormatter(cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var path = upload_url+'company/'+rowObject.Preview;
        var select = rowObject.Action;
        var type = $(select).attr('type');
        if(type.startsWith("image")){
            var appendType = '<a href="' + path + '" target="_blank"><img width=100 height=100 src=' + path + '></a>';
        } else if(type.startsWith("video")){
            var appendType = '<video width=240 height=140 controls>' +
                '<source src="'+path+'" type="'+type+'">' +
                '</video>';
        }
        return appendType;
    }
}

$(document).on('change', '#propertPhotovideos-table .select_options', function() {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    var row_num = $(this).parent().parent().index();
    if (opt == 'Email' || opt == 'EMAIL') {


    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/property/photos_videos',
                        data: {class: 'propertyPhotosvideos', action: "deleteFile", id: id},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                }
                $('#propertPhotovideos-table').trigger('reloadGrid');
            }
        });
    }
});