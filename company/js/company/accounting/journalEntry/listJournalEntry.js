$(document).ready(function () {

    if(localStorage.getItem("rowcolorTenant")){
        setTimeout(function(){
            jQuery('#journal-entry-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('#journal-entry-table').find('tr:eq(1)').find('td:eq(18)').addClass("green_row_right");
            localStorage.removeItem('rowcolorTenant');
        }, 2000);
    }

    if(localStorage.getItem('ElasticSearch')){
        var elasticSearchData = localStorage.getItem('ElasticSearch');
        setTimeout(function(){
            var grid = $("#journal-entry-table"),f = [];
            f.push({field: "accounting_journal_entries.id", op: "eq", data: elasticSearchData,int:'true'});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
            localStorage.removeItem('ElasticSearch');
        },1500);
    }


    /*  $('tr td:not(:last-child)');*/
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');
    function jqGrid(status) {
        var table = 'accounting_journal_entries';
        var columns = ['Journal Entry Id','Date','Expense Period','Portfolio','Property','Account','Debit('+default_currency_symbol+')','Credit('+default_currency_symbol+')','Property For Sale','Unit Number','Unit','Post to GL','Action'];
        var select_column = ['VIEW','REVERSE JE'];
        var joins = [{table:'accounting_journal_entries',column:'portfolio_id',primary:'id',on_table:'company_property_portfolio'},{table:'accounting_journal_entries',column:'property_id',primary:'id',on_table:'general_property'},{table:'accounting_journal_entries',column:'unit_id',primary:'id',on_table:'unit_details'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [{column:'status',value:'1',condition:'=',table:'accounting_journal_entries'},];
        var columns_options = [

            {name:'Journal Entry Id',index:'id',title:true, width:90,align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer'},
            {name: 'Date', index: 'created_at',title:true, width: 80,searchoptions: {sopt: conditions}, table: table, classes: 'cursor'}, /**cellattr:cellAttrdata**/
            {name:'ExpensePeriod',index:'accounting_period', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name: 'Portfolio', index: 'portfolio_name',title:false, width: 80,searchoptions: {sopt: conditions}, table: 'company_property_portfolio', classes: 'cursor'},
            {name: 'Property', index: 'property_name',title:false, width: 80,searchoptions: {sopt: conditions}, table: 'general_property', classes: 'cursor',formatter:propertyName},
            {name: 'Account', index: 'id',title:false, width: 150,searchoptions: {sopt: conditions}, table: table, classes: 'cursor p-0-imp',change_type:'journal_entry_account',formatter:journal_entry_formatter},
            {name: 'Debit', index: 'id',title:false, width: 80,searchoptions: {sopt: conditions}, table: table, classes: 'cursor p-0-imp',change_type:'journal_entry_debit',formatter:journal_entry_formatter},
            {name: 'Credit', index: 'id',title:false, width: 80,searchoptions: {sopt: conditions}, table: table, classes: 'cursor p-0-imp',change_type:'journal_entry_credit',secondTable:'company_chart_of_accounts',formatter:journal_entry_formatter},
            {name: 'PropertyForSale', index: 'property_for_sale',title:false, width: 80,searchoptions: {sopt: conditions}, table: 'general_property', classes: 'cursor','hidden':true},
            {name: 'Unit', index: 'unit_prefix',title:false, width: 80,searchoptions: {sopt: conditions}, table: 'unit_details', classes: 'cursor',formatter:UnitFormatter},
            {name: 'UnitNumber', index: 'floor_no',title:false, width: 80,searchoptions: {sopt: conditions}, table: 'unit_details', classes: 'cursor','hidden':true},
            {name:'Post to GL',index:'id',title:true, width:90,align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer',formatter:postToGlFormatter},
            // {name:'Created Date',index:'created_at', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            // {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},

        ];
        var ignore_array = [];
        jQuery("#journal-entry-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'accounting_journal_entries.updated_at',
            sortorder: 'desc',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Journal Enteries",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }


    function UnitFormatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var data = '';

                if (rowObject.Unit != '') {
                    data += '<span class="property_class">' + rowObject.Unit + '-' + rowObject.UnitNumber + '</span>';
                }

            return data;
        }
    }

    function statusFmatter1 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            select = ['VIEW','REVERSE JE']
            var data = '';

            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }

            return data;
        }
    }

    function journal_entry_formatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var length =cellvalue.length -1;
            var data = '';
            data += '<table class="table table-striped table-condensed table-hover-none last-tr multi-row-table"> <tbody>';
            $.each(cellvalue, function (key, val) {
                data += '<tr><td><span>'+default_currency_symbol+'</span><span>' + val + '</span></td></tr>';
                 // if(key < length) {
                 //    data += '<hr>';
                 // }
            });
            data += '</table> </tbody>';
            return data;
        }
    }


    function postToGlFormatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            return 'No';
        }
    }

    /**
     *  function to format property name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            console.log(rowObject);
            var flagValue = $(rowObject.Action).attr('flag');
            var id = $(rowObject.Action).attr('data_id');
            var clone = rowObject.clone_status;
            var flag = '';
                        if (rowObject.PropertyForSale == 'Yes') {
                            return '<div class="tooltipgridclass"><span><span style="color:#FF1A1A;text-decoration:underline;font-weight: bold;">' + cellValue + '</span></div>';
                        } else {
                            return '<div class="tooltipgridclass"><div><span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</div>';
                        }


            }
        }




    $(document).on('change', '.select_options', function () {
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        if (opt == 'View' || opt == 'VIEW') {
            window.location.href = base_url+'/Accounting/ViewJournalEntry?id='+id;
        }else if (opt == 'REVERSE JE' || opt == 'Reverse Je') {


            bootbox.confirm({
                message: "Are you sure you want to add a new Reverse Entry?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'get',
                            url: '/Accounting/journal-entry-ajax',
                            data: {
                                id: id,
                                class: 'JournalEntry',
                                action: 'Reverse'},
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.code == 200) {
                                    toastr.success(response.message);

                                    $('#journal-entry-table').trigger('reloadGrid');
                                    setTimeout(function(){
                                        jQuery('#journal-entry-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                                        jQuery('#journal-entry-table').find('tr:eq(1)').find('td:eq(18)').addClass("green_row_right");
                                    }, 2000);
                                }
                            },
                            error: function (data) {
                                var errors = $.parseJSON(data.responseText);
                                $.each(errors, function (key, value) {
                                    $('#' + key + '_err').text(value);
                                });
                            }
                        });
                    }
                }
            });


            // opt = opt.toLowerCase();
            // window.location.href = base_url+'/Accounting/ViewInvoice?id='+id;
        }
        $('.select_options').prop('selectedIndex',0);
    });

    $('#start_date').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        maxDate:new Date()
    }).datepicker("setDate", new Date());

    $('#end_date').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        maxDate:new Date()
    }).datepicker("setDate", new Date());

    $(document).on("click",".callLogSearch",function(){
        callLogFilter();
    });

    function callLogFilter(){
        var grid = $("#journal-entry-table"),f = [];
        //  var user_name = ($("#_easyui_textbox_input1").val() == '')?'all':$("#_easyui_textbox_input1").val();
        var select_type = ($("#incoming_outgoing_call_type").val() == '')?'':$("#incoming_outgoing_call_type").val();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        if(start_date != '' && end_date != '' && select_type != '') {
            f.push({
                field: "accounting_journal_entries.journal_entry_type",
                op: "cn",
                data: select_type
            }, {field: "accounting_journal_entries.created_at", op: "dateBetween", data: start_date, data2: end_date});
        }else{
            f.push( {field: "accounting_journal_entries.created_at", op: "dateBetween", data: start_date, data2: end_date});
        }
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all',extra_where :[]});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }


    function searchFilters(){
        var grid = $("#timeSheetTable"),f = [];
        var type = $('#_easyui_textbox_input1').val() != ''?$('#_easyui_textbox_input1').val():'all';
        var checked_value = $('input[name=filter_search]:checked').val()
        if(checked_value == '0'){
            var data = $('#ByWeekFilter option:selected').val();
            f.push({field: "daily_visitor_log.visitor", op: "cn", data: type},{field: "daily_visitor_log.date", op: "dateByString", data: data});
        } else if(checked_value == '1') {
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            f.push({field: "daily_visitor_log.visitor", op: "cn", data: type},{field: "daily_visitor_log.date", op: "dateBetween", data: start_date, data2:end_date});
        }
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    $(document).on('click','#journal-entry-table tr td',function(e) {
        e.preventDefault();
        var base_url = window.location.origin;
        var id = $(this).closest("tr").attr('id');
        if ($(this).index() == 12) {
            return false;
        } else {
            window.location.href = base_url + '/Accounting/ViewJournalEntry?id=' + id;
        }
    });
});


