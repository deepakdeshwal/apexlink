$(document).ready(function () {

    if(localStorage.getItem('ElasticSearch')){
        var elasticSearchData = localStorage.getItem('ElasticSearch');
        setTimeout(function(){
            var grid = $("#list_of_invoices"),f = [];
            f.push({field: "accounting_invoices.id", op: "eq", data: elasticSearchData,int:'true'});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
            localStorage.removeItem('ElasticSearch');
        },1500);
    }

    invoiceList();
    function invoiceList() {
        var checkbox = '<div class="d-flex flex-center"><input type="checkbox" id="select_all_complaint_checkbox"></div>';
        var table = ' accounting_invoices';
        var columns = [checkbox,'Invoice #','Invoice_to_id','Invoice Date', 'Late Date', 'Status','Property','Owner Property','Floor No','Unit Prefix','Invoice To','Other Name','Type','Amount Due ('+default_currency_symbol+')' ,'Action'];
        var select_column = ['Edit','Email','TEXT','View','Add In-touch','In-Touch History','Delete'];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_where = [{column:'module_type',value:'',condition:'IS NULL',table:table}];
        var extra_columns = ['accounting_invoices.updated_at'];
        var joins = [{table: 'accounting_invoices', column: 'invoice_to', primary: 'user_id', on_table: 'tenant_property'},
            {table: 'tenant_property', column: 'property_id', primary: 'id', on_table: 'general_property'},
            {table: 'tenant_property', column: 'unit_id', primary: 'id', on_table: 'unit_details'},
            {table: 'accounting_invoices', column: 'invoice_to', primary: 'id', on_table: 'users'},
            {table: 'accounting_invoices', column: 'property_id', primary: 'id', on_table: 'general_property' ,as:'OwnerProperty' },
            //     {table: 'work_order', column: 'vendor_id', primary: 'id', on_table: 'users'}
        ];
        var columns_options = [
            {name:'Id',index:'id', width:100, sortable: false, searchoptions: {sopt: conditions},search: false,table:table,formatter:actionCheckboxFmatterComplaint},
            {name:'Invoice',index:'invoice_number', width:80,searchoptions: {sopt: conditions},table:table,formatter:invoiceNumberFormatter},
            {name:'Invoice_to_id',index:'invoice_to', width:100,searchoptions: {sopt: conditions},table:table,hidden:true},
            {name:'InvoiceDate',index:'invoice_date', width:120,change_type:'date',searchoptions: {sopt: conditions},table:table},
            {name:'LateDate',index:'late_date', width:150,change_type:'date',searchoptions: {sopt: conditions},table:table},
            {name:'Status',index:'status', width:100,searchoptions: {sopt: conditions},table:table,formatter:statusPaidUnpaidFormatter},
            {name:'Property',index:'property_name', width:100,searchoptions: {sopt: conditions},table:'general_property',formatter:PropertyUnitFormatter},
            {name:'OwnerProperty',index:'property_name', alias:'owner_property' ,'hidden':true,table:'OwnerProperty'},
            {name:'FloorNo',index:'floor_no',hidden:true, width:100,searchoptions: {sopt: conditions},table:'unit_details'},
            {name:'UnitPrefix',index:'unit_prefix',hidden:true, width:100,searchoptions: {sopt: conditions},table:'unit_details'},
            {name:'InvoiceTo',index:'name', width:100,searchoptions: {sopt: conditions},table:'users',formatter:InvoiceToFormatter},
            {name:'OtherName',index:'other_name','hidden':true ,width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Type',index:'user_type', width:80,searchoptions: {sopt: conditions},table:table,formatter:UserTypeFormatter},
            {name:'Amount Due',index:'amount_due', width:150,searchoptions: {sopt: conditions},table:table,change_type:'invoice_amount_due',secondTable:'tenant_charges',update_column:'amount_due',formatter:dueDateFormat},
            {name:'Action',index:'',title:false, width:150,searchoptions: {sopt: conditions},table:table,formatter:statusFmatter1},
        ];

        var ignore_array = [];
        jQuery("#list_of_invoices").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'true'
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Invoices",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:200,drag:true,resize:false} // search options
        );
    }

    function actionCheckboxFmatterComplaint(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {

            var data = '';
            var data = '<div class="d-flex flex-center height-100"><input type="checkbox" name="maintenance_checkbox[]" class="maintenance_checkbox" id="maintenance_checkbox' + rowObject.id + '" data_id="' + rowObject.id + '"/></div>';
            return data;
        }
    }

    function statusFmatter1(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var select = ['Edit','View Detail','Apply Payment'];

            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '" data-user_id="' + rowObject.user_id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    function PropertyUnitFormatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var data = '';
            if(rowObject.Type == 2) {
                if (rowObject.Property != '') {
                    data += '<span class="property_class">' + rowObject.Property + '</br> Unit :' + rowObject.FloorNo + '-' + rowObject.UnitPrefix + '</span>';
                }
            }else if(rowObject.Type == 4){
                data += '<span class="property_class">' + rowObject.OwnerProperty  + '</span>';
            }
            return data;
        }
    }

    function dueDateFormat(cellvalue, options, rowObject){
        if (rowObject !== undefined) {
            var html = '';
            $.each(cellvalue,function(key,value){
                 html +='<span>'+value+'</span><br>';
            });
            return html;
        }
    }

    function InvoiceToFormatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var data = '';
            if(rowObject.InvoiceTo != '') {
                data += '<span class="invoice_to_name">' + rowObject.InvoiceTo +'</span>';
            }else{
                data += '<span class="invoice_to_name">' + rowObject.OtherName +'</span>';
            }
            return data;
        }
    }

    function UserTypeFormatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var data = '';
            var user = "Other";
            if (rowObject.Type != '') {
                if (rowObject.Type == 2) {
                    user = 'Tenant';

                } else if (rowObject.Type == 4) {
                    user = 'Owner';
                }
            }
            data += '<span class="invoice_to_name">' + user + '</span>';
            return data;
        }
    }

    function AmountFormatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var data = '';
            if(rowObject.Property != '') {
                data += '<span class="property_class">' + rowObject.Property + '</br> Unit :' + rowObject.FloorNo + '-' + rowObject.UnitPrefix+'</span>';
            }
            return data;
        }
    }

    function statusPaidUnpaidFormatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {

            var data = '';
            // if(rowObject.Property != '') {
                if(rowObject.LateDate != '' && rowObject.Status == 0  ){
                    var late_date = convertDateFormat(rowObject.LateDate);
                    var invoice_date = convertDateFormat(rowObject.InvoiceDate);
                   // var date1 = new Date(invoice_date);
                    var date1 = new Date();

                    var date2 = new Date(late_date);
                    var Difference_In_Time = date2.getTime() - date1.getTime();

                    // To calculate the no. of days between two dates
                    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
                    //alert(Difference_In_Days);
                    if(Difference_In_Days < 0 && rowObject.Status == 0){

                        var new_days = Math.abs(Difference_In_Days);
                        var final_days = Math.floor(new_days);
                        var day = (final_days > 1)?'Days': 'Day';
                        status = "Late ("+final_days+' '+day+")";
                        data += '<span class="red-star">'+status+'</span>';
                    }else if(Difference_In_Days > 0 && rowObject.Status == 0){
                        status = 'Unpaid';
                        data += '<span class="red-star">'+status+'</span>';
                    }else if(Difference_In_Days == 0 && rowObject.Status == 0){
                        status = 'Unpaid';
                        data += '<span class="red-star">'+status+'</span>';
                    }
                }else if(rowObject.Status == 0){
                    status = 'Unpaid';
                    data += '<span class="red-star">'+status+'</span>';
                }else if (rowObject.Status == 1){
                    status = 'Paid';
                    data += '<span class="paid_class">'+status+'</span>';
                }
            // }
            return data;
        }
    }


    function convertDateFormat(date){
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    }





    $(document).on("change","#select_all_complaint_checkbox", function(){

        if($(this).is(":checked")){
            $(".maintenance_checkbox").prop("checked",true);
        }else{
            $(".maintenance_checkbox").prop("checked",false);
        }

    });

    $(document).on("change",".maintenance_checkbox", function(){
        var checked_checkboxes_number =  $('[name="maintenance_checkbox[]"]:checked').length;
        var total_checkboxes =  $('[name="maintenance_checkbox[]"]').length;

      if(checked_checkboxes_number != total_checkboxes){
          $("#select_all_complaint_checkbox").prop("checked",false);
      }else{
          $("#select_all_complaint_checkbox").prop("checked",true);
      }

    });




    $(document).on('change', '.select_options', function () {
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var type = $('#'+id).find("td:eq(1)").find('a').attr('data-type');
        var Name = $('#'+id).find("td:eq(1)").find('a').attr('data-username');
        var UserId = $('#'+id).find("td:eq(1)").find('a').attr('data-user_id');

        if (opt == 'Edit' || opt == 'EDIT') {
            var status = $(this).closest('tr').find('.paid_class').html();
             if(status =='Paid'){
                 toastr.error("Paid Invoice cannot be edited.");
                 $('.select_options').prop('selectedIndex',0);
                 return false;
             }
            opt = opt.toLowerCase();
            window.location.href = base_url+'/Accounting/EditInvoice?id='+id;

        }else if (opt == 'View Detail' || opt == 'VIEW DETAIL') {
            opt = opt.toLowerCase();
            window.location.href = base_url+'/Accounting/ViewInvoice?id='+id;
        }else if (opt == 'Apply Payment' || opt == 'APPLY PAYMENT'){
            $.ajax({
                type: 'post',
                url: '/Accounting/invoice-ajax',
                data: {
                    invoice_id: id,
                    type: type,
                    user_id:UserId,
                    class: 'Invoice',
                    action: 'getTotalAmountDue'
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {

                            if(response.data.total_due_amount == 0) {
                                bootbox.confirm({
                                    message: "This Invoice has been paid in full, do you want to enter a Credit ?",
                                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                                    callback: function (result) {
                                        if (result == true) {
                                            localStorage.setItem("invoice_id",id);
                                            localStorage.setItem("type",type);
                                            localStorage.setItem("Name",Name);
                                            localStorage.setItem("user_id",UserId);
                                            window.location.href = base_url + '/Accounting/receivebatchpayment';
                                        }

                                    }
                                });
                            }else{
                                localStorage.setItem("invoice_id",id);
                                localStorage.setItem("type",type);
                                localStorage.setItem("Name",Name);
                                localStorage.setItem("user_id",UserId);
                                window.location.href = base_url + '/Accounting/receivebatchpayment';
                            }


                    } else if (response.code == 400) {
                        toastr.warning(response.message);
                    } else {
                        toastr.error(response.message);
                    }
                    $("#list_of_invoices").trigger('reloadGrid');
                }
            });


        }
        $('.select_options').prop('selectedIndex',0);
    });

    $(document).on("click",".delete_invoice",function(){
        var checked_checkboxes_number =  $('[name="maintenance_checkbox[]"]:checked').length;
        if(checked_checkboxes_number == 0){
            toastr.warning("Select alteast one invoice to delete");
            return false;
        }else{
            var ids =   getDataValues();
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/Accounting/invoice-ajax',
                            data: {
                                ids: ids,
                                class: 'Invoice',
                                action: 'deleteInvoices'
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                } else if (response.code == 500) {
                                    toastr.warning(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                                $("#list_of_invoices").trigger('reloadGrid');
                            }
                        });
                    }
                    $("#list_of_invoices").trigger('reloadGrid');
                }
            });
        }


    });





    $(document).on("click",".print_email_envoice",function() {
        var ids = getDataValues();
        var checked_checkboxes_number = $('[name="maintenance_checkbox[]"]:checked').length;
        if (checked_checkboxes_number == 0) {
            toastr.error("Select alteast one invoice ");
            return false;
        }else{
        $.ajax({
            type: 'post',
            url: '/Accounting/invoice-ajax',
            data: {
                ids: ids,
                class: "Invoice",
                action: "getAllInvoices"
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    // update other modal elements here too
                    $('#invoice_content').html(response.data);

                    // show modal
                    $('#InvoiceModal').modal('show');
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    });
    $('#InvoiceModal').modal({backdrop: 'static', keyboard: false,show:false});

    $(document).on("click",".close",function(){
        $(".maintenance_checkbox").prop("checked",false);
    });


    function getNameFormatter(cellvalue, options, rowObject)
    {
        if(rowObject!==undefined)
        {
            if(rowObject.Other_name=='')
            {
                return '<a href="JavaScript:Void(0);" class="getChargeTable" data-username ="'+rowObject.Name+'"  data-type="user" data-user_id="'+rowObject.user_id+'" data-invoice_id="'+rowObject.invoice_id+'" style="color:#05A0E4 !important;"><strong>'+rowObject.Name+'</strong></a>';
            }
            else
            {
                return '<a href="JavaScript:Void(0);" class="getChargeTable" data-username ="'+rowObject.Other_name+'" data-type="other" data-user_id="'+rowObject.user_id+'" data-invoice_id="'+rowObject.invoice_id+'" style="color:#05A0E4 !important;"><strong>'+rowObject.Other_name+'</strong></a>';
            }
        }
    }
    function getPropertyFormatter(cellvalue, options, rowObject)
    {
        if(rowObject!==undefined)
        {
            if(rowObject.Other_name=='')
            {
                return '<a href="JavaScript:Void(0);" class="getChargeTable" data-username ="'+rowObject.Name+'"  data-type="user" data-user_id="'+rowObject.user_id+'" data-invoice_id="'+rowObject.invoice_id+'" style="color:#05A0E4 !important;"><strong>'+rowObject.property_name+'</strong></a>';
            }
            else
            {
                return '<a href="JavaScript:Void(0);" class="getChargeTable" data-username ="'+rowObject.Other_name+'" data-type="other" data-user_id="'+rowObject.user_id+'" data-invoice_id="'+rowObject.invoice_id+'" style="color:#05A0E4 !important;"><strong>'+rowObject.property_name+'</strong></a>';
            }
        }
    }
    $(document).on("click",".getChargeTable",function(){
        var type = $(this).attr('data-type');
        var Name = $(this).attr('data-username');
        $(".userFullAccount").fadeIn();
        $(".allCharges").fadeIn();
        if(type=='user')
        {
            var user_id = $(this).attr('data-user_id');
            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {
                    "action": 'getReceiableData',
                    "class": 'accounting',
                    "type":type,
                    "user_id":user_id,
                    "username":Name
                },
                success: function (response) {
                    $(".userFullAccount").html(response);
                    getAllChargesData(type,user_id,Name);
                }
            });
        }
        else
        {
            var invoice_id = $(this).attr('data-invoice_id');
            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {
                    "action": 'getReceiableData',
                    "class": 'accounting',
                    "invoice_id":invoice_id,
                    "type":type,
                    "username":Name
                },
                success: function (response) {
                    $(".userFullAccount").html(response);
                    getAllChargesData(type,invoice_id,Name);
                }
            });
        }
    });
    function getAllChargesData(type,id,Name)
    {
        if(type=='user')
        {
            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {
                    "action": 'getAllChargesData',
                    "class": 'accounting',
                    "type":type,
                    "user_id":id,
                    "username":Name
                },
                success: function (response) {
                    $(".allCharges").html(response);
                    updateAllPayments();
                }
            });
        }
        else
        {
            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {
                    "action": 'getAllChargesData',
                    "class": 'accounting',
                    "type":type,
                    "invoice_id":id,
                    "username":Name
                },
                success: function (response) {
                    $(".allCharges").html(response);
                    updateAllPayments();
                }
            });
        }
    }





/* recivable section */

    function invoiceNumberFormatter(cellvalue, options, rowObject)
    {
        if(rowObject!==undefined)
        {
            if(rowObject.OtherName=='')
            {
                return '<a href="JavaScript:Void(0);" class="getChargeTable" data-username ="'+rowObject.InvoiceTo+'"  data-type="user" data-user_id="'+rowObject.Invoice_to_id+'" data-invoice_id="'+rowObject.id+'" style="color:#05A0E4 !important;"><strong>'+rowObject.Invoice+'</strong></a>';
            }
            else
            {
                return '<a href="JavaScript:Void(0);" class="getChargeTable" data-username ="'+rowObject.OtherName+'" data-type="other" data-user_id="'+rowObject.Invoice_to_id+'" data-invoice_id="'+rowObject.id+'" style="color:#05A0E4 !important;"><strong>'+rowObject.Invoice+'</strong></a>';
            }
        }
    }
    // function getPropertyFormatter(cellvalue, options, rowObject)
    // {
    //     if(rowObject!==undefined)
    //     {
    //         if(rowObject.Other_name=='')
    //         {
    //             return '<a href="JavaScript:Void(0);" class="getChargeTable" data-username ="'+rowObject.Name+'"  data-type="user" data-user_id="'+rowObject.user_id+'" data-invoice_id="'+rowObject.invoice_id+'" style="color:#05A0E4 !important;"><strong>'+rowObject.property_name+'</strong></a>';
    //         }
    //         else
    //         {
    //             return '<a href="JavaScript:Void(0);" class="getChargeTable" data-username ="'+rowObject.Other_name+'" data-type="other" data-user_id="'+rowObject.user_id+'" data-invoice_id="'+rowObject.invoice_id+'" style="color:#05A0E4 !important;"><strong>'+rowObject.property_name+'</strong></a>';
    //         }
    //     }
    // }

    // $(document).on('click','#journal-entry-table tr td',function(e) {
    //     e.preventDefault();
    //     var base_url = window.location.origin;
    //     var id = $(this).closest("tr").attr('id');
    //     if ($(this).index() == 7) {
    //         return false;
    //     } else {
    //         window.location.href = base_url + '/Accounting/ViewJournalEntry?id=' + id;
    //     }
    // });
    $(document).on("click","#list_of_invoices tr td",function(){
            if ($(this).index() == 14) {
                return false;
            }else {
                var id = $(this).closest("tr").attr('id');
                var type = $('#' + id).find("td:eq(1)").find('a').attr('data-type');
                var Name = $('#' + id).find("td:eq(1)").find('a').attr('data-username');
                $(".userFullAccount").fadeIn();
                $(".allCharges").fadeIn();
                if (type == 'user') {
                   
                    var user_id = $('#' + id).find("td:eq(1)").find('a').attr('data-user_id');
                  
                    $.ajax({
                        url: '/accountingReceivable',
                        type: 'POST',
                        data: {
                            "action": 'getReceiableData',
                            "class": 'accounting',
                            "type": type,
                            "user_id": user_id,
                            "username": Name
                        },
                        success: function (response) {
                            $(".userFullAccount").html(response);
                            getAllChargesData(type, user_id, Name);
                        }
                    });
                }
                else {
                    var invoice_id = id;
                    $.ajax({
                        url: '/accountingReceivable',
                        type: 'POST',
                        data: {
                            "action": 'getReceiableData',
                            "class": 'accounting',
                            "invoice_id": invoice_id,
                            "type": type,
                            "username": Name
                        },
                        success: function (response) {
                            $(".userFullAccount").html(response);
                            getAllChargesData(type, invoice_id, Name);
                        }
                    });
                }
            }
    });
    function getAllChargesData(type,id,Name)
    {
        if(type=='user')
        {
            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {
                    "action": 'getAllChargesData',
                    "class": 'accounting',
                    "type":type,
                    "user_id":id,
                    "username":Name
                },
                success: function (response) {
                    $(".allCharges").html(response);
                    $('.reallocate').attr('data-type',type);
                    $('.reallocate').attr('data-username',Name);
                    $('.reallocate').attr('data-user_id',id);
                    updateAllPayments();
                }
            });
        }
        else
        {
            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {
                    "action": 'getAllChargesData',
                    "class": 'accounting',
                    "type":type,
                    "invoice_id":id,
                    "username":Name
                },
                success: function (response) {
                    $(".allCharges").html(response);
                    $('.reallocate').attr('data-type',type);
                    $('.reallocate').attr('data-username',Name);
                    $('.reallocate').attr('data-invoice_id',id);
                   updateAllPayments();
                }
            });
        }
    }


    $(document).on('keypress keyup','.numberonly',function(){
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $(document).on("blur keyup",".paidAmt",function(){
        var paidAmt = $(".paidAmt").val().replace(/,/g, '');
        if(paidAmt=="")
        {
            paidAmt = 0;
        }
        var hiddenDueAmount = $('.hiddenDueAmount').val().replace(/,/g, '');
        var hiddenOverPay = $(".hiddenOverPay").val().replace(/,/g, '');
        var hiddenUncheckAmt = $('.hiddenUncheckedAmt').val().replace(/,/g, '');
        var overPayUnderPay = parseFloat(hiddenOverPay) + parseFloat(paidAmt) +parseFloat(hiddenUncheckAmt) -parseFloat(hiddenDueAmount);
        var overPayAmt = overPayUnderPay.toString();
        var floatAmt = changeToFloat(overPayAmt);
        $(".overUnderPay").val(floatAmt);
    });




});

/*
Function to print envelope
*/
function PrintElem(elem)
{
    Popup($(elem).html());
}

function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
        'font-weight:bold;} .right-detail{\n' +
        '        position:relative;\n' +
        '        left:+400px;\n' +
        '    }</style>');
    $(mywindow.document.body).html( '<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if(mywindow.close()){

    }
    $("#InvoiceModal").modal('hide');
    $(".maintenance_checkbox").prop("checked",false);
    return true;
}

/*function to print element by id */
function sendInvoiceEmail()
{
    SendMail();
}


function SendMail(){
    var ids =   getDataValues();
    $.ajax({
        type: 'post',
        url: '/Accounting/invoice-ajax',
        data: {class: 'Invoice', action: 'sendInvoiceEmail', ids: ids},
        success : function(response){
            var response =  JSON.parse(response);

            if(response.status == 'success' && response.code == 200) {
                toastr.success('Mail send successfully.');
                $("#InvoiceModal").modal('hide');
                $(".maintenance_checkbox").prop("checked",false);
                $("#select_all_complaint_checkbox").prop("checked",false);
            }else {
                toastr.warning('Mail not send due to technical issue.');
            }
        }
    });
}
function getDataValues()
{
    var data_ids = [];

    var inputs =  $('[name="maintenance_checkbox[]"]:checked');
    inputs.each(function(){
        data_ids.push($(this).attr('data_id'));
    });
    return data_ids;
}

function updateAllPayments()
{
    $("#updateAllCharges").validate({
        rules: {
        },
        submitHandler: function (e) {
            var form = $('#updateAllCharges')[0];
            var formData = new FormData(form);
            formData.append('action','updateAllCharges');
            formData.append('class','accounting');
            var hiddenDueAmt = $(".hiddenDueAmount").val().replace(/,/g, '');
            if(hiddenDueAmt==0)
            {
                bootbox.confirm({
                    message: "This Invoice has been paid in full, do you want to enter a Credit ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            var OverUnderPay = $(".overUnderPay").val().replace(/,/g, '');
                            if(OverUnderPay<0)
                            {
                                toastr.error("Currently you are paying less amount");
                                return false;
                            }
                            var ref = $(".ref").val();
                            var check = $(".check").val();
                            var paymnt_type = $(".paymnt_type").val();
                            var paid_amount = $(".paidAmt").val().replace(/,/g, '');
                            var hiddenChargeType = $(".hiddenChargeType").val();
                            formData.append('class','accounting');
                            formData.append('hiddenDueAmount',hiddenDueAmt);
                            formData.append('overPay',OverUnderPay);
                            formData.append('ref',ref);
                            formData.append('check',check);
                            formData.append('paymnt_type',paymnt_type);
                            formData.append('paid_amount',paid_amount);
                            formData.append('hiddenChargeType',hiddenChargeType);
                            $.ajax({
                                url:'/accountingReceivable',
                                type: 'POST',
                                data: formData,
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if(response.status=='false')
                                    {
                                        toastr.error(response.message);
                                    }
                                    else
                                    {
                                        toastr.success(response.message);
                                        $(".allCharges").fadeOut();
                                        $(".userFullAccount").fadeOut();
                                    }
                                },
                                cache: false,
                                contentType: false,
                                processData: false
                            });
                        }
                    }
                });
            }
            else
            {
                var OverUnderPay = $(".overUnderPay").val();
                if(OverUnderPay<0)
                {
                    toastr.error("Currently you are paying less amount");
                    return false;
                }
                var ref = $(".ref").val();
                var check = $(".check").val();
                var paymnt_type = $(".paymnt_type").val();
                var paid_amount = $(".paidAmt").val().replace(/,/g, '');
                var hiddenChargeType = $(".hiddenChargeType").val();
                formData.append('class','accounting');
                formData.append('hiddenDueAmount',hiddenDueAmt);
                formData.append('overPay',OverUnderPay);
                formData.append('ref',ref);
                formData.append('check',check);
                formData.append('paymnt_type',paymnt_type);
                formData.append('paid_amount',paid_amount);
                formData.append('hiddenChargeType',hiddenChargeType);
                $.ajax({
                    url:'/accountingReceivable',
                    type: 'POST',
                    data: formData,
                    success: function (response) {
                        var response = JSON.parse(response);
                        if(response.status=='false')
                        {
                            toastr.error(response.message);
                        }
                        else
                        {
                            toastr.success(response.message);
                            $(".allCharges").fadeOut();
                            $(".userFullAccount").fadeOut();
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        }
    });
}

$(document).on("click", ".pay_checkbox", function () {
    var value = $(this).val();
    if (this.checked) {
        $(".waiveOfAmount_"+value).attr('name','waiveOfAmount[]');
        $(".waiveOfComment_"+value).attr('name','waiveOfComment[]');
        $(".currentPayment_"+value).attr('name','currentPayment[]');
        var currntDueAmt = $(".current_due_amt_"+value).html().replace("$", '');
        var currntDueAmt = currntDueAmt.replace("$", '');
        $(".currentPayment_"+value).val(currntDueAmt);
        var overPay = $(".overUnderPay").val();
        var curntOvrPay = parseFloat(overPay) - parseFloat(currntDueAmt);
        var curntOvrPay =    curntOvrPay.toString()
        var curntOvrPay = changeToFloat(curntOvrPay);
        $(".overUnderPay").val(curntOvrPay);
        var hiddenUncheckAmt = $('.hiddenUncheckedAmt').val();
        var currentHiddenUncheckAmt  =  parseFloat(hiddenUncheckAmt) -  parseFloat(currntDueAmt);
        $('.hiddenUncheckedAmt').val(currentHiddenUncheckAmt);
    }
    else {
        $(".waiveOfAmount_"+value).removeAttr('name');
        $(".waiveOfComment_"+value).removeAttr('name');
        $(".currentPayment_"+value).removeAttr('name');
        $(".currentPayment_"+value).val('0.00');
        var currntDueAmt = $(".current_due_amt_"+value).html().replace("$", '');
        var overPay = $(".overUnderPay").val();
        var curntOvrPay = parseFloat(overPay) + parseFloat(currntDueAmt);
        var curntOvrPay =    curntOvrPay.toString()
        var curntOvrPay = changeToFloat(curntOvrPay);
        $(".overUnderPay").val(curntOvrPay);
        var hiddenUncheckAmt = $('.hiddenUncheckedAmt').val();
        var currentHiddenUncheckAmt  =  parseFloat(hiddenUncheckAmt) +  parseFloat(currntDueAmt);
        $('.hiddenUncheckedAmt').val(currentHiddenUncheckAmt);
    }
});
