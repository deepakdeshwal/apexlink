$(document).ready(function () {
    $(".main_Accounting").removeClass("collapsed");
    $(".main_Accounting").attr("aria-expanded","true");
    $(".check_setup").css("background","#D8D8D8");

    $("#leftnav3").addClass("in");
    $("#leftnav3").attr("aria-expanded","true");
});


$(function() {

   // setTimeout(function () {
   //     $('#standard_options').multiselect({
   //         includeSelectAllOption: true
   //     });
   //
   //     $("#standard_options").multiselect('selectAll', false);
   //     $("#standard_options").multiselect('updateButtonText');
   // },500)

});
// $('#standard_options').multiselect('destroy').multiselect({
//     includeSelectAllOption: true,
//     nonSelectedText: 'Select check options'
// });

$('#searchform').keypress(function(e){
    if ( e.which == 13 ) return false;
});

$(document).ready(function () {
    $('#check_type').change(function () {
        var check_type = $(this).val();

        ChequeImage(check_type);
    });
})

function ChequeImage(check_type)
{
    if(check_type == 1)
    {
        $('.common_check').hide();
        $('.standard_check').show();
        $('.standard_options').show();
        $('.wallet_options,.common_options').hide();
    } else if(check_type == 2)
    {
        $('.common_check').hide();
        $('.wallet_check').show();
        $('.standard_options,.common_options').hide();
        $('.wallet_options').show();
    } else if(check_type == 3)
    {
        $('.common_check').hide();
        $('.voucher_check').show();
        $('.wallet_options,.standard_options').hide();
        $('.common_options').show();
    } else if(check_type == 4)
    {
        $('.common_check').hide();
        $('.middle_style').show();
        $('.wallet_options,.standard_options').hide();
        $('.common_options').show();
    }else if(check_type == 5)
    {
        $('.common_check').hide();
        $('.bottom_style').show();
        $('.wallet_options,.standard_options').hide();
        $('.common_options').show();
    } else {
        $('.common_check').hide();
        $('.standard_check').show();
        $('.wallet_options,.standard_options').hide();
        $('.common_options').show();
    }
}

$(document).ready(function() {
    $("#standard_options,#wallet_options,#common_options").multiselect({
        includeSelectAllOption: true,
        nonSelectedText: 'Select check options',
        onChange: function(option, checked, select) {
            var opselected = $(option).val();
            console.log(opselected,checked,select);
            checkDisplay(checked,opselected);
        }
    });
    // $("#standard_options,#wallet_options,#common_options").multiselect('selectAll', false);
    $("#standard_options,#wallet_options,#common_options").multiselect('updateButtonText');

    $(".multiselect-all input[type=\"checkbox\"]").on("click",function(){
        if($(this).is(':checked'))
        {
            $('.check_fields').show();
        } else {
            $('.check_fields').hide();
        }

    });
});

function checkDisplay(checked,opselected)
{
    var opchecked = checked;
    if(opselected == '1')
    {
        if(opchecked)
        {
            $('#stDate,.waDate,#vodate,#cmvodate,#cbvodate').show();
        } else {
            $('#stDate,.waDate,#vodate,#cmvodate,#cbvodate').hide();
        }
    } else if(opselected == '2')
    {
        if(opchecked)
        {
            $("#stPayeeName,.waPayeeName,#voPayeeName,#cmvoPayeeName,#cbvoPayeeName").show();
        } else {
            $('#stPayeeName,.waPayeeName,#voPayeeName,#cmvoPayeeName,#cbvoPayeeName').hide();
        }
    }else if(opselected == '3')
    {
        if(opchecked)
        {
            $("#stAmountText,.waAmountText,#voAmountText,#cmvoAmountText,#cbvoAmountText").show();
        } else {
            $('#stAmountText,.waAmountText,#voAmountText,#cmvoAmountText,#cbvoAmountText').hide();
        }
    }else if(opselected == '4')
    {
        if(opchecked)
        {
            $('#stAmountNumeric,.waAmountNumeric,#voAmountNumeric,#cmvoAmountNumeric,#cbvoAmountNumeric').show();
        } else {
            $('#stAmountNumeric,.waAmountNumeric,#voAmountNumeric,#cmvoAmountNumeric,#cbvoAmountNumeric').hide();
        }
    }else if(opselected == '5')
    {
        if(opchecked)
        {
            $('#stSignature,.waSignature,#voSignature,#cmvoSignature,#cbvoSignature').show();
        } else {
            $('#stSignature,.waSignature,#voSignature,#cmvoSignature,#cbvoSignature').hide();
        }
    }else if(opselected == '6')
    {
        if(opchecked)
        {
            $('#stPayeeNameAddress,.waPayeeNameAddress').show();
        } else {
            $('#stPayeeNameAddress,.waPayeeNameAddress').hide();
        }
    }else if(opselected == '7')
    {
        if(opchecked)
        {
            $('#stMemoText,.waMemotext').show();
        } else {
            $('#stMemoText,.waMemotext').hide();
        }
    }else if(opselected == '8')
    {
        if(opchecked)
        {
            $('.waAccountPayable').show();
        } else {
            $('.waAccountPayable').hide();
        }
    }else if(opselected == '9')
    {
        if(opchecked)
        {
            $('.waAccounts').show();
        } else {
            $('.waAccounts').hide();
        }
    }
    // else {
    //     $('.waAccounts').hide();
    //     $('.waAccountPayable').hide();
    //     $('#stMemoText,.waMemotext').hide();
    //     $('#stPayeeNameAddress,.waPayeeNameAddress').hide();
    //     $('#stSignature,.waSignature,#voSignature,#cmvoSignature,#cbvoSignature').hide();
    //     $('#stAmountNumeric,.waAmountNumeric,#voAmountNumeric,#cmvoAmountNumeric,#cbvoAmountNumeric').hide();
    //     $('#stAmountText,.waAmountText,#voAmountText,#cmvoAmountText,#cbvoAmountText').hide();
    //     $('#stPayeeName,.waPayeeName,#voPayeeName,#cmvoPayeeName,#cbvoPayeeName').hide();
    //     $('#stDate,.waDate,#vodate,#cmvodate,#cbvodate').hide();    }
}

$(document).on('submit', '#ChequeForm', function(event){
    event.preventDefault();
    var cheque_type = $('#check_type').val();
    var standard_options = $('#standard_options').val();
    var wallet_options = $('#wallet_options').val();
    var common_options = $('#common_options').val();
    var cheque_element;

    if(cheque_type == 1){
        cheque_element = standard_options;
    } else if (cheque_type == 2)
    {
        cheque_element = wallet_options;
    } else {
        cheque_element = common_options;
    }
    // var cheque_element = $('#check_type').val();

    $.ajax({
        type: "POST",
        url: "/CheckSetupAjax",
        data: {
            "cheque_type": cheque_type,
            "cheque_element": cheque_element,
            "class": 'checkSetupDetail',
            "action": 'insert'
        },
        success: function (res) {
            var response = JSON.parse(res);
            if(response.status == 'success' && response.code == 200){
                toastr.success('The Check Set-Up has been customized.');
            } else {
                toastr.warning('RCheck Set-Up Failed due to some technical error. Please try later.');
            }

        }
    });
});

$(document).ready(function ()
{
    $.ajax({
        type: "POST",
        url: "/CheckSetupAjax",
        data: {
            "class": 'checkSetupDetail',
            "action": 'getChequeElement'
        },
        success: function (res) {
            var res = JSON.parse(res);
            $("#standard_options,#wallet_options,#common_options").val(res.elements);
            $("#standard_options,#wallet_options,#common_options").multiselect("refresh");
            $('#check_type').val(res.type);
            ChequeImage(res.type);
                $('.waAccounts').hide();
                $('.waAccountPayable').hide();
                $('#stMemoText,.waMemotext').hide();
                $('#stPayeeNameAddress,.waPayeeNameAddress').hide();
                $('#stSignature,.waSignature,#voSignature,#cmvoSignature,#cbvoSignature').hide();
                $('#stAmountNumeric,.waAmountNumeric,#voAmountNumeric,#cmvoAmountNumeric,#cbvoAmountNumeric').hide();
                $('#stAmountText,.waAmountText,#voAmountText,#cmvoAmountText,#cbvoAmountText').hide();
                $('#stPayeeName,.waPayeeName,#voPayeeName,#cmvoPayeeName,#cbvoPayeeName').hide();
                $('#stDate,.waDate,#vodate,#cmvodate,#cbvodate').hide();
            $(res.elements).each(function (key,value) {
                checkDisplay(true,value);
            });

        }
    });
});