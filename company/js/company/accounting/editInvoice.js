$(document).ready(function () {
    var id = getParameterByName('id');
    //$("#invoice_table").hide();

    /*On clik show tenant combogrid */
    $(document).on('click','#_easyui_textbox_input1',function(){
        $('.all_tenants').combogrid('showPanel');
    });

    $('.all_tenants').combogrid({
        placeholder: "Select...",
        panelWidth: 500,
        url: '/Accounting/invoice-ajax',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getAllTenants',
            class: 'Invoice'
        },
        columns: [[
            {field: 'id', hidden: true},
            {field: 'name', title: 'Tenant Name', width: 120},
            {field: 'floor_no', title: 'Unit Id', width: 120},
            {field: 'property_id', title: 'Property Id', width: 300},
            {field: 'property_name', title: 'Property Name', width: 200},
            {field: 'address1', hidden: true},
            {field: 'address2', hidden: true},
            {field: 'address3', hidden: true},
            {field: 'city', hidden: true},
            {field: 'country', hidden: true},
            {field: 'state', hidden: true},
            {field: 'zipcode', hidden: true},
        ]],
        onSelect: function (index, row) {
            var user_id =  row.id;
            var address1     = (row.address1 != null )? row.address1 : '';  // the users address1
            var address2     =  (row.address2 != null )? row.address2 : '';  // the users address2
            var address3     = (row.address3 != null )? row.address3 : '';  // the users address3
            var city         =   (row.city != null )? row.city : '';    // the users address3
            var country     = (row.country != null )? row.country : '';   // the users country
            var state     =(row.state != null )? row.state : '';  // the users state
            var zipcode     = (row.zipcode != null )? row.zipcode : '';  // the users state
            var property_name = row.property_name;
            var floor_no = row.floor_no;
            if(user_id != ''){
                $("#user_id").val(user_id)
            }
            fulladdress = '';

            if(address1 !=''){
                fulladdress = address1+"\n"
            }
            if(address2 != ''){
                fulladdress = fulladdress + address2 +"\n"
            }
            if(address3 != ''){
                fulladdress = fulladdress + address3 +"\n"
            }
            if(city != ''){
                fulladdress = fulladdress + city
            }

            if(state != ''){
                fulladdress = fulladdress + " ,"+ state
            }
            if(zipcode != ''){
                fulladdress = fulladdress + " "+ zipcode
            }
            $("#address").html(fulladdress);
            $(".property").val(property_name);
            $(".unit").val(floor_no);
            $("#invoice_table").show();
        }
    });

    /*On clik show owner combogrid */
    $(document).on('click','#_easyui_textbox_input2',function(){
        $('.all_owners').combogrid('showPanel');
    });

    $('.all_owners').combogrid({
        placeholder: "Select...",
        panelWidth: 500,
        url: '/Accounting/invoice-ajax',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getAllOwners',
            class: 'Invoice'
        },
        columns: [[
            {field: 'id', hidden: true},
            {field: 'name', title: 'Owner Name', width: 120},
            {field: 'property_id', title: 'Property Id', width: 300},
            {field: 'property_name', title: 'Property Name', width: 200},
        ]],
        onSelect: function (index, row) {

            var user_id =  row.id;
            var address1     = (row.address1 != null )? row.address1 : '';  // the users address1
            var address2     =  (row.address2 != null )? row.address2 : '';  // the users address2
            var address3     = (row.address3 != null )? row.address3 : '';  // the users address3
            var city         =   (row.city != null )? row.city : '';    // the users address3
            var country     = (row.country != null )? row.country : '';   // the users country
            var state     =(row.state != null )? row.state : '';  // the users state
            var zipcode     = (row.zipcode != null )? row.zipcode : '';  // the users state
            var property_name = row.property_name;
            var floor_no = row.floor_no;
            if(user_id != ''){
                $("#user_id").val(user_id)
            }
            fulladdress = '';

            if(address1 !=''){
                fulladdress = address1+"\n"
            }
            if(address2 != ''){
                fulladdress = fulladdress + address2 +"\n"
            }
            if(address3 != ''){
                fulladdress = fulladdress + address3 +"\n"
            }
            if(city != ''){
                fulladdress = fulladdress + city
            }

            if(state != ''){
                fulladdress = fulladdress + " ,"+ state
            }
            if(zipcode != ''){
                fulladdress = fulladdress + " "+ zipcode
            }
            $("#address").html(fulladdress);
            $(".property").val(property_name);
            $(".unit").val(floor_no);
            $("#invoice_table").show();
        }
    });

    /* Show hide combogrid on selcting radio buttons*/
    $(".all_owners_combogrid, .all_others_combogrid").hide();
    $(document).on("click","input[name='user_radio']",function(e){
        var radioValue = $("input[name='user_radio']:checked").val();
        if(radioValue == "tenat_radio"){
            $("#address").prop("disabled", true);
            $(".all_tenants_combogrid").show();
            $(".all_owners_combogrid").hide();
            $(".all_others_combogrid").hide();

            $("#address").html("");
            $("#_easyui_textbox_input1").val("");
            $("#_easyui_textbox_input2").val("");
            $('#invoice_table input[type="text"]').val('');
        }else if(radioValue == "owner_radio"){
            $("#address").prop("disabled", true);
            $(".all_owners_combogrid").show();
            $(".all_tenants_combogrid").hide();
            $(".all_others_combogrid").hide();
            $("#address").html("");
            $("#invoice_table").hide();
            $("#_easyui_textbox_input1").val("");
            $("#_easyui_textbox_input2").val("");
            $('#invoice_table input[type="text"]').val('');
        }else if(radioValue == "other_radio"){
            $("#address").prop("disabled", false);
            $(".all_others_combogrid").show();
            $(".all_tenants_combogrid").hide();
            $(".all_owners_combogrid").hide();
            $("#address").html("");
            $("#invoice_table").show();
            $("#_easyui_textbox_input1").val("");
            $("#_easyui_textbox_input2").val("");
            $('#invoice_table input[type="text"]').val('');

        }

    })

    /* Combogrid Place holder for owner and tenant */
    $("#_easyui_textbox_input2").attr("placeholder", "Click here to select a owner");
    $("#_easyui_textbox_input1").attr("placeholder", "Click here to select a Tenant");


    $(document).on("click",".additional-add-invoice",function(){

        var clone = $(".additional-invoice:first").clone();
         clone.find('.description,.money ').val('');
        $(".additional-invoice").last().after(clone);
        clone.find(".additional-add-invoice").hide();
        clone.find(".additional-remove-invoice").show();
        new AutoNumeric.multiple('.money', {
            allowDecimalPadding: true,
            maximumValue  : '9999999999',
        });


    });


    $(document).on("click",".additional-remove-invoice",function(){
        calculateSum();
        var count = $('.charge_data_html tr').length;
     if(count > 1) {
          $(this).parents(".additional-invoice").remove();
     }else{
         toastr.error("Atleast one charge code is required to generate invoice.");
     }
    });


    $("#addInvoice").validate({
        rules: {

        }
    });

    /*submit halder for add employee

     */
    $("#updateInvoice").on('submit',function(event){
        event.preventDefault();
        $(".customadditionalEmailValidation").each(function () {
            res = validations(this);
        });
        $(".customValidationCarrier").each(function () {
            res = validations(this);
        });

        $(".customPhonenumbervalidations").each(function () {
            res = validations(this);
        });
        $(".customOtherPhonevalidations").each(function () {
            res = validations(this);
        });
        if($("#updateInvoice").valid()){
            var user_id = $("#user_id").val()
            var form = $('#updateInvoice')[0];
            var formData = new FormData(form);
            formData.append('action','update');
            formData.append('class','Invoice');
            formData.append('user_id',user_id);
            action = 'update';
            $.ajax({
                url:'/Accounting/invoice-ajax',
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function (xhr) {
                    var Charge = true;
                    var Amount = true;
                    // checking portfolio validations
                    $(".customChargeValidation").each(function() {
                        Charge = validations(this);

                    });
                    $(".customAmountvalidations").each(function() {
                        Amount = validations(this);

                    });
                    if (Charge === false || Amount === false) {
                        xhr.abort();
                        return false;
                    }

                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == "success"){
                        localStorage.setItem("Message", response.message);
                        localStorage.setItem("rowcolor",true)
                        window.location.href = window.location.origin+'/Accounting/ConsolidatedInvoice';
                    }else{
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }

            });
        }
    });




    /*custom validation for carrier
     */
    $(document).on('change','.customChargeValidation',function(){
        validations(this);
    });

    /*custom validation for additional phone
     */
    $(document).on('keyup','.customAmountvalidations',function(){
        validations(this);
    });

    /* jquery for phone number format

     */
   // jQuery('.phone_format').mask('000-000-0000', {reverse: true});




    $(document).on("change",".customChargeValidation", function(e){
        var charge_descrption_value = $(this ).find('option:selected').text();
        var charge_descrption = charge_descrption_value.split("-");
        if(charge_descrption != '')
            $(this).parents("td").next().find("input").val(charge_descrption[1].trim())
    });

    $.ajax({
        type: 'get',
        url: '/Accounting/invoice-ajax',
        data: {
            id: id,
            class: 'Invoice',
            action: 'Edit'},
        success: function (response) {
            var response = JSON.parse(response);
            console.log(response);
            if (response.code == 200) {
                $("#invoice_id").val(response.data.id);
                if(response.data.email_invoice==1){

                    $("input[name='email_invoice']").attr("checked",true);
                }else{
                    $("input[name='email_invoice']").attr("checked",false);
                }

                if(response.data.no_late_fee==1){
                    $("input[name='no_late_fee']").attr("checked",true);
                }else{
                    $("input[name='no_late_fee']").attr("checked",false);
                }

                if(response.data.user_type == 2 || response.data.user_type == 4){
                    var full_address = getAddress(response.data);
                    $("#invoice_date").datepicker({dateFormat: jsDateFomat ,onSelect: function (selectedDate) {
                            $("#late_date").datepicker("option", "minDate", selectedDate);
                        }}).datepicker("setDate", response.data.invoice_date);
                    $("#late_date").datepicker({
                    dateFormat: jsDateFomat,
                        changeYear: true,
                        yearRange: "-100:+20",
                        changeMonth: true,
                        minDate:  response.data.invoice_date
                    }).datepicker("setDate", response.data.late_date);
                    $('.address').html(full_address);

                }
                if(response.data.user_type == 2){
                    $(".tenant_radio").attr("checked",true);
                    $(".owner_radio").attr("disabled",true);
                    $(".other_radio").attr("disabled",true);
                    $("#_easyui_textbox_input1").attr("disabled",true);
                    $('.all_tenants').combogrid('setValue',response.data.invoice_to);
                    $("#user_id").val(response.data.invoice_to_id);
                    $("#address").prop("disabled", true);
                    $(".all_tenants_combogrid").show();
                    $(".all_owners_combogrid").hide();
                    $(".all_others_combogrid").hide();


                }else if(response.data.user_type == 4){

                    $(".owner_radio").attr("checked",true);
                    $(".other_radio").attr("disabled",true);
                    $(".tenant_radio").attr("disabled",true);
                    $("#_easyui_textbox_input2").attr("disabled",true);
                    $('.all_owners').combogrid('setValue',response.data.invoice_to);
                    $("#user_id").val(response.data.invoice_to_id);
                    $("#address").prop("disabled", true);
                    $(".all_owners_combogrid").show();
                    $(".all_tenants_combogrid").hide();
                    $(".all_others_combogrid").hide();
                }else if(response.data.user_type == 0){
                    $("#other_name").val(response.data.other_name);
                    $(".tenant_radio").attr("disabled",true);
                    $(".owner_radio").attr("disabled",true);
                    $(".other_radio").attr("checked",true);
                    $("#address").prop("disabled", false);
                    $(".all_others_combogrid").show();
                    $(".all_tenants_combogrid").hide();
                    $(".all_owners_combogrid").hide();
                    $("#address").val(response.data.other_address);


                    $("#invoice_date").datepicker({dateFormat: jsDateFomat ,onSelect: function (selectedDate) {
                            $("#late_date").datepicker("option", "minDate", selectedDate);
                        }}).datepicker("setDate", response.data.invoice_date);
                    $("#late_date").datepicker({
                        dateFormat: jsDateFomat,
                        changeYear: true,
                        yearRange: "-100:+20",
                        changeMonth: true,
                        minDate:  response.data.invoice_date
                    }).datepicker("setDate", response.data.late_date);
                }
                var charge_data = response.charge_data;
                var charge_data_html ='';
                $.each(charge_data,function (key, value){
                    setTimeout(function(){
                    $('.new_charge_code_selected_value_'+key).val(value.charge_id)

                    }, 2000);
                    charge_data_html += '<tr class="additional-invoice">';
                    charge_data_html += '<td><input class="form-control property" disabled="" placeholder="" er="" value="'+response.data.property_name+'" type="text"></td>';
                    charge_data_html += ' <td><input class="form-control unit" disabled="" placeholder="" value="'+response.data.unit+'" type="text"></td>';
                    charge_data_html += '<td><select class="form-control charge customChargeValidation new_chargeCode_select new_charge_code_selected_value_'+key+'" id="new_chargeCode_select" data_required="true"  name="charge_code[]" ><option>Select</option></select> <span class="error red-star customError"></span></td>';
                    charge_data_html += '<td><input class="form-control description" name="description[]"  disabled="" placeholder="" type="text" value="'+value.description+'"></td>';
                    charge_data_html +=  '<td><input class="form-control add-input customAmountvalidations money" placeholder="" type="text" data_required="true" name="amount[]" value="'+value.amount+'"><i class="fa fa-times-circle red-star additional-remove-invoice"  aria-hidden="true"></i> <span class="error red-star customError"></span> </input></td>'
                    charge_data_html += '</tr>';
                });
                setTimeout(function(){
                    calculateSum();
                    if(response.data.user_type == 0) {
                        $(".property").val('');
                        $(".unit").val('');
                    }
                }, 2000);
                setTimeout(function(){
                    $(".charge_data_html").html(charge_data_html);
                }, 400);




            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });


    $(document).on("blur",".money",function(){
        calculateSum();

    });

    function calculateSum() {

        var sum = 0;
        //iterate through each textboxes and add the values
        $(".money").each(function() {

            //add only if the value is number
           var amount_value =  this.value.replace(/,/g, '');
            if(!isNaN(amount_value) && amount_value!=0) {
                sum += parseFloat(amount_value);
            }

        });
        //.toFixed() method will roundoff the final sum to 2 decimal places
        $(".total_charges_amount").html(sum.toFixed(2));
    }

    /**
     * Get Parameters by id
     * @param status
     */
    function getParameterByName(name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

    function getAddress(data){

        var address1     = (data.address1 != null )? data.address1 : '';  // the users address1
        var address2     =  (data.address2 != null )? data.address2 : '';  // the users address2
        var address3     = (data.address3 != null )? data.address3 : '';  // the users address3
        var city         =   (data.city != null )? data.city : '';    // the users address3
        // var country     = (data.country != null )? data.country : '';   // the users country
        var state     =(data.state != null )? data.state : '';  // the users state
        var zipcode     = (data.zipcode != null )? data.zipcode : '';  // the users state

        fulladdress = '';

        if(address1 !=''){
            fulladdress = address1+"\n"
        }
        if(address2 != ''){
            fulladdress = fulladdress + address2 +"\n"
        }
        if(address3 != ''){
            fulladdress = fulladdress + address3 +"\n"
        }
        if(city != ''){
            fulladdress = fulladdress + city
        }

        if(state != ''){
            fulladdress = fulladdress + " ,"+ state
        }
        if(zipcode != ''){
            fulladdress = fulladdress + " "+ zipcode
        }

        return fulladdress;
    }


    /*Ajax for charge code listing*/
    $.ajax({
        type: 'post',
        url: '/Accounting/invoice-ajax',
        data: {
            class: "Invoice",
            action: "getCharges"
        },
        success: function (response) {

            var data = $.parseJSON(response);
            if (data.status == "success") {
                var charges = data.data.charges;

                if (charges.length > 0){
                    var chargesOptions = '<option value="">Select</option>';
                    for (var i = 0; i < charges.length; i++){
                        chargesOptions += "<option value='"+charges[i].id+"'>"+charges[i].charge_code+' - '+charges[i].description+"</option>";
                    }
                    setTimeout(function(){
                        $(".new_chargeCode_select").html(chargesOptions);
                    }, 1000);
                }

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

    $(document).on("click",".cancel",function() {
        bootbox.confirm({
            message: "Do you want to cancel this invoice ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    window.location.href = base_url+'/Accounting/ConsolidatedInvoice';
                }

            }
        });
    });




});

