$(document).ready(function () {

    $("#unit_type").keypress(function(e){
        var keyCode = e.which;
        /*
          8 - (backspace)
          32 - (space)
          48-57 - (0-9)Numbers
        */
        /* Not allow special*/
        if ( !( (keyCode >= 48 && keyCode <= 57)
            ||(keyCode >= 65 && keyCode <= 90)
            || (keyCode >= 97 && keyCode <= 122) )
            && keyCode != 8 && keyCode != 32) {
            e.preventDefault();
        }
    });
    var base_url = window.location.origin;

    var status =  localStorage.getItem("active_inactive_status");
    console.log(status);
    if(status !== undefined) {
        if ($("#chargeCodePreference-table")[0].grid) {
            $('#chargeCodePreference-table').jqGrid('GridUnload');
        }
        /*intializing jqGrid*/
        if(status == 'all'){
            jqGrid('All');
        }  else {
            jqGrid(status);
        }
        $('#jqGridStatus option[value='+status+']').attr("selected", "selected");

    }else{
        if ($("#chargeCodePreference-table")[0].grid) {
            $('#chargeCodePreference-table').jqGrid('GridUnload');
        }
        jqGrid('All');
    }


    /**
     * Unit type list
     */
    function unitTypeList( ){
        $.ajax({
            type: 'post',
            url: '/ChargeCodePreferences-Ajax',
            data: {
                class: 'ChargeCodePreferencesAjax',
                action: 'getAllUnittypeList',
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#unit_type_id').empty().append('Select').val('');
                    $.each(response.data, function (key, value) {
                        $('#unit_type_id').append($("<option value = "+value.id+">"+value.unit_type+"</option>"));
                        $('#unit_type_id').multiselect('destroy').multiselect({
                            includeSelectAllOption: true,
                            nonSelectedText: 'Select Unit'
                        });
                    });

                    return response;
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }
    /**
     * cahrgecode list
     */
    function chargeCodePrefList(){
        $.ajax({
            type: 'post',
            url: '/ChargeCodePreferences-Ajax',
            data: {
                class: 'ChargeCodePreferencesAjax',
                action: 'getAllChargeCodeList',
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#charge_code').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        if(value.is_default == 1){
                            $('#charge_code').append($("<option value = "+value.id+" selected>"+value.charge_code+' - '+value.description+"</option>"));
                        } else {
                            $('#charge_code').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                        }
                    })
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }



    /** Hide add new charge code preferences type div on cancel button click */
    $(document).on("click", "#add_chargeCode_cancelbtn", function (e) {
        e.preventDefault();
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#add_charge_codepref_div").hide(500);
                }
            }
        });
    });



    /** jqGrid status change*/
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        $('#chargeCodePreference-table').jqGrid('GridUnload');
        $('#add_charge_codepref_div').hide(500);
        changeGridStatus(selected);
        jqGrid(selected, true);
    });

    $(document).on('change','.common_ddl',function(){
        var grid = $("#chargeCodePreference-table"),f = [];
        var value = $(this).attr('data_id');
        var type = $('#applicable_to_search_ddl').val();
        var frequency = $('#frequency_search_ddl').val();
        var status = $('#status_search_ddl').val();
        f.push({field: "charge_code_type", op: "eq", data: type},{field: "frequency", op: "eq", data: frequency},{field: "status", op: "eq", data: status});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    });

    /**
     * jqGrid Initialization function
     * @param status
     */
    function jqGrid(status, deleted_at) {
        var table = 'company_accounting_charge_code_preferences';
        var columns = ['Charge Code','Applicable', 'UnitTypeId','Set as Default', 'Frequency', 'Status', 'Is Editable', 'Action'];
        var select_column = ['Edit','Deactivate','Delete'];
        var joins = [{table:'company_accounting_charge_code_preferences',column:'charge_code',primary:'id',on_table:'company_accounting_charge_code'}, {table:'company_accounting_charge_code_preferences',column:'unit_type_id',primary:'id',on_table:'company_unit_type'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['company_accounting_charge_code_preferences.status', 'company_accounting_charge_code_preferences.deleted_at', 'company_accounting_charge_code_preferences.updated_at'];
        var extra_where = [];
        var columns_options = [
            {name:'Charge Code',index:'charge_code', width:90,align:"center",searchoptions: {sopt: conditions},table:'company_accounting_charge_code'},
            {name:'Applicable',index:'charge_code_type', width:100,align:"center",searchoptions: {sopt: conditions},table:table, formatter:applicableFormatter},
            {name:'UnitTypeId',index:'unit_type', width:100,align:"center", hidden: true, searchoptions: {sopt: conditions},table:'company_unit_type'},
            {name:'Set as Default',index:'is_default', width:100,align:"center", hidden: true, searchoptions: {sopt: conditions},table:table},
            {name:'Frequency',index:'frequency', width:100,align:"center",searchoptions: {sopt: conditions},table:table, formatter:frequencyFormatter},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Is Editable',index:'is_editable', hidden:true, width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Action',index:'select', title: false, width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFormatter}
        ];
        var ignore_array = [];
        jQuery("#chargeCodePreference-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_accounting_charge_code_preferences",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Charge Code Preferences",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {top:200,left:200,drag:true,resize:false}
        );
    }

    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Active";
        else if(cellValue == '0')
            return "InActive";
        else
            return '';
    }

    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function frequencyFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "One Time";
        else if(cellValue == 2)
            return "Monthly";
        else
            return '';
    }


    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function applicableFormatter (cellValue, options, rowObject){
        if (cellValue == 1){
            var unitType = rowObject;
            var unitTypeName = unitType.UnitTypeId;
            unitTypeName = "Unit - "+unitTypeName;
            return unitTypeName;
        } else if(cellValue == '0'){
            return "Property";
        } else {
            return '';
        }
    }

    /**
     * jqGrid function to format action column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function actionFormatter (cellValue, options, rowObject){
        if(rowObject !== undefined) {
            var is_default = rowObject;
            var is_editable = is_default['Is Editable'];
            console.log('is_editable>>', is_editable);
            var editable = $(cellValue).attr('editable');
            var select = '';
            if(rowObject.Status == 1)  select = ['Edit','Deactivate','Delete'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete'];
            if(is_editable == '0' || rowObject.Status == '')  select = '';
            var data = '';
            if(select != '') {
                var data = '<select ' +
                    ' class="form-control select_options" status="' + is_default['Set as Default'] + '" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    if(editable == '0' && (val == 'delete' || val == 'Delete')){
                        return true;
                    }
                    if(is_editable == '0'){
                        return true;
                    }
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    /** Show add new charge code preferences div on add new button click */
    $(document).on('click','#addchargecodeprefButton',function () {
        $('#charge_code').val('');
        $("#frequency").val('');
        $("#type").val('');
        $("#charge_code-error").text('');
        $("#frequency-error").text('');
        $("#type-error").text('');
        $("#chargeCodeErr").text('');
        $("#frequencyErr").text('');
        $("#typeErr").text('');
        $("#selectProperty").prop("checked", true);
        $("#selectUnit").prop("checked", false);
        $("#charge_code_pref_id").val('');
        $('.property_ddl').show();
        $('.unit_ddl').hide();

        chargeCodePrefList();
        headerDiv.innerText = "Add Charge Code Preferences";
        $('#is_default').prop('checked', false);
        $('#saveBtnId').val('Save');
        $('#add_charge_codepref_div').show(500);
    });

    $('input[name=charge_code_type]').on('change', function(){
        var value = $( 'input[name=charge_code_type]:checked' ).val();
        if (value == 1){
            $('.property_ddl').hide();
            $('.unit_ddl').show();
            $('#type').val('0');
            $('#charge_code').val('');
            $('#frequency').val('');
            $('#unit_type_id-error').text('');
            unitTypeList();
        } else {
            $('#unit_type_id').val('0');
            $('.property_ddl').show();
            $('.unit_ddl').hide();
            $("#type").val('');
            $('#charge_code').val('');
            $('#frequency').val('');
            $('#type-error').text('');
        }
    });

    /** Add/Edit new AccountChargeCode */
    $("#chargecodepref").validate({
        rules: {
            charge_code: {
                required: true,
            },
            frequency: {
                required:true
            },
            type: {
                required:true
            },
            "unit_type_id[]": {
                required:true
            },
            status: {
                required:true
            }
        },
        submitHandler: function () {
            var charge_code_type = $( 'input[name=charge_code_type]:checked' ).val();
            var charge_code = $('#charge_code').val();
            var frequency = $('#frequency').val();
            var type = $('#type').val();
            var status = $("#status").val();
            var charge_code_pref_id = $("#charge_code_pref_id").val();

            var unit_type_id=[];
            $('#unit_type_id :selected').each(function(){
                unit_type_id.push($(this).val());
            });

            if(charge_code_type == 1){
                type = '';
            } else {
                unit_type_id = '';
            }

            var is_default;
            if ($('#is_default').is(":checked")) {
                is_default = '1';
            } else {
                is_default = '0';
            }
            // console.log('unit_type_id>>>', unit_type_id); return
            var formData = {
                'charge_code': charge_code,
                'charge_code_type': charge_code_type,
                'frequency': frequency,
                'type': type,
                'is_default': is_default,
                'status':status,
                'unit_type_id':unit_type_id,
                'charge_code_pref_id': charge_code_pref_id
            };

            var action;
            if (charge_code_pref_id) {
                action = 'update';
            } else {
                action = 'insert';
            }
            $.ajax({
                type: 'post',
                url: '/ChargeCodePreferences-Ajax',
                data: {
                    class: 'ChargeCodePreferencesAjax',
                    action: action,
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        $("#add_charge_codepref_div").hide(500);
                        $("#chargeCodePreference-table").trigger('reloadGrid');
                        toastr.success(response.message);
                        onTop(true);
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('#' + key).text(value);
                        });
                    } else if (response.status == 'error' && response.code == 503) {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });

    /**  List Action Functions  */
    $(document).on('change', '.select_options', function() {
        setTimeout(function(){ $(".select_options").val("default"); }, 200);

        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index() ;
        var status = $(this).attr('status');
        if (opt == 'Edit' || opt == 'EDIT') {

            $("#add_charge_codepref_div").show(500);
            headerDiv.innerText = "Edit Charge Code Preferences";
            $('#saveBtnId').val('Update');
            $("#charge_code-error").text('');
            $("#frequency-error").text('');
            $("#type-error").text('');
            $("#chargeCodeErr").text('');
            $("#frequencyErr").text('');
            $("#typeErr").text('');
            chargeCodePrefList();

            $('#unit_typeErr').text('');
            $('.table').find('.green_row_left, .green_row_right').each(function(){
                $(this).removeClass("green_row_left green_row_right");
            });
            $('.table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
            $('.table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
            $(".clear-btn").text('Reset');
            $(".clear-btn").addClass('formreset');
            $(".clear-btn").removeClass('clearFormReset');
            $.ajax
            ({
                type: 'post',
                url: '/ChargeCodePreferences-Ajax',
                data: {
                    class: "ChargeCodePreferencesAjax",
                    action: "view",
                    id : id,
                },
                success: function (response) {
                    var data = $.parseJSON(response);
                    if (data.status == "success")
                    {
                        $("#charge_code").val(data.data.charge_code);
                        $("#charge_code_type").val(data.data.charge_code_type);
                        $("#frequency").val(data.data.frequency);
                        $("#type").val(data.data.type);
                        $("#charge_code_pref_id").val(data.data.id);

                        if (data.data.charge_code_type == 1){
                            $("#selectProperty").prop("checked", false);
                            $("#selectUnit").prop("checked", true);
                            $('.property_ddl').hide();
                            $('.unit_ddl').show();
                        } else {
                            $("#selectProperty").prop("checked", true);
                            $("#selectUnit").prop("checked", false);
                            $('.property_ddl').show();
                            $('.unit_ddl').hide();
                        }
                        if (data.data.is_default == 1) {
                            $('#is_default').prop('checked', true);
                        } else {
                            $('#is_default').prop('checked', false);
                        }
                        if (data.AllChargeCode.length != 0){
                            $.ajax({
                                type: 'post',
                                url: '/ChargeCodePreferences-Ajax',
                                data: {
                                    class: 'ChargeCodePreferencesAjax',
                                    action: 'getAllUnittypeList',
                                },
                                success: function (response) {
                                    var response = JSON.parse(response);

                                    if (response.status == 'success' && response.code == 200) {
                                        var unitTypeHtml = "";
                                        var selected_id = [];
                                        $.each(data.AllChargeCode, function (key1, val1) {
                                            selected_id.push(val1.unit_type_id);
                                        });

                                        selected_id = selected_id.map(Number);
                                        $.each(response.data, function (key, val) {
                                            if($.inArray( val.id, selected_id ) !== -1){
                                                unitTypeHtml += "<option value='" + val.id + "' selected>" + val.unit_type + "</option>";
                                            } else {
                                                unitTypeHtml += "<option value='" + val.id + "'>" + val.unit_type + "</option>";
                                            }
                                        });
                                        $("#unit_type_id").multiselect('destroy');
                                        $("#unit_type_id").html(unitTypeHtml);
                                        $("#unit_type_id").multiselect('refresh');
                                    }
                                }
                            });
                        }
                        defaultFormData = $('#chargecodepref').serializeArray();
                    }else if(data.status == "error"){
                        toastr.error(data.message);
                    } else{
                        toastr.error(data.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });


        } else if (opt == 'Deactivate' || opt == 'DEACTIVATE' || opt == 'Activate' || opt == 'ACTIVATE') {
            opt = opt.toLowerCase();
            bootbox.confirm({
                message: "Do you want to " + opt + " this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        var status = opt == 'activate' ? '1' : '0';
                        $.ajax({
                            type: 'post',
                            url: '/ChargeCodePreferences-Ajax',
                            data: {
                                class: 'ChargeCodePreferencesAjax',
                                action: 'updateStatus',
                                status: status,
                                id: id
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    $("#chargeCodePreference-table").trigger('reloadGrid');
                                    toastr.success(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                }
            });
        } else if (opt == 'Delete' || opt == 'DELETE') {
            opt = opt.toLowerCase();
            console.log('status>>', status);
            if (status == '1') {
                toastr.warning('A default set value cannot be deleted.');
            } else {
                bootbox.confirm({
                    message: "Do you want to delete this record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/ChargeCodePreferences-Ajax',
                                data: {
                                    class: 'ChargeCodePreferencesAjax',
                                    action: 'delete',
                                    id: id
                                },
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        $('#chargeCodePreference-table').trigger('reloadGrid');
                                        toastr.success(response.message);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    });




});