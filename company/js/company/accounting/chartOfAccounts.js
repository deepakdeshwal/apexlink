$(document).ready(function () {

    var base_url = window.location.origin;

    var status =  localStorage.getItem("active_inactive_status");
    if(status !== undefined) {
        if ($("#ChartOfAccount-table")[0].grid) {
            $('#ChartOfAccount-table').jqGrid('GridUnload');
        }
        /*intializing jqGrid*/
        if(status == 'all'){
            jqGrid('All');
        }  else {
            jqGrid(status);
        }
        $('#jqGridStatus option[value='+status+']').attr("selected", "selected");

    }else{
        if ($("#ChartOfAccount-table")[0].grid) {
            $('#ChartOfAccount-table').jqGrid('GridUnload');
        }
        jqGrid('All');
    }

    /** Show add new chart of account div on add new button click */
    $(document).on('click','#addChartOfAccountButton',function () {
        var validator = $( "#add_chart_account_form_id" ).validate();
        validator.resetForm();
        $("#chart_account_edit_id").val('');

        $("#add_chart_account_form_id")[0].reset();
        $("#status").val('1');
        $("#posting_status").val('1');

        $('.error').text('');

        headerDiv.innerText = "Add Chart of Account";
        $('#is_default').prop('checked', false);
        accountTypeList();
        $('#saveBtnId').val('Save');
        $('#add_chart_account_div').show(500);
    });

    function accountTypeList(){
        $.ajax({
            type: 'post',
            url: '/AccountType-Ajax',
            data: {
                class: 'AccountTypeAjax',
                action: 'getAllAccountType',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#account_type_id').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#account_type_id').append($("<option data-rangefrom="+value.range_from+" data-rangeto ="+value.range_to+" value = "+value.id+">"+value.account_type_name+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    function accountSubTypeList(accountSubType){
        $.ajax({
            type: 'post',
            url: '/AccountType-Ajax',
            data: {
                class: 'AccountTypeAjax',
                action: 'getAllAccountSubType',
                id: accountSubType,
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#sub_account').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#sub_account').append($("<option value = "+value.id+">"+value.account_sub_type+"</option>"));
                    })
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /** Show import excel div on import excel button click */
    $(document).on('click','#importChartOfAccountButton',function () {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $('#import_ChartOfAccount_div').show(500);
    });

    /** Hide add new chart of account div on cancel button click */
    $(document).on("click", "#add_chart_account_cancel_btn", function (e) {
        e.preventDefault();
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#add_chart_account_div").hide(500);
                }
            }
        });
    });

    /** Hide import excel div on cancel button click */
    $(document).on("click", "#import_ChartOfAccount_cancel_btn", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#import_ChartOfAccount_div").hide(500);
                }
            }
        });
    });

    /** jqGrid status change*/
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        $('#ChartOfAccount-table').jqGrid('GridUnload');
        $('#add_chart_account_div').hide(500);
        changeGridStatus(selected);
        jqGrid(selected, true);
    });

    /**
     * jqGrid Initialization function
     * @param status
     */
    function jqGrid(status, deleted_at) {
        var table = 'company_chart_of_accounts';
        var columns = ['Account Code','Account Name','Sub Account','Account Type','Set as Default', 'Is Posting', 'Status', 'IsEditable', 'Action'];
        var select_column = ['Edit','Deactivate','Delete'];
        var joins = [
                {table:'company_chart_of_accounts',column:'sub_account',primary:'id',on_table:'company_account_sub_type'},
                {table:'company_chart_of_accounts',column:'account_type_id',primary:'id',on_table:'company_account_type'}
            ];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['company_chart_of_accounts.status', 'company_chart_of_accounts.deleted_at','company_chart_of_accounts.updated_at'];
        var columns_options = [
            {name:'Account Code',index:'account_code', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Account Name',index:'account_name', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Sub Account',index:'account_sub_type', width:100,searchoptions: {sopt: conditions},table:'company_account_sub_type'},
            {name:'Account Type',index:'account_type_name', align:"center", width:100,searchoptions: {sopt: conditions},table:'company_account_type'},
            {name:'Set as Default',index:'is_default', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:isDefaultFormatter},
            {name:'Is Posting',index:'posting_status', width:100,align:"center",searchoptions: {sopt: conditions},table:table,formatter:isPostingFormatter},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'IsEditable',index:'is_editable',hidden:true, width:80,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFormatter}
        ];
        var ignore_array = [];
        jQuery("#ChartOfAccount-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_chart_of_accounts",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: 'desc',
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Chart of Accounts",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {},
            {},
            {},
            {top:200,left:200,drag:true,resize:false}
        );
    }

    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Active";
        else if(cellValue == '0')
            return "InActive";
        else
            return '';
    }

    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isDefaultFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Yes";
        else if(cellValue == '0')
            return "No";
        else
            return '';
    }

    /**
     * jqGrid function to format is_posting column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isPostingFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Posting";
        else if(cellValue == '0')
            return "Non Posting";
        else
            return '';
    }

    /**
     * jqGrid function to format action column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function actionFormatter (cellValue, options, rowObject){
        if(rowObject !== undefined) {
            var is_default = rowObject;
            var editable = $(cellValue).attr('editable');
            var select = '';
            if(rowObject.Status == 1)  select = ['Edit','Deactivate','Delete'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete'];
            var data = '';
            if(select != '') {
                var data = '<select ' +
                    ' class="form-control select_options" is_default ="'+is_default['Set as Default']+'" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    if(editable == '0' && (val == 'delete' || val == 'Delete')){
                        return true;
                    }
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    /** On change account type change sub account ddl **/
    $(document).on('change','#account_type_id',function () {
        var accountSubType = this.value;
        console.log('range>>>>', $('#account_type_id').find(':selected').attr('data-rangefrom'));
        accountSubTypeList(accountSubType);
    });

    /** Add/Edit new chart of account */
    $("#add_chart_account_form_id").validate({
        rules: {
            account_type_id: {
                required: true
            },
            account_code:{
                required: true,
                number: true,
            },
            account_name:{
                required: true
            },
            reporting_code:{
                required: true,
            }
        },
        messages: {
            account_code:{
                number: "* Please add numerical values only",
            }
        },
        submitHandler: function () {
            var account_type_id = $('#account_type_id').val();
            var account_code = $('#account_code').val();
            var account_name = $("#account_name").val();
            var reporting_code = $("#reporting_code").val();
            var sub_account = $("#sub_account").val();
            var status = $("#status").val();
            var range_from = $('#account_type_id').find(':selected').attr('data-rangefrom');
            var range_to = $('#account_type_id').find(':selected').attr('data-rangeto');
            var posting_status = $("#posting_status").val();
            var chart_account_edit_id = $("#chart_account_edit_id").val();
            var is_default;
            if ($('#is_default').is(":checked")) {
                is_default = '1';
            } else {
                is_default = '0';
            }
            var formData = {
                'account_type_id': account_type_id,
                'account_code': account_code,
                'account_name': account_name,
                'reporting_code':reporting_code,
                'sub_account':sub_account,
                'posting_status':posting_status,
                'is_default':is_default,
                'status':status,
                'range_from':range_from,
                'range_to':range_to,
                'chart_account_edit_id':chart_account_edit_id
            };
            var action;
            if (chart_account_edit_id) {
                action = 'update';
            } else {
                action = 'insert';
            }
            $.ajax({
                type: 'post',
                url: '/AddChartOfAccount-Ajax',
                data: {
                    class: 'ChartOfAccountAjax',
                    action: action,
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        $("#add_chart_account_div").hide(500);
                        $("#ChartOfAccount-table").trigger('reloadGrid');
                        toastr.success(response.message);
                        onTop(true);
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('#' + key).text(value);
                        });
                    } else if (response.status == 'error' && response.code == 503) {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });

    /**  List Action Functions  */
    $(document).on('change', '.select_options', function() {
        setTimeout(function(){ $(".select_options").val("default"); }, 200);

        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index() ;
        var status = $(this).attr('status');
        var is_default = $(this).attr('is_default');
        if (opt == 'Edit' || opt == 'EDIT') {

            $("#add_chart_account_div").show(500);
            accountTypeList();
            headerDiv.innerText = "Edit Chart of Account";
            $('#saveBtnId').val('Update');
            $('.error').text('');
            var validator = $( "#add_chart_account_form_id" ).validate();
            validator.resetForm();

            $('.table').find('.green_row_left, .green_row_right').each(function(){
                $(this).removeClass("green_row_left green_row_right");
            });
            $('.table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
            $('.table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
            $(".clear-btn").text('Reset');
            $(".clear-btn").addClass('formreset');
            $(".clear-btn").removeClass('clearFormReset');
            $.ajax({
                type: 'post',
                url: '/AddChartOfAccount-Ajax',
                data: {
                    class: "ChartOfAccountAjax",
                    action: "view",
                    id : id,
                },
                success: function (response) {
                    var data = JSON.parse(response);
                    if (data.status == "success")
                    {
                        accountSubTypeList(data.data.account_type_id);
                        $("#account_type_id").val(data.data.account_type_id);
                        $("#account_code").val(data.data.account_code);
                        $("#chart_account_edit_id").val(data.data.id);
                        $("#reporting_code").val(data.data.reporting_code);
                        $("#account_name").val(data.data.account_name);
                        $("#status").val(data.data.status);
                        $("#posting_status").val(data.data.posting_status);

                        if (data.data.is_default == 1) {
                            $('#is_default').prop('checked', true);
                        } else {
                            $('#is_default').prop('checked', false);
                        }

                        setTimeout(function(){ $("#sub_account").val(data.data.sub_account); }, 800);
                        defaultFormData = $('#add_chart_account_form_id').serializeArray();
                    }else if(data.status == "error"){
                        toastr.error(data.message);
                    } else{
                        toastr.error(data.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });


        } else if (opt == 'Deactivate' || opt == 'DEACTIVATE' || opt == 'Activate' || opt == 'ACTIVATE') {
            $('.table').find('.green_row_left, .green_row_right').each(function(){
                $(this).removeClass("green_row_left green_row_right");
            });
            opt = opt.toLowerCase();
            bootbox.confirm({
                message: "Do you want to " + opt + " this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        var status = opt == 'activate' ? '1' : '0';
                        $.ajax({
                            type: 'post',
                            url: '/AddChartOfAccount-Ajax',
                            data: {
                                class: 'ChartOfAccountAjax',
                                action: 'updateStatus',
                                status: status,
                                id: id
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                    $('#ChartOfAccount-table').trigger('reloadGrid');
                                    onTop(true);
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                }
            });
        } else if (opt == 'Delete' || opt == 'DELETE') {
            opt = opt.toLowerCase();
            if (is_default == '1') {
                toastr.warning('A default set value cannot be deleted.');
            } else {
                bootbox.confirm({
                    message: "Do you want to delete this record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/AddChartOfAccount-Ajax',
                                data: {
                                    class: 'ChartOfAccountAjax',
                                    action: 'delete',
                                    id: id
                                },
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                        $('#ChartOfAccount-table').trigger('reloadGrid');
                                    } else {
                                        toastr.error(response.message);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    });

    /** Export sample chart of account excel */
    $(document).on("click",'#exportSampleExcel',function(){
        window.location.href = base_url+"/AddChartOfAccount-Ajax?action=exportSampleExcel";
    })

    /** Export chart of account excel  */
    $(document).on("click",'#exportChartOfAccountButton',function(){
        var status = $("#jqGridStatus option:selected").val();
        var table =  'company_chart_of_accounts';
        window.location.href = base_url+"/AddChartOfAccount-Ajax?status="+status+"&&table="+table+"&&action=exportExcel";
    });

    /** Import chart of account excel */
    $("#importChartOfAccountFormId").validate({
        rules: { import_file: {
                required: true
            },
        },
        submitHandler: function (form) {
            event.preventDefault();

            var formData = $('#importPropertyForm').serializeArray();
            var myFile = $('#import_file').prop('files');
            var myFiles = myFile[0];
            var formData = new FormData();
            formData.append('file', myFiles);
            formData.append('class', 'ChartOfAccountAjax');
            formData.append('action', 'importExcel');
            $.ajax
            ({
                type: 'post',
                url: '/AddChartOfAccount-Ajax',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        toastr.success(response.message);

                        $("#import_ChartOfAccount_div").hide(500)
                        $('#ChartOfAccount-table').trigger('reloadGrid');
                    } else if(response.status == 'failed' && response.code == 503){
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });

});