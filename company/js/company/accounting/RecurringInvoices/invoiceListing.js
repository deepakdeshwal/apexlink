$(document).ready(function () {
    invoiceList();
    function invoiceList() {
        var table = ' accounting_invoices';
        var columns = ['Tenant','Type','othername', 'Frequency','id', 'Duration','Start Date','Amount('+default_currency_symbol+')','Last Generated Date','Action'];
        var select_column = ['Edit','Delete','View Detail','Cancel Recurrence'];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_where = [{column:'module_type',value:'recurring',condition:'=',table:'accounting_invoices'}];
        var extra_columns = [];
        var joins = [{
            table: 'accounting_invoices',
            column: 'invoice_to',
            primary: 'id',
            on_table: 'users'
        }];
        var columns_options = [
            {name:'Tenant',index:'name', width:100,searchoptions: {sopt: conditions},table:'users',formatter:TenantNameFormatter },
            {name:'Type',index:'user_type', width:100,searchoptions: {sopt: conditions},table:table,formatter:UserTypeFormatter},
            {name:'othername',index:'other_name', hidden:true, width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Frequency',index:'frequency', width:100,searchoptions: {sopt: conditions},table:table,formatter:FrequencyFormatter},
            {name:'id',index:'id',hidden:true, width:200,searchoptions: {sopt: conditions},table:table},
            {name:'Duration',index:'duration', width:200,searchoptions: {sopt: conditions},table:table},
            {name:'Start Date',index:'created_at', width:150,searchoptions: {sopt: conditions},table:table},
            {name:'Amount('+default_currency_symbol+')',index:'id', width:100,searchoptions: {sopt: conditions},table:table,change_type:'count_Amount'},
            {name:'Last Generated Date',index:'created_at',  width:200,searchoptions: {sopt: conditions},table:table},
             {name:'Action',index:'',title:false, width:200,searchoptions: {sopt: conditions},table:table,formatter:statusFmatter1},
        ];

        var ignore_array = [];
        jQuery("#list_of_invoices").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'true'
            },
            viewrecords: true,
            sortname: 'accounting_invoices.updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Recurring Invoices",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:200,drag:true,resize:false} // search options
        );
    }

    function TenantNameFormatter(cellvalue, options, rowObject)
    {
        if(rowObject!==undefined)
        {
          //  console.log(rowObject.other_name);
            if(rowObject.Tenant!='')
            {
                return '<a class="GoOnInvoiceview" data-id="'+rowObject.id+'" style="color:#05A0E4 ! important;font-weight: bold"><u><strong>'+rowObject.Tenant+'</strong></u></a>';
            } else {
                return '<a class="GoOnInvoiceview" data-id="'+rowObject.id+'" style="color:#05A0E4 ! important;font-weight: bold"><u><strong>'+rowObject.othername+'</strong></u></a>';

            }
        }
    }
    $(document).on('click','.GoOnInvoiceview',function () {
     var id =   $(this).attr('data-id');
        window.location.href = base_url+'/Accounting/ViewRecurringInvoice?id='+id;
      //  Accounting/ViewRecurringInvoice?id

    });
 /*   function actionCheckboxFmatterComplaint(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {

            var data = '';
            var data = '<input type="checkbox" name="maintenance_checkbox[]" class="maintenance_checkbox" id="maintenance_checkbox' + rowObject.id + '" data_id="' + rowObject.id + '"/>';
            return data;
        }
    }*/
 function FrequencyFormatter(cellvalue, options, rowObject) {
     if (rowObject !== undefined) {
          var frequency ='';
          console.log(rowObject);
         if (rowObject.Frequency == 1) {
             frequency = 'Weekly';
         } else if(rowObject.Frequency == 2){
             frequency = 'Bi-Weekly';
         }else if(rowObject.Frequency == 3){
             frequency = 'Monthly';
         }else if(rowObject.Frequency == 4){
             frequency = 'Annually';
         }else if(rowObject.Frequency == 5){
             frequency = 'Quartely';
         }else if(rowObject.Frequency == 6){
             frequency = 'Semi-Annually';
         }else if(rowObject.Frequency == 7){
             frequency = 'Daily';
         }else if(rowObject.Frequency == 8){
             frequency = 'Bi-Monthly';
         }
         return frequency;
     }
 }

    function statusFmatter1(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var select = ['Edit','Delete','View Detail','Cancel Recurrence'];

            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '" data-user_id="' + rowObject.user_id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

  /*  function PropertyUnitFormatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
          //  console.log(rowObject);
            var data = '';
            if(rowObject.Type == 2) {
                if (rowObject.Property != '') {
                    data += '<span class="property_class">' + rowObject.Property + '</br> Unit :' + rowObject.FloorNo + '-' + rowObject.UnitPrefix + '</span>';
                }
            }else if(rowObject.Type == 4){
                data += '<span class="property_class">' + rowObject.OwnerProperty  + '</span>';
            }
            return data;
        }
    }*/

    function dueDateFormat(cellvalue, options, rowObject){
        if (rowObject !== undefined) {
            var html = '';
            $.each(cellvalue,function(key,value){
                 html +='<span>'+value+'</span><br>';
            });
            return html;
        }
    }

    function InvoiceToFormatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var data = '';
            if(rowObject.InvoiceTo != '') {
                data += '<span class="invoice_to_name">' + rowObject.InvoiceTo +'</span>';
            }else{
                data += '<span class="invoice_to_name">' + rowObject.OtherName +'</span>';
            }
            return data;
        }
    }

    function UserTypeFormatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            // console.log(rowObject);
            var data = '';
            var user = "Other";
            if (rowObject.Type != '') {
                if (rowObject.Type == 2) {
                    user = 'Tenant';

                } else if (rowObject.Type == 4) {
                    user = 'Owner';
                }
            }
            data += '<span class="invoice_to_name">' + user + '</span>';
            return data;
        }
    }

    function AmountFormatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var data = '';
            if(rowObject.Property != '') {
                data += '<span class="property_class">' + rowObject.Property + '</br> Unit :' + rowObject.FloorNo + '-' + rowObject.UnitPrefix+'</span>';
            }
            return data;
        }
    }

    function statusPaidUnpaidFormatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            //console.log(rowObject);
            var data = '';
            // if(rowObject.Property != '') {
                if(rowObject.LateDate != '' && rowObject.Status == 0  ){
                    var late_date = convertDateFormat(rowObject.LateDate);
                    var invoice_date = convertDateFormat(rowObject.InvoiceDate);
                   // var date1 = new Date(invoice_date);
                    var date1 = new Date();

                    var date2 = new Date(late_date);
                    var Difference_In_Time = date2.getTime() - date1.getTime();

                    // To calculate the no. of days between two dates
                    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
                    //alert(Difference_In_Days);
                    if(Difference_In_Days < 0 && rowObject.Status == 0){

                        var new_days = Math.abs(Difference_In_Days);
                        var final_days = Math.floor(new_days);
                        var day = (final_days > 1)?'Days': 'Day';
                        status = "Late ("+final_days+' '+day+")";
                        data += '<span class="red-star">'+status+'</span>';
                    }else if(Difference_In_Days > 0 && rowObject.Status == 0){
                        status = 'Unpaid';
                        data += '<span class="red-star">'+status+'</span>';
                    }else if(Difference_In_Days == 0 && rowObject.Status == 0){
                        status = 'Unpaid';
                        data += '<span class="red-star">'+status+'</span>';
                    }
                }else if(rowObject.Status == 0){
                    status = 'Unpaid';
                    data += '<span class="red-star">'+status+'</span>';
                }else if (rowObject.Status == 1){
                    status = 'Paid';
                    data += '<span class="paid_class">'+status+'</span>';
                }
            // }
            return data;
        }
    }


    function convertDateFormat(date){
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    }





    $(document).on("change","#select_all_complaint_checkbox", function(){

        if($(this).is(":checked")){
            $(".maintenance_checkbox").prop("checked",true);
        }else{
            $(".maintenance_checkbox").prop("checked",false);
        }

    });

    $(document).on("change",".maintenance_checkbox", function(){
        var checked_checkboxes_number =  $('[name="maintenance_checkbox[]"]:checked').length;
        var total_checkboxes =  $('[name="maintenance_checkbox[]"]').length;

      if(checked_checkboxes_number != total_checkboxes){
          $("#select_all_complaint_checkbox").prop("checked",false);
      }else{
          $("#select_all_complaint_checkbox").prop("checked",true);
      }

    });




    $(document).on('change', '.select_options', function () {
        var opt = $(this).val();
        var id = $(this).attr('data_id');

        if (opt == 'Edit' || opt == 'EDIT') {
            var status = $(this).closest('tr').find('.paid_class').html();
             if(status =='Paid'){
                 toastr.error("Paid Invoice cannot be edited.");
                 $('.select_options').prop('selectedIndex',0);
                 return false;
             }
            opt = opt.toLowerCase();
            window.location.href = base_url+'/Accounting/RecurringEditInvoice?id='+id;

        }else if (opt == 'View Detail' || opt == 'VIEW DETAIL') {
            opt = opt.toLowerCase();
            window.location.href = base_url+'/Accounting/ViewRecurringInvoice?id='+id;
        } else if (opt == 'Cancel Recurrence' || opt == 'CANCEL RECURRENCE') {
            $.ajax({
                type: 'post',
                url: '/Accounting/recurring-invoice-ajax',
                data: {
                    class: 'RecurringInvoice',
                    action: 'delete',
                    id: id
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        toastr.success(response.message);
                    } else {
                        toastr.error(response.message);
                    }
                    $('#list_of_invoices').trigger('reloadGrid');
                }
            });
        }
        else if (opt == 'Delete' || opt == 'DELETE') {
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/Accounting/recurring-invoice-ajax',
                            data: {
                                class: 'RecurringInvoice',
                                action: 'delete',
                                id: id
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                                $('#list_of_invoices').trigger('reloadGrid');
                            }
                        });
                    }
                    $('#list_of_invoices').trigger('reloadGrid');
                }
            });

        }
        /*else if (opt == 'Apply Payment' || opt == 'APPLY PAYMENT'){
            localStorage.setItem("invoice_id",id);
            window.location.href = base_url+'/Accounting/receivebatchpayment';
        }*/
        $('.select_options').prop('selectedIndex',0);
    });

    $(document).on("click",".delete_invoice",function(){
        var checked_checkboxes_number =  $('[name="maintenance_checkbox[]"]:checked').length;
        if(checked_checkboxes_number == 0){
            toastr.warning("Select alteast one invoice to delete");
            return false;
        }else{
            var ids =   getDataValues();
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/Accounting/recurring-invoice-ajax',
                            data: {
                                ids: ids,
                                class: 'RecurringInvoice',
                                action: 'delete'
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                } else if (response.code == 500) {
                                    toastr.warning(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                                $("#list_of_invoices").trigger('reloadGrid');
                            }
                        });
                    }
                    $("#list_of_invoices").trigger('reloadGrid');
                }
            });
        }


    });





    $(document).on("click",".print_email_envoice",function() {
        var ids = getDataValues();
        var checked_checkboxes_number = $('[name="maintenance_checkbox[]"]:checked').length;
        if (checked_checkboxes_number == 0) {
            toastr.error("Select alteast one invoice ");
            return false;
        }else{
        $.ajax({
            type: 'post',
            url: '/Accounting/recurring-invoice-ajax',
            data: {
                ids: ids,
                class: "RecurringInvoice",
                action: "getAllInvoices"
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    // update other modal elements here too
                    $('#invoice_content').html(response.data);

                    // show modal
                    $('#InvoiceModal').modal('show');
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    });
    $('#InvoiceModal').modal({backdrop: 'static', keyboard: false,show:false});

    $(document).on("click",".close",function(){
        $(".maintenance_checkbox").prop("checked",false);
    });



});

/*
Function to print envelope
*/
function PrintElem(elem)
{
    Popup($(elem).html());
}

function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
        'font-weight:bold;} .right-detail{\n' +
        '        position:relative;\n' +
        '        left:+400px;\n' +
        '    }</style>');
    $(mywindow.document.body).html( '<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if(mywindow.close()){

    }
    $("#InvoiceModal").modal('hide');
    $(".maintenance_checkbox").prop("checked",false);
    return true;
}

/*function to print element by id */
function sendInvoiceEmail()
{
    SendMail();
}


function SendMail(){
    var ids =   getDataValues();
    $.ajax({
        type: 'post',
        url: '/Accounting/recurring-invoice-ajax',
        data: {class: 'RecurringInvoice', action: 'sendInvoiceEmail', ids: ids},
        success : function(response){
            console.log(response.status);
            var response =  JSON.parse(response);

            if(response.status == 'success' && response.code == 200) {
                toastr.success('Mail send successfully.');
                $("#InvoiceModal").modal('hide');
                $(".maintenance_checkbox").prop("checked",false);
                $("#select_all_complaint_checkbox").prop("checked",false);
            }else {
                toastr.warning('Mail not send due to technical issue.');
            }
        }
    });
}
function getDataValues()
{
    var data_ids = [];

    var inputs =  $('[name="maintenance_checkbox[]"]:checked');
    //console.log(inputs); //output input for debug
    inputs.each(function(){
        data_ids.push($(this).attr('data_id'));
    });
    return data_ids;
}