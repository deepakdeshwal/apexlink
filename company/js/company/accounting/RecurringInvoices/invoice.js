$(document).ready(function () {

    $("#invoice_table").hide();

    /*On clik show tenant combogrid */
    $(document).on('click','#_easyui_textbox_input1',function(){
        $('.all_tenants').combogrid('showPanel');
    });

    $('.all_tenants').combogrid({
        placeholder: "Select...",
        panelWidth: 500,
        url: '/Accounting/recurring-invoice-ajax',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getAllTenants',
            class: 'RecurringInvoice'
        },
        columns: [[
            {field: 'id', hidden: true},
            {field: 'name', title: 'Tenant Name', width: 120},
            {field: 'floor_no', title: 'Unit Id', width: 120},
            {field: 'property_id', title: 'Property Id', width: 300},
            {field: 'property_name', title: 'Property Name', width: 200},
            {field: 'address1', hidden: true},
            {field: 'address2', hidden: true},
            {field: 'address3', hidden: true},
            {field: 'city', hidden: true},
            {field: 'country', hidden: true},
            {field: 'state', hidden: true},
            {field: 'zipcode', hidden: true},
        ]],
        onSelect: function (index, row) {
            var user_id =  row.id;
            var address1     = (row.address1 != null )? row.address1 : '';  // the users address1
            var address2     =  (row.address2 != null )? row.address2 : '';  // the users address2
            var address3     = (row.address3 != null )? row.address3 : '';  // the users address3
            var city         =   (row.city != null )? row.city : '';    // the users address3
            var country     = (row.country != null )? row.country : '';   // the users country
            var state     =(row.state != null )? row.state : '';  // the users state
            var zipcode     = (row.zipcode != null )? row.zipcode : '';  // the users state
            var property_name = row.property_name;
            var floor_no = row.floor_no;
            if(user_id != ''){
                $("#user_id").val(user_id)
            }
            $("#property_id").val('');
            fulladdress = '';

            if(address1 !=''){
                fulladdress = address1+"\n"
            }
            if(address2 != ''){
                fulladdress = fulladdress + address2 +"\n"
            }
            if(address3 != ''){
                fulladdress = fulladdress + address3 +"\n"
            }
            if(city != ''){
                fulladdress = fulladdress + city
            }

            if(state != ''){
                fulladdress = fulladdress + " ,"+ state
            }
            if(zipcode != ''){
                fulladdress = fulladdress + " "+ zipcode
            }
            $("#address").html(fulladdress);
            $(".property").val(property_name);
            $(".unit").val(floor_no);
            $("#invoice_table").show();
        }
    });

    /*On clik show owner combogrid */
    $(document).on('click','#_easyui_textbox_input2',function(){
        $('.all_owners').combogrid('showPanel');
    });

    $('.all_owners').combogrid({
        placeholder: "Select...",
        panelWidth: 500,
        url: '/Accounting/recurring-invoice-ajax',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getAllOwners',
            class: 'RecurringInvoice'
        },
        columns: [[
            {field: 'id', hidden: true},
            {field: 'name', title: 'Owner Name', width: 120},
            {field: 'property_id', title: 'Property Id', width: 300},
            {field: 'property_name', title: 'Property Name', width: 200},
            {field: 'property_owned_id', hidden:true},
            {field: 'address1', hidden: true},
            {field: 'address2', hidden: true},
            {field: 'address3', hidden: true},
            {field: 'city', hidden: true},
            {field: 'country', hidden: true},
            {field: 'state', hidden: true},
            {field: 'zipcode', hidden: true},
        ]],
        onSelect: function (index, row) {

            var user_id =  row.id;
            var address1     = (row.address1 != null )? row.address1 : '';  // the users address1
            var address2     =  (row.address2 != null )? row.address2 : '';  // the users address2
            var address3     = (row.address3 != null )? row.address3 : '';  // the users address3
            var city         =   (row.city != null )? row.city : '';    // the users address3
            var country     = (row.country != null )? row.country : '';   // the users country
            var state     =(row.state != null )? row.state : '';  // the users state
            var zipcode     = (row.zipcode != null )? row.zipcode : '';  // the users state
            var property_name = row.property_name;
            var floor_no = row.floor_no;
            if(user_id != ''){
                $("#user_id").val(user_id)
            }

            $("#property_id").val(row.property_owned_id)
            fulladdress = '';

            if(address1 !=''){
                fulladdress = address1+"\n"
            }
            if(address2 != ''){
                fulladdress = fulladdress + address2 +"\n"
            }
            if(address3 != ''){
                fulladdress = fulladdress + address3 +"\n"
            }
            if(city != ''){
                fulladdress = fulladdress + city
            }

            if(state != ''){
                fulladdress = fulladdress + " ,"+ state
            }
            if(zipcode != ''){
                fulladdress = fulladdress + " "+ zipcode
            }
            $("#address").html(fulladdress);
            $(".property").val(property_name);
            $(".unit").val(floor_no);
            $("#invoice_table").show();
        }
    });

    /* Show hide combogrid on selcting radio buttons*/
    $(".all_owners_combogrid, .all_others_combogrid").hide();
    $(document).on("click","input[name='user_radio']",function(e){
        var radioValue = $("input[name='user_radio']:checked").val();

        if(radioValue == "tenant_radio"){
            $("#address").prop("disabled", true);
            $(".all_tenants_combogrid").show();
            $(".all_owners_combogrid").hide();
            $(".all_others_combogrid").hide();
            $("#address").html("");
            $("#_easyui_textbox_input1").val("");
            $("#_easyui_textbox_input2").val("");
            $('#invoice_table input[type="text"]').val('');
        }else if(radioValue == "owner_radio"){
            $("#address").prop("disabled", true);
            $(".all_owners_combogrid").show();
            $(".all_tenants_combogrid").hide();
            $(".all_others_combogrid").hide();
            $("#address").html("");
            $("#invoice_table").hide();
            $("#_easyui_textbox_input1").val("");
            $("#_easyui_textbox_input2").val("");
            $('#invoice_table input[type="text"]').val('');
        }else if(radioValue == "other_radio"){
            $("#address").prop("disabled", false);
            $(".all_others_combogrid").show();
            $(".all_tenants_combogrid").hide();
            $(".all_owners_combogrid").hide();
            $("#address").html("");
            $("#invoice_table").show();
            $("#_easyui_textbox_input1").val("");
            $("#_easyui_textbox_input2").val("");
            $('#invoice_table input[type="text"]').val('');
            $("#property_id").val('');
            $("#user_id").val('');

        }

    })

    /* Combogrid Place holder for owner and tenant */
    $("#_easyui_textbox_input2").attr("placeholder", "Click here to select a owner");
    $("#_easyui_textbox_input1").attr("placeholder", "Click here to select a Tenant");

    /*Ajax for charge code listing*/
    $.ajax({
        type: 'post',
        url: '/Accounting/recurring-invoice-ajax',
        data: {
            class: "RecurringInvoice",
            action: "getCharges"
        },
        success: function (response) {

            var data = $.parseJSON(response);
            if (data.status == "success") {
                var charges = data.data.charges;

                if (charges.length > 0){
                    var chargesOptions = '<option value="">Select</option>';
                    for (var i = 0; i < charges.length; i++){
                        chargesOptions += "<option value='"+charges[i].id+"'>"+charges[i].charge_code+' - '+charges[i].description+"</option>";
                    }

                    $("#new_chargeCode_select").html(chargesOptions);
                }

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

    $(document).on("click",".additional-add-invoice",function(){
        var clone = $(".additional-invoice:first").clone();
         clone.find('.description,.money ').val('');
        $(".additional-invoice").last().after(clone);
        clone.find(".additional-add-invoice").hide();
        clone.find(".additional-remove-invoice").show();
       // $('.additional-invoice .customChargeValidation').attr('required',true);
      //  $('.additional-invoice .customAmountvalidations').attr('required',true);
       /* new AutoNumeric.multiple('.money', {
            allowDecimalPadding: true,
            maximumValue  : '9999999999',
        });*/

    });


    $(document).on("click",".additional-remove-invoice",function(){
        $(this).parents(".additional-invoice").remove();
        calculateSum();
    });


    $("#invoice_date").datepicker({dateFormat: jsDateFomat ,onSelect: function (selectedDate) {
            $("#late_date").datepicker("option", "minDate", selectedDate);
        }}).datepicker("setDate", new Date());;

    $("#late_date").datepicker({dateFormat: jsDateFomat,
        changeYear: true,
        yearRange: "-100:+20",
        changeMonth: true,
        minDate: new Date()
    });



    $("#addInvoice").validate({
        rules: {
            duration: {
                required:true
            },
            amount: {
                required:true
            }
        }
    });

    /*submit halder for add employee

     */
    $("#addInvoice").on('submit',function(event){
        event.preventDefault();
        $(".customadditionalEmailValidation").each(function () {
            res = validations(this);
        });
        $(".customValidationCarrier").each(function () {
            res = validations(this);
        });

        $(".customPhonenumbervalidations").each(function () {
            res = validations(this);
        });
        $(".customOtherPhonevalidations").each(function () {
            res = validations(this);
        });
        if($("#addInvoice").valid()){
            var user_id = $("#user_id").val()
            var form = $('#addInvoice')[0];
            var formData = new FormData(form);
            formData.append('action','insert');
            formData.append('class','RecurringInvoice');
            formData.append('user_id',user_id);
            action = 'insert';
            $.ajax({
                url:'/Accounting/recurring-invoice-ajax',
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function (xhr) {
                    var Charge = true;
                    var Amount = true;
                    // checking portfolio validations
                    $(".customChargeValidation").each(function() {
                        console.log(this);
                        Charge = validations(this);

                    });
                    $(".customAmountvalidations").each(function() {
                        Amount = validations(this);

                    });
                    if (Charge === false || Amount === false) {
                        xhr.abort();
                        return false;
                    }
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == "success"){
                        toastr.success(response.message);
                        localStorage.setItem("Message", response.message);
                        localStorage.setItem("rowcolor",true)
                        window.location.href = window.location.origin+'/Accounting/RecurringInvoices';
                    }else{
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });

    /*custom validation for carrier
     */
   /* $(document).on('change','.customChargeValidation',function(){
        validations(this);
    });
*/
    /*custom validation for additional phone
     */
   /* $(document).on('keyup','.customAmountvalidations',function(){
        validations(this);
    });
*/
    /* jquery for phone number format

     */
     $(document).on("change",".customChargeValidation", function(e){
        var charge_descrption_value = $(this ).find('option:selected').text();
        var charge_descrption = charge_descrption_value.split("-");
        if(charge_descrption != '')
            $(this).parents("td").next().find("input").val(charge_descrption[1].trim())
    });

    $(document).on("blur",".money",function(){
       var amt =  $(this).val();
        var newamt =  changeToFloat(amt.toString());
        $(this).val(newamt);
        calculateSum();

    });

    function calculateSum() {
        var sum = 0;
        //iterate through each textboxes and add the values
        $(".money").each(function() {

            //add only if the value is number
            var amount_value =  this.value.replace(/,/g, '');
            if(!isNaN(amount_value) && amount_value!=0) {
                sum += parseFloat(amount_value);
            }
        });
        //.toFixed() method will roundoff the final sum to 2 decimal places
        $(".total_charges_amount").html(sum.toFixed(2));
    }

    $(document).on("click",".cancel",function() {
        bootbox.confirm({
            message: "Do you want to cancel this invoice ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    window.location.href = base_url+'/Accounting/RecurringInvoices';
                }
            }
        });
    });

    $(document).on('click','.clearFormReset',function () {
        bootbox.confirm("Do you want to clear this form?", function (result) {
            if (result == true) {
                $('#invoice_table').hide();
                $('#addInvoice')[0].reset();
            }
        });
      //  resetFormClear('addInvoice',expectArray,'form',true)
    });

});