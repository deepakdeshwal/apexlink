$(document).ready(function () {

    $("#unit_type").keypress(function(e){
        var keyCode = e.which;
        /*
          8 - (backspace)
          32 - (space)
          48-57 - (0-9)Numbers
        */
        // Not allow special
        if ( !( (keyCode >= 48 && keyCode <= 57)
            ||(keyCode >= 65 && keyCode <= 90)
            || (keyCode >= 97 && keyCode <= 122) )
            && keyCode != 8 && keyCode != 32) {
            e.preventDefault();
        }
    });
    var base_url = window.location.origin;

    // var status =  localStorage.getItem("active_inactive_status");
    // console.log(status);
    // if(status !== undefined) {
    //     if ($("#BankAccount-table")[0].grid) {
    //         $('#BankAccount-table').jqGrid('GridUnload');
    //     }
    //     //intializing jqGrid
    //     if(status == 'all'){
    //         jqGrid('All');
    //     }  else {
    //         jqGrid(status);
    //     }
    //     $('#jqGridStatus option[value='+status+']').attr("selected", "selected");
    //
    // }else{
    //     if ($("#BankAccount-table")[0].grid) {
    //         $('#BankAccount-table').jqGrid('GridUnload');
    //     }
    //     jqGrid('All');
    // }

    /** Show add new Bank Account div on add new button click */
    $(document).on('click','#addBankAccountButton',function () {

        $("#portfolioErr").text('');
        $("#bank_nameErr").text('');
        $("#bank_account_numberErr").text('');
        $("#fdi_numberErr").text('');
        $("#branch_codeErr").text('');
        $("#initial_amountErr").text('');
        $("#last_used_check_numberErr").text('');
        $("#statusErr").text('');

        $("#add_Bank_account_form")[0].reset();
        var validator = $("#add_Bank_account_form").validate();
        $("#addBankAccountButton").click(function() {
            validator.resetForm();
        });
        $('#bank_account_id').val('');
        portfolioList();
        headerDiv.innerText = "Add Bank Account";
        $('#is_default').prop('checked', false);
        $('#saveBtnId').val('Save');
        $('#add_Bank_account_div').show(500);
    });

    /**
     * Get all portfolio list for ddl
     */
    function portfolioList(){
        $.ajax({
            type: 'post',
            url: '/BankAccount-Ajax',
            data: {
                class: 'BankAccountAjax',
                action: 'getAllPortfolioList',
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#portfolio').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        if(value.is_default == 1){
                            $('#portfolio').append($("<option value = "+value.id+" selected>"+value.portfolio_name+"</option>"));
                        } else {
                            $('#portfolio').append($("<option value = "+value.id+">"+value.portfolio_name+"</option>"));
                        }
                    })
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /** Hide add new Bank Account div on cancel button click */
    $(document).on("click", "#bank_account_cancel_btn", function (e) {
        e.preventDefault();
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#add_Bank_account_div").hide(500);
                }
            }
        });
    });

    /** jqGrid status change*/
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        $('#BankAccount-table').jqGrid('GridUnload');
        $('#add_Bank_account_div').hide(500);
        changeGridStatus(selected);
        jqGrid(selected, true);
    });

    /**
     * jqGrid Initialization function
     * @param status
     */
    function jqGrid(status, deleted_at) {
        var table = 'company_accounting_bank_account';
        var columns = ['Portfolio','Bank Name', 'Bank Account Number', 'FDI Number', 'Branch Code', 'Initial Amount('+default_currency_symbol+')', 'Last Used Check Number', 'Status', 'Set as Default', 'Action'];
        var select_column = ['Edit','Deactivate','Delete'];
        // var joins = [];
        var joins = [{table:'company_accounting_bank_account',column:'Portfolio',primary:'id',on_table:'company_property_portfolio'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['company_accounting_bank_account.status', 'company_accounting_bank_account.deleted_at', 'company_accounting_bank_account.updated_at'];
        var columns_options = [
            {name:'Portfolio',index:'portfolio_name', width:90,align:"center",searchoptions: {sopt: conditions},table:'company_property_portfolio'},
            {name:'Bank Name',index:'bank_name', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Bank Account Number',index:'bank_account_number', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'FDI Number',index:'fdi_number', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Branch Code',index:'branch_code', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Initial Amount(US $)',index:'initial_amount', width:100,searchoptions: {sopt: conditions},table:table, formatter:ammountFormatter},
            {name:'Last Used Check Number',index:'last_used_check_number', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Set as Default',index:'is_default', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:isDefaultFormatter},
            {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFormatter}
        ];
        var ignore_array = [];
        jQuery("#BankAccount-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_accounting_bank_account",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Bank Accounts",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:200,left:200,drag:true,resize:false} // search options
        );
    }
    /**
     * jqGrid function to currency status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function ammountFormatter (cellValue, options, rowObject){
        if (cellValue != '')
            return default_currency_symbol+cellValue;
       else
            return '';
    }
    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Active";
        else if(cellValue == '0')
            return "InActive";
        else
            return '';
    }

    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isDefaultFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Yes";
        else if(cellValue == '0')
            return "No";
        else
            return '';
    }

    /**
     * jqGrid function to format action column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function actionFormatter (cellValue, options, rowObject){
        if(rowObject !== undefined) {
            var is_default = rowObject;
            var editable = $(cellValue).attr('editable');
            var select = '';
            if(rowObject.Status == 1)  select = ['Edit','Deactivate','Delete'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete'];
            var data = '';
            if(select != '') {
                var data = '<select ' +
                    ' class="form-control select_options" status="'+is_default['Set as Default'] +'" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    if(editable == '0' && (val == 'delete' || val == 'Delete')){
                        return true;
                    }
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }


    /** Add/Edit new Bank Account */
    // $('#add_unit_type_form').on('submit',function(e){
    $("#add_Bank_account_form").validate({
        rules: {
            portfolio: {
                required: true
            },
            bank_name: {
                required: true
            },
            bank_account_number: {
                required: true,
                minlength: 7
            },
            fdi_number: {
                required: true
            },
            branch_code: {
                required: true
            }
            ,
            routing_number: {
                required: true,
                number:true
            },
            initial_amount: {
                required: true,
                number: true
            },
            last_used_check_number: {
                required: true,
                number: true
            },
            status: {
                required: true
            }
        },
        messages: {
            portfolio: {
                required: "* This field is required",
            },
            bank_account_number: {
                minlength: "* Please enter 7 digit account number",
            }
        },
        submitHandler: function () {
            var portfolio = $('#portfolio').val();
            var bank_name = $('#bank_name').val();
            var bank_account_number = $('#bank_account_number').val();
            var fdi_number = $('#fdi_number').val();
            var branch_code = $('#branch_code').val();
            var routing_number = $('#routing_number_rec').val();
            var initial_amount = $('#initial_amount').val();
            var last_used_check_number = $('#last_used_check_number').val();
            var status = $('#status').val();
            var bank_account_id = $("#bank_account_id").val();
            var is_default;
            if ($('#is_default').is(":checked")) {
                is_default = '1';   // it is checked
            } else {
                is_default = '0';
            }
            var formData = {
                'portfolio': portfolio,
                'bank_name': bank_name,
                'bank_account_number': bank_account_number,
                'fdi_number': fdi_number,
                'branch_code': branch_code,
                'routing_number': routing_number,
                'initial_amount': initial_amount,
                'last_used_check_number': last_used_check_number,
                'status': status,
                'is_default': is_default,
                'bank_account_id': bank_account_id
            };
            var action;
            if (bank_account_id) {
                action = 'update';
            } else {
                action = 'insert';
            }
            $.ajax({
                type: 'post',
                url: '/BankAccount-Ajax',
                data: {
                    class: 'BankAccountAjax',
                    action: action,
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        $("#add_Bank_account_div").hide(500);
                        $("#BankAccount-table").trigger('reloadGrid');
                        toastr.success(response.message);
                        onTop(true);x
                    } else if (response.status == 'error' && response.code == 400) {
                        toastr.error(response.message);
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('#' + key).text(value);
                        });
                    } else if (response.status == 'warning' && response.code == 503) {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });

    /**  List Action Functions  */
    $(document).on('change', '.select_options', function() {
        setTimeout(function(){ $(".select_options").val("default"); }, 200);

        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index() ;
        var status = $(this).attr('status');
        if (opt == 'Edit' || opt == 'EDIT') {

            $("#add_Bank_account_div").show(500);
            headerDiv.innerText = "Edit Bank Account";
            portfolioList();
            $('#saveBtnId').val('Update');

            $("#add_Bank_account_form")[0].reset();
            var validator = $("#add_Bank_account_form").validate();
            $("#addBankAccountButton").click(function() {
                validator.resetForm();
            });

            $("#portfolioErr").text('');
            $("#bank_nameErr").text('');
            $("#bank_account_numberErr").text('');
            $("#fdi_numberErr").text('');
            $("#branch_codeErr").text('');
            $("#initial_amountErr").text('');
            $("#last_used_check_numberErr").text('');
            $("#statusErr").text('');
            $('.table').find('.green_row_left, .green_row_right').each(function(){
                $(this).removeClass("green_row_left green_row_right");
            });
            $('.table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
            $('.table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
            $(".clear-btn").text('Reset');
            $(".clear-btn").addClass('formreset');
            $(".clear-btn").removeClass('clearFormReset');

            $.ajax
            ({
                type: 'post',
                url: '/BankAccount-Ajax',
                data: {
                    class: "BankAccountAjax",
                    action: "view",
                    id : id,
                },
                success: function (response) {
                    var data = $.parseJSON(response);
                    if (data.status == "success")
                    {
                        // console.log(data);
                        $("#portfolio").val(data.data.portfolio);
                        $("#bank_name").val(data.data.bank_name);
                        $("#bank_account_number").val(data.data.bank_account_number);
                        $("#fdi_number").val(data.data.fdi_number);
                        $("#branch_code").val(data.data.branch_code);
                        $("#initial_amount").val(data.data.initial_amount);
                        $("#last_used_check_number").val(data.data.last_used_check_number);
                        $("#status").val(data.data.status);
                        $("#bank_account_id").val(data.data.id);

                        if(data.data.is_editable == 0) {
                            $('#unit_type').prop('disabled', true);
                        } else {
                            $('#unit_type').prop('disabled', false);
                        }
                        if (data.data.is_default == 1) {
                            $('#is_default').prop('checked', true);
                        } else {
                            $('#is_default').prop('checked', false);
                        }

                    }else if(data.status == "error"){
                        toastr.error(data.message);
                    } else{
                        toastr.error(data.message);
                    }
                    defaultFormData = $('#add_Bank_account_form').serializeArray();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });


        } else if (opt == 'Deactivate' || opt == 'DEACTIVATE' || opt == 'Activate' || opt == 'ACTIVATE') {
            opt = opt.toLowerCase();
            bootbox.confirm({
                message: "Do you want to " + opt + " this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        var status = opt == 'activate' ? '1' : '0';
                        $.ajax({
                            type: 'post',
                            url: '/BankAccount-Ajax',
                            data: {
                                class: 'BankAccountAjax',
                                action: 'updateStatus',
                                status: status,
                                id: id
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                                localStorage.setItem("rowcolor", 'add colour');
                            }
                        });
                    }
                    $('#BankAccount-table').trigger('reloadGrid');
                }
            });
        } else if (opt == 'Delete' || opt == 'DELETE') {
            opt = opt.toLowerCase();
            if (status == '1') {
                toastr.warning('A default set value cannot be deleted.');
            } else {
                bootbox.confirm({
                    message: "Do you want to delete this record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/BankAccount-Ajax',
                                data: {
                                    class: 'BankAccountAjax',
                                    action: 'delete',
                                    id: id
                                },
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                }
                            });
                        }
                        $('#BankAccount-table').trigger('reloadGrid');
                    }
                });
            }
        }
    });

    /** Export sample Bank Account excel */
    // $(document).on("click",'#exportSampleExcel',function(){
    //     window.location.href = base_url+"/BankAccount-Ajax?action=exportSampleExcel";
    // });

    /** Export Bank Account excel  */
    // $(document).on("click",'#exportBankAccountButton',function(){
    //     var status = $("#jqGridStatus option:selected").val();
    //     var table =  'company_accounting_bank_account';
    //     window.location.href = base_url+"/BankAccount-Ajax?status="+status+"&&table="+table+"&&action=exportExcel";
    // });

    /** Import Bank Account excel  */
    // $("#importBankAccountFormId").validate({
    //     rules: { import_file: {
    //             required: true
    //         },
    //     },
    //     submitHandler: function (form) {
    //         event.preventDefault();
    //
    //         var formData = $('#importPropertyForm').serializeArray();
    //         var myFile = $('#import_file').prop('files');
    //         var myFiles = myFile[0];
    //         var formData = new FormData();
    //         formData.append('file', myFiles);
    //         formData.append('class', 'BankAccountAjax');
    //         formData.append('action', 'importExcel');
    //         $.ajax
    //         ({
    //             type: 'post',
    //             url: '/BankAccount-Ajax',
    //             processData: false,
    //             contentType: false,
    //             data: formData,
    //             success: function (response) {
    //                 var response = JSON.parse(response);
    //                 if(response.status == 'success' && response.code == 200){
    //                     toastr.success(response.message);
    //
    //                     $("#import_unit_type_div").hide(500)
    //                     $('#BankAccount-table').trigger('reloadGrid');
    //                 } else if(response.status == 'failed' && response.code == 503){
    //                     toastr.error(response.message);
    //                     // $('.error').html(response.message);
    //                     // $.each(response.message, function (key, value) {
    //                     //     $('.'+key).html(value);
    //                     // });
    //                 }
    //             },
    //             error: function (data) {
    //                 var errors = $.parseJSON(data.responseText);
    //                 $.each(errors, function (key, value) {
    //                     // alert(key+value);
    //                     $('#' + key + '_err').text(value);
    //                 });
    //             }
    //         });
    //     }
    // });
});