$(document).ready(function () {
    var id = getParameterByName('id');
    $.ajax({
        type: 'get',
        url: '/Accounting/invoice-ajax',
        data: {
            id: id,
            class: 'Invoice',
            action: 'View'},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200) {
                $.each(response.data, function (key, value) {
                    // alert(value);
                    if(key=='key_access_codes_info'){
                    }else{
                        $('.' + key).html(value);
                        if(key=='invoice_to' && (value == null || value == '')){
                            $('.invoice_to').html(response.data.other_name);
                        }


                    }
                });
                var charge_data = response.charge_data;
                var charge_data_html ='';
                var property = '';
                $.each(charge_data,function (key, value){
                    if(response.data.property_name != null ){
                        property = response.data.property_name;
                    }

                    charge_data_html += '<tr>';
                    charge_data_html += '<td><a href="javascript:;">'+property+'</a></td>';
                    charge_data_html += '<td>'+response.data.unit+'</td>';
                    charge_data_html += '<td>'+value.description+'</td>';
                    charge_data_html +=  '<td>'+value.description+'</td>';
                    charge_data_html +=  '<td ><span>'+default_currency_symbol+'<span>'+changeToFloat(value.amount.toString())+'</td>';
                    charge_data_html += '</tr>';
                });
                setTimeout(function(){
                    $(".charge_data_html").html(charge_data_html);
                    }, 400);


            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

    /**
     * Get Parameters by id
     * @param status
     */
    function getParameterByName(name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }


});