$(document).ready(function(){

getProperties();
getUserNameByType(3);



$(document).on('keypress keyup','.checkNumber, .money',function(){
     $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }

});


$(document).on("change","#property",function(){

    var property_id = $(this).val();
    if(property_id=="")
    {
     var property_id = 0;	
    }
    if(property_id!=0)
    {
    	 	            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'getBuildings',
                    "class": 'accounting',
                    'property_id':property_id

                },
                success: function (response) {

                   var response = JSON.parse(response);
                   $("#building").html(response.building);
                   
                    

                }
            });

    }



  });

$(document).on("change","#building",function(){

    var building_id = $(this).val();
    if(building_id!='0')
    {
    	 	            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'getUnits',
                    "class": 'accounting',
                    'building_id':building_id

                },
                success: function (response) {

                   var response = JSON.parse(response);
                   $("#unit").html(response.unit);
                   
                    

                }
            });

    }



  });


});


	function getProperties()
	{
		            $.ajax({
	            url:'/accountingReceivable',
	            type: 'POST',
	            data: {

	                "action": 'getProperties',
	                "class": 'accounting'

	            },
	            success: function (response) {

	               var response = JSON.parse(response);
	               $("#property").html(response.property);
	               
	                

	            }
	        });
	  }



	  $(document).on("click",".selectUser",function(){
	  	
         var val = $(this).val();
         var property_id = $("#property").val();
         if(val==0)
         {
         	$(".orderOff").html("<input type='text' name='payOrderOff' class='payOrderOff form-control'>");
         	 $(".memo").attr("readonly", false); 
            $(".address").attr("readonly", false); 
            $(".address").val('');
           
         }
         else 
         {
            $(".address").val(''); 
            $(".address").attr("readonly", true); 
           
          $(".orderOff").html('<select name="payOrderOff" class="form-control payOrderOff"><option>Select</option></select>');
          if(val==3 || val==2 && (property_id!=0 || property_id!=''))
          {
          getUserNameByType(val,property_id);	
          }
          else
          {
          	getUserNameByType(val,0);
          }
          
          $(".memo").attr("readonly", true); 
         }
         

	  });



	  function getUserNameByType(type,property_id)
	  {
	   	  	   $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'getUserNameByType',
                    "class": 'accounting',
                    'type':type,
                    'property_id':property_id

                },
                success: function (response) {
                   $(".payOrderOff").html(response);

               
                }
            });

	   }

	   $(document).on("change","#property",function(){
         var property_id = $(this).val();
         	   	  	   $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'getBankListByProperty',
                    "class": 'accounting',
                    'property_id':property_id

                },
                success: function (response) {
                	
                	 $("#selectBank").html(response);
                	 $("input[name=userType][value='3']").prop('checked', true);
                	 $("input[name=userType][value='3']").trigger( "click" );
                	  
                }
            });

	   });



	   $(document).on("change","#selectBank",function(){
           var bank_id = $(this).val();
             	  	   $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'getCheckNumber',
                    "class": 'accounting',
                    'bank_id':bank_id

                },
                success: function (response) {
                	
                	 $(".checkNumber").val(response);
                	  
                }
            });



	   	 });



	   	   $(document).on("change",".payOrderOff",function(){
           var user_id = $(this).val();
           var property_id = $("#property").val();
           var type = $("input[name='userType']:checked").val();
             	  	   $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'getAddress',
                    "class": 'accounting',
                    'property_id':property_id,
                    'type':type,
                    'user_id':user_id,

                },
                success: function (response) {
                	 
                	 $(".address").val(response);
                	  
                }
            });
           });


setTimeout(function(){
	$('.calander').datepicker({
        yearRange: '2019:2040',
        changeMonth: true,
        changeYear: true,
        dateFormat: date
    });

},300);



$("#createCheckForm").validate({
    rules: {
        property: {
            required:true
        },
          selectBank: {
            required:true
        },
          checkNumber: {
            required:true
        },
          Date: {
            required:true
        },
          money: {
            required:true
        },
        payOrderOff:
            {
           required:true
            }
     },
    submitHandler: function (e) {
        var form = $('#createCheckForm')[0];
        var formData = new FormData(form);
        formData.append('action','createCheckForbank');
        formData.append('class','accounting');
        $.ajax({
            url:'/accountingReceivable',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status=="true")
                {
                  toastr.success(response.message);
                  getCheckData(response.last_id,response.property_name,response.user_type);
                  $("#createCheckForm").trigger("reset");


                }
                else
                {
                  toastr.error(response.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});



 $(document).on("click",".saveClose",function(){
   var form = $("#createCheckForm");
    var abc= form.valid();
   if(abc==true)
   {
  $('#createCheckForm').trigger('submit');
  setTimeout(function(){
    window.location.href =  base_url+'/Accounting/checkLists';
  },300)
   }

   
 });








function getCheckData(lastId,property_name,user_type)
{
	            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'getCheckData',
                    "class": 'accounting',
                    'last_id':lastId,
                    'property_name':property_name,
                    'user_type':user_type

                },
                success: function (response) {
                	
                	 $(".checkData").append(response);
                	  
                }
            });

 }



 $(document).on("click",".printBtn",function(){
  var base_url = window.location.origin;
   window.location.href =  base_url+'/Accounting/checkLists';
 });


 $(document).on('click','.clearBankForm',function () {
     bootbox.confirm("Do you want to clear this form?", function (result) {
         if (result == true) {
             $('#createCheckForm')[0].reset();
         }
     });
 });





$(document).ready(function () {
    jqGridBankTransaction("All");
    function jqGridBankTransaction(status) {
        var table = 'transactions';
        var depositeCol = "Deposit ("+default_currency_symbol+")";
        var paymentCol = "Payment ("+default_currency_symbol+")";
        var balancesCol = "Balances ("+default_currency_symbol+")";
        var columns = ['Date','Reference Number','Description','user_type','Bank Name','Reconcile',depositeCol,paymentCol,balancesCol,'total_charge_amount','initial_amount'];
        var select_column = ['Post'];
        var joins = [
            {table:table,column:'bank_id',primary:'id',on_table:'company_accounting_bank_account'},
            // {table:'company_accounting_bank_account',column:'portfolio',primary:'portfolio_id',on_table:'general_property'},
            // {table:'general_property',column:'id',primary:'property_id',on_table:'property_bank_details'}
        ];
        var conditions = ["eq","bw","ew","cn","in"];
        //var extra_where = [{column:'status',value:2,condition:'=',table:'tenant_charges'}];
         var extra_where = [];
        var extra_columns = [];
        var columns_options = [
            {name:'Date',index:'created_at', width:150,align:"left",searchoptions: {sopt: conditions},table:table},
            {name:'Reference Number',index:'reference_no', width:150, align:"left",searchoptions: {sopt: conditions},table:table},
            {name:'Description',index:'type', width:150, align:"left",searchoptions: {sopt: conditions},table:table},
            {name:'user_type',index:'user_type', hidden:true,width:150, align:"left",searchoptions: {sopt: conditions},table:table},
            {name:'Bank Name',index:'bank_name', width:150, align:"left",searchoptions: {sopt: conditions},table:'company_accounting_bank_account'},
            {name:'bank_id',index:'bank_id',align:"center", width:170,searchoptions: {sopt: conditions},table:table,formatter:reconformatter},
            {name:depositeCol,index:'total_charge_amount',align:"center", width:170,searchoptions: {sopt: conditions},table:table,formatter:paymentcheck,formatter:currencyform},
            {name:paymentCol,index:'total_charge_amount',align:"center",width:170,searchoptions: {sopt: conditions},table:table,formatter:paymentcheck1,formatter:currencyform},
            {name:balancesCol,index:'total_charge_amount', width:170,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'bank_balance',formatter:currencyform},
            {name:'total_charge_amount',index:'total_charge_amount',hidden:true, width:170,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'initial_amount',index:'initial_amount',hidden:true, width:170,align:"center",searchoptions: {sopt: conditions},table:'company_accounting_bank_account'},
        ];
        var ignore_array = [];
        jQuery("#bankregistertable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'no'
            },
            viewrecords: true,
            sortname: 'transactions.created_at',
            sortorder: "asc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Bank Register",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }
    function reconformatter(cellValue, options, rowObject) {
        if(rowObject!==undefined){
            return "No"
        }
    }
    function paymentcheck(cellValue, options, rowObject) {
        if(rowObject!==undefined){
            var pay="";
            if(rowObject.user_type=="PM"){
                pay="";
            }else if(rowObject.user_type!=="PM"){
                pay=rowObject.total_charge_amount;
            }
            return pay;
        }
    }
    function paymentcheck1(cellValue, options, rowObject) {
               if(rowObject!==undefined){
                 var pay="";
                   if(rowObject.user_type=="PM" && rowObject.Description!=="SUBSCRIPTION"){
                       pay=rowObject.total_charge_amount;

                 }else if(rowObject.user_type!=="PM"){
                       pay="";
                 }
                   return pay;
               }
           }
    function currencyform(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var val="";
            if(cellvalue !== null && cellvalue!== 'undefined' && cellvalue!==''){
                val=(Math.round(cellvalue * 100) / 100).toFixed(2);
                val=default_currency_symbol+val;
            }
            else{
                val="";
            }
            return val;
        }
    }

});


    
	   
	  
