$(document).ready(function () {

    getDataByID('','', 'all_owner_listing');
    getDataByID('','', 'all_property_listing');
    function getDataByID(data_id, prev_id, data_type){
        $.ajax({
            type: 'post',
            url: '/OwnerContribution-Ajax',
            async: false,
            data: {
                class: "ownerContribution",
                action: "getDataByID",
                id: data_id,
                prev_id: prev_id,
                data_type : data_type
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    if(data_type == 'all_owner_listing') {
                        var ownerOption = "<option value=''>Select Owner</option>";
                        if (data.data.owner_listing.length > 0) {
                            $.each(data.data.owner_listing, function (key, value) {
                                if (value.name != undefined && value.id != undefined) {
                                    ownerOption += "<option value='" + value.id + "'>" + value.name + "</option>";
                                }
                            });
                        }
                        $('#owner_name_id').html(ownerOption);
                    }
                    if(data_type == 'all_property_listing') {
                        var propertyOption = "";
                        if (data.data.property_listing.length > 0) {
                            propertyOption = "<option value=''>Select Property</option>";
                            $.each(data.data.property_listing, function (key, value) {
                                if (value.property_name != undefined && value.id != undefined) {
                                    propertyOption += "<option value='" + value.id + "'>" + value.property_name + "</option>";
                                }
                            });
                        }
                        $('#property_id').html(propertyOption);
                    }
                    if(data_type == 'property_by_owner') {
                        var propertyOption = "";
                        console.log(data.data.property_listing.length);

                        if (data.data.property_listing.length > 0) {
                            propertyOption = "<option value=''>Select Property</option>";
                            $.each(data.data.property_listing, function (key, value) {
                                if (value.property_name != undefined && value.id != undefined) {
                                    if (data.data.property_listing.length == 1) {
                                        propertyOption += "<option selected value='" + value.id + "'>" + value.property_name + "</option>";
                                        // var propertyData = getDataByID($(this).val(), value.id,'portfolio');
                                        // $('#property_id').html(propertyData);
                                    } else {
                                        propertyOption += "<option value='" + value.id + "'>" + value.property_name + "</option>";
                                    }
                                }
                            });
                            console.log(propertyOption);
                            $('#property_id').html(propertyOption);
                        } else {
                            propertyOption = "<option value=''>This Portfolio has no Property</option>";
                        }
                    }
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    $('#start_date').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: datepicker_format});
    $('#end_date').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: datepicker_format});

    $(document).on("change", "#start_date", function () {
        var start_date = $(this).val();
        var end_date = $('#end_date').val();
        if (end_date != '' && end_date < start_date){
            $('#end_date').datepicker({ dateFormat: datepicker_format}).datepicker("setDate", start_date);
        }
        $('#end_date').datepicker({dateFormat: datepicker_format, minDate: start_date});
    });

    $(document).on('change','#owner_name_id',function() {
        var owner_name_id = $('#owner_name_id').val();
        if(owner_name_id == ''){

        } else {
            getDataByID(owner_name_id,'', 'property_by_owner');
        }
    });
    $(document).on('change','.common_search_class',function(){
        var grid = $("#owner_contribution_table"),f = [];
        // var value = $(this).attr('data_id');
        var owner_name_id = $('#owner_name_id').val()== ''?'all':$('#owner_name_id').val();
        var property_id   = $('#property_id').val()== ''?'all':$('#property_id').val();
        var start_date    = $('#start_date').val() == ''?'all':$('#start_date').val();
        var end_date      = $('#end_date').val() == ''?'all':$('#end_date').val();
        f.push(
            {field: "users.id", op: "eq", data: owner_name_id},
            {field: "general_property.id", op: "eq", data: property_id},
            {field: "owner_contribution.created_at", op: "dateBetween", data: start_date, data2:end_date}
        );
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    });

    listOwnerContributionJqGrid('');
    function listOwnerContributionJqGrid(selected_owner_id, deleted_at) {
        var sortOrder = 'desc';
        var sortColumn = 'owner_contribution.created_at';

        var table = 'owner_contribution';
        var select_column = '';
        var columns = ['Owner Name','Property Name', 'transation_type', 'bank_account_number', 'created_at', 'Account No.', 'COA','Amount('+default_currency_symbol+')'];
        var joins = [
            {table: 'owner_contribution', column: 'owner_id', primary: 'id', on_table: 'users'},
            {table: 'owner_contribution', column: 'property_id', primary: 'id', on_table: 'general_property'},
            {table: 'owner_contribution', column: 'chart_of_account_id', primary: 'id', on_table: 'company_chart_of_accounts'},
            {table: 'owner_contribution', column: 'bank_account_id', primary: 'id', on_table: 'company_accounting_bank_account'},
        ];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        if (selected_owner_id != '') {
            var extra_where = [
                {column: 'user_type', value: '4', condition: '=', table: 'users'},
                { column: 'owner_id', value: selected_owner_id, condition: '=', table: 'owner_contribution'}
            ];
        } else {
            var extra_where = [{column: 'user_type', value: '4', condition: '=', table: 'users'}];
        }
        var columns_options = [
            {name: 'Owner Name', index: 'name', width: 60, align: "center", searchoptions: {sopt: conditions}, table: 'users', classes: 'cursor', formatter: ownerName },
            { name: 'Property Name', index: 'property_name', width: 60, align: "center", searchoptions: {sopt: conditions}, table: 'general_property', classes: 'pointer', formatter: propertyName},
            /*hidden fields*/
            {name: 'transation_type', index: 'transation_type', hidden: true, alias: 'property_id', width: 60, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'bank_account_number', index: 'bank_account_number', hidden: true, width: 130, align: "center", searchoptions: {sopt: conditions}, table: 'company_accounting_bank_account'},
            {name: 'created_at', index: 'created_at', hidden: true, width: 130, align: "center", searchoptions: {sopt: conditions}, table: table},
            /*hidden fields*/
            {name: 'Account No.', index: 'bank_name', width: 130, align: "center", searchoptions: {sopt: conditions}, table: 'company_accounting_bank_account', formatter: accountNameNumber},
            {name: 'COA', index: 'account_name', width: 130, align: "center", searchoptions: {sopt: conditions}, table: 'company_chart_of_accounts'},
            {name: 'Amount', index: 'amount', width: 130, align: "center", searchoptions: {sopt: conditions}, table: table,  formatter: amount}
        ];
        var ignore_array = [];

        jQuery("#owner_contribution_table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "owner_contribution",
                select: select_column,
                columns_options: columns_options,
                // status: '',
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                deleted_at: true,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Owner Contribution",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
            // {top:200,left:200,drag:true,resize:false}
        );
    }

    /**
     *  Function to format property name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyName(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }

    /**
     * Function to format owner name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function ownerName(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }

    function amount(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            return default_currency_symbol + cellValue;
        }
    }

    function accountNameNumber(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            var account_name = rowObject['Account No.'];
            var transation_type = rowObject['transation_type'];
            var bank_account_number = rowObject['bank_account_number'];
            if (transation_type == 'Check' || transation_type == 'Cash'){
                account_name = '';
            } else {
                account_name = account_name+' '+bank_account_number;
            }
            return  account_name;
        }
    }
});