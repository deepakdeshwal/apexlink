$(document).ready(function () {


    setTimeout(function() {
        var name2 = $("#first_name").val();
        var last3 = $("#last_name").val();
        $(".flag_name3424").val(name2 + ' ' + last3);
    },1000);
    getFiles('All');

    /**
     * referral source data fetching
     */
    referralSourceList();
    function referralSourceList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'referralSourceData',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#referral_id').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#referral_id').append($("<option value = "+value.id+">"+value.referral+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * phone type fetching data
     */
    phoneTypeList();
    function phoneTypeList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestPhoneType',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestPhoneType').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestPhoneType').append($("<option value = "+value.id+">"+value.type+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * carrier fetching data
     */
    guestCarrierList();
    function guestCarrierList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'carrierdata',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestCarrier').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestCarrier').append($("<option value = "+value.id+">"+value.carrier+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }


    /**
     * guest card countries fetching data
     */
    guestCountriesList();
    function guestCountriesList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestCountries',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestCountries').empty().append("<option value=''>Select</option>");
                    var html = '';
                    $.each(response.data, function (key, value) {
                        var name = value.name+' ('+value.code+ ')';
                        html += '<option value="' + value.id + '">' +name+ '</option>';
                    });
                    $('#guestCountries').append(html);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest card Ethnicity fetching data
     */
    guestEthnicityList();
    function guestEthnicityList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestEthnicity',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestEthnicity').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestEthnicity').append($("<option value = "+value.id+">"+value.title+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest marital status fetching data
     */
    guestMaritalStatusList();
    function guestMaritalStatusList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestMaritalStatus',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestMarital').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestMarital').append($("<option value = "+value.id+">"+value.marital+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest hobbies fetching data
     */
    guestHobbiesList();
    function guestHobbiesList(){
        var guest_id = $(".guestedit_id").val();
        $.ajax({
            type: 'post',
            url: '/guest-card-edit-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestHobbies',
                guest_id: guest_id

            },
            success: function (response) {
                var response = JSON.parse(response);
                // console.log(response);

                if (response.status == 'success' && response.code == 200) {

                    $('#guestHobbies').empty().append("<option value=''>Select</option>");
                    var hobbyval = response.hobby;
                    $.each(response.data, function (key, value) {
                        // $('#guestHobbies').append($("<option value = "+value.id+" Selected>"+value.hobby+"</option>"));
                        if(hobbyval.indexOf(value.id) > -1){
                            $('#guestHobbies').append($("<option value = "+value.id+" Selected>"+value.hobby+"</option>"));
                        }
                        else
                        {
                            $('#guestHobbies').append($("<option value = "+value.id+">"+value.hobby+"</option>"));
                        }
                    });
                    $("#guestHobbies").multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }


    /**
     * guest veteran status fetching data
     */
    guestVeteranList();
    function guestVeteranList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestVeteranStatus',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestVeteran').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestVeteran').append($("<option value = "+value.id+">"+value.veteran+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest properties DDL
     */
    // guestProperties();
    // function guestProperties(){
    //     $.ajax({
    //         type: 'post',
    //         url: '/guest-card-edit-ajax',
    //         data: {
    //             class: 'GuestCardAjax',
    //             action: 'guestProperty',
    //         },
    //         success: function (response) {
    //             var response = JSON.parse(response);
    //
    //             if (response.status == 'success' && response.code == 200) {
    //                 $('#PropertyEditId').empty().append("<option value=''>Select</option>");
    //                 $.each(response.data, function (key, value) {
    //                     $('#PropertyEditId').append($("<option value = "+value.id+">"+value.property_name+"</option>"));
    //                 });
    //             } else {
    //                 toastr.error(response.message);
    //             }
    //         }
    //     });
    // }

    /**
     * guest card Emergency countries fetching data
     */
    emerCountriesList();
    function emerCountriesList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'emerCountries',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#emerCountries').empty().append("<option value=''>Select</option>");
                    var html = '';
                    $.each(response.data, function (key, value) {
                        var name = value.name+' ('+value.code+ ')';
                        html += '<option value="' + value.id + '">' +name+ '</option>';
                    });
                    $('#emerCountries').append(html);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }


    /**
     * add emergency clone
     */
    $(document).on("click",".add-emergency-contant",function(){

        var clone = $(".tenant-emergency-contact:first").clone();
        clone.find('input[type=text]').val('');
        clone.find('input[name="emergency_phone[]"]').mask('000-000-0000', {reverse: true});
        $(".tenant-emergency-contact").first().after(clone);
        clone.find(".add-emergency-contant").hide();
        clone.find(".remove-emergency-contant ").show();
        clone.find(".remove-emergency-contant .fa-minus-circle").show();
    });

    $(document).on("click",".remove-emergency-contant",function(){
        $(this).parents(".tenant-emergency-contact").remove();
    });


    /**
     * add notes clone
     */
    $(document).on("click", ".guest-notes-plus", function () {
        var length = $(".guest-notes-clone-divclass").length;
        if (length <= 2) {
            var newnode = $("#guest-notes-clone-div").clone().find(".notes").val("").end().insertAfter("div.guest-notes-clone-divclass:last");
            $(".guest-notes-clone-divclass:not(:eq(0))  .guest-notes-plus").remove();
            $(".guest-notes-clone-divclass:not(:eq(0))  .guest-notes-minus").show();
            $(".guest-notes-clone-divclass:not(:eq(0))  .guest-notes-minus .fa-minus-circle").show();
            if (length == 2) {
                $('.guest-notes-plus').hide();
            }
        } else {
            $('.guest-notes-plus').hide();
        }
    });
    $(document).on('click', '.guest-notes-minus', function () {
        $(this).parent().parent().remove();
        $('.guest-notes-plus').show();
    });

    /**  List Action Functions  */
    generalInformationData();
    function generalInformationData(){
        var id = $(".guestedit_user_id").val();

        // console.log(id);
        $.ajax({
            type: 'post',
            url: '/guest-card-edit-ajax',
            data: {
                class: 'GuestCardEditAjax',
                action: 'guestCardUserID',
                user_id : id
            },
            success: function (response) {
                //console.log(response);

                var data = JSON.parse(response);
                setTimeout(function () {
                    edit_date_time(data.data.updated_at);
                },2000);
                if (data.status == "success"){

                    var data2=data.data2;

                    // var html="";
                    //
                    // html += '<option value="'+ +' "> '+data2.unit_prefix+' '+data2.unit_no+' </option>'
                    // $("#unitId").html(html);
                    $('#unitId').append('<option value="'+data2.unit_prefix+' '+data2.unit_no+'">'+data2.unit_prefix+' '+data2.unit_no+'</option>');


                    $('#PropertyEditId').append('<option value="'+data2.property_name+'">'+data2.property_name+'</option>');


                    var html = "";
                    html =  '<thead>'+
                        '<tr style="border:1px solid #C9C9C9;">'+
                        '<th scope="col" class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+'Unit Number'+'</th>'+
                        '<th scope="col" class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+'Property'+' </th>'+
                        '<th scope="col" class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+'Rent'+'</th>'+
                        '<th scope="col" class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+'Security Deposit'+'</th>'+
                        '</tr>'+
                        '</thead>'+
                            '<tr style="border-left:1px solid #afafaf; border-right:1px solid #afafaf;">'+
                            '<td class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+ data2.unit_prefix+' '+data2.unit_no +'</td>'+
                            '<td class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+ data2.property_name+'</td>'+
                            '<td class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+ data2.market_rent+'</td>'+
                            '<td class="col-sm-3" style="border-right:0px solid #dadada;border-bottom:1px solid #afafaf;">'+ data2.security_deposit + '</td>'+
                            '</tr>';

                    $("#unit_table").html(html);

// console.log(data2);
                    $("#salutation").val(data.data.salutation);
                    $("#first_name").val(data.data.first_name);
                    $("#middle_name").val(data.data.middle_name);
                    $("#last_name").val(data.data.last_name);

                    if(data.data.salutation=="Mrs." || data.data.salutation=="Madam" || data.data.salutation=="Sister" || data.data.salutation=="Mother"){
                        $(".maiden_name_hide").show();
                    }
                    $("#maiden_name").val(data.data.maiden_name);
                    $("#nick_name").val(data.data.nick_name);
                    $("#general").val(data.data.gender);
                    $("#ssn").val(data.data.ssn_sin_id);
                    $(".address1").val(data.data.address1);
                    $(".address2").val(data.data.address2);
                    $(".address3").val(data.data.address3);
                    $(".address4").val(data.data.address4);
                    $("#zipcode").val(data.data.zipcode);
                    $("#country").val(data.data.country);
                    $(".state").val(data.data.state);
                    $(".city").val(data.data.city);
                    $(".email").val(data.data.email);
                    //$("#preferred").val(data.data.first_name);
                    $("#referral_id").val(data.data.referral_source);
                    $("#phone_type").val(data.data.phone_type);
                    $("#guestCarrier").val(data.data.carrier);
                    $("#guestCountries").val(data.data.country);
                    $("#phone_number").val(data.data.phone_number);
                    $("#guestEthnicityEdit").val(data.data.ethnicity);
                    $(".guestMarital").val(data.data.maritial_status);
                    $("#guestHobbies").val(data.data.hobbies);
                    $("#guestVeteran").val(data.data.veteran_status);

                    $("#emergency").val(data.data.emergency_contact_name);
                    $("#phoneNumber").val(data.data.phone_number);
                    $(".emergencyInfo").html(data.emergencyInfo);
                    $(".notes").html(data.notes);
                    $(".phone-row-section").html(data.phonetype);
                    $("#movein_date").val(data.data1.expected_move_in);
                    // $(".editssnid").html(data.ssndata);
                    // $("#bedroomId").html(data.html);

                    $("#bedroomId").val(data2.bedrooms_no);
                    $("#floorId").val(data2.floor_no);
                    $("#bathroomId").val(data2.bathrooms_no);

                    $("#min_rent").val(data2.base_rent);
                    $("#max_rent").val(data2.market_rent);
                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
            }
        });
    }
    $(document).on("change","#salutation",function(){
        var value = $(this).val();
        if(value=="Dr.")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("0");
        }
        if(value=="Mr.")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Mrs.")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="Mr. & Mrs.")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("4");
        }
        if(value=="Ms.")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("2");
        }
        if(value=="Sir")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Madam")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="Brother")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Sister")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="Father")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Mother")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="")
        {
            $(".maiden_name_hide").hide();

        }

    });

    $(document).on('click','#add_libraray_file',function(){
        $('#file_library').val('');
        $('#file_library').trigger('click');
    });

    var file_library = [];
    $(document).on('change','#file_library',function(){

        var upload_url = window.location.origin;
        file_library = [];
        $.each(this.files, function (key, value) {
            var type = value['type'];
            var size = isa_convert_bytes_to_specified(value['size'], 'k');
            if(size > 1030) {
                toastr.warning('Please select documents less than 1 mb!');
            } else {
                size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
                if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                    file_library.push(value);
                    var src = '';
                    var reader = new FileReader();
                    $('#file_library_uploads').html('');
                    reader.onload = function (e) {
                        if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                            src = upload_url + '/company/images/excel.png';
                        } else if (type == 'application/pdf') {
                            src = upload_url + '/company/images/pdf.png';
                        } else if (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                            src = upload_url + '/company/images/word_doc_icon.jpg';
                        } else if (type == 'text/plain') {
                            src = upload_url + '/company/images/notepad.jpg';
                        } else if (type == 'text/xml') {
                            src = upload_url + '/company/images/notepad.jpg';
                        } else {
                            src = e.target.result;
                        }
                        $("#file_library_uploads").append(
                            '<div class="row" style="margin:20px">' +
                            '<div class="col-sm-12 img-upload-library-div">' +
                            '<div class="col-sm-3"><img class="img-upload-tab' + key + '" width=100 height=100 src=' + src + '></div>' +
                            '<div class="col-sm-3 show-library-list-imgs-name' + key + '">' + value['name'] + '</div>' +
                            '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data-id="' + value['size'] + '">' +
                            '<div class="col-sm-3 show-library-list-imgs-size' + key + '">' + size + '</div>' +
                            '<div class="col-sm-3"><span id=' + key + ' class="delete_pro_img cursor"><button class="btn-warning">Delete</button></span></div></div></div>');
                    };
                    reader.readAsDataURL(value);
                } else {
                    toastr.warning('Please select file with .xlsx | .pdf | .docx | .txt | .xml extension only!');
                }
            }
        });
    });



    $(document).on('click','#remove_library_file',function(){
        bootbox.confirm("Do you want to remove all files?", function (result) {
            if (result == true) {
                toastr.success('The record deleted successfully.');
                $('#file_library_uploads').html('');
                $('#file_library').val('');
                imgArray = [];
            }
        });
    });


    function isa_convert_bytes_to_specified(bytes, to) {
        var formulas =[];
        formulas['k']= (bytes / 1024).toFixed(1);
        formulas['M']= (bytes / 1048576).toFixed(1);
        formulas['G']= (bytes / 1073741824).toFixed(1);
        return formulas[to];
    }



    function getFiles(status) {

        var tenant_id = $(".guestedit_user_id").val();
        var table = 'tenant_chargefiles';
        var columns = ['Id', 'Name', 'View','File_location','File_extension','Action'];
        var select_column = ['Edit','Delete','Send'];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_where = [{column:'user_id', value :tenant_id,condition:'='}];
        var extra_columns = [];
        var joins = [];
        var columns_options = [
            {name:'Id',index:'id', width:300,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'Name',index:'filename', width:300,searchoptions: {sopt: conditions},table:table},
            {name:'View',index:'file_type', width:300,searchoptions: {sopt: conditions},table:table,formatter:fileFormatter},
            {name:'File_location',index:'file_location', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'File_extension',index:'file_extension', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'file_type', width:300,searchoptions: {sopt: conditions},table:table,formatter:selectFormatter},
        ];


        var ignore_array = [];
        jQuery("#TenantFiles-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'true'
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "File Library",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }



    function fileFormatter(cellValue, options, rowObject)
    {
        var upload_url = window.location.origin;
        if(rowObject !== undefined) {
            var file_type =  rowObject.View;
            var location = rowObject.File_location;
            var path = upload_url+'company/'+location;
            var imageData = '';
            if(file_type == '1'){

                imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
            } else {
                if (rowObject.File_extension == 'xlsx') {
                    src = upload_url + '/company/images/excel.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'pdf') {
                    src = upload_url + '/company/images/pdf.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'docx') {
                    src = upload_url + '/company/images/word_doc_icon.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }else if (rowObject.File_extension == 'txt') {
                    src = upload_url + '/company/images/notepad.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }
            }
            return imageData;
        }
    }

    function selectFormatter(cellValue, options, rowObject)
    {
       // var upload_url = upload_url;
        if(rowObject !== undefined) {
            var file_type =  rowObject.View;
            var location = rowObject.File_location;
            var path = upload_url+'company/'+location;
            var src="";
            var imageData = '';
            if(file_type == '1'){

                imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
            } else {
                if (rowObject.File_extension == 'xlsx') {
                    src = upload_url + 'company/images/excel.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'pdf') {
                    src = upload_url + 'company/images/pdf.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'docx') {
                    src = upload_url + 'company/images/word_doc_icon.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }else if (rowObject.File_extension == 'txt') {
                    src = upload_url + 'company/images/notepad.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }
            }


            var html = "<select editable='1' class='form-control select_options' data-id='"+rowObject.Id+"' data-path = '"+path+"' data-src= '"+src+"'>"

            html +=  "<option value=''>Select</option>";
            html +=  "<option value='Delete'>Delete</option>";
            html +=  "<option value='Send'>SEND</option>";

            html +="</select>";

            return html;

            return imageData;
        }
    }



    $(document).on("click",".saveChargeFile",function(){

        var tenant_id = $(".guestedit_user_id").val();
        var form = $('#addChargeNote')[0];
        var formData = new FormData(form);
        formData.append('action','insertChargeFileNote');
        formData.append('class','TenantPortal');
        formData.append('tenant_id',tenant_id);

        $.ajax({
            url:'/tenantPortal',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == "success"){
                    toastr.success(response.message);
                    setTimeout(function(){
                        $('#TenantFiles-table').trigger('reloadGrid');




                    }, 400);

                }
            },
            cache: false,
            contentType: false,
            processData: false
        });

    });




    $(document).on("change",".files_main table .select_options",function(){
        var action = $(this).val();
        var id = $(this).attr('data-id');

        if(action=="Delete")
        {
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        deleteTableData('tenant_chargefiles',id);
                        setTimeout(function(){
                            $('#TenantFiles-table').trigger('reloadGrid');

                        }, 400);

                    }

                }
            });


        }
        if(action=="Send")
        {
            file_upload_email('users','email',id,1);
        }

    });

    function deleteTableData(tablename,id)
    {

        $.ajax({
            url:'/editTenant?action=deleteRecords&class=EditTenant&id='+id+'&tablename='+tablename,
            type: 'GET',
            success: function (data) {
                var info = JSON.parse(data);
                toastr.success(info.message);

            },
        });

    }

    /**
     * phone number clone general information
     */
    $(document).on("click",".phone-row-section .primary-tenant-phone-row2 .fa-plus-circle",function(){
        var clone = $(".phone-row-section .primary-tenant-phone-row2:first").clone();
        // console.log(clone);
        clone.find('input[type=text]').val(''); //harjinder
        clone.find('input[type=hidden]').val('');
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        $(".phone-row-section .primary-tenant-phone-row2").first().after(clone);
        $(".phone-row-section .primary-tenant-phone-row2:not(:eq(0)) .fa-plus-circle").hide();
        $(".phone-row-section .primary-tenant-phone-row2:not(:eq(0)) .fa-minus-circle").show();
        $(".ext_phone:not(:eq(0))").hide();
        var phoneRowLenght = $(".phone-row-section .primary-tenant-phone-row2");
        console.log(phoneRowLenght.length);
        if (phoneRowLenght.length == 2) {
            clone.find(".fa-plus-circle").hide();
            clone.find(".fa-minus-circle").show();
        }else if (phoneRowLenght.length == 3) {
            clone.find(".fa-minus-circle").show();
            $(".phone-row-section .primary-tenant-phone-row2:eq(0) .fa-plus-circle").hide();
        }else{
            $(".phone-row-section .primary-tenant-phone-row2:not(:eq(0)) .fa-plus-circle").show();
        }
    });
    $(document).on("click",".phone-row-section .primary-tenant-phone-row2 .fa-minus-circle",function(){
        var phoneRowLenght = $(".primary-tenant-phone-row2");
        if (phoneRowLenght.length == 2) {
            $(".primary-tenant-phone-row2:eq(0) .fa-plus-circle").show();
        }else if (phoneRowLenght.length == 3) {
            $(".primary-tenant-phone-row2:eq(0) .fa-plus-circle").hide();
        }else{
            $(".primary-tenant-phone-row2:not(:eq(0)) .fa-plus-circle").show();
        }
        $(this).parents(".primary-tenant-phone-row2").remove();
    });



    /*for multiple SSN textbox*/
    $(document).on("click",".ssn-plus-sign",function(){
        var clone = $(".multipleSsn:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".multipleSsn").first().after(clone);
        $(".multipleSsn:not(:eq(0))  .ssn-plus-sign").remove();
        $(".multipleSsn:not(:eq(0))  .ssn-remove-sign").show();

    });
    /* for multiple SSN textbox*/



    /*remove ssn textbox*/
    $(document).on("click",".ssn-remove-sign",function(){
        $(this).parents(".multipleSsn").remove();
    });
    /*remove ssn textbox*/


    $(document).on("click",".additional_email-plus-sign",function(){
        var emailRowLenght = $(".additional_multipleEmail");

        if (emailRowLenght.length == 2) {
            $(".additional_email-plus-sign").hide();
        }else{
            $(".additional_email-plus-sign").show();
        }
        var clone = $(".additional_multipleEmail:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".additional_multipleEmail").first().after(clone);
        $(".additional_multipleEmail:not(:eq(0))  .additional_email-plus-sign").remove();
        $(".additional_multipleEmail:not(:eq(0))  .additional_email-remove-sign .fa-minus-circle").show();

    });


    $(document).on("click",".additional_email-remove-sign",function(){
        var emailRowLenght = $(".additional_multipleEmail");

        if (emailRowLenght.length == 3 || emailRowLenght.length == 2) {
            $(".additional_email-plus-sign").show();
        }else{
            $(".additional_email-plus-sign").hide();
        }
        $(this).parents(".additional_multipleEmail").remove();
    });





    $(document).on("click",".savelease",function() {
        update_guestCard();
    });

    function update_guestCard(){
        var base_url = window.location.origin;
        var id = $('.guestedit_id').val();
        var formData = $('#update_card :input').serializeArray();
        // var formData2=$('#guestHobbies').val();

        $.ajax({
            type: 'post',
            url: '/guest-card-edit-ajax',
            data: {
                class: 'GuestCardEditAjax',
                action: 'update',
                formData : formData,
                unit_id : id

            },
            success: function (response) {
                var data = JSON.parse(response);

                if (data.status == "success") {
                    localStorage.setItem("rowcolorGuest", "rowcolorGuest");
                    localStorage.setItem("messageGuest", "messageGuest");
                    window.location.href = base_url +'/GuestCard/ListGuestCard';
                        }
                else {
                    toastr.error(data.message);
                }
            }
        });
    }

    guestCountriesList();
    function guestCountriesList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestCountries',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestCountries').empty().append("<option value='0'>Select</option>");
                    var html = '';
                    $.each(response.data, function (key, value) {
                        var name = value.name+' ('+value.code+ ')';
                        html += '<option value="' + value.id + '">' +name+ '</option>';
                    });
                    $('#guestCountries').append(html);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }


});
$(document).on("click", "#add_company_button_cancel", function (e) {
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                var base_url = window.location.origin;
                window.location.href = base_url + '/GuestCard/ListGuestCard';
            }
        }
    });
});




