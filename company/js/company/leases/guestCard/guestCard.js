$(document).ready(function () {

    if(localStorage.getItem("rowcolorGuest")){
        setTimeout(function(){
            jQuery('#guestcard_table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('#guestcard_table').find('tr:eq(1)').find('td:eq(9)').addClass("green_row_right");
            localStorage.removeItem('rowcolorGuest');
        }, 2000);
    }

    if(localStorage.getItem("messageGuest")){
        setTimeout(function(){
            toastr.success("This record updated successfully");
            localStorage.removeItem('messageGuest');
        }, 2000);
    }

    jQuery('.phone_format').mask('000-000-0000', {reverse: true});
    $('.number_only').keydown(function (e) {
        if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return false;
        }
    });
    /**
     * for multiple SSN textbox
     */
    $(document).on("click","#multipleSsn .ssn-plus-sign",function(){
        var clone = $(".multipleSsn:first").clone();
        clone.find('input[type=text]').val('');
        $(".multipleSsn").first().after(clone);
        $(".multipleSsn:not(:eq(0))  .ssn-plus-sign").remove();
        $(".multipleSsn:not(:eq(0))  .ssn-remove-sign").show();
    });
    /**
     * remove
     */
    $(document).on("click",".ssn-remove-sign",function(){
        $(this).parents(".multipleSsn").remove();
    });

    /**
     * multiple mail textbox
     */
    $(document).on("click",".email-plus-sign",function(){
        var emailRowLenght = $(".multipleEmail");
        if (emailRowLenght.length == 2) {
            $(".email-plus-sign .fa-plus-circle").hide();
        }
        var clone = $(".multipleEmail:first").clone();
        clone.find('input[type=text ]').val('');
        clone.find('input[type=text ]').attr('disabled',false);
        $(".multipleEmail").first().after(clone);
        $(".multipleEmail:not(:eq(0))  .email-plus-sign .fa-plus-circle").remove();
        $(".multipleEmail:not(:eq(0))  .email-remove-sign .fa-minus-circle").show();
    });

    /**
     * remove multiple textbox
     */
    $(document).on("click",".email-remove-sign",function(){
        var emailRowLenght = $(".multipleEmail");
        if (emailRowLenght.length == 3 || emailRowLenght.length == 2) {
            $(".email-plus-sign .fa-minus-circle").show();
        }
        $(this).parents(".multipleEmail").remove();
    });

    /**
     * referral source data fetching
     */
    referralSourceList();
    function referralSourceList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'referralSourceData',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#referral_id').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#referral_id').append($("<option value = "+value.id+">"+value.referral+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * phone type fetching data
     */




    //
    // /**
    //  *Get location on the basis of zipcode
    //  */
    // function getAddressInfoByZip1(zip) {
    //
    //     if (zip.length >= 5 && zip != 'undefined') {
    //         var addr = {};
    //         $.ajax({
    //             type: 'post',
    //             url: '/common-ajax',
    //             data: {zip_code: zip,
    //                 class: 'CommonAjax',
    //                 action: 'getZipcode'},
    //             success: function (res) {
    //                 response1(res);
    //
    //             },
    //         });
    //
    //     } else {
    //         response1({success: false});
    //     }
    // }
    //
    // /**
    //  * Set values in the city, state and country when focus loose from zipcode field.
    //  */
    // function response1(obj) {
    //     var objres = JSON.parse(obj);
    //     if (objres.status == 'success') {
    //         $('#city').val(objres.city);
    //         $('#state').val(objres.state);
    //         $('#country').val(objres.country);
    //     }
    // }
    //
    //
    // $(function () {
    //
    //
    //     /**
    //      * Auto fill the city, state and country when focus loose on zipcode field.
    //      */
    //     $("#zipcode").focusout(function () {
    //         getAddressInfoByZip1($(this).val());
    //     });
    //
    //     /**
    //      * Auto fill the city, state and country when enter key is clicked.
    //      */
    //     $("#zipcode").keydown(function (event) {
    //         var keycode = (event.keyCode ? event.keyCode : event.which);
    //         if (keycode == '13') {
    //             getAddressInfoByZip1($(this).val());
    //         }
    //     });
    //
    //
    // });
    //
    //
    //
    //
    // /**
    //  * Auto fill the city, state and country when focus loose on zipcode field.
    //  */
    // $(function () {       $('#addForwordingAddress_Moveout #zipcode').on('keyup keypress', function (e) {
    //     var keyCode = e.keyCode || e.which;
    //     if (keyCode === 13) {
    //         e.preventDefault();
    //         return false;
    //     }
    // });       /**
    //  * Auto fill the city, state and country when focus loose on zipcode field.
    //  */
    // $("#addForwordingAddress_Moveout #FrwdAddress_Zip").focusout(function () {
    //     getZipCode('#zipcode',$(this).val(),'#city','#state','#country','','#zip_code_validate');
    //     // getAddressInfoByZip($(this).val());
    // });       /**
    //  * Auto fill the city, state and country when enter key is clicked.
    //  */
    // $("#addForwordingAddress_Moveout #FrwdAddress_Zip").keydown(function (event) {
    //     var keycode = (event.keyCode ? event.keyCode : event.which);
    //     if (keycode == '13') {
    //         getZipCode('#zipcode',$(this).val(),'#city','#state','#country','','#zip_code_validate');
    //     }
    // });
    // });
    // /**
    //  * Auto fill the city, state and country when focus loose on zipcode field.
    //  */
    //
    //
    //




    phoneTypeList();
    function phoneTypeList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestPhoneType',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestPhoneType').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestPhoneType').append($("<option value = "+value.id+">"+value.type+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * carrier fetching data
     */
    guestCarrierList();
    function guestCarrierList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'carrierdata',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestCarrier').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestCarrier').append($("<option value = "+value.id+">"+value.carrier+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }


    /**
     * guest card countries fetching data
     */

    guestCountriesList();
    function guestCountriesList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestCountries',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestCountries').empty().append("<option value='0'>Select</option>");
                    var html = '';
                    $.each(response.data, function (key, value) {
                        var name = value.name+' ('+value.code+ ')';
                        if (value.id  == '220'){
                            html += '<option value="' + value.id + '" selected>' +name+ '</option>';
                        }else{
                            html += '<option value="' + value.id + '">' +name+ '</option>';
                        }
                    });
                    $('#guestCountries').append(html);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest card Ethnicity fetching data
     */

    guestEthnicityList();
    function guestEthnicityList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestEthnicity',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestEthnicity').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestEthnicity').append($("<option value = "+value.id+">"+value.title+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest marital status fetching data
     */
    guestMaritalStatusList();
    function guestMaritalStatusList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestMaritalStatus',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestMarital').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestMarital').append($("<option value = "+value.id+">"+value.marital+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest hobbies fetching data
     */
    guestHobbiesList();
    function guestHobbiesList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestHobbies',
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
               //     $('#guestHobbies').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestHobbies').append($("<option value = "+value.id+">"+value.hobby+"</option>"));
                    });
                    $("#guestHobbies").multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest veteran status fetching data
     */
    guestVeteranList();
    function guestVeteranList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestVeteranStatus',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestVeteran').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestVeteran').append($("<option value = "+value.id+">"+value.veteran+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * clone phone number
     */
    $(document).on("click",".primary-tenant-phone-row .fa-plus-circle",function(){
        var clone = $(".primary-tenant-phone-row:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".primary-tenant-phone-row").first().after(clone);
        $(".primary-tenant-phone-row:not(:eq(0))  .fa-plus-circle").hide();
        $(".primary-tenant-phone-row:not(:eq(0))  .fa-minus-circle").show();
        var phoneRowLenght = $(".primary-tenant-phone-row");
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        $(".ext_phone:not(:eq(0))").hide();
        if (phoneRowLenght.length == 2) {
            clone.find(".primary-tenant-phone-row .fa-plus-circle").hide();
        }else if (phoneRowLenght.length == 3) {
            clone.find(".fa-plus-circle").hide();
            $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").hide();
        }else{
            $(".primary-tenant-phone-row:not(:eq(0)) .fa-plus-circle").show();
        }
    });

    $(document).on("click",".primary-tenant-phone-row .fa-minus-circle",function(){

        var phoneRowLenght = $(".primary-tenant-phone-row");
        if (phoneRowLenght.length == 2) {
            $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").show();
        }else if (phoneRowLenght.length == 3) {
            $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").hide();
        }else{
            $(".primary-tenant-phone-row:not(:eq(0)) .fa-plus-circle").show();
        }

        $(this).parents(".primary-tenant-phone-row").remove();
    });
    $(document).on("click","#phone_type",function(){
        var val= $(this).val();
        if(val=='2' || val=='5'){
            $(this).parents(".primary-tenant-phone-row").find(".ext_phone").show();
        }else{
            $(this).parents(".primary-tenant-phone-row").find(".ext_phone").hide();
        }
    });
    /**
     * general information Insert Data
     */
    $(document).on("click",".savelease",function(){
        var check = jQuery.trim($("#search_records").html());
        var check2=  $("input[name='unit_id']:checked").length;
        if(check=="")
        {
            toastr.error("Please search and select a Unit.");
        }
        if(check!="" && check2==0){
            toastr.error("Please search and select a Unit.");
        }

        if(check!="" && check2==1){

            generalInformationData();
        }
     //   generalInformationData();
    });
    function generalInformationData(){
        var formData = $('#generallease').serializeArray();
        var formData2 = $('#emergency_details').serializeArray();
        var notes=$('#notes_form').serializeArray();
        var expected_move_in=$('.expected_move_in').val();
        console.log(expected_move_in);
        var checked=$("input:radio[name=unit_id]:checked").val();
        var unit_id = 0;
        if(checked!=undefined) {
            console.log("2");
            if(checked=='on') {
                console.log("3");
                var checked1=$("input:radio[name=unit_id]:checked");
                 unit_id = checked1.parent().next().text();         // Retrieves the text within <td>
            }
            else
            {
                console.log("4");
                unit_id=0;
            }
            console.log("5");
        }else{
            console.log("6");
            unit_id = 0;
        }

        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'insert',
                formData: formData,
                unit_id: unit_id,
                formData2:formData2,
                notes: notes,
                expected_move_in: expected_move_in
            },
            success: function (response) {
                var data = JSON.parse(response);

                if (data.status == "success") {
                    $("#salutation").val('');
                    /*toastr.success("This record saved successfully");*/
                    localStorage.setItem("rowcolorGuest", "rowcolorGuest");
                    localStorage.setItem("messageGuest", "messageGuest");
                    window.location.href = window.location.origin+"/GuestCard/ListGuestCard";
                    /*setTimeout(function () {
                        localStorage.setItem("rowcolorTenant",true);
                        onTop(true);
                        window.location.href = window.location.origin+"/GuestCard/ListGuestCard";
                    }, 500);*/

                }if(data.status == "errorr"){
                    toastr.warning(data.message);
                }

                if(data.status =="error"){
                    toastr.warning(data.messsage);
                    var firstName=$("#first_name").val();
                    var lastName=$("#last_name").val();
                    var email=$(".emailll").val();
                    var carrier=$("#guestCarrier").val();
                    var phone=$(".phoner").val();

                    if(firstName==""){
                        $(".val_message_first").show();
                    }
                    else
                    {
                        $(".val_message_first").hide();
                    }
                    if(lastName==""){
                        $(".val_message_last").show();
                    }else{
                        $(".val_message_last").hide();
                    }
                    if(email==""){
                        $(".val_message_email").show();
                    }
                    else{
                        $(".val_message_email").hide();
                    }
                    if(carrier==""){
                        $(".val_message_carrier").show();
                    }
                    else{
                        $(".val_message_carrier").hide();
                    }
                    if(phone==""){
                        $(".val_message_phone").show();
                    }
                    else{
                        $(".val_message_phone").hide();
                    }
                }
            }
        });
    }

    /**
     * guest properties DDL
     */
    guestProperties();
    function guestProperties(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestProperty',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#PropertyId').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#PropertyId').append($("<option value = "+value.id+">"+value.property_name+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }
    $(document).on("change","#PropertyId",function () {
        var prop_id =$('#PropertyId').val();;
        guestUnit(prop_id);
    });

    /**
     * guest unit DDL
     * @param prop_id
     */
    function guestUnit(prop_id){
        $('#PropertyId').val();
        //console.log($('#PropertyId').val());
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestUnitBuilding',
                prop_id: prop_id
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#unitId').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#unitId').append($("<option value = "+value.id+">"+value.unit_prefix+"-"+value.unit_no+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * search unit preferences guest card
     */
    $(document).on("click","#guestSearch",function(){
        getTableUnitPreferences();
        $("#search_table").show();
    });

    /**
     * unit preferences table listing
     */
    function getTableUnitPreferences(){
        var prop_id=$("#PropertyId").val();
        var unit_id=$("#unitId").val();
        var bed_id=$("#bedroomId").val();
        var bath_id=$("#bathroomId").val();
        var floor_id=$("#floorId").val();
        var move_in_id=$(".expected_move_in").val();
        var min_id=$("#min_rent").val();
        var max_id=$("#max_rent").val();
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: "GuestCardAjax",
                action: "unitPreferencesListing",
                prop_id:prop_id,
                unit_id:unit_id,
                bed_id:bed_id,
                bath_id:bath_id,
                floor_id:floor_id,
                move_in_id:move_in_id,
                min_id:min_id,
                max_id:max_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var currencySymbol = "("+currencySign+")";
                //console.log(response);
                var html = "";
                html +=   '<thead>'+
                    '<tr>'+
                    '<th scope="col"><center>'+'</center></th>'+
                    '<th style="display: none" scope="col"><center>'+'ID'+'</center></th>'+
                    '<th scope="col"><center>'+'Unit Number'+'</center></th>'+
                    '<th scope="col"><center>'+'Property'+'</center> </th>'+
                    '<th scope="col"><center>'+'Rent' + currencySymbol +'</center> </th>'+
                    '<th scope="col"><center>'+'Security Deposit'+ currencySymbol + '</center> </th>'+
                    '</tr>'+
                    '</thead>';
                for (var i = 0; i < response.data.length; i++) {
                    html +=
                        '<tr>'+
                        '<td>'+ '<input type="radio" name="unit_id" id="checked_unit">' +'</td>'+
                        '<td style="display: none"> '+response.data[i].id+'</td>'+
                        '<td>'+ response.data[i].unit_no +'</td>'+
                        '<td>'+ response.data[i].property_name +'</td>'+
                        '<td>'+currencySign+''+response.data[i].market_rent + '</td>'+
                        '<td>'+currencySign+''+ response.data[i].security_deposit + '</td>'+
                        '</tr>';
                }
                $("#search_records").html(html);
            },
            error:function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /**
     * unit preferences row data save
     */
    $.ajax({
        type: 'post',
        url: '/guest-card-ajax',
        data: {
            class: 'GuestCardAjax',
            action: 'unitRowRecord',
        },
        success: function (response) {
            var response = JSON.parse(response);

            if (response.status == 'success' && response.code == 200) {

            } else {
                toastr.error(response.message);
            }
        }
    });

    /**
     * guest card Emergency countries fetching data
     */

    emerCountriesList();
    function emerCountriesList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'emerCountries',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#emerCountries').empty().append("<option value=''>Select</option>");
                    var html = '';
                    $.each(response.data, function (key, value) {
                        var name = value.name+' ('+value.code+ ')';
                        if(value.id  == '220'){
                            html += '<option value="' + value.id + '" selected>' +name+ '</option>';
                        }else{
                            html += '<option value="' + value.id + '">' +name+ '</option>';
                        }
                    });
                    $('#emerCountries').append(html);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * emergency contact detail clone
     */
    $(document).on("click",".add-emergency-contant",function(){
        var clone = $(".lease-emergency-contact:first").clone();
        clone.find('input[type=text]').val('');
        clone.find('input[name="emergency_phone[]"]').mask('000-000-0000', {reverse: true});;
        $(".lease-emergency-contact").first().after(clone);
        clone.find(".add-emergency-contant").hide();
        clone.find(".remove-emergency-contant").show();
    });
    $(document).on("click",".remove-emergency-contant",function(){
        $(this).parents(".lease-emergency-contact").remove();
    });


    $(document).on("click",".addNote",function(){

        var clone = $(".chargeNoteHtml:first").clone();
        clone.find('input[type=textarea]').val(''); //harjinder
        $(".chargeNoteHtml").first().after(clone);
        $(".chargeNoteHtml:not(:eq(0))  .addNote").remove();
        $(".chargeNoteHtml:not(:eq(0))  .removeNote").show();


    });



    $(document).on("click",".removeNote",function(){
        var phoneRowLenght = $(".primary-tenant-phone-row");
        $(this).parents(".chargeNoteHtml").remove();
    });

    $(document).on('click','#add_libraray_file',function(){
        $('#file_library').val('');
        $('#file_library').trigger('click');
    });
    var file_library = [];
    $(document).on('change','#file_library',function(){
        file_library = [];
        $.each(this.files, function (key, value) {
            var type = value['type'];
            var size = isa_convert_bytes_to_specified(value['size'], 'k');
            var upload_url = window.location.origin;
            if(size > 1030) {
                toastr.warning('Please select documents less than 1 mb!');
            } else {
                size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
                if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                    file_library.push(value);
                    var src = '';
                    var reader = new FileReader();
                    $('#file_library_uploads').html('');
                    reader.onload = function (e) {
                        if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                            src = upload_url + '/company/images/excel.png';
                        } else if (type == 'application/pdf') {
                            src = upload_url + 'company/images/pdf.png';
                        } else if (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                            src = upload_url + 'company/images/word_doc_icon.jpg';
                        } else if (type == 'text/plain') {
                            src = upload_url + 'company/images/notepad.jpg';
                        } else if (type == 'text/xml') {
                            src = upload_url + 'company/images/notepad.jpg';
                        } else {
                            src = e.target.result;
                        }
                        $("#file_library_uploads").append(
                            '<div class="row" style="margin:20px">' +
                            '<div class="col-sm-12 img-upload-library-div">' +
                            '<div class="col-sm-3"><img class="img-upload-tab' + key + '" width=100 height=100 src=' + src + '></div>' +
                            '<div class="col-sm-3 show-library-list-imgs-name' + key + '">' + value['name'] + '</div>' +
                            '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                            '<div class="col-sm-3 show-library-list-imgs-size' + key + '">' + size + '</div>' +
                            '<div class="col-sm-3"><span id=' + key + ' class="delete_pro_img cursor"><button class="btn-warning">Delete</button></span></div></div></div>');
                    };
                    reader.readAsDataURL(value);
                } else {
                    toastr.warning('Please select file with .xlsx | .pdf | .docx | .txt | .xml extension only!');
                }
            }
        });
    });

    function isa_convert_bytes_to_specified(bytes, to) {
        var formulas =[];
        formulas['k']= (bytes / 1024).toFixed(1);
        formulas['M']= (bytes / 1048576).toFixed(1);
        formulas['G']= (bytes / 1073741824).toFixed(1);
        return formulas[to];
    }
    $(document).on('click','.delete_pro_img',function(){
        $(this).parent().parent().parent('.row').remove();
    });

    $(document).on('click','#remove_library_file',function(){
        $('#file_library_uploads').html('');
        $('#file_library').val('');
    });

    /*Changes on 22/08/19 Start*/
    $(document).on("change","#salutation",function(){
        var value = $(this).val();
        if(value=="Dr.")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("0");
        }
        if(value=="Mr.")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Mrs.")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="Mr. & Mrs.")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("4");
        }
        if(value=="Ms.")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="Sir")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Madam")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="Brother")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Sister")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="Father")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Mother")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="")
        {
            $(".maiden_name_hide").hide();

        }

    });

    $(document).on("click", "#add_company_button_cancel", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    var base_url = window.location.origin;
                    window.location.href = base_url + '/GuestCard/ListGuestCard';
                }
            }
        });
    });
    /*Changes on 22/08/19 End*/

    $(".reset-btn").click(function(){
        bootbox.confirm({
            message: "Do you want to Reset?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    var base_url = window.location.origin;
                    window.location.href = base_url + '/GuestCard/NewGuestCard';
                }
            }
        });
    });

    /*Changes Country code*/
    $(function () {
        $('#generallease .zipcode').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $("#generallease .zipcode").focusout(function () {
            getZipCode('.zipcode',$(this).val(),'.city','.state','.country','','#zip_code_validate');
            // getAddressInfoByZip($(this).val());
        });

        $("#generallease .zipcode").keydown(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                getZipCode('.zipcode',$(this).val(),'#FrwdAddress_City','#FrwdAddress_State','#FrwdAddress_Country','','#zip_code_validate');
            }
        });
    });
    /*Changes Country code*/

});