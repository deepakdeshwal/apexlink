$(document).on("change",".range",function() {

    getAffordability();
})
function getAffordability(){
    var annualIncome = 70000;
    var monthlyDebts = 250;
    var downPayment = 20000;

    var debtToIncome = 36;
    var interestRate = 3.875;
    var loanTerm = 30;

    var incTaxIns = true;
    var taxes = 2800;
    var taxPercent = 0.8;
    var homeInsurance = 800;
    var incPMI = true;
    var hoaDues = 0;

    var payment = (annualIncome / 12) * (debtToIncome / 100) - monthlyDebts*1;
    var yrs = loanTerm * 12;
    var rte = interestRate / 1200;
    var pmt = payment;
    var affordableValue = 308122;

    $(".payment").text(payment);

    if(debtToIncome <= 36 ){
        $('.affordable-txt').html('<span>Based on your income, a house at this price should fit comfortably within your budget.</span>')

        $(".payment").addClass("label-success");

    }else if(debtToIncome > 36){
        $('.affordable-txt').html('<span>Based on your income, a house at this price may stretch your budget too thin.</span>')
        $(".range").addClass("warning");
        $(".payment").addClass("label-warning");
    }

    if(incTaxIns){
        pmt = payment - ((taxes*1 / 12) + (homeInsurance*1/12));
    }
    pmt = pmt - hoaDues*1;
    var amt = pmt/(rte+(rte/(Math.pow((1+rte),(yrs))-1)));
    if (incPMI && incTaxIns) {
        //80: 0, 85: .0044, 90: .0059, 95: .0076, 100: .0098
        //.007104
        pmt = pmt - (amt * .0071043 / 12);
        amt = pmt/(rte+(rte/(Math.pow((1+rte),(yrs))-1)));
    }
    if(amt < 1){amt = 0;}
    if(payment < 300){payment = 300;}

     affordableValue = amt + downPayment*1;



    $('.affordable-number').html(affordableValue);



}