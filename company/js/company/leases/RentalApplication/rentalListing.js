$(document).ready(function () {
    $.ajax({
        type: 'post',
        url: '/RentalApplication/Ajax',
        data: {
            class: "RenatlApplicationAjax",
            action: "getcount",

        },
        success: function (response) {
            var response = JSON.parse(response);
           $(".status1").text(response.data.active.length);
           $(".status3").text(response.data.approved.length);
           $(".status4").text(response.data.declined.length);
           $(".status5").text(response.data.lease_generated.length);

        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
    if(localStorage.getItem("rowcolorRental")){
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(5)').addClass("green_row_right");
            localStorage.removeItem('rowcolorRental');
            console.log("check2");
        }, 2000);
    }

    if(localStorage.getItem("table_green_tableid")){
        setTimeout(function(){
            var tableId = localStorage.getItem("table_green_tableid")
            jQuery(tableId).find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery(tableId).find('tr:eq(1)').find('td:eq(5)').addClass("green_row_right");
            localStorage.removeItem('table_green_id');
            localStorage.removeItem('table_green_tableid');
        }, 2000);
    }

    function statusFormatter1(cellValue, options, rowObject) {
        console.log(cellValue);

        if (cellValue == '1')
            return "Active";
        else if (cellValue == '2')
            return "Archived";
        else if (cellValue == '3')
            return "Lease Generated";
        else if (cellValue == '4')
            return "Approved";
        else if (cellValue == '5')
            return "Declined";
        else
            return '';
    }

    $(document).on('click','.active_class',function(){
        $('#rental_application_table').jqGrid('GridUnload');
        jqGrid('All','1');
    });
    $(document).on('click','.lease_generated_class',function(){
        $('#rental_application_table').jqGrid('GridUnload');
        jqGrid('All','3');
    });
    $(document).on('click','.approved_class',function(){
        $('#rental_application_table').jqGrid('GridUnload');
        jqGrid('All','4');
    });
    $(document).on('click','.declined_class',function(){
        $('#rental_application_table').jqGrid('GridUnload');
        jqGrid('All','5');
    });
    $(document).on('click','#AZ',function(){
        $('.AZ').hide();
        $('#apex-alphafilter').show();
    });

    $(document).on('click','#allAlphabet',function(){
        var grid = $("#rental_application_table");
        $('.AZ').show();
        $('#apex-alphafilter').hide();
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        //$('#companyUser-table').trigger( 'reloadGrid' );
    });

    $(document).on('click','.getAlphabet',function(){
        var grid = $("#rental_application_table"),f = [];
        var value = $(this).attr('data_id');
        var search = $(this).text();
        if(value != '0'){
            f.push({field:"name",op:"bw",data:search});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
        }
    });

    alphabeticSearch();
    function alphabeticSearch(){
        $.ajax({
            type: 'post',
            url:'/Companies/List/jqgrid',
            data: {class: 'jqGrid',
                action: "alphabeticSearch",
                table: 'users',
                column: 'name',
                where: [{column:'user_type',value:'10',condition:'=',table:'users'}]},
            success : function(response){
                var response = JSON.parse(response);
                console.log(response);
                if(response.code == 200){
                    var html = '';

                    $.each(response.data, function(key,val) {
                        var color = '#05A0E4';
                        if(val == 0) color = '#c5c5c5';
                        html += '<span class="getAlphabet" style="color:'+color+'" data_id="'+val+'">'+key+'</span>';
                    });
                    $('.AtoZ').html(html);
                }
            }
        });
    }
    $(document).on('click', '.tab-content #rental-application .sub-tabs ul li a', function () {
        var grid = $("#rental_application_table"), f = [];
        var value = $(this).attr('data_id');
        console.log(value);
        var search = $(this).text();
        if (value != '1') {
            f.push({field: "company_rental_applications.status", op: "eq", data: value});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData, {filters: JSON.stringify(f)});
            grid.trigger("reloadGrid", [{page: 1, current: true}]);
        }
    });
    function actionFmatter1(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var select = '';

            if(rowObject['Status'] == '1')  select = ['Edit', 'Convert Applicant To Tenant', 'Email', 'Email History', 'Text', 'Text History','Flag Bank', 'File Library', 'Notes History', 'Run Background Check', 'Archive Applicant','Delete Applicant'];
            if(rowObject['Status'] == '2')  select = ['Run Background Check','Activate Applicant','Delete Applicant'];
            if(rowObject['Status'] == '4')  select = ['Edit', 'Convert Applicant To Tenant','Create Tenant', 'Email', 'Email History', 'Text', 'Text History','Flag Bank', 'File Library', 'Notes History', 'Run Background Check', 'Archive Applicant','Delete Applicant'];
            if(rowObject['Status'] == '5')  select = ['Edit', 'Convert Applicant To Tenant', 'Email', 'Email History', 'Text', 'Text History','Flag Bank', 'File Library', 'Notes History', 'Run Background Check', 'Archive Applicant','Delete Applicant'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '" data-user_id="' + rowObject.user_id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    $('.rental_type_status #jqgridOptions').on('change', function () {
        var selected = this.value;
        $('#rental_application_table').jqGrid('GridUnload');
        jqGrid(selected);
    });

    if(localStorage.getItem('ElasticSearch')){
        var elasticSearchData = localStorage.getItem('ElasticSearch');
        setTimeout(function(){
            var grid = $("#rental_application_table"),f = [];
            f.push({field: "users.id", op: "eq", data: elasticSearchData,int:'true'});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
            localStorage.removeItem('ElasticSearch');
        },1500);
    }

    jqGrid('All');
    function jqGrid(status,check, deleted_at) {
        if(jqgridNewOrUpdated == 'true'){
            var sortOrder = 'desc';
            var sortColumn = 'users.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }
        var table = 'users';
        var columns = ['Applicant Name', 'Property Name', 'Unit', ' Move In Date', 'Status', 'Action'];
        var select_column = ['Edit', 'Convert Applicant To Tenant', 'Email', 'Email History', 'Text', 'Text History','Flag Bank', 'File Library', 'Notes History', 'Run Background Check', 'Archive Applicant','Delete Applicant'];
        var joins = [{table: 'users', column: 'id', primary: 'user_id', on_table: 'company_rental_applications'},{table: 'company_rental_applications', column: 'prop_id', primary: 'id', on_table: 'general_property'},{table: 'company_rental_applications', column: 'unit_id', primary: 'id', on_table: 'unit_details'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['unit_details.unit_no'];
        if(check==1){
            var extra_where = [{column: 'user_type', value: '10', condition: '=', table: 'users'},{column: 'record_status', value: '1', condition: '=', table: 'users'},{column: 'status', value: '1', condition: '=', table: 'company_rental_applications'}];
        }
       else if(check==3){
            var extra_where = [{column: 'user_type', value: '10', condition: '=', table: 'users'},{column: 'record_status', value: '1', condition: '=', table: 'users'},{column: 'status', value: '3', condition: '=', table: 'company_rental_applications'}];
        }
        else if(check==4){
            var extra_where = [{column: 'user_type', value: '10', condition: '=', table: 'users'},{column: 'record_status', value: '1', condition: '=', table: 'users'},{column: 'status', value: '4', condition: '=', table: 'company_rental_applications'}];
        }
        else if(check==5){
            var extra_where = [{column: 'user_type', value: '10', condition: '=', table: 'users'},{column: 'record_status', value: '1', condition: '=', table: 'users'},{column: 'status', value: '5', condition: '=', table: 'company_rental_applications'}];
        }
        else{
            var extra_where = [{column: 'user_type', value: '10', condition: '=', table: 'users'},{column: 'record_status', value: '1', condition: '=', table: 'users'}];
        }

        // var pagination =[];

        var columns_options = [

            /*{name:'Prospect Name',index:'name', width:90,align:"center",searchoptions: {sopt: conditions},table:'users',},*/
            {name:'Applicant Name',index:'name', width:90,align:"left",searchoptions: {sopt: conditions},table:table},
            {name:'Property Name',index:'property_name', width:100,searchoptions: {sopt: conditions},table:'general_property'},
            {name:'Unit',index:'unit_prefix', width:80, align:"center",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
            {name:'Move In Date',index:'exp_move_in',searchoptions: {sopt: conditions},table:'company_rental_applications',change_type:'date'},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:'company_rental_applications',formatter: statusFormatter1},
            {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFmatter1},

        ];
        var ignore_array = [];
        jQuery("#rental_application_table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "users",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Rental Applications",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }

    $(document).on("change", "#rental_application_table .select_options", function (e) {
        e.preventDefault();
        var base_url = window.location.origin;
        var action = this.value;
        var id = $(this).attr('data_id');
        var name = $("tr#"+id).find("td").eq(0).text();
        var elem = $(this);
        switch (action) {
            case "Edit":
                $(".hidden_name_val").val(name);
                window.location.href = base_url + '/RentalApplication/EditRentalApplications?rental_id='+id;
                break;
            case "Delete Applicant":

                bootbox.confirm({
                    message: "Are you sure you want to delete this entry?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                           deleteRentalApplication(id);
                        }
                    }
                });
                break;
            case "Archive Applicant":
                archiveRentalApplication(id);
                break;

            case "Activate Applicant":
                activeRentalApplication(id);
                break;
            case "Convert Applicant To Tenant":
                window.location.href = base_url + '/Lease/Leases?id='+id;
                break;
            case "Flag Bank":
                $("#rental_hide").hide();
                $("#rental_show").show();
                $("#hidden_id_view").val(id);
                $(".hidden_name_val").val(name);
                viewrentalapplication(id);
                additionalInc('All');
                getFiles('All');
                jqGrid_Occupants('All');
                $('html, body').animate({ scrollTop: $('#flags').offset().top }, 1000);
                break;

                case "File Library":
                $("#rental_hide").hide();
                $("#rental_show").show();
                $("#hidden_id_view").val(id);
                viewrentalapplication(id);
                additionalInc('All');
                getFiles('All');
                jqGrid_Occupants('All');
                $('html, body').animate({ scrollTop: $('#filelibrary').offset().top }, 1000);
                break;
                case "Notes History":
                $("#rental_hide").hide();
                $("#rental_show").show();
                $("#hidden_id_view").val(id);
                viewrentalapplication(id);
                additionalInc('All');
                getFiles('All');
                jqGrid_Occupants('All');
                $('html, body').animate({ scrollTop: $('#notes_div').offset().top }, 1000);
                break;
                case "Create Tenant":
                    window.location.href = base_url + '/Tenantlisting/add?id='+id;
                    break;
                case "Email":
                    var data_email = "";
                    localStorage.setItem('predefined_mail',data_email);
                    localStorage.setItem('table_green_id',id);
                    localStorage.setItem('table_green_tableid', '#rental_application_table');
                    localStorage.setItem('table_green_url','/RentalApplication/RentalApplications/');
                    window.location.href = base_url + '/Communication/ComposeEmail';
                    break;
                case "Email History":
                    window.location.href = base_url + '/Communication/SentEmails';
                    break;
                case "Text":
                    localStorage.setItem('table_green_id',id);
                    localStorage.setItem('table_green_tableid', '#rental_application_table');
                    localStorage.setItem('table_green_url','/RentalApplication/RentalApplications/');
                    window.location.href = base_url + '/Communication/AddTextMessage';
                    break;
                case "Text History":
                    window.location.href = base_url + '/Communication/TextMessage';
                    break;
                case "Run Background Check":
                    $('#backGroundCheckPopCondition').modal('show');
                    $('#btnBackgroundSelection').attr('tent_id',id);
                    elem.val('default');
                    break;
            default:
                window.location.href = base_url + '/Setting/ApexNewUser';
        }
    });
    $(document).on('click','#rental_application_table tr td:not(:last-child)',function(e){
        e.preventDefault();
        var base_url = window.location.origin;
        var id = $(this).closest("tr").attr('id');
        $("#rental_hide").hide();
        $("#rental_show").show();
        $("#hidden_id_view").val(id);
        viewrentalapplication(id);
        additionalInc('All');
        getFiles('All');
        jqGrid_Occupants('All');

        /* ('tr td:not(:last-child)')*/
    });
    function deleteRentalApplication(id) {
        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: "RenatlApplicationAjax",
                action: "deleteRentalApplication",
                id:id
            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);
                $('#rental_application_table').trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    function archiveRentalApplication(id) {
        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: "RenatlApplicationAjax",
                action: "archiveRentalApplication",
                id:id
            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);
                $('#rental_application_table').trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    function activeRentalApplication(id) {
        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: "RenatlApplicationAjax",
                action: "activeRentalApplication",
                id:id
            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);
                $('#rental_application_table').trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    function viewrentalapplication(id) {
        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: "RenatlApplicationAjax",
                action: "viewrentalapplication",
                id:id
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log(response);
                var expected_move_in=(response.data1.exp_move_in);
                var dateParts = expected_move_in.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var move_in_date= $.datepicker.formatDate(jsDateFomat, jsDate);
                console.log(move_in_date);
                $("#moveindate").text(move_in_date);
                var expected_move_out=(response.data1.exp_move_out);
                var dateParts = expected_move_out.split("-");
                var jsDate2 = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var move_out_date= $.datepicker.formatDate(jsDateFomat, jsDate2);
                $(".moveout").text(move_out_date);
                $(".lease_term").text(response.data1.lease_term);
                $(".req_lease_period").val(response.data1.lease_tenure);
                $(".ZIP").text(response.data1.zip_code);
                $(".Property").text(response.prop.property_name);
                $(".Building_name").text(response.build.building_name);
                $(".Referral").text(response.referral.referral);
                $(".referrals").text(response.referral.referral);

                if(response.ethinicity!=null){
                    $(".Ethnicity").text(response.ethinicity.title);
                }
                if(response.hobbies!=null) {
                    $(".Hobbies").text(response.hobbies.hobby);
                }
                if(response.veteran!=null) {
                    $(".Veteran").text(response.veteran.veteran);
                }
                $(".unit").text(response.unit.unit_prefix+"-"+response.unit.unit_no);
                $(".City_unit").text(response.data1.city);
                $(".State_unit").text(response.data1.state);
                $(".Market_rent").text(response.data1.market_rent);
                $(".base_rent").text(response.data1.base_rent);
                $(".sec_dep").text(response.data1.sec_deposite);
                $(".Address").text(response.data1.address1+""+response.data1.address2+""+response.data1.address3+""+response.data1.address4);
                $("#salutation").val(response.data2.salutation);
                $(".name").text(response.data2.name);
                $("#first_name").val(response.data2.first_name);
                $("#middle_name").val(response.data2.mi);
                $("#last_name_user").val(response.data2.last_name);
                $("#maiden_name").val(response.data2.maiden_name);
                $("#nick_name").val(response.data2.nick_name);
                if(response.data2.gender=='1'){
                    var gender='Male';
                }
                if(response.data2.gender=='2'){
                    var gender='Female';
                }
                if(response.data2.gender=='3'){
                    var gender='Prefer not to say';
                }
                if(response.data2.gender=='4'){
                    var gender='Other';
                }

                $(".Gender").text(gender);
                $(".stateLicence").val(response.data2.state);
                $(".ssn").text(response.data2.ssn_sin_id);
                var dateParts = response.data2.dob.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var dob= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".Birth").text(dob);
                $("#referralsource").val(response.data2.referral_source);
                $("#phone_type_rental").val(response.data2.phone_type);
                $("#guestCarrier").val(response.data2.carrier);
                $("#guestCountries").val(response.data2.country);
                $(".phone_number").text(response.data2.phone_number);
                $(".Email").text(response.data2.email);
                $(".State").text(response.data2.state);
                $(".State").text(response.data2.state);
                $("#guestEthnicity").val(response.data2.ethnicity);
                if(response.data2.maritial_status=='0'){
                    var martial='N/A';
                }
                if(response.data2.maritial_status=='1'){
                    var martial='Single';
                }
                if(response.data2.maritial_status=='2'){
                    var martial='Married';
                }
                $(".Martial").text(martial);
                $("#guestHobbies").val(response.data2.hobbies);
                $("#guestVeteran").val(response.data2.veteran_status);
                for(var i=0;i<response.data3.length;i++){
                    $(".other_Name").val(response.data3[i].first_name);
                    $(".other_ssn").val(response.data3[i].ssn);
               /* <option value="1">Daughter</option>
                        <option value="2">Father</option>
                        <option value="3">Friend</option>
                        <option value="4">Mother</option>
                        <option value="5">Owner</option>
                        <option value="6">Partner</option>
                        <option value="7">Son</option>
                        <option value="8">Spouse</option>
                        <option value="9">Other</option>*/
                    if(response.data3[i].relationship=='1'){
                        var relation ="Daughter";
                    }else if(response.data3[i].relationship=='2'){
                        var relation ="Father";
                    }else if(response.data3[i].relationship=='3'){
                        var relation ="Friend";
                    }else if(response.data3[i].relationship=='4'){
                        var relation ="Mother";
                    }else if(response.data3[i].relationship=='5'){
                        var relation ="Owner";
                    }else if(response.data3[i].relationship=='6'){
                        var relation ="Partner";
                    }else if(response.data3[i].relationship=='7'){
                        var relation ="Son";
                    }else if(response.data3[i].relationship=='8'){
                        var relation ="Spouse";
                    }else if(response.data3[i].relationship=='9'){
                        var relation ="Other";
                    }
                    $("#other_relation").val(relation);
                }
                $(".curr_addr").text(response.data4.current_address);
                $(".zip_postal_current").text(response.data4.current_zip_code);
                $(".city_current").text(response.data4.current_city);
                $(".curr_state").text(response.data4.current_state);
                $(".curr_manager").text(response.data4.current_landlord);
                $(".land_phone").text(response.data4.current_landlord_no);
                $(".monthly_rent").text(response.data4.current_salary);
                var dateParts = response.data4.current_resided_from.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var current_residence_from= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".resided_from").text(current_residence_from);
                var dateParts = response.data4.current_resided_to.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var current_residence_to= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".resided_to").text(current_residence_to);
                $("._current").text(response.curr_reason.reason);
                //$("._current").text(response.curr_reason.reason);
                if(response.data4.previous_address!=""){
                    $("#previousrental").show();
                   // document.getElementById("currentrental123").checked = true;
                }
                $(".previous_addr").text(response.data4.previous_address);
                $(".zip_postal_previous").text(response.data4.previous_zip_code);
                $(".city_previous").text(response.data4.previous_city);
                $(".previous_state").text(response.data4.previous_state);
                $(".previous_manager").text(response.data4.previous_landlord);
                $(".previous_phone").text(response.data4.previous_landlord);
                $(".previousmonthly_rent").text(response.data4.previous_salary);
                var dateParts = response.data4.previous_resided_from.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var previous_resided_from= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".previousresided_from").text(previous_resided_from);
                if(response.data4.previous_resided_to!=null){
                    var dateParts = response.data4.previous_resided_to.split("-");
                    var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                    var previous_resided_to= $.datepicker.formatDate(jsDateFomat, jsDate);
                    $(".previousresided_to").val(previous_resided_to);
                }
                $(".previous_reason").text(response.previous_reason.reason);
                $(".curr_emplyyeee").text(response.data5.previous_employee_address);
                $(".address_employyer").text(response.data5.current_employee_address);
                $(".zip_emplyyeee").text(response.data5.current_employer_zip);
                $(".city_emplyyer").text(response.data5.current_employee_city);
                $(".state_province").text(response.data5.current_employee_state);
                $(".position_held").text(response.data5.current_employee_position);
                $(".monthly_salary").text(response.data5.current_employee_salary);
                var dateParts = response.data5.current_employee_from_date.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var current_employee_from_date= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".employed_from").text(current_employee_from_date);
                var dateParts = response.data5.current_employee_to_date.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var current_employee_to_date= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".emplyed_to").text(current_employee_to_date);
                $(".employe_phone").text(response.data5.current_employee_no);
                if(response.data5.previous_employer!="" || response.data5.previous_employer!=null){
                    $("#previousemployer").show();
                   // document.getElementById("emphistory").checked = true;
                }
                $(".previous_emplyyeee").text(response.data5.previous_employer);
                $(".previousaddress_employyer").text(response.data5.previous_employee_address);
                $(".previouszip_emplyyeee").text(response.data5.previous_employer_zip);
                $(".previouscity_emplyyer").text(response.data5.previous_employee_city);
                $(".previous_state_province").text(response.data5.previous_employee_state);
                $(".previous_position_held").text(response.data5.previous_employee_position);
                $(".previous_monthly_salary").text(response.data5.previous_employee_salary);
                var dateParts = response.data5.previous_employee_from_date.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var previous_employee_from_date= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".previous_employed_from").text(previous_employee_from_date);
                var dateParts = response.data5.previous_employee_to_date.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var previous_employee_to_date= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".previous_emplyed_to").text(previous_employee_to_date);
                $(".previous_employe_phone").text(response.data5.previous_employee_no);
                for(var j=0;j<response.data6.length;j++){
                    $("#additional_amount").val(response.data6[j].amount);
                    $(".additional_income").val(response.data6[j].source_of_income);
                }
                for(var j=0;j<response.data7.length;j++){
                    $(".emer_name").text(response.data7[j].emergency_contact_name);
                    if(response.data7[j].emergency_relation=='1'){
                        var relation1 ="Daughter";
                    }else if(response.data7[j].emergency_relation=='2'){
                        var relation1 ="Father";
                    }else if(response.data7[j].emergency_relation=='3'){
                        var relation1 ="Friend";
                    }else if(response.data7[j].emergency_relation=='4'){
                        var relation1 ="Mother";
                    }else if(response.data7[j].emergency_relation=='5'){
                        var relation1 ="Owner";
                    }else if(response.data7[j].emergency_relation=='6'){
                        var relation1 ="Partner";
                    }else if(response.data7[j].emergency_relation=='7'){
                        var relation1 ="Son";
                    }else if(response.data7[j].emergency_relation=='8'){
                        var relation1 ="Spouse";
                    }else if(response.data7[j].emergency_relation=='9'){
                        var relation1 ="Other";
                    }
                    $(".emner_relation").text(relation1);
                    $(".emr_exten").text(response.data7[j].emergency_country_code);
                    $(".emr_phone").text(response.data7[j].emergency_phone_number);
                    $(".email_emer").text(response.data7[j].emergency_email);
                }
                for(var j=0;j<response.data8.length;j++){
                    $(".co_salutaion").val(response.data8[j].salutation);
                    $(".co_name").text(response.data8[j].first_name+" "+response.data8[j].last_name);
                    $(".co_middle_name").val(response.data8[j].middle_name);
                    $(".co_last_name").val(response.data8[j].mc_last_name);
                    $(".co_gender").val(response.data8[j].gender);

                    $(".co_relationship").val(response.data8[j].relationship);
                    $(".co_address").text(response.data8[j].address1+" "+response.data8[j].address2+" "+response.data8[j].address3+" "+response.data8[j].address4);
                    $(".zip_postal_co").text(response.data8[j].zip_code);
                    $(".city_co").text(response.data8[j].city);
                    $(".co_state").text(response.data8[j].state);
                    $(".email_co").text(response.data8[j].email1);
                    if(response.data8[j].relationship=='1'){
                        var relation11 ="Daughter";
                    }else if(response.data8[j].relationship=='2'){
                        var relation11 ="Father";
                    }else if(response.data8[j].relationship=='3'){
                        var relation11 ="Friend";
                    }else if(response.data8[j].relationship=='4'){
                        var relation11 ="Mother";
                    }else if(response.data8[j].relationship=='5'){
                        var relation11 ="Owner";
                    }else if(response.data8[j].relationship=='6'){
                        var relation11 ="Partner";
                    }else if(response.data8[j].relationship=='7'){
                        var relation11="Son";
                    }else if(response.data8[j].relationship=='8'){
                        var relation11 ="Spouse";
                    }else if(response.data8[j].relationship=='9'){
                        var relation11 ="Other";
                    }

                    $(".co_relation").text(relation11);
                }
                for(var j=0;j<response.data9.length;j++){
                    $(".chargeNote_co").val(response.data9[j].notes);
                }
                for(var j=0;j<response.data10.length;j++){
                    $(".guar_phone_type").val(response.data10[j].phone_type);
                    $(".guar_carrier").val(response.data10[j].carrier);
                    $(".guar_coun_code").val(response.data10[j].country_code);
                    $(".co_phone").text(response.data10[j].phone_number);
                }
                $(".prop_short").val(response.data1.prop_id);

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $(document).on("click",".approve_rental",function(){
        bootbox.confirm({
            message: "Do you want to approve this action?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    approverental();
                }
            }
        });

    });
    $(document).on("click",".decline_rental",function(){

        bootbox.confirm({
            message: "Do you want to decline this action?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    declinerental();
                }
            }
        });
    });
    $(document).on("click",".edit_redirection",function(){
        var base_url = window.location.origin;
        var id=$("#hidden_id_view").val();
        window.location.href = base_url + '/RentalApplication/EditRentalApplications?rental_id='+id;

    });

    function approverental() {
        var id=$("#hidden_id_view").val();
        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: "RenatlApplicationAjax",
                action: "approverental",
                id:id
            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);
                $(".approve_application").hide();
                $(".approve_application_none").show();
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    function declinerental() {
        var id=$("#hidden_id_view").val();
        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: "RenatlApplicationAjax",
                action: "declinerental",
                id:id
            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);
                $(".approve_application").show();
                $(".approve_application_none").hide();
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    function additionalInc(status) {
        var id=$("#hidden_id_view").val();
        var table = 'company_annual_income';
        var columns = ['Amount','Source of income'];
        var select_column = ['Edit','Delete','Completed'];
        //var joins = [{table:'users',column:'id',primary:'user_id',on_table:'tenant_property'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_details'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_lease_details'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_phone'},{table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},{table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        //var extra_where = [{column:'object_type',value:'tenant',condition:'=',table:'flags'},{column:'object_id',value:tenantId,condition:'=',table:'flags'}];
        var extra_where = [{column: 'user_id', value: id, condition: '=', table: 'company_annual_income'}];
        var pagination=[];
        var columns_options = [
            {name:'Amount',index:'amount', width:400,align:"center",searchoptions: {sopt: conditions},table:table,formatter:currencyFormatter},
            {name:'Source of income',index:'source_of_income', width:400,searchoptions: {sopt: conditions},table:table},
        ];
        var ignore_array = [];
        jQuery(".additional_income").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_annual_income",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }
    function currencyFormatter (cellValue, options, rowObject) {
        if (cellValue !== undefined && cellValue !== "") {
            return currencySymbol + '' + cellValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ".00";
        } else {
            return "";
        }
    }
    function statusFormatter (cellValue, options, rowObject){
        console.log("hereiam");
        if (cellValue == 0)
            return "No";
        else if(cellValue == '1')
            return "Yes";
        else
            return 'No';

    }

    function getFiles(status) {
        //var rental_id=$(".rental_id").val();

        var tenant_id = $("#hidden_id_view").val();
        var table = 'tenant_chargefiles';
        var columns = ['Id', 'Name', 'View','File_location','File_extension','Action'];
        var select_column = ['Edit','Delete','Send'];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_where = [{column:'user_id', value :tenant_id,condition:'='}];
        var extra_columns = [];
        var joins = [];
        var columns_options = [
            {name:'Id',index:'id', width:300,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'Name',index:'filename', width:300,searchoptions: {sopt: conditions},table:table},
            {name:'View',index:'file_type', width:300,searchoptions: {sopt: conditions},table:table,formatter:fileFormatter},
            {name:'File_location',index:'file_location', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'File_extension',index:'file_extension', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'file_type', width:300,searchoptions: {sopt: conditions},table:table,formatter:selectFormatter},
        ];


        var ignore_array = [];
        jQuery("#TenantFiles-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'true'
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "File Library",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }
    function fileFormatter(cellValue, options, rowObject)
    {
        var upload_url = window.location.origin;
        if(rowObject !== undefined) {
            var file_type =  rowObject.View;
            var location = rowObject.File_location;
            var path = upload_url+'company/'+location;
            var imageData = '';
            if(file_type == '1'){

                imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
            } else {
                if (rowObject.File_extension == 'xlsx') {
                    src = upload_url + '/company/images/excel.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'pdf') {
                    src = upload_url + '/company/images/pdf.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'docx') {
                    src = upload_url + '/company/images/word_doc_icon.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }else if (rowObject.File_extension == 'txt') {
                    src = upload_url + '/company/images/notepad.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }
            }
            return imageData;
        }
    }

    function selectFormatter(cellValue, options, rowObject)
    {
        var upload_url = upload_url;
        if(rowObject !== undefined) {
            var file_type =  rowObject.View;
            var location = rowObject.File_location;
            var path = upload_url+'company/'+location;

            var imageData = '';
            if(file_type == '1'){

                imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
            } else {
                if (rowObject.File_extension == 'xlsx') {
                    src = upload_url + 'company/images/excel.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'pdf') {
                    src = upload_url + 'company/images/pdf.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'docx') {
                    src = upload_url + 'company/images/word_doc_icon.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }else if (rowObject.File_extension == 'txt') {
                    src = upload_url + 'company/images/notepad.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }
            }


            var html = "<select editable='1' class='form-control select_options' data-id='"+rowObject.Id+"' data-path = '"+path+"' data-src= '"+src+"'>"

            html +=  "<option value=''>Select</option>";
            html +=  "<option value='Delete'>Delete</option>";
            html +=  "<option value='Send'>SEND</option>";

            html +="</select>";

            return html;

            return imageData;
        }
    }

    function jqGrid_Occupants(status, deleted_at) {
        var tenant_id = $("#hidden_id_view").val();
        var table = 'tenant_additional_details';
        var columns = ['Other Occupants','SSN/SIN/ID','Relationship'];
        //if(status===0) {
        var select_column = [''];
        //}
        var joins = [{table:'tenant_additional_details',column:'id',primary:'user_id',on_table:'tenant_ssn_id'},{table:'tenant_additional_details',column:'ethnicity',primary:'id',on_table:'tenant_ethnicity'},{table:'tenant_additional_details',column:'veteran_status',primary:'id',on_table:'tenant_veteran_status'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [{column:'user_id',value:tenant_id,condition:'=',table:'tenant_additional_details'}];
        var pagination=[];
        var columns_options = [

            {name:'Other Occupants',index:'first_name', width:195,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'name'},
            {name:'SSN/SIN/ID',index:'ssn',align:"center", width:195,searchoptions: {sopt: conditions},table:'tenant_ssn_id'},
             {name:'Relationship',index:'relationship', width:200, align:"center",searchoptions: {sopt: conditions},table:table},
            ];
        var ignore_array = [];
        jQuery("#occcupants_table1").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "tenant_additional_details",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: '',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }
    $(document).on("click",".print_lease", function () {
        var sList = [];
        getHtmlPdfConverter(sList,'pdf');
    });
   $(document).on("click",".generate_lease", function () {
       var base_url = window.location.origin;
       var id = $("#hidden_id_view").val();
       window.location.href = base_url + '/Lease/Leases?id='+id;
    });

    function getHtmlPdfConverter(dataArray,datatypes){
        var rental_id = $("#hidden_id_view").val();
        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: "RenatlApplicationAjax",
                action: "getPdfContent",
                dataArray: dataArray,
                type: datatypes,
                rental_id:rental_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    var link=document.createElement('a');
                    document.body.appendChild(link);
                    link.target="_blank";
                    link.download="Ticket.pdf";
                    link.href=response.data.record;
                    link.click();
                } else if(response.code == 500) {
                    //toastr.warning(response.message);
                } else {
                    //toastr.error(response.message);
                }
            }
        });
    }
});

$(document).on('click','.background_check_open',function () {
    if( $(this).is(':checked') )
    {
        $(this).prop('checked', false);
        // $('#backGroundCheckPop').modal('hide');
        // $('#backgroundReport').modal('show');
        window.open('https://www.victig.com', '_blank');
    }
});