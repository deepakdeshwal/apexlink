$(document).ready(function () {
    $(document).on("click","#add_rental_button_cancel",function(){
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    var baseUrl = window.location.origin;
                    window.location.href = baseUrl+'/RentalApplication/RentalApplications';
                }
            }
        });
    });
    phoneTypeList();
    function phoneTypeList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestPhoneType',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestPhoneType').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#phone_type_rental').append($("<option value = "+value.id+">"+value.type+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }
    guestCarrierList();
    function guestCarrierList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'carrierdata',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestCarrier').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestCarrier').append($("<option value = "+value.id+">"+value.carrier+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }


    /**
     * guest card countries fetching data
     */

    guestCountriesList();
    function guestCountriesList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestCountries',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestCountries').empty().append("<option value='0'>Select</option>");
                    $('#guestCountries1').empty().append("<option value='0'>Select</option>");
                    var html = '';
                    $.each(response.data, function (key, value) {
                        var name = value.name+' ('+value.code+ ')';
                        html += '<option value="' + value.id + '">' +name+ '</option>';
                    });
                    $('#guestCountries').append(html);
                    $('#guestCountries1').append(html);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest card Ethnicity fetching data
     */

    guestEthnicityList();
    function guestEthnicityList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestEthnicity',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestEthnicity').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestEthnicity').append($("<option value = "+value.id+">"+value.title+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest marital status fetching data
     */
    guestMaritalStatusList();
    function guestMaritalStatusList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestMaritalStatus',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestMarital').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestMarital').append($("<option value = "+value.id+">"+value.marital+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest hobbies fetching data
     */
    guestHobbiesList();
    function guestHobbiesList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestHobbies',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestHobbies').empty();
                    $.each(response.data, function (key, value) {
                        $('#guestHobbies').append($("<option value = "+value.id+">"+value.hobby+"</option>"));
                    });
                    $("#guestHobbies").multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest veteran status fetching data
     */
    guestVeteranList();
    function guestVeteranList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestVeteranStatus',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestVeteran').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestVeteran').append($("<option value = "+value.id+">"+value.veteran+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * clone phone number
     */

    function myDateFormatter(dateObject){
        var newdate = dateObject.getFullYear() + "-" + (dateObject.getMonth() + 1) + "-" + dateObject.getDate();
        var tmp = [];
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/getDateFormat',
            data: {class: "TenantAjax",action: "getDateformat",date: newdate,},
            async: false,
            global:false,
            success: function (response) {
                tmp = $.parseJSON(response);
            }
        });
        return tmp;
    }
    $(document).on("change",".move_in_date",function(){

        var checkdate=$(".move_in_date").val();
        var term=$(".req_lease_term").val();
        var period=$(".req_lease_term").val();
        var d = $.datepicker.parseDate('mm/dd/yy', checkdate);
        var abc=  d.getDate();
        d.setFullYear(d.getFullYear() + 1);
        if(abc !="31"){

            d.setMonth(d.getMonth() + 1);
            d.setDate(0);
        }
        var outdate=myDateFormatter(d)
        $(".move_out_day").val(outdate);
    });
    searchingProp();
    function searchingProp() {
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "searchingProp",
            },
        success: function (response) {
            var response = JSON.parse(response);
                var html = "";
                html += ' <option value="">' + 'Select Property' + '</option>';
                for (var i = 0; i < response.data.prop.length; i++) {
                    html += ' <option value="' + response.data.prop[i].id + '">' + response.data.prop[i].property_name + '</option>';
                }
                $("#rent_details_form .prop_short").html(html);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $(document).on('change', '.prop_short', function () {
        var id = $(".prop_short").val();
        getpropdetails(id);
        searchingBuilding(id);
    });

    function searchingBuilding(id) {
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "searchingBuilding",
                id: id
            },
            success: function (response) {
                var html = "";
                var response = JSON.parse(response);
                html += ' <option value="">' + 'Select Building' + '</option>';
                for (var i = 0; i < response.data.prop.length; i++) {
                    html += ' <option value="' + response.data.prop[i].id + '">' + response.data.prop[i].building_name + '</option>';
                }
                $(".build_short").html(html);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on('change', '.build_short', function () {
        var id = $(".build_short").val();
        searchingUnit(id);
    });

    function searchingUnit(id) {
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "searchingUnit",
                id: id
            },
            success: function (response) {
                var html = "";
                var response = JSON.parse(response);
                html += ' <option value="">' + 'Select Unit' + '</option>';
                for (var i = 0; i < response.data.prop.length; i++) {
                    html += ' <option value="' + response.data.prop[i].id + '">' + response.data.prop[i].unit_prefix + "-" + response.data.prop[i].unit_no + '</option>';
                }
                $(".unit_short").html(html);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $(document).on('change', '.unit_short', function () {
        var id = $(".unit_short").val();
        getdetails(id);
    });
    function getdetails(id) {
        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: "RenatlApplicationAjax",
                action: "getdetails",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                $(".marketRent").val(response.data.market_rent);
                $(".baseRent").val(response.data.base_rent);
                $(".secdeposite").val(response.data.security_deposit);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    function getpropdetails(id) {
        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: "RenatlApplicationAjax",
                action: "getpropdetails",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log(response);
                $(".address1").val(response.data.address1);
                $(".address2").val(response.data.address2);
                $(".address3").val(response.data.address3);
                $(".address4").val(response.data.address4);
                $(".zip__code").val(response.data.zipcode);
                $(".cityRental").val(response.data.city);
                $(".stateRental").val(response.data.state);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $.ajax({
        type: 'post',
        url: '/RentalApplication/Ajax',
        data: {
            class: "RenatlApplicationAjax",
            action: "getresonleaving",
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == "success") {
                if (response.data.length > 0){
                    var reasonLeavingOption = "<option value='0'>Select</option>";
                    $.each(response.data, function (key, value) {
                        reasonLeavingOption += "<option value='"+value.id+"'>"+value.reasonName+"</option>";
                    });
                    $('#current_reason').html(reasonLeavingOption);
                    $('#previous_reason').html(reasonLeavingOption);
                }
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getInitialData',
        data: {
            class: "TenantAjax",
            action: "getIntialData"
        },
        success: function (response) {
            var data = $.parseJSON(response);

            if (data.status == "success") {

                if (data.data.state.state != ""){
                    $("#driverProvince").val(data.data.state.state);
                }

                if (data.data.country.length > 0){
                    var countryOption = "<option value='0'>Select</option>";
                    $.each(data.data.country, function (key, value) {
                        countryOption += "<option value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")"+"</option>";
                    });
                    $('#addTenant .countycodediv select').html(countryOption);
                    $('.emergencycountry').html(countryOption);
                    $('#guar_coun_code').html(countryOption);
                    $('.guar_coun_code1').html(countryOption);
                }

                if (data.data.propertylist.length > 0){
                    var propertyOption = "<option value=''>Select</option>";
                    $.each(data.data.propertylist, function (key, value) {
                        propertyOption += "<option value='"+value.id+"' data-id='"+value.property_id+"'>"+value.property_name+"</option>";
                    });
                    $('#addTenant #property').html(propertyOption);
                }

                if (data.data.phone_type.length > 0){
                    var phoneOption = "";
                    $.each(data.data.phone_type, function (key, value) {
                        phoneOption += "<option value='"+value.id+"'>"+value.type+"</option>";
                    });
                    $('.primary-tenant-phone-row select[name="phoneType[]"]').html(phoneOption);
                    $('.addition_tenant_block select[name="additional_phoneType"]').html(phoneOption);
                    $('#guar_phone_type').html(phoneOption);
                }

                if (data.data.carrier.length > 0){

                    var carrierOption = "<option value=''>Select</option>";
                    $.each(data.data.carrier, function (key, value) {
                        carrierOption += "<option value='"+value.id+"'>"+value.carrier+"</option>";
                    });
                    $('.primary-tenant-phone-row select[name="carrier[]"]').html(carrierOption);
                    $('#guar_carrier').html(carrierOption);
                }
                if (data.data.carrier.length > 0){

                    var carrierOption = "<option value='0'>Select</option>";
                    $.each(data.data.carrier, function (key, value) {
                        carrierOption += "<option value='"+value.id+"'>"+value.carrier+"</option>";
                    });
                    $('.addition_tenant_block select[name="additional_carrier"]').html(carrierOption);
                    $('.property_guarantor_form1 select[name="guarantor_carrier_1[]"]').html(carrierOption);
                    $('.additional_carrier').html(carrierOption);
                    $(' select[name="guarantor_form2_carrier_1[]"]').html(carrierOption);
                }

                if (data.data.referral.length > 0){
                    var referralOption = "";
                    $.each(data.data.referral, function (key, value) {
                        referralOption += "<option value='"+value.id+"'>"+value.referral+"</option>";
                    });
                    $('select[name="referralSource"]').html(referralOption);
                    $('.addition_tenant_block select[name="additional_referralSource"]').html(referralOption);
                }

                if (data.data.ethnicity.length > 0){
                    var ethnicityOption = "";
                    $.each(data.data.ethnicity, function (key, value) {
                        ethnicityOption += "<option value='"+value.id+"'>"+value.title+"</option>";
                    });
                    $('select[name="ethncity"]').html(ethnicityOption);
                    $('.addition_tenant_block select[name="additional_ethncity"]').html(ethnicityOption);
                }

                if (data.data.marital.length > 0){
                    var maritalOption = "";
                    $.each(data.data.marital, function (key, value) {
                        maritalOption += "<option value='"+value.id+"'>"+value.marital+"</option>";
                    });
                    $('select[name="maritalStatus"]').html(maritalOption);
                    $('.addition_tenant_block select[name="maritalStatus"]').html(maritalOption);
                    $('.additional_maritalStatus').html(maritalOption);
                }

                if (data.data.hobbies.length > 0){
                    var hobbyOption = "";
                    $.each(data.data.hobbies, function (key, value) {
                        hobbyOption += "<option value='"+value.id+"'>"+value.hobby+"</option>";
                    });
                    $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').html(hobbyOption);
                    $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                }

                if (data.data.veteran.length > 0){
                    var abc = "";
                    var veteranOption = "<option value='0'>Select</option>";
                    $.each(data.data.veteran, function (key, value) {
                        veteranOption += "<option value='"+value.id+"'>"+value.veteran+"</option>";
                    });
                    $('select[name="veteranStatus"]').html(veteranOption);
                    $('.additional_veteranStatus').html(veteranOption);
                }

                if (data.data.collection_reason.length > 0){
                    var reasonOption = "";
                    $.each(data.data.collection_reason, function (key, value) {
                        reasonOption += "<option value='"+value.id+"'>"+value.reason+"</option>";
                    });
                   // $('#previous_reason').html(reasonOption);
                   // $('#current_reason').html(reasonOption);
                }

                if (data.data.credential_type.length > 0){
                    var typeOption = "";
                    var typeOption = "<option value='0'>Select</option>";
                    $.each(data.data.credential_type, function (key, value) {
                        typeOption += "<option value='"+value.id+"'>"+value.credential_type+"</option>";
                    });
                    $('.tenant-credentials-control select[name="credentialType[]"]').html(typeOption);
                }


            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
    //
    $('input[type=checkbox][name=rentalhistory]').change(function() {
        var check=$("#rental_history_form").serializeArray();
        if (check[10]['value'] == 1) {
            $("#previousrental").show();
        }
        else {

            $("#previousrental").hide();

        }
    });

    $('input[type=checkbox][name=employmenthistory]').change(function() {
        var check=$("#employment_history_form").serializeArray();
        if (check[10]['value'] == '1') {
            $("#previousemployer").show();
        }
        else {

            $("#previousemployer").hide();

        }
    });


    $(document).on("click","#phone_type_rental",function(){
        var val= $(this).val();
        if(val=='2' || val=='5'){
            $(this).parents("#short_term_rental").find(".ext_phone").show();
        }else{
            $(this).parents("#short_term_rental").find(".ext_phone").hide();
        }
    });
    $(document).on("change","#salutation",function(){
        var value = $(this).val();
        if(value=="Dr.")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("0");
        }
        if(value=="Mr.")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Mrs.")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="Mr. & Mrs.")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("4");
        }
        if(value=="Ms.")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("2");
        }
        if(value=="Sir")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Madam")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="Brother")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Sister")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="Father")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Mother")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="")
        {
            $(".maiden_name_hide").hide();

        }
    });

    getFiles('All',);
    function getFiles(status) {
       //var rental_id=$(".rental_id").val();
        var tenant_id = $(".rental_id").val();
        var table = 'tenant_chargefiles';
        var columns = ['Id', 'Name', 'View','File_location','File_extension','Action'];
        var select_column = ['Edit','Delete','Send'];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_where = [{column:'user_id', value :tenant_id,condition:'='}];
        var extra_columns = [];
        var joins = [];
        var columns_options = [
            {name:'Id',index:'id', width:300,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'Name',index:'filename', width:300,searchoptions: {sopt: conditions},table:table},
            {name:'View',index:'file_type', width:300,searchoptions: {sopt: conditions},table:table,formatter:fileFormatter},
            {name:'File_location',index:'file_location', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'File_extension',index:'file_extension', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'file_type', width:300,searchoptions: {sopt: conditions},table:table,formatter:selectFormatter},
        ];


        var ignore_array = [];
        jQuery("#TenantFiles-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'true'
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "File Library",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }

    $(document).on('click','#add_libraray_file',function(){
        $('#file_library').val('');
        $('#file_library').trigger('click');
    });

    var file_library = [];
    $(document).on('change','#file_library',function(){

        var upload_url = window.location.origin;
        file_library = [];
        $.each(this.files, function (key, value) {
            var type = value['type'];
            var size = isa_convert_bytes_to_specified(value['size'], 'k');
            if(size > 1030) {
                toastr.warning('Please select documents less than 1 mb!');
            } else {
                size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
                if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                    file_library.push(value);
                    var src = '';
                    var reader = new FileReader();
                    $('#file_library_uploads').html('');
                    reader.onload = function (e) {
                        if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                            src = upload_url + '/company/images/excel.png';
                        } else if (type == 'application/pdf') {
                            src = upload_url + '/company/images/pdf.png';
                        } else if (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                            src = upload_url + '/company/images/word_doc_icon.jpg';
                        } else if (type == 'text/plain') {
                            src = upload_url + '/company/images/notepad.jpg';
                        } else if (type == 'text/xml') {
                            src = upload_url + '/company/images/notepad.jpg';
                        } else {
                            src = e.target.result;
                        }
                        $("#file_library_uploads").append(
                            '<div class="row" style="margin:20px">' +
                            '<div class="col-sm-12 img-upload-library-div">' +
                            '<div class="col-sm-3"><img class="img-upload-tab' + key + '" width=100 height=100 src=' + src + '></div>' +
                            '<div class="col-sm-3 show-library-list-imgs-name' + key + '">' + value['name'] + '</div>' +
                            '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data-id="' + value['size'] + '">' +
                            '<div class="col-sm-3 show-library-list-imgs-size' + key + '">' + size + '</div>' +
                            '<div class="col-sm-3"><span id=' + key + ' class="delete_pro_img cursor"><button class="btn-warning">Delete</button></span></div></div></div>');
                    };
                    reader.readAsDataURL(value);
                } else {
                    toastr.warning('Please select file with .xlsx | .pdf | .docx | .txt | .xml extension only!');
                }
            }
        });
    });
    $(document).on('click','#remove_library_file',function(){
        bootbox.confirm("Do you want to remove all files?", function (result) {
            if (result == true) {
                toastr.success('The record deleted successfully.');
                $('#file_library_uploads').html('');
                $('#file_library').val('');
                imgArray = [];
            }
        });
    });

    function fileFormatter(cellValue, options, rowObject)
    {
        var upload_url = window.location.origin;
        if(rowObject !== undefined) {
            var file_type =  rowObject.View;
            var location = rowObject.File_location;
            var path = upload_url+'company/'+location;
            var imageData = '';
            if(file_type == '1'){

                imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
            } else {
                if (rowObject.File_extension == 'xlsx') {
                    src = upload_url + '/company/images/excel.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'pdf') {
                    src = upload_url + '/company/images/pdf.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'docx') {
                    src = upload_url + '/company/images/word_doc_icon.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }else if (rowObject.File_extension == 'txt') {
                    src = upload_url + '/company/images/notepad.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }
            }
            return imageData;
        }
    }

    function selectFormatter(cellValue, options, rowObject)
    {
      //  var upload_url = upload_url;
        if(rowObject !== undefined) {
            var file_type =  rowObject.View;
            var location = rowObject.File_location;
            var path = upload_url+'company/'+location;
            var src="";
            var imageData = '';
            if(file_type == '1'){

                imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
            } else {
                if (rowObject.File_extension == 'xlsx') {
                    src = upload_url + 'company/images/excel.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'pdf') {
                    src = upload_url + 'company/images/pdf.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'docx') {
                    src = upload_url + 'company/images/word_doc_icon.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }else if (rowObject.File_extension == 'txt') {
                    src = upload_url + 'company/images/notepad.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }
            }


            var html = "<select editable='1' class='form-control select_options' data-id='"+rowObject.Id+"' data-path = '"+path+"' data-src= '"+src+"'>"

            html +=  "<option value=''>Select</option>";
            html +=  "<option value='Delete'>Delete</option>";
            html +=  "<option value='Send'>SEND</option>";

            html +="</select>";

            return html;

            return imageData;
        }
    }



    $(document).on("click",".saveChargeFile",function(){

        var tenant_id =  $(".rental_id").val();
        var form = $('#addChargeNote')[0];
        var formData = new FormData(form);
        formData.append('action','insertChargeFileNote');
        formData.append('class','TenantPortal');
        formData.append('tenant_id',tenant_id);

        $.ajax({
            url:'/tenantPortal',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == "success"){
                    //toastr.success(response.message);
                    setTimeout(function(){
                        $('#TenantFiles-table').trigger('reloadGrid');
                        $(".img-upload-library-div").hide();
                        $("#file_library").val("");


                    }, 400);

                }
            },
            cache: false,
            contentType: false,
            processData: false
        });

    });




    $(document).on("change",".files_main table .select_options",function(){
        var action = $(this).val();
        var id = $(this).attr('data-id');

        if(action=="Delete")
        {
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        deleteTableData('tenant_chargefiles',id);
                        setTimeout(function(){
                            $('#TenantFiles-table').trigger('reloadGrid');

                        }, 400);

                    }

                }
            });


        }
        if(action=="Send")
        {
            file_upload_email('users','email',id,1);
        }


    });

    function deleteTableData(tablename,id) {

        $.ajax({
            url:'/editTenant?action=deleteRecords&class=EditTenant&id='+id+'&tablename='+tablename,
            type: 'GET',
            success: function (data) {
                var info = JSON.parse(data);
                toastr.success(info.message);

            },
        });

    }
    function isa_convert_bytes_to_specified(bytes, to) {
        var formulas =[];
        formulas['k']= (bytes / 1024).toFixed(1);
        formulas['M']= (bytes / 1048576).toFixed(1);
        formulas['G']= (bytes / 1073741824).toFixed(1);
        return formulas[to];
    }
    $(document).on("click", ".primary-tenant-phone-row-short .fa-plus-circle", function () {
        var clone = $(".primary-tenant-phone-row-short:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".primary-tenant-phone-row-short").first().after(clone);
        $(".primary-tenant-phone-row-short:not(:eq(0))  .fa-plus-circle").hide();
        $(".primary-tenant-phone-row-short:not(:eq(0))  .fa-minus-circle").show();
        var phoneRowLenght = $(".primary-tenant-phone-row-short");
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        $(".ext_phone:not(:eq(0))").hide();
        if (phoneRowLenght.length == 2) {
            clone.find(".fa-plus-circle").hide();
        } else if (phoneRowLenght.length == 3) {
            clone.find(".fa-plus-circle").hide();
            $(".primary-tenant-phone-row-short:eq(0) .fa-plus-circle").hide();
        } else {
            $(".primary-tenant-phone-row-short:not(:eq(0)) .fa-plus-circle").show();
        }
    });

    $(document).on("click", ".primary-tenant-phone-row-short .fa-minus-circle", function () {

        var phoneRowLenght = $(".primary-tenant-phone-row-short");
        if (phoneRowLenght.length == 2) {
            $(".primary-tenant-phone-row-short:eq(0) .fa-plus-circle").show();
        } else if (phoneRowLenght.length == 3) {
            $(".primary-tenant-phone-row-short:eq(0) .fa-plus-circle").hide();
        } else {
            $(".primary-tenant-phone-row-short:not(:eq(0)) .fa-plus-circle").show();
        }

        $(this).parents(".primary-tenant-phone-row-short").remove();
    });
    $(document).on("click", ".primary-tenant-phone-row-emergency .fa-plus-circle", function () {
        var clone = $(".primary-tenant-phone-row-emergency:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".primary-tenant-phone-row-emergency").first().after(clone);
        $(".primary-tenant-phone-row-emergency:not(:eq(0))  .fa-plus-circle").hide();
        $(".primary-tenant-phone-row-emergency:not(:eq(0))  .fa-minus-circle").show();
        var phoneRowLenght = $(".primary-tenant-phone-row-emergency");
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        if (phoneRowLenght.length == 2) {
            clone.find(".fa-plus-circle").hide();
        } else if (phoneRowLenght.length == 3) {
            clone.find(".fa-plus-circle").hide();
            $(".primary-tenant-phone-row-emergency:eq(0) .fa-plus-circle").hide();
        } else {
            $(".primary-tenant-phone-row-emergency:not(:eq(0)) .fa-plus-circle").show();
        }
    });

    $(document).on("click", ".primary-tenant-phone-row-emergency .fa-minus-circle", function () {

        var phoneRowLenght = $(".primary-tenant-phone-row-emergency");
        if (phoneRowLenght.length == 2) {
            $(".primary-tenant-phone-row-emergency:eq(0) .fa-plus-circle").show();
        } else if (phoneRowLenght.length == 3) {
            $(".primary-tenant-phone-row-emergency:eq(0) .fa-plus-circle").hide();
        } else {
            $(".primary-tenant-phone-row-emergency:not(:eq(0)) .fa-plus-circle").show();
        }

        $(this).parents(".primary-tenant-phone-row-emergency").remove();
    });
    /**
     * for multiple SSN textbox
     */
    $(document).on("click","#multipleSsn .ssn-plus-sign",function(){
        var clone = $(".multipleSsn:first").clone();
        clone.find('input[type=text]').val('');
        clone.find('input[type=number]').val('');
        $(".multipleSsn").first().after(clone);
        $(".multipleSsn:not(:eq(0))  .ssn-plus-sign").remove();
        $(".multipleSsn:not(:eq(0))  .ssn-remove-sign").show();
    });
    /**
     * remove
     */
    $(document).on("click",".ssn-remove-sign",function(){
        $(this).parents(".multipleSsn").remove();
    });

    /**
     * multiple mail textbox
     */
    $(document).on("click", ".email-plus-sign-emergency", function () {
        var emailRowLenght = $(this).parents(".multipleEmailadditional").parent().find(".multipleEmailadditional");
        if (emailRowLenght.length == 2) {
            $(this).parents(".multipleEmailadditional:first").find(".email-plus-sign-emergency").hide();
        }
        var clone = $(this).parents(".multipleEmailadditional:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(this).parents(".multipleEmailadditional").first().after(clone);
        clone.find(".email-plus-sign-emergency").remove();
        clone.find(".fa-minus-circle-emergency").show();

    });

    /*for multiple email textbox*/


    /*Remove Email Textbox*/
    $(document).on("click", ".fa-minus-circle-emergency", function () {
        var emailRowLenght = $(this).parents(".multipleEmailadditional").parent().find(".multipleEmailadditional");
        if (emailRowLenght.length == 3 || emailRowLenght.length == 2) {
            $(".email-plus-sign-emergency").show();
        }
        $(this).parents(".multipleEmailadditional").remove();
    });

    /*Remove Email Textbox*/
    $(document).on("click", ".email-plus-sign-short-term", function () {
        var emailRowLenght = $(".multipleEmailShortTerm");
        if (emailRowLenght.length == 2) {

            $(".email-plus-sign-short-term").hide();
        }
        var clone = $(".multipleEmailShortTerm:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".multipleEmailShortTerm").first().after(clone);
        $(".multipleEmailShortTerm:not(:eq(0))  .email-plus-sign-short-term").remove();
        $(".multipleEmailShortTerm:not(:eq(0))  .fa-minus-circle-short-term").show();

    });

    /*for multiple email textbox*/


    /*Remove Email Textbox*/
    $(document).on("click", ".fa-minus-circle-short-term", function () {
        var emailRowLenght = $(".multipleEmailShortTerm");

        if (emailRowLenght.length == 3 || emailRowLenght.length == 2) {
            $(".email-plus-sign-short-term").show();
        }
        $(this).parents(".multipleEmailShortTerm").remove();
    });

    /*Remove Email Textbox*/


    /**
     * referral source data fetching
     */
    $(document).on("click", ".otheroccupants .fa-plus-circle", function () {
        var clone = $(".otheroccupants:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        clone.find('input[type=number]').val(''); //harjinder
        $(".otheroccupants").first().after(clone);
        $(".otheroccupants  .fa-plus-circle:not(:eq(0))").remove();
        $(".otheroccupants  .fa-minus-circle:not(:eq(0))").show();

    });
    /* for multiple SSN textbox*/


    /*remove ssn textbox*/
    $(document).on("click", ".otheroccupants .fa-minus-circle", function () {
        $(this).parents(".otheroccupants").remove();
    });
    $(document).on("click", ".additionalincome .fa-plus-circle", function () {
        var clone = $(".additionalincome:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        clone.find('input[type=number]').val(''); //harjinder
        $(".additionalincome").first().after(clone);
        $(".additionalincome:not(:eq(0))  .fa-plus-circle").hide();
        $(".additionalincome:not(:eq(0))  .fa-minus-circle").show();

        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        if (phoneRowLenght.length == 2) {
            clone.find(".fa-plus-circle").hide();
        } else if (phoneRowLenght.length == 3) {
            clone.find(".fa-plus-circle").hide();
            $(".additionalincome:eq(0) .fa-plus-circle").hide();
        } else {
            $(".additionalincome:not(:eq(0)) .fa-plus-circle").show();
        }
    });
    $(document).on("click", ".additionalincome .fa-minus-circle", function () {

        $(this).parents(".additionalincome").remove();
    });
    /**
     * emergency contact detail clone
     */
    $(document).on("click",".add-emergency-contant",function(){
        var clone = $(".lease-emergency-contact:first").clone();
        clone.find('input[type=text]').val('');
        clone.find('input[name="emergency_phone[]"]').mask('000-000-0000', {reverse: true});;
        $(".lease-emergency-contact").first().after(clone);
        clone.find(".add-emergency-contant").hide();
        clone.find(".remove-emergency-contant").show();
    });
    $(document).on("click",".remove-emergency-contant",function(){
        $(this).parents(".lease-emergency-contact").remove();
    });
    $(document).on("click",".emergency_div_plus",function(){
        // alert("hello");
        var count = $(".lease-emergency-div");
        var divLength = count.length;
        var clone = $(".lease-emergency-div:first").clone();
        clone.find('input[type=text]').val('');
        clone.find('input[type=number]').val('');
        clone.find('input[type=textarea]').val(''); //harjinder
        clone.find("input[name='email_0[]']").attr("name","email_"+divLength+'[]');
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        $(".lease-emergency-div").first().after(clone);
        $(".lease-emergency-div:not(:eq(0))  .emergency_div_plus").remove();
        $(".lease-emergency-div:not(:eq(0))  .emergency_div_minus").show();
    });

    $(document).on("click",".emergency_div_minus",function(){
        $(this).parents(".lease-emergency-div").remove();
    });

    $(document).on("click",".addNote",function(){

        var clone = $(".chargeNoteHtml:first").clone();
        clone.find('textarea').val(''); //harjinder
        $(".chargeNoteHtml").first().after(clone);
        $(".chargeNoteHtml:not(:eq(0))  .addNote").remove();
        $(".chargeNoteHtml:not(:eq(0))  .removeNote").show();
    });

    $(document).on("click",".removeNote",function(){
        var phoneRowLenght = $(".primary-tenant-phone-row");
        $(this).parents(".chargeNoteHtml").remove();
    });
    $(document).on("click",".updateRenatlApplications",function(){
        updaterentalApplicaions();
    });

    function updaterentalApplicaions() {
        var formData=$("#save_rental_general").serializeArray();
        var formData2=$("#rent_details_form").serializeArray();
        var formData3=$("#rental_history_form").serializeArray();
        var formData4=$("#employment_history_form").serializeArray();
        var formData5=$("#emergency_details").serializeArray();
        var formData6=$("#rental_emergency_details").serializeArray();
        var formData7=$("#guarranter_form").serializeArray();
        var notes=$("#notes_form").serializeArray();
        var rental_id=$(".rental_id").val();
        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: "RenatlApplicationAjax",
                action: "updaterentalApplicaions",
                formData:formData,
                formData2:formData2,
                formData3:formData3,
                formData4:formData4,
                formData5:formData5,
                formData6:formData6,
                formData7:formData7,
                notes:notes,
                rental_id:rental_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.code == 400 && response.status == 'error'){
                    toastr.warning(response.message);
                }else{
                    /*toastr.success(response.message);*/
                    localStorage.setItem("Message", 'Record updated successfully.');
                    localStorage.setItem("rowcolorRental",'green');
                    var baseUrl = window.location.origin;
                    window.location.href = baseUrl+'/RentalApplication/RentalApplications';
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});
var rental_id=$(".rental_id").val();
getInitialData(rental_id);
function getInitialData(rental_id){

    $.ajax({
        type: 'post',
        url: '/RentalApplication/Ajax',
        data: {
            class: 'RenatlApplicationAjax',
            action: 'getInitialData',
            rental_id:rental_id
        },
        success: function (response) {
            var response = JSON.parse(response);
            setTimeout(function () {
                edit_date_time(response.data2.updated_at);
            },2000);
            if (response.status == 'success' && response.code == 200) {
                setTimeout(function(){
                    $('#rent_details_form [name="prop_short"]').val(response.data1.prop_id);
                    $('#rent_details_form [name="prop_short"]').trigger('change');
                },500);
                setTimeout(function(){
                    $('#rent_details_form [name="build_short"]').val(response.data1.build_id);
                    $('#rent_details_form [name="build_short"]').trigger('change');
                },1000);
                setTimeout(function(){
                    $('#rent_details_form [name="unit_short"]').val(response.data1.unit_id);
                    $('#rent_details_form [name="unit_short"]').trigger('change');
                },1500);
                var expected_move_in=(response.data1.exp_move_in);
                var dateParts = expected_move_in.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var move_in_date= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".move_in_date").val(move_in_date);
                var expected_move_out=(response.data1.exp_move_out);
                var dateParts = expected_move_out.split("-");
                var jsDate2 = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var move_out_date= $.datepicker.formatDate(jsDateFomat, jsDate2);
                $(".move_out_day").val(move_out_date);
                $(".req_lease_term").val(response.data1.lease_term);
                $(".req_lease_period").val(response.data1.lease_tenure);
                $("#salutation").val(response.data2.salutation);
                $("#first_name").val(response.data2.first_name);
                var namee = response.data2.first_name+" "+response.data2.last_name;
                $("#flagform #flag_name1").val(namee);
                $("#middle_name").val(response.data2.mi);
                $("#last_name_user").val(response.data2.last_name);
                if(response.data2.salutation=="Mrs." || response.data2.salutation=="Madam" || response.data2.salutation=="Sister" || response.data2.salutation=="Mother"){
                    $(".maiden_name_hide").show();
                }
                $("#maiden_name").val(response.data2.maiden_name);
                $("#nick_name").val(response.data2.nick_name);
                $("#general").val(response.data2.gender);
                $(".stateLicence").val(response.data2.state);
                $("#ssn").val(response.data2.ssn_sin_id);
                var dateParts = response.data2.dob.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var dob= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".birthdate").val(dob);
                $("#referralsource").val(response.data2.referral_source);
                $("#phone_type_rental").val(response.data2.phone_type);
                $("#guestCarrier").val(response.data2.carrier);
                $("#guestCountries").val(response.data2.country);
                $("#guestCountries1").val(response.data2.country);
                $(".phoneeee").val(response.data2.phone_number);
                $("#phone_number_flag").val(response.data2.phone_number);
                if(response.data2.phone_type=='2' || response.data2.phone_type=='5'){
                    $(".ext_phone").show();
                    $(".Extension_phone:eq(0)").val(response.data2.work_phone_extension);
                }
                $("#phone_type_rental:eq(0)").val(response.data2.phone_type);
                $("#guestCarrier:eq(0)").val(response.data2.carrier);
                $("#guestCountries:eq(0)").val(response.data2.country);
                $(".phoneeee:eq(0)").val(response.data2.phone_number);
                $(".email_user").val(response.data2.email);

                if(response.data1.email2!="" && response.data1.email2!=null){
                    var clone = $(".multipleEmailShortTerm:first").clone();
                    clone.find('input[type=text]').val(''); //harjinder
                    $(".multipleEmailShortTerm").first().after(clone);
                    $(".multipleEmailShortTerm:not(:eq(0))  .email-plus-sign-short-term").remove();
                    $(".multipleEmailShortTerm:not(:eq(0))  .fa-minus-circle-short-term").show();
                    $(".email_user:eq(1)").val(response.data1.email2);
                }
                if(response.data1.email3!="" && response.data1.email3!=null){
                    var clone = $(".multipleEmailShortTerm:first").clone();
                    clone.find('input[type=text]').val(''); //harjinder
                    $(".multipleEmailShortTerm").first().after(clone);
                    $(".multipleEmailShortTerm:not(:eq(0))  .email-plus-sign-short-term").remove();
                    $(".multipleEmailShortTerm:not(:eq(0))  .fa-minus-circle-short-term").show();
                    $(".email-plus-sign-short-term").hide();
                    $(".email_user:eq(2)").val(response.data1.email3);
                    $(".email_user:eq(1)").val(response.data1.email2);
                }

                if(response.data11.length>=1){
                    var clone = $(".primary-tenant-phone-row-short:first").clone();
                    clone.find('input[type=text]').val(''); //harjinder
                    $(".primary-tenant-phone-row-short").first().after(clone);
                    $(".primary-tenant-phone-row-short:not(:eq(0))  .fa-plus-circle").hide();
                    $(".primary-tenant-phone-row-short:not(:eq(0))  .fa-minus-circle").show();
                    var phoneRowLenght = $(".primary-tenant-phone-row-short");
                    clone.find('.phone_format').mask('000-000-0000', {reverse: true});
                    $(".phone_type_rental:eq(1)").val(response.data11[0].phone_type);
                    $(".Extension_phone:eq(1)").val(response.data2.work_phone_extension);
                    $(".guestCarrier:eq(1)").val(response.data11[0].carrier);
                    $(".guestCountries:eq(1)").val(response.data11[0].country_code);
                    $(".phoneeee:eq(1)").val(response.data11[0].phone_number);
                    // if (phoneRowLenght.length == 1) {
                    //     clone.find(".fa-plus-circle").hide();
                    // } else if (phoneRowLenght.length == 3) {
                    //     clone.find(".fa-plus-circle").hide();
                    //     $(".primary-tenant-phone-row-short:eq(0) .fa-plus-circle").hide();
                    // } else {
                    //     $(".primary-tenant-phone-row-short:not(:eq(0)) .fa-plus-circle").show();
                    // }
                }
                if(response.data11.length>1){
                    alert('hiii');
                    var clone = $(".primary-tenant-phone-row-short:first").clone();

                    clone.find('input[type=text]').val(''); //harjinder
                    $(".primary-tenant-phone-row-short").first().after(clone);
                    $(".primary-tenant-phone-row-short:not(:eq(0))  .fa-plus-circle").hide();
                    $(".primary-tenant-phone-row-short:not(:eq(0))  .fa-minus-circle").show();
                    var phoneRowLenght = $(".primary-tenant-phone-row-short");
                    clone.find('.phone_format').mask('000-000-0000', {reverse: true});
                    $(".phone_type_rental:eq(1)").val(response.data11[0].phone_type);
                    $(".Extension_phone:eq(1)").val(response.data11[0].work_phone_extension);
                    $(".guestCarrier:eq(1)").val(response.data11[0].carrier);
                    $(".guestCountries:eq(1)").val(response.data11[0].country_code);
                    $(".phoneeee:eq(1)").val(response.data11[0].phone_number);

                    $(".phone_type_rental:eq(2)").val(response.data11[1].phone_type);
                    $(".Extension_phone:eq(2)").val(response.data11[1].work_phone_extension);
                    $(".guestCarrier:eq(2)").val(response.data11[1].carrier);
                    $(".guestCountries:eq(2)").val(response.data11[1].country_code);
                    $(".phoneeee:eq(2)").val(response.data11[1].phone_number);
                    // if (phoneRowLenght.length == 2) {
                    //     clone.find(".fa-plus-circle").hide();
                    // } else if (phoneRowLenght.length == 3) {
                    //     clone.find(".fa-plus-circle").hide();
                    //     $(".primary-tenant-phone-row-short:eq(0) .fa-plus-circle").hide();
                    // } else {
                    //     $(".primary-tenant-phone-row-short:not(:eq(0)) .fa-plus-circle").show();
                    // }
                }






                $("#guestEthnicity").val(response.data2.ethnicity);
                $("#guestMarital").val(response.data2.maritial_status);
                $("#guestHobbies").val(response.data2.hobbies);
                $("#guestVeteran").val(response.data2.veteran_status);
                for(var i=0;i<response.data3.length;i++){
                    $(".other_Name").val(response.data3[i].first_name);
                    $(".other_email").val(response.data3[i].email1);
                    $(".other_ssn").val(response.data3[i].ssn);
                    $("#other_relation").val(response.data3[i].relationship);
                }
                $(".current_address").val(response.data4.current_address);
                $(".current_zip").val(response.data4.current_zip_code);
                $(".current_city").val(response.data4.current_city);
                $(".current_state").val(response.data4.current_state);
                $(".current_landlord").val(response.data4.current_landlord);
                $(".current_landlord_no").val(response.data4.current_landlord_no);
                $(".current_salary").val(response.data4.current_salary);
                var dateParts = response.data4.current_resided_from.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var current_residence_from= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".current_residence_from").val(current_residence_from);
                var dateParts = response.data4.current_resided_to.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var current_residence_to= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".current_residence_to").val(current_residence_to);
                $("#current_reason").val(response.data4.current_reason);
                if(response.data4.previous_address!=""){
                    $("#previousrental").show();
                    document.getElementById("currentrental123").checked = true;
                }
                $(".previous_address").val(response.data4.previous_address);
                $(".previous_zip").val(response.data4.previous_zip_code);
                $(".previous_city").val(response.data4.previous_city);
                $(".previous_state").val(response.data4.previous_state);
                $(".previous_landlord").val(response.data4.previous_landlord);
                $(".previous_landlord_no").val(response.data4.previous_landlord);
                $(".previous_salary").val(response.data4.previous_salary);
                var dateParts = response.data4.previous_resided_from.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var previous_resided_from= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".previous_residing_from").val(previous_resided_from);
                if(response.data4.previous_resided_to!=null){
                    var dateParts = response.data4.previous_resided_to.split("-");
                    var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                    var previous_resided_to= $.datepicker.formatDate(jsDateFomat, jsDate);
                    $(".previous_resided_to").val(previous_resided_to);
                }
                $("#previous_reason").val(response.data4.previous_reason);
                $(".current_employer").val(response.data5.previous_employee_address);
                $(".current_employee_address").val(response.data5.current_employee_address);
                $(".current_employer_zip").val(response.data5.current_employer_zip);
                $(".current_employee_city").val(response.data5.current_employee_city);
                $(".current_employee_state").val(response.data5.current_employee_state);
                $(".current_employee_position").val(response.data5.current_employee_position);
                $(".current_employee_salary").val(response.data5.current_employee_salary);
                var dateParts = response.data5.current_employee_from_date.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var current_employee_from_date= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".current_employee_from_date").val(current_employee_from_date);
                var dateParts = response.data5.current_employee_to_date.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var current_employee_to_date= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".current_employee_to_date").val(current_employee_to_date);
                $(".current_employee_no").val(response.data5.current_employee_no);
                if(response.data5.previous_employer!="" || response.data5.previous_employer!=null){
                    $("#previousemployer").show();
                    document.getElementById("emphistory").checked = true;
                }
                $(".previous_employer").val(response.data5.previous_employer);
                $(".previous_employee_address").val(response.data5.previous_employee_address);
                $(".previous_employer_zip").val(response.data5.previous_employer_zip);
                $(".previous_employee_city").val(response.data5.previous_employee_city);
                $(".previous_employee_state").val(response.data5.previous_employee_state);
                $(".previous_employee_position").val(response.data5.previous_employee_position);
                $(".previous_employee_salary").val(response.data5.previous_employee_salary);
                var dateParts = response.data5.previous_employee_from_date.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var previous_employee_from_date= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".previous_employee_from_date").val(previous_employee_from_date);
                var dateParts = response.data5.previous_employee_to_date.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var previous_employee_to_date= $.datepicker.formatDate(jsDateFomat, jsDate);
                $(".previous_employee_to_date").val(previous_employee_to_date);
                $(".previous_employee_no").val(response.data5.previous_employee_no);
                for(var j=0;j<response.data6.length;j++){
                    $("#additional_amount").val(response.data6[j].amount);
                    $(".additional_income").val(response.data6[j].source_of_income);
                }
                for(var j=0;j<response.data7.length;j++){
                    $(".emergency_contact_name").val(response.data7[j].emergency_contact_name);
                    $(".emergency_relation").val(response.data7[j].emergency_relation);
                    $(".emergencycountry").val(response.data7[j].emergency_country_code);
                    $(".emergency_phone").val(response.data7[j].emergency_phone_number);
                    $(".emailll").val(response.data7[j].emergency_email);
                }
                for(var j=0;j<response.data8.length;j++){
                    $(".co_salutaion").val(response.data8[j].salutation);
                    $(".first_name_co").val(response.data8[j].first_name);
                    $(".co_middle_name").val(response.data8[j].middle_name);
                    $(".co_last_name").val(response.data8[j].mc_last_name);
                    $(".co_gender").val(response.data8[j].gender);
                    $(".co_relationship").val(response.data8[j].relationship);
                    $(".co_address1").val(response.data8[j].address1);
                    $(".co_address2").val(response.data8[j].address2);
                    $(".co_address3").val(response.data8[j].address3);
                    $(".co_address4").val(response.data8[j].address4);
                    $(".co_zipcode").val(response.data8[j].zip_code);
                    $(".co_emer_city").val(response.data8[j].city);
                    $(".co_state_province").val(response.data8[j].state);
                    $(".email_co").val(response.data8[j].email1);
                }
                for(var j=0;j<response.data9.length;j++){
                    $(".chargeNote_co").val(response.data9[j].notes);
                }
                for(var j=0;j<response.data10.length;j++){
                    $(".guar_phone_type").val(response.data10[j].phone_type);
                    $(".guar_carrier").val(response.data10[j].carrier);
                    $(".guar_coun_code").val(response.data10[j].country_code);
                    $(".guar_phone_number").val(response.data10[j].phone_number);
                }
                $("#rent_details_form .prop_short").val(response.data1.prop_id);
                var id = $("#rent_details_form.prop_short").val();
                searchingBuilding(id);
                getpropdetails(id);
                setTimeout(function() {
                    $("#rent_details_form .build_short").val(response.data1.build_id);
                },1500);
                setTimeout(function() {
                    var id = $("#rent_details_form .build_short").val();
                    searchingUnit(id);
                },1700);
                setTimeout(function() {
                    $(".unit_short").val(response.data1.unit_id);
                    var id = $(".unit_short").val();
                    getdetails(id);
                },1900);



            } else {
                toastr.error(response.message);
            }
        }
    });
}