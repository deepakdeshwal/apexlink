$(document).ready(function() {

    $(window).on("resize", function () {
        var $grid = $("#list"),
            newWidth = $grid.closest(".ui-jqgrid").parent().width();
        $grid.jqGrid("setGridWidth", newWidth, true);
    });

    jqGrid('All');

    var getParam = getParameters('id');
    getTenantInfo(getParam);
    
    $(document).on("click", "#movein_table tbody tr td", function (e) {
        var id = $(this).parents("tr").attr("id");
        var base_url = window.location.origin;
        window.location.href = base_url + '/Lease/Move-in?id='+id;
    });

    $(document).on("click", ".cancel_move_in", function () {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    var base_url = window.location.origin;
                    window.location.href = base_url + '/Lease/Movein';
                }
            }
        });
    });

    $(document).on("click",".save_move_in", function () {
        var getParam = getParameters('id');
        var mi_schedule_move_in = $(".mi_schedule_move_in").text();
        var mi_actual_move_in = $(".mi_actual_move_in").text();
        var paid_amount = $("input[name='paid_amount']").val();
        var check_no = $("input[name='check_no']").val();
        var prorated_amount = $("input[name='prorated_amount']").val();
        var memo = $("input[name='memo']").val();
        $.ajax({
            type: 'post',
            url: '/EsignatureUser/getMoveInListRecord',
            data: {
                class: "GenerateLease",
                action: "getMoveInListRecord",
                id: getParam,
                mi_schedule_move_in: mi_schedule_move_in,
                mi_actual_move_in: mi_actual_move_in,
                paid_amount: paid_amount,
                check_no: check_no,
                prorated_amount: prorated_amount,
                memo: memo
            },
            async: false,
            success: function (response) {
                var res = JSON.parse(response);
                if (res.status == "success") {
                    if (res.code == 503){
                        toastr.warning(res.message);
                    }else{
                        toastr.success(res.message);
                        localStorage.setItem("Message", res.message);
                        localStorage.setItem("rowcolorTenant", 'green');
                        var base_url = window.location.origin;
                        window.location.href = base_url + '/Tenantlisting/Tenantlisting';
                    }
                }else{
                    toastr.error(res.message);
                }
            }
        });
    });

    if(localStorage.getItem("rowcolorTenant")){
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(9)').addClass("green_row_right");
            localStorage.removeItem('rowcolorTenant');
        }, 2000);
    }

    function jqGrid(status, deleted_at) {

        /*if(jqgridNewOrUpdated == 'true'){
            var sortOrder = 'desc';
            var sortColumn = 'users.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }*/

        var sortOrder = 'desc';
        var sortColumn = 'users.updated_at';

        var table = 'users';
        var columns = ['Tenant Name', 'Property Name','Unit_no','Unit Number','Status','Move-In Date'];
        var select_column = [];
        var joins = [
            {table:'users',column:'id',primary:'user_id',on_table:'tenant_property'},
            {table:'users',column:'id',primary:'user_id',on_table:'tenant_details'},
            {table:'users',column:'id',primary:'user_id',on_table:'tenant_lease_details'},
            {table:'users',column:'id',primary:'user_id',on_table:'movein_list_record'},
            {table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},
            {table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [{column:'user_type',value:'2',condition:'=',table:'users'}
            /*{column:'record_status',value:'1',condition:'=',table:'movein_list_record'},*/
            ];
        var columns_options = [
            {name:'Tenant Name',index:'name', width:90,align:"left",searchoptions: {sopt: conditions},table:table},
            {name:'Property Name',index:'property_name', width:80, align:"left",searchoptions: {sopt: conditions},table:'general_property'},
            {name:'Unit_no',index:'unit_no',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details'},
            {name:'Unit Number',index:'unit_prefix', width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
            {name:'Status',index:'status', width:80,align:"left",searchoptions: {sopt: conditions},table:'tenant_details',formatter:statusFormatter},
            {name:'Move-In Date',index:'move_in', width:100,searchoptions: {sopt: conditions},table:'tenant_details',formatter:moveDateFormat}
        ];
        var ignore_array = [];
        jQuery("#movein_table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "users",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [10, 20, 30, 50, 100, 200],
            caption: "List of Move-ins",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }

    function onTop(rowdata,table){
        if(rowdata){
            setTimeout(function(){
                if (table == ""){
                    jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('.table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                }else{
                    jQuery('#'+table).find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('#'+table).find('tr:eq(1)').find('td').last().addClass("green_row_right");
                }
            }, 300);
        }
    }

    function moveDateFormat( cellvalue, options, rowObject){
        if (cellvalue != undefined && cellvalue != ""){
            var dateParts = cellvalue.split("-");
            var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
            return $.datepicker.formatDate(jsDateFomat, jsDate);
        }
    }

    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == 0)
            return "Inactive";
        else if(cellValue == '1')
            return "Active";
        else if(cellValue == '2')
            return "Evicting";
        else if(cellValue == '3')
            return "In-Collection";
        else if(cellValue == '4')
            return "Bankruptcy";
        else if(cellValue == '5')
            return "Evicted";
        else
            return '';
    }

    function getTenantInfo(tenantId) {
        $.ajax({
            type: 'post',
            url: '/EsignatureUser/getTenantRentInfoMoveIn',
            data: {
                class: "GenerateLease",
                action: "getTenantRentInfoMoveIn",
                id: tenantId
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    $(".moveinlabeldata .mi_schedule_move_in").text(data.data.move_in);
                    $(".moveinlabeldata .mi_actual_move_in").text(data.data.actual_move_in);
                    $(".moveinlabeldata .mi_start_date").text(data.data.start_date);
                    $(".moveinlabeldata .mi_end_date").text(data.data.end_date);
                    $(".moveinlabeldata .mi_amount").text(data.data.rent_amount);
                    $(".moveintags .tenantNametag").text(data.data.tenantName);
                    $(".moveintags .propertyAddtag").text(data.data.address_list);
                    $(".moveintags .statetag").text(data.data.state);
                }
            }
        });
    }

    function getParameters(name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.search);
        return (results !== null) ? results[1] || 0 : false;
    }
});