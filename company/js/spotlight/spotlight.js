$(document).ready(function () {
     getSpotData();
     ajaxDataSpot();
     showDatesInDatePicker();
     ajaxTaskReminderData();
     ajaxWorkOrder();
     ajaxVacentoccupaidUnit();
     ajaxTenantInsuranceData();
     ajaxLeaseStatusData();
   //  ajaxPropertyInsuranceData();
     ajaxDelinQuencyData();
     ajaxInventoryData();
     ajaxCredentialData();
     ajaxMoveInData();
     ajaxMoveOutData();
     ajaxpropertyVacantData();
    ajaxAnnounmentData();
    ajaxGetPortfolioData();
    ajaxPaidUnpaidInvoices();
    ajaxPaidUnpaidBills();
    /*ajaxNotifications();*/
    ajaxReceiveablePayable();
    notificationProperty();
    ajaxNotificationData();
     /*------------------------------------Chart js Start----------------------------------------------------------------------*/

    $(document).on('click','#Spotlight-setting .close,#Spotlight-setting .close-bttn',function () {
        window.location.reload();
    });

    /*Give Dynamic height of modal Start*/
    function alignModal(){
        var modalDialog = $(this).find(".modal-dialog");
        /* Applying the top margin on modal dialog to align it vertically center */
        modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
    }
    // Align modal when it is displayed
    $(".modal").on("shown.bs.modal", alignModal);

    // Align modal when user resize the window
    $(window).on("resize", function(){
        $(".modal:visible").each(alignModal);
    });
    /*Give Dynamic height of modal End*/

    $(document).on('click','.announCFbtn',function () {
        var d = new Date(),
            n = d.getMonth(),
            y = d.getFullYear();

        var year = '';
        for (var i = y; i > y-101; i--)
        {
            year += "<option value='"+i+"'>"+i+"</option>";
        }
        $('#announcementChartFilter #months option:eq('+n+')').prop('selected', true);
        $('#announcementChartFilter #years').append(year);
        $('#announcementChartFilter').modal('show');
    });

    $(document).on('click','.notifiCFbtn',function () {
        var d = new Date(),
            n = d.getMonth(),
            y = d.getFullYear();

        var year = '';
        for (var i = y; i > y-101; i--)
        {
            year += "<option value='"+i+"'>"+i+"</option>";
        }
        $('#notificationChartFilter #months option:eq('+n+')').prop('selected', true);
        $('#notificationChartFilter #years').append(year);
        $('#notificationChartFilter').modal('show');
    });


    /*  Print/Download  images Start*/
    var passId;
    $(document).on('click','.listing-download-icon',function () {
        $('.active_ul').removeClass('active_ul');
        $(this).addClass('active_ul');
        $(this).find('.listing-download-list').toggle();
       });

    $("body").click(function(e){
        if(e.target.className !== "listing-download-list"){
            $(".listing-download-list").hide();
        }
    });

    /*print options Start*/
    $(document).on('click','.printImageData',function () {
        var passId = $(this).parent().attr('data-id');
        printCanvas(passId);
    });

    function printCanvas(passId)
    {
        const dataUrl = document.getElementById(passId).toDataURL();

        let windowContent = '<!DOCTYPE html>';
        windowContent += '<html>';
        windowContent += '<head><title>Print canvas</title></head>';
        windowContent += '<body>';
        windowContent += '<img src="' + dataUrl + '">';
        windowContent += '</body>';
        windowContent += '</html>';

        const printWin = window.open('', '', 'width=' + screen.availWidth + ',height=' + screen.availHeight);
        printWin.document.open();
        printWin.document.write(windowContent);

        printWin.document.addEventListener('load', function() {
            printWin.focus();
            printWin.print();
            printWin.document.close();
            printWin.close();
        }, true);
    }
    /*print options End*/

    /*Download PNG Image*/
    $(document).on('click','.download',function () {
        var passId2 = $(this).parent().parent().attr('data-id');
        download_image(passId2);
    });
    function download_image(passId2) {
        var canvas = document.getElementById(passId2);
        image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
        var link = document.createElement('a');
        link.download = "my-image.png";
        link.href = image;
        link.click();
    }
    /*Download PNG Image*/

    /*Download jpeg Image*/
    $(document).on('click', '.download1', function () {
        var passId3 = $(this).parent().parent().attr('data-id');
        download_image1(passId3);
    });
    function download_image1(passId3) {
        var canvas = document.getElementById(passId3);
        image = canvas.toDataURL('image/jpg',1.0).replace("image/jpg", "image/octet-stream");
        var link = document.createElement('a');
        link.download = "my-image.jpeg";
        link.href = image;
        link.click();
    }
    /*Download jpeg Image*/

    /*Download svg Image*/
    $(document).on('click','.download2',function () {

        var passId5 = $(this).parent().parent().parent().parent().find(".listing-download-list").attr('data-id');
        var ctx = new C2S(500,500); //width, height of your desired svg file
//do your normal canvas stuff:
        ctx.fillStyle="red";
        ctx.fillRect(100,100,100,100);
//ok lets serialize to SVG:
        var myRectangle = ctx.getSerializedSvg(true); //true here will replace any named entities with numbered ones.
        //Standalone SVG doesn't support most named entities.
        ctx.prototype.getContext = function (contextId) {
            if (contextId=="2d" || contextId=="2D") {
                return this;
            }
            return null;
        }

        ctx.prototype.style = function () {
            return this.__canvas.style
        }

        ctx.prototype.getAttribute = function (name) {
            return this[name];
        }

        ctx.prototype.addEventListener =  function(type, listener, eventListenerOptions) {
            console.log("canvas2svg.addEventListener() not implemented.")
        }
        console.log(ctx);
        download_image2(passId5);
    });

     function download_image2(passId5){
         var canvas = document.getElementById('receivablesChart');
         var image = canvas.toDataURL('image/jpg',1.0).replace("image/jpg", "image/octet-stream");
         $.ajax({
             type: 'POST',
             url: '/Spotlight',
             data: {
                 class: "Spotlight",
                 action: "downloadsvg",
                 image:image
             },
             success: function (response) {
                 var res= JSON.parse(response);
                 console.log("res",response);
                 var link=document.createElement('a');
                 document.body.appendChild(link);
                 link.target="_blank";
                 link.download="Report.xml";
                 link.href=res.data;
                 link.click();
             }
         });


        /* console.log(">>>>>>>>>>",image);return;

         var svg = diagram_div.find('receivablesChart')[0];
//console.log(svg);return;
         var width = parseInt(svg.width.baseVal.value);
         var height = parseInt(svg.height.baseVal.value);
         var data = editor.getValue();
         var xml = '<?xml version="1.0" encoding="utf-8" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20010904//EN" "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd"><svg xmlns="http://www.w3.org/2000/svg" width="' + width + '" height="' + height + '" xmlns:xlink="http://www.w3.org/1999/xlink"><source><![CDATA[' + data + ']]></source>' + svg.innerHTML + '</svg>';

         var a = $(this);
         a.attr("download", "diagram.svg"); // TODO I could put title here
         a.attr("href", "data:image/svg+xml," + encodeURIComponent(xml));

         var canvas = document.getElementById('receivablesChart');
         image = canvas.toDataURL('image/jpg',1.0).replace("image/jpg", "image/octet-stream");



             console.log(image);return
         var image =  canvas.toDataURL( "data:image/svg+xml;base64");
         //console.log(image);debugger;
         var div = document.getElementById('imageDownload');
         var svg = document.createElementNS('http://www.w3.org/2000/svg','svg');
         svg.setAttributeNS(null,'height','200');
         svg.setAttributeNS(null,'width','200');
         svg.setAttributeNS('http://www.w3.org/1999/xlink','href', image);
         svg.setAttributeNS(null,'x','10');
         svg.setAttributeNS(null,'y','10');
         svg.setAttributeNS(null, 'visibility', 'visible');
         var eee = div.appendChild(svg);*/
         //console.log(eee);debugger;
       /*  var link = document.createElement('a');
         link.download = "my-image.svg";
         link.href = svg;
         link.click();*/
    }

    /*Download svg Image*/

    /*Download Pdf Image*/
    $(document).on('click','.download-pdf2',function () {
        var passId4 = $(this).parent().parent().parent().find(".listing-download-list").attr('data-id');
        downloadPDF2(passId4);
    });
    //download pdf form hidden canvas
    function downloadPDF2(passId4) {
        var newCanvas = document.querySelector('#' + passId4);
        //create image from dummy canvas
        var newCanvasImg = newCanvas.toDataURL("image/jpg", 1.0);
        //creates PDF from img
        var doc = new jsPDF('landscape');
        doc.setFontSize(20);
        doc.setTextColor(255,0,0);
        //doc.text(20, 20, "Super Cool Chart");
        doc.addImage(newCanvasImg, 'JPG', 10, 10, 280, 150 );
        doc.save('new-canvas.pdf');
    }
    /*Download Pdf Image*/


    function saveImage(passId5){

         var img = document.getElementById(passId5).toDataURL("image/svg+xml svg svgz");
         window.open(img,'Download');

    }

    /*$(document).on('click','.download2',function () {
        var passId5 = $(this).parent().parent().parent().parent().find(".listing-download-list").attr('data-id');
        var canvas = document.getElementById(passId5);
        var image = canvas.toDataURL( "image/svg+xml");
        // var image = canvas.toDataURL("image/svg+xml svg svgz");
        console.log("svg >>",image);
        var svgimg = document.createElementNS('http://www.w3.org/2000/svg','image');
        svgimg.setAttributeNS(null,'height','200');
        svgimg.setAttributeNS(null,'width','200');
        svgimg.setAttributeNS('http://www.w3.org/1999/xlink','href', image);
        svgimg.setAttributeNS(null,'x','10');
        svgimg.setAttributeNS(null,'y','10');
        svgimg.setAttributeNS(null, 'visibility', 'visible');
        $('svg').append(svgimg);


        //$('#drawing svg').append(draw.image(svgimg, 200, 300));

        canvg('canvas', canvg, {renderCallback: saveImage(passId5), ignoreMouse: true, ignoreAnimation: true, ignoreDimensions: true});

    });*/
    /*------------------------------------Chart js End----------------------------------------------------------------------*/

    /*------------------------------------Chart and its Ajax Start----------------------------------------------------------------------*/

    /*Receivables / Payables Start*/
    var receivablesChart = document.getElementById('receivablesChart');
    var receivePayble= new Chart(receivablesChart, {
        type: 'pie',
        data: {
            datasets: [{
                label: '# of Votes',
                data: [100, 0],
                backgroundColor: [
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)'
                ],
                borderColor: [
                    'rgb(255,255,255)',
                    'rgb(255,255,255)'
                ],
                borderWidth: 1
            }],
            labels: ['Total Receivables %', 'Total Payables %']
        }
    });
    /* Receivables / Payables End */

    /*Move-Ins Start*/
    var moveInsCharts1 = document.getElementById("moveInsCharts").getContext('2d');

    var chartOptions = {
        legend: {
            display: true,
            position: 'top',
            labels: {
                boxWidth: 80,
                fontColor: 'Black'
            }
        },
        scales: {

            yAxes: [{
                ticks: {
                    display: true,
                    beginAtZero: true,
                    //  steps: 50,
                    max: 5,
                    min: 0,
                    stepSize: 1
                },
                scaleLabel: {
                    display: true,
                    /*labelString: 'Moola',*/
                    fontSize: 14
                },
                // stacked: true
             }],
             xAxes: [{
                 gridLines: {
                     display: false
                 },
                 display: true,

 scaleLabel: {
     labelString: '(M-S)',
         display: true,
         fontSize: 14

 }
 }],
 }
 };

 var moveChartData = new Chart(moveInsCharts1, {
     type: 'line',
     data: {
         labels: ["Two Weeks Ago","Last Week","Current Week","Next Week","Two weeks AHead"],
         datasets: [{
             label: "Move-Ins",
             data: [0, 0, 0, 0,0,0],
             lineTension: 0,
             fill: false,
             borderColor: 'rgb(255, 179, 57)'
         }],
     },
     options: chartOptions
 });
 /*Move-Ins End*/


 /*Move-Out Start*/
 var moveOutCharts1 = document.getElementById("moveOutsCharts").getContext('2d');

 var chartOptionsMoveOut = {
     legend: {
         display: true,
         position: 'top',
         labels: {
             boxWidth: 80,
             fontColor: 'Black'
         }
     },
     scales: {
         yAxes: [{
             ticks: {
                 display: true,
                 beginAtZero: true,
                 //  steps: 50,
                 max: 5,
                 min: 0,
                 stepSize: 1
             },
             scaleLabel: {
                 display: true,
                 /*labelString: 'Moola',*/
                 fontSize: 14
             },
             // stacked: true
         }],
         xAxes: [{
             display: true,
             gridLines: {
                 display: false
             },
             scaleLabel: {
                 labelString: '(M-S)',
                 display: true,
                 fontSize: 14

             }
         }],
     }
 };

 var moveOutChartData = new Chart(moveOutCharts1, {
     type: 'line',
     data: {
         labels: ["Two Weeks Ago","Last Week","Current Week","Next Week","Two weeks AHead"],
         datasets: [{
             label: "Move-Outs",
             data: [0, 0, 0, 0,0,0],
             lineTension: 0,
             fill: false,
             borderColor: 'rgb(255, 179, 57)'
         }],
     },
     options: chartOptionsMoveOut
 });
 /*Move-out End*/

    /* Work Order Start */
    var workOrder = document.getElementById('workOrder');
    var WO1 = ['0-30', '30-60', '60-90', '90+'];
    var work = new Chart(workOrder, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: WO1,
              datasets: [{
                    label: 'Open',
                    backgroundColor: 'rgb(214, 68, 27)',
                    data: [],
              }, {
                    label: 'Closed',
                    backgroundColor: 'rgb(254, 154, 32)',
                    data: []
                  }, {
                    label: 'Completed',
                    backgroundColor: 'rgb(71, 113, 185)',
                    data: []
                  },{
                  label: 'Completed,no need to bill',
                  backgroundColor: 'rgb(147, 181, 86)',
                  data: []
              },{
                  label: 'Scheduled',
                  backgroundColor: 'rgb(236, 197, 31)',
                  data: []
              }],
            // drawBorder: true,
            // borderColor: 'black',
            // borderWidth: 2,
        },
        // Configuration options go here
            options: {
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                hover: {
                    mode: 'index',
                    intersect: false
                },
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false
                    },
                }],
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        stepValue: 10,
                        stepSize: 20,
                        max: 80
                    }
                }]
            }
        }
    });
    /* Work Order End */

 /* Unit Vacancies Start */
 var UnitVacanChart = document.getElementById('UnitVacanChart');
 var unitVacancies = new Chart(UnitVacanChart, {
     type: 'pie',
     data: {
         datasets: [{
             label: '# of Votes',
             data: [100, 0],
             backgroundColor: [
                 'rgb(254, 154, 32)',
                 'rgb(214, 68, 27)'
             ],
             borderColor: [
                 'rgb(255,255,255)',
                 'rgb(255,255,255)'
             ],
             borderWidth: 1
         }],
         labels: ['Vacant Unit %', 'Leased Unit % ']
     }
 });
 /* Unit Vacancies End */

 /*Properties (Vacancy Rates Over a%) Start*/
 var propertiesChart = document.getElementById('propertiesChart');
 var propertiesChart1 = new Chart(propertiesChart, {
     type: 'pie',
     /* scaleSteps:9,
      scaleStartValue:0,*/

     data: {
         datasets: [{
             label: '# of Votes',
             data: [100, 0],
             backgroundColor: [
                 'rgb(116, 70, 169)',
                 'rgb(254, 154, 32)'
             ],
             borderColor: [
                 'rgb(255,255,255)',
                 'rgb(255,255,255)'
             ],
             borderWidth: 1
         }],
         labels: ['Properties Vacancy rate over 5%', 'Properties Vacancy rate below 5%']
     }
 });
 /*Properties (Vacancy Rates Over a%) End*/

 /*Tenant Insurance Start*/
 var tenantInsuranceChart = document.getElementById('tenantInsuranceChart');
 var tenantInsurance = new Chart(tenantInsuranceChart, {
     type: 'pie',
     data: {
         datasets: [{
             label: '# of Votes',
             data: [0, 100],
             backgroundColor: [
                 'rgb(254, 154, 32)',
                 'rgb(214, 68, 27)'
             ],
             borderColor: [
                 'rgb(255, 255, 255)',
                 'rgb(255, 255, 255)'
             ],
             borderWidth: 1
         }],
         labels: ['Active % ', 'Inactive %']
     }
 });
 /*Tenant Insurance End*/

 /*Lease Status Chart Start*/
 var leaseStatusChart = document.getElementById('leaseStatusChart');
 var leaseStatus = new Chart(leaseStatusChart, {
     type: 'pie',
     data: {
         datasets: [{
             label: '# of Votes',
             data: [65, 35],
             backgroundColor: [
                 'rgb(254, 154, 32)',
                 'rgb(214, 68, 27)'
             ],
             borderColor: [
                 'rgb(255, 255, 255)',
                 'rgb(255, 255, 255)'
             ],
             borderWidth: 1
         }],
         labels: ['Active % ', 'Inactive %']
     }
 });

 /*Lease Status End*/

 /*Property  Insurance Status Start*/
 var proptyInsStusChart = document.getElementById('proptyInsStusChart');
 var proptyInsStus = new Chart(proptyInsStusChart, {
     type: 'pie',
     data: {
         datasets: [{
             label: '# of Votes',
             data: [3, 97],
             backgroundColor: [
                 'rgb(254, 154, 32)',
                 'rgb(214, 68, 27)'
             ],
             borderColor: [
                 'rgb(255, 255, 255)',
                 'rgb(255, 255, 255)'
             ],
             borderWidth: 1
         }],
         labels: ['Active % ', 'Inactive %']
     }
 });
 /* Property  Insurance Status*/

    /* Paid / Unpaid Invoices Start*/
    var paidUnpaidInvoicesChart = document.getElementById('paidUnpaidInvoicesChart');
    var paidUnpaidInvoices = new Chart(paidUnpaidInvoicesChart, {
        type: 'pie',
        data: {
            datasets: [{
                label: '# of Votes',
                data: [0,100],
                backgroundColor: [
                    'rgb(254, 154, 32)',
                    'rgb(214, 68, 27)'
                ],
                borderColor: [
                    'rgb(255, 255, 255)',
                    'rgb(255, 255, 255)'
                ],
                borderWidth: 1
            }],
            labels: ['Paid Invoice %', 'Unpaid Invoice %']
        }
    });
    /* Paid / Unpaid Invoices End*/

 /*Paid / Unpaid Bill Status Start*/
 var paidUnpaidBillChart = document.getElementById('paidUnpaidBillChart');
 var paidUnpaidBill = new Chart(paidUnpaidBillChart, {
     type: 'pie',
     data: {
         datasets: [{
             label: '# of Votes',
             data: [0, 100],
             backgroundColor: [
                 'rgb(254, 154, 32)',
                 'rgb(214, 68, 27)'
             ],
             borderColor: [
                 'rgb(255, 255, 255)',
                 'rgb(255, 255, 255)'
             ],
             borderWidth: 1
         }],
         labels: ['Paid Bill %', 'Unpaid Bill %']
     }
 });
 /*Paid / Unpaid Bill Status End*/

 /*Work by Amandeep*/
    var CredentialExp = document.getElementById('CredentialExp').getContext('2d');
    var months = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    var CredentExp = new Chart(CredentialExp, {
        type: 'bar',
        data: {
            labels: months,
            datasets: [{
                label: 'Series:1',
                data: [],
                backgroundColor: [
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)'

                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false
                    },
                }],
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        //  steps: 50,
                        stepValue: 10,
                        stepSize: 50,
                        //  scaleStepWidth : 10,
                        // scaleSteps : 10,
                        max: 150
                    }
                }]
            }
        }

 });
    /* inventory */
    var InventoryExp = document.getElementById('InventoryExp').getContext('2d');
    //   var months = ['jan', 'Feb', 'March', 'April', 'May', 'June','July','Aug','Sept','Oct','Nov','Dec'];
    var InventoryExpiration = new Chart(InventoryExp, {
        type: 'bar',
        data: {
            labels: months,
            datasets: [{
                label: 'Series:1',
                data: [],
                backgroundColor: [
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false
                    },
                }],
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        //  steps: 50,
                        stepValue: 10,
                        stepSize: 5,
                        //  scaleStepWidth : 10,
                        // scaleSteps : 10,
                        max: 15
                    }
                }]
            }
        }
 });

 /* Birthday List START */

 var birthdayList = document.getElementById('birthdayList').getContext('2d');
    var birthdayList = document.getElementById('birthdayList').getContext('2d');

     var Birthday = new Chart(birthdayList, {
         type: 'bar',
         data: {
             labels: months,
             datasets: [{
                 label: 'Series:1',
                 data: [],
                 backgroundColor: [
                     'rgb(71, 113, 185)',
                     'rgb(71, 113, 185)',
                     'rgb(71, 113, 185)',
                     'rgb(71, 113, 185)',
                     'rgb(71, 113, 185)',
                     'rgb(71, 113, 185)',
                     'rgb(71, 113, 185)',
                     'rgb(71, 113, 185)',
                     'rgb(71, 113, 185)',
                     'rgb(71, 113, 185)',
                     'rgb(71, 113, 185)',
                     'rgb(71, 113, 185)',
                 ],
             }]
         },
         options: {
             tooltips: {
                 mode: 'index',
                 intersect: false
             },
             hover: {
                 mode: 'index',
                 intersect: false
             },
             scales: {
                 xAxes: [{
                     gridLines: {
                         display: false
                     },
                 }],
                 yAxes: [{
                     display: true,
                     ticks: {
                         beginAtZero: true,
                         stepValue: 10,
                         stepSize: 5,
                         max: 20
                     }
                 }]
             }
         }
     });
     /* Birthday List End */

    /* Announcement START */
    var AnnouncementList = document.getElementById('Announcement').getContext('2d');
    var Announcelabels = ['Week 1','Week 2','Week 3','Week 4','Week 5'];
    var Announcemnt = new Chart(AnnouncementList, {
        type: 'bar',
        data: {
            labels: Announcelabels,
            datasets: [{
                label: 'Series:1',
                data: [],
                backgroundColor: [
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)'
                ],
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false,
                    },
                }],
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        stepSize:1,
                        max: 5
                    },
                }]
            }
        }
    });
    /* Announcement End */

    /* Notification START */
    var notification = document.getElementById('notification').getContext('2d');
    var AllNotifications = new Chart(notification, {
        type: 'bar',
        data: {
            labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5'],
            datasets: [{
                label: 'Series:1',
                data: [],
                backgroundColor: [
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false
                    },
                }],
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        stepValue: 10,
                        stepSize: 2,
                        max: 10
                    }
                }]
            }
        }
    });
    /* Notification End */

 /* Tasks and Reminders START */

    var taskReminder = document.getElementById('taskReminder').getContext('2d');
    var taskRemind = new Chart(taskReminder, {
        type: 'bar',
        data: {
            labels: months,
            datasets: [{
                label: 'Not Assigned',
                backgroundColor: 'rgb(214, 68, 27)',
                data: []
            }, {
                label: 'Not Started',
                backgroundColor: 'rgb(254, 154, 32)',
                data: []
            }, {
                label: 'In Progress',
                backgroundColor: 'rgb(71, 113, 185)',
                 data: []
                },
                {
                 label: 'Completed',
                 backgroundColor: 'rgb(147, 181, 86)',
                 data: []
                },
                {
                 label: 'Cancelled and Resigned',
                 backgroundColor: 'rgb(236, 197, 31)',
                 data: []
                }],
        },
        backgroundColor: [
            'rgb(214, 68, 27)',
            'rgb(214, 68, 27)',
            'rgb(214, 68, 27)',
            'rgb(214, 68, 27)',
            'rgb(214, 68, 27)',
            'rgb(214, 68, 27)',
            'rgb(214, 68, 27)',
            'rgb(214, 68, 27)',
            'rgb(214, 68, 27)',
            'rgb(214, 68, 27)',
            'rgb(214, 68, 27)',
            'rgb(214, 68, 27)'
        ],
        borderWidth: 1,
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'index',
                intersect: false
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false
                    },
                }],
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        //  steps: 50,
                        stepValue: 10,
                        stepSize: 1,
                        //  scaleStepWidth : 10,
                        // scaleSteps : 10,
                        max: 4
                    }
                }]
            }
        }

     });
     /* Tasks and Reminders End */

    /* Delinquencies START */
    var delinquency = document.getElementById('delinquency').getContext('2d');
    var DelinQuencies = new Chart(delinquency, {
        type: 'bar',
        data: {
            labels: ['Tenant', 'Vendor', 'Owner'],
            datasets: [{
                label: 'Series:1',
                data: [],
                backgroundColor: [
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                    'rgb(214, 68, 27)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false
                    },
                }],
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        //  steps: 50,
                        stepValue: 10,
                        stepSize: 100,
                        max: 400
                    }
                }]
            }
        }
     });
     /* Delinquencies End */



     /*------------------------------------Chart  Ajax End----------------------------------------------------------------------*/

     /* ajaxBirthday Chart */
    function ajaxDataSpot() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getBirthdayData",
            },
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success' && response.code == 200) {
                    var MaxValue = response.maxValue;
                    var Half = response.halfvalue;
                    var YAxes = {
                        beginAtZero: true,
                        stepSize: Half,
                        max: MaxValue
                    }
                    Birthday.options.scales.yAxes[0].ticks = YAxes;
                    Birthday.data.datasets[0].data = response.data.data;
                    Birthday.chart.update();
                }
            }
        });
        return false;
    }
    /* ajaxBirthday Chart */

     /* ajax DelinQuency Chart */
    function ajaxDelinQuencyData() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getComplaintData",
            },
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success' && response.code == 200) {
                    var MaxValue = response.maxValue;
                    var Half = response.halfvalue;
                    var YAxes = {
                        beginAtZero: true,
                        stepSize: Half,
                        max: MaxValue
                    }
                    DelinQuencies.options.scales.yAxes[0].ticks = YAxes;
                    DelinQuencies.data.datasets[0].data = response.data;
                    DelinQuencies.chart.update();
                }
            }
        });
        return false;
    }
    /* ajax DelinQuency Chart */

    /* ajax Announce Chart Start*/

   /* function ajaxAnnounceData() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getAnnouncementData",
            },
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success' && response.code == 200) {
                    var MaxValue = response.maxValue;
                   var Half = response.halfvalue;
                    var YAxes = {
                            beginAtZero: true,
                            stepSize: Half,
                            max: MaxValue
                        }
                    Announcemnt.options.scales.yAxes[0].ticks = YAxes;
                    Announcemnt.data.datasets[0].data = response.data;
                    Announcemnt.chart.update();
                }
            }
        });
        return false;
    }*/
    /* ajax Announce Chart End*/

    /* ajax Task&Reminder Chart */
    function ajaxTaskReminderData() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getTaskReminderData",
            },
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success' && response.code == 200) {
                    var Datasets1 = response.data.NotAssigned.data;
                    var Datasets2 = response.data.NotStarted.data;
                    var Datasets3 = response.data.InProgress.data;
                    var Datasets4 = response.data.Completed.data;
                    var Datasets5 = response.data.CancelledandResigned.data;
                    var alldata = {
                        labels: months,
                        datasets: [{
                            label: 'Not Assigned',
                            backgroundColor: 'rgb(214, 68, 27)',
                            data: Datasets1
                        }, {
                            label: 'Not Started',
                            backgroundColor: 'rgb(254, 154, 32)',
                            data: Datasets2
                        },
                            {
                                label: 'In Progress',
                                backgroundColor: 'rgb(71, 113, 185)',
                                data: Datasets3
                            },
                            {
                                label: 'Completed',
                                backgroundColor: 'rgb(147, 181, 86)',
                                data: Datasets4
                            },
                            {
                                label: 'Cancelled and Resigned',
                                backgroundColor: 'rgb(236, 197, 31)',
                                data: Datasets5
                            }],
                    }
                    var MaxValue = response.maxValue;
                    var Half = response.halfvalue;
                    var YAxes = {
                        beginAtZero: true,
                        stepSize: Half,
                        max: MaxValue
                    }
                    taskRemind.options.scales.yAxes[0].ticks = YAxes;
                    taskRemind.data = alldata;
                    taskRemind.chart.update();
                }
            }
        });
        return false;
    }
    /* ajax Task&Reminder Chart */

    /* ajax Work Order Chart */
    function ajaxWorkOrder() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getWorkOrderData",
            },
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success' && response.code == 200) {

                      console.log(response.data);
                    var WO1 = ['0-30', '30-60', '60-90', '90+'];

                    var Datasets1 = response.data.Open;
                    var Datasets2 = response.data.Closed;
                    var Datasets3 = response.data.Completed;
                    var Datasets4 = response.data.Completednoneedtobill;
                    var Datasets5 = response.data.Scheduled;

                    var alldata = {
                        labels: WO1,
                        datasets: [{
                            label: 'Open',
                            backgroundColor: 'rgb(214, 68, 27)',
                            data: Datasets1
                        }, {
                            label: 'Closed',
                            backgroundColor: 'rgb(254, 154, 32)',
                            data: Datasets2
                        },
                            {
                                label: 'Completed',
                                backgroundColor: 'rgb(71, 113, 185)',
                                data: Datasets3
                            },
                            {
                                label: 'Completed ,no need to bill',
                                backgroundColor: 'rgb(147, 181, 86)',
                                data: Datasets4
                            }, {
                                label: 'Scheduled',
                                backgroundColor: 'rgb(236, 197, 31)',
                                data: Datasets5
                            }],
                    }
                    var MaxValue = response.maxValue;
                    var Half = response.halfvalue;
                    var YAxes = {
                        beginAtZero: true,
                        stepSize: Half,
                        max: MaxValue
                    }
                    work.options.scales.yAxes[0].ticks = YAxes;
                    work.data = alldata;
                    work.chart.update();
                }
            }
        });
        return false;
    }
 /* ajax Work Order Chart */

     /* ajax Inventory Expiration Chart */
    function ajaxInventoryData() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getInventoryExpiration",
            },
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success' && response.code == 200) {
                    var MaxValue = response.maxValue;
                    var Half = response.halfvalue;
                    var YAxes = {
                        beginAtZero: true,
                        stepSize: Half,
                        max: MaxValue
                    }
                    InventoryExpiration.options.scales.yAxes[0].ticks = YAxes;
                    InventoryExpiration.data.datasets[0].data = response.data.data;
                    InventoryExpiration.chart.update();
                }
            }
        });
        return false;
    }
    /* ajax Inventory Expiration Chart */

    /* ajax Credential Expiration Chart */

    function ajaxCredentialData() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getCredentialExpiration",
            },
            dataType: 'json',
            success: function (response) {
                 CredentExp.data.datasets[0].data = response.data.data;
                   CredentExp.chart.update();
            }
        });
        return false;
    }
     /* ajax Credential Expiration Chart */

 /* Pie Chart Unit Vacancies ajax Start */
 function ajaxVacentoccupaidUnit() {
     var base_url = window.location.origin;
     var formData = $('#comp_unitSpotChart').serializeArray();
     $.ajax({
         type: 'POST',
         url: '/Spotlight',
         data: {
             class: "Spotlight",
             action: "getVacentoccupaidUnit",
             formData : formData
         },
         success: function (response) {
             var response = JSON.parse(response);
             if (response.status == 'success' && response.code == 200) {
                 if (response.dataVacant == '0' && response.dataOccupiad == '0') {
                     toastr.error("There is no property in this portfolio !!");
                     $('#unitVacancyChartFilter').modal('hide');
                 }
                 if (response.dataVacant.length && response.dataOccupiad.length > 0) {
                     var data2 = {
                         datasets: [{
                             label: ['sadsa', 'sdadsa'],
                             data: [response.dataVacant, response.dataOccupiad],
                             backgroundColor: [
                                 'rgb(254, 154, 32)',
                                 'rgb(214, 68, 27)'
                             ],
                             borderColor: [
                                 'rgb(255,255,255)',
                                 'rgb(255,255,255)'
                             ],
                             borderWidth: 1
                         }],
                         labels: ['Vacant Unit %', 'Leased Unit % ']
                     }
                     unitVacancies.data = data2;
                     unitVacancies.update();
                     $('#unitVacancyChartFilter').modal('hide');
                 }
             }
         }, error: function (jqXHR, status, err) {
             //   console.log(err);
         },
     });
 }
 /*Pie Chart Unit Vacancies ajax End*/

    function ajaxReceivablePayable() {
        var base_url = window.location.origin;
        var formData = $('#comp_receivableSpotChart').serializeArray();
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getreceivablePayable",
                formData : formData
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log(response);
            }, error: function (jqXHR, status, err) {
                //   console.log(err);
            },
        });
    }

 /* Pie Chart Tenant Insurance ajax Start */
 function ajaxTenantInsuranceData() {
     $.ajax({
         type: 'POST',
         url: '/Spotlight',
         data: {
             class: "Spotlight",
             action: "getTenantInsuranceData",
         },
         success: function (response) {
             var response = JSON.parse(response);
             //console.log("tenant insurance >>",response.dataActive.length);
             console.log("tenant in >>",response);
             if (isNaN(response.dataActive && response.dataInactive)) {
                 return 0;
             }
             if (response.status == 'success' && response.code == 200) {
                 if (response.dataActive.length && response.dataInactive.length > 0) {
                     var data11 = {
                         datasets: [{
                             //label: [,'sdadsa'],
                             data: [response.dataActive, response.dataInactive],
                             backgroundColor: [
                                 'rgb(254, 154, 32)',
                                 'rgb(214, 68, 27)'
                             ],
                             borderColor: [
                                 'rgb(255,255,255)',
                                 'rgb(255,255,255)'
                             ],
                             borderWidth: 1
                         }],
                         labels: ['Active % ', 'Inactive %']
                     }
                     tenantInsurance.data = data11;
                     tenantInsurance.update();
                 }
             }
         }, error: function (jqXHR, status, err) {
             //  console.log(err);
         }, 
     });
 }
 /*Pie Chart Tenant Insurance ajax End*/

 /* Pie Chart Lease Status ajax Start */
 function ajaxLeaseStatusData() {
     $.ajax({
         type: 'POST',
         url: '/Spotlight',
         data: {
             class: "Spotlight",
             action: "getLeaseStatus",
         },
         success: function (response) {
             var response = JSON.parse(response);
             //   console.log(response);
             if (isNaN(response.dataActive && response.dataInactive)) {
                 return 0;
             }
             if (response.status == 'success' && response.code == 200) {
                 if (response.dataActive.length && response.dataInactive.length > 0) {
                     var dataLease = {
                         datasets: [{
                             //label: [,'sdadsa'],
                             data: [response.dataActive, response.dataInactive],
                             backgroundColor: [
                                 'rgb(254, 154, 32)',
                                 'rgb(214, 68, 27)'
                             ],
                             borderColor: [
                                 'rgb(255,255,255)',
                                 'rgb(255,255,255)'
                             ],
                             borderWidth: 1
                         }],
                         labels: ['Active % ', 'Inactive %']
                     }

                     leaseStatus.data = dataLease;
                     leaseStatus.update();
                 }

             }
         }, error: function (jqXHR, status, err) {
             //    console.log(err);
         },
     });
 }
 /*Pie Chart Lease status ajax End*/

 /* Pie Chart Lease Status ajax Start */
 function ajaxPropertyInsuranceData() {
     $.ajax({
         type: 'POST',
         url: '/Spotlight',
         data: {
             class: "Spotlight",
             action: "getPropertyInsurance",
         },
         success: function (response) {
             var response = JSON.parse(response);
             if (response.status == 'success' && response.code == 200) {
                 if (isNaN(response.dataActive && response.dataInactive)) {
                     return 0;
                 }
                 if (response.dataActive.length && response.dataInactive.length > 0) {
                     //   console.log(response);
                     var dataInsStus = {
                         datasets: [{
                             //label: [,'sdadsa'],
                             data: [response.dataActive, response.dataInactive],
                             backgroundColor: [
                                 'rgb(254, 154, 32)',
                                 'rgb(214, 68, 27)'
                             ],
                             borderColor: [
                                 'rgb(255,255,255)',
                                 'rgb(255,255,255)'
                             ],
                             borderWidth: 1
                         }],
                         labels: ['Active % ', 'Inactive %']
                     }

                     proptyInsStus.data = dataInsStus;
                     proptyInsStus.update();

                 }
             }
         }, error: function (jqXHR, status, err) {
             console.log(err);
         },
     });
 }
 /*Pie Chart Lease status ajax End*/

 /* Pie Chart Move In ajax Start */
 function ajaxMoveInData() {
     $.ajax({
         type: 'POST',
         url: '/Spotlight',
         data: {
             class: "Spotlight",
             action: "getMoveInData",
         },
         success: function (response) {
             var response = JSON.parse(response);
           //  console.log(response);
             if (response.status == 'success' && response.code == 200) {
                 var a1 = response.maxData;
                 var b1 = 3;
                 var maxDataSpot1 = a1 + b1;
                 var data = [response.data.twoWeekAgo.ami, response.data.weekago.ami, response.data.currentWeek.ami, response.data.nextWeek.ami, response.data.twoWeekHead.ami];

                 var dataInsStus = {
                     labels: ["Two Weeks Ago", "Last Week", "Current Week", "Next Week", "Two weeks AHead"],
                     datasets: [{
                         label: "Move-Ins",
                         data: [response.data.twoWeekAgo.ami, response.data.weekago.ami, response.data.currentWeek.ami, response.data.nextWeek.ami, response.data.twoWeekHead.ami],
                         lineTension: 0,
                         fill: false,
                         borderColor: 'rgb(255, 179, 57)'
                     }],
                 }
                 moveChartData.options.scales.yAxes[0].ticks = {
                     max: maxDataSpot1,
                     min: 0,
                     stepSize: 1

                 };
                 moveChartData.data = dataInsStus;
                 moveChartData.update();
             }
         }, error: function (jqXHR, status, err) {
             console.log(err);
         },
     });
 }
 /*Pie Chart Move In ajax End*/

 /* Pie Chart Move Out ajax Start */
 function ajaxMoveOutData() {
     $.ajax({
         type: 'POST',
         url: '/Spotlight',
         data: {
             class: "Spotlight",
             action: "getMoveOutData",
         },
         success: function (response) {
             var response = JSON.parse(response);
             if (response.status == 'success' && response.code == 200) {
                 var a = response.maxData;
                 var b = 3;
                 var maxDataSpot = a + b;
                 var datavari = [response.data.twoWeekAgo.amod, response.data.weekago.amod, response.data.currentWeek.amod, response.data.nextWeek.amod, response.data.twoWeekHead.amod];

                 var dataInsStus = {
                     labels: ["Two Weeks Ago", "Last Week", "Current Week", "Next Week", "Two weeks AHead"],
                     datasets: [{
                         label: "Move-Outs",
                         data: datavari,
                         lineTension: 0,
                         fill: false,
                         borderColor: 'rgb(255, 179, 57)'
                     }],
                 }

                 moveOutChartData.options.scales.yAxes[0].ticks = {
                     max: maxDataSpot,
                     min: 0,
                     stepSize: 1

                 };
                 moveOutChartData.data = dataInsStus;
                 moveOutChartData.update();
             }
         }, error: function (jqXHR, status, err) {
             console.log(err);
         },
     });
 }
 /*Pie Chart Unit Vacancies ajax End*/

     /* Pie Chart property vacant status ajax Start */
     function ajaxpropertyVacantData() {
         $.ajax({
             type: 'POST',
             url: '/Spotlight',
             data: {
                 class: "Spotlight",
                 action: "getpropertyVacantData",
             },
             success: function (response) {
                 var response = JSON.parse(response);
                 if (response.status == 'success' && response.code == 200) {
                     if (isNaN(response.vacantOver && response.vacantBelow)) {
                         return 0;
                     }
                     var dataInsStus = {
                         datasets: [{
                             //label: [,'sdadsa'],
                             data: [response.vacantOver, response.vacantBelow],
                             backgroundColor: [
                                 'rgb(116, 70, 169)',
                                 'rgb(254, 154, 32)'
                             ],
                             borderColor: [
                                 'rgb(255,255,255)',
                                 'rgb(255,255,255)'
                             ],
                             borderWidth: 1
                         }],
                         labels: ['Properties Vacancy rate over 5%', 'Properties Vacancy rate below 5%']
                     }
                     propertiesChart1.data = dataInsStus;
                     propertiesChart1.update();
                 }
             }, error: function (jqXHR, status, err) {
                 console.log(err);
             },
         });
     }
 /*Pie Chart property vacant status ajax End*/

 /*-------------------------------------------------Chart and its Ajax End------------------------------------------------------*/


    /* Announcement Start */
    function ajaxAnnounmentData() {
        var base_url = window.location.origin;
        var formData = $('#comp_annoSpotChart').serializeArray();
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getAnnouncementData",
                formData :formData
            },
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success' && response.code == 200) {

                    var MaxValue = response.maxValue;
                    var Half = response.halfvalue;
                    var YAxes = {
                        beginAtZero: true,
                        stepSize: Half,
                        max: MaxValue
                    }
                    Announcemnt.options.scales.yAxes[0].ticks = YAxes;
                    Announcemnt.data.datasets[0].data = response.data;
                    Announcemnt.chart.update();

                }
                $('#announcementChartFilter').modal('hide');
            }
        });
        return false;
    }
    /*  Announcement * End  /

    /* Notification Start */

    function ajaxNotificationData() {
        var base_url = window.location.origin;
        var formData = $('#comp_notifSpotChart').serializeArray();
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getNotificationData",
                formData :formData
            },
            dataType: 'json',
            success: function (response) {
                console.log("notification Data >>",response);
                if (response.status == 'success' && response.code == 200) {

                    var MaxValue = response.maxValue;
                    var Half = response.halfvalue;
                    var YAxes = {
                        beginAtZero: true,
                        stepSize: Half,
                        max: MaxValue
                    }
                    AllNotifications.options.scales.yAxes[0].ticks = YAxes;
                    AllNotifications.data.datasets[0].data = response.data;
                    AllNotifications.chart.update();

                }
                $('#notificationChartFilter').modal('hide');
            }
        });
        return false;
    }
    /*  Notification End */

    /* Paid unpaid invoices  */

    function ajaxPaidUnpaidInvoices() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getPaidUnpaidInvoices",
            },
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success' && response.code == 200) {
                    var active = response.dataActive;
                    var Inactive = response.dataInactive;
                    paidUnpaidInvoices.data.datasets[0].data = [active,Inactive];
                    paidUnpaidInvoices.chart.update();
                }
            }
        });
        return false;
    }

    /* Paid Unpaid Bills Start*/

    function ajaxPaidUnpaidBills() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getPaidUnpaidBills",
            },
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success' && response.code == 200) {
                    var active = response.dataActive;
                    var Inactive = response.dataInactive;
                    console.log(response);
                    paidUnpaidBill.data.datasets[0].data = [active,Inactive];
                    paidUnpaidBill.chart.update();
                }
            }
        });
        return false;
    }
    /*  Paid Unpaid Bills End */

   /* function ajaxNotifications() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getNotifications",
            },
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success' && response.code == 200) {
                    AllNotifications.data.datasets[0].data = [5,7,3,10,9];
                    AllNotifications.chart.update();
                }
            }
        });
        return false;
    }*/

    function ajaxReceiveablePayable() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getReceiveablePayable",
            },
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success' && response.code == 200) {
                    var active = response.dataActive;
                    var Inactive = response.dataInactive;
                    receivePayble.data.datasets[0].data = [active,Inactive] ;
                    receivePayble.chart.update();
                }
            }
        });
        return false;
    }
    /*-------------------------------------------------Chart and its Ajax End------------------------------------------------------

    /* report js*/
    $(document).on('click','.filterby',function () {
        var ReportFilter =  $(this).attr('data-id');
            showPopUps(ReportFilter);
            });

    $(document).on('click','.reset_btn',function () {
        var ReportFilter =  $(this).attr('data-id');
        showPopUps(ReportFilter);
      //  console.log(ReportFilter);
        if(ReportFilter == 'companyfilterby'){
            $('#category').trigger('change');
        } else if(ReportFilter =='Announcementfilterby'){
            $('#title').val('all');
        } else if(ReportFilter =='leasefilterby'){
            $('#lease_report_filter #status').val('all');
        } else if(ReportFilter =='tenantinsfilterby'){
            $('#tenantIns_report_filter #status').val('all');
        } else if(ReportFilter =='birthdayfilterby'){
            $('#module').val('all');
            $('#select_month').val('');
        }  else if(ReportFilter =='billfilterby'){
            $('#bill_report_filter #status').val('all');
        }
        showDatesInDatePicker();


    });
    function showPopUps(Filter) {
        switch(Filter) {
            case "workfilterby":
                workReportFilter('work_report_filter');
                break;
            case "credentialfilterby":
                credentialReportFilter();
                break;
            case "InventoryFilterby":
                inventoryReportFilter();
                break;
            case "TaskFilterby":
                taskRemindReportFilter();
                break;
            case "delinquencyfilterby":
                DelinQuencyReportFilter();
                break;
            case "MoveInFilterby":
                MoveInReportFilter();
                $('#movein_report_filter .blue-btn').addClass('movein_report_submit');
                break;
            case "Unitvaccanyfilterby":
                UnitVaccanyReportFilter();
                break;
            case "leasefilterby":
                LeaseReportFilter('lease_report_filter');
                break;
            case "tenantinsfilterby":
                LeaseReportFilter('tenantIns_report_filter');
                break;
            case "proInsfilterby":
                workReportFilter('proIns_report_filter');
                $('#proIns_report_filter .blue-btn').addClass('proIns_report_submit');
                break;
            case "MoveoutFilterby":
                MoveInReportFilter();
                $('#movein_report_filter .blue-btn').addClass('moveout_report_submit');
                break;
            case "PropertyVaccanyRatesfilterby":
                ProVaccanyRateReportFilter();
                break;
            case "InvoicesFilterby":
                workReportFilter('proIns_report_filter');
                $('#proIns_report_filter .blue-btn').addClass('invoice_report_submit');
                break;
            case "billfilterby":
                billReportFilter('proIns_report_filter');
            //    $('#proIns_report_filter .blue-btn').addClass('invoice_report_submit');
                break;
            case "Receivablesfilterby":
                receivablesReportFilter();
                break;
            case "Notifictnfilterby":
                notificationProperty();
                break;
            default:
                break;
        }
    }

    function showDatesInDatePicker() {

        var date = new Date(), y = date.getFullYear(), m = date.getMonth();
        var firstDay = new Date(y, m, 1);
        var lastDay = new Date(y, m + 1, 0);


        $(".from_date").datepicker({
            dateFormat: jsDateFomat,
            changeMonth: true,
            changeYear: true
        }).datepicker("setDate", firstDay);

        $(".to_date").datepicker({
            dateFormat: jsDateFomat,
            changeMonth: true,
            changeYear: true
        }).datepicker("setDate", lastDay);

    }
    function UnitVaccanyReportFilter() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getPortfolios",
            },
            dataType: 'json',
            success: function (response) {
                var option ="";
                var portfolio  ="";
                response['data']['Properties'].forEach(function(key){
                    option += '<option value="'+key.id+'">'+key.property_name+'</option>';
                });

                response['data']['portfolio'].forEach(function(key){
                    portfolio += '<option value="'+key.id+'">'+key.portfolio_name+'</option>';
                });

                $('#unitvaccany_report_filter .property').html(option);

                $('#unitvaccany_report_filter .property').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
                $('#unitvaccany_report_filter .portfolio').html(portfolio);

                $('#unitvaccany_report_filter .portfolio').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            }
        });
        return false;
    }

    function  ProVaccanyRateReportFilter() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "gePropertyAndManager",
            },
            dataType: 'json',
            success: function (response) {
                var option ="";
                var manager  ="";
                response['data']['Properties'].forEach(function(key){
                    option += '<option value="'+key.id+'">'+key.property_name+'</option>';
                });
                $('#provaccrate_report_filter .select_property').html(option);

                $('#provaccrate_report_filter .select_property').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');

                response['data']['propertyManager'].forEach(function(key){
                    manager += '<option value="'+key.id+'">'+key.name+'</option>';
                });
                $('#provaccrate_report_filter .property_manager').html(manager);

                $('#provaccrate_report_filter .property_manager').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            }
        });
        return false;
    }

    function workReportFilter(formid){
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getAllProperties",
            },
            dataType: 'json',
            success: function (response) {
                var option ="";
                response['data'].forEach(function(key){
                    option += '<option value="'+key.id+'">'+key.property_name+'</option>';
                });
                $('#'+formid+' .select_property').html(option);

                $('#'+formid+' .select_property').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            }
        });
         return false;
    }


    function receivablesReportFilter() {
            $.ajax({
                type: 'POST',
                url: '/Spotlight',
                data: {
                    class: "Spotlight",
                    action: "getPortfolios",
                },
                dataType: 'json',
                success: function (response) {
                    var option ="";
                    var portfolio  ="";
                    response['data']['Properties'].forEach(function(key){
                        option += '<option value="'+key.id+'">'+key.property_name+'</option>';
                    });

                    response['data']['portfolio'].forEach(function(key){
                        portfolio += '<option value="'+key.id+'">'+key.portfolio_name+'</option>';
                    });

                    $('#receivables_report_filter .property').html(option);

                    $('#receivables_report_filter .property').multiselect({
                        includeSelectAllOption: true,
                        allSelectedText: 'All Selected',
                        enableFiltering: true,
                        nonSelectedText: 'Select'
                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                    $('#receivables_report_filter .portfolio').html(portfolio);

                    $('#receivables_report_filter .portfolio').multiselect({
                        includeSelectAllOption: true,
                        allSelectedText: 'All Selected',
                        enableFiltering: true,
                        nonSelectedText: 'Select'
                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                }
            });
            return false;
        }

    function credentialReportFilter(){
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getAllCredentialtype",
            },
            dataType: 'json',
            success: function (response) {
                var option ="";
                response['data'].forEach(function(key){
                    option += '<option value="'+key.id+'">'+key.credential_type+'</option>';
                });
                $('#credential_report_filter #credentialType').html(option);
                $('#credential_report_filter #credentialType').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            }
        });
        return false;
    }

    function inventoryReportFilter(){
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getPropertiesCatSubcat",
            },
            dataType: 'json',
            success: function (response) {
                var option ="";
                var catoption ="";
                response.data['Properties'].forEach(function(key){
                    option += '<option value="'+key.id+'">'+key.property_name+'</option>';
                });
                $('#inventory_report_filter .property').html(option);

                response.data['category'].forEach(function(key){
                    catoption += '<option value="'+key.id+'">'+key.category+'</option>';
                });
                $('#inventory_report_filter #category').html(catoption);

                $('#inventory_report_filter .property').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');

                $('#inventory_report_filter #category').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            }
        });
        return false;
    }

    function MoveInReportFilter(){
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getPortfolios",
            },
            dataType: 'json',
            success: function (response) {
                var option ="";
                var portfolio  ="";
                response['data']['Properties'].forEach(function(key){
                    option += '<option value="'+key.id+'">'+key.property_name+'</option>';
                });
                $('#movein_report_filter .select_property').html(option);

                $('#movein_report_filter .select_property').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');

                response['data']['portfolio'].forEach(function(key){
                    portfolio += '<option value="'+key.id+'">'+key.portfolio_name+'</option>';
                });
                $('#movein_report_filter .portfolio').html(portfolio);

                $('#movein_report_filter .portfolio').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            }
        });
        return false;
    }

    $(document).on('change','#inventory_report_filter #category',function () {
       var id = $(this).val();
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getSubCategorybyCat",
                id : id,
            },
            dataType: 'json',
            success: function (response) {
                var option ="";
                response['data'].forEach(function(key){
                    option += '<option value="'+key.id+'">'+key.sub_category+'</option>';
                });
                $('#inventory_report_filter #sub_category').html(option);
                $('#inventory_report_filter #sub_category').attr('multiple','multiple');
                $('#inventory_report_filter #sub_category').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            }
        });
        return false;
    });

    function taskRemindReportFilter(){
    $.ajax({
        type: 'POST',
        url: '/Spotlight',
        data: {
            class: "Spotlight",
            action: "getAllProperties",
        },
        dataType: 'json',
        success: function (response) {
            var option ="";
            response['data'].forEach(function(key){
                option += '<option value="'+key.id+'">'+key.property_name+'</option>';
            });
            $('#task_report_filter .select_property').html(option);

            $('#task_report_filter .select_property').multiselect({
                includeSelectAllOption: true,
                allSelectedText: 'All Selected',
                enableFiltering: true,
                nonSelectedText: 'Select'
            }).multiselect('selectAll', false).multiselect('updateButtonText');

            $('#task_report_filter #status option[value="all"]').prop("selected",true);
        }
    });
    return false;
    }

    function DelinQuencyReportFilter(){
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getComplaintTypes",
            },
            dataType: 'json',
            success: function (response) {
                var option ="";
                response['data'].forEach(function(key){
                    option += '<option value="'+key.id+'">'+key.complaint_type+'</option>';
                });
                $('#delinquency_report_filter #complaint_type').html(option);

                $('#delinquency_report_filter #complaint_type').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            }
        });
        return false;
    }

    function LeaseReportFilter(formid){
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "GetUsersByUserType",
                usertype:2,
            },
            dataType: 'json',
            success: function (response) {
                var option ="";
                response['data'].forEach(function(key){
                    option += '<option value="'+key.id+'">'+key.name+'</option>';
                });
                $('#'+formid+' .tenant').html(option);

                $('#'+formid+' .tenant').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            }
        });
        return false;
    }

    function billReportFilter(){
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "GetUsersByUserType",
                usertype:3,
            },
            dataType: 'json',
            success: function (response) {
                var option ="";
                response['data'].forEach(function(key){
                    option += '<option value="'+key.id+'">'+key.name+'</option>';
                });
                $('#bill_report_filter .vendors').html(option);

                $('#bill_report_filter .vendors').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            }
        });
        return false;
    }
    $(document).on('click','.bill_report_submit',function (e) {
        e.preventDefault();
        searchBillFilters();
    });

    $(document).on('click','.work_report_submit',function (e) {
        e.preventDefault();
        searchFilters();
    });

    $(document).on('click','.tenantins_report_submit',function (e) {
        e.preventDefault();
        searchTenantInsFilters();
        $('#TenantInsReportFilter').modal('hide');
    });

    $(document).on('click','.credential_report_submit',function (e) {
        e.preventDefault();
        searchCredentialFilter();
        $('#credentialReportFilter').modal('hide');
    });

    $(document).on('click','.birth_report_submit',function (e) {
        e.preventDefault();
        searchBirthdayFilters();
        $('#BirthdayReportFilter').modal('hide');
    });

    $(document).on('click','.movein_report_submit',function (e) {
        e.preventDefault();
        searchMoveInFilters();
        $('#MoveInReportFilter').modal('hide');
    });

    $(document).on('click','.unitvaccant_report_submit',function (e) {
        e.preventDefault();
        searchUnitVaccantFilters();
        $('#UnitVaccanyReportFilter').modal('hide');
    });

    $(document).on('click','.task_report_submit',function (e) {
        e.preventDefault();
        searchTaskreminderFilter();
        $('#taskReminderReportFilter').modal('hide');
    });

    $(document).on('click','.receivables_report_submit',function (e) {
        alert("receivable");
        e.preventDefault();
        //searchUnitVaccantFilters();
        $('#receivablesReportFilter').modal('hide');
    });


    $(document).on('click','.inventory_report_submit',function (e) {
        e.preventDefault();
        searchInventoryFilter();
        $('#inventoryReportFilter').modal('hide');
    });

    $(document).on('click','.moveout_report_submit',function (e) {
        e.preventDefault();
        searchMoveoutFilter();
        $('#MoveInReportFilter').modal('hide');
    });

    $(document).on('click','.lease_report_submit',function (e) {
        e.preventDefault();
        searchLeaseFilters();
        $('#LeaseReportFilter').modal('hide');
    });
    $(document).on('click','.proIns_report_submit',function (e) {
        e.preventDefault();
        searchProInsFilters();
        $('#PropertyInsuranceReportFilter').modal('hide');
    });
    $(document).on('click','.announcemnt_report_submit',function (e) {
        e.preventDefault();
        searchAnnouncemntFilters();
        $('#AnnouncemntReportFilter').modal('hide');
    });

    $(document).on('click','.provaccrate_report_submit',function (e) {
        e.preventDefault();
        searchProVaccRateFilters();
        $('#ProVaccantRateReportFilter').modal('hide');
    });

    $(document).on('click','.delinquency_report_submit',function (e) {
        e.preventDefault();
        searchDelinQuencyFilters();
        $('#DelinquencyeReportFilter').modal('hide');
    });

    $(document).on('click','.invoice_report_submit',function (e) {
        e.preventDefault();
        searchInvoicesFilters();
        $('#PropertyInsuranceReportFilter').modal('hide');
    });
    $(document).on('click','.notification_report_submit',function (e) {
        e.preventDefault();
        searchNotifiFilters();
        $('#NotificationReportFilter').modal('hide');
    });

    function searchFilters(){
        var grid = $("#WorkOrderTable"),f = [];
        var property = $('#work_report_filter .select_property').val() == ''?'all':$('#work_report_filter .select_property').val();
        var from_date = $('#work_report_filter .from_date').val();
        var todate = $('#work_report_filter .todate').val();
        f.push({field: "general_property.id", op: "in", data: property},{field: "work_order.created_on", op: "dateBetween", data: from_date, data2:todate});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        $('#WorkReportFilter').modal('hide');
    }

    function searchBirthdayFilters() {
        var grid = $("#BirthdayTable"),f = [];
        var month = $('#birthday_report_filter #select_month').val() == ''?'all':$('#birthday_report_filter #select_month').val();
        var module =  $('#birthday_report_filter #module').val();
        f.push({field: "users.user_type", op: "eq", data: module},{field: "users.dob", op: "dateMonth", data: month});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function searchCredentialFilter() {
        var grid = $("#CredentialExpTable"),f = [];
        var type = $('#credential_report_filter #credentialType').val() == ''?'all':$('#credential_report_filter #credentialType').val();
        var from_date = $('#credential_report_filter .from_date').val();
        var todate = $('#credential_report_filter .todate').val();
        f.push({field: "users.user_type", op: "in", data: type},{field: "users.created_at", op: "dateBetween", data: from_date, data2:todate});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function  searchTaskreminderFilter() {
        var grid = $("#taskReminderTable"),f = [];
        var property = $('#task_report_filter .select_property').val()== ''?'all':$('#task_report_filter .select_property').val();
        var status = $('#task_report_filter #status').val();
        f.push({field: "task_reminders.property", op: "in", data: property},{field: "task_reminders.status", op: "eq", data: status});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function searchDelinQuencyFilters() {
        var grid = $("#delinquencyDataTable"),f = [];
        var property = $('#delinquency_report_filter #complaint_type').val()== ''?'all':$('#delinquency_report_filter #complaint_type').val();
        var from_date = $('#delinquency_report_filter .from_date').val();
        var todate = $('#delinquency_report_filter .todate').val();
        f.push({field: "complaints.complaint_type_id", op: "in", data: property},{field: "complaints.complaint_date", op: "dateBetween", data: from_date, data2:todate});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }
       function  searchTenantInsFilters() {
             var grid = $("#tenantInsuraceTable"),f = [];
            var tenant = $('#tenantIns_report_filter .tenant').val()== ''?'all':$('#tenantIns_report_filter .tenant').val();
            var status = $('#tenantIns_report_filter #status').val();
            f.push({field: "tenant_renter_insurance.user_id", op: "in", data: tenant},{field: "tenant_renter_insurance.status", op: "eq", data: status});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
       }

    function searchMoveInFilters() {
        var grid = $("#MoveInTable"),f = [];
        var property = $('#movein_report_filter .select_property').val() == ''?'all':$('#movein_report_filter .select_property').val();
        var portfolio = $('#movein_report_filter .portfolio').val();
        f.push({field: "company_property_portfolio.id", op: "in", data: portfolio},{field: "general_property.id", op: "in", data: property});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function  searchUnitVaccantFilters() {
        var grid = $("#UnitVacanciesTable"),f = [];
        var property = $('#unitvaccany_report_filter .property').val() == ''?'all':$('#unitvaccany_report_filter .property').val();
        var portfolio = $('#unitvaccany_report_filter .portfolio').val();
        var from_date = $('#unitvaccany_report_filter .from_date').val();
        var todate = $('#unitvaccany_report_filter .todate').val();
        f.push({field: "company_property_portfolio.id", op: "in", data: portfolio},{field: "general_property.id", op: "in", data: property},{field: "general_property.created_at", op: "dateBetween", data: from_date, data2:todate});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function  searchInventoryFilter() {
        var grid = $("#ajaxInventoryExpTable"),f = [];
        var property = $('#inventory_report_filter .property').val() == ''?'all':$('#inventory_report_filter .property').val();
        var from_date = $('#inventory_report_filter .from_date').val();
        var todate = $('#inventory_report_filter .todate').val();
        var cat = $('#inventory_report_filter #category').val();
        var subcat = $('#inventory_report_filter #sub_category').val();
        if(subcat=='' || subcat == 'undefined'){
            subcat ='all';
        }
        f.push({field: "maintenance_inventory.property_id", op: "in", data: property},{field: "maintenance_inventory.category_id", op: "in", data: cat},{field: "maintenance_inventory.sub_category_id", op: "in", data: subcat},{field: "maintenance_inventory.purchase_date", op: "dateBetween", data: from_date, data2:todate});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function searchLeaseFilters(){
        var grid = $("#leaseStatusSpotTable"),f = [];
        var tenant = $('#lease_report_filter .tenant').val() == ''?'all':$('#lease_report_filter .tenant').val();
        var from_date = $('#lease_report_filter .from_date').val();
        var todate = $('#lease_report_filter .todate').val();
        var status = $('#lease_report_filter #status').val();
        console.log(tenant,from_date,todate,status)
        f.push({field: "users.id", op: "in", data: tenant},{field: "users.status", op: "eq", data: status},{field: "tenant_lease_details.move_in", op: "dateBetween", data: from_date, data2:todate});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function searchNotifiFilters(){
        var grid = $("#NotificationDataTable"),f = [];
        var property = $('#notification_report_filter .property').val() == ''?'all':$('#notification_report_filter .property').val();
        var module_type = $('#notification_report_filter #module_type').val();
        var from_date = $('#notification_report_filter .from_date').val();
        var todate = $('#notification_report_filter .todate').val();
        console.log(property,from_date,todate,module_type)
        f.push({field: "notification_apex.property_id", op: "in", data: property},{field: "users.user_type", op: "eq", data: module_type},{field: "notification_apex.created_at", op: "dateBetween", data: from_date, data2:todate});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function searchProInsFilters(){
        var grid = $("#PropertyInsurenceSpotTable"),f = [];
        var property = $('#proIns_report_filter .select_property').val() == ''?'all':$('#proIns_report_filter .select_property').val();
        var status = $('#proIns_report_filter #status').val();
        f.push({field: "property_insurance.property_id", op: "in", data: property},{field: "property_insurance.status", op: "eq", data: status});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function searchAnnouncemntFilters(){
        var grid = $("#announcementDataTable"),f = [];
        var title = $('#announce_report_filter #title').val() == ''?'all':$('#announce_report_filter #title').val();
        var from_date = $('#lease_report_filter .from_date').val();
        var todate = $('#lease_report_filter .todate').val();
        if(title == 1){
            title = 'System Maintenance';
            cond = 'eq';
        }else if(title == 0){
            title = 'System Maintenance';
              cond = 'nc';
        } else if(title == 'all'){
            title = 'all';
            cond = 'in';
        }
        f.push({field: "announcements.title", op: cond, data: title},{field: "announcements.start_date", op: "dateBetween", data: from_date, data2:todate});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function searchMoveoutFilter(){
        var grid = $("#MoveOutTable"),f = [];
        var property = $('#movein_report_filter .select_property').val() == ''?'all':$('#movein_report_filter .select_property').val();
        var portfolio = $('#movein_report_filter .portfolio').val();
        f.push({field: "company_property_portfolio.id", op: "in", data: portfolio},{field: "general_property.id", op: "in", data: property});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function  searchProVaccRateFilters(){
        var grid = $("#PropertySpotTable"),f = [];
        var property = $('#provaccrate_report_filter .select_property').val() == ''?'all':$('#provaccrate_report_filter .select_property').val();
        var manager = $('#provaccrate_report_filter .property_manager').val();
       // console.log(manager);
        f.push({field: "general_property.id", op: "in", data: property},{field: "general_property.manager_id", op: "in", data: manager});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function searchInvoicesFilters(){
        var grid = $("#InvoicesTable"),f = [];
        var property = $('#proIns_report_filter .select_property').val() == ''?'all':$('#proIns_report_filter .select_property').val();
        var status = $('#proIns_report_filter #status').val();

        f.push({field: "ten_property.id", op: "in", data: property},{field: "accounting_invoices.status", op: "eq", data: status});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function searchBillFilters(){
        var grid = $("#BillDataTable"),f = [];
        var vendor = $('#bill_report_filter .vendors').val() == ''?'all':$('#bill_report_filter .vendors').val();
        var status = $('#bill_report_filter #status').val();

        f.push({field: "company_bills.vendor_id", op: "in", data: vendor},{field: "company_bills.status", op: "eq", data: status});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    /*Chart filter ajax End*/
    $(document).on("click","#save_vehiclelog",function() {
        if($('#comp_unitSpotChart').valid()){
            ajaxVacentoccupaidUnit();
        }
    });

    $(document).on("click","#save_receivable",function() {
        if($('#comp_receivableSpotChart').valid()){
            ajaxReceivablePayable();
        }
    });


    $(document).on("click","#annomntChFilter",function() {
        if($('#comp_annoSpotChart').valid()){
            ajaxAnnounmentData();
        }
    });

    $(document).on("click","#notifiChFilter",function() {
        if($('#comp_notifSpotChart').valid()){
            ajaxNotificationData();
        }
    });

    $(document).on('click','.cancel_Popup',function () {
        $("#unitVacancyChartFilter").modal('hide');
    });

    $(document).on('click','.cancel_Popup4',function () {
        location.reload();

    });

    $(document).on('click','.cancel_Popup1',function () {
        $("#announcementChartFilter").modal('hide');
    });

    function ajaxGetPortfolioData(){
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getportfolioData",
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log("portfolio >>>",response);
                if (response.status == 'success' && response.code == 200) {
                    if (response.data.length > 0) {
                        var portfolio_List = '';
                        for (var i = 0; i < response.data.length; i++) {
                            portfolio_List += '<option value="' + response.data[i].id + '">' + response.data[i].portfolio_name + '</option>';
                        }
                        $('#portfolio_List').append(portfolio_List);
                        $('#receivablesChartFilter #portfolio_List').append(portfolio_List);
                    }
                }
            }
        });
    }

    /*Spotlight sitting Start*/
    $(document).on('click','#Spotlight-setting input[type="checkbox"]',function () {
       // var valueData = $(this).val('0');
        var valueData1 = $(this).val();
        var id = $(this).attr('id');
        if(valueData1 == '1'){
            $(this).val('0');
        }else{
            $(this).val('1');
        }
       // alert(valueData1);
    });

    $(document).on('click',"#Spotlight-setting #addSettingSpotChart #saveSettingBtn",function(e){
        /*alert("here");*/
        e.preventDefault();
        var formData = $('#addSettingSpotChart').serializeArray();
        console.log('formData >> ',formData);
        $.ajax({
            type: 'post',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "restoreSettingSpot",
                formData:formData
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log("res>>>");
                console.log(response.data);
                /*$.each(response.data,function(key,value){
                    if(key != 'id') {
                        console.log("test>>",$("#addSettingSpotChart [name=" + key + "]"));
                        $("#addSettingSpotChart [name=" + key + "]").val(value);
                        console.log('key',key);
                        console.log('value',value);
                        if(value == '0'){
                            $("#addSettingSpotChart[name=" + key + "]").prop( "checked", false );
                            $(".dashboard-flex-graph #"+key).css( "display", "none" );
                        } else if(value == '1') {
                            $("#addSettingSpotChart[name=" + key + "]").prop( "checked", true );
                            $(".dashboard-flex-graph #"+key).css( "display", "block" );
                        }
                    }
                });
               getSpotData();*/

/*
                $("#firstSpotChart input[type='checkbox']").val(response.data.firstSpotChart);
                $("#secSpotChart input[type='checkbox']").val(response.data.secSpotChart);
                $("#thirdSpotChart input[type='checkbox']").val(response.data.thirdSpotChart);
                $("#forthSpotChart input[type='checkbox']").val(response.data.forthSpotChart);
                $("#fifthSpotChart input[type='checkbox']").val(response.data.fifthSpotChart);
                $("#sixSpotChart input[type='checkbox']").val(response.data.sixSpotChart);
                $("#sevenSpotChart input[type='checkbox']").val(response.data.sevenSpotChart);
                $("#eightSpotChart input[type='checkbox']").val(response.data.eightSpotChart);
                $("#nineSpotChart input[type='checkbox']").val(response.data.nineSpotChart);
                $("#tenSpotChart input[type='checkbox']").val(response.data.tenSpotChart);
                $("#elevenSpotChart input[type='checkbox']").val(response.data.elevenSpotChart);
                $("#twelveSpotChart input[type='checkbox']").val(response.data.twelveSpotChart);
                $("#fourteenSpotChart input[type='checkbox']").val(response.data.fourteenSpotChart);
                $("#fifteenSpotChart input[type='checkbox']").val(response.data.fifteenSpotChart);
                $("#sixteenSpotChart input[type='checkbox']").val(response.data.sixteenSpotChart);
                $("#seventeenSpotChart input[type='checkbox']").val(response.data.seventeenSpotChart);
                $("#eighteenSpotChart input[type='checkbox']").val(response.data.eighteenSpotChart);*/
                getSpotData();
                toastr.success(response.message);
                $('#Spotlight-setting').modal('hide');
                toastr.success(response.message);
                window.location.href = '/Dashboard/Dashboard';

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {

                });
            }
        });
        return false;
    });

    function getSpotData() {
        $.ajax({
            type: 'post',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getSettingSpot"
            },
            success: function (response) {
                var response = JSON.parse(response);
                /*console.log("dsfdsfds>>dfsdf>>",response);*/
                if(response.data != "" ){

                    $.each(response.data,function(key,value){
                        if(key != 'id') {
                            var element = $("#addSettingSpotChart [name=" + key + "]");
                            console.log('element',element);
                            $("#addSettingSpotChart [name=" + key + "]").val(value);
                            console.log('key',key);
                            console.log('value',value);
                            if(value == '0'){
                                $("#addSettingSpotChart [name=" + key + "]").prop( "checked", false );
                                $(".dashboard-flex-graph #"+key).css( "display", "none" );
                            } else if(value == '1') {
                                $("#addSettingSpotChart [name=" + key + "]").prop( "checked", true );
                                $(".dashboard-flex-graph #"+key).css( "display", "block" );
                            }
                        }
                    });


                    /*if(response.data.eightSpotChart == '0' ){
                        $("#addSettingSpotChart #eightSpotChart").prop( "checked", false );
                        $(".dashboard-flex-graph #eightSpotChart").css( "display", "none" );

                    }
                    if( response.data.seventeenSpotChart == '0'){
                        $("#addSettingSpotChart #seventeenSpotChart").prop( "checked", false );
                        $(".dashboard-flex-graph #seventeenSpotChart").css( "display", "none" );
                    }
                    if(response.data.elevenSpotChart == '0') {
                        $("#addSettingSpotChart #elevenSpotChart").prop("checked", false);
                        $(".dashboard-flex-graph #elevenSpotChart").css("display", "none");
                    }
                    if(response.data.secSpotChart == '0') {
                        $("#addSettingSpotChart #secSpotChart").prop( "checked", false );
                        $(".dashboard-flex-graph #secSpotChart").css( "display", "none" );
                    }
                    if(response.data.thirdSpotChart == '0') {
                        $("#addSettingSpotChart #thirdSpotChart").prop("checked", false);
                        $(".dashboard-flex-graph #thirdSpotChart").css("display", "none");
                    }
                    if(response.data.forthSpotChart == '0') {
                        $("#addSettingSpotChart #forthSpotChart").prop("checked", false);
                        $(".dashboard-flex-graph #forthSpotChart").css( "display", "none" );
                    }
                    if(response.data.fifthSpotChart == '0') {
                        $("#addSettingSpotChart #fifthSpotChart").prop("checked", false);
                        $(".dashboard-flex-graph #fifthSpotChart").css("display", "none");
                    }
                    if(response.data.sixSpotChart == '0') {
                        $("#addSettingSpotChart #sixSpotChart").prop( "checked", false );
                        $(".dashboard-flex-graph #sixSpotChart").css( "display", "none" );
                    }
                    if(response.data.sevenSpotChart == '0') {
                        $("#addSettingSpotChart #sevenSpotChart").prop("checked", false);
                        $(".dashboard-flex-graph #sevenSpotChart").css("display", "none");
                    }
                    if(response.data.nineSpotChart == '0') {
                        $("#addSettingSpotChart #nineSpotChart").prop( "checked", false );
                          $(".dashboard-flex-graph #nineSpotChart").css( "display", "none" );
                    }
                    if(response.data.tenSpotChart == '0') {
                        $("#addSettingSpotChart #tenSpotChart").prop("checked", false);
                        $(".dashboard-flex-graph #tenSpotChart").css("display", "none");
                    }
                    if(response.data.twelveSpotChart == '0') {
                        $("#addSettingSpotChart #twelveSpotChart").prop("checked", false);
                        $(".dashboard-flex-graph #twelveSpotChart").css( "display", "none" );
                    }
                    if(response.data.fourteenSpotChart == '0') {
                        $("#addSettingSpotChart #fourteenSpotChart").prop("checked", false);
                        $(".dashboard-flex-graph #fourteenSpotChart").css( "display", "none" );
                    }
                    if(response.data.fifteenSpotChart == '0') {
                        $("#addSettingSpotChart #fifteenSpotChart").prop("checked", false);
                        $(".dashboard-flex-graph #fifteenSpotChart").css( "display", "none" );
                    }
                    if(response.data.sixteenSpotChart == '0') {
                        $("#addSettingSpotChart #sixteenSpotChart ").prop("checked", false);
                        $(".dashboard-flex-graph #sixteenSpotChart ").css("display", "none");
                    }
                    if(response.data.eighteenSpotChart == '0') {
                        $("#addSettingSpotChart #eighteenSpotChart").prop( "checked", false );
                        $(".dashboard-flex-graph #eighteenSpotChart").css( "display", "none" );
                    }
                    if(response.data.firstSpotChart == '0') {
                        $("#addSettingSpotChart #firstSpotChart ").prop("checked", false);
                        $(".dashboard-flex-graph #firstSpotChart ").css("display", "none");
                    }
                    if(response.data.thirteenSpotChart == '0') {
                        $("#addSettingSpotChart #thirteenSpotChart").prop( "checked", false );
                        $(".dashboard-flex-graph #thirteenSpotChart").css( "display", "none" );
                    }*/
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    }
    /*Spotlight sitting End*/
});


$(document).on('click','.receivableChartFilter',function () {
    $('#receivablesChartFilter').modal('show');
});


function notificationProperty(){
    $.ajax({
        type: 'POST',
        url: '/Spotlight',
        data: {
            class: "Spotlight",
            action: "getAllProperties",
        },
        dataType: 'json',
        success: function (response) {
            console.log("notification here",response);
            var option ="";
            response['data'].forEach(function(key){
                option += '<option value="'+key.id+'">'+key.property_name+'</option>';
            });
            $('#notification_report_filter .property').html(option);

            $('#notification_report_filter .property').multiselect({
                includeSelectAllOption: true,
                allSelectedText: 'All Selected',
                enableFiltering: true,
                nonSelectedText: 'Select'
            }).multiselect('selectAll', false).multiselect('updateButtonText');
        }
    });
    return false;
}