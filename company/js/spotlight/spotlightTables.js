$(document).ready(function () {

    $('.chartbtn').hide();
    $('.filterby').hide();


    $(document).on('click','.rptbtn',function () {
        $(this).hide();
      $(this).closest('.mob-change').find('.filterby').show();
      $(this).closest('.mob-change').find('.chartbtn').show();
      $(this).closest('.mob-change').find('.Filterchart').hide();
     var  canvasID = $(this).attr('data-id');
      var tableID = $(this).attr('data-tableid');
        $('#'+canvasID).parent().children('.listing-download-icon').hide();
        console.log($(this).parent());



        switch(canvasID) {
            case "workOrder":
                $('#'+tableID).jqGrid('GridUnload');
                WorkOrder();
                break;
            case "moveInsCharts":
                $('#'+tableID).jqGrid('GridUnload');
                MoveInList();
                break;
                case "UnitVacanChart":
                $('#'+tableID).jqGrid('GridUnload');
                    UnitVacaniesList();
                break;
                case "moveOutsCharts":
                $('#'+tableID).jqGrid('GridUnload');
                    MoveOutList();
                break;
                case "CredentialExp":
                $('#'+tableID).jqGrid('GridUnload');
                    CredentialExpList();
                break;
                case "InventoryExp":
                $('#'+tableID).jqGrid('GridUnload');
                    InventoryExpList();
                break;
            case "birthdayList":
                $('#'+tableID).jqGrid('GridUnload');
                Birthdaylist();
                break;
            case "taskReminder":
                $('#'+tableID).jqGrid('GridUnload');
                TaskReminderlist();
                break;
            case "tenantInsuranceChart":
                $('#'+tableID).jqGrid('GridUnload');
                jqGridTenantinsurace('All');
                break;
            case "leaseStatusChart":
                $('#'+tableID).jqGrid('GridUnload');
                jqGridLeaseStatus('All');
                break;
            case "proptyInsStusChart":
                $('#'+tableID).jqGrid('GridUnload');
                jqGridPropertyInsurance('all');
                break;
            case "Announcement":
                $('#'+tableID).jqGrid('GridUnload');
                jqGridannouncement('all');
                break;
            case "propertiesChart":
                $('#'+tableID).jqGrid('GridUnload');
                jqGridProperty('all');
                break;
            case "delinquency":
                $('#'+tableID).jqGrid('GridUnload');
                 delinquencyList('All');
                break;
            case "paidUnpaidInvoicesChart":
                $('#'+tableID).jqGrid('GridUnload');
                InvoicesList();
                break;
            case "paidUnpaidInvoicesChart":
                $('#'+tableID).jqGrid('GridUnload');
                InvoicesList();
                break;
            case "paidUnpaidBillChart":
                $('#'+tableID).jqGrid('GridUnload');
                BillList();
                break;
            case "receivablesChart":
                $('#'+tableID).jqGrid('GridUnload');
                receivablesTableList();
                break;
            case "notification":
                $('#'+tableID).jqGrid('GridUnload');
                notificationTableList();
                break;
            default:
                break;
        }

        var tableshowID = $(this).attr('tableshowID');
           $('.'+tableshowID).show();
             $('#'+canvasID).hide();

        var $grid = $("#"+tableID),
            newWidth = $grid.closest(".ui-jqgrid").parent().width();
        $grid.jqGrid("setGridWidth", newWidth, true);

    });

    $('.chartbtn').click(function () {
        $(this).hide();
        $(this).closest('.mob-change').find('.filterby').hide();
        $(this).closest('.mob-change').find('.rptbtn').show();
        $(this).closest('.mob-change').find('.Filterchart').show();
        var  canvasID = $(this).attr('data-id');
        var tableID = $(this).attr('data-tableid');

        var tableshowID = $(this).attr('tableshowID');
        $('#'+canvasID).show();
        $('#'+canvasID).parent().children('.listing-download-icon').show();
        $('.'+tableshowID).hide();
    });

   // Work order grid start

    function WorkOrder() {
        var sortColumn = 'work_order.updated_at';
        var table = 'work_order';
        var columns = ['Property ID', 'Property', 'State', 'City','Property Manager','No. of Buildings', 'No. of Units','property_id','Open','InProgress','Closed','Status'];
        //'Property Manager','Open','Closed','In Progress',
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [];
        var extra_columns = [];
        var joins = [{table: 'work_order', column: 'property_id', primary: 'id', on_table: 'general_property'}];
        var columns_options = [{ name: 'Property ID',
                index: 'property_id',
                width: 100,
                searchoptions: {sopt: conditions},
                table: 'general_property' ,alias:'property_random'},
            {
                name: 'Property',
                index: 'property_name',
                width: 200,
                searchoptions: {sopt: conditions},
                table: 'general_property'
            },
            {name: 'State', index: 'state', width: 100, searchoptions: {sopt: conditions}, table: 'general_property'},
            {name: 'City', index: 'city', width: 100, searchoptions: {sopt: conditions}, table: 'general_property'},
            {name: 'Property Manager', index: 'manager_list', width: 100, searchoptions: {sopt: conditions}, table: 'general_property'},
            {
                name: 'No. of Buildings',
                index: 'no_of_buildings',
                width: 100,
                searchoptions: {sopt: conditions},
                table: 'general_property'
            },
            {
                name: 'No. of Units',
                index: 'no_of_units',
                width: 100,
                searchoptions: {sopt: conditions},
                table: 'general_property'
            },
            {name: 'property_id', index: 'property_id', hidden:true , width: 200, searchoptions: {sopt: conditions}, table: table},
            {name: 'Open', index: 'status_id', width: 100, searchoptions: {sopt: conditions}, table: table, change_type: 'count_workOrders',  type:'Open'},
            {name: 'InProgress', index: 'status_id', width: 100, searchoptions: {sopt: conditions}, table: table, change_type: 'count_workOrders', type:'InProgress'},
            {name: 'Closed', index: 'status_id', width: 100, searchoptions: {sopt: conditions}, table: table, change_type: 'count_workOrders', type:'Closed'},
            {name: 'Status', index: 'status_id', width: 100, searchoptions: {sopt: conditions}, table: table},

        ];
        var ignore_array = [];
        jQuery("#WorkOrderTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                //  select: select_column,
                columns_options: columns_options,
                ignore: ignore_array,
                joins: joins,
                extra_where: extra_where,
                extra_columns: extra_columns,
                deleted_at: 'true'
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Work Orders",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10,
             left: 200,
             drag: true,
             resize: false
             } // search options
        );
    }

    /*Work order grid End*/

    function Birthdaylist() {
        var table = 'users';
        var columns = ['Name', 'Type', 'Birth Date'];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [];
        var extra_columns = [];

        var columns_options = [
            {name: 'Name', index: 'name', width: 100, searchoptions: {sopt: conditions}, table: table},
            {
                name: 'Type',
                index: 'user_type',
                width: 200,
                searchoptions: {sopt: conditions},
                table: table,
                formatter: typeformatter
            },
            {name: 'Birth Date', index: 'dob', width: 100, searchoptions: {sopt: conditions}, table: table},
        ];
        var ignore_array = [];
        jQuery("#BirthdayTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                //  select: select_column,
                columns_options: columns_options,
                ignore: ignore_array,
                //   joins:joins,
                extra_where: extra_where,
                extra_columns: extra_columns,
                deleted_at: 'true'
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Birthday List ",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }

    /*Formatter*/
    function typeformatter(cellvalue, options, rowObject) {
        if (cellvalue != undefined) {
            var type = "";
            if (cellvalue == 1) {
                type = 'Admin';
            } else if (cellvalue == 2) {
                type = 'Tenant';
            } else if (cellvalue == 3) {
                type = 'Owner';
            } else if (cellvalue == 4) {
                type = 'Vendor';
            } else if (cellvalue == 5) {
                type = 'Employee';
            }
            return type;
        }
    }
    /*Formatter*/
    /*Birthday grid End*/

    /*Task and Reminder grid start*/

    function TaskReminderlist() {

        var sortColumn = 'task_reminders.updated_at';
        var table = 'task_reminders';
        var columns = ['Title', 'Property', 'Building', 'Unit', 'Assigned to', 'Status'];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [];
        var extra_columns = [];
        var joins = [{table: 'task_reminders', column: 'property', primary: 'id', on_table: 'general_property'},
            {table: 'task_reminders', column: 'building', primary: 'id', on_table: 'building_detail'},
            {table: 'task_reminders', column: 'unit', primary: 'id', on_table: 'unit_details'},
            {table: 'task_reminders', column: 'assigned_to', primary: 'id', on_table: 'users'}
        ];

        var columns_options = [
            {name: 'Title', index: 'title', width: 100, searchoptions: {sopt: conditions}, table: table},
            {
                name: 'Property',
                index: 'property_name',
                width: 200,
                searchoptions: {sopt: conditions},
                table: 'general_property'
            },
            {
                name: 'Building',
                index: 'building_name',
                width: 100,
                searchoptions: {sopt: conditions},
                table: 'building_detail'
            },
            {
                name: 'Unit',
                index: 'unit_prefix',
                width: 80,
                align: "left",
                searchoptions: {sopt: conditions},
                table: 'unit_details',
                change_type: 'combine_column_hyphen2',
                extra_columns: ['unit_prefix', 'unit_no'],
                original_index: 'unit_prefix'
            },
            {name: 'Assigned to', index: 'name', width: 100, searchoptions: {sopt: conditions}, table: 'users'},
            {
                name: 'Status',
                index: 'status',
                width: 100,
                searchoptions: {sopt: conditions},
                table: table,
                formatter: statusformatterTask
            },
        ];
        var ignore_array = [];
        jQuery("#taskReminderTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                //  select: select_column,
                columns_options: columns_options,
                ignore: ignore_array,
                joins: joins,
                extra_where: extra_where,
                extra_columns: extra_columns,
                deleted_at: 'true'
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Task & Reminders",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }

    /*formatter*/

    function statusformatterTask(cellvalue, options, rowObject) {
        if (cellvalue != undefined) {
            var type = "";
            if (cellvalue == 1) {
                type = 'Not Assigned';
            } else if (cellvalue == 2) {
                type = 'Not Started';
            } else if (cellvalue == 3) {
                type = 'In Progress';
            } else if (cellvalue == 3) {
                type = 'Completed';
            } else if (cellvalue == 3) {
                type = 'Cancelled and Resigned';
            }
            return type;
        }
    }


    /*formatter*/
    /*Task and Reminder grid End*/

    /*Property Table Start*/

    /**
     * jqGrid Initialization function
     * @param status
     */

    function jqGridProperty(status) {
        if (jqgridNewOrUpdated == 'true') {
            var sortOrder = 'desc';
            var sortColumn = 'general_property.update_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'general_property.property_name';
        }
        var table = 'general_property';
        var columns = ['Property ID','Property','clone_status','Property Address','State','City','Property Manager', 'No. of Buildings', 'No. of Units', 'Type', 'total_unit', 'No. of  Vacant Units', 'Status', 'Owner'];
        var select_column = [];
        var joins = [{table: 'general_property', column: 'property_type', primary: 'id', on_table: 'company_property_type'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['general_property.deleted_at', 'general_property.address1', 'general_property.address2', 'general_property.address3', 'general_property.address4', 'general_property.manager_id', 'general_property.owner_id'];
        var extra_where = [];
        var columns_options = [
            {name: 'Property ID', title:false, index: 'property_id', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'Property', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'clone_status',hidden:true, title:false,hidden:true ,index: 'clone_status', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Property Address',hidden:true, index: 'address_list', width: 100, searchoptions: {sopt: conditions}, table: table, change_type: 'combine_column_line', extra_columns: ['address1', 'address2', 'address3', 'address4'], update_column: 'address_list',original_index: 'address1', classes: 'pointer',type:'line'},
            {name: 'State', index: 'state', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyUnitBuilding, classes: 'pointer'},
            {name: 'City', index: 'city', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyUnitBuilding, classes: 'pointer'},
            {name: 'Property Manager', index: 'manager_list', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'serialize', secondTable: 'users', index2: 'name', update_column: 'manager_list', original_index: 'manager_id', classes: 'pointer',type:'line'},
            {name: 'No. of Buildings', index: 'building_exist', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyUnitBuilding, classes: 'pointer'},
            {name: 'No. of Units', index: 'unit_exist', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyUnitBuilding, classes: 'pointer'},
            {name: 'Type',hidden:true, index: 'property_type', width: 80, align: "center", searchoptions: {sopt: conditions}, table: 'company_property_type', classes: 'pointer'},
            {name: 'total_unit', index: 'no_of_units',hidden:true, width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'No. of Vacant Units',index: 'vacant', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',change_type: 'property_vacant',update_column:'vacant'},
            {name: 'Status', index: 'status', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyStatus, classes: 'pointer'},
            {name: 'Owner', index: 'owner_list',hidden:true, width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'serialize', secondTable: 'users', index2: 'name', update_column: 'owner_list', original_index: 'owner_id', classes: 'pointer',type:'line',attr:[{name:'flag',value:'property'}]},
            // {name: 'Short-Term Rental', index: 'is_short_term_rental', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyShortTermRental, classes: 'pointer'},
          /*  {name: 'Action', index: 'select', title: false, width: 80, align: "right", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table,formatter:actionFmatter}*/
        ];
        var ignore_array = [];
        jQuery("#PropertySpotTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Properties",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
        );
    }

    /**
     *  function to format property name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyName(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            var flagValue = $(rowObject.Action).attr('flag');
            var id = $(rowObject.Action).attr('data_id');
            var clone = rowObject.clone_status;
            var flag = '';
            if (rowObject.Status == '4') {
                if (flagValue == 'yes') {
                    if (clone == 1) {
                        if (cellValue.indexOf("_copy") >= 0) {
                            var cellValueData = cellValue.replace("_copy", "");
                            return '<div class="tooltipgridclass"><span><span style="color:#FF1A1A;text-decoration:underline;font-weight: bold;">' + cellValueData + '</span><span style="color:red;">_copy</span><a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id=' + id + '" ><img src="/company/images/Flag.png"></a></span><span class="tooltiptextbotclass">THIS PROPERTY IS ON THE MARKET FOR SALE</span></div>';
                        } else {
                            return '<div class="tooltipgridclass"><div><span style="color:#FF1A1A;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id=' + id + '" ><img src="/company/images/Flag.png"></a></span><span class="tooltiptextbotclass">THIS PROPERTY IS ON THE MARKET FOR SALE</span></div>';
                        }
                    } else {
                        return '<div class="tooltipgridclass"><span style="color:#FF1A1A;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id=' + id + '" ><img src="/company/images/Flag.png"></a></span><span class="tooltiptextbotclass">THIS PROPERTY IS ON THE MARKET FOR SALE</span></div>';
                    }
                } else {
                    if (clone == 1) {
                        if (cellValue.indexOf("_copy") >= 0) {
                            var cellValueData = cellValue.replace("_copy", "");
                            return '<div class="tooltipgridclass"><span><span style="color:#FF1A1A;text-decoration:underline;font-weight: bold;">' + cellValueData + '</span><span style="color:red;">_copy</span></span><span class="tooltiptextbotclass">THIS PROPERTY IS ON THE MARKET FOR SALE</span></div>';
                        } else {
                            return '<div class="tooltipgridclass"><span><span style="color:#FF1A1A;text-decoration:underline;font-weight: bold;">' + cellValue + '</span><span class="tooltiptextbotclass">THIS PROPERTY IS ON THE MARKET FOR SALE</span></div>';
                        }
                    } else {
                        return '<div class="tooltipgridclass"><span style="color:#FF1A1A;text-decoration:underline;font-weight: bold;">' + cellValue + '</span><span class="tooltiptextbotclass">THIS PROPERTY IS ON THE MARKET FOR SALE</span></div>';
                    }
                }
            } else {
                if (flagValue == 'yes') {
                    if (clone == 1) {
                        if (cellValue.indexOf("_copy") >= 0) {
                            var cellValueData = cellValue.replace("_copy", "");
                            return '<span> <span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValueData + '</span><span style="color:red;">_copy</span><a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id=' + id + '" ><img src="/company/images/Flag.png"></a></span>';
                        } else {
                            return '<span> <span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id=' + id + '" ><img src="/company/images/Flag.png"></a></span>';
                        }
                    } else {
                        return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id=' + id + '" ><img src="/company/images/Flag.png"></a></span>';
                    }
                } else {
                    if (clone == 1) {
                        if (cellValue.indexOf("_copy") >= 0) {
                            var cellValueData = cellValue.replace("_copy", "");
                            return '<span><span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValueData + '</span><span style="color:red;">_copy</span></span>';
                        } else {
                            return '<span><span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
                        }
                    } else {
                        return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
                    }

                }
            }
        }

    }

    /**
     * function to format property unit and building
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyUnitBuilding(cellValue, options, rowObject) {
        if (cellValue == '') {
            return '<span data_redirect="no" style="text-decoration: underline;">0</span>';
        } else {
            return '<span data_redirect="yes" style="text-decoration: underline;">' + cellValue + '</span>';
        }
    }

    /**
     * function to format property status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyStatus(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "Active";
        else if (cellValue == '2')
            return "Active";
        else if (cellValue == '3')
            return "Archive";
        else if (cellValue == '4')
            return "Active";
        else if (cellValue == '5')
            return "Active";
        else if (cellValue == '6')
            return "Resign";
        else if (cellValue == '7')
            return "Active";
        else if (cellValue == '8')
            return "Active";
        else
            return '';
    }

    /*Property Table Start*/

    /*Property Insurence Status Start*/

    /**
     * jqGrid Initialization function
     * @param status
     */
    //jqGridPropertyInsurance('all');
    function jqGridPropertyInsurance(status) {
        if (jqgridNewOrUpdated == 'true') {
            var sortOrder = 'desc';
            var sortColumn = 'property_insurance.update_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'general_property.property_name';
        }
        var table = 'property_insurance';
        var columns = ['Property Name', 'Insurance type', 'Provider', 'Start Date', 'End Date', 'Policy #', 'Amount', 'Status'];
        var select_column = [];
        var joins = [{
            table: 'property_insurance',
            column: 'property_id',
            primary: 'id',
            on_table: 'general_property'
        }, {
            table: 'property_insurance',
            column: 'company_insurance_type',
            primary: 'id',
            on_table: 'company_property_insurance'
        }];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['property_insurance.deleted_at'];
        var extra_where = [];
        var columns_options = [
            {
                name: 'Property Name',
                index: 'property_name',
                width: 80,
                align: "left",
                searchoptions: {sopt: conditions},
                table: 'general_property'
            },
            {
                name: 'Insurance type',
                index: 'insurance_type',
                width: 80,
                align: "left",
                searchoptions: {sopt: conditions},
                table: 'company_property_insurance'
            },
            {
                name: 'Provider',
                index: 'insurance_company_name',
                width: 80,
                align: "center",
                searchoptions: {sopt: conditions},
                table: table,
                classes: 'pointer'
            },
            {
                name: 'Start Date',
                index: 'start_date',
                width: 100,
                searchoptions: {sopt: conditions},
                table: table,
                change_type: 'date'
            },
            {
                name: 'End Date',
                index: 'end_date',
                width: 100,
                searchoptions: {sopt: conditions},
                table: table,
                change_type: 'date'
            },
            {
                name: 'Policy #',
                index: 'policy',
                width: 80,
                align: "center",
                searchoptions: {sopt: conditions},
                table: table,
                classes: 'pointer'
            },
            {
                name: 'Amount',
                index: 'coverage_amount',
                width: 80,
                align: "center",
                searchoptions: {sopt: conditions},
                table: table,
                classes: 'pointer'
            },
            {
                name: 'Status',
                index: 'status',
                width: 80,
                align: "center",
                searchoptions: {sopt: conditions},
                table: table,
                formatter: statusFormatterDate,
                classes: 'pointer'
            },
        ];
        var ignore_array = [];
        jQuery("#PropertyInsurenceSpotTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Property Insurance",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
        );
    }

    function statusFormatterDate(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            var endDate = rowObject['End Date'];
            var startDay = new Date(endDate);
            var endDay = new Date();
            var millisecondsPerDay = 1000 * 60 * 60 * 24;

            var millisBetween = startDay.getTime() - endDay.getTime();
            var days = millisBetween / millisecondsPerDay;
            var remainingDays = Math.floor(days);
            if (remainingDays >= 0) {
                return "active";
            } else {
                return "inactive";
            }
        }
    }
    /*Property Insurence Status End*/
    /*Announcement Start*/
    /**
     * jqGrid Initialization function
     * @param status
     */
    //jqGridPropertyInsurance('all');
    function jqGridannouncement(status) {
        if (jqgridNewOrUpdated == 'true') {
            var sortOrder = 'desc';
            var sortColumn = 'announcements.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'announcements.title';
        }
        var table = 'announcements';
        var columns = ['Type', 'Date of Announcement', 'Action'];
        var select_column = [];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['announcements.deleted_at'];
        var extra_where = [];
        var columns_options = [
            {
                name: 'Type',
                index: 'title',
                width: 220,
                align: "left",
                searchoptions: {sopt: conditions},
                table: 'announcements'
            },
            {
                name: 'Date of Announcement',
                index: 'end_date',
                width: 220,
                align: "left",
                searchoptions: {sopt: conditions},
                table: 'announcements',
                change_type: 'date'
            },
            {
                name: 'Action',
                index: 'action',
                width: 220,
                align: "center",
                searchoptions: {sopt: conditions},
                table: table,
                formatter: actionFormatter,
                classes: 'pointer'
            },
        ];
        var ignore_array = [];
        jQuery("#announcementDataTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Announcements",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
        );
    }

    /*Announcement End*/
    function actionFormatter(cellValue, options, rowObject) {
        if (rowObject != 'undefined') {
            return '<a class="pointer" href="/Announcement/Announcements">view</a>';
        }
    }




    /* Unit Vacancies */

    function UnitVacaniesList() {

        var sortColumn = 'general_property.update_at';
        var table = 'general_property';
        var columns = ['Portfolio', 'Property', 'No. of Building', 'No. of Units', 'Vacant Unit'];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [];
        var extra_columns = [];
        var joins = [{
            table: 'general_property',
            column: 'portfolio_id',
            primary: 'id',
            on_table: 'company_property_portfolio'
        },];

        var columns_options = [
            {
                name: 'Portfolio',
                index: 'portfolio_name',
                width: 100,
                searchoptions: {sopt: conditions},
                table: 'company_property_portfolio'
            },
            {name: 'Property', index: 'property_name', width: 200, searchoptions: {sopt: conditions}, table: table},
            {
                name: 'No. of Building',
                index: 'no_of_buildings',
                width: 100,
                searchoptions: {sopt: conditions},
                table: table
            },
            {name: 'No. of Units', index: 'no_of_units', width: 100, searchoptions: {sopt: conditions}, table: table},
            {name: 'Vacant Unit', index: 'vacant', width: 100, searchoptions: {sopt: conditions}, table: table},
        ];
        var ignore_array = [];
        jQuery("#UnitVacanciesTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                //  select: select_column,
                columns_options: columns_options,
                ignore: ignore_array,
                joins: joins,
                extra_where: extra_where,
                extra_columns: extra_columns,
                deleted_at: 'true'
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Unit Vaccanies",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }

    /* Unit Vacancies */

    /* Inventory Expiration */

    function InventoryExpList() {

        var sortColumn = 'general_property.update_at';
        var table = 'general_property';
        var columns = ['Property Name', 'Building', 'Unit', 'Inventory Category', 'Inventory SubCategory', 'Installation date', 'Warranty', 'Expiration Date'];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [];
        var extra_columns = [];
        var joins = [{
            table: 'general_property',
            column: 'id',
            primary: 'property_id',
            on_table: 'building_detail'
        }, {
            table: 'general_property',
            column: 'id',
            primary: 'property_id',
            on_table: 'unit_details'
        }, {
            table: 'general_property',
            column: 'id',
            primary: 'property_id',
            on_table: 'maintenance_inventory'
        }, {
            table: 'maintenance_inventory',
            column: 'category_id',
            primary: 'id',
            on_table: 'company_maintenance_subcategory'
        }, {
            table: 'maintenance_inventory',
            column: 'sub_category_id',
            primary: 'id',
            on_table: 'company_maintenance_subcategory',
            as: 'company_maintenance_subcategory1'
        }];

        var columns_options = [
            {
                name: 'Property Name',
                index: 'property_name',
                width: 100,
                searchoptions: {sopt: conditions},
                table: table
            },
            {
                name: 'Building',
                index: 'building_name',
                width: 200,
                searchoptions: {sopt: conditions},
                table: 'building_detail'
            },
            {
                name: 'Unit',
                index: 'unit_prefix',
                width: 80,
                align: "left",
                searchoptions: {sopt: conditions},
                table: 'unit_details',
                change_type: 'combine_column_hyphen2',
                extra_columns: ['unit_prefix', 'unit_no'],
                original_index: 'unit_prefix'
            },
            {
                name: 'Inventory Category',
                index: 'category',
                width: 100,
                searchoptions: {sopt: conditions},
                table: 'company_maintenance_subcategory'
            },
            {
                name: 'Inventory SubCategory',
                index: 'sub_category',
                width: 100,
                searchoptions: {sopt: conditions},
                table: 'company_maintenance_subcategory1'
            },
            {
                name: 'Installation date',
                index: 'purchase_date',
                width: 100,
                searchoptions: {sopt: conditions},
                table: 'maintenance_inventory'
            },
            {
                name: 'Warranty',
                index: 'vacant',
                width: 100,
                searchoptions: {sopt: conditions},
                table: table,
                formatter: wrantyformatter
            },
            {
                name: 'Expiration Date',
                index: 'expected_deliver_date',
                width: 100,
                searchoptions: {sopt: conditions},
                table: 'maintenance_inventory'
            },

        ];
        var ignore_array = [];
        jQuery("#ajaxInventoryExpTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                //  select: select_column,
                columns_options: columns_options,
                ignore: ignore_array,
                joins: joins,
                extra_where: extra_where,
                extra_columns: extra_columns,
                deleted_at: 'true'
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Inventory Expiration",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }

    function monthDiff(d1, d2) {
        var months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth() + 1;
        months += d2.getMonth();
        return months <= 0 ? 0 : months;
    }

    function wrantyformatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var startDate = rowObject['Installation date'];
            var endDate = rowObject['Expiration Date'];

            var startDay = new Date(startDate);
            var endDay = new Date(endDate);
            var millisecondsPerDay = 1000 * 60 * 60 * 24;

            var millisBetween = endDay.getTime() - startDay.getTime();
            var days = millisBetween / millisecondsPerDay;
            var remainingDays = Math.floor(days);

            if (remainingDays != 'NaN' && remainingDays > 0) {
                var diff = remainingDays;
            } else {
                var diff = 0;
            }
            return diff;
        }
    }

    /* Inventory Expiration */

    /* MoveIn Start*/

    function MoveInList() {
        var sortColumn = 'general_property.update_at';
        var table = 'general_property';
        var columns = ['Property ID', 'Property', 'Portfolio', 'tenant_user_id', 'Two Weeks Ago','Last Week','Current Week','Next Week','Two Weeks Ahead'];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [];
        var extra_columns = [];
        var joins = [{
            table: 'general_property',
            column: 'portfolio_id',
            primary: 'id',
            on_table: 'company_property_portfolio'
        }, {
            table: 'general_property',
            column: 'id',
            primary: 'property_id',
            on_table: 'tenant_property'
        }, {table: 'tenant_property', column: 'user_id', primary: 'user_id', on_table: 'tenant_move_in'}];

        var columns_options = [
            {name: 'Property ID', index: 'property_id', width: 100, searchoptions: {sopt: conditions}, table: table},
            {name: 'Property', index: 'property_name', width: 200, searchoptions: {sopt: conditions}, table: table},
            {name: 'Portfolio', index: 'portfolio_name', width: 200, searchoptions: {sopt: conditions}, table: 'company_property_portfolio'},
            {name: 'tenant_user_id', index: 'user_id', hidden:true , width: 200, searchoptions: {sopt: conditions}, table: 'tenant_move_in'},
            {name: 'Two Weeks Ago', index: 'actual_move_in', width: 100, searchoptions: {sopt: conditions}, table: 'tenant_move_in', change_type: 'check_week_date', weekType:'2weekago',formatter:tenantmoveformatter},
            {name: 'Last Week', index: 'actual_move_in', width: 100, searchoptions: {sopt: conditions},  table: 'tenant_move_in', change_type: 'check_week_date', weekType:'1weekago',formatter:tenantmoveformatter},
            {name: 'Current Week', index: 'actual_move_in', width: 100, searchoptions: {sopt: conditions}, table: 'tenant_move_in', change_type: 'check_week_date', weekType:'currentWeek',formatter:tenantmoveformatter},
            {name: 'Next Week', index: 'actual_move_in', width: 100, searchoptions: {sopt: conditions}, table: 'tenant_move_in', change_type: 'check_week_date', weekType:'nextWeek',formatter:tenantmoveformatter},
            {name: 'Two Weeks Ahead', index: 'actual_move_in', width: 100, searchoptions: {sopt: conditions}, table: 'tenant_move_in', change_type: 'check_week_date', weekType:'twoWeeksAhead',formatter:tenantmoveformatter},

        ];
        var ignore_array = [];
        jQuery("#MoveInTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                //  select: select_column,
                columns_options: columns_options,
                ignore: ignore_array,
                joins: joins,
                extra_where: extra_where,
                extra_columns: extra_columns,
                deleted_at: 'true'
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Move In List ",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }

    /* MoveIn End*/

    /* Move Out Start */

    function MoveOutList() {
        var sortColumn = 'general_property.update_at';
        var table = 'general_property';
        var columns = ['Property ID', 'Property', 'Portfolio', 'Two Weeks Ago','Last Week','Current Week','Next Week','Two Weeks Ahead'];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [];
        var extra_columns = [];
        var joins = [{
            table: 'general_property',
            column: 'portfolio_id',
            primary: 'id',
            on_table: 'company_property_portfolio'
        }, {
            table: 'general_property',
            column: 'id',
            primary: 'property_id',
            on_table: 'tenant_property'
        }, {table: 'tenant_property', column: 'user_id', primary: 'user_id', on_table: 'moveouttenant'}];

        var columns_options = [
            {name: 'Property ID', index: 'property_id', width: 100, searchoptions: {sopt: conditions}, table: table},
            {name: 'Property', index: 'property_name', width: 200, searchoptions: {sopt: conditions}, table: table},
            {name: 'Portfolio', index: 'portfolio_name', width: 200, searchoptions: {sopt: conditions}, table: 'company_property_portfolio'},
          //  {name: 'tenant_user_id', index: 'user_id', hidden:true , width: 200, searchoptions: {sopt: conditions}, table: 'moveouttenant'},
            {name: 'Two Weeks Ago', index: 'actualMoveOutDate', width: 100, searchoptions: {sopt: conditions}, table: 'moveouttenant', change_type: 'Count_tenant_move_out', weekType:'2weekago',formatter:tenantmoveformatter},
            {name: 'Last Week', index: 'actualMoveOutDate', width: 100, searchoptions: {sopt: conditions},  table: 'moveouttenant', change_type: 'Count_tenant_move_out', weekType:'1weekago',formatter:tenantmoveformatter},
            {name: 'Current Week', index: 'actualMoveOutDate', width: 100, searchoptions: {sopt: conditions}, table: 'moveouttenant', change_type: 'Count_tenant_move_out', weekType:'currentWeek',formatter:tenantmoveformatter},
            {name: 'Next Week', index: 'actualMoveOutDate', width: 100, searchoptions: {sopt: conditions}, table: 'moveouttenant', change_type: 'Count_tenant_move_out', weekType:'nextWeek',formatter:tenantmoveformatter},
            {name: 'Two Weeks Ahead', index: 'actualMoveOutDate', width: 100, searchoptions: {sopt: conditions}, table: 'moveouttenant', change_type: 'Count_tenant_move_out', weekType:'twoWeeksAhead',formatter:tenantmoveformatter},

        ];
        var ignore_array = [];
        jQuery("#MoveOutTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                //  select: select_column,
                columns_options: columns_options,
                ignore: ignore_array,
                joins: joins,
                extra_where: extra_where,
                extra_columns: extra_columns,
                deleted_at: 'true'
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Move Out List ",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }
    /* MoveIn End*/

    /* tenant Move fromatter */
    function tenantmoveformatter(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            if(cellValue == ''){
                return '0';
            }
            return cellValue;
        }
    }
    /* formatter*/

    /*  Credential Expiration Start*/

    function CredentialExpList() {

        var sortColumn = 'tenant_credential.updated_at';
        var table = 'tenant_credential';
        var columns = ['Name', 'Credential Name', 'Credential type', 'Expiration Date'];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [];
        var extra_columns = [];
        var joins = [{
            table: 'tenant_credential',
            column: 'credential_type',
            primary: 'id',
            on_table: 'tenant_credential_type'
        }, {table: 'tenant_credential', column: 'user_id', primary: 'id', on_table: 'users'}];

        var columns_options = [
            {name: 'Name', index: 'name', width: 100, searchoptions: {sopt: conditions}, table: 'users'},
            {
                name: 'Credential Name',
                index: 'credential_name',
                width: 200,
                searchoptions: {sopt: conditions},
                table: table
            },
            {
                name: 'Credential type',
                index: 'credential_type',
                width: 200,
                searchoptions: {sopt: conditions},
                table: 'tenant_credential_type'
            },
            {name: 'Expiration Date', index: 'expire_date', width: 100, searchoptions: {sopt: conditions}, table: table}
        ];
        var ignore_array = [];
        jQuery("#CredentialExpTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                //  select: select_column,
                columns_options: columns_options,
                ignore: ignore_array,
                joins: joins,
                extra_where: extra_where,
                extra_columns: extra_columns,
                deleted_at: 'true'
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Credential Expiration",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }

    /* Credential Expiration End*/

    /*Delinquencies Start*/

    function delinquencyList(status, deleted_at) {
        var table = 'complaints';
        var columns = ['Complaint ID', 'Date of Complaint', 'Complaint Type', 'Name', 'Description'];
        var select_column = [];
     //   var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var joins = [{
            table: 'complaints',
            column: 'user_id',
            primary: 'id',
            on_table: 'users'
        },{
            table: 'complaints',
            column: 'complaint_type_id',
            primary: 'id',
            on_table: 'complaint_types'
        }];
        var extra_where = [];
    //    var pagination = [];
        var columns_options = [
            { name: 'Complaint ID', index: 'complaint_id', width: 150, align: "center",searchoptions: {sopt: conditions}, table: table },
            {name: 'Date of Complaint', index: 'complaint_date', width: 150, searchoptions: {sopt: conditions}, table: table},
            { name: 'Complaint Type',index: 'complaint_type', width: 110, align: "center",  searchoptions: {sopt: conditions},table: 'complaint_types' },
            { name: 'Name', index: 'name', width: 110, align: "center",searchoptions: {sopt: conditions},table: 'users' },
            { name: 'Description', index: 'complaint_by_about', width: 110, align: "center",  searchoptions: {sopt: conditions}, table: table },
        ];
        var ignore_array = [];
        jQuery("#delinquencyDataTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "complaints",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                deleted_at: deleted_at,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'complaints.updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Complaint",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 0, left: 400, drag: true, resize: false} // search options
        );
    }

    function statusDelinquencyFormatter(cellValue, options, rowObject) {
        if (cellValue == 0)
            return "Inactive";
        else if (cellValue == '1')
            return "Active";
        else
            return 'Inactive';

    }

    function twoWeeksAgo(cellValue, options, rowObject) {
        if(rowObject !== undefined && cellValue !== undefined) {

            var g1 = new Date();
            var g2 = new Date(cellValue);
            var diff = (g2.getTime() - g1.getTime()) / 1000;
            diff /= (60 * 60 * 24 * 7);
            var week = Math.abs(Math.round(diff));
            if(week >= 2){
                return '1';
            } else if(week <= 1) {
                return '1';
            } else {
                return '0';
            }
        } else {
            return '0';
        }
    }
    /*Tenant Insurance End*/

    /*Tenant Insurance Start*/

    function jqGridTenantinsurace(status,deleted_at) {
        //var tenantId = $(".tenant_id").val();
        var table = 'tenant_renter_insurance';
        var columns = ['Tenant Name','Policy Info','Provider','Expiration Date','Next Renewal','Status'];
        var select_column = [];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [];
     //   var pagination=[];
        var columns_options = [
            {name:'Tenant Name',index:'policy_holder', width:150,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Policy Info',index:'policy_no', width:150,searchoptions: {sopt: conditions},table:table},
            {name:'Provider',index:'provider', width:140, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Expiration Date',index:'expire_date', width:110, align:"center",searchoptions: {sopt: conditions},table:table},
            /*{name:'Effective Date',index:'effective_date', width:110, align:"center",searchoptions: {sopt: conditions},table:table},*/
            {name:'Next Renewal',index:'renewel_date', width:110, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Status',index:'status', width:110, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatterDate1},
            /*{name:'Last Update',index:'updated_at', width:110, align:"center",searchoptions: {sopt: conditions},table:table},*/
            /*{name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table},*/

        ];
        var ignore_array = [];
        jQuery("#tenantInsuraceTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "tenant_renter_insurance",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'tenant_renter_insurance.updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Renter Insurance",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == 0)
            return "Inactive";
        else if(cellValue == '1')
            return "Active";
        else
            return 'Inactive';

    }
    function statusFormatterDate1 (cellValue, options, rowObject){
        if(rowObject !== undefined) {
            var endDate = rowObject['Expiration Date'];
            var startDay = new Date(endDate);
            var endDay = new Date();
            var millisecondsPerDay = 1000 * 60 * 60 * 24;

            var millisBetween = startDay.getTime() - endDay.getTime();
            var days = millisBetween / millisecondsPerDay;
            var remainingDays = Math.floor(days);
            if (remainingDays >= 0) {
                return "active";
            } else {
                return "inactive";
            }

        }
    }
    function jqGridLeaseStatus(status, deleted_at) {
        if(jqgridNewOrUpdated == 'true'){
            var sortOrder = 'desc';
            var sortColumn = 'users.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }
        var table = 'tenant_lease_details';
        var columns = ['Tenant name','Property Name','user_id','Unit', ' Move In Date', 'Lease ID','Expiration Date','Status'];
        var select_column = ['Edit', 'Start Application', 'Email', 'Email History', 'Text', 'Text History', 'File Library', 'Notes History', 'Flag Bank', 'Run Background Check', 'Archive Guest', 'Delete Guest', 'Print Envelope'];
        var joins = [
        {table: 'tenant_lease_details', column: 'user_id', primary: 'id', on_table: 'users'},
        {table:'users',column:'id',primary:'user_id',on_table:'tenant_property'},
        {table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},
        /*{table: 'tenant_lease_details', column: 'unit_id', primary: 'id', on_table: 'unit_details'},*/
        {table:'users',column:'id',primary:'user_id',on_table:'tenant_details'},
        {table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}
       /* {table:'tenant_lease_details',column:'unit_id',primary:'id',on_table:'unit_details'}*/
            ];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        /*var extra_where = [{column: 'user_type', value: '6', condition: '=', table: 'users'},{column:'record_status',value:'1',condition:'=',table:'tenant_details'}];*/
        var extra_where = [{column:'user_type',value:'2',condition:'=',table:'users'},{column:'record_status',value:'1',condition:'=',table:'tenant_details'},{column:'record_status',value:'1',condition:'=',table:'tenant_lease_details'}];

       //  var pagination =[];

        var columns_options = [
            {name:'Tenant Name',index:'name', width:100,align:"center",searchoptions: {sopt: conditions},table:'users',},
            {name:'Property Name',index:'property_name', width:100, align:"left",searchoptions: {sopt: conditions},table:'general_property'},
            {name:'user_id',index:'user_id', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
            /*{name:'Guest Card Number',index:'id', width:80, align:"center",searchoptions: {sopt: conditions},table:table},*/
            {name:'Unit Number',index:'unit_prefix', width:100, align:"center",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
            {name:'Move In Date',width:100, align:"center",index:'move_in',searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Lease ID',width:100, align:"center",index:'id',searchoptions: {sopt: conditions},table:table},
            {name:'Expiration Date',width:100, align:"center",index:'end_date',searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Status',index:'status', width:100,align:"left",searchoptions: {sopt: conditions},table:'tenant_details',formatter:statusFormatterLease},
            /*{name:'Building Unit ID',index:'id', width:80,align:"center",searchoptions: {sopt: conditions},table:'unit_details',formatter:getUnitDetails,alias:'unit_data_id'},*/

        ];
        var ignore_array = [];
        jQuery("#leaseStatusSpotTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "tenant_lease_details",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Guest Cards",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }

    function statusFormatterLease (cellValue, options, rowObject){

        if (cellValue == '1')
            return "Active";
        else if(cellValue == '0')
            return "Inactive";
        else if(cellValue == '6')
            return "Move In";
        else if(cellValue == '7')
            return "Renewed";
        else
            return '';

    }
    /*lease Status End*/



    function InvoicesList() {
        var table = 'accounting_invoices';
        var columns = ['Invoice', 'Invoice Date','FloorNo','UnitPrefix','Property','Invoice To', 'Type','Amount','Status'];
        var select_column = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var joins = [{table: 'accounting_invoices', column: 'invoice_to', primary: 'user_id', on_table: 'tenant_property'},
            {table: 'tenant_property', column: 'property_id', primary: 'id', on_table: 'general_property'},
            {table: 'tenant_property', column: 'unit_id', primary: 'id', on_table: 'unit_details'},
            {table: 'accounting_invoices', column: 'invoice_to', primary: 'id', on_table: 'users'},
            {table: 'accounting_invoices', column: 'property_id', primary: 'id', on_table: 'general_property' ,as:'OwnerProperty' },
            //     {table: 'work_order', column: 'vendor_id', primary: 'id', on_table: 'users'}
        ];
        var joins = [{
            table: 'accounting_invoices',
            column: 'id',
            primary: 'invoice_id',
            on_table: 'tenant_charges'
        },{
            table: 'accounting_invoices',
            column: 'invoice_to',
            primary: 'id',
            on_table: 'users'
        },{
            table: 'accounting_invoices',
            column: 'invoice_to',
            primary: 'user_id',
            on_table: 'tenant_property'
        },{
            table: 'accounting_invoices',
            column: 'invoice_to',
            primary: 'user_id',
            on_table: 'owner_property_owned'
        },{
            table: 'tenant_property',
            column: 'property_id',
            primary: 'id',
            on_table: 'general_property',
            as:'ten_property'
        },{
            table: 'owner_property_owned',
            column: 'property_id',
            primary: 'id',
            on_table: 'general_property',
            as: 'gen_property'
        }, {table: 'tenant_property', column: 'unit_id', primary: 'id', on_table: 'unit_details'}];
        var extra_where = [];
    //     var pagination = [];
        var ignore_array = [];
        var columns_options = [
            { name: 'Invoice', index: 'invoice_number', width: 150, align: "center",searchoptions: {sopt: conditions}, table: table },
            {name: 'Invoice Date', index: 'invoice_date', width: 150, searchoptions: {sopt: conditions}, table: table},
            {name:'FloorNo',index:'floor_no',hidden:true, width:100,searchoptions: {sopt: conditions},table:'unit_details'},
            {name:'UnitPrefix',index:'unit_prefix',hidden:true, width:100,searchoptions: {sopt: conditions},table:'unit_details'},
            { name: 'Property',index: 'property_name', width: 110, align: "center",  searchoptions: {sopt: conditions},table: 'ten_property',formatter:PropertyUnitFormatter},
            { name: 'Invoice To', index: 'name', width: 110, align: "center",searchoptions: {sopt: conditions},table: 'users'},
            { name: 'Type', index: 'user_type', width: 110, align: "center",  searchoptions: {sopt: conditions}, table: table,formatter:typeformatter },
            { name: 'Amount', index: 'amount', width: 110, align: "center",  searchoptions: {sopt: conditions}, table: 'tenant_charges' },
            { name: 'Status', index: 'status', width: 110, align: "center",  searchoptions: {sopt: conditions}, table: table,formatter:InvoicestatusFormatter },
        ];

        jQuery("#InvoicesTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "accounting_invoices",
                select: select_column,
                columns_options: columns_options,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
             //   deleted_at: deleted_at,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'accounting_invoices.updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Paid / Unpaid Invoices",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 0, left: 400, drag: true, resize: false} // search options
        );
    }
    function InvoicestatusFormatter(cellvalue, options, rowObject) {
      //  console.log(rowObject);
        if (cellvalue != undefined) {
            var type = "";
            console.log(cellvalue);
            if (cellvalue == 1) {
                type = 'Paid';
            } else if (cellvalue == 0) {
                type = 'Unpaid';
            }
            return type;
        }
    }

    function PropertyUnitFormatter(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {
        console.log(rowObject);
        var data = '';
        if(rowObject.Type == 2) {
            if (rowObject.Property != '') {
                data += '<span class="property_class">' + rowObject.Property + '</br> Unit :' + rowObject.FloorNo + '-' + rowObject.UnitPrefix + '</span>';
            }
        }else if(rowObject.Type == 4){
            data += '<span class="property_class">' + rowObject.OwnerProperty  + '</span>';
        }
        return data;
    }
    }

    function BillList() {
    var table = 'company_bills';
    var columns = ['Vendor Name', 'Ref.No','Due Date','Paid On','Amount','Status'];
    var select_column = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = [];
    var joins = [{table: 'company_bills', column: 'vendor_id', primary: 'id', on_table: 'users'}];

    var extra_where = [];
    //  var pagination = [];
    var ignore_array = [];
    var columns_options = [
        { name: 'Vendor Name', index: 'name', width: 150, align: "center",searchoptions: {sopt: conditions}, table: 'users' },
        {name: 'Ref.No', index: 'refrence_number', width: 150, searchoptions: {sopt: conditions}, table: table},
        {name:'Due Date',index:'due_date',width:100,searchoptions: {sopt: conditions},table:table},
        {name:'Paid On',index:'created_at',width:100,searchoptions: {sopt: conditions},table:table},
        { name: 'Amount',index: 'amount', width: 110, align: "center",  searchoptions: {sopt: conditions},table: table,formatter:AmtFormatter},
        { name: 'Status', index: 'status', width: 110, align: "center",searchoptions: {sopt: conditions},table: table,formatter:StatusFormatter},
            ];

    jQuery("#BillDataTable").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "company_bills",
            select: select_column,
            columns_options: columns_options,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            //   deleted_at: deleted_at,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'company_bills.updated_at',
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Paid / Unpaid Invoices",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 0, left: 400, drag: true, resize: false} // search options
    );
    }

    function AmtFormatter(cellvalue, options, rowObject){
    if(rowObject!==undefined)
    {
        var symbol = default_symbol;
        var amt = changeToFloat(rowObject.Amount.toString());
        if(rowObject.Amount!='')
        {
            return symbol+''+amt;
        }
    }
}
    function StatusFormatter(cellvalue, options, rowObject) {
        if (cellvalue != undefined) {
            var type = "";
            if (cellvalue == 1) {
                type = 'Active';
            } else if (cellvalue == 0) {
                type = 'Inactive';
            }
            return type;
        }
    }




    function receivablesTableList() {

        var sortColumn = 'general_property.update_at';
        var table = 'general_property';
        var Receivables = "Receivables (" + currencySymbol + ")";
        var Payables = "Payables (" + currencySymbol + ")";
        var Balance = "Balance (" + currencySymbol + ")";
        var columns = ['Portfolio', 'Property', Receivables, Payables, Balance];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [];
        var extra_columns = [];
        var joins = [{
            table: 'general_property',
            column: 'portfolio_id',
            primary: 'id',
            on_table: 'company_property_portfolio'
        },];

        var columns_options = [
            {name: 'Portfolio',index: 'portfolio_name',width: 100,searchoptions: {sopt: conditions},table: 'company_property_portfolio'},
            {name: 'Property', index: 'property_name', width: 200, searchoptions: {sopt: conditions}, table: table},
            {name: Receivables,index: 'no_of_buildings',width: 100,searchoptions: {sopt: conditions},table: table},
            {name: Payables, index: 'no_of_units', width: 100, searchoptions: {sopt: conditions}, table: table},
            {name: Balance, index: 'vacant', width: 100, searchoptions: {sopt: conditions}, table: table},
        ];
        var ignore_array = [];
        jQuery("#receivablesTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                //  select: select_column,
                columns_options: columns_options,
                ignore: ignore_array,
                joins: joins,
                extra_where: extra_where,
                extra_columns: extra_columns,
                deleted_at: 'true'
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Receivables / Payables",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }


    function notificationTableList() {

        var sortColumn = 'notification_apex.updated_at';
        var table = 'notification_apex';
        var columns = ['Property', 'Type', 'Name', 'Date Of Notification', 'Action'];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [];
        var extra_columns = [];
        var joins = [{table: 'notification_apex', column: 'module_id', primary: 'id', on_table: 'users'},{table: 'notification_apex', column: 'property_id', primary: 'id', on_table: 'general_property'}];

        var columns_options = [
            {name: 'Property',index: 'property_name',width: 100,searchoptions: {sopt: conditions},table:'general_property'},
            {name: 'Type', index: 'name', width: 200, searchoptions: {sopt: conditions},table: 'users'},
            {name: 'Name',index: 'module_title',width: 100,searchoptions: {sopt: conditions}, table: table},
            {name: 'Date Of Notification', index: 'created_at', width: 100, searchoptions: {sopt: conditions}, table: table},
            {name: 'Action', index: 'vacant', width: 100, searchoptions: {sopt: conditions}, table: table,formatter:actionFormatterView},
        ];
        var ignore_array = [];
        jQuery("#NotificationDataTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                //  select: select_column,
                columns_options: columns_options,
                ignore: ignore_array,
                joins: joins,
                extra_where: extra_where,
                extra_columns: extra_columns,
                deleted_at: 'no'
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Notifications",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }

    function actionFormatterView(cellValue, options, rowObject) {
        if (rowObject !== 'undefined') {
            return '<a class="pointer" href="/Alert/Notifications">view</a>';
        }
    }

    function getpropertlist(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            console.log("pro >>",rowObject);
        }
    }
});