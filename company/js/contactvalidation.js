/*          ADD CONTACT FORM CLIENT SIDE VALIDATIONS              */

// $("#contactvalidations").validate({
//     rules: {
//         first_name: {
//             required: true
//         },
//         last_name: {
//             required: true
//         },
//         carrier: {
//             required: true
//         },
//         'email[]': {
//             required: true,
//             email: true
//         }
//      }
//
// });

$("#contactvalidations").validate({
    rules: {

        carrier: {
            required:true
        },
        salutation: {
            required:true
        },
        first_name: {
            required:true
        },
        last_name: {
            required:true
        },
        'email[]': {
            required: true,
            email: true,
        },
        'email1[]': {
            required: true,
            email: true,
        },
        'carrier[]': {
            required: true,
        },
        'phoneNumber[]': {
            required: true,
        },
        referral: {
            required:true
        },
        gender: {
            required:true
        }
    },
});