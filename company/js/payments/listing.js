jqGrid('All');
/**
 * jqGrid Initialization function
 * @param status
 */
function jqGrid(status) {
    var table = 'transactions';
    var columns = ['Plan Name','Price('+default_currency_symbol+')','Billing Cycle','hidden_subPlan','Action'];
    var extra_dropdown = [];
    var select_column = [];
    var ignore_array = [];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'user_id',value:'1',condition:'='},{column:'user_type',value:'PM',condition:'='}];
    var extra_columns = [];
    var columns_options = [
        {name:'Plan Name',index:'created_at', width:100,searchoptions: {sopt: conditions},table:table,change_type:'subscription_date',formatter:subscriptionFormatter,classes:'cursor'},
        {name:'Price',index:'total_charge_amount', width:180,align:"center",searchoptions: {sopt: conditions},table:table,formatter:priceFormatter,classes:'cursor'},
        {name:'Plan Start Date',index:'created_at', width:200,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'subscription_date',classes:'cursor'},
        {name:'hidden_subPlan',index:'id',hidden:true ,width:200,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'id', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:'plans',formatter:actionFormatter,classes:'cursor'}
    ];
    jQuery("#SubscriptionHistory-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            extra_dropdown:extra_dropdown,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'no',
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'transactions.updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Billing & Subscription",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {top:10,left:200,drag:true,resize:false}
    );
}

/**
 * jqGrid function to format status
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function actionFormatter(cellValue, options, rowObject){
    if(rowObject !== undefined) {
        return "<i data_io='" + rowObject.hidden_subPlan + "' class=\"fa fa-download fa-lg downloadPDF\" aria-hidden=\"true\"></i>";
    }
}

/**
 *
 * @param cellValue
 * @param options
 * @param rowObject
 */
function subscriptionFormatter(cellValue, options, rowObject){
    if (rowObject !== undefined){
        return "<span style='text-decoration: blink;color: #22ade6;'>Subscription payment statement "+cellValue+"</span>";
    }
}
function priceFormatter(cellValue, options, rowObject){
    return default_currency_symbol+cellValue;
}

$(document).on('click','.downloadPDF',function(){
    var dataValue = $(this).attr('data_io');
    getHtmlPdfConverter(dataValue);
});

function getHtmlPdfConverter(id){
    $.ajax({
        url:'/company-subscription-ajax',
        type: 'POST',
        data: {
            class: "subscription",
            action: "createTransactionPdf",
            plan_id: id
        },
        success: function (response) {
            var response = JSON.parse(response);
            console.log(response);
           // return false;
            if (response.status == 'success' && response.code == 200) {
                var link=document.createElement('a');
                document.body.appendChild(link);
                link.target="_blank";
                link.download="payment.pdf";
                link.href=response.data;
                link.click();
            } else if(response.code == 500) {
                //toastr.warning(response.message);
            } else {
                //toastr.error(response.message);
            }

        }
    });
}