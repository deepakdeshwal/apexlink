$(document).on("click",".payNow",function(){

$('#payNow').modal('toggle');

function stripePaymentVariables(){
    var stripe = Stripe('pk_test_jF5tXlX0qLcCR1YAX9WB8FaL');
    // Create an instance of Elements.
    var elements = stripe.elements();
    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#32325d',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

    // Create an instance of the card Element.
    var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    var cardElemLength = $("#card-element").length;
    if (cardElemLength > 0) {
        card.mount('#card-element');

        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
            $(".submit_payment_class").attr("disabled",true);
            event.preventDefault();
            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    var token_id = result.token.id;
                    stripeTokenHandler(token_id);
                }
           });
        });
    }
}
});


paymentCalculateForPM();
proratedCalculaion();


function paymentCalculateForPM(){
	/*alert('hi');*/

    $.ajax({
        url:'/payment-stripe-ajax',
        type: 'POST',
        data: {
        "action": 'paymentCalculateForPM',
        "class": 'payment'
        },
        success: function (response) {
        var info = JSON.parse(response);
    


        }
        });

}




function proratedCalculaion(){

    $.ajax({
        url:'/payment-stripe-ajax',
        type: 'POST',
        data: {
        "action": 'proratedCalculaion',
        "class": 'payment'
        },
        success: function (response) {
        var info = JSON.parse(response);
        $(".paidAmount").html(info.prorated_plan);
        $(".admin_fee").html(info.apexlinkAdminFee);
        $(".service_fee").html(info.service_fee);
        $(".stripeAccountFee").html(info.stripeAccountFee);
        $(".stripeTranactionFee").html(info.stripeTranactionFee);
        $(".fee_for_card").html(info.fee_for_card);
        $(".days_remaining").val(info.remaining_days);
        $(".next_charge_amount").html(default_currency_symbol+info.next_amount);
        $(".due_date").html(info.due_date);
        $(".default_source").html(info.default_source);
        $(".units").html(info.units+ ' Unit Plan');
        $(".plan_id").val(info.plan_id);
        $(".term_plan").val(info.term_plan);
        $(".prorated_fee").html(info.pro_rate);


       


        
        
    



            }
        });

      }


// function convertAmount(amount) {
//     var amount = amount.toString()
//     if (amount != '' && amount.indexOf(".") == -1) {
//         var bef = $(this).val().replace(/,/g, '');
//         var value = numberWithCommas(bef) + '.00';
//        return value;
//     } else {
//         var bef = amount.replace(/,/g, '');
//         var before = bef.substr(0, bef.indexOf('.'));
//         var after = bef.split('.')[1];
//       return numberWithCommas(before) + '.' + after.slice(0, 2);
//     }
// }
//
// function numberWithCommas(x) {
//     return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
// }



