var linkHandler = Plaid.create({
    clientName: 'Client Name',
    // Change sandbox to development to test with live users;
    // Change to production when you're ready to go live!
    env: 'sandbox',
    // Replace with your public_key from the Dashboard
    key: '8639bc12b510d57d72bace4edfc6d6',
    product: ['auth'],
    onLoad: function() {
        // The Link module finished loading.
    },
    onSuccess: function(public_token, metadata) {
        // The onSuccess function is called when the user has
        // successfully authenticated and selected an account to use.
        //
        // When called, you will send the public_token and the selected
        // account ID, metadata.account_id, to your backend app server.
        //
        sendDataToBackendServer({
          public_token: public_token,
          metadata: metadata.accounts
        });
    },
    onExit: function(err, metadata) {
        // The user exited the Link flow.
        if (err != null) {
            // The user encountered a Plaid API error prior to exiting.
        }
        // metadata contains information about the institution
        // that the user selected and the most recent API request IDs.
        // Storing this information can be helpful for support.
    },
});

// Trigger the standard Institution Select view
$(document).on('change','#payment_method',function(){
    var data = $(this).val();
    if(data == '1'){
        $('#financialDiv').show();
    } else {
        // Trigger the standard Institution Select view
        linkHandler.open();
        $('#billing-subscription').modal('hide');
        $('#financial-info').modal('hide');
    }
});


/**
 * function to get stripe account token
 * @param obj
 */
function sendDataToBackendServer(obj){
    var id = $('#company_user_id').val();
    var data_id = (id === undefined)?0:id
    obj['id'] = data_id;
    $.ajax({
        type: 'post',
        url: '/plaid/curl',
        data: {class: 'PlaidApis', action: "exchangeToken", obj: obj},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                toastr.success(response.message);
            } else {
                toastr.error(response.message);
            }

        }
    });
}