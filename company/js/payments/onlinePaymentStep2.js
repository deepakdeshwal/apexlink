$(document).on('focusout','#fpostal_code',function(){
    getZipCode('#fpostal_code', $(this).val(), '#fcity', '#fstate', '#fcountry', '', '');
});

$("#fdob").datepicker({    changeYear: true,
    yearRange: "-100:+20",
    changeMonth: true
});

/**
 * submit owner basic form step 2
 */
$(document).on("submit","#ownerBasicPaymentDetails",function(e){
    e.preventDefault();
    if ($('#ownerBasicPaymentDetails').valid()) {
        $('#loadingmessage').show();
        var owner_id=$(".company_user_id").val();
        var form = $('#ownerBasicPaymentDetails')[0];
        var formData =new FormData(form);
        formData.append('action','addVendorAccountDetails');
        formData.append('class','Stripe');
        formData.append('user_id',owner_id);
        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                $('#loadingmessage').hide();
                if(response.status == 'success' && response.code == 200){
                    toastr.success(response.message);
                    $("#financial-info").modal("hide");
                } else if(response.status == 'failed'){
                    toastr.error(response.message);
                }else if(response.status == 'AccountDetailupdated'){
                    $('#loadingmessage').hide();
                    // console.log(response.enableAccount);
                    if(response.enableAccount.code == 400){
                        toastr.error(response.enableAccount.message);
                    }else if(response.enableAccount.code == 200){
                        toastr.success(response.enableAccount.message);
                        $("#financial-info").modal("hide");
                    }
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});

$(document).on("click","#basicDiv",function(){
    $('#loadingmessage').show();
    step2Detail(false);
});

function step2Detail(owner_id) {
    var owner_id=$(".company_user_id").val();
    $("#ownerBasicPaymentDetails").trigger("reset");
    $.ajax({
        type: 'post',
        url:'/stripeCheckout',
        data: {
            class: 'Stripe',
            action: 'getAccountDetail',
            user_id: owner_id},
        success: function (response) {
            var response = JSON.parse(response);
            $('#loadingmessage').hide();
            if(response.status == 'AccountDetail'){
                $.each(response.data, function (key, value) {
                    console.log(key+' '+value);
                    $('.' + key).val(value);
                    if(key == 'dob'){
                        $(".dob").val(response.dob);
                    }

                });

            } else if(response.code == 500){
                toastr.error('error');
            }else if(response.message == 'pending'){
                $(".basic-payment-detail").hide();
                setTimeout(function(){
                    // $(".verification-message").html("<p class='verification-due-p'>Verification Due</p>");
                },500);

            }else if(response.message == 'verified'){
                $("#paymentDiv").addClass('active')
                $("#basicDiv").hide();
                $(".basic-payment-detail").hide();
                $('.payment_li').addClass('after-hide');
                $("#basicDiv").next().html('');
            }
        },
    });
}
