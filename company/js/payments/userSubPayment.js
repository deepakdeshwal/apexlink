$('#financial-info').modal({backdrop: 'static', keyboard: false,show:false});
$('#financial-company-info').modal({backdrop: 'static', keyboard: false,show:false});
$( "#fcczipcode" ).focusout(function() {
    getZipCode('#fcczipcode',$('#fcczipcode').val(),'#fcccity','#fccstate','','','');
});

$( "#fpppzipcode" ).focusout(function() {
    getZipCode('#fpppzipcode',$('#fpppzipcode').val(),'#fpppcity','#fpppstate','','','');
});
$( "#fppzipcode" ).focusout(function() {
    getZipCode('#fppzipcode',$('#fppzipcode').val(),'#fppcity','#fppstate','','','');
});

$( "#czip_code" ).focusout(function() {
    getZipCode('#czip_code',$('#czip_code').val(),'#ccity','#cstate','#ccountry','','');
});

$(document).on('click','.subscription_modal',function(){
    $("#ccard_number").val('');
    $("#ccvv").val('');
    $('#paymentDiv').click();
});

$(document).on('click','#paymentDiv',function(){
    $('.basic-user-payment-detail').hide();
})


$(document).on("click","#savefinancialtype",function(){
    var user_id = $("#company_user_id").val();
    getAccountDetails(user_id);
});

function getAccountDetails(data_id){

    $('#financial-infotype').modal('hide');

    var radioValue = $("input[name='companytype']:checked").val();

    $('#financial-indiviual').show();
    $('#financial-company').show();

    if(radioValue == 'individual') {
        $.ajax
        ({
            type: 'post',
            url: '/company-subscription-ajax',
            data: {
                class: "subscription",
                action: "getCompanyUserData",
                id: data_id
            },
            success: function (response) {
                var response = $.parseJSON(response);

                // return false;
                if (response.code == 200) {

                    var first_name = response.data.data.first_name;
                    var last_name = response.data.data.last_name;
                    var phone_number = response.data.data.phone_number;
                    var city = response.data.data.city;
                    var zipcode = response.data.data.zipcode;
                    // var city = $('#city').val();
                    var state = response.data.data.state;
                    var email = response.data.data.email;
                    var address1 = response.data.data.address1;
                    var address2 = response.data.data.address2;

                    $('.fname').val(first_name);
                    $('.lname').val(last_name);
                    $('.fphone').val(phone_number);
                    $('#fppzipcode').val(zipcode);
                    $('#fppcity').val(city);
                    $('#fppstate').val(state);
                    $('.femail').val(email);
                    $('#faddress').val(address1);
                    $('#faddress2').val(address2);
                    $('#financialInfoIndiviual [name ="fmcc"]').val(6513);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('.' + key + 'Err').text(value);
                });
            }
        });
        //  e.preventDefault();

        var phone_prefix = $('input#phone_number_prefix').val();
        //   var address1 = $('#address1').val();
        //   var address2 = $('#address2').val();
        $('.fphone_num input#phone_number_prefix').val(phone_prefix);
        $('.fphone_num li.country').each(function (key, value) {
            if ($(this).attr('data-dial-code') == phone_prefix) {
                var countrycode = $(this).attr('data-country-code');
                $("#fphone_number").intlTelInput(
                    "setCountry", countrycode
                );
            }
        });
        $("#fbusiness").val(radioValue);
        $('#financial-info').modal('toggle');
    }else{
        $.ajax
        ({
            type: 'post',
            url: '/company-subscription-ajax',
            data: {
                class: "subscription",
                action: "getCompanyUserData",
                id: data_id
            },
            success: function (response) {
                var response = $.parseJSON(response);

                // return false;
                if (response.code == 200) {

                    var first_name = response.data.data.first_name;
                    var last_name = response.data.data.last_name;
                    var phone_number = response.data.data.phone_number;
                    var city = response.data.data.city;
                    var zipcode = response.data.data.zipcode;
                    var state = response.data.data.state;
                    var email = response.data.data.email;
                    var address1 = response.data.data.address1;
                    var address2 = response.data.data.address2;

                    $('#financialCompanyInfoCom [name ="ffirst_name"]').val(first_name);
                    $('#financialCompanyInfoCom [name ="flast_name"]').val(last_name);
                    $('#financialCompanyInfoCom [name ="fphone_number"]').val(phone_number);
                    $('#financialCompanyInfoCom [name ="faddress"]').val(address1);
                    $('#financialCompanyInfoCom [name ="faddress2"]').val(address2);
                    $('#financialCompanyInfoCom [name ="fcity"]').val(city);
                    $('#financialCompanyInfoCom [name ="fstate"]').val(state);
                    $('#financialCompanyInfoCom [name ="fzipcode"]').val(zipcode);
                    $('#financialCompanyInfoCom [name ="femail"]').val(email);
                    $('#financialCompanyInfoCom [name ="fmcc"]').val(6513);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('.' + key + 'Err').text(value);
                });
            }
        });
        $("#fcbusiness").val(radioValue);
        $('#financial-company-info').modal('toggle');



    }

    $('.select_options').prop('selectedIndex',0);
}

$(document).ready(function(){
    $(document).on('click','#savefinancialindi',function (e) {
        e.preventDefault();
        var financial_data = $( "#financialInfoIndiviual" ).serializeArray();

        $.ajax
        ({
            type: 'post',
            url: '/company-subscription-ajax',
            data: {
                class: "subscription",
                action: "validateFinancialData",
                data:financial_data
            },
            success: function (response) {
                var response = $.parseJSON(response);
                if(response.code == 400)
                {
                    $.each(response.data, function (key) {
                        $('.' + key).text('* This field is required');
                    });
                }else{
                    localStorage.setItem('financial_data',JSON.stringify(financial_data));


                    toastr.clear();
                    createUserAccount(financial_data);

                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('.' + key + 'Err').text(value);
                });
            }
        });




    });


    $(document).on('click','#savecompanyfinancialcom',function (e) {
        e.preventDefault();
        var financial_data = $( "#financialCompanyInfoCom" ).serializeArray();

        $.ajax
        ({
            type: 'post',
            url: '/company-subscription-ajax',
            data: {
                class: "subscription",
                action: "validateCompanyFinancialData",
                data:financial_data
            },
            success: function (response) {
                var response = $.parseJSON(response);
                if(response.code == 400)
                {
                    $.each(response.data, function (key) {
                        $('.' + key).text('* This field is required');
                    });
                }else{
                    localStorage.setItem('financial_data',JSON.stringify(financial_data));
                    toastr.clear();
                    createCompanyAccount(financial_data);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('.' + key + 'Err').text(value);
                });
            }
        });




    });
});

$(function () {
    $(":file").change(function () {
        if (this.files && this.files[0]) {
            file = this.files[0];
            formdata = new FormData();
            formdata.append("company_document", file);
            formdata.append("action",'uploadCompanyDocument');
            formdata.append("class",'subscription');
            $.ajax({
                url: '/company-subscription-ajax',
                enctype: 'multipart/form-data',
                data: formdata,
                type: "POST",
                processData: false,
                contentType: false,
                beforeSend: function() {
                    // setting a timeout
                    $('#loadingmessage').show();
                },
                success: function (response) {
                    $('#loadingmessage').hide();
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        // $('#company_document').val('');
                        $("#company_document_con_id").val(response.document_id);
                        toastr.success(response.message);
                    } else {
                        $('#company_document_con').val('')
                        toastr.error(response.message);
                    }
                },
                error: function (response) {

                }
            });
        }
    });
});
createYears();
/**
 * function to create year options
 */
function createYears() {
    var min = new Date().getFullYear(),
        max = min - 49;
    for (var i = max; i <= min; i++) {
        $('.fpyear').append('<option value=' + i + '>' + i + '</option>');
    }
}
createDays();
/**
 * function to create days options
 */
function createDays() {
    var min = 1,
        max = 31;
    for (var i = min; i <= max; i++) {
        $('.fpday').append('<option value=' + i + '>' + i + '</option>');
    }

}

createMonths();
/**
 * function to create months name
 */
function createMonths() {
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var month = 0;
    for (; month < monthNames.length; month++) {

        var new_month = month + 1;
        $('.fpmonth').append('<option value=' + new_month + '>' + monthNames[month] + '</option>');
    }
}

function createUserAccount(financial_data){
    var company_id = $("#company_user_id").val();

    $.ajax({
        type: 'post',
        url: '/company-subscription-ajax',
        data: {class: 'subscription', action: "createUserConnectedAccount",financial_data:financial_data,"company_id":company_id },
        beforeSend: function() {
            // setting a timeout
            $('#loadingmessage').show();
        },
        success : function(response){
            $('#loadingmessage').hide();
            var response = JSON.parse(response);
            if(response.status == 'success' && response.code == 200){
                $('#loadingmessage').hide();
                localStorage.removeItem("financial_data");
                $('#financial-info').modal('toggle');
                toastr.success("Account Created Successfully.");
               // triggerReload();
                $('#financialInfoIndiviual').trigger('reset');
                $('#fbusiness').val('individual');
                setTimeout(function(){
                    window.location.reload();
                }, 3000);
            } else if(response.status == 'error' && response.code == 400){
                if(response.message == 'online payment error')
                {
                    toastr.error('Please fill all online payment required fields.');
                }else{

                }
                $('#loadingmessage').hide();
                $('.error').html('');
            }
            else if(response.status == 'failed' && response.code == 400){
                toastr.error(response.message);
            }

            $('.error').html('');
        },
        error: function (response) {

        }
    });
}


function createCompanyAccount(financial_data){
    var company_id = $("#company_user_id").val();
    $.ajax({
        type: 'post',
        url: '/company-subscription-ajax',
        data: {class: 'subscription', action: "createCompanyConnectedAccount",financial_data:financial_data,"company_id":company_id },
        beforeSend: function() {
            // setting a timeout
            $('#loadingmessage').show();
        },
        success : function(response){
            $('#loadingmessage').hide();
            var response = JSON.parse(response);
            if(response.status == 'success' && response.code == 200){
                localStorage.removeItem("financial_data");
                $('#financial-company-info').modal('toggle');
                toastr.success("Account Created Successfully.");
               // triggerReload();
                $('#financialCompanyInfoCom').trigger('reset');
                $('#fbusiness').val('company');
                setTimeout(function(){
                    window.location.reload();
                }, 3000);
            } else if(response.status == 'error' && response.code == 400){
                if(response.message == 'online payment error')
                {
                    toastr.error('Please fill all online payment required fields.');
                }else{

                }
                $('#loadingmessage').hide();
                $('.error').html('');
            }
            else if(response.status == 'failed' && response.code == 400){
                toastr.error(response.message);
            }
            $('#loadingmessage').hide();
            $('.error').html('');
        },
        error: function (response) {

        }
    });
}
fetchCustomerCardDetail('1');
function fetchCustomerCardDetail(id) {
    $.ajax({
        type: 'post',
        url: '/common-ajax',
        data: {
            class: 'CommonAjax',
            action: 'getCustomerDetails',
            id: id},
        success: function (response) {
            var res = JSON.parse(response);

            if(res.code == 200){
                $('#financialCardInfo [name ="cfirst_name"]').val(res.data.data.first_name);
                $('#financialCardInfo [name ="clast_name"]').val(res.data.data.last_name);
                $('#financialCardInfo [name ="cCompany"]').val(res.data.data.company_name);
                $('#financialCardInfo [name ="cphoneNumber"]').val(res.data.data.phone_number);
                $('#financialCardInfo [name ="caddress1"]').val(res.data.data.address1);
                $('#financialCardInfo [name ="caddress2"]').val(res.data.data.address2);
                $('#financialCardInfo [name ="ccity"]').val(res.data.data.city);
                $('#financialCardInfo [name ="cstate"]').val(res.data.data.state);
                $('#financialCardInfo [name ="czip_code"]').val(res.data.data.zipcode);
                $('#financialCardInfo [name ="ccountry"]').val(res.data.data.country);
            }
        },
    });
}