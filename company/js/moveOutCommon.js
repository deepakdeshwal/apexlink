$("#moveout_Search").click(function(){
    $('#moveout_listSearch').modal('show');
});
$(document).on("click",".moveout_input",function (e){
    getTenantMoveOutDetails();
});
function getTenantMoveOutDetails() {
    //var id = $("#wrapper .moveOutTenant_id").val();
    //alert(id);
    $.ajax({
        type: 'post',
        url: '/moveOutTenangrt',
        data: {
            class: "moveOutTenant",
            action: "getTenantMoveOutDetails",
            // id: id
        },
        success: function (response) {
            var response = JSON.parse(response);
            var html1 = "";
            if (response.status == 'success' && response.code == 200) {
                /*for(var i=0; i < response.data.length; i++){
                    $('#myTable tbody').html(response.data);
                }*/
                html1 += '<table class="table table-hover table-dark" id="myTable"><thead>\n' +
                    '                           <tr class="header">\n' +
                    '                            <th>Tenant Name</th>\n' +
                    '                        <th>Email</th>\n' +
                    '                        <th>Phone</th>\n' +
                    '                        <th>propertyName</th>\n' +
                    '                        <th>Unit</th>\n' +
                    '                        <th>Rent()</th>\n' +
                    '                        <th>Balance()</th>\n' +
                    '                        </tr>\n' +
                    '                        </thead>\n' +
                    '                        <tbody>'+response.data+'</tbody>\n' +
                    '                        </table>';


                $("#tenantData_search").html(html1);
            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
            }
            $('#ContactListing-table').trigger( 'reloadGrid' );
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
$(document).on("keyup",".moveout_input",function(event){
    var keyString = $(this).val();
    $.ajax({
        type: 'post',
        url: '/moveOutTenant',
        data: {
            class: "moveOutTenant",
            action: "getTenantMoveOutSearch",
            keyValue: keyString
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                var html = "";
                if(response.data !== ''){
                    html += '<input type="hidden" id="search_id" value="">\n'+
                        '<table class="table table-hover table-dark" id="myTable"><thead>\n' +
                        '<tr class="header">\n' +
                        '<th>Tenant Name</th>\n' +
                        '<th>Phone</th>\n' +
                        '<th>Email</th>\n' +
                        '<th>propertyName</th>\n' +
                        '<th>Unit</th>\n' +
                        '<th>Rent()</th>\n' +
                        '<th>Balance()</th>\n' +
                        '</tr>\n' +
                        '</thead>\n' +
                        '<tbody>'+response.data+'</tbody>\n' +
                        '</table>';

                }
                $("#tenantData_search").html(html);

            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
            }
            $('#ContactListing-table').trigger( 'reloadGrid' );
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
    keypressCodeSearch();
});
function keypressCodeSearch(){
    $(document).on("keypress",".moveout_input",function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {

            var id = $(".tenantIdget").val();
            var MoveOutTenantName = $('.tenantNameget').val();
            var path = window.location.origin;

            setTimeout(function () {
                if(id == undefined){
                    return false;
                }else{
                    alert(MoveOutTenantName);
                    $(".moveout_input").val(MoveOutTenantName);
                    window.location.href = path + '/Tenant/MoveOutTenant?id=' + id;
                }
            }, 500);
            return false;
        }
    });
}


$(document).on("click","#new_chargeCode_form",function (e){
    $("#myTable").hide();
});

$(document).on("click", "#myTable tbody tr", function (e) {

    var id = $(this).children("input[type='hidden']").val();    // Find the row
    var base_url = window.location.origin;
    window.location.href = base_url + '/Tenant/MoveOutTenant?id='+id;

})  ;