
var base_url = window.location.origin;
jQuery(document).ready(function($){
    var already_html = $('.easy-search').html();
    $('.easy-search').html("<form autocomplete='off'>"+already_html+"</form>");
    $(document).on('keyup','.easy-search input',function () {
        if($("#easy_search").hasClass("in")){ $(".modal-backdrop").addClass("newtest").removeClass("modal-backdrop"); } else{ $(".newtest").addClass("modal-backdrop").removeClass("newtest"); }
        if($("#easy_search").hasClass("in")){  $(".modal-backdrop").addClass("newtest").removeClass("modal-backdrop"); } else{  $(".newtest").addClass("modal-backdrop").removeClass("newtest"); }
        var val = $(this).val();
        searchingEasysearch(val,'searchingActive');
    });
function searchingEasysearch(val,action) {
    $.ajax({
        type: 'post',
        url: '/elastic/search',
        data: {
            class: "ElasticAjax",
            action: action,
            val:val
        },
        success: function (response) {
            console.log(response);
            /* var data = $.parseJSON(response);*/

            // $('#easy_search').modal({backdrop: 'false', keyboard: false})
            $('#easy_search .modal-body').html(response);
            highlight(val);

        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

    function highlight(text) {
        if(text != '') {
            var newHtml = $('#highlightYellow').html();
            var regEx = new RegExp("(" + text+ ")(?!([^<]+)?>)", "gi");
            var output = newHtml.replace(regEx, "<span class='highlight'>$1</span>");
            $('#highlightYellow').html(output );
        }
    }
    $(document).on('click','.advance_search',function () {

        var val = $(".easy-search input").val();
        searchingEasysearch(val,"searchingAll");
    });
$(document).on("keyup",".notes_date_right",function () {
   // $(".notes-timer-outer span").css('background','#ddd');
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    var today=new Date();
    var hh=today.getHours();
    var mm=today.getMinutes();
    var ss=today.getSeconds();
    var week=(today.getDay());
    week=(days[week]).substr(0,3);
    today= (today.getMonth()+1)+'/'+today.getDate()+'/'+today.getFullYear()+' '+'('+week+'.'+')';
    $(".time_span").text(today+' '+hh+':'+mm);
 });


    var length = $('.notes_date_right_div').length;
    if(length > 0){
     //   alert("hello");
        $('.notes_date_right').each(function(key,value){

            var innerHtml = $(value).wrap('<p/>').parent().html();
            var value_notes=$(this).val();
            $(value).parent().html('<div class="notes-timer-outer">'+innerHtml+'<span class="time_span"></span> </div>');
            $(this).val(value_notes);
        });
        }else{
       // alert("hello1");
        setTimeout(function () {
            var value_notes=$(".notes_date_right").val();
            var html=$('.notes_date_right_div').html();
            $(".notes_date_right_div").html('<div class="notes-timer-outer">'+html+'<span class="time_span"></span> </div>');
            $(".notes_date_right").val(value_notes);
        },2000);

    }

/*});*/

  /*  $(document).on("mouseup",".notes_date_right",function () {

        var width = $('.notes_date_right').width()-420;
        $('.notes-timer-outer span').css('right','-'+width+'px');
    });*/
  /*06-02-2020*/
  //  $(".easy-search input[type='text']").val("Easy Search");
    /*06-02-2020*/

    $(document).on("keyup",".easy-search input",function () {
      /*  $("#easy_search").modal('hide');*/
        $("#easy_search").modal('show');
    });
    $(document).on("click",".clsSuggestion.help_dp", function(){
        setTimeout(function(){
            $("#txtSupportSessionCode").trigger("focus");
        },1000);
    });

    $(document).on("keypress",".onlyFirstCaps", function(){
        var value = $(this).val();
        if(value.length > 0){
            $(this).removeClass("capsOn");
        }else{
            $(this).addClass("capsOn");
        }
    });

    $('input,textarea').not('.bootstrap-tagsinput input, #guarantor_email, #guarantor_form2_email, input[type="emergency_email"],input[type="password"],input[type="email"],input[type="image"],input[name="email"],input[name="email[]"],input[name="femail"],input[name="account_admin_email_address"],input[name="support_email"],input[name="additional_email[]"],input[name="emergency_email[]"],input[name="other_email"],#billAmount,.customItemAmountValidation,.input_discount,.number_only').addClass("capital");
    $('.calander').prop('readonly',true);
        if(localStorage.getItem("Message")) {
        var message = localStorage.getItem("Message");
        toastr.success(message);
        localStorage.removeItem('Message');
        jqgridNewOrUpdated = 'true';
    }
    if(localStorage.getItem("rowcolor"))
    {
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
            localStorage.removeItem('rowcolor');
        }, 1000);
    }

    if(localStorage.getItem("rowcolor_vendor"))
    {
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(12)').addClass("green_row_right");
            localStorage.removeItem('rowcolor_vendor');
        }, 1000);
    }
    if(localStorage.getItem("cancelRecord"))
    {
      //  toastr.success(message);
        setTimeout(function(){
            var table_green_id = localStorage.getItem("table_green_tableid");
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
            jQuery(table_green_id).find('tr:eq(1)').find('td').last().addClass("green_row_right");
            localStorage.removeItem('cancelRecord');
            localStorage.removeItem('table_green_tableid');
        }, 1000);
        jqgridNewOrUpdated = 'true';
    }
    if(localStorage.getItem("rowcolorgroup"))
    {
        setTimeout(function(){
            jQuery('#communication-draft-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('#communication-draft-table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
            jQuery('#communication-draft-table-mesg').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('#communication-draft-table-mesg').find('tr:eq(1)').find('td').last().addClass("green_row_right");
            jQuery('#communication-sent-table-email').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('#communication-sent-table-email').find('tr:eq(1)').find('td').last().addClass("green_row_right");
            jQuery('#communication-sent-table-text').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('#communication-sent-table-text').find('tr:eq(1)').find('td').last().addClass("green_row_right");
            localStorage.removeItem('rowcolorgroup');
        }, 1000);
    }
    if(localStorage.getItem("contactrowcolor"))
    {
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(6)').addClass("green_row_right");
            localStorage.removeItem('contactrowcolor');
        }, 1000);
    }

    $(window).on("scroll", function() {
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        if (scrollPosition >1000)  {
            $("footer a").show(500);
        }else{
            $("footer a").hide(500);
        }
    });
    $(document).on("click", ".movetotopicon", function(){
        $("html, body").animate({scrollTop : 0},700);
    });
    document.addEventListener ("keydown", function (zEvent) {
        if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.key === "p") {  //  case sensitive
            window.location.href='/Tenantlisting/Tenantlisting';
        }if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.key === "b") {  //  case sensitive
            window.location.href='/Accounting/Accounting';
        }if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.key === "1") {  // case sensitive
            window.location.href='/GuestCard/ListGuestCard';
        }if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.key === "2") {  // case sensitive
            window.location.href='/Tenantlisting/Tenantlisting';
        }if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.key === "3") {  // case sensitive
            window.location.href='/Property/PropertyModules';
        }if (zEvent.ctrlKey  &&  zEvent.altKey  &&  (zEvent.key === "4" || zEvent.key === "₹")) {  // case sensitive
            window.location.href='/Communication/InboxMails';
        }if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.key === "6" ) {  // case sensitive
            window.location.href='/ticket/tickets';
        }if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.key === "7") {  // case sensitive
            window.location.href='/Marketing/MarketingListing';
        }if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.key === "z") {  // case sensitive
            window.location.href='/Dashboard/Dashboard';
        }if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.key === "h") {  // case sensitive
            window.open("https://help.apexlink.com/?h=TRUE");
        }if (zEvent.ctrlKey  &&  zEvent.altKey  &&  (zEvent.key === "o" || zEvent.key === "ō")) {  // case sensitive
            window.location.href='/Setting/Settings/';
         /*   window.open("http://phytotherapy.in:8097/");*/
        }if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.key === "r") {  // case sensitive
           // alert("hello");
            var email = $("#email").val();

            var pass = $("#password").val();
            var super_admin_name = $("#super_admin_name").val();

            if($("#remember").is(':checked'))
                var remember = 'on';  // checked
            else
                var remember = '';  // unchecked
            var systemDate = new Date();
            var year = systemDate.getFullYear();
            var month = systemDate.getMonth()+1;
            var date = systemDate.getDate();
            var hour= systemDate.getHours();
            var minutes= systemDate.getMinutes();
            var seconds= systemDate.getSeconds();
            var dateformats = year +'-'+month+'-'+date+' '+hour+':'+minutes+':'+seconds;
            $.ajax
            ({
                type: 'post',
                url: 'company-user-ajax',
                data: {
                    class: "UserAjax",
                    action: "login",
                    email: email,
                    password: pass,
                    super_admin_name : super_admin_name,
                    remember: remember,
                    timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                    dateformats:dateformats
                },
                success: function (response) {
                    var data = $.parseJSON(response);

                    if (data.status == "success")
                    {
                        var redirect = (data.go_to_site_link != '' && data.go_to_site_link !== undefined)? data.go_to_site_link :data.data.enabled_2fa;

                        if(redirect == 'on' || redirect == '0'){

                            localStorage.setItem("login_user_id", data.data.id);
                            // alert(data.active_inactive_status.status);
                            localStorage.setItem("active_inactive_status",(data.active_inactive_status.status == undefined   ) ? 'all': data.active_inactive_status.status );
                            if (data.data.last_user_url) {
                                window.location.href = data.data.last_user_url;
                            } else {
                                window.location.href = "/Dashboard/Dashboard";
                            }
                        }

                        if(redirect == '1') {
                            window.location.href = "/2fa";
                        }
                    } else
                    {
                        toastr.error(data.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.key === "8") {  // case sensitive
            $.ajax
            ({
                type: 'post',
                url: '/company-user-ajax',
                data: {
                    class: "UserAjax",
                    action: "logout",
                    data :window.location.pathname+window.location.search
                },
                success: function (response) {
                    var data = $.parseJSON(response);
                    if (data.status == "success")
                    {
                        localStorage.removeItem("announcement_ids");
                        window.location.href = "/";
                    } else
                    {
                        toastr.error(data.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
        if (zEvent.ctrlKey  &&   zEvent.key === "x") {
        var url=$(".main-nav .active").next().find("a").attr("href");
        if(url!=undefined && url!=""){
            window.location.href = url;
            }
        }
        if (zEvent.key === "t") {
          //  if($(".calander").is(":focus")){
            var focused = document.activeElement;
        if($(focused).hasClass('calander')){
        var name1=$(focused).attr("name");
            var date = $.datepicker.formatDate(jsDateFomat, new Date());
            var currencySign = currencySign;
            $("input[name='"+name1+"']").val(date);
         }
        }
        if (zEvent.key === "y") {
            var focused = document.activeElement;
            if($(focused).hasClass('calander')){
                var name1=$(focused).attr("name");
                var date = $.datepicker.formatDate(jsDateFomat, new Date());
                var result = new Date(date);
                result.setDate(result.getDate() - 1);
                var date1 = $.datepicker.formatDate(jsDateFomat, new Date(result));
                var currencySign = currencySign;
                $("input[name='"+name1+"']").val(date1);
            }
        }
        if (zEvent.key === "+") {
            var focused = document.activeElement;
            if($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");

                var value =$("input[name='"+name1+"']").val();
                var result = new Date(value);
                result.setDate(result.getDate() + 1);
                var date = $.datepicker.formatDate(jsDateFomat, new Date(result));
                $("input[name='"+name1+"']").val(date);

            }
            }
        if (zEvent.key === "-") {
            var focused = document.activeElement;
            if($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");

                var value =$("input[name='"+name1+"']").val();
                var result = new Date(value);
                result.setDate(result.getDate() - 1);
                var date = $.datepicker.formatDate(jsDateFomat, new Date(result));
                $("input[name='"+name1+"']").val(date);

            }
        }
        if (zEvent.key === "F9") {
            var focused = document.activeElement;
            if($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");
                var timestmp = new Date().setFullYear(new Date().getFullYear(), 0, 1);
                var date = $.datepicker.formatDate(jsDateFomat, new Date(timestmp));
                $("input[name='"+name1+"']").val(date);

            }
        } if (zEvent.key === "F10") {
            var focused = document.activeElement;
            if($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");
                var timestmp = new Date().setFullYear(new Date().getFullYear(), 11, 31);
                var date = $.datepicker.formatDate(jsDateFomat, new Date(timestmp));
                $("input[name='"+name1+"']").val(date);

            }
        }if (zEvent.key === "w") {
            var focused = document.activeElement;
            if($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");
                d = new Date();
                var day = d.getDay();
                var timestmp = new Date().setFullYear(new Date().getFullYear(),  new Date().getMonth(),d.getDate() - day + (day == 0 ? -6:1));
                var date = $.datepicker.formatDate(jsDateFomat, new Date(timestmp));
                $("input[name='"+name1+"']").val(date);

            }
        }if (zEvent.key === "k") {
            var focused = document.activeElement;
            if($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");
                d = new Date();
               // var day = d.getDay();
                var timestmp = new Date().setFullYear(new Date().getFullYear(),  new Date().getMonth(),d.getDate() - (d.getDay() - 1) + 6);
                var date = $.datepicker.formatDate(jsDateFomat, new Date(timestmp));
                $("input[name='"+name1+"']").val(date);

            }
        }
        if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.key === "m") {
            var focused = document.activeElement;
            if ($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");
                var timestmp = new Date().setFullYear(new Date().getFullYear(),  new Date().getMonth(), 1);
                var date = $.datepicker.formatDate(jsDateFomat, new Date(timestmp));
                $("input[name='" + name1 + "']").val(date);

            }
        }if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.key === "n") {
            var focused = document.activeElement;
            if ($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");
                var timestmp = new Date().setFullYear(new Date().getFullYear(),  new Date().getMonth()+1, 0);
                var date = $.datepicker.formatDate(jsDateFomat, new Date(timestmp));
                $("input[name='" + name1 + "']").val(date);

            }
        }
        });
    $(document).on("keydown", "body", function(e){

        var checkId = e.which;

        if(checkId == 36){
            $("#homebtn_srtct").modal('show');
            $(".modal-backdrop").css("opacity","1");
            $(".modal-backdrop").css("background-color","#303030");
            return false;
        }
        if(checkId == 27){
            //alert("hello");
            $("#homebtn_srtct").modal('hide');
            $(".modal-backdrop").css("opacity","0");
            $(".modal-backdrop").css("background-color","");
            return false;
        }

    });


    $(window).on("resize", function () {
        var grid = $(".ui-jqgrid-btable");
        var newWidth = $(".apx-table").width();
        grid.jqGrid("setGridWidth", newWidth, true);
        if(grid.length > 1){
        /*    for(var i = 0; i < grid.length; i++){
                var grids = grid[i];

            }*/
        }else{
            var $grid = $(".ui-jqgrid-btable"),newWidth = $grid.closest(".ui-jqgrid").parent().width();
            $grid.jqGrid("setGridWidth", newWidth, true);
        }
    });
});

function onTop(rowdata){
    if(rowdata){
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
        }, 1000);
    }
}


function getZipCode(element,zipcode,city,state,country,county,validation){

    $(validation).val('1');
    $.ajax({
        type: 'post',
        url: '/common-ajax',
        data: {zip_code: zipcode,class: 'CommonAjax', action: 'getZipcode'},
        success: function (response) {
            var data = JSON.parse(response);
            if(data.code == 200){
                if(city !== undefined)$(city).val(data.data.city);
                if(state !== undefined)$(state).val(data.data.state);
                if(country !== undefined)$(country).val(data.data.country);
                if(county !== undefined)$(county).val(data.data.county);
            } else {
                // console.log('sss>>>', zipcode);
                getAddressInfoByZip(element,zipcode,city,state,country,county,validation);
            }
        },
        error: function (data) {

        }
    });
}


/**
 *Get location on the basis of zipcode
 */
function getAddressInfoByZip(element,zip,city,state,country,county,validation){
    // console.log('ssaaas>>>', zip);
    var addr = {};
    addr.element = element;
    addr.elecity = city;
    addr.elestate = state;
    addr.elecountry = country;
    addr.elecounty = county;
    addr.validation = validation;

    if(zip.length >= 5 && typeof google != 'undefined'){
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){

            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        // console.log('addr>>>', addr);
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    response(addr);
                } else {
                    addr.success = false;
                    response(addr);
                }
            } else {
                addr.success = false;
                response(addr);
            }
        });
    } else {
        addr.success = false;
        response(addr);
    }
}

/**
 * Set values in the city, state and country when focus loose from zipcode field.
 */
function response(obj){
    if(obj.success){
        if(obj.elecity !== undefined)$(obj.elecity).val(obj.city);
        if(obj.elestate !== undefined)$(obj.elestate).val(obj.state);
        if(obj.elecountry !== undefined)$(obj.elecountry).val(obj.country);
        //if(obj.elecounty !== undefined)$(obj.elecounty).val(obj.country);
    } else {
        $(obj.validation).val('0');
        if(obj.elecity !== undefined)$(obj.elecity).val('');
        if(obj.elestate !== undefined)$(obj.elestate).val('');
        if(obj.elecountry !== undefined)$(obj.elecountry).val('');
        if(obj.elecounty !== undefined)$(obj.elecounty).val('');
        // toastr.error('Please enter a valid zipcode!');
        $(obj.element).valid();
    }
}

//custom validation
function validations(element){
    var required = $(element).attr('data_required');
    var number = $(element).attr('data_number');
    var only_number = $(element).attr('data_only_number');
    var url = $(element).attr('data_url');
    var max = $(element).attr('data_max');
    var min = $(element).attr('data_min');
    var email = $(element).attr('data_email');
    var value = $(element).val();
    var msg = '';
    if(required == "true"){
        if(value == ''){
            msg = '* This field is required.';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }
    if(max !== undefined){
        if(value.length > max){
            msg = '* Please enter less than '+max+' characters.';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }

    if(min !== undefined){
        if(value.length > min){
            msg = '* Minimum '+min+' character required.';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }

    if(only_number !== undefined) {
        if (isNaN(value)) {
            msg = '* Only number are allowed!';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }

    if(number !== undefined) {
        if (isNaN(value)) {
            msg = '* Only number are allowed!';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        } else {
            if(value > number){
                msg = '* please enter less than '+number;
                $(element).siblings('.customError').text(msg);
                $(element).focus();
                return false;
            }
        }
    }

    if(url !== undefined){
        var expression =  /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm;
        var regex = new RegExp(expression);
        if(value.match(regex)){
        } else {
            msg = '* Please enter a valid URL.';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }

    if(email !== undefined){
        var expression =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(expression.test(value)){

        } else {
            msg = '* Please enter a valid email address.';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }
    $(element).siblings('.customError').text('');
    return true;
}


function validations_old(element){
    var required = $(element).attr('data_required');
    var number = $(element).attr('data_number');
    var only_number = $(element).attr('data_only_number');
    var url = $(element).attr('data_url');
    var max = $(element).attr('data_max');
    var min = $(element).attr('data_min');
    var email = $(element).attr('data_email');
    var value = $(element).val();
    var msg = '';
    if(required == "true"){
        if(value == ''){
            msg = '* This field is required.';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }
    if(max !== undefined){
        if(value.length > max){
            msg = '* Please enter less than '+max+' characters.';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }

    if(min !== undefined){
        if(value.length > min){
            msg = '* Minimum '+min+' character required.';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }

    if(only_number !== undefined) {
        if (isNaN(value)) {
            msg = '* Only number are allowed!';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }

    if(number !== undefined) {
        if (isNaN(value)) {
            msg = '* Only number are allowed!';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        } else {
            if(value > number){
                msg = '* please enter less than '+number;
                $(element).siblings('.customError').text(msg);
                $(element).focus();
                return false;
            }
        }
    }

    if(url !== undefined){
        var expression =  /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm;
        var regex = new RegExp(expression);
        if(value.match(regex)){
        } else {
            msg = '* Please enter a valid URL.';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }

    if(email !== undefined){
        var expression =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(expression.test(value)){

        } else {
            msg = '* Please enter a valid email address.';
            $(element).siblings('.customError').text(msg);
            $(element).focus();
            return false;
        }
    }
    $(element).siblings('.customError').text('');
    return true;
}

function randomNumberString(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

$(document).ready(function(){
    $(".only_letter").keypress(function(event){
        var inputValue = event.which;
        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)||inputValue == 91||inputValue == 93||inputValue == 94||inputValue == 95){
            event.preventDefault();
        }
    });
});

function getBuildings(property_id){

        $.ajax({
            type: 'post',
            url: '/common-ajax',
            data: {id: property_id, class: 'CommonAjax', action: 'getBuildings'},
            success: function (response) {
                var data = $.parseJSON(response);
                return data;
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

}

$(document).ready(function() {
    $('.first_capital').on('keydown', function(event) {
        if (this.selectionStart == 0 && event.keyCode >= 65 && event.keyCode <= 90 && !(event.shiftKey) && !(event.ctrlKey) && !(event.metaKey) && !(event.altKey)) {
            var $t = $(this);
            event.preventDefault();
            var char = String.fromCharCode(event.keyCode);
            $t.val(char + $t.val().slice(this.selectionEnd));
            this.setSelectionRange(1,1);
        }
    });

    $(document).on('keydown', '.capital', function(event) {
        if (this.selectionStart == 0 && event.keyCode >= 65 && event.keyCode <= 90 && !(event.shiftKey) && !(event.ctrlKey) && !(event.metaKey) && !(event.altKey)) {
            var $t = $(this);
            event.preventDefault();
            var char = String.fromCharCode(event.keyCode);
            $t.val(char + $t.val().slice(this.selectionEnd));
            this.setSelectionRange(1,1);
        }
    });
});
$(document).on('focusout','.amount_num',function(){
    if($(this).val() != '' && $(this).val().indexOf(".") == -1) {
        var bef = $(this).val().replace(/,/g, '');
        var value = numberWithCommas(addZeroes(bef));
        $(this).val(value);
    } else {
        var bef = $(this).val().replace(/,/g, '');
        $(this).val(numberWithCommas(addZeroes(bef)));
    }
});

function addZeroes(num) {
    // Convert input string to a number and store as a variable.
    var value = Math.round(Number(num) * 100) / 100;
    // Split the input string into two arrays containing integers/decimals
    var res = num.split(".");
    // If there is no decimal point or only one decimal place found.
    if(res.length == 1 || res[1].length < 3) {
    // Set the number to two decimal places
        value = value.toFixed(2);
    }
    // Return updated or original number.
    return value;
}


function changeToFloat(amount){
    if(amount != '' && amount.indexOf(".") == -1) {
        var bef = amount.replace(/,/g, '');
        var value = numberWithCommas(addZeroes(bef));
        return value;
    } else {
        var bef = amount.replace(/,/g, '');
        return numberWithCommas(addZeroes(bef));
    }
}


$(document).on('focusout','.amount',function(){
    var id = this.id;
    if($('#' + id).val() != '' && $('#' + id).val().indexOf(".") == -1) {
        var bef = $('#' + id).val().replace(/,/g, '');
        var value = numberWithCommas(bef) + '.00';
        $('#' + id).val(value);
    } else {
        var bef = $('#' + id).val().replace(/,/g, '');
        $('#' + id).val(numberWithCommas(bef));
    }
});


$(document).on('keypress','.amount_class',function(e){
    if(this.value.length==10) return false;
});

$(document).on('focusout','.amount_class',function(){
    if($(this).val() != '' && $(this).val().indexOf(".") == -1) {
        var bef = $(this).val().replace(/,/g, '');
        var value = numberWithCommas(bef) + '.00';
        $(this).val(value);
    } else {
        var bef = $(this).val().replace(/,/g, '');
        $(this).val(numberWithCommas(bef));
    }
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

$(document).on('keydown', '.number_only', function (e) {
    if (e.which != 8 && e.which != 110 && e.which != 190 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
        return false;
    }
});

$(document).on('keydown', '.number_only_for_extension', function (e) {
    if (e.which != 187 && e.which != 46 && e.which != 37 && e.which != 39 &&e.which != 116 && e.which != 16 && e.which != 17 &&  e.which != 107 && e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
        return false;
    }
});

$(document).on('click', '.right-links', function(e) {
    $(this).parent().toggleClass('show-links');
    $(this).parent().toggleClass('hide-links');
});
/**
 * To change the format of phone number
 */
$(document).on('keydown', '.phone_number_format', function (e) {
    if (e.which != 8 && e.which != 110 && e.which != 190 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
        return false;
    }
    $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
});

$(document).on('click', '.side-nav-new-conversation', function(e) {
    localStorage.setItem('side-nav', 'new-conversation');
    var base_url = window.location.origin;
    window.location.href = base_url+"/Communication/Conversation";
});


$(document).on('click', '.side-nav-task-reminder', function(e) {
    localStorage.setItem('side-nav', 'task-reminder');
    var base_url = window.location.origin;
    window.location.href = base_url+"/Communication/Conversation";
});


$(document).on('click','.compose-mail',function () {
    localStorage.setItem('composer_mail_id','');
    localStorage.setItem('composer_mail_type','new');
    var urltype = $(this).attr('data-urltype');
    window.location.href=urltype+'/Communication/ComposeEmail';
});

$(document).on('click','#composer_group',function () {
    localStorage.setItem('composer_mail_id','');
    localStorage.setItem('composer_mail_type','new');
    var urltype = $(this).attr('data-urltype');
    window.location.href='/Communication/AddGroupMessage';
});

$(document).on('click','.compose-text',function () {
    localStorage.setItem('composer_mail_id','');
    localStorage.setItem('composer_mail_type','new');
    var urltype = $(this).attr('data-urltype');
    window.location.href='/Communication/AddTextMessage';
});

$(document).on('click', '.gotoAddNewLetter', function(e) {
    localStorage.setItem('side-nav', 'new-letter');
    var base_url = window.location.origin;
    window.location.href = base_url+"/MasterData/Documents";
});

$(document).on('keypress','#txtSupportSessionCode',function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
});
$(document).on('click','a.clsSuggestion.help_dp',function () {
   $('#txtSupportSessionCode').focus();
});

$(document).on("cut copy paste",".hide_copy",function(e) {
    e.preventDefault();
});
getFlagCount();
function getFlagCount(){
    $.ajax({
        type: 'POST',
        url: '/flag-ajax',
        data: {class: 'Flag', action: "getFlagCount"},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200){
                setTimeout(function () {
                    $('#spnFlagCount').html(res.count);
                },50);
            }
        },
        error: function (data) {

        }
    });
}
systemLogout();
function systemLogout(){
    $.ajax({
        type: 'post',
        url: '/common-ajax',
        data: { class: 'CommonAjax', action: 'systemLogout'},
        success: function (response) {

            var data = $.parseJSON(response);

            var IDLE_TIMEOUT = data*60; //seconds
            var _idleSecondsTimer = null;
            var _idleSecondsCounter = 0;

            document.onclick = function() {
                _idleSecondsCounter = 0;
            };

            document.onmousemove = function() {
                _idleSecondsCounter = 0;
            };

            document.onkeypress = function() {
                _idleSecondsCounter = 0;
            };

            _idleSecondsTimer = window.setInterval(CheckIdleTime, 1000);

            function CheckIdleTime() {
                _idleSecondsCounter++;
                var oPanel = document.getElementById("SecondsUntilExpire");
                if (oPanel) oPanel.innerHTML = (IDLE_TIMEOUT - _idleSecondsCounter) + "";
                // console.log(_idleSecondsCounter);

                if (_idleSecondsCounter >= IDLE_TIMEOUT) {
                    window.clearInterval(_idleSecondsTimer);
                    $.ajax
                    ({
                        type: 'post',
                        url: '/company-user-ajax',
                        data: {
                            class: "UserAjax",
                            action: "logout",
                            data :window.location.pathname+window.location.search
                        },
                        success: function (response) {
                            var data = $.parseJSON(response);
                            if (data.status == "success")
                            {
                                localStorage.removeItem("announcement_ids");
                                window.location.href = "/";
                            } else
                            {
                                toastr.error(data.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                // alert(key+value);
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                }
            }

        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
/*$(document).on('keyup','.notesDateTime',function () {
    $('.time_notes').remove();
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    var today=new Date();
    var hh=today.getHours();
    var mm=today.getMinutes();
    var ss=today.getSeconds();
    var week=(today.getDay());
    week=(days[week]).substr(0,3);
    today= (today.getMonth()+1)+'/'+today.getDate()+'/'+today.getFullYear()+' '+'('+week+'.'+')';
    $(this).prepend('<span class="time_notes">'+today+' '+hh+':'+mm+':'+ss+'</span>');
});*/

function resetEditForm(formId,expectArray,boot,defaultFormData,defaultIgnoreArray){
    if(boot) {
        bootbox.confirm("Do you want to reset this form?", function (result) {
            if (result == true) {
                resetEditData(formId,expectArray,defaultFormData,defaultIgnoreArray);
                return true;
            } else {
                bootbox.hideAll()
            }
            return false;
        });
    } else {
        resetEditData(formId,expectArray,defaultFormData,defaultIgnoreArray);
        return true;
    }
}

function resetEditData(formId,expectArray,defaultFormData,defaultIgnoreArray){

    var vaildate = $(formId).validate();
    vaildate.resetForm();
    $('.error').text('');
    $.each(defaultFormData, function (key, value) {
        if (!expectArray.includes(value.name)) {
            if(defaultIgnoreArray !== undefined && !defaultIgnoreArray.includes(value.name)){
                var element = $(formId + ' [name="' + value.name + '"]');
                element.val(value.value);

            }
        }
    });
}

function resetFormClear(formId,expectArray,form,boot,defaultFormData,defaultIgnoreArray){
    if(boot) {
        bootbox.confirm("Do you want to clear this form?", function (result) {
            if (result == true) {
                resetFromData(formId,expectArray,form,defaultFormData,defaultIgnoreArray);
                return true;
            } else {
                bootbox.hideAll()
            }
            return false;
        });
    } else {
        resetFromData(formId,expectArray,form,defaultFormData,defaultIgnoreArray);
        return true;
    }
}

function resetFromData(formId,expectArray,form,defaultFormData,defaultIgnoreArray){
    if(form == 'form') {
        var formData = $(formId).serializeArray();
        var vaildate = $(formId).validate();
        vaildate.resetForm();
        $('.error').text('');
        $.each(formData, function (key, value) {
            if (!expectArray.includes(value.name)) {
                var element = $(formId + ' [name="' + value.name + '"]');
                if (element[0].type == 'select-one') {
                    element.prop("selectedIndex", 0);
                } else {
                    element.val('');
                }
            }
        });

        $.each(defaultFormData, function (key, value) {
            if (expectArray.includes(value.name)) {

                if(defaultIgnoreArray !== undefined && !defaultIgnoreArray.includes(value.name)){
                    var element = $(formId + ' [name="' + value.name + '"]');

                    if (element[0].type == 'select-one') {
                        if(value.value != ''){
                            element.val(value.value);
                        } else {
                            element.val("selectedIndex", 0);
                        }

                    } else {
                        element.val(value.value);
                    }
                }
            }
        });

    } else if(form == 'div'){
        var formData = getDivInputs(formId);
        $('.customError').text('');
        $.each(formData, function (key, value) {
            if (!expectArray.includes(value.name)) {
                var element = $(value.element);
                if (element[0].type == 'select-one') {
                    element.prop("selectedIndex", 0);
                } else {
                    element.val('');
                }
            }
        });
    }

}

function getDivInputs(formId){
    var data = $(formId).find('input[type!="button"]','input[type!="hidden"]','input[type!="submit"]');
    var arrayData = [];
    $.each(data,function(key,value){
        if($(value).attr('name') != ''){
            arrayData.push({'name':$(value).attr('name'),'value':$(value).val(),'element':value});
        }
    });

    return arrayData;
}

function edit_date_time(date){
    //alert(date);
   // $(".notes-timer-outer span").css('background','#ddd');
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    var today=new Date('2020-01-13 06:08:11');
    var hh=today.getHours();
    var mm=today.getMinutes();
    var ss=today.getSeconds();
    var week=(today.getDay());
    week=(days[week]).substr(0,3);
    today= (today.getMonth()+1)+'/'+today.getDate()+'/'+today.getFullYear()+' '+'('+week+'.'+')';

    $(".time_span").text(today+' '+hh+':'+mm+':'+ss);
}

/*$(document).on("keyup",".notes_date_right",function () {*/


//sdfsdfsdffdst654
// $(document).on('keypress','.hide_copy',function (e) {
//     if ((e.ctrlKey || e.metaKey) && e.keyCode == 65 && e.keyCode == 97) {
//         e.preventDefault();
//     }
// });


/*Timezone system Time*/

// Set timeout variables.

    function file_upload_email(table_name,column_name,row_id,user_id,url=null,srcc=null) {
        $.ajax({
            type: 'post',
            url: '/common-ajax',
            data: {
                class: 'CommonAjax',
                action: 'getuserinfo',
                id: user_id,
                table_name:table_name,
                column_name:column_name
            },
            success: function (response) {
                var res = JSON.parse(response);

                if(res.code == 200){
                    $("#sendEmail").trigger('reset');
                    $('#sendMailModal').modal('toggle');
                    var path = ($("#"+row_id).find("td:last").find('select').attr("data-path") !== undefined ? $("#"+row_id).find("td:last").find('select').attr("data-path"):url) ;
                    var src =  ($("#"+row_id).find("td:last").find('select').attr("data-src") !== undefined ? $("#"+row_id).find("td:last").find('select').attr("data-src"):srcc) ;

                    var fileNameIndex = path.lastIndexOf("/") + 1;
                    var filename = path.substr(fileNameIndex);
                    var imageFile = '<a href="'+path+'" class="attachments"><img class="img-upload-tab" width="100" height="100" src="'+src+'"></a>';
                    $(".attachmentFile").html(imageFile);
                    //$('.to').tagsinput('add', res.val[0]);
                    $('.to').tagsinput('add', res.val[0], {preventPost: true});
                    $('.to').tagsinput('add', res.val[0], {preventPost: true});
                    $(".filename").html('');
                    $('.attachmentFile').after('<div class="filename">'+filename+'</div>');
                }
            },
        });

    }
function sendmailattachment(){

}

/**
 * function to get status saved in db
 */
$(document).on('change','.jqGridStatusClass',function(){
    var module = $(this).attr('data-module');
    var value = $(this).val();
    $.ajax
    ({
        type: 'post',
        url: '/global-user-module-ajax',
        data: {
            class: "GlobalUserModule",
            action: "saveModuleStatus",
            data :{module:module,value:value}
        },
        success: function (response) {
            var data = $.parseJSON(response);
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
        }
    });
});

jQuery(document).ready(function($) {
    getGridStatus();
    function getGridStatus() {
        var module = $('.jqGridStatusClass').attr('data-module');
        if(module !== undefined && module != '') {
            $.ajax
            ({
                type: 'post',
                url: '/global-user-module-ajax',
                data: {
                    class: "GlobalUserModule",
                    action: "getModuleStatus",
                    data: {module: module}
                },
                success: function (response) {
                    var data = $.parseJSON(response);
                    if (data.code == 200) {
                        if(data.data_status != '') {
                            $('.jqGridStatusClass').val(data.data_status);
                            if(module == 'PROPERTY'){
                                setTimeout(function(){
                                    $('.jqGridStatusClass').trigger('change');
                                },1000)
                            } else {
                                $('.jqGridStatusClass').trigger('change');
                            }

                        }
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                }
            });
        }
    }

});
function update_users_flag(table,id){
    var res;
    $.ajax
    ({
        type: 'post',
        url: '/common-ajax',
        async: false,
        data: { class: 'CommonAjax',
            action: 'updateuserflag',
            id:id,
            table:table
        },
        success: function (response) {
            var data = $.parseJSON(response);
            res = data;
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
        }
    });
    return res;
}

$(document).on('click','.modal-body table tbody tr',function(){
    var tr_id =  $(this).children('td:first').text();
    var module_table = $(this).parent().parent().parent().attr('data-table');
    var heading = $(this).parent().parent().parent().parent().parent().parent().find('h4').text();
    console.log('tr_id',tr_id);
    console.log('module_table',module_table);
    console.log('heading',heading);
    if(tr_id == '') return ;
    switch (heading) {
        case "Property":
            localStorage.setItem('ElasticSearch',tr_id);
            window.location.href = "/Property/PropertyModules";
            break;
        case "Building":
            localStorage.setItem('ElasticSearch',tr_id);
           // window.location.href = "/Tenantlisting/Tenantlisting";
            break;
        case "Units":
            localStorage.setItem('ElasticSearch',tr_id);
         //   window.location.href = "/Tenantlisting/ActiveTenant?id=" + id;
            // window.location.href = "/GuestCard/ListGuestCard";
            break;
        case "Vendors":
            localStorage.setItem('ElasticSearch',tr_id);
            window.location.href = "/Vendor/Vendor";
            break;
        case "Contacts":
            localStorage.setItem('ElasticSearch',tr_id);
            window.location.href = "/People/ContactListt";
            break;
        case "employee":
            localStorage.setItem('ElasticSearch',tr_id);
            window.location.href = "/People/GetEmployeeList";
            break;
        case "Owners":
            localStorage.setItem('ElasticSearch',tr_id);
            window.location.href = "/People/Ownerlisting";
            break;
        case "Tenants":
            localStorage.setItem('ElasticSearch',tr_id);
            window.location.href = "/Tenantlisting/Tenantlisting";
            break;
        case "rental_application":
            localStorage.setItem('ElasticSearch',tr_id);
            window.location.href = "/RentalApplication/RentalApplications";
            break;
        case "communication":
            localStorage.setItem('ElasticSearch',tr_id);
            window.location.href = "/MasterData/Documents";
            break;
        case "lease":
            localStorage.setItem('ElasticSearch',tr_id);
            window.location.href = "/Lease/ViewEditLease";
            break;
        case "instrument_register":
            localStorage.setItem('ElasticSearch',tr_id);
            // window.location.href = "/Property/GeneralLedger?id=" + id;
            break;
        case "deposit_register":
            localStorage.setItem('ElasticSearch',tr_id);
          //  window.location.href = "/Property/PropertyView?id=" + id;
            break;
        case "journal_entry":
            localStorage.setItem('ElasticSearch',tr_id);
            window.location.href = "/Accounting/JournalEntries";
            break;
        case "budget":
            localStorage.setItem('ElasticSearch',tr_id);
            window.location.href = "/Accounting/Budgeting";
            break;
        case "Tickets":
            localStorage.setItem('ElasticSearch',tr_id);
          //  window.location.href = "/Property/PropertyView?id=" + id;
            break;
        case "invoice":
            localStorage.setItem('ElasticSearch',tr_id);
            window.location.href = "/Accounting/ConsolidatedInvoice";
            break;
        case "payment_register":
            localStorage.setItem('ElasticSearch',tr_id);
        //    window.location.href = "/Property/PropertyView?id=" + id;
            break;
        case "check_register":
            localStorage.setItem('ElasticSearch',tr_id);
           // window.location.href = "/Communication/InTouch";
            break;
        case "Waiting List":
            localStorage.setItem('ElasticSearch',tr_id);
         //   window.location.href = "/Communication/WaitingList";
            break;
        case "Purchase Order":
            localStorage.setItem('ElasticSearch',tr_id);
            window.location.href = "/PurchaseOrder/PurchaseOrderListing";
            break;
        default:

    }

});

function checkPermissions(permissions,module,permission_module){
    var checkPermissionModule = permissions[module.parent];
    if(permissions['selected'] !== undefined && permissions['selected'] == 'all'){
        return 'Access Granted';
    }

    if(permissions[module.parent]['selected'] == 'true'){
        return 'Access Granted';
    }

    if(checkPermissionModule['child']['length'] > 0 && checkPermissionModule['child'][module.child]['selected'] == 'true'){
        return 'Access Granted';
    } else if(checkPermissionModule['child']['length'] > 0){
        checkPermissionModule = checkPermissionModule['child'][module.child];
    }

    if(checkPermissionModule['child']['length'] > 0 && checkPermissionModule['child'][module.subChild]['selected'] == 'true'){
        return 'Access Granted';
    } else if(checkPermissionModule['child']['length'] > 0){
        checkPermissionModule = checkPermissionModule['child'][module.subChild];
    }

    if(checkPermissionModule['child'][permission_module]['selected'] == 'true'){
        return 'Access Granted';
    } else {
        toastr.error('Access Denied!');
        return 'Access Denied';
    }
}