<?php
/**
 * DDL class used for DDL operations
 * Created by PhpStorm.
 * User: Parvesh
 * Date: 17-April-19
 */

include(ROOT_URL."/config.php");

if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
    $url = BASE_URL."login";
    header('Location: '.$url);
}

class MigrationCompanySetup extends DBConnection{


    function __construct()
    {
        parent::__construct();
        //$action = $_REQUEST['action'];
        //echo json_encode($this->$action());

    }
    /**
     *  Insert Data to Company Users
     */
    public static function createPropertyType($connection, $runQuery=null)
    {
        $return = array();

        try {
            if($runQuery == 'all')
            {
               // $connection = DBConnection::dynamicDbConnection();

            } else {
                $table = "CREATE TABLE IF NOT EXISTS company_property_type (
                 `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                 `property_type`  VARCHAR(255) DEFAULT NULL,
                 `description`  text,
                 `user_id` int(11) DEFAULT NULL,
                 `is_default` enum('0','1') NOT NULL,
                 `status` enum('0','1') NOT NULL,
                 `deleted_at` timestamp NULL DEFAULT NULL,
                 `created_at` timestamp NULL DEFAULT NULL,
                 `updated_at` timestamp NULL DEFAULT NULL,
                 `is_editable` TINYINT DEFAULT NULL,
                  PRIMARY KEY (`id`)
                )";

                $connection->prepare($table)->execute();
                return array('code' => 200, 'status' => 'success','message' => 'company propertySetup type table created successfully');

            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }

        return $return;
    }

    /**
     * Create propertySetup unit type
     * @param $connection
     * @param null $runQuery
     * @return array
     */
    public static function createPropertyUnitType($connection, $runQuery=null)
    {
        $return = array();
        try {
            if($runQuery == 'all')
            {
                $connection = DBConnection::dynamicDbConnection();
            } else {
                $table = "CREATE TABLE IF NOT EXISTS company_unit_type (
                 `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                 `user_id` int(11) DEFAULT NULL,
                 `unit_type`  VARCHAR(255) DEFAULT NULL,
                 `description`  text,
                 `is_default` enum('0','1') NOT NULL,
                 `status` enum('0','1') NOT NULL,
                 `deleted_at` timestamp NULL DEFAULT NULL,
                 `created_at` timestamp NULL DEFAULT NULL,
                 `updated_at` timestamp NULL DEFAULT NULL,
                 `is_editable`  TINYINT DEFAULT NULL,
                  PRIMARY KEY (`id`)
                )";
                $connection->prepare($table)->execute();
                return array('code' => 200, 'status' => 'success','message' => 'company propertySetup type table created successfully');
            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
        return $return;
    }

    /**
     * Create propertySetup sub type
     * @param $connection
     * @param null $runQuery
     * @return array
     */
    public static function createPropertySubType($connection, $runQuery=null)
    {
        $return = array();
        try {
            if($runQuery == 'all')
            {
                $connection = DBConnection::dynamicDbConnection();
            } else {
                $table = "CREATE TABLE IF NOT EXISTS company_property_subtype (
                 `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                 `user_id` int(11) DEFAULT NULL,
                 `property_subtype`  VARCHAR(255) DEFAULT NULL,
                 `description`  text,
                 `is_default` enum('0','1') NOT NULL,
                 `status` enum('0','1') NOT NULL,
                 `deleted_at` timestamp NULL DEFAULT NULL,
                 `created_at` timestamp NULL DEFAULT NULL,
                 `updated_at` timestamp NULL DEFAULT NULL,
                 `is_editable`  TINYINT DEFAULT NULL,
                  PRIMARY KEY (`id`)
                )";
                $connection->prepare($table)->execute();
                return array('code' => 200, 'status' => 'success','message' => 'company propertySetup type table created successfully');
            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
        return $return;
    }

    /**
     * Create propertySetup sub type
     * @param $connection
     * @param null $runQuery
     * @return array
     */
    public static function createPropertyStyle($connection, $runQuery=null)
    {
        $return = array();
        try {
            if($runQuery == 'all')
            {
                $connection = DBConnection::dynamicDbConnection();
            } else {
                $table = "CREATE TABLE IF NOT EXISTS company_property_style (
                 `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                 `user_id` int(11) DEFAULT NULL,
                 `property_style`  VARCHAR(255) DEFAULT NULL,
                 `description`  text,
                 `is_default` enum('0','1') NOT NULL,
                 `status` enum('0','1') NOT NULL,
                 `deleted_at` timestamp NULL DEFAULT NULL,
                 `created_at` timestamp NULL DEFAULT NULL,
                 `updated_at` timestamp NULL DEFAULT NULL,
                 `is_editable`  TINYINT DEFAULT NULL,
                  PRIMARY KEY (`id`)
                )";
                $connection->prepare($table)->execute();
                return array('code' => 200, 'status' => 'success','message' => 'company propertySetup style table created successfully');
            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
        return $return;
    }


    /**
     *  Insert Data to Update Users
     *  Use 'add' to add column
     *  Use alter to update datatype and lenth of column
     *  Use change to change columnname
     */
    public static function update($conn, $tables, $operation, $colData)
    {
        $return = array();
        for ($i = 0; $i < count($tables); $i++) {
            if ($operation == 'add') {
                //return $colData;
                //foreach ($colData as $key => $value) {
                for($j = 0; $j < count($colData); $j++){
                    $type = $colData[$j]['col']." ".$colData[$j]['data_type']."(".$colData[$j]['length'].")";
                    $addColumn = "ALTER TABLE ".$tables[$i]." ADD ".$type;
                    $return['add_column'][] = $conn->prepare($addColumn)->execute();
                }
            }else if($operation == 'change'){
                for($j = 0; $j < count($colData); $j++){
                    $type=$colData[$j]['col']." ".$colData[$j]['new_col']." ".$colData[$j]['data_type']."(".$colData[$j]['length'].")";
                    $changeColumn = "ALTER TABLE ".$tables[$i]." Change ".$type;
                    $return['change_column'][] = $conn->prepare($changeColumn)->execute();
                    //ALTER TABLE "table_name" Change "column 1" "column 2" ["Data Type"];
                }

            }else{
                for($j = 0; $j < count($colData); $j++){
                    $type = $colData[$j]['col']." ".$colData[$j]['data_type']."(".$colData[$j]['length'].")";
                    $alterColumn = "ALTER TABLE ".$tables[$i]." MODIFY ".$type;
                    $return['alter_column'][] = $conn->prepare($alterColumn)->execute();
                }
                //ALTER TABLE table_name MODIFY COLUMN column_name datatype;
            }
        }
        return $return;
    }

    /**
     *  Insert Data from Delete Users
     */
    public static function delete($conn, $tables, $colName = array())
    {
        $return = array();
        if (!empty($colName)) {
            //ALTER TABLE table_name DROP COLUMN column_name;
            for ($i = 0; $i < count($tables); $i++) {
                foreach ($colName as $key => $value) {
                    $dropCol = "ALTER TABLE ".$tables[$i]." DROP COLUMN ".$value;
                    $return['drop_column'][] = $conn->prepare($dropCol)->execute();
                }
            }
        }else{
            for ($i = 0; $i < count($tables); $i++) {
                $dropTable = "DROP TABLE ".$tables[$i];
                $return['drop_table'][] = $conn->prepare($dropTable)->execute();
            }
            //DROP TABLE table_name;
        }
        return $return;
    }

}

$ddl = new MigrationCompanySetup();