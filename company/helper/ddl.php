<?php
/**
 * DDL class used for DDL operations
 * Created by PhpStorm.
 * User: Parvesh
 * Date: 17-April-19
 */

include("$_SERVER[DOCUMENT_ROOT]/config.php");

if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
    $url = BASE_URL."login";
    header('Location: '.$url);
}


class DDL extends DBConnection{
	//public $conn;
	function __construct()
	{
		parent::__construct();
        //$action = $_REQUEST['action'];
        //echo json_encode($this->$action());
        //$this->conn = $db;
	}

	 /**
     *  Insert Data to Company Users
     */
    public static function insert($conn)
    {
        $return = array();

        try {
        	$default_date_clock_settings = "CREATE TABLE IF NOT EXISTS `default_date_clock_settings2` (
			  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			  `user_id` int(11) NOT NULL,
			  `company_id` int(11) DEFAULT NULL,
			  `default_date_format` enum('1','2','3','4') NOT NULL DEFAULT '1',
			  `date_format` int(11) NOT NULL,
			  `default_clock_format` enum('12','24') NOT NULL DEFAULT '12',
			  `c_id` int(11) NOT NULL,
			  `created_at` timestamp NULL DEFAULT NULL,
			  `updated_at` timestamp NULL DEFAULT NULL,
			  PRIMARY KEY (`id`)
			)";

			$return['default_date_clock_settings'][] = $conn->prepare($default_date_clock_settings)->execute();
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }

        try {
        $default_settings = "CREATE TABLE IF NOT EXISTS `default_settings2` (
			  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
			  `user_id` int(11) NOT NULL,
			  `company_name` varchar(255) NOT NULL,
			  `timeout` enum('15','30','45','60','90') NOT NULL DEFAULT '15',
			  `zip_code` varchar(255) NOT NULL,
			  `address1` varchar(255) NOT NULL,
			  `city` varchar(255) DEFAULT NULL,
			  `state` varchar(255) DEFAULT NULL,
			  `country` varchar(255) DEFAULT NULL,
			  `address2` varchar(255) DEFAULT NULL,
			  `address3` varchar(255) DEFAULT NULL,
			  `phone_number` varchar(255) DEFAULT NULL,
			  `created_at` timestamp NULL DEFAULT NULL,
			  `updated_at` timestamp NULL DEFAULT NULL,
			  PRIMARY KEY (`id`)
			)";

        $return['default_settings'][] = $conn->prepare($default_settings)->execute();
    }
    catch (PDOException $e) {
        echo $e->getMessage();
    }

        try {
            $default_settings = "CREATE TABLE IF NOT EXISTS `support_team` (
			  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
			  `user_id` int(11) NOT NULL,
			  `first_name` varchar(255) NOT NULL,
			  `email` varchar(255) NOT NULL,
			  `middle_name` varchar(255) NOT NULL,
			  `last_name` varchar(255) DEFAULT NULL,
			  `maiden_name` varchar(255) DEFAULT NULL,
			  `nick_name` varchar(255) DEFAULT NULL,
			  `phone_number` varchar(255) DEFAULT NULL,
              `deleted_at` timestamp NULL DEFAULT NULL,
			  `created_at` timestamp NULL DEFAULT NULL,
			  `updated_at` timestamp NULL DEFAULT NULL,
			  PRIMARY KEY (`id`)
			)";

            $return['support_team'][] = $conn->prepare($default_settings)->execute();
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }

        
        return $return;
    }

    /**
     *  Insert Data to Update Users
     *  Use 'add' to add column
     *  Use alter to update datatype and lenth of column
     *  Use change to change columnname
     */
    public static function update($conn, $tables, $operation, $colData)
    {
    	$return = array();
		for ($i = 0; $i < count($tables); $i++) { 
	    	if ($operation == 'add') {
	    		//return $colData;
	    		//foreach ($colData as $key => $value) {
	    		for($j = 0; $j < count($colData); $j++){
	    			$type = $colData[$j]['col']." ".$colData[$j]['data_type']."(".$colData[$j]['length'].")";
	    			$addColumn = "ALTER TABLE ".$tables[$i]." ADD ".$type;
	    			$return['add_column'][] = $conn->prepare($addColumn)->execute();
	    		}
	    	}else if($operation == 'change'){
    			for($j = 0; $j < count($colData); $j++){
	    			$type=$colData[$j]['col']." ".$colData[$j]['new_col']." ".$colData[$j]['data_type']."(".$colData[$j]['length'].")";
	    			$changeColumn = "ALTER TABLE ".$tables[$i]." Change ".$type;
	    			$return['change_column'][] = $conn->prepare($changeColumn)->execute();
    				//ALTER TABLE "table_name" Change "column 1" "column 2" ["Data Type"];
	    		}

	    	}else{
	    		for($j = 0; $j < count($colData); $j++){
		    		$type = $colData[$j]['col']." ".$colData[$j]['data_type']."(".$colData[$j]['length'].")";
		    		$alterColumn = "ALTER TABLE ".$tables[$i]." MODIFY ".$type;
		    		$return['alter_column'][] = $conn->prepare($alterColumn)->execute();
	    		}
    			//ALTER TABLE table_name MODIFY COLUMN column_name datatype; 
	    	}
		}
		return $return;
    }

    /**
     *  Insert Data from Delete Users
     */
    public static function delete($conn, $tables, $colName = array())
    {
	    $return = array();
    	if (!empty($colName)) {
    		//ALTER TABLE table_name DROP COLUMN column_name;
			for ($i = 0; $i < count($tables); $i++) {
				foreach ($colName as $key => $value) {
					$dropCol = "ALTER TABLE ".$tables[$i]." DROP COLUMN ".$value;
					$return['drop_column'][] = $conn->prepare($dropCol)->execute();
				}
			}
    	}else{
    		for ($i = 0; $i < count($tables); $i++) {
				$dropTable = "DROP TABLE ".$tables[$i];
				$return['drop_table'][] = $conn->prepare($dropTable)->execute();
			}
			//DROP TABLE table_name;
    	}
    	return $return;
    }

}

$ddl = new DDL();