<?php
include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");
include_once( $_SERVER['DOCUMENT_ROOT']."/helper/globalHelper.php");
class addVendor extends DBConnection {
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }
    public function addVendorType() {
        try {
            $data = $_POST['form'];
            $custom_field = isset($_POST['custom_field']) ? $_POST['custom_field'] : [];
            $data = postArray($data, 'true');
//Required variable array
            $required_array = ['vendor_type', 'description'];
            /* Max length variable array */
            $maxlength_array = ['vendor_type' => 20, 'description' => 30];
//Number variable array
            $number_array = [];
//Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
//declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                if (!empty($id))
                    unset($data['id']);
                if (!empty($custom_field)) {
                    foreach ($custom_field as $key => $value) {
                        if ($value['data_type'] == 'date' && !empty($value['default_value']))
                            $custom_field[$key]['default_value'] = mySqlDateFormat($value['default_value'], null, $this->companyConnection);
                        if ($value['data_type'] == 'date' && !empty($value['value']))
                            $custom_field[$key]['value'] = mySqlDateFormat($value['value'], null, $this->companyConnection);
                        continue;
                    }
                }
                $insert = [];
                $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $insert['is_default'] = isset($data['is_default']) ? '1' : '0';
                $insert['is_editable'] = '1';
                $insert['vendor_type'] = $data['vendor_type'];
                $insert['description'] = $data['description'];
//                $insert['custom_field'] = (count($custom_field) > 0) ? serialize($custom_field) : null;
                $insert['status'] = '1';
                $insert['created_at'] = date('Y-m-d H:i:s');
                $insert['updated_at'] = date('Y-m-d H:i:s');
//check if custom field already exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'company_vendor_type', 'vendor_type', $insert['vendor_type'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Vendor Type already exists!');
                    }
                }
//Save Data in Company Database
                $sqlData = createSqlColVal($insert);
                $custom_data = ['is_deletable' => '0', 'is_editable' => '0'];
                $updateColumn = createSqlColValPair($custom_data);
                if (count($custom_field) > 0) {
                    foreach ($custom_field as $key => $value) {
                        updateCustomField($this->companyConnection, $updateColumn, $value['id']);
                    }
                }
                if (getRecordCount($this->companyConnection, 'company_vendor_type') == 0)
                    $insert['is_default'] = '1';
                if ($insert['is_default'] == 1) {
                    $updateDafault = createSqlColValPair(['is_default' => 0]);
                    updateDefaultField($this->companyConnection, $updateDafault, 'company_vendor_type');
                }
                $query = "INSERT INTO company_vendor_type (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($insert);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function addEthnicity() {
        try {
            $data = $_POST['form'];
            $custom_field = isset($_POST['custom_field']) ? $_POST['custom_field'] : [];
            $data = postArray($data, 'true');
//Required variable array
            $required_array = ['title'];
            /* Max length variable array */
            $maxlength_array = ['title' => 20];
//Number variable array
            $number_array = [];
//Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
//declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                if (!empty($id))
                    unset($data['id']);
                if (!empty($custom_field)) {
                    foreach ($custom_field as $key => $value) {
                        if ($value['data_type'] == 'date' && !empty($value['default_value']))
                            $custom_field[$key]['default_value'] = mySqlDateFormat($value['default_value'], null, $this->companyConnection);
                        if ($value['data_type'] == 'date' && !empty($value['value']))
                            $custom_field[$key]['value'] = mySqlDateFormat($value['value'], null, $this->companyConnection);
                        continue;
                    }
                }
                $insert = [];
                $insert['title'] = $data['title'];
//                $insert['custom_field'] = (count($custom_field) > 0) ? serialize($custom_field) : null;
                $insert['created_at'] = date('Y-m-d H:i:s');
                $insert['updated_at'] = date('Y-m-d H:i:s');
//check if custom field already exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'tenant_ethnicity', 'title', $insert['title'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Ethnicity already exists!');
                    }
                }
//Save Data in Company Database
                $sqlData = createSqlColVal($insert);
//                $updateColumn = createSqlColValPair($custom_data);
//                if (count($custom_field) > 0) {
//                    foreach ($custom_field as $key => $value) {
//                        updateCustomField($this->companyConnection, $updateColumn, $value['id']);
//                    }
//                }
                $query = "INSERT INTO tenant_ethnicity (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($insert);
                $id = $this->companyConnection->lastInsertId();

                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function fetchAllethnicity() {
        $html = '';
        $sql = "SELECT * FROM tenant_ethnicity";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html ='<option value="">Select</option>';
        foreach ($data as $d) {
            $html.= "<option value='" . $d['id'] . "'>" . $d['title'] . "</option>";
        }
        return array('data' => $html, 'status' => 'success');
    }
    public function fetchAllVendorType() {
        $html = '';
        $sql = "SELECT * FROM company_vendor_type";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html = '<option value="default">Select Vendor Type</option>';
        foreach ($data as $d) {
            if ($d['is_default'] == '1') {
                $html.= "<option value='" . $d['id'] . "' selected>" . $d['vendor_type'] . "</option>";
            } else {
                $html.= "<option value='" . $d['id'] . "'>" . $d['vendor_type'] . "</option>";
            }
        }

        return array('data' => $html, 'status' => 'success');
    }
    public function addaccountName() {
        try {
            $data = $_POST['form'];
            $data = postArray($data, 'true');
//Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
//Number variable array
            $number_array = [];
//Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
//declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
//check if custom field already exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'account_name_details', 'account_name', $data['account_name'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Account Name already exists!');
                    }
                }
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
//Save Data in Company Database
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO account_name_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function fetchAllAccountName() {
        $html = '';
        $sql = "SELECT * FROM account_name_details";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html = '<option value="default">Select</option>';
        foreach ($data as $d) {
            $html.= "<option value='" . $d['id'] . "'>" . $d['account_name'] . "</option>";
        }
        return array('data' => $html, 'status' => 'success');
    }
    public function addCredentialType() {
        try {
            $data = $_POST['form'];
            $custom_field = isset($_POST['custom_field']) ? $_POST['custom_field'] : [];
            $data = postArray($data, 'true');
//Required variable array
            $required_array = ['credential_type'];
            /* Max length variable array */
            $maxlength_array = ['credential_type' => 20];
//Number variable array
            $number_array = [];
//Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
//declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                if (!empty($id))
                    unset($data['id']);
                if (!empty($custom_field)) {
                    foreach ($custom_field as $key => $value) {
                        if ($value['data_type'] == 'date' && !empty($value['default_value']))
                            $custom_field[$key]['default_value'] = mySqlDateFormat($value['default_value'], null, $this->companyConnection);
                        if ($value['data_type'] == 'date' && !empty($value['value']))
                            $custom_field[$key]['value'] = mySqlDateFormat($value['value'], null, $this->companyConnection);
                        continue;
                    }
                }
                $insert = [];
                $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $insert['credential_type'] = $data['credential_type'];
//                $insert['custom_field'] = (count($custom_field) > 0) ? serialize($custom_field) : null;
                $insert['created_at'] = date('Y-m-d H:i:s');
                $insert['updated_at'] = date('Y-m-d H:i:s');
//check if custom field already exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'tenant_credential_type', 'credential_type', $insert['credential_type'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        echo json_encode(array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Credential Type already exists!'));
                        return;
                    }
                }
//Save Data in Company Database
                $sqlData = createSqlColVal($insert);
                if (count($custom_field) > 0) {
                    foreach ($custom_field as $key => $value) {
                        updateCustomField($this->companyConnection, $updateColumn, $value['id']);
                    }
                }
                $query = "INSERT INTO  tenant_credential_type (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($insert);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function fetchAllcredentialtypes() {
        $html = '';
        $sql = "SELECT * FROM tenant_credential_type";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html = '<option value="default">Select</option>';
        foreach ($data as $d) {
            $html.= "<option value='" . $d['id'] . "'>" . $d['credential_type'] . "</option>";
        }
        return array('data' => $html, 'status' => 'success');
    }
    public function addVendor() {
        try {
            $files = $_FILES;
            $libraryFiles = $_FILES;
            unset($libraryFiles['file_library']);
            $custom_field = isset($_POST['custom_field']) ? json_decode(stripslashes($_POST['custom_field'])) : [];
            if (!empty($custom_field)) {
                foreach ($custom_field as $key => $value) {
                    $custom_field[$key] = (array) $value;
                }
            }
            $data = $_POST;
            $img = json_decode($_POST['vendor_image']);
            $data = $_POST;
            $vendor_email= $_POST['additional_email'];
            $userData = [];
            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $subdomain = array_shift($domain);
            if((isset($_POST['company_name']) && (isset($_POST['use_company_name'])) && $_POST['use_company_name']== '1') && !empty($_POST['company_name'])){
                $userData['name'] = $_POST['company_name'];
            } else {
                if (!empty($data['nick_name']) && isset($data['nick_name'])) {
                    $userData['name'] = $_POST['nick_name'] . ' ' . $_POST['last_name'];
                } else {
                    $userData['name'] = $_POST['first_name'] . ' ' . $_POST['last_name'];
                }
            }

            $userData['phone_type']             = (isset($_POST['phone_type']))? $_POST['phone_type'][0] : '';
            $userData['carrier']                = (isset($_POST['additional_carrier']))? $_POST['additional_carrier'][0] : '';
            $userData['phone_number']           = (isset($_POST['additional_phone']))? $_POST['additional_phone'][0] : '';
            if ( $userData['phone_type'] == 5 ||  $userData['phone_type']  == 2){
                $userData['other_work_phone_extension'] = (isset($_POST['other_work_phone_extension']))? $_POST['other_work_phone_extension'][0] : '';
            } else{
                $userData['other_work_phone_extension'] = '';
            }

            $userData['country_code']           = (isset($_POST['additional_countryCode']))? $_POST['additional_countryCode'][0] : '';
            $userData['salutation'] = $_POST['salutation'];
            $userData['first_name'] = $_POST['first_name'];
            $userData['middle_name'] = $_POST['middle_name'];
            $userData['status'] = '1';
            $userData['role'] = '';
            $userData['user_type'] = '3';
            $userData['domain_name'] = $subdomain;
            $userData['record_status'] = '1';
            $userData['last_name'] = $_POST['last_name'];
            $userData['maiden_name'] = $_POST['maiden_name'];
            $userData['nick_name'] = $_POST['nick_name'];
            $userData['gender'] = (isset($_POST['gender']) && !empty($_POST['gender']) ? $_POST['gender'] : 0);
            $userData['company_name'] = (isset($_POST['company_name']) && !empty($_POST['company_name']) ? $_POST['company_name'] : '');
            $userData['ethnicity'] = $_POST['ethnicity'];
            $userData['maritial_status'] = $_POST['maritalStatus'];
            $userData['tax_payer_name'] = $_POST['tax_payer_name'];
            $userData['tax_payer_id'] = $_POST['tax_payer_id'];
            $userData['hobbies'] = (isset($_POST['hobbies']) && !empty($_POST['hobbies']) ? serialize($_POST['hobbies']) : '');
            $userData['veteran_status'] = $_POST['veteranStatus'];
            foreach($_POST['ssn_sin_id'] as $key=>$value)
            {
                if(is_null($value) || $value == '')
                    unset($_POST['ssn_sin_id'][$key]);
            }
            $userData['ssn_sin_id'] = (isset($_POST['ssn_sin_id']) && !empty($_POST['ssn_sin_id']) ? serialize($_POST['ssn_sin_id']) : null);
            $userData['created_at'] = date('Y-m-d H:i:s');
            $userData['updated_at'] = date('Y-m-d H:i:s');
            $userData['website'] = $_POST['website'];
            $userData['referral_source'] = empty($_POST['additional_referralSource']) ? NULL : $_POST['additional_referralSource'];
            $userData['address1'] = $_POST['address1'];
            $userData['address2'] = $_POST['address2'];
            $userData['address3'] = $_POST['address3'];
            $userData['address4'] = $_POST['address4'];
            $userData['zipcode'] = $_POST['zipcode'];
            $userData['country'] = $_POST['country'];
            $userData['dob']=$_POST['dob'];
            $userData['state'] = $_POST['state'];
            $userData['city'] = $_POST['city'];
            $userData['phone_number_note'] = $_POST['phone_number_note'];
            $userData['email'] = $vendor_email[0];
            $userData['if_entity_name_display'] = (isset($_POST['use_company_name']) && !empty($_POST['use_company_name'])) ? '1' : '0';
            $userData['work_phone_extension'] = (isset($_POST['work_phone_extension']))? serialize($_POST['work_phone_extension']) : '';
//            $userData['other_work_phone_extension'] =  $exe_phone_number[0];

            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
//Number variable array
            $number_array = [];
//Server side validation

            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $table="users";
                $vendor_email = $userData['email'];

                $columns = [$table . '.first_name', $table . '.last_name', $table . '.address1', $table . '.address2', $table . '.address3', $table . '.address4', $table . '.middle_name', $table . '.dob', $table . '.ssn_sin_id'];

                if (isset($_POST['dob']) && $_POST['dob'] != "") {
                    $data['dob'] = mySqlDateFormat($_POST['dob'],null,$this->companyConnection);
                }else{
                    $data['dob'] ='';
                }
                $where = [['table' => 'users', 'column' => 'first_name', 'condition' => '=', 'value' => $_POST['first_name']],['table' => 'users', 'column' => 'last_name', 'condition' => '=', 'value' => $_POST['last_name']],['table' => 'users', 'column' => 'middle_name', 'condition' => '=', 'value' => $_POST['middle_name']],['table' => 'users', 'column' => 'address1', 'condition' => '=', 'value' => $_POST['address1']],['table' => 'users', 'column' => 'address2', 'condition' => '=', 'value' => $_POST['address2']],['table' => 'users', 'column' => 'address3', 'condition' => '=', 'value' => $_POST['address3']],['table' => 'users', 'column' => 'address4', 'condition' => '=', 'value' => $_POST['address4']],['table' => 'users', 'column' => 'phone_number', 'condition' => '=', 'value' => $_POST['additional_phone'][0],['table' => 'users', 'column' => 'dob', 'condition' => '=', 'value' => $data['dob']]]];
                $duplicate_record = checkDuplicateRecord($this->companyConnection, 'users',$columns,$where,$userData['ssn_sin_id'], '');
                if ($duplicate_record['is_exists'] == 1) {
                    return array('code' => 400, 'status' => 'error', 'message' => 'Record already exists!');
                }

//                $getdetailVendor="SELECT * FROM users WHERE first_name='".$userData['first_name']."' AND user_type='3'";
//                $datadetailVendor = $this->companyConnection->query($getdetailVendor)->fetch();
//                if (!empty($datadetailVendor)) {
//                    return array('code' => 500, 'status' => 'error', 'message' => 'Vendor Name already exists!');
//                }

                $getdetailVendorEmail="SELECT * FROM users WHERE email='".$vendor_email[0]."' AND  user_type ='3'";
                $datadetailVendorEmail = $this->companyConnection->query($getdetailVendorEmail)->fetch();
                if (!empty($datadetailVendorEmail)) {
                    return array('code' => 500, 'status' => 'error','message' => 'Email already exists!');
                }
                if (!empty($custom_field)) {
                    foreach ($custom_field as $key => $value) {
                        if ($value['data_type'] == 'date' && !empty($value['default_value']))
                            $custom_field[$key]['default_value'] = mySqlDateFormat($value['default_value'], null, $this->companyConnection);
                        if ($value['data_type'] == 'date' && !empty($value['value']))
                            $custom_field[$key]['value'] = mySqlDateFormat($value['value'], null, $this->companyConnection);
                        continue;
                    }
                }
//declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                if (!empty($id)) unset($data['id']);
//Save Data in Company Database
                $userData['dob'] = (!empty($userData['dob'])) ? mySqlDateFormat($userData['dob'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection) : null;
                $userData['custom_fields'] = (count($custom_field) > 0) ? serialize($custom_field) : null;
//Save Data in Company Database
                $custom_data = ['is_deletable' => '0', 'is_editable' => '0'];
                $updateColumn = createSqlColValPair($custom_data);
                if (count($custom_field) > 0) {
                    foreach ($custom_field as $key => $value) {
                        updateCustomField($this->companyConnection, $updateColumn, $value['id']);
                    }
                }
                $sqlData = createSqlColVal($userData);
                $query = "INSERT INTO  users (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($userData);
                $id = $this->companyConnection->lastInsertId();
                updateUsername($id,$this->companyConnection);
                $addVendorInfo = $this->addVendorInfo($id);
                $addPhoneInfo = $this->addPhoneInfo($id);
                $addEmergencyContact = $this->addEmergencyContact($id);
                $addCredentialsControl = $this->addCredentialsControl($id);
                $addNotes = $this->addNotes($id);
                $libraryData = $this->addFileLibrary($id, $libraryFiles);

                if(!empty($data['ccard_number'])) {
                    $VendorPaymentStep1 = $this->VendorPaymentStep1($id, $data['ccard_number'], $data['ccvv'], $data['cexpiry_year'], $data['cexpiry_month']);

                }
                if(!empty($_POST['hidden_acc_detail'][0])){
                    $detail_data=explode(',',$_POST['hidden_acc_detail'][0]);
//                    echo "<pre>";print_r($detail_data);die;

                        $birthdate= mySqlDateFormat($detail_data[14],null,$this->companyConnection);
                        $dateSplit=explode('-',$birthdate);
                        $yearDob=$dateSplit[0];
                        $monthDob=$dateSplit[1];
                        $dayDob=$dateSplit[2];
                    $stripe_account_array = ["email"=>$detail_data[9],"first_name" => $detail_data[10], "last_name" => $detail_data[11],
                        "city" => $detail_data[4],"state" => $detail_data[8], "line1" => $detail_data[5], "line2" => $detail_data[6],
                        'postal_code' => $detail_data[7],'day' => $dayDob, 'month' => $monthDob, 'year' => $yearDob,
                        'ssn_last_4' => $detail_data[13],'phone' => $detail_data[12],'country' => 'US', 'currency' => 'USD',
                        'routing_number' => $detail_data[3],'account_number' => $detail_data[2],
                        'document_front'=> '','document_back'=> '',
                        'website_url'=> SUBDOMAIN_URL.'/','product_description'=> '','mcc'=>'6513','support_email'=>$detail_data[9]];

                    $VendorPaymentStep2=$this->VendorPaymentStep2($stripe_account_array,$id);


                }

//                $this->sendMail($id);
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function updateVendor() {
        try {

            $vendor_edit_id=$_POST['vendor_edit_id'];
            $files = $_FILES;
            $libraryFiles = $_FILES;
            unset($libraryFiles['file_library']);
            $custom_field = isset($_POST['custom_field']) ? json_decode(stripslashes($_POST['custom_field'])) : [];
            if (!empty($custom_field)) {
                foreach ($custom_field as $key => $value) {
                    $custom_field[$key] = (array) $value;
                }
            }
            $data = $_POST;
            $img = json_decode($_POST['vendor_image']);
            $data = $_POST;
            $vendor_email= $_POST['additional_email'];
            $userData = [];
            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $subdomain = array_shift($domain);
//            if((isset($_POST['company_name']) && (isset($_POST['use_company_name'])) && $_POST['use_company_name']== '1') && !empty($_POST['company_name'])){
//                $userData['name'] = $_POST['company_name'];
//            } else {
            if (!empty($data['nick_name']) && isset($data['nick_name'])) {
                $userData['name'] = $_POST['nick_name'] . ' ' . $_POST['last_name'];
            } else {
                $userData['name'] = $_POST['first_name'] . ' ' . $_POST['last_name'];
            }
//            }



            $userData['phone_type']             = (isset($_POST['phone_type']))? $_POST['phone_type'][0] : '';
            $userData['carrier']                = (isset($_POST['additional_carrier']))? $_POST['additional_carrier'][0] : '';
            $userData['phone_number']           = (isset($_POST['phoneNumber']))? $_POST['phoneNumber'][0] : '';
            if ( $userData['phone_type'] == 5 ||  $userData['phone_type']  == 2){
                $userData['other_work_phone_extension'] = (isset($_POST['other_work_phone_extension']))? $_POST['other_work_phone_extension'][0] : '';
            } else{
                $userData['other_work_phone_extension'] = '';
            }

            $userData['country_code']           = (isset($_POST['additional_countryCode']))? $_POST['additional_countryCode'][0] : '';

            $userData['salutation'] = $_POST['salutation'];
            $userData['first_name'] = $_POST['first_name'];
            $userData['middle_name'] = $_POST['middle_name'];
            $userData['status'] = '1';
            $userData['role'] = '';
            $userData['user_type'] = '3';
            $userData['domain_name'] = $subdomain;
            $userData['record_status'] = '1';
            $userData['last_name'] = $_POST['last_name'];
            $userData['maiden_name'] = $_POST['maiden_name'];
            $userData['nick_name'] = $_POST['nick_name'];
            $userData['gender'] =(isset($_POST['gender']) && !empty($_POST['gender']) ? $_POST['gender'] : 0);
            $userData['dob'] = $_POST['dob'];
            $userData['company_name'] = (isset($_POST['company_name']) && !empty($_POST['company_name']) ? $_POST['company_name'] : '');
            $userData['ethnicity'] = $_POST['ethnicity'];
            $userData['maritial_status'] = $_POST['maritalStatus'];
            $userData['hobbies'] = (isset($_POST['hobbies']) && !empty($_POST['hobbies']) ? serialize($_POST['hobbies']) : '');
            $userData['veteran_status'] = $_POST['veteranStatus'];
            foreach($_POST['ssn_sin_id'] as $key=>$value)
            {
                if(is_null($value) || $value == '')
                    unset($_POST['ssn_sin_id'][$key]);
            }
            $userData['ssn_sin_id'] = (isset($_POST['ssn_sin_id']) && !empty($_POST['ssn_sin_id']) ? serialize($_POST['ssn_sin_id']) : null);
            $userData['created_at'] = date('Y-m-d H:i:s');
            $userData['updated_at'] = date('Y-m-d H:i:s');
            $userData['website'] = $_POST['website'];
            $userData['referral_source'] = empty($_POST['additional_referralSource']) ? NULL : $_POST['additional_referralSource'];
            $userData['address1'] = $_POST['address1'];
            $userData['address2'] = $_POST['address2'];
            $userData['address3'] = $_POST['address3'];
            $userData['address4'] = $_POST['address4'];
            $userData['zipcode'] = $_POST['zipcode'];
            $userData['country'] = $_POST['country'];
            $userData['state'] = $_POST['state'];
            $userData['city'] = $_POST['city'];
            $userData['phone_number_note'] = $_POST['phone_number_note'];
            $userData['email'] = $vendor_email[0];
            $userData['if_entity_name_display'] = (isset($_POST['use_company_name']) && !empty($_POST['use_company_name'])) ? '1' : '0';
//            $userData['other_work_phone_extension'] =  $exe_phone_number[0];

            $userData['work_phone_extension'] = (isset($_POST['work_phone_extension']))? serialize($_POST['work_phone_extension']) : '';
          //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
//Number variable array
            $number_array = [];
//Server side validation


            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $table="users";
                $vendor_email = $userData['email'];

                $columns = [$table . '.first_name', $table . '.last_name', $table . '.address1', $table . '.address2', $table . '.address3', $table . '.address4', $table . '.middle_name', $table . '.dob', $table . '.ssn_sin_id'];

                if (isset($_POST['dob']) && $_POST['dob'] != "") {
                    $data['dob'] = mySqlDateFormat($_POST['dob'],null,$this->companyConnection);
                }else{
                    $data['dob'] ='';
                }
                $where = [['table' => 'users', 'column' => 'first_name', 'condition' => '=', 'value' => $_POST['first_name']],['table' => 'users', 'column' => 'last_name', 'condition' => '=', 'value' => $_POST['last_name']],['table' => 'users', 'column' => 'middle_name', 'condition' => '=', 'value' => $_POST['middle_name']],['table' => 'users', 'column' => 'address1', 'condition' => '=', 'value' => $_POST['address1']],['table' => 'users', 'column' => 'address2', 'condition' => '=', 'value' => $_POST['address2']],['table' => 'users', 'column' => 'address3', 'condition' => '=', 'value' => $_POST['address3']],['table' => 'users', 'column' => 'address4', 'condition' => '=', 'value' => $_POST['address4']],['table' => 'users', 'column' => 'phone_number', 'condition' => '=', 'value' => $userData['phone_number'],['table' => 'users', 'column' => 'dob', 'condition' => '=', 'value' => $data['dob']]]];
                $duplicate_record = checkDuplicateRecord($this->companyConnection, 'users',$columns,$where,$userData['ssn_sin_id'], $vendor_edit_id);
                if ($duplicate_record['is_exists'] == 1) {
                    return array('code' => 400, 'status' => 'error', 'message' => 'Record already exists!');
                }

//                $getdetailVendor="SELECT * FROM users WHERE id !='".$vendor_edit_id."' AND first_name='".$userData['first_name']."' AND user_type='3'";
//                $datadetailVendor = $this->companyConnection->query($getdetailVendor)->fetch();
//                if (!empty($datadetailVendor)) {
//                    return array('code' => 500, 'status' => 'error', 'message' => 'Vendor Name already exists!');
//                }

                $getdetailVendorEmail="SELECT * FROM users WHERE id !='".$vendor_edit_id."' AND email='".$vendor_email[0]."' AND user_type='3'";
                $datadetailVendorEmail = $this->companyConnection->query($getdetailVendorEmail)->fetch();
                if (!empty($datadetailVendorEmail)) {
                    return array('code' => 500, 'status' => 'error','message' => 'Email already exists!');
                }
                if (!empty($custom_field)) {
                    foreach ($custom_field as $key => $value) {
                        if ($value['data_type'] == 'date' && !empty($value['default_value']))
                            $custom_field[$key]['default_value'] = mySqlDateFormat($value['default_value'], null, $this->companyConnection);
                        if ($value['data_type'] == 'date' && !empty($value['value']))
                            $custom_field[$key]['value'] = mySqlDateFormat($value['value'], null, $this->companyConnection);
                        continue;
                    }
                }
//                dd($_SESSION[SESSION_DOMAIN]['cuser_id']);
//declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                if (!empty($id)) unset($data['id']);
//Save Data in Company Database
                $userData['dob'] = (!empty($userData['dob'])) ? mySqlDateFormat($userData['dob'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection) : null;
                $userData['custom_fields'] = (count($custom_field) > 0) ? serialize($custom_field) : null;
//Save Data in Company Database
                $custom_data = ['is_deletable' => '0', 'is_editable' => '0'];
                $updateColumn = createSqlColValPair($custom_data);
                if (count($custom_field) > 0) {
                    foreach ($custom_field as $key => $value) {
                        updateCustomField($this->companyConnection, $updateColumn, $value['id']);
                    }
                }
                $sqlData = createSqlUpdateCase($userData);
                $query =  "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=" .$vendor_edit_id;
//                echo"<pre>";print_r($userData['ssn_sin_id']);
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($sqlData['data']);
                $addVendorInfo = $this->addVendorInfo($vendor_edit_id);
                $updatePhoneInfo = $this->updatePhoneInfo($vendor_edit_id);
                $updateEmergencyContant = $this->updateEmergencyContant($vendor_edit_id);
                $updateCredentialControl = $this->updateCredentialControl($vendor_edit_id);
                $addNotes = $this->addNotes($vendor_edit_id);
                $libraryData = $this->addFileLibrary($vendor_edit_id, $libraryFiles);
                updateUsername($vendor_edit_id,$this->companyConnection);
                return array('code' => 200, 'status' => 'success', 'data' => $stmt, 'message' => 'Record updated successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function addVendorInfo($id) {
        try {

            $vendorData = [];
            $vendorData['vendor_rate'] = $_POST['vendor_rate'];
            $vendorData['vendor_random_id'] = $_POST['vendor_random_id'];
            if(isset($_POST['vendor_type_id']) && $_POST['vendor_type_id'] != 'default'){

                $vendorData['vendor_type_id'] = $_POST['vendor_type_id'];
            }else{
                $vendorData['vendor_type_id']= null;
            }
            $vendorData['vendor_id'] = $id;
            $vendorData['image'] = json_decode($_POST['vendor_image']);

            $vendorData['is_pmc'] = (isset($_POST['is_pmc']) && !empty($_POST['is_pmc'])) ? '1' : '0';
            $vendorData['use_company_name'] = (isset($_POST['use_company_name']) && !empty($_POST['use_company_name'])) ? '1' : '0';
            $vendorData['created_at'] = date('Y-m-d H:i:s');
            $vendorData['updated_at'] = date('Y-m-d H:i:s');
            $vendorData['email'] =(isset($_POST['additional_email']))? serialize($_POST['additional_email']) : '';
//Save Data in Company Database
            $getdetailVendor="SELECT * FROM vendor_additional_detail WHERE vendor_id=" . $id . "";
            $datadetailVendor = $this->companyConnection->query($getdetailVendor)->fetch();
            if(empty($datadetailVendor)) {
                $vendorData['account_name'] = $_POST['account_name'];
                $vendorData['bank_account_number'] = $_POST['bank_account_number'];
                $vendorData['routing_number'] = $_POST['routing_number'];
                $vendorData['eligible_for_1099'] = $_POST['eligible_for_1099'];
                $vendorData['status'] = '1';
                $vendorData['name_on_check'] = $_POST['name_on_check'];
                $vendorData['comment'] = $_POST['comment'];
                $vendorData['consolidate_checks'] = $_POST['consolidate_checks'];
                $vendorData['order_limit'] = $_POST['order_limit'];
                $vendorData['default_GL'] = $_POST['default_GL'];
                $sqlData = createSqlColVal($vendorData);
                $query = "INSERT INTO  vendor_additional_detail (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($vendorData);
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }else{
                $sqlData = createSqlUpdateCase($vendorData);
                $query = "UPDATE vendor_additional_detail SET " . $sqlData['columnsValuesPair'] . " where vendor_id='$id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($sqlData['data']);
                return array('code' => 200, 'status' => 'success', 'message' => 'Record updated Successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function addPhoneInfo($id) {
        $phoneType = $_POST['additional_phoneType'];
        $i = 0;
        foreach ($phoneType as $phone) {
            $data['user_id'] = $id;
            $data['parent_id'] = 0;
            $data['phone_type'] = empty($phone) ? '0' : $phone;
            $data['carrier'] = empty($_POST['additional_carrier'][$i]) ? '0' : $_POST['additional_carrier'][$i];
            $data['phone_number'] = $_POST['additional_phone'][$i];
            $data['work_phone_extension'] =  empty($_POST['work_phone_extension'][$i]) ? '0' : $_POST['work_phone_extension'][$i];
//            $data['other_work_phone_extension'] =  empty($_POST['other_work_phone_extension'][$i]) ? '0' : $_POST['other_work_phone_extension'][$i];
            if ( $data['phone_type'] == 5 ||  $data['phone_type']  == 2){
                $data['other_work_phone_extension'] = (isset($_POST['other_work_phone_extension']))? $_POST['other_work_phone_extension'][$i] : '';
            } else{
                $data['other_work_phone_extension'] = '';
            }
            $data['country_code'] = $_POST['additional_countryCode'][$i];
            $data['user_type'] = 0; //0 value for main tenant
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['phone_type'] = (!empty($data['phone_type']) && $data['phone_type'] != 'default') ? $data['phone_type'] : null;
            $data['carrier'] = (!empty($data['carrier']) && $data['carrier'] != 'default') ? $data['carrier'] : null;
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO tenant_phone(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            $i++;
        }
        return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'tenant_phone');
    }
    public function addFileLibrary($id, $fileData) {
        try {
            $files = $fileData;
            unset($files['tenant_image']);
            $user_id = $id;
            if (!empty($files)) {
                $domain = getDomain();
                $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
                foreach ($files as $key => $value) {
                    $file_name = $value['name'];
                    $file_tmp = $value['tmp_name'];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $name = time() . uniqid(rand());
                    $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
//Check if the directory already exists.
                    if (!is_dir(ROOT_URL . '/company/' . $path)) {
//Directory does not exist, so lets create it.
                        mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                    }
                    move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                    $data = [];
                    $data['user_id'] = $user_id;
                    $data['name'] = $file_name;
//                    $data['file_size'] = isa_convert_bytes_to_specified($value['size'], 'K').'kb';
                    $data['path'] = $path . '/' . $name . '.' . $ext;
                    $data['extension'] = $ext;
//                    $data['marketing_site'] = '0';
//                    $data['file_type'] = '2';
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO tenant_file_library (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                }
                return array('code' => 200, 'status' => 'success', 'message' => 'Documents uploaded successfully.');
            }
        } catch (PDOException $e) {
            dd($e);
            return array('code' => 500, 'status' => 'failed', 'message' => $e->getMessage());
        }
    }
    public function fetchAllPhonetypes() {
        $html = '';
        $sql = "SELECT * FROM  phone_type";
        $data = $this->companyConnection->query($sql)->fetchAll();
        foreach ($data as $d) {
            $html.= "<option value='" . $d['id'] . "'" . (($d['type'] == 'Mobile') ? "selected" : "") . ">" . $d['type'] . "</option>";
        }
        return array('data' => $html, 'status' => 'success');
    }
    public function fetchAllCarrier() {
        $html = '';
        $sql = "SELECT * FROM  carrier";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html = '<option value="default">Select</option>';
        foreach ($data as $d) {
            $html.= "<option value='" . $d['id'] . "'>" . $d['carrier'] . "</option>";
        }
        return array('data' => $html, 'status' => 'success');
    }
    public function addEmergencyContact($id) {
        try {
            $i = 0;
            foreach ($_POST['emergency_contact_name'] as $emergencycontactname) {
                $emergencydata=[];
                $emergencydata['user_id'] = $id;
                $emergencydata['parent_id'] = 0;
                $emergencydata['emergency_contact_name'] = $_POST['emergency_contact_name'][$i];
                $emergencydata['emergency_relation'] = $_POST['emergency_relation'][$i];
                $emergencydata['emergency_country_code'] = $_POST['emergency_country'][$i];
                $emergencydata['emergency_phone_number'] = $_POST['emergency_phone'][$i];
                $emergencydata['emergency_email'] = $_POST['emergency_email'][$i];
                $emergencydata['other_relation'] =  $_POST['other_relation'][$i];
                $sqlData = createSqlColVal($emergencydata);
                $query = "INSERT INTO emergency_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($emergencydata);
                $i++;
            }
            return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'emergency_details');
        } catch (PDOException $e) {
            dd($e);
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'emergency_details');
            printErrorLog($e->getMessage());
        }
    }
    public function addCredentialsControl($id) {
        try {
            $i = 0;
            foreach ($_POST['credentialName'] as $credentialName) {
                $acquire_date = $_POST['acquireDate'][$i];
                $expireDate = $_POST['expirationDate'][$i];
                $data['user_id'] = $id;
                $data['credential_name'] = $_POST['credentialName'][$i];
                $data['credential_type'] = !empty($_POST['credentialType'][$i]) ? $_POST['credentialType'][$i] : NULL;
                $data['acquire_date'] = !empty($acquire_date) ? mySqlDateFormat($acquire_date, null, $this->companyConnection) : NULL;
                $data['expire_date'] = !empty($expireDate) ? mySqlDateFormat($expireDate, null, $this->companyConnection) : NULL;
                $data['notice_period'] = $_POST['noticePeriod'][$i];
                /*  return  $data; */
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_credential(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($query);
                $stmt1->execute($data);
                $i++;
            }
            return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'tenant_credentials');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_credentials');
            printErrorLog($e->getMessage());
        }
    }
    public function addNotes($id) {
        try {
            if (isset($_POST['note'])) {
                $chargeNote = $_POST['note'];
                $i = 0;
                foreach ($chargeNote as $note) {
                    if ($note != "") {
                        $data['user_id'] = $id;
                        $data['note'] = $note;
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO tenant_chargenote(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                    }
                    $i++;
                }
            }
            return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'tenant_chargenote');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_credentials');
            printErrorLog($e->getMessage());
        }
    }
    public function addvendorNotes() {
        try {
            if($_POST['type']=="add")
            {
                $data['user_id'] = $_POST['id'];
                $data['note'] = $_POST['note'];
                $tenantchargenotes=(isset($_POST['tenantchargenotes']) && !empty($_POST['tenantchargenotes'])) ? $_POST['tenantchargenotes'] : null;

                if (empty($tenantchargenotes)) {
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO tenant_chargenote(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    return array('code' => 200,'status' => 'success', 'message' => 'Record added successfully.', 'table' => 'tenant_chargenote');
                }else{
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE tenant_chargenote SET " . $sqlData['columnsValuesPair'] . " where id=$tenantchargenotes";
                    $stmt = $this->companyConnection->prepare($query);

                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated Successfully');
                }
            }

        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_chargenote');
            printErrorLog($e->getMessage());
        }
    }
    public function fetchChartAccounts() {
        $html = '';
        $sql = "SELECT * FROM company_chart_of_accounts";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html = '<option value="default">Select</option>';
        foreach ($data as $d) {
            if ($d['is_default'] == '1') {
                $html.= "<option value='" . $d['id'] . "' selected>" . $d['account_name'] . '-' . $d['account_code'] . "</option>";
            } else {
                $html.= "<option value='" . $d['id'] . "'>" . $d['account_name'] . '-' . $d['account_code'] . "</option>";
            }
        }
        return array('data' => $html, 'status' => 'success');
    }
    public function getAllAccountType() {
        try {
            $query = $this->companyConnection->query("SELECT * FROM company_account_type WHERE status = '1'");
            $data = $query->fetchAll();
            return array('status' => 'success', 'code' => 200, 'data' => $data);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
            printErrorLog($e->getMessage());
        }
    }
    /**
     * download Sample
     */
    public function exportSampleExcel() {
        $file_url = COMPANY_SITE_URL . "/excel/VendorSample.xlsx";
        if (ob_get_contents()) ob_end_clean();
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\"");
        readfile($file_url); // do the double-download-dance (dirty but worky)
        die();
    }
    /**
     * Import function
     */
    public function importExcel() {
        try {
            if (isset($_FILES['file'])) {
                if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
                    $allowedExtensions = array("xls", "xlsx", "csv");
                    $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                    if (in_array($ext, $allowedExtensions)) {
                        $file_size = $_FILES['file']['size'] / 1024;
                        if ($file_size < 3000) {
                            $file = "uploads/" . $_FILES['file']['name'];
                            $isUploaded = copy($_FILES['file']['tmp_name'], $file);
                            if ($isUploaded) {
                                include(SUPERADMIN_DIRECTORY_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
                                try {
//Load the excel(.xls/.xlsx/ .csv) file
                                    $objPHPExcel = PHPExcel_IOFactory::load($file);
                                } catch (Exception $e) {
                                    $error_message = 'Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage();
                                    return ['status' => 'failed', 'code' => 503, 'data' => $error_message];
                                    printErrorLog($e->getMessage());
                                }
//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
                                $sheet = $objPHPExcel->getSheet(0);
//It returns the highest number of rows
                                $total_rows = $sheet->getHighestRow();
//                                dd($total_rows);
//It returns the first element of 'A1'
                                $first_row_header = $objPHPExcel->getActiveSheet()->getCell('A1')->getValue();
//It returns the highest number of columns
                                $total_columns = $sheet->getHighestDataColumn();
                                $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
//                            $extra_columns = ",`status`,`created_at`, `updated_at`";
//                            $status = 1;
//                            $extra_values = ",'" . $status . "','" . date("Y-m-d h:i:s") . "','" . date("Y-m-d h:i:s") . "'";
//                            $query = "insert into `general_property` (`property_name`,`legal_name`,`portfolio_id`,`zipcode`,`country`,`state`,`city`,`address1`,`address2`,`property_price`,`attach_groups`,`property_type`,`property_style`,`property_subtype`,`property_year`,`property_squareFootage`,`no_of_buildings`,`no_of_units`,`amenities`,`description`$extra_columns) VALUES ";
//Loop through each row of the worksheet
//                            dd($total_rows);
                                for ($row = 2; $row <= $total_rows; $row++) {
//Read a single row of data and store it as a array.
//This line of code selects range of the cells like A1:D1
                                    $single_row = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row, NULL, TRUE, FALSE);
//Creating a dynamic query based on the rows from the excel file
//$data = $this->companyConnection->query("SELECT property_name FROM general_property")->fetchAll();
//                                dd($single_row[0]);
                                    if ($first_row_header == 'Salutation') {
                                        $continueloop = true;
                                        foreach ($single_row[0] as $key => $value) {
                                            if (empty($value)) continue;
                                            switch ($key) {
                                                case "0":
                                                    $single_row[0][$key] = getSalutationId($value);
                                                    break;
                                                case "6":
                                                    $vendorData = getSingleRecord($this->companyConnection, ['column' => 'vendor_type', 'value' => $value], 'company_vendor_type');
                                                    if ($vendorData['code'] == 200) {
                                                        $single_row[0][$key] = $vendorData['data']['id'];
                                                    } else {
                                                        $insert = [];
                                                        $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                                        $insert['vendor_type'] = $value;
                                                        $insert['is_default'] = '0';
                                                        $insert['is_editable'] = '1';
                                                        $insert['status'] = '1';
                                                        $insert['created_at'] = date('Y-m-d H:i:s');
                                                        $insert['updated_at'] = date('Y-m-d H:i:s');
                                                        $sqlData = createSqlColVal($insert);
                                                        $query = "INSERT INTO company_vendor_type (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                                        $stmt = $this->companyConnection->prepare($query);
                                                        $stmt->execute($insert);
                                                        $single_row[0][$key] = $this->companyConnection->lastInsertId();
                                                    }
                                                    break;
                                                case "13":
                                                    $phoneType = getSingleRecord($this->companyConnection, ['column' => 'type', 'value' => $value], 'phone_type');
                                                    if ($phoneType['code'] == 200) {
                                                        $single_row[0][$key] = $phoneType['data']['id'];
                                                    } else {
                                                        return ['status' => 'failed', 'code' => 503, 'message' => 'Phone type not found'];
                                                    }
                                                    break;
                                                case "14":
                                                    $carrierData = getSingleRecord($this->companyConnection, ['column' => 'carrier', 'value' => $value], 'carrier');
                                                    if ($carrierData['code'] == 200) {
                                                        $single_row[0][$key] = $carrierData['data']['id'];
                                                    } else {
                                                        return ['status' => 'failed', 'code' => 503, 'message' => 'Carrier not found'];
                                                    }
                                                    break;
                                                case "16":
                                                    $emailData = getSingleRecord($this->companyConnection, ['column' => 'email', 'value' => $value], 'users');
                                                    if ($emailData['code'] == 200) {
                                                        $continueloop = false;
                                                    }
                                                    break;
                                                case "23":
                                                    $single_row[0][$key] = ($value == 'Yes') ? '1' : '2';
                                                    break;
                                                default:
                                                    continue;
                                            }
                                        }
                                        if ($continueloop) {
                                            $domain = explode('.', $_SERVER['HTTP_HOST']);
                                            $subdomain = array_shift($domain);
                                            $users = [];
                                            $users['salutation'] = $single_row[0][0];
                                            $users['name'] = $single_row[0][1] . ' ' . $single_row[0][3];
                                            $users['first_name'] = $single_row[0][1];
                                            $users['middle_name'] = $single_row[0][2];
                                            $users['status'] = '1';
                                            $users['role'] = '';
                                            $users['user_type'] = '3';
                                            $users['domain_name'] = $subdomain;
                                            $users['record_status'] = '1';
                                            $users['last_name'] = $single_row[0][3];
                                            $users['company_name'] = $single_row[0][4];
                                            $users['zipcode'] = $single_row[0][7];
                                            $users['city'] = $single_row[0][10];
                                            $users['state'] = $single_row[0][9];
                                            $users['country'] = $single_row[0][8];
                                            $users['address1'] = $single_row[0][11];
                                            $users['address2'] = $single_row[0][12];
                                            $users['phone_type'] = $single_row[0][13];
                                            $users['carrier'] = $single_row[0][14];
                                            $users['phone_number'] = $single_row[0][15];
                                            $users['email'] = $single_row[0][16];
                                            $users['tax_payer_name'] = $single_row[0][17];
                                            $users['tax_payer_id'] = $single_row[0][18];
                                            $users['created_at'] = date('Y-m-d H:i:s');
                                            $users['updated_at'] = date('Y-m-d H:i:s');
                                            $sqlData = createSqlColVal($users);
                                            $query = "INSERT INTO users (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                            $stmt = $this->companyConnection->prepare($query);
                                            $executed_query = $stmt->execute($users);
                                            $vendor_id = $this->companyConnection->lastInsertId();
                                            $vendorInfo = [];
                                            $vendorInfo['vendor_random_id'] =$this->randomString();
                                            $vendorInfo['vendor_id'] = $vendor_id;
                                            $vendorInfo['status'] = '1';
                                            $vendorInfo['name_on_check'] = $single_row[0][5];
                                            $vendorInfo['vendor_type_id'] = $single_row[0][6];
                                            $vendorInfo['consolidate_checks'] = $single_row[0][22];
                                            $vendorInfo['order_limit'] = $single_row[0][24];
                                            $vendorAdditionalEmail = [];
                                            $vendorAdditionalEmail[0] = $single_row[0][16];
                                            $vendorInfo['email'] = !empty($single_row[0][16])?serialize($vendorAdditionalEmail):null;
                                            $vendorInfo['created_at'] = date('Y-m-d H:i:s');
                                            $vendorInfo['updated_at'] = date('Y-m-d H:i:s');
                                            $vendorSqlData = createSqlColVal($vendorInfo);
                                            $query2 = "INSERT INTO vendor_additional_detail (" . $vendorSqlData['columns'] . ") VALUES (" . $vendorSqlData['columnsValues'] . ")";
                                            $stmt2 = $this->companyConnection->prepare($query2);
                                            $executed_query2 = $stmt2->execute($vendorInfo);
                                            $phoneInfo = [];
                                            $phoneInfo['user_id'] = $vendor_id;
                                            $phoneInfo['parent_id'] = 0;
                                            $phoneInfo['user_type'] = '3';
                                            $phoneInfo['phone_type'] = $single_row[0][13];
                                            $phoneInfo['carrier'] = $single_row[0][14];
                                            $phoneInfo['phone_number'] = $single_row[0][15];
                                            $phoneInfo['created_at'] = date('Y-m-d H:i:s');
                                            $phoneInfo['updated_at'] = date('Y-m-d H:i:s');
                                            $phoneSqlData = createSqlColVal($phoneInfo);
                                            $query3 = "INSERT INTO tenant_phone (" . $phoneSqlData['columns'] . ") VALUES (" . $phoneSqlData['columnsValues'] . ")";
                                            $stmt3 = $this->companyConnection->prepare($query3);
                                            $executed_query3 = $stmt3->execute($phoneInfo);
                                            $note = [];
                                            $note['user_id'] = $vendor_id;
                                            $note['record_status'] = '1';
                                            $note['notes'] = $single_row[0][25];
                                            $note['created_at'] = date('Y-m-d H:i:s');
                                            $note['updated_at'] = date('Y-m-d H:i:s');
                                            $noteSqlData = createSqlColVal($note);
                                            $query4 = "INSERT INTO tenant_notes (" . $noteSqlData['columns'] . ") VALUES (" . $noteSqlData['columnsValues'] . ")";
                                            $stmt4 = $this->companyConnection->prepare($query4);
                                            $executed_query4 = $stmt4->execute($note);
                                        }
                                    } else {
                                        return ['status' => 'failed', 'code' => 503, 'message' => 'Please select valid file.'];
                                    }
                                }
                                return ['status' => 'success', 'code' => 200, 'message' => 'Data imported successfully.'];
                            } else {
                                return ['status' => 'failed', 'code' => 503, 'message' => 'File not uploaded!'];
//echo '<span class="msg">File not uploaded!</span>';
                            }
                        } else {
                            return ['status' => 'failed', 'code' => 503, 'message' => 'Maximum file size should not cross 3 MB on size!'];
//echo '<span class="msg">Maximum file size should not cross 50 KB on size!</span>';
                        }
                    } else {
                        return ['status' => 'failed', 'code' => 503, 'message' => 'Only .xls/.xlsx/.csv files are accepted.'];
//echo '<span class="msg">This type of file not allowed!</span>';
                    }
                } else {
                    return ['status' => 'failed', 'code' => 503, 'message' => 'Select an excel file first!'];
//echo '<span class="msg">Select an excel file first!</span>';
                }
            }
        } catch (Exception $exception) {
            dd($exception);
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
            printErrorLog($e->getMessage());
        }
    }
    /**
     * function to archive property
     * @return array
     */
    public function archiveVendor() {
        try {
            $id = $_POST['id'];
            $data = [];
            $data['status'] = '2';
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE vendor_additional_detail SET " . $sqlData['columnsValuesPair'] . " WHERE id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($sqlData['data']);
            return array('status' => 'success', 'code' => 200, 'message' => 'Record Archived Successfully.');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    /**
     * function to archive property
     * @return array
     */
    public function activateVendor() {
        try {
            $id = $_POST['id'];
            $data = [];
            $data['status'] = '1';
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE vendor_additional_detail SET " . $sqlData['columnsValuesPair'] . " WHERE id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($sqlData['data']);
            return array('status' => 'success', 'code' => 200, 'message' => 'Record Activated Successfully.');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    /**
     * function to resign property
     * @return array
     */
    public function resignVendor() {
        try {
            $id = $_POST['id'];
            $data = [];
            $data['status'] = '3';
            $data['reason_for_leaving'] = (isset($_POST['reasonOwner']) && !empty($_POST['reasonOwner'])) ? $_POST['reasonOwner'] : null;
            $data['reason_note'] = (isset($_POST['reasonNotes']) && !empty($_POST['reasonNotes'])) ? $_POST['reasonNotes'] : null;
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE vendor_additional_detail SET " . $sqlData['columnsValuesPair'] . " WHERE id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($sqlData['data']);
            return array('status' => 'success', 'code' => 200, 'message' => 'Record Resign Successfully.');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    /**
     * function to resign property
     * @return array
     */
    public function deleteVendor() {
        try {
            $id = $_POST['id'];
            $vendor_id = $_POST['vendor_id'];
            $query = "DELETE FROM vendor_additional_detail WHERE id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            $query1 = "DELETE FROM users WHERE id=" . $vendor_id;
            $stmt1 = $this->companyConnection->prepare($query1);
            $stmt1->execute();
            return array('status' => 'success', 'code' => 200, 'message' => 'Record Deleted Successfully.');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    public function sendMail($userId = null) {
        try {
            $server_name = 'https://' . $_SERVER['HTTP_HOST'];
            $userId = (isset($userId) ? $userId : $_REQUEST['id']);
            $user_details = $this->companyConnection->query("SELECT * FROM users where id=" . $userId)->fetch();
            $user_name = userName($user_details['id'], $this->companyConnection);
            $body = getEmailTemplateData($this->companyConnection, 'newVendorWelcome_Key');
            $body = str_replace("{Firstname}", $user_name, $body);
            $body = str_replace("{Username}", $user_details['email'], $body);
            $body = str_replace("{Url}", $server_name, $body);
            $body = str_replace("{website}", $server_name, $body);
            $logo_url = SITE_URL . 'company/images/logo.png';
            $image = '<img alt="Apexlink" src="' . $logo_url . '" width="150" height="50">';
            $body = str_replace("{CompanyLogo}", $image, $body);
            $request['action'] = 'SendMailPhp';
            $request['to[]'] = $user_details['email'];
            $request['subject'] = 'Welcome in Apexlink!';
            $request['message'] = $body;
            $request['portal'] = '1';
            curlRequest($request);
            return ['status' => 'success', 'code' => 200, 'data' => $request, 'message' => 'Email send successfully'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
    /**
     * function to resign property
     * @return array
     */
    public function rateVendor() {
        try {
            $id = $_POST['id'];
            $data = [];
            $data['rating'] = $_POST['rating'];
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE vendor_additional_detail SET " . $sqlData['columnsValuesPair'] . " WHERE vendor_id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($sqlData['data']);
            return array('status' => 'success', 'code' => 200, 'message' => 'Vendor rated Successfully.');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    /**
     *  Export Excel
     */
    public function exportExcel() {
        include(ROOT_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
        $objPHPExcel = new PHPExcel();
        $table = $_REQUEST['table'];
        $status = $_REQUEST['status'];
        $status = $_REQUEST['vendor'];
        $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
//        $query1 = "SELECT * FROM $table";
        $columns = [$table . '.*', 'users.*', 'company_vendor_type.vendor_type'];
        $where = [['table' => $table, 'column' => 'deleted_at', 'condition' => 'IS NULL', 'value' => '']];
        $joins = [
            [
                'type' => 'LEFT',
                'table' => $table,
                'column' => 'vendor_id',
                'primary' => 'id',
                'on_table' => 'users',
                'as' => 'users'
            ],
            [
                'type' => 'LEFT',
                'table' => $table,
                'column' => 'vendor_type_id',
                'primary' => 'id',
                'on_table' => 'company_vendor_type',
                'as' => 'company_vendor_type'
            ]
        ];
        $orderBy = ['column'=>'first_name','sort'=>'ASC'];
        $query = selectQueryBuilder($columns, $table, $joins, $where, $orderBy);
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
//Set header with temp array
        $tmparray = array('Salutation', 'FirstName', 'MiddleName', 'LastName', 'CompanyName', 'NameonCheck', 'VendorType', 'Zipcode', 'Country', 'State', 'City', 'Address1', 'Address2', 'PhoneType', 'Carrier', 'PhoneNumber', 'Email', 'TaxPayerName', 'TaxPayerID', 'DiscountPercentage', 'DaystoPay', 'NetDaystoPay', 'ConsolidateCheck', 'DefaultGLExpanseAccount', 'OrderLimit', 'Notes');
//take new main array and set header array in it.
        $sheet = array($tmparray);
        while ($vendor_detail_data = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $tmparray = array();
            $Salutation =  (getSalutation($vendor_detail_data['salutation']));
            array_push($tmparray, $Salutation);
            $FirstName = $vendor_detail_data['first_name'];
            array_push($tmparray, $FirstName);
            $MiddleName = $vendor_detail_data['middle_name'];
            array_push($tmparray, $MiddleName);
            $LastName = $vendor_detail_data['last_name'];
            array_push($tmparray, $LastName);
            $CompanyName = $vendor_detail_data['company_name'];
            array_push($tmparray, $CompanyName);
            $NameonCheck = $vendor_detail_data['name_on_check'];
            array_push($tmparray, $NameonCheck);
            $VendorType = $vendor_detail_data['vendor_type'];
            array_push($tmparray, $VendorType);
            $Zipcode = $vendor_detail_data['zipcode'];
            array_push($tmparray, $Zipcode);
            $Country = $vendor_detail_data['country'];
            array_push($tmparray, $Country);
            $State = $vendor_detail_data['state'];
            array_push($tmparray, $State);
            $City = $vendor_detail_data['city'];
            array_push($tmparray, $City);
            $Address1 = $vendor_detail_data['address1'];
            array_push($tmparray, $Address1);
            $Address2 = $vendor_detail_data['address2'];
            array_push($tmparray, $Address2);
            $PhoneType = null;
            array_push($tmparray, $PhoneType);
            $Carrier = null;
            array_push($tmparray, $Carrier);
            $PhoneNumber = null;
            array_push($tmparray, $PhoneNumber);
            $Email = $vendor_detail_data['email'];
            array_push($tmparray, $Email);
            $TaxPayerName = $vendor_detail_data['tax_payer_name'];
            array_push($tmparray, $TaxPayerName);
            $TaxPayerID = $vendor_detail_data['tax_payer_id'];
            array_push($tmparray, $TaxPayerID);
            $DiscountPercentage = '';
            array_push($tmparray, $DiscountPercentage);
            $DaystoPay = '';
            array_push($tmparray, $DaystoPay);
            $NetDaystoPay = '';
            array_push($tmparray, $NetDaystoPay);
            $ConsolidateCheck = $vendor_detail_data['consolidate_checks'] == '1' ? 'Yes' : 'No';
            array_push($tmparray, $ConsolidateCheck);
            $DefaultGLExpanseAccount = '';
            array_push($tmparray, $DefaultGLExpanseAccount);
            $OrderLimit = $vendor_detail_data['order_limit'];
            array_push($tmparray, $OrderLimit);
            $Notes = '';
            array_push($tmparray, $Notes);
            array_push($sheet, $tmparray);
        }
        $worksheet = $objPHPExcel->getActiveSheet();
        foreach ($sheet as $row => $columns) {
            foreach ($columns as $column => $data) {
                $worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
            }
        }
//make first row bold
        $objPHPExcel->getActiveSheet()->getStyle("A1:B1")->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        if (ob_get_contents()) ob_end_clean();
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="VendorData.xlsx"');
        $objWriter->save('php://output');
        die();
    }
    public function getVendorDetail() {
        $id = (isset($_POST['vendor_id']) && !empty($_POST['vendor_id'])) ? $_POST['vendor_id'] : null;
        $table = 'users';
        $columns = ['users.name','users.country_code','users.phone_number', 'users.first_name','users.dob as vendorbirthdate', 'users.last_name', 'users.salutation', 'users.middle_name', 'users.nick_name', 'users.maiden_name', 'users.company_name as compname', 'users.zipcode as zipcode', 'users.city as citys', 'users.state as states', 'users.country', 'users.address1 as address123', 'users.address2 as address2edit', 'users.address3 as address3edit', 'users.address4 as address4edit', 'users.maritial_status', 'users.hobbies', 'users.veteran_status', 'users.ssn_sin_id',
            'users.email as vendoremailView','users.work_phone_extension','users.phone_number_note', 'users.gender', 'users.website', 'users.referral_source as additional_referralSource', 'users.tax_payer_name', 'users.tax_payer_id', 'users.ethnicity as ethnicity_options', 'users.dob as birth_date', 'users.hobbies',
            'vendor_additional_detail.image as tenant_image_id', 'vendor_additional_detail.vendor_rate', 'vendor_additional_detail.vendor_random_id', 'vendor_additional_detail.vendor_type_id',
            'vendor_additional_detail.routing_number', 'vendor_additional_detail.eligible_for_1099', 'vendor_additional_detail.comment', 'vendor_additional_detail.consolidate_checks',
            'vendor_additional_detail.use_company_name','vendor_additional_detail.order_limit', 'vendor_additional_detail.default_GL', 'vendor_additional_detail.is_pmc', 'vendor_additional_detail.name_on_check','vendor_additional_detail.email as vendorEmail','tenant_phone.phone_type as phone_typeResult','tenant_phone.country_code as country_codeRes','emergency_details.emergency_relation as emergency_relation'
        ];
        $joins = [[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'vendor_id',
            'on_table' => 'vendor_additional_detail',
            'as' => 'vendor_additional_detail'
        ],[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_phone',
            'as' => 'tenant_phone'
        ],[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'emergency_details',
            'as' => 'emergency_details'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'id',
            'condition' => '=',
            'value' => $id
        ]];
        $query = selectQueryBuilder($columns, $table, $joins, $where);
        $dataVendors = $this->companyConnection->query($query)->fetch();
        $dataphone = array();
        $sqlphone = "SELECT * FROM  tenant_phone WHERE `user_id` = $id";
        $dataphone = $this->companyConnection->query($sqlphone)->fetchAll();
        $fullname = $dataVendors['first_name'] . ' ' . $dataVendors['last_name'];
        $hobbies = unserialize($dataVendors['hobbies']);
        $work_phone_extension =unserialize($dataVendors['work_phone_extension']);
        $randomnumner = $dataVendors['vendor_random_id'];
        $getPhoneInfo = $this->getPhoneInfo($id);
        $getEmergencyInfo = $this->getEmergencyInfo($id);
        $getCredentialInfo = $this->getCredentialInfo($id);
        $data['gn_ssn_data'] = $this->getSsnData(unserialize($dataVendors['ssn_sin_id']));
        $vendorDob=dateFormatUser($dataVendors['vendorbirthdate'],null,$this->companyConnection);
        $data['generalEmail'] = $this->getGeneralEmails(unserialize($dataVendors['vendorEmail']));
        return array('code' => 200, 'status' => 'success', 'dataVendors' => $dataVendors, 'dataphone' => $dataphone, 'fullname' => $fullname, 'randomnumner' => $randomnumner, 'message' => 'Record retrieved Successfully.', 'phoneInfo' => $getPhoneInfo,'EmergencyInfo'=>$getEmergencyInfo,'CredentialInfo'=>$getCredentialInfo,'gnEmail'=>$data['generalEmail'],'vendorbirthdate'=>$vendorDob,'gn_ssn'=>$data['gn_ssn_data'],'hobbies'=>$hobbies,'work_phone_extension'=>$work_phone_extension);
    }

    public function getAllVendors(){
        $allVendors = [];
        $sql = "SELECT u.id,u.name,u.email,u.address1,u.address2,u.address3,u.address4,u.city,u.state,u.zipcode,u.salutation,av.id as aid, cvt.vendor_type, tp.phone_number FROM 
        users u JOIN vendor_additional_detail av ON u.id = av.vendor_id JOIN tenant_phone tp ON u.id = tp.user_id JOIN company_vendor_type cvt ON av.vendor_type_id = cvt.id 
        WHERE u.user_type='3'";
        $allVendors = $this->companyConnection->query($sql)->fetchAll();
        if (!empty($allVendors)){
            return ['code' => 200, 'status' => 'success', 'data' => $allVendors];
        }
        return ['code' => 501, 'status' => 'error', 'data' => ''];
    }

    public function getPhoneInfo($vendor_id) {
        $getPhoneInfo = $this->companyConnection->query("SELECT * FROM tenant_phone WHERE parent_id=0 and user_id ='" . $vendor_id . "'")->fetchAll();
        $getPhoneType = $this->companyConnection->query("SELECT * FROM phone_type")->fetchAll();
        $getCarrierInfo = $this->companyConnection->query("SELECT * FROM carrier")->fetchAll();
        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();
        $html = "";
        if (empty($getPhoneInfo)) {
            $html .= '<div class="col-sm-12 primary-tenant-phone-row2"><div class="row">
                    <div class="col-sm-3 col-md-3 ">
                    <label>Phone Type</label>
                    <select class="form-control" name="additional_phoneType[]" id="phone_type_id">
                    <option value="">Select</option>
                    <option value="1" selected>mobile</option>
                    <option value="2">work</option>
                    <option value="3">Fax</option>
                    <option value="4">Home</option>
                    <option value="5">Other</option>
                    </select>
                    <span class="term_planErr error red-star"></span>
                    </div>
                    <div class="col-sm-3 col-md-3 ">
                    <label>Phone Number <em class="red-star">*</em></label>
                    <input class="form-control capsOn add-input phone_format" type="text"
                    name="phoneNumber[]" placeholder="123-456-7891" maxlength="12">
                    <a class="add-icon phoneclassDivplus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                    <a class="add-icon phoneclassDivminus" style="display: none;" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                    <span class="ffirst_nameErr error red-star"></span>
                    <span class="term_planErr error red-star"></span>
                    </div>
                    <div class="work_extension_div" style="display: none;">
                    <div class="col-sm-1 col-md-1">
                        <label>Extension</label>
                        <input class="form-control number_only_for_extension" maxlength="15" type="text" id="other_work_phone_extension" name="other_work_phone_extension[]" placeholder="Eg: + 161">
                        <span class="other_work_phone_extensionErr error red-star"></span>
                    </div>
                    </div>
                    <div class="col-sm-3 col-md-3 ">
                    <label>Carrier <em class="red-star">*</em></label>
                    <select class="form-control" name="carrier[]">';
            foreach ($getCarrierInfo as $carrierInfo) {
                $html .= "<option value=" . $carrierInfo['id'] . ">" . $carrierInfo['carrier'] . "</option>";
            }
            $html .= '</select>
                    <span class="term_planErr error red-star"></span>
                    </div>
                    <div class="col-sm-3 col-md-3 countycodediv">
                    <label>Country Code</label>
                    <select class="form-control" name="countryCode[]">';
            foreach ($countryCode as $code) {
                $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] .'('. $code['code'] .')'. "</option>";
            }
            $html .= '</select>
                    <span class="term_planErr error red-star"></span>
                    </div>
                    
                    <div class="clearfix"></div>
                    </div></div>';
        } else {
            $i = 0;
            foreach ($getPhoneInfo as $phoneInfo) {
                $html .= '<div class="col-sm-12 primary-tenant-phone-row2"><div class="row">
                        <div class="col-sm-3">
                        <label>Phone Type</label>
                        <select class="form-control" name="additional_phoneType[]" id="phone_type_id">';
                foreach ($getPhoneType as $phoneType) {
                    if ($phoneType['id'] == $phoneInfo['phone_type']) {
                        $selected = "selected=selected";
                    } else {
                        $selected = "";
                    }
                    $html .= "<option value=" . $phoneType['id'] . " " . $selected . ">" . $phoneType['type'] . "</option>";
                }

                if ($phoneInfo['phone_type'] == '2' || $phoneInfo['phone_type'] == '5') {
                    $display = 'style="display: block;"';
                } else {
                    $display = 'style="display: none;"';
                }

                $html.= '</select>
                        </div>';

                $html.='<div class="col-sm-3">
                <label>Phone Number</label>
                <input class="form-control  phone_format" maxlength="12" type="text" name="phoneNumber[]" value="' . $phoneInfo['phone_number'] . '">';

                $html.='</div><div class="work_extension_div" '.$display.'>
                        <div class="col-sm-1 col-md-1">
                            <label>Extension</label>
                            <input class="form-control number_only_for_extension" maxlength="15" type="text" id="other_work_phone_extension" name="other_work_phone_extension[]" placeholder="Eg: + 161" value="'.$phoneInfo['other_work_phone_extension'].'">
                            <span class="other_work_phone_extensionErr error red-star"></span>
                        </div>
                       
                        </div>
                        
                        
                         
                        <div class="col-sm-3">
                        <label>Carrier <em class="red-star">*</em></label>
                        <select class="form-control" name="carrier[]">';
                foreach ($getCarrierInfo as $carrierInfo) {
                    if ($carrierInfo['id'] == $phoneInfo['carrier']) {
                        $selected = "selected=selected";
                    } else {
                        $selected = "";
                    }
                    $html .= "<option value=" . $carrierInfo['id'] . " " . $selected . ">" . $carrierInfo['carrier'] . "</option>";
                }
                $html.= '</select></div>
                            <div class="col-sm-3">
                            <label>Country Code</label>
                            <select class="form-control add-input" name="countryCode[]">';
                foreach ($countryCode as $code) {
                    if ($code['id'] == $phoneInfo['country_code']) {
                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "' selected>" . $code['name'] .'('. $code['code'] .')'. "</option>";
                    } else {
                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] . '('. $code['code'] .')'."</option>";
                    }
                }


                $html.= '</select>';
                 if ($i == 0) {
                    $html .= '<a class="add-icon phoneclassDivplus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>';
                    $html .='<a class="add-icon phoneclassDivminus" style="display: none;" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                }
                if ($i != 0) {
                    $html .='<a class="add-icon phoneclassDivminus"  href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                }


                $html.='</div>';

                $html.= '</div></div>';
                $i++;
            }
        }
        return $html;
    }
    public function getEmergencyInfo($vendor_id) {
        $getEmergencyInfo = $this->companyConnection->query("SELECT * FROM emergency_details WHERE user_id ='" . $vendor_id . "' and parent_id=0")->fetchAll();
        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();
        $html = "";
        if (empty($getEmergencyInfo)) {
            $html .= '<div class="additional-tenant-emergency-contact"><div class="col-sm-12"><div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-3">
                        <label>Emergency Contact Name</label>
                        <input class="form-control capsOn capital" type="text"
                        id="emergency" name="emergency_contact_name[]" placeholder="Emergency Contact Name">
                        <span class="flast_nameErr error red-star"></span>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-3">
                        <label>Relation</label>
                        <select class="form-control" id="relationship"
                        name="emergency_relation[]">
                        <option value="">Select</option>
                        <option value="1">Daughter</option>
                        <option value="2">Father</option>
                        <option value="3">Friend</option>
                        <option value="4">Mother</option>
                        <option value="5">Owner</option>
                        <option value="6">Partner</option>
                        <option value="7">Son</option>
                        <option value="8">Spouse</option>
                        <option value="9">Other</option>
                        </select>
                        <span class="term_planErr error red-star"></span>
                        </div>
                        <div class="col-sm-2" id="otherRelationDiv" style="display: none;">
                            <label>Other Relation</label>
                            <input placeholder="Other Relation" class="form-control capital" type="text" id="emergency_other_relation" name="other_relation[]" maxlength="50">
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-3">
                        <label>Country Code</label>
                        <select class="form-control emergency_additional_countryCode emergency_country" name="emergency_country[]">
                        <option value="">Select</option>';
            foreach ($countryCode as $code) {

                $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] .'('. $code['code'].')'. "</option>";
            }
            $html .='</select>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-3">
                        <label>Phone Number</label>
                        <input class="form-control capsOn phone_format" type="text"
                               id="phoneNumber"
                               name="emergency_phone[]" placeholder="123-456-7891">
                        <span class="ffirst_nameErr error red-star"></span>
                        </div>
                        <div class="col-sm-3">
                        <label>Email</label>
                        <input class="form-control add-input " type="text"
                        name="emergency_email[]" placeholder="Eg: user@domain.com">
                        <span class="flast_nameErr error red-star"></span>
                        <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle additional-add-emergency-contant" aria-hidden="true"></i></a>                                                                                        
                        <a class="add-icon additional-remove-emergency-contant" style="display:none;" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                        </div>
                        </div></div></div>';
        } else {
            $i = 0;
            foreach ($getEmergencyInfo as $emergencyInfo) {
                $html .= '<div class="additional-tenant-emergency-contact"><div class="col-sm-12"><div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-3">
                        <label>Emergency Contact Name</label>
                        <input class="form-control capsOn capital" type="text"  value="'.$emergencyInfo['emergency_contact_name'].'"
                        id="emergency" name="emergency_contact_name[]" placeholder="Emergency Contact Name">
                        <span class="flast_nameErr error red-star"></span>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-3">
                        <label>Relation</label>
                        <select class="form-control" id="relationship"
                        name="emergency_relation[]">';
                $relationArr = array("" => "Select","1" => "Daughter","2" => "Father","3" => "Friend","4" => "Mother","5" => "Owner","6" => "Partner","7" => "Son","8" => "Spouse","9" => "Other");
                foreach ($relationArr as $key => $value) {
                    if ($key == $emergencyInfo['emergency_relation']) {
                        $html .= '<option value="'.$key.'" selected>'.$value.'</option>';
                    }else{
                        $html .= '<option value="'.$key.'">'.$value.'</option>';
                    }
                }
                $html .='</select>
                        <span class="term_planErr error red-star"></span>
                        </div>
                          <div class="col-sm-2" id="otherRelationDiv" style="display: none;">
                        <label>Other Relation</label>
                        <input placeholder="Other Relation" class="form-control capital" type="text" id="emergency_other_relation" name="other_relation[]" maxlength="50">
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-3">
                        <label>Country Code</label>
                        <select class="form-control emergency_additional_countryCode emergency_country" name="emergency_country[]">';

                foreach ($countryCode as $code) {

                    if ($code['id'] == $emergencyInfo['emergency_country_code']) {
                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "' selected>" . $code['name'] .'('. $code['code'].')'. "</option>";
                    } else {
                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] .'('. $code['code'].')'. "</option>";
                    }
                }


                $html .='</select>
                        </div>
                      
                        <div class="col-xs-12 col-sm-4 col-md-3">
                        <label>Phone Number</label>
                        <input class="form-control capsOn phone_format" type="text"
                               id="phoneNumber"
                               name="emergency_phone[]" placeholder="123-456-7891"></div><div class="col-sm-3">
                        <label>Email</label>
                        <input class="form-control add-input " type="text" value="'.$emergencyInfo['emergency_email'].'"
                        name="emergency_email[]" placeholder="Eg: user@domain.com">
                        <span class="flast_nameErr error red-star"></span>  ';

                if($i !=0){
                    $html .='<a class="add-icon additional-remove-emergency-contant"  href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';

                }else{
                    $html .= '<a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle additional-add-emergency-contant" aria-hidden="true"></i></a>';
                    $html .='<a class="add-icon additional-remove-emergency-contant" style="display:none;" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';

                }

                $html .='</div>
                        </div></div></div>';

                $i++;
            }
        }
        return $html;
    }
    public function getCredentialInfo($vendor_id) {
        $credential_notice_period =  $_SESSION[SESSION_DOMAIN]['default_notice_period'];
        $getCredentialInfo = $this->companyConnection->query("SELECT * FROM tenant_credential WHERE user_id ='" . $vendor_id . "'")->fetchAll();
        $getCredentialType = $this->companyConnection->query("SELECT * FROM tenant_credential_type")->fetchAll();
        $today = date('Y-m-d H:i:s');
        $todayDate = dateFormatUser($today,null,$this->companyConnection);

        $html = "";
        if (empty($getCredentialInfo)) {
            $credentialTypeData = '';
            foreach($getCredentialType as $credentialType)
            {
                $credentialTypeData .= "<option value='0'>Select</option><option value=".$credentialType['id']." >".$credentialType['credential_type']."</option>";
            }

            $html .= '<div class="form-outer vendor-credential-control-class" id="vendor-credential-control-div">
                                                                        <div class="col-sm-2">
                                                                            <label>Credential Name</label>
                                                                            <input class="form-control capsOn capital" maxlength="20"  type="text" name="credentialName[]" placeholder="Credential Name"/>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Credential Type <a class="pop-add-icon CredentialTypeplus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control credential-options" name="credentialType[]">'.$credentialTypeData.'</select>
                                                                            <div class="add-popup NewCredentialType" id=\'NewCredentialType\' style="wtdth:120%;">
                                                                                <h4>Add Credential Type</h4>
                                                                                <div class="add-popup-body">
                                                                                    <div class="form-outer">
                                                                                        <div class="col-sm-12">
                                                                                            <label>Credential Type<em class="red-star">*</em></label>
                                                                                            <input class="form-control capital customValidatecredentialname" data_required="true" data_max="150" type="text" name="@credential_type" placeholder="Eg: License"/>
                                                                                            <span class="customError required"></span>
                                                                                        </div>
                                                                                        <div class="btn-outer text-right">
                                                                                            <input type="submit" class="blue-btn" id="NewCredentialTypeSave" value="Save">
                                                                                            <button type="button"  class="clear-btn clearFormReset clearNewCredentialType" >Clear</button>
                                                                                            <input type="button"  class="grey-btn cancelPopup" value=\'Cancel\' />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Acquire Date </label>
                                                                            <input class="form-control acquireDateclass" type="text"  name="acquireDate[]" id="acquireDateid"/>
                                                                        </div>
                                                                        <div class="col-sm-2 commonendclass">
                                                                            <label>Expiration Date </label>
                                                                            <input class="form-control expirationDateclass" type="text" name="expirationDate[]" id="expirationDateid"/>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Notice Period </label>
                                                                            <select class="form-control add-input" id="notice_period"
                                                                                    name="noticePeriod[]">
                                                                                <option value="">Select</option>
                                                                                    <option value="4"'. (($credential_notice_period == 4) ? 'selected' : '') . '>5 day</option>
																					<option value="1"' . (($credential_notice_period == 1) ? 'selected' : '') .'>1 Month</option>
																					<option value="2"' . (($credential_notice_period == 2) ? 'selected' : '') .'>2 Month</option>
																					<option value="3"' . (($credential_notice_period == 3) ? 'selected' : '') .'>3 Month</option>
                                                                            </select>
                                                                            <a class="add-icon credential-control-plus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                            <a class="add-icon credential-control-minus" style="display: none;" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>

                                                                        </div>
                                                                    </div>';
        } else {
            $k = 0;
            foreach ($getCredentialInfo as $CredentialInfo) {
                $acquire_date = dateFormatUser($CredentialInfo['acquire_date'],null,$this->companyConnection);
                $expire_date = dateFormatUser($CredentialInfo['expire_date'],null,$this->companyConnection);
                $noticeperioddata=$CredentialInfo['notice_period'];

                $html .= '<div class="vendor-credential-control-class" id="vendor-credential-control-div"><div class="col-sm-12"><div class="row"><div class="col-sm-2">
                            <label>Credential Name</label>
                            <input class="form-control capital capsOn" maxlength="20"  type="text" name="credentialName[]" placeholder="Credential Name" value="'.$CredentialInfo['credential_name'].'"/>
                        </div>
                        <div class="col-sm-2">
                            <label>Credential Type <a class="pop-add-icon CredentialTypeplus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                            <select class="form-control credential-options" name="credentialType[]">';
                foreach($getCredentialType as $credentialType)

                {

                    if($credentialType['id']==$CredentialInfo['credential_type'])
                    {
                        $selected123 = "selected=selected";
                    }
                    else
                    {
                        $selected123 = "";
                    }

                    $html .= "<option value='0'>Select</option><option value=".$credentialType['id']." ".$selected123.">".$credentialType['credential_type']."</option>";
                }
                $html .= '</select>
                            <div class="add-popup NewCredentialType" id=\'NewCredentialType\' style="width:120%;">
                                <h4>Add Credential Type</h4>
                                <div class="add-popup-body">
                                    <div class="form-outer">
                                        <div class="col-sm-12">
                                            <label>Credential Type<em class="red-star">*</em></label>
                                            <input class="form-control capital customValidatecredentialname" data_required="true" data_max="150" type="text" name="@credential_type" placeholder="Eg: License"/>
                                            <span class="customError required"></span>
                                        </div>
                                        <div class="btn-outer text-right">
                                            <input type="submit" class="blue-btn" id="NewCredentialTypeSave" value="Save">
                                            <button type="button" class="clear-btn clearFormReset clearNewCredentialType">Clear</button>
                                            <input type="button" class="grey-btn cancelPopup" value="Cancel"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>Acquire Date </label>
                            <input class="form-control acquireDateclass" type="text"  value="'.$acquire_date.'" name="acquireDate[]" id="acquireDateid"/>
                        </div>
                        <div class="col-sm-2 commonendclass">
                            <label>Expiration Date </label>
                            <input class="form-control expirationDateclass" type="text" value="'.$expire_date.'" name="expirationDate[]" id="expirationDateid"/>
                        </div>
                        <div class="col-sm-2">
                            <label>Notice Period </label>
                            <select class="form-control add-input" id="notice_period"
                                    name="noticePeriod[]">
                            <option value="">Select</option>
                            <option value="4"'. (($noticeperioddata == 4) ? 'selected' : '') . '>5 day</option>
                            <option value="1"' . (($noticeperioddata == 1) ? 'selected' : '') .'>1 Month</option>
                            <option value="2"' . (($noticeperioddata == 2) ? 'selected' : '') .'>2 Month</option>
                            <option value="3"' . (($noticeperioddata == 3) ? 'selected' : '') .'>3 Month</option></select>';
                if($k !=0)
                {
                    $html .= '<a class="add-icon credential-control-minus"><i class="fa fa-times-circle red-star" aria-hidden="true "></i></a>';
                }
                else
                {
                    $html .= '<a class="add-icon credential-control-plus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>';
                    $html .= '<a class="add-icon credential-control-minus" style="display:none"><i class="fa fa-times-circle red-star" aria-hidden="true "></i></a>';
                }
                $html .= '</div></div></div></div>';


                $k++;
            }
        }
        return $html;
    }
    public function updateEmergencyContant($vendor_id)
    {
        $count=$this->companyConnection->prepare("DELETE FROM emergency_details WHERE  parent_id=0 and user_id=$vendor_id");
        $count->execute();
        try{
            $i=0;
            foreach($_POST['emergency_contact_name'] as $credentialName)
            {
                $data['user_id'] = $vendor_id;
                $data['parent_id'] = 0;
                $data['emergency_contact_name'] = $_POST['emergency_contact_name'][$i];
                $data['emergency_relation'] = $_POST['emergency_relation'][$i];
                $data['emergency_country_code'] = $_POST['emergency_country'][$i];
                $data['emergency_phone_number'] = $_POST['emergency_phone'][$i];
                $data['emergency_email'] = $_POST['emergency_email'][$i];

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO emergency_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $i++;
            }
            return array('status' => 'success',  'message' => 'Data Retrieved Successfully.','table'=>'emergency_details');
        }catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'emergency_details');
            printErrorLog($e->getMessage());
        }

    }
    public function updateCredentialControl($vendor_id)
    {
        $count=$this->companyConnection->prepare("DELETE FROM tenant_credential WHERE  user_id=$vendor_id");
        $count->execute();
        try{
            if(isset($_POST['credentialName']))
            {
                $i=0;
                foreach($_POST['credentialName'] as $credentialName)
                {
                    $acquire_date = $_POST['acquireDate'][$i];
                    $expireDate = $_POST['expirationDate'][$i];
                    $data['user_id'] = $vendor_id;
                    $data['credential_name'] = $_POST['credentialName'][$i];
                    $data['credential_type'] = $_POST['credentialType'][$i];
                    $data['acquire_date'] = (isset($acquire_date) && !empty($acquire_date))?mySqlDateFormat($acquire_date,null,$this->companyConnection):null;
                    $data['expire_date'] = (isset($expireDate) && !empty($expireDate))?mySqlDateFormat($expireDate,null,$this->companyConnection):null;
                    $data['notice_period'] = $_POST['noticePeriod'][$i];
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO tenant_credential(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt1 = $this->companyConnection->prepare($query);
                    $stmt1->execute($data);
                    $i++;
                }
            }
            return array('status' => 'success',  'message' => 'Data Retrieved Successfully.','table'=>'tenant_credentials');
        }
        catch (PDOException $e) {
//            print_r($e);
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'tenant_credentials');
            printErrorLog($e->getMessage());
        }

    }
    public function deleteVendorNotes() {
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "DELETE FROM tenant_chargenote WHERE id=$id";
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute();
            return array ('status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    public function updatePhoneInfo($vendor_id)
    {
        try{
            $phoneType = $_POST['additional_phoneType'];
            $count=$this->companyConnection->prepare("DELETE FROM tenant_phone WHERE  parent_id=0 and user_id=$vendor_id");
            $count->execute();
            $i=0;
            foreach($phoneType as $phone)
            {

                $data['user_id'] = $vendor_id;
                $data['parent_id'] = 0;
                $data['phone_type'] = $phone;
                $data['carrier'] = $_POST['carrier'][$i];
                $data['phone_number'] = $_POST['phoneNumber'][$i];
                $data['country_code'] = $_POST['countryCode'][$i];
                $data['user_type'] = 0; //0 value for main tenant

                $data['work_phone_extension'] =  empty($_POST['work_phone_extension'][$i]) ? '0' : $_POST['work_phone_extension'][$i];
                $data['other_work_phone_extension'] =  empty($_POST['other_work_phone_extension'][$i]) ? '0' : $_POST['other_work_phone_extension'][$i];


                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_phone(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $i++;
            }
            return array('status' => 'success',  'message' => 'Data Retrieved Successfully.','table'=>'tenant_phone');
        }catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','table' => "tenant_details");
            printErrorLog($e->getMessage());
        }

    }
    /**
     * fuinction to retrive flag data
     * @return array
     */
    public function getVendorNotes() {
        try {
            $id = $_POST['id'];
            $sql = "SELECT * FROM tenant_chargenote WHERE id=" . $id . "";
            $data = $this->companyConnection->query($sql)->fetch();
            if (!empty($data) && count($data) > 0) {
                $data['date'] = (!empty($data['date'])) ? dateFormatUser($data['date'], null, $this->companyConnection) : '';
                $flag_data = array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');
            } else {
                $flag_data = array('code' => 400, 'status' => 'error', 'message' => 'No Record Found!');
            }
            return $flag_data;
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }
    public function getSsnData($ssn_id)
    {
        $n=0;
        $html = '';
        if(empty($ssn_id)){
            $html='<div class="col-sm-2 ssn-sin-mainDiv" id="ssn-sin-mainid">
                        <label>SSN/SIN/ID</label>
                        <input class="form-control add-input ssn_sin_id capital" type="text" name="ssn_sin_id[]" placeholder="SSN/SIN/ID"/>
                        <a class="add-icon ssn-sin-plus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                        <a  class="add-icon ssn-sin-minus" style="display: none" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                    </div>';
        }else {
            foreach ($ssn_id as $emailid => $Eval) {
                $html .= '<div class="col-sm-2 ssn-sin-mainDiv" id="ssn-sin-mainid">
                        <label>SSN/SIN/ID</label>
                        <input class="form-control add-input ssn_sin_id capital" type="text" value="'.$Eval.'" name="ssn_sin_id[]" placeholder="SSN/SIN/ID"/>';
                if($n != 0){
                    $html .='<a  class="add-icon ssn-sin-minus" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                }else{
                    $html .='<a class="add-icon ssn-sin-plus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>';
                    $html .='<a  class="add-icon ssn-sin-minus" style="display: none" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                }

                $html .= '</div>';
                $n++;
            }
        }

        $html .='</div>';

        return $html;

    }
    public function getGeneralEmails($idds)
    {
        $m=0;
        $html = '';
        if(empty($idds)){
            $html='<div class="col-sm-3 emailaddressmainDiv" id="emailaddressmainDivId">
                        <label>Email Address </label>
                        <input name="additional_email[]" placeholder="Eg: user@domain.com" maxlength="150" class="form-control add-input" type="text" />
                        <a class="add-icon email-address-plus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                        <a class="add-icon email-address-minus" style="display:none" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                    </div>';
        }else {
            foreach ($idds as $emailid => $Eval) {
                $html .= '<div class="col-sm-3 emailaddressmainDiv" id="emailaddressmainDivId">
                            <label>Email Address </label>
                            <input name="additional_email[]" value="'.$Eval.'" placeholder="Eg: user@domain.com" maxlength="150" class="form-control add-input" type="text" />';
                if($m != 0){
                    $html .='<a class="add-icon email-address-minus" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                }else{
                    $html .='<a class="add-icon email-address-plus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>';
                    $html .='<a class="add-icon email-address-minus" style="display:none" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>';
                }

                $html .= '</div>';
                $m++;
            }
        }

        $html .='</div>';

        return $html;
    }
    public function randomString($length = 6) {
        $str = "";
        $length = 5;
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));

        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        $str1 = $str. mt_rand(0, 9);
        return $str1;
    }
    //for add vendor

    public function VendorPaymentStep1($id,$card,$cvv,$year,$month)
    {

        try{
            $customer_id = $this->createCustomerNew($id);
            $request['customer_id']=$customer_id;
            $createdData=createCard($card,$cvv,$year,$month,$request);
            return $createdData;

        }catch(\Stripe\Error\RateLimit $e){

            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            echo  json_encode(array('status' => 'false', 'message' => $results['message'] ));
            exit;
        }
        catch(\Stripe\Error\Card $e){

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            // param is '' in this case
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            echo  json_encode(array('status' => 409, 'message' => $results['message'] ));
            exit;

        } catch (\Stripe\Error\RateLimit $e) {

            // Too many requests made to the API too quickly

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message'] = 'Too many requests made to the API too quickly';
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;

        } catch (\Stripe\Error\InvalidRequest $e) {

            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  = "Invalid parameters were supplied to Stripe's API";
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;

        } catch (\Stripe\Error\Authentication $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Network communication with Stripe failed";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\Base $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Error";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $results['message']  = "Something else happened, completely unrelated to Stripe";

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;
        }
    }
    function createCustomerNew($user_id)
    {

        $getData =  $this->companyConnection->query("SELECT stripe_customer_id,email FROM users WHERE id ='" . $user_id . "'")->fetch();
        $email['email'] = $getData['email'];
        if($getData['stripe_customer_id']=='')
        {
            $customer = createCustomer($email);
            $customer_id = $customer['account_data'];
            $upateData =  "UPDATE users SET stripe_customer_id='$customer_id' where id='$user_id'";
            $stmt = $this->companyConnection->prepare($upateData);
            $stmt->execute();


            return $customer_id;
        }
        else
        {

            return $getData['stripe_customer_id'];
        }


    }
    public function VendorPaymentStep2($stripe_account_array,$id){
        try {

            $request['email']=$stripe_account_array['email'];
            $request['business_type']='individual';
            $data['line1'] = "address_full_match"; /*$fdata['faddress1'];*/
            $accountId=createAccount($request);
            if ($accountId["code"] == 200 && $accountId["status"] == "success") {
                $data['user_id'] = $id;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                $data['country'] = $stripe_account_array['country'];
                $data['account_type'] = 'individual';
                $data['account_number'] = $stripe_account_array['account_number'];
                $data['routing_number'] = $stripe_account_array['routing_number'];
                $data['city'] = $stripe_account_array['city'];
                $data['line1'] =$stripe_account_array['line1'];
                $data['address_line2'] = 'address_full_match';
                $data['postal_code'] = $stripe_account_array['postal_code'];
                $data['state'] = $stripe_account_array['state'];
                $data['email'] = $stripe_account_array['email'];
                $data['first_name'] = $stripe_account_array['first_name'];
                $data['last_name'] = $stripe_account_array['last_name'];
                $data['phone'] = $stripe_account_array['phone'];
                $data['ssn_last'] = $stripe_account_array['ssn_last_4'];
                $dateofbirth= $stripe_account_array['year'] -  $stripe_account_array['month'] -  $stripe_account_array['day'];
                $data['dob'] = $dateofbirth;

                $stripe_account_array['stripe_account_id']=$accountId['account_id'];
                $getUserAccountData =  $this->companyConnection->query("SELECT * FROM user_account_detail WHERE user_id ='" . $data['user_id'] . "'")->fetch();
                if(empty($getUserAccountData)){
                    $enableAccount = enableAccount($stripe_account_array);
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO user_account_detail(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    if (!empty($accountId['account_id'])) {
                        $upateData = "UPDATE users SET stripe_account_id= '" .$stripe_account_array['stripe_account_id'] . "' where id= " . $data['user_id'];
                        $stmt = $this->companyConnection->prepare($upateData);
                        $stmt->execute();

                    }
                    if($enableAccount["code"] == 400){
                        $account = \Stripe\Account::retrieve($accountId['account_id']);
                        $account->delete();
                    }
                    return $enableAccount;
                }else{
                    $enableAccount = enableAccount($stripe_account_array);
                    $sqlData = createSqlUpdateCase($data);
                    $query = "UPDATE user_account_detail SET " . $sqlData['columnsValuesPair'] . " WHERE user_id=" . $data['user_id'];
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);
                    return array('status' => 'AccountDetailupdated', 'enableAccount' => $enableAccount);
                }


            }else{
//                dd($accountId);
                return $accountId;
            }



        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }


}

$addVendor = new addVendor();
?>
