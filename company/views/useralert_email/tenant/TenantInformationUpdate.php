<style>
    .highlight{
        font-weight: bold;
    }
</style>
<!--<title>Apexlink</title>-->
<table style="margin: auto; width: 640px; border:2px solid #00b0f0" border="0" cellspacing="0" cellpadding="0" align="center"  bgcolor="#fff">
    <tr>
        <td>
            <table style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2d3091; margin: auto; width: 100%;" border="0"
                   cellspacing="0" cellpadding="0" align="center" bgcolor="#d6f0fd">
                <tbody>
                <tr style="background-color: #00b0f0; height: 20px;">
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr style="border-collapse: collapse; background-color: #fff;">
                    <td class="w580" style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;
                            border-collapse: collapse;" width="580">
                        <!-- <div align="center" id="headline">
                                                        <p style="color: #ffffff; font-family: 'Lucida Grande', 'Lucida Sans Unicode', Verdana, sans-serif;
                                                            font-size: 36px; text-align: center; margin-top: -40px; margin-bottom: 30px;">-->
                        <!-- http://Apexlink.seasiaconsulting.com-->
                        <center>
                            <div style="margin-top: 1px; margin-bottom: 10px;">
                                <p class="clsDefaultImage">
                                    <img alt="Apexlink" width="100" src="http://herbdigest.com/images/logo.png">
                                </p>
                            </div>
                        </center>
                        <!--  </p>
                                                    </div>-->
                    </td>
                </tr>
                <tr style="background-color: #00b0f0; height: 20px;">
                    <td>
                        &nbsp;
                    </td>
                </tr>
                </tbody>
            </table>
            <table style="width: 640px; padding:10px 20px;" border="0" cellspacing="0" cellpadding="0" align="center">
                <tbody>
                <tr>
                    <td style="border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 10px 0;"
                        valign="top" width="100%">
                        <h3 style="width: 100%;  font-size: 15px; text-align: center; text-decoration:underline">Apex Link Notification :#alert_name#</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span style="font-weight: bold;">User Alert:</span>{Subject},
                        <br/>
                        <br/>
                    </td>
                </tr>
                <tr>
                    <td>
                        Dear {Username},
                        <br/>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <p style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; font-size: 15px;">
                            The following tenant has updated information on the Tenant portal. Click below to see the change.
                            <br />
                            <br />
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td >
                                    <span style="font-weight: bold;">Tenant Name:</span> {TenantName}
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <span style="font-weight: bold;"> Unit #:</span> {UnitNumber}
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <span style="font-weight: bold;"> Property ID:</span> {PropertyId}
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <span style="font-weight: bold;">Property Name:</span> {PropertyName}
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <span style="font-weight: bold;">Building ID:</span> {BuildingId}
                                    <br />
                                    <br />
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <p style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; font-size: 15px;">
                                        Please <a href="#">click here</a> to view navigate to the portal.
                                        <br />
                                        <br />
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
                <tr>
                    <td>
                        Thank You
                        <br />
                        <br />

                    </td>
                </tr>
                <tr>
                    <td>
                        {Signature}
                        <br />
                        <br />
                    </td>
                </tr>

            </table>
            <table id="footer" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px;
       -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased;
       background-color: #585858; color: #ffffff; margin: auto; width: 640px;" border="0"
                   cellspacing="0" cellpadding="0" align="center" bgcolor="#0e867b">
                <tbody>
                <tr>
                    <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;
                font-size: 14px;" align="center"
                        bgcolor="#05a0e4">
                        ApexLink Property Manager● <a style="color: #fff; text-decoration: none;" href="javascript:;">
                            support@apexlink.com ●772-212-1950 </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>