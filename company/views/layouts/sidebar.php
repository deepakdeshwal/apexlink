<!-- MAIN Navigation Ends -->
<div class="col-sm-4 col-md-2 main-content-lt">
    <div class="left-links slide-toggle"><i class="fa fa-caret-right" aria-hidden="true"></i></div>
    <div id="LeftMenu" class="box">
        <div class="list-group panel">
            <!-- One  Starts-->
            <a href="#leftnav1" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu"><i class="fa fa-building-o" aria-hidden="true"></i>Entity/Company Setup</a>
            <div class="collapse submenu-outer" id="leftnav1">
                <a href="/User/AccountSetup" class="list-group-item sub-item account_setup"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Account Setup</a>
                <a href="/Setting/Settings" class="list-group-item sub-item default_settings"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Default Settings</a>
                <a href="/User/UserPlans" class="list-group-item sub-item existing_plan"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Existing Plans</a>
                <a href="admin-entity-export-import.html" class="list-group-item sub-item export_import_backup"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Export & Import Backup</a>
            </div>
            <!-- One Ends-->

            <!-- Two Ends-->
            <a href="#leftnav2" class="first-highlight list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu"><i class="fa fa-home" aria-hidden="true"></i>Property Setup</a>
            <div class="collapse submenu-outer" id="leftnav2">
                <a href="/MasterData/AddPropertyType" class="list-group-item sub-item property_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Property Type</a>
                <a href="/MasterData/AddUnitType" class="list-group-item sub-item unit_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Unit Type</a>
                <a href="/MasterData/AddPropertySubType" class="list-group-item sub-item property_sub_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Property Sub-Type</a>
                <a href="/MasterData/AddPropertyStyle" class="list-group-item sub-item property_style"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Property Style</a>
                <a href="/MasterData/AddPropertyGroup" class="list-group-item sub-item property_group"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Property Groups</a>
                <a href="/MasterData/AddAmenity" class="list-group-item sub-item amenity"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Amenities</a>
                <a href="/MasterData/AddPortfolio" class="list-group-item sub-item portfolio"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Portfolio</a>
            </div>
            <!-- Two Ends-->
    
            <!-- Three Starts-->
            <a href="#leftnav3" class="first-highlight list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu"><i class="fa fa-usd" aria-hidden="true"></i>Accounting</a>
            <div class="collapse submenu-outer" id="leftnav3">
                <a href="/AccountType/AccountType" class="list-group-item sub-item account_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Account Type</a>
                <a href="/ChartOfAccount/AddChartOfAccount" class="list-group-item sub-item chart_of_accounts"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Chart of Accounts</a>
                <a href="/AccountPreference/AccountPreference" class="list-group-item sub-item accounting_preferences"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Accounting Preferences</a>
                <a href="/MasterData/AddChargeCode" class="list-group-item sub-item charge_code"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Charge Code</a>
                <a href="/MasterData/ChargeCodePreferences" class="list-group-item sub-item charge_code_preferences"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Charge Code Preferences</a>
                <a href="/MasterData/AllocationOrder" class="list-group-item sub-item allocation_order"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Allocation Order</a>
                <a href="/MasterData/AddCheckSetUp" class="list-group-item sub-item check_setup"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Check Setup</a>
                <a href="/MasterData/BankAccount" class="list-group-item sub-item bank_account"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Bank Account</a>
                <a href="/MasterData/TaxSetup" class="list-group-item sub-item tax_setup"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Tax Setup</a>
            </div>
            <!-- Three Ends-->

            <!-- Four Starts-->
            <a href="#leftnav4" class="first-highlight list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu"><i class="fa fa-users" aria-hidden="true"></i>Users</a>
            <div class="collapse submenu-outer" id="leftnav4">
                <a href="/User/ManageUser" class="list-group-item sub-item manage_users"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Manage Users</a>
                <a href="/user/ManageUserRoles" class="list-group-item sub-item manage_user_Roles"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Manage User Roles</a>
                <a href="/user/UserRoleAuthorization" class="list-group-item sub-item role_authorization"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Role Authorization</a>
                <a href="/user/LoginHistory" class="list-group-item sub-item login_history"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Login History</a>
                <a href="/User/ManageGroups" class="list-group-item sub-item manage_groups"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Manage Groups</a>
                <a href="/User/ManageVehicle" class="list-group-item sub-item vehicle_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Manage Vehicle</a>
            </div>
            <!-- Four Ends-->

             <!-- Five Starts-->
            <a href="#leftnav5" class="first-highlight list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu"><i class="fa fa-user" aria-hidden="true"></i>Vendors</a>
            <div class="collapse submenu-outer" id="leftnav5">
                <a href="/MasterData/VendorType" class="list-group-item sub-item vendor_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Vendor Type</a>
            </div>
            <!-- Five Ends-->

            <!-- Six Starts-->
            <a href="#leftnav6" class="first-highlight list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu"><i class="fa fa-cog" aria-hidden="true"></i>Maintenance</a>
            <div class="collapse submenu-outer" id="leftnav6">
                <a href="/Maintenance/MaintenanceCategory" class="list-group-item sub-item category_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Category</a>
                <a href="/Maintenance/MaintenancePriority" class="list-group-item sub-item priority_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Priority</a>
                <a href="/MasterData/MaintenanceSeverity" class="list-group-item sub-item severity_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Severity</a>
                <a href="/Maintenance/WorkOrderType" class="list-group-item sub-item workorder_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Work Order Type</a>
                <a href="/Maintenance/InventoryTracker" class="list-group-item sub-item default_settings"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Inventory Tracker</a>

            </div>
            <!-- Six Starts-->

            <!-- Seven Starts-->
            <a href="#leftnav7" class="first-highlight list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu"><i class="fa fa-bell" aria-hidden="true"></i>Alert</a>
            <div class="collapse submenu-outer" id="leftnav7">
                <a href="/Alert/ManageAlert" class="list-group-item sub-item alertsetup"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>User Alerts</a>

            </div>
            <!-- Seven Ends-->

            <!-- Eight Starts-->
            <a href="#leftnav8" class="first-highlight list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu"><i class="fa fa-clock-o" aria-hidden="true"></i>Events</a>
            <div class="collapse submenu-outer" id="leftnav8">
                <a href="/MasterData/AddEventType" class="list-group-item sub-item event_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Event Types</a>
            </div>
            <!-- Eight Ends-->

            <!-- Nine Starts-->
            <a href="#leftnav9" class="first-highlight list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu"><i class="fa fa-th" aria-hidden="true"></i>Templates</a>
            <div class="collapse submenu-outer" id="leftnav9">
                <a href="/Template/EmailTemplate" class="list-group-item sub-item template"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Email Template</a>

            </div>
            <!-- Nine Ends-->

            <!-- Ten Starts-->
            <a href="#leftnav10" class="first-highlight list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu"><i class="fa fa-trash" aria-hidden="true"></i>Trash Bin</a>
            <div class="collapse submenu-outer" id="leftnav10">
                <a href="/TrashBin/PropertyType" class="list-group-item sub-item property_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Property Type</a>
                <a href="/TrashBin/UnitType" class="list-group-item sub-item unit_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Unit Type</a>
                <a href="/TrashBin/PropertySubType" class="list-group-item sub-item property_sub_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Property Sub-Type</a>
                <a href="/TrashBin/PropertyStyle" class="list-group-item sub-item leftnav10"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Property Style</a>
                <a href="/TrashBin/PropertyGroups" class="list-group-item sub-item property_group"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Property Groups</a>
                <a href="/TrashBin/Amenities" class="list-group-item sub-item amenity"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Amenities</a>
                <a href="/TrashBin/AccountType" class="list-group-item sub-item amenity1"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Account Type</a>
                <a href="/TrashBin/ChartofAccount" class="list-group-item sub-item amenity2"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Chart Of Accounts</a>
                <a href="/AccountPreference/AccountPreference" class="list-group-item sub-item accounting_preferences1"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Account Preferences</a>
                <a href="/TrashBin/ChargeCode" class="list-group-item sub-item amenity3"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Charge Code</a>
                <a href="/TrashBin/ChargeCodePreferences" class="list-group-item sub-item chargeCodePreferences"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Charge Code Preferences</a>
                <a href="/TrashBin/ManageUsers" class="list-group-item sub-item manageUsers"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Manage Users</a>
                <a href="/TrashBin/ManageUserRoles" class="list-group-item sub-item amenity5"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Manage Users Roles</a>
                <a href="/TrashBin/VendorType" class="list-group-item sub-item vendor_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Vendor Type</a>
                <a href="/TrashBin/Category" class="list-group-item sub-item category_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Category</a>
                <a href="/TrashBin/Priority" class="list-group-item sub-item priority_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Priority</a>
                <a href="/TrashBin/Severity" class="list-group-item sub-item severity_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Severity</a>
                <a href="/TrashBin/EventTypes" class="list-group-item sub-item event_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Event Types</a>
                <a href="/TrashBin/Contact" class="list-group-item sub-item amenity6"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Contacts</a>
                <a href="/TrashBin/WorkOrderType" class="list-group-item sub-item workorder_type"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Work Order Type</a>
                <a href="/TrashBin/Building" class="list-group-item sub-item amenity7"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Building</a>
                <a href="/TrashBin/Unit" class="list-group-item sub-item amenity8"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Unit</a>
                <a href="/TrashBin/TaxSetup" class="list-group-item sub-item amenity9"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Tax Setup</a>



            </div>
            <!-- Ten Ends-->
        </div>
    </div>
</div>
<script>
    $(document).on('click','.first-highlight',function(e){
        var hrefs = $(this).attr("href");
        var itemId = hrefs.substring(1, hrefs.length);
        var redirected_url = $("#"+itemId+" a").attr("href");
        window.location.href = redirected_url;
    })
    $(document).ready(function () {

        $("body").bind("keydown", function(e) {
            if (e.keyCode == 44) {
                return false;
            }
        });

    });
    </script>

