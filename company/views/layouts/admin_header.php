<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>ApexLink</title>
    <meta name="author" content="APEXLINK">
    <meta name="description" content="APEXLINK">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="icon" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/images/favicon.ico" alt="apex-icon" type="image/x-icon" sizes="16x16">
    <!-- Bootstrap -->

    <link href="https:////netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/ui.jqgrid.min.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery.timepicker.min.css">

    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/passwordscheck.css" />
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL;?>/css/calculator.css"/>
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/owl.theme.default.min.css"/>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.multiselect.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/main.css"/>

    <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />



    <!-- include libraries(jQuery, bootstrap) -->
    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.css" rel="stylesheet">
<!--    <link rel="stylesheet" href="--><?php //echo COMPANY_SUBDOMAIN_URL;?><!--/css/summernote.min.css"/>-->
    <!--<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>-->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/summernote.min.js" defer></script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-ui.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.timepicker.min.js"></script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.jqgrid.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.responsivetabs.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.validate.min.js"></script>
    <script>
        var jqgridNewOrUpdated = 'false';
    </script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/common.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/toastr.min.js"></script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootbox.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.mask.min.js"></script>
    <script defer src="<?php echo SUPERADMIN_SITE_URL; ?>/js/calculator.js"></script>
    <script defer src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/owl.carousel.js"></script>
    <script defer src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/date.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.1.4/core.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootstrap-select.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />


    <script defer>
        //var SITE_URL = <?php //echo SITE_URL ?>//;
        var subdomain_url  = "<?php echo COMPANY_SUBDOMAIN_URL; ?>";

        jQuery(document).ready(function($) {
            var countDownDate = "<?php if (isset($_SESSION[SESSION_DOMAIN]['countDownTimer'])) {
                echo $_SESSION[SESSION_DOMAIN]['countDownTimer'];
            } else {
                echo 'null';
            };?>";

            if (countDownDate != 'null') {
                countDownTimer(countDownDate);
            }
        });
        //Toast Messages
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": true,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        setTimeout(function () {
            var announcement_skip_ids = localStorage.getItem("announcement_ids");
            //getAnnouncements(announcement_skip_ids);
            setInterval(function () {
                var announcement_skip_ids = localStorage.getItem("announcement_ids");
                //getAnnouncements(announcement_skip_ids);
            }, 60000);

        }, 1000);

        function getAnnouncements(announcement_skip_ids) {
            $.ajax
            ({
                type: 'post',
                url: '/common-ajax',
                data: {
                    class: "CommonAjax",
                    action: "showAnnouncement",
                    announcement_skip_ids: announcement_skip_ids
                },
                success: function (result) {
                    var result = JSON.parse(result);
                    //console.log(result);
                    if (result != 'empty') {
                        announcement_ids = [];
                        $.each(result, function (key, value) {
                            var values;
                            if (value.seenStatus != 'seen') {
                                if (value.title == 'System Maintainance') {
                                    values = value.id;
                                    $('#announcementSys_id').val(value.id);
                                    $('#SysstartDate').text(value.start_date);
                                    $('#SysstartTime').text(value.start_time);
                                    $('#SysendTime').text(value.end_time);
                                    $('#SystemMaintenance').modal('show');
                                } else {
                                    values = value.id;
                                    $('#announcement_id').val(value.id);
                                    $('#announcementTitle').text(value.title);
                                    $('#announcementStartDate').text(value.start_date);
                                    $('#announcementEndDate').text(value.end_date);
                                    $('#announcementStartTime').text(value.start_time);
                                    $('#announcementEndTime').text(value.end_time);
                                    $('#announcementDescription').text(value.description);
                                    $('#announcement').modal('show');
                                }
                                announcement_ids.push(values);
                            }
                        });
                        if (result) {
                            localStorage.setItem("announcement_ids", JSON.stringify(announcement_ids));
                        }
                    }
                }
            });
        }

        $(document).on('click', '.closeSystemAnnouncement', function () {
            var announcement_id = $("#announcementSys_id").val();
            $.ajax
            ({
                type: 'post',
                url: '/common-ajax',
                data: {
                    class: "CommonAjax",
                    action: "updateUserAnnouncement",
                    announcement_id: announcement_id
                },
                success: function (result) {
                    var result = JSON.parse(result);
                }
            });
        });

        $(document).on('click', '.closeAnnouncement', function () {
            var announcement_id = $("#announcement_id").val();
            $.ajax
            ({
                type: 'post',
                url: '/common-ajax',
                data: {
                    class: "CommonAjax",
                    action: "updateUserAnnouncement",
                    announcement_id: announcement_id
                },
                success: function (result) {
                    var result = JSON.parse(result);
                }
            });
        });


        $(document).ready(function () {
            //  window.history.pushState(null, "", window.location.href);
            //   window.onpopstate = function () {
            //       window.history.pushState(null, "", window.location.href);
            //   };
        });


        jQuery.extend(jQuery.validator.messages, {
            required: "* This field is required.",
        });

        // Update the count down every 1 second
        function countDownTimer(date) {
            var sessionDateExpired = new Date(date).getTime();
            var x = setInterval(function () {

                // Get today's date and time
                var now = new Date().getTime();

                // Find the distance between now and the count down date
                var distance = sessionDateExpired - now;

                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Display the result in the element with id="demo"
                $('#countDownDays').html(days);
                $('#countDownHours').html(hours);
                $('#countDownMins').html(minutes);
                $('#countDownSec').html(seconds);
                //console.log('countDown', countDown);
                // If the count down is finished, write some text
                if (distance < 0) {
                    clearInterval(x);

                }
            }, 1000);
            setTimeout(function(){
                $('#trail').css('display','inline-block');
            },1000);
        }

    </script>
    <script>var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>

    <style media="print">
        body{display:none;}
    </style>
</head>
<!--<body onload="StartTimers();">-->
<body>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/modal/companyModal.php");
?>