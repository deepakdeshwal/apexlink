<header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-2 visible-xs">
                    
                </div>
                <div class="col-sm-3 col-xs-8">
                    <div class="logo"><img src="<?php echo COMPANY_SUBDOMAIN_URL; ?><?php echo!empty($_SESSION[SESSION_DOMAIN]['company_logo']) ? $_SESSION[SESSION_DOMAIN]['company_logo'] : '/images/logo.png' ?>" height="60" width="160"></div>
                </div>
                <div class="col-sm-9 col-xs-2">
                    <div class="hdr-rt">
                        <!-- TOP Navigation Starts -->

                        <nav class="navbar navbar-default">

                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                              </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                              <ul class="nav navbar-nav">
                                  <li class="hidden-xs">Welcome:  Sonny Kesseben (ACL Properties), June 01, 2018 <a herf="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i></a> <a href="javascript:;"><i class="fa fa-calculator" aria-hidden="true"></i></a></li>
                               <li class="nav-item">
                                    <a class="nav-link disabled" href="#"><i class="fa fa-question" aria-hidden="true"></i>Help</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link disabled" href="#"><i class="fa fa-wrench" aria-hidden="true"></i>Support</a>
                                  </li>

                                    <li class="nav-item">
                                    <a class="nav-link disabled" href="#"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                                  </li>
                              </ul>

                            </div><!-- /.navbar-collapse -->
                        </nav>
                        <!-- TOP Navigation Ends -->

                    </div>
                </div>
            </div>
        </div>
        
    </header>
        
    <!-- MAIN Navigation Starts -->
        <section class="main-nav">
         <nav class="navbar navbar-default">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header visible-xs">
                  <a id="show" class="icon-bar" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                  <a class="nav-close visible-xs" id="close" href="#"><i class="fa fa-times" aria-hidden="true"></i> </a>
                  <li class="active"><a href="spotlight.html">My Account</a></li>
                  <li ><a href="properties.html">Work Order</a></li>
                    <li ><a href="properties.html">Transactions</a></li>
                  <li><a href="leases.html">File Library</a></li>
                  <li><a href="maintenance.html">Vendor Insurance</a></li>
                  <li><a href="communication.html">Manage Bill</a></li>
                  <li><a href="communication.html">Communication</a></li>
                  <li><a href="communication.html">Reporting</a></li>
                </ul>

              </div><!-- /.navbar-collapse -->
          </nav>
        </section>