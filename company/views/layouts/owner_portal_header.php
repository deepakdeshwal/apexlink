<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>ApexLink</title>
    <meta name="author" content="APEXLINK">
    <meta name="description" content="APEXLINK">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="icon" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/images/favicon.ico" alt="apex-icon" type="image/x-icon" sizes="16x16">
    <!-- Bootstrap -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/ui.jqgrid.min.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery.timepicker.min.css">

    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/passwordscheck.css" />
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL;?>/css/calculator.css"/>
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/owl.theme.default.min.css"/>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.multiselect.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/main.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />



    <!-- include libraries(jQuery, bootstrap) -->
    <!-- include summernote css/js -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
    <!--<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>-->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/summernote.js" defer></script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-ui.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.timepicker.min.js"></script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.jqgrid.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.responsivetabs.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.validate.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/common.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/toastr.min.js"></script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootbox.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.mask.min.js"></script>
    <script defer src="<?php echo SUPERADMIN_SITE_URL; ?>/js/calculator.js"></script>
    <script defer src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/owl.carousel.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/core-js/2.1.4/core.min.js"></script>
    <script>
        var upload_url = "<?php echo SITE_URL; ?>";
        var subdomain_url  = "<?php echo COMPANY_SUBDOMAIN_URL; ?>";
    </script>
    <script defer>

        //Toast Messages
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": true,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        // setTimeout(function () {
        //     var announcement_skip_ids = localStorage.getItem("announcement_ids");
        //     //getAnnouncements(announcement_skip_ids);
        //     setInterval(function () {
        //         var skip_announcement_ids = localStorage.getItem("skip_announcement_ids");
        //         getAnnouncements(skip_announcement_ids);
        //     }, 6000);
        //
        // }, 1000);

        function getAnnouncements(skip_announcement_ids) {
            $.ajax
            ({
                type: 'post',
                url: '/Announcement-Ajax',
                data: {
                    class: "announcementAjax",
                    action: "showCompanyAnnouncement",
                    skip_announcement_ids: skip_announcement_ids
                },
                success: function (result) {
                    var result = JSON.parse(result);
                    console.log(result);
                    if (result.code == 200 && result.status == 'success') {
                        console.log('result>>>', result.data);
                        skip_announcement_ids = [];
                        $.each(result.data, function (key, value) {
                            var show_ann_id;
                            if (value.seenStatus != 'seen') {
                                show_ann_id = value.id;
                                $('#company_announcement_id').val(value.id);
                                $('#companyAnnouncementTitle').text(value.title);
                                $('#companyAnnouncementStartDate').text(value.start_date);
                                $('#companyAnnouncementEndDate').text(value.end_date);
                                $('#companyAnnouncementStartTime').text(value.start_time);
                                $('#companyAnnouncementEndTime').text(value.end_time);
                                $('#companyAnnouncementDescription').text(value.description);
                                if (value.file_library.length > 0){
                                    $('#companyAnnouncement .view_ann_attachment').show();
                                    var src = '';
                                    var html = '';
                                    var finalHtml = '';
                                    $.each(value.file_library, function (k, v) {
                                        var filename = "'"+v.filename+"'";
                                        src = upload_url+ 'company/' + v.file_location;
                                        html += "<a class='show-attachment' href='"+src+"' target='_blank'>"+filename+"</a><br>";
                                    });
                                    $('.view_ann_attachment_list').html(html);

                                }
                                $('#companyAnnouncement').modal('show');

                                skip_announcement_ids.push(show_ann_id);
                            }
                        });
                        if (result) {
                            localStorage.setItem("skip_announcement_ids", JSON.stringify(skip_announcement_ids));
                        }
                    }
                }
            });
        }
        // $('#companyAnnouncementAttachment').modal('show');
        /* $(document).on('click', '.view-attachment', function () {
             $('#companyAnnouncement').modal('hide');
             $('#companyAnnouncementAttachment').modal('show');
         });*/
        $(document).on('click', '.closeCompanyAnnouncement, .view-attachment', function () {
            var company_announcement_id = $("#company_announcement_id").val();
            $.ajax({
                type: 'post',
                url: '/Announcement-Ajax',
                data: {
                    class: "announcementAjax",
                    action: "updateCompanyAnnouncementStatus",
                    company_announcement_id: company_announcement_id
                },
                success: function (result) {
                    var result = JSON.parse(result);
                }

            });
        });


        $(document).ready(function () {
            //  window.history.pushState(null, "", window.location.href);
            //   window.onpopstate = function () {
            //       window.history.pushState(null, "", window.location.href);
            //   };
        });
        $(document).on('click', '#owner_portal_logout', function () {
            console.log('window.location.pathname>>', window.location.pathname);
            $.ajax({
                type: 'post',
                url: '/OwnerPortal-Ajax',
                data: {
                    class: 'viewAccountInfoAjax',
                    action: 'portalLogout',
                    id: owner_id,
                    data :window.location.pathname
                },
                success: function (response) {
                    console.log('response>>', response);
                    var response = JSON.parse(response);
                    if (response.status == "success")
                    {
                        console.log('resp>>', response);
                        window.location.href = "/Owner/Login";
                    } else
                    {
                        toastr.error(response.message);
                    }
                }
            });
        });

        jQuery.extend(jQuery.validator.messages, {
            required: "* This field is required.",
        });



    </script>
    <style media="print">
        body{display:none;}
    </style>
</head>
<body>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/modal/companyModal.php");
?>
