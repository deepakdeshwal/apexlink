<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="#tenants"><a href="#guest-cards" aria-controls="home" role="tab" data-toggle="tab">Tenants</a></li>
                                <li role="presentation"><a href="active" aria-controls="profile" role="tab" data-toggle="tab">Owners</a></li>
                                <li role="presentation"><a href="#leases" aria-controls="profile" role="tab" data-toggle="tab">Leases</a></li>
                                <li role="presentation"><a href="#moveIn" aria-controls="profile" role="tab" data-toggle="tab">Move-In</a></li>
                            </ul>
                            <div class="atoz-outer">
                                A-Z <span>All</span>
                            </div>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="guest-cards">
                                    <div class="accordion-form">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> General Information</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="latefee-check-outer">
                                                                        <div class="col-sm-4">
                                                                            <div class="check-outer">
                                                                                <input type="checkbox" />
                                                                                <label class="blue-label">Use Entity/Company Name on Check/Reports</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <label>Upload Picture</label>
                                                                        <div class="upload-logo">
                                                                            <div class="img-outer"></div>
                                                                            <a href="javascript:;"><i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Logo</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-2">
                                                                            <label>Salutation</label>
                                                                            <select class="form-control"><option>Select</option></select>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>First Name <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Middle Name</label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Last Name <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Maiden Name</label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Nick Name</label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Gender</label>
                                                                            <select class="form-control"><option>Select</option></select>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Entity/Company </label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>D.O.B</label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Ethnicity <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Martial Status <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Hobbies <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Veteran Status <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>SSN/SIN/ID <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Contact Details</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">

                                                                        <div class="col-sm-2">
                                                                            <label>Zip/Postal Code <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Country <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>State / Province <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>City <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Address1 <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Address2 <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Address3 <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Address4 <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Phone Type</label>
                                                                            <select class="form-control"><option>Select</option></select>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Carrier <em class="red-star">*</em></label>
                                                                            <select class="form-control"><option>Select</option></select>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Country Code </label>
                                                                            <select class="form-control"><option>Select</option></select>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Phone Number <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a> </label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Add Note for this number </label>
                                                                            <textarea class="form-control" type="textArea"></textarea>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Email  <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Refernce Source <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control"><option>Select</option></select>
                                                                        </div>




                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Emergency Contact Details</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-2">
                                                                            <label>Emergency Contact Name</label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Relationship <em class="red-star">*</em></label>
                                                                            <select class="form-control"><option>Select</option></select>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Country Code</label>
                                                                            <select class="form-control"><option>Select</option></select>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Phone Number <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Email</label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Banking Information</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="grey-box-add">
                                                                    <div class="grey-box-add-inner">
                                                                        <div class="grey-box-add-lt">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-2">
                                                                                    <label>Account Name <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                    <input class="form-control" type="text" />
                                                                                </div>
                                                                                <div class="col-sm-2">
                                                                                    <label>Relationship <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                    <select class="form-control"><option>Select</option></select>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <label>Bank Acc. for ACH Transcation <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                    <select class="form-control"><option>Select</option></select>
                                                                                </div>
                                                                                <div class="col-sm-2">
                                                                                    <label>Bank Account Type </label>
                                                                                    <select class="form-control"><option>Select</option></select>
                                                                                </div>
                                                                                <div class="col-sm-2">
                                                                                    <label>Bank Account Number</label>
                                                                                    <input class="form-control hide_copy" type="text" />
                                                                                </div>
                                                                                <div class="col-sm-2">
                                                                                    <label>Bank Fraction Number</label>
                                                                                    <input class="form-control" type="text" />
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="grey-box-add-rt">
                                                                            <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <label>Draw Payment Mode</label>
                                                                    <div class="check-outer">
                                                                        <input type="checkbox" />
                                                                        <label>Check</label>
                                                                    </div>
                                                                    <div class="check-outer">
                                                                        <input type="checkbox" />
                                                                        <label>EFT</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Tax Filling Information</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-2">
                                                                            <label>Tax Payer Name</label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Tax Payer ID</label>
                                                                            <input class="form-control" type="text" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Eligible for 1099</label>
                                                                            <div class="check-outer">
                                                                                <input type="radio" />
                                                                                <label>Yes</label>
                                                                                <input type="radio" />
                                                                                <label>No</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Send 1099</label>
                                                                            <div class="check-outer">
                                                                                <input type="checkbox" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Properties owned</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="grey-box-add">
                                                                    <div class="grey-box-add-inner">
                                                                        <div class="grey-box-add-lt">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-2">
                                                                                    <label>Property  Name</label>
                                                                                    <select class="form-control"><option>Select</option></select>
                                                                                </div>
                                                                                <div class="col-sm-2">
                                                                                    <label>Property Owned</label>
                                                                                    <input class="form-control" type="text" />
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="grey-box-add-rt">
                                                                            <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span>Owner Crendital Control</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-2">
                                                                            <label>Credential Name</label>
                                                                            <input class="form-control" type="text" />

                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Credential Type <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control"><option>Select</option></select>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Acquire Date</label>
                                                                            <input class="form-control" type="text" />

                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Expiration Date</label>
                                                                            <input class="form-control" type="text" />

                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Notice Period <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control"><option>Select</option></select>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Notes</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-4">
                                                                            <textarea class="notes capital"></textarea>
                                                                            <a class="add-icon-textarea" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> File Library</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-4">
                                                                            <button class="green-btn">Add Files...</button>
                                                                            <button class="orange-btn">Remove All Files...</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Custom Fields</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-2">
                                                                            <label>No Custom Fields</label>
                                                                        </div>
                                                                        <div class="col-sm-2 text-right pull-right">
                                                                            <label></label>
                                                                            <button class="blue-btn">New Custom Field</button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="btn-outer">
                                                        <button class="blue-btn">Save</button>
                                                        <button class="grey-btn">Cancel</button>
                                                        <button class="grey-btn">Reset</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- Accordian Ends -->
                                        <!--tab Ends -->

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->



<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



</script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
