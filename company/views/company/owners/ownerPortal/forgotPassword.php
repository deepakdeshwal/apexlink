<?php

include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
?>
    <div class="apxpg-login">
        <div id="wrapper">
            <div class="login-bg">
                <div class="login-outer">
                    <div class="login-logo"><img src="<?php echo COMPANY_SUBDOMAIN_URL;?>/images/logo-login.png"/></div>
                    <div class="login-inner">
                        <div id="send_mail">
                            <form name="forgot_password" id="forgot_password" method="post" enctype="multipart/form-data" >
                                <h2>Enter Your Email</h2>
                                <div class="login-data">
                                    <label>
                                        <input class="form-control" name="email" placeholder="Email" id="email" type="text"/>
                                    </label>
                                    <div class="btn-outer">
                                        <input type="submit" name="send_mail" value="Send Mail" class="blue-btn" id="send_mail"/>
                                        <input  type="button" value="Cancel" class="grey-btn" id="forgot_cancel_owner"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div id="click_here_login_link" style="display: none;">
                        <p>The Reset Link associated to the email address you provided was emailed to you. Thank you!</p>
                        <a href="/Owner/Login" style="text-decoration: underline;">Click Here to Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Wrapper Ends -->

    <!--    <script src="--><?php //echo COMPANY_SUBDOMAIN_URL;?><!--/js/validation/users/users.js"></script>-->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/ownerPortal/portalLogin.js"></script>

<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>