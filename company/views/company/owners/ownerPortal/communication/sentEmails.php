<?php
//if (!isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id']) && ($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'] == '')) {
//    $url = SUBDOMAIN_URL;
//    header('Location: ' . $url);
//}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
?>

<div id="wrapper">
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_top_navigation.php");
    ?>
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="/Owner/Communication/SentEmails">Email</a></li>
                                <li role="presentation" class=""><a href="/Owner/Communication/TextMessage">Text Message</a></li>
                                <li role="presentation" class=""><a href="/Owner/MyAccount/AllCompanyAdminAnnouncements">Announcement</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="property-status text-right">
                                    <a class="blue-btn" href="javascript:;">Inbox</a>
                                    <a class="blue-btn" href="javascript:;">Drafts</a>
                                    <a class="blue-btn" href="javascript:;">Compose Email</a>
                                </div>
                                <div role="tabpanel" class="tab-pane active" id="tenant-communi-one">
                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Sent Mails</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="tab-content">
                                                <form id="addFileOwner" enctype='multipart/form-data'>
                                                    <div role="tabpanel" class="tab-pane active" id="contact-info-tab">
                                                        <div class="form-outer">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <div class="apx-table listinggridDiv">
                                                                        <div class="table-responsive">
                                                                            <table id="list_bills" class="table table-bordered"></table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Form Outer Ends-->
                                                    </div>
                                                    <!-- Sixth Tab Ends-->
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Tab One Ends -->

                                <div role="tabpanel" class="tab-pane" id="tenant-communi-two">
                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Sent Messages</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="grid-outer">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-dark">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">Name</th>
                                                            <th scope="col">To</th>
                                                            <th scope="col">User</th>
                                                            <th scope="col">Message</th>
                                                            <th scope="col">Delivery</th>
                                                            <th scope="col">Date</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Tab Two Ends -->

                                <div role="tabpanel" class="tab-pane" id="tenant-communi-three">

                                </div>
                                <!-- Tab Two Ends -->

                                <div role="tabpanel" class="tab-pane" id="tenant-communi-four">

                                </div>
                                <!-- Tab Two Ends -->

                            </div>
                            <!--tab Ends -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->


<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_footer.php");
?>

<script>
    propertyown(1);
    function propertyown(status) {
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";

        var owner_id = <?php echo $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']; ?>;

        // var table = 'owner_property_owned';
        var table = 'general_property';
        var columns = ['To / Name','To / Email Address','User / From','Subject','Delivery','Date'];
        var joins = [{table:'general_property',column:'id',primary:'property_id',on_table:'owner_property_owned',type:'JOIN'},{table: 'general_property', column: 'property_type', primary: 'id', on_table: 'company_property_type'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['owner_property_owned.updated_at', 'general_property.property_name','owner_property_owned.property_percent_owned','general_property.deleted_at', 'general_property.address1', 'general_property.address2', 'general_property.address3', 'general_property.address4', 'general_property.manager_id', 'general_property.owner_id','owner_property_owned.updated_at'];
        var extra_where = [{column: 'user_id', value: '1000000', condition: '=',table:'owner_property_owned'}];
        var columns_options = [
            {name: 'To / Name', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'To / Email Address', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'User / From', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'Subject', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'Delivery', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'Date', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
        ];
        var ignore_array = [];
        jQuery("#list_bills").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",

            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                //data:unserialize(),
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'owner_property_owned.updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Sent Emails",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: false, refresh:false, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 200, drag: true, resize: false}
        );
    }

    function propertyName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            var flagValue = $(rowObject.Action).attr('flag');
            var id = $(rowObject.Action).attr('data_id');
            var flag = '';
            if (flagValue == 'yes') {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id='+id+'" ><img src="/company/images/Flag.png"></a></span>';
            } else {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
            }
        }

    }

    $("#communication_top").addClass("active");
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
    <!--- Accordians -->
</script>

