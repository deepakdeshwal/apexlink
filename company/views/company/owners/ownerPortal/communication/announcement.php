<?php
//if (!isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id']) && ($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'] == '')) {
//    $url = SUBDOMAIN_URL;
//    header('Location: ' . $url);
//}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
?>
<style>
    .announcement-hdr {
        background: /*url(../images/form-hdr.png) repeat-x scroll 0 0 rgba(0, 0, 0, 0)*/ #eee;
        border-bottom: 1px solid #d0d0d0;
        color: #2d3091;
        float: left;
        font-size: 15px;
        font-weight: bold;
        height: auto;
        line-height: 22px;
        margin: 0;
        padding: 1%;
        width: 98%;
    }
    .tabs-content-data {
        width: 100% !important;
        margin: 0 0 0 0;
        background: #fff;
        padding: 1%;
        float: left;
    }
    .form-outer {
        width: 100% !important;
        margin: 15px 0px 0px 0px;
        padding: 0;
        float: left;
        border-radius: 5px;
        border: 1px solid #d0d0d0;
        overflow: inherit !important;
        /* overflow: hidden; */
        box-shadow: 4px 4px 4px -2px #D2D2D2;
        background: #fff;
    }
</style>
<div id="wrapper">

    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_top_navigation.php");
    ?>
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class=""><a href="/Owner/Communication/SentEmails">Email</a></li>
                                <li role="presentation" class=""><a href="/Owner/Communication/TextMessage">Text Message</a></li>
                                <li role="presentation" class="active"><a href="/Owner/MyAccount/AllCompanyAdminAnnouncements">Announcement</a></li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tenant-communi-one">
                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Announcements</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="form-outer"><div class="announcement-hdr"><div>03/28/2019</div>Road Cleaning</div><div class="tabs-content-data"><p>Test</p></div></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Tab One Ends -->
                            </div>
                            <!--tab Ends -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->


<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_footer.php");
?>

<script>
    $("#communication_top").addClass("active");
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
    <!--- Accordians -->
</script>

