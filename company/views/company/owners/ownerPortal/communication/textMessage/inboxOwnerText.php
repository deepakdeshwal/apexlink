<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_top_navigation.php");
?>
<?php

if (!isset($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']) && ($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'] == '')) {

    $url = SUBDOMAIN_URL;
    header('Location: ' . $url.'/Owner/Login');
}
?>
<section class="main-content">
    <div class="container-fluid">
        <div class="row">
            <div class="bread-search-outer">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="breadcrumb-outer">
                            Communication &gt;&gt; <span>Text Message </span>
                        </div>

                    </div>
                    <div class="col-sm-4">
                        <div class="easy-search">
                            <input placeholder="Easy Search" type="text"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-data">
                <!--                --><?php //include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                <!--Tabs Starts -->
                <div class="main-tabs">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation"><a href="/Owner/Communication/SentEmails">Email</a></li>
                        <li role="presentation" class="active"><a href="/Owner/Communication/TextMessage">Text Message</a></li>
                    </ul>

                    <!-- Tab panes -->

                    <div class="tab-content">
                        <div class="panel-heading">

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="receivables">
                                    <div class="property-status">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="content-section">
                                                    <div class="main-tabs">
                                                        <div role="tabpanel" class="tab-pane active" id="communication-one">
                                                            <!-- Sub Tabs Starts-->
                                                            <div class="property-status">
                                                                <div class="row">
                                                                    <div class="col-sm-2">
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <div class="btn-outer text-right">
                                                                            <a class="blue-btn" href="/Owner/Communication/TextMessageDrafts">Drafts</a>
                                                                            <a data-urltype="" href="/Owner/Communication/AddTextMessage" class="blue-btn margin-right compose-text">Compose Message</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="accordion-form">
                                                                <div class="accordion-outer">
                                                                    <div class="bs-example">
                                                                        <div class="panel-group" id="accordion">
                                                                            <div class="panel panel-default">
                                                                                <div class="panel-heading">
                                                                                    <h4 class="panel-title">
                                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Inbox</a>
                                                                                    </h4>
                                                                                </div>
                                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                    <div class="panel-body">
                                                                                        <div class="inbox-outer">
                                                                                            <ul id="communication_inbox">

                                                                                            </ul>

                                                                                        </div>
                                                                                        <div class="detailed_inbox">

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- Accordian Ends -->
                                                                    </div>
                                                                </div>

                                                            </div>


                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Regular Rent Ends -->

                            </div>
                        </div>
                        <!-- Sub tabs ends-->
                    </div>



                    <!-- Sub Tabs Starts-->

                    <!-- Sub tabs ends-->
                </div>

            </div>
        </div>

        <!--Tabs Ends -->

    </div>
    </div>
    </div>
</section>
</div>
<script>
    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
        $("#communication").addClass("active");
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/email/file_library.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/email/common.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/owner-portal/textMessage/inbox-owner-text.js"></script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
