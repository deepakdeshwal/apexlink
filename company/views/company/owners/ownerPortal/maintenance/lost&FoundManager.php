<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_top_navigation.php");
?>
<div class="popup-bg"></div>
<div id="wrapper">
    <?php

    if (!isset($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']) && ($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'] == '')) {

        $url = SUBDOMAIN_URL;
        header('Location: ' . $url.'/Owner/Login');
    }
    ?>
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">

                        <!-- Tabs Start -->
                        <div class="main-tabs">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" ><a href="/Owner/Ticket/Tickets">Tickets</a></li>
                                <li role="presentation" ><a href="/Owner/WorkOrder/WorkOrders">Work Orders</a></li>
                                <li role="presentation" class="active"><a href="/Owner/LostAndFoundOwner/LostAndFound">Lost &  Found Manager</a></li>
                                <li role="presentation"><a href="/Owner/MyAccount/OwnerPurchaseOrder" >Purchase Orders</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tickets-lost-found">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#lostfound-one" aria-controls="profile" role="tab" data-toggle="tab">Dashboard</a></li>
                                        <li role="presentation"><a href="#lostfound-two" aria-controls="home" role="tab" data-toggle="tab">Items</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active " id="lostfound-one">
                                            <div class="property-status">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <label>Property</label>
                                                        <select class="fm-txt form-control">
                                                            <option></option>
                                                            <option></option>
                                                            <option></option>
                                                        </select>

                                                    </div>
                                                    <div class="col-sm-3 row">
                                                        <label>&nbsp;</label>
                                                        <button onclick="window.location.href='new-property.html'" class="blue-btn">Add Lost</button>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="lost-found-items">

                                                <div class="lost-found-columns">
                                                    <span class="count">0</span>
                                                    <label>Lost Items</label>
                                                    <span class="item-btn"><button class="white-btn">Show all Items</button></span>
                                                </div>

                                                <div class="lost-found-columns">
                                                    <span class="count">25</span>
                                                    <label>Claimed Items</label>
                                                    <span class="item-btn"><button class="white-btn">Show all Items</button></span>
                                                </div>

                                                <div class="lost-found-columns">
                                                    <span class="count">71</span>
                                                    <label>Unclaimed Items</label>
                                                    <span class="item-btn"><button class="white-btn">Show all Items</button></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Lost Items</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Date</th>
                                                                                                <th scope="col">Item Type</th>
                                                                                                <th scope="col">Property</th>
                                                                                                <th scope="col">More</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- Regular Rent Ends -->
                                        <div role="tabpanel" class="tab-pane" id="lostfound-two">

                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#lostfound-sub-one" aria-controls="home" role="tab" data-toggle="tab">Lost Items</a></li>
                                                <li role="presentation"><a href="#lostfound-sub-two" aria-controls="profile" role="tab" data-toggle="tab">Found Items</a></li>
                                                <li role="presentation"><a href="#lostfound-sub-three" aria-controls="home" role="tab" data-toggle="tab">Claimed Items</a></li>
                                                <li role="presentation"><a href="#lostfound-sub-four" aria-controls="profile" role="tab" data-toggle="tab">Unclaimed Items</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="lostfound-sub-one">
                                                    <div class="property-status">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <label>&nbsp;</label>
                                                                <select class="fm-txt form-control"> <option>Property</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>&nbsp;</label>
                                                                <select class="fm-txt form-control"> <option>All</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label>&nbsp;</label>
                                                                <input type="text" placeholder="Search by item #, serial#, Desc, Brand Name and other Details" class="form-control"/>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>&nbsp;</label>
                                                                <div class="btn-outer">
                                                                    <button class="blue-btn">Search</button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Lost Items</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Item Number</th>
                                                                                                <th scope="col">Description</th>
                                                                                                <th scope="col">Category</th>
                                                                                                <th scope="col">Lost Date</th>
                                                                                                <th scope="col">Expiration Date</th>
                                                                                                <th scope="col">Status</th>
                                                                                                <th scope="col">Full View</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div role="tabpanel" class="tab-pane " id="lostfound-sub-two">
                                                    <div class="property-status">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <label>&nbsp;</label>
                                                                <select class="fm-txt form-control"> <option>Property</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>&nbsp;</label>
                                                                <select class="fm-txt form-control"> <option>All</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label>&nbsp;</label>
                                                                <input type="text" placeholder="Search by item #, serial#, Desc, Brand Name and other Details" class="form-control"/>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>&nbsp;</label>
                                                                <div class="btn-outer">
                                                                    <button class="blue-btn">Search</button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Found Items</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Item Number</th>
                                                                                                <th scope="col">Description</th>
                                                                                                <th scope="col">Category</th>
                                                                                                <th scope="col">Lost Date</th>
                                                                                                <th scope="col">Expiration Date</th>
                                                                                                <th scope="col">Status</th>
                                                                                                <th scope="col">Full View</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div role="tabpanel" class="tab-pane " id="lostfound-sub-three">
                                                    <div class="property-status">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <label>&nbsp;</label>
                                                                <select class="fm-txt form-control"> <option>Property</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>&nbsp;</label>
                                                                <select class="fm-txt form-control"> <option>All</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label>&nbsp;</label>
                                                                <input type="text" placeholder="Search by item #, serial#, Desc, Brand Name and other Details" class="form-control"/>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>&nbsp;</label>
                                                                <div class="btn-outer">
                                                                    <button class="blue-btn">Search</button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Claimed Items</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Item Number</th>
                                                                                                <th scope="col">Description</th>
                                                                                                <th scope="col">Category</th>
                                                                                                <th scope="col">Lost Date</th>
                                                                                                <th scope="col">Expiration Date</th>
                                                                                                <th scope="col">Status</th>
                                                                                                <th scope="col">Full View</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div role="tabpanel" class="tab-pane " id="lostfound-sub-four">
                                                    <div class="property-status">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <label>&nbsp;</label>
                                                                <select class="fm-txt form-control"> <option>Property</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>&nbsp;</label>
                                                                <select class="fm-txt form-control"> <option>All</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label>&nbsp;</label>
                                                                <input type="text" placeholder="Search by item #, serial#, Desc, Brand Name and other Details" class="form-control"/>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>&nbsp;</label>
                                                                <div class="btn-outer">
                                                                    <button class="blue-btn">Search</button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Unclaimed Items</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Item Number</th>
                                                                                                <th scope="col">Description</th>
                                                                                                <th scope="col">Category</th>
                                                                                                <th scope="col">Lost Date</th>
                                                                                                <th scope="col">Expiration Date</th>
                                                                                                <th scope="col">Status</th>
                                                                                                <th scope="col">Full View</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>l-00421</td>
                                                                                                <td>Black Phone</td>
                                                                                                <td>Electronic Items</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                <td>InActive</td>
                                                                                                <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Tabs End -->
                </div>
            </div>
        </div>
    </section>
</div>



<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<!-- Jquery Starts -->
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script>var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>
    $(document).ready(function () {
        var owner_id = <?php echo $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']; ?>;

        var formData = new FormData();
        formData.append('action','ownerPropertyList');
        formData.append('class','ownerPropertyList');
        formData.append('owner_id',owner_id);
        $.ajax({
            url:'/viewAccountInfoAjax',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == "success"){
                    $('#owner_property').html(response.data);
                } else if (response.status == 'error'){

                }
            },
            cache: false,
            contentType: false,
            processData: false
        });

        $('#startDate').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat})
            .datepicker("setDate", new Date());

        $('#endDate').datepicker({yearRange: '-0:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat, minDate:  new Date(),})
            .datepicker("setDate", new Date());
    });
</script>

<script>
    $(document).ready(function () {
        $('#ownerBills').on('submit',function (e) {
            e.preventDefault();

        })
    });
</script>
<script>
    propertyown(1);
    function propertyown(status) {
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";

        var owner_id = <?php echo $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']; ?>;

        // var table = 'owner_property_owned';
        var table = 'general_property';
        var columns = ['Work Order Number','Work Order Category','Vendor','Property','Unit','Created On','Caller/Requested By','Estimated Cost($)','Priority','Status'];
        var joins = [{table:'general_property',column:'id',primary:'property_id',on_table:'owner_property_owned',type:'JOIN'},{table: 'general_property', column: 'property_type', primary: 'id', on_table: 'company_property_type'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['owner_property_owned.updated_at', 'general_property.property_name','owner_property_owned.property_percent_owned','general_property.deleted_at', 'general_property.address1', 'general_property.address2', 'general_property.address3', 'general_property.address4', 'general_property.manager_id', 'general_property.owner_id','owner_property_owned.updated_at'];
        var extra_where = [{column: 'user_id', value: '1000000', condition: '=',table:'owner_property_owned'}];
        var columns_options = [
            {name: 'Work Order Number', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'Work Order Category', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'Vendor', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'Property', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'Unit', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'Created On', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'Caller/Requested By', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'Estimated Cost($)', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'Priority', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'Status', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
        ];
        var ignore_array = [];
        jQuery("#list_bills").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",

            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                //data:unserialize(),
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'owner_property_owned.updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Work Orders",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: false, refresh:false, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 200, drag: true, resize: false}
        );
    }

    function propertyName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            var flagValue = $(rowObject.Action).attr('flag');
            var id = $(rowObject.Action).attr('data_id');
            var flag = '';
            if (flagValue == 'yes') {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id='+id+'" ><img src="/company/images/Flag.png"></a></span>';
            } else {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
            }
        }

    }

    function propertyUnitBuilding(cellValue, options, rowObject) {
        if(cellValue == ''){
            return '<span data_redirect="no" style="text-decoration: underline;">0</span>';
        } else {
            return '<span data_redirect="yes" style="text-decoration: underline;">' + cellValue + '</span>';
        }
    }

    function propertyStatus(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "Active";
        else if (cellValue == '2')
            return "Active";
        else if (cellValue == '3')
            return "Archive";
        else if (cellValue == '4')
            return "Active";
        else if (cellValue == '5')
            return "Active";
        else if (cellValue == '6')
            return "Resign";
        else if (cellValue == '7')
            return "Active";
        else if (cellValue == '8')
            return "Active";
        else
            return '';
    }
    $('#maintenance_top').addClass('active');

</script>
</body>
</html>
