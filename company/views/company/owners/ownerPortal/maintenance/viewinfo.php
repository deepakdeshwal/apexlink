<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
?>
<?php
// include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_top_navigation.php");
?>

    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-12">
                           
                        </div>
                    </div>

                </div>
              
                    <div class="view_mainenance_section">
                        <div class="content-section">

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Purchase Order <a class="back" href="/Owner/MyAccount/OwnerPurchaseOrder"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>                                    </h3>
                                </div>
                                <div class="form-data detail-outer">
                                    <div class="col-sm-12" style="min-height: 30px;">
                                        <div class="col-sm-6" style="min-height: 30px;">
                                            <label class="text-right">PO Number:</label><span class="po_number">df5445d2</span>
                                        </div>
                                        <div class="col-sm-6" style="min-height: 30px;">
                                            <label class="text-right">Required By Date:</label><span class="required_by">24-09-209</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="min-height: 30px;">
                                        <div class="col-sm-6" style="min-height: 30px;">
                                            <label class="text-right">Current Date:</label><span class="currentdate">12-05-2015</span>
                                        </div>
                                        <div class="col-sm-6" style="min-height: 30px;">
                                            <label class="text-right">Status:</label><span class="status">Created</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Property Information</h3>
                                </div>
                                <div class="form-data">
                                    <div class="col-sm-12">
                                        <table id="property_table" style="width: 100%;" class="table table-hover table-dark">
                                            <thead>
                                                <tr>
                                                    <th>Property</th>
                                                    <th>Building</th>
                                                    <th>Units</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>General</h3>
                                </div>
                                <div class="form-data">
                                    <div class="col-sm-12">
                                        <div class="grey-add-table">
                                            <div class="table-responsive">
                                                <table class="table table-dark" id="amnt_table">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Number</th>
                                                        <th scope="col">GL Account</th>
                                                        <th scope="col">Description</th>
                                                        <th scope="col">Item Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</th>
                                                        <th scope="col">Total (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td><input class="form-control" type="text" name="qty_number[]" value="1"/></td>
                                                        <td><select class="form-control" name="gl_account[]"><option value="0">Select</option></select></td>
                                                        <td><input class="form-control" type="text" name="description[]"/></td>
                                                        <td><input class="form-control" type="text" name="item_amount[]" value="0.00"/></td>
                                                        <td><input class="form-control" type="text" name="total_amount[]" value="0.00"/></td>
                                                        <td>
                                                            <a class="minus-icon remove_clone" href="javascript:void(0);" style="color:#000 !important;"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <div class="col-sm-12">
                                                    <div class="col-sm-6"></div>
                                                    <div class="col-sm-6" style="padding-left: 0px;">
                                                        <div class="col-sm-5 totallabel">Total (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>):</div>
                                                        <div class="col-sm-6 totalamt">0.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Vendor Information</h3>
                                </div>
                                <div class="form-data">
                                    <div class="col-sm-12">
                                        <table id="vendor_table" style="width: 100%;" class="table table-hover table-dark">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                  
                                </div>
                            </div>

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Vendor Instructions</h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-3 clear">
                                            <textarea class="form-control" style="width: 100%;" name="vendor_instruction"></textarea>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                                <!-- Form Outer Ends -->
                            <div class="form-outer">
                                <div class="files_main">
                                        <div class="form-outer">
                                            <div class="form-hdr"><h3>File</h3></div>
                                            <div class="form-data">
                                                <div class="apx-table">
                                                    <div id="collapseSeventeen" class="panel-collapse in">
                                                        <div class="grid-outer">
                                                            <div class="apx-table">
                                                                <div class="table-responsive">
                                                                    <table id="viewpurchaseordertable" class="table table-bordered"></table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Custom Fields</h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">

                                    </div>
                                 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
<script>
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/maintenance/purchase_orders/purchase_orders.js"></script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>