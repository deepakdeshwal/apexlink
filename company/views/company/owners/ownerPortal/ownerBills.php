<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_top_navigation.php");

?>
<div class="popup-bg"></div>
<div id="wrapper">
    <?php

    if (!isset($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']) && ($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'] == '')) {

        $url = SUBDOMAIN_URL;
        header('Location: ' . $url.'/Owner/Login');
    }
    //    echo '<pre>';print_r($_SESSION[SESSION_DOMAIN]);
    ?>
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">

                        <!-- Tabs Start -->
                        <div class="main-tabs">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <form id="ownerBills">
                                    <div role="tabpanel" class="tab-pane active" id="contact-info-tab">
                                        <div class="form-outer">

                                            <div class="form-hdr">
                                                <h3>Owner Bills</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="col-sm-2">
                                                    <label>Filter By</label>
                                                    <select class="form-control common_search_class" id="filter_by">
                                                         <option value="">Select</option>
                                                        <option value="1">Last Quarter</option>
                                                        <option value="2">Last Month </option>
                                                        <option value="3">Last Week</option>
                                                        <option value="4">Select Date</option>
                                                        <option value="5">Select Range</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>From</label>
                                                    <input class="form-control startDateClass" disabled type="text" id="startDate" name="startDate[]">
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>To</label>
                                                    <input class="form-control endDateClass" disabled  type="text" id="endDate" name="endDate[]">
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>Status</label>
                                                    <select class="form-control common_search_class" id="status">
                                                        <option value="">Select</option>
                                                        <option value="0">Due</option>
                                                        <option value="3">OnHold</option>
                                                        <option value="2">Overdue</option>
                                                        <option value="1">Paid</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>Property</label>
                                                    <select class="form-control common_search_class" id="owner_property"></select>
                                                </div>
                                                <div class="col-sm-2 text-right">
                                                    <label style="height: 15px;"></label>
                                                    <input type="button" class="blue-btn" id="searchFilter" value="Search">
                                                </div>
                                            </div>

                                        </div>
                                        <!-- Form Outer Ends-->
                                    </div>
                                    <!-- Sixth Tab Ends-->
                                </form>
                            </div>
                        </div>

                        <div class="tab-content">
                            <form id="addFileOwner" enctype='multipart/form-data'>
                                <div role="tabpanel" class="tab-pane active" id="contact-info-tab">
                                    <div class="form-outer">
                                        <div class="grid-outer">
                                            <div class="table-responsive">
                                                <div class="apx-table listinggridDiv">
                                                    <div class="table-responsive">
                                                        <table id="list_bills" class="table table-bordered"></table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends-->
                                </div>
                                <!-- Sixth Tab Ends-->
                            </form>
                        </div>
                    </div>
                    <!--Tabs End -->
                </div>
            </div>
        </div>
    </section>


</div>



<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<!-- Jquery Starts -->
<script>
    //var jsDateFomat = "<?php //echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>//";
    //var datepicker = "<?php //echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>//";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var owner_portal_id = "<?php echo $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']; ?>";
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var owner_id = "<?php echo $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['Owner_Portal']['default_currency_symbol']; ?>";
    console.log('default_currency_symbol>>', default_currency_symbol);

    $('#bills_top').addClass('active');

</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/ownerPortal/ownerBills.js"></script>
</body>
</html>
