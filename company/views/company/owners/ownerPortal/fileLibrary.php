<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_top_navigation.php");
?>
<div class="popup-bg"></div>
<div id="wrapper">

    <?php

    if (!isset($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']) && ($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'] == '')) {

        $url = SUBDOMAIN_URL;
        header('Location: ' . $url.'/Owner/Login');
    }
    ?>
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">

                        <!-- Tabs Start -->
                        <div class="main-tabs">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <form id="addFileOwner" enctype='multipart/form-data'>
                                    <div role="tabpanel" class="tab-pane active" id="contact-info-tab">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>File Library</h3>
                                            </div>
                                            <div class="form-data">

                                                <div class="form-outer2">
                                                    <div class="col-sm-12 text-right">
                                                        <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                        <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                        <button class="orange-btn" id="remove_library_file">Remove All Files</button>
                                                        <button type="button" class="blue-btn" style="" id="SaveAllimagesFileOwner">Save                                 </button>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row" id="file_library_uploads">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="grid-outer listinggridDiv">
                                                    <div class="apx-table apxtable-bdr-none">
                                                        <div class="table-responsive">
                                                            <table id="file-library" class="table table-bordered"></table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Form Outer Ends-->
                                    </div>
                                    <!-- Sixth Tab Ends-->
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--Tabs End -->
                </div>
            </div>
        </div>
    </section>


</div>



<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<!-- Jquery Starts -->
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script>var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<!--<script>var owner_id = localStorage.getItem('owner_portal_id');</script>-->

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/intlTelInput.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/ownerPortal/editAccountInfo.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/file_library.js" type="text/javascript"></script>

<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var owner_id = <?php echo $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']; ?>;
    fileLibrary();

    function fileLibrary(status) {

        var table = 'tenant_chargefiles';
        // var columns = ['Name','Preview','Action'];
        var columns = ['Id', 'Name', 'Preview','File_location','File_extension','Action']
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        // var extra_columns = [];
        var extra_where = [{column: 'user_id', value: owner_id, condition: '='}];
        var columns_options = [
            {name:'Id',index:'id', width:150,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'Name',index:'filename', width:150,searchoptions: {sopt: conditions},table:table},
            {name:'Preview',index:'file_extension',width:150,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
            {name:'File_location',index:'file_location', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'File_extension',index:'file_extension', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'file_type',align:"center", width:50,searchoptions: {sopt: conditions},table:table,formatter:fileFormatter},
        ];
        var ignore_array = [];
        jQuery("#file-library").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                //  extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Files",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 200, drag: true, resize: false}
        );
    }

    /**
     * jqGrid function to format file extension
     * @param status
     */
    function imageFormatter(cellvalue, options, rowObject){
        var location = (rowObject)?rowObject.File_location:'';

        var path = upload_url+'company/'+location;
        if(rowObject !== undefined) {
            var src = '';
            if (rowObject.Preview == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
            } else if (rowObject.Preview == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
            } else if (rowObject.Preview == 'docx' || rowObject.Preview == 'doc') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
            }else if (rowObject.Preview == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
            }
            return '<a href=" '+path+'" download><img data-path = '+path+' class="img-upload-tab" width=30 height=30 src="' + src + '"></a>';
        }
    }

    function statusFmatter1 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {

            var select = '';


            var data = '';

            if(select != '') {
                var data = 'X';
            }

            return data;
        }
    }

    function fileFormatter(cellValue, options, rowObject)
    {

        if(rowObject !== undefined) {
            var file_type =  rowObject.View;
            var location = rowObject.File_location;
            var path = upload_url+'company/'+location;

            var imageData = '';
            if(file_type == '1'){

                imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
            } else {
                if (rowObject.File_extension == 'xlsx') {
                    src = upload_url + 'company/images/excel.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'pdf') {
                    src = upload_url + 'company/images/pdf.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'docx') {
                    src = upload_url + 'company/images/word_doc_icon.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }else if (rowObject.File_extension == 'txt') {
                    src = upload_url + 'company/images/notepad.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }
            }

            var html = "<a href='javascript:void(0)' class='deleteFile' data-id="+rowObject.Id+" data-path = "+path+"><i class=\"fa fa-times\" aria-hidden=\"true\" style='font-size: 16px;color:#000;'></i></a>\n";
            return html;
        }
    }

    $('#remove_library_file').attr('disabled',true);
    $('#SaveAllimagesFileOwner').attr('disabled',true);

    $(document).on('click','#SaveAllimagesFileOwner',function () {

        var form = $('#addFileOwner')[0];
        var formData = new FormData(form);
        formData.append('id',owner_id);
        formData.append('action','ownerFileAdd');
        formData.append('class','EditOwnerAjax');

        $.ajax({
            url:'/EditOwnerAjax',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == "success"){
                    localStorage.setItem("Message", 'Record added successfully.');
                    $('#file_library_uploads').html('');
                    $('#file-library').trigger( 'reloadGrid' );
                    $('#remove_library_file').attr('disabled',true);
                    $('#SaveAllimagesFileOwner').attr('disabled',true);
                } else if (response.code == 500){
                    toastr.warning(response.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    $(document).on('click','#remove_library_file',function (e) {
        e.preventDefault();
    });
    $(document).on('click','.deleteFile',function (e) {
        e.preventDefault();
        var selectID = $(this).attr('data-id');
        bootbox.confirm("Do you want to delete this record?", function(result) {
            if (result == true) {

                $.ajax({
                    type: 'post',
                    url:'/EditOwnerAjax',
                    data: {
                        class: "EditOwnerAjax",
                        action: 'ownerFileDelete',
                        file_id: selectID
                    },
                    success: function (response) {
                        var response = JSON.parse(response);
                        if (response.status == 'success' && response.code == 200) {
                            toastr.success(response.message);
                            $('#file-library').trigger( 'reloadGrid' );
                        } else if (response.status == 'error' && response.code == 400) {
                            $('.error').html('');
                            $.each(response.data, function (key, value) {
                                $('.' + key).html(value);
                            });
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            // alert(key+value);
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
            }
        });
    });
    $('#file_library_top').addClass('active');

</script>

</body>
</html>
