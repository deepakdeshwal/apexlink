
<input type="hidden" name="owner_id" class="owner_id" value="<?php echo $_GET['owner_id']; ?>">
<input type="hidden" name="secretkey" class="secretkey"  value="<?php echo $_GET['secretkey']; ?>">

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
?>
<div class="login-bg owner_portal_set_password">
    <div class="login-outer">
        <div class="login-logo"><img src="<?php echo COMPANY_SUBDOMAIN_URL;?>/images/logo-login.png"/></div>

        <div class="login-inner">
            <form id="set_new_password">
                <h2>Set New Password For Your Account</h2>
                <div class="login-data">
                    <div class="col-sm-12 mb-15 p-0">
                        <label>New Password <em class="red-star">*</em></label>
                        <input class="form-control" type="password" name="new_password" id="new_password">
                    </div>
                    <div class="col-sm-12  mb-15 p-0">
                        <label>Confirm New Password <em class="red-star">*</em></label>
                        <input class="form-control" type="password" name="confirm_password" id="confirm_password">
                    </div>
                    <div class="btn-outer">
                        <div class="btn-outer">
                            <button type="submit" class="blue-btn">Save</button>
                            <button type="reset" class="grey-btn">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/ownerPortal/portalLogin.js"></script>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>

<script>
    var owner_id = $(".owner_id").val();
    var key = $(".secretkey").val();
    $.ajax({
        type: 'post',
        url: '/OwnerPortalLogin-Ajax',
        data: {
            class: 'portalLoginAjax',
            action: 'checkSecretKey',
            'id':owner_id,
            'secretKey':key
        },
        success : function(response){
            var response =  JSON.parse(response);
            console.log('response>>', response);
            if(response.status == 'success') {
                toastr.success(response.message);
                //  resetPassword();
            }else {
                toastr.error(response.message);
            }
        }
    });
</script>













