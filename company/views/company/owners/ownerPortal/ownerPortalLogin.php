<?php

include_once(COMPANY_DIRECTORY_URL. "/views/layouts/admin_header.php");

$email='';
$pass='';
$check='';
$super_admin_name = '';
if(isset($_COOKIE['remember_login'])) {
    $data   = unserialize($_COOKIE['remember_login']);
    $email  =$data['email'];
    $pass   =$data['password'];
    $check  ='checked';
    $super_admin_name = '';
}

?>
    <div class="apxpg-login">
        <div id="wrapper">
            <div class="ie_wrapper">
                <div class="outdated-browser-warning" id="outdatedBrowserWarning" style="display: none; opacity: 1;">
                    <h1 class="outdated-browser-warning__title">Your browser is not supported</h1>
                    <p class="outdated-browser-warning__close">
                        <a class="outdated-browser-warning__close-button" id="outdatedBrowserWarningClose" href="#">×</a>
                    </p>
                    <p class="outdated-browser-warning__contents">

                        Apexlink no longer supports Internet Explorer.
                        To avoid potential issues, we recommend using a supported internet browser.
                        <br>
                    </p>
                    <a class="btn btn-secondary outdated-browser-warning__update-link" href="#" target="_blank">Find a Supported Browser</a>
                </div>
            </div>
            <div class="login-bg">
                <div class="login-outer">
                    <div class="login-logo"><img src="<?php echo COMPANY_SUBDOMAIN_URL;?>/images/logo-login.png"/></div>
                    <div class="login-inner">
                        <form id="ownerPortal-login-form" >
                            <h2>SIGN IN TO YOUR USER ACCOUNT</h2>
                            <div class="login-data">
                                <label>
                                    <input class="form-control" name="email" placeholder="Email" value="<?php echo $email?>" id="email" type="text"/>
                                </label>

                                <label>
                                    <input class="form-control email-psw-input" id="password" value="<?php echo $pass?>" name="password" placeholder="Password" type="password"/>
                                </label>
                                <div class="remember-psw">
                                    <div class="remember-psw-lt"><input type="checkbox" name='remember' <?php echo $check?> id="remember"   /> Remember Me</div>
                                    <div class="remember-psw-rt"><a href="/Owner/Login/ForgotPassword">Forgot password?</a></div>
                                </div>
                                <div class="btn-outer">
                                    <input type="submit" name="Login" value="Login" class="blue-btn"  id="login_button"/>
                                    <input  type="button" value="Cancel" class="grey-btn" id="cancel"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Wrapper Ends -->

    <!-- Jquery Starts -->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL;?>/js/company/people/owner/ownerPortal/portalLogin.js"></script>
    <script type="text/javascript">
        $(document).on('click','.toggle-password',function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    </script>

<?php
if (isset($_SESSION[SESSION_DOMAIN]["message"])) {
    $message = $_SESSION[SESSION_DOMAIN]["message"];
//    print_r($message); exit;
    ?>
    <script>
        toastr.success("<?php echo $message ?>");
    </script>
    <?php
    unset($_SESSION[SESSION_DOMAIN]["message"]);
}
?>
    <script>
        $(document).ready(function () {
            $('a#outdatedBrowserWarningClose').on('click',function () {
                $('#outdatedBrowserWarning').hide();
                setCookie("outdatedBrowser", true, 365);
            })
            function isIE() {
                ua = navigator.userAgent;
                /* MSIE used to detect old browsers and Trident used to newer ones*/
                var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

                return is_ie;
            }

            function getCookie(cname) {
                var name = cname + "=";
                var ca = document.cookie.split(';');
                for(var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }

            function setCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                var expires = "expires="+d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            }


            if (isIE()){
                var get_cookie = getCookie('outdatedBrowser');
                if(!get_cookie)
                {
                    $('#outdatedBrowserWarning').show();
                }
            }
        })

        $("#ownerPortal-login-form").validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                    maxlength: 50
                },
                password: {
                    required: true,
                }
            },
            submitHandler: function (e) {

                if($("#remember").is(':checked'))
                    var remember = 'on';  // checked
                else
                    var remember = '';  // unchecked

                var form = $('#ownerPortal-login-form')[0];
                var formData = new FormData(form);
                formData.append('remember',remember);
                formData.append('action','portalLogin');
                formData.append('class','TenantPortal');
                // console.log('rem>>', formData); return;
                $.ajax({
                    url:'/tenantPortal',
                    type: 'POST',
                    data: formData,
                    success: function (response) {
                        var response = JSON.parse(response);
                        if(response.status=='success') {
                            var user_id = response.user_id;
                            var url = response.portal_url;
                            toastr.success(response.message);
                            setTimeout(function () {
                                window.location.href =  window.location.origin+url
                            }, 1000);

                        } else {
                            toastr.error(response.message);
                        }

                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                });
            }
        });
    </script>
<?php include_once(COMPANY_DIRECTORY_URL."/views/layouts/admin_footer.php"); ?>