<!-- General Finance Starts -->
<!-- Balance Sheet Model Starts -->
<div class="container">
    <div class="modal fade" id="balance-sheet-detail" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupBalanceSheetData">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Balance Sheet Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="bl_current_date" class="is_required form-control datefield calander current_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="balance_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn test" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Balance Sheet Model Ends-->

<!-- Balance Sheet Consolidate Model Starts-->
<div class="container">
    <div class="modal fade" id="balance-sheet-consolidated" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupBalanceSheetConsolidated">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Balance Sheet Consolidated</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="bl_consolidate_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="consolidated_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="consolidated_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn balance_sheet_consolidated" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Balance Sheet Consolidate Model Ends-->

<!-- Income Statement Model Starts-->
<div class="container">
    <div class="modal fade" id="income_statement" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupIncomeStatement">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Income Statement</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="income_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="income_statement_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="income_statement_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn income-statement" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Income Statement Model Ends-->

<!-- Income Statement Consolidated Model Starts-->
<div class="container">
    <div class="modal fade" id="income_statement_consolidated" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupIncomeStatementConsolidated">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Income Statement Consolidated</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="income_con_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="income_consolidated_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="income_consolidated_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn income_statement_consolidated" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Income Statement Consolidated Model Ends-->

<!-- Income Statement -12 Months Model Starts-->
<div class="container">
    <div class="modal fade" id="income_statement_months" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupIncomeStatementMonth">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Income Statement -12 Months Camparison</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            From Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="from_date" id="income_statement_from_date" class="is_required form-control datefield  capital fromDate" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="income_statement_end_date" class="form-control datefield end_date" readonly="readonly"></div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="income_months_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="income_months_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn income_statement_months" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Income Statement -12 Months Model Ends-->

<!-- Cash Flow Model Starts-->
<div class="container">
    <div class="modal fade" id="cash" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupCash">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Cash Flow Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="cash_start_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="cash_end_date" class="form-control datefield end_date" readonly="readonly"></div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="cash_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="cash_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn cash" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Cash Flow Model Model Ends-->

<!-- Cash Flow -12 Months Model Starts-->
<div class="container">
    <div class="modal fade" id="cash_flow_months" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupCashFlowMonths">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Cash Flow-12 Months Comparison</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12"><label is_required="no">From Date</label><input type="text" name="cash_flow_end_date" id="end_date" class="form-control datefield fromDate" readonly="readonly"></div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            To Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="cash_flow_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                      
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="cash_flow_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="cash_flow_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn cash_flow" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Cash Flow Model -12 Months Model Ends-->

<!-- Cash Flow Comparsion Model Starts-->
<div class="container">
    <div class="modal fade" id="cash_flow_comparsions" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupCashflowComparsions">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Cash Flow Comparsion</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="cash_flow_comparsion_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="cash_flow_comparsion_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn cash_flow_comparsions" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Cash Flow Comparsion Model Ends-->

<!-- Equality Statement Model Starts-->
<div class="container">
    <div class="modal fade" id="gfrm1_equaity" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupEquality">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Equity Statement Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="equality_start_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>
                                         <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="equality_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="equity_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="equity_balance_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn equity_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Equality Statement Model Ends-->

<!--Account Total Filters Starts-->
<div class="container">
    <div class="modal fade" id="gfrm1_accounts" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupAccounts">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Account Total Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            From Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="from_date" id="account_from_date" class="is_required form-control datefield  capital fromDate" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="account_end_date" class="form-control datefield end_date" readonly="readonly"></div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn accounts_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Account Total Filters Ends-->

<!--Expense Distribution Starts-->
<div class="container">
    <div class="modal fade" id="gmf2_expenses" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupExpenseDistribution">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Expense Distribution</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="expenses_start_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="expenses_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="expenses_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="expenses_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn expense_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Expense Distribution Ends-->

<!--Expense Register Starts-->
<div class="container">
    <div class="modal fade" id="gfrm1_exp_register" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupExpenseRegister">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Expense Register Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="exp_register_start_date" class="is_required form-control datefield  capital fromDate" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="exp_register_end_date" class="form-control datefield end_date" readonly="readonly"></div>

                                         <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="exp_register_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="exp_register_property_id" class="form-control"></select>
                                            
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Vendor Name<em class="red-star">*</em></label>
                                             <select multiple name="vendor_id[]" id="exp_register_vendor_id" class="form-control"></select>
                                            
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Bill Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="bill_date" id="exp_register_bill_date" class="is_required form-control datefield  capital fromDate" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                          Paid/Unpaid Amount
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="paid_amount" id="exp_register_bill_date" class="is_required form-control   capital">
                                        </div>

                                         <div class="col-sm-12">
                                            <label is_required="yes">
                                           Reference Number/Check Number
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="reference" id="exp_register_bill_date" class="is_required form-control   capital">
                                        </div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                          Check Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="reference" id="exp_register_bill_date" class="is_required form-control start_date  capital" readonly="readonly">
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn exp_register" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Expense Register Ends-->

<!--General Leger Filter Starts-->
<div class="container">
    <div class="modal fade" id="gmf3_ledger" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupGeneralLedger">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">General Ledger Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="ledger_start_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="ledger_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="ledger_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="ledger_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn ledger_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--General Leger Filter Ends-->
<!--Security Deposit Filter Starts-->
<div class="container">
    <div class="modal fade" id="gmf4_security" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupSecurity">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Security Deposit Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="security_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="security_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn security_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Security Deposit Filter Ends-->

<!--Open Invoice Filter Starts-->
<div class="container">
    <div class="modal fade" id="gmf4_open_invoice" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupOpenInvoice">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Open Invoice Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                       <div class="col-sm-12">
                                            <label is_required="yes">
                                           Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="open_invoice_start_date" class="is_required form-control start_date  capital fromDate" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="open_invoice_end_date" class="form-control datefield end_date" readonly="readonly"></div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="open_invoice_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="open_invoice_property_id" class="form-control"></select>
                                            
                                        </div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant Name<em class="red-star">*</em></label>
                                             <select multiple name="" id="open_invoice_tenant_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn open_invoice_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Open Invoice Filter Ends-->
<!--Aduit Log Filter Starts-->
<div class="container">
    <div class="modal fade" id="gfrm1_aduit_log" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupAduitLog">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Aduit Log Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="from_date" id="aduit_start_date" class="is_required form-control datefield  capital start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="aduit_end_date" class="form-control datefield end_date" readonly="readonly"></div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn aduit_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Aduit Log Filter ends-->

<!--Recurring Journal Entries Filter Starts-->
<div class="container">
    <div class="modal fade" id="gmf4_recurring" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupRecurring">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Recurring Journal Entries Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                     
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="recurring_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="recurring_property_id" class="form-control"></select>
                                            
                                        </div>
                                       
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn recurring_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Recurring Journal Entries Filter Ends-->

<!--Recent Automatic Transaction Starts-->
<div class="container">
    <div class="modal fade" id="gf1_auto_trans" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupRecentAutomatic">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Recent Automatic Transaction</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="automatic_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="automatic_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="automatic_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="automatic_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn automatic_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Recent Automatic Transaction Ends-->

<!--Transaction Detail Bt Account Starts-->
<div class="container">
    <div class="modal fade" id="gf1_transactions" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupExpenseLedger">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Transaction Detail By Account</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="transaction_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="transactions_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="transactions_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="transactions_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn transactions_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Transaction Detail Bt Account Ends-->

<!--Journal Entry Filter Starts-->
<div class="container">
    <div class="modal fade" id="gf1_journal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupJournalEntry">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Journal Entry</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="journal_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="journal_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="journal_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="journal_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn journal_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Journal Entry Filter Ends-->

<!--Deposit Register Starts-->
<div class="container">
    <div class="modal fade" id="gf1_deposit" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupDeposit">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Deposit Register</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="deposit_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="deposit_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn deposit_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Deposit Register Filter Ends-->

<!--Check Register Starts-->
<div class="container">
    <div class="modal fade" id="gf1_check" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupCheck">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Check Register</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="check_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="check_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                       
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn check_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Check Register Filter Ends-->

<!--Annual Budget Comparison Filter Starts-->
<div class="container">
    <div class="modal fade" id="gf1_annual_budget" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupAnnualBudget">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Annual Budget Comparison Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="annual_budget_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="annual_budget_property_id" class="form-control"></select>
                                            
                                        </div>

                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Month
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="annual_budget_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn annual_budget_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Annual Budget Comparison Filter Ends-->

<!--Budget vs Actual Filter Starts-->
<div class="container">
    <div class="modal fade" id="gf1_budget_annual" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupAnnualvsBudget">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Budget vs Actual Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="budget_annual_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="budget_annual_property_id" class="form-control"></select>
                                            
                                        </div>

                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="annual_budget_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn budgets_annual_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!---Budget vs Actual Filter Ends-->

<!--Adjusting Journal Entries Filter Starts-->
<div class="container">
    <div class="modal fade" id="adjusting_journal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupAdjustingJournal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Adjusting Journal Entries Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Account Peroid
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="adjusting_journal_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="adjusting_journal_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="adjusting_journal_property_id" class="form-control"></select>
                                            
                                        </div>     
                                        
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn adjusting_journal_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!---Adjusting Journal Entries Filter Filter Ends-->

<!--Collection Filter Starts-->
<div class="container">
    <div class="modal fade" id="gmf_collection" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupCollection">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Collection Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                       <div class="col-sm-12">
                                            <label is_required="yes">
                                           Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="collection_start_date" class="is_required form-control start_date  capital fromDate" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="collection_end_date" class="form-control datefield end_date" readonly="readonly"></div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="collection_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                         <select multiple name="property[]" id="collection_property_id" class="form-control"></select>
                                            
                                        </div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant Name<em class="red-star">*</em></label>
                                             <select multiple name="Tenant[]" id="collection_tenant_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn collection_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Collection Filter Ends-->

<!-- General Finance Ends -->
<!-- Employees Reports Starts -->
<!-- Payment Plan Consolidated Starts -->
<div class="container">
    <div class="modal fade" id="emp1_ppc" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupPaymentPlans">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Payment Plan Consolidated Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="emp_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                         <div class="col-sm-12">
                                            <label is_required="yes">
                                           End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="emp_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Payer<em class="red-star">*</em></label>
                                            <select  name="payer" id="emp_payer_id" class="form-control">
                                               <option value="">Select</option>
                                               <option value="Vendor">Vendor</option>
                                               <option value="Owner">Owner</option>
                                               <option value="Employee">Employee</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                         <div class="col-sm-12">
                                            <label is_required="yes">Status<em class="red-star">*</em></label>
                                            <select  name="payer" id="emp_payer_id" class="form-control">
                                               <option value="">Select</option>
                                               <option value="Vendor">All</option>
                                               <option value="Owner">Paid-In-Full</option>
                                               <option value="Employee">In-Process</option>
                                             
                                            </select>
                                        </div>
                                     
                                        
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn emp_pay_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Payment Plan Consolidated Ends -->
<!-- Employees Reports Ends -->

<!-- Inventory Reports Starts -->

<!-- Inventory Transfer Starts -->

<div class="container">
    <div class="modal fade" id="ir_model" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupInventoryTransfer">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Inventory Transfer Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div class="col-sm-12">
                                            <label is_required="yes">Inventory Transfer Filter<em class="red-star">*</em></label>
                                            <select  name="payer" id="trans_id" class="form-control">
                                               <option value="">Select</option>
                                               <option value="All">All</option>
                                               <option value="Active">Active</option>
                                               <option value="Inactive">Inactive</option>
                                             
                                            </select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="inventory_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="inventory_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="inventory_property_id" class="form-control"></select>
                                            
                                        </div>     
                                        
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn inventory_transfer_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Inventory Transfer Ends -->

<!-- Supplier Purchase Starts -->

<div class="container">
    <div class="modal fade" id="ir_supplier" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupSupplierPurchase">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Supplier Purchase</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div class="col-sm-12">
                                            <label is_required="yes">Inventory Transfer Filter<em class="red-star">*</em></label>
                                            <select  name="payer" id="trans_id" class="form-control">
                                               <option value="">Select</option>
                                               <option value="All">All</option>
                                               <option value="Active">Active</option>
                                               <option value="Inactive">Inactive</option>
                                             
                                            </select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="supplier_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="supplier_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="supplier_property_id" class="form-control"></select>
                                            
                                        </div>     
                                        
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn supplier_purchase_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Supplier Purchase Ends -->

<!-- Inventory Reports Ends -->

<!-- Property Starts -->

<!-- Vacant Unit Starts -->
<div class="container">
    <div class="modal fade" id="property_vacant" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupVacantUnit">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Vacant Unit Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="vacant_date" class="is_required form-control datefield calander current_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="vacant_portfolio_id" class="form-control"></select>
                                        </div>
                                         <div class="col-sm-12">
                                            <label is_required="yes">Status<em class="red-star">*</em></label>
                                            <select  name="payer" id="vacant_status_id" class="form-control">
                                               <option value="">Select</option>
                                               <option value="All">All</option>
                                               <option value="1">Active</option>
                                               <option value="0">Inactive</option>
                                             
                                            </select>
                                        </div>

                                         <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="vacant_property_id" class="form-control"></select>
                                            
                                        </div> 
                                     
                                        
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn vacant_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Vacant Unit Ends -->
<!-- Property Statement Starts -->
<div class="container">
    <div class="modal fade" id="property_statement" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupPropertyStatement">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Property Statement Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="properties_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="properties_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="properties_portfolio_id" class="form-control"></select>
                                        </div>
                                         <div class="col-sm-12">
                                            <label is_required="yes">Status<em class="red-star">*</em></label>
                                            <select  name="status" id="prop_record_id" class="form-control">
                                               <option value="">Select</option>
                                               <option value="All">All</option>
                                               <option value="1" selected="selected">Active</option>
                                               <option value="0">Inactive</option>
                                             
                                            </select>
                                        </div>

                                         <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="properties_property_id" class="form-control"></select>
                                            
                                        </div> 
                                     
                                        
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn properties_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Property Statement Ends -->
<!-- Unpaid Bill Statement Starts -->
<div class="container">
    <div class="modal fade" id="ps_unbill" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupUnbill">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Unpaid Bills by Property Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                       

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="unbill_portfolio_id" class="form-control"></select>
                                        </div>
                                         <div class="col-sm-12">
                                            <label is_required="yes">Status<em class="red-star">*</em></label>
                                            <select  name="payer" id="pr_unbill_id" class="form-control">
                                               <option value="">Select</option>
                                               <option value="All">All</option>
                                               <option value="1" selected="selected">Active</option>
                                               <option value="0">Inactive</option>
                                             
                                            </select>
                                        </div>

                                         <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="unbill_property_id" class="form-control"></select>
                                            
                                        </div> 
                                          <div class="col-sm-12">
                                            <label is_required="yes">Vendor Name<em class="red-star">*</em></label>
                                             <select multiple name="vendor_id[]" id="unbill_vendor_id" class="form-control"></select>
                                            
                                        </div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">Amount Due<em class="red-star">*</em></label>
                                             <input type="text" name="" id="unbill_amount" class="form-control"></input>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn unbill_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Unpaid Bill Statement Ends -->
<!-- Rental Filter Starts -->
<div class="container">
    <div class="modal fade" id="pr_rental" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupRental">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Rental Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                       <div class="col-sm-12">
                                            <label is_required="yes">
                                           From Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="rental_start_date" class="is_required form-control start_date  capital fromDate" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="rental_end_date" class="form-control datefield end_date" readonly="readonly"></div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">Applicant Name<em class="red-star">*</em></label>
                                            <select multiple name="" id="rental_applicant_id" class="form-control"></select>
                                        </div>
                                            
                                       
                                       
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn rental_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Rental Filter Ends -->
<!-- Property Ends -->



