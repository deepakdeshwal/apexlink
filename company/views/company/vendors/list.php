<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/starability-all.css">
<div id="wrapper">
    <!-- Top navigation start -->
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php"); ?>
    <!-- Top navigation end -->

    <style>
        .closeRating{
            font-size: 21px !important;
            color: #fff !important;
            font-weight: 500 !important;
            border: none !important;
            margin-right: -8px;
        }
    </style>
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/shorttermcss.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-pseudo-ripple.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-nao-calendar.css">
    <link rel="stylesheet" href="https://unpkg.com/balloon-css/balloon.min.css">
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                People &gt;&gt; <span>Vendor Listing</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--Right Navigation Link Starts-->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>PEOPLE</h2>
                            <div class="list-group panel">
                                <a href="/Tenantlisting/add" class="list-group-item list-group-item-success strong collapsed">New Tenant</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong" id="moveout_Search">Move Out</a>
                                <a id ="shorttermrentals" href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Book Now</a>
                                <a href="/People/AddOwners" class="list-group-item list-group-item-success strong collapsed">New Owner</a>
                                <a href="/Vendor/AddVendor" class="list-group-item list-group-item-success strong collapsed">New Vendor</a>
                                <a href="/People/AddEmployee" class="list-group-item list-group-item-success strong collapsed">New Employee</a>
                                <a href="/People/AddContact" class="list-group-item list-group-item-success strong collapsed">New Contact</a>
                                <a href="/Tenant/Receivebatchpayment" class="list-group-item list-group-item-success strong collapsed">Recieve Payment</a>
                                <a href="/Tenant/TenantStatements" class="list-group-item list-group-item-success strong collapsed">Send Tenant Statements</a>
                                <a href="/Tenant/BatchRegister" class="list-group-item list-group-item-success strong collapsed">Bank Deposit</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Bill</a>
                                <a href="/Tenant/InstrumentRegister" class="list-group-item list-group-item-success strong collapsed">Tenant Instrument Register</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Vendor Instrument Register</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Tenant Transfer</a>
                                <a href="/Tenant/Transfer" class="list-group-item list-group-item-success strong collapsed">All Tenant Transfer</a>
                                <a href="/Lease/Movein" class="list-group-item list-group-item-success strong collapsed">All Move In</a>
                                <a href="/Tenant/MoveOut" class="list-group-item list-group-item-success strong collapsed">All Move Out</a>
                                <a href="/Tenant/FormarTenant" class="list-group-item list-group-item-success strong collapsed">Former Tenants</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Owners</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Vendors</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">NSF Listing</a>
                            </div>
                        </div>
                    </div>

                    <!--Right Navigation Link Ends-->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a  href="/Tenantlisting/Tenantlisting">Tenants</a></li>
                            <li role="presentation"><a  href="/People/Ownerlisting">Owners</a></li>
                            <li role="presentation" class="active"><a class="full-width" href="/Vendor/Vendor" >Vendors</a></li>
                            <li role="presentation"><a  href="/People/ContactListt">Contacts</a></li>
                            <li role="presentation"><a  href="/People/GetEmployeeList">Employee</a></li>
                        </ul>

                        <div class="atoz-outer2" style="position: absolute;top: 0px;right: 25px;width: 60%;">
                            <span class="apex-alphabets" id="apex-alphafilter" style="display:none;background:white;">
                            <span class="AtoZ"></span></span><span class="AZ" id="AZ">A-Z</span>
                            <span id="allAlphabet" style="cursor:pointer;">All</span>
                        </div>

                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div role="tabpanel" class="" id="people-vendor">
                                <div class="property-status">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>Vendor Type</label>
                                            <select id="vendor_type" class="common_ddl fm-txt form-control">
                                            </select>

                                        </div>
                                        <div class="col-sm-2">
                                            <label>Status</label>
                                            <select id="grid_status" data-module="VENDOR" class="common_ddl fm-txt form-control jqGridStatusClass">
                                                <option value="all">All</option>
                                                <option value="1">Active</option>
                                                <option value="2">Archive</option>
                                                <option value="3">Former Vendor</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="btn-outer text-right">
                                                <button class="blue-btn" id="export_sample_property_button">Download Sample</button>
                                                <button class="blue-btn" id="import_property_button">Import Vendor</button>
                                                <button class="blue-btn" id="export_property_button">Export Vendor</button>
                                                <a href="/Vendor/AddVendor" class="blue-btn margin-right">New Vendor</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default panel-d" style="display: none;" id="ImportVendor">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class=""><span class="pull-right glyphicon glyphicon-menu-up"></span> Import Vendor </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                        <div class="panel-body">
                                            <div class="row">
                                                <form id="importVendorForm"  enctype="multipart/form-data" >
                                                    <div class="form-outer">
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <input type="file" name="import_file" id="import_file"   accept=".csv, .xls, .xlsx" required/>
                                                            <span class="error"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-12">
                                                        <div class="btn-outer">
                                                            <input type="submit" value="Submit" class="blue-btn" id="save_import_file"/>
                                                            <input  type="button" value="Cancel" class="grey-btn" id="import_property_cancel"/>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive overflow-unset">
                                                                        <table id="vendorTable" class="table table-bordered">
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tabs Ends -->
                </div>
            </div>
        </div>
    </section>
</div>

<div class="overlay">
    <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
        <img width="200"  height="200" src='<?php echo COMPANY_SUBDOMAIN_URL ?>/images/loading.gif'/>
    </div>
</div>
<!--Tool tip label Start-->

<!-- Tool tip label -->
<div class="modal fade" id="PrintEmployeeEnvelope" role="dialog">
    <div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: unset;">
                <a class="close" data-dismiss="modal" href="javascript:;"> X
                </a>
                <input type="button" class="blue-btn pull-left" id="print_building_enevelope" onclick="PrintElem('#employee_print_envelope_content')" onclick="window.print();"  value="Print"/>
                <input type="hidden" id="announcementSys_id" value="">

            </div>
            <div class="modal-body" id="employee_print_envelope_content">
                <div>
                    <div class="left-detail">
                        <ul>
                            <li class="padding-top bold mt-n1" id="user_company_name" >

                            </li>
                            <li class="padding-top bold mt-n1" id="user_address1">

                            </li>
                            <li class="padding-top bold mt-n1" id="user_address2">

                            </li>
                            <li class="padding-top bold mt-n1" id="user_address3">

                            </li >
                            <li class="padding-top bold mt-n1" id="user_address4">

                            </li>
                            <li class="padding-top bold mt-n1">
                                <span id="city" class="city"></span>, <span id="state" class="state" ></span> <span id="postal_code" class="postal_code"> </span>
                            </li>
                        </ul>
                    </div>
                    <div class="right-detail " style=" position:relative; left:+400px;">
                        <ul>
                            <li class="padding-top bold mt-n1" id="" >
                                <span id="employee_last_name"></span> <span id="employee_first_name"></span>
                            </li>
                            <li class="padding-top bold mt-n1" id="employee_address1">

                            </li>
                            <li class="padding-top bold mt-n1" id="employee_address2">

                            </li >
                            <li class="padding-top bold mt-n1" id="employee_address3">

                            </li>
                            <li class="padding-top bold mt-n1" id="employee_address4">

                            </li>
                            <li class="padding-top bold mt-n1">
                                <span id="employee_city" class="employee_city"></span>, <span id="employee_state" class="employee_state" ></span> <span id="employee_postal_code" class="employee_postal_code"> </span>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!--Resign Modal start-->
<div class="modal fade" id="resignReason" role="dialog">
    <div class="modal-dialog modal-sm" style="width:600px;">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: unset;background: #05A0E4;">
                <button type="button" class="bootbox-close-button close closeRating" data-dismiss="modal" aria-hidden="true">×</button>


                <h4 style="padding:5px;color: #fff;font-weight: 500;">Reason for Leaving</h4>
            </div>
            <div class="modal-body" id="reasonResignDiv" style="height:150px;">
                <div class="form-outer">
                    <div class="col-sm-6">
                        <label>Reason for Leaving</label>
                        <select id="ReasonOwner" class="form-control">
                            <option value="0" selected="selected">Select</option>
                            <option value="Cancelled Service">Cancelled Service</option>
                            <option value="Dissatisfied">Dissatisfied</option>
                            <option value="Health Issues">Health Issues</option>
                            <option value="Moved out of State">Moved out of State</option>
                            <option value="Questionable Service">Questionable Service</option>
                            <option value="Retired">Retired</option>
                            <option value="Unprofessional">Unprofessional</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                    <div class="col-sm-6" id="reasonNoteDiv" style="display: none;">
                        <label>Reason Notes</label>
                        <textarea name="reasonNote" rows="4" cols="35" id="reasonNotes"></textarea>
                    </div>
                    <div class="btn-outer">
                        <input type="hidden" id="resignReasonId" value="">
                        <input type="button" id="addResignReason" class="blue-btn" value="Save">
                        <input type="button" data-dismiss="modal" class="grey-btn cancelResign" value="Cancel">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Resign Modal End-->

<!--Star Modal Start-->
<div class="modal fade" id="starDiv" role="dialog">
    <div class="modal-dialog modal-sm" style="width:300px;">
        <div class="modal-content">

            <div class="modal-header" style="border-bottom: unset;background: #05A0E4";>

                <button type="button" class="bootbox-close-button close closeRating" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 style="color:#fff;font-weight:500;font-size: 15px;">Rate this Vendor (<span id="Ratingname"></span>)</h4>
            </div>
            <div class="modal-body" id="starVendorDiv" style="height:110px;">
                <div class="form-outer2">
                    <form id="starForm">
                        <div class="col-sm-12">
                            <fieldset class="starability-growRotate vendorRatingDiv">
                                <input type="radio"  id="rate1" name="rating" value="1" />
                                <label for="rate1" class="faded-star starRatingClass" title="Terrible">
                                    <img src="images/grey_star.png"/>
                                </label>

                                <input type="radio" id="rate2" name="rating" value="2" />
                                <label for="rate2" class="faded-star starRatingClass" title="Not good">2 stars</label>

                                <input type="radio" id="rate3" name="rating" value="3" />
                                <label for="rate3" class="faded-star starRatingClass" title="Average">3 stars</label>

                                <input type="radio" id="rate4" name="rating" value="4" />
                                <label for="rate4" class="faded-star starRatingClass" title="Very good">4 stars</label>

                                <input type="radio" id="rate5" name="rating" value="5" />
                                <label for="rate5" class="faded-star starRatingClass" title="Amazing">5 star</label>
                            </fieldset>
                        </div>
                        <div class="btn-outer text-right">
                            <input type="hidden" id="starVendor_id" value="">
                            <input type="submit" id="addRating" class="blue-btn" value="Save">
                            <input type="button" id="resetRating" class="blue-btn" value="Reset">
                            <input type="button" data-dismiss="modal" class="grey-btn cancelResign" value="Cancel">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="backgroundReport" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content  pull-left" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-left">Background Reports</h4>
                </div>
                <div class="modal-body">
                    <div class="tenant-bg-data">
                        <h3 style="text-align: center;">
                            Open Your Free Account Today!
                        </h3>
                        <p>
                            ApexLink is pleased to partner with Victig Screening Solutions to provide you with
                            the highest quality, timely National Credit, Criminal and Eviction Reports.
                        </p>
                        <div class="grid-outer " style="max-width: 1421px;">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                <tr>
                                    <th colspan="3" align="center" class="bg-Bluehdr">
                                        Package Pricing<br>
                                        Properties Managing
                                    </th>
                                </tr>
                                <tr>
                                    <td width="29%" align="left">
                                        National Criminal
                                    </td>
                                    <td width="29%" align="left">
                                        National Criminal and Credit
                                    </td>
                                    <td width="42%" align="left">
                                        National Criminal, Credit and State Eviction
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        $15
                                    </td>
                                    <td align="left">
                                        $20
                                    </td>
                                    <td align="left">
                                        $25
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        &nbsp;
                                    </td>
                                    <td align="left">
                                        &nbsp;
                                    </td>
                                    <td align="left">
                                        &nbsp;
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <p>
                                *$100 fee to authorize credit report
                            </p>
                        </div>
                        <div class="grid-outer " style="max-width: 1421px;">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                <tr>
                                    <th colspan="5" align="center" class="bg-Bluehdr">
                                        High Volume Users and Larger Properties
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="5" align="left">
                                        <p align="center">
                                            <strong>Contact our Victig Sales Team for custom pricing</strong>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="26%" align="center">
                                        <a href="mailto:sales@victig.com"><strong>sales@victig.com</strong></a>
                                    </td>
                                    <td width="15%" align="center">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td width="21%" align="center">
                                        <strong>866.886.5644 </strong>
                                    </td>
                                    <td width="13%" align="center">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td width="25%" align="center">
                                        <a href="http://www.victig.com" target="_blank"><strong>www.victig.com</strong></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="div-full">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                <tr>
                                    <td width="52%" height="20" style="font-size: 14px;" align="left">
                                        Already have an Account? <a href="https://apexlink.instascreen.net" target="_blank">
                                            Log in here
                                        </a>
                                    </td>
                                    <td width="48%" rowspan="2" align="right">
                                        <a href="https://www.victig.com/wp-content/uploads/2016/05/sample-tenant-report.jpg" target="_blank">
                                            <!--                                            <input value="View Sample Report" class="blue-btn fr" type="button" spellcheck="true" autocomplete="off">-->
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-size: 14px;">
                                        New Users: <a href="https://www.victig.com/victig-on-boarding/" target="_blank">
                                            Click
                                            here to register
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="div-full3">
                            <p>
                                Please be advised that all background services are provided by Victig Screening
                                Solutions. Direct all contact and questions pertaining to background check services
                                to <a href="mailto:sales@victig.com">sales@victig.com</a>, 866.886.5644, <a href="http://www.victig.com" target="_blank">www.victig.com</a>
                            </p>
                        </div>
                        <div class="notice">
                            NOTICE: The use of this system is restricted. Only authorized users may access this
                            system. All Access to this system is logged and regularly monitored for computer
                            security purposes. Any unauthorized access to this system is prohibited and is subject
                            to criminal and civil penalties under Federal Laws including, but not limited to,
                            the Computer Fraud and Abuse Act and the National Information Infrastructure Protection
                            Act.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="backGroundCheckPop" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content  pull-left" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">Disclaimer</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <p style="font-size: 14px; line-height: 20px;"> I authorize ApexLink Inc. to submit this request to generate and present a background/any check report for the Person/Company I have listed. I understand that ApexLink Inc. is not responsible or liable for any data breach, loss/loss of profits or any claim against the filer (Myself or my company) by any party. I understand that in no event is ApexLink Inc. liable for any and all damages. I also understand that ApexLink Inc. grants no implied warranties, including without limitation, warranties, of merchantability, or of fitness for any particular purpose. </p></div>
                    <div class="col-sm-12"><input type="checkbox" name="" class="background_check_open"><label style="display: inline;"> Check to proceed further.</label></div>
                    <div class="col-sm-12 btn-outer mg-btm-20">
                        <input type="button" class="blue-btn"  data-dismiss="modal" value="Cancel">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--<div class="container">
    <div class="overlay">
        <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
            <img width="200"  height="200" src='<?php //echo COMPANY_SUBDOMAIN_URL ?>/images/loading.gif'/>
        </div>
    </div>
    <div class="modal fade" id="financial-info" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>

                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="steps">
                        <ul>
                            <li class="payment_li">
                                <a id="paymentDiv" class="paymentDiv active" href="javascript:;">1</a>
                                <span>Payment</span>
                            </li>
                            <li>
                                <a id="basicDiv" class="basicDiv" href="javascript:;">2</a>
                                <span>Basic</span>
                            </li>
                        </ul>
                    </div>
                    <div class="apx-adformbox-content">

                        <form method="post" id="addVendorCardDetailsForm">
                            <input type="hidden" id="stripe_vendor_id" value="">
                            <input type="hidden" id="company_user_id" value="">
                            <h3>Payment Method <em class="red-star">*</em></h3>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label></label>
                                        <select class="form-control" name="payment_method" id="payment_method">
                                            <option value="1">Credit Card/Debit Card</option>
                                            <option value="2">ACH</option>

                                        </select>
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4 clear">
                                        <label>Card Type<em class="red-star">*</em></label>
                                        <img src="/company/images/card-payment.png" style="width: 190px;">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-4 col-md-4 clear">
                                        <label>Card Holder First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn capital" type="text" id="ffirst_name" onkeypress="return (event.charCode > 64 &&
	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" name="cfirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Card Holder Last Name</label>
                                        <input class="form-control capsOn capital" onkeypress="return (event.charCode > 64 &&
	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type="text" id="flast_name" name="clast_name">
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Card Number <em class="red-star">*</em></label>
                                        <input class="form-control hide_copy" type="text" id="ccard_number" name="ccard_number" maxlength="16">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label> Expiry Year <em class="red-star">*</em></label>
                                        <select class="form-control"  id="cexpiry_year" name="cexpiry_year">
                                        </select>
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4 fphone_num">
                                        <label> Expiry Month <em class="red-star">*</em></label>
                                        <select class="form-control"  id="cexpiry_month" name="cexpiry_month">
                                        </select>
                                        <span class="fphone_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>CVC<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fbirth_date" name="ccvv" maxlength="5">
                                        <span class="fbirth_dateErr error red-star"></span>
                                    </div>
                                  <div class="col-sm-4 col-md-4">
                                      <label>Company <em class="red-star">*</em></label>
                                     <input type="text" class="form-control capital" id="cCompany" name="cCompany" value="">
                                       <span class="fzipcodeErr error red-star"></span>
                                  </div>
                                    <div class="col-sm-4 col-md-4 clear">
                                        <label>Card Billing Phone Number <em class="red-star">*</em></label>
                                        <input type="text" class="form-control phone_format" id="cphoneNumber" name="cphoneNumber" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Card Billing Address Line 1 <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="caddress1" name="caddress1" value="">
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Card Billing Address Line 2</label>
                                        <input type="text" class="form-control capital" id="caddress2" name="caddress2" value="">
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>City<em class="red-star">*</em></label>
                                        <input class="form-control capsOn capital" type="text" id="ccity" name="ccity">
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>State/Province <em class="red-star">*</em></label>
                                        <input type="text" class="form-control cstate capital"  name="cstate" value="">
                                        <span class="fssnErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Zip/Postal Code<em class="red-star">*</em></label>
                                        <input type="text" class="form-control czip_code"  name="czip_code" value="<?php //$_SESSION[SESSION_DOMAIN]['default_zipcode'] ?>" >
                                        <span class="faccount_holderErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Country <em class="red-star">*</em></label>
                                        <input type="text" class="form-control ccountry capital"  name="ccountry" value="">
                                        <span class="faccount_holdertypeErr error red-star"></span>
                                    </div>

                                </div>
                            </div>
                            <div class="row" style="text-align: center;">
                                <button  class="blue-btn">Continue</button>
                            </div>

                        </form>
                    </div>
                    <div class="verification-message"><p></p></div>
                    <div class="basic-payment-detail" style="display: none;">
                        <div class="row">
                            <form id="createAccountIdForm">
                            <div class="form-outer">
                                <div class="col-sm-4 col-md-4">
                                    <label>Country <em class="red-star">*</em></label>
                                    <input class="form-control ccountry capital"  type="text" id="acc_country" name="country">
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>Account Type <em class="red-star">*</em></label>
                                    <input class="form-control account_type"  value="Indiviual" type="text" id="acc_account_type" name="account_type" readonly>
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>Account Number <em class="red-star">*</em></label>
                                    <input class="form-control account_number hide_copy" type="text" id="acc_account_number" name="account_number" maxlength="16">
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>Routing Number <em class="red-star">*</em></label>
                                    <input class="form-control routing_number" type="text" id="acc_routing_number" name="routing_number" maxlength="9">
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>City <em class="red-star">*</em></label>
                                    <input class="form-control ccity capital"  type="text" id="acc_city" name="city">
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>Line1 <em class="red-star">*</em></label>
                                    <input class="form-control line1 capital" type="text" id="acc_line1" name="line1">
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>Address Line2 </label>
                                    <input class="form-control address_line2 capital" type="text" id="acc_address_line2" name="address_line2">
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>POSTAL CODE <em class="red-star">*</em></label>
                                    <input class="form-control postal_code czip_code" type="text" id="acc_postal_code" name="postal_code">
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>State <em class="red-star">*</em></label>
                                    <input class="form-control cstate capital"  type="text" id="acc_state" name="state">
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>Email <em class="red-star">*</em></label>
                                    <input class="form-control email" type="text" id="acc_email" name="email">
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>First_name <em class="red-star">*</em></label>
                                    <input class="form-control first_name capital" type="text" id="acc_first_name" name="first_name">
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>Last_name <em class="red-star">*</em></label>
                                    <input class="form-control last_name capital" type="text" id="acc_last_name" name="last_name">
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>Phone <em class="red-star">*</em></label>
                                    <input class="form-control phone" type="text" id="acc_phone" name="phone">
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>SSN Last 4 <em class="red-star">*</em></label>
                                    <input class="form-control ssn_last" type="text" id="acc_ssn_last" name="ssn_last" maxlength="4">

                                </div>
                                <div class="col-sm-12 date-outer-form">
                                    <label> Date of birth    <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">DATE OF BIRTH</span></div></label>

                                    <div class="col-sm-12 col-md-3 fphone_num  pad-none">
                                        <select class="form-control"  id="fpday" name="day" >
                                            <option value="">Select</option>
                                        </select>
                                        <span class="fmonthErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <select class="form-control"  id="fpmonth" name="month" >
                                            <option value="">Select</option>
                                        </select>
                                        <span class="fmonthErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 date-year">
                                        <select class="form-control"  id="fpyear" name="year" >
                                            <option value="">Select</option>
                                        </select>
                                        <span class="fyearErr error red-star"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center">
                                    <button  id="savefinancial" class="blue-btn">Submit</button>
                                </div>

                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->

<div class="overlay">
    <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
        <img width="200"  height="200" src='<?php echo COMPANY_SUBDOMAIN_URL ?>/images/loading.gif'/>
    </div>
</div>

<div class="modal fade" id="billing-subscription" role="dialog">
    <input type="hidden" id="business_type" name="business_type" value="">
    <input type="hidden" id="account_verification"  value="">
    <input type="hidden" id="stripe_vendor_id" value="">
    <input type="hidden" id="company_user_id" value="">
    <div class="modal-dialog modal-md" style="width: 60%;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>

                <h4 class="modal-title">Online Payment Details<span class='userName'></span></h4>
            </div>
            <div class="modal-body">
                <div class="steps">
                    <ul>
                        <li class="payment_li">
                            <a id="paymentDiv" class="paymentDiv active" href="javascript:;">1</a>
                            <span >Payment</span>
                        </li>
                        <li class="basicDiv_li">
                            <a id="basicDiv" class="basicDiv" href="javascript:;">2</a>
                            <span class="basic_div_span">Basic</span>
                        </li>
                    </ul>
                </div>
                <div class="apx-adformbox-content">
                    <form method="post" id="financialCardInfo" name="financialCardInfo">
                        <h3>Payment Method <em class="red-star">*</em></h3>

                        <div class="row">
                            <div class="col-sm-4 col-md-4 mb-15">
                                <label></label>
                                <select class="form-control setType" name="payment_method" id="payment_method" >
                                    <option value="1">Credit Card/Debit Card</option>
                                    <option value="2">ACH</option>

                                </select>
                                <span class="ffirst_nameErr error red-star"></span>
                            </div>
                        </div>

                        <div class="row cards">
                            <div class="form-outer">
                                <div class="col-sm-4 col-md-4">
                                    <label></label>


                                    <span class="ffirst_nameErr error red-star"></span>
                                </div>
                                <div class="row cards">
                                    <div class="cardDetails table-responsive col-sm-12"></div>
                                    <div class="col-sm-4 col-md-4 clear">
                                        <label>Card Type<em class="red-star">*</em></label>
                                        <img src="/company/images/card-payment.png" style="width: 190px;">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-4 col-md-4 clear">
                                        <label>Card Holder First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn capital" onkeypress="return (event.charCode > 64 &&
	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type="text" id="cfirst_name" name="cfirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Card Holder Last Name</label>
                                        <input class="form-control capsOn capital" onkeypress="return (event.charCode > 64 &&
	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type="text" id="clast_name" name="clast_name">
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Card Number <em class="red-star">*</em></label>
                                        <input class="form-control capsOn hide_copy" type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" id="ccard_number" name="ccard_number" type="number" maxlength="16" placeholder="1234 1234 1234 1234">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label> Expiry Year <em class="red-star">*</em></label>
                                        <select class="form-control"  id="cexpiry_year" name="cexpiry_year" >
                                        </select>
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4 fphone_num">
                                        <label> Expiry Month <em class="red-star">*</em></label>
                                        <select class="form-control"  id="cexpiry_month" name="cexpiry_month" >
                                        </select>
                                        <span class="fphone_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>CVC<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" id="ccvv" name="ccvv" type="number" maxlength="4">
                                        <span class="fbirth_dateErr error red-star"></span>
                                    </div>
                                    <!--                                    <div class="col-sm-4 col-md-4">-->
                                    <!--                                        <label>Company <em class="red-star">*</em></label>-->
                                    <!--                                        <input type="text" class="form-control capital" id="cCompany" name="cCompany" value="">-->
                                    <!--                                        <span class="fzipcodeErr error red-star"></span>-->
                                    <!--                                    </div>-->
                                    <!--                                <div class="col-sm-4 col-md-4 clear">-->
                                    <!--                                    <label>Card Billing Phone Number <em class="red-star">*</em></label>-->
                                    <!--                                    <input type="text" class="form-control phone_format" id="cphoneNumber" name="cphoneNumber" value="" >-->
                                    <!--                                    <span class="fcityErr error red-star"></span>-->
                                    <!--                                </div>-->
                                    <!--                                <div class="col-sm-4 col-md-4">-->
                                    <!--                                    <label>Card Billing Address Line 1 <em class="red-star">*</em></label>-->
                                    <!--                                    <input type="text" class="form-control capital" id="caddress1" name="caddress1">-->
                                    <!--                                    <span class="fstateErr error red-star"></span>-->
                                    <!--                                </div>-->
                                    <!--                                <div class="col-sm-4 col-md-4">-->
                                    <!--                                    <label>Card Billing Address Line 2 </label>-->
                                    <!--                                    <input type="text" class="form-control capital" id="caddress2" name="caddress2" value="">-->
                                    <!--                                    <span class="faddressErr error red-star"></span>-->
                                    <!--                                </div>-->
                                    <!--                                <div class="col-sm-4 col-md-4">-->
                                    <!--                                    <label>City<em class="red-star">*</em></label>-->
                                    <!--                                    <input class="form-control capsOn capital" type="text" id="ccity" name="ccity" readonly>-->
                                    <!--                                    <span class="fbusinessErr error red-star"></span>-->
                                    <!--                                </div>-->
                                    <!--                                <div class="col-sm-4 col-md-4">-->
                                    <!--                                    <label>State/Province <em class="red-star">*</em></label>-->
                                    <!--                                    <input type="text" class="form-control capital" id="cstate" name="cstate" value="" maxlength="4" readonly>-->
                                    <!--                                    <span class="fssnErr error red-star"></span>-->
                                    <!--                                </div>-->
                                    <!--                                <div class="col-sm-4 col-md-4">-->
                                    <!--                                    <label>Zip/Postal Code<em class="red-star">*</em></label>-->
                                    <!--                                    <input type="text" class="form-control" id="czip_code" name="czip_code" value="--><?php //$_SESSION[SESSION_DOMAIN]['default_zipcode'] ?><!--" >-->
                                    <!--                                    <span class="faccount_holderErr error red-star"></span>-->
                                    <!--                                </div>-->
                                    <!--                                <div class="col-sm-4 col-md-4">-->
                                    <!--                                    <label>Country <em class="red-star">*</em></label>-->
                                    <!--                                    <input type="text" class="form-control capital" id="ccountry" name="ccountry" value="" >-->
                                    <!--                                    <span class="faccount_holdertypeErr error red-star"></span>-->
                                    <!--                                </div>-->
                                </div>
                            </div>
                            <div class="row" style="text-align: center;">
                                <button type="submit"  id="savefinancialCard" class="blue-btn">Continue</button>
                            </div>

                        </div>



                    </form>
                    <div class="row accounts" style="display:none;">
                        <div class="accountDetails table-responsive col-sm-12"></div>
                    </div>
                </div>
                <div class="basic-payment-detail" style="display: none;">
                    <div class="row">

                        <form method="post" id="financialCompanyInfo">
                            <input type="hidden" id="company_document_id" name="company_document_id" value="">
                            <div class="form-outer">
                                <div class="col-sm-12 col-md-3">
                                    <label>Country<em class="red-star">*</em></label>
                                    <select class="form-control capital" name="fcountry" readonly="">
                                        <option value="usa" selected>USA</option>
                                    </select>
                                    <span class="fcountryErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>MCC <em class="red-star">*</em></label>
                                    <select class="form-control" id="fcmcc" name="fmcc" readonly="">
                                    </select>
                                </div>

                                <div class="col-sm-12 col-md-3">
                                    <label>URL <em class="red-star">*</em></label>
                                    <input class="form-control capsOn" type="text" id="fcurl" name="furl">
                                    <span class="furlErr error red-star"></span>
                                </div>

                                <div class="col-sm-12 col-md-3">
                                    <label>Business Type<em class="red-star">*</em></label>
                                    <input class="form-control capsOn" type="text" id="fcbusiness" name="fbusiness" readonly>
                                    <span class="fbusinessErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>Account Number<em class="red-star">*</em></label>
                                    <input type="text" class="form-control hide_copy" id="fcaccount_number" name="faccount_number" value="">
                                    <span class="faccount_numberErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>Routing Number<em class="red-star">*</em></label>
                                    <input type="text" class="form-control" id="fprouting_number" name="frouting_number" value="">
                                    <span class="frouting_numberErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>City <em class="red-star">*</em></label>
                                    <input type="text" class="form-control capital" id="fccity" name="fccity" value="" >
                                    <span class="fcityErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>Address 1 <em class="red-star">*</em></label>
                                    <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                    <input class="form-control capital" id="fcaddress" name="fcaddress" >
                                    <span class="faddressErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>Address 2</label>
                                    <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                    <input class="form-control capital" id="fcaddress2" name="fcaddress2" >
                                    <span class="fcaddress2Err error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>Postal Code <em class="red-star">*</em></label>
                                    <input type="text" class="form-control capital" id="fczipcode" name="fczipcode" value=""  maxlength="9">
                                    <span class="fzipcodeErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>State <em class="red-star">*</em></label>
                                    <input type="text" class="form-control capital" id="fcstate" name="fcstate" value="" readonly>
                                    <span class="fstateErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>Name <em class="red-star">*</em></label>
                                    <input type="text" class="form-control capital" id="fcname" name="fcname" value="">
                                    <span class="fcnameErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3 fphone_num">
                                    <label>Phone <em class="red-star">*</em></label>
                                    <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fcphone_number" name="fcphone_number" value="">
                                    <span class="fphone_numberErr error red-star"></span>
                                    <input type="hidden" value="" id="fcphone_number_prefix" name="phone_number_prefix">
                                </div>
                                <div class="col-sm-12 col-md-3 fphone_num">
                                    <label>Tax_id <em class="red-star">*</em></label>
                                    <input   class="form-control" fixedLength="10" id="fctax_id" name="fctax_id" value="">
                                    <span class="fphone_numberErr error red-star"></span>
                                </div>

                                <div class="col-sm-12 col-md-3 fphone_num">
                                    <label>Company Document <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">Upload Company Document here.</span></div></label>
                                    <input type='file' name="company_document" id="company_document" />
                                    <span class="company_document_idErr error red-star"></span>
                                </div>




                            </div>
                            <div class="col-sm-12">
                                <div class="form-hdr">
                                    <h3>Person</h3>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-outer mt-15">
                                    <div class="col-sm-12 col-md-3">
                                        <label>First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn capital" type="text" id="fpfirst_name" name="ffirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Last Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn capital" type="text" id="fplast_name" name="flast_name">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Email <em class="red-star">*</em></label>
                                        <input class="form-control" type="email" id="fpemail" name="femail" >
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fpphone_number" name="fphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="phone_number_prefix" name="phone_number_prefix">
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label> Day <em class="red-star">*</em></label>
                                        <select class="form-control"  id="fpday" name="fday" >
                                        </select>
                                        <span class="fmonthErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Month <em class="red-star">*</em></label>
                                        <select class="form-control"  id="fpmonth" name="fmonth" >
                                        </select>
                                        <span class="fmonthErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Year <em class="red-star">*</em></label>
                                        <select class="form-control"  id="fpyear" name="fyear" >
                                        </select>
                                        <span class="fyearErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fpssn" name="fssn" value="" maxlength="4">
                                        <span class="fssnErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fpaddress" name="faddress" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2</label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fpaddress2" name="faddress2" >
                                        <span class="faddress2Err error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fpzipcode" name="fzipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fpstate" name="fstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                </div>
                                <div class="form-outer">
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fpcity" name="fcity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-12">
                                    <button type="submit"  id="savecompanyfinancial" class="blue-btn">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="basic-user-payment-detail" style="display: none;">
                    <div >
                        <form method="post" id="financialInfo">

                            <div class="form-outer">
                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <label>Country<em class="red-star">*</em></label>
                                        <select class="form-control" id="fcountry" name="fcountry" readonly="">
                                            <option value="usa" selected>USA</option>
                                        </select>
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>MCC <em class="red-star">*</em></label>
                                        <select class="form-control" id="fmcc" name="fmcc" readonly="">
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>URL <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="furl" name="furl">
                                        <span class="furl_nameErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Business Type<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fbusiness" name="fbusiness" readonly>
                                        <!--                                        <select class="form-control" id="fbusiness" name="fbusiness" readonly="">-->
                                        <!--                                            <option value="individual" selected>Individual</option>-->
                                        <!--                                        </select>-->
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Account Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control hide_copy" id="faccount_number" name="faccount_number" value="">
                                        <span class="faccount_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Routing Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="frouting_number" name="frouting_number" value="">
                                        <span class="frouting_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fcity" name="fcity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="faddress" name="faddress" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2</label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="faddress2" name="faddress2" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fzipcode" name="fzipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fstate" name="fstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label> Day <em class="red-star">*</em></label>
                                        <select class="form-control"  id="fday" name="fday" >
                                        </select>
                                        <span class="fmonthErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Month <em class="red-star">*</em></label>
                                        <select class="form-control"  id="fmonth" name="fmonth" >
                                        </select>
                                        <span class="fmonthErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Year <em class="red-star">*</em></label>
                                        <select class="form-control"  id="fyear" name="fyear" >
                                        </select>
                                        <span class="fyearErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Email <em class="red-star">*</em></label>
                                        <input class="form-control" type="email" id="femail" name="femail" >
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="ffirst_name" name="ffirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Last Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="flast_name" name="flast_name">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fphone_number" name="fphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="phone_number_prefix" name="phone_number_prefix">
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fssn" name="fssn" value="" maxlength="4">
                                        <span class="fssnErr error red-star"></span>
                                    </div>

                                </div>
                            </div>
                            <button type="submit"  id="savefinancial" class="blue-btn">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Wrapper Ends -->

<div class="container">
    <div class="modal fade" id="financial-infotype" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>
                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="financial-infotype-radio">
                        <label class="radio-inline">
                            <input type="radio" name="companytype" checked value="individual"> Individual
                        </label>
                        <input type="hidden" value="<?php echo $_SESSION[SESSION_DOMAIN]['cuser_id'] ?>" id="hidden_company_id" >
                        <label class="radio-inline">
                            <input type="radio" name="companytype" value="company" >Company
                        </label>
                    </div>

                    <div class="financial-infotype-btn text-right">
                        <button type="button"  id="savefinancialtype" class="blue-btn">Submit</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="financial-info" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>

                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="apx-adformbox-content" id="financial-indiviual">
                        <form method="post" id="financialInfoIndiviual">
                            <div class="form-outer">
                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <label>Country<em class="red-star">*</em></label>
                                        <select class="form-control" id="fcountry" name="fcountry" readonly="">
                                            <option value="usa" selected>USA</option>
                                        </select>
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fmcc">
                                        <label>MCC <em class="red-star">*</em></label>
                                        <select class="form-control" id="fmcc" name="fmcc" readonly="">
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-3 furl">
                                        <label>URL <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="furl" name="furl">
                                        <span class="furl_nameErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Business Type<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fbusiness" name="fbusiness" value="individual" readonly>
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Account Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control hide_copy" id="faccount_number" name="faccount_number" value="">
                                        <span class="faccount_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Routing Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="frouting_number" name="frouting_number" value="">
                                        <span class="frouting_numberErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="faddress" name="faddress" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2</label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="faddress2" name="faddress2" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fppcity" name="fcity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fppzipcode" name="fzipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fppstate" name="fstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Email <em class="red-star">*</em></label>
                                        <input class="form-control femail" type="email" id="femail" name="femail" >
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="date-outer-form">
                                        <label class="col-sm-12"> Date of birth    <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">DATE OF BIRTH</span></div></label>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <!--                                            <label> Day <em class="red-star">*</em></label>-->
                                            <select class="form-control fpday"  id="fday" name="fday" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fdayErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <!--                                            <label>Month <em class="red-star">*</em></label>-->
                                            <select class="form-control fpmonth"  id="fmonth" name="fmonth" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <!--                                            <label>Year <em class="red-star">*</em></label>-->
                                            <select class="form-control fpyear"  id="fyear" name="fyear" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fyearErr error red-star"></span>
                                        </div>
                                    </div>


                                    <div class="col-sm-12 col-md-3">
                                        <label>First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn fname capital" type="text" id="ffirst_name" name="ffirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Last Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn lname capital" type="text" id="flast_name" name="flast_name">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control fphone phone_number_format" fixedLength="10" id="fphone_number" name="fphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="phone_number_prefix" class="fphonepre" name="phone_number_prefix">
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fssn" name="fssn" value="" maxlength="4">
                                        <span class="fssnErr error red-star"></span>
                                    </div>

                                </div>
                            </div>
                            <button type="submit"  id="savefinancialindi" class="blue-btn">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="modal fade" id="moveout_listSearch" role="dialog">
        <div class="modal-dialog modal-lg unit-new-renovation-form">
            <div class="modal-content pull-left" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Charges</h4>
                </div>

                <div class="modal-body">
                    <div class="form-outer col-sm-12">
                        <form id="new_chargeCode_form">
                            <!-- <input type ="hidden" id="chargeCode_userId" name="chargeCode_userId" value="<?php /*echo $_REQUEST["id"]*/?>"  />-->
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <label>Tenant</label>
                                    <input class="form-control moveout_input" type="text"  placeholder="Click here or start typing the name of the tenant you want to move-out" title="Type in a name">

                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="grid-outer">
                                            <div class="table-responsive" id="tenantData_search">

                                                <!--<table class="table table-hover table-dark" id="myTable">
                                                    <thead>
                                                        <tr class="header">
                                                            <th>Tenant Name</th>
                                                            <th>Phone</th>
                                                            <th>Email</th>
                                                            <th>propertyName</th>
                                                            <th>Unit</th>
                                                            <th>Rent()</th>
                                                            <th>Balance()</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="modal fade" id="financial-company-info" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>

                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="apx-adformbox-content" id="financial-company">
                        <form method="post" id="financialCompanyInfoCom">
                            <input type="hidden" id="company_document_con_id" name="company_document_id" value="">
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-12 col-md-3">
                                        <label>Country<em class="red-star">*</em></label>
                                        <select class="form-control capital" name="country" readonly="">
                                            <option value="usa" selected>USA</option>
                                        </select>
                                        <span class="fcountryErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>MCC <em class="red-star">*</em></label>
                                        <select class="form-control" id="fcmcc" name="fmcc" readonly="">
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>URL <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fcurl" name="furl">
                                        <span class="furlErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Business Type<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fcbusiness" name="fbusiness" value="company" readonly>
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Account Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control hide_copy" id="fcaccount_number" name="faccount_number" value="">
                                        <span class="faccount_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Routing Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fprouting_number" name="frouting_number" value="">
                                        <span class="frouting_numberErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fcaddress" name="fcaddress" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2</label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fcaddress2" name="fcaddress2" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fcczipcode" name="fczipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fcccity" name="fccity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fccstate" name="fcstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Name <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fcname" name="fcname" value="">
                                        <span class="fcnameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fcphone_number" name="fcphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="fcphone_number_prefix" name="phone_number_prefix">
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Tax_id <em class="red-star">*</em></label>
                                        <input   class="form-control" fixedLength="10" id="fctax_id" name="fctax_id" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Company Document <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">Upload Company Document here.</span></div></label>
                                        <input type='file' name="company_document" id="company_document_con" />
                                        <span class="company_document_idErr error red-star"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-hdr">
                                    <h3>Person</h3>
                                </div>
                                <div class="form-outer mt-15">
                                    <div class="col-sm-12 col-md-3">
                                        <label>First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn fname capital" type="text" id="fpfirst_name" name="ffirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Last Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn lname capital" type="text" id="fplast_name" name="flast_name">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Email <em class="red-star">*</em></label>
                                        <input class="form-control femail" type="email" id="fpemail" name="femail" >
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control fphone phone_number_format" fixedLength="10" id="fpphone_number" name="fphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="phone_number_prefix" name="phone_number_prefix" class="fphonepre">
                                    </div>
                                    <div class="date-outer-form">
                                        <label class="col-sm-12"> Date of birth    <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">DATE OF BIRTH</span></div></label>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <!--                                            <label> Day <em class="red-star">*</em></label>-->
                                            <select class="form-control fpday"  id="fpday" name="fday" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fdayErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <!--                                            <label>Month <em class="red-star">*</em></label>-->
                                            <select class="form-control fpmonth"  id="fpmonth" name="fmonth" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <!--                                            <label>Year <em class="red-star">*</em></label>-->
                                            <select class="form-control fpyear"  id="fpyear" name="fyear" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fyearErr error red-star"></span>
                                        </div>
                                    </div>


                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control" id="fpaddress" name="faddress" >
                                        <span class="faddressErr error red-star capital"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fpaddress2" name="faddress2" >
                                        <span class="faddress2Err error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fpppzipcode" name="fzipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fpppcity" name="fcity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fpppstate" name="fstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fpssn" name="fssn" value="" maxlength="4">
                                        <span class="fssnErr error red-star"></span>
                                    </div>
                                </div>

                            </div>
                    </div>
                    <button type="submit"  id="savecompanyfinancialcom" class="blue-btn">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Star Modal End-->

<!-- Wrapper Ends -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script>


    $("#people_top").addClass("active");
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->
    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/vendor/vendorList.js"></script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/payments/plaidIntialisation.js"></script>
<!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/company/people/vendor/onlinePayment.js"></script>-->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<?php include_once(COMPANY_DIRECTORY_URL . "/views/company/tenants/modals.php"); ?>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/fullcal.js" integrity="sha384-vk5WoKIaW/vJyUAd9n/wmopsmNhiy+L2Z+SBxGYnUkunIxVxAv/UtMOhba/xskxh" crossorigin="anonymous"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-pseudo-ripple.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-nao-calendar.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/shortTermRental.js"></script>
<script src="https://js.stripe.com/v3/"></script>
<script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/payments/userSubPayment.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/moveOutCommon.js"></script>