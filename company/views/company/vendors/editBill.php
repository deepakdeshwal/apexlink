<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
$satulation_array =['1'=>'Dr.','2'=>'Mr.','3'=>'Mrs.','4'=>'Mr. & Mrs.','5'=>'Ms.',6=>'Sir',7=>'Madam',8=>'Sister',9=>'Mother'];
$gender_array =['1'=>'Male','2'=>'Female','3'=>'Prefer Not To Say','4'=>'Other'];
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<style>
    .grid-outer select {
        padding: 6px 12px;
    }

    .datagrid-view2 .datagrid-body .datagrid-btable td {
        padding: unset;
    }
    .datagrid-view2 .datagrid-header {
        padding: 5px 0px;
    }
    .panel-htop .combo-panel {
        margin-top: unset;
        margin-left: unset;
    }
    .error {
        width:100% !important;
    }


</style>
<div class="popup-bg"></div>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <!-- New Bill Section Start -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting >> <span>Edit Bill</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-sm-12">

                </div>
                <div class="col-sm-12">
                    <div class="content-section">
                        <div class="main-tabs">
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Edit Bill<a class="back" href="/Accounting/paybills"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                </div>
                                <form id="saveNewBill">
                                    <input name="bill_id" type="hidden" id="bill_id" value="<?php echo $_GET['id'] ?>">
                                    <div class="form-data">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="one-line-form">
                                                    <label>Vendor Name <em class="red-star">*</em></label>
                                                    <span><input class="form-control vendorSelect" placeholder="Vendor Name" type="text"/> <a class="add-icon addVendorModal" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></span>
                                                    <input type="hidden" name="vendor_id" id="vendor_id">
                                                </div>

                                                <div class="one-line-form textarea-form">
                                                    <label>Address <em class="red-star">*</em></label>
                                                    <span>
                                                        <textarea id="vendorAddress" rows="6" name="address" readonly class="form-control"></textarea>
                                                        <a class="pull-right" id="EditVendorAddress" style="display: none;" href="javascript:;">Edit Address</a>
                                                    </span>
                                                </div>
                                                <div class="one-line-form">
                                                    <label>Charge To</label>
                                                    <span>
                                                        <div class="check-outer">
                                                            <input value="1" name="change_to" class="change_to" id="change_to_tenant_radio" type="radio"/>
                                                            <label>Tenant</label>
                                                        </div>
                                                        <div class="check-outer">
                                                            <input value="2" name="change_to" class="change_to" id="change_to_other_radio" type="radio"/>
                                                            <label>Other</label>
                                                        </div>
                                                    </span>
                                                </div>
                                                <div class="one-line-form" style="display: none;" id="change_to_div">
                                                    <label></label>
                                                    <span>
                                                        <input type="hidden" name="tenant_id" value="" id="tenant_id">
                                                        <span id="tenant_div_box"><input name="change_to_other" maxlength="50" class="form-control" id="change_to_other_tenant" type="text"/></span>
                                                        <span id="other_div_box"><input name="change_to_other" maxlength="50" class="form-control" placeholder="Other"  id="change_to_other" type="text"/></span>
                                                    </span>
                                                </div>
                                                <div class="one-line-form">
                                                    <label>Invoice Number</label>
                                                    <span><input name="invoice_number" maxlength="50" class="form-control" placeholder="Invoice Number" type="text"/></span>
                                                </div>
                                                <div class="one-line-form">
                                                    <label>Select Term</label>
                                                    <span>
                                                        <select name="term" class="form-control">
                                                            <option value="">Select</option>
                                                            <option class="clsTerms" value="Net 30">Net 30</option>
                                                            <option class="clsTerms" value="Net 7">Net 7</option>
                                                            <option class="clsTerms" value="Net 10">Net 10</option>
                                                            <option class="clsTerms" value="Net 15">Net 15</option>
                                                            <option class="clsTerms" value="Net 60">Net 60</option>
                                                            <option class="clsTerms" value="Net 90">Net 90</option>
                                                            <option class="clsTerms" value="PIA">PIA</option>
                                                            <option class="clsTerms" value="EOM">EOM</option>
                                                            <option class="clsTerms" value="21 MFI">21 MFI</option>
                                                            <option class="clsTerms" value="Due on Receipt">Due on Receipt</option>
                                                            <option class="clsTerms" value="1% 10 Net 30">1% 10 Net 30</option>
                                                        </select>
                                                    </span>
                                                </div>
                                                <div class="one-line-form">
                                                    <label>Memo</label>
                                                    <span>
                                                        <textarea placeholder="Memo text here ..." name="memo" class="form-control" ></textarea>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="one-line-form">
                                                    <label>Bill Date <em class="red-star">*</em></label>
                                                    <span><input name="bill_date" class="form-control" id="billDate" readonly type="text"/></span>
                                                </div>
                                                <div class="one-line-form">
                                                    <label>Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>) <em class="red-star">*</em></label>
                                                    <span><input name="amount" maxlength="10" class="form-control amount number_only" id="billAmount" placeholder="0.00" type="text"/></span>
                                                </div>
                                                <div class="one-line-form">
                                                    <label>Reference #</label>
                                                    <span><input name="refrence_number" class="form-control" placeholder="Reference Number" type="text"/></span>
                                                </div>
                                                <div class="one-line-form">
                                                    <label>Due Date <em class="red-star">*</em></label>
                                                    <span><input class="form-control" readonly id="dueDate" name="due_date" type="text"/></span>
                                                </div>
                                                <div class="one-line-form">
                                                    <label>Work Order <em class="red-star">*</em></label>
                                                    <span><select name="work_order" multiple="multiple" id="work_order" class="form-control"></select></span>
                                                </div>
                                                <div class="one-line-form">
                                                    <label>Portfolio <em class="red-star">*</em></label>
                                                    <span><select name="portfolio_id" id="portfolio_id" class="form-control"><option></option></select></span>
                                                </div>
                                                <div class="one-line-form">
                                                    <label>Property <em class="red-star">*</em></label>
                                                    <span><select name="property_id" id="property_id" class="form-control"><option value="">Select</option></select></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-outer">
                                            <div class="form-hdr form-black-hdr">
                                                <h3>Add Items <span></span></h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="grid-outer">
                                                    <div class="table-responsive v-middle">
                                                        <table class="table table-hover table-dark">
                                                            <thead>
                                                                <tr>
                                                                    <th>Building</th>
                                                                    <th>Unit Number</th>
                                                                    <th>Account</th>
                                                                    <th>Amount ($)</th>
                                                                    <th>Description</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="appendTable">
                                                                <tr class="tableData">
                                                                    <td width="20%"><select class="form-control building_select customBuildingValidation" data_required="true" name="building[]"><option value="">Select</option></select><span class="error red-star customError"></span></td>
                                                                    <td width="10%"><select class="form-control unit_select customUnitValidation" data_required="true" name="unit[]"><option value="">Select</option></select><span class="error red-star customError"></span></td>
                                                                    <td width="30%"><select class="form-control account_select customAccountValidation" data_required="true" name="account[]"><option value="">Select</option></select><span class="error red-star customError"></span></td>
                                                                    <td width="15%"><input placeholder="0.00" type="text" class="form-control customItemAmountValidation amount_num number_only item_amount" data_required="true" id="item_amount" name="item_amount[]"><span class="error red-star customError"></span></td>
                                                                    <td width="15%"><input placeholder="Description" type="text" class="form-control customItemDescriptionValidation item_description" data_required="true" name="item_description[]"><span class="error red-star customError"></span></td>
                                                                    <td width="10%"><i class="fa fa-times cursor crossRow" aria-hidden="true"></i></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="btn-outer">
                                                    <button type="button" class="blue-btn" id="addNewRow">Add</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Notes <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row" id="note_div">
                                                    <div class="form-outer vendor-notes-clone-divclass" id="vendor-notes-clone-div">
                                                        <div class="col-sm-4">
                                                            <div class="notes_date_right_div">
                                                            <textarea class="notes capital notes_date_right" placeholder="" name="note[]" ></textarea>
                                                            </div>
                                                            <a class="add-icon-textarea vendor-notes-plus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            <a class="add-icon-textarea vendor-notes-minus" style="display: none" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>File Library <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="form-outer2">
                                                    <div class="col-sm-12">
                                                        <button type="button" id="add_libraray_file" class="green-btn">Click Here to Upload</button>
                                                        <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                        <button type="button" class="orange-btn" id="remove_library_file">Remove All Files</button>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row" id="file_library_uploads">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-data">
                                                <div class="grid-outer">
                                                    <div class="apx-table">
                                                        <div class="table-responsive">
                                                            <table id="billLibrary_table" class="table table-hover table-dark">
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-outer text-right">
                                            <button type="submit" class="blue-btn">Update</button>
                                            <button type="button"  class="clear-btn ClearPayBillForm">Reset</button>
                                            <button type="button" id="cancelEditBill" class="grey-btn">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- New Bill Section End -->
</div>

<div id="addVendorModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Vendor</h4>
            </div>
            <div class="modal-body">
                <form id="add_vendor_form">
                    <div class="row">
                        <div class="form-outer">
                            <div class="col-sm-3">
                                <label>Vendor ID <em class="red-star">*</em></label>
                                <input class="form-control vendor_random_id" type="text" value="" readonly  maxlength="6" name="vendor_random_id"/>
                            </div>
                            <div class="col-sm-3">
                                <label>Salutation</label>
                                <select class="form-control" id="salutation" name="salutation">
                                    <option value="">Select</option>
                                    <?php foreach ($satulation_array as $key => $value) { ?>
                                        <option value="<?= $key ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label>First Name</label>
                                <input class="form-control capital" type="text" maxlength="20" name="first_name" id="first_name" placeholder="First Name"/>
                            </div>
                            <div class="col-sm-3">
                                <label>Middle Name</label>
                                <input class="form-control capital" type="text" maxlength="50" name="middle_name" placeholder="Middle Name"/>
                            </div>
                            <div class="col-sm-3">
                                <label>Last Name</label>
                                <input class="form-control capital" type="text" maxlength="20" name="last_name" placeholder="Last Name" id="last_name"/>
                            </div>
                            <div class="col-sm-3">
                                <label>Gender</label>
                                <select id="gender" name="gender" class="form-control">
                                    <option value="">Select</option>
                                    <?php foreach ($gender_array as $key => $value) { ?>
                                        <option value="<?= $key ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label>Entity/Company Name</label>
                                <input class="form-control capital company_name" type="text" placeholder="Eg: Apexlink" maxlength="50" name="company_name"/>
                            </div>
                            <div class="col-sm-3">
                                <label>Phone</label>
                                <input class="form-control phone_number_format" maxlength="12" placeholder="123-456-7890" type="text" name="phone_number" id="phone_number">
                            </div>
                            <div class="col-sm-3">
                                <label>Zip / Postal Code</label>
                                <input name="zipcode" maxlength="9" placeholder="Eg: 10001" id="zip_code" class="form-control capital" type="text" />
                            </div>
                            <div class="col-sm-3">
                                <label>Country</label>
                                <input name="country" maxlength="50" placeholder="Eg: US" id="country" class="form-control capital" type="text" />
                            </div>
                            <div class="col-sm-3">
                                <label>State / Province </label>
                                <input name="state" id="state" maxlength="100" placeholder="Eg: AL"  class="form-control capital states" type="text" />
                            </div>
                            <div class="col-sm-3">
                                <label>City</label>
                                <input name="city" maxlength="100" placeholder="Eg: Huntsville"  class="form-control capital citys" type="text" />
                            </div>
                            <div class="col-sm-3">
                                <label>Address 1</label>
                                <input name="address1" placeholder="Eg: Street Address 1" maxlength="200" id="vAddress1" class="capital form-control address_field" type="text" />
                            </div>
                            <div class="col-sm-3">
                                <label>Address 2</label>
                                <input name="address2" placeholder="Eg: Street Address 2" maxlength="200" id="vAddress2" class="form-control capital address_field" type="text" />
                            </div>
                            <div class="col-sm-3">
                                <label>Address 3</label>
                                <input name="address3" placeholder="Eg: Street Address 3" maxlength="200" id="vAddress3" class="form-control capital address_field" type="text" />
                            </div>
                            <div class="col-sm-3">
                                <label>Email</label>
                                <input class="form-control" type="text" name="email" placeholder="Eg: user@domain.com">
                            </div>
                            <div class="col-sm-3">
                                <label>Vendor Type <a class="pop-add-icon vendortypeplus" onclick="clearPopUps('#Newvendortype')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                <select class="form-control" id='vendor_type_options' name='vendor_type_id'></select>
                                <div class="add-popup" id='Newvendortype' style="width: 127%;">
                                    <h4>Add Vendor Type</h4>
                                    <div class="add-popup-body">

                                        <div class="form-outer">
                                            <div class="col-sm-12">
                                                <label>Vendor Type<em class="red-star">*</em></label>
                                                <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20" name='@vendor_type' id='vendor_type' placeholder="Eg: PMB"/>
                                                <span class="customError required"></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <label> Description <em class="red-star">*</em></label>
                                                <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="50" name='@description' id='vendor_description' placeholder="Eg: Plumber"/>
                                                <span class="customError required"></span>
                                            </div>
                                            <div class="btn-outer text-right">
                                                <input type="submit"  id='NewvendortypeSave' class="blue-btn" value="Save" />
                                                <input type="button"  id='vendortypeReset' class="clear-btn" value="Clear" />
                                                <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Vendor Rate <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> </label>
                                <input class="form-control amount number_only" type="text" id="vendor_rate" name="vendor_rate" placeholder="Vendor Rate"/>
                                <span class='add-icon-span'>/hr </span>
                            </div>
                            <div class="col-sm-3">
                                <label>Referral Source <a class="pop-add-icon additionalReferralResource" href="javascript:;" onclick="clearPopUps('#additionalReferralResource1')"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                <select name="additional_referralSource" id="additional_referralSource" class="form-control"><option>Select</option></select>
                                <div class="add-popup" id="additionalReferralResource1"  style="width: 127%;">
                                    <h4>Add New Referral Source</h4>
                                    <div class="add-popup-body">
                                        <div class="form-outer">
                                            <div class="col-sm-12">
                                                <label>New Referral Source <em class="red-star">*</em></label>
                                                <input class="form-control reff_source1 customReferralSourceValidation capital" data_required="true" type="text" placeholder="New Referral Source">
                                                <span class="customError required"></span>
                                                <span class="red-star" id="reff_source1"></span>
                                            </div>
                                            <div class="btn-outer text-right">
                                                <button type="button" class="blue-btn add_single1" data-validation-class="customReferralSourceValidation" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source1" data-name="additional_referralSource">Save</button>
                                                <input type="button" class="clear-btn clearReferralSource" value="Clear">
                                                <input type="button" class="grey-btn cancelPopup" value="Cancel">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="blue-btn add_vendor_save_update" >Save</button>
                        <button type="button" class="clear-btn Clearform" >Clear</button>
                        <button type="button" class="grey-btn cancel_add_vendor" >Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Modal Popup Ends-->

<!--Vendor Address Popup Start-->
<div id="updateVendorAddress" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Vendor Address</h4>
            </div>
            <div class="modal-body">
                <form id="update_vendor_address_form">
                    <input type="hidden" name="id" id="vendor_address_id" >
                    <div class="row">
                        <div class="form-outer">
                            <div class="col-sm-4">
                                <label>Address 1</label>
                                <input name="address1" placeholder="Eg: Street Address 1" maxlength="200" id="vendorAddress1" class="capital form-control address_field" type="text" />
                            </div>
                            <div class="col-sm-4">
                                <label>Address 2</label>
                                <input name="address2" placeholder="Eg: Street Address 2" maxlength="200" id="vendorAddress2" class="form-control capital address_field" type="text" />
                            </div>
                            <div class="col-sm-4">
                                <label>Address 3</label>
                                <input name="address3" placeholder="Eg: Street Address 3" maxlength="200" id="vendorAddress13" class="form-control capital address_field" type="text" />
                            </div>
                            <div class="col-sm-4">
                                <label>Zip / Postal Code</label>
                                <input name="zipcode" maxlength="9" placeholder="Eg: 10001" id="vendor_zip_code" class="form-control capital" type="text" />
                            </div>
                            <div class="col-sm-4">
                                <label>Country</label>
                                <input name="country" maxlength="50" placeholder="Eg: US" id="vendor_country" class="form-control capital" type="text" />
                            </div>
                            <div class="col-sm-4">
                                <label>State / Province </label>
                                <input name="state" id="vendor_state" maxlength="100" placeholder="Eg: AL"  class="form-control capital states" type="text" />
                            </div>
                            <div class="col-sm-4">
                                <label>City</label>
                                <input name="city" maxlength="100" placeholder="Eg: Huntsville" id="vendor_city"  class="form-control capital citys" type="text" />
                            </div>
                        </div>
                    </div>
                    <div>
                        <button type="submit" class="blue-btn vendor_address_update" >Update</button>
                        <button type="button" class="grey-btn cancel_address_update" >Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Vendor Address Popup Ends-->

<script type="text/javascript">
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var property_unique_id = '';
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var default_name = "<?php echo $_SESSION[SESSION_DOMAIN]['default_name']; ?>";
    var manager_id = "";
    var attachGroup_id = "";
    var credential_notice_period =  "<?php echo $_SESSION[SESSION_DOMAIN]['default_notice_period']; ?>";
    var bill_edit_id =  "<?php echo $_GET['id']; ?>";
    var document_root = "<?php echo $_SERVER['DOCUMENT_ROOT'] ?>";

    $(function () {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function () {
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function () {
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->
    $(document).ready(function () {
        $(".slide-toggle").click(function () {
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function () {
        $(".slide-toggle2").click(function () {
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
    getCompanyDefaultData();
    function getCompanyDefaultData() {
        $.ajax({
            type: 'post',
            url: '/get-company-default-data',
            data: {
                class: 'commonPopup',
                action: 'getCompanyDefaultData'},
            success: function (response) {
                var res = JSON.parse(response);
                $('.citys').val(res.record.city);
                $('.states').val(res.record.state);
                $('#country').val(res.record.country);
                $('#zip_code').val(res.record.zip_code);
            },
        });

    }
    $(document).on('click','.Clearform',function () {
        bootbox.confirm("Do you want to clear this form?", function (result) {
            if (result == true) {
                resetFormClear("#add_vendor_form",['vendor_random_id'],'form',false);
            }
        });
    });

    $(document).on('click','#vendortypeReset',function () {
       $('#vendor_type').val('');
       $('#vendor_description').val('');
    });

    $(document).on('click','.clearReferralSource',function () {
       $('.reff_source1 ').val('');
    });

    $(document).on('click','.ClearPayBillForm',function () {
        bootbox.confirm("Do you want to reset this form?", function (result) {
            if (result == true) {
                location.reload();
            }
        });
    });
</script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.easyui.min.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/autonumeric.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/autonumericDecimal.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/commonPopup.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/additionalMethods.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/newBills/billValidations.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/accountingModule/addVendor.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accountingModule/payBills/addVendor.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/bills/bills.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/vendor/file_library.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/bills/editBills.js" type="text/javascript"></script>
<link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.easyui.css" rel="stylesheet">
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
