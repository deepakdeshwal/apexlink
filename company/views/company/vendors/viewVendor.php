<?php
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];

include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<style>
    .birth_date_color{
        background: none !important;
    }
    .multipleEmail input{margin:5px auto; }
    select[name=hobbies] + .btn-group button{
        margin-top: 0px;
    }
    .closeimagepopupicon{
        float: right;
        font-weight: bold;
        border: 2px solid #666;
        border-radius: 12px;
        padding: 0px 4px;
        cursor: pointer;
    }
    .fa-minus-circle {
        display: none;
    }

    .apx-inline-popup {
        position: relative;
    }

    .apx-inline-popup-box {
        position: absolute;
        top: 0;
        left: 0;
        background: #fff;
        z-index: 2;
        border: 1px solid #ddd;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
        padding: 15px;
        width: 100%;
    }

    .apx-inline-popup-box > h4 {
        font-size: 12px
    }

    .image-editor {
        text-align: center;
    }

    .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 5px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 250px;
        height: 250px;
        display: inline-block;
    }

    .cropit-preview-image-container {
        cursor: move;
    }

    .cropit-preview-background {
        opacity: .2;
        cursor: auto;
    }

    .image-size-label {
        margin-top: 10px;
    }

    .export {
        /* Use relative position to prevent from being covered by image background */
        position: relative;
        z-index: 10;
        display: block;
    }

    .image-editor input[type="file"] {
        opacity: 0;
        position: absolute;
        top: 45px;
        left: 91px;
    }

    .upload-logo .img-outer img {
        max-width: 100%;
    }

    .choose-img {
        margin: 0 !important;
    }



    .vehicle_image1, .vehicle_image2, .vehicle_image3 {
        width: 100px;
        height: 100px;
    }

    .vehicle_image1 img, .vehicle_image2 img, .vehicle_image3 img {
        width: 100%;
    }
</style>

<div class="popup-bg"></div>
<div id="wrapper">
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <div class="container">
        <div class="modal fade" id="print_complaint" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="blue-btn" id='email_complaint' onclick="sendComplaintsEmail('#modal-body-complaints')">Email</button>
                        <button type="button" class="blue-btn" id='print_complaints'  onclick="PrintElem('#modal-body-complaints')" onclick="window.print();">Print</button>
                        <button type="button" class="close" data-dismiss="modal">×</button>

                    </div>
                    <div class="modal-body" id="modal-body-complaints" style="height: 380px; overflow: auto;">

                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                People >> <span>View Vendor</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text">
                            </div>
                        </div>
                    </div>
                </div>

                <input type="hidden" id="building_property_id" val=""/>
                <input type="hidden" value="<?php echo $_GET['id']; ?>" name="vendor_id" class="vendor_id">
                <div class="col-sm-12">
                    <div class="content-section">
                        <!--Tabs Starts -->
                        <!--- Right Quick Links ---->
                        <div class="right-links-outer hide-links">
                            <div class="right-links">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </div>
                            <div id="RightMenu" class="box2">
                                <h2>PEOPLE</h2>
                                <div class="list-group panel">
                                    <a href="/Tenantlisting/add" class="list-group-item list-group-item-success strong collapsed">New Tenant</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Move Out</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Book Now</a>
                                    <a href="/People/AddOwners" class="list-group-item list-group-item-success strong collapsed">New Owner</a>
                                    <a href="/Vendor/AddVendor" class="list-group-item list-group-item-success strong collapsed">New Vendor</a>
                                    <a href="/People/AddEmployee" class="list-group-item list-group-item-success strong collapsed">New Employee</a>
                                    <a href="/People/AddContact" class="list-group-item list-group-item-success strong collapsed">New Contact</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Recieve Payment</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Send Tenant Statements</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Bank Deposite</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Bill</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Tenant Instrument Register</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Vendor Instrument Register</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Tenant Transfer</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Tenant Transfer</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Move In</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Move Out</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Tenants</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Owners</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Vendors</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">NSF Listing</a>
                                </div>
                            </div>
                        </div>
                        <!--- Right Quick Links ---->

                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"><a href="/Tenantlisting/Tenantlisting">Tenants</a></li>
                                <li role="presentation"><a href="/People/Ownerlisting">Owners</a></li>
                                <li role="presentation" class="active"><a href="/Vendor/Vendor">Vendors</a></li>
                                <li role="presentation"><a href="/People/ContactListt">Contacts</a></li>
                                <li role="presentation"><a href="/People/GetEmployeeList">Employee</a></li>
                            </ul>
                            <!-- Nav tabs -->
                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane active" id="guest-cards">

                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Vendor Information <a onclick="goBack()" href="/Vendor/Vendor/" class="back" style="cursor: pointer;"><i class="fa fa-angle-double-left" aria-hidden="true" ></i> Back</a></h3>
                                        </div>
                                        <div class="form-data pad-none">
                                            <div class="grey-detail-box">
                                                <div class="col-sm-12">
                                                    <div class="col-xs-3">
                                                        <label class="text-right">Vendor Name :</label>
                                                        <span id="Vendorname" class="vendor_name"></span>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <label class="text-right">Vendor ID :</label>
                                                        <span class="vendor_id"></span>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <label class="text-right">Total Payables :</label>
                                                        <span class="owner_note_for_Phone"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grey-detail-box">
                                                <div class="col-sm-12">
                                                    <div class="col-xs-3">
                                                        <label class="text-right">YTD Payment :</label>
                                                        <span class="owner_note_for_Phone"></span>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <label class="text-right">Open Work Orders :</label>
                                                        <span class="owner_Email"></span>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <label class="text-right">Phone :</label>
                                                        <span class="Phone"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Form Outer Ends -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="vendorTab" data_tab="" id="generalTab" class="active"><a href="#people-vendor-one" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">General</a></li>
                                    <li role="presentation" class="vendorTab" data_tab="" id="payablesTab"><a href="#people-vendor-two" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Payables</a></li>
                                    <li role="presentation" class="vendorTab" data_tab="" id="ledgerTab"><a href="#people-vendor-three" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Ledger</a></li>
                                    <li role="presentation" class="vendorTab" data_tab="" id="paymentTab"><a href="#people-vendor-four" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Payment Settings</a></li>
                                    <li role="presentation" class="vendorTab" data_tab="vendor_table" id="notesTab"><a href="#people-vendor-five" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Notes & History</a></li>
                                    <li role="presentation" class="vendorTab" data_tab="fileLibraryTable" id="libraryTab"><a href="#people-vendor-six" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">File Library</a></li>
                                    <li role="presentation" class="vendorTab" data_tab="" id="workTab"><a href="#people-vendor-seven" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Work Orders</a></li>
                                    <li role="presentation" class="vendorTab" data_tab="" id="portalTab"><a href="#people-vendor-eight" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Vendor Portal</a></li>
                                    <li role="presentation" class="vendorTab" data_tab="vendor-complaints" id="complaintsTab"><a href="#people-vendor-nine" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Vendor Complaints</a></li>
                                    <li role="presentation" class="vendorTab" data_tab="vendorFlag-table" id="flagTab"><a href="#people-vendor-flagbank" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Flag Bank</a></li>
                                </ul>

                                <div class="tab-content">
                                    <input type="hidden" class="vendor_edit_id" value="<?= $_GET['id']; ?>">
                                    <div role="tabpanel" class="tab-pane active" id="people-vendor-one">

                                        <div class="form-data" id="GenVendorOne">
                                            <div class="detail-outer" id="general1st">
                                                <div class="form-outer form-outer2">
                                                    <div class="form-hdr">
                                                        <h3>General Information</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="detail-outer">
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <?php $dummypath = 'http://' . $_SERVER['HTTP_HOST'] . '/company/images/dummy-img.jpg'; ?>
                                                                    <div class="vendor_image img-outer">
                                                                        <img src="<?php echo $dummypath; ?>">

                                                                    </div>
                                                                    <div align="center"><h5><span class="name"></span></h5></div>
                                                                    <!--    <img src="uploads/<?php /* echo $row['pic_name']; */ ?>">-->


                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="col-xs-12">
                                                                        <label class="text-right"> Salutation :</label>
                                                                        <span class="salutation"></span>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <label class="text-right"> Middle Name :</label>
                                                                        <span class=" middle_name "></span>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <label for="maiden_name" class="text-right">Maiden Name :</label>
                                                                        <span class="maiden_name"></span>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <label class="text-right">Gender  :</label>
                                                                        <span class="Gender"></span>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <label class="text-right">Entity/Company Name :</label>
                                                                        <span class="company_name"></span>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <label class="text-right">Vendor ID  :</label>
                                                                        <span class="vendor_id"></span>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <label class="text-right">Hobbies :</label>
                                                                        <span class="Hobbies"></span>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <label class="text-right">MaritalStatus :</label>
                                                                        <span class="vendor_marital_status"></span>
                                                                    </div>

                                                                    <div class="col-xs-12">
                                                                        <label class="text-right">Veteran Status :</label>
                                                                        <span class="vendor_vetran_status"></span>
                                                                    </div>

                                                                    <div class="col-xs-12">
                                                                        <label class="text-right">DOB :</label>
                                                                        <span class="vendor_dob"></span>
                                                                    </div>

                                                                </div>
                                                                <div class="col-sm-5">
                                                                    <div class="col-xs-12">
                                                                        <label class="text-right">First Name :</label>
                                                                        <span class="First_Name"></span>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <label class="text-right"> Last Name :</label>
                                                                        <span class=" Last_Name"></span>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <label class="text-right"> Nickname :</label>
                                                                        <span class="Nickname"></span>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <label class="text-right"> Name on Check :</label>
                                                                        <span class=" Name_on_Check"></span>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <label class="text-right">Notes for this Phone Number :</label>
                                                                        <span class="Notes_for_Phone"></span>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <label class="text-right">Vendor Type :</label>
                                                                        <span class="Vendor_Type"></span>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <label class="text-right">Vendor Rate :</label>
                                                                        <span class="Vendor_Rate"></span>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <label class="text-right">SSN/SIN/ID :</label>
                                                                        <span class="SSN"></span>
                                                                    </div>

                                                                    <div class="col-xs-12">
                                                                        <label class="text-right">Ethnicity :</label>
                                                                        <span class="Ethnicity"></span>
                                                                    </div>



                                                                </div>

                                                            </div>

                                                        </div>

                                                        <div class="edit-foot">
                                                            <a href="javascript:;" class="edit_redirectionTab1">
                                                              <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                                Edit
                                                            </a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- Form Outer Ends -->

                                            <!-- Form Outer starts -->
                                            <div id="general2nd" class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Contact Information</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="detail-outer">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="col-xs-6">
                                                                    <label class="text-right"> Zip / Postal Code :</label>
                                                                    <span class="Postal_Code"></span>
                                                                </div>
                                                                <div class="col-xs-6">
                                                                    <label class="text-right">Country :</label>
                                                                    <span class="Country"></span>
                                                                </div>

                                                                    <div class="col-xs-6">
                                                                        <label class="text-right"> State / Province :</label>
                                                                        <span class="State"></span>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <label class="text-right">City :</label>
                                                                        <span class="City"></span>
                                                                    </div>

                                                                    <div class="col-xs-6">
                                                                        <label class="text-right">Address :</label>
                                                                        <span class="Address"></span>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <label class="text-right">Website :</label>
                                                                        <span class="Website"></span>
                                                                    </div>
                                                                    <div class="col-xs-6">

                                                                    </div>

                                                                    <div class="col-xs-6">
                                                                        <label class="text-right">Email :</label>
                                                                        <span class="email"></span>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <label class="text-right">Phone :</label>
                                                                        <span class="Phone"></span>
                                                                    </div>
                                                                    <div class="col-xs-6">

                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="edit-foot">
                                                        <a class="edit_redirectionTab1" href="javascript:;">
                                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Form Outer Ends -->

                                            <!-- Form Outer starts -->
                                            <div id="general3rd" class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Emergency Contact Details</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="detail-outer">
                                                        <div class="viewEmergency"></div>
                                                    </div>
                                                    <div class="edit-foot">
                                                        <a class="edit_redirectionTab1" href="javascript:;">
                                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>


                                            <!-- Form Outer Ends -->

                                            <!-- Form Outer starts -->
                                            <div id="general4th" class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3> Vendor Credential Control</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="detail-outer">
                                                        <div class="viewCredential"></div>
                                                    </div>
                                                    <div class="edit-foot">
                                                        <a  href="javascript:;" class="edit_redirectionTab1">
                                                           <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="general5th" class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Custom Fields</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="col-sm-7 custom_field_html_view_mode" readonly="readonly" >
                                                    </div>


                                                    <div class="edit-foot">
                                                        <a  href="javascript:;" class="edit_redirectionTab1">
                                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Form Outer Ends -->
                                        </div>

<!--                                        edit view div -->
                                        <div class="form-outer" id="editViewLinkTab1" style="display: none">
                                        <form id="updateFormSaveNewVendor" method="post">
                                            <div  class="form-hdr"><h3>General Information</h3></div>
                                            <div id="#general1st" class="form-data">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label>Upload Picture</label>
                                                        <div class="upload-logo">
                                                            <div class="tenant_image img-outer" id="tenant_image_id"><img src="<?php echo COMPANY_SITE_URL ?>/images/dummy-img.jpg"></div>
                                                            <a href="javascript:;">
                                                                <i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Image</a>
                                                            <span>(Maximum File Size Limit: 1MB)</span>
                                                        </div>
                                                        <div class="image-editor">
                                                            <input type="file" class="cropit-image-input" name="tenant_image">
                                                            <div class='cropItData' style="display: none;">

                                                                <span class="closeimagepopupicon">X</span>
                                                                <div class="cropit-preview"></div>
                                                                <div class="cropit-rotate">
                                                                    <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                    </a>
                                                                    <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="image-size-label">Resize image</div>
                                                                <input type="range" class="cropit-image-zoom-input">

                                                                <input type="hidden" name="image-data" class="hidden-image-data"/>
                                                                <input type="button" id="saveVendorImage" class="export" value="Done"
                                                                       data-val="tenant_image">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <label>Salutation</label>
                                                                <select class="form-control" id="salutation" name="salutation">
                                                                    <option value="">Select</option>
                                                                    <?php foreach ($satulation_array as $key => $value) { ?>

                                                                        <option value="<?= $key ?>"><?php echo $value; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>First Name <em class="red-star">*</em></label>
                                                                <input class="form-control capital" type="text" maxlength="20" name="first_name" id="first_name" placeholder="First Name" value=""/>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Middle Name</label>
                                                                <input class="form-control capital" type="text" maxlength="50" name="middle_name" placeholder="Middle Name" id="middle_name"/>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Last Name <em class="red-star">*</em></label>
                                                                <input class="form-control capital" type="text" maxlength="20" name="last_name" placeholder="Last Name" id="last_name"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-2 hidemaiden">
                                                                <label>Maiden Name</label>
                                                                <input class="form-control capital" type="text" maxlength="20" name="maiden_name" placeholder="Maiden Name" id="maiden_name"/>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Nick Name</label>
                                                                <input class="form-control capital" type="text" maxlength="20" name="nick_name" placeholder="Nick Name" id="nick_name"/>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Gender</label>
                                                                <select id="gender" name="gender"   class="form-control"><option value="">Select</option>
                                                                    <?php foreach ($gender_array as $key => $value) { ?>
                                                                        <option value="<?= $key ?>"><?php echo $value; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Vendor Rate <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> </label>
                                                                <input class="form-control amount number_only" type="text" id="vendor_type_options" name="vendor_rate" placeholder="Vendor Rate" value=""/>
                                                                <span class='add-icon-span'>/hr </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <label>Vendor ID <em class="red-star">*</em></label>
                                                                <input class="form-control vendor_random_id" type="text" value=""  maxlength="6" name="vendor_random_id" id="vendor_random_id"/>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Entity/Company Name</label>
                                                                <input class="form-control capital" type="text" value="" placeholder="Eg: Apexlink" maxlength="50" name="company_name" id="compname"/>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Name on Check</label>
                                                                <input class="form-control capital" name="name_on_check" type="text" id="name_on_check" placeholder="Name on Check"/>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="check-outer">
                                                                    <input type="checkbox" name="is_pmc" class="is_pmc" id="is_pmc"/>
                                                                    <label class="blue-label">Is PMC</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <label>Vendor Type <a class="pop-add-icon vendortypeplus" onclick="clearPopUps('#Newvendortype')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select class="form-control" id='vendor_type_options' name='vendor_type_id'>




                                                                </select>
                                                               <div class="add-popup" id='Newvendortype'>
                                                                    <h4>Add Vendor Type</h4>
                                                                    <div class="add-popup-body">

                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>Vendor Type<em class="red-star">*</em></label>
                                                                                <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20" name='@vendor_type' id='vendor_type' placeholder="Eg: PMB"/>
                                                                                <span class="customError required"></span>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <label> Description <em class="red-star">*</em></label>
                                                                                <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="50" name='@description' id='vendor_description' placeholder="Eg: Plumber"/>
                                                                                <span class="customError required"></span>
                                                                            </div>
                                                                            <div class="btn-outer">
                                                                                <input type="submit"  id='NewvendortypeSave' class="blue-btn" value="Save" />
                                                                                <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Birth Date</label>
                                                                <input class="form-control birth_date_color" type="text" placeholder="Birth Date" id="birth_date" name="dob" readonly/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="entity-company-check-div">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <label>Ethnicity <a class="pop-add-icon ethnicityplus" href="javascript:;" onclick="clearPopUps('#Newethnicity')"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>

                                                                    <select class="form-control" id='ethnicity_options' name='ethnicity'> <option value="0">Select</option></select>
                                                                    <div class="add-popup" id='Newethnicity'>
                                                                        <h4>Add New Ethnicity</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>New Ethnicity<em class="red-star">*</em></label>
                                                                                    <input class="form-control capital customValidateEthnicity" data_required="true" data_max="150" type="text" name="@title" placeholder="Add New Ethnicity"/>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                                <div class="btn-outer">
                                                                                    <input type="submit" class="blue-btn" id="NewethnicitySave" value="Save" />
                                                                                    <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Martial Status
                                                                        <a class="pop-add-icon selectPropertyMaritalStatus" href="javascript:;" onclick="clearPopUps('#selectPropertyMaritalStatus1')">
                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                        </a>
                                                                    </label>
                                                                    <select class="form-control" name="maritalStatus" id="maritial_status">
                                                                        <option value="0">Select</option>
                                                                    </select>
                                                                    <div class="add-popup" id="selectPropertyMaritalStatus1">
                                                                        <h4>Add New Marital Status</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>New Marital Status <em class="red-star">*</em></label>
                                                                                    <input class="form-control maritalstatus_src customMaritalValidation capital" type="text" placeholder="Add New Marital Status" data_required="true">
                                                                                    <span class="customError required red-star" aria-required="true" id="maritalstatus_src"></span>
                                                                                </div>
                                                                                <div class="btn-outer">
                                                                                    <button type="button" class="blue-btn add_single1" data-table="tenant_marital_status" data-validation-class="customMaritalValidation" data-cell="marital" data-class="maritalstatus_src" data-name="maritalStatus">Save</button>
                                                                                    <input type="button" class="grey-btn" value="Cancel">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Hobbies
                                                                        <a class="pop-add-icon selectPropertyHobbies" href="javascript:;" onclick="clearPopUps('#selectPropertyHobbies1')">
                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                        </a>
                                                                    </label>
                                                                    <select class="form-control" name="hobbies[]"  id="hobbies" multiple>
                                                                        <option value="0">Select</option>
                                                                    </select>
                                                                    <div class="add-popup" id="selectPropertyHobbies1">
                                                                        <h4>Add New Hobbies</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>New Hobbies <em class="red-star">*</em></label>
                                                                                    <input class="form-control hobbies_src customHobbiesValidation capital" type="text"  placeholder="Add New Hobbies" data_required="true">
                                                                                    <span class="customError required red-star" aria-required="true" id="hobbies_src"></span>
                                                                                </div>
                                                                                <div class="btn-outer">
                                                                                    <button type="button" class="blue-btn add_single1" data-table="hobbies" data-cell="hobby" data-validation-class="customHobbiesValidation" data-class="hobbies_src" data-name="hobbies">Save</button>
                                                                                    <input type="button" class="grey-btn" value="Cancel">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Veteran Status
                                                                        <a class="pop-add-icon selectPropertyVeteranStatus" href="javascript:;" onclick="clearPopUps('#selectPropertyVeteranStatus1')">
                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                        </a>
                                                                    </label>
                                                                    <select class="form-control" name="veteranStatus" id="veteran_status">
                                                                        <option value="">Select</option>
                                                                    </select>
                                                                    <div class="add-popup" id="selectPropertyVeteranStatus1">
                                                                        <h4>Add New VeteranStatus</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>New VeteranStatus <em class="red-star">*</em></label>
                                                                                    <input class="form-control veteran_src customVeteranValidation" type="text" placeholder="Add New VeteranStatus" data_required="true">
                                                                                    <span class="customError required red-star" aria-required="true" id="veteran_src"></span>
                                                                                </div>
                                                                                <div class="btn-outer">
                                                                                    <button type="button" class="blue-btn add_single1" data-table="tenant_veteran_status" data-validation-class="customVeteranValidation" data-cell="veteran" data-class="veteran_src" data-name="veteranStatus">Save</button>
                                                                                    <input type="button" class="grey-btn" value="Cancel">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="ssnMainVendor"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <label>Website </label>
                                                                <input placeholder="Eg: http://www.apexlink.com" maxlength="50" id="website" class="form-control" type="text" name="website"/>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <label>Referral Source <a class="pop-add-icon additionalReferralResource" href="javascript:;" onclick="clearPopUps('#additionalReferralResource1')"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select name="additional_referralSource" id="additional_referralSource" class="form-control"><option>Select</option></select>
                                                                <div class="add-popup" id="additionalReferralResource1">
                                                                    <h4>Add New Referral Source</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>New Referral Source <em class="red-star">*</em></label>
                                                                                <input class="form-control reff_source1 customReferralSourceValidation capital" data_required="true" type="text" placeholder="New Referral Source">
                                                                                <span class="customError required"></span>
                                                                                <span class="red-star" id="reff_source1"></span>
                                                                            </div>
                                                                            <div class="btn-outer">
                                                                                <button type="button" class="blue-btn add_single1" data-validation-class="customReferralSourceValidation" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source1" data-name="additional_referralSource">Save</button>
                                                                                <input type="button" class="grey-btn" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <label>Address 1</label>
                                                                <input name="address1" placeholder="Eg: Street Address 1" maxlength="200" id="address123" class="capital form-control" type="text" value=""/>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <label>Address 2</label>
                                                                <input name="address2" placeholder="Eg: Street Address 2" maxlength="200" id="address2edit" class="form-control capital" type="text" />
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <label>Address 3</label>
                                                                <input name="address3" placeholder="Eg: Street Address 3" maxlength="200" id="address3edit" class="form-control capital" type="text" />
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <label>Address 4</label>
                                                                <input name="address4" placeholder="Eg: Street Address 4" maxlength="200" id="address4edit" class="form-control capital" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="phonemainIdDiv"></div>

                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <label>Zip / Postal Code </label>
                                                                <input name="zipcode" maxlength="9" placeholder="Eg: 10001" id="zipcode" class="form-control capital" type="text" />
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <label>Country </label>
                                                                <input name="country" maxlength="50" placeholder="Eg: US" id="country" class="form-control capital" type="text" />
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <label>State / Province </label>
                                                                <input name="state" maxlength="100" placeholder="Eg: AL"  class="form-control capital states" type="text" />
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <label>City </label>
                                                                <input name="city" maxlength="100" placeholder="Eg: Huntsville"  class="form-control capital citys" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <label>Add Note for this Phone Number </label>
                                                                <div class="notes_date_right_div">
                                                                    <textarea placeholder="Add Note for this Phone Number" name="phone_number_note" maxlength="500" rows="2" class="capital form-control notes_date_right" id="phone_number_note"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="emailGetDiv"></div>
                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="form-outer"></div>
                                                <!-- Form Outer Ends -->

                                                <div id="general3rd" class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>Emergency Contact Details</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="emergencyDetailMainDiv">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Form Outer Ends -->


                                                <div id="general4th" class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>Vendor Credential Control</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="credentialMainDiv"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Form Outer Ends -->

                                                <div id="general5th" class="form-outer2">
                                                    <div class="form-hdr">
                                                        <h3>Custom Fields</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12">
                                                                    <div class="custom_field_html">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3 text-right pull-right min-height-0">
                                                                    <div class="btn-outer min-height-0">
                                                                        <button type="button"  id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">New Custom Field</button>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Form Outer Ends -->
                                                <div class="btn-outer text-right">
                                                    <input type="submit" id="btnSaveNewVendor" class="blue-btn" value="Update">
                                                    <input type="button"  class="grey-btn vendorGeneralInformation" value="Reset">
                                                    <input type="button" class="grey-btn  btnSaveNewVendorCancel"  value="Cancel">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
<!--                                        edit view div end-->
                                    </div><!--tab four starts-->


                                    <div role="tabpanel" class="tab-pane" id="people-vendor-two">
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Pay Bills</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-sm-1 text-right">
                                                        <label>Show Bills</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="check-outer clear">
                                                            <input name="alt" id="dueBefore" checked class="searchRadio" value="2" type="radio">
                                                            <label>Due on or before</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2" id="dateFilterDiv">
                                                        <div class="check-outer">
                                                            <input class="form-control" readonly id="searchDate">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 text-right" style="margin-top: 10px;">
                                                        <label>Select Net Due Limit</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select id="termNet" class="form-control">
                                                            <option value="all">Select</option>
                                                            <option class="clsTerms" value="Net 30">Net 30</option>
                                                            <option class="clsTerms" value="Net 7">Net 7</option>
                                                            <option class="clsTerms" value="Net 10">Net 10</option>
                                                            <option class="clsTerms" value="Net 15">Net 15</option>
                                                            <option class="clsTerms" value="Net 60">Net 60</option>
                                                            <option class="clsTerms" value="Net 90">Net 90</option>
                                                            <option class="clsTerms" value="PIA">PIA</option>
                                                            <option class="clsTerms" value="EOM">EOM</option>
                                                            <option class="clsTerms" value="21 MFI">21 MFI</option>
                                                            <option class="clsTerms" value="Due on Receipt">Due on Receipt</option>
                                                            <option class="clsTerms" value="1% 10 Net 30">1% 10 Net 30</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <button class="blue-btn btn-block" id="searchBillTable">Search</button>
                                                    </div>
                                                </div>
                                                <div class="panel-body pad-none">
                                                    <div class="grid-outer">
                                                        <div class="apx-table">
                                                            <div class="table-responsive">
                                                                <table id="paybills_table" class="table table-hover table-dark">
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid-outer">
                                                    <form id="paymentForm">
                                                        <input type="hidden" name="vendor_id" id="userData" value="">
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-2">
                                                                    <label>Date</label>
                                                                    <input type="text" name="date" class="form-control" id="selectDate" readonly placeholder="Select Date">
                                                                </div>
                                                                <div class="col-sm-2" id="bankAccount" style="display: none;">
                                                                    <label>Bank <em class="red-star">*</em></label>
                                                                    <select name="bank" id="bankName" class="form-control">
                                                                        <option value="">Select Bank</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Payment Type <em class="red-star">*</em></label>
                                                                    <select id="payment_type" name="payment_type" class="form-control">
                                                                        <option value="">Select</option>
                                                                        <option value="Check">Check</option>
                                                                        <option value="Cash">Cash</option>
                                                                        <option value="Credit">Credit Card/Debit Card</option>
                                                                        <option value="MoneyOrder">Money Order</option>
                                                                        <!--                                                                    <option value="ACH">ACH</option>-->
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2" id="checkNumber" style="display: none;">
                                                                    <label>Check Number <em class="red-star">*</em></label>
                                                                    <input name="check_number" type="text" class="form-control number_only" >
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                                    <input id="checkoutAmount" name="amount" type="text" class="form-control" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="btn-outer text-right">
                                                            <button type="submit" class="blue-btn" id="paySelectedBill">Pay Invoice</button>
                                                            <button class="grey-btn" id="resetPayBill" type="button">Reset</button>
                                                            <button class="grey-btn cancelActionBtn" type="button">Cancel</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div role="tabpanel" class="tab-pane" id="people-vendor-three">
                                        <!-- Form Outer starts -->

                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Ledger</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="grid-outer col-sm-12">
                                                    <div class="col-sm-2">
                                                        <label> Start Date</label>
                                                        <input type="text" id="txtLedgerStartDate" class="form-control" autocomplete="off"></span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label> End Date</label>
                                                        <input type="text" id="txtLedgerStartDate" class="form-control" autocomplete="off"></span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label> </label>
                                                        <input type="button" id="updateEmergencyButton" class="blue-btn" value="Search">
                                                    </div>
                                                </div>

                                                <div class="grid-outer col-sm-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-dark">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">Date</th>
                                                                <th scope="col">Transaction Id</th>
                                                                <th scope="col">Transaction Description</th>
                                                                <th scope="col">Reference No.</th>
                                                                <th scope="col">Original Amount</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>September Ledger Balance</td>
                                                                <td>US $0.00</td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>August Ledger Balance</td>
                                                                <td>US $0.00</td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>July Ledger Balance</td>
                                                                <td>US $0.00</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    <div role="tabpanel" class="tab-pane" id="people-vendor-four">
                                        <!-- Form Outer starts -->
                                        <div class="paymentDivViewTab">
                                        <div id="payment1st" class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Tax Information</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="detail-outer">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="col-xs-6">
                                                                <label class="text-right">Tax Payer Name  :</label>
                                                                <span class="Payer_Name"></span>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <label class="text-right">Tax Payer ID :</label>
                                                                <span class="Payer_ID"></span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edit-foot">
                                                    <a href="javascript:;" class="edit_redirectionTab2">
                                                        <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Form Outer starts -->
                                        <div id="payment2nd" class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Payment Settings</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="detail-outer">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="col-xs-6">
                                                                <label class="text-right"> Consolidate Checks :</label>
                                                                <span class="checks"></span>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <label class="text-right">Default GL Expense Account :</label>
                                                                <span class=" Expense_Account"></span>
                                                            </div>

                                                            <div class="row">

                                                                <div class="col-xs-6">
                                                                    <label class="text-right"> Order Limit :</label>
                                                                    <span class="Order_Limit"></span>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edit-foot">
                                                    <a href="javascript:;" class="edit_redirectionTab2">
                                                        <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="payment3rd" class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Bank Account Information</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="detail-outer">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="col-xs-6">
                                                                <label class="text-right"> Account Name :</label>
                                                                <span class="Account_Name"></span>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <label class="text-right">Routing Number :</label>
                                                                <span class="Routing_Number"></span>
                                                            </div>

                                                            <div class="row">

                                                                <div class="col-xs-6">
                                                                    <label class="text-right"> Bank Account Number :</label>
                                                                    <span class="Account_Number"></span>
                                                                </div>
                                                                <div class="col-xs-6">
                                                                    <label class="text-right"> Eligible For 1099 :</label>
                                                                    <span class="Eligible_1099"></span>
                                                                </div>

                                                            </div>
                                                            <div class="row">

                                                                <div class="col-xs-6">
                                                                    <label class="text-right">  Comments :</label>
                                                                    <span class=" Comments"></span>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edit-foot">
                                                    <a href="javascript:;" class="edit_redirectionTab2">
                                                        <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="paymentDivTab2" style="display: none;">
                                    <form id="payment_setting_form">
                                    <!--Tax Information-->
                                    <div id="payment1st" class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Tax Information</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                <div class="form-outer">

                                                    <div class="col-sm-3">
                                                        <label>Tax Payer Name</label>
                                                        <input name="tax_payer_name" placeholder="Eg: Tax Payer Name" maxlength="200" id="tax_payer_name_payment" class="capital form-control address_field" type="text">
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <label>Tax Payer ID</label>
                                                        <input name="tax_payer_id" placeholder="Eg: Tax Payer ID" maxlength="200" id="tax_payer_id_payment" class="form-control address_field" type="text">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Payment Settings-->
                                    <div id="payment2nd" class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Payment Settings</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                <div class="form-outer">
                                                    <div class="col-sm-3">
                                                        <label>Consolidate Checks</label>
                                                        <select class="form-control" id="consolidate_check_payment" name="consolidate_checks"><option value="">Select Consolidate Cheque</option><option value="1" selected="">Yes</option><option value="2">No</option></select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Default GL Expense Account <a class="pop-add-icon" href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#chartofaccountmodal"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                        <select class="form-control default_security_bank_account_payment hide_copy" name="default_GL" id="default_security_bank_account"><option>Select</option></select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Order Limit </label>
                                                        <input class="form-control amount number_only" type="text" placeholder="Eg:40.00"  name="order_limit" id="order_limit_payment"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Bank Account Information-->
                                    <div id="payment3rd" class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Bank Account Information</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-2">
                                                            <label>Account Name <a class="pop-add-icon newaccountnameplus" href="javascript:;" onclick="clearPopUps('.Newaccountname')"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                            <select class="form-control bankAccountName account_name_payment" id="account_name" name="account_name"><option>Select</option></select>
                                                            <div class="add-popup Newaccountname" id="NewaccountnameIdd" style="width: 135%;">
                                                                <h4>Add New Account Name</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>Account Name<em class="red-star">*</em></label>
                                                                            <input class="form-control capital customValidateAccountName" data_required="true" data_max="150" type="text" name="@account_name" placeholder="Add New Account Name"/>
                                                                            <span class="customError required"></span>
                                                                        </div>
                                                                        <div class="btn-outer text-right">
                                                                            <button type="submit" class="blue-btn NewaccountnameSave">Save</button>
                                                                            <button type="button" rel="NewaccountnameIdd" class="clear-btn clearVendorcredentialForm">Clear</button>
                                                                            <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label>Bank Account Number </label>
                                                            <input class="form-control number_only hide_copy" id="bank_account_number_payment" maxlength="20"  type="text" name="bank_account_number" placeholder="Bank Account Number"/>

                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label>Routing Number</label>
                                                            <input class="form-control number_only" id="routing_number_payment" maxlength="20"  type="text" name="routing_number" placeholder="Routing Number"/>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label>Eligible for 1099</label>
                                                            <select class="form-control" id="eligible_payment" name="eligible_for_1099">
                                                                <option value="default">Select</option>
                                                                <option value="1" selected="selected">Yes</option>
                                                                <option value="2">No</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-4">
                                                            <label>Comments</label>
                                                            <textarea class="form-control capital" id="comment_payment" name="comment" placeholder="Comments"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- form update -->
                                    <div class="btn-outer text-right">
                                        <input type="submit" id="btnUpdateVendor" class="blue-btn" value="Update">
                                        <input type="button"  class="grey-btn resetButtonVendor" value="Reset">
                                        <input type="button" class="grey-btn btnUpdateVendorCancel"  value="Cancel">
                                    </div>
                                    </form>
                                </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="people-vendor-five">
                                        <div class="form-outer " id="OwnerNotes">
                                            <div class="form-hdr">
                                                <h3>History / Notes
                                                    <a href="javascript:;" class="blue-btn pull-right" id="add_notes"
                                                       style="display: none;">Add Notes</a>
                                                </h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="accordion-grid">
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div  class="vendor_note" style="display: none;">
                                                                        <div class="row">
                                                                            <div class="form-outer vendor-notes-clone-divclass" id="vendor-notes-clone-div">
                                                                                <form class="vendorNotes" id="vendorNotes" id="note">
                                                                                    <input type="hidden" value="add" class="note_type">
                                                                                    <input type="hidden" value="" class="tenantchargenotes" name="id">
                                                                                    <div class="col-sm-4">
                                                                                        <div class="notes_date_right_div">
                                                                                            <textarea class="notes capital note notes_date_right" name="note" ></textarea>
                                                                                        </div>
                                                                                        <div class="btn-outer text-right">
                                                                                            <input type="submit"  id='vendorAdd' class="blue-btn" value="Save" />
                                                                                            <button rel="vendorNotes" data-id="vendorAdd" type="button"  class="grey-btn notesVendorView">Reset</button>
                                                                                            <input type="button" class="grey-btn cancelhistorynotes" value='Cancel' />
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="grid-outer">
                                                                        <div class="apx-table apxtable-bdr-none">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-hover table-dark"
                                                                                       id="vendor_table">
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edit-foot">
                                                    <a href="javascript:;">
                                                                <span class="pull-right" id='vendor_edit_notes' style='cursor: pointer;'><i class="fa fa-pencil-square-o" id="vendor_edit_notes" aria-hidden="true"></i>Edit
                                                                </span>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="people-vendor-six">
                                        <div class="form-outer" id="filelibrary">
                                            <div class="form-hdr">
                                                <h3>File Library</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="accordion-grid">
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="form-outer2 add_libraray_Div" style="display:none">
                                                                        <div class="col-sm-12 text-right">
                                                                            <button type="button" id="add_libraray_file" class="green-btn">Click Here to Upload</button>
                                                                            <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                                            <button class="orange-btn" id="remove_library_file">Remove All Files</button>
                                                                            <button type="button" id="saveLibraryFiles" class="blue-btn">Save</button>
                                                                            <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="row" id="file_library_uploads">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="grid-outer listinggridDiv">
                                                                        <div class="apx-table apxtable-bdr-none">
                                                                            <div class="table-responsive">
                                                                                <table id="fileLibraryTable" class="table table-bordered">
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edit-foot">
                                                    <a href="javascript:;" class="edit_vendorlink" redirection_data="fileLibrary">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="people-vendor-seven">
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Work Orders</h3>
                                            </div>

                                            <div class="form-data">

                                                <div class="accordion-outer">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="property-status">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="col-sm-4" id="filter_div" >
                                                                        <select id="ddlWorkOrderListingFrequency" class="common_radio form-control select-dd">
                                                                            <option value="all">Select</option>
                                                                            <option value="Daily">Open</option>
                                                                            <option value="Weekly">Closed</option>
                                                                            <option value="Bi-Weekly">InProgress</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="btn-outer text-right">
                                                                        <a href="/WorkOrder/AddWorkOrder" class="blue-btn">New Work Order</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <div class="grid-outer listinggridDiv">
                                                                <div class="apx-table apxtable-bdr-none">
                                                                    <div class="table-responsive">
                                                                        <table id="workOrder-table" class="table table-bordered">
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="people-vendor-eight">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Vendor Portal</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <form id="sendEmailTemplate">
                                                        <div class="col-sm-2">
                                                            <label>Email ID <em class="red-star">*</em></label>
                                                            <input class="form-control autoVendoremail" id="sendEmail" type="text" name="sendEmail"/>
                                                        </div>
                                                        <div class="col-sm-2 clear">
                                                            <label>&nbsp;</label>
                                                            <input class="blue-btn" type="submit" id="sendEmail" value="Send Password">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div role="tabpanel" class="tab-pane" id="people-vendor-nine">
                                        <div style="display:none" class="form-outer" id="new_complaint">
                                            <form action="post" id="new_complaintform">
                                                <div class="form-hdr">
                                                    <h3>Vendor Complaints</h3>
                                                </div>
                                                <div class="form-data form-data2">

                                                    <input type="hidden" name="edit_complaint_id" id="edit_complaint_id" />

                                                    <div id="dvVehicleRadio" class="property-status" style="height: auto;">
                                                        <div class="check-outer">
                                                            <input type="radio" name="complaint_by_about" value="Complaint By Vendor" class="clsVendorComplaints" style="text-transform: capitalize;" checked>
                                                            <label>Complaint By Vendor</label>
                                                        </div>
                                                        <div class="check-outer">

                                                            <input type="radio" name="complaint_by_about" value="Complaint about Vendor" class="clsVendorComplaints" style="text-transform: capitalize;">
                                                            <label>Complaint about Vendor</label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <label>Complaint ID</label>
                                                            <input class="form-control" name="complaint_id" id="complaint_id"  placeholder="Eg: AB01234C " type="text" value="GKATCK">
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label>Complaint Date </label>
                                                            <input class="form-control calander" id="complaint_date" name="complaint_date" type="text" readonly>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label>Complaint Type<a id="complainttype" class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" style="margin-left: 5px;" aria-hidden="true"></i></a></label>
                                                            <select class='form-control' id='complaint_type_options' name='complaint_type_options' >
                                                                <option disabled selected value> -- select an option -- </option>
                                                            </select>
                                                            <div class="add-popup" id='ComplaintTypePopup'>
                                                                <h4>Add New Complaint Type</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>Complaint Type<em class="red-star">*</em></label>
                                                                            <input class="form-control capital capitalize_popup_input" type="text"  name='@complaint_type'/>
                                                                        </div>
                                                                        <div class="btn-outer text-right">
                                                                            <button type="button" class="blue-btn" id="NewvendorComplaintSave"/>Save</button>
                                                                            <button type="button" rel="ComplaintTypePopup" class="clear-btn clearVendorcredentialForm">Clear</button>
                                                                            <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4" id="other_notes_div" style="display:none">
                                                            <label>Other Notes</label>
                                                            <textarea class="form-control capital" rows="1" cols="5"  id="other_notes" name="other_notes"></textarea>
                                                        </div>
                                                        <div class="col-sm-6 clear">
                                                            <label>Complaint Notes</label>
                                                            <div class="notes_date_right_div">
                                                                <textarea class="form-control capital notes_date_right" rows="8" id="complaint_note" name="complaint_note"></textarea></div>
                                                        </div>

                                                    </div>

                                                    <div class="btn-outer text-right">
                                                        <button type="button" class="blue-btn" id="complaint_save">Save</button>
                                                        <button type="button" class="grey-btn compVendorView" rel="new_complaintform" data-id="complaint_save">Reset</button>
                                                        <button type="button" class="grey-btn" id="complaint_cancel">Cancel</button>
                                                    </div>



                                                </div>
                                            </form>
                                        </div>
                                        <!-- Wating List table-->
                                        <div class="row">


                                            <div class="col-sm-2">
                                                <!--                                            <label>Status</label>-->
                                                <select id="grid_status" class="common_ddl fm-txt form-control">
                                                    <option value="all">All</option>
                                                    <option value="Complaint By Vendor">Complaint By Vendor</option>
                                                    <option value="Complaint about Vendor">Complaint about Vendor</option>

                                                </select>
                                            </div>


                                            <div class="col-sm-10">
                                                <div class="btn-outer text-right">
                                                    <button type="button" id="new_complaint_button" class="blue-btn">New Complaint</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <div class="apx-table listinggridDiv">
                                                                        <div class="table-responsive">
                                                                            <table id="vendor-complaints" class="table table-bordered"></table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>




                                        <!-- Complaint List table-->


                                        <div class="col-sm-12 form-group">
                                            <div class="btn-outer text-right">
                                                <button type="button" class=" blue-btn" id="print_email_button">Print/Email</button>
                                            </div>
                                        </div>


                                    </div>

                                    <div role="tabpanel" class="tab-pane" id="people-vendor-flagbank">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Flag Bank</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="col-sm-12">
                                                    <div class="btn-outer text-right">
                                                        <button id="new_flag" class="blue-btn">New Flag</button>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <form id="flagForm">
                                                        <div class="col-sm-12" id="flagFormDiv" style="display: none;">
                                                            <div class="grey-box-add">
                                                                <div class="form-outer">
                                                                    <input type="hidden" name="id" id="flag_id">

                                                                    <input type="hidden"  id="flag_view_name" value="">
                                                                    <input type="hidden"  id="flag_view_phone_number" value="">
                                                                    <input type="hidden"  id="flag_country_code" value="">

                                                                    <div class="col-xs-12 col-sm-3">
                                                                        <label>Flagged By</label>
                                                                        <input class="form-control capital" name="flag_by" id="flag_flag_by" placeholder="Flagged By" maxlength="100" type="text" value="<?php echo $_SESSION[SESSION_DOMAIN]['default_name'] ?>"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-3">
                                                                        <label>Date</label>
                                                                        <input class="form-control" name="date" id="flag_flag_date" readonly value="" type="text"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-3">
                                                                        <label>Flag Name <em class="red-star">*</em></label>
                                                                        <input class="form-control capital" name="flag_name" id="flag_flag_name" maxlength="100" placeholder="Please Enter the Name of this Flag" type="text"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-3">
                                                                        <label>Country Code</label>
                                                                        <select class='form-control flag_country_codeClass' name='country_code' id="flag_country_code">
                                                                            <?php
                                                                            $select;
                                                                            foreach ($countryArray as $code => $country) {
                                                                                $countryName = ucwords(strtolower($country["name"])); // Making it look good
                                                                                echo "<option value='" . $code . "' " . (($code == 'US') ? "selected" : "") . ">" . $code . " - " . $countryName . " (+" . $country["code"] . ")</option>";
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-3">
                                                                        <label>Phone Number</label>
                                                                        <input class="form-control phone_number phone_format" name="flag_phone_number" id="flag_phone_number" maxlength="12" value="<?php echo $_SESSION[SESSION_DOMAIN]['phone_number'] ?>" placeholder="123-456-7890" type="text"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-3">
                                                                        <label>Flag Reason</label>
                                                                        <input CLASS="form-control capital" name="flag_reason" id="flag_flag_reason" maxlength="100" placeholder="Flag Reason" type="text"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-3">
                                                                        <label>Flagged For</label>
                                                                        <input class="form-control capital" readonly id="flagged_for" maxlength="100"  type="text"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-3">
                                                                        <label>Completed</label>
                                                                        <select class='form-control' name='completed' id="completed">
                                                                            <option value="0">No</option>
                                                                            <option value="1">Yes</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-6">
                                                                        <label>Note</label>
                                                                        <div class="notes_date_right_div">
                                                                            <textarea class="form-control capital notes_date_right" name="flag_note" id="flag_note" placeholder="Flag Notes"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="btn-outer text-right">
                                                                        <button class="blue-btn" type="submit" id="flagSaveBtnId">Save</button>
                                                                        <button data-id="flagSaveBtnId"  rel="flagForm" class="grey-btn flagVendorView" type="button">Reset</button>
                                                                        <button class="grey-btn" type="button" id="flagCancel">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>
                                                <!-- Flag Note table-->
                                                <div class="row">
                                                    <div class="grid-outer listinggridDiv">

                                                        <div class="apx-table apxtable-bdr-none">
                                                            <div class="table-responsive">
                                                                <table id="vendorFlag-table" class="table table-bordered">
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>




    </section>
</div>
<div class="container">
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body" style="height: 380px;">
                    <div class="form-outer col-sm-12">
                        <form id="custom_field">
                            <input type="hidden" id="customFieldModule" name="module" value="vendor">
                            <input type="hidden" name="id" id="custom_field_id" value="">
                            <div class="row custom_field_form">
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Field Name <em class="red-star">*</em></label>
                                    </div>
                                    <div class="col-sm-9 field_name">
                                        <input class="form-control" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                        <span class="required error"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Data Type</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control data_type" id="data_type" name="data_type">
                                            <option value="text">Text</option>
                                            <option value="number">Number</option>
                                            <option value="currency">Currency</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="url">URL</option>
                                            <option value="date">Date</option>
                                            <option value="memo">Memo</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Default value</label>
                                    </div>
                                    <div class="col-sm-9 default_value">
                                        <input class="form-control default_value" id="default_value" type="text" name="default_value" placeholder="">
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Required Field</label>
                                    </div>
                                    <div class="col-sm-9 is_required">
                                        <select class="form-control" name="is_required" id="is_required">
                                            <option value="1">Yes</option>
                                            <option value="0" selected="selected">No</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-outer">

                                <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Email Model start -->
<div class="modal fade" id="sendMailModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header">
                <h4 class="modal-title">Email</h4>
            </div>
            <form id="sendEmailModal">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-1">
                            <button class="blue-btn compose-email-btn">Send</button>
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>To <em class="red-star">*</em></label></div>
                                <div class="col-sm-10"><span><input class="form-control to" name="to" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <a class="add-recipient-link addToRecepent" href="JavaScript:Void(0)"> Add Recipients</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Cc</div>
                                <div class="col-sm-10"><span><input class="form-control cc" name="cc" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <a class="add-recipient-link addCcRecepent" href="JavaScript:Void(0)"> Add Recipients</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Bcc </label></div>
                                <div class="col-sm-10"><span><input class="form-control bcc" name="bcc" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <a class="add-recipient-link addBccRecepent" href="JavaScript:Void(0)"> Add Recipients</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Subject <em class="red-star">*</em></label></div>
                                <div class="col-sm-10"><span><input class="form-control subject" name="subject" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Body <em class="red-star">*</em></label></div>
                                <div class="col-sm-10">
                                    <span><textarea class="form-control summernote" name="body"></textarea></span>
                                    <div class="btn-outer">
                                        <button  class="blue-btn compose-email-btn">Send</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>
                    <div class="attachmentFile"></div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container">
    <div class="modal fade" id="torecepents" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 600px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;height:300px;">
                        <div class="col-sm-6">
                            <label>Select <em class="red-star">*</em></label>
                            <select class="form-control selectUsers">
                                <option value="2">Tenant</option>
                                <option value="4">Owner</option>
                                <option value="3">Vendor</option>
                                <option value="5">Other Contacts</option>
                                <option value="6">Guest Card</option>
                                <option value="7">Rental Application</option>
                                <option value="8">Employee</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-12">
                            <div class="userDetails"></div>
                        </div>
                        <div class="col-sm-12">
                            <button class="blue-btn" id="SendselectToUsers">Done</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="modal fade" id="bccrecepents" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 600px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;height:300px;">
                        <div class="col-sm-6">
                            <label>Select <em class="red-star">*</em></label>
                            <select class="form-control selectBccUsers">
                                <option value=""></option>
                                <option value="2">Tenant</option>
                                <option value="4">Owner</option>
                                <option value="3">Vendor</option>
                                <option value="5">Other Contacts</option>
                                <option value="6">Guest Card</option>
                                <option value="7">Rental Application</option>
                                <option value="8">Employee</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <div class="userBccDetails"></div>
                        </div>
                        <div class="col-sm-12">
                            <button class="blue-btn" id="SendselectBccUsers">Done</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="modal fade" id="ccrecepents" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 600px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;height:300px;">
                        <div class="col-sm-6">
                            <label>Select <em class="red-star">*</em></label>
                            <select class="form-control selectCcUsers">
                                <option value=""></option>
                                <option value="2">Tenant</option>
                                <option value="4">Owner</option>
                                <option value="3">Vendor</option>
                                <option value="5">Other Contacts</option>
                                <option value="6">Guest Card</option>
                                <option value="7">Rental Application</option>
                                <option value="8">Employee</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <div class="userCcDetails"></div>
                        </div>
                        <div class="col-sm-12">
                            <button class="blue-btn" id="SendselectCcUsers">Done</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="modal fade" id="chartofaccountmodal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add Chart Of Account</h4>
                </div>
                <div class="modal-body">
                    <form name="add_chart_account_form" id="add_chart_account_form_id">
                        <div class="panel-body">
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>Account Type <em class="red-star">*</em></label>
                                            <select class="fm-txt form-control" name="account_type_id" id="account_type_id"></select>
                                            <span id="account_type_idErr" class="error"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Account Code <em class="red-star">*</em></label>
                                            <input id="account_code" name="account_code" maxlength="4" placeholder="Eg: 1110" class="form-control" type="text"/>
                                            <span id="account_codeErr" class="error"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Account Name <em class="red-star">*</em></label>
                                            <input id="com_account_name" name="account_name" maxlength="50" placeholder="Eg: Property Trust" class="form-control" type="text"/>
                                            <span id="account_nameErr" class="error"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>Reporting Code <em class="red-star">*</em></label>
                                            <select class="fm-txt form-control" id="reporting_code" name="reporting_code">
                                                <option value="">Select</option>
                                                <option value="1">C</option>
                                                <option value="2">L</option>
                                                <option value="3">M</option>
                                                <option value="4">B</option>
                                            </select>
                                            <span id="reporting_codeErr" class="error"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Sub Account of</label>
                                            <select class="fm-txt form-control" id="sub_account" name="sub_account">
                                                <option value="">Select</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Status</label>
                                            <select class="fm-txt form-control" id="status" name="status">
                                                <option value="1">Active</option>
                                                <option value="0">InActive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>
                                                <div class="check-outer">
                                                    <input name="is_default" id="is_default" type="checkbox"/>
                                                    <label>Set as Default</label>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <select class="fm-txt form-control" id="posting_status" name="posting_status">
                                                <option value="1">Posting</option>
                                                <option value="0">Non Posting</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="btn-outer text-right">
                                        <input type="hidden" value="" name="chart_account_edit_id" class="form-control" id="chart_account_edit_id"/>
                                        <input type="submit" value="Save" class="blue-btn" id="saveBtnId"/>
                                        <button type="button"  class="clear-btn clearFormReset" id="clearChartsOfAccountsModal">Clear</button>
                                        <button type="button"  class="grey-btn" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Email Model End -->
<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL; ?>/css/bootstrap-tagsinput.css" />
<script src="<?php echo COMPANY_SITE_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
<!-- Wrapper Ends -->
<script type="text/javascript">
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var property_unique_id = '';
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var default_name = "<?php echo $_SESSION[SESSION_DOMAIN]['default_name']; ?>";
    var manager_id = "";
    var attachGroup_id = "";
    $(function () {
        $('.nav-tabs').responsiveTabs();
    });


    $("#show").click(function () {
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function () {
        $("#bs-example-navbar-collapse-2").hide();
    });



    $(document).ready(function () {
        $(".slide-toggle").click(function () {
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function () {
        $(".slide-toggle2").click(function () {
            $(".box2").animate({
                width: "toggle"
            });
        });
    });


    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });

    $(document).on("click", '#print_email_button', function () {
        favorite = [];
        var vendor_id=$(".vendor_id").val();
        var no_of_checked = $('[name="complaint_checkbox[]"]:checked').length;
        if (no_of_checked == 0) {
            toastr.error('Please select at least one complaint.');
            return false;
        }
        $.each($("input[name='complaint_checkbox[]']:checked"), function () {
            favorite.push($(this).attr('data_id'));
        });
//alert(favorite);
        $.ajax({

            type: 'post',
            url: '/vendor-view-ajax',
            data: {
                vendor_id: vendor_id,
                class: 'ViewVendor',
                action: 'getComplaintsDataForPrint',
                'complaint_ids': favorite,
                //       'owner_id' : id
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $("#print_complaint").modal('show');
                    $("#modal-body-complaints").html(response.html)
                } else {
                    toastr.warning('Record not updated due to technical issue.');
                }
            }
        });
    });



    /*function to print element by id */
    function sendComplaintsEmail(elem)
    {
        var mail_length = $('#print_complaint #modal-body-complaints>table').length;

        var vendor_email = $('.email').text();

        SendMail($(elem).html(), mail_length, vendor_email);
    }

    function SendMail(data, mail_length, vendor_email){
        var vendor_id=$(".vendor_id").val();
        $.ajax({
            type: 'post',
            url:'/vendor-view-ajax',
            data: {
                class: 'ViewVendor',
                action: 'SendComplaintMail',
                data_html: data,
                'complaint_ids': favorite,
                'vendor_id' : vendor_id,
                mail_length : mail_length,
                vendor_email : vendor_email,
            },
            success : function(response){
                var response =  JSON.parse(response);
                if(response.status == 'success' && response.code == 200) {
                    toastr.success('Mail sent successfully.');
                    $("#print_complaint").modal('hide');
                }else {
                    toastr.warning('Mail not sent due to technical issue.');
                }
            }
        });
    }

</script>
<script type="text/javascript">

    var vendor_unique_id = '<?= $_GET['id']; ?>';


    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var amount_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var pagination  = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url  = "<?php echo SITE_URL; ?>";
    var login_user_name =  "<?php echo $_SESSION[SESSION_DOMAIN]['name']; ?>";
    var vendor_id = "<?php echo $_GET['id']; ?>";
    var vendor_unique_id = '<?= $_GET['id']; ?>';
    var defaultFormData='';
    var defaultFormData2='';
    var vendorcompData='';
</script>

<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/autonumeric.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/autonumericDecimal.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/additionalMethods.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/commonPopup.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
<!--<script src="--><?php //echo COMPANY_SITE_URL; ?><!--/js/custom_fields.js" type="text/javascript"></script>-->
<!-- file library js -->

<script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/custom_fields.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/custom_fields.js" type="text/javascript"></script>
<!--<script src="--><?php //echo COMPANY_SITE_URL; ?><!--/js/company/people/vendor/file_library.js" type="text/javascript"></script>-->
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/vendor/viewVendor.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/vendor/addVendor.js" type="text/javascript"></script>


<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/vendor/vendorComplaints.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/vendor/vendorFileLibratry.js" type="text/javascript"></script>

<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/vendor/vendor_notes.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/vendor/flag.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/vendor/flagValidation.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/vendor/vendorList.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/people/paymentValidations.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/vendor/vendorPayment.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/vendor/payables.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/property/emailAttach.js"></script>
<script>

    $('.calander').datepicker({
        yearRange: '1919:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('input[name="birth"]').datepicker({
        yearRange: '1900:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    $(".calander").val(date);



    $('.calander').datepicker({
        yearRange: '1919:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });   var date = $.datepicker.formatDate(jsDateFomat, new Date());
    $(".calander").val(date);
    $(document).on('focus',".acquireDateclass,.expirationDateclass", function(){
        $(this).datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
    });

    $('#resetPayBill').on('click',function(){
        var validator = $( "#paymentForm" ).validate();
        validator.resetForm();
        $('#paymentForm').trigger("reset");
    });


</script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
