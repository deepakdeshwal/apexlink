<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">

    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <head>
        <style>
            /*html, body {*/
            /*    margin: 0;*/
            /*    padding: 0;*/
            /*    font-family: Arial, Helvetica Neue, Helvetica, sans-serif;*/
            /*    font-size: 12px;*/
            /*}*/

            #calendar {
                max-width: 800px;
                margin: 40px auto;
            }
            #calendar1 {
                max-width: 800px;
                margin: 40px auto;
            }
            #calendar2 {
                max-width: 800px;
                margin: 40px auto;
            }

            #calendar-container {
                position: relative;
                z-index: 1;
                margin-left: 200px;
            }
        </style>
        <link rel='stylesheet' href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/main.css" />
        <link rel='stylesheet' href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/main.css/main.min.css"/>
        <link rel='stylesheet' href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/main.min_week.css" />
        <link rel='stylesheet' href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/main.min_week1.css" />
        <link rel='stylesheet' href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/main.min_week2.css" />
        <link rel='stylesheet' href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/resource.css" />
        <link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/jquery-pseudo-ripple.css">
        <link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/jquery-nao-calendar.css">
        <link rel='stylesheet' href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/timeline/main.css" />
        <link rel='stylesheet' href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/timeline/main.min.css" />
        <link rel='stylesheet' href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-timeline/main.css" />
        <link rel='stylesheet' href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-timeline/main.min.css" />
        <link rel='stylesheet' href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/timepicker/jquery.timepicker.css" />
        <link rel='stylesheet' href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/timepicker/jquery.timepicker.min.css" />
        <link rel='stylesheet' href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/timepicker/jquery.timepicker.min.css.map" />
<!--    </head>-->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">

                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-4 pull-right">
                            <div class="easy-search ">
                                <input placeholder="Easy Search" class="" type="text"/>
                            </div>
                        </div>
                    </div>
                    <div class="cal-head text-center">
                        <h2>  Calendar </h2>
                    </div>

                    <div class="calender-links">
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list-inline">
                                    <li>
                                        <div class="cal-view" id="new_entry" style="cursor: pointer;">
                                            <span class="cal-icon">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                            </span>
                                            <h5> New Entry </h5>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="cal-view" id="day_view" style="cursor: pointer;">
                                            <span class="cal-icon">
                                                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                            </span>
                                                <h5>  Day View  </h5>
                                        </div>
                                    </li><li>
                                        <div class="cal-view" id="week_view" style="cursor: pointer;">
                                            <span class="cal-icon">
                                               <i class="fa fa-calendar-minus-o" aria-hidden="true"></i>
                                            </span>
                                                <h5>  Week View  </h5>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="cal-view" id="month_view" style="cursor: pointer;">
                                            <span class="cal-icon">
                                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                            </span>
                                                <h5> Month View  </h5>
                                        </div>

                                    </li>

                                </ul>
                            </div>
                            <div class="col-md-6 text-right">
                                <ul class="list-inline">
                                    <li>
                                        <div class="cal-view">
                                            <a href="javascript:void(0)" id="share_calendar">
                                               <span class="cal-icon">
                                                <i class="fa fa-share-alt" aria-hidden="true"></i>
                                            </span>
                                                <h5> Share Calender </h5>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="cal-view">
                                            <a href="javascript:void(0)" id="print_calendar">
                                               <span class="cal-icon">
                                                <i class="fa fa-print" aria-hidden="true"></i>
                                            </span>
                                                <h5> Print Calender </h5>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="cal-view">
                                            <a href="javascript:void(0)" id="search_cal">
                                               <span class="cal-icon">
                                              <i class="fa fa-search" aria-hidden="true"></i>
                                            </span>
                                                <h5> Search Calender </h5>
                                            </a>
                                        </div>
                                        <div class="cal-search" style="display: none;" id="search_cal_val">
                                            <input class="form-control" type="text" id="search_cal_input"/>
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <div id="month_view">
                     <div class="row" id="main_sec">
                         <div class="col-sm-3">
                             <div class="side-cal">
                                 <ul class="sidecalendar-list">
                                     <li>
                                         <div class="myCalendar nao-month"></div>
                                     </li>
                                     <li>
                                         <div class="myCalendar1 nao-month"></div>
                                     </li>

                                     <li>
                                         <div class="panel-group calender-panel">
                                             <div class="panel panel-default">
                                                 <div class="panel-heading">
                                                     <h4 class="panel-title">
                                                         <a data-toggle="collapse" href="#collapse1">
                                                             <span class="arrow-icon"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
                                                             <span> <i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                             Calendars in View
                                                         </a>

                                                     </h4>
                                                 </div>
                                                 <div id="collapse1" class="panel-collapse collapse">
                                                     <div class="panel-body cal-view">

                                                         <div class="checkbox">
                                                             <label><input type="checkbox" value="1" name="cal_grp1"><span class="bg-skyblue">My Calendar</span></label>
                                                         </div>
                                                         <div class="checkbox">
                                                             <label><input type="checkbox" value="2" name="cal_grp1"><span class="bg-green">Company Calendar</span></label>
                                                         </div>
                                                         <div class="checkbox">
                                                             <label><input type="checkbox" value="3" name="cal_grp1"><span class="bg-purple">Regional Calendar </span></label>
                                                         </div>
                                                         <div class="checkbox">
                                                             <label><input type="checkbox" value="4" name="cal_grp1"><span class="bg-orange">Property Calendar</span></label>
                                                         </div>
                                                         <div class="checkbox">
                                                             <label><input type="checkbox" value="5" name="cal_grp1"> <span class="bg-pink">Staff Calendar</span></label>
                                                         </div>
                                                         <div class="checkbox">
                                                             <label><input type="checkbox" value="6" name="cal_grp1"><span class="bg-pink">Group Calendar</span></label>
                                                         </div>
                                                     </div>

                                                 </div>
                                             </div>
                                         </div>
                                     </li><!-----li ---end--->

                                     <li>
                                         <div class="panel-group calender-panel">
                                             <div class="panel panel-default">
                                                 <div class="panel-heading">
                                                     <h4 class="panel-title">
                                                         <a data-toggle="collapse" href="#collapse2">
                                                             <span class="arrow-icon"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
                                                             <span> <i class="fa fa-share-alt" aria-hidden="true"></i></span>
                                                           Share Calendars
                                                         </a>

                                                     </h4>
                                                 </div>
                                                 <div id="collapse2" class="panel-collapse collapse">
                                                     <div class="panel-body cal-view">

                                                         <div class="checkbox">
                                                             <label><input type="checkbox" value=""><span class="bg-skyblue">Option 1</span></label>
                                                         </div>
                                                         <div class="checkbox">
                                                             <label><input type="checkbox" value=""><span class="bg-green">Option 1</span></label>
                                                         </div>
                                                         <div class="checkbox">
                                                             <label><input type="checkbox" value=""><span class="bg-purple">Option 1</span></label>
                                                         </div>
                                                         <div class="checkbox">
                                                             <label><input type="checkbox" value=""><span class="bg-orange">Option 1</span></label>
                                                         </div>
                                                         <div class="checkbox">
                                                             <label><input type="checkbox" value=""><span class="bg-pink">Option 1</span></label>
                                                         </div>
                                                     </div>

                                                 </div>
                                             </div>
                                         </div>
                                     </li>

                                 </ul>
                             </div>
                         </div>
                         <div class="col-sm-6" id="month-cal">
                        <div id='calendar'></div>

                         </div>
                         <div class="col-sm-6" id="day-cal">
                             <div id='calendar2'></div>
                         </div>
                         <div class="col-sm-6" id="week-cal">
                             <div id='calendar1'></div>
                         </div>
                         <div class="col-sm-3" id="week_glance">
                            <div class="Glance-outer">
                                <h4>Week-At-A-Glance</h4>
                                <div id="week_schedule">
<!--                                <h5>Today</h5>-->
<!--                                <div class="row glance-meeting">-->
<!--                                    <div class="col-sm-3">-->
<!--                                        <p class="glance-time">-->
<!--                                            15:30-->
<!--                                        </p>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-9">-->
<!--                                         <p class="glance-task bg-skyblue">-->
<!--                                            17-->
<!--                                        </p>-->
<!--                                    </div>-->
<!--                                </div>-->

<!--                                <div class="row glance-meeting">-->
<!--                                    <div class="col-sm-3">-->
<!--                                        <p class="glance-time">-->
<!--                                            15:30-->
<!--                                        </p>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-9">-->
<!--                                        <p class="glance-task bg-skyblue">-->
<!--                                            17-->
<!--                                        </p>-->
<!--                                    </div>-->
<!--                                </div> <div class="row glance-meeting">-->
<!--                                    <div class="col-sm-3">-->
<!--                                        <p class="glance-time">-->
<!--                                            15:30-->
<!--                                        </p>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-9">-->
<!--                                        <p class="glance-task bg-skyblue">-->
<!--                                            17-->
<!--                                        </p>-->
<!--                                    </div>-->
                                </div>
                            </div>

                            </div>

                         </div>

                     </div>







                    </div>
<!--                    <div id="week_view">-->
<!--                    <div id='calendar1'></div>-->
<!--                    </div>-->
<!--                    <div id="week_view">-->
<!--                        <div id='calendar2'></div>-->
<!--                    </div>-->
<!--                    </div>-->
            </div>
            <div class="accordion-grid" style="display: none;" id="search_cal_outer">
                <div class="accordion-outer">
                    <div class="bs-example">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div id="collapseOne" class="panel-collapse collapse  in">
                                    <div class="panel-body pad-none">
                                        <div class="accordion-grid">
                                            <div class="panel-group" id="accordion">
                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                    <div class="grid-outer">
                                                        <div class="table-responsive">
                                                            <div class="apx-table listinggridDiv">
                                                                <div class="table-responsive">
                                                                    <table id="search_calendar_grid" class="table table-bordered"></table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<input type="hidden" id="events">

<!-- Modal -->
<div class="modal fade" id="newentry" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_modal" >&times;</button>
                <h4 class="modal-title text-center">New Entry</h4>
            </div>
            <div class="modal-body p-0">
                <div class="entry-header">
                    <ul class="list-inline text-center">
                        <li>
                            <div class="cal-view">
                                <a href="javascript:void(0)" id="cal_group">
                                   <span class="cal-icon">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </span>
                                    <h5>  Calendar Group </h5>
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="cal-view">
                                <a href="javascript:void(0)" id="reccur_pattern">
                                   <span class="cal-icon">
                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                    </span>
                                    <h5>   Recurring Entry
                                    </h5>
                                </a>
                            </div>
                        </li> <li>
                            <div class="cal-view">
                                <a href="javascript:void(0)" id="send_invitation">
                                   <span class="cal-icon">
                                     <i class="fa fa-users" aria-hidden="true"></i>
                                    </span>
                                    <h5>  Send Invitation  </h5>
                                </a>
                            </div>
                        </li> <li>
                            <div class="cal-view">
                                <a href="javascript:void(0)" id="private_meeting" class="not_private">
                                   <span class="cal-icon">
                                     <i class="fa fa-lock" aria-hidden="true"></i>
                                    </span>
                                    <h5>  Private  </h5>
                                </a>
                            </div>
                        </li> <li>
                            <div class="cal-view">
                                   <span class="cal-icon">
                                    <div class="form-group mb-0">
                                      <select class="form-control" id="remm">
                                       <option value="">None</option>
                                       <option value="0">0 minutes</option>
                                       <option value="5">5 minutes</option>
                                       <option value="10">10 minutes</option>
                                       <option selected="selected" value="15">15 minutes</option>
                                       <option value="30">30 minutes</option>
                                       <option value="60">1 hour</option>
                                       <option value="120">2 hours</option>
                                       <option value="180">3 hours</option>
                                       <option value="240">4 hours</option>
                                       <option value="300">5 hours</option>
                                        <option value="360">6 hours</option>
                                        <option value="420">7 hours</option>
                                        <option value="480">8 hours</option>
                                        <option value="540">9 hours</option>
                                        <option value="600">10 hours</option>
                                        <option value="660">11 hours</option>
                                        <option value="720">12 hours</option>
                                        <option value="1080">18 hours</option>
                                        <option value="1440">1 day</option>
                                        <option value="2880">2 days</option>
                                        <option value="4320">3 days</option>
                                        <option value="5760">4 days</option>
                                        <option value="10080">1 week</option>
                                        <option value="20160">2 weeks</option>
                                      </select>
                                    </div>
                                    </span>
                                    <h5>  Reminder </h5>
                            </div>
                        </li>
                        <li>
                            <div class="cal-view">
                                <a href="javascript:void(0)" id="delete_schedule">
                                   <span class="cal-icon">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                    </span>
                                    <h5> Delete Meeting </h5>
                                    <input type="hidden" id="del_meeting" value="0"/>
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="cal-view">
                                <a href="javascript:void(0)" id="save_schedule">
                                   <span class="cal-icon">
                                   <i class="fa fa-download" aria-hidden="true"></i>
                                    </span>
                                    <h5>   Save & Close </h5>
                                </a>
                            </div>
                        </li>
                    </ul>

                </div>

                <div class="entry-form">
                    <form class="form-inline" id="meeting_form">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="pwd">Subject</label>
                            </div>
                            <div class="col-sm-9">
                                <select class="form-control all_sub" id="subject">
                                    <option value="">Select</option>
                                    <option value="Apartment Marketing">Apartment Marketing</option>
                                    <option value="Appointment">Appointment</option>
                                    <option value="Award Ceremony">Award Ceremony</option>
                                    <option value="Birthday Party">Birthday Party</option>
                                    <option value="Business Dinner">Business Dinner</option>
                                    <option value="Board Meeting">Board Meeting</option>
                                    <option value="Charitable Event">Charitable Event</option>
                                    <option value="Complaint">Complaint</option>
                                    <option value="Conference">Conference</option>
                                    <option value="Community Socials">Community Socials</option>
                                    <option value="Dinner">Dinner</option>
                                    <option value="Enquiry">Enquiry</option>
                                    <option value="Executive Retreat">Executive Retreat</option>
                                    <option value="Family Event">Family Event</option>
                                    <option value="Golf Event">Golf Event</option>
                                    <option value="Incentive Event">Incentive Event</option>
                                    <option value="Meeting" selected>Meeting</option>
                                    <option value="Move In">Move In</option>
                                    <option value="Move Out">Move Out</option>
                                    <option value="Neighborhood Event">Neighborhood Event</option>
                                    <option value="Networking Event">Networking Event</option>
                                    <option value="Office Team Building">Office Team Building</option>
                                    <option value="Opening Ceremony">Opening Ceremony</option>
                                    <option value="Open House">Open House</option>
                                    <option value="Party">Party</option>
                                    <option value="Press Conference">Press Conference</option>
                                    <option value="Product Launch">Product Launch</option>
                                    <option value="Rent Payment">Rent Payment</option>
                                    <option value="Resident Appreciation">Resident Appreciation</option>
                                    <option value="Report">Report</option>
                                    <option value="Shareholder Meetings">Shareholder Meetings</option>
                                    <option value="Seminar">Seminar</option>
                                    <option value="Showing">Showing</option>
                                    <option value="Site Visit">Site Visit</option>
                                    <option value="Summer Party">Summer Party</option>
                                    <option value="Team Building Event">Team Building Event</option>
                                    <option value="Trade Fair">Trade Fair</option>
                                    <option value="Trade Show">Trade Show</option>
                                    <option value="Transfer">Transfer</option>
                                    <option value="Travel">Travel</option>
                                    <option value="Visit">Visit</option>
                                    <option value="VIP Event">VIP Event</option>
                                    <option value="Wedding">Wedding</option>
                                    <option value="End of Year Party">End of Year Party</option>
                                    <option value="Other">Other</option><
                                </select>
                            </div>
                        </div><!-----row--end--->
                        <div class="row" style="display: none" id="other_sub">
                            <div class="col-sm-3">

                            </div>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="other_sub">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="pwd">Location</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="location">
                            </div>
                        </div><!-----row--end--->
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="pwd">Start-Time</label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                <div class="col-sm-6">
                                    <div class="date-outer">
                                    <input type="text" class="form-control calander start_time" id="start_time">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="date-outer">
                                    <input type="text" class="form-control time-picker" id="st_time">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div><!-----row--end--->

                        <div class="row">
                            <div class="col-sm-3">
                                <label for="pwd">End-Time</label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="date-outer">
                                            <input type="text" class="form-control calander end_time" id="end_time">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="date-outer">
                                            <input type="text" class="form-control time-picker" id="e_time">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-----row--end--->


                        <div class="row">
                            <div class="col-sm-12">
                                <textarea class="form-control" rows="5" id="comment"></textarea>
                            </div>
                        </div><!-----row--end--->
                    </form>
                </div>
            </div>

        </div>

    </div>
</div>

<div id="calendar-group" class="modal fade in" role="dialog">
    <div class="modal-dialog spotlight-pop">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                    Calendar Group Window
                </h4>
                <h5 class="modal-title2">Select a Meeting</h5>
            </div>
            <div class="modal-body">
                <ul id="all_cal_group">
                    <li><input type="checkbox" name="cal_grp" value="0"/> <label>All</label></li>
                    <li><input type="checkbox" checked value="1" name="cal_grp"/> <label>My Calendar</label></li>
                    <li><input type="checkbox" value="2" name="cal_grp"/> <label>Company</label></li>
                    <li><input type="checkbox" value="3" name="cal_grp"/> <label>Regional</label></li>
                    <li><input type="checkbox" value="4" name="cal_grp"/> <label>Property</label></li>
                    <li><input type="checkbox" value="5" name="cal_grp"/> <label>Staff</label></li>
                    <li><input type="checkbox" value="6" name="cal_grp"/> <label>Group</label></li>
                </ul>



            </div>
        </div>

    </div>

</div>


    <div id="calendar-share" class="modal fade in calendar-pop" role="dialog">
    <div class="modal-dialog spotlight-pop">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                    Share Calendar Permissions
                </h4>
            </div>
            <div class="modal-body">
                <div class="share-cal-permission">
                    <div class="reminder-table">
                        <table class="table">
                            <thead>
                            <tr class="">
                                <th><input type="checkbox" class="user_mul"></th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                            </tr>
                            </thead>
                                <tbody id="user_data">

                                </tbody>

                        </table>

                    </div>
                </div>
                <div class="btn-outer text-right">
                    <button class="grey-btn">Remove</button>
                    <button class="blue-btn" id="btnShowAddUserPopup">Add</button>
                </div>
                <div class="cal-separator"></div>
                <div class="permission-calender-left">
                    <h5>Permission to View Access</h5>
                    <ul>
                        <li><input type="checkbox" checked id="view_access_perm"/> <label>None</label></li>
                        <li><input type="checkbox" class="view_acess_class" disabled="disabled"/> <label>Free / Busy Time</label></li>
                        <li><input type="checkbox" disabled="disabled" class="view_acess_class" id="free_busy_time" value="0"/> <label>Free / Busy Time subject and location</label></li>
                        <li><input type="checkbox" disabled="disabled" class="view_acess_class" id="full_access" value="0"/> <label>Full Access</label></li>
                    </ul>
                </div>
                <div class="permission-calender-right">
                    <h5>Permission to make Changes</h5>
                    <ul>
                        <li><input type="checkbox" checked disabled="disabled" id="permission_changes"/> <label>None / View meetings only</label></li>
                        <li><input type="checkbox" disabled="disabled" class="changes_class" id="schedule_create" value="0"/> <label>Schedule / Create Meetings</label></li>
                        <li><input type="checkbox" disabled="disabled" class="changes_class" id="move_change" value="0"/> <label>Move / Change meetings</label></li>
                    </ul>
                </div>


                <div class="btn-outer text-right">
                    <button class="blue-btn" id="accept_permissions">Accept</button>
                    <button class="grey-btn">Cancel</button>
                    <button class="blue-btn" id="apply_permissions">Apply</button>
                </div>
                <div class="cal-separator"></div>

            </div>
        </div>

    </div></div>

<div id="user-names" class="modal fade in calendar-pop" role="dialog">
    <div class="modal-dialog spotlight-pop">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                  Add Users
                </h4>
            </div>
            <div class="modal-body">
                <div class="">
                    <table class="table">
                        <thead>
                        <tr class="active">
                            <th><input type="checkbox" class="check_user_mul"></th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                        </tr>
                        </thead>
                        <div style="max-height: 150px; overflow-y: scroll;">
                        <tbody id="user_data_table">

                        </tbody>
                        </div>
                    </table>
                </div>
                <div class="btn-outer text-right">
                    <button class="blue-btn" id="add_userss">Add</button>

                </div>

            </div>
        </div>

    </div></div>


<div id="calendar-print" class="modal fade in calendar-pop" role="dialog">
    <div class="modal-dialog spotlight-pop">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                    Print Calendar
                </h4>
            </div>
            <div class="modal-body">
                <div class="print-calender">
                    <h5>Print View</h5>
                    <ul>
                        <li><input type="radio" name="print_cal" value="0"/> <label>Daily Calendar</label></li>
                        <li><input type="radio" name="print_cal" value="1"/> <label>Weekly Calendar</label></li>
                        <li><input type="radio" name="print_cal" value="2" checked/> <label>Monthly Calendar</label></li>
                    </ul>
                    <h5>Print Range</h5>
                    <ul class="print-range">
                        <li><span>Start Date</span> <label><input class="form-control start_time" type="text"/></label></li>
                        <li><span>End Date</span> <label><input class="form-control end_time" type="text"/></label></li>
                        <li><input type="checkbox"/> <label>Hide details of private appointments</label></li>
                    </ul>
                    <div class="btn-outer">
                        <button class="blue-btn" id="print_btn">Print</button>
                        <button class="grey-btn">Cancel</button>
                    </div>

                </div>
            </div>

        </div>

    </div></div>
<div id="calendar-reminder" class="modal fade in calendar-pop" role="dialog">
    <div class="modal-dialog ">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="close_reminder">×</button>
                <h4 class="modal-title">
                   Reminder
                </h4>
            </div>
            <div class="modal-body">
                <div class="reminder-cal">
                    <h5>Start Time :&nbsp;<span class="reminder_start_time"></span></h5>
                    <h5>location :&nbsp;<span class="reminder_location"></span></h5>
<div class="reminder-table">
    <table class="table">
        <thead>
        <tr class="active">
            <th>Subject</th>
            <th>Due in</th>
        </tr>
        </thead>
        <tbody id="reminder_data">

        </tbody>
    </table>

<!--    <div class="reminder-select">-->
<!--        <select class="form-control"><option>safdaf</option></select>-->
<!--        <button class="blue-btn" id="print_btn">Snooze</button>-->
<!--    </div>-->
</div>



                </div>
            </div>

        </div>

    </div></div>
<div id="calendar-print1" class="modal fade in calendar-pop multical-print" role="dialog">
    <div class="modal-dialog spotlight-pop">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header"calendar-print2>
                <button type="button" class="close" data-dismiss="modal">×</button>
                <button type="button" class="print blue-btn" data-dismiss="modal" onclick="printcalendar('#print_cal_all')" onclick="window.print();" id="new_cal_print">Print</button>
            </div>
            <div class="modal-body" id="print_cal_all">
                <div class="print-calender">
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-8 text-right">
                            <li>
                                <div class="myCalendar2 nao-month"></div>
                            </li>
                            <li>
                                <div class="myCalendar3 nao-month"></div>
                            </li>
                        </div>
                    </div>

                    <li id="month_cal_list1">
                        <div id="calendar5"></div>
                    </li>
                    <li id="week_cal_list1" style="display: none;">
                        <div id="calendar6"></div>
                    </li>
                    <li id="day_cal_list1" style="display: none;">
                        <div id="calendar7"></div>
                    </li>

                </div>
            </div>

        </div>

    </div></div>

<div id="sendInvitation" class="modal fade in calendar-pop" role="dialog">
    <div class="modal-dialog spotlight-pop">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                    Send Invitation
                </h4>
            </div>
            <div class="modal-body">
                <div class="send-invitation">
                    <ul>
                        <li><label>To</label> <span><input class="form-control invitation_name" type="text"/></span>
                            <input type="hidden" class="email_id"/>
                        <div class="emails_users"></div>
                        </li>
                        <li><label>Subject</label> <span class="meeting_invitation"></span></li>
                        <li><label>Location</label> <span class="Office_invitaion"></span></li>
                        <li><label>When</label> <span class="time_invitaion"></span></li>
                        <li><label>From</label> <span class="name_invitaion"></span></li>
                        <li><textarea class="form-control message_invite"></textarea></li>
                    </ul>
                    <div class="btn-outer">
                        <a class="blue-btn sendInvitation">Send</a>
                        <a class="grey-btn">Cancel</a>
                    </div>

                </div>
            </div>

        </div>

    </div></div>

<div id="calendar-recurring" class="modal fade in calendar-pop" role="dialog">
    <div class="modal-dialog spotlight-pop">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                    Daily Recurring Entry
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-data">
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Start Time</label>
                            <input class="form-control time-picker" type="text" id="start_recurr_time">
                        </div>
                        <div class="col-sm-4">
                            <label>End Time</label>
                            <input class="form-control time-picker" type="text" id="end_recurr_time">
                        </div>
                        <div class="col-sm-4">
                            <label>Duration</label>
                            <select class="form-control"></select>
                        </div>
                    </div>
                </div>
                <div class="cal-separator"></div>

                <div class="details-frequency">
                    <div class="details-frequency-lt">
                        <h5>Details</h5>
                        <ul>
                            <li><input name="freq" type="radio" checked value="1"/> <label>Daily</label></li>
                            <li><input name="freq" type="radio" value="2"/> <label>Weekly</label></li>
                            <li><input name="freq" type="radio" value="3"/> <label>Monthly</label></li>
                            <li><input name="freq" type="radio" value="4"/> <label>Yearly</label></li>
                        </ul>
                    </div>
                    <div class="details-frequency-rt">
                        <h5>Frequency</h5>
                        <ul class="daily">
                            <li><input type="radio" name="daily" value="0" checked/> <label>Every </label><input class="cal-day form-control" type="number" id="daily_skip"/> <label>Day(s)</label></li>
                            <li><input type="radio" name="daily" value="1"/> <label>Every Weekday M-F</label></li>
                        </ul>
                        <ul style="display: none" class="weekly">
                            <li><input type="checkbox" checked/> <label>Reccur Every</label> <input class="cal-day form-control" type="text" id="weekly_recurr"/> <label>Weeks(s) on:</label></li>
                            <li>
                                <ul class="cal-all-days" id="weekly_days">
                                    <li><input type="checkbox" value="0" name="weekly"/> <label>Sunday</label></li>
                                    <li><input type="checkbox" value="1" name="weekly"/> <label>Monday</label></li>
                                    <li><input type="checkbox" value="2" name="weekly"/> <label>Tuesday</label></li>
                                    <li><input type="checkbox" value="3" name="weekly"/> <label>Wednesday</label></li>
                                    <li><input type="checkbox" value="4" name="weekly"/> <label>Thursday</label></li>
                                    <li><input type="checkbox" value="5" name="weekly"/> <label>Friday</label></li>
                                    <li><input type="checkbox" value="6" name="weekly"/> <label>Saturday</label></li>
                                </ul>
                            </li>
                        </ul>
                        <ul style="display: none" class="monthly">
                            <li><input type="radio" name="monthly_freq" value="0" checked/> <label>Day of </label><input class="cal-day form-control" id="day_of_month" type="text"/><label> of Every</label>
                                <input class="cal-day form-control" type="text" id="skip_month"/> <label>Month(s)</label></li>
                            <li class="yearly-select"><input type="radio" name="monthly_freq" value="1" /> <label>The</label>
                                <select class="form-control" id="day_number_month">
                                    <option value="1">First</option><option value="2">Second</option><option value="3">Third</option><option value="4">Fourth</option><option value="5">Last</option>
                                </select> <select class="form-control" id="weekdays_monthly">
                                    <option value="0">Sunday</option>
                                    <option value="1">Monday</option>
                                    <option value="2">Tuesday</option>
                                    <option value="3">Wednesday</option>
                                    <option value="4">Thursday</option>
                                    <option value="5">Friday</option>
                                    <option value="6">Saturday</option>
                                </select>
                                <label>Every</label><input class="cal-day form-control" type="text" id="skip_month1"/> <label>months(s)</label></li>
                        </ul>
                        <ul style="display: none" class="yearly">
                            <li> <label>Recurr Every</label> <input class="cal-day form-control" type="text" id="recurr_every_yearly"/> <label>Years(s) on:</label></li>
                            <li ><input type="radio" name="yearly_freq" value="0" checked/> <select class="form-control" id="yearly_months">
                                    <option value="01">January</option> <option value="02">February</option> <option value="03">March</option> <option value="04">April</option>
                                    <option value="05">May</option> <option value="06">June</option> <option value="07">July</option> <option value="08">August</option>
                                    <option value="09">September</option> <option value="10">October</option> <option value="11">November</option> <option value="12">December</option>
                                </select> <input class="cal-day form-control" type="text" id="end_freq_yearly"/> </li>
                            <li class="yearly-select" ><input type="radio" name="yearly_freq" value="1"/> <label>The</label> <select class="form-control" id="recurr_number"><option value="1">First</option><option value="2">Second</option><option value="3">Third</option><option value="4">Fourth</option><option value="5">Last</option></select>
                                <select class="form-control" id="weekdays_yearly">
                                    <option value="0">Sunday</option>
                                    <option value="1">Monday</option>
                                    <option value="2">Tuesday</option>
                                    <option value="3">Wednesday</option>
                                    <option value="4">Thursday</option>
                                    <option value="5">Friday</option>
                                    <option value="6">Saturday</option>
                                </select><label> of</label> <select class="form-control" id="month_yearly">
                                    <option value="1">January</option> <option value="2">February</option> <option value="3">March</option> <option value="4">April</option>
                                    <option value="5">May</option> <option value="6">June</option> <option value="7">July</option> <option value="8">August</option>
                                    <option value="9">September</option> <option value="10">October</option> <option value="11">November</option> <option value="12">December</option>
                                </select></li>
                        </ul>
                    </div>
                </div>
                <div class="cal-separator"></div>
                <div class="details-frequency">
                    <div class="details-frequency-lt">
                        <h5>Length of Recurrence</h5>
                        <ul>
                            <li><span>Start</span><input class="date-input form-control start_time start_date_recurr" placeholder="" type="text" id="recurr_start_by"/></li>
                        </ul>
                    </div>
                    <div class="details-frequency-rt">
                        <ul id="count_recurr">
                            <li><input type="radio" name="count" value="0" checked/> <label>No End Date</label></li>
                            <li><input type="radio" name="count" value="1"/> <label>End After</label> <input class="cal-day form-control" type="text" id="end_by"/> <label>meeting</label></li>
                            <li class="end-by" ><input type="radio" name="count" value="2"/> <label>End By</label> <input class="date-input form-control start_time"  placeholder="" type="text" id="recurr_end_by"/> </li>
                        </ul>
                    </div>
                    <a href="javascript:void(0)" id="accept_reoccurence">Accept</a>
                </div>
            </div>

        </div>

    </div></div>
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/bootstrap-tagsinput.css" />
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootstrap-tagsinput.min.js"></script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/chat/node_modules/socket.io-client/dist/socket.io.js"></script>
<script>
    var domain="<?php echo $_SESSION[SESSION_DOMAIN]['name']; ?>";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var chatId = "<?php echo $_SESSION[SESSION_DOMAIN]['cuser_id']; ?>";
    console.log('chatId',chatId);
    var socket = io.connect( 'http://localhost:3000', { query: "user_id="+chatId });
    console.log('socket',socket);
    // socket.on('notification', function(data) {
    //     console.log(data);
    //     var date_arr=[];
    //     var date_arr1=[];
    //     $.each(data,function (k,v) {
    //     var dd=v.end_date;
    //     dd=new Date(dd);
    //     var t_index=dd.toString().replace("00:00:00",v.start_time);
    //     console.log("t_index",new Date(t_index));
    //     var mm=new Date(t_index);
    //     var tt=new Date(mm.setMinutes(mm.getMinutes()-parseInt(v.reminder)));
    //     var obj={"id":v.id,"date":tt,"subject":v.subject,"end":v.start_time,"loc":v.location};
    //     date_arr.push(obj);
    //     date_arr1.push(tt);
    //     });
    //     var min = date_arr1.reduce(function (a, b) { return a < b ? a : b; });
    //     console.log("date_arr",date_arr);
    //     console.log("min",min);
    //     $.each(date_arr,function(key,value){
    //      var date=new Date(value.date);
    //      var new_date=new Date();
    //         if(value.date.toString()===min.toString()){
    //          var m=date.getMonth()+1;
    //          var m1=new_date.getMonth()+1;
    //          var make_date=date.getFullYear()+"-"+m+"-"+date.getDate();
    //          var make_date1=new_date.getFullYear()+"-"+m1+"-"+new_date.getDate();
    //          var timee=date.getHours()+":"+date.getMinutes();
    //          var timee1=new_date.getHours()+":"+new_date.getMinutes();
    //          if(make_date.toString()==make_date1.toString() && timee==timee1){
    //          var html="";
    //              html="<tr>\n" +
    //                  "            <td>"+value.subject+"</td>\n" +
    //                  "            <td>"+value.end+"</td>\n" +
    //                  "        </tr>";
    //
    //          $("#reminder_data").html(html);
    //          $("#reminder_location").text(value.loc);
    //          setTimeout(function () {
    //              $("#calendar-reminder").modal('show');
    //          },100);
    //              $.ajax({
    //                  type: 'post',
    //                  url: '/calendar/ajax',
    //                  async: false,
    //                  data: {
    //                      class: "calendar",
    //                      action: "update_status",
    //                      'id':value.id
    //                  },
    //                  success: function (response) {
    //
    //
    //                  }
    //              });
    //          }
    //         }
    //     });
    // });
</script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<?php include_once(COMPANY_DIRECTORY_URL . "/views/company/tenants/modals.php"); ?>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/payments/plaidIntialisation.js"></script>

<!--script type="text/javascript"
        src="https://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>-->
<script>



    $('.start_time').datepicker({
        yearRange: '1990:2030',
        changeMonth: true,
        changeYear: true,
        minDate: 0,
        dateFormat: jsDateFomat
    });
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    $(".start_time").val(date);
  $('.end_time').datepicker({
        yearRange: '1990:2030',
        changeMonth: true,
        changeYear: true,
        minDate: 0,
        dateFormat: jsDateFomat
    });
    var datee = $.datepicker.formatDate(jsDateFomat, new Date());
    $(".end_time").val(datee);

</script>

<script>
    var date=new Date();
    var hour=date.getHours();
    var minutes=date.getMinutes();
    var valAsString = minutes.toString();
    var default_time="";
    if(valAsString.length ===1){
         default_time=hour+':'+'0'+minutes;
    }else {
         default_time=hour+':'+minutes;
    }

    $('.time-picker').timepicker({
        timeFormat: 'h:mm p',
        interval: 15,
       // minTime: '10',
        maxTime: '11:00pm',
        defaultTime: default_time,
        startTime: '12:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
    function timeTo12HrFormat(time)
    {   // Take a time in 24 hour format and format it in 12 hour format
        var time_part_array = time.split(":");
        var ampm = 'AM';

        if (time_part_array[0] >= 12) {
            ampm = 'PM';
        }

        if (time_part_array[0] > 12) {
            time_part_array[0] = time_part_array[0] - 12;
        }

        formatted_time = time_part_array[0] + ':' + time_part_array[1] + ':' + time_part_array[2] + ' ' + ampm;

        return formatted_time;
    }

</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/calendar_main/calendar.js"></script>
<script>

    var events=new Array();
     if(localStorage.getItem('events') !=="") events=JSON.parse(localStorage.getItem('events'));
        console.log('rr',localStorage.getItem('events'));
        document.addEventListener('DOMContentLoaded', function() {
        var Calendar = FullCalendar.Calendar;
        var calendarEl = document.getElementById('calendar5');
        (events !==null) ? events=events : events=[{title:'new events',start:'2019-02-02'}];

        var calendar = new Calendar(calendarEl, {
            plugins: ['interaction','dayGrid','resourceTimeGrid'],
            selectable: true,
            schedulerLicenseKey: '0701543106-fcs-1582552645',
            eventLimit: true,
            timeZone: 'EST',
            defaultView: 'dayGridMonth',
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar
            dateClick: function(info) {


            },
            select: function(info) {

                //   alert('selected ' + info.startStr + 'to' + info.endStr);
            },
            eventClick: function(info) {
                var eventObj = info.event;
                console.log(info.event);
                console.log(info);

            },
            eventReceive:function(info){
            //  alert('gg');
            },
            eventDragStart:function(info){
              //  console.log('rr',info);
            },
            eventDrop:function(info){


            },
            eventDragStop:function(info){

            },
            drop: function(info) {
                alert('dd');
                console.log(info.allDay);
                // is the "remove after drop" checkbox checked?
                if (checkbox.checked) {
                    // if so, remove the element from the "Draggable Events" list
                    info.draggedEl.parentNode.removeChild(info.draggedEl);
                }
            },
            events:events
        });
        calendar.render();

    });
//},1000);
    document.addEventListener('DOMContentLoaded', function() {
        var Calendar = FullCalendar.Calendar;
        var Draggable = FullCalendarInteraction.Draggable;
        var calendarEl = document.getElementById('calendar');
        (events !==null) ? events=events : events=[{title:'new events',start:'2020-02-02'}]

        // -----------------------------------------------------------------

        // initialize the calendar
        // -----------------------------------------------------------------

        var calendar = new Calendar(calendarEl, {
            plugins: ['interaction','dayGrid','resourceTimeGrid'],
            selectable: true,
            schedulerLicenseKey: '0701543106-fcs-1582552645',
            eventLimit: true,
            timeZone: 'EST',
            defaultView: 'dayGridMonth',
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar
            dateClick: function(info) {
                var selected_date=new Date(info.dateStr);
                var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
                var week=(selected_date.getDay());
                week=(days[week]).substr(0,3);
                selected_date= (selected_date.getMonth()+1)+'/'+selected_date.getDate()+'/'+selected_date.getFullYear()+' '+'('+week+'.'+')';
                $(".start_time").val(selected_date);
                $(".end_time").val(selected_date);
                $("#location").val("Office");
                //  $("#newentry").modal('show');
                $("#del_meeting").val("0");
                $("#newentry").modal('show');

            },
            select: function(info) {

                //   alert('selected ' + info.startStr + 'to' + info.endStr);
            },
            eventClick: function(info) {
                var eventObj = info.event;
                console.log(info.event);
                console.log(info);
                if (eventObj.url) {
                    alert(
                        'Clicked ' + eventObj.title + '.\n' +
                        'Will open ' + eventObj.url + ' in a new tab'
                    );
                    window.open(eventObj.url);
                    info.jsEvent.preventDefault(); // prevents browser from following link in current tab.
                } else {
                    var end_date="";
                    var start_date=new Date(eventObj.start);
                    if(eventObj.end !==null){
                        end_date=new Date(eventObj.end)
                    }else{
                        end_date=start_date;
                    }
                    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
                    var week=(start_date.getDay());
                    week=(days[week]).substr(0,3);
                    start_date = (start_date.getMonth()+1)+'/'+start_date.getDate()+'/'+start_date.getFullYear()+' '+'('+week+'.'+')';
                    var week1=(end_date.getDay());
                    week1=(days[week1]).substr(0,3);
                    end_date = (end_date.getMonth()+1)+'/'+end_date.getDate()+'/'+end_date.getFullYear()+' '+'('+week1+'.'+')';
                    $("#del_meeting").val(eventObj.id);
                    $(".start_time").val(start_date);
                    $(".end_time").val(end_date);
                    $("#subject").val(eventObj.title);
                    $("#newentry").modal('show');
                }
            },
            eventReceive:function(info){
                //  alert('gg');
            },
            eventDragStart:function(info){
                //  console.log('rr',info);
            },
            eventDrop:function(info){
                var eventObj=info.event;
                var id=eventObj.id;
                var end_date="";
                var start_date=new Date(eventObj.start);
                if(eventObj.end !==null){
                    end_date=new Date(eventObj.end);
                }else{
                    end_date=start_date;
                }
                var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
                var week=(start_date.getDay());
                week=(days[week]).substr(0,3);
                start_date = (start_date.getMonth()+1)+'/'+start_date.getDate()+'/'+start_date.getFullYear()+' '+'('+week+'.'+')';
                var week1=(end_date.getDay());
                week1=(days[week1]).substr(0,3);
                end_date = (end_date.getMonth()+1)+'/'+end_date.getDate()+'/'+end_date.getFullYear()+' '+'('+week1+'.'+')';
                var data={};
                var event_data=[];
                var i;
                console.log('st',info);
                $.ajax({
                    type: 'post',
                    url: '/calendar/ajax',
                    async: false,
                    data: {
                        class: "calendar",
                        action: "updateafterdrag",
                        start_time:start_date,
                        end_time:end_date,
                        id:id
                    },
                    success: function (response) {
                        var res = JSON.parse(response);
                        if (res.status == "success") {
                            toastr.success(res.message);
                            console.log(res);
                            var grop_arr=[];
                            $("input[name=cal_grp1]").each(function () {
                                if($(this).is(":checked")){
                                    grop_arr.push($(this).val());
                                }
                            });
                            $.ajax({
                                type: 'post',
                                url: '/calendar/ajax',
                                async: false,
                                data: {
                                    class: "calendar",
                                    action: "getdataafterdel",
                                    calendar:grop_arr
                                },
                                success: function (response) {
                                    var res = JSON.parse(response);
                                    if (res.status == "success") {
                                        $("#newentry").modal('hide');
                                        for (i = 0; i < res.count.total; i++) {
                                            data = {
                                                id:res.data2[i].id,
                                                title: res.data2[i].subject,
                                                start: res.data2[i].start_date+'T'+res.data2[i].start_time,
                                                end:res.data2[i].end_date+'T'+res.data2[i].end_time,
                                                allDay : false,
                                                displayEventTime:true
                                            };

                                            event_data.push(data);
                                        }
                                        console.log(event_data);
                                        var event_val = [];
                                        event_val = event_data;
                                        localStorage.setItem('events',JSON.stringify(event_val));
                                        console.log('fdf',$("#events").val());
                                        setTimeout(function () {
                                            if(event_val !== []) {
                                                setTimeout(function () {
                                                    window.location.reload();
                                                },2000);
                                            }
                                        },100);

                                    }

                                }
                            });


                        }
                        //   return event_data;
                    }
                });
            },
            eventDragStop:function(info){

            },
            drop: function(info) {
                alert('dd');
                console.log(info.allDay);
                // is the "remove after drop" checkbox checked?
                if (checkbox.checked) {
                    // if so, remove the element from the "Draggable Events" list
                    info.draggedEl.parentNode.removeChild(info.draggedEl);
                }
            },
            events:events
        });
        calendar.render();
        var st_date="";
        var end_date="";
        var year= date.getFullYear();
        var m=date.getMonth() +1;
        var dd=date.getDate();
        var valAsString1 = m.toString();
        var valAsString2 = dd.toString();
        if(valAsString1.length ===1 && valAsString2.length ===1){
            st_date=year+'-'+'0'+m+'-'+'0'+dd;
            st_date=year+'-'+'0'+m+'-'+'0'+dd;
        }else if(valAsString1.length ===1 && valAsString2.length !==1){
            st_date=year+'-'+'0'+m+'-'+dd;
            end_date=year+'-'+'0'+m+'-'+dd;
        }else if(valAsString1.length !==1 && valAsString2.length ===1){
            st_date=year+'-'+m+'-'+'0'+dd;
            end_date=year+'-'+m+'-'+'0'+dd;
        }else {
            st_date=year+'-'+m+'-'+dd;
            end_date=year+'-'+m+'-'+dd;
        }
        calendar.select(st_date, [end_date])
    });


</script>

<script>

    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar1');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: ['interaction','timeGrid','resourceTimeGrid'],
            timeZone: 'EST',
            selectable: true,
            schedulerLicenseKey: '0701543106-fcs-1582552645',
            defaultView: 'timeGridWeek',
            editable: true,
            droppable: true,
            dateClick: function(info) {
                var index=info.dateStr.indexOf('T');
                var date1=info.dateStr.substr(0,index);
                var time=info.dateStr.substr(index+1,info.dateStr.length);
                time=timeTo12HrFormat(time);
                var selected_date=new Date(date1);
                var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
                var week=(selected_date.getDay());
                week=(days[week]).substr(0,3);
                selected_date= (selected_date.getMonth()+1)+'/'+selected_date.getDate()+'/'+selected_date.getFullYear()+' '+'('+week+'.'+')';
                $(".start_time").val(selected_date);
                $(".end_time").val(selected_date);
                console.log('dd',time);
                $("#st_time").val(time);
                $("#e_time").val(time);
                $("#location").val("Office");
                $("#newentry").modal('show');
            },
            select: function(info) {

                //   alert('selected ' + info.startStr + 'to' + info.endStr);
            },
            dayClick:function(info){
                console.log(info);
            },
            eventClick: function(info) {
                var eventObj = info.event;
                console.log(info.event);
                if (eventObj.url) {
                    alert(
                        'Clicked ' + eventObj.title + '.\n' +
                        'Will open ' + eventObj.url + ' in a new tab'
                    );
                    window.open(eventObj.url);
                    info.jsEvent.preventDefault(); // prevents browser from following link in current tab.
                } else {
                    alert('Clicked ' + eventObj.title);
                }
            },
            eventReceive:function(info){
                console.log(info);
                console.log("dd");
            },
            drop: function(info) {
                alert('dd');
                console.log(info.allDay);
                // is the "remove after drop" checkbox checked?
                if (checkbox.checked) {
                    // if so, remove the element from the "Draggable Events" list
                    info.draggedEl.parentNode.removeChild(info.draggedEl);
                }
            },

            events:events
        });
        calendar.render();
        var st_date="";
        var end_date="";
        var year= date.getFullYear();
        var m=date.getMonth() +1;
        var dd=date.getDate();
        var valAsString1 = m.toString();
        var valAsString2 = dd.toString();
        if(valAsString1.length ===1 && valAsString2.length ===1){
            st_date=year+'-'+'0'+m+'-'+'0'+dd;
            st_date=year+'-'+'0'+m+'-'+'0'+dd;
        }else if(valAsString1.length ===1 && valAsString2.length !==1){
            st_date=year+'-'+'0'+m+'-'+dd;
            end_date=year+'-'+'0'+m+'-'+dd;
        }else if(valAsString1.length !==1 && valAsString2.length ===1){
            st_date=year+'-'+m+'-'+'0'+dd;
            end_date=year+'-'+m+'-'+'0'+dd;
        }else {
            st_date=year+'-'+m+'-'+dd;
            end_date=year+'-'+m+'-'+dd;
        }
        calendar.select(st_date, [end_date])
    });


    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar6');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: ['interaction','timeGrid','resourceTimeGrid'],
            timeZone: 'EST',
            selectable: true,
            schedulerLicenseKey: '0701543106-fcs-1582552645',
            defaultView: 'timeGridWeek',
            editable: true,
            droppable: true,
            dateClick: function(info) {

            },
            select: function(info) {

                //   alert('selected ' + info.startStr + 'to' + info.endStr);
            },
            dayClick:function(info){
                console.log(info);
            },
            eventClick: function(info) {

            },
            eventReceive:function(info){

            },
            drop: function(info) {

            },

            events:events
        });
        calendar.render();

    });

</script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar2');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: [ 'interaction','timeGrid','resourceTimeGrid' ],
            timeZone: 'EST',
            schedulerLicenseKey: '0701543106-fcs-1582552645',
            defaultView: 'timeGridDay',
            selectable: true,
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar
            dateClick: function(info) {
                console.log(info);
                var selected_date=new Date(info.dateStr);
                var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
                var week=(selected_date.getDay());
                week=(days[week]).substr(0,3);
                selected_date= (selected_date.getMonth()+1)+'/'+selected_date.getDate()+'/'+selected_date.getFullYear()+' '+'('+week+'.'+')';
                $(".start_time").val(selected_date);
                $(".end_time").val(selected_date);
                $("#location").val("Office");
                $("#newentry").modal('show');
            },
            select: function(info) {

                //   alert('selected ' + info.startStr + 'to' + info.endStr);
            },
            eventClick: function(info) {
                var eventObj = info.event;
                console.log(info.event);
                if (eventObj.url) {
                    alert(
                        'Clicked ' + eventObj.title + '.\n' +
                        'Will open ' + eventObj.url + ' in a new tab'
                    );
                    window.open(eventObj.url);
                    info.jsEvent.preventDefault(); // prevents browser from following link in current tab.
                } else {
                    alert('Clicked ' + eventObj.title);
                }
            },
            eventDragStop:function(info){
                console.log('fsddd',info);
            },
            eventResize:function(info){
                console.log('fsd',info);
            },
            eventReceive:function(info){
                console.log(info);
                console.log("dd");
            },
            eventResizeStop:function(info){
               console.log(info);
            },
            drop: function(info) {
                alert('dd');
                console.log(info.allDay);
                if (checkbox.checked) {
                    info.draggedEl.parentNode.removeChild(info.draggedEl);
                }
            },
            events:events
        });
        // var slotDuration = calendar.getOption('slotDuration');
        // console.log(slotDuration);
        calendar.render();
        var st_date="";
        var end_date="";
        var year= date.getFullYear();
        var m=date.getMonth() +1;
        var dd=date.getDate();
        var valAsString1 = m.toString();
        var valAsString2 = dd.toString();
        if(valAsString1.length ===1 && valAsString2.length ===1){
            st_date=year+'-'+'0'+m+'-'+'0'+dd;
            st_date=year+'-'+'0'+m+'-'+'0'+dd;
        }else if(valAsString1.length ===1 && valAsString2.length !==1){
            st_date=year+'-'+'0'+m+'-'+dd;
            end_date=year+'-'+'0'+m+'-'+dd;
        }else if(valAsString1.length !==1 && valAsString2.length ===1){
            st_date=year+'-'+m+'-'+'0'+dd;
            end_date=year+'-'+m+'-'+'0'+dd;
        }else {
            st_date=year+'-'+m+'-'+dd;
            end_date=year+'-'+m+'-'+dd;
        }
        calendar.select(st_date, [end_date])
    });

    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar7');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: [ 'interaction','timeGrid','resourceTimeGrid' ],
            timeZone: 'EST',
            schedulerLicenseKey: '0701543106-fcs-1582552645',
            defaultView: 'timeGridDay',
            selectable: true,
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar
            dateClick: function(info) {

            },
            select: function(info) {

                //   alert('selected ' + info.startStr + 'to' + info.endStr);
            },
            eventClick: function(info) {

            },
            eventDragStop:function(info){

            },
            eventResize:function(info){

            },
            eventReceive:function(info){

            },
            eventResizeStop:function(info){

            },
            drop: function(info) {

            },
            events:events
        });
        // var slotDuration = calendar.getOption('slotDuration');
        // console.log(slotDuration);
        calendar.render();
    });
</script>
<script>
    $(document).on('click','#print_btn',function () {
var checked_cal=$("input[name=print_cal]:checked").val();
        if(checked_cal==0){
            $("#month_cal_list1").hide();
            $("#day_cal_list1").show();
            $("#week_cal_list1").hide();
        }else if(checked_cal==1){
            $("#month_cal_list1").hide();
            $("#day_cal_list1").hide();
            $("#week_cal_list1").show();
        }else if(checked_cal==2){
            $("#month_cal_list1").show();
            $("#day_cal_list1").hide();
            $("#week_cal_list1").hide();
        }
    });
    function printcalendar(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data)
    {
        var base_url = window.location.origin;
        var mywindow = window.open('', 'my div');
        $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
            'font-weight:bold;} .right-detail{\n' +
            '        position:relative;\n' +
            '        left:+400px;\n' +
            '    }</style>');
        $(mywindow.document.body).html( '<body>' + data + '</body>');
        mywindow.document.close();
        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
        if(mywindow.close()){

        }
        //   $("#PrintEnvelope").modal('hide');
        return true;
    }


    // $('.invitation_name').tagsinput();
    $('.bootstrap-tagsinput input').removeAttr('style');
    $('.bootstrap-tagsinput').css('width','100%');
    $('input.form-control.subject').css('margin-bottom','10px');

    // $('.bootstrap-tagsinput input').removeAttr('style');
    // $('.bootstrap-tagsinput').css('width','100%');
    // $('input.form-control.subject').css('margin-bottom','10px');
</script>
<!---->
<!--<link rel="stylesheet" href="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/css/bootstrap-tagsinput.css" />-->

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/main.min.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/main.min.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/main.min_week.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/main.min_week1.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/main.min_week2.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/main.min_resource.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/main.min_resource1.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/main.min_resource2.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/interaction.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/resource-common.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/resource-timeline.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/timeline.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/timepicker/jquery.timepicker.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/timepicker/jquery.timepicker.min.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/timepicker/jquery.timepicker.min.js.map"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/fullcal.js" integrity="sha384-vk5WoKIaW/vJyUAd9n/wmopsmNhiy+L2Z+SBxGYnUkunIxVxAv/UtMOhba/xskxh" crossorigin="anonymous"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery-pseudo-ripple.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery-nao-calendar.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-common/main.d.ts"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-common/main.esm.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-common/main.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-common/main.min.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-daygrid/main.d.ts"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-daygrid/main.esm.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-daygrid/main.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-daygrid/main.min.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-timegrid/main.d.ts"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-daygrid/main.esm.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-daygrid/main.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-daygrid/main.min.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-timeline/main.d.ts"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-timeline/main.esm.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-timeline/main.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-timeline/main.min.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/resource-timeline/main.esm.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/timeline/main.d.ts"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/timeline/main.esm.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/timeline/main.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/calendar_module/fullcalendar-scheduler/packages-premium/timeline/main.min.js.js"></script>

