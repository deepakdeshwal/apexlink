<!-- 1)Balance Sheet Model -->
<div class="container">
    <div class="modal fade" id="balance-sheet" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup-balance-sheet-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Balance Sheet Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="bl_sheet_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="bl_sheet_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="bl_sheet_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="bl_sheet_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->



<!-- 2)Balance Sheet Comparison Modal -->
<div class="container">
    <div class="modal fade" id="bl-comparison-modal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup-bl-comparison-modal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Balance-Sheet -2-year Comparison  </h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="bl_comparison_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="bl_comparison_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="bl_comparison_modal_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->



<!-- 3)Income Statement Detail Modal -->
<div class="container">
    <div class="modal fade" id="income_statement_detail" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_income_statement_detail">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Income Statement Details Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="is_detail_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="is_detail_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="is_detail_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="is_detail_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->


<!-- 4)Income Statement Comparison 2Years -->
<div class="container">
    <div class="modal fade" id="income_statement_comparison" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_income_statement_comparison">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Income Statement Comparison(2 Years)</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="is_comparison_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="is_comparison_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="is_comparison_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->



<!-- 5)Profit & Loss Modal -->
<div class="container">
    <div class="modal fade" id="profit_loss" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_profit_loss">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Profit & Loss Filters </h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="profit_loss_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="profit_loss_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="profit_loss_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="profit_loss_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->

<!-- 6)Profit and Loss Detail -->
<div class="container">
    <div class="modal fade" id="profit_loss_detail" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_profit_loss_detail">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Profit & Loss Details Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="pl_detail_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="pl_detail_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="pl_detail_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="pl_detail_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!-- 7)Profit and Loss 12Months -->
<div class="container">
    <div class="modal fade" id="profit_loss_12months" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_profit_loss_12months">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Profit & Loss -12-Months Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            From Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="from_date" id="pl_12months_from_date" class="is_required form-control from_date datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            To Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="to_date" id="pl_12months_to_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="pl_12months_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="pl_12months_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="pl_12months_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!-- 8)Profit & Loss 2-Year Comparison -->
<div class="container">
    <div class="modal fade" id="profit_loss_2years" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_profit_loss_2years">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Profit & Loss 2-Year Comparison</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="pl_2years_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="pl_2years_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="pl_2years_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->


<!-- 9)Profit and Loss Consolidated -->
<div class="container">
    <div class="modal fade" id="profit_loss_consolidated" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_profit_loss_consolidated">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Profit & Loss Consolidated</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="pl_consolidated_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="pl_consolidated_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="pl_consolidated_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="pl_consolidated_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!-- 10)A/R Aging Summary -->
<div class="container">
    <div class="modal fade" id="ar_aging_summary" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_ar_aging_summary">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Account Totals Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            From Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="from_date" id="ar_aging_summary_from_date" class="is_required form-control from_date datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            To Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="to_date" id="ar_aging_summary_to_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="ar_aging_summary_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->




<!-- 11)A/R Aging Detail -->
<div class="container">
    <div class="modal fade" id="ar_aging_detail" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_ar_aging_detail">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">A/R Aging Detail Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="current_date" id="ar_aging_detail_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                            </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="ar_aging_detail_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->

<!-- 12 AP Aging Summary -->
<div class="container">
    <div class="modal fade" id="ap_aging_summary" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_ap_aging_summary">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">AP Aging Summary Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                            
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="ap_aging_summary_portfolio_id" class="form-control"></select>
                                        </div> 
                                     
                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="ap_aging_summary_status" class=" form-control selectfield onChangeStatus" data_next="tenant">
                                                <option value="">Select</option><option value="0" selected="">Active</option><option value="1">InActive</option></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="ap_aging_summary_property_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Vendor<em class="red-star">*</em></label>
                                            <select multiple name="vendor[]" id="ap_aging_summary_vendor_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Ammount Due
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="amountdue" id="ap_aging_summary_amountdue" class="form-control" readonly="readonly">
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="ap_aging_summary_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!-- 13 AP Aging Details -->
<div class="container">
    <div class="modal fade" id="ap_aging_detail" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_ap_aging_detail">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">AP Aging Details Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="ap_aging_detail_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="ap_aging_detail_property_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Vendor<em class="red-star">*</em></label>
                                            <select multiple name="vendor[]" id="ap_aging_detail_vendor_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Ammount Due
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="amountdue" id="ap_aging_summary_amountdue" class="form-control" readonly="readonly">
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="ap_aging_summary_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!-- 14)Audit Log -->
<div class="container">
    <div class="modal fade" id="audit_log" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_audit_log">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Audit Log Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="audit_log_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="audit_log_end_date" class="is_required form-control  end_date datefield calander" readonly="readonly">
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="audit_log_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->

<!-- 15)Income Register Filters -->
<div class="container">
    <div class="modal fade" id="income_register" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_income_register">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Income Register Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="income_register_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="income_register_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="income_register_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="income_register_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->




<!-- 16)Charge Detail Filters -->
<div class="container">
    <div class="modal fade" id="charge_detail" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_charge_detail">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Charge Detail Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="charge_detail_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="charge_detail_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="charge_detail_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="charge_detail_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->


<!-- 17 Expenses by Vendor -->
<div class="container">
    <div class="modal fade" id="expenses_by_vendor" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_expenses_by_vendor">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Expense by Vendor Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="expenses_by_vendor_start_date" class="is_required start_date form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="expenses_by_vendor_end_date" class="is_required end_date form-control datefield calander" readonly="readonly">
                                        </div>
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="expenses_by_vendor_portfolio_id" class="form-control"></select>
                                        </div> 
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="expenses_by_vendor_property_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="expenses_by_vendor_status" class=" form-control selectfield onChangeStatus" data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Vendor<em class="red-star">*</em></label>
                                            <select multiple name="vendor[]" id="expenses_by_vendor_vendor_id" class="form-control"></select>
                                        </div>

                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="expenses_by_vendor_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->




<!-- 18 Property Statement -->
<div class="container">
    <div class="modal fade" id="property_statement" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_property_statement">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Property Statement Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="property_statement_start_date" class="is_required start_date form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="property_statement_end_date" class="is_required end_date form-control datefield calander" readonly="readonly">
                                        </div>
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="property_statement_portfolio_id" class="form-control"></select>
                                        </div> 
                                       

                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="property_statement_status_id" class=" form-control selectfield onChangeStatus" data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                        </div>

                                         
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="property_statement_property_id" class="form-control"></select>
                                        </div>

                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="property_statement_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!-- 19)Bank Account Activity Model -->
<div class="container">
    <div class="modal fade" id="bank_account" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_bank_account">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Bank Account Activity Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                    
                                        <div class="col-sm-12">
                                            <label is_required="yes">Bank name<em class="red-star">*</em></label>
                                            <select multiple name="bank_name" id="bank_account_bank_name"  class="form-control"></select>
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="bank_account_start_date" class="is_required start_date form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="bank_account_end_date" class="is_required end_date form-control datefield calander" readonly="readonly">
                                        </div>
                                       
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="bank_account_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->



<!-- 20)Voided Checks Filter -->
<div class="container">
    <div class="modal fade" id="voided_check" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_voided_check">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Voided Checks Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="voided_check_start_date" class="is_required start_date form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="voided_check_end_date" class="is_required end_date form-control datefield calander" readonly="readonly">
                                        </div>
                                       
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="voided_check_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->


<!-- 21)Trial Balance Model -->
<div class="container">
    <div class="modal fade" id="trial_balance" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_trial_balance">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Trial Balance Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="trial_balance_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="trial_balance_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="trial_balance_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="trial_balance__btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->


<!-- 22)Trial Balance Consolidated Model -->
<div class="container">
    <div class="modal fade" id="trial_balance_consolidated" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_trial_balance_consolidated">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Trial Balance Consolidated</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                         <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="trial_balance_consolidated_start_date" class="is_required start_date form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="trial_balance_consolidated_end_date" class="is_required end_date form-control datefield calander" readonly="readonly">
                                        </div>
                                       
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="trial_balance_consolidated_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="trial_balance_consolidated_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="trial_balance_consolidated_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->

<!-- 23)Adjusted Trial Balance Model -->
<div class="container">
    <div class="modal fade" id="adjust_trial_balance" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_adjust_trial_balance">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Adjusted Trial Balance Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="at_balance_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="at_balance_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="at_balance_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="at_balance__btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->

                    <!--Tenant Modals -->
<!-- 1 Tenant Ledger Filters-->
<div class="container">
    <div class="modal fade" id="tenant_ledger" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_tenant_ledger">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Tenant Ledger Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="tenant_ledger_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="tenant_ledger_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>


                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="tenant_ledger_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="tenant_ledger_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="tenant_ledger_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="tenant_ledger_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="tenant_ledger_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!-- 2) Tenant Statement Filters-->
<div class="container">
    <div class="modal fade" id="tenant_statement" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_tenant_statement">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Tenant Statement Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="tenant_statement_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="tenant_statement_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>


                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="tenant_statement_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="tenant_statement_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="tenant_statement_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="tenant_statement_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="tenant_statement_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!--3) Tenant Payment Filters-->
<div class="container">
    <div class="modal fade" id="tenant_payment" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_tenant_payment">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Tenant Payment Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="tenant_payment_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="tenant_payment_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>


                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="tenant_payment_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="tenant_payment_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="tenant_payment_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="tenant_payment_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="tenant_payment_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!--4) Tenant Consolidated Payment Filters-->
<div class="container">
    <div class="modal fade" id="tenant_consolidated_payment" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_tenant_consolidated_payment">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Tenant Consolidated Payment Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="tc_payment_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="tc_payment_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>


                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="tc_payment_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="tc_payment_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="tc_payment_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="tc_payment_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="tc_payment_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!--5) Tenant Online Payment Filters-->
<div class="container">
    <div class="modal fade" id="tenant_online_payment" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_tenant_online_payment">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Tenant Online Payment Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="to_payment_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="to_payment_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>


                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="to_payment_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="to_payment_property_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="to_payment_tenant_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Payment Type</label>
                                                <select type="select" name="status" id="to_payment_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="1" selected="">Credit/Debit Card Payment</option><option value="0">ACH Payments</option></select>
                                        </div>

                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="to_payment_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!--6) Tenant Unpaid Charges Filters-->
<div class="container">
    <div class="modal fade" id="tenant_unpaid_charges" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_tenant_unpaid_charges">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Tenant Unpaid Charges Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="tu_charges_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="tu_charges_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>


                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="tu_charges_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="tu_charges_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="tu_charges_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="tu_charges_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="tu_charges_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!--7) Delinquent Tenant Filters-->
<div class="container">
    <div class="modal fade" id="delinquent_tenant" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_delinquent_tenant">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Delinquent Tenant Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="delinquent_tenant_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="delinquent_tenant_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="delinquent_tenant_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="delinquent_tenant_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="delinquent_tenant_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="delinquent_tenant_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!--8)Delinquent Tenant Email/Text Filters-->
<div class="container">
    <div class="modal fade" id="delinquent_tenant_email" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_delinquent_tenant_email">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Delinquent Tenant Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="dl_tenant_email_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="dl_tenant_email_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="dl_tenant_email_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="dl_tenant_email_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="dl_tenant_email_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="dl_tenant_email_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!--9)Consolidated Delinquent Tenant Filters-->
<div class="container">
    <div class="modal fade" id="consolidated_dlt" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_consolidated_dlt">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Consolidated Delinquent Tenant </h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            From Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="from_date" id="consolidated_dlt_from_date" class="is_required form-control from_date datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            To Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="to_date" id="consolidated_dlt_to_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="consolidated_dlt_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="consolidated_dlt_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="consolidated_dlt_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="consolidated_dlt_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="consolidated_dlt_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!--10)Short Term Renter List Filters-->
<div class="container">
    <div class="modal fade" id="short_term_renter" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_short_term_renter">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Short Term Renter List Tenant </h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="shrt_term_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="shrt_term_end_date" class="is_required form-control  end_date datefield calander" readonly="readonly">
                                        </div>
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="shrt_term_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="shrt_term_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="shrt_term__btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!--10)Tenant EEO Report Filters--->
<div class="container">
    <div class="modal fade" id="tenant_eeo_report" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_tenant_eeo_report">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Tenant EEO Report Filters </h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="eeo_report_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="eeo_report_end_date" class="is_required form-control  end_date datefield calander" readonly="readonly">
                                        </div>
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="eeo_report_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="eeo_report_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="eeo_report_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="tenant_eeo_report_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->





                    <!-- Maintenance Reports -->
<!-- 1)Open Work Order -->
<div class="container">
    <div class="modal fade" id="open_work_order" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_open_work_order">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Open Work Order</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="ow_order_start_date" id="tenant_ledger_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="ow_order_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="ow_order_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="ow_order_property_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant Name<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="ow_order_tenant_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Estimated Cost</label>
                                            <input type="text" name="estimated_cost" id="ow_order_estimated_cost" class="form-control">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Work Order Numbers</label>
                                            <input type="text" name="work_order_number" id="ow_order_work_order_number" class="form-control">  
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="ow_order_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


                    <!-- Owner Reports -->
<!-- 1)Owner Statement -->
<div class="container">
    <div class="modal fade" id="owner_statement" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_owner_statement">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Owner Statement  Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="owner_statement_start_date" id="tenant_ledger_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="owner_statement_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="owner_statement_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="owner_statement_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="owner_statement_status" class=" form-control selectfield" data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Owner Name</label>
                                            <select multiple name="owner_name[]" id="owner_statement_owner_name" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="owner_statement_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->




                    <!-- Vendor Reports -->
<!-- 1)Expense by Vendor -->
<div class="container">
    <div class="modal fade" id="expense_by_vendor" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_expense_by_vendor">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Expense by Vendor Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="eb_vendor_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="eb_vendor_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="eb_vendor_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="eb_vendor_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="eb_vendor_status" class=" form-control selectfield onChangeStatus" data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Vendor<em class="red-star">*</em></label>
                                            <select multiple name="vendor[]" id="eb_vendor_vendor_id" class="form-control"></select>
                                        </div>

                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="eb_vendor_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->

<!-- 2)Vendor Bills -->
<div class="container">
    <div class="modal fade" id="vendor_bill" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_vendor_bill">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Transaction by Vendor Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="vendor_bill_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="vendor_bill_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Vendor<em class="red-star">*</em></label>
                                            <select multiple name="vendor[]" id="vendor_bill_vendor_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Bill Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="bill_date" id="vendor_bill_bill_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Check/Refrence Number</label>
                                            <input type="text" name="check_number" id="vendor_bill_check_number" class="form-control">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Paid Amount</label>
                                            <input type="text" name="paid_amount" id="vendor_bill_paid_amount" class="form-control">  
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="vendor_bill_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->

<!-- 3)Transaction by Vendor Filters -->
<div class="container">
    <div class="modal fade" id="transaction_vendor" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_transaction_vendor">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Vendor Bills Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="tv_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="tv_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Vendor<em class="red-star">*</em></label>
                                            <select multiple name="vendor[]" id="tv_vendor_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Bill Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="bill_date" id="tv_bill_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Check/Refrence Number</label>
                                            <input type="text" name="check_number" id="tv_check_number" class="form-control">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Paid Amount</label>
                                            <input type="text" name="paid_amount" id="tv_paid_amount" class="form-control">  
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="transaction_vendor_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!-- Nidhi Modals -->

<!-- General Finance Starts -->
<!-- Balance Sheet Model Starts -->
<div class="container">
    <div class="modal fade" id="balance-sheet-detail" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupBalanceSheetData">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Balance Sheet Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="bl_current_date" class="is_required form-control datefield calander current_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="balance_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn test" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Balance Sheet Model Ends-->

<!-- Balance Sheet Consolidate Model Starts-->
<div class="container">
    <div class="modal fade" id="balance-sheet-consolidated" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupBalanceSheetConsolidated">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Balance Sheet Consolidated</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="bl_consolidate_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="consolidated_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="consolidated_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn balance_sheet_consolidated" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Balance Sheet Consolidate Model Ends-->

<!-- Income Statement Model Starts-->
<div class="container">
    <div class="modal fade" id="income_statement" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupIncomeStatement">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Income Statement</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="income_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="income_statement_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="income_statement_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn income-statement" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Income Statement Model Ends-->

<!-- Income Statement Consolidated Model Starts-->
<div class="container">
    <div class="modal fade" id="income_statement_consolidated" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupIncomeStatementConsolidated">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Income Statement Consolidated</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="income_con_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="income_consolidated_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="income_consolidated_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn income_statement_consolidated" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Income Statement Consolidated Model Ends-->

<!-- Income Statement -12 Months Model Starts-->
<div class="container">
    <div class="modal fade" id="income_statement_months" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupIncomeStatementMonth">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Income Statement -12 Months Camparison</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            From Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="from_date" id="income_statement_from_date" class="is_required form-control datefield  capital fromDate" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="income_statement_end_date" class="form-control datefield end_date" readonly="readonly"></div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="income_months_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="income_months_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn income_statement_months" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Income Statement -12 Months Model Ends-->

<!-- Cash Flow Model Starts-->
<div class="container">
    <div class="modal fade" id="cash" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupCash">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Cash Flow Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="cash_start_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="cash_end_date" class="form-control datefield end_date" readonly="readonly"></div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="cash_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="cash_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn cash" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Cash Flow Model Model Ends-->

<!-- Cash Flow -12 Months Model Starts-->
<div class="container">
    <div class="modal fade" id="cash_flow_months" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupCashFlowMonths">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Cash Flow-12 Months Comparison</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12"><label is_required="no">From Date</label><input type="text" name="cash_flow_end_date" id="end_date" class="form-control datefield fromDate" readonly="readonly"></div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            To Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="cash_flow_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                      
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="cash_flow_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="cash_flow_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn cash_flow" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Cash Flow Model -12 Months Model Ends-->

<!-- Cash Flow Comparsion Model Starts-->
<div class="container">
    <div class="modal fade" id="cash_flow_comparsions" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupCashflowComparsions">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Cash Flow Comparsion</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="cash_flow_comparsion_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="cash_flow_comparsion_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn cash_flow_comparsions" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Cash Flow Comparsion Model Ends-->

<!-- Equality Statement Model Starts-->
<div class="container">
    <div class="modal fade" id="gfrm1_equaity" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupEquality">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Equity Statement Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="equality_start_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>
                                         <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="equality_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="equity_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="equity_balance_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn equity_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Equality Statement Model Ends-->

<!--Account Total Filters Starts-->
<div class="container">
    <div class="modal fade" id="gfrm1_accounts" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupAccounts">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Account Total Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            From Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="from_date" id="account_from_date" class="is_required form-control datefield  capital fromDate" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="account_end_date" class="form-control datefield end_date" readonly="readonly"></div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn accounts_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Account Total Filters Ends-->

<!--Expense Distribution Starts-->
<div class="container">
    <div class="modal fade" id="gmf2_expenses" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupExpenseDistribution">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Expense Distribution</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="expenses_start_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="expenses_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="expenses_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="expenses_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn expense_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Expense Distribution Ends-->

<!--Expense Register Starts-->
<div class="container">
    <div class="modal fade" id="gfrm1_exp_register" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupExpenseRegister">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Expense Register Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="exp_register_start_date" class="is_required form-control datefield  capital fromDate" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="exp_register_end_date" class="form-control datefield end_date" readonly="readonly"></div>

                                         <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="exp_register_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="exp_register_property_id" class="form-control"></select>
                                            
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Vendor Name<em class="red-star">*</em></label>
                                             <select multiple name="vendor_id[]" id="exp_register_vendor_id" class="form-control"></select>
                                            
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Bill Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="bill_date" id="exp_register_bill_date" class="is_required form-control datefield  capital fromDate" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                          Paid/Unpaid Amount
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="paid_amount" id="exp_register_bill_date" class="is_required form-control   capital">
                                        </div>

                                         <div class="col-sm-12">
                                            <label is_required="yes">
                                           Reference Number/Check Number
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="reference" id="exp_register_bill_date" class="is_required form-control   capital">
                                        </div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                          Check Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="reference" id="exp_register_bill_date" class="is_required form-control start_date  capital" readonly="readonly">
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn exp_register" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Expense Register Ends-->

<!--General Leger Filter Starts-->
<div class="container">
    <div class="modal fade" id="gmf3_ledger" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupGeneralLedger">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">General Ledger Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="ledger_start_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="ledger_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="ledger_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="ledger_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn ledger_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--General Leger Filter Ends-->
<!--Security Deposit Filter Starts-->
<div class="container">
    <div class="modal fade" id="gmf4_security" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupSecurity">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Security Deposit Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="security_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="security_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn security_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Security Deposit Filter Ends-->

<!--Open Invoice Filter Starts-->
<div class="container">
    <div class="modal fade" id="gmf4_open_invoice" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupOpenInvoice">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Open Invoice Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                       <div class="col-sm-12">
                                            <label is_required="yes">
                                           Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="open_invoice_start_date" class="is_required form-control start_date  capital fromDate" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="open_invoice_end_date" class="form-control datefield end_date" readonly="readonly"></div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="open_invoice_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="open_invoice_property_id" class="form-control"></select>
                                            
                                        </div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant Name<em class="red-star">*</em></label>
                                             <select multiple name="" id="open_invoice_tenant_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn open_invoice_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Open Invoice Filter Ends-->
<!--Aduit Log Filter Starts-->
<div class="container">
    <div class="modal fade" id="gfrm1_aduit_log" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupAduitLog">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Aduit Log Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="from_date" id="aduit_start_date" class="is_required form-control datefield  capital start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="aduit_end_date" class="form-control datefield end_date" readonly="readonly"></div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn aduit_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Aduit Log Filter ends-->

<!--Recurring Journal Entries Filter Starts-->
<div class="container">
    <div class="modal fade" id="gmf4_recurring" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupRecurring">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Recurring Journal Entries Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                     
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="recurring_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="recurring_property_id" class="form-control"></select>
                                            
                                        </div>
                                       
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn recurring_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Recurring Journal Entries Filter Ends-->

<!--Recent Automatic Transaction Starts-->
<div class="container">
    <div class="modal fade" id="gf1_auto_trans" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupRecentAutomatic">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Recent Automatic Transaction</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="automatic_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="automatic_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="automatic_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="automatic_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn automatic_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Recent Automatic Transaction Ends-->

<!--Transaction Detail Bt Account Starts-->
<div class="container">
    <div class="modal fade" id="gf1_transactions" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupExpenseLedger">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Transaction Detail By Account</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="transaction_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="transactions_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="transactions_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="transactions_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn transactions_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Transaction Detail Bt Account Ends-->

<!--Journal Entry Filter Starts-->
<div class="container">
    <div class="modal fade" id="gf1_journal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupJournalEntry">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Journal Entry</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="journal_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="journal_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="journal_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="journal_property_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn journal_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Journal Entry Filter Ends-->

<!--Deposit Register Starts-->
<div class="container">
    <div class="modal fade" id="gf1_deposit" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupDeposit">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Deposit Register</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="deposit_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="deposit_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn deposit_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Deposit Register Filter Ends-->

<!--Check Register Starts-->
<div class="container">
    <div class="modal fade" id="gf1_check" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupCheck">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Check Register</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="check_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="check_end_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                       
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn check_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Check Register Filter Ends-->

<!--Annual Budget Comparison Filter Starts-->
<div class="container">
    <div class="modal fade" id="gf1_annual_budget" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupAnnualBudget">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Annual Budget Comparison Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="annual_budget_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="annual_budget_property_id" class="form-control"></select>
                                            
                                        </div>

                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Month
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="annual_budget_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn annual_budget_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Annual Budget Comparison Filter Ends-->

<!--Budget vs Actual Filter Starts-->
<div class="container">
    <div class="modal fade" id="gf1_budget_annual" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupAnnualvsBudget">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Budget vs Actual Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="budget_annual_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="budget_annual_property_id" class="form-control"></select>
                                            
                                        </div>

                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="annual_budget_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn budgets_annual_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!---Budget vs Actual Filter Ends-->

<!--Adjusting Journal Entries Filter Starts-->
<div class="container">
    <div class="modal fade" id="adjusting_journal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupAdjustingJournal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Adjusting Journal Entries Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Account Peroid
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="adjusting_journal_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="adjusting_journal_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="adjusting_journal_property_id" class="form-control"></select>
                                            
                                        </div>     
                                        
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn adjusting_journal_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!---Adjusting Journal Entries Filter Filter Ends-->

<!--Collection Filter Starts-->
<div class="container">
    <div class="modal fade" id="gmf_collection" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupCollection">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Collection Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                       <div class="col-sm-12">
                                            <label is_required="yes">
                                           Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="collection_start_date" class="is_required form-control start_date  capital fromDate" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="collection_end_date" class="form-control datefield end_date" readonly="readonly"></div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="collection_portfolio_id" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                         <select multiple name="property[]" id="collection_property_id" class="form-control"></select>
                                            
                                        </div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant Name<em class="red-star">*</em></label>
                                             <select multiple name="Tenant[]" id="collection_tenant_id" class="form-control"></select>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn collection_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Collection Filter Ends-->

<!-- General Finance Ends -->
<!-- Employees Reports Starts -->
<!-- Payment Plan Consolidated Starts -->
<div class="container">
    <div class="modal fade" id="emp1_ppc" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupPaymentPlans">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Payment Plan Consolidated Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="emp_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                         <div class="col-sm-12">
                                            <label is_required="yes">
                                           End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="emp_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Payer<em class="red-star">*</em></label>
                                            <select  name="payer" id="emp_payer_id" class="form-control">
                                               <option value="">Select</option>
                                               <option value="Vendor">Vendor</option>
                                               <option value="Owner">Owner</option>
                                               <option value="Employee">Employee</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                         <div class="col-sm-12">
                                            <label is_required="yes">Status<em class="red-star">*</em></label>
                                            <select  name="payer" id="emp_payer_id" class="form-control">
                                               <option value="">Select</option>
                                               <option value="Vendor">All</option>
                                               <option value="Owner">Paid-In-Full</option>
                                               <option value="Employee">In-Process</option>
                                             
                                            </select>
                                        </div>
                                     
                                        
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn emp_pay_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Payment Plan Consolidated Ends -->
<!-- Employees Reports Ends -->

<!-- Inventory Reports Starts -->

<!-- Inventory Transfer Starts -->

<div class="container">
    <div class="modal fade" id="ir_model" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupInventoryTransfer">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Inventory Transfer Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div class="col-sm-12">
                                            <label is_required="yes">Inventory Transfer Filter<em class="red-star">*</em></label>
                                            <select  name="payer" id="trans_id" class="form-control">
                                               <option value="">Select</option>
                                               <option value="All">All</option>
                                               <option value="Active">Active</option>
                                               <option value="Inactive">Inactive</option>
                                             
                                            </select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="inventory_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="inventory_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="inventory_property_id" class="form-control"></select>
                                            
                                        </div>     
                                        
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn inventory_transfer_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Inventory Transfer Ends -->

<!-- Supplier Purchase Starts -->

<div class="container">
    <div class="modal fade" id="ir_supplier" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupSupplierPurchase">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Supplier Purchase</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div class="col-sm-12">
                                            <label is_required="yes">Inventory Transfer Filter<em class="red-star">*</em></label>
                                            <select  name="payer" id="trans_id" class="form-control">
                                               <option value="">Select</option>
                                               <option value="All">All</option>
                                               <option value="Active">Active</option>
                                               <option value="Inactive">Inactive</option>
                                             
                                            </select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="supplier_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="supplier_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="supplier_property_id" class="form-control"></select>
                                            
                                        </div>     
                                        
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn supplier_purchase_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Supplier Purchase Ends -->

<!-- Inventory Reports Ends -->

<!-- Property Starts -->

<!-- Vacant Unit Starts -->
<div class="container">
    <div class="modal fade" id="property_vacant" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupVacantUnit">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Vacant Unit Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="vacant_date" class="is_required form-control datefield calander current_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="vacant_portfolio_id" class="form-control"></select>
                                        </div>
                                         <div class="col-sm-12">
                                            <label is_required="yes">Status<em class="red-star">*</em></label>
                                            <select  name="payer" id="vacant_status_id" class="form-control">
                                               <option value="">Select</option>
                                               <option value="All">All</option>
                                               <option value="1">Active</option>
                                               <option value="0">Inactive</option>
                                             
                                            </select>
                                        </div>

                                         <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="vacant_property_id" class="form-control"></select>
                                            
                                        </div> 
                                     
                                        
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn vacant_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Vacant Unit Ends -->
<!-- Property Statement Starts -->
<div class="container">
    <div class="modal fade" id="property_statement" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupPropertyStatement">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Property Statement Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="properties_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                        </div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                           End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="properties_date" class="is_required form-control datefield calander end_date" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="properties_portfolio_id" class="form-control"></select>
                                        </div>
                                         <div class="col-sm-12">
                                            <label is_required="yes">Status<em class="red-star">*</em></label>
                                            <select  name="status" id="prop_record_id" class="form-control">
                                               <option value="">Select</option>
                                               <option value="All">All</option>
                                               <option value="1" selected="selected">Active</option>
                                               <option value="0">Inactive</option>
                                             
                                            </select>
                                        </div>

                                         <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="properties_property_id" class="form-control"></select>
                                            
                                        </div> 
                                     
                                        
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn properties_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Property Statement Ends -->
<!-- Unpaid Bill Statement Starts -->
<div class="container">
    <div class="modal fade" id="ps_unbill" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupUnbill">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Unpaid Bills by Property Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                       

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="unbill_portfolio_id" class="form-control"></select>
                                        </div>
                                         <div class="col-sm-12">
                                            <label is_required="yes">Status<em class="red-star">*</em></label>
                                            <select  name="payer" id="pr_unbill_id" class="form-control">
                                               <option value="">Select</option>
                                               <option value="All">All</option>
                                               <option value="1" selected="selected">Active</option>
                                               <option value="0">Inactive</option>
                                             
                                            </select>
                                        </div>

                                         <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                             <select multiple name="property[]" id="unbill_property_id" class="form-control"></select>
                                            
                                        </div> 
                                          <div class="col-sm-12">
                                            <label is_required="yes">Vendor Name<em class="red-star">*</em></label>
                                             <select multiple name="vendor_id[]" id="unbill_vendor_id" class="form-control"></select>
                                            
                                        </div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">Amount Due<em class="red-star">*</em></label>
                                             <input type="text" name="" id="unbill_amount" class="form-control"></input>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn unbill_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Unpaid Bill Statement Ends -->
<!-- Rental Filter Starts -->
<div class="container">
    <div class="modal fade" id="pr_rental" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupRental">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Rental Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                       <div class="col-sm-12">
                                            <label is_required="yes">
                                           From Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="rental_start_date" class="is_required form-control start_date  capital fromDate" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12"><label is_required="no">End Date</label><input type="text" name="end_date" id="rental_end_date" class="form-control datefield end_date" readonly="readonly"></div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">Applicant Name<em class="red-star">*</em></label>
                                            <select multiple name="" id="rental_applicant_id" class="form-control"></select>
                                        </div>
                                            
                                       
                                       
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <a id="" class="blue-btn rental_btn" >Apply Filter</a>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Rental Filter Ends -->
<!-- Property Ends -->




<!-- end --> 
