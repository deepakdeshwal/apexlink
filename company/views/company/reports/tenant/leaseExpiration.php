<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container lease-container">

<div class="lease-links">
<a href="">Email This Report</a>
    <a href="">Filters</a>
</div>
                   <div class="lease-outer">
                 <div class="lease-top">
                     <ul class="list-inline">
                         <li><a href="#"><span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span></a></li>
                         <li><a href="#"><i class="fa fa-caret-left" aria-hidden="true"></i></a></li>
                         <li ><a href="#" class="input-outer"><input type="text" class="form-control" id="usr"> <span> of 8</span></a> </li>
                         <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
                         <li><a href="#"><span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span></a></li>
                     </ul>

                     <div class="find-outer">
                         <ul class="list-inline">
                             <li>
                         <a href="#" class="input-outer"><input type="text" class="form-control" id="usr"> <span> of 8</span></a></li>
                             <li><a href="">Find</a></li>
                             <li><a href="">Next</a></li>
                             <li class="save-list"><button ><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i></button>

                                 <div id="demo">

                                     <ul style=" display: none;" class="save-document-list">
                                         <li>lorem</li>
                                         <li>lorem</li>
                                         <li>lorem</li>
                                         <li>lorem</li>
                                     </ul>
                                 </div></li>
                             <li><a href=""></a></li>
                             <li><a href=""><i class="fa fa-refresh" aria-hidden="true"></i></a></li>
                             <li><a href=""><i class="fa fa-upload" aria-hidden="true"></i></a></li>
                     </div>



                 </div>




                <div class="grid-outer">
                    <div class="table-responsive Lease-table">
                        <div class="lease-hading">LEASE TABLE</div>
                        <table class="table table-hover table-dark">
                            <thead>
                            <tr>
                                <th scope="col">Property</th>
                                <th scope="col">Units
                                </th>
                                <th scope="col">Oct-2019</th>
                                <th scope="col">Nov-2019</th>
                                <th scope="col">Dec-2020</th>
                                <th scope="col">Jan-2020</th>
                                <th scope="col">Feb-2020</th>
                                <th scope="col">Mar-2020</th>
                                <th scope="col">Apr-2020</th>
                                <th scope="col">May-2020</th>
                                <th scope="col">June-2020</th>
                                <th scope="col">Jul-2020</th>
                                <th scope="col">AUG-2020</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>
                                <td>555-444-6666</td>
                                <td>ashleym524@gmail.com</td>
                                <td>Bill Farms</td>
                                <td>Bill B20</td>
                                <td>600.00</td>
                                <td>(575.00)</td>
                                <td>317</td>
                                <td>Active</td>
                                <td>317</td>
                                <td>Active</td>
                                <td>317</td>

                                <td><select class="form-control"><option>Select</option></select></td>
                            </tr>



                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/reportsView.js"></script>

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



</script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>

