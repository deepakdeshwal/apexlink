<?php
if ((!isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) && (!isset($_SESSION['Admin_Access']['vendor_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/VendorPortal/login');
}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_header.php");
?>

	<div id="wrapper">
	    <!-- Top navigation start -->
	    <?php
	       include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_top_navigation.php");
	    ?>
	<section class="main-content view-owner-page">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="content-section">
                        <!--Tabs Starts -->
                        <div class="container lease-container">
                    <div class="lease-links"><a id="emailOpenModalBtn">Email This Report</a><a class="show_fliter">Filters</a></div>
                    
                    <div class="lease-outer">
                    <div class="lease-top">
                       <ul class="list-inline">
                          <li></li>
                          <li ><a href="#" class="input-outer"><input type="text" class="form-control" id="usr" value="1" title="Current Page"> <span> of   </span><span id="totalPages">1</span></a> </li>
                          <li></li>
                       </ul>
                       <div class="find-outer">
                          <ul class="list-inline">
                              <li>
                                 <a href="#" class="input-outer"><input type="text" class="form-control" id="usr" title="Find Text in Report"> <span> </span></a>
                              </li>
                              <li><a href="" title="Find">Find</a></li>
                              <li>|</li>
                              <li><a href="?page=' . ($page + 1) . '" title="Find Next">Next</a></li>
                              <li class="save-list" title="Export drop down menu">
                                 <button class="ddlList"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i></button>
                                 <div id="demo">
                                    <ul style=" display: none; cursor: pointer; background-color: #ECE9D8; color: #337ab7" class="save-document-list doc_download">
                                       <li>Word</li>
                                       <li>Excel</li>
                                       <li>PowerPoint</li>
                                       <li>PDF</li>
                                       <li>TIFF file</li>
                                       <li>MHTML(web archive)</li>
                                       <li>CSV(comma delimited)</li>
                                       <li>XML file with report data</li>
                                     </ul>
                                 </div>
                              </li>
                              <li><a href=""></a></li>
                              <li><a href="?page=1"><i class="fa fa-refresh" title="Refresh "aria-hidden="true"></i></a></li>
                              <li><a class="print_class" style="cursor: pointer;"><i class="fa fa-download" title="Download" aria-hidden="true"></i></a></li>
                            </ul>
                       </div>
                       </div>
                    <div class="grid-outer htmldownload" id="htmldownload">
                        <div class="table-responsive Lease-table">
	                        <div class="rentroll">

	                    </div>
	                    <!--tab Ends -->
            		</div>
            
        </div>
    </section>
</div>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/vendor-portal/reports.js"></script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_footer.php");
?>

<script>
    $("#work-reports").addClass("active");
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
    <!--- Accordians -->
</script>