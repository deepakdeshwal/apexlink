<?php
if ((!isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) && (!isset($_SESSION['Admin_Access']['vendor_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/VendorPortal/login');
}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_header.php");
?>

    <div id="wrapper">
        <?php  include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_top_navigation.php"); ?>
        
        
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="content-section">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse">
                            <span></span> Vendor Credential Control</a>
                    </h4>
                </div>
                <div id="collapseEight" class="panel-collapse collapse  in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="credentialMainDiv"></div>
                        </div>
                    </div>
                </div>
            </div></div></div></div></div>

        </section>
    </div>
        <!-- Wrapper Ends -->

<script>var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";</script>
<script>
    $(document).ready(function () {
        $('.calander').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: jsDateFomat
        });
        var date = $.datepicker.formatDate(jsDateFomat, new Date());
        $('.calander').val(date);
    });

    var default_Image = "<?php echo COMPANY_SUBDOMAIN_URL ?>/images/profile-img.png";
    var vendor_unique_id = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'] : $_SESSION['Admin_Access']['vendor_portal_id'] ?>';
    var pagination = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['pagination']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['pagination'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['pagination'] : $_SESSION['Admin_Access']['pagination'] ?>';
    var datepicker = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'] : $_SESSION['Admin_Access']['datepicker_format'] ?>';
    var jsDateFomat = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'] : $_SESSION['Admin_Access']['datepicker_format'] ?>';
    var staticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Vehicledummy.png">';
    var petStaticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg">';
    var vendor_id = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'] : $_SESSION['Admin_Access']['vendor_portal_id'] ?>';
    var upload_url = "<?php echo SITE_URL; ?>";
    var default_currency_symbol = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_currency_symbol']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_currency_symbol'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_currency_symbol'] : $_SESSION['Admin_Access']['default_currency_symbol'] ?>';
    var default_name = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_name']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_name'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_name'] : $_SESSION['Admin_Access']['default_name'] ?>';
    var vendorPortalNote = '';
</script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/vendor-portal/vendorInsurance.js"></script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_footer.php");
?>

<script>
    $("#vendorinsurance").addClass("active");
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
    <!--- Accordians -->
</script>