
<style>
  .getPropertyData {
    max-height: 150px;
    overflow: auto;
  }
  .getPropertyData p{ margin: 0; padding-left: 5px; }
  .getPropertyData p a{ color: #000; }
</style>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

  <div id="wrapper">
      <link rel="stylesheet" type="text/css" href="<?php echo COMPANY_URL; ?>/company/css/jquery.datetimepicker.css"/>
 <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- MAIN Navigation Ends -->
        
        
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                      <div class="row">
                        <div class="col-sm-8">
                          <!-- <div class="breadcrumb-outer">
                            People Module >> <span>New Employee</span>
                          </div> -->
                        </div>
                        <div class="col-sm-4">
                          <div class="easy-search">
                              <input placeholder="Easy Search" type="text">
                          </div>
                        </div>
                      </div>
                    </div>
                   <form id="addLostFoundData"> 
                    <input type="hidden" name="user_id" class="user_id">
                    <input type="hidden" name="type" value="L">
                    <input type="hidden" name="property_id" class="property_id">
                    <div class="col-sm-12">
                      <div class="content-section">
                         <!--Tabs Starts -->
                        <div class="main-tabs">                        
                          <!-- Nav tabs -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="guest-cards">
                              <div class="form-outer">
                                <div class="form-hdr">
                                  <h3>New Lost Item Details <a class="back" href="/LostAndFound/LostAndFound"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                </div>
                                <div class="form-data">
                                  <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Lost Item Name <em class="red-star">*</em></label>
                                      <input class="form-control capital" type="text" name="lost_item_name"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Item Number</label>
                                      <input class="form-control" type="text" name="item_number" value='<?php echo 'L-'.rand(100000,9999999); ?>' readonly />
                                    </div>


                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                    <label>Category <em class="red-star">*</em> <a class="pop-add-icon" data-popup = "NewCategoryPopup" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                 <select class="form-control category" name="category" id="category">
                                      <option value="">Select</option>                               
                                   </select>
                                    <div class="add-popup categoryPopup" id='NewCategoryPopup' style="width: 97%;">
                                                    <h4>Add Category</h4>
                                                    <div class="add-popup-body">

                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>Category<em class="red-star">*</em></label>
                                                                <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20"  id='data-category'/>
                                                                <span class="customErrorCategory required"></span>
                                                            </div>
                                                      
                                                            <div class="btn-outer text-right">
                                                                <input type="button" id='addNewCategory' class="blue-btn addSingleData" value="Save" data-table='maintenance_category' data-cell="category_name" data-value="data-category" data-error="customErrorCategory" data-select = "category" data-hide="categoryPopup"/>
                                                                <input type="button" class="clear-btn ClearCategory" value='Clear' />
                                                                <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                    </div>


                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Brief Decription <em class="red-star">*</em></label>
                                      <input class="form-control capital" type="text" name="brand_description"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Brand Name</label>
                                      <input class="form-control capital" type="text" name="brand_name"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 lost-item">
                                      <div class="check-outer">
                                        <input name="userInfo" class="userInfo" checked="" type="radio" value="2"/><label>Tenant</label>
                                      </div>
                                      <div class="check-outer">
                                        <input name="userInfo" class="userInfo" type="radio" value="4"/><label>Owner</label>
                                      </div>
                                      <div class="check-outer">
                                        <input name="userInfo" class="userInfo" type="radio" value="3"/ ><label>Vendor</label>
                                      </div>
                                      <div class="check-outer">
                                        <input name="userInfo" class="userInfo" type="radio" value="1"/><label>Staff</label>
                                      </div>
                                     <input type="text" class="form-control userData" name="demoUser" placeholder="Click Here to select a user">
                                     <div class="getUserData getuser-lostdata"></div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Property Name <em class="red-star">*</em></label>
                                        <select name="user_property" class="form-control user_property property_name "></select>
<!--                                        <input class="form-control add-input user_property" type="text" name="user_property" readonly>-->
                                       <div class="getPropertyData"></div>
                                      <a class="add-icon" href="javascript:;"></a>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Color <a class="pop-add-icon" href="javascript:;" data-popup="NewColorPopup"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                    <select class="form-control color" name="color" id="color">
                                                       
                                   </select>
                                             <div class="add-popup colorPopup" id='NewColorPopup' style="width: 97%;">
                                                    <h4>Add Color</h4>
                                                    <div class="add-popup-body">

                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>Color<em class="red-star">*</em></label>
                                                                <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20" id='data-color'/>
                                                                <span class="customError required customErrorColor"></span>
                                                            </div>
                                                      
                                                            <div class="btn-outer text-right">
                                                                <input type="button" id='addColor' class="blue-btn addSingleData" value="Save" data-table='maintenance_color' data-cell="color_name" data-value="data-color" data-error="customErrorColor" data-select = "color" data-hide="colorPopup" />
                                                                <input type="button" class="clear-btn ClearColor" value='Clear' />
                                                                <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 clear">
                                      <label>Age of Item <a class="pop-add-icon" href="javascript:;" data-popup="NewItemPopup"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                     <select class="form-control data-item item" name="item" id="item">
                                         <option value="">Select</option>                            
                                   </select>
                                           <div class="add-popup itemPopup" id='NewItemPopup' style="width: 97%;">
                                                    <h4>Add Item</h4>
                                                    <div class="add-popup-body">

                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>Add Item<em class="red-star">*</em></label>
                                                                <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20" name='data-item' id='data-item'/>
                                                                <span class="customError required customErrorItem"></span>
                                                            </div>
                                                      
                                                            <div class="btn-outer text-right">
                                                                <input type="button" id='NewportfolioPopupSave' class="blue-btn addSingleData" value="Save" data-table='maintenance_item' data-cell="item_name" data-value="data-item" data-error="customErrorItem" data-select = "item" data-hide="itemPopup" />
                                                                <input type="button" class="clear-btn ClearItem" value='Clear' />
                                                                <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Serial / ID Number</label>
                                      <input class="form-control" type="text" name="serial_number"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Location Lost <em class="red-star">*</em></label>
                                      <input class="form-control capital" type="text" name="loation_lost"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Lost Date / Time <em class="red-star">*</em></label>
                                      <input class="form-control lost_date" type="text" name="lost_date"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Other Details</label>
                                      <textarea class="form-control capital" name="other_details"></textarea>
                                    </div>
                                  </div>
                                </div>
                              </div>
                               <!-- Form Outer Ends -->
                                   <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeventeen">
                                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> File Library</a>
                                            </h4>
                                        </div>
                                        <div id="collapseSeventeen" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-4">
                                                            <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                            <input id="file_library" type="file" name="file_library[]" accept="image/x-png,image/gif,image/jpeg" multiple style="display: none;">

<!--                                                            <input id="file_library" type="file" name="file_library[]" accept="image/x-png,image/gif,image/jpeg" multiple style="display: none;">-->
                                                           
                                                            <input type="button" class="orange-btn" id="remove_library_file" value="Remove All Files...">
                                                          
                                                        </div>
                                                        <div class="row" id="file_library_uploads">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid-outer">
                                                    <div class="apx-table">
                                                        <div class="table-responsive">
                                                            <table id="propertFileLibrary-table" class="table table-bordered">
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                              
                                            </div>
                                        </div>
                                    </div>
                              <!-- Form Outer Ends -->
                              <div class="form-outer">
                                <div class="form-hdr">
                                  <h3>Owner Contact Details</h3>
                                </div>
                                <div class="form-data">
                                  <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Full Name</label>
                                      <input class="form-control full_name setinput_readonly" type="text" name="full_name" readonly/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Address</label>
                                      <input class="form-control user_address setinput_readonly" name="user_address" type="text" readonly/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Email</label>
                                      <input class="form-control user_email setinput_readonly" type="text" name="user_email" readonly/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Phone Number</label>
                                      <input class="form-control user_phone setinput_readonly" type="text" name="user_phone" readonly/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Unit Number</label>
                                      <input class="form-control add-input user_unit setinput_readonly" name="user_unit" type="text" readonly>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <!-- Form Outer Ends -->

                           


                              <!-- Form Outer Ends -->
                            </div>
                                <div class="btn-outer text-right">
                                    <input type="hidden" class="onsubmit" value=""/>
                                    <input type="submit" name="save_close" class="blue-btn" value="Save & Close" id="save_close" />
                                    <input type="submit" name="save_another" class="blue-btn" value="Save & Add Another"  id="save_add_another"/>
                                    <input type="button" name="Clear"  class="clear-btn clearFormReset" value="Clear" />
                                    <input type="button" name="cancel"  class="grey-btn" value="Cancel" id="cancel_add"/>
                                </div>

                        </div>
                        <!--tab Ends -->
                        
                    </div>

                    </div>
                </div>
              </form>
            </div>
        </section>
    </div>


</div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js" type="text/javascript"></script>
     <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.datetimepicker.full.min.js"></script>
   
 <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
 

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/maintenance/lostandfound/lostandfound.js"></script>
 <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
 <script>var upload_url = "<?php echo SITE_URL; ?>";</script>
<!-- Wrapper Ends -->

    <script>
        $('.lost_date').datetimepicker({
            calendarWeeks: true,
            locale: moment.locale('se'),
            maxDate: '0'
        });
    </script>

<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });

        $(document).on('click','.clearFormReset',function () {
            resetFormClear("#addLostFoundData",['item_number','lost_date'],'form',false);
        });

        $(document).on('click','.ClearCategory',function () {
            $('#data-category').val('');
        });

        $(document).on('click','.ClearColor',function () {
            $('#data-color').val('');
        });

        $(document).on('click','.ClearItem',function () {
            $('#data-item').val('');
        });
    });



</script>


<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>