<?php
if ((!isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) && (!isset($_SESSION['Admin_Access']['vendor_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/VendorPortal/login');
}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_header.php");
?>

<div id="wrapper">
    <?php  include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_top_navigation.php"); ?>


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse">
                                        <span></span>
                                        Vendor Reports</a>
                                </h4>
                            </div>
                            <div id="collapseEight" class="panel-collapse collapse  in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-data" style="max-width: 1331px;">

                                            <div class="panel-body">
                                                <div class="reporting-con">
                                                    <div class="col-md-4">
                                                        <ul>
                                                            <li><a href="javascript:;" id="btnFinacneVendorExpence">Vendor Expense Report</a></li>
                                                            <li>
                                                                <a href="javascript:;" id="ancVendor1099SummaryFilterOpen">Vendor 1099 Summary Report</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="reportRt">
                                                        <div class="col-md-4">
                                                            <ul>
                                                                <li><a href="javascript:;" id="btnVendorBill">Vendor Bills</a></li>
                                                                <li><a href="javascript:;" id="ancVendor1099DetailFilterOpen">Vendor 1099 Detail Report</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <ul>
                                                                <li>
                                                                    <a href="javascript:;" id="btnVendorWorkOrder">
                                                                        Vendor Work Order (open
                                                                        and closed)
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div></div></div></div>

    </section>
</div>
<!-- Wrapper Ends -->
<div class="container">
    <div class="modal fade" id="vendorSummaryReportModal" role="dialog">
        <div class="modal-dialog modal-md"style="width: 500px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Vendor 1099 Summary Filters
                    </h4>
                </div>
                <div class="modal-body">
                    <form id="vendorSummaryForm">
                        <div class="panel-body">
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-12">
                                            <label>Tax Year</label>
                                          <select class="form-control selectTaxhtml"></select>
                                    </div>
                                    <div class="col-sm-12">
                                        <label>Property</label>
                                        <select class="form-control selectPropertyhtml"multiple="multiple"></select>
                                    </div>
                                    <div class="btn-outer text-right">
                                        <button type="submit" class="blue-btn">Apply Filter</button>
                                        <button type="button"  class="grey-btn cancelvendorReport">Cancel</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/vendor-portal/reports.js"></script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_footer.php");
?>

<script>
    $("#work-reports").addClass("active");
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
    <!--- Accordians -->
</script>