<?php
if ((!isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) && (!isset($_SESSION['Admin_Access']['vendor_portal_id']))) {

    $url = SUBDOMAIN_URL;
    header('Location: ' . '/VendorPortal/login');
}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_header.php");
?>
    <style>
        .form-outer div[class^="col-"] {
            min-height: 75px;
        }
        label.error {
            color: red !important;
            font-size: 12px;
            font-weight: 500;
            width: 100% !important;
        }
        .multipleEmail input{margin:5px auto; }
        select[name=hobbies] + .btn-group button{
            margin-top: 0px;
        }
        .closeimagepopupicon{
            float: right;
            font-weight: bold;
            border: 2px solid #666;
            border-radius: 12px;
            padding: 0px 4px;
            cursor: pointer;
        }
        .fa-minus-circle {
            display: none;
        }

        .apx-inline-popup {
            position: relative;
        }

        .apx-inline-popup-box {
            position: absolute;
            top: 0;
            left: 0;
            background: #fff;
            z-index: 2;
            border: 1px solid #ddd;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
            padding: 15px;
            width: 100%;
        }

        .apx-inline-popup-box > h4 {
            font-size: 12px
        }

        .image-editor {
            text-align: center;
        }

        .cropit-preview {
            background-color: #f8f8f8;
            background-size: cover;
            border: 5px solid #ccc;
            border-radius: 3px;
            margin-top: 7px;
            width: 250px;
            height: 250px;
            display: inline-block;
        }

        .cropit-preview-image-container {
            cursor: move;
        }

        .cropit-preview-background {
            opacity: .2;
            cursor: auto;
        }

        .image-size-label {
            margin-top: 10px;
        }

        .export {
            /* Use relative position to prevent from being covered by image background */
            position: relative;
            z-index: 10;
            display: block;
        }

        .image-editor input[type="file"] {
            opacity: 0;
            position: absolute;
            top: 45px;
            left: 91px;
        }

        .upload-logo .img-outer img {
            max-width: 100%;
        }

        .choose-img {
            margin: 0 !important;
        }

        button {
            margin-top: 10px;
        }

        .vehicle_image1, .vehicle_image2, .vehicle_image3 {
            width: 100px;
            height: 100px;
        }

        .vehicle_image1 img, .vehicle_image2 img, .vehicle_image3 img {
            width: 100%;
        }
        .capital{text-transform: capitalize;}
    </style>
    <div class="popup-bg"></div>
    <!--<input type="hidden" name="vendor_id" class="vendor_id" value="--><?php //echo $_GET['id']; ?><!--">-->


    <div id="wrapper">
        <?php  include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_top_navigation.php"); ?>
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="content-section">
                            <div class="form-outer form-outer2">
                                <div class="form-hdr">
                                    <h3>Account Information <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Upload Picture</label>
                                            <div class="upload-logo">
                                                <div class="tenant_image img-outer" id="tenant_image_id"><img id="vendor_image_data" src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/dummy-img.jpg"></div>
                                                <a href="javascript:;">
                                                    <i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Image</a>
                                                <span>(Maximum File Size Limit: 1MB)</span>
                                            </div>
                                            <div class="image-editor">
                                                <input type="file" class="cropit-image-input" name="tenant_image">
                                                <div class='cropItData' style="display: none;">
                                                    <span class="closeimagepopupicon">X</span>
                                                    <div class="cropit-preview"></div>
                                                    <div class="image-size-label">Resize image</div>
                                                    <input type="range" class="cropit-image-zoom-input">
                                                    <button type="button" id="rotate-ccw" class="rotate-ccw">Rotate counterclockwise</button>
                                                    <button type="button" id="rotate-cw" class="rotate-cw">Rotate clockwise</button>
                                                    <input type="hidden" name="image-data" class="hidden-image-data"/>
                                                    <input type="button" class="export" value="Done"
                                                           data-val="tenant_image">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="grey-detail-box">
                                            <div class="col-xs-12 col-sm-3">
                                                <label class="blue-label">Vendor Name:</label>
                                                <span class="name_account"></span>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <label class="blue-label">Vendor ID:</label>
                                                <span class="vendor_random_id_account"></span>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <label class="blue-label">Total Payables :</label>
                                                <span class="total_payables_account">USh 0.00</span>
                                            </div>
                                        </div>
                                        <div class="grey-detail-box">
                                            <div class="col-xs-12 col-sm-3">
                                                <label class="blue-label">YTD Payment:</label>
                                                <span class="ytd_payments_account">USh 0.00</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <label class="blue-label">Open Work Orders:</label>
                                                <span class="open_work_orders_account">0</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <label class="blue-label">Phone:</label>
                                                <span class="phone_number_account" >N/A</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Tabs Start -->
                            <div class="main-tabs">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#tenant-acc-tab1" aria-controls="home" role="tab" data-toggle="tab">General</a></li>
                                    <li role="presentation" class="ledger_tab"><a href="#tenant-acc-tab2" aria-controls="profile" role="tab" data-toggle="tab">Ledger</a></li>
                                    <li role="presentation"><a href="#tenant-acc-tab3" aria-controls="profile" role="tab" data-toggle="tab">Payment Settings</a></li>
                                    <li role="presentation" id="notesAndHistory"><a href="#tenant-acc-tab4" aria-controls="profile" role="tab" data-toggle="tab">Notes & History</a></li>
                                    <li role="presentation"><a href="#tenant-acc-tab5" aria-controls="profile" role="tab" data-toggle="tab">Conversation</a></li>
                                    <li role="presentation"><a href="#tenant-acc-tab6" aria-controls="profile" role="tab" data-toggle="tab">Change Password</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="tenant-acc-tab1">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>General Information</h3>
                                            </div>
                                            <div class="form-data form-outer2">
                                                <div class="detail-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Salutation :</label>
                                                                <span class="salutation_account"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Middle Name :</label>
                                                                <span class="middle_name_account"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Maiden Name :</label>
                                                                <span class="maiden_name_account"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Gender :</label>
                                                                <span class="gender_account"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Entity/Company Name :</label>
                                                                <span class="company_name_account"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Vendor ID :</label>
                                                                <span class="vendor_random_id_account"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">First Name :</label>
                                                                <span class="first_name_account"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Last Name :</label>
                                                                <span class="last_name_account"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Nickname :</label>
                                                                <span class="nick_name_account"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Name on Check :</label>
                                                                <span class="name_on_check_account"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Vendor Type :</label>
                                                                <span class="vendor_type_id_account" id="vendor_type_id_acc"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Contact Information</h3>
                                            </div>
                                            <div class="form-data form-outer2">
                                                <div class="detail-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Zip / Postal Code :</label>
                                                                <span class="zipcode_account"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">State / Province :</label>
                                                                <span class="state_account"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Address :</label>
                                                                <span class="address_account"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Email :</label>
                                                                <span class="email_account"></span>
                                                            </div>


                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Country :</label>
                                                                <span class="country_account"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">City :</label>
                                                                <span class="city_account"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Website :</label>
                                                                <span class="website_account"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Phone :</label>
                                                                <span class="phone_number_account"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Emergency Contact Details</h3>
                                            </div>
                                            <div class="form-data form-outer2" id="emergency_view">
                                                <div class="detail-outer" >
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Emergency Contact Name :</label>
                                                                <span class="emergency_contact_name_account">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Country Code :</label>
                                                                <span class="emergency_country_code_account">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Emergency Contact Phone :</label>
                                                                <span class="emergency_phone_number_account">N/A</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Emergency Contact Email :</label>
                                                                <span class="emergency_email_account">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Emergency Contact Relationship :</label>
                                                                <span class="emergency_relation_account">N/A</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="edit-foot pb-15">
                                                    <a href="javascript:;" class="viewEditClass" id="editEmergencyData" data_href="#collapseTwo" redirection_data="info">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="form-data" id="emergency_edit_div"  style="display:none;">
                                                <div class="col-sm-12" id="emergency_edit">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Custom Fields</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="custom_field_html_view_mode">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- tab 2 ledger -->
                                    <div role="tabpanel" class="tab-pane" id="tenant-acc-tab2">
                                        <!-- Form Outer starts -->

                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Ledger</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <label>Start Date</label>
                                                        <span><input class="form-control calander st_date" type="text"/></span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>End Date</label>
                                                        <span><input class="form-control calander end_date" type="text"/></span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>&nbsp;</label>
                                                        <span><button class="blue-btn" id="search_ledger">Search</button></span>
                                                    </div>
                                                </div>
                                                <div class="grid-outer" style="padding-top: 20px">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-dark" id="ledger_table">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">Date</th>
                                                                <th scope="col">Transaction ID</th>
                                                                <th scope="col">Transaction Description</th>
                                                                <th scope="col">Reference No.</th>
                                                                <th scope="col">Original Amount </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="vendor_ledger">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="ledger-icon" style="padding-top: 30px">
                                                        <button id="exportpdf" class="blue-btn pull-right">Download</button>
                                                        <button id="printsample" class="blue-btn pull-right">Print</button>
                                                        <button id="exportexcel" style="margin-right: 1%;margin-top: 10px;" class="blue-btn pull-right" >Excel</button>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- tab 3 payment -->
                                    <div role="tabpanel" class="tab-pane" id="tenant-acc-tab3">
                                        <!-- Form Outer starts -->

                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Online Payment Details</h3>
                                            </div>

                                            <div class="form-outer">

                                                <div class="form-data">
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <label>Payment Method <em class="red-star">*</em></label>
                                                            <select class="form-control setType" id="payment_method">
                                                                <option value='1'>Credit Card/Debit Card</option>
                                                                <option value='2'>ACH</option>
                                                            </select>
                                                        </div>
<!--                                                        <div class="col-sm-3">-->
<!--                                                            <label>&nbsp;</label>-->
<!--                                                            <a class="verification-due" href="javascript:;">Verification Due</a>-->
<!--                                                        </div>-->
                                                    </div>

                                                    <div class="row cards">
                                                        <div class="cardDetails col-sm-12"></div>
                                                        <form id="addCards">
                                                            <div class="detail-outer ">
                                                                <div class="col-sm-5 clear">
                                                                    <label class="black-label">Card Type<em class="red-star">*</em></label>
                                                                    <img src="/company/images/card-payment.png" style="width: 190px;">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-5 clear">
                                                                    <label class="black-label">Card Number <em class="red-star">*</em></label>
                                                                    <span>
                                                                        <input type="hidden" id="company_user_id" value="<?= $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'];?>">
                                                                <input class="form-control hide_copy" name="card_number"  type="text" maxlength="16" placeholder="1234 1234 1234 1234"/>
                                                                <em class="nicknameErr error red-star"></em>
                                                            </span>
                                                                </div>


                                                                <div class="col-sm-5 clear">
                                                                    <label class="black-label">Expiration Date <em class="red-star">*</em></label>
                                                                    <span>
                                                            <input class="form-control exp_date" name="exp_date"  type="text" placeholder="MM / YY" readonly/>
                                                            <em class="nicknameErr error red-star"></em>
                                                        </span>
                                                                </div>

                                                                <div class="col-sm-5 clear">
                                                                    <label class="black-label">CVC <em class="red-star">*</em></label>
                                                                    <span>
                                                        <input class="form-control" name="cvc"  type="text" placeholder="CVC" maxlength="5"/>
                                                        <em class="nicknameErr error red-star"></em>
                                                        </span>
                                                                </div>

                                                                <div class="col-sm-5 clear">
                                                                    <label class="black-label">Cardholder Name <em class="red-star">*</em></label>
                                                                    <span>
                                                            <input class="form-control" name="holder_name" type="text" placeholder="Cardholder Name"/>
                                                            <em class="nicknameErr error red-star"></em>
                                                        </span>
                                                                </div>

                                                                <div class="col-sm-5 clear">
<!--                                                                    <label class="black-label">Autopay </label>-->
<!--                                                                    <span>-->
<!--                                                            <input class="auto_pay" name="auto_pay" type="checkbox" value="ON">-->
<!---->
<!--                                                        </span>-->
                                                                </div>



                                                            </div>
                                                            <div class="col-sm-12 text-right">
                                                                <input type="submit" class="blue-btn" value="Confirm">
                                                                <input type="button" class="blue-btn cancel" value="Cancel">
                                                            </div>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row accounts" style="display:none;">
                                                <div class="accountDetails"></div>


                                            </div>




                                        </div>
                                    </div>
                                    <!-- Notes and History-->
                                    <div role="tabpanel" class="tab-pane" id="tenant-acc-tab4">
                                        <div class="form-outer " id="OwnerNotes">
                                            <div class="form-hdr">
                                                <h3>History / Notes
                                                    <a href="javascript:;" class="blue-btn pull-right" id="add_notes"
                                                       style="display: none;">Add Notes</a>
                                                </h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="accordion-grid">
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div  class="vendor_note" style="display: none;">
                                                                        <div class="row">
                                                                            <div class="form-outer vendor-notes-clone-divclass" id="vendor-notes-clone-div">
                                                                                <form class="vendorNotes" id="vendorNotes" id="note">
                                                                                    <input type="hidden" value="add" class="note_type">
                                                                                    <input type="hidden" value="" class="tenantchargenotes" name="id">
                                                                                    <div class="col-sm-4">
                                                                                        <textarea class="notes capital note" name="note" ></textarea>
                                                                                        <div class="btn-outer text-right">
                                                                                            <input type="submit"  id='vendorAdd' class="blue-btn" value="Save" />
                                                                                            <button type="button"  class="clear-btn clearFormReset" id="clearVendorNotes">Clear</button>
                                                                                            <button type="button" style="display: none;"  class="clear-btn clearFormReset" id="resetVendorNotes">Reset</button>
                                                                                            <input type="button" class="grey-btn cancelhistorynotes" value='Cancel' />
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="grid-outer">
                                                                        <div class="apx-table apxtable-bdr-none">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-hover table-dark"
                                                                                       id="vendor_table">
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edit-foot">
                                                    <a href="javascript:;">
                                                        <span class="pull-right" id='vendor_edit_notes' style='cursor: pointer;'><i class="fa fa-pencil-square-o" id="vendor_edit_notes" aria-hidden="true"></i>Edit</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--Conversation tab -->
                                    <div role="tabpanel" class="tab-pane" id="tenant-acc-tab5">
                                        <!-- Form Outer starts -->
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Conversation</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="owner_conversation_div">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="conversation-outer">
                                                                <ul class="conversation_list">
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-sm-1 conversation-lt">
                                                                                <div class="img-outer">
                                                                                    <img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/profile-img.png">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-10 pad-none conversation-rt">
                                                                                <span class="dark_name">Kambo K(Admin) :</span>
                                                                                <span class="light_name">Sharma Aryan2 (Tenant)</span>
                                                                                <span class="dark_name">Personal Issues</span>
                                                                                <span class="black_text" >Fsdfsdsdsdsds</span>
                                                                                <div class="conversation-time clear">
                                                                                    <span class="black_text" >Sep 25, 2019 (Wed.)-</span>
                                                                                    <a class="light_name" >Comment</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-1 pull-right">
                                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-outer text-right">
                                                    <button class="blue-btn" id="add_owner_conversation_btn">New Conversation</button>
                                                    <hr/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="add_owner_conversation_div" style="display: none;">
                                            <div class="form-outer">
                                                <div class="form-hdr">
                                                    <h3> New Conversation</h3>
                                                </div>
                                                <div class="form-data">
                                                    <form id="add_owner_conversation_form_id">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Property Manager  <em class="red-star">*</em></label>
                                                                <select class="form-control select_mangers_option" name="selected_user_id"><option>Select</option></select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Problem Category <em class="red-star">*</em>
                                                                    <a class="pop-add-icon selectProblemCategory" href="javascript:;">
                                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                    </a>
                                                                </label>
                                                                <select class="form-control problem_category" name="problem_category"></select>
                                                                <div class="add-popup" id="selectProblemCategoryPopup">
                                                                    <h4>Add Problem Category</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>Add New Problem Category <em class="red-star">*</em></label>
                                                                                <input class="form-control problem_category_src customValidateGroup capital" maxlength="50" type="text" placeholder="Add New Problem Category" data_required="true" >
                                                                                <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                                            </div>
                                                                            <div class="btn-outer text-right">
                                                                                <button type="button" class="blue-btn add_single1"  data-table="conversation_problem_category" data-cell="problem_category_name" data-class="problem_category_src" data-name="problem_category">Save</button>
                                                                                <button type="button"  class="clear-btn clearFormReset" id="clearselectProblemCategoryPopup">Clear</button>
                                                                                <input type="button" class="grey-btn" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-6 clear">
                                                                <label>Description <em class="red-star">*</em></label>
                                                                <textarea rows="4" class="form-control capital" name="description"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="btn-outer top-marginCls text-right">
                                                            <button class="blue-btn add_form_submit_btn">Send</button>
                                                            <button type="button"  class="clear-btn clearFormReset" id="clearadd_owner_conversation_form_id">Clear</button>
                                                            <button id="cancel_add_owner_conversation"  type="button" class="grey-btn">Cancel</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Change password-->
                                    <div role="tabpanel" class="tab-pane" id="tenant-acc-tab6">
                                        <div class="accordion-form">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="title-color">Change Password</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body">
                                                                    <form id="change_pass_form" name="change_pass_form" method="post">
                                                                        <div class="row">
                                                                            <div class="form-outer">
                                                                                <div class="col-xs-12 col-sm-4 col-md-4">
                                                                                    <label>Current Password <em class="red-star">*</em></label>
                                                                                    <input type="password" maxlength="25" id="current_password" name="password" class="form-control" placeholder="Type in your current password">
                                                                                    <span class="password"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-4">
                                                                                    <label>New Password <em class="red-star">*</em></label>
                                                                                    <input type="password" maxlength="25" name="newpassword" class="form-control" placeholder="Type in your new password" id="newpassword2">
                                                                                    <span class="newpassword"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-4">
                                                                                    <label>Confirm New Password <em class="red-star">*</em></label>
                                                                                    <input type="password" maxlength="25" name="confirmpassword" class="form-control" placeholder="Please confirm your new password">
                                                                                    <span class="confirmpassword"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer text-right">
                                                                    <button class="blue-btn changePassBtn">Update</button>
                                                                    <button type="button"  class="clear-btn" id="clearchange_pass_form">Clear</button>
                                                                    <button class="grey-btn yes-cancel-time" id="cancelpassword">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Accordian Ends -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Tabs End -->
                </div>
            </div>
    </div>
    </section>

    <div id="imageModel" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-4" id="getImage1"></div>
                    <div class="col-sm-4" id="getImage2"></div>
                    <div class="col-sm-4" id="getImage3"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <script>var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";</script>
    <script>
        $(document).ready(function () {
            $('.calander').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: jsDateFomat
            });
            var date = $.datepicker.formatDate(jsDateFomat, new Date());
            $('.calander').val(date);
        });



        var default_Image = "<?php echo COMPANY_SUBDOMAIN_URL ?>/images/profile-img.png";
        var vendor_unique_id = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'] : $_SESSION['Admin_Access']['vendor_portal_id'] ?>';
        var pagination = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['pagination']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['pagination'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['pagination'] : $_SESSION['Admin_Access']['pagination'] ?>';
        var datepicker = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'] : $_SESSION['Admin_Access']['datepicker_format'] ?>';
        var jsDateFomat = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'] : $_SESSION['Admin_Access']['datepicker_format'] ?>';
        var staticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Vehicledummy.png">';
        var petStaticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg">';
        var vendor_id = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'] : $_SESSION['Admin_Access']['vendor_portal_id'] ?>';
        var upload_url = "<?php echo SITE_URL; ?>";
        var default_currency_symbol = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_currency_symbol']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_currency_symbol'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_currency_symbol'] : $_SESSION['Admin_Access']['default_currency_symbol'] ?>';
        var default_name = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_name']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_name'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_name'] : $_SESSION['Admin_Access']['default_name'] ?>';
        var vendorPortalNote = '';
    </script>
    <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>

    <script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/additionalMethods.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/vendor-portal/vendorDetail.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/vendor-portal/vendor_notes.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/vendor-portal/vendor_change_password.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/people/paymentValidations.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/vendor-portal/vendorPayment.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/payments/plaidIntialisation.js"></script>
    <script>

        if(localStorage.getItem('windowScroll') !== null){
            setTimeout(function(){
                $('html, body').animate({
                    'scrollTop':  localStorage.getItem('windowScroll')
                });
                $('#editEmergencyData').click();
                localStorage.removeItem('windowScroll');
            },300);
        }

        $(document).on('click', '#resetClearEmergencyInfo', function () {
            bootbox.confirm("Do you want to reset this form?", function (result) {
                if (result == true) {
                    localStorage.setItem('windowScroll', document.body.scrollHeight);
                    window.location.reload();
                }
            });
        });

        $(document).on('click','#clearVendorNotes',function(){
            resetFormClear('#vendorNotes',[],'form',false,vendorPortalNote);
        });

        $(document).on('click','#resetVendorNotes',function(){
            resetEditForm('#vendorNotes',[],true,vendorPortalNote,[]);
        });

        $(document).on('click','#clearselectProblemCategoryPopup',function(){
            resetFormClear('#selectProblemCategoryPopup',[],'div',false);
        });

        $(document).on('click','#clearadd_owner_conversation_form_id',function(){
            resetFormClear('#add_owner_conversation_form_id',[],'form',false);
        });

        $(document).on('click','#cancelpassword',function(){
            bootbox.confirm("Do you want to cancel this action?", function (result) {
                if (result == true) {
                    window.location.reload();
                }
            });
        });

        $(document).on('click','#clearchange_pass_form',function(){
            resetFormClear('#change_pass_form',[],'form',true);
        });

    </script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_footer.php");
?>