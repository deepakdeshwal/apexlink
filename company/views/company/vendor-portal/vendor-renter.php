<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Apexlink</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css" />

</head>

<body>

    <div id="wrapper">
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-2 visible-xs">
                    
                </div>
                <div class="col-sm-3 col-xs-8">
                    <div class="logo"><img src="images/logo.png"></div>
                </div>
                <div class="col-sm-9 col-xs-2">
                    <div class="hdr-rt">
                        <!-- TOP Navigation Starts -->

                        <nav class="navbar navbar-default">

                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                              </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                              <ul class="nav navbar-nav">
                                  <li class="hidden-xs">Welcome:  Sonny Kesseben (ACL Properties), June 01, 2018 <a herf="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i></a> <a href="javascript:;"><i class="fa fa-calculator" aria-hidden="true"></i></a></li>
                                <li><a href="#">Link</a></li>
                                <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i>Settings <span class="caret"></span></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                  </ul>
                                </li>
                                 <li class="nav-item">
                                    <a class="nav-link disabled" href="#"><i class="fa fa-question" aria-hidden="true"></i>Help</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link disabled" href="#"><i class="fa fa-wrench" aria-hidden="true"></i>Support</a>
                                  </li>

                                    <li class="nav-item">
                                    <a class="nav-link disabled" href="#"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                                  </li>
                              </ul>

                            </div><!-- /.navbar-collapse -->
                        </nav>
                        <!-- TOP Navigation Ends -->

                    </div>
</div>
            </div>
        </div>
        
    </header>
        
    <!-- MAIN Navigation Starts -->
        <section class="main-nav">
         <nav class="navbar navbar-default">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header visible-xs">
                  <a id="show" class="icon-bar" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                  <a class="nav-close visible-xs" id="close" href="#"><i class="fa fa-times" aria-hidden="true"></i> </a>
                  <li><a href="tenant-myaccount.html">My Account</a></li>
                  <li><a href="tenant-transaction.html">Transactions</a></li>
                  <li><a href="tenant-maintenance.html">Maintenance</a></li>
                  <li><a href="tenant-file.html">File Library</a></li>
                  <li class="active"><a href="tenant-renter.html">Renter's Insurance</a></li>
                  <li><a href="tenant-communication.html">Communication</a></li>
                </ul>

              </div><!-- /.navbar-collapse -->
          </nav>
        </section>
    <!-- MAIN Navigation Ends -->
        
        
        <section class="main-content">
          <div class="container-fluid">
            <div class="row">                   
              <div class="col-sm-12">
                <div class="content-section">
                  <div class="property-status">
                          <div class="btn-outer">
                              <a class="blue-btn pull-right">New Insurance</a>
                          </div>
                      </div>
                  <div class="form-outer form-outer2">
                    <div class="form-hdr">
                      <h3>Renter's Insurance</h3>
                    </div>
                    <div class="form-data">
                      <div class="grid-outer">
                        <div class="table-responsive">
                          <table class="table table-hover table-dark">
                            <thead>
                              <tr>
                                <th scope="col">Policy Holder</th>
                                <th scope="col">Policy No.</th>
                                <th scope="col">Provider</th>
                                <th scope="col">Status</th>
                                <th scope="col">Expire Date</th>
                                <th scope="col">Effective Date</th>
                                <th scope="col">Next Renewal</th>
                                <th scope="col">Last Update</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Baker Francis</td>
                                <td>PI45632362</td>
                                <td>Nation Wide Insurance</td>
                                <td>Active</td>
                                <td>11/30/2021 7:46:49 PM</td>
                                <td>12/01/2018 7:47:37 PM</td>
                                <td>11/30/2021 7:46:49 PM</td>
                                <td>12/01/2018 7:47:37 PM</td>
                                <td></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </div>
        <!-- Wrapper Ends -->
    
    <footer>
      <div class="container-fluid">
          © apexlink 2018. All rights reserved
        </div>
    </footer>
    <!-- Footer Ends -->


    <!-- Jquery Starts -->

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.responsivetabs.js"></script>
     
    <script>
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });
 
        <!--- Main Nav Responsive --> 
        $("#show").click(function(){
    $("#bs-example-navbar-collapse-2").show();
});
         $("#close").click(function(){
    $("#bs-example-navbar-collapse-2").hide();
});
         <!--- Main Nav Responsive -->
        
        
        $(document).ready(function(){
          $(".slide-toggle").click(function(){
            $(".box").animate({
              width: "toggle"
            });
          });
        });
        
        $(document).ready(function(){
          $(".slide-toggle2").click(function(){
            $(".box2").animate({
              width: "toggle"
            });
          });
        });
        
         <!--- Accordians -->
       $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
        	$(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });
        
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
         <!--- Accordians --> 
    </script>

    
    


    <!-- Jquery Starts -->

</body>

</html>