
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

  <div id="wrapper">
    <link rel="stylesheet" type="text/css" href="<?php echo COMPANY_URL; ?>/company/css/jquery.datetimepicker.css"/>
      <style>
          .getPropertyData {
              max-height: 150px;
              overflow: auto;
          }
          .getPropertyData p{ margin: 0; padding-left: 5px; }
          .getPropertyData p a{ color: #000; }
      </style>


      <style>
          td, th {
              border: 1px solid #dddddd;
              text-align: left;
              padding: 8px;
          }
          .deleteNote {
              padding: 6px 12px !important;
          }


          .bootstrap-tagsinput { width: 100%; }
          #ccrecepents .modal-content, #ccrecepents .modal-body  { width: 100%; }
          #sendMailModal .modal-body {
              max-height: 400px;
              overflow: auto;
          }
          #sendMailModal .modal-body .note-editor { width: 100% !important; }

      </style>
          <form id="sendEmail">
    <div class="modal fade" id="sendMailModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Manager</h4>
                </div>
                <div class="modal-body">
                    <!-- <div class="form-outer" style="float: none;">
                        <label>To <em class="red-star">*</em></label>
                        <input class="form-control to" name="to" type="text" data-role="tagsinput" /><span class=""><a href="#" class="addToRecepent">Add Recepent</a></span>

                           <label>CC <em class="red-star">*</em></label>
                        <input class="form-control cc" name="cc" type="text" />

                           <label>BCC </label>
                        <input class="form-control bcc" name="bcc" type="text" />

                           <label>Subject <em class="red-star">*</em></label>
                        <input class="form-control subject" name="subject" type="text" />

                         <label>Body <em class="red-star">*</em></label>
                        <textarea class="form-control body" name="body"></textarea>

                        <div class="attachmentFile"></div>
                      
                     
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                          <button class="blue-btn" value="Send">Send</button>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div> -->

                       <div class="row">
                           <div class="col-sm-1">
                              <button class="blue-btn compose-email-btn">Send</button>
                           </div>
                           <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>To <em class="red-star">*</em></label></div>
                                <div class="col-sm-10"><span><input class="form-control to" name="to" type="text"/></span></div>
                            </div>
                           </div>
                           <div class="col-sm-3">
                                <a class="add-recipient-link addToRecepent" href="#"> Add Recipients</a>
                            </div>
                       </div>

                       <div class="row">
                           <div class="col-sm-1">
                           </div>
                           <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Cc</div>
                                <div class="col-sm-10"><span><input class="form-control cc" name="cc" type="text"/></span></div>
                            </div>
                           </div>
                           <div class="col-sm-3">
                                <a class="add-recipient-link addCcRecepent" href="#"> Add Recipients</a>
                            </div>
                       </div>

                       <div class="row">
                           <div class="col-sm-1">
                           </div>
                           <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Bcc </label></div>
                                <div class="col-sm-10"><span><input class="form-control bcc" name="bcc" type="text"/></span></div>
                            </div>
                           </div>
                           <div class="col-sm-3">
                                <a class="add-recipient-link addBccRecepent" href="#"> Add Recipients</a>
                            </div>
                       </div>

                       <div class="row">
                           <div class="col-sm-1">
                           </div>
                           <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Subject <em class="red-star">*</em></label></div>
                                <div class="col-sm-10"><span><input class="form-control subject" name="subject" type="text"/></span></div>
                            </div>
                           </div>
                           <div class="col-sm-3">
                            </div>
                       </div>

                       <div class="row">
                           <div class="col-sm-1">
                           </div>
                           <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Body <em class="red-star">*</em></label></div>
                                <div class="col-sm-10">
                                    <span><textarea class="form-control summernote" name="body"></textarea></span>
                                    <div class="btn-outer">
                                        <button class="blue-btn">Send</button>
                                
                                    </div>
                                </div>
                            </div>
                           </div>
                           <div class="col-sm-3">
                            </div>
                       </div> 


                       <div class="attachmentFile"></div>


                </div>
            </div>
        </div>

    </div>
  </form>
        
 <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- MAIN Navigation Ends -->
    
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                      <div class="row">
                        <div class="col-sm-8">
                          <!-- <div class="breadcrumb-outer">
                            People Module >> <span>New Employee</span>
                          </div> -->
                        </div>
                        <div class="col-sm-4">
                          <div class="easy-search">
                              <input placeholder="Easy Search" type="text">
                          </div>
                        </div>
                      </div>
                    </div>
                   <form id="editLostFoundData"> 
                    <input type="hidden" name="item_id" class="item_id" value="<?php echo $_GET['id'];  ?>">
                    <input type="hidden" name="user_id" class="user_id">
                    <input type="hidden" name="type" value="L" class="type">
                    <input type="hidden" name="property_id" class="property_id">
                   
                    <div class="col-sm-12">
                      <div class="content-section">
                         <!--Tabs Starts -->
                        <div class="main-tabs">                        
                          <!-- Nav tabs -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="guest-cards">
                              <div class="form-outer">
                                <div class="form-hdr">
                                  <h3>New <span class="found_label">Lost</span> Item Details <a class="back" href="/LostAndFound/LostAndFound" ><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>  </h3>
                                </div>
                                <div class="form-data">
                                  <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label><span class="found_label">Lost</span> Item Name <em class="red-star">*</em></label>
                                      <input class="form-control item_name readOnlyProperty" type="text" name="lost_item_name"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Item Number</label>
                                      <input class="form-control item_number itemNumberProperty " type="text" name="item_number" readonly  />
                                    </div>


                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                    <label>Category <em class="red-star">*</em> <a class="pop-add-icon" data-popup = "NewCategoryPopup" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                 <select class="form-control category category_id " name="category" id="category">
                                      <option value="">Select</option>                               
                                   </select>
                                    <div class="add-popup categoryPopup" id='NewCategoryPopup' style="width: 97%;">
                                                    <h4>Add Category</h4>
                                                    <div class="add-popup-body">

                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>Category<em class="red-star">*</em></label>
                                                                <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20"  id='data-category'/>
                                                                <span class="customErrorCategory required"></span>
                                                            </div>
                                                      
                                                            <div class="btn-outer text-right">
                                                                <input type="button" id='addNewCategory' class="blue-btn addSingleData" value="Save" data-table='maintenance_category' data-cell="category_name" data-value="data-category" data-error="customErrorCategory" data-select = "category" data-hide="categoryPopup"/>
                                                                <input type="button" class="clear-btn ClearCategory" value='Clear' />
                                                                <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                    </div>


                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Brief Decription <em class="red-star">*</em></label>
                                      <input class="form-control description readOnlyProperty capital" type="text" name="brand_description"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Brand Name</label>
                                      <input class="form-control brand_name readOnlyProperty capital" type="text" name="brand_name"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 lost-item">
                                      <div class="check-outer">
                                        <input name="userInfo" class="userInfo" checked="" type="radio" value="2"/><label>Tenant</label>
                                      </div>
                                      <div class="check-outer">
                                        <input name="userInfo" class="userInfo" type="radio" value="4"/><label>Owner</label>
                                      </div>
                                      <div class="check-outer">
                                        <input name="userInfo" class="userInfo" type="radio" value="3"/ ><label>Vendor</label>
                                      </div>
                                      <div class="check-outer">
                                        <input name="userInfo" class="userInfo" type="radio" value="1"/><label>Staff</label>
                                      </div>
                                     <input type="text" class="form-control userData name readOnlyProperty">
                                     <div class="getUserData getuser-lostdata"></div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Property Name <em class="red-star">*</em></label>
                                        <select name="user_property" class="form-control user_property property_name readOnlyProperty"></select>
                                       <div class="getPropertyData"></div>
                                      <a class="add-icon" href="javascript:;"></a>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Color <a class="pop-add-icon" href="javascript:;" data-popup="NewColorPopup"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                    <select class="form-control color color_id " name="color" id="color">
                                                       
                                    </select>
                                             <div class="add-popup colorPopup" id='NewColorPopup' style="width: 97%;">
                                                    <h4>Add Color</h4>
                                                    <div class="add-popup-body">

                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>Color<em class="red-star">*</em></label>
                                                                <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20" id='data-color'/>
                                                                <span class="customError required customErrorColor"></span>
                                                            </div>
                                                      
                                                            <div class="btn-outer text-right">
                                                                <input type="button" id='addColor' class="blue-btn addSingleData" value="Save" data-table='maintenance_color' data-cell="color_name" data-value="data-color" data-error="customErrorColor" data-select = "color" data-hide="colorPopup" />
                                                                <input type="button" class="clear-btn ClearColor" value='Clear' />
                                                                <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 clear">
                                      <label>Age of Item <a class="pop-add-icon" href="javascript:;" data-popup="NewItemPopup"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                     <select class="form-control data-item item age_item_id " name="item" id="item">
                                         <option value="">Select</option>                            
                                   </select>
                                           <div class="add-popup itemPopup" id='NewItemPopup' style="width: 97%;">
                                                    <h4>Add Item</h4>
                                                    <div class="add-popup-body">

                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>Add Item<em class="red-star">*</em></label>
                                                                <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20" name='data-item' id='data-item'/>
                                                                <span class="customError required customErrorItem"></span>
                                                            </div>
                                                      
                                                            <div class="btn-outer text-right">
                                                                <input type="button" id='NewportfolioPopupSave' class="blue-btn addSingleData" value="Save" data-table='maintenance_item' data-cell="item_name" data-value="data-item" data-error="customErrorItem" data-select = "item" data-hide="itemPopup" />
                                                                <input type="button" class="clear-btn ClearItem" value='Clear' />
                                                                <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Serial / ID Number</label>
                                      <input class="form-control serial_number readOnlyProperty" type="text" name="serial_number"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Location <span class="found_label">Lost</span> <em class="red-star">*</em></label>
                                      <input class="form-control lost_location readOnlyProperty capital" type="text" name="loation_lost"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Lost Date / Time <em class="red-star">*</em></label>
                                      <input class="form-control setinput_readonly lost_date" type="text"  readonly name="lost_date"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Other Details</label>
                                      <textarea class="form-control other_details capital readOnlyProperty" name="other_details" ></textarea>
                                    </div>

                                  </div>
                                </div>
                              </div>
                               <!-- Form Outer Ends -->


                                  <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeventeen">
                                                     File Library</a>
                                            </h4>
                                        </div>
                                        <div id="collapseSeventeen">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-4">
                                                            <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                            <input id="file_library" type="file" name="file_library[]" accept="image/x-png,image/gif,image/jpeg" multiple style="display: none;">
                                                           
                                                            <input type="button" class="orange-btn" id="remove_library_file" value="Remove All Files...">
                                                          
                                                        </div>
                                                        <div class="row" id="file_library_uploads">
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                            <div class="grid-outer">
                                                <div class="apx-table">
                                                    <div class="table-responsive">
                                                        <table id="itemFiles-table" class="table table-bordered">
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                      <div class="grid-outer">
                                         <div class="apx-table">
                                        <div class="table-responsive">
                                            <table id="itemFiles-table" class="table table-bordered">
                                            </table>
                                        </div>
                                      </div>
                                      </div>
                                    </div>



                                
                              <!-- Form Outer Ends -->
                              <div class="form-outer">
                                <div class="form-hdr">
                                  <h3>Owner Contact Details</h3>
                                </div>
                                <div class="form-data">
                                  <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Full Name</label>
                                      <input class="form-control full_name setinput_readonly" type="text" readonly/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Address</label>
                                      <input class="form-control user_address setinput_readonly" type="text" readonly/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Email</label>
                                      <input class="form-control user_email setinput_readonly" type="text" name="user_email" readonly/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Phone Number</label>
                                      <input class="form-control user_phone setinput_readonly" type="text" readonly/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                      <label>Unit Number</label>
                                      <input class="form-control add-input user_unit setinput_readonly" type="text" readonly>
                                    </div>
                                  </div>
                                </div>
                              </div>

                                    <div class="form-outer">
                                <div class="form-hdr">
                                  <h3>Notes</h3>
                                </div>
                                <div class="form-data">
                                  <div class="getNotesData"></div>
                                    <div class="btn-outer"><input type='button' class='blue-btn addNoteForm pull-right' value='Add Notes'></div>
                                      <div class="noteDataForm row" style="display: none;"> 
                                        <div class="col-sm-10">
                                            <div class="notes_date_right_div ">
                                                <textarea class='itemNotes form-control notes_date_right'></textarea> </div>
                            </div>
                            <div class="col-sm-2">
                              <input type='button' class='blue-btn addNoteButton' value='Add'>
                            </div>

                            </div>
                                   </div>
                                </div>
                             

                        

                               <div class="form-outer form-outer2 action-edititem">
                                   <div class="form-hdr" style="display:block"><h3 style="display:inline-block">Actions</h3>
                                 <input type="checkbox"  name="editItem" class="editItem" style="margin-left: 30px;"> <h3 style="display:inline-block">Edit Item  </h3>
                                   </div>
                                <div class="form-data ">
                                  <div class="row">
                                    <div class="col-sm-12">
                                      <div class="check-outer subhdr">
                                        <input type="checkbox"  name="markedAsMatched" class="markedAsMatched"> <label>Marked as Match</label>
                                      </div>
                                    </div>
                                    <div class="form-outer">
                                    <div class="col-sm-3">
                                      <label>Match Date</label>
                                      <span><input class="form-control matchedCalander" name="matched_date" type="text" disabled readonly></span>
                                    </div>

                                    <div class="col-sm-3">
                                      <label>Matched With Item Code F</label>
                                      <span><input class="form-control matchWithF" type="text" name="matchWithF" disabled></span>
                                    </div>
                                  </div>
                                  </div>
                                <div class="row">
                                 <div class="col-sm-12">
                                      <div class="check-outer subhdr">
                                        <input type="checkbox"  name="matchedAsReturn" class="matchedAsReturn" disabled > <label>Matched as Return</label>
                                      </div>
                                    </div>
                                  <div class="form-outer">
                                    <div class="col-sm-3">
                                      <label>Returned Date</label>
                                      <span><input class="form-control returnDate" type="text" name="returnDate" disabled readonly></span>
                                    </div>
                                  </div>
                                </div>
                                   <div class="row">


                                 <div class="col-sm-12">
                                      <div class="check-outer subhdr">
                                        <input type="checkbox" class="markedReleased" name="markedAs"> <label>Mark as Released</label>
                                      </div>
                                    </div>


                                   <div class="col-sm-3">
                                      <label></label>
                                      <span><select class="form-control releaseMethod release_method" disabled name="releaseMethod" id="releaseMethod">
                              <option value="">Select</option> 
                               <option value="Disposed">Disposed</option>                                
                                <option value="Donated">Donated</option>  
                                 <option value="Recycled">Recycled</option>  
                              </select></span>
                                    </div>   
                                    </div> 

                              
                                </div>

                              </div>
                              <!-- Form Outer Ends -->

                           


                              <!-- Form Outer Ends -->
                            </div>
                            <div class="btn-outer text-right">
                                <button class="blue-btn">Save & Close</button>
                                <input type="button" value="Reset" class="clear-btn resetBtnUpdate">
                            </div>

                        </div>
                        <!--tab Ends -->
                        
                    </div>

                    </div>
                </div>
              </form>
            </div>
        </section>


  <div class="container">
    <div class="modal fade" id="torecepents" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body">
                   <div class="form-outer" style="float: none;">
                     <label>Select <em class="red-star">*</em></label>
                       <select class="form-control selectUsers">
                        <option value=""></option>
                        <option value="tenant">Tenant</option>
                        <option value="owner">Owner</option>
                        <option value="vendor">Vendor</option>
                        <option value="other_contacts">Other Contacts</option>
                        <option value="guestCard">Guest Card</option>
                        <option value="rental_application">Rental Application</option>
                        <option value="employee">Employee</option>   
                       </select>

                        <div class="userDetails"></div>
                      
                     
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                          <button class="blue-btn" value="Send">Send</button>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>



<div class="container">
    <div class="modal fade" id="ccrecepents" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body pull-left">
                   <div class="form-outer" style="float: none;">
                    <div class="row">
                    <div class="col-sm-6">
                     <label>Select <em class="red-star">*</em></label>
                       <select class="form-control selectCcUsers">
                        <option value=""></option>
                        <option value="tenant">Tenant</option>
                        <option value="owner">Owner</option>
                        <option value="vendor">Vendor</option>
                        <option value="other_contacts">Other Contacts</option>
                        <option value="guestCard">Guest Card</option>
                        <option value="rental_application">Rental Application</option>
                        <option value="employee">Employee</option>   
                       </select>
                     </div>
                   </div>

                        <div class="userCcDetails grid-outer"></div>
                      
                     <div class="row">
                        <div class="col-sm-12">
                          <button class="blue-btn" value="Send">Send</button>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>


  <div class="container">
    <div class="modal fade" id="bccrecepents" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body">
                   <div class="form-outer" style="float: none;">
                     <label>Select <em class="red-star">*</em></label>
                       <select class="form-control selectBccUsers">
                        <option value=""></option>
                        <option value="tenant">Tenant</option>
                        <option value="owner">Owner</option>
                        <option value="vendor">Vendor</option>
                        <option value="other_contacts">Other Contacts</option>
                        <option value="guestCard">Guest Card</option>
                        <option value="rental_application">Rental Application</option>
                        <option value="employee">Employee</option>   
                       </select>

                        <div class="userBccDetails"></div>
                      
                     
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                          <button class="blue-btn" value="Send">Send</button>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>


    </div>


</div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js" type="text/javascript"></script>
     <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.datetimepicker.full.min.js"></script>
   
 <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
 <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/maintenance/lostandfound/lostandfound.js"></script>
 <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
  <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/bootstrap-tagsinput.css" />
  <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
 <script>var upload_url = "<?php echo SITE_URL; ?>";</script>
 <script>getitemDetails();</script>
 <script>

  $(".to").tagsinput('items');
   $(".cc").tagsinput('items')
    $(".bcc").tagsinput('items')


 </script>

     <script>
        $('.lost_date').datetimepicker({
            calendarWeeks: true,
            locale: moment.locale('se'),
        });
    </script>

 
<!-- Wrapper Ends -->



<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
        $(document).on('click','.resetBtnUpdate',function () {
            bootbox.confirm("Do you want to reset this action now?", function (result) {
                if (result == true) {
                    window.location.reload();
                }
            });
        });

        $(document).on('click','.ClearCategory',function () {
             $('#data-category').val('');
        });

        $(document).on('click','.ClearColor',function () {
            $('#data-color').val('');
        });

        $(document).on('click','.ClearItem',function () {
            $('#data-item').val('');
        });

    });



</script>


<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>