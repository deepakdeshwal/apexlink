<div class="modal fade" id="myModalCalc" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body" style="padding:0px;">
                <main class="containerCalc">
                    <div id="calculator">
                        <header class="top-part">
                            <div class="top">
                                <h5 class="branding">Calculator</h5>
                                <div class="window">
                                    <div class="close button CalcClose" data-dismiss="modal" data-target="#myModalCalc" role="button">
                                        <img class="icon icons8-Multiply" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAABBUlEQVRYR+2W6w3CMAyErxuwCYwAG7AJjAATMQKMUDZhBHQSQaFqYp8DqpDSn2nj+3J+NAMWfoaF9dEBugN/4cAewA3AQ+yYFYADgHNtn+UAxS8ARgA7AYLiVwCbF8CpBGEBMBBPvxYgcvE7gG0N3AIguAIhiTO4B8ALIYsrABZESFwFKEFwPRWcmfNpMXpTkO+b1gTfsdpl8YgDCSSH4FpIvBUg2c446px4OxpNQZ5zBlPmxEcZqABz1c6A6rAKOVBrNWVYhRzw9HkIwpMCj/hcd7gK0wJQxEMQFkD6Hat9nqeD94Hw75inarmQHGviLYNIvByVP7dS8DWh6I2oA3QHfu7AEzfHWCF+OOmJAAAAAElFTkSuQmCC">
                                    </div>
                                </div>
                            </div>
                            <div class="type">
                                <div class="column menu">
                                    <div class="calculator-menu button-top" role="button">
                                        <img class="icon icons8-Menu" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAATElEQVRYR+3TsQ0AMAgDQdh/aFIj0ZKn+AyQOCc7Az4Jvx8GUOCkQC0vo316EsADLAP06092QAEF8BniAeyAAgrgM8QD2AEFFPgq8ACF3gYho2IK1QAAAABJRU5ErkJggg==">
                                    </div>
                                    <label class="calculator-chosen">STANDARD</label>
                                </div>
                                <div class="column history">
                                    <div class="button-top" role="button">
                                        <img class="icon icons8-Past" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAB4klEQVRYR+2W4U3DQAyFXyeAEWACygZsAEwAbFA2gAmACYAJgA1ggsIEMAJMAPqQXSXpxXfXRO2fWqoa5S7287PvnSfasE02HF9bALUM7Eo6k3RipTuy/3dJX5JeJb3Yc1F1SwEQ+EbSeZFX6UHSdQmQFIBfC+JrZHsvCRAYGRLg2zLm3dR+MAJDGOsXkp4j0DkAZExw7M0YgOrI9iTdSjq2TYAAcNIiAKeSnuwr6LxKeOiy1dwys7LxDl9JJiIAUAjtfcFxHAFg3UHga9/K0sojAuC0e6enKMwB4BsypxyPqSbOAegG7e4vAUBPfJojWGj10DoANFm4tAZdJFaqA31NXMIA3/pp4gi7iP37XBcAdGIuCcU8bGYzFoAUQ0X9khOijOYsjuGoAKDpwKjieQzzEnyYZIdN6Oc2lNBKVFVN6OqVFI7KwL7dkyo6hqFwrACgWoiIwe3FtQpyLpIhVi3FBAM1DbgjaYm2CjRezh/zyaXUskgHUCy/jnF0VxGYrStfx804zYEEKmGjZCBhfHPJrR5IuoniiJ6gHN4fDJ+cadcJzjnawdXtcyO081w9kqWYpieYiHzey1WDI0wJlmpe0wN9QGCETAFF1hhs+FhOxrkyhUqYy27U9aG34WAwWwB/XytsITVxIuMAAAAASUVORK5CYII=">
                                    </div>
                                </div>
                            </div>
                            <div class="screen">
                                <div class="history-screen">
                                    <input id="history" name="history" type="text" value="" disabled>
                                </div>
                                <div class="main-screen">
                                    <input id="main" name="main" value="" type="text" disabled>
                                </div>
                            </div>
                            <div class="bottom">
                                <div class="memory">
                                    <div role="button">MC</div>
                                    <div role="button">MR</div>
                                    <div role="button">M+</div>
                                    <div role="button">M-</div>
                                    <div role="button">MS</div>
                                    <div role="button">M&darr;</div>
                                </div>
                            </div>
                        </header>
                        <section class="bottom-part">
                            <div class="features">
                                <div class="sub-menu">
                                    <div class="rowCalc">
                                        <div id="percentage" class="button image sign" data-value="%" role="button">
                                            <img class="icon icons8-Percentage" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAABK0lEQVRYR+2WYQ3CMBCFvznAAgrAAeAAHOAACYADHCABJIACcAA4wAHkkpYM6K7XZs3+rAnZYNf169u7Nyo6HlXH69MD9ArEFJgDK2AKnICtO2re3bk5D2AMPLViDUAWPwQmzxQIWfDi5iyAY6zLNIArMHK73gDyWQNnp0jo3qLSJFLzNU8DeLnKek3oN3/DJbB3X4bAPbZ7ud4WwAC4AXIUn4haptEWQJLx6mRtACQbzwrgTShyellDJkw2nhXA0oZZxrMCSJ0EkCjgW0vOZccyso2XApCTeEnpGYviJoAm41kemzmItN03GS85PXMU0IyXmp7J/4hixisOEEu8ogCWxCsKYEk8a3p+DG41oTXxirRhzHi/7aql519rWxUwvdtzinqAXoHOFXgD+J9gIZOwvCMAAAAASUVORK5CYII=">
                                        </div>
                                        <div id="sqrt" class="button image sign" data-value="sqrt" role="button">
                                            <img class="icon icons8-Square-Root" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAABP0lEQVRYR+3V4S0FQRQF4O9VgApQATrQASpABXSAClABOqCCRwWoABWgAnKTnWSzeV5mdkf2z55fm83sPWfPuXfuzMiYjcxvEjA5UNuBd6z/0dgLuWoK2MV8yVT9u4BbHOIC57njXcuBVXw2pJuIKLJQS8ARbvCA/Szm5lAtAS/YwjEiimzUELCNZ3wjoihCDQFXOME1TovYqbILvrCCHUQURRjqQGq+V0QUxVgm4KdVLcYqxquLe+z1ab5UKFdAnO92+AbemkJriCiKkRNBsrnrQjTcJe4QZ3ohR0AUTkum7UJ6d4CIohdyBXRdSIvnAxFFb+QKaLsQfxzXbfHiWaSyREBy4bEZubj1ihbPUAFBGLnHpRN4QkQxCCUOBFHs+bOGsXjxDHUgvk8upOdBfx8flzowmLBbYBIwOTC6A7+1/zEhMa617wAAAABJRU5ErkJggg==">
                                        </div>
                                        <div id="sqr" class="button sign" data-value="sqr" role="button">
                                          <span class="x-sign">
                                            <sub>x</sub>
                                          </span>
                                            <span>
                                            <sup>2</sup>
                                          </span>
                                        </div>
                                        <div id="fraction" class="button sign" data-value="1/x" role="button">
                                          <span>
                                            <sup>1</sup>
                                          </span>
                                            <span>&frasl;</span>
                                            <span class="x-sign">
                                            <sub><em>x</em></sub>
                                          </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="main-menu">
                                    <div class="rowCalc">
                                        <div id="clear-entry" class="button sign" data-value="CE" role="button">CE</div>
                                        <div id="clear" class="button sign ClearCalc" data-value="C" role="button">C</div>
                                        <div id="clear-element" class="button image sign" data-value="CS" role="button">
                                            <img class="icon icons8-Clear-Symbol" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAABDklEQVRYR+2W4Q3CIBSErxu4iY6gm+hmbqIj6CZuUHNJMeQFeAelaUzoT8LjPo570Ak7f9PO+hgAw4G/cuAA4AHg1KFzfhtXHegpTv4qgFj8DeAM4NPowrzUyQA9xaldBaCIc451IzUWDJMBFPE7gCOASwQR6l4AboljkgAUcc55LgAUIwS/0CW5rLgAinjYmIXgOFu0FNQiQI14CoJjXpe4ANZWr93s/RCOI1cnHYEKYR2jAwxlCcIF4CKpgKVazQaOtR68BKBCsA0ZuvhmDPB04drahrmUx/0ez9nkIqqBqHkW5COIF1UyoUI0AdhMqGKlefJrmHNiLUQTwFrRZL36R7SJOBcdAMOB3R34AqM1YCEu+sgCAAAAAElFTkSuQmCC">
                                        </div>
                                        <div id="divide" class="button image sign" data-value="/" role="button">
                                            <img class="icon icons8-Divide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAo0lEQVRYR+2VSxGDMBRFTxTUAkioklrAQSVAHSChjmqhdYCDMm8mCz7DIp/yurisQ+6Zk5u8gPMXnPMRgAyUGBiBWyzxE3jkFDoXwMLvm0ADGFIhcgEm4AJcY+ALeAOtJ8AHaM4CMNW95xFYtkF0ixImn7/9m9uBVNOH6wVwZOBbzfF6o13e3wL8SMB+W5WwxICmoaahpmGVx6rkGgpABqoYmAH2Lh4hfK3KfwAAAABJRU5ErkJggg==">
                                        </div>
                                    </div>
                                    <div class="rowCalc">
                                        <div class="button number" role="button" data-value="7">7</div>
                                        <div class="button number" role="button" data-value="8">8</div>
                                        <div class="button number" role="button" data-value="9">9</div>
                                        <div id="multiply" class="button image sign" data-value="*" role="button">
                                            <img class="icon icons8-Multiply" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAABBUlEQVRYR+2W6w3CMAyErxuwCYwAG7AJjAATMQKMUDZhBHQSQaFqYp8DqpDSn2nj+3J+NAMWfoaF9dEBugN/4cAewA3AQ+yYFYADgHNtn+UAxS8ARgA7AYLiVwCbF8CpBGEBMBBPvxYgcvE7gG0N3AIguAIhiTO4B8ALIYsrABZESFwFKEFwPRWcmfNpMXpTkO+b1gTfsdpl8YgDCSSH4FpIvBUg2c446px4OxpNQZ5zBlPmxEcZqABz1c6A6rAKOVBrNWVYhRzw9HkIwpMCj/hcd7gK0wJQxEMQFkD6Hat9nqeD94Hw75inarmQHGviLYNIvByVP7dS8DWh6I2oA3QHfu7AEzfHWCF+OOmJAAAAAElFTkSuQmCC">
                                        </div>
                                    </div>
                                    <div class="rowCalc">
                                        <div class="button number" role="button" data-value="4">4</div>
                                        <div class="button number" role="button" data-value="5">5</div>
                                        <div class="button number" role="button" data-value="6">6</div>
                                        <div id="minus" class="button image sign" data-value="-" role="button">
                                            <img class="icon icons8-Minus-Math-" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAARElEQVRYR+3V0Q0AMAQFQPYfuh1A+ql8nAF4LhIZw5XD80MAAgQIECCwVuA0fcmy8EtgPEATQG279gYIECBAgACBbwIXi38CITaYSB4AAAAASUVORK5CYII=">
                                        </div>
                                    </div>
                                    <div class="rowCalc">
                                        <div class="button number" role="button" data-value="1">1</div>
                                        <div class="button number" role="button" data-value="2">2</div>
                                        <div class="button number" role="button" data-value="3">3</div>
                                        <div id="plus" class="button image sign" data-value="+" role="button">
                                            <img class="icon icons8-Plus-Math" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAWUlEQVRYR+2WMQoAIAgA9f+PrsWmEMQSEa7Z8rzEUmle2pxfABhvYFkPpQtJb7TEAGAAAxgoM3AO/v1YXoPPm4TtANHKy64AAAxgAANjDERB3bjXXzEA8w1s3k4WIU0YaEoAAAAASUVORK5CYII=">
                                        </div>
                                    </div>
                                    <div class="rowCalc">
                                        <div id="plus-minus" class="button image sign" data-value="+-" role="button">
                                            <img class="icon icons8-Plus-Minus" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAYUlEQVRYR+2VMQ4AIAgD4f+P1gUdCakx1VB3pL0UcCM/J/c3CfiewIgMwUbgwmgsASLQg8ByiW7tdNIqY0gXkDnvkQEREIGnCaDLaddVFtFxk+wDCagQoN8CugCFUASuEpjXIBIhP3OSdgAAAABJRU5ErkJggg==">
                                        </div>
                                        <div class="button number" role="button" data-value="0">0</div>
                                        <div id="dot" class="button dot sign" data-value="." role="button">.</div>
                                        <div id="equal" class="button image equals sign" data-value="=" role="button">
                                            <img class="icon icons8-Equal-Sign" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAQ0lEQVRYR+3U0Q0AMAQFQPYfuh1A+ikkPQMgF0/GcOXw/LAAAQIECLwETtOLLvPWLtAEUNs6wrUCUiAFBAgQIPCPwAX8vwQhcNJI5wAAAABJRU5ErkJggg==" width="32" height="32">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </main>
            </div>

        </div>
    </div>
</div>
<script defer>
    // Close Calculator
    $(".CalcClose").click(function(){
        $('.ClearCalc').click();
    });
</script>


<!-Calculator Model Start->
<div class="modal fade" id="myModalApexNew" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
<!--                <button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <a class="close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>
                <h4 class="modal-title">WHAT'S NEW AT APEXLINK</h4>
            </div>
            <div class="modal-body" style="padding:0px;">
                <div class="new-apex-data">
                </div>
            </div>

        </div>
    </div>
</div>
<!-Calculator Model End->


<div class="container">
    <div class="modal fade" id="helpThird" style="margin-top: 100px;" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>

                    <h4 class="modal-title" style="text-align: center;"><img src="<?php echo COMPANY_SUBDOMAIN_URL;?>/images/logo.png"></h4>
                </div>
                <div class="">
                    <title></title>
                    <div class="col-sm-12">
                        <h4 style="margin: 15px auto; font-size:16px; font-weight: 600">Allow an ApexLink Representative Remote Access to Solve Your Issue. </h4>
                    </div>
                    <div class="col-sm-12" style="text-align: justify; font-size: 14px; color: #000;">
                        Sometimes it’s just easier if we show you the answer to your question on your computer. We can provide you a real-time solution right on your computer, and answer any other questions you have. Training – right when you need it!
                    </div>
                    <div class="col-sm-12">
                        <h4 style="margin: 15px auto; font-size:16px; font-weight: 600"> Please enter the 7 digit Access Code the Representative will give to you to access your computer</h4>
                    </div>
                    <div class="col-sm-12">
                        <div bgcolor="silver">
                            <form METHOD="GET" ACTION="https://apexlink.bomgar.com/check_access_key" name="ssid">
                                <table style="width: 100%;">
                                    <tbody>
                                    <tr>
                                        <td style="font-size:16px; font-weight: 600; width: 120px; vertical-align: middle; ">Name:</td>
                                        <td valign="middle">
                                            <input required type="text" class="form-control mb-15" value="<?php echo $_SESSION[SESSION_DOMAIN]['default_name']; ?>" name="" placeholder="Testapex Kambo"></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size:16px; font-weight: 600; width: 120px; vertical-align: middle; ">Company:</td>
                                        <td valign="middle"><input type="text" name="" class="form-control mb-15" value="<?php echo $_SESSION[SESSION_DOMAIN]['company_name']; ?>" placeholder="Apexlink LLC"></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size:16px; font-weight: 600; width: 120px; vertical-align: middle;">Access Code * :</td>
                                        <td valign="middle">
                                            <input required type="tel" id="txtSupportSessionCode" name="access_key_pretty" class="form-control mb-15" placeholder="Access Code"  maxlength="7" spellcheck="true" autocomplete="off" fixedlength="7">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <p style="font-size: 16px; font-family: arial; color: #000; font-weight: normal;">
                                                <i>*Your Representative will provide you with this code. </i>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="submit" name="" class="btn connect-btn-pop"  value="Connect Now">
                                        </td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>

    $(document.body).click( function(e) {
        var container = $(".support_class");
        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            $('#support-submenu').hide();
        }

    });

    $('#suggest_features_form_id').validate({
        rules: {
            name: {
                required: true
            },
            email: {
                required: true
            },
            subject: {
                required: true
            },
            suggest_feature: {
                required: true
            }
        },
        submitHandler: function () {
            var name = $('#name').val();
            var company_name = $('#company_name').val();
            var email = $('#email').val();
            var phone_number = $('#phone_number').val();
            var subject = 'Suggest Features';
            var suggest_feature = $('#suggest_feature').val();


            var formData = {
                'name': name,
                'company_name': company_name,
                'email': email,
                'phone_number': phone_number,
                'subject': subject,
                'message': suggest_feature,
                'mail_subject' : 'support_suggestion'
            };
            $.ajax({
                type: 'post',
                url: '/SupportManagement-Ajax',
                data: {
                    class: 'supportManagement',
                    action: 'sendMail',
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    console.log('response>>>', response);
                    if (response.status == 'success' && response.code == 200) {
                        toastr.success(response.message);
                        $('#myModalSuggestFeature').modal('hide');
                    } else if (response.status == 'error' && response.code == 504){
                        toastr.error(response.message);
                    } else {
                        toastr.error("Something went wrong! Please contact to Administrator.");
                    }
                }
            });
        }
    });

    $(document).on('click', '#suggest_feature_validation_id', function() {
        setTimeout(function () {
            $( "#suggest_feature" ).focus();
        },200);
        $(".error").text('');
        $("#suggest_feature").val('');
    });

    $('#support_report_form_id').validate({
        rules: {
            name: {
                required: true
            },
            email: {
                required: true
            },
            subject: {
                required: true
            },
            message: {
                required: true
            }
        },
        submitHandler: function () {
            // var formData = $('#support_report_form_id').serializeArray();

            var name = $('#request_name').val();
            var company_name = $('#request_company_name').val();
            var email = $('#request_email').val();
            var phone_number = $('#request_phone_number').val();
            var subject_class = $('#request_subject').val();
            var message = $('#message').val();
            var formData = {
                'name': name,
                'company_name': company_name,
                'email': email,
                'phone_number': phone_number,
                'subject': subject_class,
                'message': message,
                'mail_subject' : 'support_request'
            };
            $.ajax({
                type: 'post',
                url: '/SupportManagement-Ajax',
                data: {
                    class: 'supportManagement',
                    action: 'sendMail',
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    console.log('response>>>', response);
                    if (response.status == 'success' && response.code == 200) {
                        toastr.success(response.message);
                        $('#myModalSupportRequest').modal('hide');
                    }
                }
            });
        }
    });

    $(document).on('click', '#support_validation_id', function() {
        setTimeout(function () {
            $( "#request_subject" ).focus();
        },200);
        $(".error").text('');
        $("#request_subject").val('');
        $("#message").val('');
    });

    /**
     * Change status of Grid
     * @param status
     */
    function changeGridStatus(status){
        $.ajax
        ({
            type: 'post',
            url: '/common-ajax',
            data: {
                class: "CommonAjax",
                action: "changeStatus",
                status:status
            },
            success: function (response) {
                var response =  JSON.parse(response);
                if(response.status == 'success' && response.code == 200) {
                    localStorage.setItem('active_inactive_status',response.data)
                    return false;
                } else {

                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
</script>
<footer>
    <div class="container-fluid">
        © <?php echo date("Y"); ?> Apexlink, Inc.  All rights reserved.
    </div>
</footer>

<!-- Footer Ends -->
