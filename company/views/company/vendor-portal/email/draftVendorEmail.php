<?php
//if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
//    $url = SUBDOMAIN_URL;
//    header('Location: ' . $url);
//}
if ((!isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) && (!isset($_SESSION['Admin_Access']['vendor_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/VendorPortal/login');
}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_header.php");
?>
<?php  include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_top_navigation.php"); ?>
<section class="main-content">
    <div class="container-fluid">
        <div class="row">
            <div class="bread-search-outer">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="breadcrumb-outer">
                            Communication &gt;&gt; <span>Email </span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="easy-search">
                            <input placeholder="Easy Search" type="text"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-data">

                <!--Tabs Starts -->
                <div class="main-tabs">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="/VendorPortal/Vendor/Communication/SentEmails">Email</a></li>
                        <li role="presentation"><a href="/VendorPortal/Communication/TextMessage">Text Message</a></li>
                    </ul>

                    <!-- Tab panes -->

                    <div class="tab-content">
                        <div class="panel-heading">
<!--                            --><?php //include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="receivables">
                                    <div class="property-status">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="content-section">
                                                    <div class="main-tabs">
                                                        <div role="tabpanel" class="tab-pane active" id="communication-one">
                                                            <!-- Sub Tabs Starts-->
                                                            <div class="property-status">
                                                                <div class="row">
                                                                    <div class="col-sm-2">
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <div class="btn-outer text-right">
                                                                            <a href="/VendorPortal/Communication/InboxMails" class="blue-btn margin-right">Inbox</a>
                                                                            <a href="/VendorPortal/Vendor/Communication/SentEmails" class="blue-btn margin-right">Sent Mails</a>
                                                                            <a data-urltype="" href="/VendorPortal/Communication/ComposeEmail" class="blue-btn margin-right compose-mail">Compose Mail</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-outer" >
                                                                <div class="form-hdr">
                                                                    <h3>Drafts</h3>
                                                                </div>
                                                                <div class="form-data">
                                                                    <div class="col-sm-12">
                                                                        <div class="grid-outer">
                                                                            <div class="table-responsive">
                                                                                <div class="apx-table ">
                                                                                    <div class="table-responsive">
                                                                                        <table id="communication-draft-vendor-table" class="table table-bordered"></table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Regular Rent Ends -->

                            </div>
                        </div>
                        <!-- Sub tabs ends-->
                    </div>



                    <!-- Sub Tabs Starts-->

                    <!-- Sub tabs ends-->
                </div>

            </div>
        </div>

        <!--Tabs Ends -->

    </div>
    </div>
    </div>
</section>
</div>

<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
</script>
<script>
    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
        $("#communication").addClass("active");
    });
</script>
<!-- Wrapper Ends -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/email/common.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/vendor-portal/email/draft-vendor-email.js"></script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>

<!-- Footer Ends -->


<!-- Jquery Starts -->

</body>

</html>
