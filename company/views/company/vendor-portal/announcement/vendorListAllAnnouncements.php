<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
//if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
//    $url = SUBDOMAIN_URL;
//    header('Location: ' . $url);
//}
if ((!isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) && (!isset($_SESSION['Admin_Access']['vendor_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/VendorPortal/login');
}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_header.php");
?>


    <div id="wrapper">
        <!-- Top navigation start -->
        <?php  include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_top_navigation.php"); ?>
        <!-- Top navigation end -->


        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data">
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Announcements</h3>
                                </div>
                                <div class="form-data allMasterAnnouncements">
                                    No records found!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Wrapper Ends -->


    <!-- Footer Ends -->
    <script>
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        $.ajax({
            type: 'post',
            url: '/VendorPortalAnnouncement-Ajax',
            data: {
                class: "announcementAjax",
                action: "showAllMasterAnnouncement",
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    var res = response.data;
                    console.log('res>sdsdsds>>', res);
                    var html = '';
                    $.each(res, function (key, value) {
                        html += '<div class="form-outer">\n' +
                            '<div class="form-hdr">\n' +
                            '<h3>'+value.created_at+'</h3>\n' +
                            '<h3>'+value.title+'</h3>\n' +
                            '</div>\n' +
                            '<div class="form-data">\n' +
                            '<span>'+value.description+'</span>\n' +
                            '</div>\n' +
                            '</div>';

                    });
                    $('.allMasterAnnouncements').html(html);
                }
            }
        });
    </script>
    <script>
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });
        });

        $(document).ready(function(){
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
            $("#communication").addClass("active");
        });


    </script>
    <!-- Jquery Starts -->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>