<?php
if ((!isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) && (!isset($_SESSION['Admin_Access']['vendor_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/VendorPortal/login');
}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_header.php");
?>

<div id="wrapper">
    <?php  include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_top_navigation.php"); ?>


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">
                        <div class="form-outer form-outer2">
                            <div class="form-hdr">
                                <h3>
                                    Transaction Details</h3>
                            </div>
                            <div class="form-data">
                                <div class="col-sm-2">
                                    <label>Start Date</label>
                                    <span><input class="form-control calander st_date" type="text"/></span>
                                </div>
                                <div class="col-sm-2">
                                    <label>End Date</label>
                                    <span><input class="form-control calander end_date" type="text"/></span>
                                </div>
                                <div class="col-sm-2">
                                    <label> </label>
                                    <input type="button" id="search_ledger" class="blue-btn" value="Search">
                                </div>
                                <div class="grid-outer" style="padding-top: 20px">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-dark" id="ledger_table">
                                            <thead>
                                            <tr>
                                                <th scope="col">Date</th>
                                                <th scope="col">Transaction ID</th>
                                                <th scope="col">Transaction Description</th>
                                                <th scope="col">Reference No.</th>
                                                <th scope="col">Original Amount </th>
                                            </tr>
                                            </thead>
                                            <tbody id="vendor_ledger">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="ledger-icon" style="padding-top: 20px">
                                       <div class="text-right">
                                           <button id="printsample" class="blue-btn" title="Print" data-toggle="tooltip"><i class="fa fa-print"></i> </button>
                                           <button id="exportpdf" class="blue-btn " title="Download PDF" data-toggle="tooltip"><i class="fa fa-download"></i> </button>
                                           <button  class="blue-btn" title="Email" data-toggle="tooltip"><i class="fa fa-envelope"></i> </button>
                                           <button id="exportexcel" class="blue-btn" title="Download Excel" data-toggle="tooltip"><i class="fa fa-file-excel-o"></i> </button>
                                       </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->
<script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_footer.php");
?>

<script>

    $("#transaction").addClass("active");
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });

    $('.calander').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    $(".calander").val(date);

    <!--- Accordians -->
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/vendor/vendortransactions.js"></script>