<?php
if ((!isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) && (!isset($_SESSION['Admin_Access']['vendor_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/VendorPortal/login');
}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];

include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_header.php");
?>
<div id="wrapper">
    <?php  include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_top_navigation.php"); ?>
    <!-- MAIN Navigation Starts -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">
                        <div class="form-outer2" id="filelibrary">
                            <div class="form-hdr">
                                <h3>File Library</h3>
                            </div>
                            <div class="form-data">
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                    <div class="form-outer2 add_libraray_Div" >
                                                        <div class="col-sm-12">
                                                            <button type="button" id="add_libraray_file" class="green-btn">Click Here to Upload</button>
                                                            <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                            <button class="orange-btn" id="remove_library_file">Remove All Files</button>
                                                            <button type="button" id="saveLibraryFiles" class="blue-btn">Save</button>
<!--                                                            <button type="button" class="grey-btn cancel-all">Cancel</button>-->
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="row" id="file_library_uploads">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="grid-outer listinggridDiv">
                                                        <div class="apx-table apxtable-bdr-none">
                                                            <div class="table-responsive">
                                                                <table id="fileLibraryTable" class="table table-bordered">
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Jquery Starts -->
<script>
    var pagination = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['pagination']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['pagination'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['pagination'] : $_SESSION['Admin_Access']['pagination'] ?>';
    var datepicker = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'] : $_SESSION['Admin_Access']['datepicker_format'] ?>';
    var jsDateFomat = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'] : $_SESSION['Admin_Access']['datepicker_format'] ?>';
    var staticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Vehicledummy.png">';
    var petStaticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg">';
    var vendor_id = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'] : $_SESSION['Admin_Access']['vendor_portal_id'] ?>';
    var upload_url = "<?php echo SITE_URL; ?>";
    var default_currency_symbol = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_currency_symbol']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_currency_symbol'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_currency_symbol'] : $_SESSION['Admin_Access']['default_currency_symbol'] ?>';
    var default_name = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_name']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_name'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_name'] : $_SESSION['Admin_Access']['default_name'] ?>';
</script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/additionalMethods.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/vendor-portal/vendorFileLibratry.js" type="text/javascript"></script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_footer.php");
?>