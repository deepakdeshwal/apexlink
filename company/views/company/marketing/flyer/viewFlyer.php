<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

    <div id="wrapper">
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- MAIN Navigation Starts -->

        <!-- MAIN Navigation Ends -->


        <section class="main-content">
            <div class="container-fluid">
                <div class="row flex">
                    <?php
                    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/marketing/layouts/leftBar.php");
                    ?>

                    <div class="col-sm-8 col-md-10 main-content-rt">
                        <div class="welcome-text visible-xs">
                            <div class="welcome-text-inner">
                                <div class="col-xs-9">    Welcome:  Sonny Kesseben (ACL Properties), June 01, 2018 </div>
                                <div class="col-xs-3">
                                    <a herf="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                                    <a href="javascript:;"><i class="fa fa-calculator" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content-rt">
                            <div class="bread-search-outer">
                                <div class="col-sm-8">
                                    <div class="breadcrumb-outer">
                                        Marketing >> <span>Flyer</span>
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    <div class="easy-search">
                                        <input placeholder="Easy Search" type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="content-data">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Flyers</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="flyer-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <h4>Flyer Template 1</h4>
                                                            <div class="flyer-links">
                                                                <a href="javascript:;" class="flyerStatus" rel="Template1" id="t1">Set as Default</a>
                                                                <a href="javascript:;" class="defaultChange" id="td1" style="display:none">Default <i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                                                <a href="/Marketing_Flyer_Templates/FlyerTemplate1" target="_blank">Preview</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 text-right"> <img src="<?php echo COMPANY_SITE_URL;?>/images/flyer1.jpg"/></div>
                                                    </div>
                                                </div>

                                                <div class="flyer-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <h4>Flyer Template 2</h4>
                                                            <div class="flyer-links">
                                                                <a href="javascript:;" class="flyerStatus" rel="Template2" id="t2">Set as Default</a>
                                                                <a href="javascript:;" class="defaultChange" style="display:none" id="td2">Default <i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                                                <a href="/Marketing_Flyer_Templates/FlyerTemplate2" target="_blank">Preview</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 text-right"> <img src="<?php echo COMPANY_SITE_URL;?>/images/flyer2.jpg"/></div>
                                                    </div>
                                                </div>

                                                <div class="flyer-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <h4>Flyer Template 3</h4>
                                                            <div class="flyer-links">
                                                                <a href="javascript:;" class="flyerStatus" rel="Template3" id="t3">Set as Default</a>
                                                                <a href="javascript:;" class="defaultChange" style="display:none" id="td3">Default <i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                                                <a href="/Marketing_Flyer_Templates/FlyerTemplate3"target="_blank">Preview</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 text-right"> <img  src="<?php echo COMPANY_SITE_URL;?>/images/flyer3.jpg"/></div>
                                                    </div>
                                                </div>

                                                <div class="flyer-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <h4>Flyer Template 4</h4>
                                                            <div class="flyer-links">
                                                                <a href="javascript:;" class="flyerStatus" rel="Template4" id="t4">Set as Default</a>
                                                                <a href="javascript:;" class="defaultChange" style="display:none" id="td4">Default <i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                                                <a href="/Marketing_Flyer_Templates/FlyerTemplate4" target="_blank">Preview</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 text-right"> <img  src="<?php echo COMPANY_SITE_URL;?>/images/flyer4.jpg"/></div>
                                                    </div>
                                                </div>

                                                <div class="flyer-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <h4>Flyer Template 5</h4>
                                                            <div class="flyer-links">
                                                                <a href="javascript:;" class="flyerStatus" rel="Template5" id="t5">Set as Default</a>
                                                                <a href="javascript:;" class="defaultChange" style="display:none" id="td5">Default <i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                                                <a href="/Marketing_Flyer_Templates/FlyerTemplate5" target="_blank">Preview</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 text-right"> <img  src="<?php echo COMPANY_SITE_URL;?>/images/flyer5.jpg"/></div>
                                                    </div>
                                                </div>

                                                <div class="flyer-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <h4>Flyer Template 6</h4>
                                                            <div class="flyer-links">
                                                                <a href="javascript:;" class="flyerStatus" rel="Template6" id="t6">Set as Default</a>
                                                                <a href="javascript:;" class="defaultChange" style="display:none" id="td6">Default <i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                                                <a href="/Marketing_Flyer_Templates/FlyerTemplate6" target="_blank">Preview</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 text-right"> <img style="width: 150px; height: 111px;" src="<?php echo COMPANY_SITE_URL;?>/images/flyer6.jpg"/></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Flyers</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="flyer-outer">
                                                    <form id="flyerUpload">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>Upload Logo</label>
                                                            <span><input type="file" name="logo"/></span>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label></label>
                                                            <button class="blue-btn">Upload</button>
                                                        </div>
                                                    </div>
                                                        <div class="include-realtor-logo">
                                                            <span id="spnMarketingLogoName"></span>
                                                            <div class="include-realtor-logo-img">
                                                                <img id="imgMarketingLogo" width="58" height="58" alt="" src="" style="width: 58px; height: 58px;">
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- Content Data Ends ---->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Wrapper Ends -->



    <!-- Jquery Starts -->

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.responsivetabs.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/marketing/flyer/flyer.js" type="text/javascript"></script>

    <script>
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });
        });

        $(document).ready(function(){
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
        });



    </script>




    <!-- Jquery Starts -->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>