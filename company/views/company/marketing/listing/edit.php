<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

    <div id="wrapper">

        <!-- MAIN Navigation Starts -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
 <!-- MAIN Navigation Ends -->
        
        
        <section class="main-content">
                <div class="container-fluid">
                <div class="row flex">
                    <?php
                    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/marketing/layouts/leftBar.php");
                    ?>

                <div class="col-sm-8 col-md-10 main-content-rt">

                    <div class="content-rt">
                        <!--- Right Quick Links ---->


                        <!--- Right Quick Links ---->
                        
                        <div class="bread-search-outer">
                          <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                              Marketing >> <span>Listing Edit</span>
                            </div>

                          </div>
                          <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text">
                            </div>
                          </div>
                        </div>

                        <div class="content-data">
                
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="form-outer">

                                <div class="form-hdr">
                                  <h3>Listing Information</h3><a class="back pull-right" href="/Marketing/view?id=<?= $_GET['id'];?>"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                </div>
                                <div class="form-data">
                                    <form id="updatePropertyPostData">
                                  <div class="row"> 
                                    <div class="col-sm-3">
                                        <label>Listing Type</label>
                                        <select class="form-control property_for_sale" name="property_for_sale">
                                            <option value="">Select</option>
                                            <option value="No">For Rent</option>
                                            <option value="Yes">For Sale</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Posting Title</label>
                                        <input class="form-control" type="text" name="posting_type">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Property Style</label>
                                        <select class="form-control" name="property_style" id="property_style_options"></select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Address</label>
                                        <input class="form-control" type="text" name="address1">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>City</label>
                                        <input class="form-control" type="text" name="city">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>State / Province</label>
                                        <input class="form-control" type="text" name="state">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Country</label>
                                        <input class="form-control" type="text" name="country">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>No. of Units</label>
                                        <input disabled="" class="form-control" type="text" name="no_of_units">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Year Built</label>
                                        <?php
                                        $yearArray = range(1900, 2019);
                                        ?>
                                        <select class="form-control" id="year_built" name="property_year">
                                            <option value="" selected="">(Select Year Build)</option>
                                            <?php
                                            foreach ($yearArray as $year) {
                                                echo '<option value="' . $year . '">' . $year . '</option>';
                                            }
                                            ?>
                                        </select>

                                    </div>
                                    <div class="col-sm-3">
                                        <label>Vacant Units</label>
                                        <input disabled="" class="form-control" type="text" name="vacant">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Property Type</label>
                                        <select class="form-control" name="property_type" id="property_type_options"><option>Commercial</select>
                                    </div>

                                    <div class="col-sm-3 pet_friendly_radio_btn">

                                    </div>
                                    <div class="col-sm-3">
                                        <label>No. of Building</label>
                                        <input disabled="" class="form-control" type="text" name="no_of_buildings">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Description</label>
                                        <textarea class="form-control" name="description"></textarea>
                                    </div>
                                    <div class="col-sm-3 parking_radio_btn">

                                    </div>
                                    <div class="col-sm-12">
                                        <div class="btn-outer text-right">
                                          <button class="blue-btn">Update</button>
                                          <button type="button" class="clear-btn clearFormReset">Reset</button>
                                          <button class="grey-btn">Cancel</button>
                                        </div>
                                    </div>
                                  
                                  </div>
                                </form>
                                </div>

                                
                              </div>
                              <div class="form-outer form-outer2">
                                <div class="form-hdr">
                                  <h3>Amenities</h3>
                                </div>
                                <div class="form-data">
                                    <form id="updateAmentiesData">
                                      <div class="row">
                                          <div class="col-md-12">
                                          <div id="amenties_box1">

                                          </div>
                                          </div>

                                        <div class="col-sm-12">
                                          <div class="btn-outer text-right">
                                            <button class="blue-btn">Update</button>
                                              <button  type="button" class="clear-btn clearFormReset">Reset</button>
                                            <button class="grey-btn">Cancel</button>
                                          </div>
                                        </div>
                                      </div>
                                </form>
                                </div>

                                
                              </div>

                              <div class="form-outer">
                                <div class="form-hdr">
                                  <h3>Listing Agent</h3>
                                </div>
                                <div class="form-data">
                                  <div class="property-status">
                                    <div class="btn-outer">
                                     
                                    </div>
                                  </div>
                                  <div class="grid-outer">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-dark">
                                          <thead>
                                            <tr>
                                              <th scope="col">Name</th>
                                              <th scope="col">Contact No.</th>
                                              <th scope="col">Email</th>
                                              <th scope="col">Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                              <td>John</td>
                                              <td>122,455,6549</td>
                                              <td><select class="form-control"><option>Select</option></select></td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                </div>

                                </div>
                                  <div class="form-outer form-outer2">
                                       <div class="form-hdr">
                                        <h3>Media</h3>
                                       </div>
                                       <div class="form-data">


                                           <div class="form-outer2">
                                               <div class="col-sm-12">
                                                   <div class="btn-outer text-right">
                                                       <button type="button" id="uploadPhotoVideo" class="green-btn">Click Here to Upload</button>
                                                       <input id="photosVideos" type="file" name="photo_videos[]" accept="image/*, .mp4, .mkv, .avi" multiple style="display: none;">
                                                       <button type="button" id="removePhotoVideo" class="orange-btn">Remove All Photos</button>
                                                       <button type="button" id="savePhotoVideo" class="blue-btn">Save </button>
                                                   </div>
                                               </div>
                                               <div class="col-sm-12">
                                                   <div class="row" id="photo_video_uploads">
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="row">
                                               <div class="col-sm-12">
                                                   <div class="grid-outer listinggridDiv">
                                                       <div class="apx-table apxtable-bdr-none">
                                                           <div class="table-responsive">
                                                               <table id="propertPhotovideos-table" class="table table-bordered">
                                                               </table>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                      </div>




                              </div>



                            

                            </div>
                          </div>
                                                
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
                    </div>
                    </div>
        </section>
    </div>
        <!-- Wrapper Ends -->
    

    <!-- Footer Ends -->


    <!-- Jquery Starts -->

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.responsivetabs.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/marketing/listing/edit.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/marketing/listing/photoVideos.js" type="text/javascript"></script>
     
    <script>
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        var upload_url = "<?php echo SITE_URL; ?>";
        var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });
 
        <!--- Main Nav Responsive --> 
        $("#show").click(function(){
    $("#bs-example-navbar-collapse-2").show();
});
         $("#close").click(function(){
    $("#bs-example-navbar-collapse-2").hide();
});
         <!--- Main Nav Responsive -->
        
        
        $(document).ready(function(){
          $(".slide-toggle").click(function(){
            $(".box").animate({
              width: "toggle"
            });
          });
        });
        
        $(document).ready(function(){
          $(".slide-toggle2").click(function(){
            $(".box2").animate({
              width: "toggle"
            });
          });
        });


        
       
        
    </script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>