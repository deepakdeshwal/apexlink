<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
    <style>
        ul, li {
            list-style-type: none !important;
        }

    </style>
    <div id="wrapper">

        <!-- MAIN Navigation Starts -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- MAIN Navigation Ends -->


        <section class="main-content">
            <div class="container-fluid">
                <div class="row flex">
                    <?php
                    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/marketing/layouts/leftBar.php");
                    ?>

                    <div class="col-sm-8 col-md-10 main-content-rt">
                        <div class="content-rt">
                            <!--- Right Quick Links ---->


                            <!--- Right Quick Links ---->

                            <div class="bread-search-outer">
                                <div class="col-sm-8">
                                    <div class="breadcrumb-outer">
                                        Marketing >> <span>Listing</span>
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    <div class="easy-search">
                                        <input placeholder="Easy Search" type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="content-data">
                                <!-- Main tabs -->
                                <!--Tabs Starts -->
                                <div class="main-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="/Marketing/MarketingListing">Listing</a></li>
                                        <li role="presentation"><a href="/Marketing/MarketingPropertiesMap">Map</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="market-listing">
                                            <div class="accordion-form">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion"> Properties</a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne" class="panel-collapse collapse in">
                                                                    <div class="panel-body">
                                                                        <div class="form-outer">
                                                                            <div class="row">
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Price</label>
                                                                                    <input class="form-control property_price" type="text" name="property_price">
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Property Name</label>
                                                                                    <select class="form-control" name="property_id" id="property_id"></select>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <div class="btn-outer">
                                                                                        <label>&nbsp;</label>
                                                                                        <button class="blue-btn search_property_btn">Search</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id="market-list">

                                                                        </div>

                                                                        <div id="pagination">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- First Tab Ends -->



                                    </div>
                                </div>


                            </div>
                            <!-- Content Data Ends ---->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Wrapper Ends -->


    <!-- Footer Ends -->
    <script>
        var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
        var upload_url = "<?php echo SITE_URL; ?>";
    </script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/marketing/listing/listing.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/autonumeric.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.twbsPagination.min.js" type="text/javascript"></script>

    <!-- Jquery Starts -->
    <script>

        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });
        });

        $(document).ready(function(){
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
        });



    </script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>

<div  class="modal fade postData" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Vaccant Units</h4>
            </div>
            <div class="modal-body">
    <form id="postDataForm">
             <div class="row">
<!--                 <div class="col-md-12">-->
<!--                     <div class="col-md-4">-->
<!--                        <input type="checkbox" value="1" id="publish1">-->
<!--                     </div>-->
<!--                     <div class="col-md-8">-->
<!--                         <span>Post Listing</span>-->
<!--                     </div>-->
<!--                 </div>-->
<!---->
<!--                 <div class="col-md-12">-->
<!--                     <div class="col-md-4">-->
<!--                         <input type="checkbox" value="2" id="feature1">-->
<!--                     </div>-->
<!--                     <div class="col-md-8">-->
<!--                         <span>Show on Map</span>-->
<!--                     </div>-->
<!--                 </div>-->
<!---->
<!--                 <div class="col-md-12">-->
<!--                     <div class="col-md-4">-->
<!--                         <input type="checkbox" value="3" id="show_on_map1">-->
<!--                     </div>-->
<!--                     <div class="col-md-8">-->
<!--                         <span>To feature/ un-feature this property</span>-->
<!--                     </div>-->
<!--                 </div>-->


                 <div class="col-md-12">
                     <table class="table table-hover table-bordered">
                         <tbody>
                            <tr>
                                <td>
                                    <input type="checkbox" value="1" id="publish1">
                                </td>
                                <td>
                                    <span>Post Listing</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" value="2" id="feature1">
                                </td>
                                <td>
                                    <span>Show on Map</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" value="3" id="show_on_map1">
                                </td>
                                <td>
                                    <span>To feature/ un-feature this property</span>
                                </td>
                            </tr>
                         </tbody>
                     </table>
                 </div>

             </div>
                <div class="row">
                    <div class="col-md-12">
                        <a class="blue-btn postDataBtn">Done</a>
                    </div>
                </div>
    </form>
            </div>

        </div>

    </div>
</div>
