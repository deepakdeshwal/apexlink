<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- MAIN Navigation Ends -->
    <section class="main-content">
        <div class="container-fluid">

</div>
<!-- Content Data Ends ---->
<!-- Wrapper Ends -->


<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
</script>
<!-- Jquery Starts -->
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SITE_URL;?>js/jquery.responsivetabs.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/default_settings.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/defaultsetting/defaultSetting.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/defaultsetting/defaultSetting_tab1.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/defaultsetting/changepassword.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/defaultsetting/timesetting.js" type="text/javascript"></script>
<!--<script src="--><?php //echo COMPANY_SITE_URL; ?><!--/js/validation/changepassword.js" type="text/javascript"></script>-->
<script>
    $('#leftnav1').addClass('in');
    $('.default_settings').addClass('active');

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
</script>
<script>

    $(document).ready(function() {
        $('.summernote').summernote({
            addclass: {
                debug: false,
                classTags: [{title:"Button","value":"btn btn-success"},"jumbotron", "lead","img-rounded","img-circle", "img-responsive","btn", "btn btn-success","btn btn-danger","text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
            },
            width: '60%',
            //margin-left: '15px',
            toolbar: [
                // [groupName, [list of button]]
                ['img', ['picture']],
                ['style', ['style', 'addclass', 'clear']],
                ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
                ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
                ['extra', ['video', 'table', 'height']],
                ['misc', ['undo', 'redo', 'codeview', 'help']]
            ]
        });
    });

    $('input[type=checkbox]').click(function () {
        $(this).parent().find('li input[type=checkbox]').prop('checked', $(this).is(':checked'));
        var sibs = false;
        $(this).closest('ul').children('li').each(function () {
            if($('input[type=checkbox]', this).is(':checked')) sibs=true;
        })
        $(this).parents('ul').prev().prop('checked', sibs);
    });

</script>
<script type="text/javascript">
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    $(function () {
        $(".numeric").bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            $(".error").css("display", ret ? "none" : "inline");
            return ret;
        });
        $(".numeric").bind("paste", function (e) {
            return false;
        });
        $(".numeric").bind("drop", function (e) {
            return false;
        });
    });
</script>
<?php
/*include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
*/?>