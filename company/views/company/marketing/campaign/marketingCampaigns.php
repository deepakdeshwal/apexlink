<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

    <div id="wrapper">
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- MAIN Navigation Starts -->
 <!-- MAIN Navigation Ends -->
        
        
        <section class="main-content">
                <div class="container-fluid">
                <div class="row flex">
                    <?php
                    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/marketing/layouts/leftBar.php");
                    ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="welcome-text visible-xs">
                      <div class="welcome-text-inner">
                        <div class="col-xs-9">    Welcome:  Sonny Kesseben (ACL Properties), June 01, 2018 </div>
                        <div class="col-xs-3">
                          <a herf="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i></a> 
                          <a href="javascript:;"><i class="fa fa-calculator" aria-hidden="true"></i></a>
                        </div>
                      </div>
                    </div>
                    <div class="content-rt">
                        <div class="bread-search-outer">
                          <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                              Marketing >> <span>Campaign</span>
                            </div>

                          </div>
                          <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text">
                            </div>
                          </div>
                        </div>

                        <div class="content-data">
                
                          <div class="row">
                            <div class="col-sm-12">
                                <div class="form-outer add-campaign-div" style="display:none">
                                    <form id="add_new_campaign_form">
                                        <input type="hidden" id="campaign_id">
                                    <div class="form-hdr">
                                        <h3>Add New Campaign</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Source Name <em class="red-star">*</em></label>
                                                <select class="form-control source_name" name="source_name">
                                                 <option value="">Select</option>
                                                    <option value="Google">Google</option>
                                                    <option value="Craigslist">Craigslist</option>
                                                    <option value="Trulia">Trulia</option>
                                                    <option value="oodles">Oodles</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Campaign Name <em class="red-star">*</em></label>
                                                <input class="form-control compaign_name" type="text" name="compaign_name">
                                            </div>
                                            <div class="col-md-2">
                                                <label>Start Date</label>
                                                <input class="form-control start_date_campaign" type="text" id="start_date_campaign_id" name="start_date">
                                            </div>
                                            <div class="col-md-2">
                                                <label>End Date </label>
                                                <input class="form-control end_date_campaign" type="text" id="end_date_campaign_id" name="end_date">
                                            </div>
                                            <div class="col-md-2">
                                                <label>Call Tracking No. <em class="red-star">*</em></label>
                                                <input class="form-control call_track_number" type="text" name="call_track_number">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="btn-outer text-right">
                                        <button  class="blue-btn add_campains_btn">Save</button>
                                        <button rel="add_new_campaign_form" data-id="add_campains_btn" type="button" class="clear-btn clearFormReset">Clear</button>
                                        <button  type="button" class="grey-btn cancel_campains_btn">Cancel</button>
                                    </div>
                                </form>
                                </div>
                              <div class="form-outer">
                                <div class="form-hdr">
                                  <h3>Campaigns</h3></div>
                                <div class="form-data">
                                  <div class="property-status">
                                    <div class="btn-outer">
                                      <button class="blue-btn pull-right add-campaign-btn">Add New Campaign</button>
                                    </div>
                                  </div>
                                    <div class="grid-outer listinggridDiv">
                                        <div class="apx-table apxtable-bdr-none">
                                            <div class="table-responsive">
                                                <table id="campaign-table" class="table table-bordered">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>


                            </div>
                          </div>
                                                
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
                    </div>
                    </div>
        </section>
    </div>
        <!-- Wrapper Ends -->
    



    <!-- Jquery Starts -->

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.responsivetabs.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/marketing/campaigns/campaigns.js" type="text/javascript"></script>
     
    <script>
        var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
        var defaultFormData = '';
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });
 
        <!--- Main Nav Responsive --> 
        $("#show").click(function(){
    $("#bs-example-navbar-collapse-2").show();
});
         $("#close").click(function(){
    $("#bs-example-navbar-collapse-2").hide();
});
         <!--- Main Nav Responsive -->
        
        
        $(document).ready(function(){
          $(".slide-toggle").click(function(){
            $(".box").animate({
              width: "toggle"
            });
          });
        });
        
        $(document).ready(function(){
          $(".slide-toggle2").click(function(){
            $(".box2").animate({
              width: "toggle"
            });
          });
        });
        
       
        
    </script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>