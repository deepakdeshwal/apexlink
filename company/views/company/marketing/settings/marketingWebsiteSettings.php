<?php

/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

$WebURL= $_SESSION[SESSION_DOMAIN]['domain_name'].'.'.COMPANY_WEBSITE_URL;
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");


?>
<style>

    #capitalRemove
    {
        text-transform: lowercase;
    }
</style>

    <div id="wrapper">
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- MAIN Navigation Starts -->

 <!-- MAIN Navigation Ends -->


        <section class="main-content">
                <div class="container-fluid">
                <div class="row flex">
                    <?php
                    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/marketing/layouts/leftBar.php");
                    ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <form id="edit-form-contactdetails">
                        <input type="hidden" id="contact_detail_id" value="">
                    <div class="content-rt">
                        
                        <div class="bread-search-outer">
                          <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                              Marketing >> <span>Settings</span>
                            </div>

                          </div>
                          <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text">
                            </div>
                          </div>
                        </div>

                        <div class="content-data">
                
                          <div class="row">
                            <div class="col-sm-12">
                                <div class="view-marketing-contactdetails">
                                  <div class="form-outer">
                                    <div class="form-hdr">
                                      <h3>Marketing Website Settings</h3>
                                    </div>
                                    <div class="form-data">
                                      <div class="row">
                                        <div class="col-md-3">
                                            <label>Entity Company Name <em class="red-star">*</em></label>
                                            <input disabled="" class="form-control company_name_view" type="text">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Marketing Website URL</label>
                                            <input  class="form-control marketing_web_view" type="text" value="<?= $WebURL; ?>" readonly>
                                        </div>
                                        <div class="col-md-3">
                                            <label>&nbsp;</label>
                                            <div class="check-outer">
                                              <input class="marketing_setting_publish" disabled=""  type="checkbox"/> <label>Publish Marketing Website </label>
                                            </div>
                                        </div>
                                      </div>
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <label>&nbsp;</label>
                                                <div class="check-outer">
                                                    <img class='marketing_logo' style='width: 100%' src="">
                                                </div>
                                            </div>
                                        </div>
                                      <div class="edit-foot">
                                                <a href="javascript:;" class="click-marketing-contact">
                                                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                  Edit
                                                </a>
                                              </div>
                                    </div>


                                  </div>
                                  <div class="form-outer form-outer2">
                                    <div class="form-hdr">
                                      <h3>Contact Information</h3>
                                    </div>
                                    <div class="form-data">
                                      <div class="detail-outer">
                                                <div class="row">
                                                  <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                      <label class="text-right">Phone :</label>
                                                      <span class="contact_info_phn"></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Address :</label>
                                                        <span class="contact_info_add"></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                      <label class="text-right">Email :</label>
                                                      <span class="contact_info_email"></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                      <label class="text-right">Country :</label>
                                                      <span class="contact_info_country"></span>
                                                    </div>

                                                  </div>
                                                  <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                      <label class="text-right">Website :</label>
                                                      <span class="contact_info_Website"></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                      <label class="text-right">State :</label>
                                                      <span class="contact_info_state"></span>
                                                    </div>

                                                    <div class="col-xs-12">
                                                      <label class="text-right">City :</label>
                                                      <span class="contact_info_city"></span>
                                                    </div>

                                                    <div class="col-xs-12">
                                                      <label class="text-right">Hours of Operations :</label>
                                                      <span class="contact_info_ope"></span>
                                                    </div>

                                                  </div>
                                                </div>
                                              </div>
                                              <div class="edit-foot">
                                                <a href="javascript:;" class="click-marketing-contact">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                Edit
                                              </a>
                                            </div>
                                    </div>


                                  </div>
                                </div>

                              <div class="form-outer edit-marketing-contactdetails" style="display:none">
                                  <div class="form-outer">
                                      <div class="form-hdr">
                                          <h3>Marketing Website Settings</h3>
                                      </div>
                                      <div class="form-data">
                                          <div class="row">
                                              <div class="col-sm-3">
                                                  <label>Entity Company Name <em class="red-star">*</em></label>
                                                  <input  class="form-control" type="text" name="company_name">
                                              </div>
                                              <div class="col-sm-3">
                                                  <label>Marketing Website URL</label>
                                                  <input  class="form-control marketing_web_view" value="<?= $WebURL; ?>" type="text" name="url" readonly>
                                              </div>
                                              <div class="col-sm-3">
                                                  <label>&nbsp;</label>
                                                  <div class="check-outer">
                                                      <input type="checkbox" name="publish"/> <label>Publish Marketing Website </label>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-sm-2">
                                                  <label>&nbsp;</label>
                                                  <div class="check-outer">
                                                      <img class='marketing_logo' style='width: 100%' src="">
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-sm-3">
                                                  <label>&nbsp;</label>
                                                  <div class="check-outer">
                                                      <label>Upload Load </label><input type="file" name="logo" value="" class="marketing_logo_class"/>
                                                  </div>
                                              </div>
                                          </div>

                                      </div>


                                  </div>
                                <div class="form-hdr">
                                  <h3>Contact Information</h3>
                                </div>
                                <div class="form-data">
                                  <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                            <label>Phone</label>
                                            <input class="form-control conatct_phone_number" type="text" name="phone_number">
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                            <label>Address</label>
                                            <input class="form-control" type="text" name="address">
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                            <label>Email</label>
                                            <input class="form-control" type="text" name="email">
                                        </div>

                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                            <label>Zip/Postal Code</label>
                                            <input class="form-control" type="text" name="zip_code" id="zip_code">
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                            <label>Country</label>
                                            <input class="form-control" type="text" name="country" id="country">
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                            <label>State</label>
                                            <input class="form-control state" type="text" name="state">
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                            <label>City</label>
                                            <input class="form-control citys" type="text" name="city">
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                            <label>Website</label>
                                            <input class="form-control" type="text" name="website" placeholder="http://apexlink.com" id="capitalRemove">
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                            <label>Hours of Operation</label>
                                            <textarea class="form-control" name="hours_of_operation"></textarea>
                                        </div>
                                    </div>
                                        <div class="btn-outer text-right">
                                            <button class="blue-btn save_settings_btn">Update</button>
                                            <button rel="edit-form-contactdetails"  type="button" class="clear-btn clearFormReset">Reset</button>
                                            <button type="button" class="grey-btn cancel_settings_btn">Cancel</button>
                                        </div>
                                  </div>
                              </form>
                                </div>
                              </div>

                            </div>
                          </div>
                                                
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
                    </div>
                    </div>
        </section>
    </div>
        <!-- Wrapper Ends -->



    <!-- Jquery Starts -->
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/common.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/marketing/settings/settings.js" type="text/javascript"></script>




     
    <script>
        var userId =  "<?php echo $_SESSION[SESSION_DOMAIN]['cuser_id']; ?>";
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });
 
        <!--- Main Nav Responsive --> 
        $("#show").click(function(){
    $("#bs-example-navbar-collapse-2").show();
});
         $("#close").click(function(){
    $("#bs-example-navbar-collapse-2").hide();
});
         <!--- Main Nav Responsive -->
        
        
        $(document).ready(function(){
          $(".slide-toggle").click(function(){
            $(".box").animate({
              width: "toggle"
            });
          });
        });
        
        $(document).ready(function(){
          $(".slide-toggle2").click(function(){
            $(".box2").animate({
              width: "toggle"
            });
          });
        });
        
       
        
    </script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>

