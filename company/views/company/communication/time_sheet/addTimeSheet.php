<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Communication &gt;&gt; <span>Time Sheet </span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data">
                        <!--- Right Quick Links ---->
                        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                        <!--- Right Quick Links ---->
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                                <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                                <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                                <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                                <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                                <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                                <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                                <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                                <li role="presentation" class="active"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                                <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                                <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                                <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                                <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                                <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                                <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="communication-one">
                                    <!-- Sub Tabs Starts-->
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Add Employee Timesheet</h3>
                                        </div>
                                        <form id="time_sheet_form">
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Employee Name <em class="red-star">*</em></label>
                                                        <select class="form-control property" name="employee_id" id="employee_id">
                                                            <option value="default">Select Employee</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Position <em class="red-star">*</em></label>
                                                        <select class="form-control building" name="position" id="position">
                                                            <option value="default">Select Position</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Email <em class="red-star">*</em></label>
                                                        <input type="text" placeholder="Eg: jholder@gmail.com" class="form-control" id="employeeEmail" name="employeeEmail">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Mobile Number </label>
                                                        <input type="text" placeholder="Eg: 154-175-4301" class="form-control" id="employeePhone" name="mobile_number">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Last Login </label>
                                                        <input type="text" placeholder="Eg: <?php echo date('Y-m-d'); ?>" class="form-control" id="employeeLogin" name="last_login">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Date </label>
                                                        <input type="text" readonly placeholder="Eg: <?php echo date('Y-m-d'); ?>" class="form-control cursor" name="date" id="employeeDate">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Time In </label>
                                                        <input type="text" placeholder="Eg: Time in" class="form-control time_picker" name="time_in" id="time_in_employee">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Time Out for Meal </label>
                                                        <input type="text" placeholder="Eg: Time Out for Meal" class="form-control time_picker" name="time_out_for_meal" id="time_out_for_meal">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Time In after Meal </label>
                                                        <input type="text" placeholder="Eg: Time In after Meal" class="form-control" name="time_in_after_meal" id="time_in_after_meal">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Time Out </label>
                                                        <input type="text" placeholder="Eg: Time Out" class="form-control" name="time_out" id="time_out">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Total Calculated Hours </label>
                                                        <input type="text" placeholder="Eg: Total Calculated Hours" class="form-control" id="total_hours" value="2" name="total_hours">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-4 clear textarea-form">
                                                        <label>Notes </label>
                                                        <div class="notes_date_right_div">
                                                            <textarea placeholder="Note for Time Sheet" name="note" class="form-control capital notes_date_right"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-outer text-right">
                                                    <button type="submit" class="blue-btn">Save</button>
                                                    <button type="button" class="clear-btn email_clear">Clear</button>
                                                    <button type="button" class="grey-btn cancel_timeSheet">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Wrapper Ends -->
    <!-- Jquery Starts -->
    <script>
        $(document).on("click",".clear-btn.email_clear",function () {
            resetFormClear('#time_sheet_form',['time_in','time_out_for_meal','time_in_after_meal','time_out','total_hours'],'form',false);
        });
    </script>

    <script type="text/javascript">
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    </script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/additionalMethods.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/communication/timeSheet.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/timeSheet/timeSheetAjax.js" type="text/javascript"></script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>