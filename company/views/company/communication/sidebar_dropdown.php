<!--- Right Quick Links ---->
<div class="right-links-outer hide-links">
    <div class="right-links">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
        <i class="fa fa-angle-right" aria-hidden="true"></i>
    </div>
    <div id="RightMenu" class="box2">
        <h2>COMMUNICATION</h2>
        <div class="list-group panel">
            <a data-urltype="" href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed compose-mail">Compose New Mail</a>
            <!-- Two Ends-->
            <!-- Three Starts-->
            <a data-urltype="" href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed compose-text">Compose New Text Message</a>
            <!-- Three Ends-->
            <!-- Four Starts-->
            <a class="list-group-item list-group-item-success strong collapsed"  id="composer_group" href="javascript:void(0)">Compose New Group Message</a>
            <!-- Four Ends-->
            <!-- Five Starts-->
            <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed gotoAddNewLetter">Create New Letter</a>
            <!-- Five Ends-->
            <!-- Six Starts-->
            <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed side-nav-new-conversation" >New Conversation</a>
            <!-- Six Ends-->
            <!-- Seven Starts-->
            <a id="" href="/Communication/AddNewTaskAndReminders" class="list-group-item list-group-item-success strong collapsed side-nav-task-remind" >New Task and Reminders</a>
            <!-- Seven Ends-->
            <a id="" href="/Communication/NewInTouch" class="list-group-item list-group-item-success strong collapsed" >In-Touch</a>
            <a id="" href="/Communication/PhoneCall" class="list-group-item list-group-item-success strong collapsed" >Phone Call Log</a>
            <a id="" href="/Communication/TimeSheet" class="list-group-item list-group-item-success strong collapsed" >Employee Timesheet</a>
            <a id="" href="/Communication/Chat" class="list-group-item list-group-item-success strong collapsed" >Staff Chat Room</a>
            <a id="" href="/Communication/DailyVisitor" class="list-group-item list-group-item-success strong collapsed" >Daily Visitor Log</a>
            <a id="" href="/Package/Packages" class="list-group-item list-group-item-success strong collapsed" >Package Tracker</a>
            <a id="" href="/Announcement/Announcements" class="list-group-item list-group-item-success strong collapsed" >Announcement</a>
            <a id="" href="/Communication/WaitingList" class="list-group-item list-group-item-success strong collapsed" >Waiting List</a>
        </div>
    </div>
</div>
<!--- Right Quick Links ---->