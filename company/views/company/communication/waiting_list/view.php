<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication &gt;&gt; <span>Waiting List </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                            <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                            <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                            <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                            <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                            <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                            <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                            <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                            <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                            <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                            <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                            <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                            <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                            <li role="presentation" class="active"><a href="/Communication/WaitingList">Waiting List</a></li>
                            <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>

                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content">
                            <div class="panel-heading">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="receivables">
                                        <div class="property-status">
                                            <div class="row">

                                                <div class="col-sm-12">
                                                    <div class="btn-outer text-right">
                                                        <a href="javascript:;"><input id="waitingBtnPopup" type="button" class="blue-btn" value="Add to Waiting List"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body pad-none">
                                                                    <div class="accordion-grid">
                                                                        <div class="panel-group" id="accordion">
                                                                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <div class="apx-table listinggridDiv">
                                                                                            <div class="table-responsive">
                                                                                                <table id="waiting_list" class="table table-bordered"></table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Regular Rent Ends -->

                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>



                        <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
</div>
<input class="hidden_form" type="hidden" val=""/>
<input class="hidden_form2" type="hidden" val=""/>
<div class="container">
    <div class="modal fade" id="waitingListPopup" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Waiting List</h4>
                </div>
                <div class="modal-body ">
                    <div class="row">
                        <div class="form-outer">
                            <form method="post" id="waiting_form">
                                <div class="col-xs-12">
                                    <div class="col-xs-2" style="width: 0%; padding-right: 9px">
                                        <input  type="checkbox" val="1" id="check"/>  <label>

                                    </div>
                                    <div class="col-sm-5 col-xs-10 pull-left" style="padding-top: 3px">

                                       <label> Use Entity/Company Name</label>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4 clear">
                                            <label>
                                                Preferred </label>

                                            <select class="form-control " type="text" maxlength="20" name="preferred"  id="preferred">
                                                <option value="0">Select</option>
                                                <option value="1stBR">1stBR</option>
                                                <option value="2ndBR">2ndBR</option>
                                                <option value="3rdBR">3rdBR</option>
                                                <option value="4thBR">4thBR</option>
                                                <option value="5thBR">5thBR</option>
                                                <option value="6thBR">6thBR</option>
                                                <option value="Commercial">Commercial</option>
                                                <option value="House">House</option>
                                                <option value="Single/Family">Single/Family</option>
                                                <option value="Multi Family">Multi Family </option>
                                                <option value="Studio">Studio</option>
                                                <option value="Office">Office</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>
                                                Pets </label>
                                            <select class="form-control " type="text" maxlength="20" name="pets"  id="pets">

                                                <option value="0" selected="selected">Select</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>
                                                Property </label>
                                            <select class="form-control property" type="text" maxlength="20" name="property"  ></select>
                                        </div>
                                        <div class="col-sm-4 ">
                                            <label>
                                                Building </label>
                                            <select class="form-control building" type="text" maxlength="20" name="building"  ></select>
                                        </div>
                                        <div class="col-sm-4 ">
                                            <label>
                                                Unit </label>
                                            <select class="form-control unit" type="text" maxlength="20" name="unit" ></select>
                                        </div>
                                        <div class="col-sm-4 company" style="display: none">
                                            <label>
                                                Entity/Company Name </label>
                                            <input class="form-control capital company_name" type="text" maxlength="20" name="company_name" placeholder="Company Name" />
                                            <label id="company-error" class="error" for="company" style="display: none">* This field is required.</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>
                                                First Name <em class="red-star">*</em> </label>
                                            <input class="form-control capital" type="text" maxlength="20" name="first_name" placeholder="First Name" id="first"/>
                                        </div>
                                        <div class="col-sm-4 ">
                                            <label>
                                                Middle Name</label>
                                            <input class="form-control capital middle_name" type="text" maxlength="20" name="middle_name" placeholder="Middle Name" />
                                        </div>
                                        <div class="col-sm-4 ">
                                            <label>
                                                Last Name <em class="red-star">*</em></label>
                                            <input class="form-control capital" type="text" maxlength="20" name="last_name" placeholder="Last Name" id="last"/>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>
                                                Maiden Name </label>
                                            <input class="form-control capital maiden_name" type="text"  name="maiden_name" placeholder="Maiden Name" />
                                        </div>
                                        <div class="col-sm-4 ">
                                            <label>
                                                Nickname </label>
                                            <input class="form-control capital nickname" type="text"  name="nickname" placeholder="Nickname" />
                                        </div>
                                        <div class="col-sm-4 ">
                                            <label>
                                                Phone </label>
                                            <input class="form-control capital phone_format phone" type="text" maxlength="12" name="phone" placeholder=" Phone" />
                                        </div>
                                        <div class="col-sm-4">
                                            <label>
                                                Email </label>
                                            <input class="form-control  email" type="text" name="email" placeholder="Email" />
                                        </div>
                                        <div class="col-sm-4 ">
                                            <label>
                                                Address 1 </label>
                                            <input class="form-control capital  address_1" type="text" maxlength="20" name="address_1" placeholder=" Address 1" />
                                        </div>
                                        <div class="col-sm-4 ">
                                            <label>
                                                Address 2 </label>
                                            <input class="form-control capital address_2" type="text" maxlength="20" name="address_2" placeholder=" Address 2" />
                                        </div>
                                        <div class="col-sm-4">
                                            <label>
                                                Address 3 </label>
                                            <input class="form-control capital address_3" type="text" maxlength="20" name="address_3" placeholder=" Address 3" />
                                        </div>
                                        <div class="col-sm-4 ">
                                            <label>
                                                Address 4</label>
                                            <input class="form-control capital address_4" type="text" maxlength="20" name="address_4" placeholder="Address 4" />
                                        </div>
                                        <div class="col-sm-4">
                                            <label>
                                                Zip / Postal Code </label>
                                            <input class="form-control capital postal_code" id="postal_code_zip" type="text" name="postal_code"/>
                                        </div>
                                        <div class="col-sm-4 ">
                                            <label>
                                                Country </label>
                                            <input class="form-control capital country" type="text" maxlength="20" name="country" placeholder="Country" />
                                        </div>
                                        <div class="col-sm-4 ">
                                            <label>
                                                State / Province </label>
                                            <input class="form-control capital state" type="text" maxlength="20" name="state" placeholder="State" />
                                        </div>
                                        <div class="col-sm-4">
                                            <label>
                                                City </label>
                                            <input class="form-control capital  city" type="text" maxlength="20" name="city" placeholder="City" />
                                        </div>
                                        <div class="col-sm-4">
                                            <label>

                                                Note </label>
                                            <div class="notes_date_right_div"><textarea type="textarea" class="form-control capital note notes_date_right" type="textarea" maxlength="150" name="note" placeholder="Note"></textarea></div>
                                        </div>
                                    </div>


                                </div>
                            </form>
                            <div class="btn-outer text-right">
                                <input type="button" value="Save" class="blue-btn" id="save_list">
                                <button type="button" class="clear-btn email_clear">Clear</button>
                                <input type="button" class="grey-btn cancel_Popup" value="Cancel">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Wrapper Ends -->


<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });

        $("#communication_top").addClass("active");
    });

    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    $(document).on("click",".clear-btn.email_clear",function () {

        resetFormClear('#waiting_form',['postal_code','country','state','city'],'form',false);
    });
</script>

<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/waiting_list/waiting_list.js"></script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
