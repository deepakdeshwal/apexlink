<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication &gt;&gt; <span>Waiting List </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>COMMUNICATION</h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" >Compose New Email</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Compose New Text Message</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed">Compose New Group Message</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="#" id="" class="list-group-item list-group-item-success strong collapsed">Create New Letter</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">Conversation</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >New Task and Reminders</a>
                                <!-- Seven Ends-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >In-Touch</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Phone Call Log</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Employee Timesheet</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Staff Chat Room</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Daily Visitor Log</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Package Tracker</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Announcement</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Waiting List</a>
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Tenant/ViewEditWatingList">Flags</a></li>


                        </ul>

                        <!-- Tab panes -->

                        <div class="flag-container">
                            <div class="form-outer">
                                <div class="form-hdr"><h3>Flag</h3></div>
                                <div class="form-data">
                                    <div class="property-status form-outer2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="btn-outer text-right">
                                                    <a class="blue-btn addbtnflag">New Flag
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- New Flag Form -->
                                    <div class="row">
                                        <form name="flagform" id="flagform" method="post"
                                              style="display: none;">
<!--                                            <input name="flag_tenant_id" type="hidden"-->
<!--                                                   class="hiddenflag tenant_id"-->
<!--                                                   value="--><?php //echo $_GET['id']; ?><!--">-->
                                            <input name="id" type="hidden" value="">
                                            <div class="col-sm-2">
                                                <label>Flagged By</label>
                                                <input type="text" name="flag_by"
                                                       class="capsOn form-control" value="<?php print_r($_SESSION[SESSION_DOMAIN]['name']); ?>">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Date</label>
                                                <input type="text" name="date" class="form-control calander">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Flag Name</label>
                                                <input name="flag_name" class="form-control" placeholder="Please Enter the Name of this Flag"
                                                       type="text">
                                            </div>
                                            <div class="col-sm-2" style="display: none;">
                                                <label>Country Code</label>
                                                <select name="country_code"
                                                        class="form-control" id="guestCountries"></select>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Phone Number</label>
                                                <input name="flag_phone_number"
                                                       class="phone_format form-control" type="text" value='<?php echo $_SESSION[SESSION_DOMAIN]['phone_number'];?>';>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Flag Reason</label>
                                                <input name="flag_reason" class="form-control"
                                                       type="text">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Flagged For</label>
                                                <input class="form-control capital" name="object_name"  id="flagged_for" maxlength="100"  type="text" readonly/>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Completed</label>
                                                <select name="completed" class="form-control">
                                                    <option value="1">Yes</option>
                                                    <option value="0" selected>No</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Note</label>
                                                <div class="notes_date_right_div">
                                                <textarea name="flag_note" class="form-control notes_date_right"></textarea>
                                                   </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="btn-outer">
                                                    <a class="blue-btn saveflag"
                                                       href="javascript:void(0)">Save</a>
                                                    <a class="grey-btn"
                                                       href="javascript:void(0)">Cancel</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- New Flag Form -->

                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">


                                                    <div id="collapseOne"
                                                         class="panel-collapse collapse in"
                                                         aria-expanded="true" style="">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive apx-table">
                                                                    <table class="table table-hover table-dark"
                                                                           id="flaglistingtable"></table>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>


                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
</div>
<!--<input class="hidden_form" type="hidden" val=""/>-->
<!--<input class="hidden_form2" type="hidden" val=""/>-->
<!--<div class="container">-->
<!--    <div class="modal fade" id="waitingListPopup" role="dialog">-->
<!--        <div class="modal-dialog modal-lg">-->
<!--            <div class="modal-content">-->
<!--                <div class="modal-header">-->
<!--                    <button type="button" class="close" data-dismiss="modal">&times;</button>-->
<!--                    <h4 class="modal-title">Waiting List</h4>-->
<!--                </div>-->
<!--                <div class="modal-body pull-left">-->
<!--                    <div class="">-->
<!--                        <div class="form-outer">-->
<!--                            <form method="post" id="waiting_form">-->
<!--                                <div class="">-->
<!--                                    <div class="col-sm-2">-->
<!---->
<!--                                        <input  type="checkbox" val="1" id="check"/>-->
<!--                                    </div>-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-sm-4 clear">-->
<!--                                            <label>-->
<!--                                                Preferred </label>-->
<!---->
<!--                                            <select class="form-control " type="text" maxlength="20" name="preferred"  id="preferred">-->
<!--                                                <option value="0">Select</option>-->
<!--                                                <option value="1stBR">1stBR</option>-->
<!--                                                <option value="2ndBR">2ndBR</option>-->
<!--                                                <option value="3rdBR">3rdBR</option>-->
<!--                                                <option value="4thBR">4thBR</option>-->
<!--                                                <option value="5thBR">5thBR</option>-->
<!--                                                <option value="6thBR">6thBR</option>-->
<!--                                                <option value="Commercial">Commercial</option>-->
<!--                                                <option value="House">House</option>-->
<!--                                                <option value="Single/Family">Single/Family</option>-->
<!--                                                <option value="Multi Family">Multi Family </option>-->
<!--                                                <option value="Studio">Studio</option>-->
<!--                                                <option value="Office">Office</option>-->
<!--                                                <option value="Other">Other</option>-->
<!--                                            </select>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4">-->
<!--                                            <label>-->
<!--                                                Pets </label>-->
<!--                                            <select class="form-control " type="text" maxlength="20" name="pets"  id="pets">-->
<!---->
<!--                                                <option value="0" selected="selected">Select</option>-->
<!--                                                <option value="Yes">Yes</option>-->
<!--                                                <option value="No">No</option>-->
<!--                                                <option value="Other">Other</option>-->
<!--                                            </select>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4">-->
<!--                                            <label>-->
<!--                                                Property </label>-->
<!--                                            <select class="form-control property" type="text" maxlength="20" name="property"  ></select>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 clear">-->
<!--                                            <label>-->
<!--                                                Building </label>-->
<!--                                            <select class="form-control building" type="text" maxlength="20" name="building"  ></select>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 ">-->
<!--                                            <label>-->
<!--                                                Unit </label>-->
<!--                                            <select class="form-control unit" type="text" maxlength="20" name="unit" ></select>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4">-->
<!--                                            <label>-->
<!--                                                First Name </label>-->
<!--                                            <input class="form-control capital first_name" type="text" maxlength="20" name="first_name" placeholder="First Name" />-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 clear">-->
<!--                                            <label>-->
<!--                                                Middle Name</label>-->
<!--                                            <input class="form-control capital middle_name" type="text" maxlength="20" name="middle_name" placeholder="Middle Name" />-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 ">-->
<!--                                            <label>-->
<!--                                                Last Name</label>-->
<!--                                            <input class="form-control capital last_name" type="text" maxlength="20" name="last_name" placeholder="Last Name" />-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4">-->
<!--                                            <label>-->
<!--                                                Maiden Name </label>-->
<!--                                            <input class="form-control capital maiden_name" type="text" maxlength="20" name="maiden_name" placeholder="Maiden Name" />-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 clear">-->
<!--                                            <label>-->
<!--                                                Nickname </label>-->
<!--                                            <input class="form-control capital nickname" type="text" maxlength="20" name="nickname" placeholder="Nickname" />-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 ">-->
<!--                                            <label>-->
<!--                                                Phone </label>-->
<!--                                            <input class="form-control capital phone" type="text" maxlength="20" name="phone" placeholder=" Phone" />-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4">-->
<!--                                            <label>-->
<!--                                                Email </label>-->
<!--                                            <input class="form-control capital email" type="text" maxlength="20" name="email" placeholder="Email" />-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 clear">-->
<!--                                            <label>-->
<!--                                                Address 1 </label>-->
<!--                                            <input class="form-control capital  address_1" type="text" maxlength="20" name="address_1" placeholder=" Address 1" />-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 ">-->
<!--                                            <label>-->
<!--                                                Address 2 </label>-->
<!--                                            <input class="form-control capital address_2" type="text" maxlength="20" name="address_2" placeholder=" Address 2" />-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4">-->
<!--                                            <label>-->
<!--                                                Address 3 </label>-->
<!--                                            <input class="form-control capital address_3" type="text" maxlength="20" name="address_3" placeholder=" Address 3" />-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 clear">-->
<!--                                            <label>-->
<!--                                                Address 4</label>-->
<!--                                            <input class="form-control capital address_4" type="text" maxlength="20" name="address_4" placeholder="Address 4" />-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4">-->
<!--                                            <label>-->
<!--                                                Zip / Postal Code </label>-->
<!--                                            <input class="form-control capital  postal_code" type="text" maxlength="20" name="postal_code" placeholder=" Zip / Postal Code" />-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 ">-->
<!--                                            <label>-->
<!--                                                Country </label>-->
<!--                                            <input class="form-control capital country" type="text" maxlength="20" name="country" placeholder="Country" />-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 clear">-->
<!--                                            <label>-->
<!--                                                State / Province </label>-->
<!--                                            <input class="form-control capital state" type="text" maxlength="20" name="state" placeholder="State" />-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4">-->
<!--                                            <label>-->
<!--                                                City </label>-->
<!--                                            <input class="form-control capital  city" type="text" maxlength="20" name="date" placeholder="City" />-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4">-->
<!--                                            <label>-->
<!--                                                Note </label>-->
<!--                                            <input class="form-control capital note" type="text" maxlength="20" name="note" placeholder="Note" />-->
<!--                                        </div>-->
<!--                                    </div>-->
<!---->
<!---->
<!--                                </div>-->
<!--                            </form>-->
<!--                            <div class="btn-outer">-->
<!--                                <input type="button" value="Save" class="blue-btn" id="save_list">-->
<!--                                <input type="button" class="grey-btn cancel_Popup" value="Cancel">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="modal-footer">-->
<!---->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!-- Wrapper Ends -->


<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });

        $("#communication_top").addClass("active");
    });

    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    $('.calander').datepicker({
        yearRange: '1919:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });   var date = $.datepicker.formatDate(jsDateFomat, new Date());

</script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/waiting_list/waiting_list_flag.js"></script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
