<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication &gt;&gt; <span>Task and Reminders </span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>COMMUNICATION</h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" >Compose New Email</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Compose New Text Message</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed">Compose New Group Message</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="#" id="" class="list-group-item list-group-item-success strong collapsed">Create New Letter</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">Conversation</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >New Task and Reminders</a>
                                <!-- Seven Ends-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >In-Touch</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Phone Call Log</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Employee Timesheet</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Staff Chat Room</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Daily Visitor Log</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Package Tracker</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Announcement</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Waiting List</a>
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                            <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                            <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                            <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                            <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                            <li role="presentation" class="active"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                            <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                            <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                            <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                            <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                            <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                            <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                            <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                            <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                            <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>

                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content">
                            <div class="panel-heading">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="receivables">
                                        <div class="property-status">
                                            <div class="row">

                                                <div class="col-sm-12">
                                                    <div class="btn-outer text-right">
                                                        <a href="/Communication/AddNewTaskAndReminders"><input type="button" class="blue-btn" value="New Task and Reminders"></a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> Tasks and Reminders</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-hover table-dark">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th scope="col">Title</th>
                                                                                    <th scope="col">Property</th>
                                                                                    <th scope="col">Building</th>
                                                                                    <th scope="col">Unit Number</th>
                                                                                    <th scope="col">Status</th>
                                                                                    <th scope="col">Assigned To</th>
                                                                                    <th scope="col">Due Date</th>
                                                                                    <th scope="col">Actions</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>
                                                                                    <td>Bill Farms</td>
                                                                                    <td>Bill B20</td>
                                                                                    <td>600.00</td>
                                                                                    <td>Active</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td><select class="form-control"><option>Select</option></select></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>
                                                                                    <td>Bill Farms</td>
                                                                                    <td>Bill B20</td>
                                                                                    <td>600.00</td>
                                                                                    <td>Active</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td><select class="form-control"><option>Select</option></select></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>
                                                                                    <td>Bill Farms</td>
                                                                                    <td>Bill B20</td>
                                                                                    <td>600.00</td>
                                                                                    <td>Active</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td><select class="form-control"><option>Select</option></select></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>
                                                                                    <td>Bill Farms</td>
                                                                                    <td>Bill B20</td>
                                                                                    <td>600.00</td>
                                                                                    <td>Active</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td><select class="form-control"><option>Select</option></select></td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Regular Rent Ends -->

                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>



                        <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>
            <!--Tabs Ends -->
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

    <!-- Unit selection starts here -->
    <div class="modal fade in" id="add_contact" role="dialog" style="display: none;">
        <div class="modal-dialog modal-lg shortkeys-popup">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Unit</h4>
                </div>
                <div class="modal-body">
                    <form id="add_contact_popup" enctype="multipart/form-data">
                        <div class="panel-body pad-none">
                            <div class="row">
                                <div class="property-status">
                                    <div class="col-sm-12">
                                        <div class="check-outer">
                                            <input type="checkbox"/>
                                            <label>Select All</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <div class="check-outer">
                                            <input type="checkbox"/>
                                            <label>1</label>
                                        </div>
                                    </div>
                                    <div class="btn-outer">
                                        <button type="button" class="blue-btn">Save</button>
                                        <button type="button" class="grey-btn">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form></div>

            </div>
        </div>
    </div>
    <!-- Unit  selection ends here -->


    <!-- Task reminder Popup Starts -->
    <div class="modal fade in" id="add_contactd" role="dialog" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Task And Reminder</h4>
                </div>
                <div class="modal-body">
                    <form id="add_contact_popup" enctype="multipart/form-data">
                        <div class="panel-body pad-none">
                            <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Status</label>
                                    <select class="form-control"><option>Not Assigned</option></select>
                                    <span class="first_nameErr error red-star"></span>
                                </div>
                                <div class="col-sm-3">
                                    <label>Title</label>
                                    <input class="form-control" placeholder="New Task/Reminder" type="text">
                                    <span class="first_nameErr error red-star"></span>
                                </div>
                                <div class="col-sm-3">
                                    <label>Property</label>
                                    <select class="form-control"><option>Achme 5</option></select>
                                    <span class="first_nameErr error red-star"></span>
                                </div>
                                <div class="col-sm-3">
                                    <label>Building</label>
                                    <select class="form-control"><option>Achme 5</option></select>
                                    <span class="first_nameErr error red-star"></span>
                                </div>
                                <div class="col-sm-3">
                                    <label>Unit</label>
                                    <select class="form-control"><option>All Selected</option></select>
                                    <span class="first_nameErr error red-star"></span>
                                </div>
                                <div class="col-sm-3">
                                    <label>Due Date</label>
                                    <input class="form-control" placeholder="Feb 28, 2019 (Thu)" type="text">
                                    <span class="first_nameErr error red-star"></span>
                                </div>
                                <div class="col-sm-3">
                                    <label>Details</label>
                                    <textarea class="form-control"></textarea>
                                    <span class="first_nameErr error red-star"></span>
                                </div>
                                <div class="col-sm-3">
                                    <label>Assigned To</label>
                                    <select class="form-control"><option>Select</option></select>
                                    <span class="first_nameErr error red-star"></span>
                                </div>

                            </div>
                                <div class="btn-outer">
                                    <button type="button" class="blue-btn">Update</button>
                                    <button type="button" class="grey-btn">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form></div>

            </div>
        </div>
    </div>
    <!-- Task reminder Popup Starts -->


<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
        $("#communication_top").addClass("active");
    });



</script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>