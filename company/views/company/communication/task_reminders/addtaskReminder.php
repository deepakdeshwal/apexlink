<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>



    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->


        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Communication >> <span>Task and Reminders</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text" />
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-12">

                    </div>
                    <div class="col-sm-12">
                        <div class="content-section">
                            <div class="main-tabs">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                                    <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                                    <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                                    <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                                    <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                                    <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                                    <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                                    <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                                    <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                                    <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                                    <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                                    <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                                    <li role="presentation" class="active"><a href="/Announcement/Announcements">Announcement</a></li>
                                    <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                                    <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="communication-one">
                                        <!-- Sub Tabs Starts-->
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Task and Reminders</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <label>Title <em class="red-star">*</em></label>
                                                        <input class="form-control" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2 clear">
                                                        <label>Property <em class="red-star">*</em></label>
                                                        <select class="form-control property" name="property" id="property">
                                                            <option value="">Select</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <label>Building <em class="red-star">*</em></label>
                                                        <select class="form-control building" name="building" id="building">
                                                            <option value="">Select</option>
                                                        </select>
                                                        <span class="last_nameErr error red-star"></span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <label>Unit <em class="red-star">*</em></label>
                                                        <select class="form-control"><option>Select</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-4 clear textarea-form">
                                                        <label>Details <em class="red-star">*</em></label>
                                                        <textarea class="form-control"></textarea>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2 clear">
                                                        <label>Status</label>
                                                        <select class="form-control"><option>Select</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <label>Assigned To</label>
                                                        <select class="form-control"><option>Select</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <label>Due Date</label>
                                                        <input class="form-control" type="text"/>
                                                    </div>

                                                </div>

                                                <div class="row form-outer2">
                                                    <div class="col-xs-12 col-sm-4 col-md-12">
                                                        <div class="check-outer">
                                                            <input type="checkbox"/>
                                                            <label>Reccuring Task</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-outer">
                                                    <button onclick="window.location.href='inbox-msg.html'" class="blue-btn">Save</button>
                                                    <button onclick="window.location.href='inbox-msg.html'" class="grey-btn">Cancel</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
    <!-- Wrapper Ends -->
    <!-- Footer Ends -->

    <script>
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });
        });

        $(document).ready(function(){
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
            $("#communication_top").addClass("active");
        });



    </script>
    <script>
        var pagination  = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
        var upload_url  = "<?php echo SITE_URL; ?>";
    </script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/communication/announcement/fileLibrary.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/communication/announcement/addAnnouncement.js"></script>

    <!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>