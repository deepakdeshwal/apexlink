<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->

        <input type="hidden" id="editPackageData" value="<?php echo $_GET['id'];?>">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Package Tracker >> <span>New Package</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text" />
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-12">

                    </div>
                    <div class="col-sm-12">
                        <div class="content-section">
                            <div class="main-tabs">
                                <form id="packageTracker_form">
                                    <div role="tabpanel" class="tab-pane active" id="communication-one">
                                    <!-- Sub Tabs Starts-->
                                    <div class="accordion-form">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="collapsed" aria-expanded="false"> New Package</a>
                                                                <a class="back" href="/Package/Packages"><< Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                                                            <label>Package Owner Type <em class="red-star">*</em></label>
                                                                            <select class="form-control" name="pcktrackList" id="pcktrackList" class="pcktrackList">
                                                                                <option value="">Select Owner Type</option>
                                                                                <option value="1">Tenant</option>
                                                                                <option value="2">Owner</option>
                                                                                <option value="3">Vendor</option>
                                                                                <option value="4">Employee</option>
                                                                                <option value="5">Other</option>
                                                                            </select>
                                                                            <span class="pcktrackListErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                                                            <label>Package Owner Name <em class="red-star">*</em></label>
                                                                            <select class="form-control" name="packageOwnerName" id="packageOwnerName">
                                                                                <option value="">Select</option>
                                                                            </select>
                                                                            <input type="text" class="form-control capital" name="packageAddOwnerName" id="packageAddOwnerName" style="display: none;">
                                                                            <span id="ownerTypeEmailErr" class="ownerTypeEmailErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                                                            <label>Email <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" id="ownerTypeEmail" name="ownerTypeEmail"/>
                                                                            <span class="ownerTypeEmailErr error red-star"></span>
                                                                        </div>

                                                                        <div class="col-xs-12 col-sm-6 col-md-3 countycodediv">
                                                                            <label>Country Code</label>
                                                                            <select class="form-control"
                                                                                    name="country_code"
                                                                                    id="country_code" maxlength="30"></select>
                                                                            <span class="term_planErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                                                            <label>Phone Number</label>
                                                                            <input type="text" name="ownerType_phone_number" class="form-control p-number-format phone_number ownerType_phone_number" maxlength="12">
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                                                            <label>Parcel #</label>
                                                                            <input class="form-control parcelRandomData" name="parcelNumber" id="parcelRandomData" type="text"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                                                            <label>Quantity of Packages</label>
                                                                            <select class="form-control quantityOfPackages" name="quantityOfPackages" id="quantityOfPackages">


                                                                            </select>
                                                                        </div>

                                                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                                                            <label>Date</label>
                                                                            <input class="form-control calander" type="text" id="trackerDatePackage" name="trackerDatePackage"/>
                                                                        </div>

                                                                        <div class="col-xs-12 col-sm-6 col-md-2 apx-inline-popup">
                                                                            <label>Carrier <em class="red-star">*</em>
                                                                                <a class="pop-add-icon propPackageicon" href="javascript:;">
                                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                                </a>
                                                                            </label>
                                                                            <select class="form-control" id="carrierName_id" name="carrierName_id">
                                                                                <option value="">Select</option>
                                                                            </select>
                                                                            <div class="add-popup" id="selectpackageTracker" style="width: 127%;">
                                                                                <h4>Add New Carrier</h4>
                                                                                <div class="add-popup-body">
                                                                                    <div class="form-outer">
                                                                                        <div class="col-sm-12">
                                                                                            <label>Name <em class="red-star">*</em></label>
                                                                                            <input name="carrierName" class="form-control customValidateGroup package_tracker capital" type="text" data_required="true" data_max="150" placeholder="Eg: GEN">
                                                                                            <span class="customError required" aria-required="true" id="package_tracker"></span>
                                                                                        </div>
                                                                                        <div class="btn-outer text-right">
                                                                                            <button type="button" class="blue-btn add_package" data-table="packageTrackerCarrier_table" data-cell="carrierName" data-class="package_tracker" data-name="carrierName_id">Save</button>
                                                                                            <input type="button" class="clear-btn ClearCarrier" value="Clear">
                                                                                            <input type="button" class="grey-btn packageCancelButton" value="Cancel">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                                                            <label>Package Size</label>
                                                                            <input class="form-control capital" id="packageSize" name="packageSize" type="text"/>
                                                                        </div>

                                                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                                                            <label>Received By</label>
                                                                            <select class="form-control" id="receivedByPackage" name="receivedByPackage" value="">
                                                                                <option>Select</option>
                                                                            </select>
                                                                        </div>

                                                                        <div class="col-xs-12 col-sm-6 col-md-2 apx-inline-popup">
                                                                            <label>Location Stored <em class="red-star">*</em>
                                                                                <a class="pop-add-icon propLocationicon" href="javascript:;">
                                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                                </a>
                                                                            </label>
                                                                            <select class="form-control" id="locationName_id" name="locationName_id">
                                                                                <option value="">Select</option>
                                                                            </select>
                                                                            <div class="add-popup" id="selectLocationStore" style="width: 127%;">
                                                                                <h4>Add New Location</h4>
                                                                                <div class="add-popup-body">
                                                                                    <div class="form-outer">
                                                                                        <div class="col-sm-12">
                                                                                            <label>Name <em class="red-star">*</em></label>
                                                                                            <input name="locationName" class="form-control customValidateGroup Location_Store capital" type="text" data_required="true" data_max="150" placeholder="Eg: GEN">
                                                                                            <span class="customError required" aria-required="true" id="Location_Store"></span>
                                                                                        </div>
                                                                                        <div class="btn-outer text-right">
                                                                                            <button type="button" class="blue-btn add_Location" data-table="locationStored_table" data-cell="locationName" data-class="Location_Store" data-name="locationName_id">Save</button>
                                                                                            <input type="button" class="clear-btn Clearlocation" value="Clear">
                                                                                            <input type="button" class="grey-btn locationCancelButton" value="Cancel">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 col-sm-6 col-md-4 clear">
                                                                            <label>Notes</label>
                                                                            <div class="notes_date_right_div">
                                                                                <textarea name="notesPackages" id="notesPackages" class="form-control capital notes_date_right"></textarea>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12 text-right">
                                                                <input type="hidden" value="<?php echo $_GET['edit'];?>" id="packageTrackerUpdate">
                                                            <?php if(isset($_GET['edit']) && !empty($_GET['edit'])){?>
                                                                <input type="button"  class="blue-btn packageData" id="packageData" value="Update">
                                                                <input type="button"  class="blue-btn saveSendEmailText" value="Update & Send Email/SMS">
                                                            <?php }else{?>
                                                                <input type="button"  class="blue-btn packageData" id="packageData" value="Save">
                                                                <input type="button"  class="blue-btn saveSendEmailText" value="Save & Send Email/SMS">

                                                            <?php }?>
                                                            <button type="button" class="clear-btn email_clear">Clear</button>
                                                                <input type="button" class="grey-btn CancelBtnPackage" value="Cancel">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </section>

    </div>
    <!-- Wrapper Ends -->
<!--Model Save & Submit -->
    <div class="container">
        <div class="modal fade" id="myModalPackage" role="dialog">
            <input type="hidden" value="" class="emailPackageData">
            <input type="hidden" value="" class="phonePackageData">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Select Type</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-outer col-sm-12">
                            <form id="packageData_model">

                                <div class="custom_field_form">
                                    <div class="table-responsive panel-collapse pull out" id="tools">
                                                <div class="grid-outer">
                                                <table align="center" class="table packagemodal-table">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center">
                                                            <div class="form-data-range3">
                                                                <input type="checkbox" style="vertical-align: bottom;" name="checkAll" id="CheckProp" spellcheck="true" autocomplete="off" value="all">
                                                            </div>
                                                        </td>
                                                        <td  align="center"></td>
                                                    </tr>
                                                    <tr>
                                                        <td  align="center">
                                                            <div class="form-data-range3">
                                                                <input type="checkbox" style="vertical-align: bottom;" name="phoneNumberTracker" id="CheckMsg" class="chkList" spellcheck="true" autocomplete="off" value="1">
                                                            </div>
                                                        </td>
                                                        <td  align="center">
                                                            <label style="color: #2D3091;font-weight:bold">
                                                                Text Message
                                                            </label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td  align="center">
                                                            <div class="form-data-range3">
                                                                <input type="checkbox" style="vertical-align: bottom;" name="emailSendTracker" id="CheckEmail" class="chkList" spellcheck="true" autocomplete="off" value="2">
                                                            </div>
                                                        </td>
                                                        <td  align="center">
                                                            <label style="color: #2D3091;font-weight:bold">
                                                                Email
                                                            </label>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                    </div>
                                </div>
                                <div class="btn-outer">
                                    <input type="button" class="blue-btn" id='doneEmailTextMessage' value="Done">
                                </div>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End custom field model -->
<!--Model Save & Submit -->

    <!-- Footer Ends -->
    <script type="text/javascript">
        var date_format = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format'] ?>";
        var upload_url = "<?php echo SITE_URL; ?>";
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        var userNamepackage = "<?php echo $_SESSION[SESSION_DOMAIN]['name']; ?>";

    </script>
    <script>
        $("input[name='trackerDatePackage']").datepicker({
            dateFormat: date_format,
            autoclose: true,
            changeMonth: true,
            changeYear: true
        }).datepicker("setDate", new Date());
    </script>
    <script>

        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });

            $(document).on('click','.ClearCarrier',function () {
                $('.package_tracker').val('');
            });

            $(document).on('click','.Clearlocation',function () {
                $('.Location_Store').val('');
            });
            $("#ownerTypeEmail").removeClass('capital');
        });

        $(document).ready(function(){
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
            $("#communication_top").addClass("active");


                $(function(){
                    var $select = $("#communication-one .quantityOfPackages");
                    for (i=1;i<=100;i++){
                        $select.append($('<option></option>').val(i).html(i));
                    }
                });

        });

        /**
         * If the letter is not digit in phone number then don't type anything.
         */
        $(document).on('keypress','.phone_number',function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });

        /**
         * To change the format of phone number
         */
        $(document).on('keydown','.phone_number',function (e) {
            $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
        });



    </script>
    <script>
        $(document).on("click",".clear-btn.email_clear",function () {

            resetFormClear('#packageTracker_form',['parcelNumber','trackerDatePackage','length_of_visit'],'form',false);
        });
    </script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/packageTracker/packageTracker.js"></script>
    <!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>