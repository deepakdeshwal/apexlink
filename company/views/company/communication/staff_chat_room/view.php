<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication &gt;&gt; <span>Staff Chat Room </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                            <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                            <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                            <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                            <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                            <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                            <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                            <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                            <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                            <li role="presentation" class="active"><a href="/Communication/Chat">Staff Chat Room</a></li>
                            <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                            <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                            <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                            <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                            <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>
                        </ul>
                        <!-- Tab panes -->

                        <div class="tab-content tab-pane">

                            <div class="staff-chatroom">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="staff-chat-left">
                                            <div class="staff-chat-hdr">
                                                <h4><?php echo $_SESSION[SESSION_DOMAIN]['name']; ?></h4>
                                                <p><i class="fa fa-check-circle" aria-hidden="true"></i> Online</p>
                                            </div>
                                            <div class="user_listing ">
                                                <div class="user-bar">
                                                    <div class="row">
                                                        <div class="col-sm-1 text-right">
                                                            <div class="status-dot active"></div>
                                                        </div>
                                                        <div class="col-sm-10">
                                                            <div class="user-bar-name">Piyush Auto</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 staff-chat-rt">
                                        <div class="hide_none_select" style="display: none;">
                                            <div class="staff-chat-hdr">
                                                <h3 class="chat_to_name">Piyush Auto</h3>
                                                <p id="online_status"><i class="fa fa-check-circle" aria-hidden="true"></i> Online</p>
                                                <input type="hidden" id="chat_user_id"  value="">
                                                <input type="hidden" id="room_id"  value="">
                                            </div>
                                            <div class="staff-chat-data">
                                            </div>
                                            <div class="staff-chat-input">
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <textarea id="chat_input" style="text-transform: capitalize;" class="form-control" placeholder="Type a message here"></textarea>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <button id="sendMessage" class="enter-btn"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>



                        <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
</div>
<!-- Wrapper Ends -->
<!-- Footer Ends -->
<script>
    var chatName = "<?php echo $_SESSION[SESSION_DOMAIN]['name']; ?>";
    var `chatId` = "<?php echo $_SESSION[SESSION_DOMAIN]['cuser_id']; ?>";
    var chatRole = "<?php echo $_SESSION[SESSION_DOMAIN]['role']; ?>";
    var sendTo = '';
</script>
<script type="text/javascript">
    var dateFormat = function () {
        var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
            timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
            timezoneClip = /[^-+\dA-Z]/g,
            pad = function (val, len) {
                val = String(val);
                len = len || 2;
                while (val.length < len) val = "0" + val;
                return val;
            };

        // Regexes and supporting functions are cached through closure
        return function (date, mask, utc) {
            var dF = dateFormat;

            // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
            if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
                mask = date;
                date = undefined;
            }

            // Passing date through Date applies Date.parse, if necessary
            date = date ? new Date(date) : new Date;
            if (isNaN(date)) throw SyntaxError("invalid date");

            mask = String(dF.masks[mask] || mask || dF.masks["default"]);

            // Allow setting the utc argument via the mask
            if (mask.slice(0, 4) == "UTC:") {
                mask = mask.slice(4);
                utc = true;
            }

            var _ = utc ? "getUTC" : "get",
                d = date[_ + "Date"](),
                D = date[_ + "Day"](),
                m = date[_ + "Month"](),
                y = date[_ + "FullYear"](),
                H = date[_ + "Hours"](),
                M = date[_ + "Minutes"](),
                s = date[_ + "Seconds"](),
                L = date[_ + "Milliseconds"](),
                o = utc ? 0 : date.getTimezoneOffset(),
                flags = {
                    d:    d,
                    dd:   pad(d),
                    ddd:  dF.i18n.dayNames[D],
                    dddd: dF.i18n.dayNames[D + 7],
                    m:    m + 1,
                    mm:   pad(m + 1),
                    mmm:  dF.i18n.monthNames[m],
                    mmmm: dF.i18n.monthNames[m + 12],
                    yy:   String(y).slice(2),
                    yyyy: y,
                    h:    H % 12 || 12,
                    hh:   pad(H % 12 || 12),
                    H:    H,
                    HH:   pad(H),
                    M:    M,
                    MM:   pad(M),
                    s:    s,
                    ss:   pad(s),
                    l:    pad(L, 3),
                    L:    pad(L > 99 ? Math.round(L / 10) : L),
                    t:    H < 12 ? "a"  : "p",
                    tt:   H < 12 ? "am" : "pm",
                    T:    H < 12 ? "A"  : "P",
                    TT:   H < 12 ? "AM" : "PM",
                    Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                    o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                    S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
                };

            return mask.replace(token, function ($0) {
                return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
            });
        };
    }();

    // Some common format strings
    dateFormat.masks = {
        "default":      "ddd mmm dd yyyy HH:MM:ss",
        shortDate:      "m/d/yy",
        mediumDate:     "mmm d, yyyy",
        longDate:       "mmmm d, yyyy",
        fullDate:       "dddd, mmmm d, yyyy",
        shortTime:      "h:MM TT",
        mediumTime:     "h:MM:ss TT",
        longTime:       "h:MM:ss TT Z",
        isoDate:        "yyyy-mm-dd",
        isoTime:        "HH:MM:ss",
        isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
        isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
    };

    // Internationalization strings
    dateFormat.i18n = {
        dayNames: [
            "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
            "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
        ],
        monthNames: [
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
        ]
    };

    // For convenience...
    Date.prototype.format = function (mask, utc) {
        return dateFormat(this, mask, utc);
    };
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/chat/node_modules/socket.io-client/dist/socket.io.js"></script>
<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->

    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
        $("#communication_top").addClass("active");
    });

    var socket = io.connect( 'http://localhost:3000', { query: "user_id="+chatId } );

    socket.on('online_users', function(data) {
        getChatMembers(chatId,data);
    });

    $(document).on('click','.user-bar',function(){
        $('.hide_none_select').show();
        $('.user-bar').removeClass('active-chat');
        var status = $(this).attr('data_status') == 'Offline'? '<i class="fa fa-check-circle" aria-hidden="true" style="color:grey;"></i> Offline' : '<i class="fa fa-check-circle" aria-hidden="true"></i> Online';
        $('#online_status').html(status);
        $(this).addClass('active-chat');
        sendTo = $(this).attr('data_id');
        $('#chatNotifi'+sendTo).hide().html('');
        $('#chat_user_id').val(sendTo);
        $('#chat_input').focus();
        showPrivateChat(chatId,sendTo);
    });

    $(document).on('keydown','#chat_input',function(event){
        var sendTo = $('#chat_user_id').val();
        socket.emit('typing', {
            user_name: chatName,
            sender: chatId,
            receiver: sendTo,
            user_role: chatRole,
            user_image: ''
        });
        if(event.which === 13){
            event.preventDefault();
            socket.emit('stoptyping', {
                user_name: chatName,
                sender: chatId,
                receiver: sendTo,
                user_role: chatRole,
                user_image: ''
            });
            $('#sendMessage').click();
        }
    });

    $(document).on('click','#sendMessage',function(){
        var chatData = $('#chat_input').val();
        chatData = chatData.substr(0,1).toUpperCase()+chatData.substr(1);
        var chatTo = $('#chat_user_id').val();
        var random = $('#room_id').val();
        var sendTo = $('#chat_user_id').val();
        socket.emit('stoptyping', {
            user_name: chatName,
            sender: chatId,
            receiver: sendTo,
            user_role: chatRole,
            user_image: ''
        });
        if(chatData != '') {
            socket.emit('private_chat', {
                user_name: chatName,
                sender: chatId,
                receiver: sendTo,
                user_role: chatRole,
                user_image: '',
                message: chatData
            });

            var date = dateFormat(new Date(), "d mmmm, yyyy h:MM TT");
            var newMsgContent = '<div class="msg-sent"><div class="row"><div class="col-sm-6">'+date+'</div><div class="col-sm-6 text-right">' + chatData + '</div></div></div>';
            $(".staff-chat-data").append(newMsgContent);
            $('.staff-chat-data').animate({scrollTop: $('.staff-chat-data').prop("scrollHeight")}, 500);
            //   Ajax call for saving data
            var objectData = new Object();
            objectData.id = chatId;
            objectData.sendTo = chatTo;
            objectData.message = chatData;
            objectData.type = '1';
            objectData.class = 'ChatAjax';
            objectData.action = 'saveChatHistory';
            saveChatHistory(objectData);
        }
        $('#chat_input').val('');
        $('#chat_input').focus();
        return false;
    });

    socket.on('conversation private post', function(data) {
        //display data.message
        if(data.receiver == chatId) {
            var sender = $('#chat_user_id').val();
            if(data.sender == sender){
                var newMsgContent = '<div class="msg-received"><div class="row"><div class="col-sm-6">'+data.message+'</div><div class="col-sm-6 text-right">29 Aug, 2019 02:36:07 AM</div></div></div>';
                $( ".staff-chat-data" ).append( newMsgContent );
                $('.staff-chat-data').animate({scrollTop: $('.staff-chat-data').prop("scrollHeight")}, 500);
            } else {
                var count = $('#chatNotifi'+data.sender).html() != ''? parseInt($('#chatNotifi'+data.sender).html()):0;
                count++;
                $('#chatNotifi'+data.sender).show().html(count);
            }
        }
    });

    socket.on('typing', function(data) {
        $('#chat_input').attr('placeholder',data.user_name+' is typing ....');
    });

    socket.on('stoptyping', function(data) {
        $('#chat_input').attr('placeholder','');
    });

    function showPrivateChat(user_id,sendTo){
        $.ajax({
            type: 'post',
            url: '/chat-ajax',
            data: {
                class: 'ChatAjax',
                action: 'getChatHistory',
                id:user_id,
                sendTo:sendTo,
                type:'1'},
            success: function (response) {
                var res = JSON.parse(response);
                $('.chat_to_name').html(res.sendToDetail.name);
                $('#chat_user_id').html(res.sendToDetail.id);
                if(res.code == 200) {
                    var html = '';
                    $.each(res.data, function( index, value ) {
                        if(res.userDetail.id == value.sender_id) {
                            html += '<div class="msg-sent">';
                            html += '<div class="row">';
                            html += '<div class="col-sm-6">'+value.date+' '+value.time+'</div>';
                            html += '<div class="col-sm-6 text-right">'+value.message+'</div>';
                            html += '</div>';
                            html += '</div>';
                        } else if(res.userDetail.id == value.recevier_id) {
                            html += '<div class="msg-received">';
                            html += '<div class="row">';
                            html += '<div class="col-sm-6">'+value.message+'</div>';
                            html += '<div class="col-sm-6 text-right">'+value.date+' '+value.time+'</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                    });
                    $('.staff-chat-data').html(html);
                    $('.staff-chat-data').animate({scrollTop: $('.staff-chat-data').prop("scrollHeight")}, 500);
                } else {
                    $('.staff-chat-data').html('');
                }
            },
        });
    }

    function saveChatHistory(objectData){
        $.ajax({
            url: '/chat-ajax',
            type: "POST",
            data: objectData,
            success: function(response) {
                var res = JSON.parse(response);
                if(res.code == 200) {
                }
            }
        });
    }

    function getChatMembers($id,onlineData){
        $.ajax({
            type: 'post',
            url: '/chat-ajax',
            data: {
                class: 'ChatAjax',
                action: 'getChatMembers'},
            success: function (response) {
                var res = JSON.parse(response);
                var html = '';
                $.each(res.data, function( index, value ) {
                    if($id != value.id) {
                        var userData = onlineData.find(x => x.user_id == value.id);
                        var active = (userData && userData.user_id && userData.is_online === true) ? '#79ac29' : 'grey';
                        var status = (userData && userData.user_id && userData.is_online === true) ? 'Online' : 'Offline';
                        html += '<div class="user-bar cursor" data_id="' + value.id + '" data_status="'+status+'">';
                        html += '<div class="row">';
                        html += '<div class="col-sm-1 text-right">';
                        html += '<div class="status-dot active" style="background:'+active+'"></div>';
                        html += '</div>';
                        html += '<div class="col-sm-10">';
                        html += '<div class="user-bar-name">' + value.name + '</div>';
                        html += '</div>';
                        html += '<div id="chatNotifi'+value.id+'" class="col-sm-1 msg-notifi" style="display:none;"></div>';
                        html += '</div>';
                        html += '</div>';
                    }
                });
                $('.user_listing').html(html);
            },
        });
    }
</script>


<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
