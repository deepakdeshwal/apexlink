<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<style>
    .popup-rgt {
        float: left;
        width: 90%;
        border-left: 1px solid #dcdcdc;
        padding: 0 10px;
    }
    .popup-rgt span {
        font-weight: 600;
        padding: 10px 0;
        display: block;
    }
    .popup-rgt p {
        font-weight: normal;
        padding: 10px 0;
        display: block;
        font-size: 15px;
    }
    i#Complete {
        cursor: pointer;
    }
    i#cross {
        cursor: pointer;
    }
    i#delete {
        cursor: pointer;
    }
    i#edit-in-touch {
        cursor: pointer;
    }
</style>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->


        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Communication &gt;&gt; <span>In Touch </span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data">
                        <!--- Right Quick Links ---->
                        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                        <!--- Right Quick Links ---->
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                                <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                                <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                                <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                                <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                                <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                                <li role="presentation" class="active"><a href="/Communication/NewInTouch">In-Touch</a></li>
                                <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                                <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                                <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                                <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                                <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                                <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                                <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                                <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>

                            </ul>

                            <!-- Tab panes -->

                            <div class="tab-content">
                                <div class="panel-heading">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="receivables">
                                            <div class="property-status">
                                                <div class="row">
                                                    <div class="col-sm-2">

                                                    </div>
                                                    <div class="col-sm-10 in-touch-outer">

                                                        <input type="text"  placeholder="Search Text Here" class="form-control in-touch-input searchinput">

                                                            <div class="btn-outer pull-right in-touch-button ">
                                                            <button class="blue-btn searchInTouch">Search</button>
                                                            <a href='/Communication/NewInTouch' class="blue-btn">New In-Touch</a>
                                                            </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table id="in-touch-table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
    </section>
    </div>



    <div class="container">
        <div class="modal fade" id="notesHistoryModal" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">In Touch History

                            <a id="Notesedit" style="color:blue;float: right;margin-right: 10px;cursor:pointer;">Edit</a>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <form name="add_chart_account_form" id="add_chart_account_form_id">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-outer">
                                        <div class="col-sm-12">
                                            <div class="col-sm-4">
                                                <h4 style="text-align:center;margin-top: 25px;">Notes</h4>
                                            </div>
                                            <div class="col-sm-8">

                                                <div class="historyhtml"></div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="modal fade" id="transactionHistoryModal" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="width: 640px;">
                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Transaction
                        </h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" value="" id="historyT_id">
                            <div class="apx-table">
                                <div class="table-responsive">
                                    <table id="in-touch-transaction" class="table table-bordered"></table>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Wrapper Ends -->

    <script>
        var pagination  = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
        var Byname = "<?php echo $_SESSION[SESSION_DOMAIN]['name']; ?>";
        var upload_url  = "<?php echo SITE_URL; ?>";
    </script>


    <script>
        function goBack() {
            window.history.back();
        }

        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });
            $("#communication_top").addClass("active");
        });

        $(document).ready(function(){
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
        });

    </script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/communication/inTouch/ListInTouch.js" type="text/javascript"></script>

    <!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>