<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>


<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/south-street/jquery-ui.css" rel="stylesheet"> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/jquery.signature.css"/>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/jquery.ui.touch-punch.js"></script>
<script defer src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/jquery.signature.js"></script>
<div id="wrapper">
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php"); ?>
    <!-- Top navigation end -->
    <section class="main-content letterNotTabs">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication &gt;&gt; <span>Documents</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                            <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                            <li role="presentation" class="active"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                            <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                            <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                            <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                            <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                            <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                            <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                            <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                            <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                            <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                            <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                            <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                            <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <ul class="nav nav-tabs" role="tablist" id="active_tabs">
                            <li role="presentation" class="active_tab active"><a href="#Lease-Documents" id="" aria-controls="home" role="tab" data-toggle="tab">Lease Documents</a></li>
                            <li role="presentation" class="active_tab"><a href="#Tenant-Documents" id="" aria-controls="home" role="tab" data-toggle="tab">Tenant Documents</a></li>
                            <li role="presentation" class="active_tab"><a href="#LandLord-Documents" id="" aria-controls="home" role="tab" data-toggle="tab">LandLord Documents</a></li>
                            <li role="presentation" class="active_tab esignBtn"><a href="#e-Sign-Documents" id="" aria-controls="home" role="tab" data-toggle="tab">e-Sign Documents</a></li>
                        </ul>

                        <div class="btn-outer text-right mg-btm-20">
                            <button class="blue-btn mg-rt-15" id="create_new_letter">Create New Letter</button>
                        </div>

                        <div id="create_new_letter_div" style="display: none;">
                            <form id="lettersNoticeForm">
                                <input type="hidden" id="template_id" value="">
                                <div class="form-outer">
                                    <div class="form-hdr">
                                        <h3 id="header_text">Upload New Document</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-9">
                                                <div class="col-sm-3">
                                                    <label>Letter Name <em class="red-star">*</em></label>
                                                    <input class="form-control capital" name="first_name" placeholder="Letter Name"  id="first_name" type="text" />
                                                    <span class="first_nameErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Letter Type <em class="red-star">*</em></label>
                                                    <select class="form-control" name="letter_type" id="letter_type">
                                                        <option value="">Select Type</option>
                                                        <option value="4">E-Sign</option>
                                                        <option value="3">Landlord Actions</option>
                                                        <option value="1">Lease Items</option>
                                                        <option value="2">Tenant Information</option>

                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Tags</label>
                                                    <select class="form-control" name="tag_name" id="tag_name">
                                                        <option value="">Select</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Description</label>
                                                    <input class="form-control capital mddlName" name="middle_name" id="middle_name" placeholder="Description" type="text" />
                                                </div>
                                                <div class="col-sm-9">
                                                    <!--                                    <label>Description</label>-->
                                                    <textarea class="form-control summernote" name="body"></textarea>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Select Existing Letter</label>
                                                    <select class="form-control ddlExistingLetter" name="ddlExistingLetter" id="ddlExistingLetter">
                                                        <option value="">Select</option>
                                                    </select>
                                                </div>

                                                <div class="col-sm-3">
                                                    <input type="button" id="restoreOrignalBtn" value="Restore Original" class="blue-btn">
                                                </div>
                                                <div class="col-sm-9">
                                                <div class="btn-outer mg-btm-20 text-right" >
                                                    <input type="hidden" id="saveDoc" name="saveDoc" value="">
                                                    <button class="blue-btn" id="save_new_letter">Save</button>
                                                    <button type="button" class="clear-btn email_clear">Clear</button>
                                                    <a href="javascript:;"><input type="button" class="grey-btn" id="cancel_create_letter" value="Cancel"></a>
                                                </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- Tab panes -->
                        <div class="tab-content">

                            <!-- First Tab Start-->
                            <div role="tabpanel" class="tab-pane active" id="Lease-Documents">

                                <div class="form-outer">
                                    <div class="form-hdr">
                                        <h3>List of Lease Documents</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class=" pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="Lease-Documents-table" class="table table-bordered">

                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-outer searchboxData" id="searchboxData" style="display:none;">
                                <div class="form-hdr">
                                    <h3 class="back-building">Search Property
                                        <a href="javascript:;" class="back-link" style="float:right;"><< Back</a>
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="searchboxPropertyAll">
                                        <input type="hidden" id="searchTemplateId" value="">
                                        <div class="row col-sm-3">
                                            <div class="property-status">
                                                <label>Search Property</label>
                                                <select id="propertyListAll" class="fm-txt form-control">
                                                    <option value="all">Select</option>
                                                </select>
                                            </div>
                                        </div>

                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="apx-table">
                                                                                <div class="table-responsive">
                                                                                    <table id="letterNoticesProperty-table" class="table table-bordered">

                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="btn-outer text-right mg-btm-20">
                                            <button class="blue-btn mg-rt-15 sendMailLetter" id="sendMailLetter">Send Email</button>
                                        </div>

                                    </div>


                                </div>
                            </div>
                            <!-- First Tab Ends-->

                            <!-- Second Tab Start-->
                            <div role="tabpanel" class="tab-pane" id="Tenant-Documents">

                                <div class="form-outer">
                                    <div class="form-hdr">
                                        <h3>List of Tenant Documents</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="Tenant-Documents-table" class="table table-bordered">

                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-outer searchboxData1" id="searchboxData1" style="display:none;">
                                <div class="form-hdr">
                                    <h3 class="back-building">Search Property
                                        <a href="javascript:;" class="back-link-tenantF" style="float:right;"><< Back</a>
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="searchboxPropertyAll">
                                        <input type="hidden" id="searchTemplateId" value="">
                                        <div class="row col-sm-3">
                                            <div class="property-status">
                                                <label>Search Property</label>
                                                <select id="propertyListAll1" class="fm-txt form-control">
                                                    <option value="all">Select</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="letterNoticesPropertyTenant-table" class="table table-bordered">

                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-outer text-right mg-btm-20">
                                            <button class="blue-btn mg-rt-15" id="sendMailLetter1">Send Email</button>
                                        </div>

                                    </div>


                                </div>
                            </div>
                            <!-- Second Tab Ends-->

                            <!-- Third Tab Start-->
                            <div role="tabpanel" class="tab-pane" id="LandLord-Documents">
                                <div class="form-outer">
                                    <div class="form-hdr">
                                        <h3>LandLord Documents</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="LandLord-Documents-table" class="table table-bordered">

                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-outer outer_ownerProperty_search" id="searchboxDataOwner" style="display:none;">
                                <div class="form-hdr">
                                    <h3 class="back-building">Search Property
                                        <a href="javascript:;" class="back-link" style="float:right;"><< Back</a>
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="searchbox_ownerPropertyAll">
                                        <input type="hidden" id="searchTemplateIdOwner" value="">
                                        <div class="row col-sm-12">
                                            <div class="col-sm-3">
                                                <div class="outer_ownerProperty_search">
                                                    <label>Select Owner</label>
                                                    <select id="owner_listing_search" name="owner_listing_search" class="form-control common_ddl">
                                                        <option value="all">Select</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class="property-status">
                                                    <label>Search Property</label>
                                                    <select id="ownerPropertyListAll" class="fm-txt form-control common_ddl">
                                                        <option value="all">Select</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="letterNoticesOwner-table" class="table table-bordered">

                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-outer text-right mg-btm-20">
                                            <button class="blue-btn mg-rt-15" id="sendMailLetterLandLord">Send Email</button>
                                        </div>

                                    </div>


                                </div>
                            </div>
                            <!-- Third Tab Ends-->

                            <!-- Forth Tab Start-->
                            <div role="tabpanel" class="tab-pane" id="e-Sign-Documents">
                                <div class="form-outer">
                                    <div class="form-hdr">
                                        <h3>E-Sign Documents</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="e-Sign-Documents-table" class="table table-bordered">

                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-outer searchboxDataEsign" id="searchboxDataEsign" style="display:none;">
                                <div class="form-hdr">
                                    <h3 class="back-building">Search Property
                                        <a href="javascript:;" class="back-link-tenant" style="float:right;"><< Back</a>
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="searchboxPropertyAll">
                                        <input type="hidden" id="searchTemplateIdEsign" value="">
                                        <div class="row col-sm-3">
                                            <div class="property-status">
                                                <label>Search Property</label>
                                                <select id="propertyListAllEsign" class="fm-txt form-control">
                                                    <option value="all">Select</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="letterNoticesTenantEsign-table" class="table table-bordered">

                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-outer text-right mg-btm-20">
                                            <button class="blue-btn mg-rt-15" id="sendMailLetterEsignDoc">Send Email</button>
                                        </div>

                                    </div>


                                </div>
                            </div>
                            <!--Guest Card Start-->
                            <div class="form-outer searchboxDataGuest" id="searchboxDataGuest" style="display:none;">
                                <div class="form-hdr">
                                    <h3 class="back-building">Search Property
                                        <a href="javascript:;" class="back-link-guest" style="float:right;"><< Back</a>
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="searchboxPropertyAll">
                                        <input type="hidden" id="searchTemplateId" value="">
                                        <div class="row col-sm-3">
                                            <div class="property-status">
                                                <label>Search Property</label>
                                                <select id="propertyListAll2" class="fm-txt form-control">
                                                    <option value="all">Select</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="letterNoticesGuest-table" class="table table-bordered">

                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-outer text-right mg-btm-20">
                                            <button class="blue-btn mg-rt-15 sendMailLetter" id="sendMailLetterGuest">Send Email</button>
                                        </div>

                                    </div>


                                </div>
                            </div>
                            <!--Guest Card Start End-->
                            <!--Rental Application Start-->
                            <div class="form-outer searchboxDataRental" id="searchboxDataRental" style="display:none;">
                                <div class="form-hdr">
                                    <h3 class="back-building">Search Property
                                        <a href="javascript:;" class="back-link-rental" style="float:right;"><< Back</a>
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="searchboxPropertyAll">
                                        <input type="hidden" id="searchTemplateId" value="">
                                        <div class="row col-sm-3">
                                            <div class="property-status">
                                                <label>Search Property</label>
                                                <select id="propertyListAll3" class="fm-txt form-control">
                                                    <option value="all">Select</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="letterNoticesRental-table" class="table table-bordered">

                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-outer text-right mg-btm-20">
                                            <button class="blue-btn mg-rt-15" id="sendMailLetter">Send Email</button>
                                        </div>

                                    </div>


                                </div>
                            </div>
                            <!--Rental Application End-->

                            <div class="form-outer outer_ownerProperty_search" id="searchboxDataOwnerNew" style="display:none;">
                                <div class="form-hdr">
                                    <h3 class="back-building">Search Property
                                        <a href="javascript:;" class="back-link" style="float:right;"><< Back</a>
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="searchbox_ownerPropertyAll">
                                        <input type="hidden" id="searchTemplateIdOwner" value="">
                                        <div class="row col-sm-12">
                                            <div class="col-sm-3">
                                                <div class="outer_ownerProperty_search">
                                                    <label>Select Owner</label>
                                                    <select id="owner_listing_search" name="owner_listing_search" class="form-control common_ddl">
                                                        <option value="all">Select</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class="property-status">
                                                    <label>Search Property</label>
                                                    <select id="ownerPropertyListAll" class="fm-txt form-control common_ddl">
                                                        <option value="all">Select</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="letterNoticesOwnerNew-table" class="table table-bordered">

                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-outer text-right mg-btm-20">
                                            <button class="blue-btn mg-rt-15" id="sendMailLetterLandLord">Send Email</button>
                                        </div>

                                    </div>


                                </div>
                            </div>
                            <!-- Forth Tab Ends-->

                        </div>
                    </div>
                </div>
                <!--Tabs Ends -->
                 <div id="convasData" style="display: block;"></div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<div class="loader">
    <img src="<?php echo COMPANY_SITE_URL ?>/images/loader.gif">
</div>
 <div class="container">
    <div class="modal fade" id="eSignModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Signature Type</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="radio" name="typing" value="t">Typing
                                <input type="radio" name="typing" value="s" checked>Sketching
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-7 sketchingarea">
                                <div id="defaultSignature" style="overflow-x: scroll;"></div>
                                <textarea id="signatureJSON" style="display: none;"></textarea>
                                <div id="redrawSignature" style="display: none;"></div>
                                <a style="margin: 6px 15px auto auto; background: #7bbb15;" href="javascript:void(0);" class='blue-btn pull-right clearSignature'>Clear</a>
                            </div>
                            <div class="col-sm-12 typingarea" style="display: none;">
                                <div class="col-sm-12">
                                    <div class="col-sm-6">
                                        <input data-font="" type="text" name="typingtext" placeholder="Enter your name" class="form-control typingtext">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="enterplaceholder">Please enter your name here</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12 fontlisting">
                                    <?php
                                    $arrayFont = ['inherit','Arial','Times+New+Roman', 'Verdana','Action Man', 'Bauer', 'Bubble','Piedra', 'Questrial', 'Ribeye'];
                                    foreach ($arrayFont as $fonts){
                                        echo "<div style='min-height:0px;font-weight:bold;font-family: ".$fonts."' class='col-sm-12'><input type='radio' name='selectfonts' value='".$fonts."'>Guest User</div>";
                                    }
                                    ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 submitsign" style="min-height: 0px;">
                                <a class="blue-btn pull-right" data-user="" data-type="" data-parent="">Submit</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="font-weight: bold; color: #fff; background: #666; text-align: center; word-spacing: 4px; padding: 10px;">
                    Please select atleast one type and submit it!
                </div>
            </div>
        </div>
    </div>
</div>
<!--Modal E-sign popup Start-->
<!--chart filter Start-->
<div class="modal fade" id="eSignFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-md esignfilter-modal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                    e-Signature document to
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-outer form-outer2">
                    <form method="post" id="comp_unitSpotChart">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3 clear" id="eSignDivData">
                                <input type="hidden" value="" id="eSignId">
                                <!--<select class="form-control " type="text" maxlength="20" name="portfolio_List"  id="portfolio_List"></select>-->
                                <select id="ddlRecipeint" class="form-control">
                                    <option value="">Select</option>
                                    <option value="Tenant">Tenants</option>
                                    <option value="Owner">Owners/Landlords</option>
                                    <option value="Rental">Rental Applicants</option>
                                    <option value="GuestCard">Guest Cards</option>
                                </select>
                            </div>
                        </div>
                    </form>
                    <div class="btn-outer text-center">
                        <input type="button" value="Done" class="blue-btn" id="DoneButtn">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <p>Please click the close button(X) on the top right to continue!</p>
            </div>
        </div>
    </div>
</div>
<!--chart filter End-->
<!--Modal E-sign popup End-->
<!-- Footer Ends -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<script>
    $(document).on("click",".clear-btn.email_clear",function () {
        $(".note-editable").text('');

          resetFormClear('#lettersNoticeForm',[''],'form',false);
    });
</script>
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    

    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
  
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
        $("#communication_top").addClass("active");



    });
</script>
<div class="modal fade" id="imagemodal"  aria-hidden="false" tabindex="-1" style="display: none;">
    <div class="modal-dialog">  <div class="modal-content">    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
                <h4 class="modal-title">Insert Image</h4>
    </div>
    <div class="modal-body"><div class="form-group note-form-group note-group-select-from-files">
        <label class="note-form-label">Select from files</label>
            <button id="upload-button">Select PDF</button>
            <input type="file" id="file-to-upload" accept="application/pdf" />
            <canvas id="pdf-canvas" width="400"></canvas>
        </div>
    </div>
</div>

<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/communication/letterNotices/e-signPdf.js"></script>

<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/communication/letterNotices/createNewLetter.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/communication/letterNotices/createNewLetterTenant.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/communication/letterNotices/createNewletterLandlord.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/communication/letterNotices/createEsignDoc.js"></script>


