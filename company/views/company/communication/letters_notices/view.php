<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication &gt;&gt; <span>Documents  </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                            <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                            <li role="presentation" class="active"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                            <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                            <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                            <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                            <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                            <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                            <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                            <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                            <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                            <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                            <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                            <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                            <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>

                        </ul>

                        <!-- Tab panes -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="changeTab active"><a href="#Lease-Documents" id="" aria-controls="home" role="tab" data-toggle="tab">Lease Documents</a></li>
                            <li role="presentation" class="changeTab"><a href="#Tenant-Documents" id="" aria-controls="home" role="tab" data-toggle="tab">Tenant Documents</a></li>
                            <li role="presentation" class="changeTab"><a href="#LandLord-Documents" id="" aria-controls="home" role="tab" data-toggle="tab">LandLord Documents</a></li>
                            <li role="presentation" class="changeTab"><a href="#e-Sign-Documents" id="" aria-controls="home" role="tab" data-toggle="tab">e-Sign Documents</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div role="tabpanel" class="tab-pane active" id="Lease-Documents dff">

                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>List of Lease Documents</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="apx-table">
                                                                                <div class="table-responsive">
                                                                                    <table id="Lease-Documents-table" class="table table-bordered">

                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends-->
                            </div>
                            <!-- First Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="Tenant-Documents">

                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>List of Tenant Documents</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">

                                            </div>
                                        </div>

                                    </div>
                            </div>
                            <!-- Second Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="LandLord-Documents">
                                <div class="form-outer">

                                        <div class="form-hdr">
                                            <h3>List of LandLord Documents</h3>
                                        </div>
                                        <div class="form-data">

                                        </div>

                                </div>
                            </div>
                            <!-- Third Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="e-Sign-Documents">

                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>List of E-Sign Documents</h3>
                                        </div>
                                        <div class="form-data">

                                        </div>
                                    <!-- Form Outer Ends-->
                                    </div>
                            <!-- Forth Tab Ends-->
                            </div>



                        <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
</div>
<!-- Wrapper Ends -->
var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/letterNotices/createNewLetter.js"></script>

<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
        $("#communication_top").addClass("active");
    });



</script>

<!-- Jquery Starts -->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
