<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/images/favicon.ico" alt="apex-icon" type="image/ico" sizes="16x16">

    <title>Apexlink</title>
    <!-- Bootstrap -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/ui.jqgrid.min.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery.timepicker.min.css">

    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/passwordscheck.css" />
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL;?>/css/calculator.css"/>
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/owl.theme.default.min.css"/>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.multiselect.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/main.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery.signature.css"/>
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery.fontselect.css"/>


    <!-- include libraries(jQuery, bootstrap) -->
    <!-- include summernote css/js -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/summernote.js" defer></script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-ui.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.timepicker.min.js"></script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.jqgrid.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.responsivetabs.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.validate.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/common.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/toastr.min.js"></script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootbox.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.mask.min.js"></script>
    <script defer src="<?php echo SUPERADMIN_SITE_URL; ?>/js/calculator.js"></script>
    <script defer src="<?php echo COMPANY_SITE_URL; ?>/js/owl.carousel.js"></script>
    <script defer src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/jquery.signature.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/core-js/2.1.4/core.min.js"></script>


</head>
<body>
<div class="container">
    <div class="modal fade" id="eSignModalLetter" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Signature Type</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <div class="row">
                            <div class="col-sm-12">
                                <input class="typingData" type="radio" name="typing" value="t">Typing
                                <input class="typingSketching" type="radio" name="typing" value="s" checked>Sketching
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-7 sketchingarea">
                                <div id="defaultSignature" style="overflow-x: scroll;"></div>
                                <textarea id="signatureJSON" style="display: none;"></textarea>
                                <div id="redrawSignature" style="display: none;"></div>
                                <a style="margin: 6px 15px auto auto; background: #7bbb15;" href="javascript:void(0);" class='blue-btn pull-right clearSignature'>Clear</a>
                            </div>
                            <div class="col-sm-12 typingarea" style="display: none;">
                                <div class="col-sm-12">
                                    <div class="col-sm-6">
                                        <input data-font="" type="text" name="typingtext" placeholder="Enter your name" class="form-control typingtext">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="enterplaceholder">Please enter your name here</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12 fontlisting">
                                        <?php
                                        $arrayFont = ['inherit','Arial','Times+New+Roman', 'Verdana','Action Man', 'Bauer', 'Bubble','Piedra', 'Questrial', 'Ribeye'];
                                        foreach ($arrayFont as $fonts){
                                            echo "<div style='min-height:0px;font-weight:bold;font-family: ".$fonts."' class='col-sm-12'><input type='radio' name='selectfonts' value='".$fonts."'>Guest User</div>";
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 submitsign" style="min-height: 0px;">
                                <a class="blue-btn pull-right" data-user="" data-type="" data-parent="">Submit</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="font-weight: bold; color: #fff; background: #666; text-align: center; word-spacing: 4px; padding: 10px;">
                    Please select atleast one type and submit it!
                </div>
            </div>
        </div>
    </div>
</div>

<!--<input type="hidden" value="<?php /*echo $_GET['id'] */?>" class="leaseid">-->
<div class="header" style="max-height: 70px; margin-bottom: 20px;width: 100%;background: #d6f0fe;float: left;padding: 1px 0;min-width: 1024px;max-width: 100%;">
    <div class="header-inner">
        <div class="logo">
            <a class="navbar-brand" href="">
                <img src="http://<?php echo $_SERVER['HTTP_HOST'] ?>/company/images/logo.png" alt="Company Logo" width="143" height="49">
            </a>
        </div>
        <div class="hdr-rt">
        </div>
    </div>
</div>
<input type="hidden" id="currUserData" value="<?php echo $_GET['CuserId'];?>">
<input type="hidden" id="userType_id" value="<?php echo $_GET['userId'];?>">
<input type="hidden" id="currUserType" value="">
<div style="float: right;margin-right: 138px;margin-bottom: 10px;">
    <input type="button" id="btnDownload" value="Download" style="margin-right:10px;" class="blue-btn">
    <input type="button" id="btnSaveTemplate" value="Submit" class="blue-btn">
</div>
<div class="" style="float: left; width: 100%;" id="dv-Main-pdf">
    <div class="" id="dvPdfToHtml" style="min-height:500px;overflow-y:scroll;float:left;width:80%; padding:2%;border:1px solid #ddd;margin-left: 8%;">
        <meta charset="utf-8">
        <title></title>
        <div width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#d6f0fd" style="border-radius: 6px 6px 0px 0px; -moz-border-radius: 6px 6px 0px 0px; -webkit-border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2D3091; margin: auto;" align="center">
            <table style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse; padding: 0; margin: 0;">
                <tbody>
                <tr style="background-color: #00b0f0; height: 20px;"><td></td></tr>
                <tr style="border-collapse: collapse; background-color: #fff;">
                    <td class="w580" style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse: collapse;" width="580">
                        <center><img class="COMPANYLOGO" src="http://<?php echo $_SERVER['HTTP_HOST'] ?>/company/images/logo.png" width="150" height="50"></center>
                    </td>
                </tr>
                <tr style="background-color: #00b0f0; height: 20px;"><td></td></tr>
                </tbody>
            </table>
        </div>

        <div width="640" border="0" cellpadding="0" cellspacing="0" align="center">
            <div>
                <div width="528" valign="top" style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; font-size: 13px; padding: 0; line-height: 19px; margin: 0; border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 10px 0;">
                    <p style="margin: 5px 0; text-align: left;">&nbsp;</p>
                        <input type="hidden" class="templateEmailData" data-id="<?php echo $_GET['templateId'];?>">
                        <input type="hidden" class="templateUserId" data-id="<?php echo $_GET['userId'];?>">

                    <div style="width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px">
                        <p style="text-align: left;">&nbsp;</p>
                        <p style="text-align: left;">&nbsp;</p>
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <div id="eSignDataTemplate" class="eSigntempData">

                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#585858" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px;
            -webkit-font-smoothing: antialiased; background-color: #585858; color: #fff;clear: both;" align="center">
            <table style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;
                border-collapse: collapse; padding: 0; margin: 0;">
                <tbody>
                <tr>
                    <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;" align="center">
                        ApexLink Property Manager● <a href="javascript:;" style="color: #fff; text-decoration: none;">support@apexlink.com &nbsp; ●772-212-1950
                        </a></td>
                </tr>
                <tr><td style="line-height: 5px" id="Td2" valign="middle" align="center">&nbsp;</td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.8/highlight.min.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/jquery.fontselect.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/jquery.ui.touch-punch.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/communication/letterNotices/emailSign.js"></script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
