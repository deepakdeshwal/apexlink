<?php
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
$user_type = explode("/",$_SERVER['REQUEST_URI']);
if(isset($user_type[1]) && $user_type[1] == 'Owner')
{
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
    echo '<div id="wrapper">';
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_top_navigation.php");
    $_SESSION[SESSION_DOMAIN]['current_user_type'] = 'Owner_Portal' ;
    $button_url = '/Owner';

}elseif(isset($user_type[1]) && $user_type[1] == 'Communication'){

    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
    echo '<div id="wrapper">';
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    $_SESSION[SESSION_DOMAIN]['current_user_type'] = 'Communication_Portal' ;
    $button_url = '';

}elseif(isset($user_type[1]) && $user_type[1] == 'Vendor'){
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
    echo '<div id="wrapper">';
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    $_SESSION[SESSION_DOMAIN]['current_user_type'] = 'Vendor_Portal' ;
    $button_url = '/Vendor';
}
elseif(isset($user_type[1]) && $user_type[1] == 'Tenant'){
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
    echo '<div id="wrapper">';
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    $_SESSION[SESSION_DOMAIN]['current_user_type'] = 'Tenant_Portal' ;
    $button_url = '/Tenant';

}else{
}

?>
<section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication &gt;&gt; <span>Drafts Group Message/Email </span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                            <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                            <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                            <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                            <li role="presentation" class="active"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                                <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                            <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                            <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                            <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                            <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                            <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                            <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                            <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                            <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                            <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>

                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content">
                            <div class="panel-heading">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="receivables">
                                        <div class="property-status">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="content-section">
                                                        <div class="main-tabs">
                                                            <div role="tabpanel" class="tab-pane active" id="communication-one">
                                                                <!-- Sub Tabs Starts-->
                                                                <div class="property-status">
                                                                    <div class="row">
                                                                        <div class="col-sm-12 text-right" style="padding-bottom: 10px;
    float: left;
    width: 100%;
    margin-bottom: 17px;">
                                                                            <a class="blue-btn"  id="composer_group" href="javascript:void(0)">Compose New Mass Message</a>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="btn-outer text-right">
                                                                                <a class="drafted_mail blue-btn" href="javascript:void(0)">Drafted Group Emails</a>
                                                                                <a class="drafted_message blue-btn" href="javascript:void(0)">Drafted Group Text Messages</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-outer" >
                                                                    <div class="form-hdr">
                                                                        <h3>Drafts</h3>
                                                                    </div>
                                                                    <div class="form-data">
                                                                        <div class="col-sm-12">
                                                                            <div class="grid-outer">
                                                                                <div class="table-responsive">
                                                                                    <div class="apx-table ">
                                                                                        <div class="email_tab table-responsive">
                                                                                            <table id="communication-draft-table" class="table table-bordered"></table>

                                                                                        </div>
                                                                                        <div class="text_tab table-responsive">
                                                                                        <table id="communication-draft-table-mesg" class="table table-bordered"></table>
                                                                                    </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Regular Rent Ends -->

                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>



                        <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
</div>
<!-- Wrapper Ends -->


<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
</script>

<!-- Wrapper Ends -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/email/common.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/groupMessages/draft-group-message.js"></script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>


</body>

</html>
