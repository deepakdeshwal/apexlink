<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                           <!-- <div class="breadcrumb-outer">
                                Tenant Module &gt;&gt; <span>Tenant Listing</span>
                            </div>-->
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">

                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                       <!--Right Navigation Link Starts-->
                        <div class="right-links-outer hide-links">
                            <div class="right-links">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </div>
                            <div id="RightMenu" class="box2">
                                <h2>PEOPLE</h2>
                                <div class="list-group panel">
                                    <a href="/Tenantlisting/add" class="list-group-item list-group-item-success strong collapsed">New Tenant</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed" id="moveout_Search">Move Out</a>
                                    <a id ="shorttermrentals" href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Book Now</a>
                                    <a href="/People/AddOwners" class="list-group-item list-group-item-success strong collapsed">New Owner</a>
                                    <a href="/Vendor/AddVendor" class="list-group-item list-group-item-success strong collapsed">New Vendor</a>
                                    <a href="/People/AddEmployee" class="list-group-item list-group-item-success strong collapsed">New Employee</a>
                                    <a href="/People/AddContact" class="list-group-item list-group-item-success strong collapsed">New Contact</a>
                                    <a href="/Tenant/Receivebatchpayment" class="list-group-item list-group-item-success strong collapsed">Recieve Payment</a>
                                    <a href="/Tenant/TenantStatements" class="list-group-item list-group-item-success strong collapsed">Send Tenant Statements</a>
                                    <a href="/Tenant/BatchRegister" class="list-group-item list-group-item-success strong collapsed">Bank Deposite</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Bill</a>
                                    <a href="/Tenant/InstrumentRegister" class="list-group-item list-group-item-success strong collapsed">Tenant Instrument Register</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Vendor Instrument Register</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed" id="transfer_search">Tenant Transfer</a>
                                    <a href="/Tenant/Transfer" class="list-group-item list-group-item-success strong collapsed">All Tenant Transfer</a>
                                    <a href="/Lease/Movein" class="list-group-item list-group-item-success strong collapsed">All Move In</a>
                                    <a href="/Tenant/MoveOut" class="list-group-item list-group-item-success strong collapsed">All Move Out</a>
                                    <a href="/Tenant/FormarTenant" class="list-group-item list-group-item-success strong collapsed">Former Tenants</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Owners</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Vendors</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">NSF Listing</a>
                                </div>
                            </div>
                        </div>
                        <!--Right Navigation Link Ends-->

                        <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="regular-rent">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h4 class="panel-title">
                                                <a data-toggle="">
                                                    <span>Move Out</span>
                                                </a>
                                            </h4>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                <div class="col-sm-2 moveOut_type_status">
                                                    <label>Status</label>
                                                    <select class="fm-txt form-control"  id="jqgridOptions">
                                                        <option value="All">Select</option>
                                                        <option value="1">Notice Given</option>
                                                        <option value="2">MoveOut Recorded</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="grid-outer">
                                                <div class="table-responsive">
                                                    <div class="grid-outer">
                                                        <div class="apx-table">
                                                            <table class="table table-bordered" id="MoveOuttenant_listing">
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->
</div>
<!--Model For Move Search Start-->
<div class="container">
    <div class="modal fade" id="moveout_listSearch1" role="dialog">
        <div class="modal-dialog modal-lg unit-new-renovation-form">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Charges</h4>
                </div>

                <div class="modal-body">
                    <div class="form-outer col-sm-12">
                        <form id="new_chargeCode_form">
                            <!-- <input type ="hidden" id="chargeCode_userId" name="chargeCode_userId" value="<?php /*echo $_REQUEST["id"]*/?>"  />-->
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <label>Tenant</label>
                                    <input class="form-control" type="text" id="myInput"  placeholder="Click here or start typing the name of the tenant you want to move-out" title="Type in a name">

                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="grid-outer">
                                            <div class="table-responsive" id="tenantData_search">

                                                <!--<table class="table table-hover table-dark" id="myTable">
                                                    <thead>
                                                        <tr class="header">
                                                            <th>Tenant Name</th>
                                                            <th>Phone</th>
                                                            <th>Email</th>
                                                            <th>propertyName</th>
                                                            <th>Unit</th>
                                                            <th>Rent()</th>
                                                            <th>Balance()</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Model For Move Search Start-->
<!--Model for Tenant Transfer start-->
<div class="container">
    <div class="modal fade" id="transfer_listSearch1" role="dialog">
        <div class="modal-dialog modal-lg unit-new-renovation-form">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Tenant Transfer</h4>
                </div>

                <div class="modal-body">
                    <div class="form-outer col-sm-12">
                        <form id="new_chargeCodeTransfer_form">
                            <!-- <input type ="hidden" id="chargeCode_userId" name="chargeCode_userId" value="<?php /*echo $_REQUEST["id"]*/?>"  />-->
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <label>Tenant</label>
                                    <input class="form-control" type="text" id="myInputTransfer"  placeholder="Click here or start typing the tenant's name" title="Type in a name">

                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="grid-outer">
                                            <div class="table-responsive" id="transferData_search"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Model for Tenant Transfer End-->

<script>
var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    $(document).ready(function(){


        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });

        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });

        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script>
    $('#people_top').addClass('active');
    $("#moveout_Search").click(function(){
        $('#moveout_listSearch1').modal('show');
    });
	/*Tenant transfer*/
	$("#transfer_search").click(function(){
        $('#transfer_listSearch1').modal('show');
    });
</script>

<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantListing.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/moveOuttenantListing.js"></script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->