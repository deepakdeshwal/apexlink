<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">

    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

<head>
    <link rel="stylesheet" href="<?php echo COMPANY_SITE_URL; ?>/library/Scheduler/css/TimeSheet.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/jquery-pseudo-ripple.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/jquery-nao-calendar.css">
    <style>

        /*html, body {*/
        /*    margin: 0;*/
        /*    padding: 0;*/
        /*    font-family: Arial, Helvetica Neue, Helvetica, sans-serif;*/
        /*    font-size: 14px;*/
        /*}*/

        /*#calendar {*/
        /*    max-width: 900px;*/
        /*    margin: 40px auto;*/
        /*}*/



    </style>
</head>
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                               People >> Tenants &gt;&gt; <span>Short Term Renters</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">

                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="javascript:;" aria-controls="home" role="tab" data-toggle="tab">Tenants</a></li>
                            <li role="presentation"><a href="/People/Ownerlisting">Owners</a></li>
                            <li role="presentation"><a href="/Vendor/Vendor" >Vendors</a></li>
                            <li role="presentation"><a href="/People/ContactListt">Contacts</a></li>
                            <li role="presentation"><a href="/People/GetEmployeeList">Employee</a></li>
                        </ul>

                        <!--Right Navigation Link Starts-->
                        <div class="right-links-outer hide-links">
                            <div class="right-links">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </div>
                            <div id="RightMenu" class="box2">
                                <h2>PEOPLE</h2>
                                <div class="list-group panel">
                                    <a href="/Tenantlisting/add" class="list-group-item list-group-item-success strong collapsed">New Tenant</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed" id="moveout_Search">Move Out</a>
                                    <a id ="shorttermrentals" href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Book Now</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Owner</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Vendor</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Employee</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Contact</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Recieve Payment</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Send Tenant Statements</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Bank Deposite</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Bill</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Tenant Instrument Register</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Vendor Instrument Register</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed" id="transfer_search">Tenant Transfer</a>
                                    <a href="/Tenant/Transfer" class="list-group-item list-group-item-success strong collapsed">All Tenant Transfer</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Move In</a>
                                    <a href="/Tenant/MoveOut" class="list-group-item list-group-item-success strong collapsed">All Move Out</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Tenants</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Owners</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Vendors</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">NSF Listing</a>
                                </div>
                            </div>
                        </div>
                        <!--Right Navigation Link Ends-->

                        <!--<div class="property-status">
                            <div class="atoz-outer2">
                                    <span class="apex-alphabets" id="apex-alphafilter" style="display:none;">
                                    <span class="AtoZ"></span></span>
                                <span class="AZ" id="AZ">A-Z</span>
                                <span id="allAlphabet" style="cursor:pointer;">All</span>
                            </div>
                        </div>-->

                        <input type="hidden" class="hidden_name_val">
                        <div class="container">
                            <div class="modal fade" id="calendar" role="dialog" data-backdrop="static" data-keyboard="false">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Calendar View : Short Term Booking</h4>
                                        </div>
                                        <div class="modal-body pull-left">
                                            <div class="col-sm-4 ">
                                                <input class="form-control capital calander" type="text" maxlength="20" name="from_date" id="start_date"/>
                                            </div>
                                            <div class="col-sm-4 ">
                                                <input class="form-control capital calander" type="text" maxlength="20" name="till_date" id="last_date"/>
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="button" value="Search" class="blue-btn" id="search_list"/>
                                            </div>
                                            <div class="col-sm-3 clear" style="padding-top: 20px; padding-bottom: 30px;">
                                                <label>
                                                   Search</label>
                                                <input class="form-control capital" type="text" maxlength="20" placeholder="search"  name="last_name" id="all_search"/>
                                            </div>
                                            <div class="cal-booked cal-bookedtime">
                                                <label></label>
                                                <span>Booked</span>
                                            </div>
                                        <div class="col-sm-9" style="padding-top: 40px; padding-bottom: 30px; padding-left: 90px;">
                                             <a id="left_inc" class="timesheet-arrow" style="cursor: pointer">
                                                   &laquo;</a><b> <span id="montth" style="font-size:20px">
                                                   </span></b> <a id="right_inc" class="timesheet-arrow" style="cursor: pointer">
                                                   &raquo;</a>
                                                <input class="form-control capital" type="hidden" maxlength="20" placeholder="search" id="date_change"/>
                                                <input class="form-control capital" type="hidden" maxlength="20" placeholder="search" id="date_dynamic"/>
                                                <input class="form-control capital" type="hidden" maxlength="20" placeholder="search" id="date_dynamic_last"/>
                                                <input class="form-control capital" type="hidden" maxlength="20" placeholder="search" id="dynamic_year"/>
                                                <input class="form-control capital" type="hidden" maxlength="20" placeholder="search" id="count_prop"/>
                                                <input class="form-control capital" type="hidden" maxlength="20" placeholder="search" id="unit_detail"/>
                                                <input class="form-control capital" type="hidden" maxlength="20" placeholder="search" id="all_dates"/>
                                                <input class="form-control capital" type="hidden" maxlength="20" placeholder="search" id="all_units"/>
                                                <input class="form-control capital" type="hidden" maxlength="20" placeholder="search" id="all_names"/>
                                            </div>

                                            <div class="" style='overflow-x:scroll;overflow-y:scroll;width:850px; height: auto;'>
                                                    <table id="cal">
                                                        <thead></thead>
                                                        <tbody id="J_timedSheet">
                                                        </tbody>
                                                    </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="tab-content" id="listing_tab">
                            <div role="tabpanel" class="tab-pane active" id="people-tenant">

                                <div class="sub-tabss sub-navs">

                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation"><a href="/Tenantlisting/Tenantlisting">Regular</a></li>
                                        <li role="presentation" class="active"><a href="/ShortTermRental/RentersListing">Short Term Renters</a></li><a class="img-calendar" style="cursor: pointer;"><img class="pull-right property-status" width="150px"  src="<?php echo COMPANY_SITE_URL ?>/images//Daycal.png" style="width: 70px;height:65px"/></a>
                                    </ul>
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div class="panel panel-default">
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body pad-none">
                                                                <div class="accordion-grid">
                                                                    <div class="panel-group" id="accordion">
                                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                            <div class="grid-outer">
                                                                                <div class="table-responsive">
                                                                                    <div class="apx-table listinggridDiv short_term_grid">
                                                                                        <div class="table-responsive">
                                                                                            <table id="short_term" class="table table-bordered"></table>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="booking_history_shortTerm" style="display: none;">
                                                                                    <div class="form-hdr">
                                                                                        <h3>
                                                                                            <strong class="left">Renter History</strong>
                                                                                            <a herf="" class="back goback_func pull-right rental_history_back" style="cursor: pointer;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                                                        </h3>
                                                                                    </div>
                                                                                    <div class="form-data">
                                                                                    <div class="apx-table listinggridDiv short_term_history">
                                                                                        <div class="table-responsive">
                                                                                            <table id="short_term_history_table" class="table table-bordered"></table>
                                                                                        </div>
                                                                                    </div>
                                                                                    </div>

                                                                                </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                <div class="short_term_view_div" style="display: none;">
                                    <div class="form-outer form-outer2">
                                        <input type="hidden" class="hidden_id_edit"/>
                                        <div class="form-hdr">
                                            <h3>General <a class="back backshort" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="detail-outer">
                                                <div class="row">
                                                    <div class="col-sm-2 text-center">
                                                        <img id="shortimagege" src="http://apex.local/company/images/dummy-img.jpg">
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Renter Name :</label>
                                                            <span class="renter_name"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Building Name :</label>
                                                            <span class="building_name"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Phone Number :</label>
                                                            <span class="phone_no"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">office/Work Extention :</label>
                                                            <span class="office_ext"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Email :</label>
                                                            <span class="email_short"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Martial Status :</label>
                                                            <span class="martialStatus"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Veteran Status :</label>
                                                            <span class="veteranShort"></span>
                                                        </div>


                                                    </div>

                                                    <div class="col-sm-5">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Property Name:</label>
                                                            <span class="property_name"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Unit# :</label>
                                                            <span class="unit_no"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Note for this Number :</label>
                                                            <span class="red-star note_phone"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Entity/Company Name :</label>
                                                            <span class="company_name"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Hobbies :</label>
                                                            <span class="hobbies"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Ethnicity :</label>
                                                            <span class="ethnicity"></span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="edit-foot">
                                                    <a class="edit_short" href="javascript:;">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Booking Details</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="detail-outer">
                                                <div class="row">

                                                    <div class="col-sm-6">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Booking Type :</label>
                                                            <span class="booking_type"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Booking Dates :</label>
                                                            <span class="bookingDates"></span>
                                                        </div>


                                                    </div>

                                                    <div class="col-sm-5">

                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                </div></div></div>


                        <div class="tab-content" id="edit_tab" style="display: none;">
                            <div role="tabpanel" class="tab-pane active" id="people-tenant">
                                <div class="sub-tabss sub-navs">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation"><a href="/Tenantlisting/Tenantlisting" >Regular</a></li>
                                        <li role="presentation" class="active"><a href="/ShortTermRental/RentersListing">Short Term Renters</a></li><img class="pull-right property-status" width="150px"  src="<?php echo COMPANY_SITE_URL ?>/images//Daycal.png" style="width: 70px;height:65px"/>
                                    </ul>
                                    <div class="row sub-tabss sub-navs">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#edit_info" aria-controls="profile" role="tab" data-toggle="tab">General Information</a></li>
                                            <li role="presentation" ><a href="#flag_tab" class="flag_short" aria-controls="profile" role="tab" data-toggle="tab">Flag Bank</a></li>
                                        </ul>
                                    </div>
                                    <div role="tabpanel" class="tab-pane active" id="edit_info">
                                        <div class="form-outer">
                                            <form id="edit_general_short_term">
                                                <div class="form-data">
                                                    <div class="row">
                                                        <div class="form-outer2 property-status">
                                                            <div class="col-sm-12">
                                                                <div class="check-outer">
                                                                    <input type="checkbox" name="entity_company" id="entity_company1" class="entity_company1" value="1"/> <label>Check this Box if this Tenant is an Entity/Company</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4" id="company_hidden_field1" style="display: none">
                                                                <label>Contact for this tenant <em class="red-star">*</em></label>
                                                                <input class="form-control" name="comapany_name"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <label>Upload Picture</label>
                                                            <div class="upload-logo clearfix">
                                                                <div class="img-outer"><img id="short_term_edit" src="<?php echo COMPANY_SITE_URL ?>/images/dummy-img.jpg"></div>

                                                                <a href="javascript:;">
                                                                    <i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Image</a>
                                                                <span>(Maximum File Size Limit: 1MB)</span>
                                                            </div>
                                                            <div class="image-editor">
                                                                <input type="file" class="cropit-image-input tenant_image" name="tenant_image">
                                                                <div class='cropItData' style="display: none;">
                                                                    <span class="closeimagepopupicon">X</span>
                                                                    <div class="cropit-preview"></div>
                                                                    <div class="image-size-label">Resize image</div>
                                                                    <input type="range" class="cropit-image-zoom-input">
                                                                    <input type="hidden" name="image-data" class="hidden-image-data"/>
                                                                    <input type="button" class="export" value="Done"
                                                                           data-val="tenant_image">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="company_hidden_div1">
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Salutation</label>
                                                                <select class="form-control salutation" name="salutation">
                                                                    <option value="Select">Select</option>
                                                                    <option value="Dr.">Dr.</option>
                                                                    <option value="Mr.">Mr.</option>
                                                                    <option value="Mrs.">Mrs.</option>
                                                                    <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                                                    <option value="Ms.">Ms.</option>
                                                                    <option value="Sir">Sir</option>
                                                                    <option value="Madam">Madam</option>
                                                                    <option value="Brother">Brother</option>
                                                                    <option value="Sister">Sister</option>
                                                                    <option value="Father">Father</option>
                                                                    <option value="Mother">Mother</option>
                                                                </select>
                                                            </div>
                                                             <input class="form-control id" type="hidden" name="user_id" id="user_id">
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>First Name <em class="red-star">*</em></label>
                                                                <input class="form-control first_name" type="text" name="first_name">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Middle Name</label>
                                                                <input class="form-control middle_name" type="text" name="middle_name">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Last Name <em class="red-star">*</em></label>
                                                                <input class="form-control last_name" type="text" name="last_name">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Maiden Name</label>
                                                                <input class="form-control maiden_name" type="text" name="maiden_name">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Nick Name</label>
                                                                <input class="form-control nick_name" type="text" name="nickname">
                                                            </div>
                                                        </div>
                                                        <div id="short_term_rental" class="primary-tenant-phone-row-short1">
                                                            <div class="col-xs-12 col-sm-3 col-md-3">

                                                                <label>Phone Type</label>
                                                                <select class="form-control phone_type_rental phone_typee" name="phone_typpe">
                                                                    <option value="1">Mobile</option>
                                                                    <option value="2">Work</option>
                                                                    <option value="3">Fax</option>
                                                                    <option value="4">Home</option>
                                                                    <option value="5">Other</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <label>Carrier <em class="red-star">*</em></label>
                                                                <select class="form-control carrier_shortTerm career" name="carriers"><option>Select</option></select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <label>Country Code</label>
                                                                <select class="form-control country_shortTerm codde" name="country"><option>United States (+1)</option></select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <label>Phone Number<em class="red-star">*</em> </label>
                                                                <input class="form-control add-input phone_numbeer" name="phone_number" type="text">
                                                                <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                                            <label>Add Note for this Phone Number </label>
                                                            <input class="form-control add-input capital" type="text" name="notePhone">
                                                        </div>
                                                        <div class="multipleEmailShortTerm1">
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Email </label>
                                                                <input class="form-control add-input email" type="text" name="email">
                                                                <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle email-plus-sign-short-term" aria-hidden="true"></i></a>
                                                                <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle fa-minus-circle-short-term" aria-hidden="true"></i></a>
                                                                <span class="err-class"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                                            <label>Referral Source <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle propreerralicon1" aria-hidden="true"></i></a></label>
                                                            <select class="form-control referralShortTerm reff_source referal_source" name="referralSource"></select>
                                                            <div class="add-popup" id="selectPropertyReferralResource1">
                                                                <h4>Add New Referral Source</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>New Referral Source <em class="red-star">*</em></label>
                                                                            <input name="referral" class="form-control customValidateGroup reff_source capital" type="text" data_required="true" data_max="150" placeholder="New Referral Source">
                                                                            <span class="customError required" aria-required="true" id="reff_source"></span>
                                                                        </div>
                                                                        <div class="btn-outer">
                                                                            <button type="button" class="blue-btn add_single1" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source" data-name="referralSource">Save</button>
                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                                            <label>Ethnicity  <a class="pop-add-icon selectPropertyEthnicity1" href="javascript:;">
                                                                    <i class="fa fa-plus-circle"aria-hidden="true"></i>
                                                                </a></label>
                                                            <select class="form-control ethnicity_short1 ethnicity" name="ethncity"><option>United States (+1)</option></select>
                                                            <div class="add-popup" id="selectPropertyEthnicity1">
                                                                <h4>Add New Ethnicity</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>New Ethnicity <em class="red-star">*</em></label>
                                                                            <input class="form-control ethnicity_src customValidateGroup capsOn" type="text" placeholder="Add New Ethnicity" data_required="true" name="ethncity1">
                                                                            <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                                        </div>
                                                                        <div class="btn-outer">
                                                                            <button type="button" class="blue-btn add_single1"  data-table="tenant_ethnicity" data-cell="title" data-class="ethnicity_src" data-name="ethncity1">Save</button>
                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                                            <label>Martial Status <a class="pop-add-icon selectPropertyMaritalStatus1" href="javascript:;">
                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                </a></label>
                                                            <select class="form-control martial_short marital_status" name="maritalStatus"><option>United States (+1)</option></select>
                                                            <div class="add-popup" id="selectPropertyMaritalStatus1">
                                                                <h4>Add New Marital Status</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>New Marital Status <em class="red-star">*</em></label>
                                                                            <input class="form-control maritalstatus_src capsOn" type="text" placeholder="Add New Marital Status">
                                                                            <span class="customError required red-star" aria-required="true" id="maritalstatus_src"></span>
                                                                        </div>
                                                                        <div class="btn-outer">
                                                                            <button type="button" class="blue-btn add_single1" data-table="tenant_marital_status" data-cell="marital" data-class="maritalstatus_src" data-name="maritalStatus">Save</button>
                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                                            <label>Hobbies <a class="pop-add-icon selectPropertyHobbies1" href="javascript:;">
                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                </a></label>
                                                            <select class="form-control hobbies_short multiselect" name="hobbies[]" multiple><option>United States (+1)</option></select>
                                                            <div class="add-popup" id="selectPropertyHobbies1">
                                                                <h4>Add New Hobbies</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>New Hobbies <em class="red-star">*</em></label>
                                                                            <input class="form-control hobbies_src capsOn" type="text" placeholder="Add New Hobbies" data_required="true">
                                                                            <span class="red-star" id="hobbies_src"></span>
                                                                        </div>
                                                                        <div class="btn-outer">
                                                                            <button type="button" class="blue-btn add_single1" data-table="hobbies" data-cell="hobby" data-class="hobbies_src" data-name="hobbies[]">Save</button>
                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                                            <label>Veteran Status <a class="pop-add-icon selectPropertyVeteranStatus1" href="javascript:;">
                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                </a></label>
                                                            <select class="form-control veteran_shortTerm vet_status" name="veteranStatus"><option>Select</option></select>
                                                            <div class="add-popup" id="selectPropertyVeteranStatus1">
                                                                <h4>Add New VeteranStatus</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>New VeteranStatus <em class="red-star">*</em></label>
                                                                            <input class="form-control veteran_src capsOn" type="text" placeholder="Add New VeteranStatus">
                                                                            <span class="red-star" id="veteran_src"></span>
                                                                        </div>
                                                                        <div class="btn-outer">
                                                                            <button type="button" class="blue-btn add_single1" data-table="tenant_veteran_status" data-cell="veteran" data-class="veteran_src" data-name="veteranStatus">Save</button>
                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="multipleSsnShortTerm1">
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>SSN/SIN/ID</label>
                                                                <input class="form-control add-input" type="text" name="ssn_id"/>
                                                                <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle ssn-plus-sign-short_term" aria-hidden="true"></i></a>
                                                                <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle ssn-remove-sign-short-term" aria-hidden="true"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="btn-outer">
                                                        <a class="blue-btn pull-left" type="button" id="update_geninfo" style="cursor: pointer;">Update</a>
                                                        <a class="grey-btn pull-left" type="button" id="cancel_genifo" style="cursor: pointer;">Cancel</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div></div></div>


                        <div id="flag_tab" role="tabpanel" class="flag-container flag-tab tab-pane" style="display: none;">
                            <div class="form-outer">
                                <div class="form-hdr"><h3>Flag</h3></div>
                                <div class="form-data">
                                    <div class="property-status form-outer2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="btn-outer text-right">
                                                    <a class="blue-btn addbtnflag">New Flag
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- New Flag Form -->
                                    <div class="row">
                                        <form name="flagform" id="flagform" method="post"
                                              style="display: none;">
                                            <input name="flag_tenant_id" type="hidden"
                                                   class="hiddenflag tenant_id">
                                            <input name="record_id" type="hidden" value="">
                                            <div class="col-sm-2">
                                                <label>Flagged By</label>
                                                <input type="text" name="flagged_by_name"
                                                       class="capsOn form-control" value="<?php print_r($_SESSION[SESSION_DOMAIN]['name']); ?>">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Date</label>
                                                <input type="text" name="date" class="form-control calander">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Flag Name</label>
                                                <input name="flag_name" class="form-control" placeholder="Please Enter the Name of this Flag"
                                                       type="text">
                                            </div>
                                            <div class="col-sm-2" style="display: none;">
                                                <label>Country Code</label>
                                                <select name="country_code"
                                                        class="form-control" id="guestCountries"></select>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Phone Number</label>
                                                <input name="phone_number"
                                                       class="phone_format form-control" type="text">
                                            </div>

                                            <div class="col-sm-2">
                                                <label>Flag Reason</label>
                                                <input name="flag_reason" class="form-control"
                                                       type="text">
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <label>Flagged For</label>
                                                <input class="form-control capital" disabled id="flagged_for" maxlength="100"  type="text"/>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Completed</label>
                                                <select name="status" class="form-control">
                                                    <option value="1">Yes</option>
                                                    <option value="0" selected>No</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Note</label>
                                                <textarea name="note" class="form-control"></textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="btn-outer">
                                                    <a class="blue-btn saveflag"
                                                       href="javascript:void(0)">Save</a>
                                                    <a class="grey-btn"
                                                       href="javascript:void(0)">Cancel</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- New Flag Form -->

                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">

                                                    <div id="collapseOne"
                                                         class="panel-collapse collapse in"
                                                         aria-expanded="true" style="">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive apx-table">
                                                                    <table class="table table-hover table-dark"
                                                                           id="flaglistingtable"></table>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>


                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



<!--                                    Tab panes -->
<!--                                    <div class="tab-content">-->
<!---->
<!--                                        <div class="accordion-grid">-->
<!--                                            <div class="accordion-outer">-->
<!--                                                <div class="bs-example">-->
<!--                                                    <div class="panel-group" id="accordion">-->
<!--                                                        <div class="panel panel-default">-->
<!--                                                            <div class="panel-heading">-->
<!--                                                                <h4 class="panel-title">-->
<!--                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> List of Short Term Renters</a>-->
<!--                                                                </h4>-->
<!--                                                            </div>-->
<!--                                                            <div id="collapseOne" class="panel-collapse collapse  in">-->
<!--                                                                <div class="panel-body pad-none">-->
<!--                                                                    <div class="grid-outer">-->
<!--                                                                        <div class="table-responsive">-->
<!--                                                                            <table class="table table-hover table-dark">-->
<!--                                                                                <thead>-->
<!--                                                                                <tr>-->
<!--                                                                                    <th scope="col">Renter Name</th>-->
<!--                                                                                    <th scope="col">Email</th>-->
<!--                                                                                    <th scope="col">Phone</th>-->
<!--                                                                                    <th scope="col">Property Name</th>-->
<!--                                                                                    <th scope="col">Unit</th>-->
<!--                                                                                    <th scope="col">Rent($)</th>-->
<!---->
<!--                                                                                    <th scope="col">Days Remaining</th>-->
<!--                                                                                    <th scope="col">Status</th>-->
<!--                                                                                    <th scope="col">Actions</th>-->
<!--                                                                                </tr>-->
<!--                                                                                </thead>-->
<!--                                                                                <tbody>-->
<!--                                                                                <tr>-->
<!--                                                                                    <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>-->
<!--                                                                                    <td>555-444-6666</td>-->
<!--                                                                                    <td>ashleym524@gmail.com</td>-->
<!--                                                                                    <td>Bill Farms</td>-->
<!--                                                                                    <td>Bill B20</td>-->
<!--                                                                                    <td>600.00</td>-->
<!--                                                                                    <td>(575.00)</td>-->
<!---->
<!--                                                                                    <td>Active</td>-->
<!--                                                                                    <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                                </tr>-->
<!--                                                                                <tr>-->
<!--                                                                                    <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>-->
<!--                                                                                    <td>555-444-6666</td>-->
<!--                                                                                    <td>ashleym524@gmail.com</td>-->
<!--                                                                                    <td>Bill Farms</td>-->
<!--                                                                                    <td>Bill B20</td>-->
<!--                                                                                    <td>600.00</td>-->
<!--                                                                                    <td>(575.00)</td>-->
<!---->
<!--                                                                                    <td>Active</td>-->
<!--                                                                                    <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                                </tr>-->
<!--                                                                                <tr>-->
<!--                                                                                    <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>-->
<!--                                                                                    <td>555-444-6666</td>-->
<!--                                                                                    <td>ashleym524@gmail.com</td>-->
<!--                                                                                    <td>Bill Farms</td>-->
<!--                                                                                    <td>Bill B20</td>-->
<!--                                                                                    <td>600.00</td>-->
<!--                                                                                    <td>(575.00)</td>-->
<!---->
<!--                                                                                    <td>Active</td>-->
<!--                                                                                    <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                                </tr>-->
<!--                                                                                <tr>-->
<!--                                                                                    <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>-->
<!--                                                                                    <td>555-444-6666</td>-->
<!--                                                                                    <td>ashleym524@gmail.com</td>-->
<!--                                                                                    <td>Bill Farms</td>-->
<!--                                                                                    <td>Bill B20</td>-->
<!--                                                                                    <td>600.00</td>-->
<!--                                                                                    <td>(575.00)</td>-->
<!---->
<!--                                                                                    <td>Active</td>-->
<!--                                                                                    <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                                </tr>-->
<!--                                                                                </tbody>-->
<!--                                                                            </table>-->
<!--                                                                        </div>-->
<!--                                                                    </div>-->
<!--                                                                </div>-->
<!--                                                            </div>-->
<!--                                                        </div>-->
<!--                                                    </div>-->
<!---->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                Sub tabs ends-->
<!--                            </div>-->
<!--                            <div role="tabpanel" class="tab-pane" id="people-owner">-->
<!--                                <div class="property-status">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-sm-2">-->
<!--                                            <label>Status</label>-->
<!--                                            <select class="fm-txt form-control"> <option>Active</option>-->
<!--                                                <option></option>-->
<!--                                                <option></option>-->
<!--                                                <option></option>-->
<!--                                            </select>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-10">-->
<!--                                            <div class="btn-outer text-right">-->
<!--                                                <button class="blue-btn">Download Sample</button>-->
<!--                                                <button class="blue-btn">Import Owner</button>-->
<!--                                                <button onclick="window.location.href='new-property.html'" class="blue-btn">New Owner</button>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="accordion-grid">-->
<!--                                    <div class="accordion-outer">-->
<!--                                        <div class="bs-example">-->
<!--                                            <div class="panel-group" id="accordion">-->
<!--                                                <div class="panel panel-default">-->
<!--                                                    <div class="panel-heading">-->
<!--                                                        <h4 class="panel-title">-->
<!--                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">-->
<!--                                                                <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Owners</a>-->
<!--                                                        </h4>-->
<!--                                                    </div>-->
<!--                                                    <div id="collapseOne" class="panel-collapse collapse  in">-->
<!--                                                        <div class="panel-body pad-none">-->
<!--                                                            <div class="grid-outer">-->
<!--                                                                <div class="table-responsive">-->
<!--                                                                    <table class="table table-hover table-dark">-->
<!--                                                                        <thead>-->
<!--                                                                        <tr>-->
<!--                                                                            <th scope="col">Owner Name</th>-->
<!--                                                                            <th scope="col">Company</th>-->
<!--                                                                            <th scope="col">Phone</th>-->
<!--                                                                            <th scope="col">Email</th>-->
<!--                                                                            <th scope="col">Date Created</th>-->
<!--                                                                            <th scope="col">Owner's Portal</th>-->
<!--                                                                            <th scope="col">Status</th>-->
<!--                                                                            <th scope="col">Actions</th>-->
<!--                                                                        </tr>-->
<!--                                                                        </thead>-->
<!--                                                                        <tbody>-->
<!--                                                                        <tr>-->
<!--                                                                            <td><a class="grid-link" href="javascript:;">Alien N West</a></td>-->
<!--                                                                            <td></td>-->
<!--                                                                            <td>555-444-6666</td>-->
<!--                                                                            <td>allen09@gmail.com</td>-->
<!--                                                                            <td>11/23/2018 (Fri.)</td>-->
<!--                                                                            <td>Yes</td>-->
<!--                                                                            <td>Active</td>-->
<!--                                                                            <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                        </tr>-->
<!--                                                                        <tr>-->
<!--                                                                            <td><a class="grid-link" href="javascript:;">Alien N West</a></td>-->
<!--                                                                            <td></td>-->
<!--                                                                            <td>555-444-6666</td>-->
<!--                                                                            <td>allen09@gmail.com</td>-->
<!--                                                                            <td>11/23/2018 (Fri.)</td>-->
<!--                                                                            <td>Yes</td>-->
<!--                                                                            <td>Active</td>-->
<!--                                                                            <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                        </tr>-->
<!--                                                                        <tr>-->
<!--                                                                            <td><a class="grid-link" href="javascript:;">Alien N West</a></td>-->
<!--                                                                            <td></td>-->
<!--                                                                            <td>555-444-6666</td>-->
<!--                                                                            <td>allen09@gmail.com</td>-->
<!--                                                                            <td>11/23/2018 (Fri.)</td>-->
<!--                                                                            <td>Yes</td>-->
<!--                                                                            <td>Active</td>-->
<!--                                                                            <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                        </tr>-->
<!--                                                                        <tr>-->
<!--                                                                            <td><a class="grid-link" href="javascript:;">Alien N West</a></td>-->
<!--                                                                            <td></td>-->
<!--                                                                            <td>555-444-6666</td>-->
<!--                                                                            <td>allen09@gmail.com</td>-->
<!--                                                                            <td>11/23/2018 (Fri.)</td>-->
<!--                                                                            <td>Yes</td>-->
<!--                                                                            <td>Active</td>-->
<!--                                                                            <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                        </tr>-->
<!--                                                                        </tbody>-->
<!--                                                                    </table>-->
<!--                                                                </div>-->
<!--                                                            </div>-->
<!--                                                        </div>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!---->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div role="tabpanel" class="tab-pane" id="people-vendor">-->
<!--                                <div class="property-status">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-sm-2">-->
<!--                                            <label>Vendor Type</label>-->
<!--                                            <select class="fm-txt form-control"> <option>Select</option>-->
<!--                                                <option></option>-->
<!--                                                <option></option>-->
<!--                                                <option></option>-->
<!--                                            </select>-->
<!---->
<!--                                        </div>-->
<!--                                        <div class="col-sm-2">-->
<!--                                            <label>Status</label>-->
<!--                                            <select class="fm-txt form-control"> <option>Active</option>-->
<!--                                                <option></option>-->
<!--                                                <option></option>-->
<!--                                                <option></option>-->
<!--                                            </select>-->
<!---->
<!--                                        </div>-->
<!--                                        <div class="col-sm-8">-->
<!--                                            <div class="btn-outer text-right">-->
<!--                                                <button class="blue-btn">Download Sample</button>-->
<!--                                                <button class="blue-btn">Import Vendor</button>-->
<!--                                                <button onclick="window.location.href='new-property.html'" class="blue-btn">New Vendor</button>-->
<!--                                            </div>-->
<!---->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="accordion-grid">-->
<!--                                    <div class="accordion-outer">-->
<!--                                        <div class="bs-example">-->
<!--                                            <div class="panel-group" id="accordion">-->
<!--                                                <div class="panel panel-default">-->
<!--                                                    <div class="panel-heading">-->
<!--                                                        <h4 class="panel-title">-->
<!--                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">-->
<!--                                                                <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Vendors</a>-->
<!--                                                        </h4>-->
<!--                                                    </div>-->
<!--                                                    <div id="collapseOne" class="panel-collapse collapse  in">-->
<!--                                                        <div class="panel-body pad-none">-->
<!--                                                            <div class="grid-outer">-->
<!--                                                                <div class="table-responsive">-->
<!--                                                                    <table class="table table-hover table-dark">-->
<!--                                                                        <thead>-->
<!--                                                                        <tr>-->
<!--                                                                            <th scope="col">Vendor Name</th>-->
<!--                                                                            <th scope="col">Phone</th>-->
<!--                                                                            <th scope="col">Open Work Orders</th>-->
<!--                                                                            <th scope="col">YTD Payment</th>-->
<!--                                                                            <th scope="col">Type</th>-->
<!--                                                                            <th scope="col">Rate</th>-->
<!--                                                                            <th scope="col">Rating</th>-->
<!--                                                                            <th scope="col">Email</th>-->
<!--                                                                            <th scope="col">Status</th>-->
<!--                                                                            <th scope="col">Actions</th>-->
<!--                                                                        </tr>-->
<!--                                                                        </thead>-->
<!--                                                                        <tbody>-->
<!--                                                                        <tr>-->
<!--                                                                            <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>-->
<!--                                                                            <td>555-444-6666</td>-->
<!--                                                                            <td>1</td>-->
<!--                                                                            <td>0.00</td>-->
<!--                                                                            <td>Contractor-General</td>-->
<!--                                                                            <td>$19.00 /hr</td>-->
<!--                                                                            <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>-->
<!--                                                                            <td>skkokjim@gmail.com</td>-->
<!--                                                                            <td>Active</td>-->
<!--                                                                            <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                        </tr>-->
<!--                                                                        <tr>-->
<!--                                                                            <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>-->
<!--                                                                            <td>555-444-6666</td>-->
<!--                                                                            <td>1</td>-->
<!--                                                                            <td>0.00</td>-->
<!--                                                                            <td>Contractor-General</td>-->
<!--                                                                            <td>$19.00 /hr</td>-->
<!--                                                                            <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>-->
<!--                                                                            <td>skkokjim@gmail.com</td>-->
<!--                                                                            <td>Active</td>-->
<!--                                                                            <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                        </tr>-->
<!--                                                                        <tr>-->
<!--                                                                            <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>-->
<!--                                                                            <td>555-444-6666</td>-->
<!--                                                                            <td>1</td>-->
<!--                                                                            <td>0.00</td>-->
<!--                                                                            <td>Contractor-General</td>-->
<!--                                                                            <td>$19.00 /hr</td>-->
<!--                                                                            <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>-->
<!--                                                                            <td>skkokjim@gmail.com</td>-->
<!--                                                                            <td>Active</td>-->
<!--                                                                            <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                        </tr>-->
<!--                                                                        <tr>-->
<!--                                                                            <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>-->
<!--                                                                            <td>555-444-6666</td>-->
<!--                                                                            <td>1</td>-->
<!--                                                                            <td>0.00</td>-->
<!--                                                                            <td>Contractor-General</td>-->
<!--                                                                            <td>$19.00 /hr</td>-->
<!--                                                                            <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>-->
<!--                                                                            <td>skkokjim@gmail.com</td>-->
<!--                                                                            <td>Active</td>-->
<!--                                                                            <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                        </tr>-->
<!--                                                                        </tbody>-->
<!--                                                                    </table>-->
<!--                                                                </div>-->
<!--                                                            </div>-->
<!--                                                        </div>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!---->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div role="tabpanel" class="tab-pane" id="people-contact">-->
<!--                                 Sub Tabs Starts-->
<!--                                <div class="sub-tabs">-->
<!--                                  Nav tabs -->
<!--                                    <ul class="nav nav-tabs" role="tablist">-->
<!--                                        <li role="presentation" class="active"><a href="#contact-tenant" aria-controls="home" role="tab" data-toggle="tab">Tenants<span class="tab-count">84</span></a></li>-->
<!--                                        <li role="presentation"><a href="#contact-owner" aria-controls="profile" role="tab" data-toggle="tab">Owners<span class="tab-count">36</span></a></li>-->
<!--                                        <li role="presentation"><a href="#contact-vendor" aria-controls="home" role="tab" data-toggle="tab">Vendors<span class="tab-count">71</span></a></li>-->
<!--                                        <li role="presentation"><a href="#contact-users" aria-controls="profile" role="tab" data-toggle="tab">Users<span class="tab-count">199</span></a></li>-->
<!--                                        <li role="presentation"><a href="#contact-others" aria-controls="profile" role="tab" data-toggle="tab">Others<span class="tab-count">84</span></a></li>-->
<!--                                    </ul>-->
<!--                                    Tab panes -->
<!--                                    <div class="tab-content">-->
<!--                                        <div role="tabpanel" class="tab-pane active" id="contact-tenant">-->
<!--                                            <div class="property-status">-->
<!--                                                <div class="row">-->
<!--                                                    <div class="col-sm-2">-->
<!--                                                        <label>Status</label>-->
<!--                                                        <select class="fm-txt form-control"> <option>Active</option>-->
<!--                                                            <option></option>-->
<!--                                                            <option></option>-->
<!--                                                            <option></option>-->
<!--                                                        </select>-->
<!---->
<!--                                                    </div>-->
<!--                                                    <div class="col-sm-10">-->
<!--                                                        <div class="btn-outer text-right">-->
<!--                                                            <button class="blue-btn">Download Sample</button>-->
<!--                                                            <button class="blue-btn">Import Contact</button>-->
<!--                                                            <button onclick="window.location.href='new-property.html'" class="blue-btn">New Contact</button>-->
<!--                                                        </div>-->
<!---->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                            <div class="accordion-grid">-->
<!--                                                <div class="accordion-outer">-->
<!--                                                    <div class="bs-example">-->
<!--                                                        <div class="panel-group" id="accordion">-->
<!--                                                            <div class="panel panel-default">-->
<!--                                                                <div class="panel-heading">-->
<!--                                                                    <h4 class="panel-title">-->
<!--                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">-->
<!--                                                                            <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Contacts</a>-->
<!--                                                                    </h4>-->
<!--                                                                </div>-->
<!--                                                                <div id="collapseOne" class="panel-collapse collapse  in">-->
<!--                                                                    <div class="panel-body pad-none">-->
<!--                                                                        <div class="grid-outer">-->
<!--                                                                            <div class="table-responsive">-->
<!--                                                                                <table class="table table-hover table-dark">-->
<!--                                                                                    <thead>-->
<!--                                                                                    <tr>-->
<!--                                                                                        <th scope="col">Contact Name</th>-->
<!--                                                                                        <th scope="col">Phone</th>-->
<!--                                                                                        <th scope="col">Email</th>-->
<!--                                                                                        <th scope="col">Date Created</th>-->
<!--                                                                                        <th scope="col">Status</th>-->
<!--                                                                                        <th scope="col">Actions</th>-->
<!--                                                                                    </tr>-->
<!--                                                                                    </thead>-->
<!--                                                                                    <tbody>-->
<!--                                                                                    <tr>-->
<!--                                                                                        <td><a class="grid-link" href="javascript:;">Abby N Wesley</a></td>-->
<!--                                                                                        <td>555-444-6666</td>-->
<!--                                                                                        <td>abby768@gmail.com</td>-->
<!--                                                                                        <td>12/7/2018 12:30:53 PM</td>-->
<!--                                                                                        <td>Active</td>-->
<!--                                                                                        <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                                    </tr>-->
<!--                                                                                    <tr>-->
<!--                                                                                        <td><a class="grid-link" href="javascript:;">Arsh Sandhu</a></td>-->
<!--                                                                                        <td>555-444-6666</td>-->
<!--                                                                                        <td>abby768@gmail.com</td>-->
<!--                                                                                        <td>12/7/2018 12:30:53 PM</td>-->
<!--                                                                                        <td>Active</td>-->
<!--                                                                                        <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                                    </tr>-->
<!--                                                                                    <tr>-->
<!--                                                                                        <td><a class="grid-link" href="javascript:;">Ben Snow</a></td>-->
<!--                                                                                        <td>555-444-6666</td>-->
<!--                                                                                        <td>abby768@gmail.com</td>-->
<!--                                                                                        <td>12/7/2018 12:30:53 PM</td>-->
<!--                                                                                        <td>Active</td>-->
<!--                                                                                        <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                                    </tr>-->
<!--                                                                                    <tr>-->
<!--                                                                                        <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>-->
<!--                                                                                        <td>555-444-6666</td>-->
<!--                                                                                        <td>abby768@gmail.com</td>-->
<!--                                                                                        <td>12/7/2018 12:30:53 PM</td>-->
<!--                                                                                        <td>Active</td>-->
<!--                                                                                        <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                                    </tr>-->
<!--                                                                                    </tbody>-->
<!--                                                                                </table>-->
<!--                                                                            </div>-->
<!--                                                                        </div>-->
<!--                                                                    </div>-->
<!--                                                                </div>-->
<!--                                                            </div>-->
<!--                                                        </div>-->
<!---->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                       Regular Rent Ends -->
<!--                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">-->
<!---->
<!--                                        </div>-->
<!--                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">-->
<!---->
<!--                                        </div>-->
<!--                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">-->
<!---->
<!--                                        </div>-->
<!--                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">-->
<!---->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                               Sub tabs ends-->
<!--                            </div>-->
<!--                            <div role="tabpanel" class="tab-pane" id="people-employee">-->
<!--                                <div class="property-status">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-sm-2">-->
<!--                                            <label>Status</label>-->
<!--                                            <select class="fm-txt form-control"> <option>Active</option>-->
<!--                                                <option></option>-->
<!--                                                <option></option>-->
<!--                                                <option></option>-->
<!--                                            </select>-->
<!---->
<!--                                        </div>-->
<!--                                        <div class="col-sm-10">-->
<!--                                            <div class="btn-outer text-right">-->
<!--                                                <button class="blue-btn">Download Sample</button>-->
<!--                                                <button class="blue-btn">Import Employee</button>-->
<!--                                                <button onclick="window.location.href='new-property.html'" class="blue-btn">New Employee</button>-->
<!--                                            </div>-->
<!---->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="accordion-grid">-->
<!--                                    <div class="accordion-outer">-->
<!--                                        <div class="bs-example">-->
<!--                                            <div class="panel-group" id="accordion">-->
<!--                                                <div class="panel panel-default">-->
<!--                                                    <div class="panel-heading">-->
<!--                                                        <h4 class="panel-title">-->
<!--                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">-->
<!--                                                                <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Employees</a>-->
<!--                                                        </h4>-->
<!--                                                    </div>-->
<!--                                                    <div id="collapseOne" class="panel-collapse collapse  in">-->
<!--                                                        <div class="panel-body pad-none">-->
<!--                                                            <div class="grid-outer">-->
<!--                                                                <div class="table-responsive">-->
<!--                                                                    <table class="table table-hover table-dark">-->
<!--                                                                        <thead>-->
<!--                                                                        <tr>-->
<!--                                                                            <th scope="col">Employee Name</th>-->
<!--                                                                            <th scope="col">Phone</th>-->
<!--                                                                            <th scope="col">Email</th>-->
<!--                                                                            <th scope="col">Date Created</th>-->
<!--                                                                            <th scope="col">Status</th>-->
<!--                                                                            <th scope="col">Actions</th>-->
<!--                                                                        </tr>-->
<!--                                                                        </thead>-->
<!--                                                                        <tbody>-->
<!--                                                                        <tr>-->
<!--                                                                            <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>-->
<!--                                                                            <td>555-444-6666</td>-->
<!--                                                                            <td>adam@gmail.com</td>-->
<!--                                                                            <td>11/23/2018 8:48:45 AM</td>-->
<!--                                                                            <td>Active</td>-->
<!--                                                                            <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                        </tr>-->
<!--                                                                        <tr>-->
<!--                                                                            <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>-->
<!--                                                                            <td>555-444-6666</td>-->
<!--                                                                            <td>adam@gmail.com</td>-->
<!--                                                                            <td>11/23/2018 8:48:45 AM</td>-->
<!--                                                                            <td>Active</td>-->
<!--                                                                            <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                        </tr>-->
<!--                                                                        <tr>-->
<!--                                                                            <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>-->
<!--                                                                            <td>555-444-6666</td>-->
<!--                                                                            <td>adam@gmail.com</td>-->
<!--                                                                            <td>11/23/2018 8:48:45 AM</td>-->
<!--                                                                            <td>Active</td>-->
<!--                                                                            <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                        </tr>-->
<!--                                                                        <tr>-->
<!--                                                                            <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>-->
<!--                                                                            <td>555-444-6666</td>-->
<!--                                                                            <td>adam@gmail.com</td>-->
<!--                                                                            <td>11/23/2018 8:48:45 AM</td>-->
<!--                                                                            <td>Active</td>-->
<!--                                                                            <td><select class="form-control"><option>Select</option></select></td>-->
<!--                                                                        </tr>-->
<!--                                                                        </tbody>-->
<!--                                                                    </table>-->
<!--                                                                </div>-->
<!--                                                            </div>-->
<!--                                                        </div>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!---->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->
<!--Model For Move Search Start-->
<div class="container">
    <div class="modal fade" id="moveout_listSearch" role="dialog">
        <div class="modal-dialog modal-lg unit-new-renovation-form">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Charges</h4>
                </div>

                <div class="modal-body">
                    <div class="form-outer col-sm-12">
                        <form id="new_chargeCode_form">
                            <!-- <input type ="hidden" id="chargeCode_userId" name="chargeCode_userId" value="<?php /*echo $_REQUEST["id"]*/?>"  />-->
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <label>Tenant</label>
                                    <input class="form-control" type="text" id="myInput"  placeholder="Click here or start typing the name of the tenant you want to move-out" title="Type in a name">

                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="grid-outer">
                                            <div class="table-responsive" id="tenantData_search">

                                                <!--<table class="table table-hover table-dark" id="myTable">
                                                    <thead>
                                                        <tr class="header">
                                                            <th>Tenant Name</th>
                                                            <th>Phone</th>
                                                            <th>Email</th>
                                                            <th>propertyName</th>
                                                            <th>Unit</th>
                                                            <th>Rent()</th>
                                                            <th>Balance()</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Model For Move Search Start-->

<!--Model for Tenant Transfer start-->
<div class="container">
    <div class="modal fade" id="transfer_listSearch" role="dialog">
        <div class="modal-dialog modal-lg unit-new-renovation-form">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Tenant Transfer</h4>
                </div>

                <div class="modal-body">
                    <div class="form-outer col-sm-12">
                        <form id="new_chargeCodeTransfer_form">
                            <!-- <input type ="hidden" id="chargeCode_userId" name="chargeCode_userId" value="<?php /*echo $_REQUEST["id"]*/?>"  />-->
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <label>Tenant</label>
                                    <input class="form-control" type="text" id="myInputTransfer"  placeholder="Click here or start typing the tenant's name" title="Type in a name">

                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="grid-outer">
                                            <div class="table-responsive" id="transferData_search"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!--Model for Tenant Transfer End-->

<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";

    var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script>
    $("#moveout_Search").click(function(){
        $('#moveout_listSearch').modal('show');
    });
</script>
<script>
    $("#transfer_search").click(function(){
        $('#transfer_listSearch').modal('show');
    });
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<!--<script>
function myFunction() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();

    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");

    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        //alert(td);
        if (td) {
        txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                //console.log(tr[i].getElementsByTagName("td")[0]);debugger;
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "";
            }
        }
    }
}
</script>-->
<script src="https://js.stripe.com/v3/"></script>
<script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/payments/subPayment.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/library/Scheduler/js/TimeSheet.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantListing.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/shortTermRental.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/shortTermListing.js"></script>
<script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/leases/guestCard/guestcardflag.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/fullcal.js" integrity="sha384-vk5WoKIaW/vJyUAd9n/wmopsmNhiy+L2Z+SBxGYnUkunIxVxAv/UtMOhba/xskxh" crossorigin="anonymous"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery-pseudo-ripple.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery-nao-calendar.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>



<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<?php include_once(COMPANY_DIRECTORY_URL . "/views/company/tenants/modals.php"); ?>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/payments/plaidIntialisation.js"></script>
<!--script type="text/javascript"
        src="https://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>-->
<script>

    $('.calander').datepicker({
        yearRange: '1919:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('input[name="birth"]').datepicker({
        yearRange: '1900:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    $(".calander").val(date);


    setTimeout(function () {
        $('input[name="from_date"]').datepicker({
            //  minDate: "dateToday",
            yearRange: '2000:2025',
            changeMonth: true,
            changeYear: true,
            dateFormat: jsDateFomat
        });

    },50);

    $('input[name="till_date"]').datepicker({
        minDate: "dateToday",
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $(document).on('change','input[name="from_date"]',function () {
        var date=$(this).val();
        $('input[name="till_date"]').val(date);
        $('input[name="till_date"]').datepicker({
            minDate: date,
            changeMonth: true,
            changeYear: true,
            dateFormat: jsDateFomat})
    });


</script>
<!-- Footer Ends -->