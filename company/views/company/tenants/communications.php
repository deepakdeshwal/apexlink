<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */






?>
  <style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>



<input type="hidden" name="tenant_id" class="tenant_id" value="<?php echo $_GET['tenant_id']; ?>">

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_top_navigation.php");
?>
<div class="popup-bg"></div>
<div id="wrapper">
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_portal_header.php"); ?>


         <section class="main-content">
            <div class="container-fluid">
              <div class="row">
                <div class="bread-search-outer">
                  <div class="row">
                    <div class="col-sm-8">
                        <div class="breadcrumb-outer">
                          Communication &gt;&gt; <span>Email</span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="easy-search">
                          <input placeholder="Easy Search" type="text"/>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="content-data">

                    <!--Tabs Starts -->
                    <div class="main-tabs">                        
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#communication-one" aria-controls="home" role="tab" data-toggle="tab">Email</a></li>
                          <li role="presentation"><a href="#communication-two" aria-controls="profile" role="tab" data-toggle="tab">Text Message</a></li>
                      </ul>
                      <div class="atoz-outer"> 
                        A-Z <span>All</span>
                      </div>
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div role="tabpanel" class="tab-pane active" id="communication-one">
                              <!-- Sub Tabs Starts-->
                              <div class="property-status">
                                <div class="row">
                                  <div class="col-sm-2">
                                  </div>
                                  <div class="col-sm-10">
                                    <div class="btn-outer text-right">
                                      <button onclick="window.location.href='sent-emails.html'" class="blue-btn">Sent Mails</button>
                                      <button onclick="window.location.href='draft-emails.html'" class="blue-btn">Draft Emails</button>
                                      <button onclick="window.location.href='compose-emails.html'" class="blue-btn">Compose Email</button>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="accordion-form">
                                <div class="accordion-outer">
                                  <div class="bs-example">
                                    <div class="panel-group" id="accordion">
                                      <div class="panel panel-default">
                                          <div class="panel-heading">
                                              <h4 class="panel-title">
                                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Inbox</a>
                                              </h4>
                                          </div>
                                          <div id="collapseOne" class="panel-collapse collapse  in">
                                            <div class="panel-body">
                                              <div class="inbox-outer">
                                                <ul>
                                                  <li>
                                                    <div class="col-sm-6">
                                                      <div class="inbox-lt">
                                                        <label>Nov 13, 2018 (Tue.)</label>
                                                        <span>Test</span>
                                                      </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                      <div class="inbox-rt pull-right">
                                                        <label>Posted by</label>
                                                        <span>Vishu sharma Kamboj</span>
                                                      </div>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="col-sm-6">
                                                      <div class="inbox-lt">
                                                        <label>Nov 13, 2018 (Tue.)</label>
                                                        <span>Test</span>
                                                      </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                      <div class="inbox-rt pull-right">
                                                        <label>Posted by</label>
                                                        <span>Vishu sharma Kamboj</span>
                                                      </div>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="col-sm-6">
                                                      <div class="inbox-lt">
                                                        <label>Nov 13, 2018 (Tue.)</label>
                                                        <span>Test</span>
                                                      </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                      <div class="inbox-rt pull-right">
                                                        <label>Posted by</label>
                                                        <span>Vishu sharma Kamboj</span>
                                                      </div>
                                                    </div>
                                                  </li>
                                                </ul>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                <!-- Accordian Ends -->
                                </div>
                              </div>
                              
                          </div>
                          <!--- Communication One Ends -->

                          <div role="tabpanel" class="tab-pane" id="communication-two">
                            <div class="property-status">
                              <div class="row">
                                <div class="col-sm-2">
                                </div>
                                <div class="col-sm-10">
                                  <div class="btn-outer text-right">
                                    <button onclick="window.location.href='inbox-msg.html'" class="blue-btn">Inbox</button>
                                    <button onclick="window.location.href='draft-msg.html'" class="blue-btn">Drafts</button>
                                    <button onclick="window.location.href='compose-msg.html'" class="blue-btn">Compose Message</button>
                                  </div>
                                </div>
                              </div>
                            </div>

                              <div class="accordion-form">
                                <div class="accordion-outer">
                                  <div class="bs-example">
                                    <div class="panel-group" id="accordion">
                                      <div class="panel panel-default">
                                          <div class="panel-heading">
                                              <h4 class="panel-title">
                                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Senxcxcxcd Messages</a>
                                              </h4>
                                          </div>
                                          <div id="collapseOne" class="panel-collapse collapse  in">
                                            <div class="panel-body">
                                               <div class="accordion-grid">
                                      <div class="accordion-outer">
                                          <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> List of Sent Messages</a> 
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                              <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark">
                                                                      <thead>
                                                                        <tr>
                                                                          <th>To / Name</th>
                                                                          <th>To / Email</th>
                                                                          <th>User / From</th>
                                                                          <th>Message</th>
                                                                          <th>Delivery</th>
                                                                          <th>Date</th>
                                                                        </tr>
                                                                      </thead>
                                                                      <tbody>
                                                                        <tr>
                                                                          <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                          <td>555-444-6666</td>
                                                                          <td></td>
                                                                          <td>Tenant Name</td>
                                                                          <td>Delivered</td>
                                                                          <td>Jan 03, 2019 (Thu.)</td>
                                                                        </tr>
                                                                       <tr>
                                                                          <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                          <td>555-444-6666</td>
                                                                          <td></td>
                                                                          <td>Tenant Name</td>
                                                                          <td>Delivered</td>
                                                                          <td>Jan 03, 2019 (Thu.)</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                          <td>555-444-6666</td>
                                                                          <td></td>
                                                                          <td>Tenant Name</td>
                                                                          <td>Delivered</td>
                                                                          <td>Jan 03, 2019 (Thu.)</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                          <td>555-444-6666</td>
                                                                          <td></td>
                                                                          <td>Tenant Name</td>
                                                                          <td>Delivered</td>
                                                                          <td>Jan 03, 2019 (Thu.)</td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                          <td>555-444-6666</td>
                                                                          <td></td>
                                                                          <td>Tenant Name</td>
                                                                          <td>Delivered</td>
                                                                          <td>Jan 03, 2019 (Thu.)</td>
                                                                        </tr>
                                                                      </tbody>
                                                                    </table>
                                                                  </div>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                
                                          </div>
                                      </div>
                                    </div>
                                            </div>
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                <!-- Accordian Ends -->
                                </div>
                              </div>
                          </div>
                          <!--- Communication Two Ends -->

                          <div role="tabpanel" class="tab-pane" id="communication-three">
                             <div class="sub-tabs">                        
                                <!-- Nav tabs -->
                              <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#letter-notice-one" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Lease Documents</a></li>
                                  <li role="presentation"><a href="#letter-notice-two" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Tenant Documents</a></li>
                                  <li role="presentation"><a href="#letter-notice-three" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Landlord Documents</a></li>
                                  <li role="presentation"><a href="#letter-notice-four" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">e-Sign Documents</a></li>
                              </ul>
                              <!-- Tab panes -->
                              <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="letter-notice-one">
                                  <div class="property-status">
                                    <div class="row">
                                      <div class="col-sm-2">
                                      </div>
                                      <div class="col-sm-10">
                                        <div class="btn-outer text-right">
                                          <button onclick="window.location.href='inbox-msg.html'" class="blue-btn">Create New Letter</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="accordion-grid">
                                    <div class="accordion-outer">
                                      <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                          <div class="panel panel-default">
                                            <div class="panel-heading">
                                              <h4 class="panel-title">
                                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-up"></span> List of Lease Documents</a> 
                                              </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                              <div class="panel-body pad-none">
                                                <div class="grid-outer">
                                                  <div class="table-responsive">
                                                    <table class="table table-hover table-dark">
                                                      <thead>
                                                        <tr>
                                                          <th scope="col">Letter Name</th>
                                                          <th scope="col">Description</th>
                                                          <th scope="col">Actions</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody>
                                                        <tr>
                                                          <td>Insurance Waiver</td>
                                                          <td>Insurance Waiver</td>
                                                          <td>
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                          </td>
                                                        </tr>
                                                      <tr>
                                                          <td>Insurance Waiver</td>
                                                          <td>Insurance Waiver</td>
                                                          <td>
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td>Insurance Waiver</td>
                                                          <td>Insurance Waiver</td>
                                                          <td>
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <!-- Create New LEtter -->
                                  <div class="accordion-grid">
                                    <div class="accordion-outer">
                                      <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-up"></span> Upload New Documents</a> 
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                      <div class="form-outer">
                                                        <div class="row">
                                                          <div class="col-sm-2">
                                                            <label>Letter Name <em class="red-star">*</em></label>
                                                            <input class="form-control" type="text"/>
                                                          </div>
                                                          <div class="col-sm-2">
                                                            <label>Letter Type <em class="red-star">*</em></label>
                                                            <select class="form-control"><option>Select</option></select>
                                                          </div>
                                                          <div class="col-sm-2">
                                                            <label>Tags <em class="red-star">*</em></label>
                                                            <select class="form-control"><option>Select</option></select>
                                                          </div>
                                                          <div class="col-sm-2">
                                                            <label>Description </label>
                                                            <textarea class="form-control"></textarea>
                                                          </div>
                                                          <div class="col-sm-2">
                                                            <label>Select Existing Letter </label>
                                                            <select class="form-control"><option>Select</option></select>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                  <div class="col-sm-12">
                                                    <div class="btn-outer">
                                                      <button onclick="window.location.href='inbox-msg.html'" class="blue-btn">Save</button>
                                                       <button onclick="window.location.href='inbox-msg.html'" class="grey-btn">Cancel</button>
                                                    </div>
                                                  </div>
                                                </div>
                                        </div>
            
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Create New LEtter -->

                                </div>
                                <!-- Letter Notice One Ends -->


                                <div role="tabpanel" class="tab-pane" id="letter-notice-two">
                                  <div class="property-status">
                                    <div class="row">
                                      <div class="col-sm-2">
                                      </div>
                                      <div class="col-sm-10">
                                        <div class="btn-outer text-right">
                                          <button onclick="window.location.href='inbox-msg.html'" class="blue-btn">Create New Letter</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="accordion-grid">
                                    <div class="accordion-outer">
                                      <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                          <div class="panel panel-default">
                                            <div class="panel-heading">
                                              <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-up"></span> List of Tenant Documents</a> 
                                              </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                <div class="panel-body pad-none">
                                                  <div class="grid-outer">
                                                    <div class="table-responsive">
                                                      <table class="table table-hover table-dark">
                                                        <thead>
                                                          <tr>
                                                            <th scope="col">Letter Name</th>
                                                            <th scope="col">Description</th>
                                                            <th scope="col">Actions</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>Mutual Termination of Lease Tenancy</td>
                                                            <td></td>
                                                            <td>
                                                              <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            </td>
                                                          </tr>
                                                        <tr>
                                                            <td>Mutual Termination of Lease Tenancy</td>
                                                            <td></td>
                                                            <td>
                                                              <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            </td>
                                                          </tr>
                                                          <tr>
                                                            <td>Mutual Termination of Lease Tenancy</td>
                                                            <td></td>
                                                            <td>
                                                              <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            </td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!-- Letter Notice Two Ends -->


                                <div role="tabpanel" class="tab-pane" id="letter-notice-three">
                                  <div class="property-status">
                                    <div class="row">
                                      <div class="col-sm-2">
                                      </div>
                                      <div class="col-sm-10">
                                        <div class="btn-outer text-right">
                                          <button onclick="window.location.href='inbox-msg.html'" class="blue-btn">Create New Letter</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="accordion-grid">
                                    <div class="accordion-outer">
                                      <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                          <div class="panel panel-default">
                                            <div class="panel-heading">
                                              <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-up"></span> List of Landlord Documents</a> 
                                              </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                              <div class="panel-body pad-none">
                                                <div class="grid-outer">
                                                  <div class="table-responsive">
                                                    <table class="table table-hover table-dark">
                                                      <thead>
                                                        <tr>
                                                          <th scope="col">Letter Name</th>
                                                          <th scope="col">Description</th>
                                                          <th scope="col">Actions</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody>
                                                        <tr>
                                                          <td>Mutual Termination of Lease Tenancy</td>
                                                          <td></td>
                                                          <td>
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                          </td>
                                                        </tr>
                                                      <tr>
                                                          <td>Mutual Termination of Lease Tenancy</td>
                                                          <td></td>
                                                          <td>
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td>Mutual Termination of Lease Tenancy</td>
                                                          <td></td>
                                                          <td>
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                  
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!-- Letter Notice Three Ends -->


                                <div role="tabpanel" class="tab-pane" id="letter-notice-four">
                                  <div class="property-status">
                                    <div class="row">
                                      <div class="col-sm-2">
                                      </div>
                                      <div class="col-sm-10">
                                        <div class="btn-outer text-right">
                                          <button onclick="window.location.href='inbox-msg.html'" class="blue-btn">Create New Letter</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="accordion-grid">
                                          <div class="accordion-outer">
                                            <div class="bs-example">
                                              <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                  <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-up"></span> List of Lease Documents</a> 
                                                    </h4>
                                                  </div>
                                                      <div id="collapseOne" class="panel-collapse collapse  in">
                                                          <div class="panel-body pad-none">
                                                                <div class="grid-outer">
                                                                  <div class="table-responsive">
                                                                      <table class="table table-hover table-dark">
                                                                        <thead>
                                                                          <tr>
                                                                            <th scope="col">Letter Name</th>
                                                                            <th scope="col">Description</th>
                                                                            <th scope="col">Actions</th>
                                                                          </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                          <tr>
                                                                            <td>Insurance Waiver</td>
                                                                            <td>Insurance Waiver</td>
                                                                            <td>
                                                                              <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                              <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                                            </td>
                                                                          </tr>
                                                                        <tr>
                                                                            <td>Insurance Waiver</td>
                                                                            <td>Insurance Waiver</td>
                                                                            <td>
                                                                              <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                              <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                                            </td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td>Insurance Waiver</td>
                                                                            <td>Insurance Waiver</td>
                                                                            <td>
                                                                              <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                              <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                                            </td>
                                                                          </tr>
                                                                        </tbody>
                                                                      </table>
                                                                    </div>
                                                                  </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                  
                                            </div>
                                          </div>
                                        </div>
                                </div>
                                <!-- Letter Notice Four Ends -->

                              </div>
                            </div>
                          </div>
                          <!--- Communication Three Ends -->

                          <div role="tabpanel" class="tab-pane" id="communication-four">
                            <!-- Sub Tabs Starts-->
                            <div class="sub-tabs">                        
                                <!-- Nav tabs -->
                              <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#conversation-one" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Tenant</a></li>
                                  <li role="presentation"><a href="#conversation-two" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Owner</a></li>
                                  <li role="presentation"><a href="#conversation-three" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Vendor</a></li>
                              </ul>
                              <!-- Tab panes -->
                              <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="conversation-one">
                                  <div class="accordion-form">
                                    <div class="accordion-outer">
                                      <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                          <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="collapsed" aria-expanded="false"> Conversation</a>
                                                    <a class="conversation-link back" href="javascript:;"> New Conversation</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                              <div class="panel-body">
                                                <div class="row">
                                                  <div class="form-outer">
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                      <label>Property Manager</label>
                                                      <select class="form-control"><option>Select</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                      <label>Tenant</label>
                                                      <select class="form-control"><option>Select</option></select>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-sm-12">
                                                    <div class="conversation-outer">
                                                      <ul>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                      </ul>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="accordion-form">
                                    <div class="accordion-outer">
                                      <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                          <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="collapsed" aria-expanded="false"> New Conversation</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                              <div class="panel-body">
                                                <div class="row">
                                                  <div class="form-outer">
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                      <label>Tenant <em class="red-star">*</em></label>
                                                      <select class="form-control"><option>Select</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                      <label>Problem Category <em class="red-star">*</em> <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                      <select class="form-control"><option>Select</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-4 clear">
                                                      <label>Description <em class="red-star">*</em></label>
                                                      <textarea class="form-control capital"></textarea>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            
                                          </div>
                                          <div class="row">
                                            <div class="col-sm-12">
                                              <div class="btn-outer">
                                                <button onclick="window.location.href='inbox-msg.html'" class="blue-btn">Send</button>
                                                 <button onclick="window.location.href='inbox-msg.html'" class="grey-btn">Cancel</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!-- Letter Notice One Ends -->
                                <div role="tabpanel" class="tab-pane" id="conversation-two">
                                  <div class="accordion-form">
                                    <div class="accordion-outer">
                                      <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                          <div class="panel panel-default">
                                            <div class="panel-heading">
                                              <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="collapsed" aria-expanded="false"> Conversation</a>
                                                <a class="conversation-link back" href="javascript:;"> New Conversation</a>
                                              </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in">
                                              <div class="panel-body">
                                                <div class="row">
                                                  <div class="form-outer">
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                      <label>Property Manager</label>
                                                      <select class="form-control"><option>Select</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                      <label>Owner</label>
                                                      <select class="form-control"><option>Select</option></select>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-sm-12">
                                                    <div class="conversation-outer">
                                                      <ul>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                      </ul>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!-- Letter Notice Two Ends -->

                                <div role="tabpanel" class="tab-pane" id="conversation-three">
                                  <div class="accordion-form">
                                    <div class="accordion-outer">
                                      <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                          <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="collapsed" aria-expanded="false"> Conversation</a>
                                                  <a class="conversation-link back" href="javascript:;"> New Conversation</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                              <div class="panel-body">
                                                <div class="row">
                                                  <div class="form-outer">
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                      <label>Property Manager</label>
                                                      <select class="form-control"><option>Select</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                      <label>Vendor</label>
                                                      <select class="form-control"><option>Select</option></select>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-sm-12">
                                                    <div class="conversation-outer">
                                                      <ul>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="row">
                                                            <div class="col-sm-1">
                                                              <div class="user-pic"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                            </div>
                                                            <div class="col-sm-10 pad-none">
                                                              <h5>Achal Sharma(Property Manager): Loud Noise <span>Test</span></h5>
                                                              <p>May 15, 2018 (Tue.)- <span>Comment</span> </p>
                                                            </div>
                                                            <div class="col-sm-1 pull-right">
                                                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </div>
                                                          </div>
                                                        </li>
                                                      </ul>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    <!-- Accordian Ends -->
                                    </div>
                                  </div>
                                </div>
                                <!-- Letter Notice Three Ends -->

                                <!-- Letter Notice Four Ends -->

                              </div>
                            </div>
                             <!-- Sub tabs ends-->

                          </div> 
                          <!--- Communication Four Ends -->

                        </div>
                    </div>
                    <!--Tabs Ends -->
                    
              </div>
              </div>
            </div>
        </section>





     




    <!-- Jquery Starts -->
    <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
    <script>var staticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Vehicledummy.png">';</script>
    <script>var petStaticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">';</script>
    <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
   <link rel="stylesheet" href="<?php echo COMPANY_SITE_URL; ?>/css/bootstrap-tagsinput.css" />
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/intlTelInput.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.cropit.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantportal.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
     <script>var upload_url = "<?php echo SITE_URL; ?>";</script>

</div>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<script>

  $(".to").tagsinput('items')


 </script>