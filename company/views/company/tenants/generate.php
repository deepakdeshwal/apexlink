<div class="col-sm-12 generatepage">
    <div class="content-section">
        <!--Tabs Starts -->
        <div class="main-tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="javascript:void(0);" data-tab="property">Select Property</a></li>
                <li role="presentation"><a href="javascript:void(0);" data-tab="lease">Lease Details</a></li>
                <li role="presentation"><a href="javascript:void(0);" data-tab="charges">Charges</a></li>
                <li role="presentation" class="active"><a href="javascript:void(0);" data-tab="generate">Generate
                        Lease</a></li>
            </ul>
            <!-- Nav tabs -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="guest-cards">
                    <div class="form-outer">
                        <div class="form-hdr">
                            <h3>Generate Lease <a class="back" href="javascript:;"><i class="fa fa-angle-double-left"
                                                                                      aria-hidden="true"></i> Back</a>
                            </h3>
                        </div>
                        <div class="form-data">
                            <div class="btn-outer">
                                <a class="blue-btn generate_lease_btn"  data-toggle="modal" data-target="#generate_lease" data-backdrop="static" data-keyboard="false">Generate Lease</a>
                                <a class="blue-btn generate_lease_online_btn" data-toggle="modal" data-target="#generate_online_lease" data-backdrop="static" data-keyboard="false">Generate Online Lease</a>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p><a href="<?php echo COMPANY_SITE_URL; ?>/uploads/tenant_lease/online_offline/lease.pdf" download class="download_blank_lease">Download Blank Lease Document</a></p>
                                    <p><a href="<?php echo COMPANY_SITE_URL; ?>/uploads/tenant_lease/online_offline/lease.pdf" download class="download_prefilled_lease">Download Pre-filled Lease Document</a></p>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <input class="add-file-btn" placeholder="Click here to add" type="text"/>
                                <button class="orange-btn">Remove All Files</button>
                                <button class="blue-btn">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/generateLease.js"></script>

<!-- Footer Ends -->