<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
					<div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Tenant >> <span>Tenant Statements</span>
                            </div>
                        </div>
                    
                        <div class="col-sm-4 pull-right">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
               
						<div class="content-data">
						<!--Right Navigation Link Starts-->
                        <div class="right-links-outer hide-links">
                            <div class="right-links">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </div>
                            <div id="RightMenu" class="box2">
                                <h2>PEOPLE</h2>
                                <div class="list-group panel">
                                    <a href="/Tenantlisting/add" class="list-group-item list-group-item-success strong collapsed">New Tenant</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed" id="moveout_Search">Move Out</a>
                                    <a id ="shorttermrentals" href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Book Now</a>
                                    <a href="/People/AddOwners" class="list-group-item list-group-item-success strong collapsed">New Owner</a>
                                    <a href="/Vendor/AddVendor" class="list-group-item list-group-item-success strong collapsed">New Vendor</a>
                                    <a href="/People/AddEmployee" class="list-group-item list-group-item-success strong collapsed">New Employee</a>
                                    <a href="/People/AddContact" class="list-group-item list-group-item-success strong collapsed">New Contact</a>
                                    <a href="/Tenant/Receivebatchpayment" class="list-group-item list-group-item-success strong collapsed">Recieve Payment</a>
                                    <a href="/Tenant/TenantStatements" class="list-group-item list-group-item-success strong collapsed">Send Tenant Statements</a>
                                    <a href="/Tenant/BatchRegister" class="list-group-item list-group-item-success strong collapsed">Bank Deposite</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Bill</a>
                                    <a href="/Tenant/InstrumentRegister" class="list-group-item list-group-item-success strong collapsed">Tenant Instrument Register</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Vendor Instrument Register</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed" id="transfer_search">Tenant Transfer</a>
                                    <a href="/Tenant/Transfer" class="list-group-item list-group-item-success strong collapsed">All Tenant Transfer</a>
                                    <a href="/Lease/Movein" class="list-group-item list-group-item-success strong collapsed">All Move In</a>
                                    <a href="/Tenant/MoveOut" class="list-group-item list-group-item-success strong collapsed">All Move Out</a>
                                    <a href="/Tenant/FormarTenant" class="list-group-item list-group-item-success strong collapsed">Former Tenants</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Owners</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Vendors</a>
                                    <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">NSF Listing</a>
                                </div>
                            </div>
                        </div>
                        <!--Right Navigation Link Ends-->
								<div class="form-outer">
                                    <div class="form-hdr">
                                        <h3>Tenant Statement <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                    </div>
                                    <div class="form-data">
										<div class="row">
											<div class="col-xs-12 col-sm-4 col-md-3">
											  <label>Tenant</label>
											  <input class="form-control" type="text" placeholder="Click here or start typing the tenant’s name"/>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3">
											  <label>Start Date</label>
											  <input class="form-control" type="text"/>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3">
											  <label>End Date</label>
											  <input class="form-control" type="text"/>
											</div>
										</div>
									</div>
								</div>

									<div class="form-outer">
                                      <div class="form-hdr">
                                        <h3>Tenant Statement</h3>
                                      </div>
                                      <div class="form-data">
										  <div class="grid-outer">
											<div class="table-responsive">
											  <table class="table table-hover table-dark">
												<thead>
												  <tr>
													<th scope="col"><input type="checkbox"/></th>
													<th scope="col">Tenant</th>
													<th scope="col">Property</th>
													<th scope="col">Status</th>
													<th scope="col">Last Letter</th>
													<th scope="col">Date Send</th>
													<th scope="col">Action</th>
												  </tr>
												</thead>
												<tbody>
												  <tr>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												  </tr>
												  <tr>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												  </tr>
												</tbody>
											  </table>
											</div>
										  </div>
                                     
                                        </div>
										<div class="btn-outer">
											<button type="button" class="grey-btn">Clear</button>
											<button type="button" class="grey-btn">Cancel</button>
										</div>
                                      </div>
                                    </div>

                </div>

                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->



<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
   
    var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
	
	$("#people_top").addClass("active");
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/shortTermRental.js"></script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->