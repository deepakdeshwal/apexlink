<!DOCTYPE html>
<!-- saved from url=(0072)http://shrey.apex.local/Tenantlisting/TenantLease_Generate_Pdf/?_token=1 -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <style type="text/css">
        body{
            width: 640px;
            margin: 0 auto;
        }
        @font-face {
            font-weight: 400;
            font-style:  normal;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-Regular.woff2') format('woff2');
        }
        @font-face {
            font-weight: 400;
            font-style:  italic;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-Italic.woff2') format('woff2');
        }

        @font-face {
            font-weight: 500;
            font-style:  normal;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-Medium.woff2') format('woff2');
        }
        @font-face {
            font-weight: 500;
            font-style:  italic;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-MediumItalic.woff2') format('woff2');
        }

        @font-face {
            font-weight: 700;
            font-style:  normal;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-Bold.woff2') format('woff2');
        }
        @font-face {
            font-weight: 700;
            font-style:  italic;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-BoldItalic.woff2') format('woff2');
        }

        @font-face {
            font-weight: 900;
            font-style:  normal;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-Black.woff2') format('woff2');
        }
        @font-face {
            font-weight: 900;
            font-style:  italic;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-BlackItalic.woff2') format('woff2');
        }</style></head>
<body>
<div id="print_div">
    <div class="head-img" style="border-top: 20px solid #00bfff; border-bottom: 20px solid #00bfff;     ">
        <div class="inner-div-img" style="text-align: center; background-color: #b0e0e6; ">
            <a href="javascript:;" id="ancViewPrintLedger" title="Print">
                <img src="./shrey.apex.download_files/new_property.jpg&#39;) }}" alt="" width="150" height="100">
            </a>
        </div>
    </div>
    <div class="outer-div-content" style="border: 1px solid #b0e0e6; border-top: 1px solid #FFFFFF;">
        <div style=" padding: 20px;" >
            <h2 style="text-align: center;     color: #00b0f0;
    font-family: arial;
    text-align: center;"><b>Residential Lease Agreement Rent Due Date</b></h2>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="" name="country"><p style="margin: 5px 0; font-family: arial;">The following residential agreement is between COMPANYNAME and TENANTNAME, as of LEASESTARTDATE. Each numbered section defines the term, conditions, and responsibility of each party.
                </p><br><p style="margin: 5px 0; font-family: arial;"> Neschers,Auvergne-Rhône-Alpes,France</p>
            </div><br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <h3 style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;"><b>LANDLORD</b></h3>
                <p style="margin: 5px 0; font-family: arial;">In this agreement the Landlord(s) and/or related agent(s) is/are referred to as “Landlord”. COMPANYNAME is the Landlord.</p>

            </div>
            <br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <label style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;"><b>TENANT</b></label>
                <p style="margin: 5px 0; font-family: arial;">Every instance of the term “Tenant” in this Lease will refer to TENANTNAME.</p>
            </div><br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <label style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;"><b>RENTAL PROPERTY</b></label>
                <p style="margin: 5px 0; font-family: arial;">The property located at , PROPERTYADDRESS, which is owned/managed by the Landlord will be rented to the Tenant. In this Lease the property will be referred to as “lease Premises”.</p>
            </div><br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <label style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">TERM OF LEASE AGREEMENT</label>
                <p style="margin: 5px 0; font-family: arial;">This Lease Agreement will start on LEASESTARTDATE and ends on LEASEENDDATE.</p>
            </div><br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <h3 style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;"><b>USE &amp; OCCUPANCY OF PROPERTY</b></h3>
                <p style="margin: 5px 0; font-family: arial;">The only authorized person(s) that may live in the Leased Premises is/are: TENANTNAME.</p>
            </div><br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <h3 style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;"><b>AMOUNT OF RENT</b></h3>
                <p style="margin: 5px 0; font-family: arial;">The exact amount of Rent due monthly for the right to live in the Leased Premises is 800.00.</p>
            </div><br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <h3 style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;"><b>DUE DATE FOR RENT</b></h3>
                <p style="margin: 5px 0; font-family: arial;">Rent is to be paid before or on the RENTBEFOREDAY. A GRACEPERIOD day grace period will ensue; nevertheless, rent cannot be paid after this grace period without incurring a late fee.</p>
            </div><br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <h3 style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;"><b>LATE FEE</b></h3>
                <p style="margin: 5px 0; font-family: arial;">In the event that rent or associated fees are not paid in full by the rental due date or before the end of the GRACEPERIOD day grace period, a late fee of __________________  () will be incurred.</p>
            </div><br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <h3 style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;"><b>RETURNED PAYMENTS</b></h3>
                <p style="margin: 5px 0; font-family: arial;">Any returned payments from financial institutions that the Tenant utilizes will incur an addition fee of 0.00 that will be added to the rental amount.</p>
            </div><br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <h3 style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;"><b>SECURITY DEPOSIT</b></h3>
                <p style="margin: 5px 0; font-family: arial;">A. Security Deposit of SECURITYDEPOSITE is to be paid by the Tenant before move-in.
                    B. This Security Deposit is reserved for the purpose of paying any cost toward damages, cleaning, excessive wear to the property, and in the event of unreturned keys or abandonment of the Leased Premises before or upon the end of the Lease.</p>
            </div><br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <h3 style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;"><b>UTILITIES &amp; SERVICES</b></h3>
                <p style="margin: 5px 0; font-family: arial;">A. The responsibility of the registering and paying for the following utility services is upon the Tenant:</p><br>
                <input type="text" class=""> <input type="text" class="">
                <p style="margin: 5px 0; font-family: arial;">These services should be maintained at all time during the leasing period.</p>
                <p style="margin: 5px 0; font-family: arial;">B. The Landlord has included the Water utility as a part of rent; however, if the property water usage exceeds Enter amount here the Landlord reserves the right to request that any overages in usage be compensated as a fee in addition to the normal rental rate. These overages must be paid within 20 days.</p>
            </div><br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <h3 style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;"><b>MAINTENANCE &amp; REPAIRS</b></h3>
                <p style="margin: 5px 0; font-family: arial;">General maintenance and repairs are the responsibility of the Landlord, except in cases where the Tenant has been negligent or has accidentally caused damages to the property.</p>
                <p style="margin: 5px 0; font-family: arial;"> A. It will remain the Tenant’s responsibility to promptly notify the Landlord of any necessary repairs to the property.</p>
                <p style="margin: 5px 0; font-family: arial;"> B. Any damages that have resulted from the Tenant or the Tenant’s guests will be the Tenants full responsibility to pay for the costs of repair that is required.</p>
            </div><br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <h3 style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;"><b>CONDITION OF PROPERTY</b></h3>
                <p style="margin: 5px 0; font-family: arial;">A. The Tenant has inspected the Leased Premises acknowledges and accepts that the Leased Premises is in an acceptable living condition and that all parts of the Leased Premises are in good working order.</p>
                <p style="margin: 5px 0; font-family: arial;"> B. It is agreed by the Landlord and Tenant that no promises are made concerning the condition of the Leased Premises.</p>
                <p style="margin: 5px 0; font-family: arial;"> C. The Leased Premises will be returned to the Landlord by the Tenant in the same or better condition that it was in at the commencement of the Lease.</p>
            </div><br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <h3 style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;"><b>ENDING OR RENEWING THE LEASE AGREEMENT</b></h3>
                <p style="margin: 5px 0; font-family: arial;">A. When this Lease ends, if the Tenant or Landlord do not furnish a written agreement to end the Lease it will continue on a month to month basis indefinitely. In order to terminate or renew the Lease agreement either party, Landlord or Tenant, must furnish a written notice at least NOTICEDAYS days before the end of this Lease agreement.</p>
            </div><br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <h3 style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;"><b>GOVERNING LAW</b></h3>
                <p style="margin: 5px 0; font-family: arial;">The terms, conditions, and any addenda to this Lease are to be governed and held in accord to the statutes and Laws of Enter Governing entity here.</p>
            </div><br>
            <div style="width: 97%; margin-top: 30px; padding: 0 10px;" class="">
                <h3 style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;"><b>ENTIRE AGREEMENT</b></h3>
                <p style=" font-family: arial;">NOTICE: This is a LEGALLY binding document.</p>
                <p style=" font-family: arial;">A. You are relinquishing specific important rights.</p>
                <p style="font-family: arial;">B. You may reserve the right to have an attorney review the Lease Agreement before signing it.</p>
                <p style=" font-family: arial;">By signing this Lease Agreement, the Tenant certifies in good faith to have read, understood, and agrees to abide by all of the terms and conditions stated within this Lease.</p>
                <div class="col-sm-12">
                    <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                            <td style="font-family: arial;" width="28%" height="50">Tenant Signature:</td>
                            <td style="font-family: arial;" width="28%" height="50">&nbsp;</td>
                            <td style="padding-right: 10px; font-family: arial;" align="right" width="22%" height="50">Date:</td>
                            <td width="22%" height="50">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="font-family: arial;" height="40">Landlord / Agent Signature:</td>
                            <td height="40">&nbsp;</td>
                            <td style="padding-right: 10px; font-family: arial;" align="right" height="40">Date:</td>
                            <td height="40">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="table-content-outer">
            <div class="col-sm-12">
                <p style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; background: #05a0e4; margin-bottom: 0;
    text-align: center;" >COMPANYNAME Property Manager ● COMPANYPHONENUMBER</p>
            </div>
        </div>
    </div>
</div>

<div style="width: 100%; margin-top: 10px; float: left;margin:10px;text-align: right;"  class="btn-outer text-right">
    <button type="button" id="task_print" style="cursor:pointer;background: #05A0E4; color: #fff; font-weight: 600; border: none; line-height: normal; outline: none; padding: 7px 15px; border-radius: 5px; font-size: 13px;" class="blue-btn task_reminder_save">Print</button>
    <button type="button" id="continue" style="cursor:pointer;background: #4d4d4d ; color: #fff; font-weight: 600; border: none; line-height: normal; outline: none; padding: 7px 15px; border-radius: 5px; font-size: 13px; margin-right: 1%" class="grey-btn task_reminder_cancel">Continue</button>
    <button type="button" id="cancel" style="cursor:pointer;background: #4d4d4d ; color: #fff; font-weight: 600; border: none; line-height: normal; outline: none; padding: 7px 15px; border-radius: 5px; font-size: 13px; margin-right: 1%" class="grey-btn task_reminder_cancel">Cancel</button>
</div>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-3.3.1.min.js"></script>
<script>
    /*
Function to print envelope
*/
    $(document).on('click','#continue',function(){
        window.close();
    });

    $(document).on('click','#cancel',function(){
        if(localStorage.getItem('generateLease') == 'genrateLease'){
            localStorage.removeItem('generateLease');
            window.location.href= '/Tenantlisting/Tenantlisting';
        } else {
            window.location.href= '/Property/PropertyModules';
        }

    });

    $(document).on('click','#task_print',function(){
        PrintElem('#print_div');
    });

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data)
    {
        var base_url = window.location.origin;
        var mywindow = window.open('', 'my div');
        $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
            'font-weight:bold;} .right-detail{\n' +
            '        position:relative;\n' +
            '        left:+400px;\n' +
            '    }</style>');
        $(mywindow.document.body).html( '<body>' + data + '</body>');
        mywindow.document.close();
        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
        if(mywindow.close()){

        }
        return true;
    }
</script>
</body><loom-container id="lo-engage-ext-container"><loom-shadow classname="resolved"></loom-shadow></loom-container></html>