<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/images/favicon.ico" alt="apex-icon" type="image/ico" sizes="16x16">
    <title>Apexlink</title>

    <style>
        .container{
            padding: 0 235px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div id="welcome-newapp">
            <div class="modal-body ">
                <table>
                    <tr>
                        <table style="width: 85%">
                            <tr>
                                <td style="border-bottom: 4px solid #1cabd5; padding-bottom: 15px;"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/logo123.png" alt="emaillogo" height="45px" width="270px"></td>
                            </tr>
                        </table>
                    </tr>
                    <tr>
                        <table style="width: 85%; padding: 0 25px;">
                            <tr><td style="margin-top: 35px;  display: inline-block; font-style: normal;  font-weight: 600;font-size: 20px;">Hello,</td></tr>
                            <tr><td style="margin-top: 28px;  display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px;">SMCOMPANYNAME has requested permission to access your credit and criminal history for consideration in leasing the property located at:</td></tr>
                            <tr><td style="margin-top: 35px;  display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px;">House Name,</td></tr>
                            <tr><td style="display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px;">SMADDRESS,</td></tr>
                            <tr><td style="display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px;">SMCITYSTATEZIP</td></tr>
                            <tr><td style="display: inline-block; font-style: normal;  font-weight: 600;font-size: 15px; margin-top: 30px">Please <a href="SMTENANTLINK">Click here</a> to accept the screening request and authorizes TransUnion to release your Consumer Report information to the landlord/agent listed above.</td></tr>
                            <tr><td style="display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px; margin-top: 30px">If you have any questions regarding the request, please contact Customer Support at 1-866-775-0961.</td></tr>
                            <tr><td style="display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px; margin-top: 30px">Sincerly</td></tr>
                            <tr><td style="display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px;">The SmartMove Team</td></tr>
                            <tr><td style="display: inline-block; font-style: normal;  font-weight: 400;font-size: 15px; margin-top: 30px; line-height: 25px;">Powered by TransUnion, a trusted source of consumer data, SmartMove is a quick and easy way for rental applications like you to safely provide background information to property owners for consideration in the leasing process. At no point will the property owner have to access to your SSN and other sensitive personal information. And your credit rating will not be affected in any way.</td></tr>
                        </table>
                    </tr>
                    <tr>
                        <table style="width: 85%; background: #0095c0; margin-top: 30px;">
                            <tr>
                                <td style="width: 72%; padding: 10px 0 0 20px;"><img src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/images/logo_white.png" alt="footerlogo" height="45px" width="270px"></td>
                                <td style="width: 28%; font-size: 14px; font-style: normal; color: #ffffff; font-weight: 400; padding: 20px 0 0 0;">6430  S. Fiddler's Green Circle</td>
                            </tr>
                            <tr>
                                <td style="width: 72%;"></td>
                                <td style="width: 28%; font-size: 14px; font-style: normal; color: #ffffff; font-weight: 400;">Suite #500 Greenwood Village</td>
                            </tr>
                            <tr>
                                <td style="width: 72%; font-size: 12px; font-style: normal; color: #ffffff; font-weight: 300; padding: 10px 0 25px 20px;">@2015 TRANSUNION RENTAL SCREENING SOLUTIONS.<br> INC. ALL RIGHT RESERVED</td>
                                <td style="width: 28%; font-size: 14px; font-style: normal; color: #ffffff; font-weight: 400; padding: 10px 0 25px 0px;">CO 80111</td>
                            </tr>
                        </table>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</body>
</html>