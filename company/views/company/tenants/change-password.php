
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/images/favicon.ico" alt="apex-icon" type="image/ico" sizes="16x16">

<title>Apexlink</title>
<!-- Bootstrap -->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
<link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/toastr.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/intlTelInput.css">
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/ui.jqgrid.min.css">
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery.timepicker.min.css">

<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/passwordscheck.css" />
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/intlTelInput.css">
<link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL;?>/css/calculator.css"/>
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/owl.theme.default.min.css"/>
<link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.multiselect.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/main.css"/>
<link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />





<!-- include libraries(jQuery, bootstrap) -->
<!-- include summernote css/js -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>-->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/summernote.js" defer></script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-ui.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.timepicker.min.js"></script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootstrap.min.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.jqgrid.min.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.responsivetabs.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.validate.min.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/common.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/toastr.min.js"></script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootbox.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.mask.min.js"></script>
<script defer src="<?php echo SUPERADMIN_SITE_URL; ?>/js/calculator.js"></script>
<script defer src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/owl.carousel.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/core-js/2.1.4/core.min.js"></script>

<script defer>

    //Toast Messages
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "preventDuplicates": true,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "7000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    setTimeout(function () {
        var announcement_skip_ids = localStorage.getItem("announcement_ids");
        //getAnnouncements(announcement_skip_ids);
        setInterval(function () {
            var announcement_skip_ids = localStorage.getItem("announcement_ids");
            //getAnnouncements(announcement_skip_ids);
        }, 60000);

    }, 1000);

    function getAnnouncements(announcement_skip_ids) {
        $.ajax
        ({
            type: 'post',
            url: '/common-ajax',
            data: {
                class: "CommonAjax",
                action: "showAnnouncement",
                announcement_skip_ids: announcement_skip_ids
            },
            success: function (result) {
                var result = JSON.parse(result);
                //console.log(result);
                if (result != 'empty') {
                    announcement_ids = [];
                    $.each(result, function (key, value) {
                        var values;
                        if (value.seenStatus != 'seen') {
                            if (value.title == 'System Maintainance') {
                                values = value.id;
                                $('#announcementSys_id').val(value.id);
                                $('#SysstartDate').text(value.start_date);
                                $('#SysstartTime').text(value.start_time);
                                $('#SysendTime').text(value.end_time);
                                $('#SystemMaintenance').modal('show');
                            } else {
                                values = value.id;
                                $('#announcement_id').val(value.id);
                                $('#announcementTitle').text(value.title);
                                $('#announcementStartDate').text(value.start_date);
                                $('#announcementEndDate').text(value.end_date);
                                $('#announcementStartTime').text(value.start_time);
                                $('#announcementEndTime').text(value.end_time);
                                $('#announcementDescription').text(value.description);
                                $('#announcement').modal('show');
                            }
                            announcement_ids.push(values);
                        }
                    });
                    if (result) {
                        localStorage.setItem("announcement_ids", JSON.stringify(announcement_ids));
                    }
                }
            }
        });
    }

    $(document).on('click', '.closeSystemAnnouncement', function () {
        var announcement_id = $("#announcementSys_id").val();
        $.ajax
        ({
            type: 'post',
            url: '/common-ajax',
            data: {
                class: "CommonAjax",
                action: "updateUserAnnouncement",
                announcement_id: announcement_id
            },
            success: function (result) {
                var result = JSON.parse(result);
            }

        });
    });

    $(document).on('click', '.closeAnnouncement', function () {
        var announcement_id = $("#announcement_id").val();
        $.ajax
        ({
            type: 'post',
            url: '/common-ajax',
            data: {
                class: "CommonAjax",
                action: "updateUserAnnouncement",
                announcement_id: announcement_id
            },
            success: function (result) {
                var result = JSON.parse(result);
            }

        });
    });


    $(document).ready(function () {
        //  window.history.pushState(null, "", window.location.href);
        //   window.onpopstate = function () {
        //       window.history.pushState(null, "", window.location.href);
        //   };
    });


    jQuery.extend(jQuery.validator.messages, {
        required: "* This field is required.",
    });

</script>
<style media="print">
    body{display:none;}

  .password-setup label{ min-height: 70px !important; }
  .password-setup input{ margin-top: 10px; }
</style>

    <input type="hidden" name="tenant_id" class="tenant_id" value="<?php echo $_GET['tenant_id']; ?>">
    <input type="hidden" name="secretkey" class="secretkey"  value="<?php echo $_GET['secretkey']; ?>">


<?php
//include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_portal_header.php");
?>
                 <div class="login-bg">
                <div class="login-outer">
                    <div class="login-logo"><img src="<?php echo COMPANY_SUBDOMAIN_URL;?>/images/logo-login.png"/></div>
                    
                    <div class="login-inner">
                      <form id="confirmPassword"> 
                            <h2>Password Setup</h2>
                            <div class="login-data password-setup">
                              
                                 <label>New Password <em class="red-star">*</em>
                                 <input class="form-control" type="password" name="new_password" id="new_password">
                               </label>
                                       
                                  
                              

                                 
                                        <label>Confirm New Password <em class="red-star">*</em>
                                        <input class="form-control" type="password" name="confirm_password"></label>
                                        
                                     
                             
                             
                                    <div class="btn-outer">
                                     <button class="blue-btn passwordSetup">Save</button>
                                     <input type='reset' value="Cancel" class="grey-btn">
                                  </div>                               
                            </div>
                        </form>
                    </div>

                </div>
            </div>

    <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
    <script>var staticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Vehicledummy.png">';</script>
    <script>var petStaticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg">';</script>
    <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/intlTelInput.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/tenantportal.js"></script>

</div>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<script>
  var tenant_id = $(".tenant_id").val();
  var key = $(".secretkey").val();
  resetPassword(); /*this function is write in tenantportal.js */
  //alert(key);
     $.ajax({
            type: 'post',
            url: '/tenantPortal',
            data: {class: 'TenantPortal', action: 'checkSceretKey','id':tenant_id,'secretKey':key},
            success : function(response){
                var response =  JSON.parse(response);
                if(response.status == 'success') {
                    toastr.success(response.message);
                  //  resetPassword();
                }else {

                    toastr.error(response.message);
                    $(".passwordSetup").attr('disabled',true);
                }
            }
        });


</script>













