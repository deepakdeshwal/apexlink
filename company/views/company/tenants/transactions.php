<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_top_navigation.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_portal_header.php");
?>

<div id="wrapper">
    <div class="popup-bg"></div>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>


    <input type="hidden" name="tenant_id" class="tenant_id" value="<?php echo $_GET['tenant_id']; ?>">



                   <section class="main-content">
          <div class="container-fluid">
            <div class="row">                   
              <div class="col-sm-12">
                <div class="content-section">
                  <div class="form-outer form-outer2">
                    <div class="form-hdr">
                      <h3>Rental Details</h3>
                    </div>
                    <div class="form-data pad-none">
                      <div class="grey-detail-box">
                        <div class="col-xs-12 col-sm-3">
                          <label class="blue-label">Lease ID:</label>
                          <span>11</span>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                          <label class="blue-label">Start Date:</label>
                          <span>December 01, 2018 (Sat.)</span>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                          <label class="blue-label">End Date:</label>
                          <span>November 30, 2019 (Sat.)</span>
                        </div>
                      </div>
                      <div class="grey-detail-box">
                        <div class="col-xs-12 col-sm-3">
                          <label class="blue-label">Move-In Date:</label>
                          <span>November 15, 2019 (Thu.)</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-outer">
                    <div class="form-hdr">
                      <h3>Rental Details</h3>
                    </div>
                    <div class="form-data">
                      <div class="row">
                        <div class="col-xs-12 col-sm-2">
                          <label>Start Date</label>
                          <input class="form-control" type="text"/>
                        </div>
                        <div class="col-xs-12 col-sm-2">
                          <label>End Date</label>
                          <input class="form-control" type="text"/>
                        </div>
                        <div class="col-xs-12 col-sm-2">
                          <label>&nbsp;</label>
                          <button type="button" class="blue-btn">Search</button>
                        </div>
                      </div>
                      <div class="grid-outer">
                        <div class="table-responsive">
                          <table class="table table-hover table-dark">
                            <thead>
                              <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Transaction Code</th>
                                <th scope="col">Transaction Description</th>
                                <th scope="col">Debit(USh)</th>
                                <th scope="col">Credit (USh)</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div class="text-right pull-right print-icon-outer">
                        <a href="#"><i class="fa fa-print fa-lg" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-download fa-lg" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-envelope-o fa-lg" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-file-excel-o fa-lg" aria-hidden="true"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>





     




    <!-- Jquery Starts -->
    <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
    <script>var staticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Vehicledummy.png">';</script>
    <script>var petStaticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg">';</script>
    <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
   <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/bootstrap-tagsinput.css" />
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/intlTelInput.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/tenantportal.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
     <script>var upload_url = "<?php echo SITE_URL; ?>";</script>

</div>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<script>

  $(".to").tagsinput('items')


 </script>