
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<link rel="stylesheet" type="text/css" href="<?php echo COMPANY_SITE_URL; ?>/css/tooltipster.bundle.css" />
<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/smartmove.css"/>
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/shorttermcss.css">
<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/jquery-pseudo-ripple.css">
<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/jquery-nao-calendar.css">
<style>
    #caddress1
    {
        text-transform:capitalize;
    }

    #caddress2
    {
        text-transform:capitalize;
    }

    #cCompany
    {
        text-transform:capitalize;
    }



</style>
<div id="wrapper">

    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Tenant Module &gt;&gt; <span>Active Tenant Listing</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">

                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <!--Tab panes -->

                        <!-- Sub Tabs Starts-->
                        <!-- Tab panes -->
                        <div class="tab-content" style="padding-top: 0px;">
                            <div role="tabpanel" class="tab-pane active" id="regular-rent">
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">

                                                <div class="form-hdr">
                                                    <h3>List Of Tenants
                                                        <span style="float:right;">
                                                            <a class="back" type="button" href="/Property/PropertyModules" style="cursor: pointer;"><< Back</a>
                                                        </span>
                                                    </h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="panel panel-default">
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body pad-none">
                                                                <div class="grid-outer">
                                                                    <div class="table-responsive overflow-unset">
                                                                        <div class="grid-outer">
                                                                            <div class="apx-table">
                                                                                <table class="table table-hover table-dark" id="active_tenant_listing">
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Regular Rent Ends -->
                    <div role="tabpanel" class="tab-pane" id="shortterm-rent">

                    </div>
                </div>
                <!-- Sub tabs ends-->
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
</div>
<!-- Wrapper Ends -->
<div class="overlay">
    <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
        <img width="200"  height="200" src='<?php echo COMPANY_SITE_URL ?>/images/loading.gif'/>
    </div>
</div>





<!--Model for Tenant Transfer End-->

<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var property_id = "<?php echo $_GET['id']; ?>";
</script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/activeTenant.js"></script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->