<?php include_once(SUPERADMIN_DIRECTORY_URL. "/views/layouts/admin_header.php"); ?>
<div class="login-bg">
    <div class="login-outer">
        <div class="login-logo"><img src="<?php echo COMPANY_SITE_URL;?>/images/logo-login.png"/></div>

        <div class="login-inner">
            <form id="portal-login" >
                <h2>SIGN IN TO Tenant Portal</h2>
                <div class="login-data">
                    <label>
                        <input class="form-control" name="email" placeholder="Email" id="email" type="text"/>

                    </label>

                    <label>
                        <input class="form-control email-psw-input" id="password" name="password"  placeholder="Password"  type="password"/>
                    </label>
                    <div class="remember-psw">
                        <div class="remember-psw-lt"><input type="checkbox" name='remember' id="remember" /> Remember Me</div>
                        <div class="remember-psw-rt"><a href="/User/ForgotPassword">Forgot password?</a></div>
                    </div>
                    <div class="btn-outer">
                        <input type="submit" name="Sign In" value="Sign In" class="blue-btn"  id="login_button"/>
                        <!--<input  type="button" value="Cancel" class="grey-btn" id="cancel"/>-->
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>
<?php include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php"); ?>

<script>var staticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Vehicledummy.png">';</script>
<script>var petStaticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">';</script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>

<script>
    $(document).ready(function(){
        $("#portal-login").validate({
            rules: {
                email: {
                    required:true,
                },
                confirm_password: {
                    required:true,
                    equalTo: "#new_password"
                }
            },
            submitHandler: function (e) {

                var form = $('#portal-login')[0];
                var formData = new FormData(form);
                formData.append('action','portalLogin');
                formData.append('class','TenantPortal');
                $.ajax({
                    url:'/tenantPortal',
                    type: 'POST',
                    data: formData,
                    success: function (response) {
                        var response = JSON.parse(response);
                        if(response.status=='success') {
                            var user_id = response.user_id;
                            var url = response.portal_url;
                            toastr.success(response.message);
                            setTimeout(function () {
                                window.location.href =  window.location.origin+url
                            }, 1000);

                        } else {
                            toastr.error(response.message);
                        }

                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                });
            }
        });
    });
</script>