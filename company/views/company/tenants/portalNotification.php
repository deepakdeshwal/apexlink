<?php

if ((!isset($_SESSION[SESSION_DOMAIN]['Tenant_Portal']['tenant_portal_id'])) && (!isset($_SESSION['Admin_Access']['tenant_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/TenantPortal/login');
}
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
?>


<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_top_navigation.php"); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_portal_header.php"); ?>
<div id="wrapper">

    <div class="popup-bg"></div>
    <style>
        label.error {
            color: red !important;
            font-size: 12px;
            font-weight: 500;
            width: 100% !important;
        }

    </style>
    <script>
        var default_form_data = "";
        var defaultFormData = "";

    </script>
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">
                        <div class="content-data">
                            <div class="panel panel-default">
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <div class="panel-body pad-none">
                                        <div class="grid-outer">
                                            <div class="apx-table">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#">
                                                            <span class="pull-right"></span> Notifications</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body user-alerts">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-dark">
                                                                    <thead>
                                                                    <tr class="clsUserAlert">
                                                                        <th width="50%" align="left" scope="col">Title</th>
                                                                        <th width="50%" align="left" scope="col">Description</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="userNotification">
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Tabs End -->
                </div>
            </div>
        </div>
    </section>

    <div id="imageModel" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-4" id="getImage1"></div>
                    <div class="col-sm-4" id="getImage2"></div>
                    <div class="col-sm-4" id="getImage3"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <script>
        $(document).on("click",".clear-btn.email_clear",function () {

            resetFormClear('#add_owner_conversation_form_id',['postal_code','country','state','city'],'form',false);
        });$(document).on("click",".clear-btn.email_clearpass",function () {

            resetFormClear('#changePassword',[''],'form',false);
        });
    </script>
    <!-- Jquery Starts -->
    <script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
    <script>var default_Image = "<?php echo COMPANY_SITE_URL ?>/images/profile-img.png";</script>
    <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
    <script>var staticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Vehicledummy.png">';</script>
    <script>var petStaticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">';</script>
    <script type="text/javascript">var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";</script>
    <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>

    <script src="<?php echo COMPANY_SITE_URL; ?>/js/intlTelInput.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
    <!--    <script src="--><?php //echo COMPANY_SITE_URL; ?><!--/js/validation/custom_fields.js"></script>-->
    <!--    <script src="--><?php //echo COMPANY_SITE_URL; ?><!--/js/custom_fields.js"></script>-->
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.cropit.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantportal.js"></script>
    <script>var upload_url = "<?php echo SITE_URL; ?>"; var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";</script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/payments/plaidIntialisation.js"></script>

<!--js notification-->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/notification/notifiApex.js"></script>
<!--js notification-->
</div>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

