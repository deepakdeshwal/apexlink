
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */






?>





  <style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>



<input type="hidden" name="tenant_id" class="tenant_id" value="<?php echo $_GET['tenant_id']; ?>">
<input type="hidden" class="ticket_id" value="<?php echo $_GET['ticket_id']; ?>">


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_top_navigation.php");
?>
<div class="popup-bg"></div>
<div id="wrapper">
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_portal_header.php"); ?>

 <?php 
    if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id'])) {
     if((!isset($_SESSION['tenant_id'])))
    {
     header('Location: '.SUBDOMAIN_URL.'/TenantPortal/login');
    }

   }

?>
                    


        <section class="main-content">

               <div class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>View Ticket</h3>
                                                </div>
                                                <div class="form-data tenantAdditionalInfo" style="display:none;"></div>
                                                <div class="form-data additionalInfoData">
                                                    <div class="detail-outer">
                                                        <div class="col-sm-6">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Frequency :</label>
                                                                    <span class="frequency"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Ticket Number :</label>
                                                                    <span class="ticket_number"></span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Created On:
                                                                        :</label>
                                                                    <span class="created_on"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Category :</label>
                                                                    <span class="category"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Start Date :</label>
                                                                    <span class="start_date"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">End Date :</label>
                                                                    <span class="end_date"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Priority:</label>
                                                                    <span class="priority"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Estimated Cost:</label>
                                                                    <span class="estimate_cost"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Required Materials:</label>
                                                                    <span class="required_materials"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Vendor Instruction :</label>
                                                                    <span class="vendor_instruction"></span>
                                                                </div>
                                                                 <div class="col-xs-12">
                                                                    <label class="text-right">Description :</label>
                                                                    <span class="description"></span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                      
                                                    </div>

                                                </div>
                                            </div>

                                                 <div class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Property Information</h3>
                                                </div>
                                                <div class="form-data tenantAdditionalInfo" style="display:none;"></div>
                                                <div class="form-data additionalInfoData">
                                                    <div class="detail-outer">
                                                        <div class="col-sm-6">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Property:</label>
                                                                    <span class="property_name"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Unit:</label>
                                                                    <span class="building_unit"></span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Tenant Name:
                                                                        :</label>
                                                                    <span class="name"></span>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Phone:</label>
                                                                    <span class="phone"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Assign To :</label>
                                                                    <span class="name"></span>
                                                                </div>
                                                               
                                                            </div>

                                                        </div>
                                                        
                                                    </div>

                                                </div>
                                            </div>
                                                 <div class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Notes</h3>
                                                </div>
                                                
                                                <div class="form-data notes">
                                               

                                                </div>
                                            </div>
            <div role="tabpanel" class="tab-pane" id="tenant-detail-eight">
                                            <div class="col-sm-12 maintenancefiles_main">
                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>
                                                            Files
                                                        </h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="apx-table">
                                                            <form id="addMaintenanceFiles">

                                                                <div id="collapseSeventeen" class="panel-collapse in">
                                                             
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="maintenanceFiles-table" class="table table-bordered">
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <!-- Form Outer Ends -->
                                                            </form>
                                                           
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
              </section>



     




    <!-- Jquery Starts -->
    <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
    <script>var staticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Vehicledummy.png">';</script>
    <script>var petStaticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">';</script>
    <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
    <link rel="stylesheet" href="<?php echo COMPANY_SITE_URL; ?>/css/bootstrap-tagsinput.css" />
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/intlTelInput.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.cropit.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantportal.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/edit-maintenance.js"></script>
     <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/view-maintenance.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
     <script>var upload_url = "<?php echo SITE_URL; ?>";</script>

</div>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<script>

  $(".to").tagsinput('items');



 </script>