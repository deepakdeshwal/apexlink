<div class="container">
    <div class="modal fade" id="print_envelope" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="blue-btn" id='print_hoa'  onclick="PrintElem('#modal-body-hoa')" onclick="window.print();">Print</button>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body" id="modal-body-print-env">
                    <div class="col-sm-12">
                        <div class="col-sm-3 print_company_address">
                            <label>ApexLink Inc.</label>
                            <label>950114 Snow CT.</label>
                            <label>Marco Island, FL 34145</label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-5">&nbsp;</div>
                        <div class="col-sm-5 print_tenant_address">
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="modal fade" id="selectProperty" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">General Information</h4>
                </div>
                <div class="modal-body pad-none">
                    <form method="post" id="addPropertyForm">
                        <input type="hidden" class="propertyIddata" value="">
                        <div class="panel-body" style="border-color: transparent;">
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-3">
                                        <label>Property ID <em class="red-star">*</em> </label>
                                        <input name="property_id" placeholder="Auto Generated" id="autoGenProperty_id" class="form-control" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Property Name <em class="red-star">*</em></label>
                                        <input name="property_name" id="generalPropertyName" placeholder="Eg: The Fairmont Waterfront" class="form-control capsOn capital" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Legal Name</label>
                                        <input name="legal_name" id="generalLegalName" placeholder="Eg: The Fairmont Waterfront" class="form-control capsOn capital" type="text">
                                    </div>
                                    <div class="col-sm-3 add-category">
                                        <label>Portfolio Name <em class="red-star">*</em> <i class="fa fa-plus-circle NewportfolioPopup" id=""></i></label>
                                        <span id="dynamic_portfolio">
                                            <select name="portfolio_id" class='form-control'>
                                                <option value="">Select</option>
                                            </select>
                                        </span>
                                        <div class="add-popup" id="NewportfolioPopup" style="width: 246px;">
                                            <h4>Add New Portfolio</h4>
                                            <div class="add-popup-body">

                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>Portfolio ID<em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePortfoio" type="text" data_required="true" data_max="20" name="@portfolio_id" id="portfolio_id">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label> Portfolio Name <em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="50" name="@portfolio_name" id="portfolio_name" placeholder="Add New Portfolio Name">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="btn-outer text-right">
                                                        <button type="button" id="NewportfolioPopupSave" class="blue-btn" value="Save">Save</button>
                                                        <button type="button" class="clear-btn" value="Clear">Clear</button>
                                                        <button type="button" class="grey-btn" value="Cancel">Cancel</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Zip / Postal Code</label>
                                        <input name="zipcode" value="10001" id="propertyZipcode" class="form-control p-number-format" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Country</label>
                                        <input name="country" id="propertyCountry_name" class="form-control" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>State / Province</label>
                                        <input name="state" id="propertyState_name" class="form-control" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>City</label>
                                        <input name="city" id="propertyCity_name" class="form-control" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Street Address 1</label>
                                        <input name="address1" placeholder="Eg: Street address 1" class="form-control capital" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Street Address 2</label>
                                        <input name="address2" placeholder="Eg: Street address 2" class="form-control capital" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Street Address 3</label>
                                        <input name="address3" placeholder="Eg: Street address 3" class="form-control capital" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Street Address 4</label>
                                        <input name="address4" placeholder="Eg: Street address 4" class="form-control capital" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Manager Name <i class="fa fa-plus-circle" id="Newmanager"></i></label>
                                        <span id="dynamic_manager">
                                             <select name="manager_id[]" id="select_mangers_options" class="form-control select_manager" multiple="multiple">
                                            </select>
                                        </span>
                                        <div class="add-popup" id="NewmanagerPopup" style="width: 247px;">
                                            <h4>Add New Manager</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>First Name<em class="red-star">*</em></label>
                                                        <input class="form-control customValidateManager capital" type="text" data_required="true" data_max="30" name="@first_name">
                                                        <span class="customError required" aria-required="true"></span>
                                                        <span class="first_nameErr error"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label> Last Name <em class="red-star">*</em></label>
                                                        <input class="form-control customValidateManager capital" type="text" data_required="true" data_max="30" name="@last_name">
                                                        <span class="customError required" aria-required="true"></span>
                                                        <span class="last_nameErr error"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label> Email <em class="red-star">*</em></label>
                                                        <input class="form-control customValidateManager capital" type="text" data_required="true" data_max="50" data_email="true" name="@email">
                                                        <span class="customError required" aria-required="true"></span>
                                                        <span class="emailErr error"></span>
                                                    </div>
                                                    <div class="btn-outer">
                                                        <button type="button" id="NewmanagerPopupSave" class="blue-btn">Save</button>
                                                        <button type="button" class="clear-btn">Clear</button>
                                                        <button type="button" class="grey-btn" value="Cancel">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Property Price</label>
                                        <input name="property_price" placeholder="547895" class="form-control p-number-format" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Attach Group <i class="fa fa-plus-circle NewattachPopup"></i></label>
                                        <span id="dynamic_groups">
                                            <select name="attach_groups[]" class="form-control attach_groups" multiple="multiple">
                                            </select>
                                        </span>
                                        <div class="add-popup" id="NewattachPopup" style="width: 246px;">
                                            <h4>Add New Property Group</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>Group Name<em class="red-star">*</em></label>
                                                        <input class="form-control customValidateGroup1 capital" type="text" data_required="true" data_max="150" name="@group_name">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label> Descrition <em class="red-star">*</em></label>
                                                        <input class="form-control customValidateGroup1 capital" type="text" data_required="true" data_max="150" name="@description">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="btn-outer">
                                                        <button type="button" class="blue-btn" id="NewattachPopupSave">Save</button>
                                                        <button type="button" class="clear-btn">Clear</button>
                                                        <button type="button" class="grey-btn" value="Cancel">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Property Type <em class="red-star">*</em> <i class="fa fa-plus-circle property_type_options"></i></label>
                                        <span id="dynamic_property_type">
                                            <select name="property_type" class="form-control property_type" >
                                                <option value="0">Select</option>
                                            </select>
                                        </span>
                                        <div class="add-popup" id="NewtypePopup" style="display: block; width: 246px;">
                                            <h4>Add New Property Type</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>Property Type<em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePropertyType capital" data_required="true" data_max="150" type="text" name="@property_type">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label> Description <em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePropertyType capital" data_required="true" data_max="200" type="text" name="@description">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="btn-outer">
                                                        <button type="button" class="blue-btn" id="NewtypePopupSave">Save</button>
                                                        <button type="button" class="clear-btn">Clear</button>
                                                        <button type="button" class="grey-btn" value="Cancel">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Property Style <i class="fa fa-plus-circle Newprofile"></i></label>
                                        <span id="dynamic_property_style">
                                             <select name="property_style" class="form-control property_style" id="property_style_options" >
                                                <option value="0">Select</option>
                                            </select>
                                        </span>
                                        <div class="add-popup" id="NewpstylePopup" style="width: 127%;">
                                            <h4>Add New Property Style</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>Property Style <em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePropertyStyle capital" data_required="true" data_max="150" type="text" name="@property_style">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label>Description <em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePropertyStyle capital" data_required="true" data_max="200" type="text" name="@description">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="btn-outer text-right">
                                                        <button type="button" id="NewpstylePopupSave" class="blue-btn">Save</button>
                                                        <button type="button" class="clear-btn">Clear</button>
                                                        <button type="button" class="grey-btn" value="Cancel">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Property Sub-Type <i class="fa fa-plus-circle Newsubtype"></i></label>
                                        <span id="dynamic_property_subtype">
                                            <select name="property_subtype" class="form-control property_subtype" id="property_subtype_options">
                                                <option value="0">Select</option>
                                            </select>
                                        </span>
                                        <div class="add-popup" id="NewsubtypePopup" style="width: 127%;">
                                            <h4>Add New Property Sub-Type</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>Property Sub-Type<em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePropertySubType capital" data_required="true" data_max="150" type="text" name="@property_subtype">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label> Description <em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePropertySubType capital" data_required="true" data_max="200" type="text" name="@description">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="btn-outer text-right">
                                                        <button type="button" class="blue-btn" id="NewsubtypePopupSave">Save</button>
                                                        <button type="button" class="clear-btn">Clear</button>
                                                        <button type="button" class="grey-btn" value="Cancel">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Year Built</label>
                                        <!-- <input name="property_year" class="form-control" type="text"> -->

                                        <select name="property_year" class="form-control">
                                            <option value="0">(Select Year Built)</option>
                                            <?php
                                            for($i = 1990; $i <= 2018; $i++){
                                                echo '<option value="'.$i.'">'.$i.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Sq.ft</label>
                                        <input name="property_squareFootage" placeholder="Eg: 100" class="form-control p-number-format" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Number Of Buildings <em class="red-star">*</em> </label>
                                        <input name="no_of_buildings" placeholder="EG: 1" value="1" class="form-control p-number-format" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Number Of Units <em class="red-star">*</em> </label>
                                        <input name="no_of_units" placeholder="EG: 1" value="1" class="form-control p-number-format" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Smoking Allowed</label>
                                        <select name="smoking_allowed" class='form-control'>
                                            <option value='0' selected>No</option>
                                            <option value='1'>Yes</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Pet Friendly</label>
                                        <span id="dynamic_pet_condition">
                                            <select class="form-control" id="PetsAllowed" name="pet_friendly">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </span>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="">
                                                <div class="col-sm-6">
                                                    <label>Amenities <i class="fa fa-plus-circle Newamenities"></i></label>
                                                    <div class="check-outer amenties_box" style="height: 110px;overflow: auto;width: 100%;">
                                                        <div class="check-outer-col2 col-sm-4 col-md-4" style="padding: 0">
                                                            <input class="clsAmenityCheckboxMain all check-all-amenities" type="checkbox" >Select All</div>
                                                        <div id="dynamic_amenity"></div>
                                                    </div>
                                                    <div class="add-popup" id="NewamenitiesPopup" style="width: 60%;" >
                                                        <h4>Add New Amenity</h4>
                                                        <div class="add-popup-body">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12">
                                                                    <label>New Amenity Code<em class="red-star">*</em></label>
                                                                    <input class="form-control customValidateAmenities capital" data_required="true" data_max="15" type="text" name="@code">
                                                                    <span class="customError required" aria-required="true"></span>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <label> Amenity Name <em class="red-star">*</em></label>
                                                                    <input class="form-control customValidateAmenities capital" data_required="true" data_max="50" type="text" name="@name">
                                                                    <span class="customError required" aria-required="true"></span>
                                                                </div>
                                                                <div class="btn-outer text-right">
                                                                    <button type="button" class="blue-btn" id="NewamenitiesPopupSave">Save</button>
                                                                    <button type="button" class="clear-btn clearAmenity">Clear</button>
                                                                    <button type="button" class="grey-btn" value="Cancel">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label>Description</label>
                                                    <div class="notes_date_right_div">
                                                    <textarea name="description" class="form-control capital notes_date_right" type="text" rows="4" maxlength="500"></textarea>
                                                    </div>
                                                    </div>
                                            </div>
                                            <div class="mb-15 checkbox-outer" style="margin-top: 20px;">

                                                    <input name="default_building_unit" cstyle="margin-right: 5px;" type="checkbox" class="NoBuilding">

                                                <label>Property With No Building/Unit</label>
                                            </div>
                                            <div class="col-sm-12 details" style="display: none;">
                                                <label>Details</label>
                                                <div class="check-outer" type="text">
                                                    <div class="col-sm-3">
                                                        <label>Select Unit Type <em class="red-star">*</em> <i class="fa fa-plus-circle AddNewUnitTypeModalPlus2"></i></label>
                                                        <span id="dynamic_unit_type">
                                                            <select class="form-control unit_type" id="unit_type" name="unit_type">
                                                                <option value="">Select</option>
                                                            </select>
                                                        </span>
                                                        <div class="add-popup" id="NewunitPopup">
                                                            <h4>Add New Unit Type</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>Unit Type<em class="red-star">*</em></label>
                                                                        <input class="form-control customValidateUnitType" data_required="true" data_max="150" type="text" name="@unit_type">
                                                                        <span class="customError required" aria-required="true"></span>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <label>Description</label>
                                                                        <input class="form-control customValidateUnitType" type="text" maxlength="500" name="@description">
                                                                        <span class="customError required" aria-required="true"></span>
                                                                    </div>
                                                                    <div class="btn-outer">
                                                                        <button type="button" class="blue-btn" id="NewunitPopupSave" value="Save">Save</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Base Rent
                                                            <em class="red-star">*</em> </label>
                                                        <input placeholder="Eg: 2000" class="form-control p-number-format" type="text" name="base_rent" >
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Market Rent
                                                            <em class="red-star">*</em> </label>
                                                        <input placeholder="Eg: 2000" class="form-control p-number-format" type="text" name="market_rent">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Security Deposit
                                                            <em class="red-star">*</em> </label>
                                                        <input placeholder="Eg: 2000" class="form-control p-number-format" type="text" name="security_deposit" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="btn-outer text-right">
                                                <button type="submit" class="blue-btn" value="Save" id="AddGeneralInformationButton" placeholder="Save">Save</button>
                                                <a class="propertyCancel clear-btn clear_property">Clear</a>
                                                <button type="button" data-dismiss="modal" id="AddGeneralInformationCancel" class="propertyCancel grey-btn">Cancel</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- </div>
                </div> -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectBuilding" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Building</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <form method="post" action="" id="addBuildingForm" enctype="multipart/form-data">
                            <input type="hidden" name="tenantbuildingpopup" value="1">
                            <div class="panel-body pad-none" >
                                <div class="col-sm-12 pad-none">
                                    <div class="form-outer">
                                        <div class="div-full pad-none">
                                            <div class="col-sm-3">
                                                <label>Property <em class="red-star">*</em> </label>
                                                <select name="property_id" class="form-control">
                                                    <option value="">Select Property</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Building ID <em class="red-star">*</em> </label>
                                                <input name="building_id" class="form-control" value='' type="text">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Legal Name</label>
                                                <input name="legal_name" id="legal_name" placeholder="Eg: The Fairmont Waterfront" class="form-control capsOn" type="text">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Building Name <em class="red-star">*</em></label>
                                                <input name="building_name" id="building_name" placeholder="Eg: The Fairmont Waterfront" class="form-control capsOn" type="text" value="">
                                            </div>
                                        </div>
                                        <div class="div-full pad-none">
                                            <div class="col-sm-3">
                                                <label>Number Of Units <em class="red-star">*</em> </label>
                                                <input id="no_of_units" name="no_of_units"class="form-control" type="text" value="1">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Building Address <em class="red-star">*</em></label>
                                                <input name="address" placeholder="Eg: 8 Avenue" class="form-control capital" type="text">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Smoking Allowed</label>
                                                <select name="smoking_allowed" class='form-control' value="1" readonly="true">
                                                    <option value=''>Select</option>
                                                    <option value='1'>Yes</option>
                                                    <option value='0'>No</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Pet Friendly</label>
                                                <select class="form-control" id="PetsAllowed" name="pet_friendly">
                                                    <option value=''>Select</option>
                                                    <option value='1'>Yes</option>
                                                    <option value='0'>No</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="div-full">
                                            <div class="row">
                                                <div class="col-sm-6 school-duistrict">
                                                    <label>Amenities <i class="fa fa-plus-circle" data-toggle="modal" data-target="#AddNewAmenityModal" aria-hidden="true"></i></label>
                                                    <div class="check-outer" style="height: 110px;overflow: auto;width: 100%;" id="amenities_checkbox">
                                                        <div class="check-outer-col2 col-sm-4 col-md-4" style="min-height:40px; padding: 0;">
                                                            <input class="clsAmenityCheckboxMain capital" id="select_all" type="checkbox" > Select All
                                                        </div>
                                                        <div id="dynamic_amenity" class="dynamic_amenity"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label>Building Description</label>
                                                    <div class="notes_date_right_div">
                                                    <textarea name="description" placeholder="This is a family friendly building with onsite laundary. Suites are newly renovated with balconies" class="form-control capital notes_date_right" type="text" rows="4" maxlength="500"></textarea>
                                                    </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="div-full row">

                                            <div class="checkbox-outer school-duistrict">

                                                    <input name="default_building_unit" cstyle="margin-right: 5px;" type="checkbox" class="NoBuilding">

                                                <label>No seprate unit</label>
                                            </div>
                                            <div class="col-sm-10 details" style="display: none;">
                                                <label>Details</label>
                                                <div class="check-outer" type="text">
                                                    <div class="col-sm-3">
                                                        <label>Select Unit Type <em class="red-star">*</em> <i class="fa fa-plus-circle" data-toggle="modal" data-target="#AddNewUnitTypeModal" aria-hidden="true"></i></label>
                                                        <span id="dynamic_unit_type">
                                                    <select class="form-control" id="" name="unit_type">
                                                        <option value="">Select</option>
                                                    </select>
                                                </span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Base Rent
                                                            <em class="red-star">*</em> </label>
                                                        <input placeholder="Eg: 2000" class="form-control" type="text" name="base_rent" >
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Market Rent
                                                            <em class="red-star">*</em> </label>
                                                        <input placeholder="Eg: 2000" class="form-control" type="text" name="market_rent">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Security Deposit
                                                            <em class="red-star">*</em> </label>
                                                        <input placeholder="Eg: 2000" class="form-control" type="text" name="security_deposit" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="div-full">
                                                <div class="btn-outer text-right" >
                                                    <a class="blue-btn savebuildingpopup">Save</a>
                                                    <a class="buildingCancel clear-btn">Clear</a>
                                                    <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectUnit" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Unit</h4>
                </div>
                <div class="modal-body">
                    <form id="addUnitForm" enctype="multipart/form-data">
                        <div class="panel-body pad-none">
                            <div class="row">
                                <div class="form-outer form-data unit_form_data" id="input-form" style="border: none;">
                                    <div class=" blue-bg-outer unit-blueouter">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Select Property<em class="red-star">*</em></label>
                                                <span>
                                                    <select id="property_id" name="property_id" class="form-control" style="cursor: pointer;">
                                                        <option value="">Select</option>
                                                    </select>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Select Building<em class="red-star">*</em></label>
                                                <span>
                                                    <select id="ddlBuildings" name="building_id[]" class="form-control" style="cursor: pointer;">
                                                        <option value="">Select</option>
                                                    </select>
                                                    <span class="required_star" id="building_id_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Floor No.<em class="red-star">*</em></label>
                                                <span><input type="text" id="txtFloors" value="" name="floor_no[]" placeholder="Eg: 1" maxlength="3" class="form-control" spellcheck="true">
                                                    <span class="required_star" id="floor_no_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Unit Prefix</label>
                                                <span>
                                                    <input type="text" id="txtPrefix" name="unit_prefix[]" placeholder="Eg: A" maxlength="10" class="form-control" spellcheck="true">
                                                     <span class="required_star" id="unit_prefix_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Unit Number<em class="red-star">*</em></label>
                                                <span>
                                                    <input type="text" id="txtUnitNumber" name="unit_no[]" maxlength="7" value="" placeholder="Eg: 100" class="form-control" spellcheck="true">
                                                    <span class="required_star" id="unit_no_errmain"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row p-0">
                                        <div class="div-full pad-none">
                                            <div class="col-sm-3">
                                                <label>Select Unit Type <em class="red-star">*</em>
                                                    <i class="fa fa-plus-circle" data-toggle="modal" data-target="#AddNewUnitModal" aria-hidden="true"></i></label>
                                                <span>
                                                    <select name="unitTypeID" class="form-control" id="ddlUnitType2" name="unit_type_id" class="" style="cursor: pointer;">
                                                        <option value="default">Select Unit Type</option>
                                                    </select>
                                                    <span class="required_star" id="unit_type_id_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label id="lblSquareFootage">Total Square Footage</label>
                                                <span>
                                                    <input id="txtTotalSqFootage" type="number" placeholder="Eg: 200.00" name="square_ft" maxlength="7" class="form-control" spellcheck="true">
                                                  <span class="required_star" id="square_ft_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>No. Of Bedrooms<em class="red-star">*</em></label>
                                                <span>
                                                    <select name="bedrooms_no" id="ddlBedRoom" class="form-control" style="cursor: pointer; background-color: rgb(255, 255, 255);">
                                                        <?php
                                                        for($i=1; $i<=100; $i++) {
                                                            echo '<option value="'.$i.'" text="'.$i.'">'.$i.'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>No. Of Bathrooms<em class="red-star">*</em></label>
                                                <span>
                                                    <select name="bathrooms_no" id="ddlBathRoom" class="form-control" style="cursor: pointer; background-color: rgb(255, 255, 255);"><option value="1" text="1">1</option><option value="2" text="1½">1½</option><option value="3" text="2">2</option><option value="4" text="2½">2½</option><option value="5" text="3">3</option><option value="6" text="3½">3½</option><option value="7" text="4">4</option><option value="8" text="4½">4½</option><option value="9" text="5">5</option><option value="10" text="5½">5½</option><option value="11" text="6">6</option><option value="12" text="6½">6½</option><option value="13" text="7">7</option><option value="14" text="7½">7½</option><option value="15" text="8">8</option><option value="16" text="8½">8½</option><option value="17" text="9">9</option><option value="18" text="9½">9½</option><option value="19" text="10">10</option><option value="20" text="10½">10½</option><option value="21" text="11">11</option><option value="22" text="11½">11½</option><option value="23" text="12">12</option><option value="24" text="12½">12½</option><option value="25" text="13">13</option><option value="26" text="13½">13½</option><option value="27" text="14">14</option><option value="28" text="14½">14½</option><option value="29" text="15">15</option><option value="30" text="15½">15½</option><option value="31" text="16">16</option><option value="32" text="16½">16½</option><option value="33" text="17">17</option><option value="34" text="17½">17½</option><option value="35" text="18">18</option><option value="36" text="18½">18½</option><option value="37" text="19">19</option><option value="38" text="19½">19½</option><option value="39" text="20">20</option><option value="40" text="20½">20½</option><option value="41" text="21">21</option><option value="42" text="21½">21½</option><option value="43" text="22">22</option><option value="44" text="22½">22½</option><option value="45" text="23">23</option><option value="46" text="23½">23½</option><option value="47" text="24">24</option><option value="48" text="24½">24½</option><option value="49" text="25">25</option><option value="50" text="25½">25½</option><option value="51" text="26">26</option><option value="52" text="26½">26½</option><option value="53" text="27">27</option><option value="54" text="27½">27½</option><option value="55" text="28">28</option><option value="56" text="28½">28½</option><option value="57" text="29">29</option><option value="58" text="29½">29½</option><option value="59" text="30">30</option><option value="60" text="30½">30½</option><option value="61" text="31">31</option><option value="62" text="31½">31½</option><option value="63" text="32">32</option><option value="64" text="32½">32½</option><option value="65" text="33">33</option><option value="66" text="33½">33½</option><option value="67" text="34">34</option><option value="68" text="34½">34½</option><option value="69" text="35">35</option><option value="70" text="35½">35½</option><option value="71" text="36">36</option><option value="72" text="36½">36½</option><option value="73" text="37">37</option><option value="74" text="37½">37½</option><option value="75" text="38">38</option><option value="76" text="38½">38½</option><option value="77" text="39">39</option><option value="78" text="39½">39½</option><option value="79" text="40">40</option><option value="80" text="40½">40½</option><option value="81" text="41">41</option><option value="82" text="41½">41½</option><option value="83" text="42">42</option><option value="84" text="42½">42½</option><option value="85" text="43">43</option><option value="86" text="43½">43½</option><option value="87" text="44">44</option><option value="88" text="44½">44½</option><option value="89" text="45">45</option><option value="90" text="45½">45½</option><option value="91" text="46">46</option><option value="92" text="46½">46½</option><option value="93" text="47">47</option><option value="94" text="47½">47½</option><option value="95" text="48">48</option><option value="96" text="48½">48½</option><option value="97" text="49">49</option><option value="98" text="49½">49½</option><option value="99" text="50">50</option><option value="100" text="50½">50½</option><option value="101" text="51">51</option><option value="102" text="51½">51½</option><option value="103" text="52">52</option><option value="104" text="52½">52½</option><option value="105" text="53">53</option><option value="106" text="53½">53½</option><option value="107" text="54">54</option><option value="108" text="54½">54½</option><option value="109" text="55">55</option><option value="110" text="55½">55½</option><option value="111" text="56">56</option><option value="112" text="56½">56½</option><option value="113" text="57">57</option><option value="114" text="57½">57½</option><option value="115" text="58">58</option><option value="116" text="58½">58½</option><option value="117" text="59">59</option><option value="118" text="59½">59½</option><option value="119" text="60">60</option><option value="120" text="60½">60½</option><option value="121" text="61">61</option><option value="122" text="61½">61½</option><option value="123" text="62">62</option><option value="124" text="62½">62½</option><option value="125" text="63">63</option><option value="126" text="63½">63½</option><option value="127" text="64">64</option><option value="128" text="64½">64½</option><option value="129" text="65">65</option><option value="130" text="65½">65½</option><option value="131" text="66">66</option><option value="132" text="66½">66½</option><option value="133" text="67">67</option><option value="134" text="67½">67½</option><option value="135" text="68">68</option><option value="136" text="68½">68½</option><option value="137" text="69">69</option><option value="138" text="69½">69½</option><option value="139" text="70">70</option><option value="140" text="70½">70½</option><option value="141" text="71">71</option><option value="142" text="71½">71½</option><option value="143" text="72">72</option><option value="144" text="72½">72½</option><option value="145" text="73">73</option><option value="146" text="73½">73½</option><option value="147" text="74">74</option><option value="148" text="74½">74½</option><option value="149" text="75">75</option><option value="150" text="75½">75½</option><option value="151" text="76">76</option><option value="152" text="76½">76½</option><option value="153" text="77">77</option><option value="154" text="77½">77½</option><option value="155" text="78">78</option><option value="156" text="78½">78½</option><option value="157" text="79">79</option><option value="158" text="79½">79½</option><option value="159" text="80">80</option><option value="160" text="80½">80½</option><option value="161" text="81">81</option><option value="162" text="81½">81½</option><option value="163" text="82">82</option><option value="164" text="82½">82½</option><option value="165" text="83">83</option><option value="166" text="83½">83½</option><option value="167" text="84">84</option><option value="168" text="84½">84½</option><option value="169" text="85">85</option><option value="170" text="85½">85½</option><option value="171" text="86">86</option><option value="172" text="86½">86½</option><option value="173" text="87">87</option><option value="174" text="87½">87½</option><option value="175" text="88">88</option><option value="176" text="88½">88½</option><option value="177" text="89">89</option><option value="178" text="89½">89½</option><option value="179" text="90">90</option><option value="180" text="90½">90½</option><option value="181" text="91">91</option><option value="182" text="91½">91½</option><option value="183" text="92">92</option><option value="184" text="92½">92½</option><option value="185" text="93">93</option><option value="186" text="93½">93½</option><option value="187" text="94">94</option><option value="188" text="94½">94½</option><option value="189" text="95">95</option><option value="190" text="95½">95½</option><option value="191" text="96">96</option><option value="192" text="96½">96½</option><option value="193" text="97">97</option><option value="194" text="97½">97½</option><option value="195" text="98">98</option><option value="196" text="98½">98½</option><option value="197" text="99">99</option><option value="198" text="99½">99½</option><option value="199" text="100">100</option><option value="200" text="100½">100½</option><option value="201" text="Other">Other</option></select>
                                                    <span class="required_star" id="bathrooms_no_errmain"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="div-full pad-none">
                                            <div class="col-sm-3">
                                                <label id="changeLblAddBaseRentCurrency">Base Rent
                                                    <em class="red-star">*</em></label>
                                                <span>
                                                    <input type='text' id="txtBaseRent" placeholder="Eg: 200.00" name="baseRent" class="form-control amount number_only" value="<?php echo $_SESSION[SESSION_DOMAIN]['default_rent']; ?>">
                                                   <span class="pre-span-text2" style="display: none"><?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?></span> <span class="required_star" id="base_rent_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label id="changeLblAddMarketRentCurrency">Market Rent
                                                    <em class="red-star">*</em></label>
                                                <span>
                                                    <input type="text" id="txtMarketRent" placeholder="Eg: 5000" name="market_rent" class="form-control amount number_only" value="<?php echo $_SESSION[SESSION_DOMAIN]['default_rent']; ?>">
                                                   <span class="pre-span-text2" style="display: none"><?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?></span> <span class="required_star" id="market_rent_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label id="changeLblAddSdCurrency">Security Deposit
                                                    <em class="red-star">*</em></label>
                                                <span>
                                                    <input type="text" placeholder="Eg: 200.00" id="txtSecurityDeposit" name="securityDeposit" maxlength="8" class="form-control amount number_only" spellcheck="true">
                                                    <span class="pre-span-text2" style="display: none"><?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Smoking Allowed</label>
                                            <span>
                                                <select placeholder="Eg: Yes" class="form-control" id="NonSmokingUnit" name="smoking_allowed">
                                                    <option value="">Select</option>
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </span>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Pet Friendly</label>
                                            <span>
                                                <select placeholder="Eg: Yes" class="form-control" id="pet_friendly_id" name="pet_friendly_id">
                                                    <option value="">Select</option>
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </span>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Status</label>
                                            <span>
                                                <select name="building_unit_status" id="ddlStatus" class="form-control" style="cursor: pointer;"><option value="1">Vacant Available</option><option value="2">Unrentable</option><option value="4">Occupied</option><option value="5">Notice Available</option><option value="6">Vacant Rented</option><option value="7">Notice Rented</option><option value="8">Under Make Ready</option></select>
                                            </span>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Add Notes</label>
                                            <span>
                                                 <div class="notes_date_right_div">
                                                <textarea id="txtNotes" placeholder="Eg: Add Notes" name="building_unit_notes" maxlength="5000" class="textarea form-control capital notes_date_right" rows="3" cols="40" style="resize: none; height: 100px;" spellcheck="true"></textarea>
                                                 </div>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: inherit;">
                                        <div class="col-sm-6 school-duistrict">
                                            <label>Amenities<i class="fa fa-plus-circle" data-toggle="modal" data-target="#AddNewAmenityModal" aria-hidden="true"></i></label>
                                            <span>
                                                <div id="AmenityList" class="check-outer" style="height: 94px; overflow: auto; width: 98%;">
                                                    <div class="check-outer-col2 cols-sm-4 col-md-4" style="padding: 0">
                                                        <input class="clsAmenityCheckboxMain" id="select_all" type="checkbox" >Select All
                                                    </div>
                                                    <div id="dynamic_amenity" class="dynamic_amenity">
                                                    </div>
                                                </div>
                                            </span>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Description</label>
                                            <span>
                                            <div class="notes_date_right_div">
                                                <textarea id="txtDescription"  style="width: 101%;" name="building_description" class="textarea4 capital notes_date_right" maxlength="500" cols="40" rows="5" style="resize: none;" spellcheck="true"></textarea>
                                           </div>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="btn-outer text-right">
                                        <a class="blue-btn saveunitpopup" value="Save">Save</a>
                                        <a class="unitCancel clear-btn">Clear</a>
                                        <a id="addPropertyTypeCancel" class="grey-btn" value="Cancel" data-dismiss="modal">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addprotfolio" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Portfolio</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Portfolio ID<span class="required"> * </span></label>
                                    <input class="form-control" type="text" name="" placeholder="Auto Generated">
                                </div>
                                <div class="col-sm-12">
                                    <label>Portfolio Name<span class="required"> * </span></label>
                                    <input class="form-control capsOn" type="text" name="" placeholder="Add New Portfolio Name">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <a class="blue-btn" id=''>Save</a>
                                <a class="black-btn" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="tenantprintenvelope" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <input type="button" class="printenvelopbtn blue-btn" value="Print">
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <div class="col-sm-4">
                            <div class="col-sm-12 cinfo"></div>
                        </div>
                    </div>
                    <div class="col-sm-12"><p>&nbsp;</p></div>
                    <div class="col-sm-12">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4">
                            <div class="col-sm-12 tinfo"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectContact" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Contact Information</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <form name="" method="post">
                            @csrf
                            <div class="form-outer">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Salutation</label>
                                        <select name="" class="form-control">
                                            <option value="0">Select</option>
                                            <option value="1">Dr.</option>
                                            <option value="2">Mr.</option>
                                            <option value="3">Mrs.</option>
                                            <option value="4">Mr. &amp; Mrs.</option>
                                            <option value="5">Ms.</option>
                                            <option value="6">Sir.</option>
                                            <option value="7">Madam</option>
                                            <option value="8">Brother</option>
                                            <option value="9">Sister</option>
                                            <option value="10">Father</option>
                                            <option value="11">Mother</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>First Name<span class="required"> * </span></label>
                                        <input class="form-control capsOn" type="text" placeholder="First Name">
                                    </div>
                                    <div class="col-sm-1">
                                        <label>MI</label>
                                        <input class="form-control capsOn" type="text" name="" placeholder="MI" maxlength="1">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Last Name<span class="required"> * </span></label>
                                        <input class="form-control capsOn" type="text" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Maiden Name</label>
                                        <input class="form-control capsOn" type="text" placeholder="Maiden Name">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Nick Name</label>
                                        <input class="form-control capsOn" type="text" placeholder="Nick Name">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Gender</label>
                                        <select name="" class="form-control">
                                            <option value="0">Select</option>
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                            <option value="3">Male/Female</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Entity/Company Name</label>
                                        <input class="form-control capsOn" type="text" name="" placeholder="Company">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Zip / Postal Code</label>
                                        <input type="text" name="" class="form-control" placeholder="Ex: 85301">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Country</label>
                                        <input type="text" name="" class="form-control capsOn" placeholder="Country">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>State / Province</label>
                                        <input type="text" name="" class="form-control capsOn" placeholder="State">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>City</label>
                                        <input type="text" name="" class="form-control capsOn" placeholder="City">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Address1</label>
                                        <input type="text" name="" class="form-control" placeholder="Ex: Street Address 1">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Address2</label>
                                        <input type="text" name="" class="form-control" placeholder="Ex: Street Address 2">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Address3</label>
                                        <input type="text" name="" class="form-control" placeholder="Ex: Street Address 3">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Address4</label>
                                        <input type="text" name="" class="form-control" placeholder="Ex: Street Address 4">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Email</label>
                                        <input type="text" name="" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <div class="btn-outer">
                                    <a class="blue-btn" id=''>Save</a>
                                    <a class="black-btn cancelbtn">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addmanager1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Manager</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>First Name<span class="required"> * </span></label>
                                    <input class="form-control capsOn" type="text" name="" placeholder="First Name">
                                </div>
                                <div class="col-sm-12">
                                    <label>Last Name</label>
                                    <input class="form-control capsOn" type="text" name="" placeholder="Last Name">
                                </div>
                                <div class="col-sm-12">
                                    <label>Email<span class="required"> * </span></label>
                                    <input class="form-control" type="text" name="" placeholder="Email">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <a class="blue-btn" id=''>Save</a>
                                <a class="black-btn" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showvehiclesimages" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"></h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="row">
                            <div class="col-sm-12"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showhoasimages" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"></h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="row">
                            <div class="col-sm-12"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showpetsimages" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"></h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="row">
                            <div class="col-sm-12"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showanimalsimages" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"></h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="row">
                            <div class="col-sm-12"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showvehiclesimagesbig" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Image Preview</h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="col-sm-12">
                            <img src="" class="showbigvehicle" style="width: 100%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showhoasimagesbig" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Image Preview</h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="col-sm-12">
                            <img src="" class="showbighoa" style="width: 100%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showpetsimagesbig" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Image Preview</h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="col-sm-12">
                            <img src="" class="showbigpet" style="width: 100%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showanimalsimagesbig" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Image Preview</h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="col-sm-12">
                            <img src="" class="showbiganimal" style="width: 100%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addgroup1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Property Group</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Group Name<span class="required"> * </span></label>
                                    <input class="form-control capsOn" type="text" name="" placeholder="Add New Group Name">
                                </div>
                                <div class="col-sm-12">
                                    <label>Description</label>
                                    <input class="form-control" type="text" name="" placeholder="Description">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="black-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addpropertytype1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Property Type</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Property Type<span class="required"> * </span></label>
                                    <input class="form-control capsOn" type="text" name="" placeholder="Add New Property Type">
                                </div>
                                <div class="col-sm-12">
                                    <label>Description</label>
                                    <input class="form-control" type="text" name="" placeholder="Description">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="black-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addpropertytype1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Property Style</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Property Style<span class="required"> * </span></label>
                                    <input class="form-control capsOn" type="text" name="" placeholder="Property Style Name">
                                </div>
                                <div class="col-sm-12">
                                    <label>Description</label>
                                    <input class="form-control" type="text" name="" placeholder="Description">
                                </div>
                            </div>
                            <div class="btn-outer text-right">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="black-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addpropertystyle1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Property Style</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Property Style<span class="required"> * </span></label>
                                    <input class="form-control capsOn" type="text" name="" placeholder="Property Style Name">
                                </div>
                                <div class="col-sm-12">
                                    <label>Description</label>
                                    <input class="form-control" type="text" name="" placeholder="Description">
                                </div>
                            </div>
                            <div class="btn-outer text-right">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="black-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addpropertysubtype1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Property Sub-Type</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Property Sub-Type<span class="required"> * </span></label>
                                    <input class="form-control capsOn" type="text" name="" placeholder="Add New Property Sub-Type">
                                </div>
                                <div class="col-sm-12">
                                    <label>Description</label>
                                    <input class="form-control" type="text" name="" placeholder="Description">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="black-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addamenities1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Amenity</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>New Amenity Code</label>
                                    <input class="form-control" type="text" name="" placeholder="Ex: 12345 (optional)">
                                </div>
                                <div class="col-sm-12">
                                    <label>Amenity Name <span class="required"> * </span></label>
                                    <input class="form-control capsOn capital" type="text" name="" placeholder="Add New Amenity Name">
                                </div>
                            </div>
                            <div class="btn-outer text-right">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="clear-btn">Clear</button>
                                <button type="button" class="black-btn cancelbtn">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addunittype1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Unit Type</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Unit Type <span class="required"> * </span></label>
                                    <input class="form-control capsOn" type="text" name="" placeholder="Ex: 12345 (optional)">
                                </div>
                                <div class="col-sm-12">
                                    <label>Description</label>
                                    <input class="form-control" type="text" name="" placeholder="Description">
                                </div>
                            </div>
                            <div class="btn-outer text-right">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="black-btn cancelbtn">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyReferralResource" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Referral Source</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-outer" style="float: none;">
                            <label>New Referral Source <em class="red-star">*</em></label>
                            <input class="form-control reff_source" type="text" placeholder="New Referral Source">
                            <span class="red-star" id="reff_source"></span>
                            <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                                <a class="blue-btn add_single" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source" data-name="referralSource">Save</a>
                                <a class="clear-btn">Clear</a>
                                <a class="grey-btn" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="additionalReferralResource" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Referral Source</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New Referral Source <em class="red-star">*</em></label>
                        <input class="form-control reff_source1" type="text" placeholder="New Referral Source">
                        <span class="red-star" id="reff_source1"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source1" data-name="additional_referralSource">Save</a>
                            <a class="clear-btn">Clear</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyEthnicity" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Ethnicity</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New Ethnicity <em class="red-star">*</em></label>
                        <input class="form-control ethnicity_src" type="text" placeholder="Add New Ethnicity">
                        <span class="red-star" id="ethnicity_src"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_ethnicity" data-cell="title" data-class="ethnicity_src" data-name="ethncity">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="additionalEthnicity" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Ethnicity</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New Ethnicity <em class="red-star">*</em></label>
                        <input class="form-control ethnicity_src1" type="text" placeholder="Add New Ethnicity">
                        <span class="red-star" id="ethnicity_src1"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_ethnicity" data-cell="title" data-class="ethnicity_src1" data-name="additional_ethncity">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyMaritalStatus" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Marital Status</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New Marital Status<em class="red-star">*</em></label>
                        <input class="form-control maritalstatus_src" type="text" placeholder="Add New Marital Status">
                        <span class="red-star" id="maritalstatus_src"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single1" data-table="tenant_marital_status" data-cell="marital" data-class="maritalstatus_src" data-name="maritalStatus">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="additionalMaritalStatus" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Marital Status</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New Marital Status<em class="red-star">*</em></label>
                        <input class="form-control maritalstatus_src1" type="text" placeholder="Add New Marital Status">
                        <span class="red-star" id="maritalstatus_src1"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_marital_status" data-cell="marital" data-class="maritalstatus_src1" data-name="additional_maritalStatus">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyHobbies" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Hobbies</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New Hobbies <em class="red-star">*</em></label>
                        <input class="form-control hobbies_src" type="text" placeholder="Add New Hobbies">
                        <span class="red-star" id="hobbies_src"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="hobbies" data-cell="hobby" data-class="hobbies_src" data-name="hobbies">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="additionalHobbies" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Hobbies</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New Hobbies <em class="red-star">*</em></label>
                        <input class="form-control hobbies_src1" type="text" placeholder="Add New Hobbies">
                        <span class="red-star" id="hobbies_src1"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="hobbies" data-cell="hobby" data-class="hobbies_src1" data-name="additional_hobbies[]">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyVeteranStatus" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New VeteranStatus</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New VeteranStatus <em class="red-star">*</em></label>
                        <input class="form-control veteran_src" type="text" placeholder="Add New VeteranStatus">
                        <span class="red-star" id="veteran_src"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_veteran_status" data-cell="veteran" data-class="veteran_src" data-name="veteranStatus">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="additionalVeteranStatus" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New VeteranStatus</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New VeteranStatus <em class="red-star">*</em></label>
                        <input class="form-control veteran_src1" type="text" placeholder="Add New VeteranStatus">
                        <span class="red-star" id="veteran_src1"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_veteran_status" data-cell="veteran" data-class="veteran_src1" data-name="additional_veteranStatus">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="tenantCredentialType" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Credential Type</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>Credential Type <em class="red-star">*</em></label>
                        <input class="form-control credential_source capital" type="text" placeholder="Ex: License">
                        <span class="red-star" id="credential_source"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_credential_type" data-cell="credential_type" data-class="credential_source" data-name="credentialType[]">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addchargescode" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Charge Code</h4>
                </div>
                <div class="modal-body">
                    <form name="" method="POST" id="chargecode_popup">
                        <div class="form-outer" style="float: none;">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-4">
                                        <label>Charge Code <em class="red-star">*</em></label>
                                        <input type="text" name="charge_code" class="form-control capital">
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Credit Account <em class="red-star">*</em></label>
                                        <select class="form-control" name="credit_account">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Debit Account <em class="red-star">*</em></label>
                                        <select class="form-control" name="debit_account">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-4">
                                        <label>Status <em class="red-star">*</em></label>
                                        <select class="form-control" name="status">
                                            <option value="">Select</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Description</label>
                                        <textarea class="form-control capital" name="description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <a class="blue-btn savechargecodepopup" value="Save">Save</a>
                                <a class="grey-btn cancelchargecodepopup" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyPetSex" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Sex</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>Add New Gender <em class="red-star">*</em></label>
                        <input class="form-control gender_source" type="text" placeholder="Add New Gender">
                        <span class="red-star" id="gender_source"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_pet_gender" data-cell="gender" data-class="gender_source" data-name="pet_gender[]">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyAnimalSex" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Sex</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>Add New Gender <em class="red-star">*</em></label>
                        <input class="form-control gender_source1" type="text" placeholder="Add New Gender">
                        <span class="red-star" id="gender_source1"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_service_animal_gender" data-cell="gender" data-class="gender_source1" data-name="service_gender[]">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="collectionreason" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Reason</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>Add New Reason <em class="red-star">*</em></label>
                        <input class="form-control reason_source capsOn" type="text" placeholder="Add New Reason">
                        <span class="red-star" id="reason_source"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_collection_reason" data-cell="reason" data-class="reason_source" data-name="collection_reason">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="generate_lease" role="dialog" style="text-align: left;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Lease Generation</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none; margin: 0px;">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Do you want to generate a new lease for this Tenant?</label>
                               <!-- <label>Click OK to proceed further.</label>-->
                            </div>
                            <div class="col-sm-12">
                                <input type="button" class="blue-btn lease_generation" value="Yes">
                                <input type="button" class="grey-btn" value="Cancel" data-dismiss="modal">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="generate_online_lease" role="dialog">
        <div class="modal-dialog modal-md text-center">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Disclaimer</h4>
                </div>
                <div class="modal-body" style="text-align: left">
                    <div class="form-outer" style="float: none">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>I authorize ApexLink Inc. to submit this request to generate and present esignature for the Person/Company I have listed. I understand that ApexLink Inc. is not responsible or liable for any data breach, loss/loss of profits or any claim against the filer (Myself or my company) by any party. I understand that in no event is ApexLink Inc. liable for any and all damages. I also understand that ApexLink Inc. grants no implied warranties, including without limitation, warranties, of merchantability, or of fitness for any particular purpose.</label>
                                <label>Click Accept to proceed further.</label>
                            </div>
                            <div class="col-sm-12">
                                <input type="button" class="blue-btn accept_generate_lease" value="Accept">
                                <input type="button" class="blue-btn" value="Cancel" data-dismiss="modal">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyCustomField" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="row custom_field_form">
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Field Name<span class="required"> * </span></label>
                                </div>
                                <div class="col-sm-9 field_name">
                                    <input class="form-control capsOn" type="text" name="field_name" placeholder="">
                                    <span class="error"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Data Type</label>
                                </div>
                                <div class="col-sm-9 data_type">
                                    <select class="form-control" name="data_type">
                                        <option value="text">Text</option>
                                        <option value="number">Number</option>
                                        <option value="currency">Currency</option>
                                        <option value="percentage">Percentage</option>
                                        <option value="url">URL</option>
                                        <option value="date">Date</option>
                                        <option value="memo">Memo</option>
                                    </select>
                                    <span class="error"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Default value</label>
                                </div>
                                <div class="col-sm-9 default_value">
                                    <input class="form-control" type="text" name="default_value" placeholder="">
                                    <span class="error"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Required Field</label>
                                </div>
                                <div class="col-sm-9 is_required">
                                    <select class="form-control" name="is_required">
                                        <option value="1">Yes</option>
                                        <option value="0" selected="selected">No</option>
                                    </select>
                                    <span class="error"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <button type="button" class="blue-btn" id='saveCustomField'>Save</button>
                            <button type="button" class="black-btn" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyCustomField1" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="row custom_field_form1">
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Field Name<span class="required"> * </span></label>
                                </div>
                                <div class="col-sm-9 field_name1">
                                    <input class="form-control capsOn" type="text" name="field_name" placeholder="">
                                    <span class="error"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Data Type</label>
                                </div>
                                <div class="col-sm-9 data_type1">
                                    <select class="form-control" name="data_type">
                                        <option value="text">Text</option>
                                        <option value="number">Number</option>
                                        <option value="currency">Currency</option>
                                        <option value="percentage">Percentage</option>
                                        <option value="url">URL</option>
                                        <option value="date">Date</option>
                                        <option value="memo">Memo</option>
                                    </select>
                                    <span class="error"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Default value</label>
                                </div>
                                <div class="col-sm-9 default_value1">
                                    <input class="form-control" type="text" name="default_value" placeholder="">
                                    <span class="error"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Required Field</label>
                                </div>
                                <div class="col-sm-9 is_required1">
                                    <select class="form-control" name="is_required">
                                        <option value="1">Yes</option>
                                        <option value="0" selected="selected">No</option>
                                    </select>
                                    <span class="error"></span>
                                </div>
                            </div>
                        </div>
                        <div class="btn-outer">
                            <button type="button" class="blue-btn" id='saveCustomField1'>Save</button>
                            <button type="button" class="black-btn" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="backgroundcheckPop1" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Disclaimer</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <p style="font-size: 14px; line-height: 20px;"> I authorize ApexLink Inc. to submit this request to generate and present a background/any check report for the Person/Company I have listed. I understand that ApexLink Inc. is not responsible or liable for any data breach, loss/loss of profits or any claim against the filer (Myself or my company) by any party. I understand that in no event is ApexLink Inc. liable for any and all damages. I also understand that ApexLink Inc. grants no implied warranties, including without limitation, warranties, of merchantability, or of fitness for any particular purpose. </p></div>
                    <div class="col-sm-12"><input type="checkbox" name="" class="background_check_open"><label style="display: inline;"> Check to proceed further.</label></div>
                    <div class="col-sm-12"><input type="button" class="blue-btn"  data-dismiss="modal" value="Cancel"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="backgroundcheckPop2" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 80%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">Background Reports</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12 title1">
                        Open Your Free Account Today!
                    </div>
                    <div class="col-sm-12">
                        ApexLink is pleased to partner with Victig Screening Solutions to provide you with the highest quality, timely National Credit, Criminal and Eviction Reports.
                    </div>
                    <div class="col-sm-12">
                        <table>
                            <thead>
                            <tr><th colspan="3">Package Pricing</th></tr>
                            <tr><th colspan="3">Properties Managing</th></tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>National Criminal</td>
                                <td>National Criminal and Credit</td>
                                <td>National Criminal, Credit and State Eviction</td>
                            </tr>
                            <tr>
                                <td>$15</td>
                                <td>$20</td>
                                <td>$25</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                        *$100 fee to authorize credit report
                    </div>
                    <div class="col-sm-12">
                        <table class="table2">
                            <thead>
                            <tr><th colspan="5">High Volume Users and Larger Properties</th></tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="5">
                                    <label class="text-center">Contact our Victig Sales Team for custom pricing</label>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="mailto:sales@victig.com"><strong>sales@victig.com</strong></a></td>
                                <td><span class="glyphicon glyphicon-ok"></span></td>
                                <td><strong>866.886.5644</strong></td>
                                <td><span class="glyphicon glyphicon-ok"></span></td>
                                <td><a href="http://www.victig.com" target="_blank"><strong>www.victig.com</strong></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-7">
                            Already have an Account? <a href="https://apexlink.instascreen.net" target="_blank">Log in here</a><br>
                            New Users: <a href="https://www.victig.com/victig-on-boarding/" target="_blank">Click here to register</a>
                        </div>
                        <div class="col-sm-5 text-right"><input type="button" class="blue-btn" value="View Sample Report"></div>
                    </div>
                    <div class="col-sm-12">
                        Please be advised that all background services are provided by Victig Screening
                        Solutions. Direct all contact and questions pertaining to background check services
                        to <a href="mailto:sales@victig.com">sales@victig.com</a>, 866.886.5644, <a href="http://www.victig.com" target="_blank">www.victig.com</a>
                    </div>
                    <div class="col-sm-12 table-fotr">
                        NOTICE: The use of this system is restricted. Only authorized users may access this system. All Access to this system is logged and regularly monitored for computer security purposes. Any unauthorized access to this system is prohibited and is subject to criminal and civil penalties under Federal Laws including, but not limited to, the Computer Fraud and Abuse Act and the National Information Infrastructure Protection Act.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="quickLinkMoveOut" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Move Out</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <select class="js-example-basic-single tenant_moveout_name" name="state">
                                        <option value="1">Alabama</option>
                                        <option value="2">Wyoming</option>
                                        <option value="3">Alabama</option>
                                        <option value="4">Wyoming</option>
                                        <option value="5">Alabama</option>
                                        <option value="6">Wyoming</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addReasonmodel" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Reason</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Reason <span class="required"> * </span></label>
                                    <input class="form-control capsOn" type="text" name="" placeholder="Reason">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="black-btn cancelbtn">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="vehiclessearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="vehicle_name">Name</option>
                                        <option value="make">Make</option>
                                        <option value="license">Plate</option>
                                        <option value="color">Color</option>
                                        <option value="year">Year</option>
                                        <option value="vin">VIN</option>
                                        <option value="registration">Registration</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonvehices' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="modal fade" id="petssearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="pet_name">Name</option>
                                        <option value="pet_id">Id</option>
                                        <option value="pet_type">Type</option>
                                        <option value="pet_age">Age</option>
                                        <option value="pet_sex">Sex</option>
                                        <option value="pet_weight">Weight</option>
                                        <option value="pet_note">Note</option>
                                        <option value="pet_color">Pet Color</option>
                                        <option value="pet_chip_id">Chip ID</option>
                                        <option value="hospital_name">Vet/Hosp. Name</option>
                                        <option value="phone_number">Phone Number</option>
                                        <option value="last_visit">Last Visit</option>
                                        <option value="next_visit">Next Visit</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonpets' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="guarantorssearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="first_name">Name</option>
                                        <option value="relationship">Relation</option>
                                        <option value="address1">Address</option>
                                        <option value="email">Email</option>
                                        <option value="phone_number">PhoneNo.</option>
                                        <option value="guarantee_years">Years of Guarantee</option>
                                        <option value="note">Notes</option>
                                        <option value="file_name">File Uploaded</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonguarantors' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="collectionssearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="collection_id">Collection ID</option>
                                        <option value="reason">Reason</option>
                                        <option value="description">Description</option>
                                        <option value="amount_due">Amount Due</option>
                                        <option value="status">Status</option>
                                        <option value="note">Note</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttoncollections' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="parkingssearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="updated_at">Date</option>
                                        <option value="tenant_parking_permit_number">Parking Permit No.</option>
                                        <option value="tenant_parking_space_number">Parking Space No.</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value2" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonparkings' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="medicalssearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="allegery">Medical Allergy</option>
                                        <option value="date">Date</option>
                                        <option value="note">Note</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value1" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonmedicals' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="fileTransfermodal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="name">Name</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbutton1' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="tenantOccupantsearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="name">Name</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="valueOccpt" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonOccupt' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--modal body-->
<div class="container">
    <div class="modal fade" id="TenantNotesmodal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="date">date</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value" name="valueNotes" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonNotes' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialogNotes" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--modal body-->
<div class="container">
    <div class="modal fade" id="TenantCompalintssmodal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="date">date</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value" name="valueCompalints" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonCompalints' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialogCompalints" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="tenantHoasearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="hoa_violation_id">Hoa Violation Id</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value1" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonHoa' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewPortfolioModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Portfolio</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>Portfolio ID <em class="red-star">*</em></label>
                        <input placeholder="Add Portfolio ID" class="form-control portfolio_id_err" name="portfolio_id" type="text" />
                        <span class="red-star" id="portfolio_id_err"></span>
                        <label style="padding: 15px 0 0px 0;">Portfolio Name <em class="red-star">*</em></label>
                        <input placeholder="Add New Portfolio" class="form-control portfolio_name_err" name="portfolio_name" type="text"/>
                        <span class="red-star" id="portfolio_name_err"></span>
                        <div class="col-sm-12 text-right" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_double" data-table="company_property_portfolio" data-cell="portfolio_id,portfolio_name" data-class="portfolio_id_err,portfolio_name_err" data-name="portfolio_id">Save</a>
                            <input type="button" class="clear-btn" value="Clear">
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewManagerModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Manager</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>First Name <em class="red-star">*</em></label>
                        <input placeholder="First Name" class="form-control first_name_err" name="first_name" type="text" />
                        <span style="display: none;" class="red-star" id="first_name_err">Enter first name</span>
                        <label style="padding: 15px 0 0px 0;">Last Name</label>
                        <input placeholder="Last Name" class="form-control last_name_err" name="last_name" type="text"/>
                        <label style="padding: 15px 0 0px 0;">Email <em class="red-star">*</em></label>
                        <input placeholder="Email" class="form-control email_err" name="email" type="text" />
                        <span  style="display: none;"class="red-star" id="email_err">Enter valid email</span>
                        <div class="col-sm-12 text-right" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_double2" data-table="users" data-cell="first_name,last_name,email,user_type,status" data-class="first_name_err,last_name_err,email_err" data-name="manager_id[]">Save</a>
                            <input type="button" class="clear-btn" value="Clear">
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewPropertyGroupModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Property Group</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>Property Group Name <em class="red-star">*</em></label>
                        <input placeholder="Add New Property Group" class="form-control group_name_err" name="group_name" type="text" />
                        <span class="red-star" id="group_name_err"></span>
                        <label>Description</label>
                        <input placeholder="Description" class="form-control description_err" name="description" type="text"/>
                        <span class="red-star" id="description_err"></span>
                        <div class="col-sm-12 text-right" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_double" data-table="company_property_groups" data-cell="group_name,description" data-class="group_name_err,description_err" data-name="attach_groups[]">Save</a>
                            <input type="button" class="clear-btn" value="Clear">
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewPropertyTypeModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Property Type</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>Property Type <em class="red-star">*</em></label>
                        <input placeholder="Add New Property Type" class="form-control" name="property_type" type="text"/>
                        <span class="red-star" id="group_name_err"></span>
                        <label>Description</label>
                        <input placeholder="Description" class="form-control description_err" name="description" type="text"/>
                        <span class="red-star" id="description_err"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_double" data-table="company_property_groups" data-cell="group_name,description" data-class="group_name_err,description_err" data-name="attach_groups[]">Save</a>
                            <input type="button" class="clear-btn" value="Clear">
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="modal fade" id="AddNewPropertyStyleModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-transform: initial;">Add New Property Style</h4>
                </div>
                <div class="modal-body">
                    <form id='AddNewPropertyStyleForm' name="default-setting" method="POST">
                        @csrf
                        <div class="form-outer">
                            <label>Property Style <em class="red-star">*</em> </label>
                            <input placeholder="Add New Property Style" class="form-control" id="property_style" name="property_style" type="text" value="" />
                            <span class="required_star" id="property_style_err"></span>
                            <br>
                            <label>Description</label>
                            <input placeholder="Description" class="form-control" id="property_style_description" name="description" type="text" value="" />
                            <span class="required_star" id="description_err"></span>
                            <div class="btn-outer text-right">
                                <!-- <button type="button" class="btn btn-default blue-btn"  id='AddNewPropertyStyleButton'>Save</button> -->
                                <input type="submit" class="btn btn-default blue-btn"  id='AddNewPropertyStyleButton' value="Save" >
                                <input type="button" class="clear-btn" value="Clear">
                                <input type="button" class="btn btn-primary black-btn" class="close" data-dismiss="modal" value="Cancel">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewPropertySubTypeModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-transform: initial;">Add New Property Sub-Type</h4>
                </div>
                <div class="modal-body">
                    <form id='AddNewPropertySubTypeForm' name="default-setting" method="POST">
                        @csrf
                        <div class="form-outer">
                            <label>Property Sub-Type <em class="red-star">*</em> </label>
                            <input placeholder="Add New Property Sub-Type" class="form-control" id="property_subtype" name="property_subtype" type="text" value="" />
                            <span class="required_star" id="property_subtype_err"></span>
                            <br>
                            <label>Description</label>
                            <input placeholder="Description" class="form-control" id="property_subtype_description" name="description" type="text" value="" />
                            <span class="required_star" id="description_err"></span>
                            <div class="btn-outer">
                                <!--  <button type="button" class="btn btn-default blue-btn"  id='AddNewPropertySubTypeButton'>Save</button> -->
                                <input type="submit" class="btn btn-default blue-btn"  id='AddNewPropertySubTypeButton' value="Save" >
                                <input type="button" class="clear-btn" value="Clear">
                                <input type="button" class="btn btn-primary black-btn" class="close" data-dismiss="modal" value="Cancel">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-transform: initial;">Add New Pet Friendly</h4>
                </div>
                <div class="modal-body">
                    <form id='AddNewPetFriendlyForm' name="default-setting" method="POST">
                        @csrf
                        <div class="form-outer">
                            <label>Pet Friendly <em class="red-star">*</em></label>
                            <input placeholder="Add Pet Friendly" class="form-control" name="condition" id="pet_condition" type="text" value="" />
                            <span class="required_star" id="condition_err"></span>
                            <div class="btn-outer">
                                <!--  <button type="button" class="btn btn-default blue-btn"  id='AddNewPetFriendlyButton'>Save</button> -->
                                <input type="submit" class="btn btn-default blue-btn"  id='AddNewPetFriendlyButton' value="Save" >
                                <input type="button" class="btn btn-primary black-btn" class="close" data-dismiss="modal" value="Cancel">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewGarageAvailableModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-transform: initial;">Add New Garage Available</h4>
                </div>
                <div class="modal-body">
                    <form id='AddNewGarageAvailableForm' name="default-setting" method="POST">
                        @csrf
                        <div class="form-outer">
                            <label>New Garage Available <em class="red-star">*</em></label>
                            <input placeholder="Add Garage Available" class="form-control" id="garage_condition" name="condition" type="text" value="" />
                            <span class="required_star" id="condition_err"></span>
                            <div class="btn-outer">
                                <!-- <button type="button" class="btn btn-default blue-btn"  id='AddNewGarageAvailableButton'>Save</button> -->
                                <input type="submit" class="btn btn-default blue-btn"  id='AddNewGarageAvailableButton' value="Save" >
                                <input type="button" class="btn btn-primary black-btn" class="close" data-dismiss="modal" value="Cancel">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewAmenityModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-transform: initial;">Add New Amenity</h4>
                </div>
                <div class="modal-body">
                    <form id='AddNewAmenityForm' name="default-setting" method="POST">

                        <div class="form-outer" style="float: none;">
                            <label>New Amenity Code</label>
                            <input placeholder="Eg:- 12345 (Optional)" class="form-control" id="code" name="code" type="text" value="" />
                            <span class="required_star" id="code_err"></span>
                            <br>
                            <label>Amenity Name <em class="red-star">*</em> </label>
                            <input placeholder="Add New Amenity Name" class="form-control capital" id="amenity_name" name="name" type="text" value="" />
                            <span class="required_star" id="name_err"></span>
                            <div class="text-right">
                                <input type="submit" class="btn btn-default blue-btn"  id='AddNewAmenityButton' value="Save" >
                                <button type="button" class="clear-btn clearAmenity">Clear</button>
                                <input type="button" class="btn btn-primary grey-btn" class="close" data-dismiss="modal" value="Cancel">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewKeyAccessCodeModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-transform: initial;">Add New Key Access Code</h4>
                </div>
                <div class="modal-body">
                    <form id='AddKeyAccessCodeForm' name="default-setting" method="POST">
                        @csrf
                        <div class="form-outer">
                            <label>Key Access Code <em class="red-star">*</em></label>
                            <input placeholder="Add Key Access Code" class="form-control" name="access_code" id="access_code"type="text" value="" />
                            <span class="required_star" id="access_code_err"></span>
                            <div class="btn-outer">

                                <input type="submit" class="btn btn-default blue-btn"  id='AddNewKeyAccessCodeButton' value="Save" >
                                <input type="button" class="btn btn-primary black-btn" class="close" data-dismiss="modal" value="Cancel">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewUnitTypeModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-transform: initial;">Add New Unit Type</h4>
                </div>
                <div class="modal-body">
                    <form id='AddNewUnitTypeForm' name="default-setting" method="POST">
                        @csrf
                        <input id="addUnittoken" type="hidden" name="_token" value="">
                        <div class="form-outer">
                            <label>Unit Type <em class="red-star">*</em> </label>
                            <input placeholder="Add New Unit Type" class="form-control" id="unit_type" name="unit_type" type="text" value="" />
                            <span class="required_star" id="unit_type_err"></span>
                            <br>
                            <label>Description</label>
                            <input placeholder="Description" class="form-control" id="unit_description" name="description" type="text" value="" />
                            <span class="required_star" id="description_err"></span>

                            <div class="btn-outer text-right">
                                <!-- <button type="button" class="btn btn-default blue-btn"  id='AddNewUnitTypeButton'>Save</button> -->
                                <input type="submit" class="btn btn-default blue-btn"  id='AddNewUnitTypeButton' value="Save" >
                                <input type="button" class="clear-btn ClearUnitType"  value="Clear">
                                <input type="button" class="btn btn-primary black-btn" class="close" data-dismiss="modal" value="Cancel">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewUnitModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" style="width:300px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Unit Type</h4>
                </div>
                <div class="modal-body  add-newunitcontent add-newunitcontent-new">
                    <div class="form-data add_new_building_data">
                        <form id="AddNewUnitData" method="post" action="">
                            <div class="col-sm-12">
                                <label class="control-label">Unit Type<span class="required"> *</span></label>
                                <input type="text" maxlength="15" style="padding: 2% 2% !important;" placeholder="Add New Unit Type" class="form-control" name="unit_type" id="unit_type" spellcheck="true">
                                <span class="required_star" id="unit_type_err"></span>
                            </div>
                            <div class="col-sm-12">
                                <label class="control-label">
                                    Description
                                </label>
                                <input type="text" maxlength="50" style="padding: 2% 2% !important;" placeholder="Description" name="description" id="description" class="form-control" spellcheck="true">
                                <span class="required_star" id="description_err"></span>
                            </div>

                            <div class="col-sm-12 text-right">
                                <div class="nopadding_Up nopadding_Down">
                                    <input type="button" class="blue-btn" value="Save" id="newunittypesave">
                                    <input type="button" class="clear-btn ClearUnitType" value="Clear">
                                    <a id="AddNewAmenityCancel2"><input type="button" class="clear-btn" data-dismiss="modal" value="Cancel"></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="print_hoa" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="blue-btn" id='email_hoa'>Email</button>
                    <button type="button" class="blue-btn" id='print_hoa'  onclick="PrintElem('#modal-body-hoa')" onclick="window.print();">Print</button>
                    <button type="button" class="close" data-dismiss="modal">×</button>

                </div>
                <div class="modal-body" id="modal-body-hoa" style="height: 380px; overflow: auto;">

                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="modal fade" id="transferConfirm" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 40%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tenant Transfer</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <form method="post" action="" id="tenant_transfer_confirm" enctype="multipart/form-data">
                            <div class="panel-body pad-none" >
                                <div class="col-sm-12 pad-none">
                                    <div class="form-outer">
                                        <div class="div-full pad-none">
                                            <input type="hidden" name="hidden_ten_tras_id" class="hidden_ten_tras_id">
                                            <input type="hidden" name="hidden_transfer_id" class="hidden_transfer_id">
                                            <div class="col-sm-12">
                                                <label>Actual Transfer Date:</label>
                                                <input name="actual_transfer_date" class="form-control actual_transfer_date calander">
                                            </div>
                                            <div class="col-sm-12">
                                                <label>Scheduled Transfer Date:</label>
                                                <input name="scheduled_transfer_date" class="form-control scheduled_transfer_date" readonly>
                                            </div>
                                            <div class="col-sm-12">
                                                <label>Old Security Deposit($):</label>
                                                <input name="old_securtity" class="form-control old_securtity" readonly>
                                            </div>
                                            <div class="col-sm-12">
                                                <label>New Security Deposit($):</label>
                                                <input name="new_securtity" class="form-control new_securtity" readonly>
                                            </div>
                                            <div class="modal-header">
                                                <button type="button" class="blue-btn" id='save_transfer_tenant'>Save</button>
                                                <button type="button" class="grey-btn" id="cancel_modal" data-dismiss="modal">Cancel</button>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="modal fade" id="add_contact" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Contact Information</h4>
                </div>
                <div class="modal-body">
                    <form id="add_contact_popup" enctype="multipart/form-data">
                        <div class="panel-body pad-none">
                            <div class="row">
                                <div class="form-outer form-data unit_form_data" id="input-form" style="border: none;">
                                    <div class=" blue-bg-outer unit-blueouter">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Salutation</label>
                                                <span>
                                                    <select id="Salutation_modal" name="salutation" class="form-control" style="cursor: pointer;">
                                                        <option value="Select">Select</option>
                                                        <option value="Dr.">Dr.</option>
                                                        <option value="Mr.">Mr.</option>
                                                        <option value="Mrs.">Mrs.</option>
                                                        <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                                        <option value="Ms.">Ms.</option>
                                                        <option value="Sir">Sir</option>
                                                        <option value="Madam">Madam</option>
                                                        <option value="Brother">Brother</option>
                                                        <option value="Sister">Sister</option>
                                                        <option value="Father">Father</option>
                                                        <option value="Mother">Mother</option>
                                                    </select>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>First Name<em class="red-star">*</em></label>
                                                <span>
                                                    <input id="first_name_modal" name="first_name" placeholder="First Name" class="form-control capsOn" style="cursor: pointer;">
                                                    <span id="first_nameErr" style="display: none;"><em class="red-star">*This field is required</em> </span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Middle Name</label>
                                                <span><input type="text" id="middle_name_modal" placeholder="MI" name="middle_name" class="form-control capsOn">
                                                    <span class="required_star" id="floor_no_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Last Name <em class="red-star">*</em></label>
                                                <span>
                                                    <input type="text" id="Last_name_modal" placeholder="Last Name" name="last_name" class="form-control capsOn" spellcheck="true">
                                                     
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Maiden Name</label>
                                                <span>
                                                    <input type="text" id="maiden_modal" name="maiden_name" placeholder="First Name" class="form-control capsOn" spellcheck="true">
                                                    <span class="required_star" id="unit_no_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Nick Name</label>
                                                <span>
                                                    <input name="nick_name" class="form-control capsOn" id="Nick_name_modal" style="cursor: pointer;">
                                                    <span class="required_star" id="unit_type_id_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Gender</label>
                                                <span>
                                                    <select id="gender_modal" name="gender" class="form-control" spellcheck="true">
                                                        <option value="0">Select</option>
                                                        <option value="1">Male</option>
                                                        <option value="2">Female</option>
                                                    </select>
                                                  <span class="required_star" id="square_ft_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Entity/Company Name</label>
                                                <span>
                                                    <input name="company_name" id="Entity_modal" class="form-control capsOn" style="cursor: pointer; background-color: rgb(255, 255, 255);">

                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Zip Postal</label>
                                                <span>
                                                    <input name="zipcode" id="zip_modal" class="form-control" ">
                                                    <span class="required_star" id="bathrooms_no_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Country
                                                </label>
                                                <span>
                                                    <input type='text' id="Country_modal" name="country" class="form-control amount number_only capsOn" spellcheck="true">
                                                    <span class="required_star" id="base_rent_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>State/Province
                                                </label>
                                                <span>
                                                    <input type="text" id="State_modal" placeholder="Eg: 5000" name="state" class="form-control capsOn" spellcheck="true">
                                                    <span class="required_star" id="market_rent_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>City
                                                </label>
                                                <span>
                                                <input type="text" id="city_modal" name="city" class="form-control capsOn" spellcheck="true">
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Address 1</label>
                                                <span>
                                                <input placeholder="Eg: Yes" class="form-control" id="Address1_modal" name="address1">
                                            </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Address 2</label>
                                                <span>
                                                <input placeholder="Eg: Yes" class="form-control" id="Address2_modal" name="address2">
                                            </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Address 3</label>
                                                <span>
                                                <input name="address3" id="Address3_modal" class="form-control" style="cursor: pointer;">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Address 4</label>
                                                <span>
                                                <input id="Address4_modal" name="address4" class="textarea form-control" >
                                            </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Email</label>
                                                <span>
                                                <input id="Address4_modal" name="email" class="textarea form-control" >
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row p-0">
<!--                                        <div class="div-full pad-none">-->
<!--                                            <div class="col-sm-3">-->
<!--                                                <label>Nick Name</label>-->
<!--                                                <span>-->
<!--                                                    <input name="nick_name" class="form-control" id="Nick_name_modal" style="cursor: pointer;">-->
<!--                                                    <span class="required_star" id="unit_type_id_errmain"></span>-->
<!--                                                </span>-->
<!--                                            </div>-->
<!--                                            <div class="col-sm-3">-->
<!--                                                <label>Gender</label>-->
<!--                                                <span>-->
<!--                                                    <select id="gender_modal" name="gender" class="form-control" spellcheck="true">-->
<!--                                                    <option value="0">Select</option>-->
<!--                                                    </select>-->
<!--                                                  <span class="required_star" id="square_ft_errmain"></span>-->
<!--                                                </span>-->
<!--                                            </div>-->
<!--                                            <div class="col-sm-3">-->
<!--                                                <label>Entity/Company Name</label>-->
<!--                                                <span>-->
<!--                                                    <input name="company_name" id="Entity_modal" class="form-control" style="cursor: pointer; background-color: rgb(255, 255, 255);">-->
<!---->
<!--                                                </span>-->
<!--                                            </div>-->
<!--                                            <div class="col-sm-3">-->
<!--                                                <label>Zip Postal</label>-->
<!--                                                <span>-->
<!--                                                    <input name="zipcode" id="zip_modal" class="form-control" ">-->
<!--                                                    <span class="required_star" id="bathrooms_no_errmain"></span>-->
<!--                                                </span>-->
<!--                                            </div>-->
<!--                                        </div>-->
                                        <div class="div-full pad-none">
<!--                                            <div class="col-sm-3">-->
<!--                                                <label>Country-->
<!--                                                   </label>-->
<!--                                                <span>-->
<!--                                                    <input type='text' id="Country_modal" name="country" class="form-control amount number_only" spellcheck="true">-->
<!--                                                    <span class="required_star" id="base_rent_errmain"></span>-->
<!--                                                </span>-->
<!--                                            </div>-->
<!--                                            <div class="col-sm-3">-->
<!--                                                <label>State/Province-->
<!--                                                   </label>-->
<!--                                                <span>-->
<!--                                                    <input type="text" id="State_modal" placeholder="Eg: 5000" name="state" class="form-control" spellcheck="true">-->
<!--                                                    <span class="required_star" id="market_rent_errmain"></span>-->
<!--                                                </span>-->
<!--                                            </div>-->
<!--                                            <div class="col-sm-3">-->
<!--                                                <label>City-->
<!--                                                    </label>-->
<!--                                                <span>-->
<!--                                                <input type="text" id="city_modal" name="city" class="form-control " spellcheck="true">-->
<!--                                                </span>-->
<!--                                            </div>-->
                                        </div>
                                    </div>
                                    <div class="row">
<!--                                        <div class="col-sm-3">-->
<!--                                            <label>Address 1</label>-->
<!--                                            <span>-->
<!--                                                <input placeholder="Eg: Yes" class="form-control" id="Address1_modal" name="address1">-->
<!--                                            </span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-3">-->
<!--                                            <label>Address 2</label>-->
<!--                                            <span>-->
<!--                                                <input placeholder="Eg: Yes" class="form-control" id="Address2_modal" name="address2">-->
<!--                                            </span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-3">-->
<!--                                            <label>Address 3</label>-->
<!--                                            <span>-->
<!--                                                <input name="address3" id="Address3_modal" class="form-control" style="cursor: pointer;">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-3">-->
<!--                                            <label>Address 4</label>-->
<!--                                            <span>-->
<!--                                                <input id="Address4_modal" name="address4" class="textarea form-control" >-->
<!--                                            </span>-->
<!--                                        </div>-->
                                    </div>
                                    <div class="btn-outer text-right">
                                        <a class="blue-btn savecontactpopup" value="Save">Save</a>
                                        <button type="button" class="clear-btn clearFormReset">Clear</button>
                                        <a id="addPropertyTypeCancel" class="grey-btn" value="Cancel" data-dismiss="modal">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div class="container">-->
    <div class="modal fade" id="shortTermRental" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close_short_term">&times;</button>
                    <h4 class="modal-title">Short Term Rental</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                            <div class="col-sm-12">
                                    <div class="main-tabs">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="booknowredirect active"><a href="javascript:;" aria-controls="home" >Check Availability</a></li>
                                            <li role="presentation" class="booknowredirect1"><a href="javascript:;" aria-controls="profile" >Personal Information</a></li>
                                            <li role="presentation" class="booknowredirect2"><a href="javascript:;" aria-controls="profile" >Pay Now</a></li>

                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="account-receive">
                                                <div class="col-sm-8">
                                                    <form method="post" action="" id="shortTermRentalform" enctype="multipart/form-data">
                                                    <div class="detail-outer">
                                                        <div class="row availability">
                                                            <div class="col-xs-12">
                                                                <label class="text-right" >Frequency :</label>
                                                                <div class="check-outer">
                                                                    <input type="radio" name="frequncy_shot" class="checkFreq" value="1" id="checkFreq1" checked/><label>One Time</label>
                                                                </div>
                                                                <div class="check-outer">
                                                                    <input type="radio" name="frequncy_shot" class="checkFreq" id="checkFreq2" value="2"/><label>Recurring</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12" id="frequency_1">
                                                                <label class="text-right">From :</label>
                                                                <span>

                                                                    <div class="col-sm-5 pad-none"><input class="cal-input form-control calander from_date" name="from_date" type="text"/>
                                                                    <i class="fa fa-calendar" aria-hidden="true"></i></div>
                                                                        <div class="col-sm-2 lh30 text-right">To</div>
                                                                        <div class="col-sm-5 pad-none"><input class="cal-input form-control calander till_date" name="till_date" type="text"/>
                                                                    <i class="fa fa-calendar" aria-hidden="true"></i></div>


                                                                </span>
                                                            </div>
                                                            <div class="col-xs-12 mb-15" id="frequency_2" style="display: none;">
                                                                <label class="text-right">Recurrence Pattern:</label>
                                                                <span>

                                                                    <div class="col-sm-4 pad-none"><select class="cal-input form-control reoccurance_pattern" name="reoccurance_pattern">
                                                                            <option value="0" selected>Select</option>
                                                                            <option value="1">Daily</option>
                                                                            <option value="2">Weekly</option>
                                                                            <option value="3">Monthly</option>
                                                                        </select>
                                                                    </div>
                                                                        <div class="col-sm-4 lh30 text-right">Start From:</div>
                                                                        <div class="col-sm-4 pad-none"><input class="cal-input form-control calander start_from_reocc"  type="text" name="start_from_reocc"/>
                                                                    </div>
                                                                </span>

                                                                <div class="col-xs-12 mb-15" style="display: block;">
                                                                    <label class="text-right"></label>
                                                                    <div class="reocuring-after">
                                                                         <span>
                                                                        <div class="col-sm-5 lh30 pad-none">End After:</div>
                                                                        <div class="col-sm-2 pad-none"><input class="cal-input form-control end_after_reocc" type="text" name="end_after_reocc" value="1"/>
                                                                        </div> <div class="col-sm-5 lh30">Occurrences</div>
                                                                       </span>
                                                                    </div>

                                                                </div>

                                                                <div class="col-xs-12 mb-15 occur1" style="display: none;">
                                                                    <label class="text-right"></label>
                                                                    <span>
                                                                        <div class="col-sm-3 lh30 pad-none">
                                                                            <div class="check-outer">
                                                                                <label>Every</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2 pad-none"><input class="cal-input form-control every_reocc" type="text" name="every_reocc" value="1"/>
                                                                        </div> <div class="col-sm-2 lh30 ">day(s)</div>
                                                                    </span>
                                                                </div>
                                                                <div class="col-xs-12 mb-15 occur2" style="display: none;">
                                                                    <label class="text-right"></label>
                                                                    <div class="reocuring-after">
                                                                         <span>
                                                                        <div class="col-sm-5 lh30 pad-none">Reoccur every</div>
                                                                        <div class="col-sm-2 pad-none"><input class="cal-input form-control every_month_reocc" type="text" name="every_month_reocc" value="1"/>
                                                                        </div> <div class="col-sm-5 lh30">Week(s) on:</div>
                                                                       </span>
                                                                    </div>

                                                                    <div class="reocuring-after reocuring-days">
                                                                    <span>
                                                                        <div class="check-outer">
                                                                                <input type="checkbox" name="weekdays" class="capital" value="Sun" id="" ><label>Sunday</label>
                                                                            </div>
                                                                        <div class="check-outer">
                                                                                <input type="checkbox" name="weekdays" class=" capital" value="Mon" id=""><label>Monday</label>
                                                                </div>
                                                                        <div class="check-outer">
                                                                                <input type="checkbox" name="weekdays" class=" capital" value="Tue" id=""><label>Tuesday</label>
                                                                            </div>
                                                                        <div class="check-outer">
                                                                                <input type="checkbox" name="weekdays" class="capital" value="Wed" id=""><label>Wednesday</label>
                                                                            </div>
                                                                        <div class="check-outer">
                                                                                <input type="checkbox" name="weekdays" class=" capital" value="Thu" id=""><label>Thursday</label>
                                                                            </div>
                                                                            <div class="check-outer">
                                                                                <input type="checkbox" name="weekdays" class="capital" value="Fri" id=""><label>Friday</label>
                                                                            </div>
                                                                            <div class="check-outer">
                                                                                <input type="checkbox" name="weekdays" class=" capital" value="Sat" id=""><label>Saturday</label>
                                                                            </div>
                                                                    </span>
                                                                    </div>
                                                                </div>
                                                                <div class="monthy-reoccuring" style="display: none;">
                                                                     <div class="reocuring-after">
                                                                         <div class="labelEdit  col-sm-3">
                                                                             <input type="radio" checked="checked" spellcheck="true">
                                                                             The
                                                                         </div>
                                                                         <div class="col-sm-3">
                                                                             <select class="form-control validate[required] monthly_week" id="ddlMonthlyWeekNum">
                                                                                 <option value="first">First</option>
                                                                                 <option value="second">Second</option>
                                                                                 <option value="third">Third</option>
                                                                                 <option value="fourth">Fourth</option>
                                                                             </select>
                                                                         </div>
                                                                         <div class="col-sm-3">
                                                                             <select class="form-control validate[required] monthly_days" id="ddlMonthlyWeekNum">
                                                                                 <option value="sunday">Sunday</option>
                                                                                 <option value="monday">Monday</option>
                                                                                 <option value="tuesday">Tuesday</option>
                                                                                 <option value="wednesday">Wednesday</option>
                                                                                 <option value="thursday">Thursday</option>
                                                                                 <option value="friday">Friday</option>
                                                                                 <option value="saturday">Saturday</option>
                                                                             </select>
                                                                         </div>
                                                                         <div class="labelEdit col-sm-3">
                                                                             of every
                                                                             <div class="labelEdit months-ocuuring">
                                                                                 <input id="txtMonthlyTimes" class="input-box onlyNumInput form-control monthly_every" type="text" value="1" spellcheck="true">
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                    <div class="reocuring-after">
                                                                        <div class="labelEdit  col-sm-12">
                                                                            Month(s)
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>










                                                            <div class="col-xs-12">
                                                                <label class="text-right"></label>
                                                                <span class="btn-outer"><a class="blue-btn searching_shortTerm">Search</a></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right"></label>
                                                                <div class="short-term-rental-right">
                                                                    <div class="row">
                                                                         <div class="col-sm-4">
                                                                             <select class="select-dd form-control prop_short" name="prop_shortt"><option value="">Property</option></select>
                                                                         </div>
                                                                        <div class="col-sm-4">
                                                                            <select class="select-dd form-control build_short" name="build_shortt"><option value="">Building</option></select>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <select class="select-dd form-control unit_short" name="unit_shortt"><option value="">Unit</option></select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <span class="dd-text">Price Amount: <em class="unit_rent"></em></span>
                                                                        </div>

                                                                    </div>



                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                            <div class="btn-outer">
                                                                <a class="blue-btn pull-right booknow1">Book Now</a>
                                                            </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    </form>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="cal-available pull-right">
                                                        <label></label>
                                                        <span>Available</span>
                                                    </div>

                                                    <div class="cal-booked pull-right clear">
                                                        <label></label>
                                                        <span>Booked</span>
                                                    </div>
                                                    <div class="myCalendar"></div>
                                                  <!--  <div class="myCalendar"></div>-->

                                                </div>
                                            </div>
                                            <!-- First Tab Ends -->

                                             <div role="tabpanel" class="tab-pane" id="account-paybill">
                                                <div class="form-outer">




                                                    <form id="general_short_term">
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="form-outer2 property-status">
                                                                <div class="col-sm-12">
                                                                    <div class="check-outer">
                                                                        <input type="checkbox" name="entity_company" id="entity_company" class="entity_company" value="1"/> <label>Check this Box if this Tenant is an Entity/Company</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4" id="company_hidden_field" style="display: none">
                                                                    <label>Contact for this tenant <em class="red-star">*</em></label>
                                                                    <input class="form-control" name="comapany_name" id="comp_name"/>
                                                                    <label id="comapany_name-error" class="error" for="comapany_namee">* This field is required.</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <label>Upload Picture</label>
                                                                <div class="upload-logo shortImage clearfix">
                                                                    <div class="img-outer"><img src="<?php echo COMPANY_SITE_URL ?>/images/dummy-img.jpg"></div>

                                                                    <a href="javascript:;">
                                                                        <i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Image</a>
                                                                    <span>(Maximum File Size Limit: 1MB)</span>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file" class="cropit-image-input" name="tenant_image">
                                                                    <div class='cropItData' style="display: none;">
                                                                        <span class="closeimagepopupicon">X</span>
                                                                        <div class="cropit-preview"></div>
                                                                        <div class="image-size-label">Resize image</div>
                                                                        <input type="range" class="cropit-image-zoom-input">
                                                                        <input type="hidden" name="image-data" class="hidden-image-data"/>
                                                                        <input type="button" class="export" value="Done"
                                                                               data-val="tenant_image">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                                <div id="company_hidden_div">
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Salutation</label>
                                                                <select class="form-control" name="salutation">
                                                                    <option value="Select">Select</option>
                                                                    <option value="Dr.">Dr.</option>
                                                                    <option value="Mr.">Mr.</option>
                                                                    <option value="Mrs.">Mrs.</option>
                                                                    <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                                                    <option value="Ms.">Ms.</option>
                                                                    <option value="Sir">Sir</option>
                                                                    <option value="Madam">Madam</option>
                                                                    <option value="Brother">Brother</option>
                                                                    <option value="Sister">Sister</option>
                                                                    <option value="Father">Father</option>
                                                                    <option value="Mother">Mother</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>First Name <em class="red-star">*</em></label>
                                                                <input class="form-control first_name_shoort" type="text" name="first_name">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Middle Name</label>
                                                                <input class="form-control" type="text" name="middle_name">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Last Name <em class="red-star">*</em></label>
                                                                <input class="form-control short_term_last_name" type="text" name="last_name">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Maiden Name</label>
                                                                <input class="form-control" type="text" name="maiden_name">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Nick Name</label>
                                                                <input class="form-control" type="text" name="nickname">
                                                            </div>
                                                                </div>
                                                            <div id="short_term_rental" class="primary-tenant-phone-row-short">
                                                            <div class="col-xs-12 col-sm-3 col-md-3">

                                                                <label>Phone Type</label>
                                                                <select class="form-control phone_type_rental" name="phone_type">
                                                                    <option value="1">Mobile</option>
                                                                    <option value="2">Work</option>
                                                                    <option value="3">Fax</option>
                                                                    <option value="4">Home</option>
                                                                    <option value="5">Other</option>

                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <label>Carrier <em class="red-star">*</em></label>
                                                                <select class="form-control carrier_shortTerm" name="carrier"><option>Select</option></select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <label>Country Code</label>
                                                                <select class="form-control country_shortTerm" name="country"><option>United States (+1)</option></select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <label>Phone Number<em class="red-star">*</em> </label>
                                                                <input class="form-control add-input phone_format" name="phone_number" type="text">
                                                                <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                                            </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Add Note for this Phone Number </label>
                                                                <input class="form-control add-input capital" type="text" name="notePhone">
                                                            </div>
                                                            <div class="multipleEmailShortTerm">
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Email<em class="red-star">*</em> </label>
                                                                <input class="form-control add-input" type="text" name="email">
                                                                <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle email-plus-sign-short-term" aria-hidden="true"></i></a>
                                                                <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle fa-minus-circle-short-term" aria-hidden="true"></i></a>
                                                            <span class="err-class"></span>
                                                            </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Referral Source <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle propreerralicon" aria-hidden="true"></i></a></label>
                                                                <select class="form-control referralShortTerm reff_source" name="referralSource"></select>
                                                                <div class="add-popup" id="selectPropertyReferralResource1">
                                                                    <h4>Add New Referral Source</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>New Referral Source <em class="red-star">*</em></label>
                                                                                <input name="referral" class="form-control customValidateGroup reff_source capital" type="text" data_required="true" data_max="150" placeholder="New Referral Source">
                                                                                <span class="customError required" aria-required="true" id="reff_source"></span>
                                                                            </div>
                                                                            <div class="btn-outer">
                                                                                <button type="button" class="blue-btn add_single1" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source" data-name="referralSource">Save</button>
                                                                                <a class="clear-btn">Clear</a>
                                                                                <input type="button" class="grey-btn" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Ethnicity  <a class="pop-add-icon selectPropertyEthnicity" href="javascript:;">
                                                                        <i class="fa fa-plus-circle"aria-hidden="true"></i>
                                                                    </a></label>
                                                                <select class="form-control ethnicity_short" name="ethncity"><option>United States (+1)</option></select>
                                                                <div class="add-popup" id="selectPropertyEthnicity1">
                                                                    <h4>Add New Ethnicity</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>New Ethnicity <em class="red-star">*</em></label>
                                                                                <input class="form-control ethnicity_src customValidateGroup capsOn" type="text" placeholder="Add New Ethnicity" data_required="true" >
                                                                                <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                                            </div>
                                                                            <div class="btn-outer">
                                                                                <button type="button" class="blue-btn add_single1"  data-table="tenant_ethnicity" data-cell="title" data-class="ethnicity_src" data-name="ethncity">Save</button>
                                                                                <input type="button" class="grey-btn" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Martial Status <a class="pop-add-icon selectPropertyMaritalStatus" href="javascript:;">
                                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                    </a></label>
                                                                <select class="form-control martial_short" name="maritalStatus"><option>United States (+1)</option></select>
                                                                <div class="add-popup" id="selectPropertyMaritalStatus1">
                                                                    <h4>Add New Marital Status</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>New Marital Status <em class="red-star">*</em></label>
                                                                                <input class="form-control maritalstatus_src capsOn" type="text" placeholder="Add New Marital Status">
                                                                                <span class="customError required red-star" aria-required="true" id="maritalstatus_src"></span>
                                                                            </div>
                                                                            <div class="btn-outer">
                                                                                <button type="button" class="blue-btn add_single1" data-table="tenant_marital_status" data-cell="marital" data-class="maritalstatus_src" data-name="maritalStatus">Save</button>
                                                                                <input type="button" class="grey-btn" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Hobbies <a class="pop-add-icon selectPropertyHobbies" href="javascript:;">
                                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                    </a></label>
                                                                <select class="form-control hobbies_short multiselect" name="hobbies[]" multiple><option>United States (+1)</option></select>
                                                                <div class="add-popup" id="selectPropertyHobbies1">
                                                                    <h4>Add New Hobbies</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>New Hobbies <em class="red-star">*</em></label>
                                                                                <input class="form-control hobbies_src capsOn" type="text" placeholder="Add New Hobbies" data_required="true">
                                                                                <span class="red-star" id="hobbies_src"></span>
                                                                            </div>
                                                                            <div class="btn-outer">
                                                                                <button type="button" class="blue-btn add_single1" data-table="hobbies" data-cell="hobby" data-class="hobbies_src" data-name="hobbies[]">Save</button>
                                                                                <input type="button" class="grey-btn" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Veteran Status <a class="pop-add-icon selectPropertyVeteranStatus" href="javascript:;">
                                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                    </a></label>
                                                                <select class="form-control veteran_shortTerm" name="veteranStatus"><option>United States (+1)</option></select>
                                                                <div class="add-popup" id="selectPropertyVeteranStatus1">
                                                                    <h4>Add New VeteranStatus</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>New VeteranStatus <em class="red-star">*</em></label>
                                                                                <input class="form-control veteran_src capsOn" type="text" placeholder="Add New VeteranStatus">
                                                                                <span class="red-star" id="veteran_src"></span>
                                                                            </div>
                                                                            <div class="btn-outer">
                                                                                <button type="button" class="blue-btn add_single1" data-table="tenant_veteran_status" data-cell="veteran" data-class="veteran_src" data-name="veteranStatus">Save</button>
                                                                                <input type="button" class="grey-btn" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="multipleSsnShortTerm">
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>SSN/SIN/ID</label>
                                                                <input class="form-control add-input" type="text" name="ssn_id"/>
                                                                <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle ssn-plus-sign-short_term" aria-hidden="true"></i></a>
                                                                <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle ssn-remove-sign-short-term" aria-hidden="true"></i></a>
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div class="btn-outer">
                                                            <a class="blue-btn pull-right savePersonalShortTerm" >Pay Now</a>
                                                            <a class="grey-btn pull-right cancel_div" style="display: none;">Cancel</a>
                                                            <a class="blue-btn pull-right ok_preview" style="display: none; margin-right: 21px;">Ok</a>
                                                            <!--<a >Pay Now</a>-->
                                                        </div>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- second Tab Ends -->
                                            <div role="tabpanel" class="tab-pane" id="account-paybill2">
                                                <div id='loadingmessage'
                                                     style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'
                                                     ;>
                                                    <img width="200" height="200"
                                                         src='<?php echo SUPERADMIN_SITE_URL ?>/images/loading.gif'/>
                                                </div>
                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>Payment Details</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Amount ($)<em class="red-star">*</em></label>
                                                                <input class="form-control amountTobePaid" type="text" readonly>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Description</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>Billing Address</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Zip Code</label>
                                                                <input class="form-control current_zip" type="text">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Country</label>
                                                                <input class="form-control propertyCountry_name" type="text">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>State</label>
                                                                <input class="form-control current_state" type="text">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>City</label>
                                                                <input class="form-control current_city" type="text">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <label>Address</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>Payment Method</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="form-outer2 property-status">
                                                                <div class="col-sm-12">
                                                                    <div class="check-outer">
                                                                        <input type="radio"/><img width="150px" src="<?php echo COMPANY_SITE_URL ?>/images/cards.png"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <form action="/charge" method="post" class="cc-card-number" id="payment-form">

                                                                <input type='hidden' class='days_remaining'>
                                                                <input type='hidden' class='plan_id'>


                                                                <input type='hidden' class='days_remaining'>
                                                                <input type='hidden' class='plan_id'>

                                                                <div class="form-row">
                                                                    <div class="col-sm-4">
                                                                        <p  class="payment-method-text">Name on Card</p>
                                                                        <input placeholder="Name on card" class="cardname-input" type="text"/>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <label for="card-element" class="card-payment-text">
                                                                            Credit / Debit card
                                                                        </label>
                                                                        <div id="card-element">
                                                                            <!-- A Stripe Element will be inserted here. -->
                                                                        </div>
                                                                        <!-- Used to display form errors. -->
                                                                        <div id="card-errors" class="red-star" role="alert"></div>
                                                                    </div>


                                                                </div>
                                                                <div class="row">
<!--                                                                    <div class="col-sm-6">-->
<!--                                                                        <img src="--><?php //echo COMPANY_SITE_URL;?><!----><?php //echo !empty($_SESSION[SESSION_DOMAIN]['company_logo'])? $_SESSION[SESSION_DOMAIN]['company_logo'] :  '/images/logo.png' ?><!--"  class="payment-method-logo">-->
<!--                                                                    </div>-->

                                                                        <a class="blue-btn submit_payment_class pull-right payment-submit-button">Submit Payment</a>

                                                                </div>

                                                            </form>
                                                            <!--<div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Name on Card <em class="red-star">*</em></label>
                                                                <input class="form-control" type="text"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Card Number <em class="red-star">*</em></label>
                                                                <input class="form-control" type="text"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Expiration Month <em class="red-star">*</em></label>
                                                                <select class="form-control"><option>MM</option></select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Expiration Year <em class="red-star">*</em></label>
                                                                <select class="form-control"><option>YY</option></select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>CVC <em class="red-star">*</em></label>
                                                                <input class="form-control" type="text"/>
                                                            </div>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-sm-10 text-right">
                                                    <a class="blue-btn preview_short">Preview</a>
<!--                                                    <a class="blue-btn submit_pay">Submit Payment</a>-->
                                                </div>
                                                </div>
                                            </div>

                                        <!-- Tab panes -->


                                    </div>
                                </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--</div>-->
    <div class="container">
        <div class="modal fade" id="backgroundReport" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content  pull-left" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-left">Background Reports</h4>
                    </div>
                    <div class="modal-body">
                        <div class="tenant-bg-data">
                            <h3 style="text-align: center;">
                                Open Your Free Account Today!
                            </h3>
                            <p>
                                ApexLink is pleased to partner with Victig Screening Solutions to provide you with
                                the highest quality, timely National Credit, Criminal and Eviction Reports.
                            </p>
                            <div class="grid-outer " style="max-width: 1421px;">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                    <tr>
                                        <th colspan="3" align="center" class="bg-Bluehdr">
                                            Package Pricing<br>
                                            Properties Managing
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="29%" align="left">
                                            National Criminal
                                        </td>
                                        <td width="29%" align="left">
                                            National Criminal and Credit
                                        </td>
                                        <td width="42%" align="left">
                                            National Criminal, Credit and State Eviction
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            $15
                                        </td>
                                        <td align="left">
                                            $20
                                        </td>
                                        <td align="left">
                                            $25
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <p>
                                    *$100 fee to authorize credit report
                                </p>
                            </div>
                            <div class="grid-outer " style="max-width: 1421px;">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                    <tr>
                                        <th colspan="5" align="center" class="bg-Bluehdr">
                                            High Volume Users and Larger Properties
                                        </th>
                                    </tr>
                                    <tr>
                                        <td colspan="5" align="left">
                                            <p align="center">
                                                <strong>Contact our Victig Sales Team for custom pricing</strong>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="26%" align="center">
                                            <a href="mailto:sales@victig.com"><strong>sales@victig.com</strong></a>
                                        </td>
                                        <td width="15%" align="center">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </td>
                                        <td width="21%" align="center">
                                            <strong>866.886.5644 </strong>
                                        </td>
                                        <td width="13%" align="center">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </td>
                                        <td width="25%" align="center">
                                            <a href="http://www.victig.com" target="_blank"><strong>www.victig.com</strong></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="div-full">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                    <tr>
                                        <td width="52%" height="20" style="font-size: 14px;" align="left">
                                            Already have an Account? <a href="https://apexlink.instascreen.net" target="_blank">
                                                Log in here
                                            </a>
                                        </td>
                                        <td width="48%" rowspan="2" align="right">
                                            <a href="https://www.victig.com/wp-content/uploads/2016/05/sample-tenant-report.jpg" target="_blank">
                                                <!--                                            <input value="View Sample Report" class="blue-btn fr" type="button" spellcheck="true" autocomplete="off">-->
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="font-size: 14px;">
                                            New Users: <a href="https://www.victig.com/victig-on-boarding/" target="_blank">
                                                Click
                                                here to register
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="div-full3">
                                <p>
                                    Please be advised that all background services are provided by Victig Screening
                                    Solutions. Direct all contact and questions pertaining to background check services
                                    to <a href="mailto:sales@victig.com">sales@victig.com</a>, 866.886.5644, <a href="http://www.victig.com" target="_blank">www.victig.com</a>
                                </p>
                            </div>
                            <div class="notice">
                                NOTICE: The use of this system is restricted. Only authorized users may access this
                                system. All Access to this system is logged and regularly monitored for computer
                                security purposes. Any unauthorized access to this system is prohibited and is subject
                                to criminal and civil penalties under Federal Laws including, but not limited to,
                                the Computer Fraud and Abuse Act and the National Information Infrastructure Protection
                                Act.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="backgroundModel">
        <div class="modal fade" id="backGroundCheckPopCondition" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content  pull-left" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-left">Run Background Check Via</h4>
                    </div>
                    <div class="modal-body checkedPop-style">
                        <div class="col-sm-12">
                            <p style="font-size: 14px; line-height: 20px;">  </p></div>
                        <div class="col-sm-6 col-md-offset-3"><label><input checked type="radio" name="background_type" value="1">TransUnion(SmartMove)</label>
                            <!--<label><input type="radio" name="background_type" value="2">Victig</label></div>-->
                        <div class="col-sm-6 col-md-offset-3 btn-outer mg-btm-20">
                            <a class="blue-btn" id="btnBackgroundSelection">OK</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="sm-mainpage" class="modal fade smart-move-modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <header class="smart-move-header">
                        <div class="header-top">
                            <div class="container">
                                <div class="row d-flex align-item-center">
                                    <div class="col-sm-6">
                                        <a href="index.html" class="logo"><img src="<?php echo COMPANY_SITE_URL; ?>/images/tulogo.png" alt="logo" class="img-responsive"></a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="#" class="greenplan-button pull-right landlord_account1">Login</a>
                                        <a href="javascript:;" class="yellow-button pull-right mr-8 landlord_account"  data-toggle="modal" data-target="#signup-modal" data-dismiss="modal">Sign Up</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header-bottom">
                            <nav class="navbar">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="collapse navbar-collapse" id="myNavbar">
                                    <ul class="nav navbar-nav">
                                        <li class="active"><a href="#">Home</a></li>
                                        <li><a href="#">Screen a Tanent</a></li>
                                        <li><a href="#">For Renters</a></li>
                                        <li><a href="#">News</a></li>
                                        <li><a href="#">Support</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </header>
                </div>
                <div class="modal-body p-0">
                    <div class="main-page-banner d-flex align-item-center">
                        <div class="row main-page-banner-content">
                            <div class="col-sm-6">
                                <h2 class="main-heading text-center">Tenants</h2>
                            </div>
                            <div class="col-sm-6 text-center">
                                <h2 class="main-heading text-center"><b>Great</b> Tenants</h2>
                                <p class="text-center">SmartMove can make the difference</p>
                                <a href="#" class="btn  yellow-button">Start Today</a>
                            </div>
                        </div>
                    </div>
                    <div class="main-page-bottom">
                        <h2 class="main-heading">How SmartMove Works</h2>
                        <div class="row">
                            <div class="col-sm-4">
                                <p><b>Landlord</b> invites an applicant to send tenant
                                    screening reports through Smartfriove</p>
                            </div>
                            <div class="col-sm-4">
                                <p><b>Rental applicant</b> authorizes screening and passes online identity verification.</p>
                            </div>
                            <div class="col-sm-4">
                                <p><b>SmartMove</b> deLivers tenant reports and a recommendation to the Landlord... within minutes.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
            </div>

        </div>
    </div>

    <div id="smsignup-modal" class="modal fade smart-move-modal"  data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <header class="smart-move-header">
                        <div class="header-top">
                            <div class="container">
                                <div class="row d-flex align-item-center">
                                    <div class="col-sm-6">
                                        <a href="index.html" class="logo"><img src="<?php echo COMPANY_SITE_URL; ?>/images/logo.png" alt="logo" class="img-responsive"></a>
                                    </div>
                                    <div class="col-sm-6">
                                        <!--<a href="#" class="btn  greenplan-button pull-right">Login</a>
                                        <a href="#" class="btn  yellow-button pull-right mr-8">Sign Up</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header-bottom">
                            <nav class="navbar">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="collapse navbar-collapse" id="myNavbar">
                                    <ul class="nav navbar-nav">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">Screen a Tanent</a></li>
                                        <li><a href="#">For Renters</a></li>
                                        <li><a href="#">News</a></li>
                                        <li><a href="#">Support</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </header>
                </div>
                <div class="modal-body p-0 sm-signup-form-body">
                    <h2 class="sm-main-heading">Sign Up for SmartMove</h2>
                    <form method="post" action="#" id="landlord_signup_form">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4>Create Your Account</h4>
                                <div class="form-group">
                                    <label>Account Type: <span class="required">*</span></label>
                                    <select class="form-control" name="landlord">
                                        <option value="1" selected>Landlord/Property Manger</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Email: <span class="required">*</span></label>
                                    <input type="email" name="email" class="form-control" placeholder="Enter Your Email Address" value="<?php echo $_SESSION[SESSION_DOMAIN]['email']; ?>" readonly>
                                </div>
                                <div class="form-group height-auto">
                                    <label>Service Agreement:</label>
                                    <textarea class="form-control" name="service_agreement">This is the service agreement to use TransUnion® SmartMove® (“Service Agreement”).  Your acknowledgement and agreement to these terms, as well as the Terms and Conditions of the internet site you are accessing (“Site”), are required to access and/or use TransUnion SmartMove.  You agree to be legally bound by these terms.  This Service Agreement is made and entered into as by and between TransUnion Rental Screening Solutions, Inc. (“TURSS”) and you/your company (“Subscriber”, “You” or “Property Owner”). In consideration of the promises and mutual covenants hereinafter set forth, TURSS and Subscriber hereto agree as follows:
Scope of Agreement. This Agreement applies to any of those information services which Subscriber may desire to receive from TURSS and which TURSS offers to Subscriber via this Site. Such information services shall herein be collectively referred to as “Services” and all information derived therefrom shall be collectively referred to as “Services Information.”  Subscriber enters in this Agreement on behalf of itself and its affiliates under common ownership and control, all of which are referred to collectively as Subscriber.
Subscriber’s business.  Subscriber certifies that it is utilizing the Services solely for assisting with making a residential or storage leasing decision.
Consumer Reporting Services.
1.1	Consumer Report Information. TURSS makes certain consumer report information services from consumer reporting databases, including but not limited to consumer credit reports and criminal record reports (“Consumer Report Information”) available to its customers who have a permissible purpose for receiving such information in accordance with the Fair Credit Reporting Act (15 U.S.C. §1681 et seq.) including, without limitation, all amendments thereto (“FCRA”).  For the purposes of this Agreement, the term “adverse action” shall have the same meaning as that term is defined in the FCRA.
1.2	FCRA Penalties.  THE FCRA PROVIDES THAT ANY PERSON WHO KNOWINGLY AND WILLFULLY OBTAINS INFORMATION ON A CONSUMER FROM A CONSUMER REPORTING AGENCY UNDER FALSE PRETENSES SHALL BE FINED UNDER TITLE 18, OR IMPRISONED NOT MORE THAN TWO YEARS, OR BOTH.
1.3	 Subscriber Certifications.  Subscriber certifies that it shall request Consumer Report Information pursuant to the written authorization of the consumer who is the subject of the Consumer Report Information. Subscriber certifies that each such written authorization will expressly authorize Subscriber to obtain the Consumer Report Information, and will contain at a minimum the subject’s name, address, social security number (where available) and signature.  Subscriber shall use such Consumer Report Information solely for Subscriber’s exclusive one-time use and pursuant to the consumer’s written authorization use such information solely for assisting with making a residential or storage leasing decision, and for no other purpose, subject however, to the additional restrictions set forth herein.  Moreover, if requested by TURSS, Subscriber agrees to, and shall, individually certify the permissible purpose for each Consumer Report Information it requests.  Such individual certification shall be made by Subscriber pursuant to instructions provided from time to time to Subscriber by TURSS. Nothing in this certification, or elsewhere in this Agreement, is intended to allow Subscriber to purchase Consumer Report Information for the purpose of selling or giving the report, or information contained in or derived from it, to the subject of the report, or to any other third party, and Subscriber expressly agrees to refrain from such conduct.
1.4	 Recommendations.   Using Applicant and/or Tenant information provided to TURSS by Subscriber (“Applicant/Tenant Information”), TURSS will obtain consumer reports relating to each Applicant and/or Tenant and TURSS will evaluate the consumer reports (“Applicant/Tenant Reviews”).  Based on the results of the Applicant/Tenant Reviews, TURSS will provide to Subscriber a Recommendation with respect to the Applicant and/or Tenant, based on the initial thresholds established by TURSS.  Such thresholds, delivery specifications and decision criteria, and any changes thereto, shall be supplied or confirmed by Subscriber in writing.  As part of the Recommendation service, TURSS shall also provide to Subscriber a sample letter containing information as to why the Applicant and/or Tenant was or was not approved.  It is Subscriber’s obligation, however, to ensure compliance with any legal obligations when providing any information to an Applicant and/or Tenant.
1.4.2  	All Rental Decisions to be made by Subscriber. Subscriber acknowledges and agrees that TURSS provides only Recommendations as to actions concerning an Applicant or a Tenant, and further acknowledges and agrees that all decisions of whether or not to rent property to a particular Applicant or Tenant, as well as the length of and terms of any such rental, will be made by Subscriber.  TURSS shall have no liability to Subscriber or to any Applicant, Tenant or other person or entity for any rental, or the failure to rent, to any Applicant or Tenant, or the terms of any such rental, regardless of whether or not Subscriber’s decision was based on Recommendations, Consumer Report Information, public records, or other information provided to Subscriber by TURSS.
1.6	Compliance with Laws.  Subscriber shall be responsible for compliance with all applicable federal (including, but not limited to the FCRA) and state laws, rules, regulations and judicial actions, as now or as may become effective, to which it is subject.
1.6.1	Subscriber certifies it shall comply with all requirements related to the public record information (“Public Record Information”) and other applicable data use restrictions (“Data Source Requirements”) described at rentalscreening.transunion.com/datasourcerequirements, which may be altered by TURSS from time to time, and certifies that any distribution of the Public Record Information or a Consumer Report shall comply with and contain the state-specific requirements described at rentalscreening.transunion.com/datasourcerequirements, which may be altered by TURSS from time to time.
1.6.2	Joint Use.  To the extent Subscriber elects to make available Consumer Report Information to another subscriber or to the extent another TURSS subscriber elects to make available Consumer Report Information to Subscriber (in each case, a “Joint User”), Subscriber shall at all times be jointly and severally liable for the Joint User’s use of the Consumer Report Information.  Subscriber shall be responsible to ensure the Joint User’s compliance with the terms and conditions of this Agreement.  Subscriber hereby represents to TURSS that it has entered into an agreement with the Joint Users containing obligations and restrictions consistent with its obligations and restrictions under this Agreement.  Subscriber certifies that the Joint User’s permissible purpose to view the Consumer Report Information of any consumer is the same as Subscriber’s permissible purpose, and that such use results from a legitimate business relationship between Subscriber and the Joint User requiring joint use of the report in connection with the business transaction with a consumer.  Subscriber shall remain liable for any and all requirements under this Agreement and applicable law for the use of Consumer Report Information it receives under this Agreement.
2	Ancillary Services
2.1	Fraud Prevention Services.  TURSS offers several fraud prevention services that evaluate inquiry input elements against other input elements and/or against proprietary databases, to identify potential discrepancies and/or inaccuracies.  Fraud prevention service messages may be delivered with Consumer Report Information as a convenience, but are not part of a consumer’s file nor are they intended to be consumer reports.  In the event Subscriber obtains any fraud prevention services from TURSS in conjunction with Consumer Report Information or as a standalone service, Subscriber shall not use the fraud prevention services, in whole or in part, as a factor in establishing an individual’s creditworthiness or eligibility for credit or insurance, or employment, nor for any other purposes under the FCRA.  Moreover, Subscriber shall not take any adverse action, which is based in whole or in part on the fraud prevention services, against any consumer.  As a result of information obtained from the fraud prevention services, it is understood that Subscriber may choose to obtain additional information from one or more additional independent sources.  Any action or decision as to any individual which is taken or made by Subscriber based solely on such additional information obtained from such additional independent source(s) shall not be deemed prohibited by this paragraph.
2.2	 Scores.  Subscriber may request, in writing, that TURSS provide Subscriber certain scores (e.g. scores received from credit reporting agencies (“Bureau Score”), SmartMove, Score, CreditRetrieverSM Score), in connection with the delivery of a consumer report obtained hereunder, collectively referred to herein as “Scores” for Subscriber’s exclusive use. TURSS agrees to perform such processing as reasonably practicable.  Subscriber shall use Scores only in accordance with its permissible purpose under the FCRA and may store Scores solely for Subscriber’s own use in furtherance of Subscriber’s original purpose for obtaining the Scores.  Subscriber shall not use the Scores for model development or model calibration and shall not reverse engineer the Scores.
2.2.1	 Adverse Action Factors.  Subscriber recognizes that factors other than the Scores may be considered in making a decision as to a consumer.  Such other factors include, but are not limited to, the credit report, the individual account history, application information, and economic factors.  TURSS may provide score reason codes to Subscriber, which are designed to indicate the principal factors that contributed to the Bureau Score, and may be disclosed to consumers as the reasons for taking adverse action, as required by the Equal Credit Opportunity Act (“ECOA”) and its implementing Regulation (“Reg. B”).  The Bureau Score itself, when accompanied by the corresponding reason codes, may also be disclosed to the consumer who is the subject of the Bureau Score.  However, the Bureau Score itself may not be used as the reason for adverse action under Reg. B.
2.2.2	 Confidentiality of Scores. The CreditRetrieverSM Score and the SmartMove Score are proprietary to TURSS and the BureauScore is proprietary to the credit reporting agency supplying the Bureau Score and, accordingly, without appropriate prior written consent, neither the CreditRetrieverSM Score, the SmartMove Score, or the Bureau Score may be sold, licensed, copied, reused, disclosed, reproduced, revealed or made accessible, in whole or in part, to any Person except: (a) as expressly permitted herein; (b) to those employees of Subscriber with a need to know and in the course of their employment; (c) to those third party processing agents of Subscriber who have executed an agreement that limits the use of the Scores by the third party only to the use permitted to Subscriber and contains the prohibitions set forth herein regarding model development, model calibration and reverse engineering; (d) when accompanied by the corresponding reason codes, to the consumer who is the subject of the Score;  or (e) as required by law.  Subscriber shall not, nor permit any third party to, publicly disseminate any results of the validations or other reports derived from the Scores without prior written consent.
2.2.3	 Score Performance.  Certain Scores are implemented with standard minimum exclusion criteria.  TURSS shall not be liable to Subscriber for any claim, injury or damage suffered directly or indirectly by Subscriber as a result of any Subscriber requested changes to the exclusion criteria which result in normally excluded records being scored by such Scores.  TURSS warrants that the scoring algorithms used in the computation of the scoring services, provided under this Agreement, (“Models”) are empirically derived from credit data and are a demonstrably and statistically sound method of rank-ordering candidate records with respect to the purpose of the Scores when applied to the population for which they were developed, and that no scoring algorithm used by a Score uses a “prohibited basis” as that term is defined in ECOA and Reg. B promulgated thereunder.  The Bureau Score may appear on a credit report for convenience only, but is not a part of the credit report nor does it add to the information in the report on which it is based.
2.3	 Third Party Scores and Other Third Party Services.  TURSS has the capability to offer scores derived from models built jointly with third parties, and other services provided by third parties, which are subject to additional warranties offered or terms imposed by such third parties.  If desired by Subscriber, such third party scores and services shall be made available pursuant to separate agreement, which shall be appended as a schedule to this Agreement.
2.4	TURSS Models.  Certain TURSS consumer reports made available to Subscriber may consist of models that estimate a consumer’s income (the “Models”).  The Models may be made available to Subscriber in conjunction with its request for tenant screening services.  Subscriber represents and warrants that when Subscriber requests Models in conjunction with its tenant screening requests, Subscriber shall not use such Models, nor any information derived therefrom: (i) to take any adverse action as to any individual consumer, or, (ii) for any other reason, including, but not limited to, and action relating to the lease of Subscriber’s residential property.	Such Models may produce Scores, which are confidential and proprietary to TURSS and subject to the confidentiality provisions of section 2.2.2, above.
2.5	Subscriber Forms.  TURSS may offer the ability to electronically maintain and make available to Subscriber, at Subscriber’s request and direction, Subscriber’s forms including, but not limited to, lease forms, lease addenda and consumer correspondence.  Subscriber acknowledges and agrees that it is Subscriber’s obligation to ensure the accuracy and completeness of the forms and to ensure its compliance with all applicable laws related to the use of such forms.  TURSS makes no representations or warranties as to the content or use of such forms.
2.6	Subscriber Access.  Subscriber agrees that TURSS may store data provided to Subscriber hereunder on behalf of Subscriber to be used by Subscriber solely for audit purposes and for no other purpose.  All data stored on behalf of Subscriber by TURSS shall be owned by Subscriber and may not be modified in any manner.
3.	 Additional Terms and Conditions.
3.1	Confidentiality.  Subscriber shall hold all Services Information in confidence and shall not disclose the Services to any third party, except as required by law (i.e., an order of a court or data request from an administrative or governmental agency with competent jurisdiction) to be disclosed; provided however, that Subscriber shall provide TURSS ten (10) days prior written notice before the disclosure of such information pursuant to this Paragraph 5.1.  However, this restriction shall not prohibit Subscriber from disclosing to the subject of the Consumer Report Information, who is the subject of an adverse action, the content of the Consumer Report Information as it relates to any such adverse action.
3.2	Web Site Access.  TURSS will provide Subscriber with access to TURSS’s web site (the “TURSS Site”) so that Subscriber may, by accessing the TURSS Site, (i) initiate Applicant Reviews and Tenant Reviews and (ii) obtain or review TURSS’s Recommendations to Subscriber.  TURSS will assign one or more passwords and identification numbers (“Program Codes”) to Subscriber for use in accessing the TURSS Site.  Subscriber represents and warrants that it will use its best reasonable efforts to ensure that: (1) only authorized Subscriber employees have access to the TURSS Site through Workstations; (2) TURSS Services obtained by Subscriber via the TURSS Site are not accessible by unauthorized parties via Subscriber’s connection to the Internet or otherwise; (3) all Passwords are kept confidential and secure by such authorized Subscriber employees (e.g., Subscriber shall ensure that Passwords are not stored on any Workstation nor other storage and retrieval system and/or media and that Internet browser caching functionality is not used to store Passwords; (4) each User ID and Password is used solely by the authorized Subscriber employee to whom such User ID and Password was issued; and (5) all documentation and other materials provided by TURSS to Subscriber under this Agreement are held in confidence by Subscriber (and accessible only to those Subscriber employees who Subscriber has authorized to use the TURSS Site).  Subscriber shall immediately notify TURSS if a Subscriber user with access to Program Codes no longer works for Subscriber and shall be fully responsible for any use of the TURSS site by users accessing the site through the Program Codes assigned to the Subscriber.  In the event of any compromise of security involving User Ids or Passwords, Subscriber shall immediately notify TURSS.
3.3	Safeguards.  Each party shall implement, and shall take measures to maintain, reasonable and appropriate administrative, technical, and physical security safeguards (“Safeguards”) to (a) insure the security and confidentiality of non-public personal information; (b) protect against anticipated threats or hazards to the security or integrity of non-public personal information; and (c) protect against unauthorized access or use of non-public personal information that could result in substantial harm or inconvenience to any consumer.  When a consumer’s first name or first initial and last name in combination with a social security number, driver’s license or Identification Card Number, or account number, credit or debit card number, in combination with any required security code, access code, or password that would permit access to an individual’s financial account (“Personal Information”), is delivered to Subscriber unencrypted, Subscriber shall implement and maintain reasonable security procedures and practices appropriate to the nature of the information and to protect the Personal Information from unauthorized access, destruction, use, modification, or disclosure.  Subscriber shall notify TURSS in writing as soon as practicable but in no event later than forty-eight hours after which Subscriber becomes aware of any potential and/or actual misappropriation of, and/or any unauthorized disclosures of, any information provided to Subscriber by TURSS, including, but not limited to theft, loss or interception of Consumer Report Information, unauthorized use of TURSS subscriber codes and passwords, unauthorized entry to the facilities where TURSS data may have been accessible, or unauthorized release of or access to TURSS data by an employee or Agent of Subscriber. Subscriber shall fully cooperate with TURSS in any communications to consumers regarding the data incident and mitigating, to the extent practicable, any damages due to such misappropriation and/or unauthorized disclosure.  Such cooperation shall include, but not necessarily be limited to, allowing TURSS to participate in the investigation of the cause and extent of such misappropriation and/or unauthorized disclosure.  Such cooperation shall not relieve Subscriber of any liability it may have as a result of such a misappropriation and/or unauthorized disclosure.  Moreover, without TURSS’s prior consent, Subscriber shall make no public notification, including but not limited to press releases or consumer notifications, of the potential or actual occurrence of such misappropriation and/or unauthorized disclosure of any such information provided to Subscriber.
3.4	Authorized Requests.  Subscriber shall use the Services: (a) for its certified permissible purpose above to assist in making a residential or storage leasing decision; (b) solely for Subscriber’s exclusive one-time use; and (c) subject to the terms and conditions of this Agreement.  Subscriber shall not request, obtain or use Services for any other purpose including, but not limited to, for the purpose of selling, leasing, renting or otherwise providing information obtained under this Agreement to any other party, whether alone, in conjunction with Subscriber’s own data, or otherwise in any service which is derived from the Services.  Services shall be requested by, and disclosed by Subscriber to only Subscriber’s designated and authorized employees having a need to know and only to the extent necessary to enable Subscriber to use the Services in accordance with this Agreement.  Subscriber shall ensure that such Subscriber designated and authorized employees shall not attempt to obtain any Services on themselves, associates, or any other person except in the exercise of their official duties.
3.5	Third Party Intermediaries.  In the event Subscriber will utilize a third party intermediary (e.g., Internet service provider or other network provider) for the purpose of receiving Services, Subscriber shall first enter into an agreement with such third party under which such third party acts solely as a network conduit for the delivery of the Services to Subscriber and which prohibits such third party from using, or otherwise accessing, the Services for any other purpose.  Subscriber shall be solely liable for any actions or omissions of such third parties which result in a breach of this Agreement.
3.6	Rights to Services.  Subscriber shall not attempt, directly or indirectly, to reverse engineer, decompile, or disassemble Services or any confidential or proprietary criteria developed or used by TURSS relating to the Services provided under this Agreement.  Except as explicitly set forth in this Agreement, the entire right, title and interest in and to the Services shall at all times vest exclusively in TURSS.  TURSS reserves all rights not explicitly granted to Subscriber under this Agreement.
3.6.1	Notwithstanding anything to the contrary in the Agreement, TURSS hereby grants a limited, non-exclusive, non-transferable license to the Public Record Information, and the Services derived from the Public Record Information, from TURSS and that the material content of the Public Record Information and the Consumer Reports delivered by TURSS may not be altered, edited, or otherwise changed without the prior written consent from TURSS.
3.7	Fees and Payments.  Though Subscriber has the option to request that the applicant/prospective tenant pay for the Services, the Subscriber is ultimately responsible to TURSS for the full payment of the Services.  The fees associated with the Services are as stated on this website and are incorporated by reference.  Upon delivery of the Services, Subscriber will be responsible for immediate payment, and outstanding amounts will be subject to a late charge of one and one-half percent (1.5%) per month (18% per year) or the maximum allowed by law, whichever is less.  If collection efforts are required, Subscriber shall pay all costs of collection, including reasonable attorney’s fees.  Any periodic and/or minimum Subscriber fees under this Agreement are non-refundable, in whole or in part, in the event of a termination of this Agreement.  TURSS reserves the right to change the fees and charges from time to time, with such changes referenced on this website.
3.7.1	In addition, in the event that TURSS’s cost of rendering Services increases as a result of federal, state or local laws, ordinances or other regulatory, administrative or governmental acts, then TURSS may implement a surcharge subject to the following: (a) any surcharge will be applicable generally to TURSS’s customers; and (b) any surcharge will be applied only to services pertaining to consumers in the geographic area so affected.  A legislative surcharge is imposed on certain types of reports pertaining to consumers residing in the United States, and an additional surcharge is imposed on certain reports pertaining to only Colorado residents.
3.8	Term, Termination and Survival.  The term of this Agreement shall commence upon the agreeing to the terms of this Agreement and shall remain in effect until terminated by any party hereto for any reason whatsoever.
3.8.1	With the exception of TURSS’s obligation to provide Services under this Agreement, all provisions of this Agreement shall survive any such termination of this Agreement including, but not limited to, all restrictions on Subscriber’s use of Services Information.  Moreover, any such termination shall not relieve Subscriber of any fees or other payments due to TURSS through the date of any such termination nor affect any rights, duties or obligations of either party that accrue prior to the effective date of any such termination.
3.9	Limited Warranty.  TURSS represents and warrants that the Services will be provided in a professional and workmanlike manner consistent with industry standards.  TURSS DOES NOT WARRANT THE SERVICES TO BE UNINTERRUPTED OR ERROR-FREE OR THAT THE SERVICES WILL MEET SUBSCRIBER’S REQUIREMENTS. THE WARRANTY SET FORTH IN THIS SECTION 5.10 IS IN LIEU OF ALL OTHER WARRANTIES, WHETHER STATUTORY, EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, WARRANTIES THAT MIGHT BE IMPLIED FROM A COURSE OF PERFORMANCE OR DEALING OR TRADE USAGE OR WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
3.10	Limitation of Liability. TURSS’S SOLE LIABILITY, AND SUBSCRIBER’S SOLE REMEDY, FOR VIOLATIONS OF THIS AGREEMENT BY TURSS OR FOR BREACH OF TURSS’S OBLIGATIONS SHALL BE THE CORRECTION OF ANY DEFECTIVE SERVICE OR THE REFUND OF FEES PAID FOR SAME.
3.10.1	IN NO EVENT SHALL TURSS BE LIABLE FOR ANY CONSEQUENTIAL, INCIDENTAL, INDIRECT, SPECIAL, OR PUNITIVE DAMAGES IN CONNECTION WITH THIS AGREEMENT, INCLUDING BUT NOT LIMITED TO LOSS OF GOOD WILL AND LOST PROFITS OR REVENUE, WHETHER OR NOT SUCH LOSS OR DAMAGE IS BASED IN CONTRACT, WARRANTY, TORT, NEGLIGENCE, STRICT LIABILITY, INDEMNITY, OR OTHERWISE, EVEN IF TURSS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  THESE LIMITATIONS SHALL APPLY NOTWITHSTANDING ANY FAILURE OF ESSENTIAL PURPOSE OF ANY LIMITED REMEDY.
3.10.2	ADDITIONALLY, TURSS SHALL NOT BE LIABLE TO SUBSCRIBER FOR ANY AND ALL CLAIMS ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT BROUGHT MORE THAN TWO (2) YEARS AFTER THE CAUSE OF ACTION HAS ACCRUED.
3.10.3		Notwithstanding anything to the contrary in the Agreement, use of Public Record Information, and the Services derived from the Public Record Information, from TURSS shall be subject to the following:  THE PUBLIC RECORD INFORMATION IS PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS.  TURSS AND ITS DATA PROVIDERS MAKE NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, WITH RESPECT TO THE PUBLIC RECORD INFORMATION AND DISCLAIM ANY EXPRESS OR IMPLIED WARRANTIES WITH RESPECT THERETO.  WITHOUT LIMITING THE FOREGOING, TURSS AND ITS DATA PROVIDERS DO NOT GUARANTEE OR WARRANT THE ACCURACY, TIMELINESS, COMPLETENESS, CURRENTNESS, MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE OF THE PUBLIC RECORD INFORMATION AND SHALL NOT BE LIABLE FOR ANY LOSS OR INJURY ARISING OUT OF OR CAUSED IN WHOLE OR IN PART BY USE OF THE PUBLIC RECORD INFORMATION.  Subscriber shall indemnify, defend, and hold harmless TURSS and its data providers, from and against any and all liabilities, damages, losses, claims, costs, fees, and expenses (including but not limited to reasonable attorney and expert witness fees and expenses) arising out of or related to Subscriber’s use of the Public Record Information obtained from TURSS.  Subscriber acknowledges and agrees that TURSS’s data providers are a third party beneficiary of the provisions of this section, with right of enforcement.
3.11	Assignment and Subcontracting.  Neither party may assign or otherwise transfer this Agreement, in whole or in part without the prior written consent of the other. Notwithstanding the foregoing, TURSS may assign or transfer this Agreement to a wholly-owned subsidiary or in the event of a purchase of substantially all of TURSS’s assets or in the event of a corporate form reorganization (e.g., LLC to C-Corporation).  Moreover, TURSS shall have the unrestricted right to subcontract the Services to be provided to Subscriber by TURSS under this Agreement; provided however, that such subcontracting shall not relieve TURSS of its obligations under this Agreement.  The limited warranty and limitation of liability provisions set forth in this Agreement shall also apply for the benefit of TURSS’s licensors, subcontractors and agents.
3.12	No Waiver.  No failure on the part of either party to enforce any covenant, agreement, or condition of this Agreement shall operate as a discharge of such covenant, agreement, or condition, or render the same invalid, or impair the right of either party to enforce the same in the event of any subsequent breach by the other party.
3.13	Independent Contractors.  This Agreement is not intended to create or evidence any employer-employee arrangement, agency, partnership, joint venture, or similar relationship.
3.14	Severability.  Whenever possible, each provision of this Agreement will be interpreted in such manner as to be effective and valid under applicable law, but if any provision of this Agreement is held to be prohibited by or invalid under applicable law, such provision will be ineffective only to the extent of such prohibition or invalidity, without invalidating the remainder of this Agreement.
3.15	Force Majeure.  TURSS shall not be liable for any delay in performance or failure to perform under this Agreement if such delay or failure us caused by conditions beyond TURSS’ reasonable control.
3.16	Audit Rights. During the term of this Agreement and for a period of five (5) years thereafter, TURSS may audit Subscriber’s policies, procedures and records which pertain to this Agreement, to ensure compliance with this Agreement, upon reasonable notice and during normal business hours.
3.17	Governing Law.  This Agreement shall be construed and governed by the laws of the State of Illinois, without reference to the choice of law principles thereof.
3.18	Notices.  Subscriber acknowledges and agrees that any notice provided by TURSS to any electronic mail address provided by Subscriber shall suffice for proper notice under this Agreement.  Additionally, all of Subscriber’s communications or notices required or permitted by this Agreement shall be sufficiently given for all purposes hereunder if given in writing and delivered to TURSS (i) personally, (ii) by United States first class mail, (iii) by reputable overnight delivery service, (iv) by electronic mail, or (v) by facsimile.  All notices delivered in accordance with this Section for TURSS shall be sent to the appropriate address or number, as set forth below:
	      TURSS:	TransUnion Rental Screening Solutions
                6430 S. Fiddler’s Green Circle, Suite 500,
                Greenwood Village, CO 80111

3.19	Trademarks.   Both Subscriber and TURSS shall submit to the other party for written approval, prior to use, distribution, or disclosure, any material including, but not limited to, all advertising, promotion, or publicity in which any trade name, trademark, service mark, and/or logo (hereinafter collectively referred to as the “Marks”) of the other party are used (the “Materials”).  Both parties shall have the right to require, at each party’s respective discretion and as communicated in writing, the correction or deletion of any misleading, false, or objectionable material from any Materials.  Neither party shall remove any of the other party’s Marks from any information materials or reports provided to the other party and shall comply with the other party’s instructions with respect to the use of any such Marks.  Moreover, when using the other party’s Marks pursuant to this Agreement, a party shall take all reasonable measures required to protect the other party’s rights in such Marks, including, but not limited to, the inclusion of a prominent legend identifying such Marks as the property of the other party. In using each other’s Marks pursuant to this Agreement, each party acknowledges and agrees that (i) the other party’s Marks are and shall remain the sole properties of the other party, (ii) nothing in this Agreement shall confer in a party any right of ownership in the other party’s Marks, and (iii) neither party shall contest the validity of the other party’s Marks. Notwithstanding anything in this Agreement to the contrary, without the prior written approval of Subscriber, TURSS shall have the right to disclose to third parties Subscriber’s marks in consumer credit reports containing Subscriber’s account information.
3.20	By signing this Agreement, Subscriber acknowledges receipt of a copy of the Federal Trade Commission’s “Notice to Users of Consumer Reports: Obligations of Users Under the FCRA” and a copy of the Federal Trade Commission’s “Notices to Furnishers of Information: Obligations of Furnishers Under the FCRA”.
3.21	The individual executing this Agreement has direct knowledge of all facts certified and the authority to both execute this Agreement on behalf of Subscriber and bind Subscriber to the terms of this Agreement.
3.22	ID Manager Service:  In connection with the Services, Subscriber desires to obtain TransUnion’s ID Manager Service pursuant to the following additional terms and conditions:
3.22.1	With respect to request for ID Manager Service, Subscriber hereby certifies that its use of the ID Manager Service will be requested, obtained and used for one or more of the following permitted uses as described in, and as may be interpreted from time to time, by competent legislative, regulatory or judicial authority, as being encompassed by, Section (6802) (e) of the Gramm-Leach-Bliley Act (GLB), Title V, Subtitle A, Financial Privacy (15 U.S.C. § 6801-6809) and the United States Federal Trade Commission rules promulgated thereunder.
o	To protect against or prevent actual fraud, unauthorized transactions, claims or other liability; or
o	To comply with Federal, State or local laws, rules and other applicable legal requirements.
3.22.2	Subscriber further represents that:
(a) Subscriber shall not request, obtain or use such ID Manager Service for any other purpose including, but not limited to, in whole or in part, as a factor in establishing an individual’s creditworthiness or eligbility for (i) credit or insurance, or (ii) employment, nor for any other purpose under the FCRA.  Moreover, Subscriber shall not take any adverse action, which is based in whole or in part on the ID Manager Service, against any consumer.
(b) Subscriber shall comply with all other applicable federal, state and local laws, statutes, rules and regulations including, but not limited to, the Drivers Privacy Protection Act.
3.22.3	To the extent that the ID Manager Service requested by Subscriber utilize, in whole or in part, Consumer Reports as defined in the FCRA, Subscriber certifies that it will request and use each such request ID Manager Service solely for one of the permissible purposes certified below:
o	In connection with a credit transaction involving the individual on whom the information is to be furnished and involving the extension of credit to the individual.
o	Pursuant to the written authorization of the individual who is subject of the individual ID Manager Service request.
o	In connection with a business transaction initiated by the individual.
3.22.4	Subscriber expressly acknowledges and agrees that where the ID Manager Service to be provided to Subscriber under this Agreement utilizes Consumer Reports TURSS expresses no opinion regarding a Consumer’s creditworthiness in rendering such ID Manager Service.  Moreover, in the event Subscriber’s requested ID Manager Service utilize, in whole or in part, Consumer Reports, without limiting Subscriber’s obligations set forth elsewhere in this Agreement, Subscriber shall comply with any and all adverse action notice requirements of the FCRA.

Entire Agreement.  THIS AGREEMENT INCLUDING, WITHOUT LIMITATION, ALL EXHIBITS AND ATTACHMENTS HERETO, CONSTITUTES THE ENTIRE AGREEMENT BETWEEN TURSS AND SUBSCRIBER AND SUPERSEDES ALL PREVIOUS AGREEMENTS AND UNDERSTANDINGS, WHETHER ORAL OR WRITTEN, EXPRESS OR IMPLIED, SOLELY WITH RESPECT TO THE SUBJECT MATTER OF THIS AGREEMENT.  THIS AGREEMENT MAY NOT BE ALTERED, AMENDED, OR MODIFIED EXCEPT BY WRITTEN INSTRUMENT SIGNED BY THE DULY AUTHORIZED REPRESENTATIVES OF BOTH PARTIES.


</textarea>
                                </div>
                                <div class="form-group height-auto">
                                    <div class="checkbox"> <label><input type="checkbox" name="accept_term" value="1">I accept the smartmove service agreement</label></div>
                                </div>
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="mr-2 greenplan-button" data-dismiss="modal">Cancel</a>
                                    <a href="javascript:void(0)" class="yellow-button saveAccountType">Submit</a>
                                </div>
                            </div>
                            <div class="col-sm-5 pull-right">
                                <h4>Tenant Screening you can trust:</h4>
                                <p class="text">SmartMove is a product developed by TransUnion, a trusted source of
                                    consumer data and a global leader in credit and information management.</p>
                                <p class="text">SmartMove ensures that:</p>
                                <ul class="list">
                                    <li>Your credit rat ng is completely unaffected</li>
                                    <li>The screening process is quick easy—and all online</li>
                                    <li>Only limited Information is provided to the property manager — your SSN and account numbers are not provided</li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
            </div>

        </div>
    </div>

    <div id="multistep-signup-modal" class="modal fade smart-move-modal" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header pt-0 pb-0">
                    <header class="smart-move-header smart-move-header-black">
                        <div class="header-top">
                            <div class="container">
                                <div class="row d-flex align-item-center">
                                    <div class="col-sm-6">
                                        <a href="index.html" class="logo"><img src="<?php echo COMPANY_SITE_URL; ?>/images/white-logo.png" alt="logo" class="img-responsive"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>
                </div>
                <div class="modal-body p-0">
                    <div class="mutistep-form-top">
                        <h2 class="sm-main-heading black">Sign up for SmartMove <a href="#" class="product-edit-icon"><i class="fa fa-edit"></i></a><a href="#" class="product-edit-icon"><i class="fa fa-envelope"></i></a></h2>
                        <div class="mutistep-form-top-inner">
                            <div class="stepwizard">
                                <div class="stepwizard-row setup-panel">
                                    <div class="stepwizard-step">
                                        <a href="#step-1" type="button" class="btn btn-primary btn-circle btn-circle1">1</a>
                                        <p>Create Account</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <a href="#step-2" type="button" class="btn btn-default btn-circle btn-circle2" disabled="disabled">2</a>
                                        <p>Personal Information</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <a href="#step-3" type="button" class="btn btn-default btn-circle btn-circle3" disabled="disabled">3</a>
                                        <p>Confirmation</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--<form role="form" action="" method="post">-->
                        <div class="row setup-content" id="step-1">
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <h3> Step 1 - Create Account</h3>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Create SmartMove Password<span class="required">*</span></label>
                                                <input type="password" class="form-control" placeholder="Enter Password" name="l_password"/>
                                                <p class="eight-fifeteen pass_error" style="color: red"></p>
                                                <p class="eight-fifeteen">8 to 15 character long including one capital letter, one lower case letter, and one number or special character</p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Confirm Password<span class="required">*</span></label>
                                                <input type="password" class="form-control" placeholder="Confirm Password" name="lc_password"  />
                                                <p class="eight-fifeteen cpass_error"></p>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <p class="questionstype">The questions below will be used to help you log into your account if you forget your password</p>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Security Question 1<span class="required">*</span></label>
                                                <select name="security_question_1" form="carform">
                                                    <option value="">Please Select</option>
                                                    <option value="1">What was the make and model of your first car?</option>
                                                    <option value="2">What is your mother’s middle name?</option>
                                                    <option value="3">What is your father’s middle name?</option>
                                                    <option value="4">What city were you born in?</option>
                                                    <option value="5">What is your grandmother's first name (on your mother's side)?</option>
                                                    <option value="6">What is your grandfather's first name (on your father's side)?</option>
                                                    <option value="7">What is your spouse’s middle name?</option>
                                                    <option value="8">What is your favorite teacher’s name?</option>
                                                    <option value="9">What is your favorite state/country?</option>
                                                    <option value="10">What is your favorite vacation spot?</option>
                                                    <option value="11">What was your high school mascot?</option>
                                                    <option value="12">What was your first grade teacher’s last name?</option>
                                                    <option value="13">What is your anniversary (mm/dd/yyyy)?</option>
                                                </select>

                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Security Question 2<span class="required">*</span></label>
                                                <select name="security_question_2" form="carform">
                                                    <option value="">Please Select</option>
                                                    <option value="1">What was the make and model of your first car?</option>
                                                    <option value="2">What is your mother’s middle name?</option>
                                                    <option value="3">What is your father’s middle name?</option>
                                                    <option value="4">What city were you born in?</option>
                                                    <option value="5">What is your grandmother's first name (on your mother's side)?</option>
                                                    <option value="6">What is your grandfather's first name (on your father's side)?</option>
                                                    <option value="7">What is your spouse’s middle name?</option>
                                                    <option value="8">What is your favorite teacher’s name?</option>
                                                    <option value="9">What is your favorite state/country?</option>
                                                    <option value="10">What is your favorite vacation spot?</option>
                                                    <option value="11">What was your high school mascot?</option>
                                                    <option value="12">What was your first grade teacher’s last name?</option>
                                                    <option value="13">What is your anniversary (mm/dd/yyyy)?</option>
                                                </select>

                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Security Question 3<span class="required">*</span></label>
                                                <select name="security_question_3" form="carform">
                                                    <option value="">Please Select</option>
                                                    <option value="1">What was the make and model of your first car?</option>
                                                    <option value="2">What is your mother’s middle name?</option>
                                                    <option value="3">What is your father’s middle name?</option>
                                                    <option value="4">What city were you born in?</option>
                                                    <option value="5">What is your grandmother's first name (on your mother's side)?</option>
                                                    <option value="6">What is your grandfather's first name (on your father's side)?</option>
                                                    <option value="7">What is your spouse’s middle name?</option>
                                                    <option value="8">What is your favorite teacher’s name?</option>
                                                    <option value="9">What is your favorite state/country?</option>
                                                    <option value="10">What is your favorite vacation spot?</option>
                                                    <option value="11">What was your high school mascot?</option>
                                                    <option value="12">What was your first grade teacher’s last name?</option>
                                                    <option value="13">What is your anniversary (mm/dd/yyyy)?</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Answer</label>
                                                <input maxlength="100" type="text" class="form-control"/>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Answer</label>
                                                <input maxlength="100" type="text" class="form-control"/>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Answer</label>
                                                <input maxlength="100" type="text" class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <p class="eight-fifeteen security_error" style="color:red"></p>
                                    </div>
                                    <div class="setupbutton">
                                        <button class="btn yellow-button nextBtn btn-lg pull-right getlandlordinfo" type="button" >Next</button>
                                    </div>
                                </div>
                                <div class="pop-footer"></div>
                            </div>

                        </div>


                        <div class="row setup-content" id="step-2">
                            <form role="form" action="" method="post" id="smartmove_personal_info">
                                <div class="col-xs-12">
                                    <div class="col-md-12">
                                        <h3> Step 2 - Personal Information</h3>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">First Name<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="first_name"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">Middle Name</label>
                                                    <input type="text" class="form-control" name="middle_name"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">Last Name<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="last_name"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <label class="control-label">Street Address<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="address1"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">Address Line2:</label>
                                                    <input type="text" class="form-control" name="address2"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">City<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="city"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">State<span class="required">*</span></label>
                                                    <select name="state"></select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">Zip Code:<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="zipcode"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">Primary Phone:<span class="required">*</span></label>
                                                    <input type="tel" class="form-control" name="phone_number"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">Alternate Phone:</label>
                                                    <input type="tel" class="form-control" name="phone_number2"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <label class="control-label">Alternate Phone:</label>
                                                            <input type="tel" class="form-control" name="phone_number3"/>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label class="control-label">EXT</label>
                                                            <input type="tel" class="form-control" name="extension"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">Role<span class="required">*</span></label>
                                                    <select name="role">
                                                        <option value="Both">Both</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">No of Units:<span class="required">*</span></label>
                                                    <input type="number" class="form-control" name="units"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">Position<span class="required">*</span></label>
                                                    <select name="position"></select>
                                                </div>
                                            </div>
                                            <h3 class="companyinformation">Company Information</h3>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label">Company Name (if applicable)</label>
                                                    <input type="text" class="form-control" name="company_name"/>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Street Addres</label>
                                                    <input type="text" class="form-control" name="company_address1"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Address Line 2 </label>
                                                    <input type="text" class="form-control" name="company_address2"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">City</label>
                                                    <input type="text" class="form-control" name="company_city"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">State</label>
                                                    <select name="company_state" form="">
                                                        <option value="volvo">Volvo</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">ZIP Code</label>
                                                    <input type="number" class="form-control" name="company_zipcode"/>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="setupbutton-radiobutton">
                                            <h3>Payment Information</h3>
                                            <p>Set default payment credit card?</p>
                                            <div class="setupbutton-radiobutton-label">
                                                <label class="radio-inline">
                                                    <input type="radio" name="optradio" checked>Yet Let's do that now
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="optradio">No I'll do that later
                                                </label>
                                            </div>
                                        </div>
                                        <div class="setupbutton setupbutton-step2">
                                            <button class="btn yellow-button nextBtn btn-lg pull-right confirm_info_details" type="button" name="nextinfo">Next</button>
                                            <a class="btn greenplan-button yellow-button btn-lg pull-right" id="btn-circle1" href="#step-1" >Previous</a>
                                        </div>

                                    </div>
                                    <div class="col-md-12 pop-footer"></div>
                                </div>

                            </form>
                        </div>
                        <div class="setup-content setup-content3" id="step-3">
                            <h3>Step 3 - Confirm Account Details</h3>
                            <h5>Please confirm the details of your account</h5>

                            <div class="setup-content3-item">
                                <p>Personal Information</p>
                                <h4>First Name:<span class="sh_first_name">Scott</span></h4>
                                <h4>Middle Name:<span class="sh_middle_name">Scott</span></h4>
                                <h4>Last Name:<span class="sh_last_name">Adams</span></h4>
                                <h4>Street Address<span class="sh_address1">222 2 Summerside</span></h4>
                                <h4>Address Line2: <span class="sh_address2"></span></h4>
                                <h4>City<span class="sh_city">San Diego</span></h4>
                                <h4>State<span class="sh_state">CA</span></h4>
                                <h4>Zip Code<span class="sh_zipcode">92101</span></h4>
                                <h4>Primary Phone<span class="sh_phone_number">222-222-3333</span></h4>
                                <h4>Alternate Phone<span class="sh_phone_number2">333-444-0000</span></h4>
                                <h4>Alternate Phone<span class="sh_phone_number3">222-222-2222</span></h4>
                                <h4>EXT<span class="sh_extension">123</span></h4>
                            </div>

                            <div class="setup-content3-item">
                                <p>Professional Information</p>
                                <h4>Role:<span class="sh_role">Both</span></h4>
                                <h4>No of Units:<span class="sh_units">2</span></h4>
                                <h4>Position:<span class="sh_position">Landlord</span></h4>
                            </div>

                            <div class="setup-content3-item">
                                <p>Company Information</p>
                                <h4>Company Information (if applicable):<span class="sh_company_name">Scott</span></h4>
                                <h4>Stree Address:<span class="sh_company_address1">Scott</span></h4>
                                <h4>Address Line 2:<span class="sh_company_address2">Adams</span></h4>
                                <h4>City:<span class="sh_company_city"></span></h4>
                                <h4>State: <span class="sh_company_state">Choose a state</span></h4>
                                <h4>Zip Code:<span class="sh_company_zipcode">San Diego</span></h4>
                            </div>
                            <div class="setupbutton setupbutton-step2">
                                <button class="btn btn-primary nextBtn btn-lg pull-right confirm_submit_account" type="button" >SUBMIT</button>
                                <a class="btn greenplan-button btn-lg pull-right" id="btn-circle2" href="#step-2" >Previous</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
            </div>

        </div>
    </div>

    <div class="container">
        <div class="modal fade" id="backGroundCheckPop" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-md">
                <div class="modal-content pull-left" style="width: 100%;">
                    <div class="modal-header pull-left">
                        <a style="cursor:pointer;" class="close modalhidevicting" data-dismiss="modal">&times;</a>
                        <h4 class="modal-title text-center">Disclaimer</h4>
                    </div>
                    <div class="modal-body pull-left">
                        <div class="col-sm-12">
                            <p style="font-size: 14px; line-height: 20px;"> I authorize ApexLink Inc. to submit this request to generate and present a background/any check report for the Person/Company I have listed. I understand that ApexLink Inc. is not responsible or liable for any data breach, loss/loss of profits or any claim against the filer (Myself or my company) by any party. I understand that in no event is ApexLink Inc. liable for any and all damages. I also understand that ApexLink Inc. grants no implied warranties, including without limitation, warranties, of merchantability, or of fitness for any particular purpose. </p></div>
                        <div class="col-sm-12"><input type="checkbox" name="" class="background_check_open"><label style="display: inline;"> Check to proceed further.</label></div>
                        <div class="col-sm-12 btn-outer mg-btm-20">
                            <input type="button" class="blue-btn"  data-dismiss="modal" value="Cancel">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="newproperty-modal" class="modal fade smart-move-modal newproperty-page" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body ">
                    <h2 class="sm-main-heading">Welcome <span class="welcome_pm">Scott Adam</span></h2>
                    <div class="mutistep-form-top">
                        <h2 class="sm-main-heading black">Add New Property</h2>
                        <p>Please complete this form to enter a property into SmartMove</p>
                    </div>
                    <form role="form" action="" method="post" id="property_form">
                        <div class="row addnewproperty-content">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Property Name<span class="required">*</span></label>
                                        <select class="form-control" name="property_list" placeholder="Select">
                                            <option value="0">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Unit Number:</label>
                                        <select class="form-control" name="unit_list" placeholder="Select">
                                            <option value="0">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Property ID:</label>
                                        <input type="text" class="form-control" name="property_id" placeholder="" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label class="control-label">Street Address<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="address1" placeholder="100 State St" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Address Line 2:</label>
                                        <input type="text" class="form-control" name="address2" placeholder="100 State St" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">City:<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="city" placeholder="55 Ocean Drive" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">State<span class="required">*</span></label>
                                        <select name="state" class="form-control"></select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">ZIP Code:<span class="required">*</span></label>
                                        <input type="number" name="zipcode" class="form-control" placeholder="92101" readonly>
                                    </div>
                                </div>
                            </div>


                            <div class="newproperty-modal-setup">
                                <h6>Enter your default and deposit amounts for this property. You'll have the ability to adjust these amounts for each individual application.</h6>
                                <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$'; ?>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Rent Amount (<?php echo $default_symbol; ?>):<span class="required">*</span></label>
                                        <input type="number" name="rent_amount" class="form-control" placeholder="">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Deposit Amount(<?php echo $default_symbol; ?>):<span class="required">*</span></label>
                                        <input type="number" name="deposite_amount" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <p>Which of the following criteria do you want considered in the communication</p>
                        </div>

                        <div class="newproperty-modal-setup-checkbox">
                            <input type="checkbox" name="bankrupters_check" value="1"> Automatically decline for Open Bankrupters within the last<textarea name="bankrupters_text" class="form-control" readonly="readonly"></textarea>
                            <label id="bankrupters_text-error" class="error" style="display: none; width:auto;">Month limit cannot be greater than 12 </label>months<br>
                        </div>
                        <div class="newproperty-modal-setup-checkbox">
                            <a class="btn mr-2 greenplan-button" data-dismiss="modal">Cancel</a>
                            <a class="btn  yellow-button save_property">Submit</a>
                        </div>

                    </form>

                </div>


            </div>

        </div>
    </div>


    <div id="verify-property" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog spotlight-pop">

            <!-- Modal content-->
            <div class="modal-content">
                <form role="form">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <div class="ce-loginbox-content-form">
                            <div class="verify-property-heading">
                                <h4 class="verify-property">Please Verify New Property Details</h4>
                            </div>

                            <div class="verify-content-verification">
                                <h4>Property ID: <span class="property_id">55 OceanDrive-Unit404</span></h4>
                                <h4>Street Address: <span class="address1">55 OceanDrive-Unit404</span></h4>
                                <h4>City: <span class="city">55 OceanDrive-Unit404</span></h4>
                                <h4>State: <span class="state">55 OceanDrive-Unit404</span></h4>
                                <h4>ZIP Code: <span class="zipcode">92101</span></h4>
                            </div>

                            <div class="verify-content-verification">
                                <h4>Services selected for payment:</h4>
                                <ul>
                                    <li>Credit Report</li>
                                    <li>Criminal Report</li>
                                    <li>Eviction Report</li>
                                    <li>Credit Recommendation</li>
                                </ul>
                                <p>What do you want to do next?</p>
                            </div>

                            <div class="verify-content-button">
                                <!--<button type="button" class="verify-button"><i class="fa fa-download" aria-hidden="true"></i>GO TO MY DASHBOARD</button>-->
                                <!--<button type="button" class="verify-button" id="verify-button1"><i class="fa fa-plus" aria-hidden="true"></i>ADD LEASING AGENTS</button>-->
                                <a class="verify-button" id="verify-button2"><i class="fa fa-plus" aria-hidden="true"></i>CREATE APPLICATION FOR THIS PROPERTY</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div id="newapp-property" class="modal fade smart-move-modal newproperty-page newapp-property" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-body ">
                    <div class="mutistep-form-top">
                        <h2 class="sm-main-heading black">Create New Application<a href="#" class="product-edit-icon" data-toggle="modal" data-target="#verify-property"><i class="fa fa-edit"></i></a></h2>
                        <p>Use this form to create a shape application for a property.  You can include multiple applications (i.e. roomates) as well as cosigner.  To create an application for seperate individual applying for this property.  You can create another application after this one.</p>
                    </div>
                    <form role="form" action="" method="post" id="application_form">
                        <div class="row addnewproperty-content">

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Rental Property ID<span class="required">*</span></label>
                                            <input type="text" name="app_property_id" class="form-control">
                                            <a href="/Property/AddProperty">Create New Property</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Rental Property Address:</label>
                                            <textarea name="property_address" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Unit Number:</label>
                                            <select class="form-control" placeholder="" name="unit"></select>
                                        </div>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Rent Amount ($per month)<span class="required">*</span></label>
                                        <input type="text" class="form-control" placeholder="" name="rent_amount">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Deposit Amount ($ per month):</label>
                                        <input type="text" class="form-control" placeholder="" name="security_deposite">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Lease Term<span class="required">*</span></label>
                                        <select name="lease_term" form="carform" class="form-control"></select>
                                    </div>
                                </div>
                            </div>
                         <div class="row">
                             <div class="col-sm-4">
                                 <div class="form-group">
                                     <label class="control-label">Applicant Email:<span class="required">*</span></label>
                                     <input type="email" class="form-control" name="tenant_email" readonly>
                                 </div>
                             </div>
                             <div class="col-sm-4">
                                 <div class="form-group">
                                     <label class="control-label">Joint Applicant's email address:</label>
                                     <input type="text" class="form-control" name="additional_tenants_emails" readonly>
                                 </div>
                             </div>

                             <div class="col-sm-4">
                                 <div class="form-group">
                                     <label class="control-label">Cosigner's email address:</label>
                                     <input type="text" class="form-control" name="guarantors_emails" readonly>
                                 </div>
                             </div>
                         </div>

                        </div>

                        <div class="newproperty-feature row">
                            <h5>Select a Payee</h5>
                            <h6>You can choose to have the renter pay for the application or pay for it yourself. It's your choice</h6>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="radio-inline">
                                        <input type="radio" name="pay_by" value="1"><em>Renter will pay for the services</em>
                                    </label>
                                    <ul>
                                        <li>View Criminal Report</li>
                                        <li>Credit Recommendation</li>
                                        <li>View Credit Report</li>
                                        <li>View Eviction Report</li>
                                    </ul>
                                </div>

                                <div class="col-sm-5">
                                    <label class="radio-inline">
                                        <input type="radio" name="pay_by" value="2"><em>Landlord will pay for the services</em>
                                    </label>
                                    <ul>
                                        <li>View Criminal Report</li>
                                        <li>View Eviction Report</li>
                                        <li>Credit Recommendation</li>
                                        <li>View Eviction Report</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="errorTxt" style="min-height: 20px;"></div>
                        </div>

                        <div class="newproperty-table row">
                            <div class="row">
                                <div class="col-sm-5">
                                    <table>
                                        <thead>
                                        <th>Payment Summary</th>
                                        </thead>
                                        <tbody>
                                        <tr class="table-striped">
                                            <td>Criminal Report and Credit Score Recommendation</td><td>$0.00</td>
                                        </tr>
                                        <tr>
                                            <td>View Criminal Report</td><td>0.00</td>
                                        </tr>
                                        <tr class="table-striped">
                                            <td>Credit Recommendation</td><td>0.00</td>
                                        </tr>
                                        <tr>
                                            <td>Full Credit Report <span class="full_credit_report"> $35.00 X # applicants </span></td><td>$0.00</td>
                                        </tr>
                                        <tr class="table-striped">
                                            <td>Coupon Applied</td><td>$0.00</td>
                                        </tr>
                                        <tr class="blackline">
                                            <td></td><td>$0.00</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="newproperty-btn">
                            <button class="btn yellow-button nextBtn btn-lg pull-right " type="button"  style="margin-right:15px;">CANCEL</button>
                            <a href="javascript:void(0)" class="btn save_application_btn btn-lg greenplan-button pull-right" id="btn-circle2"  >SAVE</a>
                            <!--<button type="button">CANCEL</button>
                            <a href="javascript:void(0)" class="save_application_btn">SAVE</a>-->
                        </div>
                    </form>

                </div>


            </div>
        </div>
    </div>

<div id="pm_payment" class="modal fade pm-payment-modal" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y: auto">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body ">
                <table style="width: 100%">
                    <tr>
                        <td>
                            <table style="width: 100%; background: #0095c0;">
                                <tbody>
                                <tr>
                                    <td style="width: 72%; padding: 10px 0 0 20px;">
                                        <img src="<?php echo COMPANY_SITE_URL ?>/images/logo_white.png" alt="footerlogo" border="3" height="45px" width="270px">
                                    </td>
                                    <td style="width: 28%; font-size: 16px; font-style: normal; color: #ffffff; font-weight: 400; padding: 20px 0 0 0;">6430  S. Fiddler's Green Circle</td>
                                </tr>
                                <tr>
                                    <td style="width: 72%;"></td>
                                    <td style="width: 28%; font-size: 16px; font-style: normal; color: #ffffff; font-weight: 400;">
                                        Suite #500 Greenwood Village</td>
                                </tr>
                                <tr>
                                    <td style="width: 72%; font-size: 12px; font-style: normal; color: #ffffff; font-weight: 300; padding: 10px 0 25px 20px;">@2015 TRANSUNION RENTAL SCREENING SOLUTIONS. INC. ALL RIGHT RESERVED  </td>
                                    <td style="width: 28%; font-size: 14px; font-style: normal; color: #ffffff; font-weight: 400; padding: 10px 0 25px 0px;">CO 80111</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="display: inline-block; font-style: normal;  font-weight: 400;font-size: 16px; margin-top: 30px; line-height: 22px;">
                            <b style="font-weight: 600">Below, is the 'Total' (combined) set of charges that will be applied to your Credit/Debit card once each applicant completes their SmartMove vertification.  Each charge is done, separately</b> - based on each applicant, <i><u>sucessfully</u> completing their, individual vertification inquiry with SmartMove</i>
                        </td>
                    </tr>
                    <tr style="padding: 0 0px; display: inline-block; margin-bottom: 30px;">
                        <td style="display: inline-block; font-style: normal;  font-weight: 400;font-size: 16px; margin-top: 20px; line-height: 22px;">
                            <span>*</span>Please review the scheduled charges, below, and <a href="#">Click</a> the button, under the form to approve each change to be applied to your account - for each applicant - as they complete their vertification enquiry <em style="color:red">(No charge, per applicant will be applied until they complete their, individual, vertification inquiry).</em>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 100%">
                                <thead>
                                <th style="width: 100%; display: inline-block;">
                                <td style="background:#fff559; display: flex; height: 40px; align-items: center; padding-left: 15px; font-weight: 600;">Payment Summary</td>
                                </th>
                                </thead>
                                <tbody>
                                <tr class="table-striped" style="width: 100%; display: flex; height: 40px; align-items: center; padding-left: 15px; font-weight: 600;">
                                    <td style="width: 80%">Criminal Report and Credit Score & Recommendation</td>
                                    <td style="width: 20%">$0.00</td>
                                </tr>
                                <tr style="width: 100%; display: flex; height: 40px; align-items: center; padding-left: 15px; font-weight: 600;">
                                    <td style="width: 80%">View Criminal Report</td>
                                    <td style="width: 20%">0.00</td>
                                </tr>
                                <tr class="table-striped" style="width: 100%; display: flex; height: 40px; align-items: center; padding-left: 15px; font-weight: 600;">
                                    <td style="width: 80%">Credit Recommendation</td>
                                    <td style="width: 20%">0.00</td>
                                </tr>
                                <tr style="width: 100%; display: flex; height: 40px; align-items: center; padding-left: 15px; font-weight: 600;">
                                    <td style="width: 40%">Full Credit Report</td>
                                    <td style="width: 40%">$35 x applicant(s)</td>
                                    <td style="width: 20%">35.00</td>
                                </tr>
                                <tr class="table-striped" style="width: 100%; display: flex; height: 40px; align-items: center; padding-left: 15px; font-weight: 600;">
                                    <td style="width: 80%">Coupon Applied</td>
                                    <td style="width: 20%">$0.00</td>
                                </tr>
                                <tr style="background: #555555; width: 100%; display: flex; height: 40px; align-items: center; padding-left: 15px; font-weight: 600;">
                                    <td style="width: 60%; color: #fff">Coupon Applied</td>
                                    <td style="width: 20%; color: #ffffff;">Total</td>
                                    <td style="width: 20%; color: #ffffff;">$0.00</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="width: 100%; display: flex; margin-top: 30px; margin-bottom: 30px; justify-content: center;">
                        <td><a class="pm_payment_btn pm_payment_btn-new" style="background: #0a51f2; font-size: 17px; color: #e3f705;  border-radius: 8px; display: inline-block; padding: 8px;">Pay $35.00 USD</a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
    <div class="modal fade" id="backGroundCheckPop" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content  pull-left" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">Disclaimer</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <p style="font-size: 14px; line-height: 20px;"> I authorize ApexLink Inc. to submit this request to generate and present a background/any check report for the Person/Company I have listed. I understand that ApexLink Inc. is not responsible or liable for any data breach, loss/loss of profits or any claim against the filer (Myself or my company) by any party. I understand that in no event is ApexLink Inc. liable for any and all damages. I also understand that ApexLink Inc. grants no implied warranties, including without limitation, warranties, of merchantability, or of fitness for any particular purpose. </p></div>
                    <div class="col-sm-12"><input type="checkbox" name="" class="background_check_open"><label style="display: inline;"> Check to proceed further.</label></div>
                    <div class="col-sm-12 btn-outer mg-btm-20">
                        <input type="button" class="blue-btn"  data-dismiss="modal" value="Cancel">
                    </div>
                </div>
            </div>
        </div>
    </div>
