<?php

if ((!isset($_SESSION[SESSION_DOMAIN]['Tenant_Portal']['tenant_portal_id'])) && (!isset($_SESSION['Admin_Access']['tenant_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/TenantPortal/login');
}
// echo $_GET['tenant_id']; die();
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
?>


<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_top_navigation.php"); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_portal_header.php"); ?>
<div id="wrapper">

    <div class="popup-bg"></div>
    <style>
        label.error {
            color: red !important;
            font-size: 12px;
            font-weight: 500;
            width: 100% !important;
        }

    </style>
    <script>
        var default_form_data = "";
        var defaultFormData = "";


    </script>
     <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$'; ?>
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">
                        <div class="form-outer form-outer2">
                            <div class="form-hdr">
                                <h3>Account Information</h3>
                            </div>
                            <div class="form-data">
                                <div class="row">


                                    <div class="col-sm-12 col-md-2 grey-plus-photo-upload clear">
                                        <label>Upload Picture</label>
                                        <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                        <div class="upload-logo">
                                            <div class="img-outer tenant_image" id="tenant_image"><img src="<?php echo COMPANY_SITE_URL ?>/images/dummy-img.jpg"></div>
                                            <a class="choose-img" href="javascript:;">Change/Update Photo</a>
                                        </div>
                                        <div class="image-editor">
                                            <input type="file" class="cropit-image-input form-control" name="tenant_image" accept="image/*">
                                            <div class="cropItData" style="display: none;">
                                                        <div class="cropit-preview"></div>
                                                        <div class="cropit-rotate">
                                                        <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                        </a>
                                                        <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                        </a>
                                                        </div>
                                                <div class="tenant_image"></div>
                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                <input type="range"
                                                       class="cropit-image-zoom-input"
                                                       min="0" max="1" step="0.01">
                                                <input type="hidden" name="image-data"
                                                       class="hidden-image-data">
                                                <input type="button" class="export"
                                                       value="Done"
                                                       data-val="tenant_image">
                                            </div>
                                        </div>
                                    </div>




                                    <div class="grey-detail-box">
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Property:</label>
                                            <span class="property_name"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Building & Unit No.:</label>
                                            <span class="building_unit">Bill Farms Bill A-6</span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Rent(<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>):</label>
                                            <span class="rent">USh900.00</span>
                                        </div>
                                    </div>
                                    <div class="grey-detail-box">
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Security Deposite(<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>):</label>
                                            <span class="security_deposite">USh</span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Balance Due(<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>):</label>
                                            <span class="balance_due"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">NFS(<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>):</label>
                                            <span class="nsf"></span>
                                        </div>
                                    </div>
                                    <div class="grey-detail-box">
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Lease Start Date:</label>
                                            <span class="start_date"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Lease End Date:</label>
                                            <span class="end_date"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Move in Date:</label>
                                            <span class="move_in"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Tabs Start -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#tenant-acc-tab1" aria-controls="home" role="tab" data-toggle="tab">Contact Information</a></li>
                                <li role="presentation"><a href="#tenant-acc-tab2" aria-controls="profile" role="tab" data-toggle="tab">Additional Information</a></li>
                                <li role="presentation"><a href="#tenant-acc-tab3" aria-controls="profile" role="tab" data-toggle="tab">Payment Settings</a></li>
                                <li role="presentation"><a href="#tenant-acc-tab4" aria-controls="profile" role="tab" data-toggle="tab">Billing Information</a></li>
                                <li role="presentation"><a href="#tenant-acc-tab5" aria-controls="profile" role="tab" data-toggle="tab">Conversation</a></li>
                                <li role="presentation"><a href="#tenant-acc-tab6" aria-controls="profile" role="tab" data-toggle="tab">Change Password</a></li>
                                <li role="presentation"><a href="#tenant-acc-tab7" aria-controls="profile" role="tab" data-toggle="tab">Give Notice</a></li>
                                <li role="presentation"><a href="#tenant-acc-tab8" aria-controls="profile" role="tab" data-toggle="tab">Occupants</a></li>
                                <li role="presentation"><a href="#tenant-acc-tab9" aria-controls="profile" role="tab" data-toggle="tab">Tenant Leases</a></li>
                                <li role="presentation"><a href="#tenant-acc-tab10" aria-controls="profile" role="tab" data-toggle="tab">Payment</a></li>
                                 <li role="presentation"><a href="javascript:void(0)" id='onlinePayment'>Online Payment</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tenant-acc-tab1">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Contact Information</h3>
                                        </div>
                                        <div class="form-data form-outer2">
                                            <div class="detail-outer">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Full Name :</label>
                                                            <span class="fullname"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Address 1 :</label>
                                                            <span class="address1"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Address 3 :</label>
                                                            <span class="address3"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Zip/Postal Code :</label>
                                                            <span class="zipcode"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">City :</label>
                                                            <span class="city"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Email :</label>
                                                            <span class="email"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Entity/Company :</label>
                                                            <span class="company_name"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Address 2 :</label>
                                                            <span class="address2"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Address4 :</label>
                                                            <span class="address4"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">State Province :</label>
                                                            <span class="state"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Phone :</label>
                                                            <span class="phone_number"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends-->
                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Emergency Contact Details</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="emergencyInfo" style="display:none;"></div>
                                            <div class="viewEmergencyInfo"></div>
                                            <a href="javascript:;" class="emergencySection pull-right"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit</a>

                                        </div>

                                    </div>
                                    <!-- Form Outer Ends-->
                                </div>
                                <!-- First Tab Ends-->
                                <div role="tabpanel" class="tab-pane" id="tenant-acc-tab2">
                                    <div class="form-outer vehicle_main">
                                        <div class="form-hdr">
                                            <h3 class="pull-left">Vehicles</h3>
                                            <input type="button" name="addVehicle" value="Add Vehicle"
                                                   class="addbtn blue-btn pull-right" id="add_vehicle_button" style="display: none">

                                        </div>
                                        <div class="form-data">
                                            <div>
                                                <form id='addEditVehicle' style="display: none;">
                                                    <input type="hidden" name="vehicle_action"
                                                           class="vehicle_action">
                                                    <input type="hidden" name="vehicle_id" class="vehicle_id">
                                                    <div class="row">


                                                        <div class="form-outer">
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Vehicle Type</label>
                                                                    <input class="form-control" type="text"
                                                                           name='vehicle_type'
                                                                           id="vehicle_type" maxlength="50">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Make</label>
                                                                    <input class="form-control" type="text"
                                                                           name='vehicle_make'
                                                                           id="vehicle_make" maxlength="30">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>License Plate Number</label>
                                                                    <input class="form-control" type="text"
                                                                           name='vevicle_license'
                                                                           id="vevicle_license" maxlength="30">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Color</label>
                                                                    <input class="form-control" type="text"
                                                                           name='vehicle_color'
                                                                           id="vehicle_color" maxlength="30">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <label>Year Of Vehicle</label>
                                                                <select class="form-control" id="vevicle_year"
                                                                        name="vevicle_year">
                                                                    <option value="">Select</option>

                                                                    <?php
                                                                    for ($vehiclyear = date("Y"); $vehiclyear >= 1900; $vehiclyear--) {
                                                                        echo '<option value="' . $vehiclyear . '">' . $vehiclyear . '</option>';
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>VIN </label>
                                                                    <input class="form-control" type="text"
                                                                           name='vehicle_vin' id="vehicle_vin" maxlength="30">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Registration</label>
                                                                    <input class="form-control" type="text"
                                                                           name='vehicle_registration'
                                                                           id="vehicle_registration" maxlength="30">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>


                                                            <div class="col-sm-12 col-md-2 grey-plus-photo-upload clear">
                                                                <label>Vehicle Photo/Image</label>
                                                                <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                <div class="upload-logo">
                                                                    <div class="img-outer vehicle_image1"
                                                                         id="vehicle_image1"><img
                                                                                src="<?php echo COMPANY_SITE_URL ?>/images/Vehicledummy.png">
                                                                    </div>
                                                                    <a class="choose-img" href="javascript:;">Choose
                                                                        Image</a>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file"
                                                                           class="cropit-image-input form-control"
                                                                           name="vehicle_image1[]"
                                                                           accept="image/*">
                                                                    <div class="cropItData"
                                                                         style="display: none;">
                                                                        <div class="cropit-preview cropit-image-loading"
                                                                             style="position: relative; width: 250px; height: 250px;">
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image"
                                                                                     alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image"
                                                                                     alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="vehicle_image1"></div>
                                                                        <!--  <div class="image-size-label">Resize image</div> -->
                                                                        <input type="range"
                                                                               class="cropit-image-zoom-input"
                                                                               min="0" max="1" step="0.01">
                                                                        <input type="hidden" name="image-data"
                                                                               class="hidden-image-data">

                                                                        <input type="button" class="export"
                                                                               value="Done"
                                                                               data-val="vehicle_image1">
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                <label>Vehicle Photo/Image</label>
                                                                <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                <div class="upload-logo">
                                                                    <div class="img-outer vehicle_image2"
                                                                         id="vehicle_image2"><img
                                                                                src="<?php echo COMPANY_SITE_URL ?>/images/Vehicledummy.png">
                                                                    </div>
                                                                    <a class="choose-img" href="javascript:;">Choose
                                                                        Image</a>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file"
                                                                           class="cropit-image-input form-control"
                                                                           name="vehicle_image2[]"
                                                                           accept="image/*">
                                                                    <div class="cropItData"
                                                                         style="display: none;">
                                                                        <div class="cropit-preview cropit-image-loading"
                                                                             style="position: relative; width: 250px; height: 250px;">
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image"
                                                                                     alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image"
                                                                                     alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="vehicle_image2"></div>
                                                                        <!--  <div class="image-size-label">Resize image</div> -->
                                                                        <input type="range"
                                                                               class="cropit-image-zoom-input"
                                                                               min="0" max="1" step="0.01">
                                                                        <input type="hidden" name="image-data"
                                                                               class="hidden-image-data">
                                                                        <input type="button" class="export"
                                                                               value="Done"
                                                                               data-val="vehicle_image2">
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                <label>Vehicle Photo/Image</label>
                                                                <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                <div class="upload-logo">
                                                                    <div class="img-outer vehicle_image3"
                                                                         id="vehicle_image3"><img
                                                                                src="<?php echo COMPANY_SITE_URL ?>/images/Vehicledummy.png">
                                                                    </div>
                                                                    <a class="choose-img" href="javascript:;">Choose
                                                                        Image</a>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file"
                                                                           class="cropit-image-input form-control"
                                                                           name="vehicle_image3[]"
                                                                           accept="image/*">
                                                                    <div class="cropItData"
                                                                         style="display: none;">
                                                                        <div class="cropit-preview cropit-image-loading"
                                                                             style="position: relative; width: 250px; height: 250px;">
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image"
                                                                                     alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image"
                                                                                     alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="vehicle_image3"></div>
                                                                        <!--  <div class="image-size-label">Resize image</div> -->
                                                                        <input type="range"
                                                                               class="cropit-image-zoom-input"
                                                                               min="0" max="1" step="0.01">
                                                                        <input type="hidden" name="image-data"
                                                                               class="hidden-image-data">
                                                                        <input type="button" class="export"
                                                                               value="Done"
                                                                               data-val="vehicle_image3">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <span class="ffirst_nameErr error red-star"></span>
                                                            <div class="col-sm-12 text-right">
                                                                <input type="submit" class="blue-btn"
                                                                       name="submit" value="Save">
                                                            <input type="button" class="clear-btn resetVehicle capital"
                                                                         value="Reset">
                                                                <input type="button" name="Cancel"
                                                                       value="Cancel"
                                                                       class="grey-btn cancelbtn">
                                                            </div>
                                                        </div>


                                                    </div>


                                                </form>

                                                <!-- append vehicle listing -->
                                                <div class="apx-table">

                                                    <div class="table-responsive">

                                                        <table id="TenantVehicle-table"
                                                               class="table table-bordered">

                                                        </table>
                                                    </div>
                                                </div>

                                                <div class="edit-foot edit_vehicle_button">
                                                    <a href="javascript:;">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <!-- Form-outer Ends-->
                                    <div class="form-outer  pet_main">
                                        <div class="form-hdr">
                                            <h3 class="pull-left">Pets</h3>
                                            <input type="button" value="Add Pets" id="add_pet_button" style="display: none;"
                                                   class="addbtn blue-btn pull-right">

                                        </div>
                                        <div class="form-data">
                                            <!--                                         <input type="button"   class="addbtn blue-btn" value="Add Pet">-->

                                            <form id='addEditPet' method="post" style="display:none;">
                                                <input type="hidden" class="pet_action blue-btn"
                                                       name="pet_action">
                                                <input type="hidden" class="pet_unique_id blue-btn"
                                                       name="pet_unique_id">

                                                <div class="property_pet">
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class="contactTenant">
                                                                    <label>Pet Name</label>
                                                                    <input class="form-control" type="text"
                                                                           name="pet_name" id="pet_name" maxlength="30">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class="contactTenant">
                                                                    <label>Pet ID</label>
                                                                    <input class="form-control" type="text"
                                                                           name="pet_id" maxlength="30" id="pet_id">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class="contactTenant">
                                                                    <label>Pet Type/Breed</label>
                                                                    <input class="form-control" type="text"
                                                                           name="pet_type" id="pet_type" maxlength="30">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class="contactTenant">
                                                                    <!--calander -->
                                                                    <label>Birth Date</label>
                                                                    <input class="form-control calander pet_birth"
                                                                           type="text" name="pet_birth">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2 ">
                                                                <label>Pet Age</label>
                                                                <select class="form-control" name="pet_age"
                                                                        id="pet_age">
                                                                    <?php
                                                                    for ($petAge = 1; $petAge <= 100; $petAge++) {
                                                                        echo '<option value="' . $petAge . '">' . $petAge . '</option>';
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <label>Gender</label>
                                                                <select class="form-control" id="pet_gender"
                                                                        name="pet_gender" id="pet_gender">
                                                                    <option value="0">Select</option>
                                                                    <option value="1">Male</option>
                                                                    <option value="2">Female</option>
                                                                    <option value="3">Other</option>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class="contactTenant">
                                                                    <label>Pet Weight</label>
                                                                    <input class="form-control" type="text"
                                                                           name="pet_weight" id="pet_weight" maxlength="30">
                                                                    <span class="ffirst_nameErr error red-star" ></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-1 col-md-2">
                                                                <label>Pet Weight Unit</label>
                                                                <select class="form-control"
                                                                        id="pet_weight_unit"
                                                                        name="pet_weight_unit">
                                                                    <option value="0">Select</option>
                                                                    <option value="1">lb</option>
                                                                    <option value="2">lbs</option>
                                                                    <option value="3">kg</option>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class="contactTenant">
                                                                    <label>Note</label>
                                                                    <textarea class="form-control"
                                                                              name="pet_note"
                                                                              id="pet_note"></textarea>
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class="contactTenant">
                                                                    <!--calander -->
                                                                    <label>Pet Color</label>
                                                                    <input class="form-control" type="text"
                                                                           name="pet_color" id="pet_color" maxlength="30">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class="contactTenant">
                                                                    <!--calander -->
                                                                    <label>Chip ID</label>
                                                                    <input class="form-control" type="text"
                                                                           name="pet_chipid" id="pet_chipid" maxlength="30">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <span class="ffirst_nameErr error red-star"></span>
                                                            <!-- <span class="ffirst_nameErr error red-star"></span> -->
                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class="contactTenant">
                                                                    <label>vet\Hosp. Name</label>
                                                                    <input class="form-control" type="text"
                                                                           name="pet_vet" id="pet_vet" maxlength="30">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2 countycodediv">
                                                                <label>Country Code</label>
                                                                <select class="form-control"
                                                                        name="pet_countryCode"
                                                                        id="pet_countryCode" maxlength="30"></select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class="contactTenant">
                                                                    <label>Phone Number</label>
                                                                    <input type="text" class="form-control"
                                                                           name="pet_phoneNumber"
                                                                           id="pet_phoneNumber" maxlength="30">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class="contactTenant">
                                                                    <!--calander -->
                                                                    <label>Last Visit</label>
                                                                    <input class="form-control calander pet_lastVisit"
                                                                           type="text" name="pet_lastVisit" maxlength="30">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class="contactTenant">
                                                                    <!--calander -->
                                                                    <label>Next Visit</label>
                                                                    <input class="form-control calander pet_nextVisit"
                                                                           type="text" name="pet_nextVisit" maxlength="30">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                        </div>


                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Medical Condition</label>
                                                                <div class="check-outer">
                                                                    <input name="pet_medical" type="radio"
                                                                           value="1"
                                                                           class="pet_medical_condition" maxlength="30">
                                                                    <label>Yes</label>
                                                                </div>
                                                                <div class="check-outer">
                                                                    <input name="pet_medical" type="radio"
                                                                           value="0"
                                                                           class="pet_medical_condition"
                                                                           checked="" maxlength="30">
                                                                    <label>No</label>
                                                                </div>
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="pet_medical_html" style="display:none">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-3">
                                                                        <div class="contactTenant">
                                                                            <!--calander -->
                                                                            <label>Please Add Pets Medical
                                                                                Condiction</label>
                                                                            <input class="form-control"
                                                                                   type="text"
                                                                                   name="pet_medical_condition_note"
                                                                                   id="pet_medical_condition_note" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Shots</label>
                                                                <div class="check-outer">
                                                                    <input name="pet_shots" type="radio"
                                                                           value="1" class="pet_name_shot" maxlength="30">
                                                                    <label>Yes</label>
                                                                </div>
                                                                <div class="check-outer">
                                                                    <input name="pet_shots" type="radio"
                                                                           value="0" class="pet_name_shot"
                                                                           checked="" maxlength="30">
                                                                    <label>No</label>
                                                                </div>
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>
                                                            <span class="ffirst_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="pet_shot_date" style="display:none;">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <!--calander -->
                                                                        <label>Name of the shot</label>
                                                                        <input class="form-control" type="text"
                                                                               name="pet_name_shot"
                                                                               id="pet_name_shot" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <!--calander -->
                                                                        <label>Date Given</label>
                                                                        <input class="form-control calander pet_date_given"
                                                                               type="text" name="pet_date_given"
                                                                               id="dp1561612752915">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <!--calander -->
                                                                        <label>Expiration Date</label>
                                                         <input class="form-control calander pet_expiration_date"
                                                                               type="text"
                                                                               name="pet_expiration_date" readonly>
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <!--calander -->
                                                                        <label>Follow Up</label>
                                                                        <input class="form-control calander pet_follow_up"
                                                                               type="text" name="pet_follow_up"
                                                                               id="dp1561612752917">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <!--calander -->
                                                                        <label>Note</label>
                                                                        <input class="form-control" type="text"
                                                                               name="pet_note_shot"
                                                                               id="shot_pet_note">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>

                                                        </div>


                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Rabies</label>
                                                            <div class="check-outer">
                                                                <input name="pet_rabies" type="radio" value="1"
                                                                       class="pet_rabies" maxlength="30">
                                                                <label>Yes</label>
                                                            </div>
                                                            <div class="check-outer">
                                                                <input name="pet_rabies" class="pet_rabies"
                                                                       type="radio" value="0" checked="" >
                                                                <label>No</label>
                                                            </div>
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>

                                                        <div class="clearfix"></div>


                                                        <div class="pet_rabies_html" style="display:none">

                                                            <div class="form-outer">
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <!--calander -->
                                                                        <label>Rabies#</label>
                                                                        <input class="form-control" type="text"
                                                                               name="pet_rabies_name" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <!--calander -->
                                                                        <label>Date Given</label>
                                                                        <input class="form-control calander pet_rabies_given_date"
                                                                               type="text"
                                                                               name="pet_rabies_given_date">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <!--calander -->
                                                                        <label>Expiration Date</label>
                                                                        <input class="form-control calander pet_rabies_expiration_date"
                                                                               type="text"
                                                                               name="pet_rabies_expiration_date"
                                                                               readonly>
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <!--calander -->
                                                                        <label>Note</label>
                                                                        <input class="form-control" type="text"
                                                                               name="pet_rabies_note[]">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>

                                                        </div>

                                                        <div class="form-outer">
                                                            <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                <label>Pet Photo/Image</label>
                                                                <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                <div class="upload-logo">
                                                                    <div class="img-outer pet_image1"
                                                                         id="pet_image1"><img
                                                                                src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">
                                                                    </div>
                                                                    <a class="choose-img" href="javascript:;">Choose
                                                                        Image</a>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file"
                                                                           class="cropit-image-input form-control"
                                                                           name="pet_image1[]" accept="image/*">
                                                                    <div class="cropItData"
                                                                         style="display: none;">
                                                                        <div class="cropit-preview cropit-image-loading"
                                                                             style="position: relative; width: 250px; height: 250px;">
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image"
                                                                                     alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image"
                                                                                     alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="pet_image1"></div>
                                                                        <!--  <div class="image-size-label">Resize image</div> -->
                                                                        <input type="range"
                                                                               class="cropit-image-zoom-input"
                                                                               min="0" max="1" step="0.01">
                                                                        <input type="hidden" name="image-data"
                                                                               class="hidden-image-data">
                                                                        <input type="button" class="export"
                                                                               value="Done"
                                                                               data-val="pet_image1">
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                <label>Pet Photo/Image</label>
                                                                <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                <div class="upload-logo">
                                                                    <div class="img-outer pet_image2"
                                                                         id="pet_image2"><img
                                                                                src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">
                                                                    </div>
                                                                    <a class="choose-img" href="javascript:;">Choose
                                                                        Image</a>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file"
                                                                           class="cropit-image-input form-control"
                                                                           name="pet_image2[]" accept="image/*">
                                                                    <div class="cropItData"
                                                                         style="display: none;">
                                                                        <div class="cropit-preview cropit-image-loading"
                                                                             style="position: relative; width: 250px; height: 250px;">
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image"
                                                                                     alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image"
                                                                                     alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="pet_image2"></div>
                                                                        <!--  <div class="image-size-label">Resize image</div> -->
                                                                        <input type="range"
                                                                               class="cropit-image-zoom-input"
                                                                               min="0" max="1" step="0.01">
                                                                        <input type="hidden" name="image-data"
                                                                               class="hidden-image-data">
                                                                        <input type="button" class="export"
                                                                               value="Done"
                                                                               data-val="pet_image2">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                <label>Pet Photo/Image</label>
                                                                <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                <div class="upload-logo">
                                                                    <div class="img-outer pet_image3"
                                                                         id="pet_image3"><img
                                                                                src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">
                                                                    </div>
                                                                    <a class="choose-img" href="javascript:;">Choose
                                                                        Image</a>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file"
                                                                           class="cropit-image-input form-control"
                                                                           name="pet_image3[]" accept="image/*">
                                                                    <div class="cropItData"
                                                                         style="display: none;">
                                                                        <div class="cropit-preview cropit-image-loading"
                                                                             style="position: relative; width: 250px; height: 250px;">
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image"
                                                                                     alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image"
                                                                                     alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="pet_image3"></div>
                                                                        <!--  <div class="image-size-label">Resize image</div> -->
                                                                        <input type="range"
                                                                               class="cropit-image-zoom-input"
                                                                               min="0" max="1" step="0.01">
                                                                        <input type="hidden" name="image-data"
                                                                               class="hidden-image-data">
                                                                        <input type="button" class="export"
                                                                               value="Done"
                                                                               data-val="pet_image3">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 text-right">
                                                                <input type="submit" name="submit"
                                                                       class="blue-btn" value="Save">
                                                                <input type="button" class="clear-btn resetPet capital" value="Reset">       
                                                                <input type="button" class="cancelbtn grey-btn"
                                                                       value="Cancel">
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </form>
                                            <!-- append pet listing -->


                                            <div class="apx-table">
                                                <div class="table-responsive">
                                                    <table id="TenantPet-table" class="table table-bordered">
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="edit-foot edit_pet_button">
                                                <a href="javascript:;">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form-outer Ends-->
                                    <div class="form-outer  service_main">
                                        <div class="form-hdr">
                                            <h3 class="pull-left">Service/Companion Animals</h3>
                                            <input type="button" value="Add Animal"
                                                   class="addbtn blue-btn  pull-right" id="add_animal_animal" style="display: none">


                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <form id="addEditService" method="post"
                                                          style="display:none;">
                                                        <input type="hidden" class="service_action"
                                                               name="service_action">
                                                        <input type="hidden" class="service_unique_id"
                                                               name="service_unique_id">

                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <label>Name</label>
                                                                        <input class="form-control" type="text"
                                                                               name='service_name'
                                                                               id="service_name" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <label>ID</label>
                                                                        <input class="form-control" type="text"
                                                                               name='service_id'
                                                                               id="service_id" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <label>Type/Breed</label>
                                                                        <input class="form-control" type="text"
                                                                               name='service_type'
                                                                               id="service_type" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Birth Date</label>
                                                                        <input class="form-control calander"
                                                                               type="text"
                                                                               name='service_birth'
                                                                               id="service_birth">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <label>Age</label>
                                                                    <select class="form-control"
                                                                            id="service_year"
                                                                            name="service_year">
                                                                        <option value="0">Select</option>

                                                                        <?php
                                                                        for ($serviceAge = 1; $serviceAge <= 100; $serviceAge++) {
                                                                            echo '<option value="' . $serviceAge . '">' . $serviceAge . '</option>';
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2 ">
                                                                    <label>Gender</label>
                                                                    <select class="form-control"
                                                                            name="service_gender"
                                                                            id="service_gender">
                                                                        <option value="0">Select</option>
                                                                        <option value="1">Male</option>
                                                                        <option value="2">Female</option>
                                                                        <option value="3">Other</option>
                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>

                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <label>Animal Weight</label>
                                                                        <input class="form-control" type="text"
                                                                               name='service_weight'
                                                                               id="service_weight">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2 ">
                                                                    <label>Weight Unit</label>
                                                                    <select class="form-control"
                                                                            name="service_weight_unit"
                                                                            id="service_weight_unit">
                                                                        <option value="">Select</option>
                                                                        <option value="1">lb</option>
                                                                        <option value="2">lbs</option>
                                                                        <option value="3">kg</option>
                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <label>Note</label>
                                                                        <textarea class="form-control"
                                                                                  name='service_note'
                                                                                  id="service_note"></textarea>
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Color</label>
                                                                        <input class="form-control" type="text"
                                                                               name='service_color'
                                                                               id="service_color" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Chip ID</label>
                                                                        <input class="form-control" type="text"
                                                                               name='service_chipid'
                                                                               id="service_chipid" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <label>vet\Hosp. Name</label>
                                                                        <input class="form-control" type="text"
                                                                               name='service_vet'
                                                                               id="service_vet" maxlength="30">
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2 countycodediv">
                                                                    <label>Country Code</label>
                                                                    <select class="form-control"
                                                                            name="service_countryCode"
                                                                            id="service_countryCode">
                                                                        <option value="1">Select</option>
                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <label>Phone Number</label>
                                                                        <input type='text' class="form-control"
                                                                               name='service_phoneNumber'
                                                                               id="service_phoneNumber" >
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Last Visit</label>
                                                                        <input class="form-control calander service_lastVisit"
                                                                               type="text"
                                                                               name='service_lastVisit'>
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Next Visit</label>
                                                                        <input class="form-control calander service_nextVisit"
                                                                               type="text"
                                                                               name='service_nextVisit'>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Medical Condition</label>
                                                                    <div class="check-outer">
                                                                        <input name='service_medical'
                                                                               type="radio"
                                                                               value='1'>
                                                                        <strong class='font-weight-bold'>Yes</strong>
                                                                    </div>
                                                                    <div class="check-outer">
                                                                        <input name='service_medical'
                                                                               type="radio"
                                                                               value='0' checked>
                                                                        <strong class='font-weight-bold'>No</strong>
                                                                    </div>
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--<div class="row spaceNumber">
                                                            <div class="form-outer">
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Parking Space Number<em class="red-star">*</em></label>
                                                                    <input class="form-control capsOn" type="text" id="parking_space" name="parking_space[]">
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>
                                                            </div>
                                                        </div>-->
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Shots</label>
                                                                    <input name='service_shots' type="radio"
                                                                           value='1'
                                                                           class="service_shots"><strong
                                                                            class='font-weight-bold'>Yes</strong>
                                                                    <input name='service_shots' type="radio"
                                                                           value='0'
                                                                           class="service_shots" checked><strong
                                                                            class='font-weight-bold'>No</strong>
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="animal_shot_date" style="display:none;">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-12">
                                                                        <div class="col-sm-2 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <!--calander -->
                                                                                <label>Name of the shot</label>
                                                                                <input class="form-control"
                                                                                       type="text"
                                                                                       name='service_name_shot'
                                                                                       id="service_name_shot" maxlength="30">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-sm-2 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <!--calander -->
                                                                                <label>Date Given</label>
                                                                                <input class="form-control calander service_date_given"
                                                                                       type="text"
                                                                                       name='service_date_given'
                                                                                  >
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-sm-2 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <!--calander -->
                                                                                <label>Expiration Date</label>
                                                                                <input class="form-control calander service_expiration_date"
                                                                                       type="text"
                                                                                       name='service_expiration_date'
                                                                                       >
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-sm-2 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <!--calander -->
                                                                                <label>Follow Up</label>
                                                                                <input class="form-control calander service_follow_up"
                                                                                       type="text"
                                                                                       name='service_follow_up'>
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <!--calander -->
                                                                                <label>Note</label>
                                                                                <input class="form-control service_shot_note"
                                                                                       type="text"
                                                                                       name='service_shot_note'>
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Rabies</label>
                                                                    <input name='service_rabies' type="radio"
                                                                           value='1'
                                                                           class="animal_rabies"><strong
                                                                            class='font-weight-bold'>Yes</strong>
                                                                    <input name='service_rabies' type="radio"
                                                                           value='0'
                                                                           class="animal_rabies" checked><strong
                                                                            class='font-weight-bold'>No</strong>
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row animal_rabies_html"
                                                             style="display:none">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12 col-md-12">
                                                                    <div class="col-sm-2 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>Rabies#</label>
                                                                            <input class="form-control"
                                                                                   type="text"
                                                                                   name='animal_rabies'
                                                                                   id="animal_rabies">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-2 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <!--calander -->
                                                                            <label>Date Given</label>
                                                                            <input class="form-control calander animal_date_given"
                                                                                   type="text"
                                                                                   name='animal_date_given'
                                                                                   >
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-2 col-md-2">
                                                                        <div class='contactTenant'>

                                                                            <label>Expiration Date</label>
                                                                            <input class="form-control calander animal_expiration_date"
                                                                                   type="text"
                                                                                   name='animal_expiration_date'
                                                                                   >
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-2 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <!--calander -->
                                                                            <label>Note</label>
                                                                            <input class="form-control"
                                                                                   type="text"
                                                                                   name='animal__note'
                                                                                   id="animal__note">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                    <label>Animal Photo/Image</label>
                                                                    <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                    <div class="upload-logo">
                                                                        <div class="img-outer service_image1"
                                                                             id="service_image1"><img
                                                                                    src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">
                                                                        </div>
                                                                        <a class="choose-img"
                                                                           href="javascript:;">Choose
                                                                            Image</a>
                                                                    </div>
                                                                    <div class="image-editor">
                                                                        <input type="file"
                                                                               class="cropit-image-input form-control"
                                                                               name="service_image1"
                                                                               accept="image/*">
                                                                        <div class="cropItData"
                                                                             style="display: none;">
                                                                            <div class="cropit-preview cropit-image-loading"
                                                                                 style="position: relative; width: 250px; height: 250px;">
                                                                                <div class="cropit-preview-image-container"
                                                                                     style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                    <img class="cropit-preview-image"
                                                                                         alt=""
                                                                                         style="transform-origin: left top; will-change: transform;">
                                                                                </div>
                                                                            </div>
                                                                            <div class="service_image1"></div>
                                                                            <!--  <div class="image-size-label">Resize image</div> -->
                                                                            <input type="range"
                                                                                   class="cropit-image-zoom-input"
                                                                                   min="0"
                                                                                   max="1" step="0.01">
                                                                            <input type="hidden"
                                                                                   name="image-data"
                                                                                   class="hidden-image-data">
                                                                            <input type="button" class="export"
                                                                                   value="Done"
                                                                                   data-val="service_image1">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                    <label>Animal Photo/Image</label>
                                                                    <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                    <div class="upload-logo">
                                                                        <div class="img-outer service_image2"
                                                                             id="service_image2"><img
                                                                                    src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">
                                                                        </div>
                                                                        <a class="choose-img"
                                                                           href="javascript:;">Choose
                                                                            Image</a>
                                                                    </div>
                                                                    <div class="image-editor">
                                                                        <input type="file"
                                                                               class="cropit-image-input form-control"
                                                                               name="service_image2"
                                                                               accept="image/*">
                                                                        <div class="cropItData"
                                                                             style="display: none;">
                                                                            <div class="cropit-preview cropit-image-loading"
                                                                                 style="position: relative; width: 250px; height: 250px;">
                                                                                <div class="cropit-preview-image-container"
                                                                                     style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                    <img class="cropit-preview-image"
                                                                                         alt=""
                                                                                         style="transform-origin: left top; will-change: transform;">
                                                                                </div>
                                                                            </div>
                                                                            <div class="service_image2"></div>
                                                                            <!--  <div class="image-size-label">Resize image</div> -->
                                                                            <input type="range"
                                                                                   class="cropit-image-zoom-input"
                                                                                   min="0"
                                                                                   max="1" step="0.01">
                                                                            <input type="hidden"
                                                                                   name="image-data"
                                                                                   class="hidden-image-data">
                                                                            <input type="button" class="export"
                                                                                   value="Done"
                                                                                   data-val="service_image2">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                    <label>Animal Photo/Image</label>
                                                                    <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                    <div class="upload-logo">
                                                                        <div class="img-outer service_image3"
                                                                             id="service_image3"><img
                                                                                    src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">
                                                                        </div>
                                                                        <a class="choose-img"
                                                                           href="javascript:;">Choose
                                                                            Image</a>
                                                                    </div>
                                                                    <div class="image-editor">
                                                                        <input type="file"
                                                                               class="cropit-image-input form-control"
                                                                               name="service_image3"
                                                                               accept="image/*">
                                                                        <div class="cropItData"
                                                                             style="display: none;">
                                                                            <div class="cropit-preview cropit-image-loading"
                                                                                 style="position: relative; width: 250px; height: 250px;">
                                                                                <div class="cropit-preview-image-container"
                                                                                     style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                    <img class="cropit-preview-image"
                                                                                         alt=""
                                                                                         style="transform-origin: left top; will-change: transform;">
                                                                                </div>
                                                                            </div>
                                                                            <div class="service_image2"></div>
                                                                            <!--  <div class="image-size-label">Resize image</div> -->
                                                                            <input type="range"
                                                                                   class="cropit-image-zoom-input"
                                                                                   min="0"
                                                                                   max="1" step="0.01">
                                                                            <input type="hidden"
                                                                                   name="image-data"
                                                                                   class="hidden-image-data">
                                                                            <input type="button" class="export"
                                                                                   value="Done"
                                                                                   data-val="service_image3">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <input type="submit" name="submit"
                                                                           class="blue-btn" value="Save">
                                                                             <input type="button" class="clear-btn resetService capital" value="Reset">       
                                                                    <input type="button" name="Cancel"
                                                                           value="Cancel"
                                                                           class="grey-btn cancelbtn">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--  <a class="pop-add-icon copyServiceCompanion" href="javascript:;"><i
                                                             class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                 <a class="pop-add-icon removeServiceCompanion" href="javascript:;"
                                                    style="display:none"><i class="fa fa-plus-circle"
                                                                            aria-hidden="true"></i></a> -->


                                                    </form>
                                                    <!-- append service listing -->


                                                    <div class="apx-table">
                                                        <div class="table-responsive">
                                                            <table id="TenantService-table"
                                                                   class="table table-bordered">
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="edit-foot" id="edit_animal_button">
                                                        <a href="javascript:;">
                                                            <i class="fa fa-pencil-square-o"
                                                               aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form-outer Ends-->
                                </div>

                                <div role="tabpanel" class="tab-pane" id="tenant-acc-tab3">
                            <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>
                                                <strong class="left">Payment Settings</strong>
                                            </h3>
                                        </div>
                                        <input type='hidden' id="company_user_id">
                                        <div class="form-data">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Payment Method <em class="red-star">*</em></label>
                                                    <select class="form-control setType" id="payment_method">
                                                      <option value='1'>Credit Card/Debit Card</option>
                                                      <option value='2'>ACH</option>
                                                    </select>
                                                </div>
                                                 <div class="col-sm-3">
                                                    <label>&nbsp;</label>

                                                </div>
                                            </div>

                                            <div class="row cards">
                                                <div class="cardDetails col-sm-12"></div>
                                                <form id="addCards">
                                                    <div class="detail-outer ">
                                                        <div class="col-sm-5">
                                                            <label class="black-label">Card Number <em class="red-star">*</em></label>
                                                            <span>
                                                                <input class="form-control hide_copy" name="card_number"  type="number"/>
                                                                <em class="nicknameErr error red-star"></em>
                                                            </span>
                                                        </div> 
                                                 

                                                    <div class="col-sm-5 clear">
                                                        <label class="black-label">Expiration Date <em class="red-star">*</em></label>
                                                        <span>
                                                            <input class="form-control exp_date" name="exp_date"  type="text" readonly/>
                                                            <em class="nicknameErr error red-star"></em>
                                                        </span>
                                                    </div>

                                                    <div class="col-sm-5 clear">
                                                        <label class="black-label">CVC <em class="red-star">*</em></label>
                                                        <span>
                                                        <input class="form-control" name="cvc"  type="text"/>
                                                        <em class="nicknameErr error red-star"></em>
                                                        </span>
                                                    </div>

                                                    <div class="col-sm-5 clear">
                                                        <label class="black-label">Cardholder Name <em class="red-star">*</em></label>
                                                        <span>
                                                            <input class="form-control" name="holder_name" type="text"/>
                                                            <em class="nicknameErr error red-star"></em>
                                                        </span>
                                                    </div>

                                                    <div class="col-sm-5 clear">
<!--                                                        <label class="black-label">Autopay </label>-->
<!--                                                        <span>-->
<!--                                                            <input class="auto_pay" name="auto_pay" type="checkbox" value="ON"/>-->
<!---->
<!--                                                        </span>-->
                                                    </div>
                                                       

                                                     
                                                        </div> 
                                                        <div class="col-sm-12">
                                                        <input type="submit" class="blue-btn" value="Confirm">
                                                    </div>
                                                    </form>
                                             
                                             </div>
                                <div class="row accounts" style="display:none;">
                                <div class="accountDetails"></div>


                                </div>   
                                        </div>
                                        </div>
               



                              </div>
                                <!-- Second Tab Ends-->
                                <div class="tab-pane" id="tenant-acc-tab4">
                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Billing Information</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="detail-outer">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Billing Address 1 :</label>
                                                            <span>56 High Street</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Billing Address 3 :</label>
                                                            <span>Lower Level</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Zip/Postal Code :</label>
                                                            <span>12345</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">City :</label>
                                                            <span>Los Angeles</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Billing Address 2 :</label>
                                                            <span>#23</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Billing Address 4 :</label>
                                                            <span></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">State Province :</label>
                                                            <span>CA</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Billing Email :</label>
                                                            <span>francis5632@gmail.com</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Fourth Tab Ends-->
                                <div class="tab-pane" id="tenant-acc-tab5">
                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Conversation</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="owner_conversation_div">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="conversation-outer">
                                                            <ul class="conversation_list">
                                                                <li>
                                                                    <div class="row">
                                                                        <div class="col-sm-1 conversation-lt">
                                                                            <div class="img-outer">
                                                                                <img src="<?php echo COMPANY_SITE_URL ?>/images/profile-img.png">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-10 pad-none conversation-rt">
                                                                            <span class="dark_name">Kambo K(Admin) :</span>
                                                                            <span class="light_name">Sharma Aryan2 (Tenant)</span>
                                                                            <span class="dark_name">Personal Issues</span>
                                                                            <span class="black_text" >Fsdfsdsdsdsds</span>
                                                                            <div class="conversation-time clear">
                                                                                <span class="black_text" >Sep 25, 2019 (Wed.)-</span>
                                                                                <a class="light_name" >Comment</a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-1 pull-right">
                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="btn-outer text-right">
                                                <button class="blue-btn" id="add_owner_conversation_btn">New Conversation</button>
                                                <hr/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="add_owner_conversation_div" style="display: none;">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3> New Conversation</h3>
                                            </div>
                                            <div class="form-data">
                                                <form id="add_owner_conversation_form_id">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Property Manager  <em class="red-star">*</em></label>
                                                            <select class="form-control select_mangers_option" name="selected_user_id"><option>Select</option></select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Problem Category <em class="red-star">*</em>
                                                                <a class="pop-add-icon selectProblemCategory" href="javascript:;">
                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                </a>
                                                            </label>
                                                            <select class="form-control problem_category" name="problem_category"></select>
                                                            <div class="add-popup" id="selectProblemCategoryPopup">
                                                                <h4>Add Problem Category</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>Add New Problem Category <em class="red-star">*</em></label>
                                                                            <input class="form-control problem_category_src customValidateGroup capital" maxlength="50" type="text" placeholder="Add New Problem Category" data_required="true" >
                                                                            <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                                        </div>
                                                                        <div class="btn-outer">
                                                                            <button type="button" class="blue-btn add_single1"  data-table="conversation_problem_category" data-cell="problem_category_name" data-class="problem_category_src" data-name="problem_category">Save</button>
                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-6 clear">
                                                            <label>Description <em class="red-star">*</em></label>
                                                            <textarea rows="4" class="form-control capital" name="description"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="btn-outer top-marginCls text-right">
                                                        <button class="blue-btn add_form_submit_btn">Send</button>
                                                        <button type="button" class="clear-btn email_clear">Clear</button>
                                                        <button id="cancel_add_owner_conversation"  type="button" class="grey-btn">Cancel</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Fifth Tab Ends-->
                                <div class="tab-pane" id="tenant-acc-tab6">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Change Password</h3>
                                        </div>
                                        <form id="changePassword">
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-2">
                                                        <label>Current Password <em class="red-star">*</em></label>
                                                        <input class="form-control" type="password" name="current_password" id="current_password">

                                                    </div>
                                                    <div class="col-xs-12 col-sm-2">
                                                        <label>New Password <em class="red-star">*</em></label>
                                                        <input class="form-control" type="password" name="new_password" id="new_password">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2">
                                                        <label>Confirm New Password <em class="red-star">*</em></label>
                                                        <input class="form-control" type="password" name="confirm_password">
                                                    </div>
                                                </div>
                                                <div class="btn-outer text-right">
                                                    <button class="blue-btn">Save</button>
                                                    <button type="button" class="clear-btn email_clearpass">Clear</button>
                                                    <input type="button" class="grey-btn cancelPassword" value="Cancel">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- Sixth Tab Ends-->
                                <div class="tab-pane" id="tenant-acc-tab7">
                                    <div class="form-outer ">
                                        <div class="form-hdr">
                                            <h3>Give Notice</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="detail-outer">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Notice Given Date :</label>
                                                            <span id="notices_given_dates"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Reason for Leaving :</label>
                                                            <span id="notices_reason_for_leaving">Lower Level</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Schedule Moveout Date :</label>
                                                            <span id="moves_out_notice_date"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Notice Name :</label>
                                                            <span id="notices_name"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Address :</label>
                                                            <span id="notices_tenant_address"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">City :</label>
                                                            <span id="notices_city"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Postal Code :</label>
                                                            <span id="notices_zip_codes"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">State :</label>
                                                            <span id="notices_state"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <!-- Hide Show Notice Starts -->
                                        <div class="form-outer noticesss">
                                       
                                        <div class="form-data">
                                            <div class="groupInfo" style="display:none;">
<!-- Give Notice form Starts -->
                                              <form id="groupNoticess">
                                                <!-- Reason Of Leaving -->


                                                <!-- Reason Of Leaving -->
                                                            <div class="form-hdr">
                                                                <h3>Give Notice</h3>
                                                            </div>
                                                            <div class="form-data form-hdr">

                                                                <div class="row">
                                                                       <div class="col-xs-12">
                                                                    <h3 class="margin-bottom-tenant">Move Out Information</h3>
                                                                </div>
                                                                    <!-- Starts -->
                                                                    <div class="col-xs-12">
                                                                    <div class="row">

                   <input type="hidden" value="<?php echo $_GET['tenant_id']; ?>" class="tenant_id" name="tenant_id">
                <div class="col-xs-12 col-sm-4 col-md-2">
                    <input type="hidden" class="notice-date" id="notice-date">
                    <label>Notice Date <em class="red-star">*</em></label>
                    <input class="form-control calander" type="text" id="noticeDateMoveOut" class="noticeDateMoveOut" name="notice_given_date" readonly>
                </div>

                 <div class="col-xs-12 col-sm-4 col-md-2 apx-inline-popup">
                    <label>Reason for Leaving <em class="red-star">*</em>
              
                    </label>
                    <select class="form-control notice_reason_for_leaving" name="reasonForLeaving" id="notice_reason_for_leaving">
                        <option value="">Select</option>
                       
                    </select>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-2">
                    <label>Scheduled Move Out Date <em class="red-star">*</em></label>
                    <input class="form-control" type="text" id="scheduledMoveOutDate" name="scheduled_move_out" readonly>
                </div>
               
            </div>
        </div>
      
                                                                    <!-- Ends -->
                                                                  
                                                                       <div class="col-xs-12">
                                                                      <h3 class="margin-bottom-tenant">Forwarding Address
</h3>
</div>
                                                                    <div class="col-lg-3">
                                                                        <label>First Name <em class="red-star">*</em></label>
                                                                        <input name="first_name" class="form-control capital" type="text" id="notice_tenant_fname"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Last Name <em class="red-star">*</em></label>
                                                                        <input name="last_name" class="form-control capital notice_tenant_lname" type="text" id="notice_tenant_lname"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Address </label>
                                                                        <input name="address" class="form-control" type="text" id="notice_tenant_address"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Zip / Postal Code</label>
                                                                        <input name="zip_code" class="form-control" type="text" id="notice_zip_code"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Country</label>
                                                                        <input name="country" class="form-control" type="text" id="notice_country"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>State / Province</label>
                                                                        <input name="state" class="form-control" type="text" id="notice_state"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>City</label>
                                                                        <input name="city" class="form-control" type="text" id="notice_city"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Phone</label>
                                                                        <input name="phone" class="form-control add-input phone_format valid" type="text" id="notice_phone" aria-required="true" maxlength="12"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Email</label>
                                                                        <input name="email" class="form-control error" type="text" id="notice_email" aria-required="true" aria-invalid="true"/>
                                                                    </div>
                                                                </div>
                                                                <div class="btn-outer ">
                                                                    <!--<button class="blue-btn" type="submit">Save</button>-->
                                                                    <!--<button class="grey-btn" type="submit">Cancel</button>-->
                                                                    <a href="javascript:;" ><input type="submit" class="blue-btn padding-tenant-btn" value="Update"></a>
                                                                    <!-- <input type="button" name="button" class="clear-btn ResetForm" value="Reset"> -->
                                                                    <a href="javascript:;"><input type="button" class="grey-btn cancelNotice" value="Cancel"></a>
                                                                </div>
                                                            </div>
                                                        </form>
                                            <!--Give Notice Form Ends -->



                                            </div>
                                          <!--   <div class="viewgroupInfo"></div> -->
                                            <a href="javascript:;" class="groupSection pull-right" id="iddd"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit</a>

                                        </div>

                                    </div>
                                        <!-- Hide Show Notice Ends -->
                                    </div>
                                </div>
                                <!-- Seventh Tab Ends-->
                                <div class="tab-pane" id="tenant-acc-tab8">
                                    <div class="apx-table">
                                        <div class="table-responsive">
                                            <table class="occcupants_table" class="table table-bordered">
                                            </table>
                                        </div>
                                    </div>

                                </div>











                                <!-- Eight Tab Ends-->
                                <div class="tab-pane" id="tenant-acc-tab9">
                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Tenant Leases</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="grid-outer">
                                                <div class="apx-table">
                                                    <div class="table-responsive">
                                                        <table id="TenantLease-table" class="table table-bordered"></table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <!-- Ninth Tab Ends-->
                                <div class="tab-pane" id="tenant-acc-tab10">
                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Tenant Leases</h3>
                                        </div>
                                        <input type="hidden" value="<?php echo $_GET['tenant_id']; ?>"
                                               name="tenantapplytaxid" class="tenantapplytaxid">
                                        <div class="form-data">
                                            <div class="property-status">
                                                <button type="button" class="blue-btn pull-right">Make Payment</button>
                                            </div>
                                            <div class="grid-outer">
                                                <div class="userFullAccount"></div>
                                                <form id='updateAllCharges' class='updateAllCharges'>
                                                    <div class="allCharges">

                                                    </div>
                                                </form>
<!--                                                <div class="table-responsive">-->
<!--                                                    <table class="table table-hover table-dark">-->
<!--                                                        <thead>-->
<!--                                                        <tr>-->
<!--                                                            <th scope="col">Date</th>-->
<!--                                                            <th scope="col">Description</th>-->
<!--                                                            <th scope="col">Charge Code</th>-->
<!--                                                            <th scope="col">Original Amount(--><?php //echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?><!--)</th>-->
<!--                                                            <th scope="col">Amount Paid(--><?php //echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?><!--)</th>-->
<!--                                                            <th scope="col">Amount Due(--><?php //echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?><!--)</th>-->
<!--                                                            <th scope="col">Amount Waived Off(--><?php //echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?><!--)</th>-->
<!--                                                            <th scope="col">Waive Off Comment(--><?php //echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?><!--)</th>-->
<!--                                                            <th scope="col">Current Payment(--><?php //echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?><!--) </th>-->
<!--                                                            <th scope="col">Priority</th>-->
<!--                                                            <th scope="col">Pay</th>-->
<!--                                                        </tr>-->
<!--                                                        </thead>-->
<!--                                                        <tbody>-->
<!--                                                        <tr>-->
<!--                                                        <tbody>-->
<!--                                                        <tr>-->
<!--                                                            <td>November 22, 2018 (Thu.)</td>-->
<!--                                                            <td>Lease Breakage Fee</td>-->
<!--                                                            <td>LBRKFee</td>-->
<!--                                                            <td>564.00</td>-->
<!--                                                            <td>0.00</td>-->
<!--                                                            <td>478.00</td>-->
<!--                                                            <td>0.00</td>-->
<!--                                                            <td><textarea class="form-control"></textarea></td>-->
<!--                                                            <td><input class="form-control" type="text"/></td>-->
<!--                                                            <td><input class="form-control" type="text"/></td>-->
<!--                                                            <td>4</td>-->
<!--                                                            <td><input type="checkbox"/></td>-->
<!--                                                        </tr>-->
<!--                                                        </tbody>-->
<!--                                                        </tr>-->
<!--                                                        </tbody>-->
<!--                                                    </table>-->
<!--                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Tenth Tab Ends-->
                            </div>
                        </div>
                    </div>
                    <!--Tabs End -->
                </div>
            </div>
        </div>
    </section>

    <div id="imageModel" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-4" id="getImage1"></div>
                    <div class="col-sm-4" id="getImage2"></div>
                    <div class="col-sm-4" id="getImage3"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="tenant-payment" role="dialog">
    <input type="hidden" class="stripe_amount">
    <div class="modal-dialog modal-dialog-type modal-md" style="width: 60%;">
        <div class="modal-content payment-modal-new pull-left" style="width: 100%;">
            <div class="modal-header">
                <h3 class="payment-modal-head">Tenant Payment<span class='userName'></span></h3>
                <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>
                <!-- <h4 class="modal-title">Online Payment Details</h4> -->
            </div>
            <div class="modal-body">
                <div class="apx-adformbox-content">
                    <div class="apx-adformbox-content-top">
                        <div class="col-sm-8 wd-auto">
                            <h3>Account Balance Due <em class="red-star"><?php echo $default_symbol; ?><span class="paidAmount currecy_padding"></span></em></h3>
                            <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">Sum of Rent,Charges and miscellaneous</span></div>
                            <input type="hidden" name="company_user_id" id="company_user_id">
                        </div>
                        <!--<div class="col-sm-4 text-right full-amount-btn pull-right">
                            <button type="button" class="blue-btn anotherAmountbutton" id="anotherAmountbutton"   >Pay Another Amount</button>
                            <button type="button" class="blue-btn fullAmountButton" id="fullAmountButton"  style="display:none">Full Amount</button>
                        </div>
                        <div class="form-group col-sm-12 " >
                            <input type="text" class="fm-txt form-control amount_num number_only"  id="another_amoumt" style="display: none;" >
                        </div>-->
                    </div>

                    <div class="col-sm-12 payment-method-link">
                        <div class=" stripeMsg">
                        </div>
                        <div class="payment-interval-btn">
                            <a href="#" class="setPaymentInterval">Set Payment Interval</a>
                        </div>
                    </div>
                    <form id="tenant_payment_interval_form" name="tenant_payment_interval_form">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Payment Method</label>
                                        <select class="fm-txt form-control"  id="paymentMethod" name="payment_method">
                                            <option value="one-time-payment">One-Time Payment</option>
                                            <option value="reoccurring">Reoccurring</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select class="mdb-select form-control" multiple  id="payment_category" name="payment_category[]">
                                            <option value="0" class="allselect">Select All</option>
                                            <option value="1">Rent</option>
                                            <option value="2">Utilities</option>
                                            <option value="3">Miscellaneous</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label>Comment?</label>
                                    <div class="notes_date_right_div">
                                        <textarea class="payment_comment notes_date_right" id="payment_comment" name="payment_comment"> </textarea>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 payment-custom  full-amount-btn ">
                                    <button type="button" class="blue-btn anotherAmountbutton" id="anotherAmountbutton">Pay Another Amount</button>
                                    <button type="button" class="blue-btn fullAmountButton" id="fullAmountButton"  style="display:none">Full Amount</button>
                                    <input type="text" class="fm-txt form-control amount_num number_only"  id="another_amoumt" style="display: none;" >
                                    </div>
                               
                             <div class="col-md-6">
                                <div class="form-group amountField" style="display: none;">
                                    <label>Amount</label>
                                    <input type='text' class="amount form-control" id="amount">
                                </div>
                             </div>
                            <div class="col-md-6">
                                <div class="form-group countField" style="display: none;">
                                    <label>Count</label>
                                    <input type='text' class="count form-control" id="count">
                                </div>
                             </div>
                                <div class="col-md-6">
                                    <div class="form-group paymentIntervel" style="display: none;">
                                        <label>Payment Interval</label>
                                        <select class="fm-txt form-control mb-15"  id="paymentIntervel" name="payment_interval">
                                                <option value="weekly">Weekly</option>
                                            <option value="bi-weekly">Bi-weekly</option>
                                            <option value="monthly">Monthly</option>
                                            <option value="annual">Annual</option>
                                        </select>

                                    </div>
                                    <div class="form-group paymentIntervelcalander" style="display: none;">
                                        <input type="text" class="fm-txt form-control jqGridStatusClass"  id="paymentIntervelcalander" name="payment_calender" readonly>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="agree-payment-checkbox"><input type='checkbox' id="payment_terms" name='payment_terms' class="payment_terms">I agree to the following <a href="#">payment terms</a> </label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-sm-12 online-payment-button">

                                    </div>
                                </div>
                            </div>

                            <div class="payment-button">
                                <input type="button"  id="paymentDeduction" class="blue-btn" value="Submit Payment">
                                <button type="submit"  id="paymentCancel" class="grey-btn">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="stripe-checkout" role="dialog">
    <div class="modal-dialog modal-dialog-type modal-md" style="width: 60%;">
        <div class="modal-content payment-modal-new" style="width: 100%;">
            <div class="modal-header">
                <h3 class="payment-modal-head">Account Balance Due <em class="red-star"><?php echo $default_symbol; ?><span class="paidAmount currecy_padding"></span></em></h3>
                <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>

                <!-- <h4 class="modal-title">Online Payment Details</h4> -->
            </div>
            <div class="modal-body">
                <form action="/charge" method="post" id="payment-form1">
                    <input type="hidden" name="tenant_form_serialize_date" id="tenant_form_serialize_date">
                    <div class="apx-adformbox-content">

                        <div class="form-row">
                            <label for="card-element1" class="card-payment-text">
                                Credit / Debit card
                            </label>
                            <div id="card-element1">
                                <!-- A Stripe Element will be inserted here. -->
                            </div>

                            <!-- Used to display form errors. -->
                            <div id="card-errors" role="alert"></div>
                        </div>




                        <div class="payment-button">
                            <button type="submit"  id="submit" class="blue-btn">Pay <?php echo $default_symbol; ?><span class="paidAmount currecy_padding"></span></button>

                        </div>

                    </div>
                </form>


            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="set-method" role="dialog">
    <div class="modal-dialog modal-md" style="width: 60%;">
        <div class="modal-content online-payment-new" style="width: 100%;">
            <div class="modal-header">

                <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>

                <h4 class="modal-title">Online Payment Setup<span class='userName'></span></h4>
            </div>
            <div class="modal-body">

                <div class="apx-adformbox-content">
                    <!--                        <form method="post" id="add-card" name="add-card">-->
                    <input type="hidden" name="company_user_id" id="company_user_id">
                    <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$'; ?>

                    <h3>Set Payment Method <em class="red-star">*</em></h3>

                    <div class="row">
                        <input type="hidden" class='firstPaymentStatus' value='0'>
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 mb-15">
                                    <label></label>
                                    <select class="form-control setType" name="payment_method" id="payment_method" >
                                        <option value="1">Credit Card/Debit Card</option>
                                        <option value="2">ACH</option>

                                    </select>
                                    <span class="ffirst_nameErr error red-star"></span>
                                </div>
                            </div>
                            <div class="row cards">

                                <div class="cardDetails table-responsive col-sm-12"></div>
                                <form id="addCards">
                                    <div class="detail-outer">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="black-label">Card Number <em class="red-star">*</em></label>

                                                <input class="form-control hide_copy card_number numberonly" name="card_number"  type="text" maxlength="16"/>
                                                <em class="nicknameErr error red-star"></em>

                                            </div>
                                            <div class="col-sm-6">
                                                <label class="black-label">Expiration Date <em class="red-star">*</em></label>

                                                <input class="form-control exp_date" name="exp_date"  type="text" readonly/>
                                                <em class="nicknameErr error red-star"></em>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="black-label">Cardholder Name <em class="red-star">*</em></label>

                                                <input class="form-control holder_name" name="holder_name" type="text"/>
                                                <em class="nicknameErr error red-star"></em>

                                            </div>
                                            <div class="col-sm-6">
                                                <label class="black-label">CVC <em class="red-star">*</em></label>

                                                <input class="form-control cvc" name="cvc"  type="text"/>
                                                <em class="nicknameErr error red-star"></em>

                                            </div>
                                        </div>


                                        <div class="col-sm-5 clear">
                                 
                                        </div>



                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 online-payment-button">
                                            <input type="submit" class="blue-btn" value="Confirm">
                                        </div>
                                    </div>
                                </form>

                            </div>
                            <div class="row accounts" style="display:none;">
                                <div class="accountDetails table-responsive col-sm-12"></div>
                            </div>
                        </div>
                        <!--                    <div class="row" style="text-align: center;">-->
                        <!--                        <button type="submit"  id="savefinancialCard" class="blue-btn">Continue</button>-->
                        <!--                    </div>-->

                        <!--                        </form>-->
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="set-interval" role="dialog">
    <div class="modal-dialog modal-dialog-type modal-md" style="width: 60%;">
        <div class="modal-content payment-modal-new" style="width: 100%;">

              <div class="modal-header">
                <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>
                <h4 class="modal-title">Active Payment Intervals</h4>
            </div>
            <div class="modal-body">
               <!-- <h3>Set Payment Interval</h3>-->

                <div class="getPaymentIntervals"></div>


            </div>
        </div>
    </div>
</div>



<div class="overlay">
    <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
        <img width="200"  height="200" src='<?php echo COMPANY_SITE_URL ?>/images/loading.gif'/>
    </div>
</div>
<div class="overlay">
    <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
        <img width="200"  height="200" src='<?php echo COMPANY_SITE_URL ?>/images/loading.gif'/>
    </div>
</div>




    <script>
    $(document).on("click",".clear-btn.email_clear",function () {

    resetFormClear('#add_owner_conversation_form_id',['postal_code','country','state','city'],'form',false);
    });$(document).on("click",".clear-btn.email_clearpass",function () {

    resetFormClear('#changePassword',[''],'form',false);
    });
    </script>
    <script src="https://js.stripe.com/v3/"></script>
    <!-- Jquery Starts -->
    <script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
    <script>var default_Image = "<?php echo COMPANY_SITE_URL ?>/images/profile-img.png";</script>
    <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
    <script>var staticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Vehicledummy.png">';</script>
    <script>var petStaticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">';</script>
    <script type="text/javascript">var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";</script>
    <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>

    <script src="<?php echo COMPANY_SITE_URL; ?>/js/intlTelInput.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
<!--    <script src="--><?php //echo COMPANY_SITE_URL; ?><!--/js/validation/custom_fields.js"></script>-->
<!--    <script src="--><?php //echo COMPANY_SITE_URL; ?><!--/js/custom_fields.js"></script>-->
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.cropit.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantportal.js"></script>
      <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/portalpayment.js"></script>
    <script>var upload_url = "<?php echo SITE_URL; ?>"; var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";</script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/payments/plaidIntialisation.js"></script>


</div>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

