<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
include_once(COMPANY_DIRECTORY_URL . "/views/company/tenants/modals.php");
?>
<div id="wrapper" xmlns="http://www.w3.org/1999/html">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="content-rt">
                        <div class="content-rt-hdr">
                            <div class="breadcrumb">
                                <h3>Tenant >> <span>  Tenant Transfer</span>
                                    <input type="text" class="form-control input-sm easy-search" id="easy_search" placeholder="Easy Search">
                                </h3>
                            </div>
                        </div>

                        <div class="content-data">
                            <!-- Main tabs -->
                            <div class="main-tabs">
                                <div class="">
                                    <div class="form-outer">

                                        <div class="form-hdr">
                                            <h3>
                                                <strong class="left">Transfer Requests</strong>
                                                <a class="back" href="/Tenantlisting/Tenantlisting"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                            </h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                <div class="col-sm-2 tenant_type_status">
                                                    <label>Status</label>
                                                    <select class="fm-txt form-control" id="jqgridOptions">
                                                        <option value="All">All</option>
                                                        <option value="1">Request Generated</option>
                                                        <option value="2">Approved</option>
                                                        <option value="3">Lease Generated</option>
                                                        <option value="4">Transferred</option>
                                                        <option value="5">Declined</option>
                                                    </select>
                                                </div>
                                            </div>
                                        <div class="apx-table">
                                        <div class="table-responsive">
                                            <table id="tenantTrasTable"></table>
                                        </div>
                                        </div>
                                        </div>
                                    </div>





                                </div>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>
</div>
<!-- Wrapper Ends -->

<script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>

<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantTransfer.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
<script>
    $('.calander').datepicker({
        yearRange: '1919:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('input[name="birth"]').datepicker({
        yearRange: '1900:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    $(".calander").val(date);
</script>


<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->