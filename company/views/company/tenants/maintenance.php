<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if ((!isset($_SESSION[SESSION_DOMAIN]['Tenant_Portal']['tenant_portal_id'])) && (!isset($_SESSION['Admin_Access']['tenant_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/TenantPortal/login');
}




?>





  <style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>



<input type="hidden" name="tenant_id" class="tenant_id" value="<?php echo $_GET['tenant_id']; ?>">

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_top_navigation.php");
?>
<div class="popup-bg"></div>
<div id="wrapper">
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_portal_header.php"); ?>

 <?php 
    if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id'])) {
     if((!isset($_SESSION['tenant_id'])))
    {
     header('Location: '.SUBDOMAIN_URL.'/TenantPortal/login');
    }

   }

?>
                         <section class="main-content">
                                   <div role="tabpanel" class="tab-pane" id="tenant-detail-eight">
                                            <div class="col-sm-12 maintenance">
                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>
                                                            Maintenance Details
                                                            <input type="button" class="blue-btn addTicketRedirect pull-right" value="New Ticket"> 
                                                        </h3>


                                                        
                                                    </div>
                                                    <div class="form-data maintenance_main">
                                                        <div class="apx-table">
                                                          

                                                                <div id="collapseSeventeen" class="panel-collapse in">
                                                                
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="TenantMaintenance-table" class="table table-bordered">
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                 </div>
                                                             
                                                         </div>
                                                     </div>
                                                 </div>
                                            </div>
                                      </div>
                            </section>

                         

    <div id="imageModel" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    
                </div>
                <div class="modal-body">
                    <div class="col-sm-4" id="getImage1"></div>
                    <div class="col-sm-4" id="getImage2"></div>
                    <div class="col-sm-4" id="getImage3"></div>
                </div>
                <div class="modal-footer">
                 
                </div>
            </div>

        </div>
    </div>




     




    <!-- Jquery Starts -->
    <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
    <script>var staticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Vehicledummy.png">';</script>
    <script>var petStaticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg">';</script>
    <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
   <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/bootstrap-tagsinput.css" />
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/intlTelInput.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/tenantportal.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
     <script>var upload_url = "<?php echo SITE_URL; ?>";</script>

</div>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<script>

  $(".to").tagsinput('items')


 </script>