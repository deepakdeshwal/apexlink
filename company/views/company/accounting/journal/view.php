<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->
        
        
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                      <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                              Accounting &gt;&gt; <span>Journal Entry</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="easy-search">
                              <input placeholder="Easy Search" type="text">
                          </div>
                        </div>
                      </div>
                    </div>
                   
                    <div class="col-sm-12">
                      <div class="content-section">
                         <!--Tabs Starts -->
                        <div class="main-tabs">                        
                          <!-- Nav tabs -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="guest-cards">
                              <div class="form-outer form-outer2">
                                <div class="form-hdr">
                                  <h3>View Journal Entry <a class="back" href="/Accounting/JournalEntries"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                </div>
                                <div class="form-data">
                                  <div class="detail-outer">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                  <strong>Type</strong> <span class="journal_entry_type text-capitalize view-j-entry-type">One Time</span>
                                              </div>.
                                              <div class="col-sm-6">
                                                <div class="col-xs-12">
                                                  <label class="text-right">Portfolio :</label>
                                                  <span class="portfolio_name">--</span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <label class="text-right">Property :</label>
                                                    <span class="property_name">--</span>
                                                </div>                                               
                                              </div>
                                              <div class="col-sm-6">
                                                <div class="col-xs-12">
                                                  <label class="text-right">Unit :</label>
                                                  <span class="unit_prefix">--</span>
                                                </div>
                                                <div class="col-xs-12">
                                                  <label class="text-right">Accounting Period :</label>
                                                  <span class="accounting_period">--</span>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                </div>
                              </div>
                               <!-- Form Outer Ends -->

                               <div class="form-outer">
                                 <div class="form-hdr"><h3>Details</h3></div>
                                 <div class="form-data">
                                    <div class="grid-outer">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-dark">
                                          <thead>
                                            <tr>
                                              <th scope="col">Account</th>
                                              <th scope="col">Description</th>
                                              <th scope="col">Debit (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</th>
                                              <th scope="col">Credit (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</th>
                                              <th scope="col"></th>
                                            </tr>
                                          </thead>
                                          <tbody class="account_details_html">

                                          </tbody>
                                        </table>
                                      </div>
                                     
                                    </div>

                                 </div>
                               </div>
                               <!--Form Outer Ends -->

                                <div class="form-outer">
                                    <div class="form-hdr">
                                        <h3>Notes</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <span class="notes"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--Form Outer Ends -->


                                <div class="form-outer">
                                    <div class="form-hdr"><h3>Files</h3></div>
                                    <div class="form-data">
                                        <div class="grid-outer">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-dark">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">File Name</th>
                                                        <th scope="col">Preview</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="files_details_html">

                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>

                                    </div>
                                </div>


                               

                            </div>
                      

                        </div>
                        <!--tab Ends -->
                        
                    </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
        <!-- Wrapper Ends -->

<!-- Wrapper Ends -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
</script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>

<!-- Footer Ends -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/journalEntry/viewJournalEntry.js"></script>
<script>
        var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<!-- Footer Ends -->
     
    <script>
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });
 
        <!--- Main Nav Responsive --> 
        $("#show").click(function(){
    $("#bs-example-navbar-collapse-2").show();
});
         $("#close").click(function(){
    $("#bs-example-navbar-collapse-2").hide();
});
         <!--- Main Nav Responsive -->
        
        
        $(document).ready(function(){
          $(".slide-toggle").click(function(){
            $(".box").animate({
              width: "toggle"
            });
          });
        });
        
        $(document).ready(function(){
          $(".slide-toggle2").click(function(){
            $(".box2").animate({
              width: "toggle"
            });
          });
        });
        
         <!--- Accordians -->
       $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
        	$(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });
        
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
         <!--- Accordians --> 
    </script>

    
    


    <!-- Jquery Starts -->

</body>

</html>