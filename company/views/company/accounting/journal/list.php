<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting &gt;&gt; <span>Journal Entry </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>ACCOUNTING</h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" >Receive Money</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Multi Payments</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">My Bills</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="#" id="" class="list-group-item list-group-item-success strong collapsed">Pay Bills</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">Invoices</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Write Checks</a>
                                <!-- Seven Ends-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Print Checks</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Check Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Payment Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bill Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bank Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bank Deposit</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Late Fees</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Owner Draw</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Owner Contributions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Transactions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Process Management Fee</a>
                                <a id="" href="/Accounting/PendingGLPosting" class="list-group-item list-group-item-success strong collapsed" >Pending Transactions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Credit Card Details</a>
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links ---->

                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Accounting/Accounting">Receivables</a></li>
                            <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                            <li role="presentation"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                            <li role="presentation"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                            <li role="presentation"><a href="/Accounting/BankRegister">Banking</a></li>
                            <li role="presentation" class="active"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                            <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                            <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconcilation</a></li>

                            <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                            <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                            <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                            <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content">
                            <div class="panel-heading">

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="receivables">
                                        <div class="property-status">
                                            <div class="col-sm-12">
                                                <div class="btn-outer mb-custom text-right">
                                                    <a href="/newJournalEntry" class="blue-btn">New Journal Entry</a>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="date_range" class="date_range">
                                                    <div class="col-sm-6 col-md-2">
                                                        <label>From</label>
                                                        <input class="form-control start_date calendar-input" id="start_date" readonly type="text" name="start_date">

                                                    </div>
                                                    <div class=" col-sm-6 col-md-2">
                                                        <label>To</label>
                                                        <input class="form-control end_date calander calendar-input" id="end_date" readonly type="text" name="end_date">
                                                    </div>
                                                    <div class="col-sm-6 col-md-2" id="call_category_filter" style="display:block">
                                                        <label>Type</label>
                                                        <select class="form-control" id="incoming_outgoing_call_type" >
                                                            <option value="">Select</option>
                                                            <option value="one_time">One Time</option>
                                                            <option value="monthly">Monthly</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6 col-md-2">
                                                        <label>&nbsp;</label>
                                                        <button class="blue-btn callLogSearch">Search</button>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive overflow-unset">
                                                                                <table id="journal-entry-table" class="table table-bordered"></table>
                                                                                </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Regular Rent Ends -->

                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>



                        <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
</div>
<!-- Wrapper Ends -->
    <script>
        var default_currency_symbol  = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
         var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    </script>

<!-- Footer Ends -->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/journalEntry/listJournalEntry.js"></script>
<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



</script>

<!-- Jquery Starts -->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>