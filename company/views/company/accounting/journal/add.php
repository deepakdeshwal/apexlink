<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>


    <section class="main-content">
        <form id="addJournalEntry" enctype='multipart/form-data'>
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Accounting &gt;&gt; <span>Journal Entry</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="content-section">
                            <!--Tabs Starts -->
                            <div class="main-tabs">
                                <!-- Nav tabs -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="guest-cards">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>New Journal Entry <a class="back" href="/Accounting/JournalEntries"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Type</label>
                                                        <div class="check-outer">
                                                            <input type="radio" name="journal_entry_type" class="journal_entry_type" value="one_time" checked="checked"/> <label>One Time</label>
                                                        </div>
                                                        <div class="check-outer">
                                                            <input type="radio" name="journal_entry_type" class="journal_entry_type"  value="recurring"/> <label>Recurring</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2 clear" style="display:none" id="journal_entry_type_select_div">
                                                        <select class="form-control journal_entry_type_select"  id="journal_entry_type_select" name="journal_entry_type_select"><option value="">Select</option><option value="1">Monthly</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2 clear">
                                                        <label>Portfolio<em class="red-star">*</em></label>
                                                        <select class="form-control portfolio"  id="selected_portfolio" name="selected_portfolio"><option>Select</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2">
                                                        <label>Property<em class="red-star">*</em></label>
                                                        <select class="form-control property" id="property" name="property">
                                                            <option value="" >This portfolio has no property<option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2">
                                                        <label>Building<em class="red-star">*</em></label>
                                                        <select class="form-control building" id="building" name="building" ><option value="">This property has no building</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2">
                                                        <label>Unit</label>
                                                        <select class="form-control unit" id="unit" name="unit"><option value="">This building has no unit</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2">
                                                        <label>Accounting Period<em class="red-star">*</em></label>
                                                        <input class="form-control accounting_period customValidationaccountingperiod" data_required="true" id="accounting_period" name="accounting_period"  type="text"/>
                                                        <span class="error red-star customAccPeriodError"></span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2">
                                                        <label>Bank Account<em class="red-star">*</em> <a class="pop-add-icon " href="javascript:;" id="add_more_bank_account_icon"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                        <select class="form-control bank_account" name="bank_account" id="bank_account"><option>Select</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2">
                                                        <label>Reference Number</label>
                                                        <input class="form-control refrence_number" id="refrence_number" name="refrence_number" placeholder="" type="text"/>
                                                    </div>

                                                </div>




                                            </div>
                                        </div>
                                        <!-- Form Outer Ends -->

                                        <div class="form-outer">
                                            <div class="form-hdr"><h3>Details</h3></div>
                                            <div class="form-data">
                                                <div class="grid-outer">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-dark">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">Account</th>
                                                                <th scope="col">Description</th>
                                                                <th scope="col">Debit (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</th>
                                                                <th scope="col">Credit (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</th>
                                                                <th scope="col"></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr class="additional-account-chart-details">
                                                                <td><select class="form-control chart_account customValidationchartaccount" name="chart_account[]" data_required="true"><option>Select</option></select>  <span class="customError required"></span></td>

                                                                <td><input class="form-control" placeholder="" type="text" name="description[]"></td>
                                                                <td><input class="form-control money debit" placeholder="" type="text" name="debit[]"></td>
                                                                <td><input class="form-control money credit" placeholder="" type="text" name="credit[]"></td>
                                                                <td><i title="Add" class="fa fa-plus-circle fa-lg additional-add-account-details" aria-hidden="true"></i>
                                                                    <i title="Remove" class="fa fa-times-circle fa-lg red-cross remove-account-details" style="display:none" aria-hidden="true"></i></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="" colspan="2" style="font-size: 14px;">
                                                                    <strong>Balance</strong>
                                                                </td>
                                                                <td class="text-right" style="font-size: 14px;">
                                                                    <strong>
                                                                        <span id="TotalDebit"></span>
                                                                    </strong>
                                                                </td>
                                                                <td class="text-right" style="font-size: 14px;">
                                                                    <strong>
                                                                        <span id="TotalCredit"></span>
                                                                    </strong>
                                                                </td>
                                                                <td class="" style="vertical-align: middle">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <!--Form Outer Ends -->
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Notes</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                                        <div class="notes_date_right_div">
                                                        <textarea class="form-control add-input notes notesDateTime notes_date_right" id="notes" name="notes"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Form Outer Ends -->
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>File Library</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                        <button type="button" class="orange-btn" id="remove_library_file">Remove All Files</button>
                                                        <input id="file_library" type="file" name="file_library[]" accept=".gif,.jpeg,.jpg,.png,.doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                    </div>
                                                    <div class="row" id="file_library_uploads">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Form Outer Ends -->

                                        <div class="btn-outer text-right">
                                            <input class="blue-btn submit_button" type="submit" value="Save"/>
                                            <input class="blue-btn submit_button" type="submit" value="Save & Add Another"/>
                                            <button type="button" class="clear-btn clearFormReset" id="clear_journal">Clear</button>
                                            <button type="button" class="grey-btn cancel">Cancel</button>
                                        </div>

                                    </div>


                                </div>
                                <!--tab Ends -->

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>

    </section>
</div>
<!-- Wrapper Ends -->

<div class="container">
    <div class="modal fade" id="add_more_bank_account_modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Bank Account</h4>
                </div>
                <div class="modal-body pad-none">
                    <div class="panel-body" style="border-color: transparent;">
                        <div class="row">
                            <form class="form-outer" id="add_bank_account_form">
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <label>Portfolio <em class="red-star">*</em></label>
                                        <select id="portfolio" name="portfolio" class="fm-txt form-control">
                                            <option value="">Select</option>
                                        </select>
                                        <span id="portfolioErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Bank Name  <em class="red-star">*</em></label>
                                        <input name="bank_name" id="bank_name" maxlength="100" placeholder="Eg: Wells Fargo"   class="form-control hide_copy" type="text"/>
                                        <span id="bank_nameErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Bank A/c No.  <em class="red-star">*</em></label>
                                        <input name="bank_account_number" id="bank_account_number" placeholder="Eg: 123A12345678"   class="form-control hide_copy" type="text"/>
                                        <span id="bank_account_numberErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>FDI No.  <em class="red-star">*</em></label>
                                        <input name="fdi_number" id="fdi_number" maxlength="10" placeholder="Eg: 123A12345678"   class="form-control" type="text"/>
                                        <span id="fdi_numberErr" class="error"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <label>Branch Code <em class="red-star">*</em></label>
                                        <input name="branch_code" id="branch_code" maxlength="10" placeholder="Eg: AB12 12345 "   class="form-control" type="text"/>
                                        <span id="branch_codeErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Initial Amount(<?php echo $_SESSION[SESSION_DOMAIN]["default_currency_symbol"]?>)  <em class="red-star">*</em></label>
                                        <input name="initial_amount" id="initial_amount" placeholder="Eg: 1000"   class="form-control" type="text"/>
                                        <span id="initial_amountErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Last Used Check Number  <em class="red-star">*</em></label>
                                        <input name="last_used_check_number" id="last_used_check_number" maxlength="100" placeholder="Eg: 1234"   class="form-control" type="text"/>
                                        <span id="last_used_check_numberErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Status <em class="red-star">*</em></label>
                                        <select id="status" name="status" class="fm-txt form-control">
                                            <option value="">Select</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                        <span id="statusErr" class="error"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12 bank-account-add-checkbox">



                                    <div class="col-sm-3">
                                        <label>Bank Routing  <em class="red-star">*</em></label>
                                        <input name="routing_number" id="routing_number_rec" maxlength="10" placeholder="Eg: 110000000"   class="form-control" type="text"/>
                                        <span id="routing_numberErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label></label>
                                        <div class="check-outer mg-lt-30 mb-15">
                                            <input name="is_default" id="is_default" type="checkbox"/>
                                            <label>Set as Default</label>
                                        </div>
                                    </div>

                                </div>

                                <div class="btn-outer mg-lt-30 col-sm-12 add-bank-account-modal-button text-right">
                                    <button type="submit" class="blue-btn" value="Save" >Save</button>
                                    <button type="button" class="clear-btn clearFormReset" id="clear_bank_account">Clear</button>
                                    <button type="button" id="add_bank_account_cancel_btn" class="grey-btn">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>







<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<style>
    .ui-datepicker-calendar {
        display: none;
    }
</style>
<script>
    var default_currency_symbol  = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var pagination  = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var default_form_data = '';
    var defaultIgnoreArray = [];

</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/monthpicker.min.js" type="text/javascript"></script>
<link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/MonthPicker.min.css" rel="stylesheet">
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/journalEntry/journalEntry.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/autonumeric.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/journalEntry/journalFileLibrary.js"></script>
<!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/building/buildingFileLibrary.js"></script>-->
<!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/building/buildingPhotoVideos.js"></script>-->

<!-- Footer Ends -->


<!-- Jquery Starts -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
    <!--- Accordians -->
</script>

<script>
    $(document).on('click','#clear_journal',function(){
        bootbox.confirm("Do you want to clear this form?", function (result) {
            if (result == true) {
                window.location.reload();
            }
        });
    });

    $(document).on('click','#clear_bank_account',function(){
        resetFormClear('#add_bank_account_form',['portfolio'],'form',true,default_form_data,defaultIgnoreArray);
        $("#is_default").prop('checked',false)


    });
</script>





<!-- Jquery Starts -->

</body>

</html>