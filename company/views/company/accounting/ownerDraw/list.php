<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div class="popup-bg"></div>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <div class="breadcrumb-outer">
                                    Accounting >> <span>Owner Draw</span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent">
                        <!--- Right Quick Links ---->
                        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/accounting/layout/right-nav.php");?>
                        <!--- Right Quick Links ---->

                        <form id="addOwner" enctype='multipart/form-data'>
                            <div class="form-outer" id="list_owner_draw_div" style="display: block">
                                <div class="form-hdr">
                                    <h3>List of Owner Draw<a type="button" style="padding: 8px 15px;" class="blue-btn pull-right" href="/Accounting/GenerateOwnerDraw">New Owner Draw</a></h3>
<!--                                    <div class="btn-outer pull-left">-->
<!--                                        -->
<!--                                    </div>-->
                                </div>

                                <div class="form-data">
                                    <div class="col-sm-3">
                                        <label>Owner Name</label>
                                        <select class="form-control add-input common_search_class" type="text" id="owner_name_id" name="owner_name_id" maxlength="50">
                                            <option value="all">Select</option>
                                        </select>
                                        <a class="add-icon add_owner_plus_sign add-icon-abs" href="/People/AddOwners">
                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Property Name</label>
                                        <select placeholder="Select" class="form-control add-input common_search_class" type="text" id="property_id" name="property_id">
                                            <option value="all">Select</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Start Date</label>
                                        <input class="form-control common_search_class" readonly type="text" id="start_date" name="start_date" >
                                    </div>
                                    <div class="col-sm-3">
                                        <label>End Date</label>
                                        <input class="form-control common_search_class" readonly type="text" id="end_date" name="end_date" >
                                    </div>
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <div class="apx-table listinggridDiv">
                                                                    <div class="table-responsive">
                                                                        <table id="owner_draw_listing" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<!-- Wrapper Ends -->



<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->

<script type="text/javascript">
    $('#accounting_top').addClass('active');
    var upload_url = "<?php echo SITE_URL; ?>";
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/ownerDraw/ownerDrawListing.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.easyui.min.js" type="text/javascript"></script>
<link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.easyui.css" rel="stylesheet">

<script>

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });

    <!--- Main Nav Responsive -->
    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

</script>

<style>
    select[name=hobbies] + .btn-group button{
        margin-top: 0px;
    }
    .closeimagepopupicon{
        float: right;
        font-weight: bold;
        border: 2px solid #666;
        border-radius: 12px;
        padding: 0px 4px;
        cursor: pointer;
    }

    .apx-inline-popup {
        position: relative;
    }
    .apx-inline-popup-box > h4 {
        font-size: 12px
    }

    .image-editor {
        text-align: center;
    }

    .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 5px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 250px;
        height: 250px;
        display: inline-block;
    }

    .image-size-label {
        margin-top: 10px;
    }

    .export {
        /* Use relative position to prevent from being covered by image background */
        position: relative;
        z-index: 10;
        display: block;
    }

    .image-editor input[type="file"] {
        opacity: 0;
        position: absolute;
        top: 45px;
        left: 91px;
    }

    .upload-logo .img-outer img {
        max-width: 100%;
    }


</style>
<!-- Jquery Ends -->
</body>

</html>