<?php

/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid"s>
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting &gt;&gt; <span>Recurring Invoices </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>ACCOUNTING</h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" >Receive Money</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Multi Payments</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">My Bills</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="#" id="" class="list-group-item list-group-item-success strong collapsed">Pay Bills</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">Invoices</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Write Checks</a>
                                <!-- Seven Ends-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Print Checks</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Check Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Payment Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bill Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bank Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bank Deposit</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Late Fees</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Owner Draw</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Owner Contributions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Transactions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Process Management Fee</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Pending Transactions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Credit Card Details</a>
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" ><a href="/Accounting/Accounting">Receivables</a></li>
                            <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                            <li role="presentation"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                            <li role="presentation"  class="active"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                            <li role="presentation"><a href="/Accounting/BankRegister">Banking</a></li>
                            <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                            <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                            <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconcilation</a></li>

                            <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                            <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                            <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                            <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content">
                            <div class="panel-heading">
                                <div class="form-hdr" style="margin-bottom: 20px;">
                                    <h3 class="d-inline-block">Recurring Transactions</h3>
                                    <!--<button class="blue-btn">Receive Payment</button>-->
                                    <div class="pull-right recurring-button">
                                        <a href="/Accounting/RecurringBill"><input type="button" class="blue-btn btn-pad-normal blue-btn-alt" value="Recurring Bills"></a>
                                        <a href="/Accounting/RecurringInvoices"><input type="button" class="blue-btn btn-pad-normal" value="Recurring Invoices"></a>
                                        <a href="/Accounting/RecurringChecks"><input type="button" class="blue-btn btn-pad-normal blue-btn-alt" value="Recurring Checks"></a>
                                    </div>
                                </div>
<!--                                <h4 class="panel-title">-->
<!--                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Time Settings</a>-->
<!--                                </h4>-->
                                <!-- Nav tabs -->
<!--                                <ul class="nav nav-tabs" role="tablist">-->
<!--                                </ul>-->
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="receivables">
<!--                                        <div class="property-status">-->
<!--                                            <div class="row">-->
<!---->
<!--                                                <div class="col-sm-12">-->
<!--                                                    <div class="btn-outer text-right">-->
<!--                                                        <a href="/Accounting/RecurringBill"><input type="button" class="blue-btn" value="Recurring Bills"></a>-->
<!--                                                        <a href="/Accounting/RecurringInvoices"><input type="button" class="blue-btn" value="Recurring Invoices"></a>-->
<!--                                                        <a href="/Accounting/RecurringChecks"><input type="button" class="blue-btn" value="Recurring Checks"></a>-->
<!---->
<!--                                                    </div>-->
<!---->
<!---->
<!---->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="accordion-grid">-->
<!--                                            <div class="accordion-outer">-->
<!--                                                <div class="bs-example">-->
<!--                                                    <div class="panel-group" id="accordion">-->
<!--                                                        <div class="panel panel-default">-->
<!--                                                            <div class="panel-heading">-->
<!--                                                                <h4 class="panel-title">-->
<!--                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> Invoices</a>-->
<!--                                                                </h4>-->
<!--                                                                <div class="form-control text-right">-->
<!--                                                                -->
<!--                                                                </div>-->
<!--                                                            </div>-->
<!--                                                            <div id="collapseOne" class="panel-collapse collapse  in">-->
<!--                                                                <div class="panel-body pad-none">-->
<!--                                                                    <div class="grid-outer">-->
<!--                                                                        -->
<!--                                                                    </div>-->
<!--                                                                </div>-->
<!--                                                            </div>-->
<!--                                                        </div>-->
<!--                                                    </div>-->
<!---->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Recurring Invoices <a href="/Accounting/newRecurringInvoice"><input type="button" class="blue-btn text-right pull-right btn-pad-normal blue-btn-alt" value="New Recurring Invoice"></a></h3>
                                            </div>
                                            <div class="form-data">

                                                <table id="list_of_invoices" class="table table-bordered nowrap"></table>
                                            </div>
                                        </div>
                                        <!--<div class="btn-outer pull-right">
                                            <button class="grey-btn pull-right delete_invoice">Delete</button>

                                            <button class="blue-btn pull-right print_email_envoice">Print/Email</button>
                                        </div>-->
                                    </div>
                                    <!-- Regular Rent Ends -->







                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>



                        <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
</div>
<!-- Wrapper Ends -->


<div class="modal fade" id="InvoiceModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content pull-left">
            <div class="modal-header" style="border-bottom: unset;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                </a>
                <input type="button" class="blue-btn pull-left"  onclick="sendInvoiceEmail()"  value="Email"/>
                <input type="button" class="blue-btn pull-left" onclick="PrintElem('#invoice_content')" onclick="window.print();"  value="Print"/>


            </div>
            <div class="modal-body pull-left" style="height: 487px; overflow: auto;" id="invoice_content">


            </div>

        </div>
    </div>
</div>
<!-- Footer Ends -->
<script>
    var default_currency_symbol  = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/RecurringInvoices/invoiceListing.js"></script>

<script>

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



</script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
