<div class="right-links-outer hide-links">
    <div class="right-links">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
        <i class="fa fa-angle-right" aria-hidden="true"></i>
    </div>
    <div id="RightMenu" class="box2">
        <h2>ACCOUNTING</h2>
        <div class="list-group panel">
            <!-- Two Ends-->
            <a href="#" class="list-group-item list-group-item-success strong collapsed" >Receive Money</a>
            <!-- Two Ends-->

            <!-- Three Starts-->
            <a id="" href="/MultiPay/MultiPay" class="list-group-item list-group-item-success strong collapsed" >Multi Payments</a>
            <!-- Three Ends-->

            <!-- Four Starts-->
            <a href="/Vendor/NewBill" class="list-group-item list-group-item-success strong collapsed">New Bills</a>
            <!-- Four Ends-->

            <!-- Five Starts-->
            <a href="/Accounting/paybills" id="" class="list-group-item list-group-item-success strong collapsed">Pay Bills</a>
            <!-- Five Ends-->

            <!-- Six Starts-->
            <a href="/Accounting/ConsolidatedInvoice" class="list-group-item list-group-item-success strong collapsed">Invoices</a>
            <!-- Six Ends-->

            <!-- Seven Starts-->
            <a id="" href="/Accounting/BankRegister" class="list-group-item list-group-item-success strong collapsed" >Write Checks</a>
            <!-- Seven Ends-->
            <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Print Checks</a>
            <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Check Register</a>
            <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Payment Register</a>
            <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bill Register</a>
            <a id="" href="/Accounting/BankRegister" class="list-group-item list-group-item-success strong collapsed" >Bank Register</a>
            <a id="" href="/Tenant/BatchRegister" class="list-group-item list-group-item-success strong collapsed" >Bank Deposit</a>
            <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Late Fees</a>
            <a id="" href="/Accounting/OwnerDraw" class="list-group-item list-group-item-success strong collapsed" >Owner Draw</a>
            <a id="" href="/Accounting/OwnerContribution" class="list-group-item list-group-item-success strong collapsed" >Owner Contributions</a>
            <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Transactions</a>
            <a id="" href="/Accounting/ProcessManagementFee" class="list-group-item list-group-item-success strong collapsed" >Process Management Fee</a>
            <a id="" href="/Accounting/PendingGLPosting" class="list-group-item list-group-item-success strong collapsed" >Pending Transactions</a>
            <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Credit Card Details</a>
        </div>
    </div>
</div>