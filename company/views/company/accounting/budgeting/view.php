<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->


        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Accounting &gt;&gt; <span>Budgeting </span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data">
                        <!--- Right Quick Links ---->
                        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/accounting/layout/right-nav.php");?>
                        <!--- Right Quick Links ---->
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"><a href="/Accounting/Accounting">Receivables</a></li>
                                <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                                <li role="presentation"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                                <li role="presentation"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                                <li role="presentation"><a href="/Accounting/BankRegister">Banking</a></li>
                                <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                                <li role="presentation" class="active"><a href="/Accounting/Budgeting">Budgeting</a></li>
                                <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconcilation</a></li>

                                <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                                <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                                <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                                <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="account-budget">
                                    <div class="property-status">
                                        <div class="row">
                                            <div class="col-sm-12 text-right">
                                                <div class="btn-outer">
                                                    <button onclick="window.location.href='/Accounting/Budgeting/AddBudget'" class="blue-btn">New Budget</button>
                                                    <button type="button" data-toggle="modal" data-target="#myModal" id="copyBudgetButton" class="blue-btn">Copy Budget</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-outer">
                                        <div class="row">
                                            <div class="col-sm-6 col-md-2">
                                                <label>Portfolio</label>
                                                <select id="portfolio_id" class="form-control commonFilters portfolio_id"><option value="all">Select</option></select>
                                            </div>
                                            <div class="col-sm-6 col-md-2">
                                                <label>Property</label>
                                                <select id="property_id" class="form-control commonFilters property_id"><option value="all">Select Property</option></select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div class="panel panel-default">
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body pad-none">
                                                                <div class="grid-outer">
                                                                    <div class="apx-table">
                                                                        <div class="table-responsive">
                                                                            <table id="budgetTable" class="table">
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>
                    </div>
                </div>
                <!--Tabs Ends -->
            </div>
        </section>
    </div>

    <div id="myModal" class="modal fade in" role="dialog"">
        <div class="modal-dialog spotlight-pop">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Copy Budget</h4>
                </div>
                <div class="modal-body">
                    <form id="copyBudgetForm">
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <label>Budget Name <em class="red-star">*</em></label>
                                    <input class="form-control" name="budget_name" placeholder="" type="text"/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Starting Month <em class="red-star">*</em></label>
                                    <select id="startingMonth" name="starting_month" class="form-control">
                                        <option value="">Select</option>
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option selected value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Starting Year <em class="red-star">*</em></label>
                                    <select name="starting_year" id="yearDesc" class="form-control">
                                        <option value="">Select</option>
                                        <?php for($i=0;$i<6;$i++) {?>
                                            <option value="<?php echo (date("Y",strtotime("-1 year")) + $i) ?>"><?php echo (date("Y",strtotime("-1 year")) + $i) ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Portfolio <em class="red-star">*</em></label>
                                    <select name="portfolio_id" id="copy_portfolio_id" class="form-control"></select>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Property <em class="red-star">*</em></label>
                                    <select name="property_id" id="copy_property_id" class="form-control"></select>
                                </div>
                            </div>
                        </div>
                        <div class="form-hdr">
                            <h3>Copy Budget from</h3>
                        </div>
                        <div class="form-data">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <label>Portfolio <em class="red-star">*</em></label>
                                    <select id="search_portfolio_id" name="search_portfolio_id" class="form-control"><option value="">Select Property</option></select>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Property <em class="red-star">*</em></label>
                                    <select id="search_property_id" name="search_property_id" class="form-control"><option value="">Select Property</option></select>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Budgets <em class="red-star">*</em></label>
                                    <select id="budget" name="budget" class="form-control"><option value="">Select</option></select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="btn-outer col-sm-12 text-right">
                                <button type="submit" class="blue-btn">Copy Budget</button>
                                <button type="button"  class="clear-btn clearFormReset" id="clearCopyBudget">Clear</button>
                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Wrapper Ends -->
    <script>
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
        var upload_url = "<?php echo SITE_URL; ?>";
        var property_unique_id = '';
        var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
        var default_name = "<?php echo $_SESSION[SESSION_DOMAIN]['default_name']; ?>";
        var manager_id = "";
        var attachGroup_id = "";
        var credential_notice_period =  "<?php echo $_SESSION[SESSION_DOMAIN]['default_notice_period']; ?>";
        var copyformData = '';
    </script>

    <!-- Footer Ends -->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/additionalMethods.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accountingModule/budgeting/budgetList.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accountingModule/budgeting/copyBudget.js" type="text/javascript"></script>
    <script>
        $('#accounting_top').addClass('active');
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });
        });

        $(document).ready(function(){
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
        });

        $(document).on('click','copyBudgetButton',function(){
            copyformData =$('#copyBudgetForm').serializeArray();
        });

        $(document).on('click','#clearCopyBudget',function(){
            resetFormClear('#copyBudgetForm',[],'form',false,copyformData);
        });



    </script>
    <!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");

?>