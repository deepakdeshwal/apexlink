
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting &gt;&gt; <span>Receive Payments </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/accounting/layout/right-nav.php");?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs batch-payment">
                        <!-- Nav tabs -->
        

                        <!-- Tab panes -->

                        <div class="tab-content ">
                            <div class="panel-heading">

                                    <!-- Nav tabs -->
                                    <!-- <ul class="nav nav-tabs" role="tablist">
                                    </ul> -->
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="receivables">
                                            <div class="property-status">
                                                <div class="row">
                                                    <div class='PaymentHeading panel-title recieve-panel-heading-view'>Receive Payment</div>
                                                    <div class="col-sm-6">
                                                        <!-- <label>List of tenants</label> -->
                                                        <input type='text' name='listOfReceivable' id="listOfReceivable" class="form-control listOfReceivable payment-form-control" placeholder="Please Enter Name, Invoice Number,Property or Balance Account">  

                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="btn-outer text-right">
                                                            <a herf="" class="receivableback goback_func"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="userFullAccount"></div>
                                           <form id='updateAllCharges' class='updateAllCharges'>
                                            <div class="allCharges">
                                                
                                            </div>
                                          </form>

                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> List of Tenants Receivable</a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="table-responsive">

                                                                                    <table id="accounting_invoices"
                                                                                    class="table table-bordered">

                                                                                    </table>
     
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Regular Rent Ends -->

                                    </div>
                                </div>
                                <!-- Sub tabs ends-->
                            </div>



                                <!-- Sub Tabs Starts-->

                                <!-- Sub tabs ends-->
                            </div>

                        </div>
                    </div>

                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->



<!-- Footer Ends -->
<script>var default_currency_symbol  = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/receiveBatchPayment.js"></script>
<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

</script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>