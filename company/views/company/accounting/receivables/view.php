<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting &gt;&gt; <span>Receivable </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/accounting/layout/right-nav.php");?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="/Accounting/Accounting">Receivables</a></li>
                            <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                            <li role="presentation"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                            <li role="presentation"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                            <li role="presentation"><a href="/Accounting/BankRegister">Banking</a></li>
                            <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                            <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                            <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconcilation</a></li>

                            <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                            <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                            <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                            <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content view_accounting-content">
                            <div class="panel-heading">
                                
                                   <h5 class="panel-title">Receivables</h5>
                                
                                    <!-- Nav tabs -->
                                      <div class="panel-title"></div>
                                    <ul class="nav nav-tabs" role="tablist">
                                    </ul>
                                    <!-- Tab panes -->

                                    <div class="tab-content">

                                        <div role="tabpanel" class="tab-pane active" id="receivables">

                                            <div class="property-status">

                                                <div class="row">
                                                    <div class="col-md-2 col-sm-6">
                                                        <label>Property</label>
                                                        <select class="fm-txt form-control property">
                                                           
                                                        </select>
                                                    </div>
                                                    <div class="col-md-10 col-sm-6">
                                                        <div class="btn-outer text-right">
                                                            <a href="receivebatchpayment"><button class="blue-btn">Receive Payment</button></a>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                           
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                           <table id="tenantReceivables"
                                                                                    class="table table-bordered">

                                                                                    </table>
                                                                            </div>
                                                                        </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Regular Rent Ends -->

                                    </div>
                                </div>
                                <!-- Sub tabs ends-->
                            </div>



                                <!-- Sub Tabs Starts-->

                                <!-- Sub tabs ends-->
                            </div>

                        </div>
                    </div>

                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<script>var default_currency_symbol  = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/accounting.js"></script>
<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



</script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>