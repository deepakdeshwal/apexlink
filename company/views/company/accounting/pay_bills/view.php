<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
$satulation_array =['1'=>'Dr.','2'=>'Mr.','3'=>'Mrs.','4'=>'Mr. & Mrs.','5'=>'Ms.',6=>'Sir',7=>'Madam',8=>'Sister',9=>'Mother'];
$gender_array =['1'=>'Male','2'=>'Female','3'=>'Prefer Not To Say','4'=>'Other'];
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<style>
    .datagrid-view2 .datagrid-body .datagrid-btable td {
         padding: unset;
    }
    .datagrid-view2 .datagrid-header {
        padding: 5px 0px;
    }
    .panel-htop .combo-panel {
         margin-top: unset;
         margin-left: unset;
    }
</style>
    <style type="text/css">

        .StripeElement {
            box-sizing: border-box;

            height: auto;

            padding: 10px 12px;

            border: 1px solid transparent;
            border-radius: 4px;
            background-color: white;

            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }

        #payNow {
            max-width: 600px;
            margin: auto;
            top: 20%;
        }
        #payment-form {
            background: #fff;
            padding: 20px;
        }
        #payment-form label{
            font-size: 13px;
            /*border-bottom: 1px solid #cacaca;*/
            padding-bottom: 3px;
        }
        .mt-none {
            margin-top: 0 !important;
        }
        #card-errors
        {
            color:red;
        }
        #payment-form .payment-form-label label {
            font-size: 33px;
            font-weight: normal;
        }
        #payment-form .payment-form-label label span {
            color: #f00;
        }
        .payment-form-label .payment-method-text {
            margin: 0;
            text-align: center;
            font-size: 18px;
            text-decoration: underline;
            font-weight: 600;
        }
        img.payment-method-logo {
            max-width: 110px;
            margin-top: 110px;
        }
        button.blue-btn.payment-submit-button {
            margin-top: 118px;
        }
        #payment-form label.card-payment-text {
            font-size: 16px;
            color: #1c91cc;
        }
        /*.CardField-input-wrapper .CardField-number {
            transform: translateX(0px) !important;
            float: left !important;
            border: 1px solid rgb(221, 221, 221) !important;
            width: 100% !important;
            border-radius: 5px !important;
            padding: 5px !important;
            height: auto !important;
        }
        .CardField-expiry {
            transform: translateX(0px);
            float: left;
            border: 1px solid rgb(221, 221, 221);
            width: 60%;
            border-radius: 5px;
            padding: 5px;
            height: auto;
        }
        .CardField-cvc {
            transform: translateX(0px);
            float: left;
          border: 1px solid rgb(221, 221, 221);
            width: 40%;
            border-radius: 5px;
            padding: 5px;
            height: auto;
        }*/
        .submit_payment_class {
            position: absolute;
            bottom: -50px;
            right: 13px;
        }
    </style>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting &gt;&gt; <span> Pay Bills </span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/accounting/layout/right-nav.php");?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Accounting/Accounting">Receivables</a></li>
                            <li role="presentation" class="active"><a href="/Accounting/paybills">Pay Bills</a></li>
                            <li role="presentation"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                            <li role="presentation"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                            <li role="presentation"><a href="/Accounting/BankRegister">Banking</a></li>
                            <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                            <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                            <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconcilation</a></li>

                            <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                            <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                            <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                            <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="" id="account-paybill">
                                <div class="property-status">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-1">
                                            <label>Show Bills</label>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="check-outer">
                                                <input name="alt" id="showAllBills" class="searchRadio" value="1" checked type="radio">
                                                <label>Show All Bills</label>
                                            </div>
                                            <div class="check-outer clear">
                                                <input name="alt" id="dueBefore" class="searchRadio" value="2" type="radio">
                                                <label>Due on or before</label>
                                            </div>
                                            <div class="check-outer">
                                                <input name="alt" id="showCardBills" class="searchRadio" value="3" type="radio">
                                                <label>Show credit card bills</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6" id="dateFilterDiv" style="display:none;">
                                            <div class="check-outer">
                                                <input class="form-control" readonly id="searchDate">
                                                <input type="hidden" value="" id="vendor_search_id">
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="btn-outer">
                                                <button class="blue-btn btn-block" id="searchBillTable">Search</button>
                                            </div>
                                        </div>
                                        <div class="form-outer">
                                            <div class="col-sm-6 col-md-4">
                                                <label>Select Vendor</label>
                                                <input type="text" class="form-control add-input vendorSelect" id="vendorSelect" placeholder=" Click here to select">
                                                <a class="add-icon addVendorModal pull-right" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <label>Vendor Address</label>
                                            <textarea class="form-control" rows="6" id="vendorAddress"></textarea>
                                        </div>
                                        <div class="col-md-2 col-sm-6" id="editVendorDiv" style="display: none;">
                                            <button id="EditVendorAddress" class="blue-btn btn-block" type="button">Edit Vendor Address</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table id="paybills_table" class="table table-hover table-dark">
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <form id="paymentForm">
                                                    <input type="hidden" name="vendor_id" id="userData" value="">
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-md-2 col-sm-6">
                                                                <label>Date</label>
                                                                <input type="text" name="date" class="form-control" id="selectDate" readonly placeholder="Select Date">
                                                            </div>
                                                            <div class="col-md-2 col-sm-6" id="bankAccount" style="display: none;">
                                                                <label>Bank <em class="red-star">*</em></label>
                                                                <select name="bank" id="bankName" class="form-control">
                                                                    <option value="">Select Bank</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2 col-sm-6">
                                                                <label>Payment Type <em class="red-star">*</em></label>
                                                                <select id="payment_type" name="payment_type" class="form-control">
                                                                    <option value="">Select</option>
                                                                    <option value="Check">Check</option>
                                                                    <option value="Cash">Cash</option>
                                                                    <option value="Credit">Credit Card/Debit Card</option>
                                                                    <option value="MoneyOrder">Money Order</option>
<!--                                                                    <option value="ACH">ACH</option>-->
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2 col-sm-6" id="checkNumber" style="display: none;">
                                                                <label>Check Number <em class="red-star">*</em></label>
                                                                <input name="check_number" type="text" class="form-control number_only" >
                                                            </div>
                                                            <div class="col-md-2 col-sm-6">
                                                                <label>Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                                <input id="checkoutAmount" name="amount" type="text" class="form-control" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="btm-outer">
                                                        <button type="submit" id="paySelectedBill" class="blue-btn">Pay Selected Bill</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>
                        <!-- Sub Tabs Starts-->
                        <!-- Sub tabs ends-->
                    </div>
                </div>
            </div>
            <!--Tabs Ends -->
        </div>
</div>

    <div id="addVendorModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Vendor</h4>
                </div>
                <div class="modal-body">
                    <form id="add_vendor_form">
                        <div class="row">
                            <div class="form-outer">
                                <div class="col-sm-3">
                                    <label>Vendor ID <em class="red-star">*</em></label>
                                    <input class="form-control vendor_random_id" type="text" value="" readonly  maxlength="6" name="vendor_random_id"/>
                                </div>
                                <div class="col-sm-3">
                                    <label>Salutation</label>
                                    <select class="form-control" id="salutation" name="salutation">
                                        <option value="">Select</option>
                                        <?php foreach ($satulation_array as $key => $value) { ?>
                                            <option value="<?= $key ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label>First Name</label>
                                    <input class="form-control capital" type="text" maxlength="20" name="first_name" id="first_name" placeholder="First Name"/>
                                </div>
                                <div class="col-sm-3">
                                    <label>Middle Name</label>
                                    <input class="form-control capital" type="text" maxlength="50" name="middle_name" placeholder="Middle Name"/>
                                </div>
                                <div class="col-sm-3">
                                    <label>Last Name</label>
                                    <input class="form-control capital" type="text" maxlength="20" name="last_name" placeholder="Last Name" id="last_name"/>
                                </div>
                                <div class="col-sm-3 hidemaiden" style="display: none;">
                                    <label>Maiden Name</label>
                                    <input class="form-control capital" type="text" maxlength="20" name="maiden_name" placeholder="maiden Name" id="maiden_name"/>
                                </div>
                                <div class="col-sm-3">
                                    <label>Gender</label>
                                    <select id="gender" name="gender" class="form-control">
                                        <option value="">Select</option>
                                        <?php foreach ($gender_array as $key => $value) { ?>
                                            <option value="<?= $key ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label>Entity/Company Name</label>
                                    <input class="form-control capital company_name" type="text" placeholder="Eg: Apexlink" maxlength="50" name="company_name"/>
                                </div>
                                <div class="col-sm-3">
                                    <label>Phone</label>
                                    <input class="form-control phone_number_format" maxlength="12" placeholder="123-456-7890" type="text" name="phone_number" id="phone_number">
                                </div>
                                <div class="col-sm-3">
                                    <label>Zip / Postal Code</label>
                                    <input name="zipcode" maxlength="9" placeholder="Eg: 10001" id="zip_code" class="form-control capital" type="text" />
                                </div>
                                <div class="col-sm-3">
                                    <label>Country</label>
                                    <input name="country" maxlength="50" placeholder="Eg: US" id="country" class="form-control capital" type="text" />
                                </div>
                                <div class="col-sm-3">
                                    <label>State / Province </label>
                                    <input name="state" id="state" maxlength="100" placeholder="Eg: AL"  class="form-control capital states" type="text" />
                                </div>
                                <div class="col-sm-3">
                                    <label>City</label>
                                    <input name="city" maxlength="100" placeholder="Eg: Huntsville"  class="form-control capital citys" type="text" />
                                </div>
                                <div class="col-sm-3">
                                    <label>Address 1</label>
                                    <input name="address1" placeholder="Eg: Street Address 1" maxlength="200" id="vAddress1" class="capital form-control address_field" type="text" />
                                </div>
                                <div class="col-sm-3">
                                    <label>Address 2</label>
                                    <input name="address2" placeholder="Eg: Street Address 2" maxlength="200" id="vAddress2" class="form-control capital address_field" type="text" />
                                </div>
                                <div class="col-sm-3">
                                    <label>Address 3</label>
                                    <input name="address3" placeholder="Eg: Street Address 3" maxlength="200" id="vAddress3" class="form-control capital address_field" type="text" />
                                </div>
                                <div class="col-sm-3">
                                    <label>Email</label>
                                    <input class="form-control" type="text" name="email" placeholder="Eg: user@domain.com">
                                </div>
                                <div class="col-sm-3">
                                    <label>Vendor Type <a class="pop-add-icon vendortypeplus" onclick="clearPopUps('#Newvendortype')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                    <select class="form-control" id='vendor_type_options' name='vendor_type_id'></select>
                                    <div class="add-popup" id='Newvendortype' style="width:120%;">
                                        <h4>Add Vendor Type</h4>
                                        <div class="add-popup-body">

                                            <div class="form-outer">
                                                <div class="col-sm-12">
                                                    <label>Vendor Type<em class="red-star">*</em></label>
                                                    <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20" name='@vendor_type' id='vendor_type' placeholder="Eg: PMB"/>
                                                    <span class="customError required"></span>
                                                </div>
                                                <div class="col-sm-12">
                                                    <label> Description <em class="red-star">*</em></label>
                                                    <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="50" name='@description' id='vendor_description' placeholder="Eg: Plumber"/>
                                                    <span class="customError required"></span>
                                                </div>
                                                <div class="btn-outer text-right">
                                                    <input type="button"  id='NewvendortypeSave' class="blue-btn" value="Save" />
                                                    <button type="button"  class="clear-btn clearFormReset" id="clearNewvendortype">Clear</button>
                                                    <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label>Vendor Rate <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> </label>
                                    <input class="form-control amount number_only" type="text" id="vendor_rate" name="vendor_rate" placeholder="Vendor Rate"/>
                                    <span class='add-icon-span'>/hr </span>
                                </div>
                                <div class="col-sm-3">
                                    <label>Referral Source <a class="pop-add-icon additionalReferralResource" href="javascript:;" onclick="clearPopUps('#additionalReferralResource1')"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                    <select name="additional_referralSource" id="additional_referralSource" class="form-control"><option>Select</option></select>
                                    <div class="add-popup" id="additionalReferralResource1" style="width:120%;">
                                        <h4>Add New Referral Source</h4>
                                        <div class="add-popup-body">
                                            <div class="form-outer">
                                                <div class="col-sm-12">
                                                    <label>New Referral Source <em class="red-star">*</em></label>
                                                    <input class="form-control reff_source1 customReferralSourceValidation capital" data_required="true" type="text" placeholder="New Referral Source">
                                                    <span class="customError required"></span>
                                                    <span class="red-star" id="reff_source1"></span>
                                                </div>
                                                <div class="btn-outer text-right">
                                                    <button type="button" class="blue-btn add_single1" data-validation-class="customReferralSourceValidation" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source1" data-name="additional_referralSource">Save</button>
                                                    <button type="button"  class="clear-btn clearFormReset" id="clearadditionalReferralResource1">Clear</button>
                                                    <input type="button" class="grey-btn cancelPopup" value="Cancel">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="blue-btn add_vendor_save_update" >Save</button>
                            <button type="button"  class="clear-btn clearFormReset" id="clearAdd_vendor_form">Clear</button>
                            <button type="button" class="grey-btn cancel_add_vendor" >Cancel</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
    <!--Modal Popup Ends-->

    <!--Vendor Address Popup Start-->
    <div id="updateVendorAddress" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Vendor Address</h4>
                </div>
                <div class="modal-body">
                    <form id="update_vendor_address_form">
                        <input type="hidden" name="id" id="vendor_address_id" >
                        <div class="row">
                            <div class="form-outer">
                                <div class="col-sm-4">
                                    <label>Address 1</label>
                                    <input name="address1" placeholder="Eg: Street Address 1" maxlength="200" id="vendorAddress1" class="capital form-control address_field" type="text" />
                                </div>
                                <div class="col-sm-4">
                                    <label>Address 2</label>
                                    <input name="address2" placeholder="Eg: Street Address 2" maxlength="200" id="vendorAddress2" class="form-control capital address_field" type="text" />
                                </div>
                                <div class="col-sm-4">
                                    <label>Address 3</label>
                                    <input name="address3" placeholder="Eg: Street Address 3" maxlength="200" id="vendorAddress13" class="form-control capital address_field" type="text" />
                                </div>
                                <div class="col-sm-4">
                                    <label>Zip / Postal Code</label>
                                    <input name="zipcode" maxlength="9" placeholder="Eg: 10001" id="vendor_zip_code" class="form-control capital" type="text" />
                                </div>
                                <div class="col-sm-4">
                                    <label>Country</label>
                                    <input name="country" maxlength="50" placeholder="Eg: US" id="vendor_country" class="form-control capital" type="text" />
                                </div>
                                <div class="col-sm-4">
                                    <label>State / Province </label>
                                    <input name="state" id="vendor_state" maxlength="100" placeholder="Eg: AL"  class="form-control capital states" type="text" />
                                </div>
                                <div class="col-sm-4">
                                    <label>City</label>
                                    <input name="city" maxlength="100" placeholder="Eg: Huntsville" id="vendor_city"  class="form-control capital citys" type="text" />
                                </div>
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="blue-btn vendor_address_update" >Update</button>
                            <button type="button" class="grey-btn cancel_address_update" >Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--Vendor Address Popup Ends-->
    <!-- Stripe Checkout form Start-->
    <div class="modal fade" id="payNow" role="dialog">
        Amount to paid - <div class='amountPaid'></div>
        <div class="modal-header">

            <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
            <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
            </a>

        </div>

        <form action="/charge" method="post" id="payment-form">
            <div class="row payment-form-label">
                <div class="col-sm-12">
                    <label>Amount to be paid -    <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$'; ?> <span currecy_padding"><?php echo $default_symbol ?></span> <span class='paidAmount currecy_padding'></span> </label>
                </div>
            </div>
            <input type='hidden' class='days_remaining'>
            <input type='hidden' class='plan_id'>
            <input type='hidden' class='days_remaining'>
            <input type='hidden' class='plan_id'>
            <div class="form-row">
                <label for="card-element" class="card-payment-text">
                    Credit / Debit card
                </label>
                <div id="card-element">
                    <!-- A Stripe Element will be inserted here. -->
                </div>

                <!-- Used to display form errors. -->
                <div id="card-errors" role="alert"></div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <img src="<?php echo COMPANY_SUBDOMAIN_URL;?><?php echo !empty($_SESSION[SESSION_DOMAIN]['company_logo'])? $_SESSION[SESSION_DOMAIN]['company_logo'] :  '/images/logo.png' ?>"  class="payment-method-logo">
                </div>
                <div class="col-sm-6">
                    <button class="blue-btn submit_payment_class pull-right payment-submit-button">Submit Payment</button>
                </div>
            </div>
        </form>
    </div>
    <!-- Stripe Checkout form End-->
    <div class="overlay">
        <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
            <img width="200"  height="200" src='<?php echo COMPANY_SUBDOMAIN_URL ?>/images/loading.gif'/>
        </div>
    </div>

<!-- Wrapper Ends -->
<script>
    var pagination  = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url  = "<?php echo SITE_URL; ?>";
    var login_user_name =  "<?php echo $_SESSION[SESSION_DOMAIN]['name']; ?>";
    var amount_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var add_new_vendor = '';

    $('#accounting_top').addClass('active');
    $(document).ready(function(){
        var stripe = Stripe('pk_test_jF5tXlX0qLcCR1YAX9WB8FaL');

        // Create an instance of Elements.
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {style: style});

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
            $(".submit_payment_class").attr("disabled",true)
            event.preventDefault();

            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    var token_id = result.token.id;
                    stripeTokenHandler(token_id);
                }
            });
        });

    });

    $(document).on('click','#clearAdd_vendor_form',function(){
        resetFormClear('#add_vendor_form',['vendor_random_id'],'form',false);
    });

    $(document).on('click','#clearNewvendortype',function(){
        resetFormClear('#Newvendortype',[],'div',false);
    });

    $(document).on('click','#clearadditionalReferralResource1',function(){
        resetFormClear('#additionalReferralResource1',[],'div',false);
    });
</script>
<script src="https://js.stripe.com/v3/"></script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.easyui.min.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/commonPopup.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/additionalMethods.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/accountingModule/addVendor.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accountingModule/payBills/addVendor.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accountingModule/payBills/payBillsListing.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accountingModule/payBills/payBills.js"></script>
<link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.easyui.css" rel="stylesheet">

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>