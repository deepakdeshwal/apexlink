<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting &gt;&gt; <span>Payment Plan </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/accounting/layout/right-nav.php");?>
                    <!--- Right Quick Links ---->

                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Accounting/Accounting">Receivables</a></li>
                            <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                            <li role="presentation"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                            <li role="presentation"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                            <li role="presentation"><a href="/Accounting/BankRegister">Banking</a></li>
                            <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                            <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                            <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconcilation</a></li>

                            <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                            <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                            <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                            <li role="presentation" class="active"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content" id="plan_listing">
                            <div class="panel-heading">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="receivables">
                                        <div class="property-status">
                                            <div class="row pull-right col-sm-2" style="margin-left: 10px;">
                                                    <div class="btn-outer col-sm-6">
                                                        <button style="display: inline" class="blue-btn" id="new_plan">Add Plan</button>
                                                    </div>

                                            </div>
                                        </div>

                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body pad-none">
                                                                    <div class="accordion-grid">
                                                                        <div class="panel-group" id="accordion">
                                                                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <div class="apx-table listinggridDiv">
                                                                                            <div class="table-responsive">
                                                                                                <table id="payment_plan" class="table table-bordered"></table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Regular Rent Ends -->

                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>



                        <div class="tab-content" id="add_plan" style="display: none;">
                            <div role="tabpanel" class="tab-pane active">
                                <div class="sub-tabss sub-navs">
                                    <div role="tabpanel" class="tab-pane active" id="add_info">
                                        <div class="form-hdr">
                                            <h3>
                                                <strong class="left">Add Payment Plan</strong>
                                                <a herf="/MultiPay/PaymentPlan" class="back goback_func pull-right" style="cursor: pointer"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                            </h3>
                                        </div>
                                        <div class="form-outer">
                                            <form id="paymen_plan_submit">
                                                <div class="form-data">
                                                    <div class="row">
                                                            <input class="form-control id" type="hidden" name="user_id" id="user_id">
                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <label>Payor Type <em class="red-star">*</em></label>
                                                                <select class="form-control payor_type" name="payor_type">
                                                                    <option value="" selected>Select</option>
                                                                    <option value="1" >Tenant</option>
                                                                    <option value="2" >Owner</option>
                                                                    <option value="3" >Vendor</option>
                                                                    <option value="4" >Employee</option>
                                                                    <option value="5" >Other</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3 col-md-3 new_input">
                                                                <label>Payor Name<em class="red-star">*</em></label>
                                                                <select class="form-control payor_name" name="payor_name">
                                                                    <option value="" selected>Select</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <label>Principal Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>) <em class="red-star">*</em></label>
                                                               <div class="pre-span principal_amt_div">
                                                                <input class="form-control principal_amt principal_amount" type="number" name="principal_amt">
                                                                <span class="pre-span-text" style="display: none;"><?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?></span>
                                                               </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <label>Interest rate (%)<em class="red-star">*</em></label>
                                                                <input class="form-control interest_rate interest" type="number" name="interest_rate">
                                                            </div>

                                                            <div class="col-xs-12 col-sm-3 col-md-3 clear">
                                                                <label>Number Of Installments<em class="red-star">*</em></label>
                                                                <select class="form-control installments" name="installments">
                                                                    <option value="" selected>Select</option></select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <label>Amount Due (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>)<em class="red-star">*</em></label>
                                                                <div class="pre-span amount_due_div">
                                                                <input class="form-control amount_due" type="text" name="amount_due" readonly>
                                                                    <span class="pre-span-text" style="display: none;"><?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <label>Payment Frequency<em class="red-star">*</em></label>
                                                                <select class="form-control frequency" name="frequency">
                                                                    <option value="1" >Weekly</option>
                                                                    <option value="2" >Bi-Weekly</option>
                                                                    <option value="3" >Monthly</option>
                                                                    <option value="4" >Annually</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <label>Start Date<em class="red-star">*</em> </label>
                                                                <input class="form-control add-input calendar start_date" name="start_date" type="text">
                                                                <input class="form-control add-input calendar all_dates" name="all_dates" type="hidden">
                                                            </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3 clear">
                                                            <label>Next Payment Date </label>
                                                            <input class="form-control add-input next_date" name="next_date" type="text" readonly="readonly">
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3">
                                                            <label>Next Payment Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>)</label>
                                                            <div class="pre-span next_amt_div">
                                                            <input class="form-control add-input next_amt next_payment_amount" name="next_amt" type="text" readonly="readonly">
                                                                <span class="pre-span-text" style="display: none;"><?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3">
                                                            <label>Status<em class="red-star">*</em> </label>
                                                            <select class="form-control status" name="status" readonly="readonly">
                                                                <option value="1" selected>Active</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="btn-outer text-right">
                                                        <a class="blue-btn" type="button" id="update_geninfo" style="cursor: pointer;">Save</a>
                                                        <a class="clear-btn clearFormReset" type="button" style="cursor: pointer;">Clear</a>
                                                        <a class="grey-btn " type="button" id="cancel_genifo" style="cursor: pointer;">Cancel</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div></div></div>


                        <div class="tab-content" id="update_plan" style="display: none;">
                            <div role="tabpanel" class="tab-pane active">
                                <div class="sub-tabss sub-navs">
                                    <div role="tabpanel" class="tab-pane active" id="add_info">
                                        <div class="form-hdr">
                                            <h3>
                                                <strong class="left">Add Payment Plan</strong>
                                                <a herf="" class="back goback_func pull-right" style="cursor: pointer"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                            </h3>
                                        </div>
                                        <div class="form-outer">
                                            <form id="payment_plan_submit_edit">
                                                <div class="form-data">
                                                    <div class="row">
                                                        <input class="form-control id" type="hidden" name="user_id" id="user_id">
                                                        <div class="col-xs-12 col-sm-3 col-md-3">
                                                            <label>Payor Type <em class="red-star">*</em></label>
                                                            <select class="form-control payor_typee" name="payor_type">
                                                                <option value="" selected>Select</option>
                                                                <option value="1" >Tenant</option>
                                                                <option value="2" >Owner</option>
                                                                <option value="3" >Vendor</option>
                                                                <option value="4" >Employee</option>
                                                                <option value="5" >Other</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3 new_inputt">
                                                            <label>Payor Name<em class="red-star">*</em></label>
                                                            <select class="form-control payor_namee" name="payor_namee">
                                                                <option value="" selected>Select</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3">
                                                            <label>Principal Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>) <em class="red-star">*</em></label>
                                                            <div class="pre-span">
                                                            <input class="form-control principal_amount" type="number" name="principal_amt" id="principal_amt">
                                                                <span class="pre-span-text" style="display: none;">$</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3">
                                                            <label>Interest rate (%)<em class="red-star">*</em></label>
                                                            <input class="form-control  interest" type="number" name="interest_rate" id="interest_rate">
                                                        </div>

                                                        <div class="col-xs-12 col-sm-3 col-md-3 clear">
                                                            <label>Number Of Installments<em class="red-star">*</em></label>
                                                            <select class="form-control" name="installments" id="installments">
                                                                <option value="" selected>Select</option></select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3">
                                                            <label>Amount Due (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>)<em class="red-star">*</em></label>
                                                            <div class="pre-span">
                                                            <input class="form-control" type="text" name="amount_due" id="amount_due" readonly>
                                                                <span class="pre-span-text" style="display: none;">$</span>
                                                            </div>
                                                            </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3">
                                                            <label>Payment Frequency<em class="red-star">*</em></label>
                                                            <select class="form-control" name="frequency" id="frequency">
                                                                <option value="1" >Weekly</option>
                                                                <option value="2" >Bi-Weekly</option>
                                                                <option value="3" >Monthly</option>
                                                                <option value="4" >Annually</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3">
                                                            <label>Start Date<em class="red-star">*</em> </label>
                                                            <input class="form-control add-input calendar" name="start_date" id="start_date" type="text">
                                                            <input class="form-control add-input calendar" name="all_dates" id="all_dates" type="hidden">
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3 clear">
                                                            <label>Next Payment Date </label>
                                                            <input class="form-control add-input" name="next_date" type="text" id="next_date" readonly="readonly">
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3">
                                                            <label>Next Payment Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>)</label>
                                                            <div class="pre-span">
                                                            <input class="form-control add-input  next_payment_amount" name="next_amt" type="text" id="next_amt" readonly="readonly">
                                                                <span class="pre-span-text" style="display: none;">$</span>
                                                            </div>
                                                                <input class="form-control" type="hidden" name="" id="all_amt">
                                                            <input class="form-control" type="hidden" name="" id="inst_amt">
                                                            <input class="form-control" type="hidden" name="" id="list_amt">
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3">
                                                            <label>Status<em class="red-star">*</em> </label>
                                                            <select class="form-control status" name="status" readonly="readonly">
                                                                <option value="1" selected>Active</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="btn-outer text-right">
                                                        <a class="blue-btn update_geninfoo" type="button" id="" style="cursor: pointer;">Update</a>
                                                        <a class="clear-btn formreset" type="button" style="cursor: pointer;">Reset</a>
                                                        <a class="grey-btn cancel_genifo" type="button" id="" style="cursor: pointer;">Cancel</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div></div></div>


                        <div id="payment_recieved" class="modal fade in" role="dialog"">
                        <div class="modal-dialog spotlight-pop">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                    <h4 class="modal-title">Payment Received</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="grid-outer">
                                        <div class="table-responsive">
                                            <form id="inst_submit">
                                            <table class="table table-hover table-dark" id="payment-table">
                                                <thead>
                                                <tr>
                                                    <th scope="col"></th>
                                                    <th scope="col">Installment Number</th>
                                                    <th scope="col">Installment Date</th>
                                                    <th scope="col">Installment Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>)</th>
                                                    <th scope="col">Is Paid</th>
                                                    <th scope="col">Extra Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>)</th>
                                                    <th scope="col">Balance Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>)</th>

                                                </tr>
                                                </thead>
                                                <tbody id="payment_list">
<!--                                                    <td><input type="checkbox"/></td>-->
<!--                                                    <td>1</td>-->
<!--                                                    <td>Jan 12, 2019 (Sat.)</td>-->
<!--                                                    <td>1100.00</td>-->
<!--                                                    <td>Yes</td>-->
<!--                                                    <td>0.00</td>-->
<!--                                                    <td>0.00</td>-->

                                                </tbody>
                                            </table>
                                            </form>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="btn-outer col-sm-12 text-center">
                                            <input class="form-control add-input all_ids" type="hidden">
                                            <button type="button" class="blue-btn"  id="save_installment">Save</button>
                                            <button type="button" class="grey-btn" data-dismiss="modal" id="cancel_install">Cancel</button>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>

                    </div>


<!---->
<!--                        <div class="modal fade" id="print_receipt" role="dialog">-->
<!--                            <div class="modal-dialog modal-md">-->
<!--                                <div class="modal-content" style="width: 100%;">-->
<!--                                    <div class="modal-header">-->
<!--                                        <button type="button" class="blue-btn" id='print_complaints'  onclick="printreceipt('#fetch_table_data')" onclick="window.print();">Print</button>-->
<!--                                        <button type="button" class="blue-btn" id='email_complaint' onclick="sendreceiptEmail('#fetch_table_data')">Email</button>-->
<!--                                        <button type="button" class="blue-btn" id='sms_complaints'  onclick="sendreceiptmessage('#fetch_table_data')" onclick="window.print();">SMS</button>-->
<!---->
<!--                                        <button type="button" class="close" data-dismiss="modal">×</button>-->
<!---->
<!--                                    </div>-->
<!--                                    <input type="hidden" id="email">-->
<!--                                    <div class="table-responsive" id="fetch_table_data">-->
<!---->
<!--                                            <table class="table table-hover table-dark" id="payment-table-receipt">-->
<!--                                                <thead>-->
<!--                                                <tr>-->
<!--                                                    <th>Installment Number</th>-->
<!--                                                    <th>Installment Date</th>-->
<!--                                                    <th>Installment Amount (--><?php //echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?><!--)</th>-->
<!--                                                    <th>Installment frequency</th>-->
<!--                                                    <th>Extra Amount (--><?php //echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?><!--)</th>-->
<!--                                                    <th>Interest Amount (--><?php //echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?><!--)</th>-->
<!---->
<!--                                                </tr>-->
<!--                                                </thead>-->
<!--                                                <tbody id="payment_receipt">-->
<!---->
<!--                                                </tbody>-->
<!--                                            </table>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->


                    <!-- dd-->
                    <div id="receipt-pop" class="modal fade in" role="dialog">
                        <div class="modal-dialog spotlight-pop">

                            <!-- Modal content-->
                            <div class="modal-content pull-left">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                    <h4 class="modal-title">
                                        <a href="javascript:;" class="blue-btn" onclick="printreceipt('#fetch_table_data')" onclick="window.print();">Print</a>
                                        <a href="javascript:;" class="blue-btn" onclick="sendreceiptEmail('#fetch_table_data')">Email</a>
                                        <a href="javascript:;" class="blue-btn" onclick="sendreceiptmessage('#fetch_table_data')">SMS</a>
                                    </h4>
                                </div>
                                <div class="modal-body" id="fetch_table_data">
                                    <div class="payment-plan">
                                        <div class="payment-plan-hdr">
                                            <img alt="Apexlink" src="<?php echo SITE_URL.'company/images/logo.png' ?>"/>
                                        </div>
                                        <div class="payment-plan-body">
                                            <div id="name_dis"></div>
                                            <div class="grid-outer">
                                                <input type="hidden" id="email">
                                                <div class="table-responsive" ">
                                                    <table class="table table-hover table-dark" id="payment-table-receipt">
                                                        <thead>
                                                        <tr>
                                                            <th>Installment Number</th>
                                                            <th>Installment Date</th>
                                                            <th>Installment Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>)</th>
                                                            <th>Installment Frequency</th>
                                                            <th>Extra Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>)</th>
                                                            <th>Interest Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>)</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="payment_receipt">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            </div>

                        </div>

                    </div>

                    <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
</div>
<!-- Wrapper Ends -->


<!-- Footer Ends -->
<script>var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";</script>
<script>  var defaultFormData = '';</script>

<script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/accounting/payment_plan/payment_plan.js"></script>
<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });
    $(".clearFormReset").on("mouseover", function () {
        $(this).css('text-decoration','none');$(this).css('color','#fffffff')
    });
    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    $('.calendar').datepicker({
        minDate: "dateToday",
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    $(".calendar").val(date);
    function printreceipt(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data)
    {
        var base_url = window.location.origin;
        var mywindow = window.open('', 'my div');
        $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
            'font-weight:bold;} .right-detail{\n' +
            '        position:relative;\n' +
            '        left:+400px;\n' +
            '    }</style>');
        $(mywindow.document.body).html( '<body>' + data + '</body>');
        mywindow.document.close();
        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
        if(mywindow.close()){

        }
        //   $("#PrintEnvelope").modal('hide');
        return true;
    }


    function sendreceiptEmail(elem)
    {
        var email = $('#email').val();
        SendMail($(elem).html(),email);
    }


    function SendMail(data, email){
        exppdf();
        var url=window.location.origin;
        $.ajax({
            type: 'post',
            url: '/paymentplan-ajax',
            data: {
                class: "paymentplan",
                action: "sendemail",
                data_html: data,
                email : email,
                url:url
            },
            success : function(response){
                var response =  JSON.parse(response);
                if(response.status == 'success' && response.code == 200) {
                    toastr.success('Mail sent successfully.');
                }else {
                    toastr.warning('Mail not send due to technical issue.');
                }
            }
        });
    }

    function exppdf() {
        var htmls=$("#fetch_table_data").html();
        console.log('htmls',htmls);
        $.ajax({
            type: 'post',
            url: '/paymentplan-ajax',
            data: {
                class: "paymentplan",
                action: "getPdfContent",
                htmls: htmls
            },
            success: function (res) {
                var res= JSON.parse(res);
                console.log(res.data);
                if (res.code == 200) {
                    // var link=document.createElement('a');
                    // document.body.appendChild(link);
                    // link.target="_blank";
                    // link.download="Receipt.pdf";
                    // link.href=res.data.record;
                    // link.click();
                } else if(res.code == 500) {
                    //toastr.warning(response.message);
                } else {
                    //toastr.error(response.message);
                }
            }
        });
    }


    $(document).on('click','.clearFormReset',function () {
        resetFormClear("#paymen_plan_submit",['start_date','status'],'form',false);
        $(".pre-span-text").hide();
    });

    $(document).on('click','.formreset',function () {
        resetEditForm("#payment_plan_submit_edit",['payor_namee'],true,defaultFormData,[]);
    });
</script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
