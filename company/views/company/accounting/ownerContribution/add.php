<?php
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
$_SESSION[SESSION_DOMAIN]['is_portal']='';
?>

<div id="wrapper">
    <!-- Top navigation start -->

    <?php

    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");

    ?>
    <!-- Top navigation end -->
    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <div class="breadcrumb-outer">
                                    Owner >> <span>Owner Contributions</span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent">
                        <form id="addOwnerContributionFormId" enctype='multipart/form-data'>

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>
                                        Owner Contribution
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Date <em class="red-star">*</em></label>
                                            <input class="form-control capital" name="contribution_date" readonly id="contribution_date" type="text" />
                                            <span class="contribution_dateErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3">
                                            <label> Property Owner <em class="red-star">*</em> </label>
                                            <input placeholder="Click here to pick a owner" class="form-control add-input" type="text" id="owner_id" name="owner_id">
                                            <input class="form-control" type="hidden" id="selected_owner_id" name="selected_owner_id" >
                                            <label id="owner_id-error" class="error" for="owner_id"></label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label> Portfolio <em class="red-star">*</em></label>
                                            <select class="form-control" name="portfolio_id" id="portfolio_id">
                                                <option value="">Select Portfolio</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Property <em class="red-star">*</em></label>
                                            <select class="form-control" name="property_id" id="property_id">
                                                <option value="">Select Property</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Bank Account </label>
                                            <select class="form-control" id="bank_account"  name="bank_account">
                                                <option value="">Select Bank</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>COA <em class="red-star">*</em></label></label>
                                            <select class="form-control" id="chart_of_account_id"  name="chart_of_account_id"></select>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Total Amount <em class="red-star">*</em></label></label>
                                            <input class="form-control number_only money" name="total_amount" maxlength="50" id="total_amount" type="text" />
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Transaction Type <em class="red-star">*</em></label> </label>
                                            <select class="form-control" id="transaction_type"  name="transaction_type">
                                                <option value="">Select</option>
                                                <option selected value="Check">Check</option>
                                                <option value="Cash">Cash</option>
                                                <option value="Credit Card/Debit Card">Credit Card/Debit Card</option>
                                                <option value="ACH">ACH</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Remarks</label>
                                            <textarea class="form-control capital" maxlength="500" rows="3" id="remarks" name="remarks"></textarea>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="check_div">
                                                <label>Check # <em class="red-star">*</em></label>
                                                <input class="form-control capital" name="check_number" maxlength="20"  id="check_number" type="text" />
                                            </div>
                                            <div class="cash_div" style="display: none">
                                                <label>Ref # </label>
                                                <input class="form-control capital" name="reference_number"  maxlength="20"  id="reference_number" type="text" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3 mt-20 stripe_charge_div" style="display: none">
                                            <div class="stripe_charge_card_div" >
                                                <label>Stripe Charges(2.9% + 30¢) :<span class="stripe_card_charge">0.3</span></label>
                                                <label>Total :<span class="stripe_card_total">0.3</span></label>
                                            </div>
                                            <div class="stripe_charge_ach_div" >
                                                <label>Stripe Charges(0.80 %) :<span class="stripe_ach_charge">0</span></label>
                                                <label>Total :<span class="stripe_ach_total">0</span></label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-20">
                                        <div class="form-outer2 ach_div" style="display: none">
                                            <div class="detail-outer">
                                                <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Routing Number : </label>
                                                        <span class="routing_number"></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Account Type :</label>
                                                        <span class="account_holder_type"> </span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Account Holder Name : </label>
                                                        <span class="account_holder_name"></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Account Number : </label>
                                                        <span class="ach_account_number"></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Bank Name : </label>
                                                        <span class="bank_name"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-outer2 card_div" style="display: none">
                                            <div class="detail-outer">
                                                <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Card Type : </label>
                                                        <!--                                                        <img src="/company/images/card1.png" style="width: 40px; margin-left: 5px">-->
                                                        <span class="card_type"></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Exp Month :</label>
                                                        <span class="exp_month"> </span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">CVC : </label>
                                                        <span class="cvc_number"></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Card Number : </label>
                                                        <span class="card_number"></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Exp Year : </label>
                                                        <span class="exp_year"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="btn-outer text-right">
                                <input name="owner_customer_id" id="owner_customer_id" type="hidden">
                                <button type="button" class="blue-btn" id="ownerContributionSaveBtn">Save</button>
                                <button type="button"  class="clear-btn clearOwnerContribute">Clear</button>
                                <button type="button" id="cancel_add_owner_contribution_btn" class="grey-btn">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<!-- Wrapper Ends -->


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->
<script>
    var last_url="<?php echo $_SERVER['HTTP_REFERER']?>";
    if(last_url.includes("/Owner/OwnerPayments/OwnerContribution")==false || last_url.includes("GenerateOwnerContributionPortal")==false){
        console.log('aaaaaaaa');
        localStorage.removeItem('redirect_owner_id_portal');
    }
</script>
<script type="text/javascript">
    $('#accounting_top').addClass('active');
    var upload_url = "<?php echo SITE_URL; ?>";
    var jsDateFormat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/ownerContribution/ownerContribution.js"></script>
<link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.easyui.css" rel="stylesheet">
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.easyui.min.js" type="text/javascript"></script>

<script>

    function goBack() {
        window.history.back();
    }
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });
    <!--- Main Nav Responsive -->
    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });


</script>
<!-- Jquery Ends -->
</body>

</html>