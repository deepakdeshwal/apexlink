<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div class="popup-bg"></div>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$';
    ?>
    <!-- Top navigation end -->
    <style>
        .ui-datepicker table,  .ui-datepicker-current{
            display: none;
        }
    </style>
    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <div class="breadcrumb-outer">
                                    Accounting >> <span>Process Management Fee</span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent">
                        <!--- Right Quick Links ---->
                        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/accounting/layout/right-nav.php");?>
                        <!--- Right Quick Links ---->

                        <form id="addOwner" enctype='multipart/form-data'>
                            <div class="form-outer" id="list_owner_draw_div" style="display: block">
                                <div class="form-hdr">
                                    <h3>Process Management Fees</h3>
                                </div>

                                <div class="form-data">
                                    <div class="property-status mg-tp-40">
                                        <div class="row">
                                            <div class="btn-outer text-right mg-lt">
                                                <a type="button" class="blue-btn" href="/Accounting/PaidManagementFee">Paid Management Fees</a>
                                                <a type="button" class="blue-btn blue-btn-alt" href="/Accounting/RunManagementFee">Run Management Fees</a>
                                                <a type="button" class="blue-btn blue-btn-alt" href="/Accounting/ProcessManagementFee">Set Management Fees</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 outer-border">
                                        <div class="col-sm-3 mg-tp-20">
                                            <input class="form-control" type="text"  name="process_management_fee_date" id="process_management_fee_date">
                                        </div>
                                        <div class="col-sm-3 mg-tp-20">
                                            <select class="form-control" name="property_id" id="property_id" multiple></select>
                                        </div>
                                        <div class="col-sm-6 mg-tp-20 total_management_fee_span">
                                            <span>Process Management Fees : </span>
                                            <span class=""><?php echo $default_symbol?></span><span class="total_management_fee_amount">AFN 3,890.00</span>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="grid-outer mg-btm-20" style="">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-dark" id="">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col" style="width: 50%;">Property Name</th>
                                                            <th scope="col" style="width: 50%;">Process Management Fee (<?php echo $default_symbol ?>)</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="" id="properties_listing">
                                                        <td align="center" colspan="2">
                                                            <span id="edit_frequency"> No records found</span>
                                                        </td>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<!-- Wrapper Ends -->



<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->

<script type="text/javascript">
    $('#accounting_top').addClass('active');
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker_format = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/processManagementFee/paidManagementFee.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>

<script>

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });

</script>

<!-- Jquery Ends -->
</body>

</html>