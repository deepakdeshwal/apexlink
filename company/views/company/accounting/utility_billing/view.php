<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting &gt;&gt; <span>Utility Billing </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/accounting/layout/right-nav.php");?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tab_list" role="tablist">
                            <li role="presentation"><a href="/Accounting/Accounting">Receivables</a></li>
                            <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                            <li role="presentation"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                            <li role="presentation"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                            <li role="presentation"><a href="/Accounting/BankRegister">Banking</a></li>
                            <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                            <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                            <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconciliation</a></li>
                            <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                            <li role="presentation" class="active"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                            <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                            <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse">
                                            <span></span> Utility Billing</a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse  in">
                                    <div class="panel-body">

                                        <div id="add_utility_billing_div" style="display: none">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse">
                                                            <span></span> Create New Utility Billing</a><a class="back pull-right back_button" ><< Back</a>
                                                    </h4>
                                                </div>
                                                <div class="panel-collapse collapse  in">
                                                    <div class="panel-body">
                                                        <form id="apply_utility_bill_form_id">
                                                            <div class="grid-outer" id="invoice_table" style="">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark" id="added_utility_bills_table">
                                                                        <thead>
                                                                        <tr>
                                                                            <th scope="col">Property</th>
                                                                            <th scope="col">Building</th>
                                                                            <th scope="col">Unit</th>
                                                                            <th scope="col">Tenant</th>
                                                                            <th scope="col">Utility</th>
                                                                            <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$'; ?>
                                                                            <th scope="col">Amount (<?php echo $default_symbol ?>)</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody class="utility_tbody"></tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="btn-outer text-right">
                                                                <button type="button" class="blue-btn add_new_utility_row" id="ownerSaveForm">Add</button>
                                                                <button type="button" class="blue-btn" id="apply_btn" data_tab="confirm_utility_bill_listing">Apply</button>
                                                                <button type="button" id="clear_add_utility_form" class="clear-btn">Clear</button>
                                                                <button type="button" id="cancel_add_utility_btn" class="grey-btn">Cancel</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="edit_utility_billing_div" style="display: none">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse">
                                                            <span></span> Edit Utility Billing</a>
                                                    </h4>
                                                </div>
                                                <div class="panel-collapse collapse  in">
                                                    <div class="panel-body">
                                                        <form id="edit_utility_bill_form_id">
                                                            <div class="grid-outer" id="invoice_table" style="">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark" id="">
                                                                        <thead>
                                                                        <tr>
                                                                            <th scope="col">Charges</th>
                                                                            <th scope="col">Frequency</th>
                                                                            <th scope="col">Start Date</th>
                                                                            <th scope="col">End Date</th>
                                                                            <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$'; ?>
                                                                            <th scope="col">Amount (<?php echo $default_symbol ?>)</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody class="">
                                                                        <td>
                                                                            <input type="hidden" name="utility_bill_id" id="utility_bill_id">
                                                                            <span id="edit_utility_type"></span>
                                                                        </td>
                                                                        <td>
                                                                            <span id="edit_frequency"></span>
                                                                        </td>
                                                                        <td>
                                                                            <div class="view_data">
                                                                                <span id="edit_start_date"></span>
                                                                                <a class="add-icon edit_data"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                                                            </div>
                                                                            <input style="display: none;"  class="form-control start_date calander" readonly id="start_date" name="start_date" placeholder="" type="text">
                                                                        </td>
                                                                        <td>
                                                                            <div class="view_data">
                                                                                <span id="edit_end_date"></span>
                                                                                <a class="add-icon edit_data"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                                                            </div>
                                                                            <input style="display: none;"  class="form-control end_date calander" readonly id="end_date" name="end_date" placeholder="" type="text">
                                                                        </td>
                                                                        <td>
                                                                            <div class="view_data">
                                                                                <span id="edit_amount"></span>
                                                                                <a class="add-icon edit_data"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                                                            </div>
                                                                            <input style="display: none;"  data_required="true" class="form-control add-input utility_amount" id="edit_utility_amount" name="edit_utility_amount" autocomplete="off" placeholder="" type="text">
                                                                        </td>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="btn-outer text-right">
                                                                <button type="button" class="blue-btn" id="update_btn" data_tab="update_utility_bill_listing">Update</button>
                                                                <button type="button" id="clear_edit_utility_btn" class="clear-btn">Reset</button>
                                                                <button type="button" id="cancel_edit_utility_btn" class="grey-btn">Cancel</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" id="list_utility_billing_div">
                                            <div class="property-status col-sm-12">
                                                <div class="row">
                                                    <div class="col-sm-8"></div>
                                                    <div class="col-sm-4 property-add-utility-bill">
                                                        <input class="form-control filterDateClass" readonly  type="text" id="filterDate" name="filterDate[]" placeholder="Select Date">
                                                        <input class="blue-btn" id="add_utility_billing_btn" type="button"  value="Add Utility Bill">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel-body pad-none">
                                                <div class="grid-outer">
                                                    <div class="table-responsive overflow-unset">
                                                        <div class="grid-outer">
                                                            <div class="apx-table">
                                                                <table class="table table-hover table-dark" id="utility_bills_listing">
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->
<div class="container">
    <div class="modal fade" id="utility_bills_confirm_modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Utility Charges</h4>
                </div>
                <div class="modal-body pad-none">
                    <input type="hidden" class="propertyIddata" value="">
                    <div class="panel-body" style="border-color: transparent;">
                        <div class="row">
                            <div class="form-outer">
                                <div class="panel-body pad-none">
                                    <div class="grid-outer">
                                        <div class="table-responsive overflow-unset">
                                            <div class="grid-outer">
                                                <div class="apx-table">
                                                    <table class="table table-hover table-dark" id="confirm_utility_bill_listing">
                                                    </table>
                                                    <div id="grid-pager"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-outer" style="margin-left: 15px;">
                                    <button type="button" class="blue-btn" value="Save" id="confirm_btn">Confirm</button>
                                    <button type="button" data-dismiss="modal" id="AddGeneralInformationCancel" class="propertyCancel grey-btn">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer Ends -->
<script src="<?php echo COMPANY_SITE_URL; ?>/js/autonumeric.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/accounting/utilityBilling.js"></script>
<script>
    $('#accounting_top').addClass('active');
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



</script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
</body>
</html>
