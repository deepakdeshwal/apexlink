<?php

/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <?php //include_once(COMPANY_DIRECTORY_URL . "/views/company/tenants/modals.php"); ?>
    <!-- Top navigation end -->

    <div class="popup-bg"></div>
    <input type="hidden" value="<?php echo $_GET['tenant_id']; ?>" class="tenant_id" name="tenant_id">
    <script>
        var tenant_id = $(".tenant_id").val();
        var upload_url = "<?php echo SITE_URL; ?>";
    </script>


    <!--    <div class="property_name"></div>
   <div class="address1"></div>
   <div class="address2"></div>
   <div class="address3"></div>
   <div class="address4"></div>
   <div class="building_name"></div>
   <div class="tenant_name"></div>
   <div class="tenant_phone"></div>
   <div class="lease_start"></div>
   <div class="lease_end"></div>
   <div class="security_deposite"></div>
   <div class="rent"></div> -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="breadcrumb-outer">
                                People &gt;&gt; <span>Tenant Listing</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                    <div class="right-links-outer">
                        <div class="right-links slide-toggle2"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
                        <div id="RightMenu" class="box2" style="display:none">
                            <h2>PEOPLE</h2>
                            <div class="list-group panel">
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Tenant</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Move Out</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Book Now</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Owner</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Vendor</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Employee</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Contact</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Recieve Payment</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Send Tenant Statements</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Bank Deposite</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Bill</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Tenant Instrument Register</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Vendor Instrument Register</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Tenant Transfer</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Tenant Transfer</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Move In</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Move Out</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Tenants</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Owners</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Vendors</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">NSF Listing</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">

                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#people-tenant" aria-controls="home"
                                                                      role="tab"
                                                                      data-toggle="tab">Tenants</a></li>
                            <li role="presentation"><a href="/People/Ownerlisting">Owners</a></li>
                            <li role="presentation"><a href="/Vendor/Vendor">Vendors</a></li>
                            <li role="presentation"><a href="/People/ContactListt">Contacts</a></li>
                            <li role="presentation"><a href="/People/GetEmployeeList">Employee</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="form-outer form-outer2">
                                <div class="form-hdr">

                                    <h3><span class="pull-left">Tenant Information</span>
                                        <div class="hdr-labels">
                                            <div class="column">
                                                <label>Property Name :</label>
                                                <span class="gn_property_name">Student Property</span>
                                            </div>
                                            <div class="column">
                                                <label>Building Name :</label>
                                                <span class="gn_building_name">Student Property</span>
                                            </div>
                                            <div class="column">
                                                <label>Unit # :</label>
                                                <span class="gn_unit_name">SP2</span>
                                            </div>
                                            <div class="column">
                                                <label>Tenant Name :</label>
                                                <span class="gn_tenant_name">John Doe</span>
                                            </div>
                                            <div class="column">
                                                <label>Phone :</label>
                                                <span class="gn_tenant_phone">000-0000-000</span>
                                            </div>
                                        </div>
                                        <a class="back" href="/Tenantlisting/Tenantlisting">
                                            <i class="fa fa-angle-double-left" aria-hidden="true"></i> Back
                                        </a>
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <!--                                <div class="label-auto">-->
                                    <!--                                    <div class="row">-->
                                    <!--                                        <div class="col-sm-3"><label>Property Name</label><span-->
                                    <!--                                                    class="gn_property_name">Student Property</span>-->
                                    <!--                                        </div>-->
                                    <!--                                        <div class="col-sm-3"><label>Building Name</label><span-->
                                    <!--                                                    class="gn_building_name">Student Property</span>-->
                                    <!--                                        </div>-->
                                    <!--                                        <div class="col-sm-1"><label>Unit #</label><span class="gn_unit_name">SP2</span>-->
                                    <!--                                        </div>-->
                                    <!--                                        <div class="col-sm-2"><label>Tenant Name</label><span class="gn_tenant_name">John Doe</span>-->
                                    <!--                                        </div>-->
                                    <!--                                        <div class="col-sm-2"><label>Phone</label><span-->
                                    <!--                                                    class="gn_tenant_phone">000-0000-000</span></div>-->
                                    <!--                                    </div>-->
                                    <!--                                </div>-->

                                    <div class="detail-outer">


                                        <div class="row">
                                            <div class="col-sm-4">
                                                <!--<div class="col-xs-12">
                                                    <span class="gn_tenant_name"></span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <span class="gn_address1"></span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <span class="gn_address2"></span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <span class="gn_address3"></span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <span class="gn_address4"></span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <span class="gn_city"></span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <span class="gn_state"></span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <span class="gn_country"></span>
                                                </div>-->
                                                <div class="col-xs-12 minheight">
                                                    <span class="gn_tenant_name"></span>
                                                </div>
                                                <div class="col-xs-12 minheight">
                                                    <span class="gn_address1"></span>
                                                </div>
                                                <div class="col-xs-12 minheight">
                                                    <span class="gn_address2"></span>
                                                </div>
                                                <div class="col-xs-12 minheight">
                                                    <span class="gn_address3"></span>
                                                </div>
                                                <div class="col-xs-12 minheight">
                                                    <span class="gn_address4"></span>
                                                </div>
                                                <div class="col-xs-12 minheight">
                                                    <span class="" style="width: auto;">Unit #: </span><span class="gn_unit_name"></span>
                                                </div>
                                                <div class="col-xs-12 minheight">
                                                    <span class="gn_city"></span>
                                                </div>
                                                <div class="col-xs-12 minheight">
                                                    <span class="gn_state"></span>
                                                </div>
                                                <div class="col-xs-12 minheight">
                                                    <span class="gn_country"></span>
                                                </div>

                                            </div>
                                            <div class="col-sm-4">
                                                <div class="col-xs-12">
                                                    <label class="text-right">Lease Start :</label>
                                                    <span class="gn_lease_start"></span>
                                                </div>

                                                <div class="col-xs-12">
                                                    <label class="text-right">Lease End :</label>
                                                    <span class="gn_lease_end"></span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <label class="text-right">Rent :</label>
                                                    <span class="gn_rent"></span>
                                                </div>

                                            </div>

                                            <div class="col-sm-4">

                                                <div class="col-xs-12">
                                                    <label class="text-right">Security Deposite :</label>
                                                    <span class="gn_security_deposite"></span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <label class="text-right">NSF :</label>
                                                    <span class="gn_nsf">USh60.00</span>
                                                </div>

                                                <div class="col-xs-12">
                                                    <label class="text-right">Balance Due :</label>
                                                    <span class="gn_balancedue">USh50.00</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="people-tenant">
                                <!-- Sub Tabs Starts-->
                                <div class="main-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active tenant-detail-one tenant_tab"><a
                                                href="#tenant-detail-one"
                                                aria-controls="home" role="tab"
                                                data-toggle="tab">General</a></li>
                                        <li role="presentation" class="tenant-detail-two tenant_tab" data_tab="additional_table"><a href="#tenant-detail-two"
                                                                                                                                    aria-controls="home"
                                                                                                                                    role="tab"
                                                                                                                                    data-toggle="tab">Additional
                                                Info</a>
                                        </li>
                                        <li role="presentation" class="tenant-detail-three tenant_tab"><a
                                                href="#tenant-detail-three" aria-controls="home"
                                                role="tab" data-toggle="tab">Charges</a></li>
                                        <li role="presentation" class="tenant-detail-four tenant_tab"><a href="#tenant-detail-four"
                                                                                                         aria-controls="home"
                                                                                                         role="tab"
                                                                                                         data-toggle="tab">Payment</a>
                                        </li>
                                        <li role="presentation" class="tenant-detail-five tenant_tab"><a href="#tenant-detail-five"
                                                                                                         aria-controls="home"
                                                                                                         role="tab"
                                                                                                         data-toggle="tab">Ledger</a>
                                        </li>
                                        <li role="presentation" class="tenant-detail-six tenant_tab" data_tab="occcupants_table1"><a href="#tenant-detail-six"
                                                                                                                                     aria-controls="home"
                                                                                                                                     role="tab"
                                                                                                                                     data-toggle="tab">Occupants</a>
                                        </li>
                                        <li role="presentation" class="tenant-detail-seven tenant_tab" data_tab="notes_table"><a
                                                href="#tenant-detail-seven" aria-controls="home"
                                                role="tab" data-toggle="tab">Notes and
                                                History</a></li>
                                        <li role="presentation" class="tenant-detail-eight tenant_tab" data_tab="TenantFiles-table"><a
                                                href="#tenant-detail-eight" aria-controls="home"
                                                role="tab" data-toggle="tab">File Library</a>
                                        </li>
                                        <li role="presentation" class="tenant-detail-nine tenant_tab"><a href="#tenant-detail-nine"
                                                                                                         aria-controls="home"
                                                                                                         role="tab"
                                                                                                         data-toggle="tab">Tenant
                                                Portal</a>
                                        </li>
                                        <li role="presentation" class="tenant-detail-ten tenant_tab" data_tab="renterinsurancetable"><a href="#tenant-detail-ten"
                                                                                                                                        aria-controls="home"
                                                                                                                                        role="tab"
                                                                                                                                        data-toggle="tab">Renter
                                                Insurance</a></li>
                                        <li role="presentation" class="tenant-detail-eleven tenant_tab"><a
                                                href="#tenant-detail-eleven"
                                                aria-controls="home" role="tab"
                                                data-toggle="tab">Invoice Receipt</a></li>
                                        <li role="presentation" class="rent_tab tenant-detail-twelve tenant_tab" data_tab=""><a
                                                href="#tenant-detail-twelve"
                                                aria-controls="home" role="tab"
                                                data-toggle="tab">Rent Details</a></li>
                                        <li role="presentation" class="tenant-detail-thirteen tenant_tab" data_tab="TenantComplaint-table"><a
                                                href="#tenant-detail-thirteen"
                                                aria-controls="home" role="tab"
                                                data-toggle="tab">Tenant Complaints</a></li>
                                        <li role="presentation" class="tenant-detail-forteen tenant_tab" data_tab="hoa_table"><a
                                                href="#tenant-detail-forteen"
                                                aria-controls="home" role="tab"
                                                data-toggle="tab">HOA Violation</a></li>
                                        <li role="presentation" class="tenant-detail-fifteen tenant_tab" data_tab="flaglistingtable"><a
                                                href="#tenant-detail-fifteen"
                                                aria-controls="home" role="tab"
                                                data-toggle="tab">Flag Bank</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">


                                        <div role="tabpanel" class="tab-pane active generalFullTab" id="tenant-detail-one">

                                            <div class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>General</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="detail-outer">
                                                        <div class="row">
                                                            <div class="col-sm-2 text-center gn_tenant_image">
                                                                <!--  <img src="images/dummy-img.jpg"/> -->
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Tenant Name :</label>
                                                                    <span class="gn_tenant_name"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Phone Number :</label>
                                                                    <span class="gn_tenant_phone"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Email :</label>
                                                                    <span class="gn_email1"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Move In Date :</label>
                                                                    <span class="gn_moveInDate"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Martial Status :</label>
                                                                    <span class="gn_marital_status">sdcsd</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Veteran Status :</label>
                                                                    <span class="gn_veteran_name">Net 30</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">SSN/SIN/ID :</label>
                                                                    <span class="gn_ssn"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Notice Date :</label>
                                                                    <span class="gn_moveInDate">Mar 02, 2019 (Mon.)</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Notice Period :</label>
                                                                    <select class="noticePeriod">
                                                                        <option value="30">30 Days</option>
                                                                        <option value="60">60 Days</option>
                                                                        <option value="90">90 Days</option>
                                                                    </select>
                                                                    <span class='gn_noticePeriod'></span>

                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Tenant Status :</label>
                                                                    <span class="gn_status"></span>
                                                                    <span class="gn_tenant_status"></span>
                                                                </div>


                                                            </div>

                                                            <div class="col-sm-4">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Lease Start Date :</label>
                                                                    <span class="gn_lease_start"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Note for this Phone Number
                                                                        :</label>
                                                                    <span class="phone_number_note" style="color:red;"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Lease End Date :</label>
                                                                    <span class="gn_lease_end"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Term :</label>
                                                                    <span class="gn_term"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Monthly Rent :</label>
                                                                    <span class="gn_rent"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Security Deposite
                                                                        :</label>
                                                                    <span class="gn_security_deposite"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Entity/ Company Name
                                                                        :</label>
                                                                    <span class="gn_tenant_contact"></span>
                                                                </div>

                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Hobbies :</label>
                                                                    <span class="gn_hobbies"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Ethnicity :</label>
                                                                    <span class="gn_ethnicity"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="edit-foot">
                                                            <a href="javascript:;" class="sectionOneEdit">
                                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                Edit
                                                            </a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- Form Outer Ends -->
                                            <form id="editTenant" method="post" class="editpageviewsection">
                                                <div class="form-outer">
                                                    <div class="form-hdr"><h3>Contact Details</h3></div>
                                                    <div class="form-data">
                                                        <div class="row">


                                                            <!--    <div class="col-sm-2">
                                                               <label>Upload Picture</label>
                                                               <div class="upload-logo">
                                                                   <div class="img-outer"></div>
                                                                   <a href="javascript:;"><i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Logo</a>
                                                               </div>
                                                           </div> -->


                                                            <div class="col-sm-12">



                                                                <div class="row">
                                                                    <div class="col-sm-3">

                                                                        <div class="upload-logo">
                                                                            <?php $dummypath = 'http://'.$_SERVER['HTTP_HOST'].'/company/images/dummy-img.jpg'; ?>
                                                                            <div class="tenant_image img-outer"><img src="<?php echo $dummypath; ?>"></div>

                                                                            <a href="javascript:;">
                                                                                <i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Image/Photo</a>
                                                                            <span>(Maximum File Size Limit: 1MB)</span>
                                                                        </div>

                                                                        <div class="image-editor">
                                                                            <input type="file" class="cropit-image-input" style="top: 19px;" name="tenant_image" accept="image/*">
                                                                            <div class="cropItData" style="display: none;">
                                                                                <span class="closeimagepopupicon">X</span>
                                                                                <div class="cropit-preview"></div>
                                                                                <div class="cropit-rotate">
                                                                                    <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                                    </a>
                                                                                    <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                                    </a>

                                                                                </div>
                                                                                <div class="image-size-label">Resize image</div>
                                                                                <input type="range" class="cropit-image-zoom-input" min="0" max="1" step="0.01">
                                                                                <input type="hidden" name="image-data" class="hidden-image-data">
                                                                                <input type="button" class="export" value="Done" data-val="tenant_image">
                                                                            </div>
                                                                        </div>






                                                                    </div>





                                                                    <div class="col-sm-3 tenant_hide_row">
                                                                        <label>Salutation</label>
                                                                        <select class="form-control salutation" name="salutation" id="salutation">
                                                                            <option value="Select">Select</option>
                                                                            <option value="Dr.">Dr.</option>
                                                                            <option value="Mr.">Mr.</option>
                                                                            <option value="Mrs.">Mrs.</option>
                                                                            <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                                                            <option value="Ms.">Ms.</option>
                                                                            <option value="Sir">Sir</option>
                                                                            <option value="Madam">Madam</option>
                                                                            <option value="Brother">Brother</option>
                                                                            <option value="Sister">Sister</option>
                                                                            <option value="Father">Father</option>
                                                                            <option value="Mother">Mother</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-sm-3 tenant_hide_row">
                                                                        <label>First Name <em class="red-star">*</em></label>
                                                                        <input class="form-control gn_first_name capital" type="text" name="firstname" maxlength="50">

                                                                        <input type="hidden" value="tenant" id="url_check">
                                                                    </div>
                                                                    <div class="col-sm-3 tenant_hide_row">
                                                                        <label>Middle Name</label>
                                                                        <input class="form-control gn_middle_name capsOn" type="text" name="middlename" maxlength="50">
                                                                    </div>
                                                                    <div class="col-sm-3 tenant_hide_row">
                                                                        <label>Last Name <em class="red-star">*</em></label>
                                                                        <input class="form-control gn_last_name capital" type="text" name="lastname" maxlength="50">
                                                                    </div>
                                                                    <div class="col-sm-3  tenant_hide_row maiden_name_hide" style="display: none;">
                                                                        <label>Maiden Name</label>
                                                                        <input class="form-control gn_maiden_name capsOn" type="text" name="medianName" maxlength="50">
                                                                    </div>
                                                                    <div class="col-sm-3 tenant_hide_row">
                                                                        <label>Nickname</label>
                                                                        <input class="form-control gn_nick_name capsOn" type="text" name="nickname" maxlength="50">
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="phoneInfo"></div>
                                                                    </div>
                                                                    <div class="col-sm-3 clear">
                                                                        <label>Add Note for this Phone Number</label>
                                                                        <textarea class="form-control gn_phone_number_note capital" name="note"></textarea>
                                                                    </div>
                                                                    <div class="generalEmail"></div>
                                                                    <div class="col-sm-3">
                                                                        <label>Referral Source <a class="pop-add-icon add_reff_popup"
                                                                                                  href="javascript:;"><i
                                                                                    class="fa fa-plus-circle"
                                                                                    aria-hidden="true"></i></a></label>
                                                                        <select name="edit_general_referral" class="form-control edit_general_referral" name="referralSource">
                                                                            <option value="0">Select</option>
                                                                        </select>
                                                                        <div class="add-popup" id="selectPropertyReferralResource1">
                                                                            <h4>Add New Referral Source</h4>
                                                                            <div class="add-popup-body">
                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>New Referral Source <em class="red-star">*</em></label>
                                                                                        <input name="referral" class="form-control customValidateGroup reff_source" type="text" data_required="true" data_max="150" placeholder="New Referral Source">
                                                                                        <span class="customError required" aria-required="true" id="reff_source"></span>
                                                                                    </div>
                                                                                    <div class="btn-outer">
                                                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source" data-name="referralSource">Save</button>
                                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="div-full">
                                                                        <div class="col-sm-12" style="min-height: 1px;">
                                                                            <label><input type="checkbox" class="entity" name="entity" value="1" /> Check this Box if this Tenant is an Entity/Company</label>
                                                                        </div>
                                                                        <div class="col-sm-3 contactTenant" style="display:none">
                                                                            <label>Contact for this tenant <em class="red-star">*</em></label>
                                                                            <input type="text" name="tenant_contact" class="tenant_contact form-control capital">
                                                                        </div>
                                                                    </div>
                                                                    <div class="addCheckBoxName">
                                                                        <div class="col-sm-3 tenant_hide_row">
                                                                            <label>Ethnicity <a class="pop-add-icon add_ethn_popup" href="javascript:;"><i
                                                                                        class="fa fa-plus-circle"
                                                                                        aria-hidden="true"></i></a></label>
                                                                            <select class="form-control valid ethnicity" name="ethncity"
                                                                                    aria-invalid="false"></select>
                                                                            <div class="add-popup" id="selectPropertyEthnicity1">
                                                                                <h4>Add New Ethnicity</h4>
                                                                                <div class="add-popup-body">
                                                                                    <div class="form-outer">
                                                                                        <div class="col-sm-12">
                                                                                            <label>New Ethnicity <em class="red-star">*</em></label>
                                                                                            <input class="form-control ethnicity_src customValidateGroup" type="text" placeholder="Add New Ethnicity" data_required="true" >
                                                                                            <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                                                        </div>
                                                                                        <div class="btn-outer">
                                                                                            <button type="button" class="blue-btn add_single1"  data-table="tenant_ethnicity" data-cell="title" data-class="ethnicity_src" data-name="ethncity">Save</button>
                                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3 tenant_hide_row">
                                                                            <label>Martial Status <a class="pop-add-icon add_martl_popup"
                                                                                                     href="javascript:;"><i
                                                                                        class="fa fa-plus-circle"
                                                                                        aria-hidden="true"></i></a></label>
                                                                            <select class="form-control edit_general_marital" name="maritalStatus">
                                                                                <option value="1"></option>
                                                                            </select>
                                                                            <div class="add-popup" id="selectPropertyMaritalStatus1">
                                                                                <h4>Add New Marital Status</h4>
                                                                                <div class="add-popup-body">
                                                                                    <div class="form-outer">
                                                                                        <div class="col-sm-12">
                                                                                            <label>New Marital Status <em class="red-star">*</em></label>
                                                                                            <input class="form-control maritalstatus_src" type="text" placeholder="Add New Marital Status">
                                                                                            <span class="customError required red-star" aria-required="true" id="maritalstatus_src"></span>
                                                                                        </div>
                                                                                        <div class="btn-outer">
                                                                                            <button type="button" class="blue-btn add_single1" data-table="tenant_marital_status" data-cell="marital" data-class="maritalstatus_src" data-name="maritalStatus">Save</button>
                                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3 tenant_hide_row">
                                                                            <label>Hobbies <a class="pop-add-icon add_hoby_popup" href="javascript:;"><i
                                                                                        class="fa fa-plus-circle"
                                                                                        aria-hidden="true"></i></a></label>
                                                                            <select class="form-control edit_general_hobby" multiple="" name="edit_general_hobby[]">
                                                                                <option value=""></option>
                                                                            </select>
                                                                            <div class="add-popup" id="selectPropertyHobbies1">
                                                                                <h4>Add New Hobbies</h4>
                                                                                <div class="add-popup-body">
                                                                                    <div class="form-outer">
                                                                                        <div class="col-sm-12">
                                                                                            <label>New Hobbies <em class="red-star">*</em></label>
                                                                                            <input class="form-control hobbies_src" type="text" placeholder="Add New Hobbies" data_required="true">
                                                                                            <span class="red-star" id="hobbies_src"></span>
                                                                                        </div>
                                                                                        <div class="btn-outer">
                                                                                            <button type="button" class="blue-btn add_single1" data-table="hobbies" data-cell="hobby" data-class="hobbies_src" data-name="hobbies">Save</button>
                                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3 tenant_hide_row">
                                                                            <label>Veteran Status <a class="pop-add-icon add_vtrn_popup"
                                                                                                     href="javascript:;"><i
                                                                                        class="fa fa-plus-circle"
                                                                                        aria-hidden="true"></i></a></label>
                                                                            <select class="form-control edit_general_veteran veteran" name="edit_general_veteran">
                                                                                <option value=""></option>
                                                                            </select>
                                                                            <div class="add-popup" id="selectPropertyVeteranStatus1">
                                                                                <h4>Add New VeteranStatus</h4>
                                                                                <div class="add-popup-body">
                                                                                    <div class="form-outer">
                                                                                        <div class="col-sm-12">
                                                                                            <label>New VeteranStatus <em class="red-star">*</em></label>
                                                                                            <input class="form-control veteran_src" type="text" placeholder="Add New VeteranStatus">
                                                                                            <span class="red-star" id="veteran_src"></span>
                                                                                        </div>
                                                                                        <div class="btn-outer">
                                                                                            <button type="button" class="blue-btn add_single1" data-table="tenant_veteran_status" data-cell="veteran" data-class="veteran_src" data-name="veteranStatus">Save</button>
                                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-3 tenant_hide_row">
                                                                        <div class="gn_ssn_data">



                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <!-- Form Outer Ends -->



                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-outer">
                                                    <div class="form-hdr"><h3>Emergency Contact Details</h3></div>
                                                    <div class="form-data">
                                                        <div class="emergencyInfo">


                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>Tenant Credential Control

                                                        </h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="credentialInfo">
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="btn-outer">
                                                    <button class="blue-btn">Update</button>
                                                    <a href="javascript:void(0);" class="grey-btn cancelUpdateBtn">Cancel</a>
                                                </div>
                                            </form>

                                            <div class="viewtenantpage">
                                                <div class="form-outer">
                                                    <div class="form-outer2">
                                                        <div class="form-hdr"><h3>Emergency Contact Details</h3></div>
                                                        <div class="form-data viewEmergencyInfo">
                                                            <!-- Loop Starts Here -->


                                                        </div>
                                                        <div class="edit-foot">
                                                            <a href="javascript:;" class="sectionOneEdit">
                                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                Edit
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-outer">
                                                    <div class="form-outer2">
                                                        <div class="form-hdr"><h3>Tenant Credential Control</h3></div>
                                                        <div class="form-data viewCredentialsInfo">
                                                            <!-- Loop Starts Here -->


                                                            <!-- Loop Starts Here -->


                                                        </div>
                                                        <div class="edit-foot">
                                                            <a href="javascript:;" class="sectionOneEdit">
                                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                Edit
                                                            </a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- Tab 1 Ends -->


                                        <div role="tabpanel" class="tab-pane" id="tenant-detail-two">
                                            <div class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Additional Information</h3>
                                                </div>
                                                <div class="form-data tenantAdditionalInfo" style="display:none;"></div>
                                                <div class="form-data additionalInfoData">
                                                    <div class="detail-outer">
                                                        <div class="col-sm-6">
                                                            <div class="row" style="margin-bottom:0px;">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Birth Date :</label>
                                                                    <span class="tenant_dob"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Gender :</label>
                                                                    <span class="gender"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Driver License State/Province :</label>
                                                                    <span class="license_state"></span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Driver's License # :</label>
                                                                    <span class="license"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Vehicle :</label>
                                                                    <span class="vehicle"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Semester Billing :</label>
                                                                    <span>No</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Number of Keys Signed at
                                                                        Move
                                                                        In :</label>
                                                                    <span class="movein_key_signed"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Smoker :</label>
                                                                    <span class="smoker"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Pet :</label>
                                                                    <span class="pet"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Medical Allergy :</label>
                                                                    <span class="medical_allergy"></span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="edit-foot edit_additional_button">
                                                            <a href="javascript:;">
                                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                Edit
                                                            </a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--Form Outer 1 Ends-->

                                            <!-- Vehicle Grid -->
                                            <div class="form-outer vehicle_main">
                                                <div class="form-hdr">
                                                    <h3 class="pull-left">Vehicles</h3>
                                                    <input type="button" name="addVehicle" value="Add Vehicle"
                                                           class="addbtn blue-btn pull-right" id="add_vehicle_button" style="display: none">

                                                </div>
                                                <div class="form-data">
                                                    <div>
                                                        <form id='addEditVehicle' style="display: none;">
                                                            <input type="hidden" name="vehicle_action"
                                                                   class="vehicle_action">
                                                            <input type="hidden" name="vehicle_id" class="vehicle_id">
                                                            <div class="row">


                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>Vehicle Type</label>
                                                                            <input class="form-control capsOn" type="text"
                                                                                   name='vehicle_type'
                                                                                   id="vehicle_type" maxlength="50">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>Make</label>
                                                                            <input class="form-control capsOn" type="text"
                                                                                   name='vehicle_make'
                                                                                   id="vehicle_make" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>License Plate Number</label>
                                                                            <input class="form-control" type="text"
                                                                                   name='vevicle_license'
                                                                                   id="vevicle_license" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>Color</label>
                                                                            <input class="form-control capsOn" type="text"
                                                                                   name='vehicle_color'
                                                                                   id="vehicle_color" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <label>Year Of Vehicle</label>
                                                                        <select class="form-control" id="vevicle_year"
                                                                                name="vevicle_year">
                                                                            <option value="">Select</option>

                                                                            <?php
                                                                            for ($vehiclyear = date("Y"); $vehiclyear >= 1900; $vehiclyear--) {
                                                                                echo '<option value="' . $vehiclyear . '">' . $vehiclyear . '</option>';
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>VIN </label>
                                                                            <input class="form-control" type="text"
                                                                                   name='vehicle_vin' id="vehicle_vin" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>Registration</label>
                                                                            <input class="form-control capsOn" type="text"
                                                                                   name='vehicle_registration'
                                                                                   id="vehicle_registration" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>


                                                                    <div class="col-sm-12 col-md-2 grey-plus-photo-upload clear">
                                                                        <label>Vehicle Photo/Image</label>
                                                                        <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                        <div class="upload-logo">
                                                                            <div class="img-outer vehicle_image1"
                                                                                 id="vehicle_image1"><img
                                                                                    src="<?php echo COMPANY_SITE_URL ?>/images/Vehicledummy.png">
                                                                            </div>
                                                                            <a class="choose-img" href="javascript:;">Choose
                                                                                Image</a>
                                                                        </div>
                                                                        <div class="image-editor">
                                                                            <input type="file"
                                                                                   class="cropit-image-input form-control"
                                                                                   name="vehicle_image1[]"
                                                                                   accept="image/*">
                                                                            <div class="cropItData"
                                                                                 style="display: none;">


                                                                                <div class="cropit-preview"></div>
                                                                                <div class="cropit-rotate">
                                                                                    <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                                    </a>
                                                                                    <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="vehicle_image1"></div>
                                                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                                                <input type="range"
                                                                                       class="cropit-image-zoom-input"
                                                                                       min="0" max="1" step="0.01">
                                                                                <input type="hidden" name="image-data"
                                                                                       class="hidden-image-data">
                                                                                <input type="button" class="export"
                                                                                       value="Done"
                                                                                       data-val="vehicle_image1">
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                        <label>Vehicle Photo/Image</label>
                                                                        <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                        <div class="upload-logo">
                                                                            <div class="img-outer vehicle_image2"
                                                                                 id="vehicle_image2"><img
                                                                                    src="<?php echo COMPANY_SITE_URL ?>/images/Vehicledummy.png">
                                                                            </div>
                                                                            <a class="choose-img" href="javascript:;">Choose
                                                                                Image</a>
                                                                        </div>
                                                                        <div class="image-editor">
                                                                            <input type="file"
                                                                                   class="cropit-image-input form-control"
                                                                                   name="vehicle_image2[]"
                                                                                   accept="image/*">
                                                                            <div class="cropItData"
                                                                                 style="display: none;">
                                                                                <div class="cropit-preview"></div>
                                                                                <div class="cropit-rotate">
                                                                                    <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                                    </a>
                                                                                    <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="vehicle_image2"></div>
                                                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                                                <input type="range"
                                                                                       class="cropit-image-zoom-input"
                                                                                       min="0" max="1" step="0.01">
                                                                                <input type="hidden" name="image-data"
                                                                                       class="hidden-image-data">
                                                                                <input type="button" class="export"
                                                                                       value="Done"
                                                                                       data-val="vehicle_image2">
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                        <label>Vehicle Photo/Image</label>
                                                                        <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                        <div class="upload-logo">
                                                                            <div class="img-outer vehicle_image3"
                                                                                 id="vehicle_image3"><img
                                                                                    src="<?php echo COMPANY_SITE_URL ?>/images/Vehicledummy.png">
                                                                            </div>
                                                                            <a class="choose-img" href="javascript:;">Choose
                                                                                Image</a>
                                                                        </div>
                                                                        <div class="image-editor">
                                                                            <input type="file"
                                                                                   class="cropit-image-input form-control"
                                                                                   name="vehicle_image3[]"
                                                                                   accept="image/*">
                                                                            <div class="cropItData"
                                                                                 style="display: none;">
                                                                                <div class="cropit-preview"></div>
                                                                                <div class="cropit-rotate">
                                                                                    <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                                    </a>
                                                                                    <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="vehicle_image3"></div>
                                                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                                                <input type="range"
                                                                                       class="cropit-image-zoom-input"
                                                                                       min="0" max="1" step="0.01">
                                                                                <input type="hidden" name="image-data"
                                                                                       class="hidden-image-data">
                                                                                <input type="button" class="export"
                                                                                       value="Done"
                                                                                       data-val="vehicle_image3">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <div class="col-sm-12">
                                                                        <input type="submit" class="blue-btn"
                                                                               name="submit" value="Save">
                                                                        <input type="button" name="Cancel"
                                                                               value="Cancel"
                                                                               class="grey-btn cancelbtn">
                                                                    </div>
                                                                </div>


                                                            </div>


                                                        </form>

                                                        <!-- append vehicle listing -->
                                                        <div class="apx-table">

                                                            <div class="table-responsive">

                                                                <table id="TenantVehicle-table"
                                                                       class="table table-bordered">

                                                                </table>
                                                            </div>
                                                        </div>

                                                        <div class="edit-foot edit_vehicle_button">
                                                            <a href="javascript:;">
                                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                Edit
                                                            </a>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <!--Form Outer 2 Ends-->


                                            <div class="form-outer  pet_main">
                                                <div class="form-hdr">
                                                    <h3 class="pull-left">Pets</h3>
                                                    <input type="button" value="Add Pets" id="add_pet_button" style="display: none;"
                                                           class="addbtn blue-btn pull-right">

                                                </div>
                                                <div class="form-data">
                                                    <!--                                         <input type="button"   class="addbtn blue-btn" value="Add Pet">-->

                                                    <form id='addEditPet' method="post" style="display:none;">
                                                        <input type="hidden" class="pet_action blue-btn"
                                                               name="pet_action">
                                                        <input type="hidden" class="pet_unique_id blue-btn"
                                                               name="pet_unique_id">

                                                        <div class="property_pet">
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <label>Pet Name</label>
                                                                            <input class="form-control capsOn" type="text"
                                                                                   name="pet_name" id="pet_name" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <label>Pet ID</label>
                                                                            <input class="form-control" type="text"
                                                                                   name="pet_id" maxlength="30">
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <label>Pet Type/Breed</label>
                                                                            <input class="form-control capsOn" type="text"
                                                                                   name="pet_type" id="pet_type" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <!--calander -->
                                                                            <label>Birth Date</label>
                                                                            <input class="form-control calander pet_birth"
                                                                                   type="text" name="pet_birth">
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2 ">
                                                                        <label>Pet Age</label>
                                                                        <select class="form-control" name="pet_age"
                                                                                id="pet_age">
                                                                            <?php
                                                                            for ($petAge = 1; $petAge <= 100; $petAge++) {
                                                                                echo '<option value="' . $petAge . '">' . $petAge . '</option>';
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <label>Gender</label>
                                                                        <select class="form-control" id="pet_gender"
                                                                                name="pet_gender" id="pet_gender"
                                                                        <option value="">Select</option>
                                                                        <option value="1">Male</option>
                                                                        <option value="2">Female</option>
                                                                        <option value="3">Other</option>
                                                                        </select>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <label>Pet Weight</label>
                                                                            <input class="form-control" type="text"
                                                                                   name="pet_weight" id="pet_weight" maxlength="30">
                                                                            <span class="ffirst_nameErr error red-star" ></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-1 col-md-2">
                                                                        <label>Pet Weight Unit</label>
                                                                        <select class="form-control"
                                                                                id="pet_weight_unit"
                                                                                name="pet_weight_unit">
                                                                            <option value="">Select</option>
                                                                            <option value="1">lb</option>
                                                                            <option value="2">lbs</option>
                                                                            <option value="3">kg</option>
                                                                        </select>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <label>Note</label>
                                                                            <textarea class="form-control capital"
                                                                                      name="pet_note"
                                                                                      id="pet_note"></textarea>
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <!--calander -->
                                                                            <label>Pet Color</label>
                                                                            <input class="form-control capsOn" type="text"
                                                                                   name="pet_color" id="pet_color" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <!--calander -->
                                                                            <label>Chip ID</label>
                                                                            <input class="form-control capsOn" type="text"
                                                                                   name="pet_chipid" id="pet_chipid" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <!-- <span class="ffirst_nameErr error red-star"></span> -->
                                                                </div>
                                                            </div>


                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <label>vet\Hosp. Name</label>
                                                                            <input class="form-control capsOn" type="text"
                                                                                   name="pet_vet" id="pet_vet" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2 countycodediv">
                                                                        <label>Country Code</label>
                                                                        <select class="form-control"
                                                                                name="pet_countryCode"
                                                                                id="pet_countryCode" maxlength="30"></select>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <label>Phone Number</label>
                                                                            <input type="text" class="form-control"
                                                                                   name="pet_phoneNumber"
                                                                                   id="pet_phoneNumber" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <!--calander -->
                                                                            <label>Last Visit</label>
                                                                            <input class="form-control calander pet_lastVisit"
                                                                                   type="text" name="pet_lastVisit" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <!--calander -->
                                                                            <label>Next Visit</label>
                                                                            <input class="form-control calander pet_nextVisit"
                                                                                   type="text" name="pet_nextVisit" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>


                                                                <div class="form-outer">
                                                                    <div class="col-sm-3 col-md-3">
                                                                        <label>Medical Condition</label>
                                                                        <div class="check-outer">
                                                                            <input name="pet_medical" type="radio"
                                                                                   value="1"
                                                                                   class="pet_medical_condition" maxlength="30">
                                                                            <label>Yes</label>
                                                                        </div>
                                                                        <div class="check-outer">
                                                                            <input name="pet_medical" type="radio"
                                                                                   value="0"
                                                                                   class="pet_medical_condition"
                                                                                   checked="" maxlength="30">
                                                                            <label>No</label>
                                                                        </div>
                                                                        <span class="flast_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="pet_medical_html" style="display:none">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12 col-md-3">
                                                                                <div class="contactTenant">
                                                                                    <!--calander -->
                                                                                    <label>Please Add Pets Medical
                                                                                        Condiction</label>
                                                                                    <input class="form-control"
                                                                                           type="text"
                                                                                           name="pet_medical_condition_note"
                                                                                           id="pet_medical_condition_note" maxlength="30">
                                                                                </div>
                                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-outer">
                                                                    <div class="col-sm-3 col-md-3">
                                                                        <label>Shots</label>
                                                                        <div class="check-outer">
                                                                            <input name="pet_shots" type="radio"
                                                                                   value="1" class="pet_name_shot" maxlength="30">
                                                                            <label>Yes</label>
                                                                        </div>
                                                                        <div class="check-outer">
                                                                            <input name="pet_shots" type="radio"
                                                                                   value="0" class="pet_name_shot"
                                                                                   checked="" maxlength="30">
                                                                            <label>No</label>
                                                                        </div>
                                                                        <span class="flast_nameErr error red-star"></span>
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="pet_shot_date" style="display:none;">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class="contactTenant">
                                                                                <!--calander -->
                                                                                <label>Name of the shot</label>
                                                                                <input class="form-control capsOn" type="text"
                                                                                       name="pet_name_shot"
                                                                                       id="pet_name_shot" maxlength="30">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class="contactTenant">
                                                                                <!--calander -->
                                                                                <label>Date Given</label>
                                                                                <input class="form-control pet_date_given calander"
                                                                                       type="text" name="pet_date_given"
                                                                                >
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class="contactTenant">
                                                                                <!--calander -->
                                                                                <label>Expiration Date</label>
                                                                                <input class="form-control pet_expiration_date calander"
                                                                                       type="text"
                                                                                       name="pet_expiration_date">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class="contactTenant">
                                                                                <!--calander -->
                                                                                <label>Follow Up</label>
                                                                                <input class="form-control calander pet_follow_up"
                                                                                       type="text" name="pet_follow_up"
                                                                                >
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class="contactTenant">
                                                                                <!--calander -->
                                                                                <label>Note</label>
                                                                                <input class="form-control" type="text"
                                                                                       name="shot_pet_note"
                                                                                       id="shot_pet_note">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </div>

                                                                </div>


                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Rabies</label>
                                                                    <div class="check-outer">
                                                                        <input name="pet_rabies capsOn" type="radio" value="1"
                                                                               class="pet_rabies" maxlength="30">
                                                                        <label>Yes</label>
                                                                    </div>
                                                                    <div class="check-outer">
                                                                        <input name="pet_rabies" class="pet_rabies"
                                                                               type="radio" value="0" checked="" >
                                                                        <label>No</label>
                                                                    </div>
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="clearfix"></div>


                                                                <div class="pet_rabies_html" style="display:none">

                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class="contactTenant">
                                                                                <!--calander -->
                                                                                <label>Rabies#</label>
                                                                                <input class="form-control" type="text"
                                                                                       name="pet_rabies_name" maxlength="30">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class="contactTenant">
                                                                                <!--calander -->
                                                                                <label>Date Given</label>
                                                                                <input class="form-control calander pet_rabies_given_date"
                                                                                       type="text"
                                                                                       name="pet_rabies_given_date"
                                                                                >
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>

                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class="contactTenant">
                                                                                <!--calander -->
                                                                                <label>Expiration Date</label>
                                                                                <input class="form-control calander pet_rabies_expiration_date"
                                                                                       type="text"
                                                                                       name="pet_rabies_expiration_date"
                                                                                >
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class="contactTenant">
                                                                                <!--calander -->
                                                                                <label>Note</label>
                                                                                <input class="form-control" type="text"
                                                                                       name="pet_rabies_note[]">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </div>

                                                                </div>

                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                        <label>Pet Photo/Image</label>
                                                                        <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                        <div class="upload-logo">
                                                                            <div class="img-outer pet_image1"
                                                                                 id="pet_image1"><img
                                                                                    src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">
                                                                            </div>
                                                                            <a class="choose-img" href="javascript:;">Choose
                                                                                Image</a>
                                                                        </div>
                                                                        <div class="image-editor">
                                                                            <input type="file"
                                                                                   class="cropit-image-input form-control"
                                                                                   name="pet_image1[]" accept="image/*">
                                                                            <div class="cropItData"
                                                                                 style="display: none;">

                                                                                <div class="cropit-preview"></div>
                                                                                <div class="cropit-rotate">
                                                                                    <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                                    </a>
                                                                                    <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="pet_image1"></div>
                                                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                                                <input type="range"
                                                                                       class="cropit-image-zoom-input"
                                                                                       min="0" max="1" step="0.01">
                                                                                <input type="hidden" name="image-data"
                                                                                       class="hidden-image-data">
                                                                                <input type="button" class="export"
                                                                                       value="Done"
                                                                                       data-val="pet_image1">
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                        <label>Pet Photo/Image</label>
                                                                        <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                        <div class="upload-logo">
                                                                            <div class="img-outer pet_image2"
                                                                                 id="pet_image2"><img
                                                                                    src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">
                                                                            </div>
                                                                            <a class="choose-img" href="javascript:;">Choose
                                                                                Image</a>
                                                                        </div>
                                                                        <div class="image-editor">
                                                                            <input type="file"
                                                                                   class="cropit-image-input form-control"
                                                                                   name="pet_image2[]" accept="image/*">
                                                                            <div class="cropItData"
                                                                                 style="display: none;">

                                                                                <div class="cropit-preview"></div>
                                                                                <div class="cropit-rotate">
                                                                                    <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                                    </a>
                                                                                    <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="pet_image2"></div>
                                                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                                                <input type="range"
                                                                                       class="cropit-image-zoom-input"
                                                                                       min="0" max="1" step="0.01">
                                                                                <input type="hidden" name="image-data"
                                                                                       class="hidden-image-data">
                                                                                <input type="button" class="export"
                                                                                       value="Done"
                                                                                       data-val="pet_image2">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                        <label>Pet Photo/Image</label>
                                                                        <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                        <div class="upload-logo">
                                                                            <div class="img-outer pet_image3"
                                                                                 id="pet_image3"><img
                                                                                    src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">
                                                                            </div>
                                                                            <a class="choose-img" href="javascript:;">Choose
                                                                                Image</a>
                                                                        </div>
                                                                        <div class="image-editor">
                                                                            <input type="file"
                                                                                   class="cropit-image-input form-control"
                                                                                   name="pet_image3[]" accept="image/*">
                                                                            <div class="cropItData"
                                                                                 style="display: none;">

                                                                                <div class="cropit-preview"></div>
                                                                                <div class="cropit-rotate">
                                                                                    <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                                    </a>
                                                                                    <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="pet_image3"></div>
                                                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                                                <input type="range"
                                                                                       class="cropit-image-zoom-input"
                                                                                       min="0" max="1" step="0.01">
                                                                                <input type="hidden" name="image-data"
                                                                                       class="hidden-image-data">
                                                                                <input type="button" class="export"
                                                                                       value="Done"
                                                                                       data-val="pet_image3">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <input type="submit" name="submit"
                                                                               class="blue-btn" value="Save">
                                                                        <input type="button" class="cancelbtn grey-btn"
                                                                               value="Cancel">
                                                                    </div>
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- append pet listing -->


                                                    <div class="apx-table">
                                                        <div class="table-responsive">
                                                            <table id="TenantPet-table" class="table table-bordered">
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="edit-foot edit_pet_button">
                                                        <a href="javascript:;">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Form Outer 3 Ends-->


                                            <div class="form-outer  service_main">
                                                <div class="form-hdr">
                                                    <h3 class="pull-left">Service/Companion Animals</h3>
                                                    <input type="button" value="Add Animal"
                                                           class="addbtn blue-btn  pull-right" id="add_animal_animal" style="display: none">


                                                </div>
                                                <div class="form-data">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <form id="addEditService" method="post"
                                                                  style="display:none;">
                                                                <input type="hidden" class="service_action"
                                                                       name="service_action">
                                                                <input type="hidden" class="service_unique_id"
                                                                       name="service_unique_id">

                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <label>Name</label>
                                                                                <input class="form-control capsOn" type="text"
                                                                                       name='service_name'
                                                                                       id="service_name" maxlength="30">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>

                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <label>ID</label>
                                                                                <input class="form-control" type="text"
                                                                                       name='service_id'
                                                                                       id="service_id" maxlength="30">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>

                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <label>Type/Breed</label>
                                                                                <input class="form-control capsOn" type="text"
                                                                                       name='service_type'
                                                                                       id="service_type" maxlength="30">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>

                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <!--calander -->
                                                                                <label>Birth Date</label>
                                                                                <input class="form-control calander"
                                                                                       type="text"
                                                                                       name='service_birth'
                                                                                       id="service_birth">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>

                                                                        <div class="col-sm-12 col-md-2">
                                                                            <label>Age</label>
                                                                            <select class="form-control"
                                                                                    id="service_year"
                                                                                    name="service_year">
                                                                                <option value="">Select</option>

                                                                                <?php
                                                                                for ($serviceAge = 1; $serviceAge <= 100; $serviceAge++) {
                                                                                    echo '<option value="' . $serviceAge . '">' . $serviceAge . '</option>';
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                            <span class="term_planErr error red-star"></span>
                                                                        </div>

                                                                        <div class="col-sm-12 col-md-2 ">
                                                                            <label>Gender</label>
                                                                            <select class="form-control"
                                                                                    name="service_gender"
                                                                                    id="service_gender">
                                                                                <option value="0">Select</option>
                                                                                <option value="1">Male</option>
                                                                                <option value="2">Female</option>
                                                                                <option value="3">Other</option>
                                                                            </select>
                                                                            <span class="term_planErr error red-star"></span>
                                                                        </div>

                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <label>Animal Weight</label>
                                                                                <input class="form-control" type="text"
                                                                                       name='service_weight'
                                                                                       id="service_weight">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>

                                                                        <div class="col-sm-12 col-md-2 ">
                                                                            <label>Weight Unit</label>
                                                                            <select class="form-control"
                                                                                    name="service_weight_unit"
                                                                                    id="service_weight_unit">
                                                                                <option value="">Select</option>
                                                                                <option value="1">lb</option>
                                                                                <option value="2">lbs</option>
                                                                                <option value="3">kg</option>
                                                                            </select>
                                                                            <span class="term_planErr error red-star"></span>
                                                                        </div>

                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <label>Note</label>
                                                                                <textarea class="form-control"
                                                                                          name='service_note'
                                                                                          id="service_note"></textarea>
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>

                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <!--calander -->
                                                                                <label>Color</label>
                                                                                <input class="form-control capsOn" type="text"
                                                                                       name='service_color'
                                                                                       id="service_color" maxlength="30">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>

                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <!--calander -->
                                                                                <label>Chip ID</label>
                                                                                <input class="form-control capsOn" type="text"
                                                                                       name='service_chipid'
                                                                                       id="service_chipid" maxlength="30">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <label>vet\Hosp. Name</label>
                                                                                <input class="form-control capsOn" type="text"
                                                                                       name='service_vet'
                                                                                       id="service_vet" maxlength="30">
                                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12 col-md-2 countycodediv">
                                                                            <label>Country Code</label>
                                                                            <select class="form-control"
                                                                                    name="service_countryCode"
                                                                                    id="service_countryCode">
                                                                                <option value="1">Select</option>
                                                                            </select>
                                                                            <span class="term_planErr error red-star"></span>
                                                                        </div>

                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <label>Phone Number</label>
                                                                                <input type='text' class="form-control phone_format add-input"
                                                                                       name='service_phoneNumber'
                                                                                       id="service_phoneNumber" maxlength="12">
                                                                                <span class="term_planErr error red-star"></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <!--calander -->
                                                                                <label>Last Visit</label>
                                                                                <input class="form-control calander service_lastVisit"
                                                                                       type="text"
                                                                                       name='service_lastVisit'>
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-sm-12 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <!--calander -->
                                                                                <label>Next Visit</label>
                                                                                <input class="form-control calander service_nextVisit"
                                                                                       type="text"
                                                                                       name='service_nextVisit'>
                                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-3 col-md-3">
                                                                            <label>Medical Condition</label>
                                                                            <div class="check-outer">
                                                                                <input name='service_medical'
                                                                                       type="radio"
                                                                                       value='1'>
                                                                                <strong class='font-weight-bold'>Yes</strong>
                                                                            </div>
                                                                            <div class="check-outer">
                                                                                <input name='service_medical'
                                                                                       type="radio"
                                                                                       value='0' checked>
                                                                                <strong class='font-weight-bold'>No</strong>
                                                                            </div>
                                                                            <span class="flast_nameErr error red-star"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--<div class="row spaceNumber">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-3 col-md-3">
                                                                            <label>Parking Space Number<em class="red-star">*</em></label>
                                                                            <input class="form-control capsOn" type="text" id="parking_space" name="parking_space[]">
                                                                            <span class="flast_nameErr error red-star"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-3 col-md-3">
                                                                            <label>Shots</label>
                                                                            <input name='service_shots' type="radio"
                                                                                   value='1'
                                                                                   class="service_shots"><strong
                                                                                class='font-weight-bold'>Yes</strong>
                                                                            <input name='service_shots' type="radio"
                                                                                   value='0'
                                                                                   class="service_shots" checked><strong
                                                                                class='font-weight-bold'>No</strong>
                                                                            <span class="flast_nameErr error red-star"></span>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="row">
                                                                    <div class="animal_shot_date" style="display:none;">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12 col-md-12">
                                                                                <div class="col-sm-2 col-md-2">
                                                                                    <div class='contactTenant'>
                                                                                        <!--calander -->
                                                                                        <label>Name of the shot</label>
                                                                                        <input class="form-control capsOn"
                                                                                               type="text"
                                                                                               name='service_name_shot'
                                                                                               id="service_name_shot" maxlength="30">
                                                                                    </div>
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-2 col-md-2">
                                                                                    <div class='contactTenant'>
                                                                                        <!--calander -->
                                                                                        <label>Date Given</label>
                                                                                        <input class="form-control calander service_date_given"
                                                                                               type="text"
                                                                                               name='service_date_given'
                                                                                               value="<?php echo $today = date('Y-m-d'); ?>">
                                                                                    </div>
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-2 col-md-2">
                                                                                    <div class='contactTenant'>
                                                                                        <!--calander -->
                                                                                        <label>Expiration Date</label>
                                                                                        <input class="form-control calander service_expiration_date"
                                                                                               type="text"
                                                                                               name='service_expiration_date'
                                                                                               value="<?php echo $today = date("Y-m-d"); ?>">
                                                                                    </div>
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-2 col-md-2">
                                                                                    <div class='contactTenant'>
                                                                                        <!--calander -->
                                                                                        <label>Follow Up</label>
                                                                                        <input class="form-control calander service_follow_up"
                                                                                               type="text"
                                                                                               name='service_follow_up'>
                                                                                    </div>
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-12 col-md-2">
                                                                                    <div class='contactTenant'>
                                                                                        <!--calander -->
                                                                                        <label>Note</label>
                                                                                        <input class="form-control service_shot_note"
                                                                                               type="text"
                                                                                               name='service_shot_note'>
                                                                                    </div>
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-3 col-md-3">
                                                                            <label>Rabies</label>
                                                                            <input name='service_rabies' type="radio"
                                                                                   value='1'
                                                                                   class="animal_rabies"><strong
                                                                                class='font-weight-bold'>Yes</strong>
                                                                            <input name='service_rabies' type="radio"
                                                                                   value='0'
                                                                                   class="animal_rabies" checked><strong
                                                                                class='font-weight-bold'>No</strong>
                                                                            <span class="flast_nameErr error red-star"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row animal_rabies_html"
                                                                     style="display:none">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12 col-md-12">
                                                                            <div class="col-sm-2 col-md-2">
                                                                                <div class='contactTenant'>
                                                                                    <label>Rabies#</label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           name='animal_rabies'
                                                                                           id="animal_rabies">
                                                                                </div>
                                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-sm-2 col-md-2">
                                                                                <div class='contactTenant'>
                                                                                    <!--calander -->
                                                                                    <label>Date Given</label>
                                                                                    <input class="form-control calander animal_date_given"
                                                                                           type="text"
                                                                                           name='animal_date_given'
                                                                                           value="<?php echo $today = date("Y-m-d"); ?>">
                                                                                </div>
                                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-sm-2 col-md-2">
                                                                                <div class='contactTenant'>

                                                                                    <label>Expiration Date</label>
                                                                                    <input class="form-control calander animal_expiration_date"
                                                                                           type="text"
                                                                                           name='animal_expiration_date'
                                                                                           value="<?php echo $today = date("Y-m-d"); ?>">
                                                                                </div>
                                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-sm-2 col-md-2">
                                                                                <div class='contactTenant'>
                                                                                    <!--calander -->
                                                                                    <label>Note</label>
                                                                                    <input class="form-control"
                                                                                           type="text"
                                                                                           name='animal__note'
                                                                                           id="animal__note">
                                                                                </div>
                                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                            <label>Animal Photo/Image</label>
                                                                            <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                            <div class="upload-logo">
                                                                                <div class="img-outer service_image1"
                                                                                     id="service_image1"><img
                                                                                        src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">
                                                                                </div>
                                                                                <a class="choose-img"
                                                                                   href="javascript:;">Choose
                                                                                    Image</a>
                                                                            </div>
                                                                            <div class="image-editor">
                                                                                <input type="file"
                                                                                       class="cropit-image-input form-control"
                                                                                       name="service_image1"
                                                                                       accept="image/*">
                                                                                <div class="cropItData"
                                                                                     style="display: none;">
                                                                                    <div class="cropit-preview"></div>
                                                                                    <div class="cropit-rotate">
                                                                                        <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                                        </a>
                                                                                        <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="service_image1"></div>
                                                                                    <!--  <div class="image-size-label">Resize image</div> -->
                                                                                    <input type="range"
                                                                                           class="cropit-image-zoom-input"
                                                                                           min="0"
                                                                                           max="1" step="0.01">
                                                                                    <input type="hidden"
                                                                                           name="image-data"
                                                                                           class="hidden-image-data">
                                                                                    <input type="button" class="export"
                                                                                           value="Done"
                                                                                           data-val="service_image1">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                            <label>Animal Photo/Image</label>
                                                                            <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                            <div class="upload-logo">
                                                                                <div class="img-outer service_image2"
                                                                                     id="service_image2"><img
                                                                                        src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">
                                                                                </div>
                                                                                <a class="choose-img"
                                                                                   href="javascript:;">Choose
                                                                                    Image</a>
                                                                            </div>
                                                                            <div class="image-editor">
                                                                                <input type="file"
                                                                                       class="cropit-image-input form-control"
                                                                                       name="service_image2"
                                                                                       accept="image/*">
                                                                                <div class="cropItData"
                                                                                     style="display: none;">
                                                                                    <div class="cropit-preview"></div>
                                                                                    <div class="cropit-rotate">
                                                                                        <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                                        </a>
                                                                                        <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="service_image2"></div>
                                                                                    <!--  <div class="image-size-label">Resize image</div> -->
                                                                                    <input type="range"
                                                                                           class="cropit-image-zoom-input"
                                                                                           min="0"
                                                                                           max="1" step="0.01">
                                                                                    <input type="hidden"
                                                                                           name="image-data"
                                                                                           class="hidden-image-data">
                                                                                    <input type="button" class="export"
                                                                                           value="Done"
                                                                                           data-val="service_image2">
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                            <label>Animal Photo/Image</label>
                                                                            <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                            <div class="upload-logo">
                                                                                <div class="img-outer service_image3"
                                                                                     id="service_image3"><img
                                                                                        src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">
                                                                                </div>
                                                                                <a class="choose-img"
                                                                                   href="javascript:;">Choose
                                                                                    Image</a>
                                                                            </div>
                                                                            <div class="image-editor">
                                                                                <input type="file"
                                                                                       class="cropit-image-input form-control"
                                                                                       name="service_image3"
                                                                                       accept="image/*">
                                                                                <div class="cropItData"
                                                                                     style="display: none;">
                                                                                    <div class="cropit-preview"></div>
                                                                                    <div class="cropit-rotate">
                                                                                        <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                                        </a>
                                                                                        <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="service_image2"></div>
                                                                                    <!--  <div class="image-size-label">Resize image</div> -->
                                                                                    <input type="range"
                                                                                           class="cropit-image-zoom-input"
                                                                                           min="0"
                                                                                           max="1" step="0.01">
                                                                                    <input type="hidden"
                                                                                           name="image-data"
                                                                                           class="hidden-image-data">
                                                                                    <input type="button" class="export"
                                                                                           value="Done"
                                                                                           data-val="service_image3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <input type="submit" name="submit"
                                                                                   class="blue-btn" value="Save">
                                                                            <input type="button" name="Cancel"
                                                                                   value="Cancel"
                                                                                   class="grey-btn cancelbtn">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--  <a class="pop-add-icon copyServiceCompanion" href="javascript:;"><i
                                                                     class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                         <a class="pop-add-icon removeServiceCompanion" href="javascript:;"
                                                            style="display:none"><i class="fa fa-plus-circle"
                                                                                    aria-hidden="true"></i></a> -->


                                                            </form>
                                                            <!-- append service listing -->


                                                            <div class="apx-table">
                                                                <div class="table-responsive">
                                                                    <table id="TenantService-table"
                                                                           class="table table-bordered">
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="edit-foot" id="edit_animal_button">
                                                                <a href="javascript:;">
                                                                    <i class="fa fa-pencil-square-o"
                                                                       aria-hidden="true"></i>
                                                                    Edit
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Form Outer 4 Ends -->


                                            <div class="form-outer  medical_main">
                                                <div class="form-hdr">
                                                    <h3 class="pull-left">Medical/Allergies</h3>
                                                    <input type="button" value="Add New Medical/Allergies"
                                                           class="addbtn blue-btn pull-right" id="add_button_allergies" style="display: none">

                                                </div>
                                                <div class="form-data">
                                                    <div class="row">
                                                        <div class="col-sm-12 ">
                                                            <form id="addEditMedical" method="post"
                                                                  style="display: none;">
                                                                <input type="hidden" class="medical_action"
                                                                       name="medical_action">
                                                                <input type="hidden" class="medical_id"
                                                                       name="medical_id">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-3 col-md-3">
                                                                            <label>Tenant's medical or allergy
                                                                                issues</label>
                                                                            <input class="form-control capsOn" type="text"
                                                                                   name='medical_issue'
                                                                                   id="medical_issue" maxlength="30">
                                                                            <span class="flast_nameErr error red-star"></span>
                                                                        </div>

                                                                        <div class="col-sm-3 col-md-3">
                                                                            <label>Note</label>
                                                                            <textarea class="medical_note form-control capital"
                                                                                      type="text"
                                                                                      name='medical_note'
                                                                                      id="medical_note"></textarea>
                                                                            <span class="flast_nameErr error red-star"></span>
                                                                        </div>
                                                                        <!--calander -->
                                                                        <div class="col-sm-3 col-md-3">
                                                                            <label>Date</label>
                                                                            <input class=" calander medical_date form-control"
                                                                                   type="text"
                                                                                   name='medical_date'>
                                                                            <span class="flast_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <input type="submit" name="submit"
                                                                               class="blue-btn" value="Save">
                                                                        <input type="button" name="Cancel"
                                                                               value="Cancel"
                                                                               class="grey-btn cancelbtn">
                                                                    </div>
                                                                </div>


                                                            </form>
                                                            <!-- append medical listing -->
                                                            <div class="apx-table">
                                                                <div class="table-responsive">
                                                                    <table id="TenantMedical-table"
                                                                           class="table table-bordered">
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="edit-foot edit_medical_button">
                                                                <a href="javascript:;">
                                                                    <i class="fa fa-pencil-square-o"
                                                                       aria-hidden="true"></i>
                                                                    Edit
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Form Outer 5 Ends -->


                                            <div class="form-outer  main_guarantor">
                                                <div class="form-hdr">
                                                    <h3 class="pull-left">Guarantar</h3>
                                                    <input type="button" value="Add New Guarantor"
                                                           class="addbtn blue-btn pull-right" id="add_button_gurantor" style="display: none">


                                                </div>
                                                <div class="form-data">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="property_guarantor">

                                                                <form id="addEditGuarantor" method="POST"
                                                                      enctype="multipart/form-data"
                                                                      style="display:none;">
                                                                    <input type="hidden" name="main_guarantor_action"
                                                                           class="main_guarantor_action">
                                                                    <input type="hidden" name="guarantor_id"
                                                                           class="guarantor_id">
                                                                    <div class="row">
                                                                        <div class="property-status  form-outer2">
                                                                            <div class="col-sm-3 col-md-3">
                                                                                <input class="guarantor_entity"
                                                                                       type="checkbox"
                                                                                       name='guarantor_entity'><strong
                                                                                    class='font-weight-bold'>Check
                                                                                    this box if this
                                                                                    Guarantor is entity/company</strong>
                                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                            </div>

                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="property_guarantor_form1">


                                                                        <input type='hidden'
                                                                               name='clone_guarantor_form1'
                                                                               class="clone_guarantor_form1">
                                                                        <div class="row">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Salutation</label>
                                                                                    <select class="form-control"
                                                                                            id="guarantor_salutation"
                                                                                            name="guarantor_salutation">
                                                                                        <option value="Select">Select</option>
                                                                                        <option value="Dr.">Dr.</option>
                                                                                        <option value="Mr.">Mr.</option>
                                                                                        <option value="Mrs.">Mrs.</option>
                                                                                        <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                                                                        <option value="Ms.">Ms.</option>
                                                                                        <option value="Sir">Sir</option>
                                                                                        <option value="Madam">Madam</option>
                                                                                        <option value="Brother">Brother</option>
                                                                                        <option value="Sister">Sister</option>
                                                                                        <option value="Father">Father</option>
                                                                                        <option value="Mother">Mother</option>
                                                                                    </select>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>First Name</label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_firstname"
                                                                                           name="guarantor_firstname" maxlength="30">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Middle Name</label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_middlename"
                                                                                           name="guarantor_middlename" maxlength="30">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Last Name</label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_lastname"
                                                                                           name="guarantor_lastname" maxlength="30">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Relationship</label>
                                                                                    <select class="form-control"
                                                                                            id="guarantor_relationship"
                                                                                            name="guarantor_relationship">
                                                                                        <option value="">Select</option>
                                                                                        <option value="1">Daughter
                                                                                        </option>
                                                                                        <option value="2">Father
                                                                                        </option>
                                                                                        <option value="3">Friend
                                                                                        </option>
                                                                                        <option value="4">Mother
                                                                                        </option>
                                                                                        <option value="5">Owner</option>
                                                                                        <option value="6">Partner
                                                                                        </option>
                                                                                        <option value="7">Son</option>
                                                                                        <option value="8">Spouse
                                                                                        </option>
                                                                                        <option value="9">Other</option>
                                                                                    </select>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Zip/Postal Code</label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_zipcode"
                                                                                           name="guarantor_zipcode" maxlength="30">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3 countycodediv">
                                                                                    <label>Country</label>
                                                                                    <select class="form-control"
                                                                                            name="guarantor_country"
                                                                                            id="guarantor_country">
                                                                                        <option value="">Select</option>
                                                                                    </select>
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>State/Province</label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_province"
                                                                                           name="guarantor_province" maxlength="30">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>City <em
                                                                                            class="red-star">*</em></label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_city"
                                                                                           name="guarantor_city" maxlength="30">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Address1 <em
                                                                                            class="red-star">*</em></label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_address1"
                                                                                           name="guarantor_address1" maxlength="50">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Address2 <em
                                                                                            class="red-star">*</em></label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_address2"
                                                                                           name="guarantor_address2" maxlength="50">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Address3 <em
                                                                                            class="red-star">*</em></label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_address3"
                                                                                           name="guarantor_address3" maxlength="50">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Address4 <em
                                                                                            class="red-star">*</em></label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_address4"
                                                                                           name="guarantor_address4" maxlength="50">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>


                                                                        <div id="guarantor_phoneInfo"></div>


                                                                        <div id="guarantor_generalEmails"></div>


                                                                        <div class="row">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>SSN/SIN/ID </label>
                                                                                    <input class="form-control add-input capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_ssn"
                                                                                           name="guarantor_ssn">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>

                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Years of Guarantee </label>
                                                                                    <select class="form-control capsOn"
                                                                                            type="text"
                                                                                            id="guarantor_guarantee"
                                                                                            name="guarantor_guarantee">
                                                                                        <option value="">Select</option>
                                                                                        <option value="1">1</option>
                                                                                        <option value="2">2</option>
                                                                                        <option value="3">3</option>
                                                                                        <option value="4">4</option>
                                                                                        <option value="5">5</option>
                                                                                        <option value="6">6</option>
                                                                                    </select>
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>

                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Note</label>
                                                                                    <textarea
                                                                                        class="form-control capsOn"
                                                                                        id="guarantor_note"
                                                                                        name="guarantor_note"></textarea>
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>

                                                                                <!--                                                                        <a class="add-icon guarantor-remove-sign" href="javascript:;"><i-->
                                                                                <!--                                                                                    class="fa fa-plus-circle" aria-hidden="true"></i></a>-->

                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>

                                                                    </div>


                                                                    <div class="property_guarantor_form2"
                                                                         style="display:none">
                                                                        <input type='hidden'
                                                                               class='clone_guarantor_form2'>
                                                                        <div class="row">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Name of
                                                                                        Entity/Company</label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_form2_entity"
                                                                                           name="guarantor_form2_entity">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Zip/Postal Code</label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_form2_postalcode"
                                                                                           name="guarantor_form2_postalcode">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3 countycodediv">
                                                                                    <label>Country</label>
                                                                                    <select class="form-control"
                                                                                            name="guarantor_form2_country"
                                                                                            id="guarantor_form2_country">
                                                                                        <option value="1">Select
                                                                                        </option>
                                                                                        <option value="1">Select
                                                                                        </option>
                                                                                    </select>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>State/province</label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_form2_province"
                                                                                           name="guarantor_form2_province">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>City <em
                                                                                            class="red-star">*</em></label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_form2_city"
                                                                                           name="guarantor_form2_city">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Address1 <em
                                                                                            class="red-star">*</em></label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_form2_address1"
                                                                                           name="guarantor_form2_address1">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Address2 <em
                                                                                            class="red-star">*</em></label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_form2_address2"
                                                                                           name="guarantor_form2_address2">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Address3 <em
                                                                                            class="red-star">*</em></label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_form2_address3"
                                                                                           name="guarantor_form2_address3">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Address4 <em
                                                                                            class="red-star">*</em></label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_form2_address4"
                                                                                           name="guarantor_form2_address4">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>

                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="form-outer">

                                                                                <!--      <div class="multipleEmail-form2">
                                                                                 <div class="col-sm-3 col-md-3">
                                                                                     <label>Email</label>

                                                                                     <input class="form-control capsOn" type="text"
                                                                                            id="guarantor_form2_email"
                                                                                            name="guarantor_form2_email">
                                                                                     <a class="add-icon email-form2-remove-sign"
                                                                                        href="javascript:;"><i class="fa fa-minus-circle"
                                                                                                               aria-hidden="true"></i></a>

                                                                                     <a class="add-icon email-form2-plus-sign"
                                                                                        href="javascript:;"><i class="fa fa-plus-circle"
                                                                                                               aria-hidden="true"></i></a>

                                                                                 </div>
                                                                             </div> -->


                                                                                <div id="guarantor_generalEmailForm2"></div>


                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>RelationShip</label>
                                                                                    <select class="form-control"
                                                                                            id="guarantor_form2_relationship"
                                                                                            name="guarantor_form2_relationship">
                                                                                        <option value="">Select</option>
                                                                                        <option value="1">Daughter
                                                                                        </option>
                                                                                        <option value="2">Father
                                                                                        </option>
                                                                                        <option value="3">Friend
                                                                                        </option>
                                                                                        <option value="4">Mother
                                                                                        </option>
                                                                                        <option value="5">Owner</option>
                                                                                        <option value="6">Partner
                                                                                        </option>
                                                                                        <option value="7">Son</option>
                                                                                        <option value="8">Spouse
                                                                                        </option>
                                                                                        <option value="9">Other</option>
                                                                                    </select>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Entity FID/ID Number</label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_form2_fid"
                                                                                           name="guarantor_form2_fid">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Main Contact Person's First
                                                                                        Name</label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_form2_mainContact"
                                                                                           name="guarantor_form2_mainContact">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Middle Name</label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_form2_middlename"
                                                                                           name="guarantor_form2_middlename">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Last Name</label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_form2_lastname"
                                                                                           name="guarantor_form2_lastname">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>


                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Email</label>
                                                                                    <input class="form-control capsOn"
                                                                                           type="text"
                                                                                           id="guarantor_form2_email" maxlength="100"
                                                                                           name="guarantor_form2_email2">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>


                                                                        <div id="guarantor_form2_phoneInfo"></div>

                                                                        <div class="row">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-3 col-md-3 ">
                                                                                    <label>Year Of Guarantee<em
                                                                                            class="red-star">*</em></label>
                                                                                    <select class="form-control"
                                                                                            id="guarantor_form2_guarantee"
                                                                                            name="guarantor_form2_guarantee">
                                                                                        <option value="">Select</option>
                                                                                        <option value="1">1</option>
                                                                                        <option value="2">2</option>
                                                                                        <option value="3">3</option>
                                                                                        <option value="4">4</option>
                                                                                        <option value="5">5</option>
                                                                                        <option value="6">6</option>
                                                                                    </select>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>

                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Note</label>
                                                                                    <textarea
                                                                                        class="form-control capsOn"
                                                                                        type="text"
                                                                                        id="guarantor_form2_note"
                                                                                        name="guarantor_form2_note"></textarea>
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>

                                                                                <div class="col-sm-3 col-md-3">
                                                                                    <label>Files</label>
                                                                                    <input class="form-control"
                                                                                           type="file"
                                                                                           id="guarantor_form2_files"
                                                                                           name="guarantor_form2_files">
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <a class="add-icon guarantor-form2-plus-sign"
                                                                                   href="javascript:;"><i
                                                                                        class="fa fa-plus-circle"
                                                                                        aria-hidden="true">+</i></a>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <input class="blue-btn" type="submit"
                                                                                   name="" value="Save">
                                                                            <input type="button" name="Cancel"
                                                                                   value="Cancel"
                                                                                   class="cancelbtn grey-btn">
                                                                        </div>
                                                                    </div>


                                                                </form>
                                                            </div>
                                                            <div class="apx-table" style="float: left;width: 100%;">
                                                                <div class="table-responsive">
                                                                    <table id="TenantGuarantor-table"
                                                                           class="table table-bordered">
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="edit-foot edit_guarantor_button">
                                                                <a href="javascript:;">
                                                                    <i class="fa fa-pencil-square-o"
                                                                       aria-hidden="true"></i>
                                                                    Edit
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Form Outer 6 Ends -->


                                            <div class="form-outer  collection_main">
                                                <div class="form-hdr">
                                                    <h3 class="pull-left">Collections</h3>
                                                    <input type="button" value="Add New Collection"
                                                           class="addbtn blue-btn pull-right" id="add_collection_button" style="display: none">
                                                </div>
                                                <div class="form-data">

                                                    <div id="collection_main">
                                                        <form id="addEditCollection" method="post"
                                                              style="display:none;">
                                                            <input type="hidden" class="collection_action"
                                                                   name="collection_action">
                                                            <input type="hidden" class="collection_id"
                                                                   name="collection_id">
                                                            <div class="form-outer">
                                                                <div class="row">
                                                                    <div class="col-sm-12 col-md-3">
                                                                        <label>Collection ID </label>
                                                                        <input class="form-control capsOn" type="text"
                                                                               id="collection_collectionId"
                                                                               name="collection_collectionId">
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-3">
                                                                        <label>Reason <em
                                                                                class="red-star">*</em></label>
                                                                        <select class="form-control"
                                                                                id="collection_reason"
                                                                                name="collection_reason">
                                                                            <option value="1">test</option>
                                                                        </select>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-3">
                                                                        <label>Description</label>
                                                                        <textarea class="form-control capsOn"
                                                                                  id="collection_description"
                                                                                  name="collection_description"></textarea>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-3">
                                                                        <label>Status <em
                                                                                class="red-star">*</em></label>
                                                                        <select class="form-control"
                                                                                id="collection_status"
                                                                                name="collection_status">
                                                                            <option value="">Select</option>
                                                                            <option value="1">Open</option>
                                                                            <option value="2">Close</option>
                                                                        </select>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-3">
                                                                        <label>Amount Due(KZT)</label>
                                                                        <input class="form-control capsOn" type="text"
                                                                               id="collection_amountDue"
                                                                               name="collection_amountDue" maxlength="30">
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-3">
                                                                        <label>Note</label>
                                                                        <textarea class="form-control capsOn"
                                                                                  type="text"
                                                                                  id="collection_note"
                                                                                  name="collection_notes"></textarea>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <input type="submit" name="submit" class="blue-btn"
                                                                       value="Save">
                                                                <input type="button" name="Cancel" value="Cancel"
                                                                       class="grey-btn cancelbtn">
                                                            </div>

                                                        </form>

                                                    </div>
                                                    <div class="apx-table clear">
                                                        <div class="table-responsive">
                                                            <table id="TenantCollection-table"
                                                                   class="table table-bordered">
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="edit-foot edit_button_collection">
                                                        <a href="javascript:;">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </div>


                                                </div>
                                            </div>
                                            <!-- Form Outer 7 Ends -->


                                            <div class="form-outer  parking_main">
                                                <div class="form-hdr">
                                                    <h3 class="pull-left">Parking Spaces</h3>
                                                    <input type="button" value="Add New Space"
                                                           class="addbtn blue-btn pull-right" id="add_button_parking" style="display: none">
                                                </div>
                                                <div class="form-data">
                                                    <form id="addEditParking" method="post" style="display:none;">
                                                        <input type="hidden" class="parking_action"
                                                               name="parking_action">
                                                        <div class="form-outer spaceNumber">

                                                            <div class="row">

                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Parking Space Number<em
                                                                            class="red-star">*</em></label>
                                                                    <input class="form-control capsOn" type="text"
                                                                           id="parking_space" name="parking_space" maxlength="30">
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Parking Permit Number<em
                                                                            class="red-star">*</em></label>
                                                                    <input class="form-control capsOn" type="text"
                                                                           id="parking_number" name="parking_number" maxlength="30">
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>
                                                            </div>
                                                            <input type="submit" name="submit" value="Save"
                                                                   class="blue-btn">
                                                            <input type="button" name="Cancel" value="Cancel"
                                                                   class="grey-btn cancelbtn">
                                                        </div>

                                                    </form>
                                                    <div class="apx-table clear">
                                                        <div class="table-responsive">
                                                            <table id="TenantParking-table"
                                                                   class="table table-bordered">
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="edit-foot edit_button_parking">
                                                        <a href="javascript:;">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Form Outer 8 Ends -->


                                            <div class="form-outer ">
                                                <div class="form-hdr">
                                                    <h3>Custom Fields</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="row ">
                                                        <div class="form-outer ">
                                                            <div class="custom_field_html">
                                                            </div>
                                                            <div class="clearfix "></div>
                                                        </div>
                                                    </div>
                                                    <button type="button" data-toggle="modal"
                                                            data-backdrop="static" data-target="#myModal" class="blue-btn pull-right">Add
                                                        Custom Field
                                                    </button>
                                                </div>
                                            </div>
                                            <!-- Form Outer Ends -->

                                        </div>
                                        <!-- Tab 2 Ends -->

                                        <div role="tabpanel" class="tab-pane" id="tenant-detail-three">
                                            <div class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Charges</h3>
                                                </div>
                                                <div class="form-data chargeTaxDetails">
                                                    <div class="detail-outer">
                                                        <div class="col-sm-6">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <strong>Late Fee Charges : One Time</strong>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Flat Fee (USh) :</label>
                                                                    <span class="onetimeFlatFee">0.00</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">% of Monthly Rent
                                                                        :</label>
                                                                    <span class="onetimeMonthlyRent">0</span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <strong>Late Fee Charges : Daily</strong>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Flat Fee (USh) :</label>
                                                                    <span class="dailyFlatFee">0.00</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">% of Monthly Rent
                                                                        :</label>
                                                                    <span class="dailyMonthlyRent">0</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <strong>Tax Pass Through Details</strong>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right"></label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right"></label>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <strong>Flat Amount</strong>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Amount (USh) :</label>
                                                                    <span class="tax_value"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Tax Name :</label>
                                                                    <span class="tax_name"></span>
                                                                </div>
                                                                <a href="javascript:;" class="taxChargeBtn pull-right">
                                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                    Edit
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="chargeTaxForm" style="display:none">
                                                    <form id="addLateFee" method="post">
                                                        <div class="form-outer form-outer2">
                                                            <div class="form-hdr">
                                                                <h3>Late Fee Charges</h3>
                                                            </div>
                                                            <div class="form-data">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="check-outer">
                                                                            <input type="checkbox" name="oneTime" class="applyOneTime" value="0"/> <label>Apply One Time</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="radio-input-label">
                                                                            <input type="radio" class="oneTimeRadio1 oneTimeRadio" name="oneTimeRadio" disabled value="1"/><input type="text"  class="oneTimeFeeLabel removeAttr" maxlength="5" disabled/> <label>Flat Fee (KZT)
                                                                                (Example 10.00)</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="radio-input-label">
                                                                            <input type="radio" class="oneTimeRadio2 oneTimeRadio" name="oneTimeRadio" value="0"  disabled/><input  type="text"   class="oneTimeRentLabel removeAttr" maxlength="5" disabled/> <label>% of Monthly Rent
                                                                                (Example 20)</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="check-outer">
                                                                            <input type="checkbox" name="daily" class="applyDaily" value="0"/> <label>Apply Daily</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="radio-input-label">
                                                                            <input type="radio" name="dailyRadio" value="1" class="dailyRadio1 dailyRadio" disabled/><input  type="text"  class="dailyFeeLabel removeAttr" maxlength="5"/> <label>Flat Fee (KZT)
                                                                                (Example 10.00)</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="radio-input-label">
                                                                            <input type="radio" name="dailyRadio" value="0" class="dailyRadio2 dailyRadio" disabled/><input  type="text"  class="dailyRentLabel removeAttr" maxlength="5"/> <label>% of Monthly Rent
                                                                                (Example 20)</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="radio-input-label">
                                                                            <label>Grace Period </label> <input  type="text" name="gracePeriod"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="btn-outer">
                                                                    <button class="blue-btn">Apply</button>
                                                                    <button class="grey-btn cancelBtn">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>

                                                    <div class="form-data">
                                                        <form id="addTaxDetails" method="post">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Tax Name <em class="red-star">*</em></label>
                                                                    <input class="form-control capital" type="text" name="tax_name"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Type <em class="red-star">*</em></label>
                                                                    <select class="form-control tax_type" name="tax_type">
                                                                        <option value="">Select</option>
                                                                        <option value="P">Percentage</option>
                                                                        <option value="F">Flat</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Value<em class="pSign">()</em> <em class="red-star">*</em></label>
                                                                    <input class="form-control amount number_only" type="text" id="tax_value" name="tax_value"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Select Charge Code <em class="red-star">*</em>
                                                                        <a class="pop-add-icon getchargeajax" href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#addchargescode">
                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                        </a>
                                                                    </label>
                                                                    <select class="form-control tax_chargeCode" name="tax_chargeCode">
                                                                        <option value="">Select</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="btn-outer">
                                                                <button class="blue-btn">Apply</button>
                                                                <button class="grey-btn cancelBtn">Cancel</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                            </div>
                                            <style>
                                                td, th {
                                                    border: 1px solid #dddddd;
                                                    text-align: left;
                                                    padding: 8px;
                                                }
                                            </style>
                                            <!-- Form outer ENds-->


                                            <div class="form-outer ">

                                                <div class="">
                                                    <div class="form-outer">


                                                        <div>
                                                            <div class="chargeForm" style="display:none;">


                                                                <div class="form-hdr "><h3>List of Charge</h3></div>
                                                                <div class="form-data">
                                                                    <form id="addChargeForm" class="form-outer">


                                                                        <div class="col-sm-3 col-md-3">
                                                                            <label>Select Charge Code <em
                                                                                    class="red-star">*</em>
                                                                                <a class="pop-add-icon getchargeajax"
                                                                                   href="javascript:;"
                                                                                   data-toggle="modal"
                                                                                   data-backdrop="static"
                                                                                   data-target="#addchargescode">
                                                                                    <i class="fa fa-plus-circle"
                                                                                       aria-hidden="true"></i>
                                                                                </a>
                                                                            </label>
                                                                            <select class="form-control tax_chargeCode"
                                                                                    name="chargeCode">
                                                                                <option value="">Select</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-3 col-md-3">
                                                                            <label>Frequency <em class="red-star">*</em></label>
                                                                            <select class="form-control"
                                                                                    name="frequency">

                                                                                <option value="One Time">One Time
                                                                                </option>
                                                                                <option value="Monthly2">Monthly
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-3 col-md-3">
                                                                            <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$'; ?>
                                                                            <label>Amount (<?php echo $default_symbol ?>)</label>
                                                                            <input class="form-control capsOn"
                                                                                   type="text"
                                                                                   id="amount" name="amount"
                                                                                   maxlength="5">
                                                                            <span class="flast_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-sm-3 col-md-3">
                                                                            <label>Start Date</label>
                                                                            <input class="form-control capsOn calander"
                                                                                   type="text"
                                                                                   id="startDate" name="startDate"
                                                                                   readonly>
                                                                            <span class="flast_nameErr error red-star"></span>
                                                                        </div>

                                                                        <div class="col-sm-3 col-md-3">
                                                                            <label>End Date</label>
                                                                            <input class="form-control capsOn calander"
                                                                                   type="text"
                                                                                   id="birth" name="endDate" readonly>
                                                                            <span class="flast_nameErr error red-star"></span>

                                                                        </div>
                                                                        <div class="col-sm-3 col-md-3 btn-flex">
                                                                            <button class="blue-btn">Add</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <div class="form-data">
                                                                <div class="form-hdr">
                                                                    <h3>List of Charges</h3>
                                                                </div>
                                                                <div class="col-sm-2 pull-right" style="display: none; min-height: 10px;"><button class="blue-btn" id="addChargeButton">Add Charge</button></div>
                                                                <div class="chargeData"></div>
                                                                <a href="javascript:;" class="ChargeBtn1 pull-right">
                                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                    Edit
                                                                </a>
                                                            </div>


                                                        </div>

                                                    </div>


                                                </div>
                                            </div>


                                        </div>
                                        <!-- Tab 3 Ends -->

                                        <div role="tabpanel" class="tab-pane" id="tenant-detail-four">
                                            <div class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Receive Payments
                                                        <input type="button" value="Receive Payments"
                                                               class="blue-btn pull-right"></h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="grid-outer">
                                                        <div class="table-responsive">
                                                            <table class="table table-hover table-dark">
                                                                <thead>
                                                                <tr>
                                                                    <th scope="col">Date</th>
                                                                    <th scope="col">Description</th>
                                                                    <th scope="col">Charge Code</th>
                                                                    <th scope="col">Original Amount (USh)</th>
                                                                    <th scope="col">Date (USh)</th>
                                                                    <th scope="col">Date (USh)</th>
                                                                    <th scope="col">Amount Waived Off (USh)</th>
                                                                    <th scope="col">Waive off Comment</th>
                                                                    <th scope="col">Wave off Amount (USh)</th>
                                                                    <th scope="col">Current Payment (USh)</th>
                                                                    <th scope="col">Priority</th>
                                                                    <th scope="col">Pay</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- Tab 4 Ends -->

                                        <div role="tabpanel" class="tab-pane" id="tenant-detail-five">
                                            <div class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Ledger</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="property-status">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <label>Start Date</label>
                                                                <span><input class="form-control" type="text"/></span>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>End Date</label>
                                                                <span><input class="form-control" type="text"/></span>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>&nbsp;</label>
                                                                <span><button class="blue-btn">Search</button></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="grid-outer">
                                                        <div class="table-responsive">
                                                            <table class="table table-hover table-dark">
                                                                <thead>
                                                                <tr>
                                                                    <th scope="col">Date</th>
                                                                    <th scope="col">Transaction Code</th>
                                                                    <th scope="col">Transaction Description</th>
                                                                    <th scope="col">Debit (USh)</th>
                                                                    <th scope="col">Credit (USh)</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td>June 06, 2019 (Thu.)</td>
                                                                    <td>EHGDYJ</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>10.00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>June 06, 2019 (Thu.)</td>
                                                                    <td>EHGDYJ</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>10.00</td>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Tab 5 Ends -->

                                        <div role="tabpanel" class="tab-pane" id="tenant-detail-six">
                                            <div class="form-outer occupancy_div" style="display: none;">
                                                <div class="form-hdr"><h3>Occupants</h3></div>
                                                <div class="form-data">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <form id="occupancy_Tenant" method="post">
                                                                <div class="col-sm-2">
                                                                    <label>Salutation</label>
                                                                    <select class="form-control"
                                                                            name="salutation_occupancy">
                                                                        <option value="Select">Select</option>
                                                                        <option value="Dr.">Dr.</option>
                                                                        <option value="Mr.">Mr.</option>
                                                                        <option value="Mrs.">Mrs.</option>
                                                                        <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                                                        <option value="Ms.">Ms.</option>
                                                                        <option value="Sir">Sir</option>
                                                                        <option value="Madam">Madam</option>
                                                                        <option value="Brother">Brother</option>
                                                                        <option value="Sister">Sister</option>
                                                                        <option value="Father">Father</option>
                                                                        <option value="Mother">Mother</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <input type="hidden" name="hidden_id"
                                                                           id="hidden_id">
                                                                    <label>First Name <em
                                                                            class="red-star">*</em></label>
                                                                    <input class="form-control capital" type="text"
                                                                           name="firstname_occupancy">
                                                                    <span id="first_nameErr" class="red-star"></span>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Middle Name</label>
                                                                    <input class="form-control capital" type="text"
                                                                           name="middlename_occupancy">
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Last Name <em class="red-star">*</em></label>
                                                                    <input class="form-control capital" type="text"
                                                                           name="lastname_occupancy">
                                                                    <span id="last_nameErr" class="red-star"></span>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Maiden Name</label>
                                                                    <input class="form-control capital" type="text"
                                                                           name="medianName_occupancy">
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Nickname <em class="red-star">*</em></label>
                                                                    <input class="form-control capital" type="text"
                                                                           name="nickname_occupancy">
                                                                </div>
                                                                <div class="col-sm-2 clear">
                                                                    <label>Gender</label>
                                                                    <select class="form-control" type="text"
                                                                            name="gender_occupancy" id="occ_gender">
                                                                        <option value="0">select</option>
                                                                        <option value="1">Male</option>
                                                                        <option value="2">Female</option>
                                                                        <option value="3">Prefer not to say</option>
                                                                        <option value="4">Other</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>SSN/SIN/ID
                                                                        <input class="form-control"
                                                                               name="sinn_occupancy"
                                                                               id="sinn_occupancy">
                                                                </div>

                                                                <div class="col-sm-2">
                                                                    <label>Ethnicity <a class="pop-add-icon eccupantPropertyEthnicity1" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                    <select class="form-control"
                                                                            name="ethncity_occupancy"
                                                                            id="ethncity_occupancy">
                                                                        <option></option>
                                                                    </select>
                                                                    <div class="add-popup" id="eccupantPropertyEthnicity1">
                                                                        <h4>Add New Ethnicity</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>New Ethnicity <em class="red-star">*</em></label>
                                                                                    <input class="form-control ethnicity_src customValidateGroup" type="text" placeholder="Add New Ethnicity" data_required="true" >
                                                                                    <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                                                </div>
                                                                                <div class="btn-outer">
                                                                                    <button type="button" class="blue-btn add_single1"  data-table="tenant_ethnicity" data-cell="title" data-class="ethnicity_src" data-name="ethncity_occupancy">Save</button>
                                                                                    <input type="button" class="grey-btn" value="Cancel">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Martial Status <a class="pop-add-icon occupantPropertyMaritalStatus1" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                    <select class="form-control"
                                                                            name="maritalStatus_occupancy"
                                                                            id="maritalStatus_occupancy">
                                                                        <option value="0">select</option>
                                                                        <option value="1">single</option>
                                                                        <option value="2">married</option>
                                                                    </select>
                                                                    <div class="add-popup" id="occupantPropertyMaritalStatus1">
                                                                        <h4>Add New Marital Status</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>New Marital Status <em class="red-star">*</em></label>
                                                                                    <input class="form-control maritalstatus_src" type="text" placeholder="Add New Marital Status">
                                                                                    <span class="customError required red-star" aria-required="true" id="maritalstatus_src"></span>
                                                                                </div>
                                                                                <div class="btn-outer">
                                                                                    <button type="button" class="blue-btn add_single1" data-table="tenant_marital_status" data-cell="marital" data-class="maritalstatus_src" data-name="maritalStatus_occupancy">Save</button>
                                                                                    <input type="button" class="grey-btn" value="Cancel">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Hobbies <a class="pop-add-icon occupantselectPropertyHobbies1" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                    <select class="form-control"
                                                                            name="edit_general_hobby_occupancy">
                                                                        <option value="0">select</option>
                                                                        <option value="1">shopping</option>
                                                                        <option value="2">playing</option>
                                                                    </select>
                                                                    <div class="add-popup" id="occupantselectPropertyHobbies1">
                                                                        <h4>Add New Hobbies</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>New Hobbies <em class="red-star">*</em></label>
                                                                                    <input class="form-control hobbies_src" type="text" placeholder="Add New Hobbies" data_required="true">
                                                                                    <span class="red-star" id="hobbies_src"></span>
                                                                                </div>
                                                                                <div class="btn-outer">
                                                                                    <button type="button" class="blue-btn add_single1" data-table="hobbies" data-cell="hobby" data-class="hobbies_src" data-name="edit_general_hobby_occupancy">Save</button>
                                                                                    <input type="button" class="grey-btn" value="Cancel">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Veteran Status <a class="pop-add-icon occupantPropertyVeteranStatus1" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                    <select class="form-control"
                                                                            name="edit_general_veteran_occupancy">
                                                                        <option value="0">select</option>
                                                                        <option value="1">veteran</option>
                                                                        <option value="2">newly</option>
                                                                    </select>
                                                                    <div class="add-popup" id="occupantPropertyVeteranStatus1">
                                                                        <h4>Add New VeteranStatus</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>New VeteranStatus <em class="red-star">*</em></label>
                                                                                    <input class="form-control veteran_src" type="text" placeholder="Add New VeteranStatus">
                                                                                    <span class="red-star" id="veteran_src"></span>
                                                                                </div>
                                                                                <div class="btn-outer">
                                                                                    <button type="button" class="blue-btn add_single1" data-table="tenant_veteran_status" data-cell="veteran" data-class="veteran_src" data-name="edit_general_veteran_occupancy">Save</button>
                                                                                    <input type="button" class="grey-btn" value="Cancel">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-2">
                                                                    <label>Relationship</label>
                                                                    <select class="form-control" type="text"
                                                                            name="rela_occupancy">
                                                                        <option value="0">select</option>
                                                                        <option value="1">Father</option>
                                                                        <option value="2">Mother</option>
                                                                        <option value="3">Daughter</option>
                                                                        <option value="4">Friend</option>
                                                                        <option value="5">Owner</option>
                                                                        <option value="6">Partner</option>
                                                                        <option value="7">Son</option>
                                                                        <option value="8">Spouse</option>
                                                                        <option value="9">Other</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Referral Source <a class="pop-add-icon occupantadditionalReferralResource1" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                    <select class="form-control" type="text"
                                                                            name="referal_occupancy">
                                                                        <option value="0">select</option>
                                                                        <option value="1">coworker</option>
                                                                    </select>
                                                                    <div class="add-popup" id="occupantadditionalReferralResource1">
                                                                        <h4>Add New Referral Source</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>New Referral Source <em class="red-star">*</em></label>
                                                                                    <input class="form-control reff_source1" type="text" placeholder="New Referral Source">
                                                                                    <span class="red-star" id="reff_source1"></span>
                                                                                </div>
                                                                                <div class="btn-outer">
                                                                                    <button type="button" class="blue-btn add_single1" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source1" data-name="referal_occupancy">Save</button>
                                                                                    <input type="button" class="grey-btn" value="Cancel">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <input type="checkbox" name="responsible_occupants"
                                                                           id="responsible_occupants" value="1"/>
                                                                    <label>Financially Responsible</label>

                                                                </div>
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-hover table-dark occcupants_table1" id="occcupants_table1">
                                                                        </table>
                                                                    </div>
                                                                </div>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div class>
                                            <div class="form-outer occupancy_div" style="display: none;">
                                                <div class="form-hdr"><h3>Emergency Contact Details</h3></div>
                                                <div class="form-data">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-2">
                                                                <label>Emergency Contact Name</label>
                                                                <input class="form-control capital"
                                                                       name="contact_emer_occupancy" maxlength="50">

                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Relationship</label>
                                                                <select class="form-control" type="text"
                                                                        name="rela_emer_occupancy">
                                                                    <option value="0">select</option>
                                                                    <option value="1">Father</option>
                                                                    <option value="2">Mother</option>
                                                                    <option value="3">Daughter</option>
                                                                    <option value="4">Friend</option>
                                                                    <option value="5">Owner</option>
                                                                    <option value="6">Partner</option>
                                                                    <option value="7">Son</option>
                                                                    <option value="8">Spouse</option>
                                                                    <option value="9">Other</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Country Code</label>
                                                                <select class="form-control countryCode_emer_occupancy"
                                                                        type="text" name="countryCode_emer_occupancy"
                                                                        id="countryCode_emer_occupancy">
                                                                    <option value="0">hsfj</option>
                                                                </select>
                                                            </div>

                                                            <div class="col-sm-2">
                                                                <label>Phone Number</label>
                                                                <input class="form-control phone_format" name="ph_emer_occupancy" maxlength="12">

                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Email</label>
                                                                <input class="form-control" name="email_emer_occupancy" maxlength="50">

                                                            </div>

                                                            <div class="btn-outer">

                                                                <a class="blue-btn" id="add_occupany_button">Update</a>
                                                                <button class="grey-btn" id="cancel_occupancy">Cancel
                                                                </button>
                                                            </div>
                                                            <div class="apx-table">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark occcupants_table1">

                                                                    </table>
                                                                </div>
                                                            </div>

                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div class>
                                            <div class="form-outer form-outer2 add_occupancy_div">
                                                <div class="form-hdr">
                                                    <h3>Occupants
                                                        <a href="#" class="blue-btn pull-right" id="add_occupant"
                                                           style="padding-top: 8px; display: none;">Add New Occupant</a>
                                                    </h3>

                                                </div>
                                                <div class="form-data">
                                                    <div class="grid-outer">
                                                        <div class="apx-table">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-dark occcupants_table">

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">&nbsp;</div>
                                                    <div class="form-outer">
                                                        <div class="form-hdr">
                                                            <h3>Emergency Contact Details</h3>
                                                        </div>
                                                        <input type="hidden" value="<?php echo $_GET['tenant_id']; ?>"
                                                               name="tenantapplytaxid" class="tenantapplytaxid">
                                                        <div class="form-data">
                                                            <div class="detail-outer">
                                                                <div class="col-sm-6">
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <label class="text-right">Emergency Contact
                                                                                Name
                                                                                :</label>
                                                                            <span id="emergency_detail"></span>
                                                                        </div>

                                                                        <div class="col-xs-12">
                                                                            <label class="text-right"> Country Code
                                                                                :</label>
                                                                            <span id="country_code"></span>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <label class="text-right">Emergency Contact
                                                                                Phone :</label>
                                                                            <span id="emergency_contact"></span>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <label class="text-right"> Emergency Contact
                                                                                Email :</label>
                                                                            <span id="emergency_contact_email"></span>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <label class="text-right">Emergency Contact
                                                                                Relationship :</label>
                                                                            <span id="relationship_contact"></span>
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <span class="pull-right" id='edit_occupants'
                                                          style='cursor: pointer;'><img
                                                            src='/company/images/edit-icon.png' title='Edit'
                                                            alt='my image' '/> Edit</span>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- Tab 6 Ends -->

                                        <div role="tabpanel" class="tab-pane" id="tenant-detail-seven">
                                            <div class="form-outer notes_div" style="display: none;">
                                                <div class="form-hdr"><h3>Notes and History</h3></div>
                                                <div class="form-data">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <form id="notes_his_form" method="post">
                                                                <div class="row clone_notes">
                                                                    <div class="col-sm-12">
                                                                        <div class="col-sm-11">
                                                                            <textarea class="form-control capital"
                                                                                      name="notes_text"
                                                                                      id="notes_text1"></textarea>
                                                                        </div>
                                                                        <div class="col-sm-1">
                                                                            <a class="pop-add-icon plus-circle"
                                                                               href="javascript:;"><i
                                                                                    class="fa fa-plus-circle"
                                                                                    aria-hidden="true"></i></a>
                                                                            <a class="pop-add-icon minus-circle"
                                                                               href="javascript:;"
                                                                               style="display:none"><i
                                                                                    class="fa fa-minus-circle"
                                                                                    aria-hidden="true"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="btn-outer">

                                                                    <a class="blue-btn"
                                                                       id="add_notes_history">Update</a>
                                                                    <button class="grey-btn" id="cancel_notes_history">
                                                                        Cancel
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div classtyle>
                                            <div class="form-outer notes_grid">
                                                <div class="form-hdr">
                                                    <h3>Notes & History
                                                        <a href="#" class="blue-btn pull-right" id="add_notes"
                                                           style="padding-top: 8px; display: none;">Add Notes</a>
                                                    </h3>
                                                </div>
                                                <div class="form-data" id="notes_jqgrid">
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">


                                                                    <div id="collapseOne"
                                                                         class="panel-collapse collapse  in">
                                                                        <div class="panel-body pad-none">
                                                                            <div class="grid-outer">
                                                                                <div class="apx-table">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark"
                                                                                               id="notes_table">
                                                                                        </table>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="panel panel-default">
                                                                    <span class="pull-right" id='edit_notes'
                                                                          style='cursor: pointer;'><img
                                                                            src='/company/images/edit-icon.png'
                                                                            title='Edit'
                                                                            alt='my image'/> Edit</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                        <!-- Tab 7 Ends -->

                                        <div role="tabpanel" class="tab-pane" id="tenant-detail-eight">
                                            <div class="col-sm-12 files_main">
                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>
                                                            File Library
                                                        </h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="apx-table">
                                                            <form id="addChargeNote">

                                                                <div id="collapseSeventeen" class="panel-collapse in">
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-4 min-height-0 file_main">
                                                                                <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                                                <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">

                                                                                <input type="button" class="orange-btn" id="remove_library_file" value="Remove All Files...">
                                                                                <input type="button" class="saveChargeFile blue-btn" value="Save">
                                                                                <input type="button" class="cancelbtn grey-btn" value="Cancel">
                                                                            </div>
                                                                            <div class="row" id="file_library_uploads">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="TenantFiles-table" class="table table-bordered">
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <!-- Form Outer Ends -->
                                                            </form>
                                                            <div class="table-responsive">

                                                                <table id="TenantFiles-table" class="table table-bordered">
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- Tab 8 Ends -->

                                        <div role="tabpanel" class="tab-pane" id="tenant-detail-nine">
                                            <div class="form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Tenant Portal</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="row">
                                                        <form id="sendEmailTemplate">
                                                            <div class="col-sm-2">
                                                                <label>Email ID <em class="red-star">*</em></label>
                                                                <input class="form-control sendEmail" type="text" name="sendEmail"/>
                                                            </div>
                                                            <div class="col-sm-2 clear">
                                                                <label>&nbsp;</label>
                                                                <input class="blue-btn" type="submit" id="sendEmail" value="Send Password">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- Tab 9 Ends -->


                                        <div role="tabpanel" class="tab-pane renter-container" id="tenant-detail-ten">
                                            <div class="form-outer">
                                                <div class="form-hdr">
                                                    <h3>Renter Insurance</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer renterform" style="display: none;">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div id="collapseOne"
                                                                             class="panel-collapse collapse  in">
                                                                            <div class="pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="form-data">
                                                                                        <form method="POST"
                                                                                              name="renterformfield"
                                                                                              id="renterformfield">
                                                                                            <input type="hidden"
                                                                                                   name="renter_tenant_id"
                                                                                                   value="<?php echo $_GET['tenant_id']; ?>">
                                                                                            <div class="row renterformdata">
                                                                                                <div class="col-sm-11">
                                                                                                    <div class="col-sm-12">
                                                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                            <label>Policy
                                                                                                                Info:</label>
                                                                                                            <input class="form-control capital"
                                                                                                                   name="policyinfo[]"
                                                                                                                   type="text">
                                                                                                            <input name="policyid[]"
                                                                                                                   type="hidden">
                                                                                                        </div>
                                                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                            <label>Insurance
                                                                                                                Provider
                                                                                                                :</label>
                                                                                                            <input class="form-control"
                                                                                                                   type="text"
                                                                                                                   name="provider[]">
                                                                                                        </div>
                                                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                            <label>Status</label>
                                                                                                            <select name="status[]"
                                                                                                                    class="form-control">
                                                                                                                <option value="1">
                                                                                                                    Active
                                                                                                                </option>
                                                                                                                <option value="0">
                                                                                                                    Inactive
                                                                                                                </option>
                                                                                                            </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="col-sm-12">
                                                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                            <label>Policy
                                                                                                                Expiration
                                                                                                                Date
                                                                                                                :</label>
                                                                                                            <input class="form-control"
                                                                                                                   type="text"
                                                                                                                   name="expiration[]">
                                                                                                        </div>
                                                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                            <label>Next
                                                                                                                Renewal
                                                                                                                Date
                                                                                                                :</label>
                                                                                                            <input class="form-control"
                                                                                                                   type="text"
                                                                                                                   name="renewal[]">
                                                                                                        </div>
                                                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                            <label>Effective
                                                                                                                Date
                                                                                                                :</label>
                                                                                                            <input class="form-control"
                                                                                                                   type="text"
                                                                                                                   name="effective[]">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-sm-1">
                                                                                                    <a class="clonerenteradd pop-add-icon"
                                                                                                       href="javascript:;">
                                                                                                        <i class="fa fa-plus-circle"
                                                                                                           aria-hidden="true"></i>
                                                                                                    </a>
                                                                                                    <a class="clonerenterremove pop-add-icon"
                                                                                                       href="javascript:;"
                                                                                                       style="display: none;">
                                                                                                        <i class="fa fa-minus-circle"
                                                                                                           aria-hidden="true"></i>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-sm-12">
                                                                                                <div class="col-sm-12">
                                                                                                    <div class="btn-outer">
                                                                                                        <a class="blue-btn saverenter"
                                                                                                           href="javascript:void(0)">Ufdhpdate</a>
                                                                                                        <a class="grey-btn cancelrenter"
                                                                                                           href="javascript:void(0)">Cancel</a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="accordion-outer renterlisting">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">


                                                                    <div id="collapseOne"
                                                                         class="panel-collapse collapse  in">
                                                                        <div class="panel-body pad-none">
                                                                            <div class="grid-outer">
                                                                                <div class="apx-table">
                                                                                    <table class="" id="renterinsurancetable"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="edit-foot renter-edit-icon">
                                                                        <a href="javascript:;">
                                                                            <i class="fa fa-pencil-square-o"
                                                                               aria-hidden="true"></i>
                                                                            Edit
                                                                        </a>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- Tab 10 Ends -->

                                        <div role="tabpanel" class="tab-pane" id="tenant-detail-eleven">
                                            <div class="form-outer">
                                                <div class="form-hdr">
                                                    <h3>Invoice Receipt</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse"
                                                                                   data-parent="#accordion"
                                                                                   href="#collapseOne"><span
                                                                                        class="pull-right glyphicon glyphicon-menu-up"></span>
                                                                                    List of Invoice Receipts</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne"
                                                                             class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Date
                                                                                                </th>
                                                                                                <th scope="col">Invoice
                                                                                                    Receipt Number
                                                                                                </th>
                                                                                                <th scope="col">Amount
                                                                                                    (USh)
                                                                                                </th>
                                                                                                <th scope="col">Is Mail
                                                                                                    Sent
                                                                                                </th>
                                                                                                <th scope="col">Action
                                                                                                </th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td>May 31, 2019
                                                                                                    (Fri.)
                                                                                                </td>
                                                                                                <td>S1000251</td>
                                                                                                <td>7580.00</td>
                                                                                                <td>Yes</td>
                                                                                                <td>
                                                                                                    <select class="form-control"></select>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- Tab 11 Ends -->

                                        <div role="tabpanel" class="tab-pane" id="tenant-detail-twelve">
                                            <form action="" name="edit_rent_form" method="POST" id="edit_rent_form">
                                                <input name="edit_rent_user_id" type="hidden" class="edit_rent_user_id">
                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>Rent Details</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Rent Due Day</label>
                                                                <select class=" form-control" name="edit_rent_due_date">
                                                                    <option value="0">Select</option>
                                                                    <?php
                                                                    for ($rentDue = 1; $rentDue <= 31; $rentDue++) {
                                                                        echo '<option value="' . $rentDue . '">' . $rentDue . '</option>';
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$'; ?>
                                                                <label>Rent Amount (<?php echo $default_symbol ?>) <em
                                                                        class="red-star">*</em></label>
                                                                <input class="form-control amount number_only"
                                                                       type="text" name="edit_rent_amount"
                                                                       id="edit_rent_amount">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Cam Amount (USh)</label>
                                                                <input class="form-control amount number_only"
                                                                       type="text" name="edit_cam_amount"
                                                                       id="edit_cam_amount">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Security Deposit (USh)</label>
                                                                <input class="form-control amount number_only"
                                                                       type="text" name="edit_security_amount"
                                                                       id="edit_security_amount">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>Rent Increase</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Next Rent Increase</label>
                                                                <select class=" form-control" name="edit_next_rent_inc">
                                                                    <option value="0">Select</option>
                                                                    <option value="1">1 Months</option>
                                                                    <option value="2">2 Months</option>
                                                                    <option value="3">3 Months</option>
                                                                    <option value="6">6 Months</option>
                                                                    <option value="9">9 Months</option>
                                                                    <option value="12">12 Months</option>
                                                                    <option value="15">15 Months</option>
                                                                    <option value="18">18 Months</option>
                                                                    <option value="21">21 Months</option>
                                                                    <option value="24">24 Months</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Rent Increase</label>
                                                                <div class="check-outer">
                                                                    <input type="radio" name="edit_rent_incr"
                                                                           value="flat"/>
                                                                    <label>Flat</label>
                                                                </div>
                                                                <div class="check-outer">
                                                                    <input type="radio" name="edit_rent_incr"
                                                                           value="perc"/>
                                                                    <label>Percentage</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Amount (<?php echo $default_symbol ?>)</label>
                                                                <input class="form-control amount number_only"
                                                                       type="text" name="edit_incr_amount"
                                                                       id="edit_incr_amount">
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer text-right">
                                                                    <a class="blue-btn save_edit_rent">Update</a>
                                                                    <a class="grey-btn cancel_edit_rent">Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>


                                        <div id="imageModel" class="modal fade" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Modal Header</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="col-sm-4" id="getImage1"></div>
                                                        <div class="col-sm-4" id="getImage2"></div>
                                                        <div class="col-sm-4" id="getImage3"></div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- Tab 12 Ends -->

                                        <div role="tabpanel" class="tab-pane complaint_main"
                                             id="tenant-detail-thirteen">
                                            <div class="form-outer">
                                                <div class="form-hdr">
                                                    <h3>Tenant Complaints</h3>
                                                </div>
                                                <div class="form-data">

                                                    <!-- New Complaint Form -->
                                                    <form id="addEditComplaint" style="display:none;">
                                                        <input type="hidden" name="complaint_action"
                                                               class="complaint_action">
                                                        <input type="hidden" name="complaint_unique_id"
                                                               class="complaint_unique_id">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="check-outer">
                                                                    <input type="radio" name="complaint"
                                                                           value="by_tenant" checked=checked> <label>Complaint
                                                                        by Tenant</label>
                                                                </div>
                                                                <div class="check-outer">
                                                                    <input type="radio" name="complaint"
                                                                           value="about_tenant"> <label>Complaint about
                                                                        Tenant</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Complaint ID:</label>
                                                                <span>
                                     <input class="form-control" type="text" name="complaint_id" id="complaint_id" value="<?php echo rand(); ?>"/>
                                              </span>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Complaint Date:</label>
                                                                <span>
                                     <input class="form-control calander" type="text" name="complaint_date"
                                            id="complaint_date"/>
                                              </span>
                                                            </div>

                                                            <div class="col-sm-2">
                                                                <label>Complaint Type:
                                                                    <a class="pop-add-icon companintPopupIcon" href="javascript:;">
                                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                    </a>
                                                                </label>

                                                                <select class="form-control" name="complaint_type" id="complaint_type">
                                                                    <option value="0">Select</option>
                                                                </select>
                                                                <div class="add-popup" id="selectcompanintPopupIcon">
                                                                    <h4>Add New Complaint Type</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>Complaint Type <em class="red-star">*</em></label>
                                                                                <input name="referral" class="form-control customValidateGroup complntClass" type="text" data_required="true" data_max="150" placeholder="Complaint Type">
                                                                                <span class="customError required" aria-required="true" id="complntClass"></span>
                                                                            </div>
                                                                            <div class="btn-outer">
                                                                                <button type="button" class="blue-btn add_single3" data-table="complaint_types" data-cell="complaint_type" data-class="complntClass" data-name="complaint_type">Save</button>
                                                                                <input type="button" class="grey-btn" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="col-sm-4 clear">
                                                                <label>Complaint Notes:</label>
                                                                <span>
                                                <textarea class="form-control capitale" name="complaint_note"
                                                          id="complaint_note"></textarea>
                                              </span>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer">

                                                                    <input type="submit" name="submit" value="Save"
                                                                           class="blue-btn">
                                                                    <button class="grey-btn">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>

                                                    <!-- New Complaint Form -->

                                                    <!-- New Complaint Form -->
                                                    <div class="row">

                                                        <div class="col-sm-2 pull-right text-right">
                                                            <label>&nbsp;</label>
                                                            <button type="button" class="blue-btn complaint-btn">New Complaint
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="apx-table">
                                                        <div class="table-responsive">

                                                            <table id="TenantComplaint-table" class="table table-bordered">
                                                            </table>
                                                            <input type="button" class="blue-btn pull-right" id="print_email_button" value="Print">
                                                        </div>
                                                    </div>


                                                    <!--<div class="edit-foot">
                                                        <a href="javascript:;">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </div>-->
                                                </div>
                                            </div>

                                            <div class="container">
                                                <div class="modal fade" id="print_complaint" role="dialog">
                                                    <div class="modal-dialog modal-md">
                                                        <div class="modal-content" style="width: 100%;">
                                                            <div class="modal-header">
                                                                <button type="button" class="blue-btn"
                                                                        id='email_complaint'
                                                                        onclick="sendComplaintsEmail('#modal-body-complaints')">
                                                                    Email
                                                                </button>
                                                                <button type="button" class="blue-btn"
                                                                        id='print_complaints'
                                                                        onclick="PrintElem('#modal-body-complaints')"
                                                                        onclick="window.print();">Print
                                                                </button>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">×
                                                                </button>

                                                            </div>
                                                            <div class="modal-body" id="modal-body-complaints"
                                                                 style="height: 380px; overflow: auto;">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <!-- Thirteen Ends -->

                                        <div role="tabpanel" class="tab-pane hoa-container" id="tenant-detail-forteen">
                                            <div class="container">
                                                <div class="modal fade" id="print_hoa" role="dialog">
                                                    <div class="modal-dialog modal-md">
                                                        <div class="modal-content" style="width: 100%;">
                                                            <div class="modal-header">
                                                                <button type="button" class="blue-btn"
                                                                        id='email_complaint'
                                                                        onclick="sendComplaintsEmail('#modal-body-complaints')">
                                                                    Email
                                                                </button>
                                                                <button type="button" class="blue-btn"
                                                                        id='print_complaints'
                                                                        onclick="PrintElem('#modal-body-complaints')"
                                                                        onclick="window.print();">Print
                                                                </button>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">×
                                                                </button>

                                                            </div>
                                                            <div class="modal-body" id="modal-body-complaints"
                                                                 style="height: 380px; overflow: auto;">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="container">
                                                <div class="modal fade" id="hoa_image_popup" role="dialog">
                                                    <div class="modal-dialog modal-md">
                                                        <div class="modal-content" style="width: 100%;">
                                                            <div class="modal-header">
                                                                <a href="javascript:void(0)" class="close" data-dismiss="modal">×</a>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="col-sm-12 col-md-12"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-outer">
                                                <div class="form-hdr">
                                                    <h3>HOA Violation</h3>
                                                </div>
                                                <div class="form-data">

                                                    <!-- New Flag Form -->
                                                    <div class="row hoa_form_block" style="display: none;">
                                                        <form method="POST" id="hoa_form">
                                                            <input type="hidden" name="hoa_tenant" value="">
                                                            <input type="hidden" class="formtenant_id" value="<?php echo $_GET['tenant_id'] ?>">
                                                            <div class="col-sm-2">
                                                                <label>HOA Violation ID</label>
                                                                <input class="form-control" type="text" name="hoa_id">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Date<em class="red-star">*</em></label>
                                                                <input class="form-control" type="text" name="hoa_date">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Time</label>
                                                                <input class="form-control" type="text" name="hoa_time">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>HOA Type
                                                                    <a class="pop-add-icon insertHoaType" href="javascript:;">
                                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                    </a>
                                                                </label>
                                                                <select class="form-control" name="hoa_type">
                                                                    <option value="1">type 1</option>
                                                                </select>
                                                                <div class="add-popup" id="selectPropertyHOAType">
                                                                    <h4>Add New HOA Type</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>HOA Type <em class="red-star">*</em></label>
                                                                                <input name="type" class="form-control customValidateGroup hoa_type_source" type="text" data_required="true" data_max="150" placeholder="HOA Type">
                                                                                <span class="customError required" aria-required="true" id="hoa_type_source"></span>
                                                                            </div>
                                                                            <div class="btn-outer">
                                                                                <button type="button" class="blue-btn add_single1" data-table="hoa_violation_type" data-cell="type" data-class="hoa_type_source" data-name="hoa_type">Save</button>
                                                                                <input type="button" class="grey-btn" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>HOA Violation Description</label>
                                                                <textarea class="form-control capital"
                                                                          name="hoa_text"></textarea>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <label>Upload Picture(s)/Images(s) related to HOA
                                                                    Violation</label>
                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <div class="upload-logo upload-edittenant-photo hoa_photo1">
                                                                            <div class="img-outer"><img src="<?php echo COMPANY_SITE_URL ?>/images/hoa_image.png"></div>
                                                                            <a href="javascript:;">Choose Image</a>
                                                                            <input type="file" name="hoa_image_1">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="upload-logo upload-edittenant-photo hoa_photo2">
                                                                            <div class="img-outer"><img src="<?php echo COMPANY_SITE_URL ?>/images/hoa_image.png"></div>
                                                                            <a href="javascript:;">Choose Image</a>
                                                                            <input type="file" name="hoa_image_2">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="upload-logo upload-edittenant-photo hoa_photo3">
                                                                            <div class="img-outer"><img src="<?php echo COMPANY_SITE_URL ?>/images/hoa_image.png"></div>
                                                                            <a href="javascript:;">Choose Image</a>
                                                                            <input type="file" name="hoa_image_3">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer">
                                                                    <a class="blue-btn hoa_save">Save</a>
                                                                    <a class="grey-btn hoa_cancel">Cancel</a>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!-- New Flag Form -->
                                                    <div class="form-outer form-outer2 addhoabtn_block">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer text-right">
                                                                    <a class="blue-btn addhoabtn">New HOA Violation</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-grid hoa_table_block">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">

                                                                    <div id="collapseOne"
                                                                         class="panel-collapse collapse in"
                                                                         aria-expanded="true" style="">
                                                                        <div class="panel-body pad-none">
                                                                            <div class="grid-outer">
                                                                                <div class="apx-table">
                                                                                    <div class="table-responsive">
                                                                                        <table id="hoa_table" class="table table-hover table-dark"></table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>


                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="pull-right print_hoa">
                                                        <a href="javascript:;" id="tenantPrint" class="blue-btn">Print/Email</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Forteen Ends-->

                                        <div role="tabpanel" class="tab-pane flag-container" id="tenant-detail-fifteen">
                                            <div class="form-outer">
                                                <div class="form-hdr"><h3>Flag</h3></div>
                                                <div class="form-data">
                                                    <div class="property-status form-outer2">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer text-right">
                                                                    <a href="javascript:void(0);" class="blue-btn addbtnflag11">New Flag
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- New Flag Form -->
                                                    <div class="row">
                                                        <form name="flagform" id="flagform11" method="post"
                                                              style="display: none;">
                                                            <input name="flag_tenant_id" type="hidden"
                                                                   class="hiddenflag"
                                                                   value="<?php echo $_GET['tenant_id']; ?>">
                                                            <input name="record_id" type="hidden" value="">
                                                            <div class="col-sm-2">
                                                                <label>Flagged By</label>
                                                                <input type="text" name="flagged_by_name"
                                                                       class="capsOn form-control" value="<?php print_r($_SESSION[SESSION_DOMAIN]['name']); ?>">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Date</label>
                                                                <input type="text" name="date" class="form-control">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Flag Name</label>
                                                                <input name="flag_name" class="form-control" placeholder="Please Enter the Name of this Flag"
                                                                       type="text">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Country Code</label>
                                                                <select name="country_code"
                                                                        class="form-control"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Phone Number</label>
                                                                <input name="phone_number"
                                                                       class="phone_format form-control" type="text">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Flag Reason</label>
                                                                <input name="flag_reason" class="form-control capsOn"
                                                                       type="text">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Completed</label>
                                                                <select name="status" class="form-control">
                                                                    <option value="1">Yes</option>
                                                                    <option value="0" selected>No</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Note</label>
                                                                <textarea name="note" class="form-control capital"></textarea>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer">
                                                                    <a class="blue-btn saveflag"
                                                                       href="javascript:void(0)">Save</a>
                                                                    <a class="grey-btn"
                                                                       href="javascript:void(0)">Cancel</a>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!-- New Flag Form -->

                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">


                                                                    <div id="collapseOne"
                                                                         class="panel-collapse collapse in"
                                                                         aria-expanded="true" style="">
                                                                        <div class="panel-body pad-none">
                                                                            <div class="grid-outer">
                                                                                <div class="table-responsive">
                                                                                    <div class="grid-outer">
                                                                                        <div class="apx-table">
                                                                                            <table class="table table-hover table-dark" id="flaglistingtable"></table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>


                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Tab 15 Ends -->

                                    </div>
                                </div>
                                <!-- Sub tabs ends-->
                            </div>
                            <div role="tabpanel" class="tab-pane" id="people-owner">
                                <div class="property-status">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>Status</label>
                                            <select class="fm-txt form-control">
                                                <option>Active</option>
                                                <option></option>
                                                <option></option>
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="btn-outer text-right">
                                                <button class="blue-btn">Download Sample</button>
                                                <button class="blue-btn">Import Owner</button>
                                                <button onclick="window.location.href='new-property.html'"
                                                        class="blue-btn">
                                                    New Owner
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion"
                                                               href="#collapseOne"><span
                                                                    class="pull-right glyphicon glyphicon-menu-down"></span>
                                                                List of Owners</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Owner Name</th>
                                                                            <th>Company</th>
                                                                            <th>Phone</th>
                                                                            <th>Email</th>
                                                                            <th>Date Created</th>
                                                                            <th>Owner's Portal</th>
                                                                            <th>Status</th>
                                                                            <th>Actions</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td><a class="grid-link"
                                                                                   href="javascript:;">Alien
                                                                                    N West</a></td>
                                                                            <td></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>allen09@gmail.com</td>
                                                                            <td>11/23/2018 (Fri.)</td>
                                                                            <td>Yes</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control">
                                                                                    <option>Select</option>
                                                                                </select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link"
                                                                                   href="javascript:;">Alien
                                                                                    N West</a></td>
                                                                            <td></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>allen09@gmail.com</td>
                                                                            <td>11/23/2018 (Fri.)</td>
                                                                            <td>Yes</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control">
                                                                                    <option>Select</option>
                                                                                </select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link"
                                                                                   href="javascript:;">Alien
                                                                                    N West</a></td>
                                                                            <td></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>allen09@gmail.com</td>
                                                                            <td>11/23/2018 (Fri.)</td>
                                                                            <td>Yes</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control">
                                                                                    <option>Select</option>
                                                                                </select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link"
                                                                                   href="javascript:;">Alien
                                                                                    N West</a></td>
                                                                            <td></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>allen09@gmail.com</td>
                                                                            <td>11/23/2018 (Fri.)</td>
                                                                            <td>Yes</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control">
                                                                                    <option>Select</option>
                                                                                </select></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="people-vendor">
                                <div class="property-status">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>Vendor Type</label>
                                            <select class="fm-txt form-control">
                                                <option>Select</option>
                                                <option></option>
                                                <option></option>
                                                <option></option>
                                            </select>

                                        </div>
                                        <div class="col-sm-2">
                                            <label>Status</label>
                                            <select class="fm-txt form-control">
                                                <option>Active</option>
                                                <option></option>
                                                <option></option>
                                                <option></option>
                                            </select>

                                        </div>
                                        <div class="col-sm-8">
                                            <div class="btn-outer text-right">
                                                <button class="blue-btn">Download Sample</button>
                                                <button class="blue-btn">Import Vendor</button>
                                                <button onclick="window.location.href='new-property.html'"
                                                        class="blue-btn">
                                                    New Vendor
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion"
                                                               href="#collapseOne"><span
                                                                    class="pull-right glyphicon glyphicon-menu-down"></span>
                                                                List of Vendors</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Vendor Name</th>
                                                                            <th>Phone</th>
                                                                            <th>Open Work Orders</th>
                                                                            <th>YTD Payment</th>
                                                                            <th>Type</th>
                                                                            <th>Rate</th>
                                                                            <th>Rating</th>
                                                                            <th>Email</th>
                                                                            <th>Status</th>
                                                                            <th>Actions</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td><a class="grid-link"
                                                                                   href="javascript:;">ABBOTT
                                                                                    LLC</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>1</td>
                                                                            <td>0.00</td>
                                                                            <td>Contractor-General</td>
                                                                            <td>$19.00 /hr</td>
                                                                            <td class="rating-star"><i
                                                                                    class="fa fa-star"
                                                                                    aria-hidden="true"></i>
                                                                                <i class="fa fa-star"
                                                                                   aria-hidden="true"></i> <i
                                                                                    class="fa fa-star"
                                                                                    aria-hidden="true"></i> <i
                                                                                    class="fa fa-star"
                                                                                    aria-hidden="true"></i> <i
                                                                                    class="fa fa-star-o"
                                                                                    aria-hidden="true"></i></td>
                                                                            <td>skkokjim@gmail.com</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control">
                                                                                    <option>Select</option>
                                                                                </select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link"
                                                                                   href="javascript:;">ABBOTT
                                                                                    LLC</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>1</td>
                                                                            <td>0.00</td>
                                                                            <td>Contractor-General</td>
                                                                            <td>$19.00 /hr</td>
                                                                            <td class="rating-star"><i
                                                                                    class="fa fa-star"
                                                                                    aria-hidden="true"></i>
                                                                                <i class="fa fa-star"
                                                                                   aria-hidden="true"></i> <i
                                                                                    class="fa fa-star"
                                                                                    aria-hidden="true"></i> <i
                                                                                    class="fa fa-star"
                                                                                    aria-hidden="true"></i> <i
                                                                                    class="fa fa-star-o"
                                                                                    aria-hidden="true"></i></td>
                                                                            <td>skkokjim@gmail.com</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control">
                                                                                    <option>Select</option>
                                                                                </select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link"
                                                                                   href="javascript:;">ABBOTT
                                                                                    LLC</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>1</td>
                                                                            <td>0.00</td>
                                                                            <td>Contractor-General</td>
                                                                            <td>$19.00 /hr</td>
                                                                            <td class="rating-star"><i
                                                                                    class="fa fa-star"
                                                                                    aria-hidden="true"></i>
                                                                                <i class="fa fa-star"
                                                                                   aria-hidden="true"></i> <i
                                                                                    class="fa fa-star"
                                                                                    aria-hidden="true"></i> <i
                                                                                    class="fa fa-star"
                                                                                    aria-hidden="true"></i> <i
                                                                                    class="fa fa-star-o"
                                                                                    aria-hidden="true"></i></td>
                                                                            <td>skkokjim@gmail.com</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control">
                                                                                    <option>Select</option>
                                                                                </select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link"
                                                                                   href="javascript:;">ABBOTT
                                                                                    LLC</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>1</td>
                                                                            <td>0.00</td>
                                                                            <td>Contractor-General</td>
                                                                            <td>$19.00 /hr</td>
                                                                            <td class="rating-star"><i
                                                                                    class="fa fa-star"
                                                                                    aria-hidden="true"></i>
                                                                                <i class="fa fa-star"
                                                                                   aria-hidden="true"></i> <i
                                                                                    class="fa fa-star"
                                                                                    aria-hidden="true"></i> <i
                                                                                    class="fa fa-star"
                                                                                    aria-hidden="true"></i> <i
                                                                                    class="fa fa-star-o"
                                                                                    aria-hidden="true"></i></td>
                                                                            <td>skkokjim@gmail.com</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control">
                                                                                    <option>Select</option>
                                                                                </select></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="people-contact">
                                <!-- Sub Tabs Starts-->
                                <div class="sub-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#contact-tenant"
                                                                                  aria-controls="home" role="tab"
                                                                                  data-toggle="tab">Tenants<span
                                                    class="tab-count">84</span></a></li>
                                        <li role="presentation"><a href="#contact-owner" aria-controls="profile"
                                                                   role="tab"
                                                                   data-toggle="tab">Owners<span
                                                    class="tab-count">36</span></a>
                                        </li>
                                        <li role="presentation"><a href="#contact-vendor" aria-controls="home"
                                                                   role="tab"
                                                                   data-toggle="tab">Vendors<span
                                                    class="tab-count">71</span></a></li>
                                        <li role="presentation"><a href="#contact-users" aria-controls="profile"
                                                                   role="tab"
                                                                   data-toggle="tab">Users<span
                                                    class="tab-count">199</span></a>
                                        </li>
                                        <li role="presentation"><a href="#contact-others" aria-controls="profile"
                                                                   role="tab"
                                                                   data-toggle="tab">Others<span
                                                    class="tab-count">84</span></a>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="contact-tenant">
                                            <div class="property-status">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <label>Status</label>
                                                        <select class="fm-txt form-control">
                                                            <option>Active</option>
                                                            <option></option>
                                                            <option></option>
                                                            <option></option>
                                                        </select>

                                                    </div>
                                                    <div class="col-sm-10">
                                                        <div class="btn-outer text-right">
                                                            <button class="blue-btn">Download Sample</button>
                                                            <button class="blue-btn">Import Contact</button>
                                                            <button onclick="window.location.href='new-contact.html'"
                                                                    class="blue-btn">New Contact
                                                            </button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Import Contact</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <input type="file"/>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="btn-outer">
                                                                <button class="blue-btn">Save</button>
                                                                <button class="grey-btn">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse"
                                                                           data-parent="#accordion"
                                                                           href="#collapseOne"><span
                                                                                class="pull-right glyphicon glyphicon-menu-down"></span>
                                                                            List of Contacts</a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne"
                                                                     class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-hover table-dark">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th>Contact Name</th>
                                                                                        <th>Phone</th>
                                                                                        <th>Email</th>
                                                                                        <th>Date Created</th>
                                                                                        <th>Status</th>
                                                                                        <th>Actions</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td><a class="grid-link"
                                                                                               href="new-contact-detail.html">Abby
                                                                                                N Wesley</a></td>
                                                                                        <td>555-444-6666</td>
                                                                                        <td>abby768@gmail.com</td>
                                                                                        <td>12/7/2018 12:30:53 PM</td>
                                                                                        <td>Active</td>
                                                                                        <td>
                                                                                            <select class="form-control">
                                                                                                <option>Select</option>
                                                                                            </select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><a class="grid-link"
                                                                                               href="javascript:;">Arsh
                                                                                                Sandhu</a></td>
                                                                                        <td>555-444-6666</td>
                                                                                        <td>abby768@gmail.com</td>
                                                                                        <td>12/7/2018 12:30:53 PM</td>
                                                                                        <td>Active</td>
                                                                                        <td>
                                                                                            <select class="form-control">
                                                                                                <option>Select</option>
                                                                                            </select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><a class="grid-link"
                                                                                               href="javascript:;">Ben
                                                                                                Snow</a></td>
                                                                                        <td>555-444-6666</td>
                                                                                        <td>abby768@gmail.com</td>
                                                                                        <td>12/7/2018 12:30:53 PM</td>
                                                                                        <td>Active</td>
                                                                                        <td>
                                                                                            <select class="form-control">
                                                                                                <option>Select</option>
                                                                                            </select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><a class="grid-link"
                                                                                               href="javascript:;">Asron
                                                                                                Properties</a></td>
                                                                                        <td>555-444-6666</td>
                                                                                        <td>abby768@gmail.com</td>
                                                                                        <td>12/7/2018 12:30:53 PM</td>
                                                                                        <td>Active</td>
                                                                                        <td>
                                                                                            <select class="form-control">
                                                                                                <option>Select</option>
                                                                                            </select></td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Regular Rent Ends -->
                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                        </div>
                                    </div>
                                </div>
                                <!-- Sub tabs ends-->
                            </div>
                            <div role="tabpanel" class="tab-pane" id="people-employee">
                                <div class="property-status">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>Status</label>
                                            <select class="fm-txt form-control">
                                                <option>Active</option>
                                                <option></option>
                                                <option></option>
                                                <option></option>
                                            </select>

                                        </div>
                                        <div class="col-sm-10">
                                            <div class="btn-outer text-right">
                                                <button class="blue-btn">Download Sample</button>
                                                <button class="blue-btn">Import Employee</button>
                                                <button onclick="window.location.href='new-property.html'"
                                                        class="blue-btn">
                                                    New Employee
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion"
                                                               href="#collapseOne"><span
                                                                    class="pull-right glyphicon glyphicon-menu-down"></span>
                                                                List of Employees</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Employee Name</th>
                                                                            <th>Phone</th>
                                                                            <th>Email</th>
                                                                            <th>Date Created</th>
                                                                            <th>Status</th>
                                                                            <th>Actions</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td><a class="grid-link"
                                                                                   href="javascript:;">Adam
                                                                                    Scott</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>adam@gmail.com</td>
                                                                            <td>11/23/2018 8:48:45 AM</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control">
                                                                                    <option>Select</option>
                                                                                </select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link"
                                                                                   href="javascript:;">Adam
                                                                                    Scott</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>adam@gmail.com</td>
                                                                            <td>11/23/2018 8:48:45 AM</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control">
                                                                                    <option>Select</option>
                                                                                </select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link"
                                                                                   href="javascript:;">Adam
                                                                                    Scott</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>adam@gmail.com</td>
                                                                            <td>11/23/2018 8:48:45 AM</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control">
                                                                                    <option>Select</option>
                                                                                </select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link"
                                                                                   href="javascript:;">Adam
                                                                                    Scott</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>adam@gmail.com</td>
                                                                            <td>11/23/2018 8:48:45 AM</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control">
                                                                                    <option>Select</option>
                                                                                </select></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Custom Field</h4>
                    </div>
                    <div class="modal-body" style="height: 380px;">
                        <div class="form-outer col-sm-12">
                            <form id="custom_field">
                                <input type="hidden" name="id" id="custom_field_id" value="">
                                <div class="row custom_field_form">
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Field Name <em class="red-star">*</em></label>
                                        </div>
                                        <div class="col-sm-9 field_name">
                                            <input class="form-control" type="text" maxlength="100" id="field_name"
                                                   name="field_name" placeholder="">
                                            <span class="required error"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Data Type</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="form-control data_type" id="data_type" name="data_type">
                                                <option value="text">Text</option>
                                                <option value="number">Number</option>
                                                <option value="currency">Currency</option>
                                                <option value="percentage">Percentage</option>
                                                <option value="url">URL</option>
                                                <option value="date">Date</option>
                                                <option value="memo">Memo</option>
                                            </select>
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Default value</label>
                                        </div>
                                        <div class="col-sm-9 default_value">
                                            <input class="form-control default_value" id="default_value" type="text"
                                                   name="default_value" placeholder="">
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Required Field</label>
                                        </div>
                                        <div class="col-sm-9 is_required">
                                            <select class="form-control" name="is_required" id="is_required">
                                                <option value="1">Yes</option>
                                                <option value="0" selected="selected">No</option>
                                            </select>
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-outer">
                                    <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                    <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--       {"property_name":"property1","address1":"acme1","address2":"acme2","address3":"acme3","address4":"acme4","building_name":"edwe","tenant_name":"Ankur Thakur","tenant_phone":"076-962-79772"} -->


    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
    ?>


    <!-- Jquery Starts -->
    <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
    <script language="javascript"
            src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
    <!--<script src="<?php /*echo SUPERADMIN_SITE_URL; */ ?>/js/passwordscheck.js"></script>-->
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/intlTelInput.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>

    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenant.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.cropit.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/tenant/tenant.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/tenant/edit-tenant.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenant_occupants.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantflag.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/renterinsurance.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/notes_history.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/hoa_violation.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>


    <script>
        $('.company-top').addClass('active');
        $('.cropItData').hide();

        $(document).ready(function () {

            $(document).on("click",".cancelUpdateBtn, .cancel_edit_rent",function(){
                bootbox.confirm({
                    message: "Do you want to cancel this action now?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            window.location.href=window.location.href;
                        }
                    }
                });
            });

            jQuery('.phone_format').mask('000-000-0000', {reverse: true});
            $('.number_only').keydown(function (e) {
                if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
                    return false;
                }
            });

            setTimeout(function(){
                jQuery('.phone_format').mask('000-000-0000', {reverse: true});
                $('.calander').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: jsDateFomat
                });
            }, 2000);

            $(document).on('focusout', ' #edit_incr_amount, #edit_security_amount, #edit_cam_amount, #edit_rent_amount', function () {
                var id = this.id;
                if ($('#' + id).val() != '' && $('#' + id).val().indexOf(".") == -1) {
                    var bef = $('#' + id).val().replace(/,/g, '');
                    var value = numberWithCommas(bef) + '.00';
                    $('#' + id).val(value);
                } else {
                    var bef = $('#' + id).val().replace(/,/g, '');
                    $('#' + id).val(numberWithCommas(bef));
                }
            });

            function numberWithCommas(x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

            var edit_tab = localStorage.getItem('edit_active');
            if (edit_tab != "") {
                $("." + edit_tab + " a").trigger("click");
            }
        });
        $("#people_top").addClass("active");
    </script>
    <script>
        $('.calander').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: jsDateFomat
        });
        $('input[name="birth"]').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: jsDateFomat
        });
        var date = $.datepicker.formatDate(jsDateFomat, new Date());
        var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
        $(".calander").val(date);

        var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";

    </script>

    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
    ?>
    <!-- Footer Ends -->




