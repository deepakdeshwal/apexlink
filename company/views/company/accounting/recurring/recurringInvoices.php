<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting &gt;&gt; <span>Receivable </span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>ACCOUNTING</h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" >Receive Money</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Multi Payments</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">My Bills</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="#" id="" class="list-group-item list-group-item-success strong collapsed">Pay Bills</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">Invoices</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Write Checks</a>
                                <!-- Seven Ends-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Print Checks</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Check Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Payment Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bill Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bank Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bank Deposit</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Late Fees</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Owner Draw</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Owner Contributions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Transactions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Process Management Fee</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Pending Transactions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Credit Card Details</a>
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Accounting/Accounting">Receivables</a></li>
                            <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                            <li role="presentation"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                            <li role="presentation" class="active"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                            <li role="presentation"><a href="/Accounting/BankRegister">Banking</a></li>
                            <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                            <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                            <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconcilation</a></li>

                            <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                            <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                            <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                            <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Recurring Transactions</a>
                                    <div class="col-md-6 text-right"></div>
                                    <a href="/Accounting/RecurringBill"><input type="button" class="blue-btn" value="Recurring Bills"></a>
                                    <a href="/Accounting/RecurringInvoices"><input type="button" class="blue-btn" value="Recurring Invoices"></a>
                                    <a href="/Accounting/addRecurringChecks"><input type="button" class="blue-btn" value="Recurring Checks"></a>
                            </div>
                                </h4>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="receivables">
                                        <div class="property-status">
                                            <div class="row">
                                                <!--<div class="col-sm-2">
                                                    <label>Property</label>
                                                    <select class="fm-txt form-control"> <option>Active</option>
                                                        <option></option>
                                                        <option></option>
                                                        <option></option>
                                                    </select>
                                                </div>-->
                                                <div class="col-sm-10">
                                                    <div class="btn-outer text-right">
                                                        <!--<button class="blue-btn">Receive Payment</button>-->
                                                        <!--<a href="/Accounting/addRecurringBill"><input type="button" class="blue-btn" value="Receive Payment"></a>
                                                        <a href="/Accounting/RecurringInvoices"><input type="button" class="blue-btn" value="Recurring Invoices"></a>
                                                        <a href="/Accounting/addRecurringChecks"><input type="button" class="blue-btn" value="Recurring Checks"></a>
                                                 -->   </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="viewlisting" style="display: none;">
                                            <div class="col-lg-12">
                                                <div class="col-sm-4">
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>Vendor Name :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="vndr_nme"></span>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>Bill Date :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="billdate"></span>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>Amount($) :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="amt"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>Terms :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="terms"></span>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>WorkOrderNumber :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="wrkorderno"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>Bill Recurrence :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="billrecurr"></span>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>Due Date :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="duedate"></span>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>Memo :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="memo"></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-xs-12"> <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>Reference :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="reference"></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label >Invoice Number :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="invoiceno"></span>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="accordion-grid" >
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span>Split Bill</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                        <div class="panel-body pad-none">
                                                                            <div class="grid-outer">
                                                                                <div class="table-responsive">
                                                                                    <table id="RecurringPropertyAccountTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="accordion-grid" id="NotesTable">
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> Notes</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                        <div class="panel-body pad-none">
                                                                            <div class="grid-outer">
                                                                                <div class="table-responsive">
                                                                                    <table id="RecurringNotesTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="accordion-grid" >
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> File library</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                        <div class="panel-body pad-none">
                                                                            <div class="grid-outer">
                                                                                <div class="table-responsive">
                                                                                    <table id="RecurringFilesTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="accordion-grid" id="RecurrTable">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> Recurring Bills</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body pad-none">
                                                                <div class="grid-outer">
                                                                    <div class="table-responsive">
                                                                        <table id="RecurringDataTable" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Regular Rent Ends -->

                            </div>
                        </div>
                        <!-- Sub tabs ends-->
                    </div>



                    <!-- Sub Tabs Starts-->

                    <!-- Sub tabs ends-->
                </div>

            </div>
        </div>

        <!--Tabs Ends -->

</div>
</div>
</div>
</section>
</div>
<!-- Wrapper Ends -->


<div class="modal fade" id="UnitModal" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Unit</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <div class="accordion-grid" >
                    <div class="accordion-outer">
                        <div class="bs-example">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> List of Units</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                        <div class="panel-body pad-none">
                                            <div class="grid-outer">
                                                <div class="table-responsive">
                                                    <table id="UnitDataTable" class="table table-bordered"></table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    var upload_url = "<?php echo SITE_URL; ?>";
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var property_unique_id = '';
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var default_name = "<?php echo $_SESSION[SESSION_DOMAIN]['default_name']; ?>";
    var manager_id = "";
    var attachGroup_id = "";
    var credential_notice_period =  "<?php echo $_SESSION[SESSION_DOMAIN]['default_notice_period']; ?>";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
</script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/vendor/recurr_file_library.js" type="text/javascript"></script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
