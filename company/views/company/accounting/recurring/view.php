<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
$default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$';

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting &gt;&gt; <span>Recurring Bills </span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/accounting/layout/right-nav.php");?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Accounting/Accounting">Receivables</a></li>
                            <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                            <li role="presentation"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                            <li role="presentation" class="active"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                            <li role="presentation"><a href="/Accounting/BankRegister">Banking</a></li>
                            <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                            <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                            <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconcilation</a></li>
                            <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                            <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                            <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                            <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="panel-heading">
                                <div class="form-hdr" style="margin-bottom: 20px;">
                                    <h3 class="d-inline-block">Recurring Transactions</h3>
                                    <!--<button class="blue-btn">Receive Payment</button>-->
                                    <div class="pull-right recurring-button">
                                        <a href="/Accounting/RecurringBill"><input type="button" class="blue-btn btn-pad-normal" value="Recurring Bills"></a>
                                        <a href="/Accounting/RecurringInvoices"><input type="button" class="blue-btn btn-pad-normal blue-btn-alt" value="Recurring Invoices"></a>
                                        <a href="/Accounting/RecurringChecks"><input type="button" class="blue-btn btn-pad-normal blue-btn-alt" value="Recurring Checks"></a>
                                    </div>
                                </div>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="receivables">

                                            <div class="form-outer">
                                                <div class="form-hdr">
                                                    <h3>Recurring Bills <a href="/Accounting/addRecurringBill"><input type="button" class="blue-btn text-right pull-right btn-pad-normal blue-btn-alt" value="New Recurring Bill"></a></h3>
                                                </div>
                                                <div class="form-data" id="RecurrTable">
                                                    <table id="RecurringDataTable" class="table table-bordered"></table>
                                                </div>

                                        </div>
                                    </div>

                                </div>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="receivables">
                                        <div class="row" id="viewlisting" style="display: none;">
                                            <div class="col-sm-12">
                                            <div class="form-outer custom-ht">
                                                <div class="form-hdr">
                                                    <h3>Bill Information <a class="back" href="/Accounting/RecurringBill"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                                </div>

                                            <div class="form-data">
                                                <div class="col-sm-4">
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                             <div class="col-xs-6">
                                                                 <label>Vendor Name :</label>
                                                             </div>
                                                            <div class="col-xs-6">
                                                                <span class="vndr_nme"></span>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>Bill Date :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="billdate"></span>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>Amount(<?php echo $default_symbol; ?>) :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="amt"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>Terms :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="terms"></span>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>WorkOrderNumber :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="wrkorderno"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                              <div class="col-xs-6">
                                                                  <label>Bill Recurrence :</label>
                                                              </div>
                                                            <div class="col-xs-6">
                                                                <span class="billrecurr"></span>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>Due Date :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="duedate"></span>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>Memo :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="memo"></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-xs-12"> <div class="row">
                                                            <div class="col-xs-6">
                                                                <label>Reference :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="reference"></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <label >Invoice Number :</label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <span class="invoiceno"></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="col-md-12 text-right">
                                                <a href="javascript:;" class="viewEdit" data-editid="">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="accordion-grid" id="PropertyAccountTable">
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span>Split Bill</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                        <div class="panel-body pad-none">
                                                                            <div class="grid-outer">
                                                                                <div class="table-responsive">
                                                                                    <table id="RecurringPropertyAccountTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 text-right">
                                                <a href="javascript:;" class="viewEdit" data-editid="">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="accordion-grid" id="NotesTable">
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> Notes</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                        <div class="panel-body pad-none">
                                                                            <div class="grid-outer">
                                                                                <div class="table-responsive">
                                                                                    <table id="RecurringNotesTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 text-right">
                                                <a href="javascript:;" class="viewEdit" data-editid="">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="accordion-grid" >
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> File library</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                        <div class="panel-body pad-none">
                                                                            <div class="grid-outer">
                                                                                <div class="table-responsive">
                                                                                    <table id="RecurringFilesTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 text-right">
                                                <a href="javascript:;" class="viewEdit" data-editid="">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Tabs Ends -->
</section>
</div>
<!-- Wrapper Ends -->


<div class="modal fade" id="UnitModal" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Unit</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <div class="accordion-grid" >
                    <div class="accordion-outer">
                        <div class="bs-example">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> List of Units</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                        <div class="panel-body pad-none">
                                            <div class="grid-outer">
                                                <div class="table-responsive">
                                                    <table id="UnitDataTable" class="table table-bordered"></table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    var upload_url = "<?php echo SITE_URL; ?>";
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var property_unique_id = '';
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var default_name = "<?php echo $_SESSION[SESSION_DOMAIN]['default_name']; ?>";
    var manager_id = "";
    var attachGroup_id = "";
    var default_symbol = "<?php echo $default_symbol; ?>";
    var credential_notice_period =  "<?php echo $_SESSION[SESSION_DOMAIN]['default_notice_period']; ?>";

    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
</script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/vendor/recurr_file_library.js" type="text/javascript"></script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
