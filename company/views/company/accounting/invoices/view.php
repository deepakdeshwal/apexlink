<?php

/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                      <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                              Accounting &gt;&gt; <span>Invoices</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="easy-search">
                              <input placeholder="Easy Search" type="text">
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-12">
                      <div class="content-section">
                          <div class="main-tabs">
                          <ul class="nav nav-tabs" role="tablist">
                              <li role="presentation" ><a href="/Accounting/Accounting">Receivables</a></li>
                              <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                              <li role="presentation" class="active"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                              <li role="presentation"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                              <li role="presentation"><a href="/Accounting/BankRegister">Banking</a></li>
                              <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                              <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                              <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconcilation</a></li>

                              <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                              <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                              <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                              <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                          </ul>
                              <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="guest-cards">

                       <div class="form-outer form-outer2">
                          <div class="form-hdr">
                            <h3>Invoice- View <a class="back" href="/Accounting/ConsolidatedInvoice"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                          </div>
                          <div class="form-data">

                            <div class="detail-outer">
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="col-xs-12">
                                      <label class="text-right">Invoices :</label>
                                      <span class="invoice_number">--</span>
                                    </div>

                                    <div class="col-xs-12">
                                      <label class="text-right">Late Date :</label>
                                      <span class="late_date">--</span>
                                    </div>

                                    <div class="col-xs-12">
                                      <label class="text-right">Property :</label>
                                      <span class="property_name">--</span>
                                    </div>

                                    <div class="col-xs-12">
                                      <label class="text-right">Type :</label>
                                      <span class="user_type">--</span>
                                    </div>

                                    <div class="col-xs-12">
                                      <label class="text-right">Invoice Amount :</label>
                                        <span class="default_currency"></span><span class="total_amount">--</span>
                                    </div>

                                    <div class="col-xs-12">
                                      <label class="text-right">Amount Due :</label>
                                        <span class="default_currency"></span><span class="amount_due">--</span>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                     <div class="col-xs-12">
                                        <label class="text-right">Invoice Date :</label>
                                        <span class="invoice_date">--</span>
                                    </div>
                                    <div class="col-xs-12">
                                      <label class="text-right">Amount Paid :</label>
                                        <span class="default_currency"></span><span class="amount_paid">--</span>
                                    </div>
                                     <div class="col-xs-12">
                                      <label class="text-right">Status :</label>
                                      <span class="status">--</span>
                                    </div>
                                    <div class="col-xs-12">
                                      <label class="text-right">Unit :</label>
                                      <span class="unit">--</span>
                                    </div>
                                    <div class="col-xs-12">
                                      <label class="text-right">Invoices To :</label>
                                      <span class="invoice_to">--</span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                                  </div>
                              </div>
                          </div>
                         <!-- Form Outer Ends -->


                         <div class="accordion-grid">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                      <div class="panel-group" id="accordion">
                                          <div class="panel panel-default">
                                              <div class="panel-heading">
                                                  <h4 class="panel-title">
                                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-up"></span> Item Detail</a>
                                                  </h4>
                                              </div>
                                              <div id="collapseOne" class="panel-collapse collapse  in">
                                                  <div class="panel-body pad-none">
                                                        <div class="grid-outer">
                                                          <div class="table-responsive">
                                                              <table class="table table-hover table-dark">
                                                                <thead>
                                                                  <tr>
                                                                    <th scope="col">Property</th>
                                                                    <th scope="col">Unit</th>
                                                                    <th scope="col">Charge Code</th>
                                                                    <th scope="col">Description</th>
                                                                    <th scope="col">Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']?>)</th>
                                                                  </tr>
                                                                </thead>
                                                                <tbody class="charge_data_html">

                                                                </tbody>
                                                              </table>
                                                            </div>
                                                          </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                    </div>
                                </div>
                              </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
        <!-- Wrapper Ends -->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->
<script>
    var default_currency_symbol  = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/invoiceView.js"></script>

    <!-- Footer Ends -->
    <script>
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
    $("#bs-example-navbar-collapse-2").show();
});
         $("#close").click(function(){
    $("#bs-example-navbar-collapse-2").hide();
});
         <!--- Main Nav Responsive -->


        $(document).ready(function(){
          $(".slide-toggle").click(function(){
            $(".box").animate({
              width: "toggle"
            });
          });
        });

        $(document).ready(function(){
          $(".slide-toggle2").click(function(){
            $(".box2").animate({
              width: "toggle"
            });
          });
        });

         <!--- Accordians -->
       $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
        	$(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
         <!--- Accordians -->
    </script>





    <!-- Jquery Starts -->

</body>

</html>