<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>



    <div id="wrapper">


        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->


        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                      <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                              Accounting &gt;&gt; <span>Invoices</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="easy-search">
                              <input placeholder="Easy Search" type="text">
                          </div>
                        </div>
                      </div>
                    </div>
                    <form id="updateInvoice" enctype='multipart/form-data'>
                        <input type ="hidden" id="user_id" val="">
                        <input type ="hidden" id="invoice_id" val="" name="invoice_id">
                    <div class="col-sm-12">
                      <div class="content-section">
                         <!--Tabs Starts -->
                        <div class="main-tabs">

                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" ><a href="/Accounting/Accounting">Receivables</a></li>
                                <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                                <li role="presentation" class="active"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                                <li role="presentation"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                                <li role="presentation"><a href="/Accounting/BankRegister">Banking</a></li>
                                <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                                <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                                <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconcilation</a></li>

                                <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                                <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                                <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                                <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                            </ul>
                          <!-- Nav tabs -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="guest-cards">
                              <div class="form-outer">
                                <div class="form-hdr">
                                  <h3>Invoice New <a class="back" href="/Accounting/ConsolidatedInvoice"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                </div>
                                <div class="form-data">
                                  <div class="row">
                                    <div class="col-xs-12 col-sm-6">
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>Invoice To :</label>
                                        </div>
                                        <div class="col-sm-6">
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <div class="check-outer"><input  class="tenant_radio" value="tenant_radio" name="user_radio" checked="" type="radio"/><label>Tenant</label></div>
                                        </div>
                                        <div class="col-sm-5 all_tenants_combogrid">
                                          <span> <input class="form-control all_tenants" type="text" /></span>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <div class="check-outer"><input  class="owner_radio" value="owner_radio" name="user_radio" type="radio"/><label>Owner</label></div>
                                        </div>
                                        <div class="col-sm-5 all_owners_combogrid" >
                                            <span> <input class="form-control all_owners"  type="text" /></span>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <div class="check-outer"><input class="other_radio" value="other_radio" name="user_radio" type="radio"/><label>Other</label></div>
                                        </div>
                                        <div class="col-sm-5 all_others_combogrid"">
                                             <span><input class="form-control others" placeholder="Enter Name" name="other_name" id="other_name" type="text"/></span>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-6 textarea-form">
                                          <label>Address</label>
                                          <span><textarea disabled="" rows="4" disabled="disabled" class="form-control address" name="address" id="address" ></textarea></span>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6">
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <div class="check-outer"><input class="hidden" checked="" /><label>Invoice Date</label></div>
                                        </div>
                                        <div class="col-sm-5">
                                          <span><input class="form-control" name="invoice_date" readonly id="invoice_date" placeholder="May 30, 2019 (Thu.)" type="text"/></span>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <div class="check-outer"><input class="hidden"  checked="" /><label>Late Date</label></div>
                                        </div>
                                        <div class="col-sm-5">
                                          <span><input class="form-control" id="late_date" readonly name="late_date"  placeholder="" type="text"/></span>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <div class="check-outer"><input name="email_invoice"  checked="" type="checkbox"/><label>Email this Invoice</label></div>
                                        </div>
                                        
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <div class="check-outer"><input name="no_late_fee" type="checkbox"/><label>No Late Fee for this Invoice</label></div>
                                        </div>
                                        
                                      </div>
                                    </div>
                                    

                                  </div>
                                  <div class="grid-outer" id="invoice_table" >
                                    <div class="table-responsive">
                                        <table class="table table-hover table-dark" id="charge_table">
                                          <thead>
                                            <tr>
                                              <th scope="col">Property</th>
                                              <th scope="col">Unit</th>
                                              <th scope="col">Charge Code</th>
                                              <th scope="col">Description</th>
                                              <th scope="col">Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']?>)</th>
                                            </tr>
                                          </thead>
                                          <tbody class="charge_data_html">
                                          </tbody>
                                        </table>
                                      </div>
                                      <div class="add-invoice-row">
                                        <i class="fa fa-plus-circle fa-lg additional-add-invoice" aria-hidden="true"></i>
                                          <div class="pull-right">Total Amount : <?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']?><span class="total_charges_amount">0.00</span></div>
                                      </div>
                                      <div class="btn-outer text-right">
                                          <button class="blue-btn update_invoice">Update</button>
                                          <button type="button" class="clear-btn clearFormReset" id="reset_invoice">Reset</button>
                                          <button class="grey-btn cancel" type="button">Cancel</button>
                                      </div>
                                    </div>
                             
                                </div>
                              </div>
                               <!-- Form Outer Ends -->

                            </div>
                      

                        </div>
                        <!--tab Ends -->
                        
                    </div>

                    </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
        <!-- Wrapper Ends -->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<script>

    var default_currency_symbol  = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
var pagination  = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/editInvoice.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.easyui.min.js" type="text/javascript"></script>
<link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.easyui.css" rel="stylesheet">
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/autonumeric.js"></script>



    <!-- Footer Ends -->
    <script>
        setTimeout(function(){
        new AutoNumeric.multiple('.money', {
            allowDecimalPadding: true,
            maximumValue  : '9999999999',
        });
        },2000);

        $(function() {
            $('.nav-tabs').responsiveTabs();
        });
 
        <!--- Main Nav Responsive --> 
        $("#show").click(function(){
    $("#bs-example-navbar-collapse-2").show();
});
         $("#close").click(function(){
    $("#bs-example-navbar-collapse-2").hide();
});
         <!--- Main Nav Responsive -->
        
        
        $(document).ready(function(){
          $(".slide-toggle").click(function(){
            $(".box").animate({
              width: "toggle"
            });
          });
        });
        
        $(document).ready(function(){
          $(".slide-toggle2").click(function(){
            $(".box2").animate({
              width: "toggle"
            });
          });
        });
        
         <!--- Accordians -->
       $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
        	$(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });
        
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
         <!--- Accordians --> 
    </script>


<script>
    $(document).on('click','#reset_invoice',function(){
        bootbox.confirm("Do you want to clear this form?", function (result) {
            if (result == true) {
                window.location.reload();
            }
        });
    });
</script>


    <!-- Jquery Starts -->

</body>

</html>