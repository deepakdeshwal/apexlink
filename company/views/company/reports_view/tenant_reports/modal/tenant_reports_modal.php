<!-- 1 Tenant Ledger Filters-->
<div class="container">
    <div class="modal fade" id="tenant_ledger" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_tenant_ledger">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Tenant Ledger Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="tenant_ledger_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="tenant_ledger_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>


                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="tenant_ledger_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="tenant_ledger_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="tenant_ledger_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="tenant_ledger_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="tenant_ledger_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!-- 2) Tenant Statement Filters-->
<div class="container">
    <div class="modal fade" id="tenant_statement" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_tenant_statement">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Tenant Statement Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="tenant_statement_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="tenant_statement_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>


                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="tenant_statement_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="tenant_statement_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="tenant_statement_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="tenant_statement_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="tenant_statement_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!--3) Tenant Payment Filters-->
<div class="container">
    <div class="modal fade" id="tenant_payment" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_tenant_payment">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Tenant Payment Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="tenant_payment_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="tenant_payment_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>


                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="tenant_payment_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="tenant_payment_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="tenant_payment_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="tenant_payment_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="tenant_payment_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!--4) Tenant Consolidated Payment Filters-->
<div class="container">
    <div class="modal fade" id="tenant_consolidated_payment" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_tenant_consolidated_payment">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Tenant Consolidated Payment Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="tc_payment_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="tc_payment_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>


                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="tc_payment_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="tc_payment_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="tc_payment_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="tc_payment_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="tc_payment_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!--5) Tenant Online Payment Filters-->
<div class="container">
    <div class="modal fade" id="tenant_online_payment" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_tenant_online_payment">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Tenant Online Payment Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="to_payment_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="to_payment_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>


                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="to_payment_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="to_payment_property_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="to_payment_tenant_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Payment Type</label>
                                                <select type="select" name="status" id="to_payment_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="1" selected="">Credit/Debit Card Payment</option><option value="0">ACH Payments</option></select>
                                        </div>

                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="to_payment_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!--6) Tenant Unpaid Charges Filters-->
<div class="container">
    <div class="modal fade" id="tenant_unpaid_charges" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_tenant_unpaid_charges">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Tenant Unpaid Charges Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="tu_charges_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="tu_charges_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>


                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="tu_charges_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="tu_charges_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="tu_charges_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="tu_charges_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="tu_charges_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!--7) Delinquent Tenant Filters-->
<div class="container">
    <div class="modal fade" id="delinquent_tenant" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_delinquent_tenant">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Delinquent Tenant Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="delinquent_tenant_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="delinquent_tenant_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="delinquent_tenant_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="delinquent_tenant_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="delinquent_tenant_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="delinquent_tenant_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!--8)Delinquent Tenant Email/Text Filters-->
<div class="container">
    <div class="modal fade" id="delinquent_tenant_email" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_delinquent_tenant_email">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Delinquent Tenant Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="dl_tenant_email_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="dl_tenant_email_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="dl_tenant_email_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="dl_tenant_email_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="dl_tenant_email_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="dl_tenant_email_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!--9)Consolidated Delinquent Tenant Filters-->
<div class="container">
    <div class="modal fade" id="consolidated_dlt" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_consolidated_dlt">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Consolidated Delinquent Tenant </h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            From Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="from_date" id="consolidated_dlt_from_date" class="is_required form-control from_date datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            To Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="to_date" id="consolidated_dlt_to_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="consolidated_dlt_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="consolidated_dlt_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="consolidated_dlt_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                            </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="consolidated_dlt_tenant_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="consolidated_dlt_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!--10)Short Term Renter List Filters-->
<div class="container">
    <div class="modal fade" id="short_term_renter" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_short_term_renter">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Short Term Renter List Tenant </h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="shrt_term_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="shrt_term_end_date" class="is_required form-control  end_date datefield calander" readonly="readonly">
                                        </div>
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="shrt_term_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="shrt_term_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="shrt_term__btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!--10)Tenant EEO Report Filters--->
<div class="container">
    <div class="modal fade" id="tenant_eeo_report" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_tenant_eeo_report">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Tenant EEO Report Filters </h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="eeo_report_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="eeo_report_end_date" class="is_required form-control  end_date datefield calander" readonly="readonly">
                                        </div>
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="eeo_report_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="eeo_report_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="eeo_report_status" class=" form-control selectfield " data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="tenant_eeo_report_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



