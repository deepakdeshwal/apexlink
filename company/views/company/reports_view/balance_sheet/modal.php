<!-- 1)Balance Sheet Model -->
<div class="container">
    <div class="modal fade" id="balance-sheet" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup-balance-sheet-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Balance Sheet Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="bl_sheet_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="bl_sheet_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="bl_sheet_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="bl_sheet_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->



<!-- 2)Balance Sheet Comparison Modal -->
<div class="container">
    <div class="modal fade" id="bl-comparison-modal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup-bl-comparison-modal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Balance-Sheet -2-year Comparison  </h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="bl_comparison_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="bl_comparison_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="bl_comparison_modal_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->



<!-- 3)Income Statement Detail Modal -->
<div class="container">
    <div class="modal fade" id="income_statement_detail" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_income_statement_detail">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Income Statement Details Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="is_detail_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="is_detail_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="is_detail_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="is_detail_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->


<!-- 4)Income Statement Comparison 2Years -->
<div class="container">
    <div class="modal fade" id="income_statement_comparison" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_income_statement_comparison">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Income Statement Comparison(2 Years)</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="is_comparison_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="is_comparison_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="is_comparison_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->



<!-- 5)Profit & Loss Modal -->
<div class="container">
    <div class="modal fade" id="profit_loss" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_profit_loss">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Profit & Loss Filters </h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="profit_loss_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="profit_loss_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="profit_loss_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="profit_loss_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->

<!-- 6)Profit and Loss Detail -->
<div class="container">
    <div class="modal fade" id="profit_loss_detail" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_profit_loss_detail">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Profit & Loss Details Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="pl_detail_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="pl_detail_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="pl_detail_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="pl_detail_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!-- 7)Profit and Loss 12Months -->
<div class="container">
    <div class="modal fade" id="profit_loss_12months" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_profit_loss_12months">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Profit & Loss -12-Months Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            From Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="from_date" id="pl_12months_from_date" class="is_required form-control from_date datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            To Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="to_date" id="pl_12months_to_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="pl_12months_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="pl_12months_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="pl_12months_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!-- 8)Profit & Loss 2-Year Comparison -->
<div class="container">
    <div class="modal fade" id="profit_loss_2years" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_profit_loss_2years">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Profit & Loss 2-Year Comparison</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="pl_2years_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="pl_2years_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="pl_2years_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->


<!-- 9)Profit and Loss Consolidated -->
<div class="container">
    <div class="modal fade" id="profit_loss_consolidated" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_profit_loss_consolidated">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Profit & Loss Consolidated</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="pl_consolidated_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="pl_consolidated_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="pl_consolidated_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="pl_consolidated_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!-- 10)A/R Aging Summary -->
<div class="container">
    <div class="modal fade" id="ar_aging_summary" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_ar_aging_summary">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Account Totals Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            From Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="from_date" id="ar_aging_summary_from_date" class="is_required form-control from_date datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            To Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="to_date" id="ar_aging_summary_to_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="ar_aging_summary_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->




<!-- 11)A/R Aging Detail -->
<div class="container">
    <div class="modal fade" id="ar_aging_detail" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_ar_aging_detail">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">A/R Aging Detail Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="current_date" id="ar_aging_detail_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                            </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="ar_aging_detail_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->

<!-- 12 AP Aging Summary -->
<div class="container">
    <div class="modal fade" id="ap_aging_summary" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_ap_aging_summary">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">AP Aging Summary Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                            
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="ap_aging_summary_portfolio_id" class="form-control"></select>
                                        </div> 
                                     
                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="ap_aging_summary_status" class=" form-control selectfield onChangeStatus" data_next="tenant">
                                                <option value="">Select</option><option value="0" selected="">Active</option><option value="1">InActive</option></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="ap_aging_summary_property_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Vendor<em class="red-star">*</em></label>
                                            <select multiple name="vendor[]" id="ap_aging_summary_vendor_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Ammount Due
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="amountdue" id="ap_aging_summary_amountdue" class="form-control" readonly="readonly">
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="ap_aging_summary_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!-- 13 AP Aging Details -->
<div class="container">
    <div class="modal fade" id="ap_aging_detail" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_ap_aging_detail">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">AP Aging Details Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="ap_aging_detail_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="ap_aging_detail_property_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Vendor<em class="red-star">*</em></label>
                                            <select multiple name="vendor[]" id="ap_aging_detail_vendor_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Ammount Due
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="amountdue" id="ap_aging_summary_amountdue" class="form-control" readonly="readonly">
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="ap_aging_summary_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!-- 14)Audit Log -->
<div class="container">
    <div class="modal fade" id="audit_log" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_audit_log">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Audit Log Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="audit_log_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="audit_log_end_date" class="is_required form-control  end_date datefield calander" readonly="readonly">
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="audit_log_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->

<!-- 15)Income Register Filters -->
<div class="container">
    <div class="modal fade" id="income_register" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_income_register">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Income Register Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="income_register_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="income_register_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="income_register_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="income_register_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->




<!-- 16)Charge Detail Filters -->
<div class="container">
    <div class="modal fade" id="charge_detail" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_charge_detail">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Charge Detail Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="charge_detail_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="charge_detail_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="charge_detail_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="charge_detail_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->


<!-- 17 Expenses by Vendor -->
<div class="container">
    <div class="modal fade" id="expenses_by_vendor" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_expenses_by_vendor">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Expense by Vendor Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="expenses_by_vendor_start_date" class="is_required start_date form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="expenses_by_vendor_end_date" class="is_required end_date form-control datefield calander" readonly="readonly">
                                        </div>
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="expenses_by_vendor_portfolio_id" class="form-control"></select>
                                        </div> 
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="expenses_by_vendor_property_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="expenses_by_vendor_status" class=" form-control selectfield onChangeStatus" data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Vendor<em class="red-star">*</em></label>
                                            <select multiple name="vendor[]" id="expenses_by_vendor_vendor_id" class="form-control"></select>
                                        </div>

                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="expenses_by_vendor_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->




<!-- 18 Property Statement -->
<div class="container">
    <div class="modal fade" id="property_statement" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_property_statement">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Property Statement Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="property_statement_start_date" class="is_required start_date form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="property_statement_end_date" class="is_required end_date form-control datefield calander" readonly="readonly">
                                        </div>
                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="property_statement_portfolio_id" class="form-control"></select>
                                        </div> 
                                       

                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="property_statement_status_id" class=" form-control selectfield onChangeStatus" data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                        </div>

                                         
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="property_statement_property_id" class="form-control"></select>
                                        </div>

                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="property_statement_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->



<!-- 19)Bank Account Activity Model -->
<div class="container">
    <div class="modal fade" id="bank_account" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_bank_account">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Bank Account Activity Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                    
                                        <div class="col-sm-12">
                                            <label is_required="yes">Bank name<em class="red-star">*</em></label>
                                            <select multiple name="bank_name" id="bank_account_bank_name"  class="form-control"></select>
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="bank_account_start_date" class="is_required start_date form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="bank_account_end_date" class="is_required end_date form-control datefield calander" readonly="readonly">
                                        </div>
                                       
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="bank_account_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->



<!-- 20)Voided Checks Filter -->
<div class="container">
    <div class="modal fade" id="voided_check" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_voided_check">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Voided Checks Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="voided_check_start_date" class="is_required start_date form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="voided_check_end_date" class="is_required end_date form-control datefield calander" readonly="readonly">
                                        </div>
                                       
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="voided_check_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->


<!-- 21)Trial Balance Model -->
<div class="container">
    <div class="modal fade" id="trial_balance" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_trial_balance">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Trial Balance Filter</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="trial_balance_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="trial_balance_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="trial_balance_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="trial_balance__btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->


<!-- 22)Trial Balance Consolidated Model -->
<div class="container">
    <div class="modal fade" id="trial_balance_consolidated" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_trial_balance_consolidated">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Trial Balance Consolidated</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                         <div class="col-sm-12">
                                            <label is_required="yes">
                                            Start Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="start_date" id="trial_balance_consolidated_start_date" class="is_required start_date form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            End Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="end_date" id="trial_balance_consolidated_end_date" class="is_required end_date form-control datefield calander" readonly="readonly">
                                        </div>
                                       
                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="trial_balance_consolidated_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="trial_balance_consolidated_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="trial_balance_consolidated_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->

<!-- 23)Adjusted Trial Balance Model -->
<div class="container">
    <div class="modal fade" id="adjust_trial_balance" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_adjust_trial_balance">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Adjusted Trial Balance Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->

                                    <div id="modalDataClass" class="row">
                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="current_date" id="at_balance_current_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                            <select multiple name="portfolio[]" id="at_balance_portfolio_id" onchange="" class="form-control"></select>
                                        </div>
                                            
                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                                <select multiple name="property[]" id="at_balance_property_id" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="at_balance__btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End  -->



                    <!-- Maintenance Reports -->
<!-- 1)Open Work Order -->
<div class="container">
    <div class="modal fade" id="open_work_order" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_open_work_order">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Open Work Order</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="ow_order_start_date" id="tenant_ledger_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="ow_order_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="ow_order_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="ow_order_property_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Tenant Name<em class="red-star">*</em></label>
                                            <select multiple name="tenant[]" id="ow_order_tenant_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Estimated Cost</label>
                                            <input type="text" name="estimated_cost" id="ow_order_estimated_cost" class="form-control">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Work Order Numbers</label>
                                            <input type="text" name="work_order_number" id="ow_order_work_order_number" class="form-control">  
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="ow_order_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


                    <!-- Owner Reports -->
<!-- 1)Owner Statement -->
<div class="container">
    <div class="modal fade" id="owner_statement" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_owner_statement">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Owner Statement  Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="owner_statement_start_date" id="tenant_ledger_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="owner_statement_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="owner_statement_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="owner_statement_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="owner_statement_status" class=" form-control selectfield" data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Owner Name</label>
                                            <select multiple name="owner_name[]" id="owner_statement_owner_name" class="form-control"></select>
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="owner_statement_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->




                    <!-- Vendor Reports -->
<!-- 1)Expense by Vendor -->
<div class="container">
    <div class="modal fade" id="expense_by_vendor" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_expense_by_vendor">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Expense by Vendor Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="eb_vendor_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>
                                            

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="eb_vendor_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                                <label is_required="yes">Portfolio<em class="red-star">*</em></label>
                                                <select multiple name="portfolio[]" id="eb_vendor_portfolio_id" class="form-control"></select>
                                        </div> 

                                        <div class="col-sm-12">
                                            <label is_required="yes">Property<em class="red-star">*</em></label>
                                            <select multiple name="property[]" id="eb_vendor_property_id" class="form-control"></select>
                                        </div>


                                        <div class="col-sm-12">
                                            <label is_required="no">Status</label>
                                                <select type="select" name="status" id="eb_vendor_status" class=" form-control selectfield onChangeStatus" data_next="tenant">
                                                <option value="">Select</option><option value="1" selected="">Active</option><option value="0">InActive</option></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Vendor<em class="red-star">*</em></label>
                                            <select multiple name="vendor[]" id="eb_vendor_vendor_id" class="form-control"></select>
                                        </div>

                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="eb_vendor_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->

<!-- 2)Vendor Bills -->
<div class="container">
    <div class="modal fade" id="vendor_bill" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_vendor_bill">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Transaction by Vendor Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="vendor_bill_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="vendor_bill_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Vendor<em class="red-star">*</em></label>
                                            <select multiple name="vendor[]" id="vendor_bill_vendor_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Bill Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="bill_date" id="vendor_bill_bill_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Check/Refrence Number</label>
                                            <input type="text" name="check_number" id="vendor_bill_check_number" class="form-control">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Paid Amount</label>
                                            <input type="text" name="paid_amount" id="vendor_bill_paid_amount" class="form-control">  
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="vendor_bill_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->

<!-- 3)Transaction by Vendor Filters -->
<div class="container">
    <div class="modal fade" id="transaction_vendor" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popup_transaction_vendor">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Vendor Bills Filters</h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <!-- <div id="modalDataClass" class="row">
                                    </div> -->
                                    <div id="modalDataClass" class="row">

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                Start Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="tv_start_date" class="is_required form-control start_date datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                                <label is_required="yes">
                                                End Date
                                                <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="tv_end_date" class="is_required form-control end_date datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">Vendor<em class="red-star">*</em></label>
                                            <select multiple name="vendor[]" id="tv_vendor_id" class="form-control"></select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="yes">
                                            Bill Date
                                            <em class="red-star">*</em></label>
                                            <input type="text" name="bill_date" id="tv_bill_date" class="is_required form-control datefield calander" readonly="readonly">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Check/Refrence Number</label>
                                            <input type="text" name="check_number" id="tv_check_number" class="form-control">
                                        </div>

                                        <div class="col-sm-12">
                                            <label is_required="no">Paid Amount</label>
                                            <input type="text" name="paid_amount" id="tv_paid_amount" class="form-control">  
                                        </div>
                                    </div>
                                  
                                    <div class="btn-outer text-right"> 
                                        <input type="button" class="blue-btn" id="transaction_vendor_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End -->


