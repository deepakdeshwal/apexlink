<?php
/**
 * Created by PhpStorm.
 * User: chughraghav
 * Date: 6/03/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
$edit_id = (isset($_REQUEST['id']))?$_REQUEST['id']:'';
$base_url=$_SERVER['SERVER_NAME'];
?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- MAIN Navigation Ends -->
    <section class="main-content view-owner-page">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="content-section">
                        <!--Tabs Starts -->
                        <div class="container lease-container">
                    <div class="lease-links"><a id="emailOpenModalBtn">Email This Report</a><a class="show_fliter">Filters</a></div>
                    
                    <div class="lease-outer">
                    <div class="lease-top">
                       <ul class="list-inline">
                          <li></li>
                          <li ><a href="#" class="input-outer"><input type="text" class="form-control" id="usr" value="1" title="Current Page"> <span> of   </span><span id="totalPages">1</span></a> </li>
                          <li></li>
                       </ul>
                       <div class="find-outer">
                          <ul class="list-inline">
                              <li>
                                 <a href="#" class="input-outer"><input type="text" class="form-control" id="usr" title="Find Text in Report"> <span> </span></a>
                              </li>
                              <li><a href="" title="Find">Find</a></li>
                              <li>|</li>
                              <li><a href="?page=' . ($page + 1) . '" title="Find Next">Next</a></li>
                              <li class="save-list" title="Export drop down menu">
                                 <button class="ddlList"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i></button>
                                 <div id="demo">
                                    <ul style=" display: none; cursor: pointer; background-color: #ECE9D8; color: #337ab7" class="save-document-list doc_download">
                                       <li>Word</li>
                                       <li>Excel</li>
                                       <li>PowerPoint</li>
                                       <li>PDF</li>
                                       <li>TIFF file</li>
                                       <li>MHTML(web archive)</li>
                                       <li>CSV(comma delimited)</li>
                                       <li>XML file with report data</li>
                                     </ul>
                                 </div>
                              </li>
                              <li><a href=""></a></li>
                              <li><a href="?page=1"><i class="fa fa-refresh" title="Refresh "aria-hidden="true"></i></a></li>
                              <li><a class="print_class" style="cursor: pointer;"><i class="fa fa-download" title="Download" aria-hidden="true"></i></a></li>
                            </ul>
                       </div>
                       <div class="grid-outer htmldownload" id="htmldownload">
                        <div class="table-responsive Lease-table">
                        <div class="rentroll">
                        <table width="100%" border="0" style="margin-top: 30px;">';
                            <tr>
                                <td><img style="width: 137px; height: 49px;" src="http://phytotherapy.in:8097/company/images/logo.png" alt=""> </td>
                            </tr>';
                           <tr>
                                <td style="text-align: center; font-weight: 700; font-size: 16px;">' . $title . '</td>
                            </tr>
                             <tr><td style="text-align: center; font-weight: 700; padding: 20px 0; font-size: 16px;">' . $filter_date . '</td>
                            </tr>
                            <tr>
                            <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; background: #262750; color: #fff; padding: 5px; font-size: 13px; font-weight: 700;" class="lease-hading"> ' . $table_title2 . '<br> ' . $table_title . '</td>
                            </tr>
                        </table> 
                                               
                        <table width="100%" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; font-size: 12px;" class="table table-hover table-dark reports_table">
                        <thead>
                           <tr ></tr>
                        </thead>
                    </div>
                    </div>
                    <!--tab Ends -->
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



    function goBack() {
        window.history.back();
    }
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
</script>

<!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/building/buildingView.js"></script>-->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/unit/viewOwner.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/viewOwner.js"></script>
<!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/company/people/owner/complaintsOwner.js"></script>-->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>

