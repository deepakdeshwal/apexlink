<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
include_once(COMPANY_DIRECTORY_URL . "/views/company/tenants/modals.php");
?>
<style>
   .main-tabs .nav-tabs li.active span.tab-count {
       color: #fff;
   }
</style>
    <link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/smartmove.css"/>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
<script>var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";</script>

    <section class="main-content">
        <div class="container-fluid" id="guestlisting">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Leases &gt;&gt; <span>List of Guest Cards</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">

                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="/GuestCard/ListGuestCard" >Guest Cards</a></li>
                            <li role="presentation"><a href="/RentalApplication/RentalApplications">Rental Applications</a></li>
                            <li role="presentation"><a href="/Lease/ViewEditLease" >Leases</a></li>
                            <li role="presentation"><a href="/Lease/Movein" >Move-In</a></li>
                        </ul>
                        <div class="property-status">
                        <div class="atoz-outer2 ">
                                    <span class="apex-alphabets" id="apex-alphafilter" style="display:none;">
                                    <span class="AtoZ"></span></span>
                            <span class="AZ" id="AZ">A-Z</span>
                            <span id="allAlphabet" style="cursor:pointer;">All</span>
                        </div>
                    </div>


                        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/leases/layout/right-nav.php");?>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="guest-cards">
                                <!-- Sub Tabs Starts-->
                                <div class="sub-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#contact-tenant" aria-controls="home" role="tab" data-toggle="tab" data_id="1">Active<span class="active-count tab-count"></span></a></li>
                                        <li role="presentation"><a href="#contact-owner" aria-controls="profile" role="tab" data-toggle="tab" data_id="3">View Scheduled<span class="tab-count">0</span></a></li>
                                        <li role="presentation"><a href="#contact-vendor" aria-controls="home" role="tab" data-toggle="tab" data_id="4">Application Generated<span class="tab-count">0</span></a></li>
                                        <li role="presentation"><a href="#contact-users" aria-controls="profile" role="tab" data-toggle="tab" data_id="5">Lease Generated<span class="tab-count">0</span></a></li>
                                        <li role="presentation"><a href="#contact-users" aria-controls="profile" role="tab" data-toggle="tab" data_id="2">Archived<span class="archived-count tab-count" data-id="2">0</span></a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="regular-rent">
                                            <div class="property-status">
                                                <div class="row">

                                                    <div class="col-sm-12">
                                                        <div class="btn-outer text-right">
                                                            <a href="/GuestCard/NewGuestCard" class="blue-btn">New Guest Card</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">

                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="apx-table">
                                                                        <div class="grid-outer">
                                                                            <div class="table-responsive">
                                                                             <table id="guestcard_table">

                                                                             </table>
                                                                            </div>
                                                                        </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Regular Rent Ends -->
                                        <div role="tabpanel" class="tab-pane" id="shortterm-rent">

                                        </div>
                                    </div>
                                </div>
                                <!-- Sub tabs ends-->
                            </div>
                        </div>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>



        <div class="container-fluid" id="guestfilelibrary" style="display: none">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Leases &gt;&gt; <span>List of Guest Cards</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                                <input type="hidden" id="hidden_id_view">
                                <input type="hidden" id="hidden_id_view2">
                            </div>
                        </div>
                    </div>
                </div>

                <input type="hidden" id="building_property_id" val=""/>
<!--                <input type="hidden" value="--><?php //echo $_GET['id'];?><!--" name="owner_id" class="owner_id">-->
                <div class="col-sm-12">
                    <div class="content-section">
                        <!--Tabs Starts -->

                        <div class="main-tabs">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="/GuestCard/ListGuestCard" >Guest Cards</a></li>
                                <li role="presentation"><a href="/RentalApplication/RentalApplications/">Rental Applications</a></li>
                                <li role="presentation"><a href="/Lease/ViewEditLease/" >Leases</a></li>
                                <li role="presentation"><a href="/Lease/Movein/0/" >Move-In</a></li>
                            </ul>
                            <!-- Nav tabs -->

                            <!-- Nav tabs -->
                            <div class="tab-content">
<!--                                <div role="tabpanel" class="tab-pane active" id="guest-cards">-->
<!--                                    <div class="form-outer">-->
<!--                                        <div class="form-hdr">-->
<!--                                            <h3>General Information <a onclick="goBack()" class="back" style="cursor: pointer;"><i class="fa fa-angle-double-left" aria-hidden="true" ></i> Back</a></h3>-->
<!--                                        </div>-->
<!---->
<!--                                    </div>-->
<!--                                </div>-->
                                <!-- Form Outer Ends -->

                                <div class="form-outer form-outer2">
                                    <div class="form-hdr">
                                        <h3>Contact Information</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="detail-outer">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Name :</label>
                                                        <span class="name">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Zip / Postal Code :</label>
                                                        <span class="zipcode">N/A</span>
                                                    </div>

                                                    <div class="col-xs-12">
                                                        <label class="text-right">Country  :</label>
                                                        <span class="country">N/A</span>
                                                    </div>
                                                    <div id="ethnicity_to_dob_div">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">State / Province :</label>
                                                            <span class="state">N/A</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">City :</label>
                                                            <span class="city">N/A</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">SSN/SIN/ID:</label>
                                                            <span class="ssn_sin_id">N/A</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Ethnicity  :</label>
                                                            <span class="ethnicity_name">N/A</span>
                                                        </div>

                                                        <div class="col-xs-12">
                                                            <label class="text-right">Hobbies :</label>
                                                            <span class="hobbiess">N/A</span>
                                                        </div>


                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Gender :</label>
                                                        <span class="gender">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Address 1 :</label>
                                                        <span class="address1">N/A</span>
                                                    </div>

                                                    <div class="col-xs-12">
                                                        <label class="text-right">Phone Number :</label>
                                                        <span class="phone_number">N/A</span>
                                                    </div>
                                                    <div id="ethnicity_to_dob_div">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Extension :</label>
                                                            <span class="work_phone_extension">N/A</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Email :</label>
                                                            <span class="email">N/A</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Preferred Contact Time :</label>
                                                            <span class="contact_time">N/A</span>
                                                        </div>

                                                        <div class="col-xs-12">
                                                            <label class="text-right">Referral Source  :</label>
                                                            <span class="referrals">N/A</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">MaritalStatus :</label>
                                                            <span class="marital_status">N/A</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Veteran Status :</label>
                                                            <span class="veteran_name">N/A</span>
                                                        </div>


                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>

                                </div>


                                <div class="form-outer form-outer2">
                                    <div class="form-hdr">
                                        <h3>Unit Preference</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="detail-outer">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Bedroom :</label>
                                                        <span class="bedrooms_no"></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Bathroom :</label>
                                                        <span class="bathrooms_no"></span>
                                                    </div>

                                                    <div class="col-xs-12">
                                                        <label class="text-right">Floor :</label>
                                                        <span class="floor_no"></span>
                                                    </div>
                                                    <div id="ethnicity_to_dob_div">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Expected MI Date :</label>
                                                            <span class="expected_movein"></span>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Min Rent :</label>
                                                        <span class="base_rent"></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Max Rent :</label>
                                                        <span class="market_rent"></span>
                                                    </div>

                                                    </div>
                                                </div>


                                            </div>

                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>
                                        </div>


                                    </div>

                                </div>





                            </div>
                            <!-- Form Outer Ends -->

                            <!-- Form Outer starts -->
                            <div class="form-outer form-outer2">

                                <div class="form-hdr">
                                    <h3>Emergency Contact Details</h3>
                                </div>
                                <div class="form-data">
                               <div id="emergency_detail"></div>

                                    <div class="edit-foot">
                                        <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>

                                </div>
<!--                                <div class="form-data">-->
<!--                                    <div class="detail-outer emergency_detail_data">-->
<!--                                        <div class=row style='margin-bottom: 10px;'>-->
<!--                                            <div class=col-sm-6>-->
<!--                                                <div class=col-xs-12>-->
<!--                                                    <label class=text-right>Emergency Contact Name : </label>-->
<!--                                                    <span class=emeregency_name></span>-->
<!--                                                </div>-->
<!--                                                <div class=col-xs-12>-->
<!--                                                    <label class=text-right>Country Code :</label>-->
<!--                                                    <span class=country_code> </span>-->
<!--                                                </div>-->
<!--                                                <div class=col-xs-12>-->
<!--                                                    <label class=text-right>Emergency Contact Phone : </label>-->
<!--                                                    <span class=emeregency_contact></span>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                            <div class=col-sm-6>-->
<!--                                                <div class=col-xs-12>-->
<!--                                                    <label class=text-right>Emergency Contact Email : </label>-->
<!--                                                    <span class=emeregency_email></span>-->
<!--                                                </div>-->
<!--                                                <div class=col-xs-12>-->
<!--                                                    <label class=text-right>Emergency Contact Relation : </label>-->
<!--                                                    <span class=emeregency_relation></span>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="edit-foot">-->
<!--                                        <a href="javascript:;" class="edit_redirection" redirection_data="emergencyInfoDiv">-->
<!--                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>-->
<!--                                            Edit-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                </div>-->
                            </div>
                            <!-- Form Outer Ends -->




                        <div class="form-outer form-outer2">

                            <div class="table-responsive">
                                <table class="table" id="unit_table"></table>
                            </div>

                        </div>





                            <!-- Form Outer Starts -->
                            <div class="form-outer" style="margin-bottom: 20px" id="flags">
                                <div class="form-hdr">
                                    <h3>Flag Bank</h3>
                                </div>
                                <div class="form-data">
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <div class="apx-table listinggridDiv">
                                                                    <div class="table-responsive">
                                                                        <table id="guestFlags-table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="edit_redirection" redirection_data="flagsBankInfoDiv" >
                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->




                            <!-- Form Outer starts -->
                        <div class="form-outer">
                            <div class="form-hdr">
                                <h3>Notes </h3>
                            </div>
                            <div class="form-data">

                        <div id="notes">
                        </div>
                        </div>
                        </div>
                            <!-- Form Outer Ends -->

                            <!-- Form Outer Ends -->
                            <div class="form-outer" id="filelibrary">
                                <div class="form-hdr">
                                    <h3>File Library</h3>
                                </div>
                                <div class="form-data">
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="grid-outer">
                                                            <div class="apx-table">
                                                                <div class="table-responsive">
                                                                    <table id="TenantFiles-table" class="table table-bordered">
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="edit_redirection" redirection_data="fileLibraryIfoDiv">
                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->
                            <!-- Form Outer starts -->
                            <div class="form-outer" id="customfeilddata">
                                <div class="form-hdr">
                                    <h3>Custom Fields</h3>
                                </div>
                                <div class="form-data">
                                    <input type="hidden" id="customFieldModule" name="module" value="owner">
                                    <div class="col-sm-7 custom_field_html_view_mode">
                                        No Custom Fields
                                    </div>


                                    <div class="edit-foot">
                                        <a href="javascript:;" class="edit_redirection" redirection_data="customFieldsInfoDiv">
                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                        </div>


                    </div>
                    <!--tab Ends -->

                </div>

            <div class="btn-outer apex-btn-block left">
                <a class="blue-btn rental_application" value="Generate Rental Application">Generate Rental Application</a>

                <a class="blue-btn generate_lease" value="Generate Lease">Generate Lease</a>

            </div>
        </div>
            </div>
        </div>




    </section>
</div>
<!-- Wrapper Ends -->
    <div class="container">
        <div class="modal fade" id="backgroundReport" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content  pull-left" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-left">Background Reports</h4>
                    </div>
                    <div class="modal-body">
                        <div class="tenant-bg-data">
                            <h3 style="text-align: center;">
                                Open Your Free Account Today!
                            </h3>
                            <p>
                                ApexLink is pleased to partner with Victig Screening Solutions to provide you with
                                the highest quality, timely National Credit, Criminal and Eviction Reports.
                            </p>
                            <div class="grid-outer " style="max-width: 1421px;">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                    <tr>
                                        <th colspan="3" align="center" class="bg-Bluehdr">
                                            Package Pricing<br>
                                            Properties Managing
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="29%" align="left">
                                            National Criminal
                                        </td>
                                        <td width="29%" align="left">
                                            National Criminal and Credit
                                        </td>
                                        <td width="42%" align="left">
                                            National Criminal, Credit and State Eviction
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            $15
                                        </td>
                                        <td align="left">
                                            $20
                                        </td>
                                        <td align="left">
                                            $25
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <p>
                                    *$100 fee to authorize credit report
                                </p>
                            </div>
                            <div class="grid-outer " style="max-width: 1421px;">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                    <tr>
                                        <th colspan="5" align="center" class="bg-Bluehdr">
                                            High Volume Users and Larger Properties
                                        </th>
                                    </tr>
                                    <tr>
                                        <td colspan="5" align="left">
                                            <p align="center">
                                                <strong>Contact our Victig Sales Team for custom pricing</strong>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="26%" align="center">
                                            <a href="mailto:sales@victig.com"><strong>sales@victig.com</strong></a>
                                        </td>
                                        <td width="15%" align="center">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </td>
                                        <td width="21%" align="center">
                                            <strong>866.886.5644 </strong>
                                        </td>
                                        <td width="13%" align="center">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </td>
                                        <td width="25%" align="center">
                                            <a href="http://www.victig.com" target="_blank"><strong>www.victig.com</strong></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="div-full">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                    <tr>
                                        <td width="52%" height="20" style="font-size: 14px;" align="left">
                                            Already have an Account? <a href="https://apexlink.instascreen.net" target="_blank">
                                                Log in here
                                            </a>
                                        </td>
                                        <td width="48%" rowspan="2" align="right">
                                            <a href="https://www.victig.com/wp-content/uploads/2016/05/sample-tenant-report.jpg" target="_blank">
                                                <!--                                            <input value="View Sample Report" class="blue-btn fr" type="button" spellcheck="true" autocomplete="off">-->
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="font-size: 14px;">
                                            New Users: <a href="https://www.victig.com/victig-on-boarding/" target="_blank">
                                                Click
                                                here to register
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="div-full3">
                                <p>
                                    Please be advised that all background services are provided by Victig Screening
                                    Solutions. Direct all contact and questions pertaining to background check services
                                    to <a href="mailto:sales@victig.com">sales@victig.com</a>, 866.886.5644, <a href="http://www.victig.com" target="_blank">www.victig.com</a>
                                </p>
                            </div>
                            <div class="notice">
                                NOTICE: The use of this system is restricted. Only authorized users may access this
                                system. All Access to this system is logged and regularly monitored for computer
                                security purposes. Any unauthorized access to this system is prohibited and is subject
                                to criminal and civil penalties under Federal Laws including, but not limited to,
                                the Computer Fraud and Abuse Act and the National Information Infrastructure Protection
                                Act.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="modal fade" id="backGroundCheckPop" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content  pull-left" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Disclaimer</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-sm-12">
                            <p style="font-size: 14px; line-height: 20px;"> I authorize ApexLink Inc. to submit this request to generate and present a background/any check report for the Person/Company I have listed. I understand that ApexLink Inc. is not responsible or liable for any data breach, loss/loss of profits or any claim against the filer (Myself or my company) by any party. I understand that in no event is ApexLink Inc. liable for any and all damages. I also understand that ApexLink Inc. grants no implied warranties, including without limitation, warranties, of merchantability, or of fitness for any particular purpose. </p></div>
                        <div class="col-sm-12"><input type="checkbox" name="" class="background_check_open"><label style="display: inline;"> Check to proceed further.</label></div>
                        <div class="col-sm-12 btn-outer mg-btm-20">
                            <input type="button" class="blue-btn"  data-dismiss="modal" value="Cancel">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="modal fade" id="print_envelope_guest" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content pull-left" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="blue-btn" id='print_hoa'  onclick="PrintElem('#modal-body-hoa')" onclick="window.print();">Print</button>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body" id="modal-body-print-env">
                        <div class="col-sm-12">
                            <div class="col-sm-3 print_company_address">
                                <label>ApexLink Inc.</label>
                                <label>950114 Snow CT.</label>
                                <label>Marco Island, FL 34145</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-5">&nbsp;</div>
                            <div class="col-sm-5 print_tenant_address">
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Footer Ends -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var default_name = "<?php echo $_SESSION[SESSION_DOMAIN]['default_name']; ?>";


</script>
<script>var upload_url = "<?php echo SITE_URL; ?>";</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/leases/guestCard/guestCardListing.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/smartMove.js"></script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>