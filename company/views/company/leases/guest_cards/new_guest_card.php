<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 bread-search-outer">
                    <div class="breadcrumb-outer">
                        Guest Card >> <span>New Guest Card</span>
                    </div>
                    <div class="easy-search">
                        <input placeholder="Easy Search" type="text"/>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="content-section">
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#guest-cards" >Guest Cards</a></li>
                                <li role="presentation"><a href="/RentalApplication/RentalApplications/1" >Rental Applications</a></li>
                                <li role="presentation"><a href="#leases" >Leases</a></li>
                                <li role="presentation"><a href="#moveIn" >Move-In</a></li>
                            </ul>
                           
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="guest-cards">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>General Information <a class="back" href="/GuestCard/ListGuestCard"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                <form id="generallease" class="reset-all">
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Salutation</label>
                                                        <select class="form-control" name="salutation" id="salutation">
                                                            <option value="">Select</option>
                                                            <option value="Dr.">Dr.</option>
                                                            <option value="Mr.">Mr.</option>
                                                            <option value="Mrs.">Mrs.</option>
                                                            <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                                            <option value="Ms.">Ms.</option>
                                                            <option value="Sir">Sir</option>
                                                            <option value="Madam">Madam</option>
                                                            <option value="Brother">Brother</option>
                                                            <option value="Sister">Sister</option>
                                                            <option value="Father">Father</option>
                                                            <option value="Mother">Mother</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>First Name <em class="red-star">*</em></label>
                                                        <input class="form-control capital" type="text" name="first_name" id="first_name" placeholder="First Name"/>
                                                    <span id="first_nameErr" class="val_message_first red-star" style="display: none">*This field is required</span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Middle Name</label>
                                                        <input class="form-control capsOn" type="text" name="middle_name" id="middle_name" placeholder="Middle Name"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Last Name <em class="red-star">*</em></label>
                                                        <input class="form-control capsOn" type="text" name="last_name" id="last_name" placeholder="Last Name"/>
                                                        <span id="last_nameErr" class="val_message_last red-star" style="display: none">*This field is required</span>

                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3 maiden_name_hide" style="display: none">
                                                        <label>Maiden Name</label>
                                                        <input class="form-control capsOn" type="text" name="maiden_name" id="maiden_name" placeholder="Maiden Name"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Nick Name</label>
                                                        <input class="form-control capsOn" type="text" name="nick_name" id="nick_name" placeholder="Nick Name"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Gender</label>
                                                        <select class="form-control" name="gender" id="general"><option value="0">Select</option>
                                                            <option value="1">Male</option>
                                                            <option value="2">Female</option>
                                                            <option value="3">Prefer Not to Say</option>
                                                            <option value="4">Other</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>SSN/SIN/ID</label>
                                                        <div class='multipleSsn clone-input' id="multipleSsn">
                                                            <input class="form-control add-input capsOn" type="text"
                                                                   id="ssn" name="ssn_sin_id[]" placeholder="SSN/SIN/ID">
                                                            <a class="add-icon ssn-remove-sign" href="javascript:;"
                                                               style="display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>
                                                            <a class="add-icon ssn-plus-sign" href="javascript:;"><i
                                                                        class="fa fa-plus-circle"
                                                                        aria-hidden="true"></i></a>
                                                        </div>

                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Address 1</label>
                                                        <input class="form-control capital" type="text" name="address1" id="address1"placeholder="Address 1"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Address 2</label>
                                                        <input class="form-control capital" type="text" name="address2" id="address2"placeholder="Address 2"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Address 3</label>
                                                        <input class="form-control capital" type="text" name="address3" id="address3"placeholder="Address 3"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Address 4</label>
                                                        <input class="form-control capital" type="text" name="address4" id="address4"placeholder="Address 4"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Zip/Postal Code</label>
                                                        <input class="form-control zipcode" type="text" name="zipcode" id="zipcode" placeholder="zipcode" value="<?php echo $_SESSION[SESSION_DOMAIN]['default_zipcode'] ?>"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Country</label>
                                                        <input class="form-control country capital" type="text" name="country" id="country" placeholder="country "/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>State / Province</label>
                                                        <input class="form-control state capital" type="text" name="state" id="state" placeholder="state "/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>City</label>
                                                        <input class="form-control city capital" type="text" name="city" id="city" placeholder="city "/>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>Email <em class="red-star">*</em></label>

                                                        <div class='multipleEmail clone-input'>
                                                            <input class="form-control add-input" type="text" name="email[]" id="email" placeholder="Email">

                                                            <a class="add-icon email-remove-sign" href="javascript:;">
                                                                <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                                            </a>
                                                            <a class="add-icon email-plus-sign" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                            <span id="last_nameErr" class="val_message_email red-star" style="display: none">*This field is required</span>

                                                        </div>

                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Preferred Contact Time</label>
                                                        <select class="form-control" name="preferred" id="preferred">
                                                            <option value="8:00am">8:00 am</option>
                                                            <option value="8:05am">8:05 am</option>
                                                            <option value="8:10am">8:10 am</option>
                                                            <option value="8:15am">8:15 am</option>
                                                            <option value="8:15am">8:15 am</option>
                                                            <option value="8:20am">8:20 am</option>
                                                            <option value="8:25am">8:25 am</option>
                                                            <option value="8:30am">8:30 am</option>
                                                            <option value="8:35am">8:35 am</option>
                                                            <option value="8:40am">8:40 am</option>
                                                            <option value="8:45am">8:45 am</option>
                                                            <option value="8:50am">8:50 am</option>
                                                            <option value="8:55am">8:55 am</option>
                                                            <option value="9:00am">9:00 am</option>
                                                            <option value="9:05am">9:05 am</option>
                                                            <option value="9:10am">9:10 am</option>
                                                            <option value="9:15am">9:15 am</option>
                                                            <option value="9:20am">9:20 am</option>
                                                            <option value="9:25am">9:25 am</option>
                                                            <option value="9:30am">9:30 am</option>
                                                            <option value="9:35am">9:35 am</option>
                                                            <option value="9:40am">9:40 am</option>
                                                            <option value="9:45am">9:45 am</option>
                                                            <option value="9:50am">9:50 am</option>
                                                            <option value="9:55am">9:55 am</option>
                                                            <option value="10:00am">10:00 am</option>
                                                            <option value="10:05am">10:05 am</option>
                                                            <option value="10:10am">10:10 am</option>
                                                            <option value="10:15am">10:15 am</option>
                                                            <option value="10:20am">10:20 am</option>
                                                            <option value="10:25am">10:25 am</option>
                                                            <option value="10:30am">10:30 am</option>
                                                            <option value="10:35am">10:35 am</option>
                                                            <option value="10:40am">10:40 am</option>
                                                            <option value="10:45am">10:45 am</option>
                                                            <option value="10:50am">10:50 am</option>
                                                            <option value="10:55am">10:55 am</option>
                                                            <option value="11:00am">11:00 am</option>
                                                            <option value="11:05am">11:05 am</option>
                                                            <option value="11:00am">11:00 am</option>
                                                            <option value="11:05am">11:05 am</option>
                                                            <option value="11:10am">11:10 am</option>
                                                            <option value="11:15am">11:15 am</option>
                                                            <option value="11:20am">11:20 am</option>
                                                            <option value="11:25am">11:25 am</option>
                                                            <option value="11:30am">11:30 am</option>
                                                            <option value="11:35am">11:35 am</option>
                                                            <option value="11:40am">11:40 am</option>
                                                            <option value="11:45am">11:45 am</option>
                                                            <option value="11:50am">11:50 am</option>
                                                            <option value="11:55am">11:55 am</option>
                                                            <option value="12:00am">12:00 am</option>
                                                        </select>
<!--                                                        <input class="form-control" type="text" name="preferred" id="preferred"/>-->
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Referral Source <a class="pop-add-icon additionalReferralResource" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a></label>
                                                        <select class="form-control" id="referral_id" name="referral_source" id="referral_source"><option>Ad/Sponsor</option></select>
                                                        <div class="add-popup" id="additionalReferralResource1">
                                                            <h4>Add New Referral Source</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Referral Source <em class="red-star">*</em></label>
                                                                        <input class="form-control reff_source1" type="text" placeholder="New Referral Source">
                                                                        <span class="red-star" id="reff_source"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source1" data-name="referral_source">Save</button>
                                                                        <button type="button" class="clear-btn clearFormReset1">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="primary-tenant-phone-row">
                                                            <div class="col-sm-3 col-md-3 clear">
                                                                <label>Phone Type</label>
                                                                <select class="form-control" name="phoneType[]" id="phone_type">
                                                                    <option value="0">Select</option>
                                                                    <option value="1">Mobile</option>
                                                                    <option value="2">Work</option>
                                                                    <option value="3">Fax</option>
                                                                    <option value="4">Home</option>
                                                                    <option value="5">Other</option>
                                                                </select>
                                                            </div>


                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Phone Number <em class="red-star">*</em></label>
                                                            <input class="form-control capsOn add-input phone_format phoner" type="text"
                                                                   name="phoneNumber[]" id="phone_number" placeholder="Phone Number">
                                                        </div>
                                                        <div class="col-sm-12 col-md-1 ext_phone" style="display: none;">
                                                            <label>Extension</label>
                                                            <input name="Extension[]" class="form-control" type="text" placeholder="Eg: + 161">
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Carrier <em class="red-star">*</em></label>
                                                            <select class="form-control" name="carrier[]" id="guestCarrier"></select>
                                                            <span id="carrierErr" class="val_message_carrier red-star" style="display: none;">*This field is required</span>
                                                        </div>

                                                            <div class="col-sm-3 col-md-3 countycodediv">
                                                                <label>Country Code</label>
                                                                <select class="form-control add-input" name="countryCode[]" id="guestCountries">
                                                                    <option value="">Select</option>
                                                                </select>
                                                                <a class="add-icon" href="javascript:;"><i
                                                                            class="fa fa-plus-circle"
                                                                            aria-hidden="true"></i></a>
                                                                <a class="add-icon" href="javascript:;" style=""><i
                                                                            class="fa fa-minus-circle"
                                                                            aria-hidden="true"></i></a>
                                                                <span id="carrierErr" class="val_message_phone red-star" style="display: none;">*This field is required</span>


                                                            </div>


                                                            <div class="clearfix"></div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Ethnicity  <a class="pop-add-icon selectPropertyEthnicity" href="javascript:;">
                                                                <i class="fa fa-plus-circle"aria-hidden="true"></i>
                                                            </a></label>
                                                        <select class="form-control" id="guestEthnicity" name="ethnicity"></select>
                                                        <div class="add-popup" id="selectPropertyEthnicity1">
                                                            <h4>Add New Ethnicity</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Ethnicity <em class="red-star">*</em></label>
                                                                        <input class="form-control ethnicity_src customValidateGroup capsOn" type="text" placeholder="Add New Ethnicity" data_required="true" >
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1"  data-table="tenant_ethnicity" data-cell="title" data-class="ethnicity_src" data-name="ethnicity">Save</button>
                                                                        <button type="button" class="clear-btn clearFormReset2">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--<div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Martial Status <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                        <select class="form-control" id="guestMarital" name="martial_status"></select>
                                                    </div>-->
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Martial Status <a class="pop-add-icon occupantPropertyMaritalStatus1" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                        <select class="form-control"
                                                                name="martial_status"
                                                                id="guestMarital">
                                                            <option value="0">select</option>
                                                            <option value="1">single</option>
                                                            <option value="2">married</option>
                                                        </select>
                                                        <div class="add-popup" id="occupantPropertyMaritalStatus1">
                                                            <h4>Add New Marital Status</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Marital Status <em class="red-star">*</em></label>
                                                                        <input class="form-control maritalstatus_src capsOn" type="text" placeholder="Add New Marital Status">
                                                                        <span class="customError required red-star" aria-required="true" id="maritalstatus_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_marital_status" data-cell="marital" data-class="maritalstatus_src" data-name="martial_status">Save</button>
                                                                        <button type="button" class="clear-btn clearFormReset3">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Hobbies <a class="pop-add-icon selectPropertyHobbies" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a></label>
                                                        <select class="form-control guestHobbies" name="guestHobbies[]" id="guestHobbies" multiple>
<!--                                                            <option value=""></option>-->
                                                        </select>
                                                        <div class="add-popup" id="selectPropertyHobbies1">
                                                            <h4>Add New Hobbies</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Hobbies <em class="red-star">*</em></label>
                                                                        <input class="form-control hobbies_src capsOn" type="text" placeholder="Add New Hobbies" data_required="true">
                                                                        <span class="red-star" id="hobbies_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1" data-table="hobbies" data-cell="hobby" data-class="hobbies_src" data-name="guestHobbies[]">Save</button>
                                                                        <button type="button" class="clear-btn clearFormReset4">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Veteran Status  <a class="pop-add-icon selectPropertyVeteranStatus" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a></label>
                                                        <select class="form-control" id="guestVeteran" name="veteran_status"></select>
                                                        <div class="add-popup" id="selectPropertyVeteranStatus1">
                                                            <h4>Add New VeteranStatus</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New VeteranStatus <em class="red-star">*</em></label>
                                                                        <input class="form-control veteran_src capsOn" type="text" placeholder="Add New VeteranStatus">
                                                                        <span class="red-star" id="veteran_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_veteran_status" data-cell="veteran" data-class="veteran_src" data-name="veteran_status">Save</button>
                                                                        <button type="button" class="clear-btn clearFormReset5">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <input type="hidden" value="" name="guestcard_id" class="form-control" id="guestcard_id"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Unit Preferences </h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Search Property <em class="red-star">*</em></label>
                                                        <select class="form-control" name="property_name" id="PropertyId"><option>Select</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Search Unit <em class="red-star">*</em></label>
                                                        <select class="form-control unitId" name="building" id="unitId"><option value="">Select</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Bedrooms</label>
                                                        <select class="form-control" name="bedroom" id="bedroomId">
                                                            <option value="">Select</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Bathroom <em class="red-star">*</em></label>
                                                        <select class="form-control" name="bathroom" id="bathroomId">
                                                            <option value="">Select</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Floor</label>
                                                        <select class="form-control" name="floor" id="floorId">
                                                            <option value="">Select</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Expected Move In Date</label>
                                                        <input class="form-control calander expected_move_in" type="text" name="expected_move_in">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">

                                                        <label>Minimum Rent <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                                        <input class="form-control" type="text" id="min_rent" placeholder="Minimum Rent"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Maximum Rent ($)</label>
                                                        <input class="form-control" type="text" id="max_rent" placeholder="Maximum Rent"/>
                                                    </div>

                                                    <div class="div-full">
                                                        <div class="check-outer">
                                                            <input type="checkbox" value="1" id="pet_frnd">
                                                            <label>Pet Friendly</label>
                                                        </div>
                                                        <div class="check-outer">
                                                            <input type="checkbox" value="1" id="park_frnd">
                                                            <label>Parking</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="btn-outer">
                                                            <button class="blue-btn guestCardSearch" id="guestSearch">Search</button>
                                                        </div>
                                                    </div>
                                                    <div class="grid-outer" id="search_table" style="display: none;">

                                                    <div class="col-sm-12">
                                                            <center class="table-responsive">
                                                                <table class="table table-hover table-dark" id="search_records">
                                                                </table>
                                                                <center>   <span id="no_records" style="display: none;">No Records</span></center>

                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Emergency Contact Details </h3>
                                        </div>
                                        <div class="form-data">
                                            <form id="emergency_details" class="reset-all">

                                                <div class="lease-emergency-contact">
                                                    <div class="row">

                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Emergency Contact Name</label>
                                                                <input class="form-control capsOn capital capsOn" type="text" id="emergency"
                                                                       name="emergency_contact_name[]" maxlength="50">
                                                            </div>

                                                            <div class="col-sm-3 col-md-3 ">
                                                                <label>Relationship</label>
                                                                <select class="form-control" id="relationship"
                                                                        name="emergency_relation[]">
                                                                    <option value="">Select</option>
                                                                    <option value="1">Brother</option>
                                                                    <option value="2">Daughter</option>
                                                                    <option value="3">Employer</option>
                                                                    <option value="4">Father</option>
                                                                    <option value="5">Friend</option>
                                                                    <option value="6">Mentor</option>
                                                                    <option value="7">Mother</option>
                                                                    <option value="8">Neighbor</option>
                                                                    <option value="9">Nephew</option>
                                                                    <option value="10">Niece</option>
                                                                    <option value="11">Owner</option>
                                                                    <option value="12">Partner</option>
                                                                    <option value="13">Sister</option>
                                                                    <option value="14">Son</option>
                                                                    <option value="15">Spouse</option>
                                                                    <option value="16">Teacher</option>
                                                                    <option value="17">Other</option>

                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2 countycodediv">
                                                                <label>Country Code</label>
                                                                <select name="emergency_country[]" id="emerCountries" class="form-control emergencycountry">
                                                                    <option value="0">Select</option>
                                                                </select>
                                                            </div>

                                                            <div class="col-sm-3 col-md-3 ">

                                                                <label>Phone</label>
                                                                <input class="form-control phone_format capsOn" type="text" id="phoneNumber"
                                                                       name="emergency_phone[]" maxlength="12">
                                                            </div>

                                                            <div class="clearfix"></div>


                                                    </div>
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Email</label>
                                                                <input class="form-control add-input emailll" type="text" id="email1"
                                                                       name="emergency_email[]" maxlength="50">
                                                                <a class="add-icon add-emergency-contant" href="javascript:;">
                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                <a class="add-icon remove-emergency-contant" style="display:none;"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>

                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>

                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Notes </h3>
                                        </div>
                                        <div class="form-data">
                                            <form id="notes_form" class="reset-all">
                                  <div class="chargeNoteHtml notesDateTime ">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                                            <!--<div class="notes-timer-outer">
                                                            <textarea class="form-control add-input chargeNoteClone capital notesDateTime" name="chargeNote[]"></textarea>
                                                                <span>1/10/2020 (Fri.) 12:39:3</span>
                                                            </div>-->
                                                            <div class="notes_date_right_div">
                                                            <textarea class="form-control add-input chargeNoteClone capital notesDateTime notes_date_right" name="chargeNote[]"></textarea>
                                                            </div>
                                                                <a class="add-icon addNote" href="javascript:;">

                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            <a class="add-icon addNote" href="javascript:;" style="display:none;">

                                                                <i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                                            <a class="add-icon removeNote" href="javascript:;"
                                                               style="display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>File Library </h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                    <div class="col-sm-4">
                                                        <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                        <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">

                                                        <input type="button" class="orange-btn" id="remove_library_file" value="Remove All Files...">

                                                    </div>
                                                    <div class="row" id="file_library_uploads">
                                                    </div>
                                            </div>                                        </div>
                                    </div>

                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Custom Fields </h3>
                                        </div>
                                        <div class="form-data">

                                            <input type="hidden" name="id" id="record_id">
                                            <div class="col-sm-12" style="margin-left: 15px;">
                                                <div class="collaps-subhdr">
                                                </div>
                                                <div class="row">
                                                    <div class="custom_field_html">
                                                    </div>
                                                    <div class="col-sm-6 custom_field_msg">
                                                        No Custom Fields
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="btn-outer text-right">
                                                            <button type="button" id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">Add Custom Field</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <!--<div class="row">
                                                <div class="col-sm-6 custom_field_msg">No Custom Fields</div>
                                                <div class="col-sm-6 pull-right custom_field_html">
                                                    <div class=" text-right">
                                                        <button type="button" id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">Add Custom Field</button>
                                                    </div>
                                                </div>
                                            </div>-->
                                        </div>
                                    </div>
                                    <!-- Form-outer 6 Ends -->


                                </div>
                                <div class="btn-outer apex-btn-block left text-right">
                                    <input type="submit" class="blue-btn savelease" value="Save">
                                    <input type="button" id="" class="clear-btn clearFormReset" value="Clear">
                                    <input type="button" id="add_company_button_cancel" class="grey-btn" value="Cancel">

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
    </section>
    <!-- start custom field model -->
    <div class="container">
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Custom Field</h4>
                    </div>
                    <div class="modal-body" style="height: 380px;">
                        <div class="form-outer col-sm-12">
                            <form id="custom_field" class="reset-all">
                                <input type="hidden" id="customFieldModule" name="module" value="portfolio">
                                <input type="hidden" name="id" id="custom_field_id" value="">
                                <div class="row custom_field_form">
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Field Name <em class="red-star">*</em></label>
                                        </div>
                                        <div class="col-sm-9 field_name">
                                            <input class="form-control" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Data Type</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="form-control data_type" id="data_type" name="data_type">
                                                <option value="text">Text</option>
                                                <option value="number">Number</option>
                                                <option value="currency">Currency</option>
                                                <option value="percentage">Percentage</option>
                                                <option value="url">URL</option>
                                                <option value="date">Date</option>
                                                <option value="memo">Memo</option>
                                            </select>
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Default value</label>
                                        </div>
                                        <div class="col-sm-9 default_value">
                                            <input class="form-control default_value" id="default_value" type="text" name="default_value" placeholder="">
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Required Field</label>
                                        </div>
                                        <div class="col-sm-9 is_required">
                                            <select class="form-control" name="is_required" id="is_required">
                                                <option value="1">Yes</option>
                                                <option value="0" selected="selected">No</option>
                                            </select>
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-outer text-right">
                                    <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                    <button type="button" class="clear-btn" id="ClearForm" >Clear</button>
                                    <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End custom field model -->

</div>

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    $(function() {
        $('#datepicker').datepicker();
    });


    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });


    var currencySign = "<?php echo  $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ; ?>";

   /* $(document).on("click",".clear-btn",function () {
        resetFormClear('rent_details_form',['expected_moveIn','request_lease_term','period_lease'])
    });*/
</script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<!--<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>-->
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/leases/guestCard/guestCard.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/custom_fields.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
<script>

    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
</script>
<script>
    $('.calander').datepicker({
        yearRange: '1919:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('input[name="birth"]').datepicker({
        yearRange: '1900:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    var date = $.datepicker.formatDate(jsDateFomat, new Date());

    $(".calander").val(date);
</script>
<script>
    $(document).ready(function() {
        $(document).on('click','.clearFormReset',function(){


      window.location.reload();

          /* resetFormClear('#generallease',[],'form',true);
            $('#PropertyId').val('');
            $('#unitId').val('');
            $('#bedroomId').val('');
            $('#floorId').val('');
            $('#bathroomId').val('');
            $('#min_rent').val('');
            $('#max_rent').val('');

         //   resetFormClear('#generallease',['salutation','gender','zipcode','country','state','city','preferred','referral_source','phoneType','carrier','countryCode','martial_status','guestHobbies','veteran_status'],'form',false);
            resetFormClear('#emergency_details',['emergency_relation','emergency_country']);
            resetFormClear('#notes_form',[]);*/
        });
        $(document).on('click','.clearFormReset1',function() {
            resetFormClear('#additionalReferralResource1', [], 'div', false);
        });

        $(document).on('click','.clearFormReset2',function() {
            resetFormClear('#selectPropertyEthnicity1', [], 'div', false);
        });
         $(document).on('click','.clearFormReset3',function() {
            resetFormClear('#occupantPropertyMaritalStatus1', [], 'div', false);
         });
         $(document).on('click','.clearFormReset4',function() {
            resetFormClear('#selectPropertyHobbies1', [], 'div', false);
        });
         $(document).on('click','.clearFormReset5',function() {
            resetFormClear('#selectPropertyVeteranStatus1', [], 'div', false);
        });

        $('.capsOn12').keyup(function(event) {

            var textBox = event.target;
            var lengthChar = $(this).val();
            var start = textBox.selectionStart;
            var end = textBox.selectionEnd;
            if (lengthChar.length == "2"){
                textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1);
                textBox.setSelectionRange(start, end);
            }else if(lengthChar.length > "2"){
                var lastChar = lengthChar[lengthChar.length-1];
                var socdlastChar = lengthChar[lengthChar.length-2];
                var thirdlastChar = lengthChar[lengthChar.length-3];

                if (lastChar === "." || socdlastChar === "."){
                    var getText = event.target.value.replace(/([!?.]\s+)([a-z])/g, function(m, $1, $2) {
                        return $1+$2.toUpperCase();
                    });
                    textBox.value = getText;
                }
                if ( thirdlastChar === "."){
                    console.log(lengthChar.length );
                    var getText = event.target.value.replace(/([!?.]\s+)([a-z])/g, function(m, $1, $2) {
                        return $1+$2.toUpperCase();
                    });
                    textBox.value = getText;
                }
                /*textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1);
                textBox.setSelectionRange(start, end);*/
            }

        });
        /*$('.capsOn12').keyup(function() {
            $(this).val($(this).val().substr(0, 1).toUpperCase() + $(this).val().substr(1).toLowerCase());
        });*/
        /*$('.capsOn12').on('keydown', function(event) {
            var count = $(".capsOn12").val();
            var lastChar = count[count.length-1];


            if(count.length=="0") {
                if (event.keyCode >= 65 && event.keyCode <= 90 &&  event.keyCode <= 190 && !(event.shiftKey) && !(event.ctrlKey) && !(event.metaKey) && !(event.altKey)) {
                    var $t = $(this);
                    event.preventDefault();
                    var char = String.fromCharCode(event.keyCode);
                    $t.val(char + $t.val().slice(this.selectionEnd));
                    this.setSelectionRange(1,1);
                }
            }
        });*/


        $(document).on('click','#custom_field #ClearForm',function () {
            $('#custom_field')[0].reset();
        });
    });


</script>
<style>
    .row.custom_field_class input {
        width: 258px;
    }

    /*.capsOn12:first-letter{ text-transform: capitalize !important; }*/

</style>


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
