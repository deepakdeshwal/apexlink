<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 bread-search-outer">
                    <div class="easy-search">
                        <input placeholder="Easy Search" type="text"/>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="content-section">
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"><a href="/GuestCard/ListGuestCard" >Guest Cards</a></li>
                                <li role="presentation" class="active"><a href="/RentalApplication/RentalApplications">Rental Applications</a></li>
                                <li role="presentation"><a href="/Lease/ViewEditLease" >Leases</a></li>
                                <li role="presentation"><a href="/Lease/Movein" >Move-In</a></li>
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="guest-cards">
                                    <div class="form-outer">
                                        <input type="hidden" class="hidden_name_val">
                                        <div class="form-hdr">
                                            <h3>Rent Unit Details <a class="back" href="/RentalApplication/RentalApplications"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                        </div>
                                        <form id="rent_details_form" class="reset-all">
                                            <div class="form-data">
                                                <div class="row">

                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Expected Move-In Date<em class="red-star">*</em></label>
                                                        <input class="form-control calander move_in_date" name="expected_moveIn"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Request Lease Term </label>
                                                        <input class="form-control capsOn req_lease_term" type="text" name="request_lease_term" id="request_lease_term" value="12"/>
                                                        <span id="first_nameErr" class="val_message_first red-star" style="display: none">*This field is required</span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label></label>
                                                        <select class="form-control capsOn req_lease_period" type="text" name="period_lease"><option value="month">Months</option><option value="year">Year</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3 clear">
                                                        <label>Expected Move-out Date</label>
                                                        <input class="form-control capsOn calander12 move_out_day" type="text" name="expected_moveOut"  readonly/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3 clear">
                                                        <label>Select Property<em class="red-star">*</em></label>
                                                        <select class="select-dd form-control prop_short" name="prop_short"></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Select Building<em class="red-star">*</em></label>
                                                        <select class="select-dd form-control build_short" name="build_short"></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Select Unit<em class="red-star">*</em></label>
                                                        <select class="select-dd form-control unit_short" name="unit_short"></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3 clear">
                                                        <label>Address 1</label>
                                                        <input class="form-control capsOn address1" name="address1" readonly/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Address 2</label>
                                                        <input class="form-control capsOn address2" name="address2" readonly/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Address 3</label>
                                                        <input class="form-control capsOn address3" name="address3" readonly/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Address 4</label>
                                                        <input class="form-control capsOn address4" name="address4" readonly/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Zip/Postal Code</label>
                                                        <input class="form-control capsOn zip__code" name="postalCode" readonly/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>City</label>
                                                        <input class="form-control capsOn cityRental" name="city" readonly/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>State/Province</label>
                                                        <input class="form-control capsOn stateRental" name="state" readonly/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3 clear">
                                                        <label>Market Rent<?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                                        <input class="form-control capsOn marketRent" type="number" name="marketRent"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Base Rent<?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                                        <input class="form-control capsOn baseRent" type="number" name="baseRent"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Security Deposit<?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                                        <input class="form-control capsOn secdeposite" name="secdeposite" type="number"/>
                                                    </div>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>General Information</h3>
                                </div>
                                <form id="save_rental_general" class="reset-all">
                                    <div class="form-data">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Salutation</label>
                                                <select class="form-control" name="salutation" id="salutation">
                                                    <option value="">Select</option>
                                                    <option value="Dr.">Dr.</option>
                                                    <option value="Mr.">Mr.</option>
                                                    <option value="Mrs.">Mrs.</option>
                                                    <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                                    <option value="Ms.">Ms.</option>
                                                    <option value="Sir">Sir</option>
                                                    <option value="Madam">Madam</option>
                                                    <option value="Brother">Brother</option>
                                                    <option value="Sister">Sister</option>
                                                    <option value="Father">Father</option>
                                                    <option value="Mother">Mother</option>
                                                </select>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>First Name <em class="red-star">*</em></label>
                                                <input class="form-control capsOn" type="text" name="first_name" id="first_name"/>
                                                <span id="first_nameErr" class="val_message_first red-star" style="display: none">*This field is required</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Middle Name</label>
                                                <input class="form-control capsOn" type="text" name="middle_name" id="middle_name"/>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Last Name <em class="red-star">*</em></label>
                                                <input class="form-control capsOn" type="text" name="last_name" id="last_name_user" />
                                                <span id="last_nameErr" class="val_message_last red-star" style="display: none">*This field is required</span>

                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3 maiden_name_hide" style="display: none;">
                                                <label>Maiden Name</label>
                                                <input class="form-control capsOn" type="text" name="maiden_name" id="maiden_name" />
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Nick Name</label>
                                                <input class="form-control capsOn" type="text" name="nick_name" id="nick_name" />
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3 clear">
                                                <label>Gender</label>
                                                <select class="form-control" name="gender" id="general"><option value="0">Select</option>
                                                    <option value="1">Male</option>
                                                    <option value="2">Female</option>
                                                    <option value="3">Prefer Not to Say</option>
                                                    <option value="4">Other</option>
                                                </select>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Driver's Licence/State</label>
                                                <input class="form-control capsOn stateLicence" name="stateLicence"/>
                                            </div>
                                            <div class="col-sm-3 col-md-3">
                                                <label>SSN/SIN/ID</label>
                                                <div class='multipleSsn clone-input' id="multipleSsn">
                                                    <input class="form-control add-input capsOn" type="number"
                                                           id="ssn" name="ssn_sin_id[]">
                                                    <a class="add-icon ssn-remove-sign" href="javascript:;"
                                                       style="display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>
                                                    <a class="add-icon ssn-plus-sign" href="javascript:;"><i
                                                            class="fa fa-plus-circle"
                                                            aria-hidden="true"></i></a>
                                                </div>

                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Birth Date</label>
                                                <input class="form-control capsOn calander birthdate" name="birthdate"/>
                                            </div>



                                            <div class="col-xs-12 col-sm-4 col-md-3 clear">
                                                <label> Referral Source   <a class="pop-add-icon propreerralicon" href="javascript:;">
                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                    </a></label>
                                                <select class="form-control" id="referralsource" name="referralSource"></select>
                                                <div class="add-popup" id="selectPropertyReferralResource1" style="width: 90%;">
                                                    <h4>Add New Referral Source</h4>
                                                    <div class="add-popup-body">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>New Referral Source <em class="red-star">*</em></label>
                                                                <input name="referral" class="form-control customValidateGroup reff_source capital" type="text" data_required="true" data_max="150" placeholder="New Referral Source">
                                                                <span class="customError required" aria-required="true" id="reff_source"></span>
                                                            </div>
                                                            <div class="btn-outer text-right">
                                                                <button type="button" class="blue-btn add_single1" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source" data-name="referralSource">Save</button>
                                                                <input type="button" class="clear-btn clearSource" value="Clear">
                                                                <input type="button" class="grey-btn" value="Cancel">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="short_term_rental" class="primary-tenant-phone-row-short">
                                                <div class="col-xs-12 col-sm-3 col-md-3 clear">

                                                    <label>Phone Type</label>
                                                    <select class="form-control phone_shortTerm phone_type_rental" id="phone_type_rental" name="phone_type"></select>
                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-3">
                                                    <label>Phone Number<em class="red-star">*</em> </label>
                                                    <input class="form-control add-input phone_format phoneeee" name="phone_number" type="text">
                                                </div>
                                                <div class="col-sm-12 col-md-1 ext_phone" style="display: none;">
                                                    <label>Extension</label>
                                                    <input name="Extension[]" class="form-control Extension_phone" type="text" placeholder="Eg: + 161">
                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-3">
                                                    <label>Carrier <em class="red-star">*</em></label>
                                                    <select class="form-control carrier_shortTerm guestCarrier" id="guestCarrier" name="carrier"><option>Select</option></select>
                                                </div>


                                                <div class="col-xs-12 col-sm-3 col-md-3">
                                                    <label>Country Code</label>
                                                    <select class="form-control add-input country_shortTerm guestCountries" id="guestCountries" name="country"><option>United States (+1)</option></select>
                                                    <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                    <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle" aria-hidden="true"></i></a>

                                                </div>

                                            </div>


                                            <div class="multipleEmailShortTerm">
                                                <div class="col-xs-12 col-sm-3 col-md-3 clear">
                                                    <label>Email </label>
                                                    <input class="form-control add-input email_user"  type="text" name="email[]">
                                                    <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle email-plus-sign-short-term" aria-hidden="true"></i></a>
                                                    <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle fa-minus-circle-short-term" aria-hidden="true"></i></a>
                                                    <span class="err-class"></span>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3 clear">
                                                <label>Ethnicity  <a class="pop-add-icon selectPropertyEthnicity" href="javascript:;">
                                                        <i class="fa fa-plus-circle"aria-hidden="true"></i>
                                                    </a></label>
                                                <select class="form-control" id="guestEthnicity" name="ethnicity"></select>
                                                <div class="add-popup" id="selectPropertyEthnicity1" style="width: 90%;">
                                                    <h4>Add New Ethnicity</h4>
                                                    <div class="add-popup-body">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>New Ethnicity <em class="red-star">*</em></label>
                                                                <input class="form-control ethnicity_src customValidateGroup capsOn" type="text" placeholder="Add New Ethnicity" data_required="true" >
                                                            </div>
                                                            <div class="btn-outer text-right">
                                                                <button type="button" class="blue-btn add_single1"  data-table="tenant_ethnicity" data-cell="title" data-class="ethnicity_src" data-name="ethnicity">Save</button>
                                                                <input type="button" class="clear-btn clearEthnicity" value="Clear">
                                                                <input type="button" class="grey-btn" value="Cancel">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--<div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Martial Status <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                <select class="form-control" id="guestMarital" name="martial_status"></select>
                                            </div>-->
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Martial Status <a class="pop-add-icon occupantPropertyMaritalStatus1" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                <select class="form-control"
                                                        name="martial_status"
                                                        id="guestMarital">
                                                    <option value="0">select</option>
                                                    <option value="1">single</option>
                                                    <option value="2">married</option>
                                                </select>
                                                <div class="add-popup" id="occupantPropertyMaritalStatus1" style="width: 90%;">
                                                    <h4>Add New Marital Status</h4>
                                                    <div class="add-popup-body">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>New Marital Status <em class="red-star">*</em></label>
                                                                <input class="form-control maritalstatus_src capsOn" type="text" placeholder="Add New Marital Status">
                                                                <span class="customError required red-star" aria-required="true" id="maritalstatus_src"></span>
                                                            </div>
                                                            <div class="btn-outer text-right">
                                                                <button type="button" class="blue-btn add_single1" data-table="tenant_marital_status" data-cell="marital" data-class="maritalstatus_src" data-name="martial_status">Save</button>
                                                                <input type="button" class="clear-btn clearMartial" value="Clear">
                                                                <input type="button" class="grey-btn" value="Cancel">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Hobbies <a class="pop-add-icon selectPropertyHobbies" href="javascript:;">
                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                    </a></label>
                                                <select class="form-control guestHobbies" name="guestHobbies[]" id="guestHobbies" multiple>
                                                    <option></option>
                                                </select>
                                                <div class="add-popup" id="selectPropertyHobbies1" style="width: 90%;">
                                                    <h4>Add New Hobbies</h4>
                                                    <div class="add-popup-body">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>New Hobbies <em class="red-star">*</em></label>
                                                                <input class="form-control hobbies_src capsOn" type="text" placeholder="Add New Hobbies" data_required="true">
                                                                <span class="red-star" id="hobbies_src"></span>
                                                            </div>
                                                            <div class="btn-outer text-right">
                                                                <button type="button" class="blue-btn add_single1" data-table="hobbies" data-cell="hobby" data-class="hobbies_src" data-name="guestHobbies[]">Save</button>
                                                                <input type="button" class="clear-btn ClearHobbies" value="Clear">
                                                                <input type="button" class="grey-btn" value="Cancel">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Veteran Status  <a class="pop-add-icon selectPropertyVeteranStatus" href="javascript:;">
                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                    </a></label>
                                                <select class="form-control" id="guestVeteran" name="veteran_status"></select>
                                                <div class="add-popup" id="selectPropertyVeteranStatus1">
                                                    <h4>Add New VeteranStatus</h4>
                                                    <div class="add-popup-body">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>New VeteranStatus <em class="red-star">*</em></label>
                                                                <input class="form-control veteran_src capsOn" type="text" placeholder="Add New VeteranStatus">
                                                                <span class="red-star" id="veteran_src"></span>
                                                            </div>
                                                            <div class="btn-outer text-right">
                                                                <button type="button" class="blue-btn add_single1" data-table="tenant_veteran_status" data-cell="veteran" data-class="veteran_src" data-name="veteran_status">Save</button>
                                                                <input type="button" class="clear-btn ClearVeteran" value="Clear">
                                                                <input type="button" class="grey-btn" value="Cancel">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3" style="padding:27px;">
                                                <label>Other Occupants</label>

                                            </div>
                                            <div id="otheroccupant" class="otheroccupants">
                                                <div class="col-xs-12 col-sm-3 col-md-2 clear" style="margin-left: 13px;">
                                                    <label>Name</label>
                                                    <input class="form-control other_Name capital" name="other_Name"/>

                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-2" style="margin-left: 13px;">
                                                    <label>Email</label>
                                                    <input class="form-control other_email" name="other_email"/>

                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-2">
                                                    <label>SSN/SIN/ID</label>
                                                    <input class="form-control other_ssn" name="other_ssn" type="number"/>
                                                </div>
                                                <div class="col-sm-3 col-md-2">
                                                    <label>Relation</label>
                                                    <div class=' clone-input' id="">
                                                        <select class="form-control add-input capsOn" type="text"
                                                        id="other_relation" name="other_relation[]">
                                                            <option value="Select">Select</option>
                                                            <option value="Daughter">Daughter</option><option value="Father">Father</option><option value="Friend">Friend</option><option value="Mother">Mother</option><option value="Owner">Owner</option><option value="Partner">Partner</option><option value="Son">Son</option><option value="Spouse">Spouse</option><option value="Other">Other</option></select>
                                                        <a class="add-icon " href="javascript:;"
                                                        ><i class="fa fa-minus-circle" aria-hidden="true" ></i></a>
                                                        <a class="add-icon" href="javascript:;"><i
                                                                class="fa fa-plus-circle"
                                                                aria-hidden="true"></i></a>
                                                    </div>

                                                </div>
                                            </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="form-outer">
                        <div class="form-hdr">
                            <h3> Rental History</h3>
                        </div>
                        <form id="rental_history_form">
                            <div class="form-data">

                                <div class="row">
                                    <div class="sub-heading-hdr">Current Rental History</div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> Current Address</label>
                                        <input class="form-control  current_address capital" type="text" name="current_address">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> Zip / Postal Code</label>
                                        <input class="form-control  current_zip" type="number" name="current_zip">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>City</label>
                                        <input class="form-control  current_city capital" type="text" name="current_city">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> State / Province</label>
                                        <input class="form-control  current_state capital" type="text" name="current_state">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Landlord / Manager</label>
                                        <input class="form-control capsOn current_landlord capital" name="current_landlord"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> Landlord / Manager Phone No.</label>
                                        <input class="form-control capsOn current_landlord_no" name="current_landlord_no"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Monthly Salary <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                        <input class="form-control capsOn current_salary" type="number" name="current_salary"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 clear">
                                        <label>Resided from</label>
                                        <input class="form-control capsOn calander current_residence_from" name="current_residence_from"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Resided to</label>
                                        <input class="form-control capsOn calander current_residence_to" name="current_residence_to"/>
                                    </div>



                                    <div class="col-xs-12 col-sm-4 col-md-3 ">
                                        <label>Reason for Leaving   <a class="pop-add-icon collectionreason" href="javascript:;">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                            </a></label>
                                        <select class="form-control" id="current_reason" name="current_reason"></select>
                                        <div class="add-popup" id="collectionreason1" style="width: 90%;">
                                            <h4>Add New Reason</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>Add New Reason <em class="red-star">*</em></label>
                                                        <input class="form-control reason_source capsOn" type="text" placeholder="Add New Reason">
                                                        <span class="red-star" id="reason_source"></span>
                                                    </div>
                                                    <div class="btn-outer text-right">
                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_collection_reason" data-cell="reason" data-class="reason_source" data-name="current_reason">Save</button>
                                                        <input type="button" class="clear-btn ClearReason" value="Clear">
                                                        <input type="button" class="grey-btn" value="Cancel">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="div-full sub-heading">
                                        <div class="check-outer ">
                                            <input type="checkbox" value="1" name="rentalhistory" id="currentrental123" class="currentrental123" name="rentalhistory">
                                            <label>Previous Rental History</label>

                                        </div>


                                    </div>


                                </div>


                                <div class="row" id="previousrental" style="display: none">
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>  Previous Address<em class="red-star">*</em></label>
                                        <input class="form-control  previous_address capital" type="text" name="previous_address">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> Zip / Postal Code<em class="red-star">*</em></label>
                                        <input class="form-control  previous_zip" type="text" name="previous_zip">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>City</label>
                                        <input class="form-control  previous_city capital" type="text" name="previous_city">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> State / Province</label>
                                        <input class="form-control  previous_state capital" type="text" name="previous_state">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Landlord / Manager</label>
                                        <input class="form-control capsOn previous_landlord capital" name="previous_landlord"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> Landlord / Manager Phone No.</label>
                                        <input class="form-control capsOn previous_landlord_no" name="previous_landlord_no"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Monthly Salary <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                        <input class="form-control capsOn previous_salary" name="previous_salary"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 clear">
                                        <label>Resided from</label>
                                        <input class="form-control capsOn calander previous_residing_from" name="previous_residing_from"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Resided to</label>
                                        <input class="form-control capsOn calander currentrental previous_resided_to" name="previous_resided_to"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 ">
                                        <label>Reason for Leaving   <a class="pop-add-icon collectionreason" href="javascript:;">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                            </a></label>
                                        <select class="form-control" id="previous_reason" name="previous_reason"></select>
                                        <div class="add-popup" id="collectionreason1">
                                            <h4>Add New Reason</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>Add New Reason <em class="red-star">*</em></label>
                                                        <input class="form-control reason_source capsOn" type="text" placeholder="Add New Reason">
                                                        <span class="red-star" id="reason_source"></span>
                                                    </div>
                                                    <div class="btn-outer">
                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_collection_reason" data-cell="reason" data-class="reason_source" data-name="previous_reason">Save</button>
                                                        <input type="button" class="grey-btn" value="Cancel">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </form>
                    </div>





                    <div class="form-outer">
                        <div class="form-hdr">
                            <h3> Employment History</h3>
                        </div>
                        <form id="employment_history_form"> <div class="form-data">
                                <div class="row">
                                    <div class="sub-heading-hdr">Current Employer History</div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> Current Employer</label>
                                        <input class="form-control  current_employer capital" type="text" name="current_employer">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> Employer Address</label>
                                        <input class="form-control current_employee_address capital" type="text" name="current_employee_address">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Zip / Postal Code</label>
                                        <input class="form-control current_employer_zip" type="number" name="current_employer_zip">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> City</label>
                                        <input class="form-control current_employee_city capital" type="text" name="current_employee_city">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>  State / Province</label>
                                        <input class="form-control capsOn current_employee_state capital" name="current_employee_state"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> Position Held</label>
                                        <input class="form-control capsOn current_employee_position capital" name="current_employee_position"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Monthly Salary <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                        <input class="form-control capsOn current_employee_salary" name="current_employee_salary"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 ">
                                        <label>Employed From Date</label>
                                        <input class="form-control capsOn calander current_employee_from_date" name="current_employee_from_date"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Employed To Date</label>
                                        <input class="form-control capsOn calander current_employee_to_date" name="current_employee_to_date"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> Employer Phone Number</label>
                                        <input class="form-control capsOn phone_format current_employee_no" name="current_employee_no"/>

                                        </select>
                                    </div>

                                    <div class="div-full">
                                        <div class="check-outer sub-heading">
                                            <input type="checkbox" value="1" id="emphistory" name="employmenthistory">
                                            <label>Previous Employer History</label>
                                        </div>

                                    </div>


                                </div>


                                <div class="row" id="previousemployer" style="display: none">
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> Previous Employer<em class="red-star">*</em></label>
                                        <input class="form-control  previous_employer capital" type="text" name="previous_employer">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> Employer Address<em class="red-star">*</em></label>
                                        <input class="form-control  previous_employee_address capital" type="text" name="previous_employee_address">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Zip / Postal Code</label>
                                        <input class="form-control  previous_employer_zip" type="text" name="previous_employer_zip">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> City</label>
                                        <input class="form-control  previous_employee_city capital" type="text" name="previous_employee_city">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>  State / Province</label>
                                        <input class="form-control capsOn previous_employee_state capital" name="previous_employee_state"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> Position Held</label>
                                        <input class="form-control capsOn previous_employee_position capital" name="previous_employee_position"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Monthly Salary <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                        <input class="form-control capsOn previous_employee_salary" name="previous_employee_salary"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 ">
                                        <label>Employed From Date</label>
                                        <input class="form-control capsOn calander previous_employee_from_date" name="previous_employee_from_date"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Employed To Date</label>
                                        <input class="form-control capsOn calander previous_employee_to_date" name="previous_employee_to_date"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label> Employer Phone Number</label>
                                        <input class="form-control capsOn phone_format previous_employee_no" name="previous_employee_no"/>

                                        </select>
                                    </div>


                                </div>

                        </form>
                    </div>
                </div>





                <div class="form-outer">
                    <div class="form-hdr">
                        <h3>Additional Income (If Any) </h3>
                    </div>
                    <div class="form-data">
                        <form id="emergency_details" class="reset-all">

                            <div class="row additionalincome" id="">

                                <div class="col-sm-3 col-md-3">
                                    <label> Amount <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                    <input class="form-control capsOn capital capsOn" type="number" id="additional_amount"
                                           name="additional_amount" maxlength="50">
                                </div>

                                <div class="col-sm-3 col-md-3">
                                    <label>Source of income</label>
                                    <div class='relation clone-input' id="additional_income">
                                        <input class="form-control add-input capsOn additional_income" type="text"
                                                name="additional_income[]"/>
                                        <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                        <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                    </div>

                                </div>



                            </div>

                        </form>
                    </div>
                </div>






                <!--   <div class="form-outer">
                        <div class="form-hdr">
                            <h3>Emergency Contact Details </h3>
                        </div>

                    </div>
-->

                <div class="form-outer">
                    <div class="form-hdr">
                        <h3>Emergency Contact Details </h3>
                    </div>
                    <div class="form-data">
                        <form id="rental_emergency_details" class="reset-all">

                            <div class="lease-emergency-contact">
                                <div class="row">

                                    <div class="col-sm-3 col-md-3">
                                        <label>Emergency Contact Name</label>
                                        <input class="form-control capsOn capital capsOn emergency_contact_name" type="text" id="emergency"
                                               name="emergency_contact_name[]" maxlength="50">
                                    </div>

                                    <div class="col-sm-3 col-md-3 ">
                                        <label>Relation</label>
                                        <select class="form-control emergency_relation" id="relationship"
                                                name="emergency_relation[]">
                                            <option value="">Select</option>
                                            <option value="1">Brother</option>
                                            <option value="2">Daughter</option>
                                            <option value="3">Employer</option>
                                            <option value="4">Father</option>
                                            <option value="5">Friend</option>
                                            <option value="6">Mentor</option>
                                            <option value="7">Mother</option>
                                            <option value="8">Neighbor</option>
                                            <option value="9">Nephew</option>
                                            <option value="10">Niece</option>
                                            <option value="11">Owner</option>
                                            <option value="12">Partner</option>
                                            <option value="13">Sister</option>
                                            <option value="14">Son</option>
                                            <option value="15">Spouse</option>
                                            <option value="16">Teacher</option>
                                            <option value="17">Other</option>

                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-2 countycodediv">
                                        <label>Country Code</label>
                                        <select name="emergency_country[]" id="emerCountries" class="form-control emergencycountry">
                                            <option value="0">Select</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-3 col-md-3 ">

                                        <label>Phone</label>
                                        <input class="form-control capsOn phone_format emergency_phone" type="text" id="phoneNumber"
                                               name="emergency_phone[]" maxlength="12">
                                    </div>

                                    <div class="clearfix"></div>


                                </div>
                                <div class="row">
                                    <div class="form-outer">
                                        <div class="col-sm-3 col-md-3">
                                            <label>Email</label>
                                            <input class="form-control add-input emailll" type="text" id="email1"
                                                   name="emergency_email[]" maxlength="50">
                                            <a class="add-icon add-emergency-contant" href="javascript:;">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            <a class="add-icon remove-emergency-contant" style="display:none;"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>

                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="form-outer">
                    <div class="form-hdr">
                        <h3>Co-Signer | Guarantor (If Any)
                        </h3>
                    </div>
                    <form id="guarranter_form">
                        <div class="form-data">


                            <div class="lease-emergency-div">
                                <div class="grey-box-add">
                                    <div class="grey-box-add-inner row">
                                        <div class="grey-box-add-lt">
                                            <div class="row">


                                                <div class="col-sm-3 col-md-3">
                                                    <label> Salutation</label>
                                                    <select class="form-control co_salutaion"
                                                            name="salutaion[]">
                                                        <option value="">Select</option>
                                                        <option value="Dr.">Dr.</option>
                                                        <option value="Mr.">Mr.</option>
                                                        <option value="Mrs.">Mrs.</option>
                                                        <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                                        <option value="Ms.">Ms.</option>
                                                        <option value="Sir">Sir</option>
                                                        <option value="Madam">Madam</option>
                                                        <option value="Brother">Brother</option>
                                                        <option value="Sister">Sister</option>
                                                        <option value="Father">Father</option>
                                                        <option value="Mother">Mother</option>
                                                    </select>
                                                </div>

                                                <div class="col-sm-3 col-md-3 ">
                                                    <label> First Name</label>
                                                    <input class="form-control capsOn first_name_co" type="text"
                                                           name="first_name" maxlength="12">
                                                </div>
                                                <div class="col-sm-3 col-md-3  countycodediv">
                                                    <label> Middle Name</label>
                                                    <input class="form-control capsOn co_middle_name" type="text"
                                                           name="middle_name" maxlength="12">
                                                </div>

                                                <div class="col-sm-3 col-md-3 ">

                                                    <label> Last Name</label>
                                                    <input class="form-control capsOn co_last_name" type="text"
                                                           name="last_name" maxlength="12">
                                                </div>
                                            </div>


                                            <div class="row">

                                                <div class="col-sm-3 col-md-3">
                                                    <label>Gender</label>
                                                    <select class="form-control co_gender"
                                                            name="gender">
                                                        <option value="0">Select</option>
                                                        <option value="1">Male</option>
                                                        <option value="2">Female</option>
                                                        <option value="3">Prefer Not to Say</option>
                                                        <option value="4">Other</option>

                                                    </select>
                                                </div>

                                                <div class="col-sm-3 col-md-3 ">
                                                    <label>Relation</label>
                                                    <select class="form-control co_relationship"
                                                            name="relationship">
                                                        <option value="">Select</option>
                                                        <option value="1">Brother</option>
                                                        <option value="2">Daughter</option>
                                                        <option value="3">Employer</option>
                                                        <option value="4">Father</option>
                                                        <option value="5">Friend</option>
                                                        <option value="6">Mentor</option>
                                                        <option value="7">Mother</option>
                                                        <option value="8">Neighbor</option>
                                                        <option value="9">Nephew</option>
                                                        <option value="10">Niece</option>
                                                        <option value="11">Owner</option>
                                                        <option value="12">Partner</option>
                                                        <option value="13">Sister</option>
                                                        <option value="14">Son</option>
                                                        <option value="15">Spouse</option>
                                                        <option value="16">Teacher</option>
                                                        <option value="17">Other</option>

                                                    </select>
                                                </div>
                                                <div class="col-sm-3 col-md-3  countycodediv">
                                                    <label>Address 1</label>
                                                    <input class="form-control capsOn co_address1" type="text"
                                                           name="address1" maxlength="12">
                                                </div>

                                                <div class="col-sm-3 col-md-3 ">

                                                    <label>Address 2
                                                    </label>
                                                    <input class="form-control capsOn co_address2" type="text"
                                                           name="address2" maxlength="12">
                                                </div>

                                            </div>



                                            <div class="row">

                                                <div class="col-sm-3 col-md-3">
                                                    <label>Address 3</label>
                                                    <input class="form-control capsOn capital capsOn co_address3" type="text"
                                                           name="address3" maxlength="50">
                                                </div>

                                                <div class="col-sm-3 col-md-3 ">
                                                    <label>Address 4</label>
                                                    <input class="form-control capsOn co_address4" type="text"
                                                           name="address4" maxlength="12">
                                                </div>
                                                <div class="col-sm-3 col-md-3">
                                                    <label>Zip / Postal Code</label>
                                                    <input class="form-control capsOn co_zipcode" type="number"
                                                           name="zipcode" maxlength="12">
                                                </div>

                                                <div class="col-sm-3 col-md-3 ">

                                                    <label> City</label>
                                                    <input class="form-control capsOn co_emer_city" type="text"
                                                           name="emer_city" maxlength="12">
                                                </div>
                                                <div class="col-sm-3 col-md-3 clear">

                                                    <label>State / Province</label>
                                                    <input class="form-control capsOn co_state_province" type="text"
                                                           name="state_province" maxlength="12">
                                                </div>
                                            </div>




                                            <div id="short_term_rental" class="primary-tenant-phone-row-emergency">
                                                <div class="row col-xs-12 col-sm-3 col-md-3 clear">

                                                    <label>Phone Type</label>
                                                    <select class="form-control guar_phone_type" id="guar_phone_type" name="phone_type"><option>Mobile</option></select>
                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-3">
                                                    <label>Carrier </label>
                                                    <select class="form-control guar_carrier" id="guar_carrier" name="carrier"><option>Select</option></select>
                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-3">
                                                    <label>Country Code</label>
                                                    <select class="form-control guar_coun_code" id="guar_coun_code" name="country"><option>United States (+1)</option></select>
                                                </div>
                                                <div class="col-xs-12 col-sm-3 col-md-3">
                                                    <label>Phone Number </label>
                                                    <input class="form-control add-input phone_format guar_phone_number" name="phone_number" type="text">
                                                    <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                    <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="multipleEmailadditional">
                                                    <div class="col-xs-12 col-sm-3 col-md-3 clear">
                                                        <label>Email </label>
                                                        <input class="form-control add-input email_co" type="text" name="email_0[]">
                                                        <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle email-plus-sign-emergency" aria-hidden="true"></i></a>
                                                        <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle fa-minus-circle-emergency" aria-hidden="true"></i></a>
                                                        <span class="err-class"></span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="grey-box-add-rt">
                                            <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle emergency_div_plus" aria-hidden="true"></i></a>
                                            <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle emergency_div_minus" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>




                            </div>
                        </div>
                    </form>
                </div>
                <div class="flag-container">
                    <div class="form-outer">
                        <div class="form-hdr"><h3>Flag</h3></div>
                        <div class="form-data">
                            <div class="property-status form-outer2">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-outer text-right">
                                            <a class="blue-btn addbtnflag">New Flag
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- New Flag Form -->
                            <div class="row">
                                <form name="flagform" id="flagform" method="post"
                                      style="display: none;">
                                    <input name="flag_tenant_id" type="hidden"
                                           class="hiddenflag"
                                           value="<?php echo $_GET['rental_id']; ?>">
                                    <input name="record_id" type="hidden" value="">
                                    <div class="col-sm-2">
                                        <label>Flagged By</label>
                                        <input type="text" name="flagged_by_name"
                                               class="capsOn form-control" value="<?php print_r($_SESSION[SESSION_DOMAIN]['name']); ?>">
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Date</label>
                                        <input type="text" name="date" class="form-control calander">
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Flag Name</label>
                                        <input name="flag_name" id="" class="form-control" placeholder="Please Enter the Name of this Flag"
                                               type="text">
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Country Code</label>
                                        <select name="country_code"
                                                class="form-control guestCountries1" id="guestCountries1"></select>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Phone Number</label>
                                        <input name="phone_number"
                                               class="phone_format form-control" id="phone_number_flag" type="text">
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Flag Reason</label>
                                        <input name="flag_reason" id="flag_reason" class="form-control"
                                               type="text">
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <label>Flagged For</label>
                                        <input class="form-control capital flag_name342" name="flagged_for" id="flag_name1" disabled id="flagged_for" maxlength="100"  type="text"/>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Completed</label>
                                        <select name="status" class="form-control">
                                            <option value="1">Yes</option>
                                            <option value="0" selected>No</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Note</label>
                                        <textarea name="note" class="form-control capital"></textarea>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="btn-outer">
                                            <a class="blue-btn saveflag"
                                               href="javascript:void(0)">Save</a>
                                            <a class="grey-btn"
                                               href="javascript:void(0)">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- New Flag Form -->

                            <div class="accordion-grid">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">


                                            <div id="collapseOne"
                                                 class="panel-collapse collapse in"
                                                 aria-expanded="true" style="">
                                                <div class="panel-body pad-none">
                                                    <div class="grid-outer">
                                                        <div class="table-responsive apx-table">
                                                            <table class="table table-hover table-dark"
                                                                   id="flaglistingtable"></table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-outer">
                    <div class="form-hdr">
                        <h3>Notes </h3>
                    </div>
                    <div class="form-data">
                        <form id="notes_form" class="reset-all">

                            <div class="chargeNoteHtml">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <div class="notes_date_right_div ">
                                        <textarea class="form-control add-input chargeNoteClone chargeNote_co capital notes_date_right" name="chargeNote[]"></textarea>
                                        </div>
                                        <a class="add-icon addNote" href="javascript:;">

                                            <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                        <a class="add-icon addNote" href="javascript:;" style="display:none;">

                                            <i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                        <a class="add-icon removeNote" href="javascript:;"
                                           style="display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class=" files_main">
                    <div class="form-outer">
                        <div class="form-hdr">
                            <h3>
                                File Library
                            </h3>
                        </div>
                        <div class="form-data">
                            <div class="apx-table">
                                <form id="addChargeNote">

                                    <div id="collapseSeventeen" class="panel-collapse in">
                                        <div class="row">
                                            <div class="form-outer">
                                                <div class="col-sm-4 min-height-0 file_main">
                                                    <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                    <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">

                                                    <input type="button" class="orange-btn" id="remove_library_file" value="Remove All Files...">
                                                    <input type="button" class="saveChargeFile blue-btn" value="Save">

                                                </div>
                                                <div class="row" id="file_library_uploads">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid-outer">
                                            <div class="apx-table">
                                                <div class="table-responsive">
                                                    <table id="TenantFiles-table" class="table table-bordered">
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <!-- Form Outer Ends -->
                                </form>
                                <div class="table-responsive">

                                    <table id="TenantFiles-table" class="table table-bordered">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-outer">
                    <div class="form-hdr">
                        <h3>Custom Fields </h3>
                    </div>
                    <div class="form-data">

                        <input type="hidden" name="id" id="record_id">
                        <div class="col-sm-12" style="margin-left: 15px;">
                            <div class="collaps-subhdr">
                            </div>
                            <div class="row">
                                <div class="custom_field_html">
                                </div>
                                <div class="col-sm-6 custom_field_msg">
                                    No Custom Fields
                                </div>
                                <div class="col-sm-6">
                                    <div class="btn-outer text-right">
                                        <button type="button" id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">Add Custom Field</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!--<div class="row">
                            <div class="col-sm-6 custom_field_msg">No Custom Fields</div>
                            <div class="col-sm-6 pull-right custom_field_html">
                                <div class=" text-right">
                                    <button type="button" id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">Add Custom Field</button>
                                </div>
                            </div>
                        </div>-->
                    </div>
                </div>
                <!-- Form-outer 6 Ends -->


            </div>
            <div class="btn-outer apex-btn-block left text-right">
                <a class="blue-btn updateRenatlApplications">Update</a>
                <button type="button" class="resetguestedit clear-btn">Reset</button>
                <a id="add_rental_button_cancel" class="grey-btn" style="cursor: pointer">Cancel</a>
            </div>
        </div>
</div>

</div>
</div>
</div>
</section>
<!-- start custom field model -->
<div class="container">
    <div class="modal fade" id="myModal" role="dialog">
        <input type="hidden" value="<?php echo $_GET['rental_id']; ?>" class="rental_id" name="rental_id">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body" style="height: 380px;">
                    <div class="form-outer col-sm-12">
                        <form id="custom_field" class="reset-all">
                            <input type="hidden" id="customFieldModule" name="module" value="portfolio">
                            <input type="hidden" name="id" id="custom_field_id" value="">
                            <div class="row custom_field_form">
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Field Name <em class="red-star">*</em></label>
                                    </div>
                                    <div class="col-sm-9 field_name">
                                        <input class="form-control" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Data Type</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control data_type" id="data_type" name="data_type">
                                            <option value="text">Text</option>
                                            <option value="number">Number</option>
                                            <option value="currency">Currency</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="url">URL</option>
                                            <option value="date">Date</option>
                                            <option value="memo">Memo</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Default value</label>
                                    </div>
                                    <div class="col-sm-9 default_value">
                                        <input class="form-control default_value" id="default_value" type="text" name="default_value" placeholder="">
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Required Field</label>
                                    </div>
                                    <div class="col-sm-9 is_required">
                                        <select class="form-control" name="is_required" id="is_required">
                                            <option value="1">Yes</option>
                                            <option value="0" selected="selected">No</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-outer text-right">

                                <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>

                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End custom field model -->
</div>
<div class="modal fade" id="sendMailModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header">
                <h4 class="modal-title">Email</h4>
            </div>
            <form id="sendEmail">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-1">
                            <button class="blue-btn compose-email-btn">Send</button>
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>To <em class="red-star">*</em></label></div>
                                <div class="col-sm-10"><span><input class="form-control to" name="to" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <a class="add-recipient-link addToRecepent" href="#"> Add Recipients</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Cc</div>
                                <div class="col-sm-10"><span><input class="form-control cc" name="cc" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <a class="add-recipient-link addCcRecepent" href="#"> Add Recipients</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Bcc </label></div>
                                <div class="col-sm-10"><span><input class="form-control bcc" name="bcc" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <a class="add-recipient-link addBccRecepent" href="#"> Add Recipients</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Subject <em class="red-star">*</em></label></div>
                                <div class="col-sm-10"><span><input class="form-control subject" name="subject" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Body <em class="red-star">*</em></label></div>
                                <div class="col-sm-10">
                                    <span><textarea class="form-control summernote" name="body"></textarea></span>
                                    <div class="btn-outer">
                                        <button class="blue-btn">Send</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>


                    <div class="attachmentFile"></div>


                </div>
            </form>
        </div>
    </div>

</div>
<div class="container">
    <div class="modal fade" id="torecepents" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 600px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;height:300px;">
                        <div class="col-sm-6">
                            <label>Select <em class="red-star">*</em></label>
                            <select class="form-control selectUsers">
                                <option value="2">Tenant</option>
                                <option value="4">Owner</option>
                                <option value="3">Vendor</option>
                                <option value="5">Other Contacts</option>
                                <option value="6">Guest Card</option>
                                <option value="7">Rental Application</option>
                                <option value="8">Employee</option>
                            </select>
                        </div>
                        <div class="col-sm-6">                        </div>                        <div class="col-sm-12">
                            <div class="userDetails"></div>
                        </div>                        <div class="col-sm-12">
                            <button class="blue-btn" id="SendselectToUsers">Done</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on("click",".resetguestedit",function () {

       window.location.reload();
    });
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    $(function() {
        $('#datepicker').datepicker();
    });


    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var currencySign = "<?php echo  $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ; ?>";
</script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<!--<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>-->
<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL; ?>/css/bootstrap-tagsinput.css" />
<!--<script src="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.4.2/bootstrap-tagsinput.min.js"></script>-->
<script src="<?php echo COMPANY_SITE_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/leases/RentalApplication/rentalEdit.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/custom_fields.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/leases/guestCard/guestcardflag.js"></script>

<script>

    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
</script>
<script>
    $('.calander').datepicker({
        yearRange: '1919:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('input[name="birth"]').datepicker({
        yearRange: '1900:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    var date = $.datepicker.formatDate(jsDateFomat, new Date());

    $(".calander").val(date);
</script>
<!--<script>
    $(document).ready(function(){
        $(".reset-btn").click(function(){
            $(".reset-all").trigger("reset");
        });
    });
</script>-->
<style>
    .row.custom_field_class input {
        width: 258px;
    }
</style>
<script>

    $("#sendEmail").validate({
        rules: {
            to: {
                required:true
            },
            cc: {
                required:true
            },
            body: {
                required:true
            },
        },
        submitHandler: function (e) {

            var tenant_id = $(".tenant_id").val();
            var form = $('#sendEmail')[0];
            var formData = new FormData(form);
            var path = $(".attachments").attr('href');
            /*  alert(path);*/
            var to = $(".to").val();
            formData.append('to_users',to);
            formData.append('action','sendFileLibraryattachEmail');
            formData.append('class','TenantAjax');
            formData.append('path', path);

            $.ajax({
                url: '/Tenantlisting/getInitialData',
                type: 'POST',
                data: formData,
                success: function (data) {
                    info =  JSON.parse(data);
                    if(info.status=="success"){
                        toastr.success("Email has been sent successfully");
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });


</script>
<script>
    $("#leases_top").addClass("active");
    $('.calander').datepicker({
        yearRange: '1919:2045',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('input[name="birth"]').datepicker({
        yearRange: '1900:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    var date = $.datepicker.formatDate(jsDateFomat, new Date());

    $(".calander").val(date);
</script>
<script>
    function myDateFormatter(dateObject){
        var newdate = dateObject.getFullYear() + "-" + (dateObject.getMonth() + 1) + "-" + dateObject.getDate();
        var tmp = [];
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/getDateFormat',
            data: {class: "TenantAjax",action: "getDateformat",date: newdate,},
            async: false,
            global:false,
            success: function (response) {
                tmp = $.parseJSON(response);
            }
        });
        return tmp;
    }
    var checkdate=$(".move_in_date").val();
    console.log(checkdate);
    var d = $.datepicker.parseDate('mm/dd/yy', checkdate);
    var abc=  d.getDate();
    console.log(abc);
    d.setFullYear(d.getFullYear() + 1);
    if(abc !="31"){
        d.setMonth(d.getMonth() + 1);
        d.setDate(0);
    }
    var outdate=myDateFormatter(d)
    $(".move_out_day").val(outdate);

    $(document).ready(function(){
        $('.summernote').summernote({
            addclass: {
                debug: false,
                classTags: [{title:"Button","value":"btn btn-success"},"jumbotron", "lead","img-rounded","img-circle", "img-responsive","btn", "btn btn-success","btn btn-danger","text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
            },
            width: '100%',
            height: '300px',
            //margin-left: '15px',
            toolbar: [
                // [groupName, [list of button]]
                ['img', ['picture']],
                ['style', ['style', 'addclass', 'clear']],
                ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
                ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
                ['extra', ['video', 'table', 'height']],
                ['misc', ['undo', 'redo', 'codeview', 'help']]
            ]
        });

        $(document).on('click','.clearEthnicity',function () {
            $('.ethnicity_src').val('');
        });
        $(document).on('click','.clearMartial',function () {
            $('.maritalstatus_src').val('');
        })
        $(document).on('click','.ClearHobbies',function () {
            $('.hobbies_src').val('');
        })
        $(document).on('click','.ClearVeteran',function () {
            $('.veteran_src').val('');
        })
        $(document).on('click','.ClearReason',function () {
            $('.reason_source').val('');
        })
        $(document).on('click','.clearSource',function () {
            $('.reff_source').val('');
        })

    });
</script>


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
