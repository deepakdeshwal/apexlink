<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<style>
    body {
    "Open Sans",
    open-sans,
    Arial,
    "Helvetica Neue",
    sans-serif;
    }
    .display{text-align:center;}
    .display h5{
        padding-top:40px;
        font-size:18px;
        font-weight:600;
    }
    .display .affordable-number{
        font-size:36px;
    }
    .display .affordable-txt{
        margin-left:auto;
        margin-right:auto;
        max-width:360px;
        padding:15px
    }
    .display .payment{
        padding: 10px;
        font-size: 18px;
        width: 180px;
        margin-top: 8px;
        position: absolute;
        float: left;
        left: 38%;
    }
    .form-group{
        margin-top:10px;
    }
    .input-group-addon{
        background-color:transparent;
        border:none;
        border-bottom:1px solid #ccc;
        color:#aaa;
    }
    input[type=checkbox].checkbox {
        width:20px;
        margin-right:5px;
    }
    .form-control{
        border:none;
        border-bottom:1px solid #ccc;
        box-shadow: rgba(0, 0, 0, 0.0745098) 0px 0px 0px inset;
        font-size: 16px;
        font-weight: 100;
        color: #222;
    }
    .simple a {font-size:18px;color:#fe6a07;}
    .simple a:hover{
        text-decoration:none;
        color: rgba(254, 106, 7, 0.7);
    }
    .simple .glyphicon {padding-left:5px;}
    .image-wrapper{position:relative;min-height:60px;}
    .graphic-piggy{float:left;position:absolute;bottom:0;left:15px;}
    .graphic-house{float:right;position:absolute;bottom:0;right:15px;}


    /*Chrome Range slider stuff*/
    input[type=range]{
        -webkit-appearance: none;
    }
    input[type=range]::-webkit-slider-runnable-track {

        height: 20px;
        background: #ddd;
        border: none;
        border-radius: 6px;
    }

    input[type=range]::-webkit-slider-thumb {
        -webkit-appearance: none;
        border: none;
        height: 40px;
        width: 40px;
        border-radius: 50%;
        background: #5cb85c;
        margin-top: -8px;
    }
    input[type=range].warning::-webkit-slider-thumb {
        background: #f0ad4e;
    }

    input[type=range]:focus {
        outline: none;
    }

    input[type=range]:focus::-webkit-slider-runnable-track {
        background: #ccc;
    }
    /*IE Range slider stuff*/
    input[type=range]::-ms-track {

        height: 20px;

        /*remove bg colour from the track, we'll use ms-fill-lower and ms-fill-upper instead */
        background: transparent;

        /*leave room for the larger thumb to overflow with a transparent border */
        border-color: transparent;
        border-width: 6px 0;

        /*remove default tick marks*/
        color: transparent;
    }
    input[type=range]::-ms-fill-lower {
        background: #777;
        border-radius: 10px;
    }
    input[type=range]::-ms-fill-upper {
        background: #ddd;
        border-radius: 10px;
    }
    input[type=range]::-ms-thumb {
        border: none;
        height: 26px;
        width: 26px;
        border-radius: 50%;
        background: #5cb85c;
    }
    input[type=range].warning::-ms-thumb {
        background: #f0ad4eY;
    }
    input[type=range]:focus::-ms-fill-lower {
        background: #888;
    }
    input[type=range]:focus::-ms-fill-upper {
        background: #ccc;
    }
    /*End IE Range slider stuff*/
</style>
<div id="wrapper">
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>




    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Lease  &gt;&gt; <span>Affordability Calculator</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text">
                            </div>
                        </div>
                    </div>
                </div>

                <div ng-app="affordApp">
                    <div ng-controller="affordCtrl">
                        <div class="container">
                            <div class="col-sm-12">
                            <div class="content-section">
                            <div class="row">
                                <div class="col-sm-7  col-md-8 col-md-push-4 display">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Result </h3>
                                        </div>

                                        <div class="form-data">
                                            <p class="text-center">You can afford a house up to</p>
                                    <p class="affordable-number text-center">
                                        {{user.details.affordableValue  | currency:undefined:0}}
                                    </p>
                                    <div class="affordable-txt">
                                        <p  class="text-center" ng-show="user.details.debtToIncome <= 36">Based on your income, a house at this price should fit comfortably within your budget.</p>
                                        <p  class="text-center" ng-show="user.details.debtToIncome > 36">Based on your income, a house at this price may stretch your budget too thin.</p>
                                    </div>

                                    <div class="image-wrapper">
                                        <img class="graphic-piggy" src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/piggy-bank.png" ng-style="{'width' : imgSize('pig'), 'height' : imgSize('pig')}">
                                        <div class="payment label" ng-class="user.details.debtToIncome <= 36 ? 'label-success':'label-warning'">{{user.details.payment | currency:undefined:0}}/mo</div>
                                        <img class="graphic-house" src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/home-New.png" ng-style="{'width' : imgSize('house'), 'height' : imgSize('house')}">
                                    </div>
                                    <div>
                                        <input type="range" ng-class="user.details.debtToIncome > 36 ? 'warning':''"  ng-change="getAffordability('slider')" ng-model="user.details.debtToIncome" class="range" min="0" step="1" max="43" />
                                    </div>
                                    </div>
                                </div>
                                </div>
                                <div class="col-sm-5 col-md-4 col-md-pull-8">
                                    <div class="form-outer">
                                    <div class="form-data">

                                    <div class="form-group">
                                        <label for="annualIncome">Annual income</label>
                                        <div class="input-group">

                            <span class="input-group-addon">
                                $
                            </span>

                                            <input type="text" class="form-control" ng-keyup="getAffordability()" ng-model="user.details.annualIncome" name="annualIncome" id="annualIncome" maxlength="30" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="monthlyDebts">Monthly debts</label>
                                        <div class="input-group">
                            <span class="input-group-addon">
                                $
                            </span>
                                            <input type="text" ng-keyup="getAffordability()" class="form-control" ng-model="user.details.monthlyDebts" name="monthlyDebts" id="monthlyDebts" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="downPayment">Down payment</label>
                                        <div class="input-group">
                            <span class="input-group-addon">
                                $
                            </span>
                                            <input type="text" ng-keyup="getAffordability()" class="form-control" ng-model="user.details.downPayment" name="downPayment" id="downPayment" />

                                        </div>
                                    </div>
                                    <div ng-show="!simple">
                                        <div class="form-group">
                                            <label for="debtToIncome">Debt-to-income</label>
                                            <div class="input-group">
                                <span class="input-group-addon">

                                </span>
                                                <input type="text" ng-keyup="getAffordability()" class="form-control" ng-model="user.details.debtToIncome" name="debtToIncome" id="debtToIncome" />
                                                <span class="input-group-addon">
                                    %
                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="interestRate">Interest rate</label>
                                            <div class="input-group">
                                <span class="input-group-addon">
                                    $
                                </span>
                                                <input type="text" ng-keyup="getAffordability()" class="form-control" ng-model="user.details.interestRate" name="interestRate" id="interestRate" />
                                                <span class="input-group-addon">
                                    %
                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="loanTerm">Loan term</label>
                                            <select type="text" class="form-control" ng-change="getAffordability()" ng-model="user.details.loanTerm" name="loanTerm" id="loanTerm" ng-pattern="" required>
                                                <option value="30" ng-selected="true">30-year fixed</option>
                                                <option value="15">15-year fixed</option>
                                                <option value="5">5/1 ARM</option>
                                            </select>

                                        </div>
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <input type="checkbox" ng-click="getAffordability()" class="form-control checkbox" ng-model="user.details.incTaxIns" name="showTaxes" id="incTaxIns" />
                                                <label for="incTaxIns">Include taxes/ins.</label>

                                            </div>
                                        </div>
                                        <div class="form-inline" ng-show="user.details.incTaxIns">
                                            <div class="form-group">
                                                <label for="taxes">Property tax</label>
                                                <div class="input-group">
                                    <span class="input-group-addon">
                                        $
                                    </span>
                                                    <input type="text" style="" ng-keyup="getAffordability('ptax')" class="form-control" ng-model="user.details.taxes" name="taxes" id="taxes" maxlength="10" /><span class="input-group-addon">/year</span>

                                                    <input type="text" style="" ng-keyup="getAffordability('ptaxpercent')" class="form-control" ng-model="user.details.taxPercent" name="taxPercent" id="taxPercent" maxlength="5" />
                                                    <span class="input-group-addon">
                                        %
                                    </span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="form-group" ng-show="user.details.incTaxIns">
                                            <label for="homeInsurance">Home insurance</label>
                                            <div class="input-group">
                                <span class="input-group-addon">
                                    $
                                </span>
                                                <input type="text" ng-keyup="getAffordability()" class="form-control" ng-model="user.details.homeInsurance" name="homeInsurance" id="homeInsurance" />
                                                <span class="input-group-addon">
                                    /year
                                </span>
                                            </div>
                                        </div>
                                        <div class="form-inline" ng-show="user.details.incTaxIns">
                                            <div class="form-group">
                                                <input type="checkbox" class="form-control checkbox" ng-click="getAffordability()" ng-model="user.details.incPMI" name="incPMI" id="incPMI" />
                                                <label for="incPMI">Include PMI.</label>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="hoaDues">HOA dues</label>
                                            <div class="input-group">
                                <span class="input-group-addon">
                                    $
                                </span>
                                                <input type="text" ng-keyup="getAffordability()" class="form-control" ng-model="user.details.hoaDues" name="hoaDues" id="hoaDues" />
                                                <span class="input-group-addon">
                                    /month
                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="simple" ng-show="simple"><a href="#" ng-click="simple = false">Advanced<span class="glyphicon glyphicon-chevron-down"></span></a></div>
                                    <div class="simple" ng-show="!simple"><a href="#" ng-click="simple = true">Simple<span class="glyphicon glyphicon-chevron-up"></span></a></div>



                                </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
<!-- Wrapper Ends -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->


<!-- Jquery Starts -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.8/angular.min.js"></script>

<script>
    angular.module('affordApp', [])
        .controller('affordCtrl', function($scope, $http, $filter, $attrs) {
            $scope.title = "Affordability calculator";
            $scope.simple = true;
            $scope.user = {details:{}};
            $scope.user.details.annualIncome = 70000;
            $scope.user.details.monthlyDebts = 250;
            $scope.user.details.downPayment = 20000;

            $scope.user.details.debtToIncome = 36;
            $scope.user.details.interestRate = 3.875;
            $scope.user.details.loanTerm = 30;

            $scope.user.details.incTaxIns = true;
            $scope.user.details.taxes = 2800;
            $scope.user.details.taxPercent = 0.8;
            $scope.user.details.homeInsurance = 800;
            $scope.user.details.incPMI = true;
            $scope.user.details.hoaDues = 0;
            $scope.user.details.affordableValue = 308122;
            $scope.imgSize = function(image) {
                if(image === 'pig'){
                    return (100 - $scope.user.details.debtToIncome*1)+'px';
                }
                else {
                    return (40 + $scope.user.details.debtToIncome*1)+'px';
                }
            }
            $scope.bgColor = function(){
                if($scope.user.details.debtToIncome > 36){return '#f0ad4e';}
            }
            $scope.getAffordability = function(name){

                $scope.user.details.payment = ($scope.user.details.annualIncome / 12) * ($scope.user.details.debtToIncome / 100) - $scope.user.details.monthlyDebts*1;
                var yrs = $scope.user.details.loanTerm * 12;
                var rte = $scope.user.details.interestRate / 1200;
                var pmt = $scope.user.details.payment;
                if($scope.user.details.incTaxIns){
                    pmt = $scope.user.details.payment - (($scope.user.details.taxes*1 / 12) + ($scope.user.details.homeInsurance*1/12));

                }
                pmt = pmt - $scope.user.details.hoaDues*1;
                var amt = pmt/(rte+(rte/(Math.pow((1+rte),(yrs))-1)));
                if ($scope.user.details.incPMI && $scope.user.details.incTaxIns) {
                    //80: 0, 85: .0044, 90: .0059, 95: .0076, 100: .0098
                    //.007104
                    pmt = pmt - (amt * .0071043 / 12);
                    amt = pmt/(rte+(rte/(Math.pow((1+rte),(yrs))-1)));
                }
                if(amt < 1){amt = 0;}
                if($scope.user.details.payment < 300){$scope.user.details.payment = 300;}

                if(name=='ptaxPercent'){
                    $scope.user.details.taxes =  ($scope.user.details.taxPercent / 100) * amt;
                }
                else if(name=='slider' || name == 'ptax'){
                    $scope.user.details.taxPercent = $filter('number')($scope.user.details.taxes / amt * 100, 1);
                }
                $scope.user.details.affordableValue = amt + $scope.user.details.downPayment*1;

            }

            $scope.getAffordability();

        });

</script>
