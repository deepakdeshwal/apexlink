<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <div class="col-sm-12 generatepage">
        <div class="content-section">
        <!--Tabs Starts -->
        <div class="main-tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="javascript:void(0);" data-tab="property">Select Property</a></li>
                <li role="presentation"><a href="javascript:void(0);" data-tab="lease">Lease Details</a></li>
                <li role="presentation"><a href="javascript:void(0);" data-tab="charges">Charges</a></li>
                <li role="presentation" class="active"><a href="javascript:void(0);" data-tab="generate">Generate
                        Lease</a></li>
            </ul>
            <!-- Nav tabs -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="guest-cards">
                    <div id="wrapper">
                        <section class="main-content">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="main-tabs">
                                        <div role="tabpanel" class="tab-pane active" id="communication-one">
                                            <!-- Sub Tabs Starts-->
                                            <div class="accordion-form">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <form action="" method="POST" name="move_in_form" id="move_in_form">
                                                                <input type="hidden" name="user_id" class="movein_user_id">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading moveintags">
                                                                        <div class="row">
                                                                            <div class="col-sm-9">
                                                                                <div class="col-sm-6"><a href="javascript:void(0);" class="propertyAddtag">Zen Zen-property zen12</a></div>
                                                                                <div class="col-sm-4"><a href="javascript:void(0);" class="tenantNametag">Adam Clark</a></div>
                                                                                <div class="col-sm-2"><a href="javascript:void(0);" class="statetag">NY</a></div>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <div class="col-sm-12">
                                                                                    <a class="pull-right" href="/Lease/Movein"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                        <div class="panel-body">
                                                                            <div class="row">
                                                                                <div class="form-outer tanet-outer moveinlabeldata">
                                                                                    <div class="col-xs-12 col-sm-4 col-md-2 clear ">
                                                                                        <label>Scheduled Move in Date </label>
                                                                                        <p class="mi_schedule_move_in">June 18, 2019 (Tue.)</p>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                                                        <label>Actual Move in Date</label>
                                                                                        <p class="mi_actual_move_in">June 20, 2019 (Tue.)</p>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                                                        <label>Lease Start Date</label>
                                                                                        <p class="mi_start_date">June 23, 2019 (Tue.)</p>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-sm-4 col-md-2 ">
                                                                                        <label>Lease End Date</label>
                                                                                        <p class="mi_end_date">June 26, 2019 (Tue.)</p>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-sm-12 col-md-12 tanet-inner-heading">
                                                                                        <label class="blue-label">Payments</label>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-sm-4 col-md-2 min-ht clear">
                                                                                        <label>Security :</label>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                                                        <label>Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                                                        <p class="mi_amount">800.00</p>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                                                        <label>Paid Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                                                        <input class="form-control" name="paid_amount" type="text"/>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-sm-4 col-md-2 min-ht clear">
                                                                                        <label>Prorated Rent :</label>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                                                        <label>Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                                                        <input class="form-control pro_rent" name="prorated_amount" type="text"/>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                                                        <label>MEMO</label>
                                                                                        <input class="form-control" name="memo" type="text"/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="btn-outer text-right">
                                                                            <a class="blue-btn save_move_in" href="javascript:void(0)">Move in</a>
                                                                            <button type="button" class="clear-btn" id='ClearForm'>Clear</button>
                                                                            <a class="grey-btn cancel_move_in" href="javascript:void(0)">Cancel</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";

    $(document).ready(function () {
        $(document).on('click','#ClearForm',function () {
            bootbox.confirm("Do you want to reset this form?", function(result) {
                if (result == true) {
                    location.reload();
                }
            });
        })
    });

</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/leases/move-in/tenantListing.js"></script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->