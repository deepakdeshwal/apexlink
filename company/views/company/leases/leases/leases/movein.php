<div class="col-sm-12 generatepage">
    <div class="content-section">
        <!--Tabs Starts -->
        <div class="main-tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="javascript:void(0);" data-tab="property">Select Property</a></li>
                <li role="presentation"><a href="javascript:void(0);" data-tab="lease">Lease Details</a></li>
                <li role="presentation"><a href="javascript:void(0);" data-tab="charges">Charges</a></li>
                <li role="presentation" class="active"><a href="javascript:void(0);" data-tab="generate">Generate
                        Lease</a></li>
            </ul>
            <!-- Nav tabs -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="guest-cards">
                    <div id="wrapper">
                        <section class="main-content">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="bread-search-outer">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="breadcrumb-outer">
                                                    Tenants >> <span>New Tenant</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="easy-search">
                                                    <input placeholder="Easy Search" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-12">

                                    </div>
                                    <div class="col-sm-12">
                                        <div class="content-section">
                                            <div class="main-tabs">
                                                <div role="tabpanel" class="tab-pane active" id="communication-one">
                                                    <!-- Sub Tabs Starts-->
                                                    <div class="accordion-form">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <form action="" method="POST" name="move_in_form" id="move_in_form">
                                                                        <input type="hidden" name="user_id" class="movein_user_id">
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-heading">
                                                                                <h4 class="panel-title">
                                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="collapsed" aria-expanded="false">Final Move-in</a>
                                                                                    <a class="back" href="/Tenantlisting/Tenantlisting"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                                                </h4>
                                                                            </div>
                                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                <div class="panel-body">
                                                                                    <div class="row">
                                                                                        <div class="form-outer tanet-outer">
                                                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                                                <label>Scheduled Move In Date</label>
                                                                                                <input class="form-control scheduled_move_in" name="scheduled_move_in" type="text"/>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                                                <label>Actual Move in Date </label>
                                                                                                <input class="form-control actual_move_in" name="actual_move_in" type="text"/>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-2 clear ">
                                                                                                <label>Scheduled Move in Date </label>
                                                                                                <p class="mi_schedule_move_in">June 18, 2019 (Tue.)</p>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                                                <label>Actual Move in Date</label>
                                                                                                <p class="mi_actual_move_in">June 20, 2019 (Tue.)</p>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                                                <label>Lease Start Date</label>
                                                                                                <p class="mi_start_date">June 23, 2019 (Tue.)</p>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-2 ">
                                                                                                <label>Lease End Date</label>
                                                                                                <p class="mi_end_date">June 26, 2019 (Tue.)</p>
                                                                                            </div>
                                                                                            <div class="tanet-inner-heading">
                                                                                                <label class="blue-label">Payments</label>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-2 min-ht clear">
                                                                                                <label>Security :</label>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                                                <label>Amount (USh)</label>
                                                                                                <p class="mi_amount">800.00</p>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                                                <label>Paid Amount (USh)</label>
                                                                                                <input class="form-control" name="paid_amount" type="text"/>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-2 min-ht clear">
                                                                                                    <label>Prorated Rent :</label>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                                                <label>Amount (USh)</label>
                                                                                                <input class="form-control pro_rent" name="prorated_amount" type="text"/>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                                                <label>MEMO</label>
                                                                                                <input class="form-control" name="memo" type="text"/>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <div class="btn-outer">
                                                                                    <a class="blue-btn save_move_in" href="#">Move in</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->