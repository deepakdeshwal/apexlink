<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/images/favicon.ico" alt="apex-icon" type="image/ico" sizes="16x16">

    <title>Apexlink</title>
    <!-- Bootstrap -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/ui.jqgrid.min.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery.timepicker.min.css">

    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/passwordscheck.css" />
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL;?>/css/calculator.css"/>
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/owl.theme.default.min.css"/>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.multiselect.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/main.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />



    <!-- include libraries(jQuery, bootstrap) -->
    <!-- include summernote css/js -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/summernote.js" defer></script>

    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery-ui.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.timepicker.min.js"></script>

    <script src="<?php echo COMPANY_SITE_URL; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.jqgrid.min.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.responsivetabs.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.validate.min.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/common.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/toastr.min.js"></script>

    <script src="<?php echo COMPANY_SITE_URL; ?>/js/bootbox.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.mask.min.js"></script>
    <script defer src="<?php echo SUPERADMIN_SITE_URL; ?>/js/calculator.js"></script>
    <script defer src="<?php echo COMPANY_SITE_URL; ?>/js/owl.carousel.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/core-js/2.1.4/core.min.js"></script>
</head>
<body>
    <input type="hidden" value="<?php echo $_GET['id'] ?>" class="leaseid">
    <div class="header" style="max-height: 70px; margin-bottom: 20px;width: 100%;background: #d6f0fe;float: left;padding: 1px 0;min-width: 1024px;max-width: 100%;">
        <div class="header-inner">
            <div class="logo">
                <a class="navbar-brand" href="">
                    <img src="<?php echo $_SERVER['HTTP_HOST'] ?>/company/images/logo.png" alt="Company Logo" width="143" height="49">
                </a>
            </div>
            <div class="hdr-rt">
            </div>
        </div>
    </div>
    <div style="float: right;margin-right: 138px;margin-bottom: 10px;">
        <input type="button" id="btnDownload" value="Download" style="margin-right:10px;" class="blue-btn">
        <input type="submit" id="btnSave" value="Submit" class="blue-btn">
    </div>
    <div class="" style="float: left; width: 100%;" id="dv-Main-pdf">
        <div class="" id="dvPdfToHtml" style="min-height:500px;overflow-y:scroll;float:left;width:80%; padding:2%;border:1px solid #ddd;margin-left: 8%;">
            <meta charset="utf-8">
            <title></title>
            <div width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#d6f0fd" style="border-radius: 6px 6px 0px 0px; -moz-border-radius: 6px 6px 0px 0px; -webkit-border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2D3091; margin: auto;" align="center">
                <table style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse; padding: 0; margin: 0;">
                    <tbody>
                        <tr style="background-color: #00b0f0; height: 20px;"><td></td></tr>
                        <tr style="border-collapse: collapse; background-color: #fff;">
                            <td class="w580" style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse: collapse;" width="580">
                                <center><img class="COMPANYLOGO" src="" width="150" height="50"></center>
                            </td>
                        </tr>
                        <tr style="background-color: #00b0f0; height: 20px;"><td></td></tr>
                    </tbody>
                </table>
            </div>
            <div width="640" border="0" cellpadding="0" cellspacing="0" align="center">
                <div>
                    <div width="528" valign="top" style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; font-size: 13px; padding: 0; line-height: 19px; margin: 0; border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 10px 0;">
                        <p style="margin: 5px 0; text-align: left;">&nbsp;</p>
                        <table>
                            <tbody>
                            <tr>
                                <td style="color: #000; font-weight: bold; line-height: 5px; font-size: 12px;">
                                    Residential Lease agreement<br>
                                    <center><span style="font-size: 7px;">______________________________________________</span></center>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <p style="padding: 0 10px; margin-bottom: 5px; text-align: left;">
                            The following residential agreement is between Apexlink LLC. new and <span class="TENANTNAME"></span>,
                            as of <span class="LEASESTARTDATE"></span>. Each numbered section defines the term, conditions, and
                            responsibility of each party.
                        </p>
                        <div style="width: 85%;text-align:left;">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; line-height: 5px; font-weight: bold;
                                    font-size: 12px; text-align: left;" valign="baseline">
                                        LANDLORD
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        In this agreement the Landlord(s) and/or related agent(s) is/are referred to as
                                        “Landlord”. Apexlink LLC. new is the Landlord.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left;">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; line-height: 5px; font-weight: bold;
                                    font-size: 12px; text-align: left;" valign="baseline">
                                        TENANT
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        Every instance of the term “Tenant” in this Lease will refer to <span class="TENANTNAME"></span>.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        RENTAL PROPERTY
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        The property located at <span class="ALLADDRESS"></span>,
                                        which is owned/managed
                                        by the Landlord will be rented to the Tenant. In this Lease the property will be
                                        referred to as “lease Premises”.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        TERM OF LEASE AGREEMENT
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        This Lease Agreement will start on <span class="LEASESTARTDATE"></span> and ends on <span class="LEASEENDDATE"></span>.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        USE &amp; OCCUPANCY OF PROPERTY
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        The only authorized person(s) that may live in the Leased Premises is/are: <span class="TENANTNAME"></span>.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        AMOUNT OF RENT
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        The exact amount of Rent due monthly for the right to live in the Leased Premises
                                        is <span class="RENTAMOUNT"></span>.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        DUE DATE FOR RENT
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        Rent is to be paid before or on the <span class="RENTBEFOREDAY"></span>. A <span class="GRADEPERIOD"></span> day grace period
                                        will ensue; nevertheless, rent cannot be paid after this grace period without
                                        incurring
                                        a late fee.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        LATE FEE
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        In the event that rent or associated fees are not paid in full by the rental due
                                        date or before the end of the <span class="GRADEPERIOD"></span> day grace period, a late fee of __________________
                                        will be incurred.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        RETURNED PAYMENTS
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        Any returned payments from financial institutions that the Tenant utilizes will
                                        incur an addition fee of 0.00 that will be added to the rental amount.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        SECURITY DEPOSIT
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        A. Security Deposit of <span class="SECURITYDEPOSITE"></span> is to be paid by the Tenant before move-in.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        B. This Security Deposit is reserved for the purpose of paying any cost toward
                                        damages,
                                        cleaning, excessive wear to the property, and in the event of unreturned keys or
                                        abandonment of the Leased Premises before or upon the end of the Lease.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        UTILITIES &amp; SERVICES
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        A. The responsibility of the registering and paying for the following utility
                                        services is upon the Tenant:
                                        <p style="text-align: left;">&nbsp;</p>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td align="left">
                                                    <table width="200" border="1"><tbody><tr><td>&nbsp;</td></tr></tbody></table>
                                                </td>
                                                <td align="left"><table width="200" border="1"><tbody><tr><td>&nbsp;</td></tr></tbody></table></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p style="text-align: left;">&nbsp;</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">These services should be maintained at all time during the leasing period.</td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        B. The Landlord has included the Water utility as a part of rent; however, if the
                                        property water usage exceeds Enter amount here the Landlord reserves the right to
                                        request that any overages in usage be compensated as a fee in addition to the normal
                                        rental rate. These overages must be paid within 20 days.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        MAINTENANCE &amp; REPAIRS
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        General maintenance and repairs are the responsibility of the Landlord, except in
                                        cases where the Tenant has been negligent or has accidentally caused damages to
                                        the property.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        A. It will remain the Tenant’s responsibility to promptly notify the Landlord of
                                        any necessary repairs to the property.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        B. Any damages that have resulted from the Tenant or the Tenant’s guests will be
                                        the Tenants full responsibility to pay for the costs of repair that is required.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        CONDITION OF PROPERTY
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        A. The Tenant has inspected the Leased Premises acknowledges and accepts that the
                                        Leased Premises is in an acceptable living condition and that all parts of the
                                        Leased Premises are in good working order.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        B. It is agreed by the Landlord and Tenant that no promises are made concerning
                                        the condition of the Leased Premises.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        C. The Leased Premises will be returned to the Landlord by the Tenant in the same
                                        or better condition that it was in at the commencement of the Lease.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        ENDING OR RENEWING THE LEASE AGREEMENT
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        A. When this Lease ends, if the Tenant or Landlord do not furnish a written
                                        agreement to end the Lease it will continue on a month to month basis indefinitely. In order
                                        to terminate or renew the Lease agreement either party, Landlord or Tenant, must
                                        furnish a written notice at least <span class="NOTICEPERIODDAYS"></span> days before the end of this Lease agreement.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        GOVERNING LAW
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        The terms, conditions, and any addenda to this Lease are to be governed and held
                                        in accord to the statutes and Laws of Enter Governing entity here..
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        ENTIRE AGREEMENT
                                        ___________________________________________________________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        NOTICE: This is a LEGALLY binding document.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        A. You are relinquishing specific important rights.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        B. You may reserve the right to have an attorney review the Lease Agreement before
                                        signing it.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        By signing this Lease Agreement, the Tenant certifies in good faith to have read,
                                        understood, and agrees to abide by all of the terms and conditions stated within
                                        this Lease.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%; margin-top: 20px; padding: 0 10px">
                            <div style="width:100%; float:left;">
                                <!--<b style="float:left;">LANLORD [by Agent under Property Management Agreement]:</b><br>-->
                                <b style="float:left;" class="owner-manger-block">LANLORD [by Agent under Property Management Agreement]:</b><br>
                                <div class="ownerSection"></div>
                            </div>
                            <div style="width:100%; float:left;">
                                <b style="float:left;">TENANT(S):</b><br>
                                <div class="tenantSection"></div>
                                <div class="addTenantSection"></div>
                            </div>
                            <div style="width:100%; float:left;">
                                <b style="float:left;">GUARANTOR:</b><br>
                                <div class="guarantorSection"></div>
                            </div>
                        </div>
                        <div style="width: 85%;text-align:left;  margin-top: 20px; padding: 0; height: 730px;">
                        </div>
                    </div>
                </div>
            </div>
            <div width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#585858" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px;
            -webkit-font-smoothing: antialiased; background-color: #585858; color: #fff;clear: both;" align="center">
                <table style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;
                border-collapse: collapse; padding: 0; margin: 0;">
                    <tbody>
                    <tr>
                        <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;" align="center">
                            ApexLink Property Manager● <a href="javascript:;" style="color: #fff; text-decoration: none;">support@apexlink.com &nbsp; ●772-212-1950
                            </a></td>
                    </tr>
                    <tr><td style="line-height: 5px" id="Td2" valign="middle" align="center">&nbsp;</td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/generateLease.js"></script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>