<div class="col-sm-12 chargepage">
    <div class="content-section">
        <!--Tabs Starts -->
        <div class="main-tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="javascript:void(0);" data-tab="property">Select Property</a></li>
                <li role="presentation"><a href="javascript:void(0);" data-tab="lease">Lease Details</a></li>
                <li role="presentation" class="active"><a href="javascript:void(0);" data-tab="charges">Charges</a></li>
                <li role="presentation"><a href="javascript:void(0);" data-tab="generate">Generate Lease</a></li>
            </ul>
            <!-- Nav tabs -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="guest-cards">
                    <form id="addLateFee" method="post">
                    <div class="form-outer form-outer2">
                        <div class="form-hdr">
                            <h3>Late Fee Charges <a class="back" href="javascript:;">
                                    <i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                            </h3>
                        </div>
                        <div class="form-data">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="check-outer">
                                        <input type="checkbox" name="oneTime" class="applyOneTime" value="0"/> <label>Apply One Time</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="radio-input-label">
                                        <input type="radio" class="oneTimeRadio1 oneTimeRadio" name="oneTimeRadio" disabled value="1"/><input type="text"  class="oneTimeFeeLabel removeAttr" maxlength="5" disabled/> <label>Flat Fee (KZT)
                                            (Example 10.00)</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="radio-input-label">
                                        <input type="radio" class="oneTimeRadio2 oneTimeRadio" name="oneTimeRadio" value="0"  disabled/><input  type="text"   class="oneTimeRentLabel removeAttr" maxlength="5" disabled/> <label>% of Monthly Rent
                                            (Example 20)</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="check-outer">
                                        <input type="checkbox" name="daily" class="applyDaily" value="0"/> <label>Apply Daily</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="radio-input-label">
                                        <input type="radio" name="dailyRadio" value="1" class="dailyRadio1 dailyRadio" disabled/><input  type="text"  class="dailyFeeLabel removeAttr" maxlength="5"/> <label>Flat Fee (KZT)
                                            (Example 10.00)</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="radio-input-label">
                                        <input type="radio" name="dailyRadio" value="0" class="dailyRadio2 dailyRadio" disabled/><input  type="text"  class="dailyRentLabel removeAttr" maxlength="5"/> <label>% of Monthly Rent
                                            (Example 20)</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="radio-input-label">
                                        <label>Grace Period </label> <input  type="text" name="gracePeriod"/>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button class="blue-btn">Apply</button>
                                <button class="grey-btn cancelBtn">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>








                    <!-- Form Outer Ends -->
                    <div class="form-outer">
                        <div class="form-hdr">
                            <h3>Tax Pass Through Details</h3>
                        </div>
                        <div class="form-data">
                            <form id="addTaxDetails" method="post">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-3">
                                    <label>Tax Name <em class="red-star">*</em></label>
                                    <input class="form-control" type="text" name="tax_name"/>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-3">
                                    <label>Type <em class="red-star">*</em></label>
                                    <select class="form-control tax_type" name="tax_type">
                                        <option value="">Select</option>
                                         <option value="P">Percentage</option>
                                          <option value="F">Flat</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-3">
                                    <label>Value<em class="pSign">()</em> <em class="red-star">*</em></label>
                                    <input class="form-control amount number_only" type="text" id="tax_value" name="tax_value"/>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-3">
                                    <label>Select Charge Code <em class="red-star">*</em>
                                        <a class="pop-add-icon getchargeajax" href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#addchargescode">
                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                        </a>
                                    </label>
                                    <select class="form-control tax_chargeCode" name="tax_chargeCode">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button class="blue-btn">Apply</button>
                                <button class="grey-btn cancelBtn">Cancel</button>
                            </div>
                        </form>
                        </div>
                    </div>
                    <!-- Form Outer Ends -->
                    <div class="form-outer">
                        <div class="form-hdr">
                            <h3>Charges</h3>
                        </div>
                   <div class="form-data">

                 
                   <div class="chargeForm" style="display:none;">
                     <div class="form-hdr"> <h3>List of Charge</h3>  </div>
                    <form id="addChargeForm">

                    <div class="col-sm-3 col-md-3">
                                   <label>Select Charge Code <em class="red-star">*</em>
                                        <a class="pop-add-icon getchargeajax" href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#addchargescode">
                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                        </a>
                                    </label>
                                    <select class="form-control tax_chargeCode" name="chargeCode">
                                        <option value="">Select</option>
                                    </select>
                    </div>
                     <div class="col-sm-3 col-md-3">
                                    <label>Frequency <em class="red-star">*</em></label>
                                    <select class="form-control" name="frequency">

                                          <option value="One Time">One Time</option>
                                          <option value="Monthly2">Monthly</option>
                                    </select>
                    </div>
                      <div class="col-sm-3 col-md-3">
                        <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$'; ?>
                        <label>Amount (<?php echo $default_symbol ?>)</label>
                        <input class="form-control capsOn" type="text"
                        id="amount" name="amount" maxlength="5">
                        <span class="flast_nameErr error red-star"></span>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <label>Start Date</label>
                        <input class="form-control capsOn calander" type="text"
                        id="startDate" name="startDate" readonly>
                        <span class="flast_nameErr error red-star"></span>
                    </div>

                      <div class="col-sm-3 col-md-3">
                        <label>End Date</label>
                        <input class="form-control capsOn calander" type="text"
                        id="birth" name="endDate" readonly>
                        <span class="flast_nameErr error red-star"></span>

                    </div>
                    <div class="col-sm-12 col-md-12">
                   <button class="blue-btn" id="save-charge-form">Save </button>
                  </div>
                </form>


                    </div>
                   <div class="">
                        <button class="blue-btn pull-right" id="addChargeButton">Add Charge</button>
                    <div class="chargeData">


                    </div>
                
                    </div>
                    </div>

                    
                    </div>
                    <!-- Form Outer Ends -->
                    <form id="addChargeNote">
                    <div class="form-outer">
                        <div class="form-hdr">
                            <h3>Notes</h3>
                        </div>
                        <div class="form-data chargeNoteHtml">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <textarea class="form-control add-input chargeNoteClone capital" name="chargeNote[]"></textarea>
                                    <a class="add-icon addNote" href="javascript:;">

                                        <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                        <a class="add-icon removeNote" href="javascript:;" style="display:none;">

                                        <i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeventeen">
                                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> File Library</a>
                                            </h4>
                                        </div>
                                        <div id="collapseSeventeen" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-4">
                                                            <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                            <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                           
                                                            <input type="button" class="orange-btn" id="remove_library_file" value="Remove All Files...">
                                                          
                                                        </div>
                                                        <div class="row" id="file_library_uploads">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid-outer">
                                                    <div class="apx-table">
                                                        <div class="table-responsive">
                                                            <table id="propertFileLibrary-table" class="table table-bordered">
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                              
                                            </div>
                                        </div>
                                    </div>
                    <!-- Form Outer Ends -->
                   
               </form>

                    <!-- Form Outer Ends -->
                </div>
                <div class="btn-outer">
                    <button class="blue-btn chargeNext">Next</button>
                    <button class="grey-btn cancelBtn">Cancel</button>
                </div>
            </div>
            <!--tab Ends -->
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    getCharges();

    var tenant_id = $('.tenant_session_id').val();
    $.ajax({
        url:'/tenantAjax?action=chargeDetails&class=tenantAjax&tenant_id='+tenant_id,
        type: 'GET',
        success: function (data) {
            $('.chargeData').html(data);
        },
        cache: false,
        contentType: false,
        processData: false
    });

    $.ajax({
        url:'/tenantAjax?action=chargeCode&class=tenantAjax',
        type: 'GET',
        success: function (data) {
            $('.tax_chargeCode').append(data);
        },
        cache: false,
        contentType: false,
        processData: false
    });

    $(document).on("click",".getchargeajax",function () {
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/getChargeCodes',
            data: {
                class: "TenantAjax",
                action: "getChargeCodes"
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    var debits = data.data.debits;
                    var credits = data.data.credits;
                    if (debits.length > 0){
                        var debitsOptions = '';
                        for (var i = 0; i < debits.length; i++){
                            debitsOptions += "<option value='"+debits[i].id+"'>"+debits[i].debit_accounts+"</option>";
                        }
                        $("#chargecode_popup select[name='debit_account']").html(debitsOptions);
                    }

                    if (credits.length > 0){
                        var creditOptions = '';
                        for (var i = 0; i < credits.length; i++){
                            creditOptions += "<option value='"+credits[i].id+"'>"+credits[i].credit_accounts+"</option>";
                        }
                        $("#chargecode_popup select[name='credit_account']").html(creditOptions);
                    }

                    toastr.success(data.message);
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    $(document).on("click",".savechargecodepopup",function () {
        var chargeCode = $("#chargecode_popup input[name='charge_code']").val();
        var creditValue = $("#chargecode_popup select[name='credit_account']").val();
        var debitValue = $("#chargecode_popup select[name='debit_account']").val();
        var status = $("#chargecode_popup select[name='status']").val();
        var description = $("#chargecode_popup textarea[name='description']").val();
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/saveChargeCodes',
            data: {
                class: "TenantAjax",
                action: "saveChargeCodes",
                chargeCode:chargeCode,
                creditValue:creditValue,
                debitValue:debitValue,
                status:status,
                description:description
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    $("#chargecode_popup").trigger('reset');
                    $("#addchargescode").modal('hide');
                    var lastId = data.last_insert_id;
                    var option = "<option value='"+lastId+"' selected>"+data.data.charge_code+"</option>";
                    $("#addTaxDetails .tax_chargeCode").append(option);
                    $(".tax_chargeCode").append(option);
                    toastr.success(data.message);
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    function getCharges(){
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/getCharges',
            data: {
                class: "TenantAjax",
                action: "getCharges"
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    var charges = data.data.charges;
                    if (charges.length > 0){
                        var chargesOptions = '';
                        for (var i = 0; i < charges.length; i++){
                            chargesOptions += "<option value='"+charges[i].id+"'>"+charges[i].charge_code+"</option>";
                        }
                        $("#addTaxDetails .tax_chargeCode").html(chargesOptions);
                    }

                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});
 










    var upload_url = "<?php echo SITE_URL; ?>";
    //add propertySetup form client side validations
$("#addLateFee").validate({ 
    rules: {
        lateFeeCharge: {
            required:true,
            number:true
        },
         gracePeriod: {
            required:true,
            number:true
        },
        
     },
  submitHandler: function (e) {

      var tenant_id = $('.tenant_session_id').val();

       
       var form = $('#addLateFee')[0]; 
       var formData = new FormData(form);
       formData.append('action','insertCharge');
       formData.append('class','tenantAjax');
       formData.append('tenant_id',tenant_id);
       action = 'insertCharge';      
          $.ajax({

        url:'/tenantAjax',
        type: 'POST',
        data: formData,
        success: function (data) {
            var info = JSON.parse(data);
            $("#addLateFee").find("input[type=text]").val("");
            $("#addLateFee").find("input[type=radio]").prop("checked", false);
            $("#addLateFee").find("input[type=checkbox]").prop("checked", false);
           toastr.success(info.message);
        },
        cache: false,
        contentType: false,
        processData: false
    });
        }



});



$("#addChargeForm").validate({ 
    rules: {
        chargeCode: {
            required:true
        },
         frequency: {
            required:true
        },
        amount: {
            required:true,
            number:true
        },
        startDate: {
            required:true
        },
        endDate: {
            required:true
        },
        
     },
  submitHandler: function (e) {

      var tenant_id = $('.tenant_session_id').val();

       
       var form = $('#addChargeForm')[0]; 
       var formData = new FormData(form);
       formData.append('action','insertChargeFormData');
       formData.append('class','tenantAjax');
       formData.append('tenant_id',tenant_id);
       action = 'insertCharge';      
          $.ajax({

        url:'/tenantAjax',
        type: 'POST',
        data: formData,
        success: function (data) {
         

             toastr.success("Record added successfully");
             $('.chargeData').html(data);
             $('.chargeForm').hide();
            $("#addChargeButton").show();
            $("#amount").val('');
            $(".tax_chargeCode").val('');

        },
        cache: false,
        contentType: false,
        processData: false
    });
        }



});


$(function(){


    $('.move_in_date,.move_out_date').datepicker({dateFormat: jsDateFomat});
   var date = $.datepicker.formatDate(jsDateFomat, new Date());
   
   $(".calander").val(date);




    $(".calander").datepicker({
        dateFormat: jsDateFomat,
        setDate: $.datepicker.formatDate(jsDateFomat, new Date())
    });
});

$(".calander").val(jsDateFomat);


















$("#addTaxDetails").validate({ 
    rules: {
        tax_name: {
            required:true
        },
         tax_type: {
            required:true
        },
         tax_value: {
            required:true,
            number:true
        },
        tax_chargeCode: {
            required:true
        },
        
     },
  submitHandler: function (e) {

      var tenant_id = $('.tenant_session_id').val();

       
       var form = $('#addTaxDetails')[0]; 
       var formData = new FormData(form);
       formData.append('action','insertTaxDetails');
       formData.append('class','tenantAjax');
       formData.append('tenant_id',tenant_id);
      
          $.ajax({

        url:'/tenantAjax',
        type: 'POST',
        data: formData,
        success: function (data) {
        var info =  JSON.parse(data);
        $("#addTaxDetails").find("input[type=text]").val('');
        toastr.success(info.message);

        

          
        },
        cache: false,
        contentType: false,
        processData: false
    });
        }



});


  $(document).on("click",".addNote",function(){
           
             var clone = $(".chargeNoteHtml:first").clone();
            clone.find('input[type=text]').val(''); //harjinder
            $(".chargeNoteHtml").first().after(clone);
            $(".chargeNoteHtml:not(:eq(0))  .addNote").remove();
            $(".chargeNoteHtml:not(:eq(0))  .removeNote").show();

            
        });



  $(document).on("click",".removeNote",function(){
        var phoneRowLenght = $(".primary-tenant-phone-row");
        $(this).parents(".chargeNoteHtml").remove();
    });







$(document).on("click",".chargeNext",function(){
    var tenant_id = $('.tenant_session_id').val();
    var form = $('#addChargeNote')[0];
    var formData = new FormData(form);
    formData.append('action','insertChargeFileNote');
    formData.append('class','tenantAjax');
    formData.append('tenant_id',tenant_id);
      
    $.ajax({
        url:'/tenantAjax',
        type: 'POST',
        data: formData,
        success: function (response) {
            //var response = JSON.parse(response);
            //if(response.status == "success"){
                toastr.success('Records saved successfully');
                $('#file_library_uploads').html('');
                $('#file_library').val('');
                $(".content-data").html("");
                $(".content-data").load("/Tenantlisting/getTenantGeneratePage");
                setTimeout(function () {
                    initializeGeneratePage();
                },500);
            //}
        },
        cache: false,
        contentType: false,
        processData: false
    });

});
 

     
   $(document).on('click','#add_libraray_file',function(){
    $('#file_library').val('');
    $('#file_library').trigger('click');
});
var file_library = [];
$(document).on('change','#file_library',function(){
    file_library = [];
    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        if(size > 1030) {
            toastr.warning('Please select documents less than 1 mb!');
        } else {
            size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
            if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                file_library.push(value);
                var src = '';
                var reader = new FileReader();
                $('#file_library_uploads').html('');
                reader.onload = function (e) {
                    if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                        src = upload_url + 'company/images/excel.png';
                    } else if (type == 'application/pdf') {
                        src = upload_url + 'company/images/pdf.png';
                    } else if (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                        src = upload_url + 'company/images/word_doc_icon.jpg';
                    } else if (type == 'text/plain') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else if (type == 'text/xml') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else {
                        src = e.target.result;
                    }
                    $("#file_library_uploads").append(
                        '<div class="row" style="margin:20px">' +
                        '<div class="col-sm-12 img-upload-library-div">' +
                        '<div class="col-sm-3"><img class="img-upload-tab' + key + '" width=100 height=100 src=' + src + '></div>' +
                        '<div class="col-sm-3 show-library-list-imgs-name' + key + '">' + value['name'] + '</div>' +
                        '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                        '<div class="col-sm-3 show-library-list-imgs-size' + key + '">' + size + '</div>' +
                        '<div class="col-sm-3"><span id=' + key + ' class="delete_pro_img cursor"><button class="btn-warning">Delete</button></span></div></div></div>');
                };
                reader.readAsDataURL(value);
            } else {
                toastr.warning('Please select file with .xlsx | .pdf | .docx | .txt | .xml extension only!');
            }
        }
    });
});

$('#saveLibraryFiles').on('click',function(){
    var length = $('#file_library_uploads > div').length;
    if(length > 0) {
        var data = convertSerializeDatatoArray();
        var uploadform = new FormData();
        uploadform.append('class', 'propertyFilelibrary');
        uploadform.append('action', 'file_library');
        uploadform.append('property_id', property_unique_id);
        var count = file_library.length;
        $.each(file_library, function (key, value) {
             if(compareArray(value,data) == 'true'){
                 uploadform.append(key, value);
             }
            if(key+1 === count){
                saveLibraryFiles(uploadform);
            }
        });
    } else {

    }
});

$(document).on('click','.delete_pro_img',function(){
    $(this).parent().parent().parent('.row').remove();
});

$(document).on('click','#remove_library_file',function(){
    $('#file_library_uploads').html('');
    $('#file_library').val('');
});

function saveLibraryFiles(uploadform){
    $.ajax({
        type: 'post',
        url: '/property/file_library',
        data:uploadform,
        processData: false,
        contentType: false,
        success: function (response) {
            var response = JSON.parse(response);
            if(response.code == 200){
                $('#file_library_uploads').html('');
                $('#propertFileLibrary-table').trigger('reloadGrid');
                toastr.success('Files uploaded successfully.');
            } else if(response.code == 500){
                toastr.warning(response.message);
            } else {
                toastr.success('Error while uploading files.');
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function convertSerializeDatatoArray(){
    var newData = [];
    $(".fileLibraryInput").each(function( index ) {
        var name = $(this).val();
        var size = $(this).attr('data_id');
        newData.push({'name':name,'size':size});
    });
    return newData;
}

function compareArray(data,compare){
    for(var i =0;i < compare.length;i++){
        if(compare[i].name == data['name'] && compare[i].size == data['size']){
            return 'true';
        }
    }
    return 'false';
}

function isa_convert_bytes_to_specified(bytes, to) {
    var formulas =[];
    formulas['k']= (bytes / 1024).toFixed(1);
    formulas['M']= (bytes / 1048576).toFixed(1);
    formulas['G']= (bytes / 1073741824).toFixed(1);
    return formulas[to];
}

/**
 * jqGrid Intialization function
 * @param status
 */
/*jqGridFileLibrary('All');
function jqGridFileLibrary(status) {
    var table = 'property_file_uploads';
    var columns = ['Name','Preview','Action'];
    var select_column = ['Email','Delete'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'file_type',value:'2',condition:'='}];
    var columns_options = [
        {name:'Name',index:'file_name',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'file_extension',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
        {name:'Action',index:'select',width:430,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,title:false}
    ];
    var ignore_array = [];
    jQuery("#propertFileLibrary-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: 5,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of File Library",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}*/

function imageFormatter(cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var src = '';
        if (rowObject.Preview == 'xlsx') {
            src = upload_url + 'company/images/excel.png';
        } else if (rowObject.Preview == 'pdf') {
            src = upload_url + 'company/images/pdf.png';
        } else if (rowObject.Preview == 'docx') {
            src = upload_url + 'company/images/word_doc_icon.jpg';
        }else if (rowObject.Preview == 'txt') {
            src = upload_url + 'company/images/notepad.jpg';
        }
        return '<img class="img-upload-tab" width=100 height=100 src="' + src + '">';
    }
}

$(document).on('change', '#propertFileLibrary-table .select_options', function() {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    var row_num = $(this).parent().parent().index() ;
    if (opt == 'Email' || opt == 'EMAIL') {


    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/property/file_library',
                        data: {class: 'propertyFilelibrary', action: "deleteFile", id: id},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                }
                $('#propertFileLibrary-table').trigger('reloadGrid');
            }
        });
    }
});


$(document).on("click",".cancelBtn",function(){

    bootbox.confirm({
        message: "Do you want to cancel this action now ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                var base_url = window.location.origin;
              window.location.href=base_url+"/Tenantlisting/Tenantlisting";
            }
        }
    });

});





$(document).on("click",".editEndDate",function(){
 var data_id = $(this).attr('data-id');
 $('.endDate_'+data_id).html("<input type='text' name='editDate[]' class='addCalander editCalander'>");
 $(".editCalander").datepicker({
        dateFormat: jsDateFomat,
        setDate: $.datepicker.formatDate(jsDateFomat, new Date())
    });
});


$(document).on("click",".editAmount",function(){
 var data_id = $(this).attr('data-id');
 $('.endAmount_'+data_id).html("<input type='text' name='editAmount[]'>");
});


$(document).on("click",".editChargeInfo",function(){
       var tenant_id = $('.tenant_session_id').val();
       var form = $('#editChargeDateAmount')[0]; 
       var formData = new FormData(form);
       formData.append('action','editChargeInfo');
       formData.append('class','tenantAjax');
       formData.append('tenant_id',tenant_id);
      
     action = 'insert';
        
       $.ajax({

        url:'/tenantAjax',
        type: 'POST',
        data: formData,
        success: function (data) {
            toastr.success("Charges applied successfully!");
         $('.chargeData').html(data);

          
        },
        cache: false,
        contentType: false,
        processData: false
    });


});


$(document).on("click","#addChargeButton",function(){
    $(".chargeForm").fadeIn(); 
    $("#addChargeButton").hide();


});


$(document).on("change",".tax_type",function(){
var value = $(this).val();
if(value=="P")
{
 $(".pSign").html("(%)");
}
else
{
$(".pSign").html("(USh)");
}

});







          






    </script>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->