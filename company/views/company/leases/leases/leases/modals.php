<div class="container">
    <div class="modal fade" id="selectProperty" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">General Information</h4>
                </div>
                <div class="modal-body pad-none">
                    <form method="post" id="addPropertyForm">
                        <input type="hidden" class="propertyIddata" value="">
                        <div class="panel-body" style="border-color: transparent;">
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-3">
                                        <label>Property ID <em class="red-star">*</em> </label>
                                        <input name="property_id" placeholder="Auto Generated" id="autoGenProperty_id" class="form-control" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Property Name <em class="red-star">*</em></label>
                                        <input name="property_name" id="generalPropertyName" placeholder="Eg: The Fairmont Waterfront" class="form-control" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Legal Name</label>
                                        <input name="legal_name" id="generalLegalName" placeholder="Eg: The Fairmont Waterfront" class="form-control" type="text">
                                    </div>
                                    <div class="col-sm-3 add-category">
                                        <label>Portfolio Name <em class="red-star">*</em> <i class="fa fa-plus-circle NewportfolioPopup" id=""></i></label>
                                        <span id="dynamic_portfolio">
                                            <select name="portfolio_id" class='form-control'>
                                                <option value="">Select</option>
                                            </select>
                                        </span>
                                        <div class="add-popup" id="NewportfolioPopup">
                                            <h4>Add New Portfolio</h4>
                                            <div class="add-popup-body">

                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>Portfolio ID<em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePortfoio" type="text" data_required="true" data_max="20" name="@portfolio_id" id="portfolio_id">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label> Portfolio Name <em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePortfoio" type="text" data_required="true" data_max="50" name="@portfolio_name" id="portfolio_name" placeholder="Add New Portfolio Name">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="btn-outer">
                                                        <button type="button" id="NewportfolioPopupSave" class="blue-btn" value="Save">Save</button>
                                                        <button type="button" class="grey-btn" value="Cancel">Cancel</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Zip / Postal Code</label>
                                        <input name="zipcode" value="10001" id="propertyZipcode" class="form-control p-number-format" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Country</label>
                                        <input name="country" id="propertyCountry_name" class="form-control" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>State / Province</label>
                                        <input name="state" id="propertyState_name" class="form-control" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>City</label>
                                        <input name="city" id="propertyCity_name" class="form-control" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Street Address 1</label>
                                        <input name="address1" placeholder="Eg: Street address 1" class="form-control" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Street Address 2</label>
                                        <input name="address2" placeholder="Eg: Street address 2" class="form-control" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Street Address 3</label>
                                        <input name="address3" placeholder="Eg: Street address 3" class="form-control" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Street Address 4</label>
                                        <input name="address4" placeholder="Eg: Street address 4" class="form-control" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Manager Name <i class="fa fa-plus-circle" id="Newmanager"></i></label>
                                        <span id="dynamic_manager">
                                             <select name="manager_id[]" id="select_mangers_options" class="form-control select_manager" multiple="multiple">
                                            </select>
                                        </span>
                                        <div class="add-popup" id="NewmanagerPopup">
                                            <h4>Add New Manager</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>First Name<em class="red-star">*</em></label>
                                                        <input class="form-control customValidateManager" type="text" data_required="true" data_max="30" name="@first_name">
                                                        <span class="customError required" aria-required="true"></span>
                                                        <span class="first_nameErr error"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label> Last Name <em class="red-star">*</em></label>
                                                        <input class="form-control customValidateManager" type="text" data_required="true" data_max="30" name="@last_name">
                                                        <span class="customError required" aria-required="true"></span>
                                                        <span class="last_nameErr error"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label> Email <em class="red-star">*</em></label>
                                                        <input class="form-control customValidateManager" type="text" data_required="true" data_max="50" data_email="true" name="@email">
                                                        <span class="customError required" aria-required="true"></span>
                                                        <span class="emailErr error"></span>
                                                    </div>
                                                    <div class="btn-outer">
                                                        <button type="button" id="NewmanagerPopupSave" class="blue-btn">Save</button>
                                                        <button type="button" class="grey-btn" value="Cancel">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Property Price</label>
                                        <input name="property_price" placeholder="547895" class="form-control p-number-format" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Attach Group <i class="fa fa-plus-circle NewattachPopup"></i></label>
                                        <span id="dynamic_groups">
                                            <select name="attach_groups[]" class="form-control attach_groups" multiple="multiple">
                                            </select>
                                        </span>
                                        <div class="add-popup" id="NewattachPopup">
                                            <h4>Add New Property Group</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>Group Name<em class="red-star">*</em></label>
                                                        <input class="form-control customValidateGroup" type="text" data_required="true" data_max="150" name="@group_name">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label> Descrition <em class="red-star">*</em></label>
                                                        <input class="form-control customValidateGroup" type="text" data_required="true" data_max="150" name="@description">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="btn-outer">
                                                        <button type="button" class="blue-btn" id="NewattachPopupSave">Save</button>
                                                        <button type="button" class="grey-btn" value="Cancel">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Property Type <em class="red-star">*</em> <i class="fa fa-plus-circle property_type_options"></i></label>
                                        <span id="dynamic_property_type">
                                            <select name="property_type" class="form-control property_type" >
                                                <option value="0">Select</option>
                                            </select>
                                        </span>
                                        <div class="add-popup" id="NewtypePopup" style="display: block;">
                                            <h4>Add New Property Type</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>Property Type<em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePropertyType" data_required="true" data_max="150" type="text" name="@property_type">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label> Description <em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePropertyType" data_required="true" data_max="200" type="text" name="@description">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="btn-outer">
                                                        <button type="button" class="blue-btn" id="NewtypePopupSave">Save</button>
                                                        <button type="button" class="grey-btn" value="Cancel">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Property Style <i class="fa fa-plus-circle Newprofile"></i></label>
                                        <span id="dynamic_property_style">
                                             <select name="property_style" class="form-control property_style" id="property_style_options" >
                                                <option value="0">Select</option>
                                            </select>
                                        </span>
                                        <div class="add-popup" id="NewpstylePopup">
                                            <h4>Add New Property Style</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>Property Style <em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePropertyStyle" data_required="true" data_max="150" type="text" name="@property_style">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label>Description <em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePropertyStyle capital" data_required="true" data_max="200" type="text" name="@description">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="btn-outer text-right">
                                                        <button type="button" id="NewpstylePopupSave" class="blue-btn">Save</button>
                                                        <button type="button" class="grey-btn" value="Cancel">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Property Sub-Type <i class="fa fa-plus-circle Newsubtype"></i></label>
                                        <span id="dynamic_property_subtype">
                                            <select name="property_subtype" class="form-control property_subtype" id="property_subtype_options">
                                                <option value="0">Select</option>
                                            </select>
                                        </span>
                                        <div class="add-popup" id="NewsubtypePopup">
                                            <h4>Add New Property Sub-Type</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>Property Sub-Type<em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePropertySubType" data_required="true" data_max="150" type="text" name="@property_subtype">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label> Description <em class="red-star">*</em></label>
                                                        <input class="form-control customValidatePropertySubType" data_required="true" data_max="200" type="text" name="@description">
                                                        <span class="customError required" aria-required="true"></span>
                                                    </div>
                                                    <div class="btn-outer text-right">
                                                        <button type="button" class="blue-btn" id="NewsubtypePopupSave">Save</button>

                                                        <button type="button" class="grey-btn" value="Cancel">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Year Built</label>
                                        <!-- <input name="property_year" class="form-control" type="text"> -->

                                        <select name="property_year" class="form-control">
                                            <option value="0">(Select Year Built)</option>
                                            <?php
                                            for($i = 1990; $i <= 2018; $i++){
                                                echo '<option value="'.$i.'">'.$i.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Sq.ft</label>
                                        <input name="property_squareFootage" placeholder="Eg: 100" class="form-control p-number-format" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Number Of Buildings <em class="red-star">*</em> </label>
                                        <input name="no_of_buildings" placeholder="EG: 1" value="1" class="form-control p-number-format" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Number Of Units <em class="red-star">*</em> </label>
                                        <input name="no_of_units" placeholder="EG: 1" value="1" class="form-control p-number-format" type="text">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Smoking Allowed</label>
                                        <select name="smoking_allowed" class='form-control'>
                                            <option value='0' selected>No</option>
                                            <option value='1'>Yes</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Pet Friendly</label>
                                        <span id="dynamic_pet_condition">
                                            <select class="form-control" id="PetsAllowed" name="pet_friendly">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </span>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="">
                                                <div class="col-sm-6">
                                                    <label>Amenities <i class="fa fa-plus-circle Newamenities"></i></label>
                                                    <div class="check-outer amenties_box" style="height: 80px;overflow: auto;width: 100%;">
                                                        <div class="check-outer-col2 col-sm-4 col-md-4" style="padding: 0">
                                                            <input class="clsAmenityCheckboxMain all check-all-amenities" type="checkbox" >Select All</div>
                                                        <div id="dynamic_amenity"></div>
                                                    </div>
                                                    <div class="add-popup" id="NewamenitiesPopup">
                                                        <h4>Add New Amenity</h4>
                                                        <div class="add-popup-body">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12">
                                                                    <label>New Amenity Code<em class="red-star">*</em></label>
                                                                    <input class="form-control customValidateAmenities" data_required="true" data_max="15" type="text" name="@code">
                                                                    <span class="customError required" aria-required="true"></span>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <label> Amenity Name <em class="red-star">*</em></label>
                                                                    <input class="form-control customValidateAmenities" data_required="true" data_max="50" type="text" name="@name">
                                                                    <span class="customError required" aria-required="true"></span>
                                                                </div>
                                                                <div class="btn-outer">
                                                                    <button type="button" class="blue-btn" id="NewamenitiesPopupSave">Save</button>
                                                                    <button type="button" class="grey-btn" value="Cancel">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label>Description</label>
                                                    <textarea name="description" class="form-control" type="text" rows="4" maxlength="500"></textarea>
                                                </div>
                                            </div>
                                            <div class="mb-15 checkbox-outer" style="margin-top: 20px;">

                                                    <input name="default_building_unit" cstyle="margin-right: 5px;" type="checkbox" class="NoBuilding">

                                                <label>Property With No Building/Unit</label>
                                            </div>
                                            <div class="col-sm-12 details" style="display: none;">
                                                <label>Details</label>
                                                <div class="check-outer" type="text">
                                                    <div class="col-sm-3">
                                                        <label>Select Unit Type <em class="red-star">*</em> <i class="fa fa-plus-circle AddNewUnitTypeModalPlus2"></i></label>
                                                        <span id="dynamic_unit_type">
                                                            <select class="form-control unit_type" id="unit_type" name="unit_type">
                                                                <option value="">Select</option>
                                                            </select>
                                                        </span>
                                                        <div class="add-popup" id="NewunitPopup">
                                                            <h4>Add New Unit Type</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>Unit Type<em class="red-star">*</em></label>
                                                                        <input class="form-control customValidateUnitType" data_required="true" data_max="150" type="text" name="@unit_type">
                                                                        <span class="customError required" aria-required="true"></span>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <label>Description</label>
                                                                        <input class="form-control customValidateUnitType" type="text" maxlength="500" name="@description">
                                                                        <span class="customError required" aria-required="true"></span>
                                                                    </div>
                                                                    <div class="btn-outer">
                                                                        <button type="button" class="blue-btn" id="NewunitPopupSave" value="Save">Save</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Base Rent
                                                            <em class="red-star">*</em> </label>
                                                        <input placeholder="Eg: 2000" class="form-control p-number-format" type="text" name="base_rent" >
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Market Rent
                                                            <em class="red-star">*</em> </label>
                                                        <input placeholder="Eg: 2000" class="form-control p-number-format" type="text" name="market_rent">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Security Deposit
                                                            <em class="red-star">*</em> </label>
                                                        <input placeholder="Eg: 2000" class="form-control p-number-format" type="text" name="security_deposit" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="btn-outer" style="margin-left: 15px;">
                                                <button type="submit" class="blue-btn" value="Save" id="AddGeneralInformationButton" placeholder="Save">Save</button>
                                                <button type="button" data-dismiss="modal" id="AddGeneralInformationCancel" class="propertyCancel grey-btn">Cancel</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- </div>
                </div> -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectBuilding" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Building</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <form method="post" action="" id="addBuildingForm" enctype="multipart/form-data">
                            <input type="hidden" name="tenantbuildingpopup" value="1">
                            <div class="panel-body pad-none" >
                                <div class="col-sm-12 pad-none">
                                    <div class="form-outer">
                                        <div class="div-full pad-none">
                                            <div class="col-sm-3">
                                                <label>Property <em class="red-star">*</em> </label>
                                                <select name="property_id" class="form-control">
                                                    <option value="">Select Property</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Building ID <em class="red-star">*</em> </label>
                                                <input name="building_id" class="form-control" value='' type="text">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Legal Name</label>
                                                <input name="legal_name" id="legal_name" placeholder="Eg: The Fairmont Waterfront" class="form-control" type="text">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Building Name <em class="red-star">*</em></label>
                                                <input name="building_name" id="building_name" placeholder="Eg: The Fairmont Waterfront" class="form-control" type="text" value="">
                                            </div>
                                        </div>
                                        <div class="div-full pad-none">
                                            <div class="col-sm-3">
                                                <label>Number Of Units <em class="red-star">*</em> </label>
                                                <input id="no_of_units" name="no_of_units"class="form-control" type="text" value="1">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Building Address <em class="red-star">*</em></label>
                                                <input name="address" placeholder="Eg: 8 Avenue" class="form-control" type="text">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Smoking Allowed</label>
                                                <select name="smoking_allowed" class='form-control' value="1" readonly="true">
                                                    <option value=''>Select</option>
                                                    <option value='1'>Yes</option>
                                                    <option value='0'>No</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Pet Friendly</label>
                                                <select class="form-control" id="PetsAllowed" name="pet_friendly">
                                                    <option value=''>Select</option>
                                                    <option value='1'>Yes</option>
                                                    <option value='0'>No</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="div-full">
                                            <div class="row">
                                                <div class="col-sm-6 school-duistrict">
                                                    <label>Amenities <i class="fa fa-plus-circle" data-toggle="modal" data-target="#AddNewAmenityModal" aria-hidden="true"></i></label>
                                                    <div class="check-outer" style="height: 110px;overflow: auto;width: 100%;" id="amenities_checkbox">
                                                        <div class="check-outer-col2 col-sm-4 col-md-4" style="min-height:40px; padding: 0;">
                                                            <input class="clsAmenityCheckboxMain" id="select_all" type="checkbox" > Select All
                                                        </div>
                                                        <div id="dynamic_amenity" class="dynamic_amenity"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label>Building Description</label>
                                                    <textarea name="description" placeholder="This is a family friendly building with onsite laundary. Suites are newly renovated with balconies" class="form-control" type="text" rows="4" maxlength="500"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="div-full row">

                                            <div class="checkbox-outer school-duistrict">

                                                    <input name="default_building_unit" cstyle="margin-right: 5px;" type="checkbox" class="NoBuilding">

                                                <label>No seprate unit</label>
                                            </div>
                                            <div class="col-sm-10 details" style="display: none;">
                                                <label>Details</label>
                                                <div class="check-outer" type="text">
                                                    <div class="col-sm-3">
                                                        <label>Select Unit Type <em class="red-star">*</em> <i class="fa fa-plus-circle" data-toggle="modal" data-target="#AddNewUnitTypeModal" aria-hidden="true"></i></label>
                                                        <span id="dynamic_unit_type">
                                                    <select class="form-control" id="" name="unit_type">
                                                        <option value="">Select</option>
                                                    </select>
                                                </span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Base Rent
                                                            <em class="red-star">*</em> </label>
                                                        <input placeholder="Eg: 2000" class="form-control" type="text" name="base_rent" >
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Market Rent
                                                            <em class="red-star">*</em> </label>
                                                        <input placeholder="Eg: 2000" class="form-control" type="text" name="market_rent">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Security Deposit
                                                            <em class="red-star">*</em> </label>
                                                        <input placeholder="Eg: 2000" class="form-control" type="text" name="security_deposit" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="div-full">
                                                <div class="btn-outer" >
                                                    <a class="blue-btn savebuildingpopup">Save</a>
                                                    <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectUnit" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Unit</h4>
                </div>
                <div class="modal-body">
                    <form id="addUnitForm" enctype="multipart/form-data">
                        <div class="panel-body pad-none">
                            <div class="row">
                                <div class="form-outer form-data unit_form_data" id="input-form" style="border: none;">
                                    <div class=" blue-bg-outer unit-blueouter">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Select Property<em class="red-star">*</em></label>
                                                <span>
                                                    <select id="property_id" name="property_id" class="form-control" style="cursor: pointer;">
                                                        <option value="">Select</option>
                                                    </select>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Select Building<em class="red-star">*</em></label>
                                                <span>
                                                    <select id="ddlBuildings" name="building_id[]" class="form-control" style="cursor: pointer;">
                                                        <option value="">Select</option>
                                                    </select>
                                                    <span class="required_star" id="building_id_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Floor No.<em class="red-star">*</em></label>
                                                <span><input type="text" id="txtFloors" value="" name="floor_no[]" placeholder="Eg: 1" maxlength="3" class="form-control" spellcheck="true">
                                                    <span class="required_star" id="floor_no_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Unit Prefix</label>
                                                <span>
                                                    <input type="text" id="txtPrefix" name="unit_prefix[]" placeholder="Eg: A" maxlength="10" class="form-control" spellcheck="true">
                                                     <span class="required_star" id="unit_prefix_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Unit Number<em class="red-star">*</em></label>
                                                <span>
                                                    <input type="text" id="txtUnitNumber" name="unit_no[]" maxlength="7" value="" placeholder="Eg: 100" class="form-control" spellcheck="true">
                                                    <span class="required_star" id="unit_no_errmain"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row p-0">
                                        <div class="div-full pad-none">
                                            <div class="col-sm-3">
                                                <label>Select Unit Type <em class="red-star">*</em>
                                                    <i class="fa fa-plus-circle" data-toggle="modal" data-target="#AddNewUnitModal" aria-hidden="true"></i></label>
                                                <span>
                                                    <select name="unitTypeID" class="form-control" id="ddlUnitType2" name="unit_type_id" class="" style="cursor: pointer;">
                                                        <option value="default">Select Unit Type</option>
                                                    </select>
                                                    <span class="required_star" id="unit_type_id_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label id="lblSquareFootage">Total Square Footage</label>
                                                <span>
                                                    <input id="txtTotalSqFootage" type="number" placeholder="Eg: 200.00" name="square_ft" maxlength="7" class="form-control" spellcheck="true">
                                                  <span class="required_star" id="square_ft_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>No. Of Bedrooms<em class="red-star">*</em></label>
                                                <span>
                                                    <select name="bedrooms_no" id="ddlBedRoom" class="form-control" style="cursor: pointer; background-color: rgb(255, 255, 255);">
                                                        <?php
                                                        for($i=1; $i<=100; $i++) {
                                                            echo '<option value="'.$i.'" text="'.$i.'">'.$i.'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>No. Of Bathrooms<em class="red-star">*</em></label>
                                                <span>
                                                    <select name="bathrooms_no" id="ddlBathRoom" class="form-control" style="cursor: pointer; background-color: rgb(255, 255, 255);"><option value="1" text="1">1</option><option value="2" text="1½">1½</option><option value="3" text="2">2</option><option value="4" text="2½">2½</option><option value="5" text="3">3</option><option value="6" text="3½">3½</option><option value="7" text="4">4</option><option value="8" text="4½">4½</option><option value="9" text="5">5</option><option value="10" text="5½">5½</option><option value="11" text="6">6</option><option value="12" text="6½">6½</option><option value="13" text="7">7</option><option value="14" text="7½">7½</option><option value="15" text="8">8</option><option value="16" text="8½">8½</option><option value="17" text="9">9</option><option value="18" text="9½">9½</option><option value="19" text="10">10</option><option value="20" text="10½">10½</option><option value="21" text="11">11</option><option value="22" text="11½">11½</option><option value="23" text="12">12</option><option value="24" text="12½">12½</option><option value="25" text="13">13</option><option value="26" text="13½">13½</option><option value="27" text="14">14</option><option value="28" text="14½">14½</option><option value="29" text="15">15</option><option value="30" text="15½">15½</option><option value="31" text="16">16</option><option value="32" text="16½">16½</option><option value="33" text="17">17</option><option value="34" text="17½">17½</option><option value="35" text="18">18</option><option value="36" text="18½">18½</option><option value="37" text="19">19</option><option value="38" text="19½">19½</option><option value="39" text="20">20</option><option value="40" text="20½">20½</option><option value="41" text="21">21</option><option value="42" text="21½">21½</option><option value="43" text="22">22</option><option value="44" text="22½">22½</option><option value="45" text="23">23</option><option value="46" text="23½">23½</option><option value="47" text="24">24</option><option value="48" text="24½">24½</option><option value="49" text="25">25</option><option value="50" text="25½">25½</option><option value="51" text="26">26</option><option value="52" text="26½">26½</option><option value="53" text="27">27</option><option value="54" text="27½">27½</option><option value="55" text="28">28</option><option value="56" text="28½">28½</option><option value="57" text="29">29</option><option value="58" text="29½">29½</option><option value="59" text="30">30</option><option value="60" text="30½">30½</option><option value="61" text="31">31</option><option value="62" text="31½">31½</option><option value="63" text="32">32</option><option value="64" text="32½">32½</option><option value="65" text="33">33</option><option value="66" text="33½">33½</option><option value="67" text="34">34</option><option value="68" text="34½">34½</option><option value="69" text="35">35</option><option value="70" text="35½">35½</option><option value="71" text="36">36</option><option value="72" text="36½">36½</option><option value="73" text="37">37</option><option value="74" text="37½">37½</option><option value="75" text="38">38</option><option value="76" text="38½">38½</option><option value="77" text="39">39</option><option value="78" text="39½">39½</option><option value="79" text="40">40</option><option value="80" text="40½">40½</option><option value="81" text="41">41</option><option value="82" text="41½">41½</option><option value="83" text="42">42</option><option value="84" text="42½">42½</option><option value="85" text="43">43</option><option value="86" text="43½">43½</option><option value="87" text="44">44</option><option value="88" text="44½">44½</option><option value="89" text="45">45</option><option value="90" text="45½">45½</option><option value="91" text="46">46</option><option value="92" text="46½">46½</option><option value="93" text="47">47</option><option value="94" text="47½">47½</option><option value="95" text="48">48</option><option value="96" text="48½">48½</option><option value="97" text="49">49</option><option value="98" text="49½">49½</option><option value="99" text="50">50</option><option value="100" text="50½">50½</option><option value="101" text="51">51</option><option value="102" text="51½">51½</option><option value="103" text="52">52</option><option value="104" text="52½">52½</option><option value="105" text="53">53</option><option value="106" text="53½">53½</option><option value="107" text="54">54</option><option value="108" text="54½">54½</option><option value="109" text="55">55</option><option value="110" text="55½">55½</option><option value="111" text="56">56</option><option value="112" text="56½">56½</option><option value="113" text="57">57</option><option value="114" text="57½">57½</option><option value="115" text="58">58</option><option value="116" text="58½">58½</option><option value="117" text="59">59</option><option value="118" text="59½">59½</option><option value="119" text="60">60</option><option value="120" text="60½">60½</option><option value="121" text="61">61</option><option value="122" text="61½">61½</option><option value="123" text="62">62</option><option value="124" text="62½">62½</option><option value="125" text="63">63</option><option value="126" text="63½">63½</option><option value="127" text="64">64</option><option value="128" text="64½">64½</option><option value="129" text="65">65</option><option value="130" text="65½">65½</option><option value="131" text="66">66</option><option value="132" text="66½">66½</option><option value="133" text="67">67</option><option value="134" text="67½">67½</option><option value="135" text="68">68</option><option value="136" text="68½">68½</option><option value="137" text="69">69</option><option value="138" text="69½">69½</option><option value="139" text="70">70</option><option value="140" text="70½">70½</option><option value="141" text="71">71</option><option value="142" text="71½">71½</option><option value="143" text="72">72</option><option value="144" text="72½">72½</option><option value="145" text="73">73</option><option value="146" text="73½">73½</option><option value="147" text="74">74</option><option value="148" text="74½">74½</option><option value="149" text="75">75</option><option value="150" text="75½">75½</option><option value="151" text="76">76</option><option value="152" text="76½">76½</option><option value="153" text="77">77</option><option value="154" text="77½">77½</option><option value="155" text="78">78</option><option value="156" text="78½">78½</option><option value="157" text="79">79</option><option value="158" text="79½">79½</option><option value="159" text="80">80</option><option value="160" text="80½">80½</option><option value="161" text="81">81</option><option value="162" text="81½">81½</option><option value="163" text="82">82</option><option value="164" text="82½">82½</option><option value="165" text="83">83</option><option value="166" text="83½">83½</option><option value="167" text="84">84</option><option value="168" text="84½">84½</option><option value="169" text="85">85</option><option value="170" text="85½">85½</option><option value="171" text="86">86</option><option value="172" text="86½">86½</option><option value="173" text="87">87</option><option value="174" text="87½">87½</option><option value="175" text="88">88</option><option value="176" text="88½">88½</option><option value="177" text="89">89</option><option value="178" text="89½">89½</option><option value="179" text="90">90</option><option value="180" text="90½">90½</option><option value="181" text="91">91</option><option value="182" text="91½">91½</option><option value="183" text="92">92</option><option value="184" text="92½">92½</option><option value="185" text="93">93</option><option value="186" text="93½">93½</option><option value="187" text="94">94</option><option value="188" text="94½">94½</option><option value="189" text="95">95</option><option value="190" text="95½">95½</option><option value="191" text="96">96</option><option value="192" text="96½">96½</option><option value="193" text="97">97</option><option value="194" text="97½">97½</option><option value="195" text="98">98</option><option value="196" text="98½">98½</option><option value="197" text="99">99</option><option value="198" text="99½">99½</option><option value="199" text="100">100</option><option value="200" text="100½">100½</option><option value="201" text="Other">Other</option></select>
                                                    <span class="required_star" id="bathrooms_no_errmain"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="div-full pad-none">
                                            <div class="col-sm-3">
                                                <label id="changeLblAddBaseRentCurrency">Base Rent
                                                    <em class="red-star">*</em></label>
                                                <span>
                                                    <input type='text' id="txtBaseRent" placeholder="Eg: 200.00" name="baseRent" step=".01" maxlength="8" class="form-control amount number_only" spellcheck="true">
                                                    <span class="required_star" id="base_rent_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label id="changeLblAddMarketRentCurrency">Market Rent
                                                    <em class="red-star">*</em></label>
                                                <span>
                                                    <input type="text" id="txtMarketRent" placeholder="Eg: 5000" name="market_rent" maxlength="8" class="form-control amount number_only" spellcheck="true" >
                                                    <span class="required_star" id="market_rent_errmain"></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label id="changeLblAddSdCurrency">Security Deposit
                                                    <em class="red-star">*</em></label>
                                                <span>
                                                    <input type="text" placeholder="Eg: 200.00" id="txtSecurityDeposit" name="securityDeposit" maxlength="8" class="form-control amount number_only" spellcheck="true">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Smoking Allowed</label>
                                            <span>
                                                <select placeholder="Eg: Yes" class="form-control" id="NonSmokingUnit" name="smoking_allowed">
                                                    <option value="">Select</option>
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </span>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Pet Friendly</label>
                                            <span>
                                                <select placeholder="Eg: Yes" class="form-control" id="pet_friendly_id" name="pet_friendly_id">
                                                    <option value="">Select</option>
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </span>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Status</label>
                                            <span>
                                                <select name="building_unit_status" id="ddlStatus" class="form-control" style="cursor: pointer;"><option value="1">Vacant Available</option><option value="2">Unrentable</option><option value="4">Occupied</option><option value="5">Notice Available</option><option value="6">Vacant Rented</option><option value="7">Notice Rented</option><option value="8">Under Make Ready</option></select>
                                            </span>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Add Notes</label>
                                            <span>
                                                <textarea id="txtNotes" placeholder="Eg: Add Notes" name="building_unit_notes" maxlength="5000" class="textarea form-control " rows="3" cols="40" style="resize: none; height: 100px;" spellcheck="true"></textarea>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 school-duistrict">
                                            <label>Amenities<i class="fa fa-plus-circle" data-toggle="modal" data-target="#AddNewAmenityModal" aria-hidden="true"></i></label>
                                            <span>
                                                <div id="AmenityList" class="check-outer" style="height: 94px; overflow: auto; width: 98%;">
                                                    <div class="check-outer-col2 cols-sm-4 col-md-4" style="padding: 0">
                                                        <input class="clsAmenityCheckboxMain" id="select_all" type="checkbox" >Select All
                                                    </div>
                                                    <div id="dynamic_amenity" class="dynamic_amenity">
                                                    </div>
                                                </div>
                                            </span>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Description</label>
                                            <span>
                                                <textarea id="txtDescription" name="building_description" class="textarea4 " maxlength="500" cols="40" rows="5" style="resize: none;" spellcheck="true"></textarea>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="btn-outer">
                                        <a class="blue-btn saveunitpopup" value="Save">Save</a>
                                        <a id="addPropertyTypeCancel" class="grey-btn" value="Cancel" data-dismiss="modal">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addprotfolio" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Portfolio</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Portfolio ID<span class="required"> * </span></label>
                                    <input class="form-control" type="text" name="" placeholder="Auto Generated">
                                </div>
                                <div class="col-sm-12">
                                    <label>Portfolio Name<span class="required"> * </span></label>
                                    <input class="form-control" type="text" name="" placeholder="Add New Portfolio Name">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <a class="blue-btn" id=''>Save</a>
                                <a class="black-btn" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="tenantprintenvelope" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <input type="button" class="printenvelopbtn blue-btn" value="Print">
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <div class="col-sm-4">
                            <div class="col-sm-12 cinfo"></div>
                        </div>
                    </div>
                    <div class="col-sm-12"><p>&nbsp;</p></div>
                    <div class="col-sm-12">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4">
                            <div class="col-sm-12 tinfo"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectContact" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Contact Information</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <form name="" method="post">
                            @csrf
                            <div class="form-outer">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Salutation</label>
                                        <select name="" class="form-control">
                                            <option value="0">Select</option>
                                            <option value="1">Dr.</option>
                                            <option value="2">Mr.</option>
                                            <option value="3">Mrs.</option>
                                            <option value="4">Mr. &amp; Mrs.</option>
                                            <option value="5">Ms.</option>
                                            <option value="6">Sir.</option>
                                            <option value="7">Madam</option>
                                            <option value="8">Brother</option>
                                            <option value="9">Sister</option>
                                            <option value="10">Father</option>
                                            <option value="11">Mother</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>First Name<span class="required"> * </span></label>
                                        <input class="form-control" type="text" placeholder="First Name">
                                    </div>
                                    <div class="col-sm-1">
                                        <label>MI</label>
                                        <input class="form-control" type="text" name="" placeholder="MI" maxlength="1">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Last Name<span class="required"> * </span></label>
                                        <input class="form-control" type="text" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Maiden Name</label>
                                        <input class="form-control" type="text" placeholder="Maiden Name">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Nick Name</label>
                                        <input class="form-control" type="text" placeholder="Nick Name">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Gender</label>
                                        <select name="" class="form-control">
                                            <option value="0">Select</option>
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                            <option value="3">Male/Female</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Entity/Company Name</label>
                                        <input class="form-control" type="text" name="" placeholder="Company">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Zip / Postal Code</label>
                                        <input type="text" name="" class="form-control" placeholder="Ex: 85301">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Country</label>
                                        <input type="text" name="" class="form-control" placeholder="Country">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>State / Province</label>
                                        <input type="text" name="" class="form-control" placeholder="State">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>City</label>
                                        <input type="text" name="" class="form-control" placeholder="City">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Address1</label>
                                        <input type="text" name="" class="form-control" placeholder="Ex: Street Address 1">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Address2</label>
                                        <input type="text" name="" class="form-control" placeholder="Ex: Street Address 2">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Address3</label>
                                        <input type="text" name="" class="form-control" placeholder="Ex: Street Address 3">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Address4</label>
                                        <input type="text" name="" class="form-control" placeholder="Ex: Street Address 4">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Email</label>
                                        <input type="text" name="" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <div class="btn-outer">
                                    <a class="blue-btn" id=''>Save</a>
                                    <a class="black-btn cancelbtn">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addmanager1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Manager</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>First Name<span class="required"> * </span></label>
                                    <input class="form-control" type="text" name="" placeholder="First Name">
                                </div>
                                <div class="col-sm-12">
                                    <label>Last Name</label>
                                    <input class="form-control" type="text" name="" placeholder="Last Name">
                                </div>
                                <div class="col-sm-12">
                                    <label>Email<span class="required"> * </span></label>
                                    <input class="form-control" type="text" name="" placeholder="Email">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <a class="blue-btn" id=''>Save</a>
                                <a class="black-btn" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showvehiclesimages" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"></h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="row">
                            <div class="col-sm-12"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showhoasimages" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"></h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="row">
                            <div class="col-sm-12"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showpetsimages" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"></h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="row">
                            <div class="col-sm-12"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showanimalsimages" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"></h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="row">
                            <div class="col-sm-12"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showvehiclesimagesbig" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Image Preview</h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="col-sm-12">
                            <img src="" class="showbigvehicle" style="width: 100%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showhoasimagesbig" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Image Preview</h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="col-sm-12">
                            <img src="" class="showbighoa" style="width: 100%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showpetsimagesbig" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Image Preview</h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="col-sm-12">
                            <img src="" class="showbigpet" style="width: 100%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="showanimalsimagesbig" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Image Preview</h4></div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="col-sm-12">
                            <img src="" class="showbiganimal" style="width: 100%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addgroup1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Property Group</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Group Name<span class="required"> * </span></label>
                                    <input class="form-control" type="text" name="" placeholder="Add New Group Name">
                                </div>
                                <div class="col-sm-12">
                                    <label>Description</label>
                                    <input class="form-control" type="text" name="" placeholder="Description">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="black-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addpropertytype1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Property Type</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Property Type<span class="required"> * </span></label>
                                    <input class="form-control" type="text" name="" placeholder="Add New Property Type">
                                </div>
                                <div class="col-sm-12">
                                    <label>Description</label>
                                    <input class="form-control" type="text" name="" placeholder="Description">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="black-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addpropertytype1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Property Style</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Property Style<span class="required"> * </span></label>
                                    <input class="form-control" type="text" name="" placeholder="Property Style Name">
                                </div>
                                <div class="col-sm-12">
                                    <label>Description</label>
                                    <input class="form-control" type="text" name="" placeholder="Description">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="black-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addpropertystyle1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Property Style</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Property Style<span class="required"> * </span></label>
                                    <input class="form-control" type="text" name="" placeholder="Property Style Name">
                                </div>
                                <div class="col-sm-12">
                                    <label>Description</label>
                                    <input class="form-control" type="text" name="" placeholder="Description">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="black-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addpropertysubtype1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Property Sub-Type</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Property Sub-Type<span class="required"> * </span></label>
                                    <input class="form-control" type="text" name="" placeholder="Add New Property Sub-Type">
                                </div>
                                <div class="col-sm-12">
                                    <label>Description</label>
                                    <input class="form-control" type="text" name="" placeholder="Description">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="black-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addamenities1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Amenity</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>New Amenity Code</label>
                                    <input class="form-control" type="text" name="" placeholder="Ex: 12345 (optional)">
                                </div>
                                <div class="col-sm-12">
                                    <label>Amenity Name <span class="required"> * </span></label>
                                    <input class="form-control" type="text" name="" placeholder="Add New Amenity Name">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="black-btn cancelbtn">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addunittype1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Unit Type</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Unit Type <span class="required"> * </span></label>
                                    <input class="form-control" type="text" name="" placeholder="Ex: 12345 (optional)">
                                </div>
                                <div class="col-sm-12">
                                    <label>Description</label>
                                    <input class="form-control" type="text" name="" placeholder="Description">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="black-btn cancelbtn">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyReferralResource" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Referral Source</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-outer" style="float: none;">
                            <label>New Referral Source <em class="red-star">*</em></label>
                            <input class="form-control reff_source" type="text" placeholder="New Referral Source">
                            <span class="red-star" id="reff_source"></span>
                            <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                                <a class="blue-btn add_single" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source" data-name="referralSource">Save</a>
                                <a class="grey-btn" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="additionalReferralResource" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Referral Source</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New Referral Source <em class="red-star">*</em></label>
                        <input class="form-control reff_source1" type="text" placeholder="New Referral Source">
                        <span class="red-star" id="reff_source1"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source1" data-name="additional_referralSource">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyEthnicity" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Ethnicity</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New Ethnicity <em class="red-star">*</em></label>
                        <input class="form-control ethnicity_src" type="text" placeholder="Add New Ethnicity">
                        <span class="red-star" id="ethnicity_src"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_ethnicity" data-cell="title" data-class="ethnicity_src" data-name="ethncity">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="additionalEthnicity" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Ethnicity</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New Ethnicity <em class="red-star">*</em></label>
                        <input class="form-control ethnicity_src1" type="text" placeholder="Add New Ethnicity">
                        <span class="red-star" id="ethnicity_src1"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_ethnicity" data-cell="title" data-class="ethnicity_src1" data-name="additional_ethncity">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyMaritalStatus" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Marital Status</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New Marital Status<em class="red-star">*</em></label>
                        <input class="form-control maritalstatus_src" type="text" placeholder="Add New Marital Status">
                        <span class="red-star" id="maritalstatus_src"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single1" data-table="tenant_marital_status" data-cell="marital" data-class="maritalstatus_src" data-name="maritalStatus">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="additionalMaritalStatus" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Marital Status</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New Marital Status<em class="red-star">*</em></label>
                        <input class="form-control maritalstatus_src1" type="text" placeholder="Add New Marital Status">
                        <span class="red-star" id="maritalstatus_src1"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_marital_status" data-cell="marital" data-class="maritalstatus_src1" data-name="additional_maritalStatus">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyHobbies" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Hobbies</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New Hobbies <em class="red-star">*</em></label>
                        <input class="form-control hobbies_src" type="text" placeholder="Add New Hobbies">
                        <span class="red-star" id="hobbies_src"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="hobbies" data-cell="hobby" data-class="hobbies_src" data-name="hobbies">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="additionalHobbies" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Hobbies</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New Hobbies <em class="red-star">*</em></label>
                        <input class="form-control hobbies_src1" type="text" placeholder="Add New Hobbies">
                        <span class="red-star" id="hobbies_src1"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="hobbies" data-cell="hobby" data-class="hobbies_src1" data-name="additional_hobbies[]">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyVeteranStatus" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New VeteranStatus</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New VeteranStatus <em class="red-star">*</em></label>
                        <input class="form-control veteran_src" type="text" placeholder="Add New VeteranStatus">
                        <span class="red-star" id="veteran_src"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_veteran_status" data-cell="veteran" data-class="veteran_src" data-name="veteranStatus">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="additionalVeteranStatus" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New VeteranStatus</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>New VeteranStatus <em class="red-star">*</em></label>
                        <input class="form-control veteran_src1" type="text" placeholder="Add New VeteranStatus">
                        <span class="red-star" id="veteran_src1"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_veteran_status" data-cell="veteran" data-class="veteran_src1" data-name="additional_veteranStatus">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="tenantCredentialType" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Credential Type</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>Credential Type <em class="red-star">*</em></label>
                        <input class="form-control credential_source" type="text" placeholder="Ex: License">
                        <span class="red-star" id="credential_source"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_credential_type" data-cell="credential_type" data-class="credential_source" data-name="credentialType[]">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addchargescode" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Charge Code</h4>
                </div>
                <div class="modal-body">
                    <form name="" method="POST" id="chargecode_popup">
                        <div class="form-outer" style="float: none;">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-4">
                                        <label>Charge Code <em class="red-star">*</em></label>
                                        <input type="text" name="charge_code" class="form-control">
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Credit Account <em class="red-star">*</em></label>
                                        <select class="form-control" name="credit_account">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Debit Account <em class="red-star">*</em></label>
                                        <select class="form-control" name="debit_account">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-4">
                                        <label>Status <em class="red-star">*</em></label>
                                        <select class="form-control" name="status">
                                            <option value="">Select</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Description</label>
                                        <textarea class="form-control" name="description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <a class="blue-btn savechargecodepopup" value="Save">Save</a>
                                <a class="grey-btn cancelchargecodepopup" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyPetSex" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Sex</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>Add New Gender <em class="red-star">*</em></label>
                        <input class="form-control gender_source" type="text" placeholder="Add New Gender">
                        <span class="red-star" id="gender_source"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_pet_gender" data-cell="gender" data-class="gender_source" data-name="pet_gender[]">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyAnimalSex" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Sex</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>Add New Gender <em class="red-star">*</em></label>
                        <input class="form-control gender_source1" type="text" placeholder="Add New Gender">
                        <span class="red-star" id="gender_source1"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_service_animal_gender" data-cell="gender" data-class="gender_source1" data-name="service_gender[]">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="collectionreason" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Reason</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>Add New Reason <em class="red-star">*</em></label>
                        <input class="form-control reason_source" type="text" placeholder="Add New Reason">
                        <span class="red-star" id="reason_source"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_single" data-table="tenant_collection_reason" data-cell="reason" data-class="reason_source" data-name="collection_reason">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="generate_lease" role="dialog" style="text-align: left;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Lease Generation</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none; margin: 0px;">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Do you want to generate lease?</label>
                                <label>Click OK to proceed further.</label>
                            </div>
                            <div class="col-sm-12">
                                <input type="button" class="blue-btn lease_generation" value="Ok">
                                <input type="button" class="grey-btn" value="Cancel" data-dismiss="modal">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="generate_online_lease" role="dialog">
        <div class="modal-dialog modal-md text-center">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Disclaimer</h4>
                </div>
                <div class="modal-body" style="text-align: left">
                    <div class="form-outer" style="float: none">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>I authorize ApexLink Inc. to submit this request to generate and present esignature for the Person/Company I have listed. I understand that ApexLink Inc. is not responsible or liable for any data breach, loss/loss of profits or any claim against the filer (Myself or my company) by any party. I understand that in no event is ApexLink Inc. liable for any and all damages. I also understand that ApexLink Inc. grants no implied warranties, including without limitation, warranties, of merchantability, or of fitness for any particular purpose. All questions or concerns must be directed to Docusign at: <a href="http://go.docusign.com">go.docusign.com</a></label>
                                <label>Click Accept to proceed further.</label>
                            </div>
                            <div class="col-sm-12">
                                <input type="button" class="blue-btn accept_generate_lease" value="Accept">
                                <input type="button" class="blue-btn" value="Cancel" data-dismiss="modal">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyCustomField" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="row custom_field_form">
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Field Name<span class="required"> * </span></label>
                                </div>
                                <div class="col-sm-9 field_name">
                                    <input class="form-control" type="text" name="field_name" placeholder="">
                                    <span class="error"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Data Type</label>
                                </div>
                                <div class="col-sm-9 data_type">
                                    <select class="form-control" name="data_type">
                                        <option value="text">Text</option>
                                        <option value="number">Number</option>
                                        <option value="currency">Currency</option>
                                        <option value="percentage">Percentage</option>
                                        <option value="url">URL</option>
                                        <option value="date">Date</option>
                                        <option value="memo">Memo</option>
                                    </select>
                                    <span class="error"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Default value</label>
                                </div>
                                <div class="col-sm-9 default_value">
                                    <input class="form-control" type="text" name="default_value" placeholder="">
                                    <span class="error"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Required Field</label>
                                </div>
                                <div class="col-sm-9 is_required">
                                    <select class="form-control" name="is_required">
                                        <option value="1">Yes</option>
                                        <option value="0" selected="selected">No</option>
                                    </select>
                                    <span class="error"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <button type="button" class="blue-btn" id='saveCustomField'>Save</button>
                            <button type="button" class="black-btn" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="selectPropertyCustomField1" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer">
                        <div class="row custom_field_form1">
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Field Name<span class="required"> * </span></label>
                                </div>
                                <div class="col-sm-9 field_name1">
                                    <input class="form-control" type="text" name="field_name" placeholder="">
                                    <span class="error"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Data Type</label>
                                </div>
                                <div class="col-sm-9 data_type1">
                                    <select class="form-control" name="data_type">
                                        <option value="text">Text</option>
                                        <option value="number">Number</option>
                                        <option value="currency">Currency</option>
                                        <option value="percentage">Percentage</option>
                                        <option value="url">URL</option>
                                        <option value="date">Date</option>
                                        <option value="memo">Memo</option>
                                    </select>
                                    <span class="error"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Default value</label>
                                </div>
                                <div class="col-sm-9 default_value1">
                                    <input class="form-control" type="text" name="default_value" placeholder="">
                                    <span class="error"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>Required Field</label>
                                </div>
                                <div class="col-sm-9 is_required1">
                                    <select class="form-control" name="is_required">
                                        <option value="1">Yes</option>
                                        <option value="0" selected="selected">No</option>
                                    </select>
                                    <span class="error"></span>
                                </div>
                            </div>
                        </div>
                        <div class="btn-outer">
                            <button type="button" class="blue-btn" id='saveCustomField1'>Save</button>
                            <button type="button" class="black-btn" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="backgroundcheckPop1" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Disclaimer</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <p style="font-size: 14px; line-height: 20px;"> I authorize ApexLink Inc. to submit this request to generate and present a background/any check report for the Person/Company I have listed. I understand that ApexLink Inc. is not responsible or liable for any data breach, loss/loss of profits or any claim against the filer (Myself or my company) by any party. I understand that in no event is ApexLink Inc. liable for any and all damages. I also understand that ApexLink Inc. grants no implied warranties, including without limitation, warranties, of merchantability, or of fitness for any particular purpose. </p></div>
                    <div class="col-sm-12"><input type="checkbox" name="" class="background_check_open"><label style="display: inline;"> Check to proceed further.</label></div>
                    <div class="col-sm-12"><input type="button" class="blue-btn"  data-dismiss="modal" value="Cancel"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="backgroundcheckPop2" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 80%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">Background Reports</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12 title1">
                        Open Your Free Account Today!
                    </div>
                    <div class="col-sm-12">
                        ApexLink is pleased to partner with Victig Screening Solutions to provide you with the highest quality, timely National Credit, Criminal and Eviction Reports.
                    </div>
                    <div class="col-sm-12">
                        <table>
                            <thead>
                            <tr><th colspan="3">Package Pricing</th></tr>
                            <tr><th colspan="3">Properties Managing</th></tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>National Criminal</td>
                                <td>National Criminal and Credit</td>
                                <td>National Criminal, Credit and State Eviction</td>
                            </tr>
                            <tr>
                                <td>$15</td>
                                <td>$20</td>
                                <td>$25</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                        *$100 fee to authorize credit report
                    </div>
                    <div class="col-sm-12">
                        <table class="table2">
                            <thead>
                            <tr><th colspan="5">High Volume Users and Larger Properties</th></tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="5">
                                    <label class="text-center">Contact our Victig Sales Team for custom pricing</label>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="mailto:sales@victig.com"><strong>sales@victig.com</strong></a></td>
                                <td><span class="glyphicon glyphicon-ok"></span></td>
                                <td><strong>866.886.5644</strong></td>
                                <td><span class="glyphicon glyphicon-ok"></span></td>
                                <td><a href="http://www.victig.com" target="_blank"><strong>www.victig.com</strong></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-7">
                            Already have an Account? <a href="https://apexlink.instascreen.net" target="_blank">Log in here</a><br>
                            New Users: <a href="https://www.victig.com/victig-on-boarding/" target="_blank">Click here to register</a>
                        </div>
                        <div class="col-sm-5 text-right"><input type="button" class="blue-btn" value="View Sample Report"></div>
                    </div>
                    <div class="col-sm-12">
                        Please be advised that all background services are provided by Victig Screening
                        Solutions. Direct all contact and questions pertaining to background check services
                        to <a href="mailto:sales@victig.com">sales@victig.com</a>, 866.886.5644, <a href="http://www.victig.com" target="_blank">www.victig.com</a>
                    </div>
                    <div class="col-sm-12 table-fotr">
                        NOTICE: The use of this system is restricted. Only authorized users may access this system. All Access to this system is logged and regularly monitored for computer security purposes. Any unauthorized access to this system is prohibited and is subject to criminal and civil penalties under Federal Laws including, but not limited to, the Computer Fraud and Abuse Act and the National Information Infrastructure Protection Act.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="quickLinkMoveOut" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Move Out</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <select class="js-example-basic-single tenant_moveout_name" name="state">
                                        <option value="1">Alabama</option>
                                        <option value="2">Wyoming</option>
                                        <option value="3">Alabama</option>
                                        <option value="4">Wyoming</option>
                                        <option value="5">Alabama</option>
                                        <option value="6">Wyoming</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="addReasonmodel" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Reason</h4>
                </div>
                <div class="modal-body">
                    <form name="property_name" method="post">
                        @csrf
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Reason <span class="required"> * </span></label>
                                    <input class="form-control" type="text" name="" placeholder="Reason">
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="blue-btn" id=''>Save</button>
                                <button type="button" class="black-btn cancelbtn">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="vehiclessearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="vehicle_name">Name</option>
                                        <option value="make">Make</option>
                                        <option value="license">Plate</option>
                                        <option value="color">Color</option>
                                        <option value="year">Year</option>
                                        <option value="vin">VIN</option>
                                        <option value="registration">Registration</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonvehices' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="modal fade" id="petssearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="pet_name">Name</option>
                                        <option value="pet_id">Id</option>
                                        <option value="pet_type">Type</option>
                                        <option value="pet_age">Age</option>
                                        <option value="pet_sex">Sex</option>
                                        <option value="pet_weight">Weight</option>
                                        <option value="pet_note">Note</option>
                                        <option value="pet_color">Pet Color</option>
                                        <option value="pet_chip_id">Chip ID</option>
                                        <option value="hospital_name">Vet/Hosp. Name</option>
                                        <option value="phone_number">Phone Number</option>
                                        <option value="last_visit">Last Visit</option>
                                        <option value="next_visit">Next Visit</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonpets' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="guarantorssearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="first_name">Name</option>
                                        <option value="relationship">Relation</option>
                                        <option value="address1">Address</option>
                                        <option value="email">Email</option>
                                        <option value="phone_number">PhoneNo.</option>
                                        <option value="guarantee_years">Years of Guarantee</option>
                                        <option value="note">Notes</option>
                                        <option value="file_name">File Uploaded</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonguarantors' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="collectionssearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="collection_id">Collection ID</option>
                                        <option value="reason">Reason</option>
                                        <option value="description">Description</option>
                                        <option value="amount_due">Amount Due</option>
                                        <option value="status">Status</option>
                                        <option value="note">Note</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttoncollections' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="parkingssearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="updated_at">Date</option>
                                        <option value="tenant_parking_permit_number">Parking Permit No.</option>
                                        <option value="tenant_parking_space_number">Parking Space No.</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value2" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonparkings' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="medicalssearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="allegery">Medical Allergy</option>
                                        <option value="date">Date</option>
                                        <option value="note">Note</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value1" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonmedicals' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="fileTransfermodal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="name">Name</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbutton1' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="tenantOccupantsearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="name">Name</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="valueOccpt" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonOccupt' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--modal body-->
<div class="container">
    <div class="modal fade" id="TenantNotesmodal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="date">date</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value" name="valueNotes" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonNotes' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialogNotes" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--modal body-->
<div class="container">
    <div class="modal fade" id="TenantCompalintssmodal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="date">date</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value" name="valueCompalints" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonCompalints' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialogCompalints" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="tenantHoasearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search...</h4>
                </div>
                <div class="modal-body">
                    <form id='searchform' name="default-setting" method="get">
                        @csrf
                        <div class="form-outer">
                            <div class="form-data">
                                <div class="col-sm-4">
                                    <select name="columns" id="columns" class="timeout form-control">
                                        <option value="hoa_violation_id">Hoa Violation Id</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="condition" id="condition" class="timeout form-control">
                                        <option value="equal">equal</option>
                                        <option value="begins">begins with</option>
                                        <option value="ends">ends with</option>
                                        <option value="contains">contains</option>
                                        <option value="isin">is in</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input value="" id="value1" name="value" class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="button" class="btn btn-default blue-btn" id='searchbuttonHoa' >Find</button>
                                <input type="button" class="btn btn-primary black-btn" id="ResetAdvanceSearchDialog" value="Reset" >
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewPortfolioModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Portfolio</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>Portfolio ID <em class="red-star">*</em></label>
                        <input placeholder="Add Portfolio ID" class="form-control portfolio_id_err" name="portfolio_id" type="text" />
                        <span class="red-star" id="portfolio_id_err"></span>
                        <label style="padding: 15px 0 0px 0;">Portfolio Name <em class="red-star">*</em></label>
                        <input placeholder="Add New Portfolio" class="form-control portfolio_name_err" name="portfolio_name" type="text"/>
                        <span class="red-star" id="portfolio_name_err"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_double" data-table="company_property_portfolio" data-cell="portfolio_id,portfolio_name" data-class="portfolio_id_err,portfolio_name_err" data-name="portfolio_id">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewManagerModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Manager</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>First Name <em class="red-star">*</em></label>
                        <input placeholder="First Name" class="form-control first_name_err" name="first_name" type="text" />
                        <span style="display: none;" class="red-star" id="first_name_err">Enter first name</span>
                        <label style="padding: 15px 0 0px 0;">Last Name</label>
                        <input placeholder="Last Name" class="form-control last_name_err" name="last_name" type="text"/>
                        <label style="padding: 15px 0 0px 0;">Email <em class="red-star">*</em></label>
                        <input placeholder="Email" class="form-control email_err" name="email" type="text" />
                        <span  style="display: none;"class="red-star" id="email_err">Enter valid email</span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_double2" data-table="users" data-cell="first_name,last_name,email,user_type,status" data-class="first_name_err,last_name_err,email_err" data-name="manager_id[]">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewPropertyGroupModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Property Group</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>Property Group Name <em class="red-star">*</em></label>
                        <input placeholder="Add New Property Group" class="form-control group_name_err" name="group_name" type="text" />
                        <span class="red-star" id="group_name_err"></span>
                        <label>Description</label>
                        <input placeholder="Description" class="form-control description_err" name="description" type="text"/>
                        <span class="red-star" id="description_err"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_double" data-table="company_property_groups" data-cell="group_name,description" data-class="group_name_err,description_err" data-name="attach_groups[]">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewPropertyTypeModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Property Type</h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;">
                        <label>Property Type <em class="red-star">*</em></label>
                        <input placeholder="Add New Property Type" class="form-control" name="property_type" type="text"/>
                        <span class="red-star" id="group_name_err"></span>
                        <label>Description</label>
                        <input placeholder="Description" class="form-control description_err" name="description" type="text"/>
                        <span class="red-star" id="description_err"></span>
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                            <a class="blue-btn add_double" data-table="company_property_groups" data-cell="group_name,description" data-class="group_name_err,description_err" data-name="attach_groups[]">Save</a>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="modal fade" id="AddNewPropertyStyleModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-transform: initial;">Add New Property Style</h4>
                </div>
                <div class="modal-body">
                    <form id='AddNewPropertyStyleForm' name="default-setting" method="POST">
                        @csrf
                        <div class="form-outer">
                            <label>Property Style <em class="red-star">*</em> </label>
                            <input placeholder="Add New Property Style" class="form-control" id="property_style" name="property_style" type="text" value="" />
                            <span class="required_star" id="property_style_err"></span>
                            <br>
                            <label>Description</label>
                            <input placeholder="Description" class="form-control" id="property_style_description" name="description" type="text" value="" />
                            <span class="required_star" id="description_err"></span>
                            <div class="btn-outer">
                                <!-- <button type="button" class="btn btn-default blue-btn"  id='AddNewPropertyStyleButton'>Save</button> -->
                                <input type="submit" class="btn btn-default blue-btn"  id='AddNewPropertyStyleButton' value="Save" >
                                <input type="button" class="btn btn-primary black-btn" class="close" data-dismiss="modal" value="Cancel">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewPropertySubTypeModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-transform: initial;">Add New Property Sub-Type</h4>
                </div>
                <div class="modal-body">
                    <form id='AddNewPropertySubTypeForm' name="default-setting" method="POST">
                        @csrf
                        <div class="form-outer">
                            <label>Property Sub-Type <em class="red-star">*</em> </label>
                            <input placeholder="Add New Property Sub-Type" class="form-control" id="property_subtype" name="property_subtype" type="text" value="" />
                            <span class="required_star" id="property_subtype_err"></span>
                            <br>
                            <label>Description</label>
                            <input placeholder="Description" class="form-control" id="property_subtype_description" name="description" type="text" value="" />
                            <span class="required_star" id="description_err"></span>
                            <div class="btn-outer">
                                <!--  <button type="button" class="btn btn-default blue-btn"  id='AddNewPropertySubTypeButton'>Save</button> -->
                                <input type="submit" class="btn btn-default blue-btn"  id='AddNewPropertySubTypeButton' value="Save" >
                                <input type="button" class="btn btn-primary black-btn" class="close" data-dismiss="modal" value="Cancel">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-transform: initial;">Add New Pet Friendly</h4>
                </div>
                <div class="modal-body">
                    <form id='AddNewPetFriendlyForm' name="default-setting" method="POST">
                        @csrf
                        <div class="form-outer">
                            <label>Pet Friendly <em class="red-star">*</em></label>
                            <input placeholder="Add Pet Friendly" class="form-control" name="condition" id="pet_condition" type="text" value="" />
                            <span class="required_star" id="condition_err"></span>
                            <div class="btn-outer">
                                <!--  <button type="button" class="btn btn-default blue-btn"  id='AddNewPetFriendlyButton'>Save</button> -->
                                <input type="submit" class="btn btn-default blue-btn"  id='AddNewPetFriendlyButton' value="Save" >
                                <input type="button" class="btn btn-primary black-btn" class="close" data-dismiss="modal" value="Cancel">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewGarageAvailableModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-transform: initial;">Add New Garage Available</h4>
                </div>
                <div class="modal-body">
                    <form id='AddNewGarageAvailableForm' name="default-setting" method="POST">
                        @csrf
                        <div class="form-outer">
                            <label>New Garage Available <em class="red-star">*</em></label>
                            <input placeholder="Add Garage Available" class="form-control" id="garage_condition" name="condition" type="text" value="" />
                            <span class="required_star" id="condition_err"></span>
                            <div class="btn-outer">
                                <!-- <button type="button" class="btn btn-default blue-btn"  id='AddNewGarageAvailableButton'>Save</button> -->
                                <input type="submit" class="btn btn-default blue-btn"  id='AddNewGarageAvailableButton' value="Save" >
                                <input type="button" class="btn btn-primary black-btn" class="close" data-dismiss="modal" value="Cancel">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewAmenityModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-transform: initial;">Add New Amenity</h4>
                </div>
                <div class="modal-body">
                    <form id='AddNewAmenityForm' name="default-setting" method="POST">

                        <div class="form-outer" style="float: none;">
                            <label>New Amenity Code</label>
                            <input placeholder="Eg:- 12345 (Optional)" class="form-control" id="code" name="code" type="text" value="" />
                            <span class="required_star" id="code_err"></span>
                            <br>
                            <label>Amenity Name <em class="red-star">*</em> </label>
                            <input placeholder="Add New Amenity Name" class="form-control" id="amenity_name" name="name" type="text" value="" />
                            <span class="required_star" id="name_err"></span>
                            <div class="">
                                <input type="submit" class="btn btn-default blue-btn"  id='AddNewAmenityButton' value="Save" >
                                <input type="button" class="btn btn-primary grey-btn" class="close" data-dismiss="modal" value="Cancel">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewKeyAccessCodeModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-transform: initial;">Add New Key Access Code</h4>
                </div>
                <div class="modal-body">
                    <form id='AddKeyAccessCodeForm' name="default-setting" method="POST">
                        @csrf
                        <div class="form-outer">
                            <label>Key Access Code <em class="red-star">*</em></label>
                            <input placeholder="Add Key Access Code" class="form-control" name="access_code" id="access_code"type="text" value="" />
                            <span class="required_star" id="access_code_err"></span>
                            <div class="btn-outer">

                                <input type="submit" class="btn btn-default blue-btn"  id='AddNewKeyAccessCodeButton' value="Save" >
                                <input type="button" class="btn btn-primary black-btn" class="close" data-dismiss="modal" value="Cancel">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewUnitTypeModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-transform: initial;">Add New Unit Type</h4>
                </div>
                <div class="modal-body">
                    <form id='AddNewUnitTypeForm' name="default-setting" method="POST">
                        @csrf
                        <input id="addUnittoken" type="hidden" name="_token" value="">
                        <div class="form-outer">
                            <label>Unit Type <em class="red-star">*</em> </label>
                            <input placeholder="Add New Unit Type" class="form-control" id="unit_type" name="unit_type" type="text" value="" />
                            <span class="required_star" id="unit_type_err"></span>
                            <br>
                            <label>Description</label>
                            <input placeholder="Description" class="form-control" id="unit_description" name="description" type="text" value="" />
                            <span class="required_star" id="description_err"></span>

                            <div class="btn-outer">
                                <!-- <button type="button" class="btn btn-default blue-btn"  id='AddNewUnitTypeButton'>Save</button> -->
                                <input type="submit" class="btn btn-default blue-btn"  id='AddNewUnitTypeButton' value="Save" >
                                <input type="button" class="btn btn-primary black-btn" class="close" data-dismiss="modal" value="Cancel">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="AddNewUnitModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" style="width:300px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Unit Type</h4>
                </div>
                <div class="modal-body  add-newunitcontent add-newunitcontent-new">
                    <div class="form-data add_new_building_data">
                        <form id="AddNewUnitData" method="post" action="">
                            <div class="col-sm-12">
                                <label class="control-label">Unit Type<span class="required"> *</span></label>
                                <input type="text" maxlength="15" style="padding: 2% 2% !important;" placeholder="Add New Unit Type" class="form-control" name="unit_type" id="unit_type" spellcheck="true">
                                <span class="required_star" id="unit_type_err"></span>
                            </div>
                            <div class="col-sm-12">
                                <label class="control-label">
                                    Description
                                </label>
                                <input type="text" maxlength="50" style="padding: 2% 2% !important;" placeholder="Description" name="description" id="description" class="form-control" spellcheck="true">
                                <span class="required_star" id="description_err"></span>
                            </div>

                            <div class="col-sm-12">
                                <div class="nopadding_Up nopadding_Down">
                                    <input type="submit" class="blue-btn" value="Save">
                                    <a id="AddNewAmenityCancel2"><input type="button" class="clear-btn" value="Cancel"></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="print_hoa" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="blue-btn" id='email_hoa'>Email</button>
                    <button type="button" class="blue-btn" id='print_hoa'  onclick="PrintElem('#modal-body-hoa')" onclick="window.print();">Print</button>
                    <button type="button" class="close" data-dismiss="modal">×</button>

                </div>
                <div class="modal-body" id="modal-body-hoa" style="height: 380px; overflow: auto;">

                </div>
            </div>
        </div>
    </div>
</div>