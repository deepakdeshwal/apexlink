<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
include_once(COMPANY_DIRECTORY_URL . "/views/company/tenants/modals.php");
?>
<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/smartmove.css"/>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Leases &gt;&gt; <span>List of Leases</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links4 ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/leases/layout/right-nav.php");?>
                    <!--- Right Quick Links1 ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" ><a href="/GuestCard/ListGuestCard" >Guest Cards</a></li>
                            <li role="presentation" "><a href="/RentalApplication/RentalApplications">Rental Applications</a></li>
                            <li role="presentation" class="active"><a href="/Lease/ViewEditLease" >Leases</a></li>
                            <li role="presentation"><a href="/Lease/Movein/" >Move-In</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <section class="main-content">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="content-data">

                                        <!--Tabs Starts -->
                                        <div class="main-tabs">
                                            <!-- Nav tabs -->
                                           <!-- <div class="right-links-outer">
                                                <div class="right-links slide-toggle2"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
                                                <div id="RightMenu" class="box2" style="display: none">
                                                    <h2>PEOPLE</h2>
                                                    <div class="list-group panel">
                                                        <a href="/Tenantlisting/add" class="list-group-item list-group-item-success strong collapsed">New Tenant</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Move Out</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Book Now</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Owner</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Vendor</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Employee</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Contact</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Recieve Payment</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Send Tenant Statements</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Bank Deposite</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Bill</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Tenant Instrument Register</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Vendor Instrument Register</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Tenant Transfer</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Tenant Transfer</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Move In</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Move Out</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Tenants</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Owners</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Vendors</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">NSF Listing</a>
                                                    </div>
                                                </div>
                                            </div>-->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="people-tenant">
                                                    <!-- Sub Tabs Starts-->
                                                    <div class="sub-tabss sub-navs">
                                                        <!-- Nav tabs -->
                                                        <!-- Tab panes -->

                                                            <div role="tabpanel" class="tab-pane active" id="regular-rent">
                                                                <div class="property-status">
                                                                    <div class="row">
                                                                        <div class="col-sm-2 tenant_type_status">
                                                                            <label>Status</label>
                                                                            <select class="fm-txt form-control jqGridStatusClass"  id="jqgridOptions"  data-module="LEASELISTING">
                                                                                <option value="All">All</option>
                                                                                <option value="1">Active</option>
                                                                                <option value="6">Move In</option>
                                                                                <option value="7">Renewed</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-10">
                                                                            <div class="btn-outer text-right lease-btn">
                                                                                <a href="/Lease/Leases" class="blue-btn">New Lease</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div  style="display: none;" id="import_tenant_type_div">
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-heading">
                                                                                <h4 class="panel-title">

                                                                                    <a>Import Tenant Type</a>
                                                                                </h4>
                                                                            </div>
                                                                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                                <div class="panel-body">
                                                                                    <form name="importTenantTypeForm" id="importTenantTypeFormId" >
                                                                                        <div class="row">
                                                                                            <div class="form-outer">
                                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                    <input type="file" name="import_file" id="import_file"   accept=".csv, .xls, .xlsx" required/>
                                                                                                    <span class="error"></span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-xs-12">
                                                                                                <div class="btn-outer">
                                                                                                    <button type="submit" class="blue-btn">Submit</button>
                                                                                                    <button type="button" id="import_tenant_cancel_btn" class="grey-btn">Cancel</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="accordion-grid">
                                                                    <div class="accordion-outer">
                                                                        <div class="bs-example">
                                                                            <div class="panel-group" id="accordion">
                                                                                <div class="panel panel-default">

                                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                        <div class="panel-body pad-none">
                                                                                            <div class="grid-outer">
                                                                                                <div class="table-responsive">
                                                                                                    <div class="grid-outer">
                                                                                                        <div class="apx-table">
                                                                                                            <table class="table table-hover table-dark" id="lease_listing">
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Regular Rent Ends -->
                                                            <div role="tabpanel" class="tab-pane" id="shortterm-rent">

                                                            </div>
                                                    </div>
                                                    <!-- Sub tabs ends-->
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="people-owner">
                                                    <div class="property-status">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <label>Status</label>
                                                                <select class="fm-txt form-control"> <option>Active</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-10">
                                                                <div class="btn-outer text-right">
                                                                    <button class="blue-btn">Download Sample</button>
                                                                    <button class="blue-btn">Import Owner</button>
                                                                    <button onclick="window.location.href='new-property.html'" class="blue-btn">New Owner</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Owners</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Owner Name</th>
                                                                                                <th scope="col">Company</th>
                                                                                                <th scope="col">Phone</th>
                                                                                                <th scope="col">Email</th>
                                                                                                <th scope="col">Date Created</th>
                                                                                                <th scope="col">Owner's Portal</th>
                                                                                                <th scope="col">Status</th>
                                                                                                <th scope="col">Actions</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Alien N West</a></td>
                                                                                                <td></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>allen09@gmail.com</td>
                                                                                                <td>11/23/2018 (Fri.)</td>
                                                                                                <td>Yes</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Alien N West</a></td>
                                                                                                <td></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>allen09@gmail.com</td>
                                                                                                <td>11/23/2018 (Fri.)</td>
                                                                                                <td>Yes</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Alien N West</a></td>
                                                                                                <td></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>allen09@gmail.com</td>
                                                                                                <td>11/23/2018 (Fri.)</td>
                                                                                                <td>Yes</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Alien N West</a></td>
                                                                                                <td></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>allen09@gmail.com</td>
                                                                                                <td>11/23/2018 (Fri.)</td>
                                                                                                <td>Yes</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="people-vendor">
                                                    <div class="property-status">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <label>Vendor Type</label>
                                                                <select class="fm-txt form-control"> <option>Select</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>

                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Status</label>
                                                                <select class="fm-txt form-control"> <option>Active</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>

                                                            </div>
                                                            <div class="col-sm-8">
                                                                <div class="btn-outer text-right">
                                                                    <button class="blue-btn">Download Sample</button>
                                                                    <button class="blue-btn">Import Vendor</button>
                                                                    <button onclick="window.location.href='new-property.html'" class="blue-btn">New Vendor</button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Vendors</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Vendor Name</th>
                                                                                                <th scope="col">Phone</th>
                                                                                                <th scope="col">Open Work Orders</th>
                                                                                                <th scope="col">YTD Payment</th>
                                                                                                <th scope="col">Type</th>
                                                                                                <th scope="col">Rate</th>
                                                                                                <th scope="col">Rating</th>
                                                                                                <th scope="col">Email</th>
                                                                                                <th scope="col">Status</th>
                                                                                                <th scope="col">Actions</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>1</td>
                                                                                                <td>0.00</td>
                                                                                                <td>Contractor-General</td>
                                                                                                <td>$19.00 /hr</td>
                                                                                                <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>
                                                                                                <td>skkokjim@gmail.com</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>1</td>
                                                                                                <td>0.00</td>
                                                                                                <td>Contractor-General</td>
                                                                                                <td>$19.00 /hr</td>
                                                                                                <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>
                                                                                                <td>skkokjim@gmail.com</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>1</td>
                                                                                                <td>0.00</td>
                                                                                                <td>Contractor-General</td>
                                                                                                <td>$19.00 /hr</td>
                                                                                                <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>
                                                                                                <td>skkokjim@gmail.com</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>1</td>
                                                                                                <td>0.00</td>
                                                                                                <td>Contractor-General</td>
                                                                                                <td>$19.00 /hr</td>
                                                                                                <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>
                                                                                                <td>skkokjim@gmail.com</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="people-contact">
                                                    <!-- Sub Tabs Starts-->
                                                    <div class="sub-tabs">
                                                        <!-- Nav tabs -->
                                                        <ul class="nav nav-tabs" role="tablist">
                                                            <li role="presentation" class="active"><a href="#contact-tenant" aria-controls="home" role="tab" data-toggle="tab">Tenants<span class="tab-count">84</span></a></li>
                                                            <li role="presentation"><a href="#contact-owner" aria-controls="profile" role="tab" data-toggle="tab">Owners<span class="tab-count">36</span></a></li>
                                                            <li role="presentation"><a href="#contact-vendor" aria-controls="home" role="tab" data-toggle="tab">Vendors<span class="tab-count">71</span></a></li>
                                                            <li role="presentation"><a href="#contact-users" aria-controls="profile" role="tab" data-toggle="tab">Users<span class="tab-count">199</span></a></li>
                                                            <li role="presentation"><a href="#contact-others" aria-controls="profile" role="tab" data-toggle="tab">Others<span class="tab-count">84</span></a></li>
                                                        </ul>
                                                        <!-- Tab panes -->
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane active" id="contact-tenant">
                                                                <div class="property-status">
                                                                    <div class="row">
                                                                        <div class="col-sm-2">
                                                                            <label>Status</label>
                                                                            <select class="fm-txt form-control"> <option>Active</option>
                                                                                <option></option>
                                                                                <option></option>
                                                                                <option></option>
                                                                            </select>

                                                                        </div>
                                                                        <div class="col-sm-10">
                                                                            <div class="btn-outer text-right">
                                                                                <button class="blue-btn">Download Sample</button>
                                                                                <button class="blue-btn">Import Contact</button>
                                                                                <button onclick="window.location.href='new-property.html'" class="blue-btn">New Contact</button>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="accordion-grid">
                                                                    <div class="accordion-outer">
                                                                        <div class="bs-example">
                                                                            <div class="panel-group" id="accordion">
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-heading">
                                                                                        <h4 class="panel-title">
                                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                                <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Contacts</a>
                                                                                        </h4>
                                                                                    </div>
                                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                        <div class="panel-body pad-none">
                                                                                            <div class="grid-outer">
                                                                                                <div class="table-responsive">
                                                                                                    <table class="table table-hover table-dark">
                                                                                                        <thead>
                                                                                                        <tr>
                                                                                                            <th scope="col">Contact Name</th>
                                                                                                            <th scope="col">Phone</th>
                                                                                                            <th scope="col">Email</th>
                                                                                                            <th scope="col">Date Created</th>
                                                                                                            <th scope="col">Status</th>
                                                                                                            <th scope="col">Actions</th>
                                                                                                        </tr>
                                                                                                        </thead>
                                                                                                        <tbody>
                                                                                                        <tr>
                                                                                                            <td><a class="grid-link" href="javascript:;">Abby N Wesley</a></td>
                                                                                                            <td>555-444-6666</td>
                                                                                                            <td>abby768@gmail.com</td>
                                                                                                            <td>12/7/2018 12:30:53 PM</td>
                                                                                                            <td>Active</td>
                                                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td><a class="grid-link" href="javascript:;">Arsh Sandhu</a></td>
                                                                                                            <td>555-444-6666</td>
                                                                                                            <td>abby768@gmail.com</td>
                                                                                                            <td>12/7/2018 12:30:53 PM</td>
                                                                                                            <td>Active</td>
                                                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td><a class="grid-link" href="javascript:;">Ben Snow</a></td>
                                                                                                            <td>555-444-6666</td>
                                                                                                            <td>abby768@gmail.com</td>
                                                                                                            <td>12/7/2018 12:30:53 PM</td>
                                                                                                            <td>Active</td>
                                                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>
                                                                                                            <td>555-444-6666</td>
                                                                                                            <td>abby768@gmail.com</td>
                                                                                                            <td>12/7/2018 12:30:53 PM</td>
                                                                                                            <td>Active</td>
                                                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Regular Rent Ends -->
                                                            <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                                            </div>
                                                            <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                                            </div>
                                                            <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                                            </div>
                                                            <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Sub tabs ends-->
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="people-employee">
                                                    <div class="property-status">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <label>Status</label>
                                                                <select class="fm-txt form-control"> <option>Active</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>

                                                            </div>
                                                            <div class="col-sm-10">
                                                                <div class="btn-outer text-right">
                                                                    <button class="blue-btn">Download Sample</button>
                                                                    <button class="blue-btn">Import Employee</button>
                                                                    <button onclick="window.location.href='new-property.html'" class="blue-btn">New Employee</button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Employees</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Employee Name</th>
                                                                                                <th scope="col">Phone</th>
                                                                                                <th scope="col">Email</th>
                                                                                                <th scope="col">Date Created</th>
                                                                                                <th scope="col">Status</th>
                                                                                                <th scope="col">Actions</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>adam@gmail.com</td>
                                                                                                <td>11/23/2018 8:48:45 AM</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>adam@gmail.com</td>
                                                                                                <td>11/23/2018 8:48:45 AM</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>adam@gmail.com</td>
                                                                                                <td>11/23/2018 8:48:45 AM</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>adam@gmail.com</td>
                                                                                                <td>11/23/2018 8:48:45 AM</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Tabs Ends -->

                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->
<div class="container">
    <div class="modal fade" id="backgroundReport" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content  pull-left" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-left">Background Reports</h4>
                </div>
                <div class="modal-body">
                    <div class="tenant-bg-data">
                        <h3 style="text-align: center;">
                            Open Your Free Account Today!
                        </h3>
                        <p>
                            ApexLink is pleased to partner with Victig Screening Solutions to provide you with
                            the highest quality, timely National Credit, Criminal and Eviction Reports.
                        </p>
                        <div class="grid-outer " style="max-width: 1421px;">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                <tr>
                                    <th colspan="3" align="center" class="bg-Bluehdr">
                                        Package Pricing<br>
                                        Properties Managing
                                    </th>
                                </tr>
                                <tr>
                                    <td width="29%" align="left">
                                        National Criminal
                                    </td>
                                    <td width="29%" align="left">
                                        National Criminal and Credit
                                    </td>
                                    <td width="42%" align="left">
                                        National Criminal, Credit and State Eviction
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        $15
                                    </td>
                                    <td align="left">
                                        $20
                                    </td>
                                    <td align="left">
                                        $25
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        &nbsp;
                                    </td>
                                    <td align="left">
                                        &nbsp;
                                    </td>
                                    <td align="left">
                                        &nbsp;
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <p>
                                *$100 fee to authorize credit report
                            </p>
                        </div>
                        <div class="grid-outer " style="max-width: 1421px;">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                <tr>
                                    <th colspan="5" align="center" class="bg-Bluehdr">
                                        High Volume Users and Larger Properties
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="5" align="left">
                                        <p align="center">
                                            <strong>Contact our Victig Sales Team for custom pricing</strong>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="26%" align="center">
                                        <a href="mailto:sales@victig.com"><strong>sales@victig.com</strong></a>
                                    </td>
                                    <td width="15%" align="center">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td width="21%" align="center">
                                        <strong>866.886.5644 </strong>
                                    </td>
                                    <td width="13%" align="center">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td width="25%" align="center">
                                        <a href="http://www.victig.com" target="_blank"><strong>www.victig.com</strong></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="div-full">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                <tr>
                                    <td width="52%" height="20" style="font-size: 14px;" align="left">
                                        Already have an Account? <a href="https://apexlink.instascreen.net" target="_blank">
                                            Log in here
                                        </a>
                                    </td>
                                    <td width="48%" rowspan="2" align="right">
                                        <a href="https://www.victig.com/wp-content/uploads/2016/05/sample-tenant-report.jpg" target="_blank">
                                            <!--                                            <input value="View Sample Report" class="blue-btn fr" type="button" spellcheck="true" autocomplete="off">-->
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-size: 14px;">
                                        New Users: <a href="https://www.victig.com/victig-on-boarding/" target="_blank">
                                            Click
                                            here to register
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="div-full3">
                            <p>
                                Please be advised that all background services are provided by Victig Screening
                                Solutions. Direct all contact and questions pertaining to background check services
                                to <a href="mailto:sales@victig.com">sales@victig.com</a>, 866.886.5644, <a href="http://www.victig.com" target="_blank">www.victig.com</a>
                            </p>
                        </div>
                        <div class="notice">
                            NOTICE: The use of this system is restricted. Only authorized users may access this
                            system. All Access to this system is logged and regularly monitored for computer
                            security purposes. Any unauthorized access to this system is prohibited and is subject
                            to criminal and civil penalties under Federal Laws including, but not limited to,
                            the Computer Fraud and Abuse Act and the National Information Infrastructure Protection
                            Act.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="modal fade" id="backGroundCheckPop" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content  pull-left" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">Disclaimer</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <p style="font-size: 14px; line-height: 20px;"> I authorize ApexLink Inc. to submit this request to generate and present a background/any check report for the Person/Company I have listed. I understand that ApexLink Inc. is not responsible or liable for any data breach, loss/loss of profits or any claim against the filer (Myself or my company) by any party. I understand that in no event is ApexLink Inc. liable for any and all damages. I also understand that ApexLink Inc. grants no implied warranties, including without limitation, warranties, of merchantability, or of fitness for any particular purpose. </p></div>
                    <div class="col-sm-12"><input type="checkbox" name="" class="background_check_open"><label style="display: inline;"> Check to proceed further.</label></div>
                    <div class="col-sm-12 btn-outer mg-btm-20">
                        <input type="button" class="blue-btn"  data-dismiss="modal" value="Cancel">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Footer Ends -->
<script>
    var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
      var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";

    $(".navbar-collapse #leases_top").addClass("active");
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/leases/leases/leaseListing.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/smartMove.js"></script>


<!-- Jquery Starts -->
<?php
//include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
