<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Leases &gt;&gt; <span>List of Guest Cards</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">

                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="/GuestCard/ListGuestCard" >Guest Cards</a></li>
                            <li role="presentation"><a href="/RentalApplication/RentalApplications">Rental Applications</a></li>
                            <li role="presentation"><a href="/Lease/ViewEditLease" >Leases</a></li>
                            <li role="presentation"><a href="/Lease/Movein/0/" >Move-In</a></li>
                        </ul>
                        <div class="atoz-outer">
                            A-Z <span>All</span>
                        </div>


                        <!--- Right Quick Links ---->
                        <div class="right-links-outer hide-links">
                            <div class="right-links">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </div>
                            <div id="RightMenu" class="box2">
                                <h2>LEASES</h2>
                                <div class="list-group panel">
                                    <!-- Two Ends-->
                                    <a href="#" class="list-group-item list-group-item-success strong collapsed" >New Lease</a>
                                    <!-- Two Ends-->

                                    <!-- Three Starts-->
                                    <a id="new_building_href" href="#" class="list-group-item list-group-item-success strong collapsed" >New Guest Card</a>
                                    <!-- Three Ends-->

                                    <!-- Four Starts-->
                                    <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">New Rental Application</a>
                                    <!-- Four Ends-->

                                    <!-- Five Starts-->
                                    <a href="#" id="" class="list-group-item list-group-item-success strong collapsed">Unit Vacancy Details</a>
                                    <!-- Five Ends-->

                                    <!-- Six Starts-->
                                    <a href="/Lease/LeasesAffordability" class="list-group-item list-group-item-success strong collapsed">Lease Affordability Calculator</a>
                                    <!-- Six Ends-->

                                    <!-- Seven Starts-->
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Manual Lease Renewal Request</a>
                                    <!-- Seven Ends-->
                                </div>
                            </div>
                        </div>
                        <!--- Right Quick Links ---->

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="guest-cards">
                                <!-- Sub Tabs Starts-->
                                <div class="sub-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#contact-tenant" aria-controls="home" role="tab" data-toggle="tab">Active<span class="tab-count">26</span></a></li>
                                        <li role="presentation"><a href="#contact-owner" aria-controls="profile" role="tab" data-toggle="tab">View Scheduled<span class="tab-count">0</span></a></li>
                                        <li role="presentation"><a href="#contact-vendor" aria-controls="home" role="tab" data-toggle="tab">Application Generated<span class="tab-count">0</span></a></li>
                                        <li role="presentation"><a href="#contact-users" aria-controls="profile" role="tab" data-toggle="tab">Lease Generated<span class="tab-count">0</span></a></li>
                                        <li role="presentation"><a href="#contact-users" aria-controls="profile" role="tab" data-toggle="tab">Archived<span class="tab-count">0</span></a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="regular-rent">
                                            <div class="property-status">
                                                <div class="row">

                                                    <div class="col-sm-12">
                                                        <div class="btn-outer text-right">
                                                            <a href="/GuestCard/NewGuestCard" class="blue-btn">New Guest Card</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                            <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Guest Cards</a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-hover table-dark">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th scope="col">Prospect Name</th>
                                                                                        <th scope="col">Property Name</th>
                                                                                        <th scope="col">Guest Card Number</th>
                                                                                        <th scope="col">Move In Date</th>
                                                                                        <th scope="col">Status</th>
                                                                                        <th scope="col">Building Unit ID</th>
                                                                                        <th scope="col">Action</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td><a class="grid-link" href="javascript:;">Freddy Wilson</a></td>
                                                                                        <td><a class="grid-link" href="javascript:;">Snowberry House</a></td>
                                                                                        <td>AF21C4</td>
                                                                                        <td>01/31/2019 (Thu)</td>
                                                                                        <td>Active</td>
                                                                                        <td>Units-1</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><a class="grid-link" href="javascript:;">Freddy Wilson</a></td>
                                                                                        <td><a class="grid-link" href="javascript:;">Snowberry House</a></td>
                                                                                        <td>AF21C4</td>
                                                                                        <td>01/31/2019 (Thu)</td>
                                                                                        <td>Active</td>
                                                                                        <td>Units-1</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><a class="grid-link" href="javascript:;">Freddy Wilson</a></td>
                                                                                        <td><a class="grid-link" href="javascript:;">Snowberry House</a></td>
                                                                                        <td>AF21C4</td>
                                                                                        <td>01/31/2019 (Thu)</td>
                                                                                        <td>Active</td>
                                                                                        <td>Units-1</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><a class="grid-link" href="javascript:;">Freddy Wilson</a></td>
                                                                                        <td><a class="grid-link" href="javascript:;">Snowberry House</a></td>
                                                                                        <td>AF21C4</td>
                                                                                        <td>01/31/2019 (Thu)</td>
                                                                                        <td>Active</td>
                                                                                        <td>Units-1</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Regular Rent Ends -->
                                        <div role="tabpanel" class="tab-pane" id="shortterm-rent">

                                        </div>
                                    </div>
                                </div>
                                <!-- Sub tabs ends-->
                            </div>
                        </div>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->



<!-- Footer Ends -->

<script>


    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



</script>


<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>