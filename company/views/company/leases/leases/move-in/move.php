<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Leases &gt;&gt; <span>List of Guest Cards</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>LEASES </h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" >New Lease</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="new_building_href" href="#" class="list-group-item list-group-item-success strong collapsed" >New Guest Card</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">New Rental Application</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="#" id="" class="list-group-item list-group-item-success strong collapsed">Unit Vacancy Details</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="/Lease/LeasesAffordability" class="list-group-item list-group-item-success strong collapsed">Lease Affordability Calculator</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="/Lease/ManualLeaseRequest" class="list-group-item list-group-item-success strong collapsed" >Manual Lease Renewal Request</a>
                                <!-- Seven Ends-->
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" "><a href="/GuestCard/ListGuestCard" >Guest Cards</a></li>
                            <li role="presentation"><a href="/RentalApplication/RentalApplications/">Rental Applications</a></li>
                            <li role="presentation"><a href="/Lease/ViewEditLease" >Leases</a></li>
                            <li role="presentation" class="active"><a href="/Lease/Movein/0/" >Move-In</a></li>
                        </ul>
                        <div class="atoz-outer">
                            A-Z <span>All</span>
                        </div>
                        <!-- Tab panes -->
                        <div class="tab-content">>
                            <div role="tabpanel" class="" id="moveIn">
                                <!-- Sub Tabs Starts-->
                                <div class="sub-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#contact-tenant" aria-controls="home" role="tab" data-toggle="tab">Tenants<span class="tab-count">84</span></a></li>
                                        <li role="presentation"><a href="#contact-owner" aria-controls="profile" role="tab" data-toggle="tab">Owners<span class="tab-count">36</span></a></li>
                                        <li role="presentation"><a href="#contact-vendor" aria-controls="home" role="tab" data-toggle="tab">Vendors<span class="tab-count">71</span></a></li>
                                        <li role="presentation"><a href="#contact-users" aria-controls="profile" role="tab" data-toggle="tab">Users<span class="tab-count">199</span></a></li>
                                        <li role="presentation"><a href="#contact-others" aria-controls="profile" role="tab" data-toggle="tab">Others<span class="tab-count">84</span></a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="contact-tenant">
                                            <div class="property-status">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <label>Status</label>
                                                        <select class="fm-txt form-control"> <option>Active</option>
                                                            <option></option>
                                                            <option></option>
                                                            <option></option>
                                                        </select>

                                                    </div>
                                                    <div class="col-sm-10">
                                                        <div class="btn-outer text-right">
                                                            <button class="blue-btn">Download Sample</button>
                                                            <button class="blue-btn">Import Contact</button>
                                                            <button onclick="window.location.href='new-property.html'" class="blue-btn">New Contact</button>
                                                        </div>
                                                        <div class="atoz-outer">
                                                            A-Z <span>All</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                            <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Contacts</a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-hover table-dark">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th scope="col">Contact Name</th>
                                                                                        <th scope="col">Phone</th>
                                                                                        <th scope="col">Email</th>
                                                                                        <th scope="col">Date Created</th>
                                                                                        <th scope="col">Status</th>
                                                                                        <th scope="col">Actions</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td><a class="grid-link" href="javascript:;">Abby N Wesley</a></td>
                                                                                        <td>555-444-6666</td>
                                                                                        <td>abby768@gmail.com</td>
                                                                                        <td>12/7/2018 12:30:53 PM</td>
                                                                                        <td>Active</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><a class="grid-link" href="javascript:;">Arsh Sandhu</a></td>
                                                                                        <td>555-444-6666</td>
                                                                                        <td>abby768@gmail.com</td>
                                                                                        <td>12/7/2018 12:30:53 PM</td>
                                                                                        <td>Active</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><a class="grid-link" href="javascript:;">Ben Snow</a></td>
                                                                                        <td>555-444-6666</td>
                                                                                        <td>abby768@gmail.com</td>
                                                                                        <td>12/7/2018 12:30:53 PM</td>
                                                                                        <td>Active</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>
                                                                                        <td>555-444-6666</td>
                                                                                        <td>abby768@gmail.com</td>
                                                                                        <td>12/7/2018 12:30:53 PM</td>
                                                                                        <td>Active</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Regular Rent Ends -->
                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                        </div>
                                    </div>
                                </div>
                                <!-- Sub tabs ends-->
                            </div>
                        </div>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->



<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



</script>




<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
