<div class="col-sm-12 leasepage">
    <div class="content-section">
        <!--Tabs Starts -->
        <div class="main-tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="javascript:void(0);" data-tab="property">Select Property</a></li>
                <li role="presentation" class="active"><a href="javascript:void(0);" data-tab="lease">Lease Details</a></li>
                <li role="presentation"><a href="javascript:void(0);" data-tab="charges">Charges</a></li>
                <li role="presentation"><a href="javascript:void(0);" data-tab="generate">Generate Lease</a></li>
            </ul>

            <!-- Nav tabs -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="guest-cards">
                    <form method="post" name="save_lease_form" id="save_lease_form" action="">
                        <input type="hidden" name="lease_user_id" class="lease_user_id">
                        <div class="form-outer">
                            <div class="form-hdr">
                                <h3>Lease Dates <a class="back" href="/TenantListing/TenantListing"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                            </div>
                            <div class="form-data">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Move-In Date <em class="red-star">*</em></label>
                                        <input class="form-control move_in_date calander" name="move_in_date" id="move_in_date" type="text"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Move-Out/Vacate Date</label>
                                        <input class="form-control move_out_date calander" type="text" name="move_out_date1"/>
                                        <input name="move_out_date" id="move_out_date" type="hidden"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Lease Start Date <em class="red-star">*</em></label>
                                        <input class="form-control start_date calander" type="text" name="start_date1"/>
                                        <input id="start_date" name="start_date" type="hidden"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Lease Term <em class="red-star">*</em></label>
                                        <input class="form-control lease_term" type="text" value="12" name="lease_term"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>&nbsp;</label>
                                        <select class="form-control lease_tenure" name="lease_tenure">
                                            <option value="1">Month</option>
                                            <option value="2">Year</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Lease End Date <em class="red-star">*</em></label>
                                        <input name="end_date1" class="form-control end_date" type="text"/>
                                        <input name="end_date" id="end_date" type="hidden"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Notice Period </label>
                                        <select class="form-control notice_period" name="notice_period">
                                            <option value="">Select</option>
                                            <option value="30">30 days</option>
                                            <option value="60">60 days</option>
                                            <option value="90" selected>90 days</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Notice Date <em class="red-star">*</em></label>
                                        <input name="notice_date" class="form-control notice_date" type="text"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Form Outer Ends -->
                        <div class="form-outer">
                            <div class="form-hdr">
                                <h3>Rent Details</h3>
                            </div>
                            <div class="form-data">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Rent Due Day</label>
                                        <select class="form-control" name="rent_due_day">
                                            <?php
                                            for($rentDueDay = 1;$rentDueDay <= 31; $rentDueDay++ ){
                                                echo "<option value='".$rentDueDay."'>".$rentDueDay."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <?php
                                        $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$'; ?>
                                        <label>Rent Amount (<?php echo $default_symbol ?>)<em class="red-star">*</em></label>
                                        <input id="rent_amount" class="form-control amount number_only" name="rent_amount" type="text"/>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>CAM Amount (<?php echo $default_symbol ?>)</label>
                                        <input name="cam_amount" id="cam_amount" class="form-control amount number_only" type="text"/>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Security Deposite (<?php echo $default_symbol ?>)</label>
                                        <input name="security_amount" id="security_amount" class="form-control amount number_only" type="text"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Form Outer Ends -->
                        <div class="form-outer">
                            <div class="form-hdr">
                              <h3>Rent Increase</h3>
                            </div>
                            <div class="form-data">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Next Rent Increase</label>
                                        <select class="form-control" name="next_rent_incr">
                                            <option value="">Select</option>
                                            <option value="1">1 Months</option>
                                            <option value="2">2 Months</option>
                                            <option value="3">3 Months</option>
                                            <option value="6">6 Months</option>
                                            <option value="9">9 Months</option>
                                            <option value="12">12 Months</option>
                                            <option value="15">15 Months</option>
                                            <option value="18">18 Months</option>
                                            <option value="21">21 Months</option>
                                            <option value="24">24 Months</option>

                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label>Rent Increase</label>
                                        <div class="check-outer">
                                            <input type="radio" name="incr_by" value="flat" class="increased_by"/> <label>Flat</label>
                                        </div>
                                        <div class="check-outer">
                                            <input type="radio" name="incr_by" value="perc" class="increased_by"/> <label>Percentage</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <label class="label_amount">Amount (<?php echo $default_symbol ?>)</label>
                                        <input name="increase_amount" id="increase_amount" class="form-control amount number_only" type="text"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Form Outer Ends -->
                    </form>
                </div>
                <div class="btn-outer text-right">
                    <a class="blue-btn save_lease_btn">Next</a>
                    <a class="grey-btn cancel_lease_btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var date = $.datepicker.formatDate(jsDateFomat, new Date());

    $(".calander").val(date);
</script>
<?php
//include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->