<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/starability-all.css">
<div id="wrapper">
    <!-- Top navigation start -->
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php"); ?>
    <!-- Top navigation end -->

    <style>
        .closeRating{
            font-size: 21px !important;
            color: #fff !important;
            font-weight: 500 !important;
            border: none !important;
            margin-right: -8px;
        }
    </style>
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/shorttermcss.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-pseudo-ripple.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-nao-calendar.css">
    <link rel="stylesheet" href="https://unpkg.com/balloon-css/balloon.min.css">
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">


                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">

                    <div class="main-tabs">


                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div role="tabpanel" class="" id="people-vendor">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse">
                                                <span></span> List of Manual Lease Renew Request</a> <a class="back" style="float: right;" href="/Lease/ViewEditLease">&lt;&lt; Back</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                        <div class="panel-body">
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="apx-table">
                                                                                <div class="table-responsive overflow-unset">
                                                                                    <table id="TenantLease-table" class="table table-bordered">
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!--Tabs Ends -->
                </div>
            </div>
        </div>
    </section>
</div>














<!--Star Modal End-->

<!-- Wrapper Ends -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script>


    $("#people_top").addClass("active");
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->
    $(document).ready(function(){
        getLeaseInfo('all');
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
    var currencyFormatter  = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";



    function getLeaseInfo(status) {
        var table = 'tenant_property';
        var columns = ['id','Tenant Name','Property', 'Unit', 'Rent', 'Lease ID', 'Days Remaining','last_day', 'Status', 'Action'];
        var select_column = ['Edit','Delete'];
        var joins =
            [{table:'tenant_property',column:'user_id',primary:'id',on_table:'users',as :'tp'},
                {table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details',as :'tp1'},
                {table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property',as :'tp3'},
                {table:'tenant_property',column:'user_id',primary:'user_id',on_table:'tenant_lease_details',as :'tp4'},
                {table:'tenant_property',column:'user_id',primary:'user_id',on_table:'tenant_details',as :'tp5'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_where = [] ;
        var extra_columns = [];
        var columns_options = [

            {name:'id',index:'id',alias:'user_id', width:150,hidden:true,align:"center",searchoptions: {sopt: conditions},table:'tp'},

            {name:'Tenant Name',index:'first_name', width:90,align:"left",searchoptions: {sopt: conditions},table:'tp'},
            {name:'Property',index:'property_name', width:150,align:"center",searchoptions: {sopt: conditions},table:'tp3'},
            {name:'Unit',index:'unit_no', width:150,searchoptions: {sopt: conditions},table:'tp1'},
            {name:'Rent',index:'rent_amount', width:150,align:"center",searchoptions: {sopt: conditions},table:'tp4',formatter:currencyFormatter},
            {name:'Lease ID',index:'id',alias:'third', width:150,align:"center",searchoptions: {sopt: conditions},table:'tp4'},
            {name:'Days Remaining',index:'days_remaining', width:150, align:"center",searchoptions: {sopt: conditions},table:'tp4',formatter:dayFormatter},

            {name:'last_day',index:'days_remaining', width:150, align:"center",hidden:true,searchoptions: {sopt: conditions},table:'tp4',formatter:dayFormatter},
            {name:'Status',index:'status', width:150, align:"center",searchoptions: {sopt: conditions},table:'tp5',formatter:statusFormatter5},

            {name:'Action',index:'file_type', width:300,searchoptions: {sopt: conditions},table:table,formatter:selectFormatter1},
        ];
        var ignore_array = [];
        jQuery("#TenantLease-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'true'
            },
            viewrecords: true,
            sortname: 'tenant_property.updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Tenant Lease",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }
    function dayFormatter(cellValue, options, rowObject)
    {


        if(rowObject!==undefined)
        {

            var d = new Date();

            var month = d.getMonth()+1;
            var day = d.getDate();

            var today = d.getFullYear() + '-' +
                ((''+month).length<2 ? '0' : '') + month + '-' +
                ((''+day).length<2 ? '0' : '') + day;
            var lastDay = rowObject.last_day;




            const date1 = new Date(today);
            const date2 = new Date(lastDay);
            const diffTime = Math.abs(date2.getTime() - date1.getTime());
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            return diffDays+' days';




        }










    }
    function statusFormatter5(cellValue, options, rowObject)
    {



        if(rowObject!==undefined)
        {
            console.log(rowObject);
            if(rowObject.Status =='1')
            {
                return 'Move-In';
            }
            else if(rowObject.Status =='2')
            {
                return 'Tenant Renewal Request';
            }
            else
            {
                return "";
            }

        }


    }
    function selectFormatter1(cellValue, options, rowObject)
    {

        if(rowObject !== undefined) {
            if(rowObject.Status =='1')
            {

                var html = "<select editable='1' class='form-control select_options' data-id='"+rowObject.id+"'>"

                html +=  "<option value=''>Select</option>";
                html +=  "<option value='Cancel'>Cancel</option>";

                html +="</select>";

            }
            else
            {
                return "";
            }

            return html;



        }
    }
    $(document).on("change","#TenantLease-table .select_options",function(){

        var id = $(this).attr('data-id');

        bootbox.confirm("Do you want to remove all files?", function (result) {
            if (result == true) {

                $.ajax({
                    url:'/tenantPortal',
                    type: 'POST',
                    data: {
                        "tenant_id": id,
                        "action": 'changeLeaseStatus',
                        "class": 'TenantPortal'
                    },
                    success: function (data) {
                        info = JSON.parse(data);
                        if(info.status=="success")
                        {
                            $('#TenantLease-table').trigger('reloadGrid');

                        }

                    },
                });
            }
        });

    });

</script>


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
