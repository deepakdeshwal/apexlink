<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="wrapper" xmlns="http://www.w3.org/1999/html">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="content-rt">
                        <div class="content-rt-hdr">
                            <div class="breadcrumb">
                                <h3>Tenant >> <span>  Tenant Transfer</span>
                                    <input type="text" class="form-control input-sm easy-search" id="easy_search" placeholder="Easy Search">
                                </h3>
                            </div>
                        </div>

                        <div class="content-data">
                            <!-- Main tabs -->
                            <div class="main-tabs">
                                <div class="form-outer">
                                    <div class="form-hdr">
                                        <h3>Tenant Information</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Tenant Name: <span id="tenant_name"></span></label>

                                            </div>
                                            <div class="col-sm-4">
                                                <label>Property Name :<span id="property_name"></span></label>
                                                <span></span>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Unit# :<span id="unit_name"></span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Balance Due :<span id="balance_due"></span></label>
                                                <span></span>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Rent :<span id="rent_id"></span>
                                                </label>
                                                <span></span>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Security Deposit :<span id="sec_deposit"></span>
                                                  </label>
                                                <span></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Lease Start Date :<span id="start_date"></span></label>
                                                <span></span>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Phone :<span id="phone_num"></span></label>
                                                <span></span>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Entity/Company Name :<span id="company_name"></span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-outer">
                                    <div class="form-hdr">
                                        <h3 >Tenant Information</h3>
                                    </div>
                                    <div class="form-data">
                                            <ul class="nav nav-tabs" id="myTab1" role="tablist" style="">
                                                <li class="active" role="presentation"><a href="#transfer-detail" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">Transfer Details</a></li>
                                                <li role="presentation"><a href="#lease-detail" aria-controls="home" role="tab" data-toggle="tab" class="lease_activate" aria-expanded="false">Lease Details</a></li>
                                            </ul>

                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="transfer-detail">

                                                <div class="form-outer">
                                                <div class="form-hdr">
                                                    <h3>Property Details</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label>Tenant ID</label>
                                                            <input class="form-control" type="text"name="tenant_id" disabled="disabled">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label>Expected / Actual Transfer Date</label>
                                                            <input class="form-control" type="date"name="tenant_id" >
                                                        </div>
                                                    </div>
<!--                                                    <div class="col-sm-12 chages-files-details"></div>-->
                                                </div>
                                            </div>
                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3 class="col-sm-6">Property Information</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="col-sm-3" id="salut_check_lease">
                                                                <label>Portfolios</label>
                                                                <select name="portfolios" class="form-control" id="portfolio">
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-3" id="salut_check_lease">
                                                                <label>Property</label>
                                                                <input type="hidden" name="prop_id" id="prop_id">
                                                                <select name="prop_name" id="prop_name" class="form-control">
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-3" id="salut_check_lease">
                                                                <label>Building</label>
                                                                <select name="building_det" id="building_det" class="form-control">
                                                                    <option></option>

                                                                </select>
                                                            </div>
                                                            <div class="col-sm-3" id="salut_check_lease">
                                                                <label>Unit Type</label>
                                                                <select name="unit_type_id" id="unit_type_id" class="form-control">
                                                                    <option></option>
                                                                </select>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-3" id="salut_check_lease">
                                                                <label>No. of Bedrooms</label>
                                                                <select name="Bedrooms" id="Bedrooms"class="form-control">
                                                                    <option></option>

                                                                </select>
                                                            </div>
                                                            <div class="col-sm-3" id="salut_check_lease">
                                                                <label>No. of Bathrooms</label>
                                                                <select name="Bathrooms" id="Bathrooms" class="form-control">
                                                                    <option</option>

                                                                </select>
                                                            </div>
                                                            <div class="col-sm-3" id="salut_check_lease">
                                                                <label>Floor</label>
                                                                <select name="Floor" class="form-control">
                                                                    <option value="0">1</option>
                                                                    <option value="1">2</option>
                                                                    <option value="2">3</option>
                                                                    <option value="3">4</option>
                                                                    <option value="4">5</option>

                                                                </select>
                                                            </div>
                                                            <div class="col-sm-3" id="salut_check_lease">
                                                                <label>Amenities</label>
                                                                <select name="Amenities" id="Amenities" class="form-control" multiple>
                                                                    <option></option>

                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <input type="button" name="search_btn" id="search_btn" class="search_btn blue-btn" value="Search">

                                                            </div>

                                                        </div>
                                                        <div class="grid-outer" id="search_table" style="display: none;">
                                                            <center class="table-responsive">
                                                                <table class="table table-hover table-dark" id="search_records">
                                                                    <!--<thead>
                                                                    <tr>
                                                                        <th scope="col"><center>Property</center></th>
                                                                        <th scope="col"><center>Type</center> </th>
                                                                        <th scope="col"><center>Building</center> </th>
                                                                        <th scope="col"><center>Unit</center> </th>
                                                                        <th scope="col"><center>Status</center> </th>
                                                                        <th scope="col"><center>Security Deposit</center> </th>
                                                                        <th scope="col"><center>Rent</center> </th>
                                                                    </tr>
                                                                    </thead>-->
                                                                    <!-- <tbody>
                                                                     <tr>
                                                                         <td>WELCOME EMAIL TO NEW COMPANY USER</td>
                                                                         <td><center><img src="http://shrey.apex.local/superadmin/images/edit-icon.png" id="restoreProperty" alt="my image"></center></td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td>WELCOME EMAIL TO NEW COMPANY USER</td>
                                                                         <td><center><img src="http://shrey.apex.local/superadmin/images/edit-icon.png" id="restoreProperty" alt="my image"></center></td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td>WELCOME EMAIL TO NEW COMPANY USER</td>
                                                                         <td><center><img src="http://shrey.apex.local/superadmin/images/edit-icon.png" id="restoreProperty" alt="my image"></center></td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td>WELCOME EMAIL TO NEW COMPANY USER</td>
                                                                         <td><center><img src="http://shrey.apex.local/superadmin/images/edit-icon.png" id="restoreProperty" alt="my image"></center></td>
                                                                     </tr>
                                                                     </tbody>-->
                                                                </table>
                                                                <center>   <span id="no_records" style="display: none;">No Records</span></center>
                                                        </div>
                                                    </div>

                                                    <div class="form-outer col-sm-12" style="display:none;">
                                                        <div class="form-hdr">
                                                            <h3>Primary Tenant Info</h3>
                                                        </div>
                                                        <div class="form-data">
                                                            <div class="col-sm-12 checkandhide">
                                                                <div class="col-sm-3" id="salut_check_lease">
                                                                    <label>Salutation</label>
                                                                    <select name="tenant_salutation" class="form-control">
                                                                        <option value="0">Select</option>
                                                                        <option value="1">Dr.</option>
                                                                        <option value="2">Mr.</option>
                                                                        <option value="3">Mrs.</option>
                                                                        <option value="4">Mr. &amp; Mrs.</option>
                                                                        <option value="5">Ms.</option>
                                                                        <option value="6">Sir.</option>
                                                                        <option value="7">Madam</option>
                                                                        <option value="8">Brother</option>
                                                                        <option value="9">Sister</option>
                                                                        <option value="10">Father</option>
                                                                        <option value="11">Mother</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>First Name<span class="required"> * </span></label>
                                                                    <input type="text" name="tenant_first_name" class="form-control">
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Maiden Name</label>
                                                                    <input type="text" name="tenant_maiden_name[]" class="form-control">
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Last Name<span class="required"> * </span></label>
                                                                    <input type="text" name="tenant_last_name" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12 primary-tenant-phone-row-lease">
                                                                <div class="col-sm-3">
                                                                    <label>Phone Type</label>
                                                                    <select name="tenant_phone_type" class="form-control p-type">
                                                                        <option value="1">Select Type</option>
                                                                        <option value="2">Mobile</option>
                                                                        <option value="3">Work</option>
                                                                        <option value="4">Fax</option>
                                                                        <option value="5">Home</option>
                                                                        <option value="6">Other</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <label>Carrier<span class="required required_carrier"> * </span></label>
                                                                    <select name="tenant_carrier" class="form-control">
                                                                        <option value="1">Select Type</option>
                                                                        <option value="2">Aliant</option>
                                                                        <option value="3">Alltel</option>
                                                                        <option value="4">Ameritech</option>
                                                                        <option value="5">Assurance Wireless</option>
                                                                        <option value="6">AT&T Wireless</option>
                                                                        <option value="7">Bell Mobility</option>
                                                                        <option value="8">Bell South</option>
                                                                        <option value="9">Bluegrass Cellular</option>
                                                                        <option value="10">Boost</option>
                                                                        <option value="11">Cellularone</option>
                                                                        <option value="12">Cellular One MMS</option>
                                                                        <option value="13">Cincinnati Bell</option>
                                                                        <option value="14">Cingular</option>
                                                                        <option value="15">Clearnet</option>
                                                                        <option value="16">Cricket</option>
                                                                        <option value="17">Edge Wireless</option>
                                                                        <option value="18">Fido</option>
                                                                        <option value="19">I Wireless</option>
                                                                        <option value="20">Metro PCS</option>
                                                                        <option value="21">Mobi</option>
                                                                        <option value="22">MT`s Mobility</option>
                                                                        <option value="23">Nextel</option>
                                                                        <option value="24">Ntelos</option>
                                                                        <option value="25">President</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Phone Number</label>
                                                                    <input type="text" name="tenant_phone_number" class="form-control p-number-format" maxlength="12">
                                                                </div>
                                                                <div class="col-sm-1 phone-add-remove-row-lease">
                                                                    <span class="glyphicon glyphicon-remove-sign"></span>
                                                                    <span class="glyphicon glyphicon-plus-sign"></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">

                                                                <div class="col-sm-3">
                                                                    <div class="primary-tenant-email-row-lease" style="min-height: 55px;">
                                                                        <div style="width: 95%; float: left;">
                                                                            <label>Email</label>
                                                                            <input type="email" name="tenant_email" class="form-control">
                                                                        </div>
                                                                        <div class="email-add-remove-row-lease" style="padding: 0px;float: left;">
                                                                            <span class="glyphicon glyphicon-remove-sign"></span>
                                                                            <span class="glyphicon glyphicon-plus-sign"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>



                                                            </div>
                                                            <div class="col-sm-12 checkandhide">
                                                                <div class="col-sm-2">
                                                                    <label>Move-In Date</label>
                                                                    <input type="text" class="form-control primary-tenant-movein-date datepick" name="tenant_movein_date" value="">
                                                                </div>

                                                                <div class="col-sm-2">
                                                                    <div class="primary-tenant-ssnid-row" style="min-height: 55px;">
                                                                        <div class="mb-15" style="width: 150px; float: left;">
                                                                            <label>SSN/SIN/ID</label>
                                                                            <input type="text" name="tenant_ssnid" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <form action="" id="" method="post">
                                                        <input type="hidden" name="tenant_user_id" id="tenant_user_id" value="">
                                                        <div class="row col-sm-12" style="margin-bottom: 0px;">
                                                            <div class="property-status" style="display: none;">
                                                                <input type="checkbox" class="add-more-tenant" name="add_more_tenant" value="1">
                                                                <label style="display: inline;"> Any Additional Tenant ?</label>
                                                            </div>
                                                        </div>

                                                        <div class="form-outer addition_tenant_row" style="display:none;">
                                                            <div class="form-outer">
                                                                <div class="form-hdr">
                                                                    <h3>Additional Tenant</h3>
                                                                </div>
                                                                <div class="form-data">
                                                                    <div class="col-sm-12">
                                                                        <div class="col-sm-3" id="salut_check_lease">
                                                                            <label>Salutation</label>
                                                                            <select name="additional_salutation" class="form-control">
                                                                                <option value="0">Select</option>
                                                                                <option value="1">Dr.</option>
                                                                                <option value="2">Mr.</option>
                                                                                <option value="3">Mrs.</option>
                                                                                <option value="4">Mr. &amp; Mrs.</option>
                                                                                <option value="5">Ms.</option>
                                                                                <option value="6">Sir.</option>
                                                                                <option value="7">Madam</option>
                                                                                <option value="8">Brother</option>
                                                                                <option value="9">Sister</option>
                                                                                <option value="10">Father</option>
                                                                                <option value="11">Mother</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <label>First Name<span class="required"> * </span></label>
                                                                            <input type="text" name="additional_first_name" class="form-control">
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Maiden Name</label>
                                                                            <input type="text" name="additional_maiden_name" class="form-control">
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Last Name<span class="required"> * </span></label>
                                                                            <input type="text" name="additional_last_name" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="col-sm-3">
                                                                            <div class="primary-tenant-ssnid-row4" style="min-height: 55px;">
                                                                                <div class="mb-15" style="width: 95%; float: left;">
                                                                                    <label>SSN/SIN/ID</label>
                                                                                    <input type="text" name="additional_ssn_sin_id[]" class="form-control">
                                                                                </div>
                                                                                <div class="ssnid-add-remove-row" style="padding: 0px;float: left;">
                                                                                    <span class="glyphicon glyphicon-remove-sign"></span>
                                                                                    <span class="glyphicon glyphicon-plus-sign"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <label>Relationship</label>
                                                                            <select class="form-control" name="additional_relationship">
                                                                                <option value="">Select</option>
                                                                                <option value="1">Brother</option>
                                                                                <option value="2">Daughter</option>
                                                                                <option value="3">Employer</option>
                                                                                <option value="4">Father</option>
                                                                                <option value="5">Friend</option>
                                                                                <option value="6">Mentor</option>
                                                                                <option value="7">Mother</option>
                                                                                <option value="8">Neighbor</option>
                                                                                <option value="9">Nephew</option>
                                                                                <option value="10">Niece</option>
                                                                                <option value="11">Owner</option>
                                                                                <option value="12">Partner</option>
                                                                                <option value="13">Sister</option>
                                                                                <option value="14">Son</option>
                                                                                <option value="15">Spouse</option>
                                                                                <option value="16">Teacher</option>
                                                                                <option value="17">Other</option>

                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">

                                                                        <div class="col-sm-12 primary-tenant-phone-row2 p-0">
                                                                            <div class="col-sm-2">
                                                                                <label>Phone Type</label>
                                                                                <select name="additional_phone_type[]" class="form-control p-type2">
                                                                                    <option value="1">Mobile</option>
                                                                                    <option value="2">Work</option>
                                                                                    <option value="3">Fax</option>
                                                                                    <option value="4">Home</option>
                                                                                    <option value="5">Other</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <label>Carrier<span class="addtion_required_carrier required"> * </span></label>
                                                                                <select name="additional_carrier[]" class="form-control">
                                                                                    <option value="{{$k}}">{{$val}}</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <label>Phone Number</label>
                                                                                <input type="text" name="additional_phone_number[]" class="form-control p-number-format" maxlength="12">
                                                                            </div>
                                                                            <div class="col-sm-2 phone-add-remove-row2">
                                                                                <span class="glyphicon glyphicon-remove-sign"></span>
                                                                                <span class="glyphicon glyphicon-plus-sign"></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12 p-0">
                                                                            <div class="col-sm-3 primary-tenant-email-row2" style="min-height: 55px;">
                                                                                <div style="width: 95%; float: left;">
                                                                                    <label>Email</label>
                                                                                    <input type="email" name="additional_email[]" class="form-control">
                                                                                </div>
                                                                                <div class="email-add-remove-row2" style="padding: 0px;float: left;">
                                                                                    <span class="glyphicon glyphicon-remove-sign"></span>
                                                                                    <span class="glyphicon glyphicon-plus-sign"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12 p-0">
                                                                            <div class="col-sm-3">
                                                                                <label>Tenant Status</label>
                                                                                <select name="additional_tenant_status" class="form-control">
                                                                                    <option value="">Select</option><option value="1">Active</option><option value="0">InActive</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-outer addition_tenant_row">
                                                                <input type="button" name="add_additional_tenant_save_tab" class="blue-btn add_additional_tenant_save_tab" value="Add More Additional Tenants">

                                                            </div>
                                                        </div>
                                                    </form>

                                                    <div class="form-outer col-sm-12" style="display: none;">
                                                        <div class="form-hdr">
                                                            <h3 class="col-sm-12 back-building">Lease Dates
                                                                <a href="{{URL::to('/Tenantlisting/Tenantlisting/')}}" class="back-link"><< Back</a></h3>
                                                        </div>

                                                        <div class="form-data">
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-3">
                                                                    <label>Move-In Date<span class="required"> * </span></label>
                                                                    <input type="text" name="lease_move_in" class="form-control datepick " value="{{ App\Http\Helper\Helper::dateFormat(date('Y-m-d'))}}">
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <label>Move-Out/Vacate Date</label>
                                                                    <input type="text" name="lease_move_out" class="form-control datepick lease_move_out">
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <label>Lease Start Date<span class="required"> * </span></label>
                                                                    <input type="text" name="lease_start_date" class="form-control lease_start_date">
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <label>Lease Term<span class="required"> * </span></label>
                                                                    <input type="text" name="lease_term" class="form-control lease_term" value="12">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-3">
                                                                    <label>&nbsp;<span></span></label>

                                                                    <select class="form-control lease_tenure" name="lease_tenure">
                                                                        <option value="1">Month</option>
                                                                        <option value="2">Year</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <label>Lease End Date<span class="required"> * </span></label>
                                                                    <input type="text" name="lease_end_date1" class="form-control datepick lease_end_date calander" disabled="disabled">
                                                                    <input type="hidden" name="lease_end_date" class="datepick lease_end_date" >

                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <label>Notice Period<span class="required"> * </span></label>
                                                                    <select class="form-control lease_notice_period" name="lease_notice_period">
                                                                        <option value="">Select</option>
                                                                        <option value="1">30 days</option>
                                                                        <option value="2">60 days</option>
                                                                        <option value="3" selected="selected">90 days</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <label>Notice Date<span class="required"> * </span></label>
                                                                    <input type="text" name="lease_notice_date1" disabled="disabled" class="form-control datepick lease_notice_date">
                                                                    <input type="hidden" name="lease_notice_date" class="form-control lease_notice_date datepick">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-outer col-sm-12" style="display: none;">
                                                        <div class="form-hdr">
                                                            <h3 class="col-sm-6">Rent Details</h3>
                                                        </div>
                                                        <div class="form-data">
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-3">
                                                                    <label>Rent Due Day</label>
                                                                    <select class="form-control" name="lease_rent_due_day">
                                                                        <option>select</option>
                                                                        {{-- @for($rentDay = 1; $rentDay <= 31; $rentDay++)
                                                                        <option value="{{ $rentDay }}">{{ $rentDay }}</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <label>Rent Amount :
                                                                        <span class="required"> * </span></label>
                                                                    <input type="text" class="form-control lease_rent_amt" name="lease_rent_amt" value="800.00">
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <label>CAM Amount :
                                                                    </label>
                                                                    <input type="text" class="form-control lease_cam_amt" name="lease_cam_amt">
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <label>Security Deposit :

                                                                    </label>
                                                                    <input type="text" class="form-control lease_security_deposite" name="lease_security_deposite" value="200.00">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-outer col-sm-12" style="display: none;">
                                                        <div class="form-hdr">
                                                            <h3 class="col-sm-6">Rent Increase</h3>
                                                        </div>
                                                        <div class="form-data">
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-3">
                                                                    <label>Next Rent Increase</label>
                                                                    <select class="form-control" name="lease_next_rent_incr">
                                                                        <option value="">Select</option>
                                                                        <option value="1">1 Months</option>
                                                                        <option value="2">2 Months</option>
                                                                        <option value="3">3 Months</option>
                                                                        <option value="6">6 Months</option>
                                                                        <option value="9">9 Months</option>
                                                                        <option value="12">12 Months</option>
                                                                        <option value="15">15 Months</option>
                                                                        <option value="18">18 Months</option>
                                                                        <option value="21">21 Months</option>
                                                                        <option value="24">24 Months</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-3 rent-buildincreasesec">
                                                                    <label>Rent Increase</label>
                                                                    <input type="radio" name="lease_rent_incr" value="1" class="lease_rent_incr"><label style="display: inline;" class="rent-flatlabel"> Flat   </label>
                                                                    <input type="radio" name="lease_rent_incr" value="2" class="lease_rent_incr"><label style="display: inline;"  class="rent-flatlabel"> Percentage</label>
                                                                </div>
                                                                <div class="col-sm-3 rent_incr">
                                                                    <label>Amount
                                                                    </label>
                                                                    <input type="text" class="form-control lease_amount" name="lease_amount">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-outer col-sm-12" style="display:none;">
                                                        <div class="form-hdr">
                                                            <h3 class="col-sm-6 back-building ">Late Fee Charges
                                                                <a href="{{URL::to('/Tenantlisting/Tenantlisting/')}}" class="back-link"><< Back</a></h3>
                                                        </div>
                                                        <div class="form-data">
                                                            <div class="col-sm-12 people_charges_condition">
                                                                <div class="col-sm-12">
                                                                    <input type="radio" name="one_time_charges" class="one_time_charges" value="1">
                                                                    <label style="display: inline;"> Apply One Time</label>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="col-sm-1">
                                                                        <input type="radio" name="chages_apply_1_time" class="chages_apply_1_time" id="chages_apply_1_lease" value="1" disabled="disabled">
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <input type="text" name="" placeholder="Example 10.00" class="form-control input1" readonly>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label>Flat Fee
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="col-sm-1">
                                                                        <input type="radio" name="chages_apply_1_time" class="chages_apply_1_time"  id="chages_apply_2_lease" value="2" disabled="disabled">
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <input type="text" name="" placeholder="Example 20" class="form-control input2" readonly>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label> % of Monthly Rent (Example 20)</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 people_charges_condition">
                                                                <div class="col-sm-12">
                                                                    <input type="radio" name="one_time_charges" class="one_time_charges" value="2">
                                                                    <label style="display: inline;"> Apply Daily</label>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="col-sm-1">
                                                                        <input type="radio" name="chages_apply_daily" class="chages_apply_daily" id="chages_apply_daily1_lease" value="1" disabled="disabled">
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <input type="text" name="" placeholder="Example 10.00" class="form-control input3" readonly>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label>Flat Fee
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="col-sm-1">
                                                                        <input type="radio" name="chages_apply_daily" value="2" class="chages_apply_daily" id="chages_apply_daily2_lease" disabled="disabled">
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <input type="text" name="" placeholder="Example 20" class="form-control input4" readonly>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label> % of Monthly Rent (Example 20)</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-5">
                                                                    <label>Grace Period</label>
                                                                    <input type="text" name="grace_period" class="form-control grace_period" placeholder="Example 10" readonly>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="form-outer col-sm-12" style="display: none;">
                                                        <div class="form-hdr">
                                                            <h3 class="col-sm-6">Tax Pass Through Details</h3>
                                                        </div>
                                                        <div class="form-data">
                                                            <div class="col-sm-12">
                                                                <form method="POST" id="apptaxform" action="">
                                                                    <input type="hidden" value="<?php echo $_GET['tenant_id']; ?>" name="tenantapplytaxid" class="tenantapplytaxid">
                                                                    <label>Select Type</label>
                                                                    <div class="row col-sm-12">
                                                                        <div class="col-sm-3">
                                                                            <div class="col-sm-1">
                                                                                <input type="radio" name="chages_type_1_time" class="chages_type_1_time" id="chages_type_1_lease" value="1">
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <span>Flat Amount</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row col-sm-12">
                                                                        <div class="col-sm-3">
                                                                            <div class="col-sm-1">
                                                                                <input type="radio" name="chages_type_1_time" class="chages_type_1_time" id="chages_type_1_lease" value="1">
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <span>Percentage of Monthly Rent</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-3">
                                                                        <label>Amount
                                                                        </label>
                                                                        <input type="text" name="" class="form-control chargeamountlease">
                                                                    </div>

                                                                    <div class="col-sm-3">
                                                                        <label>Frequency</label>
                                                                        <select class="form-control chargefrequencylease"><option value="">-None-</option><option value="1">One Time</option><option value="2">Monthly</option></select>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <div class="col-sm-12">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-outer col-sm-12" style="display: none;">
                                                        <div class="form-hdr">
                                                            <div class="col-sm-6" style="color: #2d3091;font-size: 16px;">Charges</div>
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-12" style="margin-bottom: 0px;">
                                                                    <input type="button" data-toggle="collapse" href="#chagescontainer_lease" aria-expanded="false" aria-controls="chagescontainer_lease" name="" class="blue-btn pull-right" value="Add Charges">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-data">
                                                            <div class="col-sm-12 collapse" id="chagescontainer_lease">
                                                                <div class="col-sm-3">
                                                                    <label>Select Charge Code <span class="glyphicon glyphicon-plus-sign" data-toggle="modal" data-target="#addchargescode" data-backdrop="static" data-keyboard="false"></span></label>
                                                                    <select class="form-control chargecode"><option value="">Select</option>
                                                                        <option value="1">A/C-A/C</option></select>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <label>Frequency</label>
                                                                    <select class="form-control chargefrequency"><option value="">Select</option><option value="1">One Time</option><option value="2">Monthly</option></select>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <label>Amount

                                                                    </label>
                                                                    <input type="text" name="" class="form-control chargeamount">
                                                                </div>
                                                                <div class="col-sm-12">&nbsp;</div>
                                                                <div class="col-sm-12">
                                                                    <input type="button" class="blue-btn apply_charges_save" value="Save">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <table class="chargeTable1">
                                                                    <thead class="chargeTableHead1">
                                                                    <tr><th></th><th>Charges</th><th>Frequency</th><th>Start Date</th><th>End Date</th><th>Amount

                                                                        </th></tr>
                                                                    </thead>
                                                                    <tbody class="chargeTableBody1">
                                                                    </tbody>
                                                                </table>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-outer col-sm-12" style="display: none;">
                                                        <div class="form-hdr">
                                                            <h3 class="col-sm-6">Notes</h3>
                                                        </div>
                                                        <div class="form-data latefeenotebar">
                                                            <div class="col-sm-12 latefeenote">
                                                                <div class="col-sm-4 feenote">
                                                                    <textarea name="latefeenote[]" class="form-control" style="display: inline; width: 95%" rows="5"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-outer col-sm-12" style="display: none;">
                                                        <div class="form-hdr">
                                                            <h3 class="col-sm-6">Files</h3>
                                                        </div>
                                                        <div class="form-data">
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-2" style="width: 110px;">
                                                                    <input type="file" class="charges-add-files" name="latefeefiles[]" multiple accept=".xlsx,.xls,.doc, .docx,.txt,.pdf">
                                                                    <input type="button" class="charges-add-files-btn" value="Add Files...">
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <input type="button" name="" class="btn btn-default remove-all-files" value="Remove All Files">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 chages-files-details"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-outer">
                                                     <div class="btn-outer">
                                                        <input type="button" name="save_transfer" class="save_transfer blue-btn" value="Save">
                                                        <input type="button" name="" class="grey-btn" value="Cancel">

                                                    </div>

                                                </div>
                                            </div>
                                           <!-- <div class="form-data">
                                                <div class="col-sm-12">
                                                    <div class="col-sm-2" style="width: 110px;">
                                                        <label>Property</label>
                                                        <span id="property_lease"></span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Unit</label>
                                                        <span id="unit_lease"></span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Building Name</label>
                                                        <span id="building_lease"></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 chages-files-details"></div>
                                            </div>-->
                                            <div role="tabpanel" class="tab-pane" id="lease-detail">
                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>Property Details</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Property:<span id="new_prop"></span></label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Unit:<span id="new_unit"></span></label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Building Name:<span id="new_build"></span></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>Primary Tenant Info</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <form id="transferTenantLease">
                                                                <label>Salutation</label>
                                                                <select class="form-control transfer_salutation" name="transfer_salutation">
                                                                        <option value="Select">Select</option>
                                                                        <option value="Dr.">Dr.</option>
                                                                        <option value="Mr.">Mr.</option>
                                                                        <option value="Mrs.">Mrs.</option>
                                                                        <option value="Mr. & Mrs.">Mr. &amp; Mrs.</option>
                                                                        <option value="Ms.">Ms.</option>
                                                                        <option value="Sir">Sir.</option>
                                                                        <option value="Madam">Madam</option>
                                                                        <option value="Brother">Brother</option>
                                                                        <option value="Sister">Sister</option>
                                                                        <option value="Father">Father</option>
                                                                        <option value="Mother">Mother</option>
                                                                    </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>First Name<em class="red-star">*</em></label>
                                                                <input class="form-control transfer_firstName" name="transfer_firstName" type="text">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Middle Name</label>
                                                                <input class="form-control transfer_middleName" name="transfer_middleName" type="text">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Last Name<em class="red-star">*</em></label>
                                                                <input class="form-control transfer_lastName" name="transfer_lastName" type="text">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Phone Type</label>
                                                                <select class=" form-control" name="transfer_phoneType">
                                                                    <option value="1">Select Type</option>
                                                                    <option value="2">Mobile</option>
                                                                    <option value="3">Work</option>
                                                                    <option value="4">Fax</option>
                                                                    <option value="5">Home</option>
                                                                    <option value="6">Other</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Carrier</label>
                                                                <select class=" form-control" name="transfer_carrier">
                                                                        <option value="1">Select Type</option>
                                                                        <option value="2">Aliant</option>
                                                                        <option value="3">Alltel</option>
                                                                        <option value="4">Ameritech</option>
                                                                        <option value="5">Assurance Wireless</option>
                                                                        <option value="6">AT&T Wireless</option>
                                                                        <option value="7">Bell Mobility</option>
                                                                        <option value="8">Bell South</option>
                                                                        <option value="9">Bluegrass Cellular</option>
                                                                        <option value="10">Boost</option>
                                                                        <option value="11">Cellularone</option>
                                                                        <option value="12">Cellular One MMS</option>
                                                                        <option value="13">Cincinnati Bell</option>
                                                                        <option value="14">Cingular</option>
                                                                        <option value="15">Clearnet</option>
                                                                        <option value="16">Cricket</option>
                                                                        <option value="17">Edge Wireless</option>
                                                                        <option value="18">Fido</option>
                                                                        <option value="19">I Wireless</option>
                                                                        <option value="20">Metro PCS</option>
                                                                        <option value="21">Mobi</option>
                                                                        <option value="22">MT`s Mobility</option>
                                                                        <option value="23">Nextel</option>
                                                                        <option value="24">Ntelos</option>
                                                                        <option value="25">President</option>
                                                                    </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Phone Number<em class="red-star">*</em></label>
                                                                <input class="form-control add-input transfer_phone_no" name="transfer_phone_no" type="text">
                                                                <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2 clear">
                                                                <label>Email</label>
                                                                <input class="form-control add-input transfer_email" name="transfer_email" type="text">
                                                                <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2 clear">
                                                                <label>Move-In Date</label>
                                                                <input class=" form-control transfer_moveIn calander" name="transfer_email">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>SSN/SIN/ID</label>
                                                                <input type="text" class=" form-control transfer_ssn" name="transfer_ssn">
                                                            </div>
                                                            <div class="form-outer2">
                                                                <div class="col-sm-12">
                                                                    <div class="check-outer">
                                                                        <input type="checkbox" name="entity_comapny">
                                                                        <label>Check this box if this Tenant is an Entity/Company</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="btn-outer">
                                                        <div class="check-outer">
                                                            <input type="checkbox" name="additional_tenant">
                                                            <label>Any Additional Tenant ?</label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- Form Outer Ends-->


                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>Lease Dates</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Move-In Date<em class="red-star">*</em></label>
                                                                <input class="form-control calander" name="transfer_move_in">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Move-Out/Vacate Date</label>
                                                                <input class="form-control calander" name="transfer_move_out">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Lease Start Date<em class="red-star">*</em></label>
                                                                <input class="form-control calander" name="transfer_start_date_1">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Lease Term<em class="red-star">*</em></label>
                                                                <input class=" form-control" name="transfer_lease_term_1">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>&nbsp;</label>
                                                                <input class=" form-control" name="transfer_tenure">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Lease End Date<em class="red-star">*</em></label>
                                                                <input class="form-control calander"  name="transfer_lease_end_Date">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Notice Period</label>
                                                                <select class=" form-control" name="transfer_notice_period">
                                                                    <option value="0">Select</option>
                                                                    <option value="1">30 Days</option>
                                                                    <option value="2">60 Days</option>
                                                                    <option value="3">90 Days</option></select>

                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Notice Date</label>
                                                                <input class="form-control calander" type="text"name="transfer_notice_date">
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- Form Outer Ends-->


                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>Rent Details</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Rent Due Day</label>
                                                                <select class=" form-control transfer_rent_due" name="transfer_rent_due_day">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                    <option value="19">19</option>
                                                                    <option value="20">20</option>
                                                                    <option value="21">21</option>
                                                                    <option value="22">22</option>
                                                                    <option value="23">23</option>
                                                                    <option value="24">24</option>
                                                                    <option value="25">25</option>
                                                                    <option value="26">26</option>
                                                                    <option value="28">28</option>
                                                                    <option value="29">29</option>
                                                                    <option value="30">30</option>
                                                                    <option value="31">31</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Rent Amount (USh)</label>
                                                                <input class="form-control transfer_rent_amount" type="text" name="transfer_rent_amount">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Security Deposite (USh)</label>
                                                                <input class="form-control transfer_sec_dep" type="text" name="transfer_move_in" name="transfer_security_deposite">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>CAM Amount (USh)</label>
                                                                <input class="form-control transfer_cam_amount" type="text" name="transfer_cam_amount">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- Form Outer Ends-->

                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>Rent Increase</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Next Rent Increase</label>
                                                                <select class=" form-control" name="transfer_next_rent_increase" >
                                                                    <option value="0">1 Month</option>
                                                                    <option value="2">2 Months</option>
                                                                    <option value="3">3 Months</option>
                                                                    <option value="4">6 Months</option>
                                                                    <option value="5">9 Months</option>
                                                                    <option value="6">12 Months</option>
                                                                    <option value="7">15 Months</option>
                                                                    <option value="8">18 Months</option>
                                                                    <option value="9">21 Months</option>
                                                                    <option value="10">24 Months</option>

                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Rent Increase</label>
                                                                <div class="check-outer">
                                                                    <input name="transfer_rent" value="flat" class="flat_transfer_rent" type="radio"><label>Flat</label>

                                                                </div>
                                                                <div class="check-outer">
                                                                    <input name="transfer_rent" value="perc" class="per_transfer_rent" type="radio"><label>Percentage</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Amount (<span id="ush_per"></span>)<em class="red-star">*</em></label>
                                                                <input class="form-control" type="text" name="transfer_amount">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- Form Outer Ends-->


                                                <div class="form-outer form-outer2">
                                                    <div class="form-hdr">
                                                        <h3>Late Fee Charges</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="check-outer">
                                                                    <input type="checkbox" class="transfer_oneTym"> <label>Apply One Time</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="radio-input-label">
                                                                    <input type="radio" name="transfer_late_fee"><input type="text"> <label>Flat Fee (KZT) (Example 10.00)</label>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <div class="radio-input-label">
                                                                    <input type="radio" name="transfer_late_fee"><input  type="text"> <label>% of Monthly Rent (Example 20)</label>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <div class="check-outer">
                                                                    <input type="checkbox"> <label>Apply Daily</label>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <div class="radio-input-label">
                                                                    <input type="radio"><input type="text"> <label>Flat Fee (KZT) (Example 10.00)</label>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <div class="radio-input-label">
                                                                    <input type="radio"><input type="text"> <label>% of Monthly Rent (Example 20)</label>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <div class="radio-input-label">
                                                                    <label>Grace Period </label> <input  type="text" name="transfer_grace_period">
                                                                </div>
                                                            </div>


                                                        </div>
                                                        <div class="btn-outer">
                                                            <button class="blue-btn">Apply</button>
                                                            <button class="grey-btn">Cancel</button>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- Form Outer Ends-->


                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>Tax Pass Through Details</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label>Select Type</label>
                                                                <div class="check-outer">
                                                                    <input type="radio" name="tax_pass">
                                                                    <label>Flat Amount</label>
                                                                </div>
                                                                <div class="check-outer clear">
                                                                    <input type="radio" name="tax_pass">
                                                                    <label>Percentage of Monthly Rent</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-outer2">
                                                                <div class="col-sm-12">&nbsp;</div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Amount (USh)</label>
                                                                <input class="form-control" type="text" name="transfer_overall_amount">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Frequency</label>
                                                                <select class="form-control" name="transfer_frequency_in">
                                                                    <option value="0">-None-</option>
                                                                    <option value="1">One Time</option>
                                                                    <option value="2">Monthly</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- Form Outer Ends-->

                                                <div class="form-outer form-outer2">
                                                    <div class="form-hdr">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <h3>Charges</h3>
                                                            </div>
                                                            <div class="col-sm-2 pull-right text-right">
                                                                <button class="blue-btn" type="button">Add charges</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-dark">
                                                                    <thead>
                                                                    <tr>
                                                                        <th scope="col"><input type="checkbox"></th>
                                                                        <th scope="col">Charges</th>
                                                                        <th scope="col">Frequency</th>
                                                                        <th scope="col">Start Date</th>
                                                                        <th scope="col">End Date</th>
                                                                        <th scope="col">Amount(USh)</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <tr>
                                                                        <td><input type="checkbox"></td>
                                                                        <td></td>
                                                                        <td>One Time</td>
                                                                        <td></td>
                                                                        <td><i class="fa fa-pencil-square-o" aria-hidden="true"></i></td>
                                                                        <td>0.00 <i class="fa fa-pencil-square-o" aria-hidden="true"></i></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Form Outer Ends-->

                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>Notes</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <textarea class="form-control" name="transfer_notes"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- Form Outer Ends-->

                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>File</h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <input type="file" name="transfer_image[]" value="Add Files..." class="green-btn" multiple>
                                                                <button class="orange-btn">Remove All Files...</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- Form Outer Ends-->
                                                <div class="btn-outer">
                                                    <input type="button" name="save" class="blue-btn" id="tenantTrns" value="Save">
                                                    <input type="button" name="" class="grey-btn" value="Cancel">
                                                    </form>
                                                </div>

                                            </div>
                                        </div>




                                    </div>
                                </div>


                                </div>

                        </div>
                    </div>
    </section>
</div>
<!-- Wrapper Ends -->
<script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });

        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
        var edit_tab = localStorage.getItem('approve_active');
        console.log(edit_tab);
        console.log('----');
        if (edit_tab !== "one") {
            $(".lease_activate").trigger("click");
        }
    });



</script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenant_transfer.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
<script>
    $('.calander').datepicker({
        yearRange: '1919:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('input[name="birth"]').datepicker({
        yearRange: '1900:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    $(".calander").val(date);
</script>


<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->