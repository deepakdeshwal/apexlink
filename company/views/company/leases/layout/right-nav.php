<!--- Right Quick Links ---->
<div class="right-links-outer hide-links">
    <div class="right-links">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
        <i class="fa fa-angle-right" aria-hidden="true"></i>
    </div>
    <div id="RightMenu" class="box2">
        <h2>LEASES</h2>
        <div class="list-group panel">
            <!-- Two Ends-->
            <a href="/Lease/Leases" class="list-group-item list-group-item-success strong collapsed" >New Lease</a>
            <!-- Two Ends-->

            <!-- Three Starts-->
            <a id="new_building_href" href="/GuestCard/NewGuestCard" class="list-group-item list-group-item-success strong collapsed" >New Guest Card</a>
            <!-- Three Ends-->
            <!-- Four Starts-->
            <a href="/RentalApplication/RentalApplication" class="list-group-item list-group-item-success strong collapsed" >New Rental Application</a>
            <!--<a href="/RentalApplication/RentalApplication" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">New Rental Application</a>-->
            <!-- Four Ends-->

            <!-- Five Starts-->
            <a href="#" id="" class="list-group-item list-group-item-success strong collapsed">Unit Vacancy Details</a>
            <!-- Five Ends-->

            <!-- Six Starts-->
            <a href="/Lease/LeasesAffordability" class="list-group-item list-group-item-success strong collapsed">Lease Affordability Calculator</a>
            <!-- Six Ends-->

            <!-- Seven Starts-->
            <a id="" href="/Lease/ManualLeaseRequest" class="list-group-item list-group-item-success strong collapsed" >Manual Lease Renewal Request</a>
            <!-- Seven Ends-->

            <!-- Seven Starts-->
            <a id="" href="/Lease/RenewLeaseRequest" class="list-group-item list-group-item-success strong collapsed" >Lease Renewal Request From Tenant Portal.</a>
            <!-- Seven Ends-->
        </div>
    </div>
</div>
<!--- Right Quick Links ---->