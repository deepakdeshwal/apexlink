<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
//dd($getBankdetails);
?>

<style>
    .ui-datepicker .ui-datepicker-title select {
        color: #000;
    }
    .ui-timepicker-container {
        z-index: 1600 !important; /* has to be larger than 1050 */
    }
    .panel-htop .combo-panel {
        margin-top: unset !important;
        margin-left: unset !important;
    }
    span.textbox.combo {
        position: relative;
        position: relative;
        border: 1px solid #95B8E7;
        background-color: #fff;
        vertical-align: middle;
        display: inline-block;
        overflow: hidden;
        white-space: nowrap;
        margin: 0;
        padding: 0;
        -moz-border-radius: 5px 5px 5px 5px;
        -webkit-border-radius: 5px 5px 5px 5px;
        border-radius: 5px 5px 5px 5px;
        width:100%;
    }


</style>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");


    ?>


    <!-- Top navigation end -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 bread-search-outer">
                    <div class="col-sm-8">
                        <div class="breadcrumb-outer">
                            Property Module >> <span>Property Listing</span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="easy-search">
                            <input placeholder="Easy Search" type="text" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">

                </div>
                <div class="col-sm-12">
                    <div class="content-section">
                        <div class="accordion-form">
                            <div class="accordion-outer">
                                <div class="bs-example">
                                    <div class="panel-group" id="accordion">
                                        <!-- General Information -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" >
                                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> General Information</a> <a class="back" href="/Property/PropertyModules"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                <div class="panel-body">
                                                    <input type="hidden" id="property_editunique_id" value="<?= $_GET['id'] ?> ">
                                                    <form action="" method="POST" id="addPropertyForm">
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Property ID <em class="red-star">*</em></label>
                                                                    <input class="form-control" type="text"  maxlength="20" name="property_id" placeholder="Auto Generated" value="<?= $data['property_id']; ?>"/>
                                                                    <span class="property_idErr error"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Property Name <em class="red-star">*</em></label>
                                                                    <input class="form-control capital" type="text" id="property_name" maxlength="150" name="property_name" placeholder="Eg: The Fairmont Waterfront" value="<?= $data['property_name']; ?>"/>
                                                                    <span class="property_nameErr error"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Legal Name</label>
                                                                    <input class="form-control capital" type="text" id="legal_name" maxlength="150" name="legal_name" placeholder="Eg: The Fairmont Waterfront" value="<?= $data['legal_name']; ?>"/>
                                                                    <span class="legal_nameErr error"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Portfolio Name <em class="red-star">*</em> <a id="Newportfolio" class="pop-add-icon" onclick="clearPopUps('#NewportfolioPopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                    <select class="form-control" id='portfolio_name_options' name='portfolio_id'>

                                                                    </select>
                                                                    <div class="add-popup NewportfolioPopupClass" id='NewportfolioPopup'>
                                                                        <h4>Add New Portfolio</h4>
                                                                        <div class="add-popup-body">

                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>Portfolio ID<em class="red-star">*</em></label>
                                                                                    <input class="form-control customValidatePortfoio" type="text" data_required="true" data_max="20" name='@portfolio_id' id='portfolio_id' readonly/>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label> Portfolio Name <em class="red-star">*</em></label>
                                                                                    <input class="form-control capital customValidatePortfoio" type="text" data_required="true" data_max="50" name='@portfolio_name' id='portfolio_name' placeholder="Add New Portfolio Name"/>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                                <div class="btn-outer text-right">
                                                                                    <input type="button" id='NewportfolioPopupSave' class="blue-btn" value="Save" />
                                                                                    <button rel="NewportfolioPopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                    <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Property For Sale</label>
                                                                    <select class="form-control" name='property_for_sale' id="property_for_sale">
                                                                        <option  value="No" <?php
                                                                        if ($data['property_for_sale'] == 'No') {
                                                                            echo "selected";
                                                                        } else {
                                                                            echo "";
                                                                        }
                                                                        ?> >No</option>
                                                                        <option  value="Yes" <?php
                                                                        if ($data['property_for_sale'] == 'Yes') {
                                                                            echo "selected";
                                                                        } else {
                                                                            echo "";
                                                                        }
                                                                        ?> >Yes</option>
                                                                        <option  value="Other" <?php
                                                                        if ($data['property_for_sale'] == 'Other') {
                                                                            echo "selected";
                                                                        } else {
                                                                            echo "";
                                                                        }
                                                                        ?> >Other</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Property Parcel #</label>
                                                                    <input class="form-control" type="text" id="property_parcel_number" placeholder="Property Parcel" maxlength="200" name="property_parcel_number" value="<?= $data['property_parcel_number']; ?>"/>
                                                                    <span class="property_parcel_numberErr error"></span>
                                                                </div>
                                                                <?php
                                                                $i=0;
                                                                if (!empty($data['property_tax_ids'])) :
                                                                    foreach ($data['property_tax_ids'] as $proptaxidds):
                                                                        ?>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3 multiplepropertydiv" id="multiplepropertydivId">
                                                                            <label>Property Tax ID </label>
                                                                            <input class="form-control add-input" type="text" id="property_tax_id" maxlength="20" name="property_tax_ids" placeholder="Property Tax" value="<?= $proptaxidds; ?>"/>
                                                                            <span class="property_tax_idsErr error"></span>
                                                                            <?php
                                                                            if($i == 0){?>
                                                                                <a id= 'multiplepropertytax' class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                                <a style='display: none;' id= 'multiplepropertytaxcross' class="add-icon" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>

                                                                            <?php  }else{?>
                                                                                <a  id= 'multiplepropertytaxcross' class="add-icon" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>

                                                                            <?php }
                                                                            ?>


                                                                        </div>
                                                                        <?php
                                                                        $i++;
                                                                    endforeach;
                                                                else:
                                                                    ?>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3 multiplepropertydiv" id="multiplepropertydivId">
                                                                        <label>Property Tax ID </label>
                                                                        <input class="form-control add-input" type="text" id="property_tax_id" maxlength="20" name="property_tax_ids" placeholder="Property Tax" value=""/>
                                                                        <span class="property_tax_idsErr error"></span>
                                                                        <a id= 'multiplepropertytax' class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                        <a style='display: none;' id= 'multiplepropertytaxcross' class="add-icon" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>

                                                                    </div>
                                                                <?php endif;
                                                                ?>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Short-Term Rental</label>
                                                                    <select class="form-control" name='is_short_term_rental' id="is_short_term_rental">
                                                                        <option value="0" <?php
                                                                        if ($data['is_short_term_rental'] == 0) {
                                                                            echo "selected";
                                                                        } else {
                                                                            echo "";
                                                                        }
                                                                        ?>>No</option>
                                                                        <option value="1" <?php
                                                                        if ($data['is_short_term_rental'] == 1) {
                                                                            echo "selected";
                                                                        } else {
                                                                            echo "";
                                                                        }
                                                                        ?>>Yes</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Zip/Postal Code</label>
                                                                    <input class="form-control zipcode" type="text" maxlength="9" id="zipcode" name="zipcode" value="<?= $data['zipcode']; ?>"/>
                                                                    <span class="zipcodeErr error"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Country</label>
                                                                    <input class="form-control zipcountry" type="text" maxlength="50" id="country" name="country" value="<?= $data['country']; ?>"/>
                                                                    <span class="countryErr error"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>State / Province</label>
                                                                    <input class="form-control zipstate" type="text" maxlength="50" id="state" name="state" value="<?= $data['state']; ?>"/>
                                                                    <span class="stateErr error"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>City</label>
                                                                    <input class="form-control zipcity" type="text" maxlength="50" id="city" name="city" value="<?= $data['city']; ?>"/>
                                                                    <span class="cityErr error"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Street Address 1</label>
                                                                    <input class="form-control capital" type="text" maxlength="150" id="street_address1" name="address1" placeholder="Eg: Street address 1" value="<?= $data['address1']; ?>"/>
                                                                    <span class="address1Err error"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Street Address 2</label>
                                                                    <input class="form-control capital" type="text" maxlength="150" id="street_address2" name="address2" placeholder="Eg: Street address 2" value="<?= $data['address2']; ?>"/>
                                                                    <span class="address2Err error"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Street Address 3</label>
                                                                    <input class="form-control capital" type="text" maxlength="150" id="street_address3" name="address3" placeholder="Eg: Street address 3" value="<?= $data['address3']; ?>"/>
                                                                    <span class="address3Err error"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Street Address 4</label>
                                                                    <input class="form-control capital" type="text" maxlength="150" id="street_address4" name="address4" placeholder="Eg: Street address 4" value="<?= $data['address4']; ?>"/>
                                                                    <span class="address4Err error"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Manager Name
                                                                        <a id="Newmanager" class="pop-add-icon" onclick="clearPopUps('#NewmanagerPopup')" href="javascript:;"> <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                        <em class="red-star">*</em>
                                                                    </label>
                                                                    <span id="dynamic_manager">
                                                                            <select name="manager_id" multiple="multiple" class="3col active" id="select_mangers_options">
                                                                            </select></span>
                                                                    <span class="manager_idErr error"></span>
                                                                    <div class="add-popup NewmanagerPopupClass" id='NewmanagerPopup'>
                                                                        <h4>Add New Manager</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>First Name<em class="red-star">*</em></label>
                                                                                    <input class="form-control capital customValidateManager" type="text" data_required="true" data_max="30" name='@first_name'/>
                                                                                    <span class="customError required"></span>
                                                                                    <span class="first_nameErr error"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label> Last Name <em class="red-star">*</em></label>
                                                                                    <input class="form-control capital customValidateManager" type="text" data_required="true" data_max="30" name='@last_name'/>
                                                                                    <span class="customError required"></span>
                                                                                    <span class="last_nameErr error"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label> Email <em class="red-star">*</em></label>
                                                                                    <input class="form-control customValidateManager capitalremove" type="text" data_required="true" data_max="50" data_email="true" name='@email'/>
                                                                                    <span class="customError required"></span>
                                                                                    <span class="emailErr error"></span>
                                                                                </div>
                                                                                <div class="btn-outer text-right">
                                                                                    <button type="button" id='NewmanagerPopupSave' class="blue-btn">Save</button>
                                                                                    <button rel="NewmanagerPopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                    <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Country Code</label>
                                                                    <select class='form-control' name='country_code' id="country_code">

                                                                    </select>
                                                                    <span class="country_codeErr error"></span>
                                                                </div>
                                                                <?php
                                                                $p=0;
                                                                if (!empty($data['phone_number'])):
                                                                    foreach ($data['phone_number'] as $phone):
                                                                        ?>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3 multiplephonediv" id="multiplephoneIDD">
                                                                            <label>Phone Number</label>
                                                                            <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control add-input phone_number phone_number_general" id="phone_number" name="phone_number" value="<?= $phone; ?>" autocomplete="off">
                                                                            <?php
                                                                            if($p == 0){ ?>
                                                                                <a id='multiplephoneno' class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                                <a id='multiplephonenocross'  class="add-icon" style='display: none;' href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>

                                                                            <?php   }else{ ?>
                                                                                <a id='multiplephonenocross'  class="add-icon" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>

                                                                            <?php  }
                                                                            ?>

                                                                        </div>
                                                                        <?php
                                                                        $p++;
                                                                    endforeach;
                                                                else:
                                                                    ?>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3 multiplephonediv" id="multiplephoneIDD">
                                                                        <label>Phone Number</label>
                                                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control add-input phone_number phone_number_general" id="phone_number" name="phone_number" value="" autocomplete="off">

                                                                        <a id='multiplephoneno' class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                        <a id='multiplephonenocross' style='display: none;' href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>

                                                                    </div>
                                                                <?php endif;
                                                                ?>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Manager's Email</label>
                                                                    <input class="form-control capital" type="text" maxlength="150" id="manager_email" name="manager_email"placeholder="Email" value="<?= $data['manager_email']; ?>" readonly/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Fax Number</label>
                                                                    <input class="form-control phone_number" type="text" maxlength="12" placeholder="Fax Number" fixedlength="12" id="fax" name="fax" value="<?= $data['fax']; ?>">
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Property Price <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                                                    <input class="form-control amount number_only" type="text" id="property_price" name="property_price" placeholder="Eg: 500.00" value="<?= $data['property_price']; ?>"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Attach Group <a id='Newattach' class="pop-add-icon" onclick="clearPopUps('#NewattachPopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label><span id="dynamic_groups">
                                                                            <select name="attach_groups" multiple="multiple"  id="select_group_options">
                                                                            </select></span>
                                                                    <div class="add-popup propertyGroupAddClass" id='NewattachPopup'>
                                                                        <h4>Add New Property Group</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>Group Name<em class="red-star">*</em></label>
                                                                                    <input class="form-control capital customValidateGroup" type="text" data_required="true" data_max="150" name='@group_name'/>
                                                                                    <span class="customError required" aria-required="true"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label> Description <em class="red-star">*</em></label>
                                                                                    <input class="form-control customValidateGroup capital" type="text" data_required="true" data_max="150" name='@description'/>
                                                                                    <span class="customError required" aria-required="true"></span>
                                                                                </div>
                                                                                <div class="btn-outer text-right">
                                                                                    <button type="button" class="blue-btn" id='NewattachPopupSave'>Save</button>
                                                                                    <button rel="propertyGroupAddClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                    <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Property Type <a id='Newtype' class="pop-add-icon" onclick="clearPopUps('#NewtypePopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                    <select class='form-control' id='property_type_options' name='property_type'></select>
                                                                    <div class="add-popup NewtypePopupClass" id='NewtypePopup'>
                                                                        <h4>Add New Property Type</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>Property Type<em class="red-star">*</em></label>
                                                                                    <input class="form-control capital customValidatePropertyType" data_required="true" data_max="150" type="text" name='@property_type'/>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label> Description <em class="red-star">*</em></label>
                                                                                    <input class="form-control capital customValidatePropertyType" data_required="true" data_max="200" type="text" name='@description'/>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                                <div class="btn-outer text-right">
                                                                                    <button type="button" class="blue-btn" id='NewtypePopupSave'>Save</button>
                                                                                    <button rel="NewtypePopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                    <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Property Style <a id="Newprofile" class="pop-add-icon" onclick="clearPopUps('#NewpstylePopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a> </label>
                                                                    <select class='form-control' id='property_style_options' name='property_style'></select>
                                                                    <div class="add-popup NewpstylePopupClass" id='NewpstylePopup'>
                                                                        <h4>Add New Property Style</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>Property Style <em class="red-star">*</em></label>
                                                                                    <input class="form-control capital customValidatePropertyStyle" data_required="true" data_max="150" type="text" name='@property_style'/>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label>Description <em class="red-star">*</em></label>
                                                                                    <input class="form-control capital customValidatePropertyStyle" data_required="true" data_max="200" type="text" name="@description"/>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                                <div class="btn-outer text-right">
                                                                                    <button type="button" id='NewpstylePopupSave' class="blue-btn">Save</button>
                                                                                    <button rel="NewpstylePopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                    <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Property Sub-Type <a id='Newsubtype' class="pop-add-icon" onclick="clearPopUps('#NewsubtypePopup')" href=""><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                    <select class='form-control' id='property_subtype_options' name='property_subtype'></select>
                                                                    <div class="add-popup NewsubtypePopupClass" id='NewsubtypePopup'>
                                                                        <h4>Add New Property Sub-Type</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>Property Sub-Type<em class="red-star">*</em></label>
                                                                                    <input class="form-control capital customValidatePropertySubType" data_required="true" data_max="150" type="text" name="@property_subtype"/>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label> Description <em class="red-star">*</em></label>
                                                                                    <input class="form-control capital customValidatePropertySubType" data_required="true" data_max="200" type="text" name="@description"/>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                                <div class="btn-outer text-right">
                                                                                    <button type="button" class="blue-btn" id="NewsubtypePopupSave">Save</button>
                                                                                    <button rel="NewsubtypePopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                    <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Year Built</label>
                                                                    <?php
                                                                    $currentYear=date('Y');
                                                                    $yearArray = range(1900, $currentYear);
                                                                    ?>
                                                                    <select class="form-control" id="year_built" name="property_year">
                                                                        <option value="" selected="">(Select Year Build)</option>
                                                                        <?php
                                                                        foreach ($yearArray as $year) {
                                                                            echo '<option value="' . $year . '"' . (($data['property_year']) == $year ? "selected" : "") . '>' . $year . '</option>';
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label><?= $_SESSION[SESSION_DOMAIN]['property_size'] ?></label>
                                                                    <input placeholder="Eg: 100" class="form-control number_only" maxlength="10" type="text" id="number_only" name="property_squareFootage" value="<?= $data['property_squareFootage']; ?>"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Last Renovation</label>
                                                                    <input class="form-control last_renovation" type="text"  name="last_renovation_date"  id="renovationdate" value="<?=  dateFormatUser($datarenovationdetails['last_renovation_date'], null, $this->companyConnection); ?>" disabled=""/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Last Renovation Time</label>
                                                                    <input class="form-control" type="text"  name="last_renovation_time"  id="renovationtime" value="<?= timeFormat($datarenovationdetails['last_renovation_time'], NULL,$this->companyConnection); ?>" disabled=""/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Last Renovation Description</label>
                                                                    <input class="form-control capital" type="text" id="last_renovation_description" name="last_renovation_description" value="<?= $datarenovationdetails['last_renovation_description']; ?>" disabled=""/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">

                                                                    <a  id="ancNewRenovation" style="cursor: pointer; text-decoration: underline; color: deepskyblue;" class="active notes-button-bold">New Renovation</a>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Number of Buildings</label>
                                                                    <input class="form-control number_only" maxlength="6" type="text" id="no_of_building" name="no_of_buildings" value="<?= $data['no_of_buildings']; ?>"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Number of Units</label>
                                                                    <input  class="form-control number_only" maxlength="6" type="text" id="no_of_units" name="no_of_units" value="<?= $data['no_of_units']; ?>"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Smoking Allowed</label>
                                                                    <select placeholder="Eg: Yes" class="form-control" id="NonSmoking" name="smoking_allowed">
                                                                        <option value="0" <?php
                                                                        if ($data['smoking_allowed'] == 0) {
                                                                            echo "selected";
                                                                        } else {
                                                                            echo "";
                                                                        }
                                                                        ?>>No</option>
                                                                        <option value="1" <?php
                                                                        if ($data['smoking_allowed'] == 1) {
                                                                            echo "selected";
                                                                        } else {
                                                                            echo "";
                                                                        }
                                                                        ?>>Yes</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Pet Friendly <a id='Newpet' class="pop-add-icon" onclick="clearPopUps('#NewpetPopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                    <select class='form-control' id='pet_options' name='pet_friendly'></select>
                                                                    <div class="add-popup NewpetPopupClass" id='NewpetPopup'>
                                                                        <h4>Add New Pet Friendly</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>Pet Friendly<em class="red-star">*</em></label>
                                                                                    <input class="form-control capital customValidatePetFriendly" data_required="true" data_max="150" type="text" name='@pet_friendly'/>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                                <div class="btn-outer text-right">
                                                                                    <button type="button" class="blue-btn" id="NewpetPopupSave">Save</button>
                                                                                    <button rel="NewpetPopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                    <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <?php
                                                                    if (!empty($schooldistrictdata)) {
                                                                        foreach ($schooldistrictdata as $school) {
                                                                            ?>
                                                                            <div class="school_district_municipality_div">
                                                                                <div class="col-sm-12 clone-1">
                                                                                    <div class="col-sm-3">
                                                                                        <label>School District or Municipality <i style="" class="add-more-div fa fa-plus-circle" id="AddNewSchoolDistrictDiv" aria-hidden="true"></i><i style="display:none;" class="add-more-div red-star glyphicon glyphicon-remove-sign RemoveNewSchoolDistrictDiv" id="" aria-hidden="true"></i></label>
                                                                                        <select class="form-control school_district_municipality_select" id="" name="school_district_municipality">
                                                                                            <option value="">Select</option>
                                                                                            <option value="School District" <?php
                                                                                            if ($school['school_district'] == 'School District') {
                                                                                                echo "selected";
                                                                                            } else {
                                                                                                echo "";
                                                                                            }
                                                                                            ?>>School District</option>
                                                                                            <option value="Municipality" <?php
                                                                                            if ($school['school_district'] == 'Municipality') {
                                                                                                echo "selected";
                                                                                            } else {
                                                                                                echo "";
                                                                                            }
                                                                                            ?>>Municipality</option>
                                                                                            <option value="School District/Municipality" <?php
                                                                                            if ($school['school_district'] == 'School District/Municipality') {
                                                                                                echo "selected";
                                                                                            } else {
                                                                                                echo "";
                                                                                            }
                                                                                            ?>>School District/Municipality</option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-sm-6 hidden_school_fields">
                                                                                        <div class="col-sm-3">
                                                                                            <label>Code</label>
                                                                                            <input type="text" name="school_district_code" class="form-control school_district_code" value="<?= $school['code']; ?>">
                                                                                        </div>
                                                                                        <div class="col-sm-3 ">
                                                                                            <label>Notes</label>
                                                                                            <input type="text" name="school_district_notes" class="form-control school_district_notes" value="<?= $school['notes']; ?>">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <div class="school_district_municipality_div">
                                                                            <div class="col-sm-12 clone-1">
                                                                                <div class="col-sm-3">
                                                                                    <label>School District or Municipality <i style="" class="add-more-div fa fa-plus-circle" id="AddNewSchoolDistrictDiv" aria-hidden="true"></i><i style="display:none;" class="add-more-div glyphicon glyphicon-remove-sign RemoveNewSchoolDistrictDiv" id="" aria-hidden="true"></i></label>
                                                                                    <select class="form-control school_district_municipality_select" id="" name="school_district_municipality">
                                                                                        <option value="">Select</option>
                                                                                        <option value="School District">School District</option>
                                                                                        <option value="Municipality">Municipality</option>
                                                                                        <option value="School District/Municipality">School District/Municipality</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-sm-6 hidden_school_fields">
                                                                                    <div class="col-sm-3">
                                                                                        <label>Code</label>
                                                                                        <input type="text" name="school_district_code" class="form-control school_district_code" value="">
                                                                                    </div>
                                                                                    <div class="col-sm-3 ">
                                                                                        <label>Notes</label>
                                                                                        <input type="text" name="school_district_notes" class="form-control school_district_notes" value="">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div><?php } ?>
                                                                </div>
                                                                <?php ?>
                                                                <?php
                                                                if (!empty($data['key_access_codes_info'])  && is_array($data['key_access_codes_info'])) {
                                                                    foreach ($data['key_access_codes_info'] as $key => $key_access_codes_info) {
                                                                        ?>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3 key_optionsDivClass" id="key_optionsDiv">
                                                                            <label>Key/Access Code Info. <a  class="pop-add-icon Newkey" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class='form-control add-input key_access_codes_list key_access_codes_info_<?= $key ?>' id='key_options' name='key_access_codes_info'></select>

                                                                            <div class="key_access_codeclass">

                                                                                <textarea name="key_access_code_desc" id='key_access_code' class="capiatl form-control key_access_codetext key_access_codetextdynm_<?= @$key ?>"><?= @$data['key_access_code_desc'][$key] ?></textarea>

                                                                            </div>
                                                                            <a id="NewkeyPlus" class="add-icon" onclick="clearPopUps('#NewkeyPopupid')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                            <a style='display:none' id="NewkeyMinus" class="add-icon" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                                            <div class="add-popup NewkeyPopup" id="NewkeyPopupid">
                                                                                <h4>Add New Key Access Code</h4>
                                                                                <div class="add-popup-body">
                                                                                    <div class="form-outer">
                                                                                        <div class="col-sm-12">
                                                                                            <label>Key Access Code<em class="red-star">*</em></label>
                                                                                            <input class="form-control keyAccessCode customValidateKeyAccessCode key-access-input capital" data_required="true" data_max="150" type="text" name='@key_code'/>
                                                                                            <span class="customError required"></span>
                                                                                        </div>

                                                                                        <div class="btn-outer text-right">
                                                                                            <button type="button" class="blue-btn NewkeyPopupSave" data_id="" >Save</button>
                                                                                            <button rel="NewkeyPopup" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                            <input type="button"  class="grey-btn NewkeyPopupCancel cancelPopup" value='Cancel'/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    <?php }
                                                                } else { ?>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3 key_optionsDivClass" id="key_optionsDiv">
                                                                        <label>Key/Access Code Info. <a  class="pop-add-icon Newkey" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                        <select class='form-control add-input key_access_codes_list key_access_codes_info' id='key_options' name='key_access_codes_info'></select>

                                                                        <div class="key_access_codeclass" style='display:none'>

                                                                            <textarea name="key_access_code_desc" id='key_access_code' class="form-control key_access_codetext"></textarea>

                                                                        </div>
                                                                        <a id="NewkeyPlus" class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                        <a style='display:none' id="NewkeyMinus" class="add-icon" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                                        <div class="add-popup NewkeyPopup" id="NewkeyPopupid">
                                                                            <h4>Add New Key Access Code</h4>
                                                                            <div class="add-popup-body">
                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Key Access Code<em class="red-star">*</em></label>
                                                                                        <input class="form-control keyAccessCode customValidateKeyAccessCode key-access-input capital" data_required="true" data_max="150" type="text" name='@key_code'/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>

                                                                                    <div class="btn-outer text-right">
                                                                                        <button type="button" class="blue-btn NewkeyPopupSave" data_id="" >Save</button>
                                                                                        <button rel="NewkeyPopup" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                        <input type="button"  class="grey-btn NewkeyPopupCancel cancelPopup" value='Cancel'/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Garage Available <a id='Newgarage' class="pop-add-icon" onclick="clearPopUps('#NewgaragePopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                    <select class='form-control' id='garage_options' name='garage_available'></select>
                                                                    <div class="add-popup NewgaragePopupClass" id='NewgaragePopup'>
                                                                        <h4>Add New Garage Available</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>Garage Available<em class="red-star">*</em></label>
                                                                                    <input class="form-control capital customValidateGarageAvailable" data_required="true" data_max="150" type="text" name="@garage_avail"/>
                                                                                    <span class="customError required"></span>
                                                                                </div>

                                                                                <div class="btn-outer text-right">
                                                                                    <button type="button" class="blue-btn" id="NewgaragePopupSave">Save</button>
                                                                                    <button rel="NewgaragePopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                    <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <?php

                                                                if($data['garage_available'] == '2'){?>
                                                                    <div class="col-md-4 garageVehicle">
                                                                        <label class="garageLabel">No. of Vehicles this Property Accommodates</label>
                                                                        <textarea class="form-control capital garage_text" name="no_of_vehicles" placeholder="Eg:- 12345 (Optional)"><?php echo $data['no_of_vehicles'] ?></textarea>
                                                                    </div>
                                                                <?php } else{ ?>
                                                                    <div class="col-md-4 garageVehicle" style="display:none;">
                                                                        <label class="garageLabel">No. of Vehicles this Property Accommodates</label>
                                                                        <textarea class="form-control garage_text" name="no_of_vehicles" placeholder="Eg:- 12345 (Optional)"></textarea>
                                                                    </div>
                                                                <?php   }
                                                                if($data['garage_available'] > '2') { ?>
                                                                    <div class="col-md-4 garageDesc">
                                                                        <label class="garageLabel">Notes</label>
                                                                        <textarea class="form-control garage_veh capital" name="garage_note" placeholder="Eg:- 12345 (Optional)"><?php echo $data['garage_note'] ?></textarea>
                                                                    </div>
                                                                <?php } else{ ?>
                                                                    <div class="col-md-4 garageDesc" style="display:none;">
                                                                        <label class="garageLabel">Notes</label>
                                                                        <textarea class="form-control garage_veh" name="garage_note" placeholder="Eg:- 12345 (Optional)"></textarea>
                                                                    </div>
                                                                <?php } ?>
                                                                <div class="div-full" style="margin-top: 0px; margin-bottom: 0px;">
                                                                    <div class="form-data-range2" style="width:auto !important;">
                                                                        <div class="check-outer amenitieschck-radio">

                                                                            <input  class="" type="checkbox" id="chkOnlineListing" name='online_listing' spellcheck="true" <?php
                                                                            if ($data['online_listing'] == 'on') {
                                                                                echo "checked";
                                                                            } else {
                                                                                echo "";
                                                                            }
                                                                            ?>> <label>Online Listing</label>

                                                                        </div>
                                                                        <div class="check-outer amenitieschck-radio">
                                                                            <input class="" type="checkbox" id="chkOnlineApplication" name='online_application' spellcheck="true" <?php
                                                                            if ($data['online_application'] == 'on') {
                                                                                echo "checked";
                                                                            } else {
                                                                                echo "";
                                                                            }
                                                                            ?>> <label>Online Application</label>
                                                                        </div>
                                                                        <div class="check-outer amenitieschck-radio">
                                                                            <input class="" type="checkbox" id="chkDisableSyndication" name='disable_sync' spellcheck="true" <?php
                                                                            if ($data['disable_sync'] == 'on') {
                                                                                echo "checked";
                                                                            } else {
                                                                                echo "";
                                                                            }
                                                                            ?>> <label>Disable Syndication</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3 textarea-form">
                                                                    <label>Amenities <a id='Newamenities' class="pop-add-icon" onclick="clearPopUps('#NewamenitiesPopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                    <div id="amenties_box" class="form-control"></div>
                                                                    <div class="add-popup NewamenitiesPopupClass" id='NewamenitiesPopup'>
                                                                        <h4>Add New Amenity</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>New Amenity Code<em class="red-star">*</em></label>
                                                                                    <input class="form-control customValidateAmenities" data_required="true" data_max="15" type="text" name="@code"/>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label> Amenity Name <em class="red-star">*</em></label>
                                                                                    <input class="form-control capital customValidateAmenities" data_required="true" data_max="50" type="text" name="@name"/>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                                <div class="btn-outer text-right">
                                                                                    <button type="button" class="blue-btn" id="NewamenitiesPopupSave">Save</button>
                                                                                    <button rel="NewamenitiesPopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                    <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>



                                                                <div class="col-xs-12 col-sm-4 col-md-3 textarea-form">
                                                                    <label>Description</label>
                                                                    <div class="notes_date_right_div">
                                                                        <textarea name="description"  id='description' class="form-control capital notes_date_right"><?= $data['description']; ?></textarea></div>
                                                                </div>


                                                                <?php // if ($data['is_property_with_no_building'] == 'on') { ?>

                                                                <!--                                                                <div class="col-sm-12">
                                                                    <div class="check-outer clear" style="margin-top: 20px;">

                                                                        <input name="is_property_with_no_building" id ="default_building_unit" style="margin-right: 5px;" type="checkbox" class="NoBuilding" <?php
                                                                //                                                                        if ($data['is_property_with_no_building'] == 'on') {
                                                                //                                                                            echo "checked";
                                                                //                                                                        } else {
                                                                //                                                                            echo "";
                                                                //                                                                        }
                                                                ?>>

                                                                        <label>Property With No Building/Unit</label>
                                                                    </div>
                                                                </div>-->
                                                                <!--                                                                <div class="col-sm-10 details">
                                                                    <label>Details</label>
                                                                    <div class="check-outer" type="text">
                                                                        <div class="postion-relative">
                                                                            <label>Select Unit Type <em class="red-star">*</em> <a class="pop-add-icon" id="AddNewUnitTypeModalPlus2" onclick="clearPopUps('#NewunitPopup')" href="javascript:;"> <i class="fa fa-plus-circle"></i></a></label>
                                                                            <span id="dynamic_unit_type">
                                                                                        <select class="form-control" id="unit_type" name="unit_type">
                                                                                            <option value="">Select</option>
                                                                                        </select>
                                                                                        <div class="add-popup" id='NewunitPopup'>
                                                                                            <h4>Add New Unit Type</h4>
                                                                                            <div class="add-popup-body">
                                                                                                <div class="form-outer">
                                                                                                    <div class="col-sm-12">
                                                                                                        <label>Unit Type<em class="red-star">*</em></label>
                                                                                                        <input class="form-control capital customValidateUnitType" data_required="true" data_max="150" type="text" name="@unit_type"/>
                                                                                                        <span class="customError required"></span>
                                                                                                    </div>
                                                                                                    <div class="col-sm-12">
                                                                                                        <label>Description</label>
                                                                                                        <input class="form-control capital customValidateUnitType" type="text" maxlength="500" name="@description"/>
                                                                                                        <span class="customError required"></span>
                                                                                                    </div>
                                                                                                    <div class="btn-outer">
                                                                                                        <button type="button" class="blue-btn" id="NewunitPopupSave">Save</button>
                                                                                                        <input type="button"  class="grey-btn" value='Cancel' />
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label>Base Rent <?php //echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> <em class="red-star">*</em> </label>
                                                                        <input placeholder="Eg: 2000" class="form-control number_only amount" type="text" name="base_rent" id="base_rent" value="<?= $data['base_rent']; ?>" >
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label>Market Rent <?php// echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> <em class="red-star">*</em> </label>
                                                                        <input placeholder="Eg: 2000"  class="form-control number_only amount" type="text"  name="market_rent" value="<?= $data['market_rent']; ?>">
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label>Security Deposit <?php //echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> <em class="red-star">*</em> </label>
                                                                        <input placeholder="Eg: 2000" class="form-control number_only amount" type="text" name="security_deposit" id="security_deposit" value="<?= $data['security_deposit']; ?>">
                                                                    </div>
                                                                </div>-->
                                                            </div>
                                                            <?php // } ?>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="btn-outer text-right">
                                                                <input type="submit" class="blue-btn" id="general_property_submit" value="Update"/>
                                                                <button rel="#collapseOne" type="button" class="grey-btn clearEditFormReset">Reset</button>
                                                                <button type="button" class="grey-btn" id='cancel_property'>Cancel</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <input type="hidden" name="saveandnext" id="saveandnext" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Link/Owner Landlords -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" data_href="#collapseTwo" id="collapse2" class="tabcheckvalidation">
                                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Link Owners/Landlords</a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <form action="" method="post" id="addOwnerForm">
                                                    <div class="form-outer">
                                                        <div class="row">
                                                            <div class="div-full">
                                                                <a id="ancCreateNewOwner" class="notes-button-bold" style="cursor: pointer" href="/People/AddOwners">Create New Owner/Landlord</a>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        $i = 0;
                                                        if (!empty($getLinkOwnerData['linkodss'])) {
                                                            foreach ($getLinkOwnerData['linkodss'] as $getLinkKey => $getLinkvalue) {
                                                                ?>
                                                                <div id='add_owners_div' class="grey-box-add owners_div">

                                                                    <div class="grey-box-add-inner">
                                                                        <div class="grey-box-add-lt">
                                                                            <div class="row">
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Owner/Landlord</label>
                                                                                    <select id='ownerSelectid' name='owner_percentowned' class="form-control ownerSelectlist_<?= $i ?> ownerSelectclass" onchange="vendorValues()">

                                                                                    </select>

                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Percent Owned (%) </label>
                                                                                    <input type="text" name="owner_percentowned" id="owner_percentowned" max="100" placeholder="Eg:50" value="<?= $getLinkvalue; ?>" class="form-control number_only percentage_owned">

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="grey-box-add-rt">
                                                                            <a id='multiplegreybox' class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                            <a style='display:none' id='multiplegreycross' style='display: none;' href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <?php $i++;
                                                            }
                                                        } else { ?>
                                                            <div id='add_owners_div' class="grey-box-add owners_div">

                                                                <div class="grey-box-add-inner">
                                                                    <div class="grey-box-add-lt">
                                                                        <div class="row">
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Owner/Landlord</label>
                                                                                <select id='ownerSelectid' name='owner_percentowned' class="form-control  ownerSelectclass" onchange="vendorValues()">

                                                                                </select>

                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Percent Owned (%) </label>
                                                                                <input type="text" name="owner_percentowned" max="100" placeholder="Eg:50" value="" class="form-control number_only percentage_owned">

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="grey-box-add-rt">
                                                                        <a id='multiplegreybox' class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                        <a style='display:none' id='multiplegreycross' style='display: none;' href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        <?php } ?>
                                                        <div class="row form-outer2">
                                                            <div class="col-sm-12">
                                                                <div class="pull-left">
                                                                    <label>Payment Type</label>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="check-outer">
                                                                        <input type="radio" name="payment_type" value="Net Income" <?php
                                                                        if ($data['payment_type'] == 'Net Income') {
                                                                            echo "checked";
                                                                        } else {
                                                                            echo " ";
                                                                        }
                                                                        ?>>
                                                                        <label>Net Income</label>
                                                                    </div>
                                                                    <div class="check-outer">
                                                                        <input type="radio" name="payment_type" value="Flat" <?php
                                                                        if ($data['payment_type'] == 'Flat') {
                                                                            echo "checked";
                                                                        } else {
                                                                            echo " ";
                                                                        }
                                                                        ?>>
                                                                        <label>Flat</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">&nbsp;</div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Vendor 1099 Payer</label>
                                                                <input type="text"  name="vendor_1099_payer" readonly="readonly" class="form-control vendor_idclass" id='vendor_id' value='<?= $data['vendor_1099_payer'] ?>' placeholder="Select">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Fiscal Year End</label>
                                                                <select class="form-control" name="fiscal_year_end">
                                                                    <?php
                                                                    for ($i = 1; $i < 13; $i++) {
                                                                        echo "<option value=" . date('F', mktime(0, 0, 0, $i, 10)) . " " . (($data['fiscal_year_end'] == date('F', mktime(0, 0, 0, $i, 10)) ? "selected" : "")) . ">" . date('F', mktime(0, 0, 0, $i, 10)) . "</option>";
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer text-right">
                                                                    <button class="blue-btn">Update</button>
                                                                    <button rel="#collapseTwo"  type="button" class="grey-btn clearEditFormReset">Reset</button>
                                                                    <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <input type="hidden" name="saveandnext" id="saveandnextowner" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Bank Accounts -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" data_href="#collapseThree" class="tabcheckvalidation" id="3collapseThree">
                                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Bank Accounts</a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <form action="" method="post" id="addBankdetailForm">
                                                    <div class="row">

                                                        <div class="form-outer">
                                                            <?php if (!empty($getBankdetails)) {
                                                                $i=0;
                                                                ?>
                                                                <?php foreach ($getBankdetails as $key => $value) { ?>
                                                                    <div class='BankDetailCloneDiv' id='BankDetailCloneId'>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Account Name <a class="pop-add-icon" id="add_more_bank_account_icon" onclick="clearPopUps('#Newaccountname')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control bankAccountName editAccountName_<?= $i ?>" name='account_name[]' id='account_name'>
                                                                                <option value=''>Select</option>
                                                                            </select>
                                                                            <div class="add-popup Newaccountname" id='Newaccountname'>
                                                                                <h4>Add New Account Name</h4>
                                                                                <div class="add-popup-body">

                                                                                    <div class="form-outer">
                                                                                        <div class="col-sm-12">
                                                                                            <label>Account Name<em class="red-star">*</em></label>
                                                                                            <input placeholder="Add new Account Name customValidateAccountName" class="form-control capital" type="text" data_required="true" data_max="20" name='@account_name' id='account_name'/>
                                                                                            <span class="customError required"></span>
                                                                                        </div>
                                                                                        <div class="btn-outer">
                                                                                            <input type="button" id='accountnameplusSave' class="blue-btn accountnameplusSave" value="Save" />
                                                                                            <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Chart of Account <a  data-backdrop="static" data-toggle="modal"  data-target="#chartofaccountmodal" class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control bankChartsOfAccounts editChartName_<?= $i ?>" name='chart_of_account[]' id='chart_of_account'>
                                                                                <option value=''>Select</option>

                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Account Type <a class="pop-add-icon accounttypeplus" id="accounttypeplus" onclick="clearPopUps('.NewaccountType')"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control prop_account_type" name='account_type[]' id='account_type'>
                                                                                <option value=''>Select</option>

                                                                            </select>
                                                                            <div class="add-popup NewaccountType">
                                                                                <h4>Add New Account Type</h4>
                                                                                <div class="add-popup-body">

                                                                                    <div class="form-outer">
                                                                                        <div class="col-sm-12">
                                                                                            <label>Account Type<em class="red-star">*</em></label>
                                                                                            <input class="form-control capital customValidateAccountType" type="text" data_required="true" data_max="20" placeholder="Account Type" name='@account_type' id='account_type'/>
                                                                                            <span class="customError required"></span>
                                                                                        </div>
                                                                                        <div class="btn-outer text-right">
                                                                                            <input type="button"  class="blue-btn accounttypeplusSave" value="Save" />
                                                                                            <button rel="NewaccountType" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                            <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Bank Name</label>
                                                                            <input class="form-control capital hide_copy" type="text" maxlength="50" name='bank_name[]' placeholder="Bank Name" id='bank_name' value="<?php echo $value['bank_name'] ?>"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Bank Routing #</label>
                                                                            <input class="form-control number_only hide_copy" type="text" maxlength="16" placeholder="Bank Routing" name='bank_routing[]' id='bank_routing' value="<?php echo $value['bank_routing'] ?>"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Bank Account #</label>
                                                                            <input class="form-control hide_copy" type="text" maxlength="16" placeholder="Bank Account" name='bank_account[]' id='bank_account' value="<?php echo $value['bank_account'] ?>"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Default Security Deposit Bank Account</label>
                                                                            <select class="form-control add-input default_security_deposit_<?= $i; ?>" name='default_security_deposit[]' id='default_security_bank_account'>
                                                                                <option value='default'>Select</option>
                                                                                <option <?php if ($value['default_security_deposit'] == 'Building Supplies : 5270') { ?> Selected <?php } ?> value='Building Supplies : 5270'>Building Supplies : 5270</option>
                                                                                <option <?php if ($value['default_security_deposit'] == 'CAM Receivable : 1150') { ?> Selected <?php } ?> value='CAM Receivable : 1150'>CAM Receivable : 1150</option>
                                                                                <option <?php if ($value['default_security_deposit'] == 'Cleaning : 5230') { ?> Selected <?php } ?> value='Cleaning : 5230'>Cleaning : 5230</option>

                                                                            </select>
                                                                            <?php if ($key == 0) { ?>
                                                                                <a  class="add-icon bankdetailClonePlus add-wholesection" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                            <?php } else { ?>
                                                                                <div class="email-add-remove-row remove-icon add-icon delete-wholesection"><i aria-hidden="true" class="fa fa-times-circle red-star-circle addremoveBankAccount" style="color: red;"></i></div>
                                                                            <?php } ?>
                                                                        </div>
                                                                        <!-- <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Bank Account Min. Reserve Amount  <?php /*echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; */?></label>
                                                                            <input class="form-control number_only amount" type="text" name='bank_account_min_reserve_amount[]' id='bank_account_min_amount' value="<?php /*echo $value['bank_account_min_reserve_amount'] */?>"/>
                                                                        </div>-->
                                                                        <div class="col-md-12">
                                                                            <div class=" col-sm-4 col-md-3 reserve_amount">
                                                                                <label>Bank Account Min. Reserve Amount  <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                                                                <input class="form-control number_only amount  hide_copy" placeholder="Bank Account Min. Reserve Amount" type="text" name='bank_account_min_reserve_amount[]' id='bank_account_min_amount' value="<?php echo $value['bank_account_min_reserve_amount'] ?>"/>
                                                                            </div>
                                                                            <div class=" col-sm-4 col-md-3 textarea-form">
                                                                                <label>Description</label>
                                                                                <div class="notes_date_right_div">
                                                                                    <textarea class="form-control capital notes_date_right" maxlength="500" placeholder="Description" name='description[]'><?php echo $value['description'] ?></textarea></div>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <label></label>
                                                                                <div class="check-outer">
                                                                                    <?php  if($value['is_default'] == '1'){
                                                                                        $is_default_checked='checked';
                                                                                    }else{ $is_default_checked=''; }?>
                                                                                    <input name="is_default[]" class="is_default" type="radio" <?= $is_default_checked; ?>/>
                                                                                    <input type="hidden" name="default_value[]" class="input_default" value="0">
                                                                                    <label>Set as Default</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php
                                                                    $i++;
                                                                }

                                                                ?>
                                                            <?php } else { ?>
                                                                <div class='BankDetailCloneDiv' id='BankDetailCloneId'>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Account Name <a class="pop-add-icon" id="add_more_bank_account_icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                        <select class="form-control bankAccountName" name='account_name[]' id='account_name'>
                                                                            <option value=''>Select</option>
                                                                        </select>
                                                                        <div class="add-popup Newaccountname" id='Newaccountname'>
                                                                            <h4>Add New Account Name</h4>
                                                                            <div class="add-popup-body">

                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Account Name <em class="red-star">*</em></label>
                                                                                        <input placeholder="Add new Account Name" class="form-control capital customValidateAccountName" type="text" placeholder="Account Name" data_required="true" data_max="20" name='@account_name' id='account_name'/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="btn-outer">
                                                                                        <input type="button" id='accountnameplusSave' class="blue-btn accountnameplusSave" value="Save" />
                                                                                        <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Chart of Account <a  data-backdrop="static" data-toggle="modal"  data-target="#chartofaccountmodal" class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                        <select class="form-control bankChartsOfAccounts capital" name='chart_of_account[]' id='chart_of_account'>
                                                                            <option value=''>Select</option>

                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Account Type <a class="pop-add-icon accounttypeplus" id="accounttypeplus"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                        <select class="form-control prop_account_type capital" name='account_type[]' id='account_type'>
                                                                            <option value=''>Select</option>

                                                                        </select>
                                                                        <div class="add-popup NewaccountType">
                                                                            <h4>Add New Account Type</h4>
                                                                            <div class="add-popup-body">

                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Account Type<em class="red-star">*</em></label>
                                                                                        <input class="form-control capital customValidateAccountType" type="text" placeholder="Account Type" data_required="true" data_max="20" name='@account_type' id='account_type'/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="btn-outer text-right">
                                                                                        <input type="button"  class="blue-btn accounttypeplusSave" value="Save" />
                                                                                        <button rel="NewaccountType" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                        <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Bank Name</label>
                                                                        <input class="form-control capital" placeholder="Bank Name" type="text" maxlength="50" name='bank_name[]' id='bank_name'/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Bank Routing #</label>
                                                                        <input class="form-control number_only hide_copy" placeholder="Bank Routing" type="text" maxlength="16" name='bank_routing[]' id='bank_routing'/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Bank Account #</label>
                                                                        <input class="form-control hide_copy" type="text" placeholder="Bank Account" maxlength="16" name='bank_account[]' id='bank_account'/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Default Security Deposit Bank Account</label>
                                                                        <select class="form-control add-input hide_copy" name='default_security_deposit[]' id='default_security_bank_account'>
                                                                            <option value='default'>Select</option>
                                                                            <option value='Building Supplies : 5270'>Building Supplies : 5270</option>
                                                                            <option value='CAM Receivable : 1150'>CAM Receivable : 1150</option>
                                                                            <option value='Cleaning : 5230'>Cleaning : 5230</option>

                                                                        </select>
                                                                        <a  class="add-icon bankdetailClonePlus add-wholesection" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>

                                                                    </div>
                                                                    <!--<div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Bank Account Min. Reserve Amount  <?php /*echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; */?></label>
                                                                        <input class="form-control number_only amount" type="text" name='bank_account_min_reserve_amount[]' id='bank_account_min_amount'/>
                                                                    </div>-->
                                                                    <div class="col-md-12">
                                                                        <div class="col-sm-4 col-md-3 reserve_amount">
                                                                            <label>Bank Account Min. Reserve Amount  <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                                                            <input class="form-control number_only amount hide_copy" placeholder="Bank Account Min. Reserve Amount" type="text" name='bank_account_min_reserve_amount[]' id='bank_account_min_amount'/>
                                                                        </div>
                                                                        <div class=" col-sm-4 col-md-3 textarea-form">
                                                                            <label>Description</label>
                                                                            <div class="notes_date_right_div">
                                                                                <textarea class="form-control capital notes_date_right" placeholder="Description" maxlength="500" name='description[]'></textarea></div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <label></label>
                                                                            <div class="check-outer">
                                                                                <input name="is_default[]" class="is_default" type="radio"/>
                                                                                <input type="hidden" name="default_value[]" class="input_default" value="0">
                                                                                <label>Set as Default</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer text-right">
                                                                    <input type='hidden' id='BankdetailClickval' value=''>
                                                                    <button id='BankdetailClickS' class="blue-btn">Update</button>
                                                                    <button rel="#collapseThree" type="button" class="grey-btn clearEditFormReset">Reset</button>
                                                                    <button type='button' class="grey-btn cancel-all">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- Late Fee Management -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" data_href="#collapseFour" class="tabcheckvalidation">
                                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Late Fee Management</a>
                                            </h4>
                                        </div>
                                        <div id="collapseFour" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <form action="" method="post" id="AddLateFee">
                                                    <div class="row">
                                                        <div class="latefee-check-outer">
                                                            <div class="col-sm-12 latefee-check-hdr">
                                                                <div class="check-outer">
                                                                    <?php
                                                                    $onetimecheck = '';
                                                                    if ($getLateFeemanagement['checkbox'] == 1) {
                                                                        $onetimecheck = "checked";
                                                                    } else {
                                                                        $onetimecheck;
                                                                    }
                                                                    ?>
                                                                    <input type="checkbox" id="apply_one_time_id" value="1" <?= $onetimecheck; ?>/>
                                                                    <label class="blue-label">Apply One Time</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <div class="check-input-outer">
                                                                    <?php
                                                                    $one_monthly_fee;
                                                                    $one_monthly_fee_radio;
                                                                    if ($getLateFeemanagement['apply_one_time_flat_fee'] == null) {
                                                                        $one_monthly_fee = "disabled";
                                                                        $one_monthly_fee_radio = " ";
                                                                    } else {
                                                                        $one_monthly_fee = " ";
                                                                        $one_monthly_fee_radio = "checked";
                                                                    }
                                                                    ?>

                                                                    <input type="radio" class="late_fee_radio" id="one_time_flat_fee" name="one_time_radio" <?= $one_monthly_fee_radio; ?>/>
                                                                    <label>

                                                                        <input class="form-control amount number_only" type="text" id='one_time_flat_fee_desc'  name="apply_one_time_flat_fee"  value="<?= $getLateFeemanagement['apply_one_time_flat_fee']; ?>" placeholder="Example 10.00" <?= $one_monthly_fee; ?>/>
                                                                    </label>
                                                                    <span>Flat Fee <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> (Example 10.00)</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <?php
                                                                $one_monthly_per;
                                                                $one_monthly_per_radio;
                                                                if ($getLateFeemanagement['apply_one_time_percentage'] == null) {
                                                                    $one_monthly_per = "disabled";
                                                                    $one_monthly_per_radio = " ";
                                                                } else {
                                                                    $one_monthly_per = " ";
                                                                    $one_monthly_per_radio = "checked";
                                                                }
                                                                ?>

                                                                <div class="check-input-outer">
                                                                    <input type="radio" class="late_fee_radio" id="one_time_fee_radio" name="one_time_radio" <?= $one_monthly_per_radio ?>/>
                                                                    <label>

                                                                        <input class="form-control number_only" type="text" id='one_time_monthly_fee_desc'  max="100"  name="apply_one_time_percentage"  value="<?= $getLateFeemanagement['apply_one_time_percentage']; ?>" placeholder="Example 20" <?= $one_monthly_per; ?>/>
                                                                    </label>
                                                                    <span>% of Monthly Rent (Example 20)</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="latefee-check-outer">
                                                            <div class="col-sm-12 latefee-check-hdr">
                                                                <div class="check-outer">
                                                                    <?php
                                                                    $dailytimecheck = '';
                                                                    if ($getLateFeemanagement['checkbox'] == 2) {
                                                                        $dailytimecheck = "checked";
                                                                    } else {
                                                                        $dailytimecheck;
                                                                    }
                                                                    ?>
                                                                    <input type="checkbox" class="late_fee_radio" id="apply_daily_id" value="2" <?= $dailytimecheck; ?>/>
                                                                    <label class="blue-label">Apply Daily</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <?php
                                                                $daily_monthly_fee;
                                                                $daily_monthly_fee_radio;
                                                                if ($getLateFeemanagement['apply_daily_flat_fee'] == null) {
                                                                    $daily_monthly_fee = "disabled";
                                                                    $daily_monthly_fee_radio = " ";
                                                                } else {
                                                                    $daily_monthly_fee = " ";
                                                                    $daily_monthly_fee_radio = "checked";
                                                                }
                                                                ?>

                                                                <div class="check-input-outer">
                                                                    <input type="radio" class="late_fee_radio" name="daily_time_radio" id="daily_flat_fee" <?= $daily_monthly_fee_radio ?>/>
                                                                    <label>
                                                                        <input class="form-control amount number_only" type="text" id='daily_flat_fee_desc' name="apply_daily_flat_fee"  placeholder="Example 10.00" value="<?= $getLateFeemanagement['apply_daily_flat_fee']; ?>" <?= $daily_monthly_fee; ?>/>
                                                                    </label>
                                                                    <span>Flat Fee <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> (Example 10.00)</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <?php
                                                                $daily_monthly_per;
                                                                $daily_monthly_per_radio;
                                                                if ($getLateFeemanagement['apply_daily_percentage'] == null) {
                                                                    $daily_monthly_per = "disabled";
                                                                    $daily_monthly_per_radio = " ";
                                                                } else {
                                                                    $daily_monthly_per = " ";
                                                                    $daily_monthly_per_radio = "checked";
                                                                }
                                                                ?>
                                                                <div class="check-input-outer">
                                                                    <input type="radio" class="late_fee_radio" name="daily_time_radio" id="daily_monthly_fee" <?= $daily_monthly_per_radio ?>/>
                                                                    <label>
                                                                        <input class="form-control number_only" type="text"  max="100" id='daily_monthly_fee_desc' name="apply_daily_percentage"  placeholder="Example 20" value="<?= $getLateFeemanagement['apply_daily_percentage']; ?>" <?= $daily_monthly_per; ?> />
                                                                    </label>
                                                                    <span>% of Monthly Rent (Example 20)</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <div class="check-outer">
                                                                <label class="blue-label lh30">Grace Period<em class="red-star">*</em></label>
                                                                <label>
                                                                    <input class="form-control number_only" type="text" maxlength="5" name='grace_period'  id="grace_period" placeholder="Example 10" value="<?= $getLateFeemanagement['grace_period'] ?>"/>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="btn-outer text-right">
                                                                <input type="hidden"  id="LatefeebuttonClicked" value="">
                                                                <button class="blue-btn" id="AddLatefeeButton">Update</button>
                                                                <button rel="#collapseFour" type="button" class="grey-btn clearEditFormReset">Reset</button>
                                                                <button type='button' class="grey-btn cancel-all">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Key Tracker -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" data_href="#collapseFive" class="tabcheckvalidation">
                                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Key Tracker</a>
                                            </h4>
                                        </div>
                                        <div id="collapseFive" class="panel-collapse collapse">
                                            <div class="panel-body">


                                                <div class="row">
                                                    <div class="latefee-check-outer">
                                                        <div class="col-sm-12 latefee-check-hdr">
                                                            <div class="check-outer">
                                                                <input type="radio" name="KeyAddTrack" checked="" class='addkeytracker' value='addkey'/>
                                                                <label class="blue-label">Add key</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 latefee-check-hdr">
                                                            <div class="check-outer">
                                                                <input type="radio" name="KeyAddTrack" class='addkeytracker' value='trackkey'/>
                                                                <label class="blue-label">Track Key</label>
                                                            </div>
                                                        </div>
                                                        <div class="div-full" id="addkeyhref">
                                                            <a href="javascript:;"  class="active notes-button-bold">Add Key</a>
                                                        </div>
                                                        <div class="div-full" id="trackkeyhref" style="display:none;">
                                                            <a href="javascript:;"  class="active notes-button-bold">Track Key</a>
                                                        </div>
                                                    </div>
                                                    <div id="addKeyDiv" style="display: none;">
                                                        <form action="" method="post" id="addkeytags">
                                                            <input type="hidden" name="id" id="key_id" value="">
                                                            <div class="form-outer" id="addKeyDivdiv1">

                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Key Tag <em class="red-star">*</em></label>
                                                                    <input class="form-control capital" type="text" maxlength="50" name="key_tag" id="key_tag" placeholder="Key Tag"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Description <em class="red-star">*</em></label>
                                                                    <input class="form-control capital description1" placeholder="Description" maxlength="50" type="text" name="description" id="description"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Total Keys <em class="red-star">*</em></label>
                                                                    <input class="form-control number_only" placeholder="Total Keys" maxlength="50" type="text" name="total_keys" id="total_keys"/>
                                                                    <input class="form-control number_only" placeholder="Total Keys" maxlength="50" type="hidden" name="available_keys" id="available_keys"/>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="btn-outer text-right">
                                                                        <input type="hidden"  id="KeybuttonClicked" value="">
                                                                        <button type="submit" class="blue-btn" id="AddKeyButton">Save</button>
                                                                        <button rel="addkeytags" type="button" class="grey-btn clearFormResetkey">Clear</button>
                                                                        <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <form action="" method="post" id="KeycheckoutForm">
                                                        <div id='KeycheckoutDiv' class="form-outer" style="display: none;">
                                                            <input type="hidden" value=""  id="checkoutid">
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Key Holder</label>
                                                                <input type="text" placeholder="Key Holder" class="form-control owner_pre_vendor" maxlength="50" id="key_holder"/>
                                                                <input type="hidden" value="" name="key_holder" id="key_holder_id">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Keys</label>
                                                                <input type="text" placeholder="Keys" class="form-control" maxlength="50" name="checkout_keys" id="checkout_keys"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Description</label>
                                                                <input type="text" placeholder="Description" class="form-control capital" maxlength="12" name="checkout_desc"/>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer">
                                                                    <button type="submit" class="blue-btn" id="KeycheckoutSave">Save</button>
                                                                    <button  rel="KeycheckoutForm" type="button" class="grey-btn clearFormResetkey">Clear</button>
                                                                    <button type="button" class="grey-btn">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <div class="grid-outer listinggridDiv addkeylistingstyle">
                                                        <div class="apx-table">
                                                            <div class="table-responsive">
                                                                <table id="key-table" class="table table-bordered">
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <form action="" method="post" id="TrackkeydetailForm">
                                                    <input type="hidden" name="id" id="trackkey_id" value="">
                                                    <div class="row">
                                                        <div id='addTrackDiv' class="form-outer divNewtracker" style="display: none;">
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Name</label>
                                                                <input type="text" class="form-control capital trackkey_name" name="key_name" id="edittrackkey_name" placeholder="Key Tag" value=""/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Email</label>
                                                                <input type="text" class="form-control capitalremove edittrackemail" placeholder="Email" maxlength="50" name="trackemail" id="trackemail"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Company Name <em class="red-star">*</em></label>
                                                                <input type="text" class="form-control capital edittrackcompany_name" placeholder="Company Name" maxlength="50" name="company_name" id="trackcompany_name"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Phone # <em class="red-star">*</em>  </label>
                                                                <input type="text" class="form-control phone_number edittrackphone" placeholder="Phone" maxlength="12" name="phone" id="trackphone"/>
                                                            </div>

                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Address1 <em class="red-star">*</em> </label>
                                                                <input type="text" class="form-control capital" maxlength="50" placeholder="Address1" name="Address1" id="trackAddress1"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Address2  <em class="red-star">*</em></label>
                                                                <input type="text" class="form-control capital" maxlength="50" placeholder="Address2" name="Address2" id="trackAddress2"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Address3  <em class="red-star">*</em></label>
                                                                <input type="text" class="form-control capital" maxlength="50" placeholder="Address3" name="Address3" id="trackAddress3"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Address4  <em class="red-star">*</em></label>
                                                                <input type="text" class="form-control capital" maxlength="50" placeholder="Address4" name="Address4" id="trackAddress4"/>
                                                            </div>

                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Key# <em class="red-star">*</em> </label>
                                                                <input type="text" class="form-control" maxlength="50" placeholder="Key" name="key_number" id="trackkey_number"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Key Quantity <em class="red-star">*</em></label>
                                                                <input type="text" class="form-control number_only" placeholder="Key Quantity" maxlength="20" name="key_quality" id="trackkey_quality"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Pick Up Date</label>
                                                                <input type="text" class="form-control calander" readonly placeholder="Pick Up Date" name="pick_up_date" id="pick_up_date" readonly=""/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Pick Up Time</label>
                                                                <input type="text" class="form-control" readonly placeholder="Pick Up Time" name="pick_up_time" id="pick_up_time" readonly=""/>
                                                            </div>

                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Return Date <em class="red-star">*</em></label>
                                                                <input type="text" class="form-control" readonly placeholder="Return Date" name="return_date" id="return_date" readonly=""/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Return Time <em class="red-star">*</em></label>
                                                                <input type="text" class="form-control" readonly placeholder="Return Time" name="return_time" id="return_time" readonly=""/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Key Designator</label>
                                                                <input type="text" class="form-control capital key_designatorclass" placeholder="Key Designator" maxlength="50" name="key_designator" id="key_designator" value=""/>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer text-right">
                                                                    <input type="hidden"  id="TrackbuttonClicked" value="">
                                                                    <button type="submit" class="blue-btn" id="TrackKeyButton">Save</button>
                                                                    <button rel="TrackkeydetailForm" type="button" class="grey-btn clearFormResetTrack">Clear</button>
                                                                    <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="grid-outer listinggridDiv trackkeylistingstyle" style="display:none">
                                                            <div class="apx-table apxtable-bdr-none">
                                                                <div class="table-responsive">
                                                                    <table id="trackkey-table" class="table table-bordered">
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Management Fee -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" data_href="#collapseSix" class="tabcheckvalidation">
                                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Management Fees</a>
                                            </h4>
                                        </div>
                                        <div id="collapseSix" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <form action="" method="post" id="addManagementFeeForm">
                                                    <input type="hidden" value="" id="managementpid" name="id"/>
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Type</label>
                                                                <select class="form-control"  name="management_type" id="magt_type">
                                                                    <option value="Select" >Select</option>
                                                                    <option value="Flat" <?php
                                                                    if ($managementFeeData['management_type'] == "Flat") {
                                                                        echo "selected";
                                                                    } else {
                                                                        " ";
                                                                    }
                                                                    ?>>Flat</option>
                                                                    <option value="Percent" <?php
                                                                    if ($managementFeeData['management_type'] == "Percent") {
                                                                        echo "selected";
                                                                    } else {
                                                                        " ";
                                                                    }
                                                                    ?>>Percent</option>
                                                                    <option value="Percentage and a min. set fee" <?php
                                                                    if ($managementFeeData['management_type'] == "Percentage and a min. set fee") {
                                                                        echo "selected";
                                                                    } else {
                                                                        " ";
                                                                    }
                                                                    ?>>Percentage and a min. set fee</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label id="value_type">Value <em class="red-star">*</em> </label>
                                                                <input class="form-control amount" type="text" name="management_value" id="management_fee_value" value="<?= $managementFeeData['management_value']; ?>"/>
                                                            </div>
                                                            <?php if ($managementFeeData['management_type'] == 'Percentage and a min. set fee') { ?>
                                                                <div id="minimum_fee" class=" col-sm-3">
                                                                    <label>Minimum <em class="red-star">*</em></label>
                                                                    <input type="text" id="minimum_management_fee" name="minimum_management_fee" class="form-control p-number-format number_only" value="<?= $managementFeeData['minimum_management_fee']; ?>">
                                                                </div>
                                                            <?php } else {?>
                                                                <div id="minimum_fee" class=" col-sm-3" style="display:none">
                                                                    <label>Minimum <em class="red-star">*</em></label>
                                                                    <input type="text" id="minimum_management_fee"  name="minimum_management_fee" class="form-control p-number-format number_only" value="<?= $managementFeeData['minimum_management_fee']; ?>">
                                                                </div>
                                                            <?php } ?>
                                                            <div class="form-outer2">
                                                                <div class="col-sm-12">
                                                                    <div class="btn-outer text-right">

                                                                        <input type="hidden" value="" id="ManagementFeeclick"/>
                                                                        <button class="blue-btn" id="AddManagementfeeButton">Update</button>
                                                                        <button rel="#collapseSix" type="button" class="grey-btn clearEditFormReset">Reset</button>
                                                                        <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <input type="hidden" id="managementFeeID">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Management Info -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" data_href="#collapseSeven" class="tabcheckvalidation">
                                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Management Info</a>
                                            </h4>
                                        </div>
                                        <div id="collapseSeven" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <form action="" method="post" id="addManagementinfoForm">
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Management Company Name <em class="red-star">*</em></label>
                                                                <input class="form-control capital" type="text" maxlength="50" name="management_company_name" value="<?= $getManagementInfo['management_company_name'] ?>"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Management Start Date <em class="red-star">*</em></label>
                                                                <input class="form-control calander" type="text" name="management_start_date" id="management_start_date" readonly="" value="<?= dateFormatUser($getManagementInfo['management_start_date'], null, $this->companyConnection); ?>"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Management End Date <em class="red-star">*</em></label>
                                                                <input class="form-control calander" type="text" name="management_end_date" id="management_end_date" readonly="" placeholder="Eg: 10/10/2014" value="<?= dateFormatUser($getManagementInfo['management_end_date'], null, $this->companyConnection); ?>">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Reason the Management Ended <a class="pop-add-icon NewReasonPlus" onclick="clearPopUps('#NewReasonPopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select class="form-control" name="reason_management_end" id="selectreasondiv">

                                                                </select>
                                                                <div class="add-popup NewkeyPopup" id="NewReasonPopup">
                                                                    <h4>Add New Reason </h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>Reason<em class="red-star">*</em></label>
                                                                                <input class="form-control capital customValidateNewReasonPopup" data_required="true" data_max="150" type="text" name='@reason'/>
                                                                                <span class="customError required"></span>
                                                                            </div>

                                                                            <div class="btn-outer text-right">
                                                                                <button type="button" class="blue-btn NewReasonSave" data_id="" >Save</button>
                                                                                <button rel="NewkeyPopup" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                <input type="button"  class="grey-btn cancelPopup NewkeyPopupCancel" value='Cancel'/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Description <em class="red-star">*</em></label>
                                                                <div class="notes_date_right_div">
                                                                    <textarea class="form-control capital notes_date_right" maxlength="50" type="text" name="description" value="<?= $getManagementInfo['description'] ?>"></textarea></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="btn-outer text-right">
                                                                <input type="hidden" id="AddManagementinfoclick" value="">
                                                                <button class="blue-btn" id="AddManagementinfoButton">Update</button>
                                                                <button rel ="#collapseSeven" type="button" class="grey-btn clearEditFormReset">Reset</button>
                                                                <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Maintenance Info -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight" data_href="#collapseEight" class="tabcheckvalidation">
                                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Maintenance Information</a>
                                            </h4>
                                        </div>
                                        <div id="collapseEight" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <form action="" method="post" id="addMaintenanceinformationForm">
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Maintenance Spending Limit</label>
                                                                <select class="form-control maintenance_speed_time"  name="maintenance_speed_time">
                                                                    <option value="No Spending Limit" <?php
                                                                    if ($getMaintenanceInformation['maintenance_speed_time'] == "No Spending Limit") {
                                                                        echo "selected";
                                                                    } else {
                                                                        " ";
                                                                    }
                                                                    ?>>No Spending Limit</option>
                                                                    <option value="Monthly" <?php
                                                                    if ($getMaintenanceInformation['maintenance_speed_time'] == "Monthly") {
                                                                        echo "selected";
                                                                    } else {
                                                                        " ";
                                                                    }
                                                                    ?>>Monthly</option>
                                                                    <option value="Quarterly" <?php
                                                                    if ($getMaintenanceInformation['maintenance_speed_time'] == "Quarterly") {
                                                                        echo "selected";
                                                                    } else {
                                                                        " ";
                                                                    }
                                                                    ?>>Quarterly</option>
                                                                    <option value="Semi-Annually" <?php
                                                                    if ($getMaintenanceInformation['maintenance_speed_time'] == "Semi-Annually") {
                                                                        echo "selected";
                                                                    } else {
                                                                        " ";
                                                                    }
                                                                    ?>>Semi-Annually</option>
                                                                    <option value="Annual" <?php
                                                                    if ($getMaintenanceInformation['maintenance_speed_time'] == "Annual") {
                                                                        echo "selected";
                                                                    } else {
                                                                        " ";
                                                                    }
                                                                    ?>>Annual</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Enter Amount <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                                                <input class="form-control amount number_only" type="text" id="maintenance_amount" name="amount" disabled="" value="<?= $getMaintenanceInformation['amount'] ?>"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Insurance Expiration <em class="red-star">*</em></label>
                                                                <input class="form-control calander" type="text" id="InsuranceExpiration" name="Insurance_expiration" readonly="" placeholder="Eg: 06/13/2019" value="<?= dateFormatUser($getMaintenanceInformation['Insurance_expiration'], null, $this->companyConnection); ?>"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Home Warranty Expiration <em class="red-star">*</em></label>
                                                                <input class="form-control calander" type="text" id="HomeWarrantyExpiration" name="warranty_expiration" readonly="" placeholder="Eg: 06/13/2019" value="<?= dateFormatUser($getMaintenanceInformation['warranty_expiration'], null, $this->companyConnection); ?>"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3 textarea-form">
                                                                <label>Special Instructions <em class="red-star">*</em></label>
                                                                <div class="notes_date_right_div">
                                                                    <textarea class="form-control capital notes_date_right" maxlength="200" name="special_instructions" placeholder="Instructions"><?= $getMaintenanceInformation['special_instructions'] ?></textarea></div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer text-right">
                                                                    <input  type="hidden" id="AddMaintenanceinfoclick" value=""/>
                                                                    <button class="blue-btn" id="AddMaintenanceinfoButton">Update</button>
                                                                    <button rel="#collapseEight" type="button" class="grey-btn clearEditFormReset">Reset</button>
                                                                    <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwenty1" data_href="#collapseTwenty1" class="tabcheckvalidation">
                                                <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Maintenance Log</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwenty1" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="col-sm-12">
                                                <div class="btn-outer text-right" style="margin-bottom: 25px;">
                                                    <a rel="<?= $_GET['id']; ?>" id="redirection_work_order" class="blue-btn">Add New Work Order</a>
                                                </div>
                                            </div>
                                            <!-- Wating List table-->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="grid-outer listinggridDiv">
                                                        <div class="apx-table apxtable-bdr-none">
                                                            <div class="table-responsive">
                                                                <table id="property_work_order" class="table table-bordered">
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Mortgage Info -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseNine" data_href="#collapseNine" class="tabcheckvalidation">
                                                <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Mortgage Information</a>
                                        </h4>
                                    </div>
                                    <div id="collapseNine" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <form action="" method="post" id="addMortgageinformationForm">
                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Mortgage Amount <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> <em class="red-star">*</em></label>
                                                            <input class="form-control amount number_only" type="text" id="mortgageAmount" name="mortgage_amount" maxlength="10" placeholder="Eg: 10.00" value="<?= $getMortgageInformation['mortgage_amount'] ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Property Assessed Amount <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> <em class="red-star">*</em></label>
                                                            <input class="form-control amount number_only" type="text" id='property_assessed_amount' name="property_assessed_amount" maxlength="10" placeholder="Eg: 10.00" value="<?= $getMortgageInformation['property_assessed_amount'] ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Mortgage Start Date <em class="red-star">*</em></label>
                                                            <input class="form-control calander" type="text" name="mortgage_start_date" id="mortgage_start_date" placeholder="Eg: 05/31/2019" readonly="" value="<?= dateFormatUser($getMortgageInformation['mortgage_start_date'], null, $this->companyConnection) ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Term <em class="red-star">*</em></label>
                                                            <input class="form-control number_only" type="text" maxlength="2" name="term" placeholder="Eg: 10" value="<?= $getMortgageInformation['term'] ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Interest Rate % <em class="red-star">*</em></label>
                                                            <input class="form-control" type="text" id="interest_rate" name="interest_rate" placeholder="Eg: 6" value="<?= $getMortgageInformation['interest_rate']; ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Financial Institution <em class="red-star">*</em></label>
                                                            <input class="form-control capital" type="text" name="financial_institution" maxlength="20" placeholder="Financial Institution" value="<?= $getMortgageInformation['financial_institution'] ?>"/>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="btn-outer text-right">
                                                                <input type="hidden" value="" id="AddMortgageinfoclick">
                                                                <button type="submit" class="blue-btn" id="AddMortgageinfoButton">Update</button>
                                                                <button rel="#collapseNine" type="button" class="grey-btn clearEditFormReset">Reset</button>
                                                                <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Property Loan -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen" data_href="#collapseTen" class="tabcheckvalidation">
                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Property Loan</a>
                                    </h4>
                                </div>
                                <div id="collapseTen" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <form action="" method="post" id="addPropertyloanForm">
                                            <input type="hidden" id="loanAutoid" value="" name="id">
                                            <div class="row">
                                                <div class="latefee-check-outer">
                                                    <div class="col-sm-12 latefee-check-hdr">
                                                        <div class="check-outer">
                                                            <label class="blue-label">Loan</label>
                                                        </div>
                                                        <div class="check-outer">
                                                            <input type="radio" name="loan_status" value="1" class="loan_status_class" <?php
                                                            if ($getPropertyLoan['loan_status'] == 1) {
                                                                echo "checked";
                                                            } else {
                                                                echo "";
                                                            }
                                                            ?>/>
                                                            <label>Yes</label>
                                                        </div>
                                                        <div class="check-outer">
                                                            <input type="radio" name="loan_status" value="0" class="loan_status_class" <?php
                                                            if ($getPropertyLoan['loan_status'] == 0) {
                                                                echo "checked";
                                                            } else {
                                                                echo "";
                                                            }
                                                            ?>/>
                                                            <label>No</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if ($getPropertyLoan['loan_status'] == 1) { ?>
                                                <div class="row" id="loanDiv">
                                                    <div class="form-outer">

                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label> Vendor Name(Bank or Lender) <em class="red-star">*</em></label>
                                                            <input class="form-control capital" maxlength="30" type="text" name="vendor_name" value="<?= $getPropertyLoan['vendor_name']; ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Loan Amount <em class="red-star">*</em></label>
                                                            <input class="form-control" id='loan_amount' type="text" name="loan_amount" value="<?= $getPropertyLoan['loan_amount']; ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Loan Number</label>
                                                            <input class="form-control" maxlength="20" type="text" name="loan_number" value="<?= $getPropertyLoan['loan_amount']; ?>" id="loan_number"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Loan Start Date</label>
                                                            <input class="form-control calander" type="text" name="loan_start_date" id="loan_start_date" readonly="" value="<?= dateFormatUser($getPropertyLoan['loan_start_date'], null, $this->companyConnection); ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Loan Maturity Date <em class="red-star">*</em></label>
                                                            <input class="form-control calander" type="text" name="loan_maturity_date" id="loan_maturity_date" readonly="" placeholder="Eg: 10/10/2014" value="<?= dateFormatUser($getPropertyLoan['loan_maturity_date'], null, $this->companyConnection); ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Monthly Due Date</label>
                                                            <select style="cursor: pointer;" id="monthly_due_date" class="form-control" name="monthly_due_date">
                                                                <option value="Select">Select</option>
                                                                <?php
                                                                for ($i = 1; $i <= 31; $i++) {
                                                                    echo "<option value='" . $i . "'" . (($getPropertyLoan['monthly_due_date'] == $i) ? "selected" : "") . ">$i</option>";
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Payment Amount <em class="red-star">*</em></label>
                                                            <input class="form-control" id='payment_amount' type="text" name="payment_amount" value="<?= $getPropertyLoan['payment_amount']; ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">

                                                            <label>Interest Only</label>

                                                            <div class="check-outer">
                                                                <input type="radio" name="interest_only" value="yes"  <?php
                                                                if ($getPropertyLoan['interest_only'] == 'yes') {
                                                                    echo "checked";
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?>/>
                                                                <label>Yes</label>
                                                            </div>
                                                            <div class="check-outer">
                                                                <input type="radio" name="interest_only" value="no"  <?php
                                                                if ($getPropertyLoan['interest_only'] == 'no') {
                                                                    echo "checked";
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?>/>
                                                                <label>No</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Loan Terms <em class="red-star">*</em></label>
                                                            <input class="form-control" type="text" name="loan_terms" placeholder="Interest Rate" value="<?= $getPropertyLoan['loan_terms']; ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>&nbsp;</label>
                                                            <select placeholder="Select the Rate"  name="loan_type" class="form-control">
                                                                <option value=" ">Select</option>
                                                                <option value="Fixed Rate" <?php
                                                                if ($getPropertyLoan['loan_type'] == 'Fixed Rate') {
                                                                    echo "selected";
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?>>Fixed Rate</option>
                                                                <option value="Variable Rate" <?php
                                                                if ($getPropertyLoan['loan_type'] == 'Variable Rate') {
                                                                    echo "selected";
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?>>Variable Rate</option>
                                                            </select>

                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>&nbsp;&nbsp;<em class="red-star">*</em></label>
                                                            <input class="form-control amount number_only" type="text" name="loan_rate" placeholder="Enter Rate" value="<?= $getPropertyLoan['loan_rate']; ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="row" id="loanDiv" style="display:none">
                                                    <div class="form-outer">
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label> Vendor Name(Bank or Lender) </label>
                                                            <input class="form-control capital" maxlength="30" type="text" name="vendor_name" value="<?= $getPropertyLoan['vendor_name']; ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Loan Amount <em class="red-star">*</em></label>
                                                            <input class="form-control" id='loan_amount' type="text" name="loan_amount" value="<?= $getPropertyLoan['loan_amount']; ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Loan Number</label>
                                                            <input class="form-control" maxlength="20" type="text" name="loan_number" value="<?= $getPropertyLoan['loan_amount']; ?>" id="loan_number"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Loan Start Date <em class="red-star">*</em></label>
                                                            <input class="form-control calander" type="text" name="loan_start_date" id="loan_start_date" readonly="" value="<?= dateFormatUser($getPropertyLoan['loan_start_date'], null, $this->companyConnection); ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Loan Maturity Date <em class="red-star">*</em></label>
                                                            <input class="form-control" type="text" name="loan_maturity_date" id="loan_maturity_date" readonly="" placeholder="Eg: 10/10/2014" value="<?= dateFormatUser($getPropertyLoan['loan_maturity_date'], null, $this->companyConnection); ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Monthly Due Date</label>
                                                            <select style="cursor: pointer;" id="monthly_due_date" class="form-control" name="monthly_due_date">
                                                                <option value="Select">Select</option>
                                                                <?php
                                                                for ($i = 1; $i <= 31; $i++) {
                                                                    echo "<option value='" . $i . "'" . (($getPropertyLoan['monthly_due_date'] == $i) ? "selected" : "") . ">$i</option>";
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Payment Amount <em class="red-star">*</em></label>
                                                            <input class="form-control" id='payment_amount' type="text" name="payment_amount" value="<?= $getPropertyLoan['payment_amount']; ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3 InterestOnly">

                                                            <label>Interest Only <em class="red-star">*</em></label>

                                                            <div class="check-outer">
                                                                <input type="radio" name="interest_only" value="yes"  <?php
                                                                if ($getPropertyLoan['interest_only'] == 'yes') {
                                                                    echo "checked";
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?>/>
                                                                <label>Yes</label>
                                                            </div>
                                                            <div class="check-outer">
                                                                <input type="radio" name="interest_only" value="no"  <?php
                                                                if ($getPropertyLoan['interest_only'] == 'no') {
                                                                    echo "checked";
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?>/>
                                                                <label>No</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Loan Terms <em class="red-star">*</em></label>
                                                            <input class="form-control" type="text" name="loan_terms" placeholder="Interest Rate" value="<?= $getPropertyLoan['loan_terms']; ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>&nbsp;</label>
                                                            <select placeholder="Select the Rate"  name="loan_type" class="form-control">
                                                                <option value=" ">Select</option>
                                                                <option value="Fixed Rate" <?php
                                                                if ($getPropertyLoan['loan_type'] == 'Fixed Rate') {
                                                                    echo "selected";
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?>>Fixed Rate</option>
                                                                <option value="Variable Rate" <?php
                                                                if ($getPropertyLoan['loan_type'] == 'Variable Rate') {
                                                                    echo "selected";
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?>>Variable Rate</option>
                                                            </select>

                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>&nbsp;</label>
                                                            <input class="form-control amount number_only" type="text" name="loan_rate" placeholder="Enter Rate" value="<?= $getPropertyLoan['loan_rate']; ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="form-outer2">
                                                <div class="btn-outer text-right">
                                                    <input type="hidden" id="AddPropertyloanclick" value="">
                                                    <button class="blue-btn" id="AddPropertyloanButton">Update</button>
                                                    <button rel="#collapseTen" type="button" class="grey-btn clearEditFormReset">Reset</button>
                                                    <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Property Insurance -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" data_href="#collapseEleven" class="tabcheckvalidation">
                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Property Insurance</a>
                                    </h4>
                                </div>
                                <div id="collapseEleven" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <form  method="post" id='addPropertyInsurance'>
                                            <div class="row">
                                                <div class="latefee-check-outer">
                                                    <div class="div-full" id="insuranceOuterDivhref">
                                                        <a href="javascript:;"  class="active notes-button-bold">Add New Insurance</a>
                                                    </div>
                                                </div>
                                                <div id="insuranceOuterDivOpen" style="display:none;">
                                                    <div class="col-sm-12">
                                                        <div class="grey-box-add" class='insuranceOuterDiv'>
                                                            <div class="insuranceOuterDivId" id='insuranceOuterDivId'>
                                                                <div class="grey-box-add-inner">
                                                                    <div class="grey-box-add-lt" >


                                                                        <div class="form-outer">
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Insurance Type <a class="pop-add-icon NewInsurancePlus" onclick="clearPopUps('.NewInsurancePopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a><em class="red-star">*</em></label>
                                                                                <select name='company_insurance_type[]' id="SelectInsuranceTypes" class="form-control"></select>
                                                                                <div class="add-popup NewInsurancePopup">
                                                                                    <h4>Add New Insurance Type</h4>
                                                                                    <div class="add-popup-body">
                                                                                        <div class="form-outer">
                                                                                            <div class="col-sm-12">
                                                                                                <label>Insurance Type <em class="red-star">*</em></label>
                                                                                                <input class="form-control customValidateNewInsurance" data_required="true" data_max="150" type="text" name='insurance_type' id='insurance_type' placeholder="Add New Insurance Type"/>
                                                                                                <span class="customError required"></span>
                                                                                                <span class="portfolio_idErr error"></span>
                                                                                            </div>
                                                                                            <div class="btn-outer text-right">
                                                                                                <input type="button"  class="blue-btn NewInsurancePopupSave" value="Save" data_id="" />
                                                                                                <button rel="NewInsurancePopup" type="button" class="grey-btn clearFormResetPopup">Clear</button>
                                                                                                <input type="button"  class="grey-btn cancelPopup NewInsuranceCancel" value='Cancel' />
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Insurance Company Name <em class="red-star">*</em></label>
                                                                                <input class="form-control capital" type="text" maxlength="50" placeholder="InsuranceCompanyName" name='insurance_company_name[]' id="insurance_company_name"/>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Policy #</label>
                                                                                <input class="form-control capital" type="text" maxlength="20" placeholder="Policy#" name="policy[]" id="policy"/>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Policy Type  <a  class="pop-add-icon NewPolicyPlus" onclick="clearPopUps('#NewPolicyPopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                <select name='policy_name[]' id='SelectPolicyTypes' class="form-control SelectPolicyTypesClass"></select>
                                                                                <div class="add-popup NewPolicyPopup" id='NewPolicyPopup'>
                                                                                    <h4>Add New Policy Type</h4>
                                                                                    <div class="add-popup-body">
                                                                                        <div class="form-outer">
                                                                                            <div class="col-sm-12">
                                                                                                <label>Policy Type<em class="red-star">*</em></label>
                                                                                                <input class="form-control customValidateNewPolicy" data_required="true" data_max="150" type="text" name='policy_type'  placeholder="Add New Policy Type"/>
                                                                                                <span class="customError required"></span>
                                                                                                <span class="portfolio_idErr error"></span>
                                                                                            </div>
                                                                                            <div class="btn-outer text-right">
                                                                                                <input type="button" class="blue-btn NewPolicyPopupSave" value="Save" data_id="" />
                                                                                                <button rel="NewPolicyPopup" type="button" class="grey-btn clearFormResetPopup">Clear</button>
                                                                                                <input type="button" class="grey-btn cancelPopup NewPolicyPopupCancel" value='Cancel' />
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Agent Name <em class="red-star">*</em></label>
                                                                                <input class="form-control capital" type="text" maxlength="50" placeholder="Agent Name" name='agent_name[]' id="agent_name"/>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Country Code <em class="red-star">*</em> </label>
                                                                                <select class='form-control' name='insurance_country_code[]' id="insurance_country_code">

                                                                                </select>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Phone Number <em class="red-star">*</em></label>
                                                                                <input class="form-control phone_number" type="text" placeholder="Phone Number" name='phone_number[]' id="ins_phone_number" maxlength="12"/>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Email <em class="red-star">*</em></label>
                                                                                <input class="form-control capitalremove" type="text" maxlength="50" placeholder="Email" name='property_insurance_email[]' id="property_insurance_email"/>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Coverage Amount <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                                                                <input class="form-control amount number_only" type="text" maxlength="10" placeholder="Coverage Amount" name='coverage_amount[]' id="coverage_amount"/>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Annual Premium <em class="red-star">*</em><?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                                                                <input class="form-control amount number_only" type="text"  maxlength="10" placeholder="Annual Premium" name='annual_premium[]' id="annual_premium"/>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Start Date <em class="red-star">*</em></label>
                                                                                <input class="form-control ins_start_date calander" type="text" placeholder="Start date" name='start_date[]'  readonly=""/>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>End Date <em class="red-star">*</em></label>
                                                                                <input class="form-control ins_end_date calander" type="text" placeholder="End date" name='end_date[]'  readonly=""/>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Renewal Date <em class="red-star">*</em></label>
                                                                                <input class="form-control ins_renewal_date calander" type="text" placeholder="Renewal Date" name='renewal_date[]'  readonly=""/>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Notes <em class="red-star">*</em></label>
                                                                                <div class="notes_date_right_div">
                                                                                    <textarea class="form-control capital notes_date_right" type="text" maxlength="200" placeholder="" name='notes[]' id='ins_notes'></textarea></div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="grey-box-add-rt">
                                                                        <a id='propertyInsurancePlus' class="pop-add-icon propertyInsurancePlus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                        <a id='propertyInsuranceCross' class="propertyInsuranceCross" style='display: none;' href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <div class="btn-outer text-right">
                                                                <input type='hidden' id='PropertyInsuranceid' value="">
                                                                <input type='hidden' id='AddPropertyInsuranceInput' value="">
                                                                <button class="blue-btn" id='AddPropertyInsuranceSave1'>Save</button>
                                                                <button rel="addPropertyInsurance" type="button" class="grey-btn clearFormResetIns">Reset</button>
                                                                <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid-outer listinggridDiv">
                                                    <div class="apx-table apxtable-bdr-none">
                                                        <div class="table-responsive">
                                                            <table id="insurance-table" class="table table-bordered">
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwenty12" data_href="#collapseTwenty12" class="tabcheckvalidation">
                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Inventory </a>
                                    </h4>
                                </div>
                                <div id="collapseTwenty12" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="col-sm-12">
                                            <div class="btn-outer text-right" style="margin-bottom: 25px;">
                                                <button  class="blue-btn" id="addInventoryDiv" >Add Inventory</button>
                                            </div>
                                        </div>
                                        <div id="inventoryDiv" class="col-sm-12" style="display: none;">
                                            <div class="flagFormDiv">
                                                <form id="propertyInventory">
                                                <input type="hidden" name="id" id="propertyInventoryId" value="">
                                                <div class='grey-box-add'>
                                                    <div class="grey-box-add-inner">
                                                        <div class="grey-box-add-lt">
                                                            <div class="form-outer">
                                                                <div class="col-xs-12 no-padding">
                                                                    <div class="col-xs-12 col-sm-3 col-md-3">
                                                                        <label>Select Type</label>
                                                                        <select id="selectType" name="select_type" class="form-control">
                                                                            <option value="0">Select</option>
                                                                            <option value="1">Building</option>
                                                                            <option value="2">Unit</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 no-padding building_div_select" style="display: none;">
                                                                    <div class="col-xs-12 col-sm-3 col-md-3">
                                                                        <label>Select Building</label>
                                                                        <select id="selectBuilding" name="building_id" class="form-control">
                                                                            <option value="0">Select</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 no-padding unit_div_select" style="display: none;">
                                                                    <div class="col-xs-12 col-sm-3 col-md-3">
                                                                        <label>Select Unit</label>
                                                                        <select id="selectUnit" name="unit_id" class="form-control">
                                                                            <option value="0">Select</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 no-padding">
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Inventory Photo/Image</label>
                                                                        <div class="upload-logo">
                                                                            <div class="inventory_img1 img-outer" id="image_id1"><img id="inventory_img1" src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/dummy-img.jpg"></div>
                                                                            <a href="javascript:;">
                                                                                <i class="fa fa-pencil-square" aria-hidden="true"></i>Choose Image</a>
                                                                        </div>
                                                                        <div class="image-editor">
                                                                            <input type="file" class="cropit-image-input" name="inventory_img1">
                                                                            <div class='cropItData' style="display: none;">
                                                                                <span class="closeimagepopupicon">X</span>
                                                                                <div class="cropit-preview"></div>
                                                                                <div class="image-size-label">Resize image</div>
                                                                                <input type="range" class="cropit-image-zoom-input">
                                                                                <button type="button" id="rotate-ccw" class="rotate-ccw">Rotate counterclockwise</button>
                                                                                <button type="button" id="rotate-cw" class="rotate-cw">Rotate clockwise</button>
                                                                                <input type="hidden" class="hidden-image-data"/>
                                                                                <input type="button" class="export" value="Done"
                                                                                       data-val="inventory_img1">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Inventory Photo/Image</label>
                                                                        <div class="upload-logo">
                                                                            <div class="inventory_img2 img-outer" id="image_id2"><img id="inventory_img2" src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/dummy-img.jpg"></div>
                                                                            <a href="javascript:;">
                                                                                <i class="fa fa-pencil-square" aria-hidden="true"></i>Choose Image</a>
                                                                        </div>
                                                                        <div class="image-editor">
                                                                            <input type="file" class="cropit-image-input" name="inventory_img1">
                                                                            <div class='cropItData' style="display: none;">
                                                                                <span class="closeimagepopupicon">X</span>
                                                                                <div class="cropit-preview"></div>
                                                                                <div class="image-size-label">Resize image</div>
                                                                                <input type="range" class="cropit-image-zoom-input">
                                                                                <button type="button" id="rotate-ccw" class="rotate-ccw">Rotate counterclockwise</button>
                                                                                <button type="button" id="rotate-cw" class="rotate-cw">Rotate clockwise</button>
                                                                                <input type="hidden" class="hidden-image-data"/>
                                                                                <input type="button" class="export" value="Done"
                                                                                       data-val="inventory_img2">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Inventory Photo/Image</label>
                                                                        <div class="upload-logo">
                                                                            <div class="inventory_img3 img-outer" id="image_id3"><img id="inventory_img3" src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/dummy-img.jpg"></div>
                                                                            <a href="javascript:;">
                                                                                <i class="fa fa-pencil-square" aria-hidden="true"></i>Choose Image</a>
                                                                        </div>
                                                                        <div class="image-editor">
                                                                            <input type="file" class="cropit-image-input" name="inventory_img1">
                                                                            <div class='cropItData' style="display: none;">
                                                                                <span class="closeimagepopupicon">X</span>
                                                                                <div class="cropit-preview"></div>
                                                                                <div class="image-size-label">Resize image</div>
                                                                                <input type="range" class="cropit-image-zoom-input">
                                                                                <button type="button" id="rotate-ccw" class="rotate-ccw">Rotate counterclockwise</button>
                                                                                <button type="button" id="rotate-cw" class="rotate-cw">Rotate clockwise</button>
                                                                                <input type="hidden" class="hidden-image-data"/>
                                                                                <input type="button" class="export" value="Done"
                                                                                       data-val="inventory_img3">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>SubLocation</label>
                                                                    <select id="sublocation" name="sublocation" class="form-control">
                                                                        <option value="0">Select</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Category</label>
                                                                    <select class="form-control" id="inventoryCategory" name="category">
                                                                        <option value="0">Select</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>SubCategory</label>
                                                                    <select class="form-control" id="inventorySubcategory" name="sub_category">
                                                                        <option value="0">Select</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Item Code</label>
                                                                    <select class="form-control" name="item_code">
                                                                        <option value="0">Select</option>
                                                                    </select>
                                                                </div>


                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Warranty From</label>
                                                                    <input class="form-control calander" id="warranty_from" type="text" name="warranty_from" readonly/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Warranty To</label>
                                                                    <input class="form-control calander" id="warranty_to" type="text" name="warranty_to" readonly/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Installation Date</label>
                                                                    <input class="form-control calander" id="installation_date" type="text" name="installation_date" readonly/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Cost</label>
                                                                    <input class="form-control number_only amount_num" type="text" placeholder="Cost" name="cost"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Replacement Date</label>
                                                                    <input class="form-control calander" type="text" id="replacement_date" name="replacement_date" readonly/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Guarantee</label>
                                                                    <select class="form-control" name="guarantee">
                                                                        <option value="0">Select</option>
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                        <option value="13">13</option>
                                                                        <option value="14">14</option>
                                                                        <option value="15">15</option>
                                                                        <option value="16">16</option>
                                                                        <option value="17">17</option>
                                                                        <option value="18">18</option>
                                                                        <option value="19">19</option>
                                                                        <option value="20">20</option>
                                                                        <option value="21">21</option>
                                                                        <option value="22">22</option>
                                                                        <option value="23">23</option>
                                                                        <option value="24">24</option>
                                                                        <option value="25">25</option>
                                                                        <option value="26">26</option>
                                                                        <option value="27">27</option>
                                                                        <option value="28">28</option>
                                                                        <option value="29">29</option>
                                                                        <option value="30">30</option>
                                                                        <option value="41">41</option>
                                                                        <option value="42">42</option>
                                                                        <option value="43">43</option>
                                                                        <option value="44">44</option>
                                                                        <option value="45">45</option>
                                                                        <option value="46">46</option>
                                                                        <option value="47">47</option>
                                                                        <option value="48">48</option>
                                                                        <option value="49">49</option>
                                                                        <option value="50">50</option>
                                                                        <option value="Lifetime">Lifetime</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-12 no-padding">
                                                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                                                        <label>Add Description </label>
                                                                        <div class="notes_date_right_div">
                                                                            <textarea class="form-control notes_date_right" name='description' maxlength="500"></textarea></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#collapseTwenty12" >
                                                                    Installer Information </a>
                                                            </h4>
                                                        </div>
                                                        <div class="panel-collapse">
                                                            <div class="panel-body">
                                                                <div calss="col-xs-12">
                                                                    <div class="form-outer2">
                                                                        <div class="col-xs-12 col-sm-3 col-md-3">
                                                                            <label>Company Name</label>
                                                                            <input class="form-control" type="text" name="company_name" placeholder="Enter Company Name" />
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Company Person</label>
                                                                            <input class="form-control" type="text" name="company_person" placeholder="Enter Name Here"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Phone Number</label>
                                                                            <input class="form-control phone_format_inventory" type="text" name="phone_number" placeholder="Enter Contact# Here"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Add Notes</label>
                                                                            <div class="notes_date_right_div">
                                                                                <textarea name='description' placeholder="Add Notes" name="notes" class="form-control capital notes_date_right"></textarea></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#collapseTwenty12">
                                                                    Notes</a>
                                                            </h4>
                                                        </div>
                                                        <div class="panel-collapse">
                                                            <div class="panel-body">
                                                                <div calss="col-xs-12">
                                                                    <div class="form-outer2">
                                                                        <div class="col-xs-12 no-padding">
                                                                            <div class="col-xs-12 col-sm-6 col-md-6 notesDateTime notes_date_right_div">
                                                                                <textarea class="form-control notes_date_right" name='secondary_note' placeholder="Add Notes For Inventory"></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <div class="btn-outer text-right">
                                                            <button class="blue-btn" type="submit">Save</button>
                                                            <button rel="#collapseTwenty12" type="button" class="clear-btn clearEditFormReset">Reset</button>
                                                            <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            </div>
                                        </div>
                                        <!-- Wating List table-->
                                        <div class="grid-outer listinggridDiv">
                                            <div class="apx-table apxtable-bdr-none">
                                                <div class="table-responsive">
                                                    <table id="inventory-table" class="table table-bordered">
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 min-height-0">
                                            <div class="btn-outer text-right">
                                                <button type="button" id="inventory_email" class="blue-btn">Email</button>
                                                <button type="button" id="inventory_preview" class="blue-btn">Preview</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Warranty Information -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" data_href="#collapseTwelve" class="tabcheckvalidation">
                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Warranty Information </a>
                                    </h4>
                                </div>
                                <div id="collapseTwelve" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <form method="post" id="addwarrantyinforform">
                                            <div class="latefee-check-outer">
                                                <div class="div-full" id="warrantyinforhref">
                                                    <a href="javascript:;"  class="active notes-button-bold">Add Fixtures</a>
                                                </div>
                                            </div>
                                            <div id="warrantyinfoDivopen" style="display:none">
                                                <div class="form-outer">
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Key Fixture Name <em class="red-star">*</em></label>
                                                        <input class="form-control" type="text" maxlength="50" name="key_fixture_name" placeholder="Key Fixture Name" id="key_fixture_name"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Fixture Type <a id='FixturePopupPlus' class="pop-add-icon" href="javascript:;" onclick="clearPopUps('#AddFixturePopup')"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                        <select id='fixture_type' name="fixture_type" class="form-control"></select>
                                                        <div class="add-popup AddFixturePopupClass" id='AddFixturePopup'>
                                                            <h4>Add New Fixture Type</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>Fixture Type<em class="red-star">*</em></label>
                                                                        <input class="form-control capital customValidateAddFixture" data_required="true" data_max="50" type="text" name='@fixture_type' id='fixture_type' placeholder="Add New Fixture Type"/>
                                                                        <span class="customError required"></span>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <label>Fixture Description<em class="red-star">*</em></label>
                                                                        <input class="form-control capital customValidateAddFixture" data_required="true" data_max="150" type="text" name='@fixture_desc' id='fixture_desc' placeholder="Fixture Description"/>
                                                                        <span class="customError required"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="submit" id='FixturePopupSave' class="blue-btn">Save</button>
                                                                        <button rel="AddFixturePopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                        <input type="button"  class="cancelPopup grey-btn" value='Cancel' />
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Model</label>
                                                        <?php
                                                        $yearArray = range( 2019,1900);
                                                        ?>
                                                        <select class="form-control" name="model" id="model">
                                                            <option value="">Select Year</option>
                                                            <?php
                                                            foreach ($yearArray as $year) {
                                                                $selected = ($year == 2019) ? 'selected' : '';
                                                                echo '<option ' . $selected . ' value="' . $year . '">' . $year . '</option>';
                                                            }
                                                            ?>
                                                        </select>

                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Assessed Age</label>
                                                        <input id='assessed_age' class="form-control number_only" maxlength="3" type="text" name='assessed_age' placeholder="Assessed Age"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Maintenance Reminder <em class="red-star">*</em></label>
                                                        <input type='text' id='maintenance_reminder' name='maintenance_reminder' class="form-control" readonly="" placeholder="Maintenance Reminder">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Warranty Expiration Date <em class="red-star">*</em></label>
                                                        <input class="form-control" type="text" id='warranty_expiration_date' name='warranty_expiration_date' readonly="" placeholder="Maintenance Reminder"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Insurance Expiration Date <em class="red-star">*</em> </label>
                                                        <input type='text' class="form-control" name='insurance_expiration_date' id='insurance_expiration_date' readonly="" placeholder="Insurance Expiration">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Manufacturer's Name <em class="red-star">*</em></label>
                                                        <input id='manufacturer_name' class="form-control" maxlength="50" type="text" name='manufacturer_name'/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Manufacturer's Phone # <em class="red-star">*</em></label>
                                                        <input id='manufacturer_phonenumber' class="form-control phone_number" type="text" name='manufacturer_phonenumber' maxlength="12"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Condition <em class="red-star">*</em></label>
                                                        <select class="form-control" style="cursor: pointer;"  name="condition_fixture">
                                                            <option value="Fair" selected="selected">Fair</option>
                                                            <option value="Good">Good</option>
                                                            <option value="Excellent">Excellent</option>
                                                            <option value="Bad">Bad</option>
                                                            <option value="Horrible">Horrible</option>
                                                            <option value="Other">Other</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Add Notes <em class="red-star">*</em></label>
                                                        <div class="notes_date_right_div">
                                                        <textarea id='add_notes' class="form-control capital notes_date_right" maxlength="500" name='add_notes' placeholder="Notes">

                                                        </textarea></div>
                                                    </div>
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <div class="btn-outer text-right">
                                                                <input type='hidden' id='Warrantyinfoid' name='id' value="">
                                                                <input type='hidden' id='warrantyinforClick' value=''>
                                                                <button class="blue-btn" id='warrantyinforClickB'>Save</button>
                                                                <button rel="addwarrantyinforform" type="button" class="clear-btn clearFormResetFixture">Reset</button>
                                                                <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid-outer listinggridDiv">
                                                <div class="apx-table apxtable-bdr-none">
                                                    <div class="table-responsive">
                                                        <table id="fixtures-table" class="table table-bordered">
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Manage Charge -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen" data_href="#collapseThirteen" class="tabcheckvalidation">
                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Manage Charges </a>
                                    </h4>
                                </div>
                                <div id="collapseThirteen" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="accordion-grid">
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div class="panel panel-default apxtable-bdr-none">
                                                                    <!-- Unit Charge table-->
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <div class="apx-table">
                                                                                <div class="table-responsive">
                                                                                    <table id="propertyCharge-table" class="table table-bordered">
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-outer">
                                                <div class="col-sm-12">
                                                    <div class="btn-outer text-right">
                                                        <button type="button" id="propertyChargeCodeSave" class="blue-btn">Apply</button>
                                                        <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="accordion-grid">
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div class="panel panel-default apxtable-bdr-none">
                                                                    <!-- Unit Charge table-->
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <div class="apx-table">
                                                                                <div class="table-responsive">
                                                                                    <table id="unitCharge-table" class="table table-bordered">
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-outer">
                                                    <div >
                                                        <div class="btn-outer text-right">
                                                            <button type="button" id="saveUnitCharges" class="blue-btn">Apply</button>
                                                            <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default apxtable-bdr-none">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsePropertyPrefrence" aria-expanded="true" >
                                                                            <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Charges : Unit</a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapsePropertyPrefrence" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <form id="manageCharge">
                                                                        <div class="panel-body pad-none">
                                                                            <div class="property-status">
                                                                                <div class="check-outer">
                                                                                    <input type="radio" checked class="unitSelect" name="charge_code_type" value="0">
                                                                                    <label>Property</label>
                                                                                </div>
                                                                                <div class="check-outer">
                                                                                    <input type="radio" class="unitSelect" name="charge_code_type" value="1">
                                                                                    <label>Unit</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Charge Code <a class="pop-add-icon" id="addChargeCodeButton" href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#chargeCodeModel"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                    <select class="form-control" name="charge_code" id="chargeCodeSelect"></select>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3" id="unitTypePropertyDiv" style="display: none;">
                                                                                    <label>Unit Type <a class="pop-add-icon" id="AddNewUnitTypeModalPlus3" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                    <select class="form-control" name="unit_type_id" multiple="multiple"  id="unit_type2" style="overflow-x: auto;">
                                                                                        <option value="">Select</option>
                                                                                    </select>
                                                                                    <div class="add-popup" id='NewunitPopup2'>
                                                                                        <h4>Add New Unit Type</h4>
                                                                                        <div class="add-popup-body">
                                                                                            <div class="form-outer">
                                                                                                <div class="col-sm-12">
                                                                                                    <label>Unit Type<em class="red-star">*</em></label>
                                                                                                    <input class="form-control capital customValidateUnitType2" data_required="true" data_max="150" type="text" name="@unit_type"/>
                                                                                                    <span class="customError required"></span>
                                                                                                </div>
                                                                                                <div class="col-sm-12">
                                                                                                    <label>Description</label>
                                                                                                    <input class="form-control capital customValidateUnitType2" type="text" maxlength="500" name="@description"/>
                                                                                                    <span class="customError required"></span>
                                                                                                </div>
                                                                                                <div class="btn-outer">
                                                                                                    <button type="button" class="blue-btn" id="NewunitPopupSave2">Save</button>
                                                                                                    <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Frequency <em class="red-star">*</em></label>
                                                                                    <select id="Frequency" class="form-control" name="frequency" style="cursor: pointer;">
                                                                                        <option value="">Select</option>
                                                                                        <option value="1">One Time</option>
                                                                                        <option value="2">Monthly</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3" id="typeForProperty">
                                                                                    <label>Type <em class="red-star">*</em></label>
                                                                                    <select id="typeForProperty" name="type" class="form-control" style="cursor: pointer;">
                                                                                        <option value="">Select</option>
                                                                                        <option value="1">CAM-Per Unit</option>
                                                                                        <option value="2">CAM-Per Sq.Ft.</option>
                                                                                        <option value="3">Non-CAM</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <div class="form-outer">
                                                                                        <div class="btn-outer text-right">
                                                                                            <button class="blue-btn">Save</button>
                                                                                            <button rel="#collapseThirteen" type="button" class="clear-btn clearEditFormReset">Reset</button>
                                                                                            <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Owner Prefred vendor -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen" data_href="#collapseFourteen" class="tabcheckvalidation">
                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Owner Preferred Vendors</a>
                                </h4>
                            </div>
                            <div id="collapseFourteen" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <form action='' method="post" id='owneredPreferredForm'>
                                        <div class="row">
                                            <div class="form-outer">
                                                <div class="col-xs-12 col-sm-4 col-md-3 easy-ui">
                                                    <label>Select Vendor</label>
                                                    <input class="form-control capital owner_pre_vendor" type="text" id="" name='vendor_name'/>
                                                    <input class="form-control owner_pre_vendorID" type="hidden" value=""/>

                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <label>Address</label>
                                                    <input class="form-control capital" type="text" name='vendor_address' id='vendor_address' readonly/>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <label>&nbsp;</label>
                                                    <button class="blue-btn">ADD</button>
                                                </div>
                                                <div class="grid-outer listinggridDiv">
                                                    <div class="apx-table apxtable-bdr-none">
                                                        <div class="table-responsive">
                                                            <table id="vendors-table" class="table table-bordered">
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-outer2">
                                                    <div class="col-sm-12">
                                                        <div class="btn-outer text-right">
                                                            <input type='hidden' id='owneredPreferredClickval' value="">
                                                            <button type='button' class="blue-btn" id='owneredPreferredClickR'>Remove</button>
                                                            <button class="blue-btn" id='owneredPreferredClickS'>Save</button>
                                                            <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Black List Vendor -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFifteen" data_href="#collapseFifteen" class="tabcheckvalidation">
                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Black Listed Vendors</a>
                                </h4>
                            </div>
                            <div id="collapseFifteen" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <form action="" method="post" id="blacklistvendorForm">
                                        <div class="row">
                                            <div class="form-outer">
                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <label>Select Vendor</label>
                                                    <input class="form-control capital" type="text" name="vendor_name" id="blacklistvendor_name"/>
                                                    <input class="form-control blacklist_vendorID" type="hidden" value=""/>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <label>Address</label>
                                                    <input class="form-control capital" type="text" name="vendor_address" id="blacklistvendor_address" disabled=""/>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <label>Date</label>
                                                    <input class="form-control" type="text" name="blacklist_Date" id="blacklist_Date"/>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <label>Reasoning For BlackListing <em class="red-star">*</em></label>
                                                    <input class="form-control" type="text" name="blacklist_reason" id="blacklist_reason"/>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <button class="blue-btn">ADD</button>
                                                </div>
                                                <div class="grid-outer listinggridDiv">
                                                    <div class="apx-table apxtable-bdr-none">
                                                        <div class="table-responsive">
                                                            <table id="blacklisted-table" class="table table-bordered">
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="btn-outer text-right">
                                                        <input type="hidden" id="BlacklistVendorClickval" value="">
                                                        <button type='button'class="blue-btn" id="BlacklistVendorClickR">Remove</button>
                                                        <button class="blue-btn" id="BlacklistVendorClickS">Save</button>
                                                        <button type="button" class="grey-btn cancel-all" id="BlacklistVendorClickC">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Photos/Virtual Tour Videos -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSixteen" data_href="#collapseSixteen" class="tabcheckvalidation">
                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Photos/Virtual Tour Videos</a>
                                </h4>
                            </div>
                            <div id="collapseSixteen" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-outer2">
                                            <div class="col-sm-12">
                                                <div class="btn-outer text-right">
                                                    <button type="button" id="uploadPhotoVideo" class="green-btn">Click Here to Upload</button>
                                                    <input id="photosVideos" type="file" name="photo_videos[]" accept="image/*, .mp4, .mkv, .avi" multiple style="display: none;">
                                                    <button type="button" id="removePhotoVideo" class="orange-btn">Remove All Photos</button>
                                                    <button type="button" id="savePhotoVideo" class="blue-btn">Save </button>
                                                </div>
                                            </div>
                                            <div class="col-sm-12" id="photo_video_uploads">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="grid-outer listinggridDiv">
                                                    <div class="apx-table apxtable-bdr-none">
                                                        <div class="table-responsive">
                                                            <table id="propertPhotovideos-table" class="table table-bordered">
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- File Library -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeventeen" data_href="#collapseSeventeen" class="tabcheckvalidation">
                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> File Library</a>
                                </h4>
                            </div>
                            <div id="collapseSeventeen" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-outer2">
                                            <div class="col-sm-12 text-right">
                                                <button type="button" id="add_libraray_file" class="green-btn">Click Here to Upload</button>
                                                <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                <button class="orange-btn" id="remove_library_file">Remove All Files</button>
                                                <button type="button" id="saveLibraryFiles" class="blue-btn">Save</button>
                                                <button type="button" class="grey-btn cancel-all">Cancel</button>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="row" id="file_library_uploads">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="grid-outer listinggridDiv">
                                            <div class="apx-table apxtable-bdr-none">
                                                <div class="table-responsive">
                                                    <table id="propertFileLibrary-table" class="table table-bordered">
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- Notes -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseEighteen" data_href="#collapseEighteen" class="tabcheckvalidation">
                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Notes </a>
                                </h4>
                            </div>
                            <div id="collapseEighteen" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <form action="" method="post" id="addPropertynotesForm">
                                        <input type="hidden" name="id" id="notes_id" value="">
                                        <div class="row">
                                            <div class="form-outer2">

                                                <div class="div-full" id="addnoteshref">
                                                    <a href="javascript:;"  class="active notes-button-bold">Add Notes</a>
                                                </div>

                                                <div id="addnotesopenDiv" style="display:none" >
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <div class="notes_date_right_div">
                                                            <textarea id="notesId" maxlength="500" class="form-control capital notes_date_right" name="notes"></textarea></div>
                                                        <input type="hidden" id="AddPropertynotesclick" value="">
                                                        <div class="form-outer">
                                                            <div class="btn-outer text-right">
                                                                <button class="blue-btn" id="AddPropertynotesButton">Save </button>
                                                                <button rel="addPropertynotesForm" type="button" class="clear-btn clearEditFormResetNotes">Reset</button>
                                                                <button type="button" class="grey-btn" id="cancel-notes">Cancel </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="grid-outer listinggridDiv">
                                                <div class="apx-table apxtable-bdr-none">
                                                    <div class="table-responsive">
                                                        <table id="notes-table" class="table table-bordered">
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Flags -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseNinteen" data_href="#collapseNinteen" class="tabcheckvalidation">
                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Flags </a>
                                </h4>
                            </div>
                            <div id="collapseNinteen" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-sm-12">
                                        <div class="btn-outer text-right">
                                            <button id="new_flag" class="blue-btn">New Flag</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <form id="flagForm">
                                            <div class="col-sm-12" id="flagFormDiv" style="display: none;">
                                                <div class="grey-box-add">
                                                    <div class="form-outer">
                                                        <input type="hidden" name="id" id="flag_id">
                                                        <div class="col-xs-12 col-sm-3">
                                                            <label>Flagged By <em class="red-star">*</em></label>
                                                            <input class="form-control capital" name="flag_by" id="flag_flag_by" placeholder="Flagged By" maxlength="100" type="text" value="<?php echo $_SESSION[SESSION_DOMAIN]['default_name'] ?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3">
                                                            <label>Date</label>
                                                            <input class="form-control calander" name="date" id="flag_flag_date" readonly value="<?php echo $current_date; ?>" type="text"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3">
                                                            <label>Flag Name <em class="red-star">*</em></label>
                                                            <input class="form-control capital" name="flag_name" id="flag_flag_name" maxlength="100" placeholder="Please Enter the Name of this Flag" type="text" value="<?= $data['property_name'];?>"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3">
                                                            <label>Country Code</label>
                                                            <select class='form-control' name='country_code' id="flag_country_code12">

                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3">
                                                            <label>Phone Number</label>
                                                            <?php
                                                            $phn=$data['phone_number'];
                                                            if(!empty( $phn)){
                                                                $phn1= $phn[0];
                                                            }else{
                                                                $phn1="";
                                                            }
                                                            ?>
                                                            <input class="form-control phone_number_format" name="flag_phone_number" id="flag_phone_number" maxlength="12" value="<?php echo $phn1; ?>" placeholder="123-456-7890" type="text"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3">
                                                            <label>Flag Reason</label>
                                                            <input CLASS="form-control capital" name="flag_reason" id="flag_flag_reason" maxlength="100" placeholder="Flag Reason" type="text"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3">
                                                            <label>Completed</label>
                                                            <select class='form-control' name='completed' id="completed">
                                                                <option value="0">No</option>
                                                                <option value="1">Yes</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <label>Note</label>
                                                            <div class="notes_date_right_div">
                                                                <textarea class="form-control capital notes_date_right" name="flag_note" id="flag_note" placeholder="Flag Notes"></textarea></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="btn-outer text-right">
                                                            <button class="blue-btn" type="submit" id="flagSaveBtnId">Save</button>
                                                            <button rel="flagForm" data-id="flagSaveBtnId" type="button" class="clear-btn clearEditFormResetFlag">Reset</button>
                                                            <button class="grey-btn" type="button" id="flagCancel">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- Flag Note table-->
                                    <div class="row">
                                        <div class="grid-outer listinggridDiv">

                                            <div class="apx-table apxtable-bdr-none">
                                                <div class="table-responsive">
                                                    <table id="propertyFlag-table" class="table table-bordered">
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Wating List -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwenty" data_href="#collapseTwenty" class="tabcheckvalidation">
                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Waiting List </a>
                                </h4>
                            </div>
                            <div id="collapseTwenty" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-sm-12">
                                        <div class="btn-outer text-right">
                                            <button id="wating_list" data-toggle="modal" data-backdrop="static" data-target="#watingListModel" class="blue-btn" >Add Wating List</button>
                                        </div>
                                    </div>
                                    <!-- Wating List table-->
                                    <div class="row">
                                        <div class="grid-outer listinggridDiv">
                                            <div class="apx-table apxtable-bdr-none">
                                                <div class="table-responsive">
                                                    <table id="propertyWatingList-table" class="table table-bordered">
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- property complaint List -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyTwo" data_href="#collapseTwentyTwo" class="tabcheckvalidation">
                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Property Complaint</a>
                                </h4>
                            </div>
                            <div id="collapseTwentyTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-sm-12 form-group">
                                        <div class="btn-outer text-right">
                                            <button type="button" id="new_complaint_button" class="blue-btn">New Complaint</button>
                                        </div>
                                    </div>
                                    <div style="display:none" id="new_complaint">
                                        <form id="new_complaint_form">
                                        <div class="row">
                                            <input type="hidden" name="edit_complaint_id" id="edit_complaint_id" />
                                            <div class="col-sm-2">
                                                <label>Complaint ID</label>
                                                <input class="form-control" name="complaint_id" id="complaint_id"  placeholder="Eg: AB01234C " type="text" readonly>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Complaint Date </label>
                                                <input class="form-control calander" id="complaint_date" name="complaint_date" placeholder="<?php echo $current_date ?>" readonly>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Complaint Type<a id="complainttype" class="pop-add-icon" href="javascript:;" onclick="clearPopUps('.ComplaintTypePopupClass')"><i class="fa fa-plus-circle" style="margin-left: 5px;" aria-hidden="true"></i></a></label>
                                                <select class='form-control' id='complaint_type_options' name='complaint_type_options'>
                                                    <option disabled selected value> -- select an option -- </option>
                                                </select>
                                                <div class="add-popup ComplaintTypePopupClass" id='ComplaintTypePopup'>
                                                    <h4>Add New Complaint Type</h4>
                                                    <div class="add-popup-body">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>Complaint Type<em class="red-star">*</em></label>
                                                                <input class="form-control capital capitalize_popup_input" type="text"  name='@complaint_type'/>
                                                            </div>
                                                            <div class="btn-outer text-right">
                                                                <input type="button"  class="blue-btn" id="NewpetComplaintSave"/>
                                                                <button rel="ComplaintTypePopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Complaint Notes</label>
                                                <div class="notes_date_right_div">
                                                    <textarea class="form-control capital notes_date_right"  id="complaint_note" name="complaint_note"></textarea></div>
                                            </div>
                                            <div class="col-sm-4" id="other_notes_div" style="display:none">
                                                <label>Other Notes</label>
                                                <textarea class="form-control capital" rows="1" cols="5"  id="other_notes" name="other_notes"></textarea>
                                            </div>
                                            <div class="col-sm-12 text-right">
                                                <button type="button" class=" blue-btn" id="complaint_save">Save</button>
                                                <button data-id="complaint_save" rel="new_complaint_form" type="button" class="clear-btn clearEditFormResetFlag">Reset</button>
                                                <button type="button" class=" grey-btn" id="complaint_cancel">Cancel</button>
                                            </div>
                                        </div>

                                        </form>


                                    </div>
                                    <!-- Wating List table-->


                                    <!-- Complaint List table-->
                                    <div class="row">
                                        <div class="grid-outer listinggridDiv">

                                            <div class="apx-table apxtable-bdr-none">
                                                <div class="table-responsive">
                                                    <table id="building-complaints" class="table table-bordered"></table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 form-group">
                                        <div class="btn-outer text-right">
                                            <button type="button" class=" blue-btn" id="print_email_button">Print/Email</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Custom Fields -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyOne" data_href="#collapseTwentyOne" class="tabcheckvalidation">
                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Custom Fields</a>
                                </h4>
                            </div>
                            <div id="collapseTwentyOne" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-outer">
                                            <div class="col-sm-12 min-height-0">
                                                <div class="custom_field_html">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3 text-right pull-right">
                                                <div class="btn-outer">
                                                    <button type="button"  id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">New Custom Field</button>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 min-height-0">
                                                <div class="btn-outer">
                                                    <button type="button" id="custom_field_submit" class="blue-btn">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- start custom field model -->
<div class="container">
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body" style="height: 380px;">
                    <div class="form-outer col-sm-12">
                        <form id="custom_field">
                            <input type="hidden" id="customFieldModule" name="module" value="property">
                            <input type="hidden" name="id" id="custom_field_id" value="">
                            <div class="row custom_field_form">
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Field Name <em class="red-star">*</em></label>
                                    </div>
                                    <div class="col-sm-9 field_name">
                                        <input class="form-control capital" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                        <span class="required error"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Data Type</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control data_type" id="data_type" name="data_type">
                                            <option value="text">Text</option>
                                            <option value="number">Number</option>
                                            <option value="currency">Currency</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="url">URL</option>
                                            <option value="date">Date</option>
                                            <option value="memo">Memo</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Default value</label>
                                    </div>
                                    <div class="col-sm-9 default_value">
                                        <input class="form-control default_value capital" id="default_value" type="text" name="default_value" placeholder="">
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Required Field</label>
                                    </div>
                                    <div class="col-sm-9 is_required">
                                        <select class="form-control" name="is_required" id="is_required">
                                            <option value="1">Yes</option>
                                            <option value="0" selected="selected">No</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-outer text-right">

                                <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                <button rel="custom_field" type="button" class="clear-btn clearFormReset">Clear</button>
                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End custom field model -->

<!-- start custom field model -->
<div class="container">
    <div class="modal fade" id="watingListModel" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Wating List</h4>
                </div>
                <div class="modal-body" style="height: 500px;overflow-x: auto;">
                    <div class="form-outer col-sm-12">
                        <div class="form-outer">
                            <form id="watingListForm">
                                <input type="hidden" name="id" id="watingList_id" value="">
                                <div class="col-xs-12 col-sm-12">
                                    <label><input type="checkbox" name="use_entity_name" id="wating_use_entity_name"> Use Entity/Company Name</label>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Preferred</label>
                                    <select name="preferred" id="wating_preferred" class="form-control" style="cursor: pointer;">
                                        <option value="0">Select</option>
                                        <option value="1">1stBR</option>
                                        <option value="2">2ndBR</option>
                                        <option value="3">3rdBR</option>
                                        <option value="4">4thBR</option>
                                        <option value="5">5thBR</option>
                                        <option value="6">6thBR</option>
                                        <option value="7">Commercial</option>
                                        <option value="8">House</option>
                                        <option value="9">Single/Family</option>
                                        <option value="10">Multi Family </option>
                                        <option value="11">Studio</option>
                                        <option value="12">Office</option>
                                        <option value="13">Other</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Pets</label>
                                    <select name="pets" id="wating_pets" class="form-control" style="cursor: pointer;">
                                        <option value="0" selected="selected">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="2">No</option>
                                        <option value="3">Other</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Property</label>
                                    <input class="form-control capital" name="property_name" readonly id="wating_property_name" placeholder="Property Name" maxlength="100" type="text" value="<?php echo $data['property_name']; ?>"/>
                                    <input class="form-control" name="property_id" id="wating_property_id" type="hidden" value="<?php echo $data['id']; ?>"/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Building</label>
                                    <select name="building_id" id="wating_building_id" class="form-control" style="cursor: pointer;">
                                        <option value="0">Any Building</option>
                                    </select>

                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Unit</label>
                                    <select name="unit" id="wating_unit" class="form-control" style="cursor: pointer;">
                                        <option value="0">Any Unit</option>
                                        <option value="1">1</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-4" id="entityCompanyName" style="display: none;">
                                    <label>Entity/Company Name <em class="red-star">*</em></label>
                                    <input class="form-control capital" name="entity_name" id="wating_entity_name" placeholder="Entity/Company Name" maxlength="100" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>First Name <em class="red-star">*</em></label>
                                    <input class="form-control capital" name="first_name" id="wating_first_name" placeholder="First Name" maxlength="50" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Middle Name</label>
                                    <input class="form-control capital" name="middle_name" id="wating_middle_name" placeholder="Middle Name" maxlength="50" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Last Name <em class="red-star">*</em></label>
                                    <input class="form-control capital" name="last_name" id="wating_last_name" placeholder="Last Name" maxlength="50" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Maiden Name</label>
                                    <input class="form-control capital" name="maiden_name" id="wating_maiden_name" placeholder="Maiden Name" maxlength="20" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Nickname</label>
                                    <input class="form-control capital" name="nickname" id="wating_nickname" placeholder="Nickname" maxlength="20" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Phone</label>
                                    <input class="form-control phone_number" name="phone" id="wating_phone" placeholder="Phone" maxlength="12" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Email</label>
                                    <input class="form-control capitalremove" name="email" id="wating_email" placeholder="Email" maxlength="50" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Zip/Postal Code</label>
                                    <input class="form-control" name="zipcode" id="wating_zipcode" placeholder="110001" maxlength="9" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Country</label>
                                    <input class="form-control capital zipcountry" name="country" id="wating_country" placeholder="Country" maxlength="50" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>State/Province</label>
                                    <input class="form-control capital zipstate" name="state" id="wating_state" placeholder="State" maxlength="100" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>City</label>
                                    <input class="form-control capital zipcity" name="city" id="wating_city" placeholder="City" maxlength="50" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Address 1</label>
                                    <input class="form-control capital" name="address1" id="wating_address1" placeholder="Address1" maxlength="200" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Address 2</label>
                                    <input class="form-control capital" name="address2" id="wating_address2" placeholder="Address2" maxlength="200" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Address 3</label>
                                    <input class="form-control capital" name="address3" id="wating_address3" placeholder="Address3" maxlength="200" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Address 4</label>
                                    <input class="form-control capital" name="address4" id="wating_address4" placeholder="Address4" maxlength="200" type="text" value=""/>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <label>Note</label>
                                    <div class="notes_date_right_div">
                                        <textarea class="form-control capital notes_date_right" name="note" id="wating_note" maxlength="500" placeholder="Notes"></textarea></div>
                                </div>

                                <div class="btn-outer text-right">
                                    <button type="submit" class="blue-btn" id='saveWatingList'>Save</button>
                                    <button rel="watingListForm" type="button" class="clear-btn clearFormReset">Reset</button>
                                    <button type="button" class="grey-btn" id="cancelWatingList">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Wrapper Ends -->
<!-- Create charge model -->
<div class="container">
    <div class="modal fade" id="chargeCodeModel" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add Chat of Account</h4>
                </div>
                <div class="modal-body" style="height: 250px;">
                    <form name="add_charge_code" id="add_charge_code_form" class="manage_charge_code_class">
                        <div class="form-outer">
                            <div class="col-sm-12">
                                <div class="col-sm-4">
                                    <label>Charge Code <em class="red-star">*</em></label>
                                    <input name="charge_code" id="charge_charge_code" maxlength="10" placeholder="Eg: 1B" class="form-control capital only_letter" type="text">
                                    <span id="charge_codeErr"></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <label>Description</label>
                                    <textarea name="description" style="resize: none;" id="charge_description" rows="1" maxlength="250" placeholder="Eg: 1 Bedroom" class="capital form-control"></textarea>
                                    <span id="charge_descriptionErr"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-4">
                                    <label>Credit Account <em class="red-star">*</em></label>
                                    <select id="charge_credit_account" name="credit_account" class="fm-txt form-control"><option value="">Select</option><option value="1">1000-Cash in Bank-Operating</option><option value="2">1001-Fdsfds</option><option value="3">1010-Cash in Bank-Saving</option><option value="4">1020-Cash in Bank-Money Market</option><option value="5">1030-Security Deposit Bank Account</option><option value="6">1035-Payroll Bank Account</option><option value="7">1040-Petty Cash</option><option value="8">1100-Tenant Receivables</option><option value="9">1125-Tenant Receivable-Utilities</option><option value="10">1128-Tenant Receivable-Taxes</option><option value="11">1150-CAM Receivable</option><option value="12">1175-Accounts Receivable</option><option value="13">1200-Prepaid 4s</option><option value="14">1290-Other Current Assets</option><option value="15">1300-Furniture &amp; Fixtures</option><option value="16">1320-Equipment</option><option value="17">1325-Vehicles</option><option value="18">1350-Leasehold Improvements</option><option value="19">1400-Building</option><option value="20">1450-Land</option><option value="21">1520-Accum. Depr. Equipment</option><option value="22">1525-Accum. Depr. Vehicles</option><option value="23">1550-Accum. Depr. Building</option><option value="24">1575-Accum. Amort. Leasehold</option><option value="25">1600-Security Deposit</option><option value="26">1700-Intercompany Accounts Receivable</option><option value="27">2000-Accounts Payable</option><option value="28">2010-Security Deposit 2</option><option value="29">2020-Prepaid Rent 2</option><option value="30">2030-Notes Payable</option><option value="31">2100-Fed. 5 Tax Withheld</option><option value="32">2110-FICA Tax Withheld</option><option value="33">2115-Medicare Tax Withheld</option><option value="34">2120-State 5 Tax Withheld</option><option value="35">2125-SUTA Tax Withheld</option><option value="36">2130-Sales Tax Payable</option><option value="37">2140-5 Taxes Payable</option><option value="38">2295-Intercompany Accounts Payable</option><option value="39">2300-Contracts Payable</option><option value="40">2400-Mortgage Payable</option><option value="41">2500-Long Term Notes Payable</option><option value="42">2990-Other Long Term Liabilities</option><option value="43">3000-Retained Earnings</option><option value="44">3010-Owner Draw</option><option value="45">3020-Owner Contribution of Capital</option><option value="46">3999-Current Year Earnings/Loss</option><option value="47">4020-Parking Income</option><option value="48">4030-Application Fee Income</option><option value="49">4040-Cleaning Income</option><option value="50">4060-Laundry Income</option><option value="51">4080-Late Charge Income</option><option value="52">4110-Prepaid Rent Income</option><option value="53">4120-NSF Fee Income</option><option value="54">4125-7787892</option><option value="55">4140-Maint &amp; Repairs Income</option><option value="56">4160-Interest Income</option><option value="57">4170-Electricity Utility Income</option><option value="58">4171-HOA Dues</option><option value="59">4180-Water Utility Income</option><option value="60">4185-Sewer Utility Income</option><option value="61">4187-A/C Utility Income</option><option value="62">4188-Waste Disposal Income</option><option value="63">4190-Gas Utility Income</option><option value="64">4200-CAM Income</option><option value="65">4250-GST Tax Income</option><option value="66">4251-HST Tax Income</option><option value="67">4253-PST Tax Income</option><option value="68">4300-Intercompany Income</option><option value="69">4450-Less: Concessions</option><option value="70">4490-Other Income</option><option value="71">5000-Administrative Salaries</option><option value="72">5010-Management Fees</option><option value="73">5020-Manager Salaries</option><option value="74">5030-Clerical Salaries</option><option value="75">5040-Maintenance Salaries</option><option value="76">5050-Payroll Taxes</option><option value="77">5060-Employee Benefits</option><option value="78">5100-Advertising</option><option value="79">5110-Office Supplies</option><option value="80">5120-Dues &amp; Subscriptions</option><option value="81">5130-Postage &amp; Delivery</option><option value="82">5140-Telephone</option><option value="83">5190-Other Administrative 4s</option><option value="84">5200-Maintenance</option><option value="85">5210-Painting &amp; Decorating</option><option value="86">5220-Landscaping</option><option value="87">5230-Cleaning</option><option value="88">5250-Plumbing</option><option value="89">5260-Security</option><option value="90">5270-Building Supplies</option><option value="91">5290-Other Operating 4s</option><option value="92">5300-Electricity</option><option value="93">5310-Water &amp; Sanitation</option><option value="94">5320-Trash Collection</option><option value="95">5330-Natural Gas</option><option value="96">5337-Heating Oil Utility Income</option><option value="97">5390-Other Utilities/Cable</option><option value="98">5400-Travel &amp; Entertainment</option><option value="99">5410-Bank Charges</option><option value="100">5420-Interest</option><option value="101">5430-Other Interest</option><option value="102">5440-Legal &amp; Accounting</option><option value="103">5490-Data Processing</option><option value="104">5500-Real Estate Taxes</option><option value="105">5510-Fees &amp; Permits</option><option value="106">5520-Insurance</option><option value="107">5530-Depreciation 4</option><option value="108">6200-Repairs &amp; Maintenance</option><option value="109">6250-Parking Lot Sweeping</option><option value="110">6300-Supplies</option><option value="111">7000-Bad Debt Expense</option><option value="112">7990-Other Operating 4s</option><option value="113">8000-Non-Operating Income</option><option value="114">9000-Non-Operating 4s-Taxes</option></select>
                                    <span id="charge_credit_accountErr"></span>
                                </div>
                                <div class="col-sm-4">
                                    <label>Debit Account <em class="red-star">*</em></label>
                                    <select id="charge_debit_account" name="debit_account" class="fm-txt form-control"><option value="">Select</option><option value="1">1000-Cash in Bank-Operating</option><option value="2">1001-Fdsfds</option><option value="3">1010-Cash in Bank-Saving</option><option value="4">1020-Cash in Bank-Money Market</option><option value="5">1030-Security Deposit Bank Account</option><option value="6">1035-Payroll Bank Account</option><option value="7">1040-Petty Cash</option><option value="8">1100-Tenant Receivables</option><option value="9">1125-Tenant Receivable-Utilities</option><option value="10">1128-Tenant Receivable-Taxes</option><option value="11">1150-CAM Receivable</option><option value="12">1175-Accounts Receivable</option><option value="13">1200-Prepaid 4s</option><option value="14">1290-Other Current Assets</option><option value="15">1300-Furniture &amp; Fixtures</option><option value="16">1320-Equipment</option><option value="17">1325-Vehicles</option><option value="18">1350-Leasehold Improvements</option><option value="19">1400-Building</option><option value="20">1450-Land</option><option value="21">1520-Accum. Depr. Equipment</option><option value="22">1525-Accum. Depr. Vehicles</option><option value="23">1550-Accum. Depr. Building&lt;</option><option value="24">1575-Accum. Amort. Leasehold</option><option value="25">1600-Security Deposit</option><option value="26">1700-Intercompany Accounts Receivable</option><option value="27">2000-Accounts Payable</option><option value="28">2010-Security Deposit 2</option><option value="29">2020-Prepaid Rent 2</option><option value="30">2030-Notes Payable</option><option value="31">2030-Notes Payable</option><option value="32">2100-Fed. 5 Tax Withheld</option><option value="33">2110-FICA Tax Withheld</option><option value="34">2115-Medicare Tax Withheld</option><option value="35">2120-State 5 Tax Withheld</option><option value="36">2125-SUTA Tax Withheld</option><option value="37">2130-Sales Tax Payable</option><option value="38">2140-5 Taxes Payable</option><option value="39">2295-Intercompany Accounts Payable</option><option value="40">2300-Contracts Payable</option><option value="41">2400-Mortgage Payable</option><option value="42">2500-Long Term Notes Payable</option><option value="43">2990-Other Long Term Liabilities</option><option value="44">3000-Retained Earnings</option><option value="45">3010-Owner Draw</option><option value="46">3020-Owner Contribution of Capital</option><option value="47">3999-Current Year Earnings/Loss</option><option value="48">4020-Parking Income</option><option value="49">4030-Application Fee Income</option><option value="50">4040-Cleaning Income</option><option value="51">4060-Laundry Income</option><option value="52">4080-Late Charge Income</option><option value="53">4110-Prepaid Rent Income</option><option value="54">4120-NSF Fee Income</option><option value="55">4125-7787892</option><option value="56">4140-Maint &amp; Repairs Income</option><option value="57">4160-Interest Income</option><option value="58">4170-Electricity Utility Income</option><option value="59">4171-HOA Dues</option><option value="60">4180-Water Utility Income</option><option value="61">4185-Sewer Utility Income</option><option value="62">4187-A/C Utility Income</option><option value="63">4188-Waste Disposal Income</option><option value="64">4190-Gas Utility Income</option><option value="65">4200-CAM Income</option><option value="66">4250-GST Tax Income</option><option value="67">4251-HST Tax Income</option><option value="68">4253-PST Tax Income</option><option value="69">4300-Intercompany Income</option><option value="70">4450-Less: Concessions</option><option value="71">4490-Other Income</option><option value="72">5000-Administrative Salaries</option><option value="73">5010-Management Fees</option><option value="74">5020-Manager Salaries</option><option value="75">5030-Clerical Salaries</option><option value="76">5040-Maintenance Salaries</option><option value="77">5050-Payroll Taxes</option><option value="78">5060-Employee Benefits</option><option value="79">5100-Advertising</option><option value="80">5110-Office Supplies</option><option value="81">5120-Dues &amp; Subscriptions</option><option value="82">5130-Postage &amp; Delivery</option><option value="83">5140-Telephone</option><option value="84">5190-Other Administrative 4s</option><option value="85">5200-Maintenance</option><option value="86">5210-Painting &amp; Decorating</option><option value="87">5220-Landscaping</option><option value="88">5230-Cleaning</option><option value="89">5250-Plumbing</option><option value="90">5260-Security</option><option value="91">5270-Building Supplies</option><option value="92">5290-Other Operating 4s</option><option value="93">5300-Electricity</option><option value="94">5310-Water &amp; Sanitation</option><option value="95">5320-Trash Collection</option><option value="96">5330-Natural Gas</option><option value="97">5337-Heating Oil Utility Income</option><option value="98">5390-Other Utilities/Cable</option><option value="99">5400-Travel &amp; Entertainment</option><option value="100">5410-Bank Charges</option><option value="101">5420-Interest</option><option value="102">5430-Other Interest</option><option value="103">5440-Legal &amp; Accounting</option><option value="104">5490-Data Processing</option><option value="105">5500-Real Estate Taxes</option><option value="106">5510-Fees &amp; Permits</option><option value="107">5520-Insurance</option><option value="108">5530-Depreciation 4</option><option value="109">6200-Repairs &amp; Maintenance</option><option value="110">6250-Parking Lot Sweeping</option><option value="111">6300-Supplies</option><option value="112">7000-Bad Debt Expense</option><option value="113">7990-Other Operating 4s</option><option value="114">8000-Non-Operating Income</option><option value="115">9000-Non-Operating 4s-Taxes</option></select>
                                    <span id="charge_debit_accountErr"></span>
                                </div>
                                <div class="col-sm-4">
                                    <label>Status <em class="red-star">*</em></label>
                                    <select name="status" id="charge_status" class="fm-txt form-control">
                                        <option value="">Select</option>
                                        <option value="1">Active</option>
                                        <option value="0">InActive</option>
                                    </select>
                                </div>
                                <div class="btn-outer text-right">
                                    <button type="submit" class="blue-btn" id='saveChargeCode'>Save</button>
                                    <button rel="add_charge_code_form" type="button" class="clear-btn clearFormReset">Clear</button>
                                    <button type="button" class="grey-btn" id="cancelChargeCode">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Create charge model end -->
<div class="container">
    <div class="modal fade" id="add_more_bank_account_modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Bank Account</h4>
                </div>
                <div class="modal-body pad-none">
                    <div class="panel-body" style="border-color: transparent;">
                        <div class="row">
                            <form class="form-outer add_bank_account_formClass" id="add_bank_account_form">
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <label>Portfolio <em class="red-star">*</em></label>
                                        <input  name="portfolio" id="portfolio_bank_name" class="form-control" type="text" disabled/>
                                        <input type="hidden" value="" id="portfolio_bank_name_hddn">

                                        <span id="portfolioErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Bank Name  <em class="red-star">*</em></label>
                                        <input name="bank_name" id="bank_name_rec" maxlength="100" placeholder="Eg: Wells Fargo"   class="form-control hide_copy" type="text"/>
                                        <span id="bank_nameErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Bank A/c No.  <em class="red-star">*</em></label>
                                        <input name="bank_account_number" id="bank_account_number" maxlength="20" placeholder="Eg: 123A12345678"   class="form-control hide_copy" type="text"/>
                                        <span id="bank_account_numberErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>FDI No.  <em class="red-star">*</em></label>
                                        <input name="fdi_number" id="fdi_number" maxlength="10" placeholder="Eg: 123A12345678"   class="form-control" type="text"/>
                                        <span id="fdi_numberErr" class="error"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <label>Branch Code <em class="red-star">*</em></label>
                                        <input name="branch_code" id="branch_code" maxlength="10" placeholder="Eg: AB12 12345 "   class="form-control" type="text"/>
                                        <span id="branch_codeErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Initial Amount(<?php echo $_SESSION[SESSION_DOMAIN]["default_currency_symbol"]?>)  <em class="red-star">*</em></label>
                                        <input name="initial_amount" id="initial_amount" placeholder="Eg: 1000"   class="form-control" type="text"/>
                                        <span id="initial_amountErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Last Used Check Number  <em class="red-star">*</em></label>
                                        <input name="last_used_check_number" id="last_used_check_number" maxlength="100" placeholder="Eg: 1234"   class="form-control" type="text"/>
                                        <span id="last_used_check_numberErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Status <em class="red-star">*</em></label>
                                        <select id="status" name="status" class="fm-txt form-control">
                                            <option value="">Select</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                        <span id="statusErr" class="error"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <label>Bank Routing  <em class="red-star">*</em></label>
                                        <input name="routing_number" id="routing_number_rec" maxlength="10" placeholder="Eg: 110000000"   class="form-control" type="text"/>
                                        <span id="routing_numberErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label></label>
                                        <div class="check-outer mg-lt-30 mb-15">
                                            <input name="is_default" class="is_default" type="checkbox"/>
                                            <label>Set as Default</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="btn-outer text-right">
                                    <button type="submit" class="blue-btn" value="Save" >Save</button>
                                    <button rel="add_bank_account_formClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                    <button type="button" id="add_bank_account_cancel_btn" class="grey-btn">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="sendMailModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header">
                <h4 class="modal-title">Email</h4>
            </div>
            <form id="sendEmail">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-1">
                            <button class="blue-btn compose-email-btn">Send</button>
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>To <em class="red-star">*</em></label></div>
                                <div class="col-sm-10"><span><input class="form-control to" name="to" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <a class="add-recipient-link addToRecepent" href="#"> Add Recipients</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Cc</div>
                                <div class="col-sm-10"><span><input class="form-control cc" name="cc" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <a class="add-recipient-link addCcRecepent" href="#"> Add Recipients</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Bcc </label></div>
                                <div class="col-sm-10"><span><input class="form-control bcc" name="bcc" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <a class="add-recipient-link addBccRecepent" href="#"> Add Recipients</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Subject <em class="red-star">*</em></label></div>
                                <div class="col-sm-10"><span><input class="form-control subject" name="subject" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Body <em class="red-star">*</em></label></div>
                                <div class="col-sm-10">
                                    <span><textarea class="form-control summernote" name="body"></textarea></span>
                                    <div class="btn-outer">
                                        <button class="blue-btn">Send</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>


                    <div class="attachmentFile"></div>


                </div>
            </form>
        </div>
    </div>

</div>

<div class="container">
    <div class="modal fade" id="torecepents" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 600px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;height:300px;">
                        <div class="col-sm-6">
                            <label>Select <em class="red-star">*</em></label>
                            <select class="form-control selectUsers">
                                <option value="2">Tenant</option>
                                <option value="4">Owner</option>
                                <option value="3">Vendor</option>
                                <option value="5">Other Contacts</option>
                                <option value="6">Guest Card</option>
                                <option value="7">Rental Application</option>
                                <option value="8">Employee</option>
                            </select>
                        </div>
                        <div class="col-sm-6">

                        </div>

                        <div class="col-sm-12">
                            <div class="userDetails"></div>
                        </div>

                        <div class="col-sm-12">
                            <button class="blue-btn" id="SendselectToUsers">Done</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="modal fade" id="bccrecepents" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 600px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;height:300px;">
                        <div class="col-sm-6">
                            <label>Select <em class="red-star">*</em></label>
                            <select class="form-control selectBccUsers">
                                <option value=""></option>
                                <option value="2">Tenant</option>
                                <option value="4">Owner</option>
                                <option value="3">Vendor</option>
                                <option value="5">Other Contacts</option>
                                <option value="6">Guest Card</option>
                                <option value="7">Rental Application</option>
                                <option value="8">Employee</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <div class="userBccDetails"></div>
                        </div>

                        <div class="col-sm-12">
                            <button class="blue-btn" id="SendselectBccUsers">Done</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="modal fade" id="ccrecepents" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 600px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;height:300px;">
                        <div class="col-sm-6">
                            <label>Select <em class="red-star">*</em></label>
                            <select class="form-control selectCcUsers">
                                <option value=""></option>
                                <option value="2">Tenant</option>
                                <option value="4">Owner</option>
                                <option value="3">Vendor</option>
                                <option value="5">Other Contacts</option>
                                <option value="6">Guest Card</option>
                                <option value="7">Rental Application</option>
                                <option value="8">Employee</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <div class="userCcDetails"></div>
                        </div>

                        <div class="col-sm-12">
                            <button class="blue-btn" id="SendselectCcUsers">Done</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/autonumeric.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/autonumericDecimal.js"></script>
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var property_unique_id = "";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var default_name = "<?php echo $_SESSION[SESSION_DOMAIN]['default_name']; ?>";
    var manager_id = JSON.parse('<?php echo json_encode($data['manager_id']); ?>');
    var attachGroup_id = JSON.parse('<?php echo json_encode($data['attach_groups']); ?>');
    var defaultFormData = '';
    var portfolio_default_id = '';


    $(document).on('focusout','.amount',function(){

        var id = $(this).attr('id');
        if($('#' + id).val() != '' && $('#' + id).val().indexOf(".") == -1) {
            var bef = $('#' + id).val().replace(/,/g, '');
            var value = numberWithCommas(bef) + '.00';
            $('#' + id).val(value);
        } else {
            var bef = $('#' + id).val().replace(/,/g, '');
            $('#' + id).val(numberWithCommas(bef));
        }
    });

</script>

<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/additionalMethods.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/PropertyValidate.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/owner.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/property.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/FeeManageValidate.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/KeyValidate.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/ManagementValidate.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/ManagementInfoValidate.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/TabValidates.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/property/emailAttach.js"></script>

<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/bootstrap-tagsinput.css" />
<!--<script src="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.4.2/bootstrap-tagsinput.min.js"></script>-->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
<!--inventory js -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/property/inventory.js"></script>
<!--file library js -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/property/photoVideos.js"></script>
<!--file library js -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/property/file_library.js"></script>
<!--custom field js -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/property/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/commonPopup.js"></script>
<!-- flag js -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/flag/flagValidation.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/flag/flag.js"></script>
<!-- Wating List -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/property/watingListValidation.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/property/watingList.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.easyui.min.js"></script>
<link href="<?php echo COMPANY_SITE_URL; ?>/css/easyui.css" rel="stylesheet">
<!-- Manage Charge Code -->
<script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/property/manageCharges.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/property/manageCharges.js"></script>

<script>
    $(document).on("click","#redirection_work_order",function () {
        var id=$(this).attr('rel');
        localStorage.setItem('redirection_module','property_module');
        window.location.href = "/WorkOrder/AddWorkOrder?id="+id;
    })
    $(document).ready(function () {
        var countrycodevalue = "<?php echo $data['country_code']; ?>";
        if(countrycodevalue != "") {
            $('#country_code option[value=' + countrycodevalue + ']').attr('selected', 'selected');
        }

        var amenities = <?php echo json_encode($data['amenities']); ?>;
        if(amenities != "") {
            setTimeout(function () {
                $.each(amenities, function (key, value) {
                    $(":checkbox[value='" + value + "']").prop("checked", "true");
                });
            }, 3000);
        }

        var bankdetail='<?php if(!empty($getBankdetails)) {echo json_encode($getBankdetails);} ; ?>';
        if(bankdetail != ""){
            bankdetail= JSON.parse(bankdetail);
            setTimeout(function () {
                $.each(bankdetail, function (key, value) {
                    $('.editAccountName_'+key).val(value.account_name);
                    $('.editChartName_'+key).val(value.chart_of_account);
                    $('.default_security_deposit_'+key).val(value.default_security_deposit);
                });
            }, 2000);
        }



        var petfriendly = '<?php echo $data['pet_friendly']; ?>' == '' ? '' : '<?php echo $data['pet_friendly']; ?>';
        setTimeout(function () {
            $("#pet_options").val(petfriendly);
        }, 1000);
        var property_type = '<?php echo $data['property_type']; ?>' == '' ? '' : '<?php echo $data['property_type']; ?>';
        setTimeout(function () {
            $("#property_type_options").val(property_type);
        }, 1000);
        var property_style = '<?php echo $data['property_style']; ?>' == '' ? '' : '<?php echo $data['property_style']; ?>';
        setTimeout(function () {
            $("#property_style_options").val(property_style);
        }, 1000);
        var property_subtype = '<?php echo $data['property_subtype']; ?>' == '' ? '' : '<?php echo $data['property_subtype']; ?>';
        setTimeout(function () {
            $("#property_subtype_options").val(property_subtype);
        }, 1000);
        var unit_type = '<?php echo $data['unit_type']; ?>' == '' ? '' : '<?php echo $data['unit_type']; ?>';
        setTimeout(function () {
            $("#unit_type").val(unit_type);
        }, 1000);
        var portfolio_id = '<?php echo $data['portfolio_id']; ?>' == '' ? '' : '<?php echo $data['portfolio_id']; ?>';
        setTimeout(function () {
            $("#portfolio_name_options").val(portfolio_id);
        }, 1000);

        var key_access_codes_info = JSON.parse('<?php echo json_encode($data['key_access_codes_info']); ?>');
        setTimeout(function () {
            $.each(key_access_codes_info, function (key, value) {
                $('.key_access_codes_info_' + key + ' option[value=' + value + ']').attr('selected', 'selected');
            });
        }, 1000);
        var ownerSelectlist = JSON.parse('<?php echo json_encode($getLinkOwnerData['evenidss']); ?>');
        setTimeout(function () {
            $.each(ownerSelectlist, function (key, value) {
                $('.ownerSelectlist_' + key + ' option[value=' + value + ']').attr('selected', 'selected');
            });
        }, 1000);

        var garage_options = '<?php echo $data['garage_available']; ?>' == '' ? '' : '<?php echo $data['garage_available']; ?>';
        if(garage_options != '') {
            setTimeout(function () {
                $('#garage_options option[value=' + garage_options + ']').attr('selected', 'selected');
            }, 1000);
        }

        if (localStorage.getItem("AccordionHref")) {
            var message = localStorage.getItem("AccordionHref");
            // $('.tabcheckvalidation').collapse('hide');
            // alert(message);'
            $('#collapseOne').removeClass('in').attr('aria-expanded', false);
            $(message).addClass('in').attr('aria-expanded', true);
            $('html, body').animate({
                'scrollTop': $(message).position().top
            });
            localStorage.removeItem('AccordionHref');
        }

    });

    getManageCharges();
    $(function () {
        $('.nav-tabs').responsiveTabs();
    });
    $(document).ready(function () {
        $(".slide-toggle").click(function () {
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function () {
        $(".slide-toggle2").click(function () {
            $(".box2").animate({
                width: "toggle"
            });
        });
    });


    $(document).on('click', '.tabcheckvalidation', function () {
        tabValidation();
    });


    function tabValidation() {
        var property_id_data = $('#property_unique_id').val();
        if (property_id_data == '') {
            $('.tabcheckvalidation').attr('href', '');
            bootbox.alert('Please Add General Information First!');
        } else {
            $(".tabcheckvalidation").each(function () {
                var dataHref = $(this).attr('data_href');
                $(this).attr('href', dataHref);
            });
        }
    }

    if ($("#propertPhotovideos-table")[0].grid) {
        $('#propertPhotovideos-table').jqGrid('GridUnload');
    } else {
        jqGridPhotovideos('All');
    }

    if ($("#propertFileLibrary-table")[0].grid) {
        $('#propertFileLibrary-table').jqGrid('GridUnload');
    } else {
        jqGridFileLibrary('All');
    }

    if ($("#key-table")[0].grid) {
        $('#key-table').jqGrid('GridUnload');
    } else {
        jqGrid('All', true);
    }
    if ($("#trackkey-table")[0].grid) {
        $('#trackkey-table').jqGrid('GridUnload');
    } else {
        jqGridTrackkey('All', true);
    }

    if ($("#blacklisted-table")[0].grid) {
        $('#blacklisted-table').jqGrid('GridUnload');
    } else {
        jqGridBVendors('All', true);
    }

    if ($("#vendors-table")[0].grid) {
        $('#vendors-table').jqGrid('GridUnload');
    } else {
        jqGridVendors('All', true);
    }

    if ($("#notes-table")[0].grid) {
        $('#notes-table').jqGrid('GridUnload');
    } else {
        jqGridNotes('All', true);
    }

    if ($("#insurance-table")[0].grid) {
        $('#insurance-table').jqGrid('GridUnload');
    } else {
        jqGridInsurance('All', true);
    }

    if ($("#fixtures-table")[0].grid) {
        $('#fixtures-table').jqGrid('GridUnload');
    } else {
        jqGridFixtures('All', true);
    }

    /**
     * To change the format of phone number
     */
    $(document).on('keydown', '.phone_number', function () {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return false;
        } else {
            $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
        }
    });

    /**
     * If the letter is not digit in phone number then don't type anything.
     */
    $(document).on('keypress', '.phone_number', function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    /**
     * If the letter is not digit in fax then don't type anything.
     */
    $(".number_only").keydown(function (e) {
        if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return false;
        }
    });

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
</script>

<!-- Jquery Starts -->
<div class="modal fade" id="renovation-info" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> X </a>

                <h4 class="modal-title">Add New Renovation</h4>
            </div>
            <div class="modal-body">
                <div class="apx-adformbox-content">
                    <form method="post" id="Re_addRenovationForm">
                        <div class="row">
                            <div class="form-outer">
                                <div class="col-sm-7 col-md-7">
                                    <label>New Renovation Date</label>
                                    <input class="form-control last_renovation" type="text"  name="last_renovation_date" id="last_renovation_date_id">
                                    <span class="ffirst_nameErr error red-star"></span>
                                </div>
                                <div class="col-sm-7 col-md-7">
                                    <label>New Renovation Time</label>
                                    <input class="form-control" type="text" name="last_renovation_time" id="last_renovation_time_id">
                                    <span class="ffirst_nameErr error red-star"></span>
                                </div>
                                <div class="col-sm-7 col-md-7">
                                    <label>New Renovation Description</label>
                                    <div class="notes_date_right_div">
                                        <textarea class="form-control capsOn notes_date_right" type="text" id="ffirst_name" name="last_renovation_description"></textarea></div>
                                    <span class="ffirst_nameErr error red-star"></span>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                        <button type="submit"  id="saveRenovationinfo" class="blue-btn">Submit</button>
                        <button rel="Re_addRenovationForm" type="button" class="clear-btn clearFormResetBank">Clear</button>
                        <a href="javascript:void(0)" class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="return-key-div" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;">X
                </a>

                <h4 class="modal-title">Return</h4>
            </div>
            <div class="modal-body">
                <div class="apx-adformbox-content">
                    <form method="post" id="returnkeyForm">

                        <div class="grid-outer grid-margin" style="max-height: 210px; overflow-x: hidden; max-width: 1347px;">
                            <table class="table table-hover table-dark" width="132%" cellspacing="0" cellpadding="0" border="0">
                                <thead>
                                <tr>
                                    <input type="hidden" value="<?= $_GET['id'] ?>" id="relid">
                                    <th width="2%" align="left">
                                    </th>
                                    <th width="10%" align="left">
                                        User
                                    </th>
                                    <th width="10%" align="left" id="thDrawAmount">
                                        Type
                                    </th>
                                    <th width="15%" align="left">
                                        Keys
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="tbodyCheckOutDetail">

                                </tbody>
                            </table>
                        </div>
                        <div class="returnbuttondiv">
                            <button type="submit" id="returnbuttonId" class="blue-btn">Return</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="modal fade" id="print_complaint" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="blue-btn" id='email_complaint' onclick="sendComplaintsEmail('#modal-body-complaints')">Email</button>
                    <button type="button" class="blue-btn" id='print_complaints'  onclick="PrintElem('#modal-body-complaints')" onclick="window.print();">Print</button>
                    <button type="button" class="close" data-dismiss="modal">×</button>

                </div>
                <div class="modal-body" id="modal-body-complaints" style="height: 380px; overflow: auto;">

                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="chartofaccountmodal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add Chat of Account</h4>
                </div>
                <div class="modal-body">
                    <form name="add_chart_account_form" id="add_chart_account_form_id" class="add_chart_account_form_Class">
                        <div class="panel-body" style="overflow: hidden;border: none;">
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>Account Type <em class="red-star">*</em></label>
                                            <select class="fm-txt form-control" name="account_type_id" id="account_type_id"></select>
                                            <span id="account_type_idErr" class="error"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Account Code <em class="red-star">*</em></label>
                                            <input id="account_code" name="account_code" maxlength="4" placeholder="Eg: 1110" class="form-control" type="text"/>
                                            <span id="account_codeErr" class="error"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Account Name <em class="red-star">*</em></label>
                                            <input id="com_account_name" name="account_name" maxlength="50" placeholder="Eg: Property Trust" class="form-control" type="text"/>
                                            <span id="account_nameErr" class="error"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>Reporting Code <em class="red-star">*</em></label>
                                            <select class="fm-txt form-control" id="reporting_code" name="reporting_code">
                                                <option value="">Select</option>
                                                <option value="1">C</option>
                                                <option value="2">L</option>
                                                <option value="3">M</option>
                                                <option value="4">B</option>
                                            </select>
                                            <span id="reporting_codeErr" class="error"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Sub Account of</label>
                                            <select class="fm-txt form-control" id="sub_account" name="sub_account">
                                                <option value="">Select</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Status</label>
                                            <select class="fm-txt form-control" id="status" name="status">
                                                <option value="1">Active</option>
                                                <option value="0">InActive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <select class="fm-txt form-control" id="posting_status" name="posting_status">
                                                <option value="1">Posting</option>
                                                <option value="0">Non Posting</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="btn-outer text-right">
                                        <input type="hidden" value="" name="chart_account_edit_id" class="form-control" id="chart_account_edit_id"/>
                                        <input type="submit" value="Save" class="blue-btn" id="saveBtnId"/>
                                        <button rel="add_chart_account_form_Class" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                        <button type="button"  class="grey-btn" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="return-key-div-history" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;">X
                </a>

                <h4 class="modal-title">Return</h4>
            </div>
            <div class="modal-body">
                <div class="apx-adformbox-content">
                    <form method="post" id="returnkeyForm">

                        <div class="grid-outer grid-margin" style="max-height: 210px; overflow-x: hidden; max-width: 1347px;">
                            <table class="table table-hover table-dark" width="132%" cellspacing="0" cellpadding="0" border="0">
                                <thead>
                                <tr>
                                    <input type="hidden" value="<?= $_GET['id'] ?>" id="relid">
                                    <th width="2%" align="left">
                                    </th>
                                    <th width="10%" align="left">
                                        User
                                    </th>
                                    <th width="10%" align="left" id="thDrawAmount">
                                        Type
                                    </th>
                                    <th width="15%" align="left">
                                        Keys
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="tbodyCheckOutDetailhistory">

                                </tbody>
                            </table>
                        </div>
                        <div class="returnbuttondiv">

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Print Modal Start-->
<div class="modal fade" id="inventoryPrintModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content pull-left">
            <div class="modal-header" style="border-bottom: unset;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                </a>
                <input type="button" class="blue-btn pull-left" onclick="PrintElem('#inventory_content')" onclick="window.print();"  value="Print"/>
                <input type="hidden" id="announcementSys_id" value="">

            </div>
            <div class="modal-body pull-left" style="height: 487px; overflow: auto;" id="inventory_content">


            </div>

        </div>
    </div>
</div>
<!-- Print Modal End-->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<script>  setTimeout(function () {
        $('.capital').css("text-transform","none");

    },1000);</script>
