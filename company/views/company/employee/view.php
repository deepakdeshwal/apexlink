<?php
/**
 * Created by PhpStorm.
 * User: chughraghav
 * Date: 6/03/2019
 * Time: 11:19 AM
 */
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
$edit_id = (isset($_REQUEST['id']))?$_REQUEST['id']:'';
$base_url=$_SERVER['SERVER_NAME'];
?>


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- MAIN Navigation Ends -->
        <section class="main-content">
            <div class="container-fluid">
                <div class="row building-listpage">
                    <div class="bread-search-outer">
                      <div class="row">
                        <div class="col-sm-8">
                          <div class="breadcrumb-outer">
                            Building Module >> <span>Building View</span>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="easy-search">
                              <input placeholder="Easy Search" type="text">
                          </div>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" id="building_property_id" val=""/>
                    <div class="col-sm-12">
                      <div class="content-section">
                         <!--Tabs Starts -->
                          <div class="right-links-outer hide-links">
                              <div class="right-links">
                                  <i class="fa fa-angle-left" aria-hidden="true"></i>
                                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </div>
                              <div id="RightMenu" class="box2">
                                  <h2>PEOPLE</h2>
                                  <div class="list-group panel">
                                      <a href="/Tenantlisting/add" class="list-group-item list-group-item-success strong collapsed">New Tenant</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Move Out</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Book Now</a>
                                      <a href="/People/AddOwners)" class="list-group-item list-group-item-success strong collapsed">New Owner</a>
                                      <a href="/Vendor/AddVendor" class="list-group-item list-group-item-success strong collapsed">New Vendor</a>
                                      <a href="/People/AddEmployee" class="list-group-item list-group-item-success strong collapsed">New Employee</a>
                                      <a href="/People/AddContact" class="list-group-item list-group-item-success strong collapsed">New Contact</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Recieve Payment</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Send Tenant Statements</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Bank Deposite</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Bill</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Tenant Instrument Register</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Vendor Instrument Register</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Tenant Transfer</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Tenant Transfer</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Move In</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Move Out</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Tenants</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Owners</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Vendors</a>
                                      <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">NSF Listing</a>
                                  </div>
                              </div>
                          </div>
                          </div>
                              <!--- Right Quick Links ---->

                              <div class="main-tabs">
                                  <!-- Nav tabs -->
                                  <!-- Nav tabs -->
                                  <ul class="nav nav-tabs" role="tablist">
                                      <li role="presentation"><a href="/Tenantlisting/Tenantlisting">Tenants</a></li>
                                      <li role="presentation"><a href="/People/Ownerlisting">Owners</a></li>
                                      <li role="presentation"><a href="/Vendor/Vendor" >Vendors</a></li>
                                      <li role="presentation"><a href="/People/ContactListt">Contacts</a></li>
                                      <li role="presentation" class="active"><a href="/People/GetEmployeeList">Employee</a></li>
                                  </ul>
                          <!-- Nav tabs -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="guest-cards">
                              <div class="form-outer form-outer2">
                                <div class="form-hdr">
                                  <h3>General Information <a class="back" href="javascript:;" onclick="goBack()"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                </div>
                                <div class="form-data">
                                  <div class="detail-outer">
                                    <div class="row">
                                    <div class="col-sm-2">
                                        <div class="dummy-img">
                                        <?= $data["employee_image"];?>
                                        </div>
                                    </div>
                                      <div class="col-sm-5">
                                        <div class="col-xs-12">
                                          <label class="text-right">Salutation  :</label>
                                          <span class="salutation"><?= @$data['salutation'];?></span>
                                        </div>
                                        <div class="col-xs-12">
                                            <label class="text-right">First Name :</label>
                                            <span class="first_name"><?= @$data['first_name'];?></span>
                                        </div>
                                        <div class="col-xs-12">
                                          <label class="text-right">Middle Name :</label>
                                          <span class="middle_name"><?= @$data["middle_name"];?></span>
                                        </div>
                                        <div class="col-xs-12">
                                          <label class="text-right">Last Name :</label>
                                          <span class="last_name"><?= @$data["last_name"];?></span>
                                        </div>
                                        <div class="col-xs-12">
                                          <label class="text-right">Maiden Name :</label>
                                          <span class="maiden_name"><?= @$data["maiden_name"];?></span>
                                        </div>

                                        <div class="col-xs-12">
                                          <label class="text-right">Nickname :</label>
                                          <span class="nick_name"><?= @$data["nick_name"];?></span>
                                        </div>

                                        <div class="col-xs-12">
                                          <label class="text-right">Gender :</label>
                                          <span class="gender"><?= @$data["gender"];?></span>
                                        </div>

                                        <div class="col-xs-12">
                                          <label class="text-right">Hobbies :</label>
                                          <span class="hobbies"><?= @$data["hobbies"];?></span>
                                        </div>
                                        <div class="col-xs-12">
                                          <label class="text-right">DOB :</label>
                                          <span class="dob"><?= $data["dob"];?></span>
                                        </div>
                                          <div class="col-xs-12">
                                              <label class="text-right">Driver License State/Province :</label>
                                              <span class="employee_license_state"><?= @$data["employee_license_state"];?></span>
                                          </div>
                                          <div class="col-xs-12">
                                              <label class="text-right">Driver License # :</label>
                                              <span class="employee_license_number"><?= @$data["employee_license_number"];?></span>
                                          </div>


                                      </div>
                                        <div class="col-sm-5">
                                            <div class="col-xs-12">
                                                <label class="text-right">Notes for this Phone Number :</label>
                                                <span class="phone_number_note"><?= @$data["phone_number_note"];?></span>
                                            </div>
                                            <div class="col-xs-12">
                                                <label class="text-right">Address1 :</label>
                                                <span class="address1"><?= @$data["address1"];?></span>
                                            </div>
                                            <div class="col-xs-12">
                                                <label class="text-right">Address2 :</label>
                                                <span class="address2"><?= @$data["address2"];?></span>
                                            </div>
                                            <div class="col-xs-12">
                                                <label class="text-right">Address3 :</label>
                                                <span class="address3"><?= @$data["address3"];?></span>
                                            </div>
                                            <div class="col-xs-12">
                                                <label class="text-right">Address4 :</label>
                                                <span class="address4"><?= @$data["address4"];?></span>
                                            </div>

                                            <div class="col-xs-12">
                                                <label class="text-right">City :</label>
                                                <span class="city"><?= @$data["city"];?></span>
                                            </div>

                                            <div class="col-xs-12">
                                                <label class="text-right">State :</label>
                                                <span class="state"><?= @$data["state"];?></span>
                                            </div>

                                            <div class="col-xs-12">
                                                <label class="text-right">Country :</label>
                                                <span class="country"><?= @$data["country"];?></span>
                                            </div>
                                            <div class="col-xs-12">
                                                <label class="text-right">Zip :</label>
                                                <span class="zipcode"><?= @$data["zipcode"];?></span>
                                            </div>
                                            <div class="col-xs-12">
                                                <label class="text-right">MaritalStatus :</label>
                                                <span class="maritial_status"><?= @$data["maritial_status"];?></span>
                                            </div>
                                            <div class="col-xs-12">
                                                <label class="text-right">Previous Company :</label>
                                                <span class="previous_company"><?= @$data["previous_company"];?></span>
                                            </div>
                                            <div class="col-xs-12">
                                                <label class="text-right">Current Company :</label>
                                                <span class="current_company"><?= @$data["current_company"];?></span>
                                            </div>

                                        </div>
                                      <div class="col-sm-12">&nbsp;</div>
                                    </div>
                                  </div>
                                  <div class="edit-foot">
                                    <a href="javascript:;" class="edit_redirection" redirection_data="info">
                                      <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                      Edit
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <!-- Form Outer Ends -->

                              <div class="form-outer form-outer2" href="#" id="vehicles">
                                <div class="form-hdr">
                                  <h3>Vehicles</h3>
                                </div>
                                <div class="form-data">
                                  <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                          <div class="panel-group" id="accordion">
                                              <div class="panel panel-default">
<!--                                                  <div class="panel-heading">-->
<!--                                                      <h4 class="panel-title">-->
<!--                                                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class=""><span class="pull-right glyphicon glyphicon-menu-up"></span> List of Keys</a> -->
<!--                                                      </h4>-->
<!--                                                  </div>-->
                                                  <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                      <div class="panel-body pad-none">
                                                          <div class="grid-outer">
                                                              <div class="apx-table listinggridDiv">
                                                            <div class="table-responsive">
                                                                <table id="employee-veichle" class="table table-bordered"></table>
                                                            </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                        </div>
                                    </div>
                                  </div>
                                   <div class="edit-foot">
                                    <a href="javascript:;" class="edit_redirection" redirection_data="vehicles">
                                      <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                      Edit
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <!-- Form Outer Ends -->

                                <div class="form-outer form-outer2" >
                                    <div class="form-hdr">
                                        <h3>Emergency Employee Details</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="detail-outer">
                                            <?php
                                             if(!empty($emergency_detail)){
                                            foreach($emergency_detail as $key => $value) { ?>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Emergency Contact Name :</label>
                                                            <span class="emergency_contact_name"><?= $value["emergency_contact_name"]; ?></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Country Code :</label>
                                                            <span class="emergency_country_code"><?= getCountryCodeValue($this->companyConnection,$value["emergency_country_code"]); ?></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Emergency Contact Phone :</label>
                                                            <span class="emergency_contact_phone"><?= $value["emergency_phone_number"]; ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Emergency Contact Email :</label>
                                                            <span class="emergency_contact_email"><?= $value["emergency_email"]; ?></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Emergency Contact Relation :</label>
                                                            <span class="emergency_contact_relation"><?= getRelation($value["emergency_relation"]); ?></span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <?php } }else{ ?>
                                                 <div class="row">
                                                     <div class="col-sm-6">
                                                         <div class="row">
                                                             <div class="col-xs-12">
                                                                 <label class="text-right">Emergency Contact Name :</label>
                                                                 <span class="emergency_contact_name"></span>
                                                             </div>
                                                             <div class="col-xs-12">
                                                                 <label class="text-right">Country Code :</label>
                                                                 <span class="emergency_country_code"></span>
                                                             </div>
                                                             <div class="col-xs-12">
                                                                 <label class="text-right">Emergency Contact Phone :</label>
                                                                 <span class="emergency_contact_phone"></span>
                                                             </div>
                                                         </div>
                                                     </div>
                                                     <div class="col-sm-6">
                                                         <div class="row">
                                                             <div class="col-xs-12">
                                                                 <label class="text-right">Emergency Contact Email :</label>
                                                                 <span class="emergency_contact_email"></span>
                                                             </div>
                                                             <div class="col-xs-12">
                                                                 <label class="text-right">Emergency Contact Relation :</label>
                                                                 <span class="emergency_contact_relation"></span>
                                                             </div>
                                                         </div>

                                                     </div>
                                                 </div>
                                            <?php } ?>
                                        </div>
                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="emergency-contact">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-outer form-outer2" >
                                    <div class="form-hdr">
                                        <h3>Credential Control</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="detail-outer">
                                            <?php
                                            if(!empty($credential_control)){
                                            foreach($credential_control as $key => $value) { ?>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Credential Name :</label>
                                                            <span class="credential_name"><?= $value["credential_name"];?></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Credential Type:</label>
                                                            <span class="credential_type"><?= getCredentialType($this->companyConnection,$value["credential_type"]);?></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Acquire Date :</label>
                                                            <span class="acquire_date"><?= dateFormatUser($value["acquire_date"],NULL,$this->companyConnection);?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Expiration Date :</label>
                                                            <span class="expiration_date"><?= dateFormatUser($value["expire_date"],NULL,$this->companyConnection);?></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Notice Period :</label>
                                                            <span class="notice_period"><?= noticePeriod($value["notice_period"]);?></span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <?php } }else{ ?>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Credential Name :</label>
                                                                <span class="credential_name"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Credential Type:</label>
                                                                <span class="credential_type"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Acquire Date :</label>
                                                                <span class="acquire_date"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Expiration Date :</label>
                                                                <span class="expiration_date"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Notice Period :</label>
                                                                <span class="notice_period"></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            <?php } ?>
                                        </div>
                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="credential_control">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>

                                </div>


                                <div class="form-outer form-outer2 mt-20" id="noteshistory">
                                    <div class="form-hdr">
                                        <h3>Notes</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="grid-outer">
                                            <div class="table-responsive">
                                                <div class="apx-table listinggridDiv">
                                                    <div class="table-responsive">
                                                        <table id="notes" class="table table-bordered"></table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="notes">
                                                <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Form Outer Ends -->

                                <!-- Form Outer Ends -->


                                <div class="form-outer form-outer2 mt-20" id="flag_bank">
                                    <div class="form-hdr">
                                        <h3>Flag Bank</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel">
                                                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                <div class=" pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="building-flags" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="flag_bank" >
                                                <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Form Outer Ends -->

                              <div class="form-outer" id="filelibraryss">
                                <div class="form-hdr">
                                  <h3>File Library</h3>
                                </div>
                                <div class="form-data" >
                                  <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                          <div class="panel-group" id="accordion" >
                                              <div class="panel "  >
<!--                                                  <div class="panel-heading">-->
<!--                                                      <h4 class="panel-title">-->
<!--                                                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class=""><span class="pull-right glyphicon glyphicon-menu-up"></span> List of Building Files</a> -->
<!--                                                      </h4>-->
<!--                                                  </div>-->
                                                  <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                      <div class="pad-none">
                                                          <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <div class="apx-table listinggridDiv">
                                                                    <div class="table-responsive">
                                                                        <table id="file-library" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                          </div>
                                                          <div class="edit-foot" >
                                                              <a href="javascript:;" class="edit_redirection" redirection_data="filelibrary">
                                                                  <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                                  Edit
                                                              </a>
                                                          </div>
                                                          
                                                      </div>
                                                  </div>
                                              </div>
                                              
                                          </div>
              
                                        </div>
                                    </div>
                                  </div>

                                </div>
                              </div >



                              <div class="form-outer">
                                <div class="form-hdr">
                                  <h3>Custom Fields</h3>

                                </div>
                                <div class="form-data">
                                    <div class="custom_field_html">
                                    </div>
                                  <div class="edit-foot">
                                    <a href="javascript:;" class="edit_redirection" redirection_data="custom">
                                      <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                      Edit
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <!-- Form Outer Ends -->
                            </div>
                        </div>
                        <!--tab Ends -->
                    </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
        <!-- Wrapper Ends -->
    <!-- Footer Ends -->
    <script>

        /*function to go back to previous page */
        function goBack() {
            window.history.back();
        }
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });
 
        <!--- Main Nav Responsive --> 
        $("#show").click(function(){
    $("#bs-example-navbar-collapse-2").show();
});
         $("#close").click(function(){
    $("#bs-example-navbar-collapse-2").hide();
});
         <!--- Main Nav Responsive -->
        
        
        $(document).ready(function(){
          $(".slide-toggle").click(function(){
            $(".box").animate({
              width: "toggle"
            });
          });
        });
        
        $(document).ready(function(){
          $(".slide-toggle2").click(function(){
            $(".box2").animate({
              width: "toggle"
            });
          });
        });
        
         <!--- Accordians -->
       $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
        	$(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });
        
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
         <!--- Accordians --> 


         <!-- SLider -->
          $(document).ready(function() {
           $('.owl-carousel').owlCarousel({
          loop: true,
          margin: 10,
          responsiveClass: true,
          autoplay:false,
          autoplayTimeout:1500,
          autoplayHoverPause:true,
          responsive: {
            0: {
            items: 1,
            nav: true
            },
            600: {
            items: 1,
            nav: true
            },
            1020: {
            items: 2,
            nav: true,
            loop: true,
            margin: 20
            }, 
            1199: {
            items: 3,
            nav: true,
            loop: true,
            margin: 20  
            }
          }
           })
         })
        var upload_url = "<?php echo SITE_URL; ?>";
    </script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/employee/employeeView.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
<!--    <script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/building/buildingList.js"></script>-->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>