<?php
$soapUrl = "https://victig.instascreen.net/send/interchange";

$xml_post_string = '
<?xml version="1.0" encoding="utf-8"?>

  <BackgroundCheck UserID="apms_xml" Password="P@ssw0rd8">
  <BackgroundSearchPackage action="submit" type="APEXLINK Package">
    <Organization type="x:requesting">
      <OrganizationName>Org. Name Goes Here</OrganizationName>
      <OrganizationUser>Hank Mess</OrganizationUser>
    </Organization>
    <PersonalData>
      <PersonName>
        <GivenName>Hank Mess</GivenName>
        <FamilyName>Family name here !!!</FamilyName>
      </PersonName>
      <DemographicDetail>
        <GovernmentId issuingAuthority="SSN">333221111</GovernmentId>
        <DateOfBirth>1973-12-25</DateOfBirth>
      </DemographicDetail>
      <PostalAddress type="current">
        <PostalCode>82082</PostalCode>
        <Region>WY</Region>
        <Municipality>Pine Bluffs</Municipality>
        <DeliveryAddress>
          <AddressLine>123</AddressLine>
          <StreetName>Main</StreetName>
        </DeliveryAddress>
      </PostalAddress>
      <PostalAddress type="previous">
        <PostalCode>87811</PostalCode>
        <Region>NM</Region>
        <Municipality>Albuquerque</Municipality>
        <DeliveryAddress>
          <AddressLine>3204</AddressLine>
          <StreetName>Lucerne</StreetName>
        </DeliveryAddress>
      </PostalAddress>
      <Email>john@test.com</Email>
      <Telephone>801-789-4229</Telephone>
    </PersonalData>
    <Screenings>
      <Screening type="credit">
        <Vendor score="yes" fraud="yes">xperian</Vendor>
      </Screening>
      <Screening type="criminal" qualifier="statewide">
        <Region>PA</Region>
      </Screening>
    </Screenings>
  </BackgroundSearchPackage>
</BackgroundCheck>
';

$headers = array(
    "POST /send/interchange",
    "Host: victig.instascreen.net",
    "Content-Type: application/soap+xml; charset=utf-8",
    "Content-Length: ".strlen($xml_post_string)
);

$url = $soapUrl;

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$response = curl_exec($ch);
curl_close($ch);

$response1 = str_replace("<soap:Body>","",$response);
$response2 = str_replace("</soap:Body>","",$response1);

$parser = simplexml_load_string($response2);
echo '<pre>'; print_r($parser); echo '</pre>';
?>