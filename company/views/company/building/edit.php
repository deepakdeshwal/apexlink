<?php
$key_access_code_info =[];
if(!empty($data["key_access_codes_info"])) {
    $key_access_code_info = unserialize($data["key_access_codes_info"]);
}

$key_access_code_desc =[];
if(!empty($data["key_access_code_desc"])) {
    $key_access_code_desc = unserialize($data["key_access_code_desc"]);
}
global $countryArray;
$countryArray = array(
    'AD' => array('name' => 'ANDORRA', 'code' => '376'),
    'AE' => array('name' => 'UNITED ARAB EMIRATES', 'code' => '971'),
    'AF' => array('name' => 'AFGHANISTAN', 'code' => '93'),
    'AG' => array('name' => 'ANTIGUA AND BARBUDA', 'code' => '1268'),
    'AI' => array('name' => 'ANGUILLA', 'code' => '1264'),
    'AL' => array('name' => 'ALBANIA', 'code' => '355'),
    'AM' => array('name' => 'ARMENIA', 'code' => '374'),
    'AN' => array('name' => 'NETHERLANDS ANTILLES', 'code' => '599'),
    'AO' => array('name' => 'ANGOLA', 'code' => '244'),
    'AQ' => array('name' => 'ANTARCTICA', 'code' => '672'),
    'AR' => array('name' => 'ARGENTINA', 'code' => '54'),
    'AS' => array('name' => 'AMERICAN SAMOA', 'code' => '1684'),
    'AT' => array('name' => 'AUSTRIA', 'code' => '43'),
    'AU' => array('name' => 'AUSTRALIA', 'code' => '61'),
    'AW' => array('name' => 'ARUBA', 'code' => '297'),
    'AZ' => array('name' => 'AZERBAIJAN', 'code' => '994'),
    'BA' => array('name' => 'BOSNIA AND HERZEGOVINA', 'code' => '387'),
    'BB' => array('name' => 'BARBADOS', 'code' => '1246'),
    'BD' => array('name' => 'BANGLADESH', 'code' => '880'),
    'BE' => array('name' => 'BELGIUM', 'code' => '32'),
    'BF' => array('name' => 'BURKINA FASO', 'code' => '226'),
    'BG' => array('name' => 'BULGARIA', 'code' => '359'),
    'BH' => array('name' => 'BAHRAIN', 'code' => '973'),
    'BI' => array('name' => 'BURUNDI', 'code' => '257'),
    'BJ' => array('name' => 'BENIN', 'code' => '229'),
    'BL' => array('name' => 'SAINT BARTHELEMY', 'code' => '590'),
    'BM' => array('name' => 'BERMUDA', 'code' => '1441'),
    'BN' => array('name' => 'BRUNEI DARUSSALAM', 'code' => '673'),
    'BO' => array('name' => 'BOLIVIA', 'code' => '591'),
    'BR' => array('name' => 'BRAZIL', 'code' => '55'),
    'BS' => array('name' => 'BAHAMAS', 'code' => '1242'),
    'BT' => array('name' => 'BHUTAN', 'code' => '975'),
    'BW' => array('name' => 'BOTSWANA', 'code' => '267'),
    'BY' => array('name' => 'BELARUS', 'code' => '375'),
    'BZ' => array('name' => 'BELIZE', 'code' => '501'),
    'CA' => array('name' => 'CANADA', 'code' => '1'),
    'CC' => array('name' => 'COCOS (KEELING) ISLANDS', 'code' => '61'),
    'CD' => array('name' => 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'code' => '243'),
    'CF' => array('name' => 'CENTRAL AFRICAN REPUBLIC', 'code' => '236'),
    'CG' => array('name' => 'CONGO', 'code' => '242'),
    'CH' => array('name' => 'SWITZERLAND', 'code' => '41'),
    'CI' => array('name' => 'COTE D IVOIRE', 'code' => '225'),
    'CK' => array('name' => 'COOK ISLANDS', 'code' => '682'),
    'CL' => array('name' => 'CHILE', 'code' => '56'),
    'CM' => array('name' => 'CAMEROON', 'code' => '237'),
    'CN' => array('name' => 'CHINA', 'code' => '86'),
    'CO' => array('name' => 'COLOMBIA', 'code' => '57'),
    'CR' => array('name' => 'COSTA RICA', 'code' => '506'),
    'CU' => array('name' => 'CUBA', 'code' => '53'),
    'CV' => array('name' => 'CAPE VERDE', 'code' => '238'),
    'CX' => array('name' => 'CHRISTMAS ISLAND', 'code' => '61'),
    'CY' => array('name' => 'CYPRUS', 'code' => '357'),
    'CZ' => array('name' => 'CZECH REPUBLIC', 'code' => '420'),
    'DE' => array('name' => 'GERMANY', 'code' => '49'),
    'DJ' => array('name' => 'DJIBOUTI', 'code' => '253'),
    'DK' => array('name' => 'DENMARK', 'code' => '45'),
    'DM' => array('name' => 'DOMINICA', 'code' => '1767'),
    'DO' => array('name' => 'DOMINICAN REPUBLIC', 'code' => '1809'),
    'DZ' => array('name' => 'ALGERIA', 'code' => '213'),
    'EC' => array('name' => 'ECUADOR', 'code' => '593'),
    'EE' => array('name' => 'ESTONIA', 'code' => '372'),
    'EG' => array('name' => 'EGYPT', 'code' => '20'),
    'ER' => array('name' => 'ERITREA', 'code' => '291'),
    'ES' => array('name' => 'SPAIN', 'code' => '34'),
    'ET' => array('name' => 'ETHIOPIA', 'code' => '251'),
    'FI' => array('name' => 'FINLAND', 'code' => '358'),
    'FJ' => array('name' => 'FIJI', 'code' => '679'),
    'FK' => array('name' => 'FALKLAND ISLANDS (MALVINAS)', 'code' => '500'),
    'FM' => array('name' => 'MICRONESIA, FEDERATED STATES OF', 'code' => '691'),
    'FO' => array('name' => 'FAROE ISLANDS', 'code' => '298'),
    'FR' => array('name' => 'FRANCE', 'code' => '33'),
    'GA' => array('name' => 'GABON', 'code' => '241'),
    'GB' => array('name' => 'UNITED KINGDOM', 'code' => '44'),
    'GD' => array('name' => 'GRENADA', 'code' => '1473'),
    'GE' => array('name' => 'GEORGIA', 'code' => '995'),
    'GH' => array('name' => 'GHANA', 'code' => '233'),
    'GI' => array('name' => 'GIBRALTAR', 'code' => '350'),
    'GL' => array('name' => 'GREENLAND', 'code' => '299'),
    'GM' => array('name' => 'GAMBIA', 'code' => '220'),
    'GN' => array('name' => 'GUINEA', 'code' => '224'),
    'GQ' => array('name' => 'EQUATORIAL GUINEA', 'code' => '240'),
    'GR' => array('name' => 'GREECE', 'code' => '30'),
    'GT' => array('name' => 'GUATEMALA', 'code' => '502'),
    'GU' => array('name' => 'GUAM', 'code' => '1671'),
    'GW' => array('name' => 'GUINEA-BISSAU', 'code' => '245'),
    'GY' => array('name' => 'GUYANA', 'code' => '592'),
    'HK' => array('name' => 'HONG KONG', 'code' => '852'),
    'HN' => array('name' => 'HONDURAS', 'code' => '504'),
    'HR' => array('name' => 'CROATIA', 'code' => '385'),
    'HT' => array('name' => 'HAITI', 'code' => '509'),
    'HU' => array('name' => 'HUNGARY', 'code' => '36'),
    'ID' => array('name' => 'INDONESIA', 'code' => '62'),
    'IE' => array('name' => 'IRELAND', 'code' => '353'),
    'IL' => array('name' => 'ISRAEL', 'code' => '972'),
    'IM' => array('name' => 'ISLE OF MAN', 'code' => '44'),
    'IN' => array('name' => 'INDIA', 'code' => '91'),
    'IQ' => array('name' => 'IRAQ', 'code' => '964'),
    'IR' => array('name' => 'IRAN, ISLAMIC REPUBLIC OF', 'code' => '98'),
    'IS' => array('name' => 'ICELAND', 'code' => '354'),
    'IT' => array('name' => 'ITALY', 'code' => '39'),
    'JM' => array('name' => 'JAMAICA', 'code' => '1876'),
    'JO' => array('name' => 'JORDAN', 'code' => '962'),
    'JP' => array('name' => 'JAPAN', 'code' => '81'),
    'KE' => array('name' => 'KENYA', 'code' => '254'),
    'KG' => array('name' => 'KYRGYZSTAN', 'code' => '996'),
    'KH' => array('name' => 'CAMBODIA', 'code' => '855'),
    'KI' => array('name' => 'KIRIBATI', 'code' => '686'),
    'KM' => array('name' => 'COMOROS', 'code' => '269'),
    'KN' => array('name' => 'SAINT KITTS AND NEVIS', 'code' => '1869'),
    'KP' => array('name' => 'KOREA DEMOCRATIC PEOPLES REPUBLIC OF', 'code' => '850'),
    'KR' => array('name' => 'KOREA REPUBLIC OF', 'code' => '82'),
    'KW' => array('name' => 'KUWAIT', 'code' => '965'),
    'KY' => array('name' => 'CAYMAN ISLANDS', 'code' => '1345'),
    'KZ' => array('name' => 'KAZAKSTAN', 'code' => '7'),
    'LA' => array('name' => 'LAO PEOPLES DEMOCRATIC REPUBLIC', 'code' => '856'),
    'LB' => array('name' => 'LEBANON', 'code' => '961'),
    'LC' => array('name' => 'SAINT LUCIA', 'code' => '1758'),
    'LI' => array('name' => 'LIECHTENSTEIN', 'code' => '423'),
    'LK' => array('name' => 'SRI LANKA', 'code' => '94'),
    'LR' => array('name' => 'LIBERIA', 'code' => '231'),
    'LS' => array('name' => 'LESOTHO', 'code' => '266'),
    'LT' => array('name' => 'LITHUANIA', 'code' => '370'),
    'LU' => array('name' => 'LUXEMBOURG', 'code' => '352'),
    'LV' => array('name' => 'LATVIA', 'code' => '371'),
    'LY' => array('name' => 'LIBYAN ARAB JAMAHIRIYA', 'code' => '218'),
    'MA' => array('name' => 'MOROCCO', 'code' => '212'),
    'MC' => array('name' => 'MONACO', 'code' => '377'),
    'MD' => array('name' => 'MOLDOVA, REPUBLIC OF', 'code' => '373'),
    'ME' => array('name' => 'MONTENEGRO', 'code' => '382'),
    'MF' => array('name' => 'SAINT MARTIN', 'code' => '1599'),
    'MG' => array('name' => 'MADAGASCAR', 'code' => '261'),
    'MH' => array('name' => 'MARSHALL ISLANDS', 'code' => '692'),
    'MK' => array('name' => 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'code' => '389'),
    'ML' => array('name' => 'MALI', 'code' => '223'),
    'MM' => array('name' => 'MYANMAR', 'code' => '95'),
    'MN' => array('name' => 'MONGOLIA', 'code' => '976'),
    'MO' => array('name' => 'MACAU', 'code' => '853'),
    'MP' => array('name' => 'NORTHERN MARIANA ISLANDS', 'code' => '1670'),
    'MR' => array('name' => 'MAURITANIA', 'code' => '222'),
    'MS' => array('name' => 'MONTSERRAT', 'code' => '1664'),
    'MT' => array('name' => 'MALTA', 'code' => '356'),
    'MU' => array('name' => 'MAURITIUS', 'code' => '230'),
    'MV' => array('name' => 'MALDIVES', 'code' => '960'),
    'MW' => array('name' => 'MALAWI', 'code' => '265'),
    'MX' => array('name' => 'MEXICO', 'code' => '52'),
    'MY' => array('name' => 'MALAYSIA', 'code' => '60'),
    'MZ' => array('name' => 'MOZAMBIQUE', 'code' => '258'),
    'NA' => array('name' => 'NAMIBIA', 'code' => '264'),
    'NC' => array('name' => 'NEW CALEDONIA', 'code' => '687'),
    'NE' => array('name' => 'NIGER', 'code' => '227'),
    'NG' => array('name' => 'NIGERIA', 'code' => '234'),
    'NI' => array('name' => 'NICARAGUA', 'code' => '505'),
    'NL' => array('name' => 'NETHERLANDS', 'code' => '31'),
    'NO' => array('name' => 'NORWAY', 'code' => '47'),
    'NP' => array('name' => 'NEPAL', 'code' => '977'),
    'NR' => array('name' => 'NAURU', 'code' => '674'),
    'NU' => array('name' => 'NIUE', 'code' => '683'),
    'NZ' => array('name' => 'NEW ZEALAND', 'code' => '64'),
    'OM' => array('name' => 'OMAN', 'code' => '968'),
    'PA' => array('name' => 'PANAMA', 'code' => '507'),
    'PE' => array('name' => 'PERU', 'code' => '51'),
    'PF' => array('name' => 'FRENCH POLYNESIA', 'code' => '689'),
    'PG' => array('name' => 'PAPUA NEW GUINEA', 'code' => '675'),
    'PH' => array('name' => 'PHILIPPINES', 'code' => '63'),
    'PK' => array('name' => 'PAKISTAN', 'code' => '92'),
    'PL' => array('name' => 'POLAND', 'code' => '48'),
    'PM' => array('name' => 'SAINT PIERRE AND MIQUELON', 'code' => '508'),
    'PN' => array('name' => 'PITCAIRN', 'code' => '870'),
    'PR' => array('name' => 'PUERTO RICO', 'code' => '1'),
    'PT' => array('name' => 'PORTUGAL', 'code' => '351'),
    'PW' => array('name' => 'PALAU', 'code' => '680'),
    'PY' => array('name' => 'PARAGUAY', 'code' => '595'),
    'QA' => array('name' => 'QATAR', 'code' => '974'),
    'RO' => array('name' => 'ROMANIA', 'code' => '40'),
    'RS' => array('name' => 'SERBIA', 'code' => '381'),
    'RU' => array('name' => 'RUSSIAN FEDERATION', 'code' => '7'),
    'RW' => array('name' => 'RWANDA', 'code' => '250'),
    'SA' => array('name' => 'SAUDI ARABIA', 'code' => '966'),
    'SB' => array('name' => 'SOLOMON ISLANDS', 'code' => '677'),
    'SC' => array('name' => 'SEYCHELLES', 'code' => '248'),
    'SD' => array('name' => 'SUDAN', 'code' => '249'),
    'SE' => array('name' => 'SWEDEN', 'code' => '46'),
    'SG' => array('name' => 'SINGAPORE', 'code' => '65'),
    'SH' => array('name' => 'SAINT HELENA', 'code' => '290'),
    'SI' => array('name' => 'SLOVENIA', 'code' => '386'),
    'SK' => array('name' => 'SLOVAKIA', 'code' => '421'),
    'SL' => array('name' => 'SIERRA LEONE', 'code' => '232'),
    'SM' => array('name' => 'SAN MARINO', 'code' => '378'),
    'SN' => array('name' => 'SENEGAL', 'code' => '221'),
    'SO' => array('name' => 'SOMALIA', 'code' => '252'),
    'SR' => array('name' => 'SURINAME', 'code' => '597'),
    'ST' => array('name' => 'SAO TOME AND PRINCIPE', 'code' => '239'),
    'SV' => array('name' => 'EL SALVADOR', 'code' => '503'),
    'SY' => array('name' => 'SYRIAN ARAB REPUBLIC', 'code' => '963'),
    'SZ' => array('name' => 'SWAZILAND', 'code' => '268'),
    'TC' => array('name' => 'TURKS AND CAICOS ISLANDS', 'code' => '1649'),
    'TD' => array('name' => 'CHAD', 'code' => '235'),
    'TG' => array('name' => 'TOGO', 'code' => '228'),
    'TH' => array('name' => 'THAILAND', 'code' => '66'),
    'TJ' => array('name' => 'TAJIKISTAN', 'code' => '992'),
    'TK' => array('name' => 'TOKELAU', 'code' => '690'),
    'TL' => array('name' => 'TIMOR-LESTE', 'code' => '670'),
    'TM' => array('name' => 'TURKMENISTAN', 'code' => '993'),
    'TN' => array('name' => 'TUNISIA', 'code' => '216'),
    'TO' => array('name' => 'TONGA', 'code' => '676'),
    'TR' => array('name' => 'TURKEY', 'code' => '90'),
    'TT' => array('name' => 'TRINIDAD AND TOBAGO', 'code' => '1868'),
    'TV' => array('name' => 'TUVALU', 'code' => '688'),
    'TW' => array('name' => 'TAIWAN, PROVINCE OF CHINA', 'code' => '886'),
    'TZ' => array('name' => 'TANZANIA, UNITED REPUBLIC OF', 'code' => '255'),
    'UA' => array('name' => 'UKRAINE', 'code' => '380'),
    'UG' => array('name' => 'UGANDA', 'code' => '256'),
    'US' => array('name' => 'UNITED STATES', 'code' => '1'),
    'UY' => array('name' => 'URUGUAY', 'code' => '598'),
    'UZ' => array('name' => 'UZBEKISTAN', 'code' => '998'),
    'VA' => array('name' => 'HOLY SEE (VATICAN CITY STATE)', 'code' => '39'),
    'VC' => array('name' => 'SAINT VINCENT AND THE GRENADINES', 'code' => '1784'),
    'VE' => array('name' => 'VENEZUELA', 'code' => '58'),
    'VG' => array('name' => 'VIRGIN ISLANDS, BRITISH', 'code' => '1284'),
    'VI' => array('name' => 'VIRGIN ISLANDS, U.S.', 'code' => '1340'),
    'VN' => array('name' => 'VIET NAM', 'code' => '84'),
    'VU' => array('name' => 'VANUATU', 'code' => '678'),
    'WF' => array('name' => 'WALLIS AND FUTUNA', 'code' => '681'),
    'WS' => array('name' => 'SAMOA', 'code' => '685'),
    'XK' => array('name' => 'KOSOVO', 'code' => '381'),
    'YE' => array('name' => 'YEMEN', 'code' => '967'),
    'YT' => array('name' => 'MAYOTTE', 'code' => '262'),
    'ZA' => array('name' => 'SOUTH AFRICA', 'code' => '27'),
    'ZM' => array('name' => 'ZAMBIA', 'code' => '260'),
    'ZW' => array('name' => 'ZIMBABWE', 'code' => '263')
);
/**
 * Created by PhpStorm.
 * User: chughraghav
 * Date: 6/03/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
    <style xmlns="http://www.w3.org/1999/html">
        .ui-timepicker-container {
            z-index: 1600 !important; /* has to be larger than 1050 */
        }
    </style>
    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- MAIN Navigation Ends -->
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12 bread-search-outer">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Building Module >> <span>Building Edit</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">

                    </div>
                    <div class="col-sm-12" id="info">
                        <div class="content-section">

                            <!--- Right Quick Links ---->
                            <!--- Right Quick Links ---->
                            <div class="right-links-outer hide-links">
                                <div class="right-links">
                                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                                <div id="RightMenu" class="box2">
                                    <h2>Properties</h2>
                                    <div class="list-group panel">
                                        <!-- Two Ends-->
                                        <a href="/Property/AddProperty" class="list-group-item list-group-item-success strong collapsed" >New Property</a>
                                        <!-- Two Ends-->

                                        <!-- Three Starts-->
                                        <a id="new_building_href" href="" class="list-group-item list-group-item-success strong collapsed" >New Building</a>
                                        <!-- Three Ends-->

                                        <!-- Four Starts-->
                                        <a href="" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">Manage Work Orders</a>
                                        <!-- Four Ends-->

                                        <!-- Five Starts-->
                                        <a href="#" id="property_inspection_link" class="list-group-item list-group-item-success strong collapsed">Property Inspection</a>
                                        <!-- Five Ends-->

                                        <!-- Six Starts-->
                                        <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">Manage Property Group</a>
                                        <!-- Six Ends-->

                                        <!-- Seven Starts-->
                                        <a id="new_unit_href" href="#" class="list-group-item list-group-item-success strong collapsed" >Add Unit</a>
                                        <!-- Seven Ends-->


                                    </div>
                                </div>
                            </div>
                            <div class="main-tabs apx-tabs">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" ><a href="#property" id="property_tab_link">Property</a></li>
                                    <li role="presentation" class="active"><a href="#" id="building_tab_link">Building</a></li>
                                    <li role="presentation"><a href="#unit" id="new_property_unit_list_href"  >Unit</a></li>
                                </ul>
                                <!--- Right Quick Links ---->
                                <div class="col-sm-12">&nbsp;</div>
                                <form name="edit_building" id="edit_building"  >
                                    <input type ="hidden" id="form_building_edit_id" name="form_building_edit_id" value="<?php echo $data["id"]?>"  />
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>
                                                Edit Building <a onclick="goBack()" class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                            </h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                <div class="div-full">
                                                    <div class="name-id-greybox">
                                                        <div class="name-id-greybox-inner">
                                                            <label>
                                                                Property Name :
                                                            </label>
                                                            <span id="property_name_span"> </span>
                                                        </div>
                                                        <div class="name-id-greybox-inner">
                                                            <label>
                                                                ID :
                                                            </label>
                                                            <span id="property_id_span"> </span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <input type="hidden" name="property_id" id="property_id" value="">
                                                <input type="hidden" name="default_login_user_name" id="default_login_user_name" value="<?php echo $_SESSION[SESSION_DOMAIN]['default_name'];?>">
                                                <input type="hidden" name="default_login_user_phone_number" id="default_login_user_phone_number" value="<?php echo $_SESSION[SESSION_DOMAIN]['phone_number'];?>">
                                                <div class="form-outer">
                                                    <div class="col-sm-2">
                                                        <label>Building ID <em class="red-star">*</em></label>
                                                        <input class="form-control" name="building_id" id="building_id" maxlength="10" placeholder="Eg: AB01234C " type="text" value="<?php echo $data["building_id"]?>"/>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Legal Name </label>
                                                        <input class="form-control" id="legal_name" placeholder="Eg: The Fairmont Waterfront" maxlength="150" name="legal_name" type="text" value="<?php echo $data["legal_name"]?>" />
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Building Name <em class="red-star">*</em></label>
                                                        <input class="form-control" id="building_name" name="building_name" placeholder="Eg: Aber B1" maxlength="150" type="text" value="<?php echo $data["building_name"]?>" />
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>No. of Units <em class="red-star">*</em> </label>
                                                        <input class="form-control" id="no_of_units" placeholder="Eg: 8"  maxlength="4" name="no_of_units"  type="text" value="<?php echo $data["no_of_units"]?>" />
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Building Address <em class="red-star">*</em></label>
                                                        <input class="form-control" placeholder="Eg: 8 Avenue" maxlength="250" name="address" id="address" type="text" value="<?php echo $data["address"]?>"/>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Smoked Allowed </label>
                                                        <?php if($data["property_smoking_allowed"] != 0){?>
                                                            <select class="form-control" id="smoking_allowed" name="smoking_allowed">
                                                                <option value="0" <?php if ($data["smoking_allowed"] == "0"){ echo "selected"; }?>>No</option>
                                                                <option value="1" <?php if ($data["smoking_allowed"] == "1"){ echo "selected"; }?>>Yes</option>
                                                            </select>
                                                        <?php } else { ?>
                                                            <select class="form-control" id="smoking_allowed" name="smoking_allowed">
                                                                <option value="0">No</option>
                                                            </select>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Pet Friendly <a id="Newpet" class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                        <?php if($data["property_pet_friendly"] != 1){?>
                                                            <select class='form-control' id='pet_options' name='pet_friendly'>
                                                            </select>
                                                        <?php } else { ?>
                                                            <select class='form-control'  name='pet_friendly'>
                                                                <option value="1">No</option>
                                                            </select>
                                                        <?php } ?>
                                                        <div class="add-popup" id='NewpetPopup' style="width: 127%;">
                                                            <h4>Add New Pet Friendly</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>Pet Friendly<em class="red-star">*</em></label>
                                                                        <input class="form-control capitalize_popup_input customValidatePetFriendly" data_required="true" data_max="150" type="text"  name='@pet_friendly'/>
                                                                        <span class="customError required"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button class="blue-btn" id="NewpetPopupSave">Save</button>
                                                                        <input type="button"  class="clear-btn clearPetForm" value='Clear' />
                                                                        <input type="button"  class="grey-btn" value='Cancel' />
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <?php
                                                    if(!empty($data["phone_number"]))
                                                        foreach($data["phone_number"] as $key => $value) { ?>
                                                            <div class="col-sm-2 multiplephonediv" >
                                                                <label>Phone Number</label>
                                                                <input class="form-control add-input p-number-format" placeholder="Phone Number" id="phone_number" maxlength="12" name="phone_number[]" value="<?php echo $value?>" type="text" />
                                                                <a class="add-icon" id="multiplephoneno" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                <a class="add-icon" id='multiplephonenocross' style="display:none" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                            </div>
                                                        <?php } else { ?>
                                                        <div class="col-sm-2 multiplephonediv" >
                                                            <label>Phone Number</label>
                                                            <input class="form-control add-input p-number-format" placeholder="Phone Number" id="phone_number" maxlength="12" name="phone_number[]" value="<?php $value?>" type="text" />
                                                            <a class="add-icon" id="multiplephoneno" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            <a class="add-icon" id='multiplephonenocross' style='display: none;' href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="col-sm-2">
                                                        <label>Note</label>
                                                        <input class="form-control capital" name="note" placeholder="Eg: Note " maxlength="150" id="note" value="<?php echo $data["note"]?>" type="text" />
                                                    </div>
                                                    <?php
                                                    if(!empty($data["fax_number"]))
                                                        foreach($data["fax_number"] as $key1 => $value1) { ?>
                                                            <div class="col-sm-2 multiplefaxdiv">
                                                                <label>Fax Number</label>
                                                                <input placeholder="Fax Number" class="form-control add-input p-number-format" maxlength="12" id="fax_number" name="fax_number[]" value="<?php echo $value1?>" type="text" />
                                                                <a class="add-icon" id="multiplefaxno" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                <a class="add-icon" id='multiplefaxnocross' style="display: none" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>

                                                            </div>
                                                        <?php } else { ?>
                                                        <div class="col-sm-2 multiplefaxdiv">
                                                            <label>Fax Number</label>
                                                            <input placeholder="Fax Number" class="form-control add-input p-number-format" maxlength="12" id="fax_number" name="fax_number[]" type="text" />
                                                            <a class="add-icon" id="multiplefaxno" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            <a class="add-icon" id='multiplefaxnocross' style='display: none;' href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="form-outer">
                                                        <div class="col-sm-2">
                                                            <label>Last Renovation</label>
                                                            <input class="form-control" placeholder="Eg: 01/01/2017" id="last_renovation_date" value="<?php echo $data["building_last_renovation_date"]?>" disabled name="last_renovation_date" type="text" />
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label>Last Renovation Time</label>
                                                            <input class="form-control" placeholder="Eg: 04:36 PM" type="text" id="last_renovation_time" value="<?php echo $data["building_last_renovation_time"]; ?>" disabled name="last_renovation_time" />
                                                        </div>
                                                        <div class="col-sm-4 textarea-form notes_date_right_div">
                                                            <label>Last Renovation Description </label>
                                                            <textarea class="form-control notes_date_right" id="last_renovation_description" disabled name="last_renovation_description"><?php echo $data["building_last_renovation_description"]; ?></textarea>
                                                        </div>
                                                        <u><a href="#" id="newRenovation" > New Renovation</a></u>
                                                    </div>
                                                    <div class="row col-sm-12" id="school_district_html" >
                                                    </div>
                                                    <?php
                                                    if(!empty($result)) {
                                                        foreach ($result as $key => $value) { ?>
                                                            <div
                                                                    class="school_district_muncipality">
                                                                <div class="col-sm-2">
                                                                    <label>School District</label>
                                                                    <input class="form-control" type="text" disabled
                                                                           value="<?php echo $value['school_district']; ?>"/>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Code</label>
                                                                    <input class="form-control" type="text" disabled
                                                                           value="<?php echo $value['code']; ?>"/>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Notes</label>
                                                                    <input class="form-control" type="text" disabled
                                                                           value="<?php echo $value['notes']; ?>"/>
                                                                </div>
                                                            </div>
                                                        <?php }
                                                    }
                                                    ?>
                                                    <?php
                                                    if(!empty($key_access_code_info)){
                                                        foreach($key_access_code_info as $key=> $key_access_code_infos) {
                                                            ?>
                                                            <div class="col-xs-12 col-sm-4 col-md-3 key_optionsDivClass" id="key_optionsDiv">
                                                                <label>Key/Access Code Info. <a  class="pop-add-icon Newkey" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>

                                                                <select class='form-control add-input key_access_codes_list key_access_codes_info_<?= $key ?>' id='key_options' name='key_access_codes_info[]'></select>

                                                                <div class="key_access_codeclass">
                                                                    <textarea name="key_access_code_desc[]" id='key_access_code' class="form-control key_access_codetext key_access_codetextdynm_<?= $key ?>"><?= isset($key_access_code_desc[$key])?$key_access_code_desc[$key]:'' ?></textarea>
                                                                </div>
                                                                <?php if($key == 0){?>
                                                                    <a id="NewkeyPlus" class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                <?php  } else { ?>
                                                                    <a class="add-icon"  id="NewkeyMinus" class="add-icon" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                                <?php }?>
                                                                <div class="add-popup NewkeyPopup" id="NewkeyPopupid">
                                                                    <h4>Add New Key Access Code</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>Key Access Code<em class="red-star">*</em></label>
                                                                                <input class="form-control keyAccessCode customValidateKeyAccessCode key-access-input" data_required="true" data_max="150" type="text" name='@key_code'/>
                                                                                <span class="customError required"></span>
                                                                            </div>

                                                                            <div class="btn-outer text-right">
                                                                                <button type="button" class="blue-btn NewkeyPopupSave" data_id="" >Save</button>
                                                                                <button type="button" class="clear-btn clearFormReset" id="clear_add_new_key_access">Clear</button>
                                                                                <input type="button"  class="grey-btn NewkeyPopupCancel" value='Cancel'/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        <?php }  } else { ?>
                                                        <div class="col-xs-12 col-sm-4 col-md-3 key_optionsDivClass" id="key_optionsDiv">
                                                            <label>Key/Access Code Info. <a  class="pop-add-icon Newkey" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>

                                                            <select class='form-control add-input key_access_codes_list'  id='key_options' name='key_access_codes_info[]'></select>

                                                            <div class="key_access_codeclass">
                                                                <textarea name="key_access_code_desc[]" id='key_access_code' class="form-control"></textarea>
                                                            </div>
                                                            <a id="NewkeyPlus" class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            <a class="add-icon" style='display:none' id="NewkeyMinus" class="add-icon" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                            <div class="add-popup NewkeyPopup" id="NewkeyPopupid">
                                                                <h4>Add New Key Access Code</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>Key Access Code<em class="red-star">*</em></label>
                                                                            <input class="form-control keyAccessCode customValidateKeyAccessCode key-access-input" data_required="true" data_max="150" type="text" name='@key_code'/>
                                                                            <span class="customError required"></span>
                                                                        </div>

                                                                        <div class="btn-outer text-right">
                                                                            <button type="button" class="blue-btn NewkeyPopupSave" data_id="" >Save</button>
                                                                            <button type="button" class="clear-btn clearFormReset" id="clear_add_new_key_access">Clear</button>
                                                                            <input type="button"  class="grey-btn NewkeyPopupCancel" value='Cancel'/>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    <?php } ?>
                                                    <div class="clearfix"></div>
                                                    <div class="col-sm-4 textarea-form">
                                                        <label>Amenities <a id='Newamenities' class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                        <div id="amenties_box" class="form-control"></div>
                                                        <div class="add-popup" id='NewamenitiesPopup'>
                                                            <h4>Add New Amenity</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Amenity Code<em class="red-star">*</em></label>
                                                                        <input class="form-control customValidateAmenities"  data_max="50" type="text" name="@code"/>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <label> Amenity Name <em class="red-star">*</em></label>
                                                                        <input class="form-control customValidateAmenities" data_required="true" data_max="50" type="text" name="@name"/>
                                                                        <span class="customError required"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button class="blue-btn" id="NewamenitiesPopupSave">Save</button>
                                                                        <button type="button" class="clear-btn clearFormReset" id="clear_add_new_ameneties_access">Clear</button>
                                                                        <input type="button"  class="grey-btn" value='Cancel' />
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 textarea-form notes_date_right_div">
                                                        <label>Building Description </label>
                                                        <textarea class="form-control notes_date_right" placeholder="Eg: This is a family friendly building with onsite laundry. Suites are newly renovated with balconies." name="description" id="description" rows="2"><?php echo $data["description"];?></textarea>
                                                    </div>

                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="photo-upload">
                                                        <label>Photos</label>
                                                        <!--                                                                <input type="button" class="blue-btn" value="Click here to Upload "></input>-->
                                                        <button type="button" id="uploadPhotoVideo" class="green-btn pull-right">Click Here to Upload</button>
                                                        <input id="photosVideos" type="file" name="photo_videos[]" accept="image/*, .mp4, .mkv, .avi" multiple style="display: none;">
                                                        <!--                                                                <button type="button" id="removePhotoVideo" class="orange-btn">Remove All Photos</button>-->
                                                        <!--                                                                <button type="button" id="savePhotoVideo" class="blue-btn">Save </button>-->
                                                    </div>
                                                    <div class="row" id="photo_video_uploads">
                                                    </div>
                                                    <div class="grid-outer listinggridDiv">
                                                        <div class="apx-table">
                                                            <div class="table-responsive">
                                                                <table id="buildingPhotovideos-table" class="table table-bordered">
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="photo-upload">
                                                        <label>File Library</label>
                                                        <button type="button" id="add_libraray_file" class="green-btn pull-right">Add Files...</button>
                                                        <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                    </div>
                                                    <div class="row" id="file_library_uploads">
                                                    </div>
                                                    <div class="grid-outer listinggridDiv">
                                                        <div class="apx-table">
                                                            <div class="table-responsive">
                                                                <table id="buildingFileLibrary-table" class="table table-bordered">
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-outer2">
                                                <div class="col-sm-12">&nbsp;</div>
                                            </div>
                                            <div class="form-outer" id="keys">
                                                <!--                                        <input type="hidden" value="" name="key_edit_id" id="key_edit_id">-->
                                                <div class="form-hdr">
                                                    <h3>
                                                        Key Tracker
                                                    </h3>
                                                </div>
                                                <div >
                                                    <div class="form-data" >
                                                        <div class="row">
                                                            <div class="latefee-check-outer form-outer2">
                                                                <div class="col-sm-12 latefee-check-hdr">
                                                                    <div class="check-outer" >
                                                                        <input type="radio" value="add_key" class="key_tracker_radio" name="@key_tracker" checked />
                                                                        <label class="blue-label">Add key</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 latefee-check-hdr" >
                                                                    <div class="check-outer">
                                                                        <input type="radio"  value="track_key" class="key_tracker_radio" name="@key_tracker" />
                                                                        <label class="blue-label">Track Key</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="div-full" id="addnewkey">
                                                                <a  style="cursor: pointer" target="_self" >Add Key</a>
                                                            </div>
                                                            <div id="add_key_tracker" class="form-outer" style="display: none">
                                                                <input type="hidden" name="edit_key_id" id="edit_key_id" value="" />
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Key ID</label>
                                                                    <input type="text" data_required="true" placeholder="Key Tag" maxlength="50" name="key_id" id="key_id" class="form-control customValidatekeys" />
                                                                    <span class="customError required"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Description</label>
                                                                    <input type="text" data_required="true" class="form-control customValidatekeys" placeholder="This key is for 1st building" maxlength="50" name="key_description" id="key_description" />
                                                                    <span class="customError required"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Total Keys</label>
                                                                    <input type="number" data_required="true"  min="1"  name="total_keys" placeholder="Eg: 10" maxlength="50" id="total_keys" class="form-control customValidatekeys" />
                                                                    <span class="customError required"></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn" type="submit" id="keySaveBtnId">Save</button>
                                                                        <button type="button" class="clear-btn clearFormReset" id="clear_add_key">Clear</button>
                                                                        <button style="display:none" type="button" class="clear-btn clearFormReset" id="reset_update_key">Reset</button>
                                                                        <button type="button" class="grey-btn" type="button" id="keyCancel">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div id='KeycheckoutDiv' class="form-outer" style="display: none;">

                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Key Holder</label>
                                                                    <input type="text" class="form-control owner_pre_vendor customValidatecheckoutkeys" data_required="true" maxlength="50" id="key_holder"/>
                                                                    <input type="hidden" value="" name="key_holder" id="key_holder_id">
                                                                    <span class="customError required"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Keys</label>
                                                                    <input type="text" type="number" min="1" data_required="true" class="form-control customValidatecheckoutkeys" maxlength="50" name="checkout_keys" id="checkout_keys"/>
                                                                    <span class="customError required"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Description</label>
                                                                    <input type="text" data_required="true" class="form-control customValidatecheckoutkeys" maxlength="12" name="checkout_desc"/>
                                                                    <span class="customError required"></span>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn" id="KeycheckoutSave">Save</button>
                                                                        <button type="button" class="clear-btn clearFormReset" id="clear_checkout">Clear</button>
                                                                        <button type="button" class="grey-btn" id="KeycheckoutCancel">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <div class="div-full" id="addtrackkey" style="display: none" >
                                                                <a  style="cursor: pointer" target="_self">Track Key</a>
                                                            </div>
                                                            <div class="form-outer" id="track_key_tracker" style="display: none">
                                                                <input type="hidden" name="edit_track_key_id" id="edit_track_key_id" value="" />
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Name<em class="red-star">*</em></label>
                                                                    <input type="text" class="form-control owner_pre_vendor1 customValidateTrackKey" data_required="true" maxlength="50" id="track_key_holder"/>
                                                                    <input type="hidden" value="" name="@track_key_holder" id="track_key_holder_id">
                                                                    <span class="customError required"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Email<em class="red-star">*</em></label>
                                                                    <input type="text" class="form-control customValidateTrackKey" data_required="true" data_max="50" data_email maxlength="50" name="@email" id="trackemail"/>
                                                                    <span class="customError required"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Company Name</label>
                                                                    <input type="text" class="form-control " data_required="true" data_max="50" maxlength="50" name="@company_name" id="trackcompany_name"/>
                                                                    <span class="customError required"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Phone #</label>
                                                                    <input type="text" class="form-control phone_number  p-number-format" data_required="true"   data_max="12" maxlength="12" name="@phone" id="trackphone"/>
                                                                    <span class="customError required"></span>
                                                                </div>

                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Address1</label>
                                                                    <input type="text" class="form-control " data_required="true" data_max="50" maxlength="50" name="@Address1" id="trackAddress1"/>
                                                                    <span class="customError required"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Address2</label>
                                                                    <input type="text" class="form-control " data_required="true" data_max="50" maxlength="50" name="@Address2" id="trackAddress2"/>
                                                                    <span class="customError required"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Address3</label>
                                                                    <input type="text" class="form-control " data_required="true" data_max="50" maxlength="50" name="@Address3" id="trackAddress3"/>
                                                                    <span class="customError required"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Address4</label>
                                                                    <input type="text" class="form-control " data_required="true" data_max="50" maxlength="50" name="@Address4" id="trackAddress4"/>
                                                                    <span class="customError required"></span>
                                                                </div>

                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Key#<em class="red-star">*</em></label>
                                                                    <input type="text" class="form-control customValidateTrackKey"  data_required="true"  maxlength="50" name="@key_number" id="trackkey_number"/>
                                                                    <span class="customError required"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Key Quantity<em class="red-star">*</em></label>
                                                                    <input type="text" class="form-control number_only customValidateTrackKey" data_only_number="true" data_required="true"  data_max="20" maxlength="20" name="@key_quality" id="trackkey_quality"/>
                                                                    <span class="customError required"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Pick Up Date<em class="red-star">*</em></label>
                                                                    <input type="text" class="form-control customValidateTrackKey" readonly name="@pick_up_date" id="pick_up_date" />
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Pick Up Time<em class="red-star">*</em></label>
                                                                    <input type="text" class="form-control " readonly name="@pick_up_time" id="pick_up_time" readonly=""/>
                                                                </div>

                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Return Date</label>
                                                                    <input type="text" class="form-control" readonly name="@return_date" id="return_date" readonly=""/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Return Time</label>
                                                                    <input type="text" class="form-control" readonly name="@return_time" id="return_time" readonly=""/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Key Designator</label>
                                                                    <span class="customError required"></span>
                                                                    <input type="text" class="form-control "  data_required="true" maxlength="50" name="@key_designator" id="key_designator"/>
                                                                </div>
                                                                <div class="col-sm-12 text-right">
                                                                    <div class="btn-outer">
                                                                        <button type="button" class="blue-btn" id="KeyTrackSave">Save</button>
                                                                        <button type="button" class="clear-btn clearFormReset" id="clear_track_key">Clear</button>
                                                                        <button style="display:none" type="button" class="clear-btn clearFormReset" id="reset_update_track_key">Reset</button>
                                                                        <button type="button" class="grey-btn" id="keyTrackCancel">Cancel</button>
                                                                    </div>
                                                                </div>



                                                            </div>
                                                            <!--  keys grid starts here  -->
                                                            <div class="col-sm-12"  id="add_key_grid">
                                                                <div class="accordion-grid">
                                                                    <div class="accordion-outer">
                                                                        <div class="bs-example">
                                                                            <div class="panel-group" id="accordion">
                                                                                <div class="panel panel-default">
                                                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                                        <div class="panel-body pad-none">
                                                                                            <div class="grid-outer">
                                                                                                <div class="apx-table listinggridDiv">
                                                                                                    <div class="table-responsive">
                                                                                                        <table id="building-keys" class="table table-bordered"></table>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--  keys grid ends here  -->

                                                            <!--  keys grid starts here  -->
                                                            <div class="col-sm-12" id="track_key_grid">
                                                                <div class="accordion-grid">
                                                                    <div class="accordion-outer">
                                                                        <div class="bs-example">
                                                                            <div class="panel-group" id="accordion">
                                                                                <div class="panel panel-default">
                                                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                                        <div class="panel-body pad-none">
                                                                                            <div class="grid-outer">
                                                                                                <div class="apx-table listinggridDiv">
                                                                                                    <div class="table-responsive">
                                                                                                        <table id="building-track-keys" class="table table-bordered"></table>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--  keys grid ends here  -->

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <!--key traker ends-->

                                            <!-- flag start-->
                                            <div class="form-outer" id="flags">
                                                <div class="form-hdr">
                                                    <h3>Flag Bank</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="property-status">
                                                        <button type="button" id="new_flag" class="blue-btn pull-right" >New Flag</button>
                                                    </div>
                                                    <div class="row" id="flagFormDiv" style="display: none;">
                                                        <!--                                        <form id="flagForm">-->
                                                        <div class="grey-box-add">
                                                            <div class="form-outer">
                                                                <input type="hidden" name="id" id="flag_id">
                                                                <div class="col-xs-12 col-sm-3">
                                                                    <label>Flagged By </label>
                                                                    <input class="form-control" name="flag_by" id="flag_flag_by" placeholder="Flagged By" maxlength="100" type="text" value=""/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-3">
                                                                    <label>Date</label>
                                                                    <input class="form-control" name="date" id="flag_flag_date" readonly placeholder="Eg: <?php echo $current_date; ?>" type="text"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-3">
                                                                    <label>Flag Name </label>
                                                                    <input class="form-control" name="flag_name" id="flag_flag_name" maxlength="100" placeholder="Please Enter the Name of this Flag" type="text"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-3">
                                                                    <label>Country Code</label>
                                                                    <select class='form-control' name='country_code' id="flag_country_code">
                                                                        <?php
                                                                        // $select;
                                                                        foreach ($countryArray as $code => $country) {
                                                                            $countryName = ucwords(strtolower($country["name"])); // Making it look good
                                                                            echo "<option value='" . $code . "' " . (($code == 'US') ? "selected" : "") . ">" . $code . " - " . $countryName . " (+" . $country["code"] . ")</option>";
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-3">
                                                                    <label>Phone Number</label>
                                                                    <input class="form-control phone_number p-number-format" name="flag_phone_number" id="flag_phone_number" maxlength="12" placeholder="123-456-7890" type="text"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-3">
                                                                    <label>Flag Reason</label>
                                                                    <input CLASS="form-control" name="flag_reason" id="flag_flag_reason" maxlength="100" placeholder="Flag Reason" type="text"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-3">
                                                                    <label>Flagged For</label>
                                                                    <input class="form-control capital" readonly id="flagged_for" maxlength="100"  type="text"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-3">
                                                                    <label>Completed</label>
                                                                    <select class='form-control' name='completed' id="completed">
                                                                        <option value="0">No</option>
                                                                        <option value="1">Yes</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6 notes_date_right_div">
                                                                    <label>Note</label>
                                                                    <textarea class="form-control notesDateTime notes_date_right" name="flag_note" id="flag_note" placeholder="Flag Notes"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <div class="btn-outer text-right">
                                                                    <button type="button" class="blue-btn" type="submit" id="flagSaveBtnId">Save</button>
                                                                    <button type="button" class="clear-btn clearFormReset" id="clear_flag_bank">Clear</button>
                                                                    <button style="display:none" type="button" class="clear-btn clearFormReset" id="reset_flag_bank">Reset</button>
                                                                    <button type="button" class="grey-btn" type="button" id="flagCancel">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--                                        </form>-->
                                                    </div>
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                            <div class="pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <div class="apx-table listinggridDiv">
                                                                                            <div class="table-responsive">
                                                                                                <table id="propertyFlag-table" class="table table-bordered"></table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- flag ends-->
                                            <!--Complaints starts -->
                                            <div class="form-outer" id="complaints">
                                                <div class="form-hdr">
                                                    <h3>Building Complaints</h3>
                                                </div>
                                                <div class="form-data">

                                                    <div class="property-status">
                                                        <button type="button" id="new_complaint_button" class="blue-btn pull-right">New Complaint</button>
                                                    </div>

                                                    <div style="display:none" id="new_complaint">
                                                        <div class="row">
                                                            <input type="hidden" name="edit_complaint_id" id="edit_complaint_id" />
                                                            <div class="col-sm-2">
                                                                <label>Complaint ID</label>
                                                                <input class="form-control" name="complaint_id" id="complaint_id"  placeholder="Eg: AB01234C " type="text" value="GKATCK">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Complaint Date </label>
                                                                <input class="form-control" id="complaint_date" name="complaint_date" placeholder="24/6/1990" readonly>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Complaint Type<a id="complainttype" class="pop-add-icon" href="javascript:;"> <i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select class='form-control' id='complaint_type_options' name='complaint_type_options'>
                                                                    <option disabled selected value> -- select an option -- </option>
                                                                </select>
                                                                <div class="add-popup" id='ComplaintTypePopup'>
                                                                    <h4>Add New Complaint TYpe</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>Complaint Type<em class="red-star">*</em></label>
                                                                                <input class="form-control capitalize_popup_input customValidateComplaint" data_required="true" placeholder="Complaint Type" type="text"  name='@complaint_type'/>
                                                                                <span class="customError required"></span>
                                                                            </div>
                                                                            <div class="btn-outer">
                                                                                <button type="button"  class="blue-btn" id="NewpetComplaintSave">Save</button>
                                                                                <button type="button" class="clear-btn clearFormReset" id="clear_complaint_type">Clear</button>

                                                                                <button type="button"  class="grey-btn" id="NewComplaintcancel" value='Cancel' >Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2 notes_date_right_div">
                                                                <label>Complaint Notes</label>
                                                                <textarea class="form-control notesDateTime notes_date_right"  id="complaint_note" name="complaint_note"></textarea>
                                                            </div>
                                                            <div class="col-sm-2" id="other_notes_div" style="display:none">
                                                                <label>Other Notes</label>
                                                                <textarea class="form-control notesDateTime"  id="other_notes" name="other_notes"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="property-status text-right">
                                                            <button type="button" class=" blue-btn" id="complaint_save">Save</button>
                                                            <button type="button"  class="clear-btn clear_complaint">Clear</button>
                                                            <button type="button"  class="clear-btn reset_complaint" style="display:none">Reset</button>
                                                            <button type="button" class=" grey-btn" id="complaint_cancel">Cancel</button>
                                                        </div>

                                                    </div>
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                            <div class=" pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <div class="apx-table listinggridDiv">
                                                                                            <div class="table-responsive">
                                                                                                <table id="building-complaints" class="table table-bordered"></table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-outer">
                                                    <button type="button" class=" blue-btn pull-right" id="print_email_button">Print/Email</button>
                                                </div>
                                            </div>
                                            <div class="form-outer" id="custom">
                                                <div class="form-hdr">
                                                    <h3>Custom Fields</h3>

                                                </div>
                                                <div class="form-data">
                                                    <div class="custom_field_html">
                                                    </div>
                                                    <div class="property-status">
                                                        <button type="button" id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn pull-right">Add Custom Field</button>
                                                    </div>
                                                </div>
                                                <div class="btn-outer text-right">
                                                    <button type="submit" class=" blue-btn" id="building_save">Update</button>
                                                    <button type="button" class="clear-btn clearFormReset" id="clear_update_building_cancel">Clear</button>
                                                    <button type="button" class=" grey-btn" id="building_cancel">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Complaints ends -->


                                        <!-- Form Outer Ends -->
                                    </div>
                            </div>
                            </form>
                        </div>
                        <div class="row">
                            <div class="property-status">
                                <div class="col-sm-12">
                                    <label class="filter-label">Filter By:</label>
                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                        <select id="jqGridStatus" class="form-control">
                                            <option value="All">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="property-status">
                                <div class="col-sm-12">
                                    <div class="listinggridDiv">
                                        <div class="grid-outer">
                                            <div class="apx-table">
                                                <div class="table-responsive">
                                                    <table id="building-table-edit" class="table table-bordered"></table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- start custom field model -->
    <div class="container">
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Custom Field</h4>
                    </div>
                    <div class="modal-body" style="height: 380px;">
                        <div class="form-outer col-sm-12">
                            <form id="custom_field">
                                <input type="hidden" id="customFieldModule" name="module" value="building">
                                <input type="hidden" name="id" id="custom_field_id" value="">
                                <div class="row custom_field_form">
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Field Name <em class="red-star">*</em></label>
                                        </div>
                                        <div class="col-sm-9 field_name">
                                            <input class="form-control" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                            <span class="required error"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Data Type</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="form-control data_type" id="data_type" name="data_type">
                                                <option value="text">Text</option>
                                                <option value="number">Number</option>
                                                <option value="currency">Currency</option>
                                                <option value="percentage">Percentage</option>
                                                <option value="url">URL</option>
                                                <option value="date">Date</option>
                                                <option value="memo">Memo</option>
                                            </select>
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Default value</label>
                                        </div>
                                        <div class="col-sm-9 default_value">
                                            <input class="form-control default_value" id="default_value" type="text" name="default_value" placeholder="">
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Required Field</label>
                                        </div>
                                        <div class="col-sm-9 is_required">
                                            <select class="form-control" name="is_required" id="is_required">
                                                <option value="1">Yes</option>
                                                <option value="0" selected="selected">No</option>
                                            </select>
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-outer text-right">

                                    <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                    <button type="button"  class="clear-btn clear_custom_filed_popup">Clear</button>
                                    <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End custom field model -->

    <!-- start custom field model -->
    <div class="container">
        <div class="modal fade" id="lastRenovationModal" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Add New Renovation</h4>
                    </div>

                    <div class="modal-body" style="height: 380px;">
                        <div class="form-outer col-sm-12">
                            <form id="new_renovation_form">
                                <input type ="hidden" id="edit_building_id" name="edit_building_id" value="<?php echo $data["id"]?>"  />
                                <div class="row custom_field_form">
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>New Renovation Date</label>
                                        </div>
                                        <div class="col-sm-9 field_name">
                                            <input class="form-control" placeholder="Eg: 01/01/2017" readonly type="text" maxlength="100" id="new_renovation_date" name="new_renovation_date" placeholder="">
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>New Renovation Time </label>
                                        </div>
                                        <div class="col-sm-9 field_name">
                                            <input class="form-control" readonly placeholder="Eg: 04:36 PM" type="text" id="new_renovation_time" name="new_renovation_time" />
                                        </div>
                                    </div>

                                    <div class="col-sm-9 textarea-form notes_date_right_div">
                                        <label>Last Renovation Description </label>
                                        <textarea class="form-control notes_date_right"  id="new_renovation_description" name="new_renovation_description"></textarea>
                                    </div>

                                </div>
                                <div class="btn-outer text-right">
                                    <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                    <button type="button" class="clear-btn clearFormReset" id="clear_update_add_renovation_building_clear">Clear</button>
                                    <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End custom field model -->

    <!-- start custom field model -->
    <div class="container">
        <div class="modal fade" id="print_complaint" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="blue-btn" id='email_complaint' onclick="sendComplaintsEmail('#modal-body-complaints')">Email</button>
                        <button type="button" class="blue-btn" id='print_complaints'  onclick="PrintElem('#modal-body-complaints')" onclick="window.print();">Print</button>
                        <button type="button" class="close" data-dismiss="modal">×</button>

                    </div>
                    <div class="modal-body" id="modal-body-complaints" style="height: 380px; overflow: auto;">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End custom field model -->

    <!-- Return checkout modal starts here -->

    <!--<div class="modal fade" id="return-key-div" role="dialog">-->
    <!--    <div class="modal-dialog modal-md">-->
    <!--        <div class="modal-content" style="width: 100%;">-->
    <!--            <div class="modal-header">-->
    <!--                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
    <!--                <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>-->
    <!--                </a>-->
    <!---->
    <!--                <h4 class="modal-title">Return</h4>-->
    <!--            </div>-->
    <!--            <div class="modal-body">-->
    <!--                <div class="apx-adformbox-content">-->
    <!--                    <form method="post" id="returnkeyForm">-->
    <!--                        <div class="grid-outer grid-margin" style="max-height: 210px; overflow-x: hidden; max-width: 1347px;">-->
    <!--                            <table width="132%" cellspacing="0" cellpadding="0" border="0">-->
    <!--                                <thead>-->
    <!--                                <tr>-->
    <!--                                    <input type="hidden" value="--><?//= $_GET['id'] ?><!--" id="relid">-->
    <!--                                    <th width="2%" align="left">-->
    <!--                                    </th>-->
    <!--                                    <th width="10%" align="left">-->
    <!--                                        User-->
    <!--                                    </th>-->
    <!--                                    <th width="10%" align="left" id="thDrawAmount">-->
    <!--                                        Type-->
    <!--                                    </th>-->
    <!--                                    <th width="15%" align="left">-->
    <!--                                        Keys-->
    <!--                                    </th>-->
    <!--                                </tr>-->
    <!--                                </thead>-->
    <!--                                <tbody id="tbodyCheckOutDetail">-->
    <!---->
    <!--                                </tbody>-->
    <!--                            </table>-->
    <!--                        </div>-->
    <!--                        <div class="returnbuttondiv">-->
    <!--                            <button type="submit" id="returnbuttonId" class="blue-btn">Return</button>-->
    <!--                        </div>-->
    <!--                    </form>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->


    <div class="modal fade" id="return-key-div" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;">X
                    </a>

                    <h4 class="modal-title">Return</h4>
                </div>
                <div class="modal-body">
                    <div class="apx-adformbox-content">
                        <form method="post" id="returnkeyForm">

                            <div class="grid-outer grid-margin" style="max-height: 210px; overflow-x: hidden; max-width: 1347px;">
                                <table class="table table-hover table-dark" width="132%" cellspacing="0" cellpadding="0" border="0">
                                    <thead>
                                    <tr>
                                        <input type="hidden" value="<?= $_GET['id'] ?>" id="relid">
                                        <th width="2%" align="left">
                                        </th>
                                        <th width="10%" align="left">
                                            User
                                        </th>
                                        <th width="10%" align="left" id="thDrawAmount">
                                            Type
                                        </th>
                                        <th width="15%" align="left">
                                            Keys
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbodyCheckOutDetail">

                                    </tbody>
                                </table>
                            </div>
                            <div class="returnbuttondiv">
                                <button type="submit" id="returnbuttonId" class="blue-btn">Return</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="return-key-div-history" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;">X
                    </a>

                    <h4 class="modal-title">Return</h4>
                </div>
                <div class="modal-body">
                    <div class="apx-adformbox-content">
                        <form method="post" id="returnkeyForm">

                            <div class="grid-outer grid-margin" style="max-height: 210px; overflow-x: hidden; max-width: 1347px;">
                                <table class="table table-hover table-dark" width="132%" cellspacing="0" cellpadding="0" border="0">
                                    <thead>
                                    <tr>
                                        <input type="hidden" value="<?= $_GET['id'] ?>" id="relid">
                                        <th width="2%" align="left">
                                        </th>
                                        <th width="10%" align="left">
                                            User
                                        </th>
                                        <th width="10%" align="left" id="thDrawAmount">
                                            Type
                                        </th>
                                        <th width="15%" align="left">
                                            Keys
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbodyCheckOutDetailhistory">

                                    </tbody>
                                </table>
                            </div>
                            <div class="returnbuttondiv">

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="sendMailModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Email</h4>
                    </a>
                </div>
                <form id="sendEmail">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-1">
                                <button class="blue-btn compose-email-btn">Send</button>
                            </div>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-2"><label>To <em class="red-star">*</em></label></div>
                                    <div class="col-sm-10"><span><input class="form-control to" name="to" type="text"/></span></div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <a class="add-recipient-link addToRecepent" href="#"> Add Recipients</a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-1">
                            </div>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-2"><label>Cc</div>
                                    <div class="col-sm-10"><span><input class="form-control cc" name="cc" type="text"/></span></div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <a class="add-recipient-link addCcRecepent" href="#"> Add Recipients</a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-1">
                            </div>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-2"><label>Bcc </label></div>
                                    <div class="col-sm-10"><span><input class="form-control bcc" name="bcc" type="text"/></span></div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <a class="add-recipient-link addBccRecepent" href="#"> Add Recipients</a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-1">
                            </div>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-2"><label>Subject <em class="red-star">*</em></label></div>
                                    <div class="col-sm-10"><span><input class="form-control subject" name="subject" type="text"/></span></div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-1">
                            </div>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-2"><label>Body <em class="red-star">*</em></label></div>
                                    <div class="col-sm-10">
                                        <span><textarea class="form-control summernote" name="body"></textarea></span>
                                        <div class="btn-outer">
                                            <button class="blue-btn">Send</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                            </div>
                        </div>


                        <div class="attachmentFile"></div>


                    </div>
                </form>
            </div>
        </div>

    </div>


    <div class="container">
        <div class="modal fade" id="torecepents" role="dialog">
            <div class="modal-dialog modal-sm" style="width: 600px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Add Recipients </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-outer" style="float: none;height:300px;">
                            <div class="col-sm-6">
                                <label>Select <em class="red-star">*</em></label>
                                <select class="form-control selectUsers">
                                    <option value="">Select</option>
                                    <option value="2">Tenant</option>
                                    <option value="4">Owner</option>
                                    <option value="3">Vendor</option>
                                    <option value="5">Other Contacts</option>
                                    <option value="6">Guest Card</option>
                                    <option value="7">Rental Application</option>
                                    <option value="8">Employee</option>
                                </select>
                            </div>
                            <div class="col-sm-6">

                            </div>

                            <div class="col-sm-12">
                                <div class="userDetails"></div>
                            </div>

                            <div class="col-sm-12">
                                <button class="blue-btn" id="SendselectToUsers">Done</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="modal fade" id="ccrecepents" role="dialog">
            <div class="modal-dialog modal-sm" style="width: 600px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Add Recipients </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-outer" style="float: none;height:300px;">
                            <div class="col-sm-6">
                                <label>Select <em class="red-star">*</em></label>
                                <select class="form-control selectCcUsers">
                                    <option value="">Select</option>
                                    <option value="2">Tenant</option>
                                    <option value="4">Owner</option>
                                    <option value="3">Vendor</option>
                                    <option value="5">Other Contacts</option>
                                    <option value="6">Guest Card</option>
                                    <option value="7">Rental Application</option>
                                    <option value="8">Employee</option>
                                </select>
                            </div>
                            <div class="col-sm-12">
                                <div class="userCcDetails"></div>
                            </div>

                            <div class="col-sm-12">
                                <button class="blue-btn" id="SendselectCcUsers">Done</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="modal fade" id="bccrecepents" role="dialog">
            <div class="modal-dialog modal-sm" style="width: 600px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Add Recipients </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-outer" style="float: none;height:300px;">
                            <div class="col-sm-6">
                                <label>Select <em class="red-star">*</em></label>
                                <select class="form-control selectBccUsers">
                                    <option value="">Select</option>
                                    <option value="2">Tenant</option>
                                    <option value="4">Owner</option>
                                    <option value="3">Vendor</option>
                                    <option value="5">Other Contacts</option>
                                    <option value="6">Guest Card</option>
                                    <option value="7">Rental Application</option>
                                    <option value="8">Employee</option>
                                </select>
                            </div>
                            <div class="col-sm-12">
                                <div class="userBccDetails"></div>
                            </div>

                            <div class="col-sm-12">
                                <button class="blue-btn" id="SendselectBccUsers">Done</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Return checkout modal ends here -->
    <!-- Wrapper Ends -->
    <script type="text/javascript">



        var date_format = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format'] ?>";
        var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format'] ?>";

        var pet_friendly = "<?php echo $data['building_pet_friendly'];?>"
        setTimeout(function(){
            $('#pet_options option[value='+pet_friendly+']').attr('selected','selected');
            var amenities = <?php echo json_encode($amenities) ?>;
            $.each(amenities, function (key, value) {
                $(":checkbox[value='"+value+"']").prop("checked","true");
            });

        }, 3000);

        var key_access_codes_infos = <?php echo json_encode($key_access_code_info) ?>;



        setTimeout(function () {
            $.each(key_access_codes_infos, function (key, value) {
                //  alert(value);
                $(".key_access_codes_info_"+key+" option[value='"+value+"']").attr("selected","selected");
            });
        }, 3000);
    </script>
    <script>
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        var defaultFormData = '';
        $(function () {
            $('.nav-tabs').responsiveTabs();
        });
$(document).ready(function () {
    setTimeout(function () {
        $('.capital').css("text-transform","none");

    },1000);
});
        <!--- Main Nav Responsive-->
        $("#show").click(function () {
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function () {
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive-->


        $(document).ready(function () {
            $(".slide-toggle").click(function () {
                $(".box").animate({
                    width: "toggle"
                });
            });
        });

        $(document).ready(function () {
            $(".slide-toggle2").click(function () {
                $(".box2").animate({
                    width: "toggle"
                });
            });
        });

        <!--- Accordians-->
        $(document).ready(function () {
            // Add minus icon for collapse element which is open by default
            $(".collapse.in").each(function () {
                $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
            });

            // Toggle plus minus icon on show hide of collapse element
            $(".collapse").on('show.bs.collapse', function () {
                $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
            }).on('hide.bs.collapse', function () {
                $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
            });

            $(document).on('click','.clearPetForm',function () {
                $('.customValidatePetFriendly').val('');
            });

        });
        <!--- Accordians-->

        $(document).on('click','#uploadPhotoVideo',function(){
            $('#photosVideos').val('');
            $('#photosVideos').trigger('click');
        });

        $(document).on('click','#add_libraray_file',function(){
            $('#file_library').val('');
            $('#file_library').trigger('click');
        });
        var upload_url = "<?php echo SITE_URL; ?>";

    </script>

    <script>

        $(document).on('click','#clear_update_building_cancel',function(){
            bootbox.confirm("Do you want to clear this form?", function (result) {
                if (result == true) {
                    window.location.reload();
                }
            });


        });
        var default_form_data = '';
        var defaultIgnoreArray = [];
        $(document).on('click','#clear_update_add_renovation_building_clear',function(){

            resetFormClear('#new_renovation_form', ['new_renovation_time'], 'form', false,default_form_data,defaultIgnoreArray);
        });
        $(document).on('click','#clear_add_new_key_access',function(){
            resetFormClear('#NewkeyPopupid',[],'div',false,default_form_data,defaultIgnoreArray);
        });
        $(document).on('click','#clear_add_new_ameneties_access',function(){
            resetFormClear('#NewamenitiesPopup',[],'div',false,default_form_data,defaultIgnoreArray);
        });

        $(document).on('click','#clear_complaint_type',function(){
            resetFormClear('#ComplaintTypePopup',[],'div',false,default_form_data,defaultIgnoreArray);
        });




        $(document).on('click','#clear_add_key',function(){
            bootbox.confirm("Do you want to clear this form?", function (result) {
                if (result == true) {

                    resetFormClear('#add_key_tracker',[],'div',false,default_form_data,defaultIgnoreArray);
                }
            });
        });

        $(document).on('click','#clear_track_key',function(){
            bootbox.confirm("Do you want to clear this form?", function (result) {
                if (result == true) {

                    resetFormClear('#track_key_tracker',['@pick_up_date','@pick_up_time','@return_date','@return_time'],'div',false,default_form_data,defaultIgnoreArray);
                }
            });
        });

        $(document).on('click','#clear_flag_bank',function(){
            bootbox.confirm("Do you want to clear this form?", function (result) {
                if (result == true) {
                    $("#flag_note").val('');
                    resetFormClear('#flagFormDiv',['flag_by','date','flag_phone_number'],'div',false,default_form_data,defaultIgnoreArray);
                }
            });
        });


        // $(document).on('click','#clear_add_key',function(){
        //     bootbox.confirm("Do you want to clear this form?", function (result) {
        //         if (result == true) {
        //
        //             resetFormClear('#add_key_tracker',[],'div',false,default_form_data,defaultIgnoreArray);
        //         }
        //     });
        // });

        $(document).on('click','.clear_complaint',function(){
            bootbox.confirm("Do you want to clear this form?", function (result) {
                if (result == true) {
                    $("#complaint_type_options").val('');
                    $("#complaint_note").val('')
                    resetFormClear('#new_complaint',['complaint_id','complaint_date'],'div',false,default_form_data,defaultIgnoreArray);
                }
            });
        });

        $(document).on('click','#clear_checkout',function(){
            bootbox.confirm("Do you want to clear this form?", function (result) {
                if (result == true) {
                    $("#_easyui_textbox_input1").val('');
                    $("#checkout_keys-error").html('');
                    resetFormClear('#KeycheckoutDiv',[],'div',false,default_form_data,defaultIgnoreArray);
                }
            });
        });



        $(document).on('click','.clear_custom_filed_popup',function(){
            resetFormClear('#custom_field',['data_type','is_required'],'form',true,default_form_data,defaultIgnoreArray);
        });



        /* reset forms */


        $(document).on('click','#reset_update_key',function () {
            resetEditForm("#add_key_tracker",[],true,defaultFormData,[]);
        });

        $(document).on('click','#reset_update_track_key',function () {
            bootbox.confirm("Do you want to reset this form?", function (result) {
                if (result == true) {
                    resetEditForm("#track_key_tracker", [], false, defaultFormData, []);
                    $("#_easyui_textbox_input2").val(defaultFormData[18].value)
                }
            });

        });

        $(document).on('click','#reset_flag_bank',function () {
            resetEditForm("#flagFormDiv",[],true,defaultFormData,[]);
        });

        $(document).on('click','.reset_complaint',function () {
            resetEditForm("#new_complaint",[],true,defaultFormData,[]);
        });


        /* reset forms */
    </script>


    <!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/building/building.js"></script>-->

    <!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/company/building.js"></script>-->
    <!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/building/building.js"></script>-->
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/bootstrap-tagsinput.css" />
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/building/buildingEdit.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/commonPopup.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/building/buildingFileLibrary.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/building/buildingPhotoVideos.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
    <!-- flag js -->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/building/flag/flagValidation.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/building/flag/flag.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/building/keys/keys.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.easyui.min.js"></script>
    <script>
        var updated_at_date = "<?php echo $data['updated_at']; ?>";
        setTimeout(function () {
            edit_date_time(updated_at_date);
        },2000);
    </script>
    <script>
        $("#sendEmail").validate({
            rules: {
                to: {
                    required:true
                },
                subject: {
                    required:true
                },
                body: {
                    required:true
                },
            },
            submitHandler: function (e) {
                var tenant_id = $(".tenant_id").val();
                var form = $('#sendEmail')[0];
                var formData = new FormData(form);
                var path = $(".attachments").attr('href');
                /*  alert(path);*/
                var to = $(".to").val();
                formData.append('to_users',to);
                formData.append('action','sendFileLibraryattachEmail');
                formData.append('class','TenantAjax');
                formData.append('path', path);
                $.ajax({
                    url: '/Tenantlisting/getInitialData',
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        info =  JSON.parse(data);
                        if(info.status=="success"){
                            toastr.success("Email has been sent successfully");
                            $("#sendMailModal").modal('hide');
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });
    </script>

    <script>
        $(document).ready(function(){
            $('.summernote').summernote({
                addclass: {
                    debug: false,
                    classTags: [{title:"Button","value":"btn btn-success"},"jumbotron", "lead","img-rounded","img-circle", "img-responsive","btn", "btn btn-success","btn btn-danger","text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
                },
                width: '100%',
                height: '300px',
                //margin-left: '15px',
                toolbar: [
                    // [groupName, [list of button]]
                    ['img', ['picture']],
                    ['style', ['style', 'addclass', 'clear']],
                    ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
                    ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
                    ['extra', ['video', 'table', 'height']],
                    ['misc', ['undo', 'redo', 'codeview', 'help']]
                ]
            });
        });

    </script>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/easyui.css" rel="stylesheet">
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>