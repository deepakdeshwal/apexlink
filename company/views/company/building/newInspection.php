<?php
/**
 * Created by PhpStorm.
 * User: chughraghav
 * Date: 6/03/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- MAIN Navigation Ends -->


    <!-- MAIN Navigation Ends -->
    <section id="main-contentId" class="main-content">
        <input type="hidden" name="building_id" id="building_id" value="<?php echo  $_GET['id']; ?>">
        <input type="hidden" name="property_id" id="property_id" value="<?php echo  $_GET['id']; ?>">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Property &gt;&gt; <span>New Inspection</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="content-section">
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <input type="hidden" id="addInspection" name="addInspection" value="addInspection">
                            <input type="hidden" id="editInspection" name="editInspection" value="editInspection">
                            <input type="hidden" id="addInspectionid" name="addInspectionid" value="<?php echo $_GET["id"]; ?>">

                            <form method="post" id="addPropertyInspection" class="addInspectionDetails" enctype="multipart/form-data">
                                <input type="hidden" name="object_type" id="object_type" value="Building Inspection">
                            <div class="accordion-form">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">New Inspection</a><a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                    </h4>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Date of Inspection</label>
                                                                <input class="form-control inspection_date calander" type="text" name="inspection_date" value=""/>                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Inspection Type <em class="red-star">*</em></label>
                                                                <select class="form-control" name="inspection_type" id="inspection_type">
                                                                    <option value="">Select</option>
                                                                    <option value="1">Exterior</option>
                                                                    <option value="2">Interior</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Inspection Name <em class="red-star">*</em></label>
                                                                <select id="inspectionNmelist" class="form-control inspectionNmelist" name="inspection_name"><option value="">Select</option></select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Property Name <em class="red-star"></em></label>
                                                                <input type="text" name="property_name" id="property_name" class="form-control" />
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Property Address</label>
                                                                <input type="text" name="property_address" id="property_address" class="form-control capital" />
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Building Name</label>
                                                                <input type="text" name="building_name" id="building_name" class="form-control capital" value=""/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Building Address</label>
                                                                <input type="text" name="building_address" id="building_address" class="form-control capital" />
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div id="modelInspectionarea" class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion">
                                                            Inspection Areas <em class="red-star">*</em>
                                                            <span class="col-sm-2 text-right pull-right">
                                                                <button type="button"  id="buildingInspection" data-toggle="modal" data-backdrop="static" data-target="#AddNewInspectionModal" class="blue-btn">New Inspection Area</button>
                                                            </span>
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div class="panel-body">
                                                    <div class="row">

                                                        <div class="inspection-outer">
                                                            <!--<div class="inspection-sub-child">
                                                                <div class="col-xs-12">
                                                                    <div class="col-xs-3">
                                                                        <div class="check-input-outer">
                                                                            <input class="checkboxInspection" type="checkbox" value="testing" name="inspection_area[inspectionName][]">
                                                                            <label>Doors</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-1">
                                                                        <select id="rating" name="inspection_area[ratingParent][]" class="form-control rating_parent valid" aria-invalid="false">
                                                                            <option value="1">A</option>
                                                                            <option value="2">B</option>
                                                                            <option value="3">C</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="inspection-sub-child">
                                                                <div class="col-xs-12">
                                                                    <div class="col-xs-3">
                                                                        <div class="check-input-outer">
                                                                            <input class="checkboxInspection" type="checkbox" value="testing" name="inspection_area[inspectionName][]">
                                                                            <label>Windows</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-1">
                                                                        <select id="rating" name="inspection_area[ratingParent][]" class="form-control rating_parent valid" aria-invalid="false">
                                                                            <option value="1">A</option>
                                                                            <option value="2">B</option>
                                                                            <option value="3">C</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>-->
                                                            <!--div id="newAddedInspectionArea" class="col-xs-12"></div>-->
                                                        </div>
                                                    </div>
                                                </div>
                                         </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion">Notes </a>
                                                    </h4>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-4 textarea-form">
                                                                <div class="notes_date_right_div">
                                                                <textarea name="notes" id="inspectionNotes" class="form-control capital notes_date_right " rows="6"></textarea>
                                                                </div>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion">Photos</a>
                                                    </h4>
                                                </div>
                                                <div>
                                                    <div class="panel-body">
                                                        <div class="form-outer">
                                                            <div class="col-xs-12 col-sm-12">
                                                                <div class="btn-outer photo-upload">
                                                                    <button type="button" id="uploadPhotoVideo" class="green-btn">Add Photos</button>
                                                                    <input id="photosVideos" type="file" name="photo_videos[]" accept="image/*, .mp4, .mkv, .avi" multiple style="display: none;">
                                                                    <button id="remove_library_file" class="remove_library_file orange-btn">Remove All Photos</button>
                                                                </div>
                                                                <div class="row" id="photo_video_uploads">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">Custom Fields</h4>
                                                </div>
                                                <div class="panel-collapse">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12">
                                                                    <div class="custom_field_html">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3 text-right pull-right">
                                                                    <div class="btn-outer">
                                                                        <button type="button"  id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">New Custom Field</button>
                                                                    </div>
                                                                </div>
                                                                <!--<div class="col-sm-12">
                                                                    <div class="btn-outer">
                                                                        <button type="button" id="custom_field_submit" class="blue-btn">Submit</button>
                                                                    </div>
                                                                </div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <div class="btn-outer text-right">
                                                           <!-- <button class="blue-btn" type="submit" >Save </button>-->
                                                            <a href="javascript:;"><input class="blue-btn" type="submit" value="Save"></a>
                                                            <input type='button'  value="Clear" class="clear-btn clearBuildingForm" >
                                                            <input type="button" class="grey-btn cancel-btn-inspect" value="Cancel">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                            <form method="post" id="editInspectionDetails" class="editInspectionDetails" enctype="multipart/form-data" style="display: none;">
                                <input type="hidden" name="object_type" id="object_type1" value="Building Inspection">
                                <div class="accordion-form">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">New Inspection</a><a class="back back-inspection-btn" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                        </h4>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Date of Inspection</label>
                                                                    <input class="form-control inspection_date calander" id="inspection_date_inspection" type="text" name="inspection_date" value=""/>                                                            </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Inspection Type <em class="red-star">*</em></label>
                                                                    <div id="inspTypeId">
                                                                        <select class="form-control" name="inspection_type" id="inspection_type1">
                                                                            <option value="">Select</option>
                                                                            <option value="1">Exterior</option>
                                                                            <option value="2">Interior</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Inspection Name <em class="red-star">*</em></label>
                                                                    <div id="abcsss">
                                                                        <select id="inspectionNmelists" class="form-control inspectionNmelist" name="inspection_name">
                                                                            <option value="">Select</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Property Name <em class="red-star"></em></label>
                                                                    <input type="text" name="property_name" id="property_name_inspection" class="form-control" />
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Property Address</label>
                                                                    <input type="text" name="property_address" id="property_address_inspection" class="form-control" />
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Building Name</label>
                                                                    <input type="text" name="building_name" id="building_name_inspection" class="form-control building_name" />
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Building Address</label>
                                                                    <input type="text" name="building_address" id="building_address_inspection" class="form-control" />
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div id="modelInspectionarea1" class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion">
                                                                Inspection Areas <em class="red-star">*</em>
                                                                <span class="col-sm-2 text-right pull-right">
                                                                <button type="button"  id="buildingInspection1" data-toggle="modal" data-backdrop="static" data-target="#AddNewInspectionModal" class="blue-btn">New Inspection Area</button>
                                                            </span>
                                                            </a>
                                                        </h4>
                                                    </div>

                                                    <div class="panel-body">
                                                        <div class="row">

                                                            <div class="inspection-outer">
                                                                <!--<div class="inspection-sub-child">
                                                                    <div class="col-xs-12">
                                                                        <div class="col-xs-3">
                                                                            <div class="check-input-outer">
                                                                                <input class="checkboxInspection" type="checkbox" value="testing" name="inspection_area[inspectionName][]">
                                                                                <label>Doors</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <select id="rating" name="inspection_area[ratingParent][]" class="form-control rating_parent valid" aria-invalid="false">
                                                                                <option value="1">A</option>
                                                                                <option value="2">B</option>
                                                                                <option value="3">C</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="inspection-sub-child">
                                                                    <div class="col-xs-12">
                                                                        <div class="col-xs-3">
                                                                            <div class="check-input-outer">
                                                                                <input class="checkboxInspection" type="checkbox" value="testing" name="inspection_area[inspectionName][]">
                                                                                <label>Windows</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <select id="rating" name="inspection_area[ratingParent][]" class="form-control rating_parent valid" aria-invalid="false">
                                                                                <option value="1">A</option>
                                                                                <option value="2">B</option>
                                                                                <option value="3">C</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                                                                <!--div id="newAddedInspectionArea" class="col-xs-12"></div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion">Notes </a>
                                                        </h4>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-4 textarea-form">
                                                                    <div class="notes_date_right_div">
                                                                        <div class="notes-timer-outer">
                                                                            <textarea name="notes" id="inspectionNotes" class="form-control notes_date_right inspectionNotesI capital" rows="6"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion">Photos</a>
                                                        </h4>
                                                    </div>
                                                    <div>
                                                        <div class="panel-body">
                                                            <div class="form-outer">
                                                                <div class="">
                                                                    <div class="btn-outer photo-upload">
                                                                        <button type="button" id="uploadPhotoVideo" class="green-btn">Add Photos</button>
                                                                        <input id="photosVideos" type="file" name="photo_videos[]" accept="image/*, .mp4, .mkv, .avi" multiple style="display: none;">
                                                                        <button id="remove_library_file" class="orange-btn">Remove All Photos</button>
                                                                    </div>
                                                                    <div class="row" id="photo_video_uploads">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="apx-table">
                                                                        <div class="table-responsive">
                                                                            <table id="inspectionPhotos-table" class="table table-bordered">
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="edit-outer edit-foot"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">Custom Fields</h4>
                                                    </div>
                                                    <div class="panel-collapse">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <div class="custom_field_html">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3 text-right pull-right">
                                                                        <div class="btn-outer">
                                                                            <button type="button"  id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">New Custom Field</button>
                                                                        </div>
                                                                    </div>
                                                                    <!--<div class="col-sm-12">
                                                                        <div class="btn-outer">
                                                                            <button type="button" id="custom_field_submit" class="blue-btn">Submit</button>
                                                                        </div>
                                                                    </div>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <div class="btn-outer text-right">
                                                                <a href="javascript:;"><input class="blue-btn" type="submit" value="Update"></a>
                                                                <?php
                                                                $url =  COMPANY_SUBDOMAIN_URL.'Building/BuildingInspection?id='.$_GET['id'];
                                                                ?>
                                                                <input type="button" class="clear-btn reset_form" value="Reset">
                                                                <!--<button class="grey-btn cancelBtnData" onclick="window.location.href=<?php /*echo $url;*/?>"> Cancel </button>-->
                                                                <input type="button" class="grey-btn cancelBtnData" value="Cancel" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- Nav tabs -->
                    </div>
                </div>

    </section>

</div>
<!-- start custom field model -->
<div class="container">
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body" style="height: 380px;">
                    <div class="form-outer col-sm-12">
                        <form id="custom_field">
                            <input type="hidden" name="id" id="custom_field_id" value="">
                            <div class="row custom_field_form">
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Field Name <em class="red-star">*</em></label>
                                    </div>
                                    <div class="col-sm-9 field_name">
                                        <input class="form-control" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                        <span class="required error"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Data Type</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control data_type" id="data_type" name="data_type">
                                            <option value="text">Text</option>
                                            <option value="number">Number</option>
                                            <option value="currency">Currency</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="url">URL</option>
                                            <option value="date">Date</option>
                                            <option value="memo">Memo</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Default value</label>
                                    </div>
                                    <div class="col-sm-9 default_value">
                                        <input class="form-control default_value" id="default_value" type="text" name="default_value" placeholder="">
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Required Field</label>
                                    </div>
                                    <div class="col-sm-9 is_required">
                                        <select class="form-control" name="is_required" id="is_required">
                                            <option value="1">Yes</option>
                                            <option value="0" selected="selected">No</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-outer text-right">
                                <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                <button type="button" class="clear-btn ClearCustomForm" >Clear</button>
                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End custom field model -->

<!-- Add New Inspection Area Modal Start-->
<div class="container">
    <div class="modal fade" id="AddNewInspectionModal" class="AddNewInspectionModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add New Inspection Area</h4>
                </div>
                <form method="post" id='addInspectionAreaForm'>
                    <div class="modal-body">
                        <div class="col-sm-12">
                            <label>New Inspection Area Name <em class="red-star">*</em> </label>
                            <input  class="form-control capital" id="inspection_area_name" name="inspection_area_name" type="text" value="" />
                            <br>
                            <label>New Sub-Inspection Area?
                                <input type="radio" value="1" name="sub_inspection_area"> Yes
                                <input type="radio" value="0"  name="sub_inspection_area" checked>No
                            </label>
                            <br>
                            <br>
                            <div id="newAddedInspectionArea" style="display: none">

                                <label>New Sub-Inspection Area Name </label>
                                <input  class="form-control modal-add-more1 add-input sub_inspection_area_name capital" name="sub_inspection_area_name[]" type="text" />
                                <i class="fa fa-plus-circle modal-add-more2 fa-add" id="addSubInspection"  aria-hidden="true"></i>
                                <div id="addMoreSubInspectionDiv"></div>

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="object_id" id="object_id" value="<?php echo  $_GET['id']; ?>">
                        <input type="hidden" name="object_type" id="object_type" value="Building Inspection">
                        <div class="btn-outer text-right">
                            <input type="submit" class="blue-btn" id='CustomSearchButton' value="Save">
                            <input type="button" class="clear-btn" id='ClearinspectionArea' value="Clear">
                            <a id=""><input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Add New Inspection Area Modal End-->

<script type="text/javascript">
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var date_format = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format'] ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
</script>
<script>
    //$("#mydate").datepicker().datepicker("setDate", new Date());
    $("input[name='inspection_date']").datepicker({
        dateFormat: date_format,
        autoclose: true,
        changeMonth: true,
        changeYear: true
    }).datepicker("setDate", new Date());
</script>
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $(function () {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive-->
    $("#show").click(function () {
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function () {
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive-->


    $(document).ready(function () {
        $(".slide-toggle").click(function () {
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function () {
        $(".slide-toggle2").click(function () {
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians-->
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
    <!--- Accordians-->

    $(document).on('click','#uploadPhotoVideo',function(){
        $('#photosVideos').val('');
        $('#photosVideos').trigger('click');
    });

    $(document).on('click','#add_libraray_file',function(){
        $('#file_library').val('');
        $('#file_library').trigger('click');
    });
    var upload_url = "<?php echo SITE_URL; ?>";
</script>
<script>
    var radio_val =  $('input[name=sub_inspection_area]:checked').val();
    $('input[name=sub_inspection_area]').on('click', function() {
        radio_val = $('input[name=sub_inspection_area]:checked').val();
        if(radio_val == '1'){
            $('#newAddedInspectionArea').show();
        } else {
            $('#newAddedInspectionArea').hide();
        }
    });

    /**
     * Add multiple 'New Sub-Inspection Area Name' div's
     */
    $("#addSubInspection").on('click',function () {
        var HTML = '<div class="forClose">\n' +
            '<label>New Sub-Inspection Area Name </label>' +
            '<input  class="form-control modal-add-more1 add-input sub_inspection_area_name capital" name="sub_inspection_area_name[]" type="text" />' +
            '<i style="color: red;" class="fa fa-times-circle remove modal-add-more2 fa-delete"  aria-hidden="true"></i>\n'+
            ' </div>'

        $('#addMoreSubInspectionDiv').append(HTML);
    });

    $(document).on('click','.remove',function () {
        $(this).closest(".forClose").remove();
    });

    $(document).on('click','#ClearinspectionArea',function () {
    $('#addInspectionAreaForm')[0].reset();
    });

    $(document).on('click','.ClearCustomForm',function () {
       $('#custom_field')[0].reset();
    });

</script>




<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/building/inspection_building.js"></script>
<!-- custom field js -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/commonPopup.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
<!-- custom field js -->


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>