<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">

                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>MAINTENANCE</h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="/WorkOrder/AddWorkOrder" class="list-group-item list-group-item-success strong collapsed" >New Work Order</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="" href="/WorkOrder/AddWorkOrder" class="list-group-item list-group-item-success strong collapsed" >New Recurring Work Order</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="/WorkOrder/WorkOrders" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">View All Work Orders</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="/ticket/tickets" id="" class="list-group-item list-group-item-success strong collapsed">View All Tickets</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="/Inspection/Inspection" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">New Inspection</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="/LostAndFound/LostAndFound" class="list-group-item list-group-item-success strong collapsed" >Lost & Found Manager</a>
                                <!-- Seven Ends-->
                                <!-- Seven Starts-->
                                <a id="" href="/MileageLog/MileageLog" class="list-group-item list-group-item-success strong collapsed" >Mileage Log</a>
                                <!-- Seven Ends-->
                                <!-- Seven Starts-->
                                <a id="" href="/Maintenance/Inventory" class="list-group-item list-group-item-success strong collapsed" >Inventory Tracker</a>
                                <!-- Seven Ends-->
                                <!-- Seven Starts-->
                                <a id="" href="/PurchaseOrder/PurchaseOrder" class="list-group-item list-group-item-success strong collapsed" >New Purchase Order</a>
                                <!-- Seven Ends-->
                            </div>
                        </div>
                    </div>
                    
                    <div class="main-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/ticket/tickets">Tickets</a></li>
                            <li role="presentation"><a href="/WorkOrder/WorkOrders">Work Orders</a></li>
                            <li role="presentation" class="active"><a href="/PurchaseOrder/PurchaseOrderListing" >Purchase Orders</a></li>
                            <li role="presentation"><a href="/LostAndFound/LostAndFound">Lost & Found Manager</a></li>
                            <li role="presentation"><a href="/MileageLog/MileageLog">Mileage Log</a></li>
                            <li role="presentation"><a href="/Inspection/Inspection">Inspections</a></li>
                            <li role="presentation"><a href="/Maintenance/Inventory">Inventory Tracker</a></li>
                        </ul>
                        <!--<div class="atoz-outer">A-Z <span>All</span></div>-->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tickets-purchase-order">
                                <div class="property-status">
                                    <div class="row">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="btn-outer text-right">
                                                <a href="/PurchaseOrder/PurchaseOrder" class="blue-btn">New Purchase Order</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Purchase Orders</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark" id="purchasetable"></table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=" col-sm-12 text-right">
                                    <a href="javascript:void(0);" class="downloadpdf blue-btn">PDF</a>
                                    <a href="javascript:void(0);" class="downloadexcel blue-btn">Excel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    if(localStorage.getItem("rowcolorPurchaseOrder")){
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(14)').addClass("green_row_right");
            localStorage.removeItem('rowcolorPurchaseOrder');
        }, 2000);
    }
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/maintenance/purchase_orders/purchase_orders.js"></script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>