<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-2 visible-xs">

                </div>
                <div class="col-sm-3 col-xs-8">
                    <div class="logo"><img src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/images/logo.png"></div>
                </div>
                <div class="col-sm-9 col-xs-2">
                    <div class="hdr-rt">
                        <!-- TOP Navigation Starts -->

                        <nav class="navbar navbar-default">

                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li class="hidden-xs">Welcome:  Sonny Kesseben (ACL Properties), June 01, 2018 <a herf="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i></a> <a href="javascript:;"><i class="fa fa-calculator" aria-hidden="true"></i></a></li>
                                    <li><a href="#">Link</a></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i>Settings <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link disabled" href="#"><i class="fa fa-question" aria-hidden="true"></i>Help</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link disabled" href="#"><i class="fa fa-wrench" aria-hidden="true"></i>Support</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link disabled" href="#"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                                    </li>
                                </ul>

                            </div><!-- /.navbar-collapse -->
                        </nav>
                        <!-- TOP Navigation Ends -->

                    </div>
                </div>
            </div>
        </div>

    </header>

    <!-- MAIN Navigation Starts -->
    <section class="main-nav">
        <nav class="navbar navbar-default">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header visible-xs">
                <a id="show" class="icon-bar" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                    <a class="nav-close visible-xs" id="close" href="#"><i class="fa fa-times" aria-hidden="true"></i> </a>

                    <li><a class="spotlight"  href="/Dashboard/Dashboard">Spotlight</a></li>
                    <li id="properties_top"><a class="properties" href="/Property/PropertyModules">Properties</a></li>
                    <li id="people_top"><a class="people" href="/Tenantlisting/Tenantlisting">People</a></li>
                    <li id="leases_top"><a class="leases" href="/GuestCard/ListGuestCard">Leases</a></li>
                    <li id="maintenance_top"><a class="maintenance" href="/ticket/tickets" >Maintenance</a></li>
                    <li id="accounting_top"><a class="accounting" href="/Accounting/Accounting">Accounting</a></li>
                    <li id="communication_top"><a class="communication" href="/Communication/InboxMails">Communication</a></li>
                    <li id="marketing"><a class="marketing" href="/Marketing/MarketingListing">Marketing</a></li>
                    <li id="reports"><a class="reports" href="/Reporting/Reporting">Reports</a></li>
                </ul>

            </div><!-- /.navbar-collapse -->
        </nav>
    </section>
    <!-- MAIN Navigation Ends -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="content-data">
                    <div class="main-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/ticket/tickets">Tickets</a></li>
                            <li role="presentation"><a href="/WorkOrder/WorkOrders">Work Orders</a></li>
                            <li role="presentation" class="active"><a href="/PurchaseOrder/PurchaseOrderListing" >Purchase Orders</a></li>
                            <li role="presentation"><a href="/LostAndFound/LostAndFound">Lost & Found Manager</a></li>
                            <li role="presentation"><a href="/MileageLog/MileageLog">Mileage Log</a></li>
                            <li role="presentation"><a href="/Inspection/Inspection">Inspections</a></li>
                            <li role="presentation"><a href="/Maintenance/Inventory">Inventory Tracker</a></li>

                        </ul>
                    </div>
                    <div class="add_mainenance_section">
                        <div class="content-section">
                            <form id="purchaseOrderForm" method="POST" enctype="multipart/form-data">
                                <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>New Purchase Order <a class="back" href="/PurchaseOrder/PurchaseOrderListing"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                </div>

                                <div class="form-data">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>PO Number <em class="red-star">*</em></label>
                                            <input class="purchaseId" type="hidden" name="purchaseId"/>
                                            <input class="form-control" type="text" name="po_number"/>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Created By</label>
                                            <select class="form-control" name="created_by">
                                                <option>Select</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Current Date</label>
                                            <input class="form-control calander" type="text"  name="current_date" readonly/>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Require By Date</label>
                                            <input class="form-control calander" type="text"  name="required_by"/>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2 clear">
                                            <label>Select Property</label>
                                            <select class="form-control" name="property">
                                                <option value="0">Select</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Select Building</label>
                                            <select class="form-control" name="building">
                                                <option value="0">Select</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Select Unit</label>
                                            <select class="form-control" name="unit">
                                                <option value="0">Select</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2 clear">
                                            <label>Work Order</label>
                                            <select class="form-control" name="work_order">
                                                <option value="0">Select</option>
                                                <option value="1">s2d1f2</option>
                                                <option value="2">we455i</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Approvers</label>
                                            <div class="check-outer">
                                                <input type="checkbox" name="owner_approve" value="1"/> Owner
                                            </div>
                                            <div class="check-outer  col-sm-2">
                                                <input type="checkbox" name="tenant_approve" value="2"/> Tenant
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2 tenants_block">
                                            <label>Tenant</label>
                                            <select class="form-control" name="tenants">
                                                <option value="0">Select</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2 owners_block">
                                            <label>Owner</label>
                                            <select class="form-control" name="owners">
                                                <option value="0">Select</option>
                                            </select>
                                        </div>
                                        <div class="vendor-list-outer">
                                            <div id="collapseOne" class="panel-collapse collapse vendor_list_container">
                                                <div class="grid-outer">
                                                    <div class="table-responsive" id="vendor_list_search">
                                                        <table class="table table-hover table-dark" id="vendorlist">
                                                            <thead>
                                                            <tr class="header">
                                                                <th style="display: none;"></th>
                                                                <th>Vendor Name</th>
                                                                <th>Address</th>
                                                                <th>Email</th>
                                                                <th>Vendor Type</th>
                                                                <th>Phone Number</th>
                                                                <th style="display: none;"></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody></tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-2 clear">
                                                <label>Invoice Number</label>
                                                <input class="form-control" type="text" name="invoice_number" />
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                <label>Vendor</label>
                                                <input class="form-control capital" type="text" name="vendor" id="vendor"/>
                                                <input type="hidden" name="vendorid"/>

                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                <label>Address</label>
                                                <textarea class="form-control capital" name="address"></textarea>
                                            </div>
                                        </div>


                                        <div class="col-sm-12">
                                            <div class="grid-outer">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-dark vendor_selected_table">
                                                        <thead>
                                                        <tr>
                                                            <th style="display: none"></th>
                                                            <th scope="col">Name</th>
                                                            <th scope="col">Category</th>
                                                            <th scope="col">Email</th>
                                                            <th scope="col">Phone Number</th>
                                                            <th scope="col" style="display: none;"></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="grey-add-table">
                                                <div class="table-responsive">
                                                    <table class="table table-dark" id="amnt_table">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">Number</th>
                                                            <th scope="col">GL Account</th>
                                                            <th scope="col">Description</th>
                                                            <th scope="col">Item Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</th>
                                                            <th scope="col">Total (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</th>
                                                            <th><input type="button" class="blue-btn add_more_list" value="Add More"></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><input class="form-control" type="text" name="qty_number[]" value="1"/></td>
                                                            <td><select class="form-control" name="gl_account[]"><option value="0">Select</option></select></td>
                                                            <td><input class="form-control capital" type="text" name="description[]"/></td>
                                                            <td><input class="form-control" type="text" name="item_amount[]" value="0.00"/></td>
                                                            <td><input class="form-control" type="text" name="total_amount[]" value="0.00"/></td>
                                                            <td>
                                                                <a class="minus-icon remove_clone" href="javascript:void(0);" style="color:#000 !important;"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6"></div>
                                                        <div class="col-sm-6" style="padding-left: 0px;">
                                                            <div class="col-sm-5 totallabel">Total (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>):</div>
                                                            <div class="col-sm-6 totalamt">0.00</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                                <!-- Form Outer Ends -->

                                <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Vendor Instructions</h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-3 clear">
                                            <textarea class="form-control capital" name="vendor_instruction"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                                <!-- Form Outer Ends -->
                            <div class="form-outer">
                                <div class="files_main">
                                        <div class="form-outer">
                                            <input type="hidden" id="purchaseIdEdit"  value="<?php echo $_GET['PurchaseOrderId'];?>" />
                                            <div class="form-hdr"><h3>File Library</h3></div>
                                            <div class="form-data">
                                                <div class="apx-table">
                                                    <div id="collapseSeventeen" class="panel-collapse in">
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-5 min-height-0 file_main">
                                                                    <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                                    <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple="multiple" style="display: none;">
                                                                    <input type="button" class="orange-btn" id="remove_library_file" value="Remove All Files...">
                                                                    <!--<input type="button" class="saveChargeFile blue-btn" value="Save">
                                                                    <input type="button" class="cancelbtn grey-btn" value="Cancel">-->
                                                                </div>
                                                                <div class="row" id="file_library_uploads"></div>
                                                            </div>
                                                        </div>
                                                        <div class="grid-outer">
                                                            <div class="apx-table">
                                                                <div class="table-responsive">
                                                                    <table id="TenantFiles-table" class="table table-bordered"></table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   <!-- <div class="table-responsive">
                                                        <table id="TenantFiles-table" class="table table-bordered"></table>
                                                    </div>-->
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <table class="table table-hover table-dark" id="file_library_table"></table>
                                            </div>
                                        </div>

                                    </div>
                            </div>
                                <!-- Form Outer Ends -->

                            <div class="form-outer">
                                <div class="form-data">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Custom Fields</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row ">
                                                <div class="form-outer col-sm-6 col-md-6 ">
                                                    <div class="custom_field_html"></div>
                                                    <div class="clearfix "></div>
                                                </div>
                                            </div>
                                            <button type="button" id="add_custom_field" class="pull-right blue-btn" data-toggle="modal"
                                                    data-backdrop="static" data-target="#myModal" class="blue-btn">Add
                                                Custom Field
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-outer text-right">
                                    <a href='javascript:void(0);' class="blue-btn update_purchase">Update</a>
                                    <input type="button" class="grey-btn resetClearFeildData" value="Reset">
                                    <a href='javascript:void(0);' class="grey-btn cancel_purchase">Cancel</a>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
    <div class="container">
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Custom Field</h4>
                    </div>
                    <div class="modal-body" style="height: 380px;">
                        <div class="form-outer col-sm-12">
                            <form id="custom_field">
                                <input type="hidden" name="id" id="custom_field_id" value="">
                                <div class="row custom_field_form">
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Field Name <em class="red-star">*</em></label>
                                        </div>
                                        <div class="col-sm-9 field_name">
                                            <input class="form-control" type="text" maxlength="100" id="field_name"
                                                   name="field_name" placeholder="">
                                            <span class="required error"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Data Type</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="form-control data_type" id="data_type" name="data_type">
                                                <option value="text">Text</option>
                                                <option value="number">Number</option>
                                                <option value="currency">Currency</option>
                                                <option value="percentage">Percentage</option>
                                                <option value="url">URL</option>
                                                <option value="date">Date</option>
                                                <option value="memo">Memo</option>
                                            </select>
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Default value</label>
                                        </div>
                                        <div class="col-sm-9 default_value">
                                            <input class="form-control default_value" id="default_value" type="text"
                                                   name="default_value" placeholder="">
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Required Field</label>
                                        </div>
                                        <div class="col-sm-9 is_required">
                                            <select class="form-control" name="is_required" id="is_required">
                                                <option value="1">Yes</option>
                                                <option value="0" selected="selected">No</option>
                                            </select>
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-outer text-right">
                                    <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                    <button type="button" class="clear-btn ClearCustomForm">Clear</button>
                                    <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    $(document).ready(function () {
        $(document).on('click','.resetClearFeildData',function () {
            bootbox.confirm("Do you want to reset this action now?", function (result) {
                if (result == true) {
                    window.location.reload();
                }
            });
        });

        $(document).on('click','.ClearCustomForm',function () {
            bootbox.confirm("Do you want to clear this form?", function (result) {
                if (result == true) {
                    $('#custom_field')[0].reset();
                }
            });
        });
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/maintenance/purchase_orders/editpurchase_orders.js"></script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>