<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Maintenance &gt;&gt; <span>Inspections</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>MAINTENANCE</h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="/WorkOrder/AddWorkOrder" class="list-group-item list-group-item-success strong collapsed" >New Work Order</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="" href="/WorkOrder/AddWorkOrder" class="list-group-item list-group-item-success strong collapsed" >New Recurring Work Order</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="/WorkOrder/WorkOrders" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">View All Work Orders</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="/ticket/tickets" id="" class="list-group-item list-group-item-success strong collapsed">View All Tickets</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="/Inspection/Inspection" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">New Inspection</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="/LostAndFound/LostAndFound" class="list-group-item list-group-item-success strong collapsed" >Lost & Found Manager</a>
                                <!-- Seven Ends-->
                                <!-- Seven Starts-->
                                <a id="" href="/MileageLog/MileageLog" class="list-group-item list-group-item-success strong collapsed" >Mileage Log</a>
                                <!-- Seven Ends-->
                                <!-- Seven Starts-->
                                <a id="" href="/Maintenance/Inventory" class="list-group-item list-group-item-success strong collapsed" >Inventory Tracker</a>
                                <!-- Seven Ends-->
                                <!-- Seven Starts-->
                                <a id="" href="/PurchaseOrder/PurchaseOrder" class="list-group-item list-group-item-success strong collapsed" >New Purchase Order</a>
                                <!-- Seven Ends-->
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/ticket/tickets">Tickets</a></li>
                            <li role="presentation"><a href="/WorkOrder/WorkOrders">Work Orders</a></li>
                            <li role="presentation"><a href="/PurchaseOrder/PurchaseOrderListing" >Purchase Orders</a></li>
                            <li role="presentation"><a href="/LostAndFound/LostAndFound">Lost & Found Manager</a></li>
                            <li role="presentation"><a href="/MileageLog/MileageLog">Mileage Log</a></li>
                            <li role="presentation" class="active"><a href="/Inspection/Inspection">Inspections</a></li>
                            <li role="presentation"><a href="/Maintenance/Inventory">Inventory Tracker</a></li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">


                            <div role="tabpanel" class="tab-pane active" id="tickets-inspection">
                                <div class="property-status">
                                    <button class="blue-btn pull-right" data-toggle="modal" data-target="#selectNewPopup" aria-hidden="true">New Inspection</button>
                                </div>
                                <div class="panel panel-default clear">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion">Inspection</a>
                                        </h4>
                                    </div>
                                    <div>
                                        <div class="panel-body">
                                            <div class="form-outer">
                                                <div class="property-status" id="filteringdata">
                                                    <input type="hidden" id="property_id">
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-3">
                                                            <label>Property</label>
                                                            <select id="property_list_inspection" name="property_list_inspection" class="fm-txt form-control property_list_inspections">
                                                                <option value="all">Select</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-6 col-md-3">
                                                            <label>Building</label>
                                                            <select id="building_list_inspection" name="building_list_inspection" class="fm-txt form-control building_list_inspections">
                                                                <option value="all">This Property has no Building</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-6 col-md-3">
                                                            <label>Unit</label>
                                                            <select id="unit_list_inspection" name="unit_list_inspection" class="fm-txt form-control unit_list_inspections">
                                                                <option value="all">This Building has no Unit</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="accordion-grid">
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div class="panel panel-default">
                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                        <div class="panel-body pad-none">
                                                                            <div class="grid-outer">
                                                                                <div class="apx-table">
                                                                                    <div class="table-responsive">
                                                                                        <table id="inspection-table" class="table table-bordered"></table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <!-- Sixth Tab Ends-->


                        </div>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>


    <!-- start custom field model -->
    <div class="container">
        <div class="modal fade" id="selectNewPopup" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">New Inspection</h4>
                    </div>

                    <form id="addNewInspection">
                    <div class="modal-body" style="height: 200px;">
                        <div class="row">
                            <div class="row">
                                <input type="hidden" id="property_id">
                                <div class="col-sm-12">
                                    <label class="col-sm-4"><input id = "inspProperty" type="radio" name="inspectionRadio" value="" checked="checked"/> Property</label>
                                    <label class="col-sm-4"><input id = "inspBuilding" type="radio" name="inspectionRadio" value=""> Building</label>
                                    <label class="col-sm-4"><input id = "inspUnit" type="radio" name="inspectionRadio" value=""> Unit</label>
                                </div>

                                <div class="col-sm-12 select-padd">
                                    <div class="col-sm-4 red box">
                                        <label>Property<em class="red-star">*</em></label>
                                        <select id="property_list_inspection" name="property_list_inspection" class="fm-txt form-control">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4 green box" style="display: none;">
                                        <label>Building<em class="red-star">*</em></label>
                                        <select id="building_list_inspection" name="building_list_inspection" class="fm-txt form-control">
                                            <option value="">This Property has no Building</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4 blue box" style="display: none;">
                                        <label>Unit<em class="red-star">*</em></label>
                                        <select id="unit_list_inspection" name="unit_list_inspection" class="fm-txt form-control">
                                            <option value="">This Building has no Unit</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <input type="button" class="blue-btn" id='saveCustomField' value="Select">
                            </div>
                        </div>

                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End custom field model -->


</div>
<!-- Wrapper Ends -->


<!-- Footer Ends -->
<script type="text/javascript">
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $(document).ready(function(){
        $('#selectNewPopup #inspProperty').click(function(){
            //alert("red");
            $(".red").css("display","block");
            $(".green").css("display","none");
            $(".blue").css("display","none");
        });

        $('#selectNewPopup #inspBuilding').click(function(){
            //alert("green");
            $(".red").css("display","block");
            $(".green").css("display","block");
            $(".blue").css("display","none");
        });

        $('#selectNewPopup #inspUnit').click(function(){
            //alert("blue");
            $(".red").css("display","block");
            $(".green").css("display","block");
            $(".blue").css("display","block");
        });
    });
</script>
<script>

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
        $('#maintenance_top').addClass('active');
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
</script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/maintenance/inspectionListing.js"></script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>