<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/constants.php");
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}

$view_id = (isset($_REQUEST['id']))?$_REQUEST['id']:'';
?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<!-- HTML Start -->



<div id="wrapper">
    <main class="apxpg-main">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <section class="main-content">

            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent" id="viewInspectionList">
                        <input type="hidden" name="inspection_uniqueId" id="inspection_uniqueId" value="<?php echo $_GET["id"]; ?>"/>
                        <div class="content-section">
                            <!--Property Information-->
                            <div class="apx-adformbox">
                                <div class="apx-adformbox-title">
                                    <strong class="left">View Inspection</strong>
                                    <a class="back back-inspection-btn" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                </div>
                                <div class="apx-adformbox-content" >
                                    <div class="row">
                                        <div class="view-outer">
                                            <div class="col-xs-12 col-sm-6">
                                                <label class="blue-label">Date :</label>
                                                <span id="DateOfInspection"></span>
                                            </div>

                                            <div class="col-xs-12 col-sm-6">
                                                <label class="blue-label">Property Address :</label>
                                                <span id="propertyAddressInspection"></span>
                                            </div>


                                            <div class="col-xs-12 col-sm-6">
                                                <label class="blue-label">Inspection Name :</label>
                                                <span id="nameInspection"></span>
                                            </div>

                                            <div class="col-xs-12 col-sm-6">
                                                <label class="blue-label">Inspection Type :</label>
                                                <span id="TypeInspection"></span>
                                            </div>

                                            <div class="col-xs-12 col-sm-6">
                                                <label class="blue-label">Property Name :</label>
                                                <span id="propertyNameInspection"></span>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <label class="blue-label">Building :</label>
                                                <span id="buildingInspection"></span>
                                            </div>

                                            <div class="col-xs-12 col-sm-6">
                                                <label class="blue-label">Number Of Units :</label>
                                                <span id="numberOfUnitInspection"></span>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="edit-outer edit-foot"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Renovation Detail-->

                                </div>
                            </div>

                            <!--Notes-->
                            <div class="apx-adformbox">
                                <div class="apx-adformbox-title">
                                    <strong class="left">Inspection Area</strong>
                                </div>
                                <div class="apx-adformbox-content">
                                    <!-- Notes table-->
                                    <div class="row">
                                        <div class="view-outer">
                                            <div class="col-xs-12 col-sm-6">
                                                <label class="blue-label"></label>
                                                <span id="property_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="edit-outer edit-foot"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Notes-->
                            <div class="apx-adformbox">
                                <div class="apx-adformbox-title">
                                    <strong class="left">Notes</strong>
                                </div>
                                <div class="apx-adformbox-content">
                                    <!-- Notes table-->
                                    <div class="row">
                                        <div class="view-outer">
                                            <div class="col-xs-12 col-sm-6">
                                                <label class="blue-label">Notes :</label>
                                                <span id="notesInspection"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="edit-outer edit-foot"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--File Library-->
                            <div class="apx-adformbox">
                                <div class="apx-adformbox-title">
                                    <strong class="left">Photos</strong>
                                </div>
                                <div class="apx-adformbox-content">
                                    <!-- File Library table-->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="apx-table">
                                                <div class="table-responsive">
                                                    <table id="inspectionPhotos-table" class="table table-bordered">
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="edit-outer edit-foot"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Custom Fields-->
                            <div class="apx-adformbox">
                                <div class="apx-adformbox-title">
                                    <strong class="left">Custom Fields</strong>
                                </div>
                                <div class="apx-adformbox-content">
                                    <!-- Custom Fields-->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="custom_field_html_view_mode">
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="edit-outer edit-foot"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!--tab Ends -->
                </div>
            </div>
        </section>
    </main>
</div>
<!-- Wrapper Ends -->
<!-- Footer Starts -->

<!-- Footer Ends -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var inspection_unique_id = getParameterByName('id');
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
   // var default_name = "<?php echo $_SESSION[SESSION_DOMAIN]['default_name']; ?>";

    /**
     * function to get id from url
     * @param name
     * @returns {string}
     */
    function getParameterByName(name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }
    var upload_url = "<?php echo SITE_URL; ?>";
</script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/maintenance/inspectionListing.js"></script>

<script>
    $(function () {
        $('.nav-tabs').responsiveTabs();
    });

    $("#show").click(function () {
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function () {
        $("#bs-example-navbar-collapse-2").hide();
    });

    $('.company-top').addClass('active');

    $(document).ready(function () {
        $(".slide-toggle").click(function () {
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function () {
        $(".slide-toggle2").click(function () {
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });

        $(document).on('click','.back-inspection-btn',function(){
            window.location.href = '/Inspection/Inspection';
        });
    });

</script>


<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>