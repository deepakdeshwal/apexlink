<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Maintenance &gt;&gt; <span>Tickets</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>MAINTENANCE</h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" >New Work Order</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >New Recurring Work Order</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">View All Work Orders</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="#" id="" class="list-group-item list-group-item-success strong collapsed">View All Tickets</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">New Inspection</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Lost & Found Manager</a>
                                <!-- Seven Ends-->
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/ticket/tickets">Tickets</a></li>
                            <li role="presentation"><a href="/WorkOrder/WorkOrders">Work Orders</a></li>
                            <li role="presentation"><a href="/PurchaseOrder/PurchaseOrderListing" >Purchase Orders</a></li>
                            <li role="presentation"><a href="/LostAndFound/LostAndFound">Lost & Found Manager</a></li>
                            <li role="presentation"><a href="/MileageLog/MileageLog">Mileage Log</a></li>
                            <li role="presentation"><a href="/Inspection/Inspection">Inspections</a></li>
                            <li role="presentation" class="active"><a href="/Maintenance/Inventory">Inventory Tracker</a></li>


                        </ul>
                      
                        <!-- Tab panes -->
                        <div class="tab-content">


                            <div role="tabpanel" class="tab-pane active" id="tickets-tracker">

                                <div class="sub-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#ticket-inventory" aria-controls="home" role="tab" data-toggle="tab">Add Item to Inventory</a></li>
                                        <li role="presentation"><a href="/Maintenance/InventorySetup" aria-controls="profile" role="tab" data-toggle="tab">Inventory Setup</a></li>

                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="ticket-inventory">

                                            <div class="panel panel-default clear">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion">New Inventory</a>
                                                    </h4>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">

                                                        <div class="form-outer">
                                                                                 <div class="col-sm-12">
                                       
                                            <div class="row">
                                                <div class="form-outer contactTenant" style="display:none">
                                                    <div class="col-sm-3 col-md-3">
                                                        <div class=''>
                                                            <strong class='font-weight-bold'>Contact for this tenant <em class="red-star">*</em></strong>
                                                            <input class="form-control capital" type="text" name='contactTenant'>
                                                        </div>
                                                        <span class="ffirst_nameErr error red-star"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="upload-logo clearfix">
                                                <div class="tenant_image img-outer"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/dummy-img.jpg"></div>

                                                <a href="javascript:;">
                                                    <i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Image</a>
                                                <span>(Maximum File Size Limit: 1MB)</span>
                                            </div>
                                            <div class="image-editor">
                                                <input type="file" class="cropit-image-input" name="tenant_image">
                                                <div class='cropItData' style="display: none;">
                                                    <span class="closeimagepopupicon">X</span>
                                                    <div class="cropit-preview"></div>
                                                    <div class="image-size-label">Resize image</div>
                                                    <input type="range" class="cropit-image-zoom-input">
                                                    <input type="hidden" name="image-data" class="hidden-image-data"/>
                                                    <input type="button" class="export" value="Done"
                                                           data-val="tenant_image">
                                                </div>
                                            </div>
                                        </div>
                                                            <div class="col-sm-2">
                                                                <label>Category <em class="red-star">*</em> <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select class="form-control"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Subcategory <em class="red-star">*</em> <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select class="form-control"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Brand <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select class="form-control"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Supplier <em class="red-star">*</em> <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select class="form-control"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Description</label>
                                                                <textarea class="form-control"></textarea>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Number of Item Purchased <em class="red-star">*</em> </label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Size</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Volume<a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select class="form-control"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Cost per Item (US$)</label>
                                                                <input class="form-control add-input" type="text">
                                                               
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Purchase Cost (US$)</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Purchase Date</label>
                                                                <input class="form-control add-input" type="text">
                                                                <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            </div>

                                                            <div class="col-sm-2">
                                                                <label>Expected Delivery Date</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Stock Reorder Level</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label>Property <em class="red-star">*</em></label>
                                                                <select class="form-control"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Building <em class="red-star">*</em></label>
                                                                <select class="form-control"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Sublocation <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select class="form-control"></select>
                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion">Custom Fields</a>
                                                            </h4>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-2">
                                                                        <label>No Custom Fields</label>
                                                                    </div>
                                                                    <div class="col-sm-2 text-right pull-right">
                                                                        <label></label>
                                                                        <button class="blue-btn">New Custom Field</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="heading-above-btn">
                                                            <div class="btn-outer">
                                                                <input type="submit" name="submit" class="blue-btn" value="Save & Close">
                                                                <input type="submit" name="submit" class="blue-btn" value="Save & Add Another Field">
                                                                <a href="javascript:void(0)" class="grey-btn cancel_invenentory">Cancel</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Inventory on Hand</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Property</th>
                                                                                                <th scope="col">Building</th>
                                                                                                <th scope="col">Inventory Sublocation</th>
                                                                                                <th scope="col">Category</th>
                                                                                                <th scope="col">Subcategory</th>
                                                                                                <th scope="col">Quantity</th>
                                                                                                <th scope="col">Actions</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td>Yellow Stone</td>
                                                                                                <td>Yellow Stone</td>
                                                                                                <td>First Floor</td>
                                                                                                <td>General Supplies</td>
                                                                                                <td>Fire Extinguisher</td>
                                                                                                <td>6</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>Yellow Stone</td>
                                                                                                <td>Yellow Stone</td>
                                                                                                <td>First Floor</td>
                                                                                                <td>General Supplies</td>
                                                                                                <td>Fire Extinguisher</td>
                                                                                                <td>6</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>Yellow Stone</td>
                                                                                                <td>Yellow Stone</td>
                                                                                                <td>First Floor</td>
                                                                                                <td>General Supplies</td>
                                                                                                <td>Fire Extinguisher</td>
                                                                                                <td>6</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>

                            
                                    </div>
                                </div>

                            </div>
                            <!-- Seventh Tab Ends-->
                        </div>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>

</div>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<!-- Wrapper Ends -->



<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



</script>




<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>