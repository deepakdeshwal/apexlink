<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->

        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Maintenance &gt;&gt; <span>Work Order</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="listing_id">
                        <div class="content-data">
                            <!--- Right Quick Links ---->
                            <div class="right-links-outer hide-links">
                                <div class="right-links">
                                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                                <div id="RightMenu" class="box2">
                                    <h2>MAINTENANCE</h2>
                                    <div class="list-group panel">
                                        <!-- Two Ends-->
                                        <a href="/WorkOrder/AddWorkOrder" class="list-group-item list-group-item-success strong collapsed" >New Work Order</a>
                                        <!-- Two Ends-->

                                        <!-- Three Starts-->
                                        <a id="" href="/WorkOrder/AddWorkOrder" class="list-group-item list-group-item-success strong collapsed" >New Recurring Work Order</a>
                                        <!-- Three Ends-->

                                        <!-- Four Starts-->
                                        <a href="/WorkOrder/WorkOrders" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">View All Work Orders</a>
                                        <!-- Four Ends-->

                                        <!-- Five Starts-->
                                        <a href="/ticket/tickets" id="" class="list-group-item list-group-item-success strong collapsed">View All Tickets</a>
                                        <!-- Five Ends-->

                                        <!-- Six Starts-->
                                        <a href="/Inspection/Inspection" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">New Inspection</a>
                                        <!-- Six Ends-->

                                        <!-- Seven Starts-->
                                        <a id="" href="/LostAndFound/LostAndFound" class="list-group-item list-group-item-success strong collapsed" >Lost & Found Manager</a>
                                        <!-- Seven Ends-->
                                        <!-- Seven Starts-->
                                        <a id="" href="/MileageLog/MileageLog" class="list-group-item list-group-item-success strong collapsed" >Mileage Log</a>
                                        <!-- Seven Ends-->
                                        <!-- Seven Starts-->
                                        <a id="" href="/Maintenance/Inventory" class="list-group-item list-group-item-success strong collapsed" >Inventory Tracker</a>
                                        <!-- Seven Ends-->
                                        <!-- Seven Starts-->
                                        <a id="" href="/PurchaseOrder/PurchaseOrder" class="list-group-item list-group-item-success strong collapsed" >New Purchase Order</a>
                                        <!-- Seven Ends-->
                                    </div>
                                </div>
                            </div>
                            <!--- Right Quick Links ---->
                            <!--Tabs Starts -->
                            <div class="main-tabs">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a href="/ticket/tickets">Tickets</a></li>
                                    <li role="presentation" class="active"><a href="/WorkOrder/WorkOrders">Work Orders</a></li>
                                    <li role="presentation"><a href="/PurchaseOrder/PurchaseOrderListing" >Purchase Orders</a></li>
                                    <li role="presentation"><a href="/LostAndFound/LostAndFound">Lost & Found Manager</a></li>
                                    <li role="presentation"><a href="/MileageLog/MileageLog">Mileage Log</a></li>
                                    <li role="presentation"><a href="/Inspection/Inspection">Inspections</a></li>
                                    <li role="presentation"><a href="/Maintenance/Inventory">Inventory Tracker</a></li>

                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">

                                    <div role="tabpanel" class="tab-pane active" id="tickets-work-order">
                                        <div class="property-status">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="check-outer">
                                                        <input name="alt" type="radio" class="common_radio" id="one_time" value="0" checked>
                                                        <label>One Time</label>
                                                    </div>
                                                    <div class="check-outer" >
                                                        <input name="alt" type="radio" class="common_radio" id="recurring" value="1">
                                                        <label>Recurring</label>
                                                    </div>
                                                    <div class="col-sm-4" id="filter_div" style="display: none">
                                                        <select id="ddlWorkOrderListingFrequency" class="common_radio form-control select-dd">
                                                            <option value="all">Select</option>
                                                            <option value="Daily">Daily</option>
                                                            <option value="Weekly">Weekly</option>
                                                            <option value="Bi-Weekly">Bi-Weekly</option>
                                                            <option value="Monthly">Monthly</option>
                                                            <option value="Quarterly">Quarterly</option>
                                                            <option value="Semi-Annually">Semi-Annually</option>
                                                            <option value="Annually">Annually</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="btn-outer text-right">
                                                        <a href="/WorkOrder/AddWorkOrder" class="blue-btn">New Work Order</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body pad-none">
                                                                    <div class="apx-table">
                                                                        <div class="grid-outer">
                                                                            <div class="table-responsive">
                                                                                <table id="workorder_table">
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                        <!-- Sub tabs ends-->

                                    </div>

                                </div>
                            </div>
                            <!--Tabs Ends -->

                        </div>
                    </div>



                    <div class="content-data" id="view_id" style="display: none">
                        <div class="row">

                            <input type="hidden" id="workorder_id" val=""/>


                            <div class="main-tabs">


                                <div class="tab-content">


                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3> Work Order <a class="back" href="/WorkOrder/WorkOrders"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="detail-outer">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Work Order No :</label>
                                                            <span class="work_order_number">N/A</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Created On :</label>
                                                            <span class="created_on">N/A</span>
                                                        </div>

                                                        <div class="col-xs-12">
                                                            <label class="text-right">Status :</label>
                                                            <span class="work_status">N/A</span>
                                                        </div>

                                                        <div class="col-xs-12">
                                                            <label class="text-right">Status Reason :</label>
                                                            <span class="work_status">N/A</span>
                                                        </div>

                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Work Order Recurrence :</label>
                                                            <span class="work_order_type">N/A</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Work Order Category :</label>
                                                            <span class="work_category">N/A</span>
                                                        </div>

                                                        <div class="col-xs-12">
                                                            <label class="text-right">Priority :</label>
                                                            <span class="priority_type">N/A</span>
                                                        </div>

                                                    </div>


                                                </div>
                                            </div>

                                            <div class="edit-foot">
                                                <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                    <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>

                                    </div>



                                    <div class="form-outer form-outer2">
                                        <div class="form-data">
                                            <div class="form-hdr">
                                                <h3> Property Information </h3>
                                            </div>
                                            <div class="">
                                                <table class="table unittable"  id="unit_table" ></table>
                                            </div>
                                            <div class="edit-foot">
                                                <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                    <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-outer form-outer2">
                                    <div class="form-hdr">
                                        <h3>  General</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="detail-outer">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Source :</label>
                                                        <span class="work_source">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Requested By :</label>
                                                        <span class="request_id">N/A</span>
                                                    </div>

                                                    <div class="col-xs-12">
                                                        <label class="text-right">Estimated Cost :</label>
                                                        <span class="estimated_cost">N/A</span>
                                                    </div>

                                                    <div class="col-xs-12">
                                                        <label class="text-right">Scheduled On :</label>
                                                        <span class="scheduled_on">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Completed On  :</label>
                                                        <span class="completed_on">N/A</span>
                                                    </div>



                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Assigned To :</label>
                                                        <span class="gender">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Authority to Enter Unit :</label>
                                                        <span class="authority">N/A</span>
                                                    </div>

                                                    <div class="col-xs-12">
                                                        <label class="text-right">Authorized Time :</label>
                                                        <span class="phone_number">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Recipient  :</label>
                                                        <span class="recipient">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Description :</label>
                                                        <span class="work_order_description">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Required Materials :</label>
                                                        <span class="required_materials">N/A</span>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>

                                </div>

                            </div>


                        </div>



                        <div class="form-outer form-outer2">
                            <div class="form-data">
                                <div class="form-hdr">
                                    <h3> Tenant Information </h3>
                                </div>
                                <div class="">
                                    <table class="table" id="tenant_table"></table>
                                </div>
                                <div class="edit-foot">
                                    <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                        <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                        Edit
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="form-outer form-outer2">
                            <div class="form-data">
                                <div class="form-hdr">
                                    <h3>   Vendor Information</h3>
                                </div>
                                <div class="">
                                    <table class="table" id="vendor_table"></table>
                                </div>
                                <div class="edit-foot">
                                    <a href="javascript:;" class="edit_redirection" redirection_data="fileLibraryIfoDiv">
                                        <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                        Edit
                                    </a>
                                </div>
                            </div>
                        </div>



                        <!-- Form Outer starts -->
                        <div class="form-outer">
                            <div class="form-hdr">
                                <h3>Notes </h3>
                            </div>
                            <div class="form-data">

                                <div id="notes">
                                    <textarea rows="6" cols="80" class="note" readonly></textarea>
                                </div>
                                <div class="edit-foot">
                                    <a href="javascript:;" class="edit_redirection" redirection_data="fileLibraryIfoDiv">
                                        <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                        Edit
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Form Outer Ends -->

                        <!-- Form Outer Ends -->
                        <div class="form-outer" id="filelibrary">
                            <div class="form-hdr">
                                <h3>File Library</h3>
                            </div>
                            <div class="form-data">
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                    <div class="grid-outer">
                                                        <div class="apx-table">
                                                            <div class="table-responsive">
                                                                <table id="workorder_files" class="table table-bordered">
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="edit-foot">
                                    <a href="javascript:;" class="edit_redirection" redirection_data="fileLibraryIfoDiv">
                                        <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                        Edit
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Form Outer Ends -->
                        <!-- Form Outer starts -->
                        <div class="form-outer">
                            <div class="form-hdr">
                                <h3>Custom Fields</h3>
                            </div>
                            <div class="form-data">
                                <input type="hidden" id="customFieldModule" name="module" value="owner">
                                <div class="col-sm-7 custom_field_html_view_mode">
                                    No Custom Fields
                                </div>


                                <div class="edit-foot">
                                    <a href="javascript:;" class="edit_redirection" redirection_data="customFieldsInfoDiv">
                                        <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                        Edit
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Form Outer Ends -->




                    </div>
                    <!--tab Ends -->

                </div>


            </div>






    </div>
    </div>
    </section>

    </div>
    <!-- Wrapper Ends -->



    <!-- Footer Ends -->

    <script>
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });
        });

        $(document).ready(function(){
            $("#maintenance_top").addClass("active");
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
        });

        var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";

    </script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/maintenance/work-order/workorderlisting.js"></script>

    <!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>