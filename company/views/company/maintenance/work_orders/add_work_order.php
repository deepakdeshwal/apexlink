﻿<style>
    .panel-htop .combo-panel {
        margin-top: 9px !important;
        margin-left: 0px !important;
    }
    #FormWorkOrder .add-icon {
        position: absolute;
    }
</style>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
$satulation_array =['1'=>'Dr.','2'=>'Mr.','3'=>'Mrs.','4'=>'Mr. & Mrs.','5'=>'Ms.',6=>'Sir',7=>'Madam',8=>'Sister',9=>'Mother'];
$gender_array =['1'=>'Male','2'=>'Female','3'=>'Prefer Not To Say','4'=>'Other'];
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
    <div id="wrapper">
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
         <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Maintenance >> <span>Work Order</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text" />
                            </div>
                        </div>
                        </div>
                        
                    </div>
                    <div class="col-sm-12">

                    </div>
                </div>
                <?php
                if(isset($_GET['id']) && $_GET['id'] != ""){
                    $paramId = $_GET['id'];
                }else{
                    $paramId = 0;
                }
                ?>
                <input type='hidden' name='id_from_other' id="id_from_other" value="<?php echo $paramId; ?>">

                    <div class="col-sm-12">
                        <form id="FormWorkOrder" method="post">
                        <div class="content-section">
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>New Work Order <a class="back back_redirection" href="/WorkOrder/WorkOrders"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="latefee-check-outer form-outer2">
                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                <div class="check-outer">
                                                    <input type="radio" name="work_order_type" class="work-order-duration" value="0" checked>
                                                    <label>One Time</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                <div class="check-outer">
                                                    <input type="radio" name="work_order_type" class="work-order-duration" value="1">
                                                    <label>Recurring</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                <div class="check-outer">
                                                    <select id="monthddl" name="rec_month" class="form-control"  style="display: none;"><option value="Daily">Daily</option><option value="Weekly">Weekly</option><option value="Bi-Weekly">Bi-Weekly</option><option value="Monthly">Monthly</option><option value="Quarterly">Quarterly</option><option value="Semi-Annually">Semi-Annually</option><option value="Annually">Annually</option></select>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Work Order Number</label>
                                            <input class="form-control" type="text"  id="work-order-randomno" name="work_order_number" readonly/>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Created On</label>
                                            <input class="form-control calander" type="text" id="workCreatedAt" name="created_on" readonly/>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Work Order Category <a class="pop-add-icon" href="javascript:;" id="add-work-cat"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                <select class="form-control ddlWorkOrderType" name="work_order_cat" id="ddlWorkOrderTypeID">

                                            </select>
                                            <div class="add-popup NewCategoryPopupCLass" id='NewCategoryPopup' style="width: 127%;">
                                                <h4>Add New Category</h4>
                                                <div class="add-popup-body">
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <label>Name<em class="red-star">*</em></label>
                                                            <input class="form-control customValidateCat capital" type="text" data_required="true" data_max="20"
                                                                   name='@category' id='category' placeholder="Eg: GEN"/>
                                                            <span class="customError required"></span>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <label>Description <em class="red-star">*</em></label>
                                                            <input class="form-control customValidateCat capital"
                                                                   type="text" data_required="true" data_max="50" name='@description' id='cat_desc' placeholder="Eg: General"/>
                                                            <span class="customError required"></span>
                                                        </div>
                                                        <div class="btn-outer text-right">
                                                            <input type="button" id='NewCatPopupSave' class="blue-btn addSingleData"
                                                                   value="Save" data-table='company_workorder_category' data-cell="category" data-cell2="description"
                                                                   data-value="category" data-value2="cat_desc" data-error="customValidateCat" data-select ="ddlWorkOrderType" data-hide="NewCategoryPopupCLass" />
                                                            <input type="button" class="clear-btn clearNewcategory" value='Clear' />
                                                            <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Priority <a onclick="clearPopUps('#NewPriorityPopup')" class="pop-add-icon" id="add-priority-cat" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                            <select class="form-control ddlPriorityType" name="priority_id" id="ddlPriorityTypeID"></select>
                                            <div class="add-popup NewPriorityPopupClass" id='NewPriorityPopup' style="width: 127%;">
                                                <h4>Add New Priority</h4>
                                                <div class="add-popup-body">
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <label>Add New Priority Name<em class="red-star">*</em></label>
                                                            <input class="form-control customValidatePriority capital" type="text" data_required="true"
                                                                   data_max="20" name='@priority' id='priority' placeholder="Eg: GEN"/>
                                                            <span class="customError required"></span>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <label>Description<em class="red-star">*</em></label>
                                                            <input class="form-control customValidatePriority capital" type="text" data_required="true" data_max="50" name='@description' id='prior_description' placeholder="Eg: General"/>
                                                            <span class="customError required"></span>
                                                        </div>
                                                        <div class="btn-outer text-right">
                                                            <input type="button" id='NewPriorityPopupSave' class="blue-btn addSingleData" value="Save"  data-table='company_priority_type'
                                                                   data-cell="priority" data-cell2="description"
                                                                   data-value="priority" data-value2="prior_description" data-error="customValidatePriority"
                                                                   data-select ="ddlPriorityType" data-hide="NewPriorityPopupClass"/>
                                                            <input type="button" class="clear-btn clearNewPriority" value='Clear' />
                                                            <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Portfolio <em class="red-star">*</em><a onclick="clearPopUps('#NewportfolioPopup')" class="pop-add-icon" id="Newportfolio" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                            <select class="form-control ddlPortfolioType" id="portfolio_name_options" name="portfolio_id"></select>
                                            <div class="add-popup NewportfolioPopupCLass" id='NewportfolioPopup' style="width: 127%;">
                                                <h4>Add New Portfolio</h4>
                                                <div class="add-popup-body">

                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <label>Portfolio ID<em class="red-star">*</em></label>
                                                            <input class="form-control customValidateport" type="text" data_required="true" data_max="20" name='@portfolio_id' id='portfolio_id'/>
                                                            <span class="customError required"></span>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <label> Portfolio Name <em class="red-star">*</em></label>
                                                            <input class="form-control customValidateport capital" type="text" data_required="true" data_max="50" name='@portfolio_name' id='portfolio_name' placeholder="Add New Portfolio Name"/>
                                                            <span class="customError required"></span>
                                                        </div>
                                                        <div class="btn-outer text-right">
                                                            <input type="button" id='NewportfolioPopupSave' class="blue-btn addSingleData" value="Save" data-table='company_property_portfolio'
                                                                   data-cell="portfolio_id" data-cell2="portfolio_name"
                                                                   data-value="portfolio_id" data-value2="portfolio_name" data-error="customValidateport" data-select ="ddlPortfolioType"
                                                                   data-hide="NewportfolioPopupCLass" />
                                                            <input type="button" class="clear-btn clearPortfolio" value='Clear' />
                                                            <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                            
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Property Information</h3>
                                </div>
                                <div class="form-data">
                                   <div class="row propertyInfoDiv" id="propertyInfoDivID">
                                        <div class="col-xs-12 col-sm-4 col-md-2" >
                                            <label>Select Property <em class="red-star">*</em></label>
                                            <input type="hidden" value="" class="portfoliodefault">
                                            <select class="form-control getallproperties" name="property_id"></select>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Select Building <em class="red-star">*</em></label>
                                            <select class="form-control getAllBuildings" name="building_id"></select>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Unit <em class="red-star">*</em></label>
                                            <select class="form-control getallunits" name="unit_id"></select>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Pet in Unit</label>
                                            <select class="form-control add-input petinUnits" name="pet_id">
                                                <option value=''>select</option>
                                          <option value=''>Yes</option>
                                           <option value=''>No</option>
                                         <option value=''>Other</option></select>
                                            <a class="add-icon property_info_plus" style="margin-top: 28px;" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            <a  class="add-icon property_info_minus" style="margin-top: 28px;display: none" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>General Information</h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Status <a class="pop-add-icon" id="add-work-status" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                            <select class="form-control NewWorkStatusPopupID" name="status_id"><option>Select</option></select>
                                            <div class="add-popup NewWorkStatusPopupCLass" id='NewWorkStatusPopup' style="width: 127%;">
                                                <h4>Add Status</h4>
                                                <div class="add-popup-body">
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <label>Status<em class="red-star">*</em></label>
                                                            <input class="form-control customWorkorderStatus capital" type="text" data_required="true" data_max="20" name='@work_order_status' id='work_order_status' placeholder="Status"/>
                                                            <span class="customError required"></span>
                                                        </div>

                                                        <div class="btn-outer text-right">
                                                            <input type="button" id='NewCatPopupSave' class="blue-btn addPopUpData" value="Save"  value="Save" data-table='company_workorder_status'
                                                                   data-cell="work_order_status" data-value="work_order_status" data-error="customWorkorderStatus" data-select ="NewWorkStatusPopupID"
                                                                   data-hide="NewWorkStatusPopupCLass" />
                                                            <input type="button" class="clear-btn clearStatus" value='Clear' />
                                                            <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label>Source <a class="pop-add-icon" id="add-work-source" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                        <select class="form-control ddlWorkOrderSource" name="source_id"><option>Select</option></select>
                                        <div class="add-popup NewWorkSourcePopupCLass" id='NewWorkSourcePopup' style="width: 127%;">
                                            <h4>Add Others</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>Source Name<em class="red-star">*</em></label>
                                                        <input class="form-control customWorkorderSource capital" type="text" data_required="true" data_max="20" name='@work_order_source' id='work_order_source' placeholder="Source Name"/>
                                                        <span class="customError required"></span>
                                                    </div>

                                                    <div class="btn-outer text-right">
                                                        <input type="button" class="blue-btn addPopUpData" value="Save" data-table='company_workorder_source'
                                                               data-cell="work_order_source"
                                                               data-value="work_order_source"  data-error="customWorkorderSource" data-select ="ddlWorkOrderSource" data-hide="NewWorkSourcePopupCLass" />
                                                        <input type="button" class="clear-btn ClearOther" value='Clear' />
                                                        <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="property-status">
                                        <div class="check-outer">
                                            <input type="checkbox" class="requestedbyCheckbox"/>
                                            <label>Check This Box If You Want To Use Requested By For This Order</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label>Requested By <a class="pop-add-icon" id="add-request-by" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                        <select disabled="" class="form-control ddlWorkOrderRequest" name="request_id"><option>Select</option></select>
                                        <div class="add-popup NewRequestByPopupClass" id='NewRequestByPopup' style="width: 127%;">
                                            <h4>Add others</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>Requested By <em class="red-star">*</em></label>
                                                        <input class="form-control customValidateRequest" type="text" data_required="true" data_max="20" name='@work_order_request' id='work_order_request' placeholder="Requested By "/>
                                                        <span class="customError required"></span>
                                                    </div>

                                                    <div class="btn-outer text-right">
                                                        <input type="button"  class="blue-btn addPopUpData" value="Save" data-table='company_workorder_requestedby' data-cell="work_order_request"
                                                               data-value="work_order_request"  data-error="customValidateRequest" data-select ="ddlWorkOrderRequest" data-hide="NewRequestByPopupClass" />
                                                        <input type="button" class="clear-btn ClearRequestedby" value='Clear' />
                                                        <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label>Estimated Cost <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>

                                        <input name="estimated_cost" type="text" maxlength="10" class="form-control" spellcheck="true" autocomplete="off" placeholder="Estimated Cost">
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-2 clear">
                                        <label>Scheduled On</label>
                                        <input class="form-control calander" type="text" readonly="readonly" id="workScheduledon" name="scheduled_on"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label>Completed On</label>
                                        <input class="form-control calander" type="text" readonly="readonly" id="workCompletedon" name="completed_on"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label>Authority to Enter Unit</label>

                                        <select class="form-control" id="ddlWorkOrderAuthority" name="authority">
                                            <option value="0">Select</option>
                                            <option value="2">Anytime</option>
                                            <option value="1">No Authority to Enter</option>
                                            <option value="3">Specified Time</option>
                                            <option value="4">Unknown</option>
                                            <option value="6">Knock First</option>
                                            <option value="10">Ring Door Bell </option>
                                            <option value="7">Use Front Door</option>
                                            <option value="8">Use Back Door</option>
                                            <option value="9">Use Basement Entrance</option>
                                            <option value="5">Other</option>
                                        </select>


                                    </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2" id="txtOther" style="display: none;">
                                            <label>Other</label>
                                            <input type="text"  placeholder="Specific Instructions" class="form-control" spellcheck="true" autocomplete="off">
                                        </div>

                                    <div class="col-xs-12 col-sm-4 col-md-2 clear">
                                        <label>Recipient</label>
                                        <input name="recipient" class="form-control capital" type="text" id="workCompletedon" placeholder="Recipient" maxlength="50"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4 textarea-form">
                                        <label>Description</label>
                                        <div class="notes_date_right_div ">
                                        <textarea class="form-control notes_date_right capital" placeholder="Description" name="work_order_description"></textarea>
                                        </div>
                                        </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <label>Required Materials</label>
                                        <textarea class="form-control capital" placeholder="Required Materials" name="required_materials"></textarea>
                                    </div>
                                    
                                </div>
                                 <div class="check-outer-bdr">
                                        <div class="check-outer">
                                            <input type="checkbox" name="approved_by_owner" class="approved_by_owner" checked/>
                                            <label>Approved by Owner</label>
                                        </div>
                                        <div class="check-outer">
                                            <input type="checkbox" name="publish_to_tenant" class="publish_to_tenant" checked/>
                                            <label>Publish to Tenant Portal</label>
                                        </div>
                                        <div class="check-outer">
                                            <input type="checkbox" name="send_alert" class="send_alert"/>
                                            <label>Send Tenant Alert</label>
                                        </div>
                                        <div class="check-outer">
                                            <input type="checkbox" name="publish_to_owner" class="publish_to_owner" checked/>
                                            <label>Publish to Owner Portal</label>
                                        </div>
                                        <div class="check-outer">
                                            <input type="checkbox" name="publish_to_vendor" class="publish_to_vendor" checked/>
                                            <label>Publish to Vendor Portal</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="form-outer">
                                <input type="hidden" value="" class="tenant_info_id">
                                <div class="form-hdr">
                                    <h3>Tenant Information</h3>
                                </div>
                                <div class="form-data">
                                   <div class="grid-outer">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-dark">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Tenant Name</th>
                                                        <th scope="col">Email</th>
                                                        <th scope="col">Phone Number</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td id="nametableVendor"></td>
                                                    <td id="emailtableVendor"></td>
                                                    <td id="phntableVendor"></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="form-outer">
                                <input type="hidden" value="" class="vendor_info_id">
                                <div class="form-hdr">
                                    <h3>Vendor Information</h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="latefee-check-outer form-outer2">
                                            <div class="col-sm-12">
                                                <div class="check-outer">
                                                    <input type="radio" name="vendor-info-radio" class="vendor-info-radio vendor-all" value="all">
                                                    <label>All</label>
                                                </div>
                                                <div class="check-outer">
                                                    <input type="radio" name="vendor-info-radio" class="vendor-info-radio vendor-preff" value="preferred" checked>
                                                    <label>Preferred</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-outer">

                                            <div class="col-xs-12 col-sm-4 col-md-2 allDiv" style="display:none">
                                                <input type="hidden" class="preferredvendorType" value="">
                                                <label>Vendor</label>
                                                <input class="form-control add-input owner_pre_vendor capital" type="text" name='vendor_name'/>
                                                <a class="add-icon" href="javascript:;" id="open-vendor-modal"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-2 preffDiv">
                                                <input type="hidden" class="preferredvendorType" value="">
                                                <label>Vendor</label>
                                                <input class="form-control add-input owner_pre_vendor2" type="text" name='vendor_name'/>
                                                <a class="add-icon" href="javascript:;" id="open-vendor-modal"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            </div>

                                            <div class="col-xs-12 col-sm-4 col-md-3 clear edit-address">
                                                <label>Address</label>
                                                <textarea class="form-control capital" id="vendor_address" name="vendor_address"></textarea>
                                                <a style="display:none" class="add-icon addressupdbtn" href="javascript:;">Edit Address</a>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="grid-outer">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-dark vendorTableData">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Name</th>
                                                                <th scope="col">Category</th>
                                                                <th scope="col">Email</th>
                                                                <th scope="col">Phone Number</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td id="nametable"></td>
                                                                <td id="cattable"></td>
                                                                <td id="emailtable"></td>
                                                                <td id="phntable"></td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>File Library</h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="form-outer2">
                                            <div class="col-sm-12">
                                                <button type="button" id="add_libraray_file" class="green-btn">Add Files</button>
                                                <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                <button type="button" class="orange-btn" id="remove_library_file">Remove All Files</button>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="row" id="file_library_uploads">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Notes</h3>
                                </div>
                                <div class="form-data">
                                   <div class="row">
                                       <div class="form-outer vendor-notes-clone-divclass" id="vendor-notes-clone-div">
                                           <div class="col-sm-4">
                                               <div class="notes-timer-outer">
                                               <textarea class="notes capital notes_date_right " name="note[]" ></textarea>
                                               </div>
                                               <a class="add-icon-textarea vendor-notes-plus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                               <a class="add-icon-textarea vendor-notes-minus" style="display: none" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                           </div>
                                       </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Custom Fields</h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="form-outer">
                                            <div class="col-sm-12">
                                                <div class="custom_field_html">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3 text-right pull-right min-height-0">
                                                <div class="btn-outer min-height-0">
                                                    <button type="button"  id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">New Custom Field</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-outer text-right">
                                    <button class="blue-btn" id="saveWorkOrderTab">Save</button>
                                    <button class="blue-btn">Save &amp; Add Another</button>
                                    <button type="button" class="clear-btn clearFormReset">Clear</button>
                                    <button type="button" class="grey-btn cancel-work-order">Cancel</button>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                           
                        </div>
                    </form>
                    </div>
                </div>
        </section>
    </div>
<div class="container">
    <div class="modal fade fdgfhyihuihi" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body" style="height: 380px;">
                    <div class="form-outer col-sm-12">
                        <form id="custom_field">
                            <input type="hidden" id="customFieldModule" name="module" value="work_order">
                            <input type="hidden" name="id" id="custom_field_id" value="">
                            <div class="row custom_field_form">
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Field Name <em class="red-star">*</em></label>
                                    </div>
                                    <div class="col-sm-9 field_name">
                                        <input class="form-control" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                        <span class="required error"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Data Type</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control data_type" id="data_type" name="data_type">
                                            <option value="text">Text</option>
                                            <option value="number">Number</option>
                                            <option value="currency">Currency</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="url">URL</option>
                                            <option value="date">Date</option>
                                            <option value="memo">Memo</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Default value</label>
                                    </div>
                                    <div class="col-sm-9 default_value">
                                        <input class="form-control default_value" id="default_value" type="text" name="default_value" placeholder="">
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Required Field</label>
                                    </div>
                                    <div class="col-sm-9 is_required">
                                        <select class="form-control" name="is_required" id="is_required">
                                            <option value="1">Yes</option>
                                            <option value="0" selected="selected">No</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-outer text-right">
                                <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>                                <button type="button" class="clear-btn" >Clear</button>
                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="modal fade" id="open-vendor-address-edit" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Vendor Address</h4>
                </div>
                <div class="modal-body" style="height: max-content;overflow: auto;">
                    <form name="FormUpdateAddress" id="FormUpdateAddress" method="post">
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Zip / Postal Code </label>
                                    <input type="hidden" value="" class="addressUserId">
                                    <input name="zipcode" maxlength="9" placeholder="Eg: 10001" id="ven_zip_code" class="form-control capital" type="text" value="<?php echo $_SESSION[SESSION_DOMAIN]['default_zipcode'] ?>"/>
                                </div>
                                <div class="col-sm-4">
                                    <label>Country </label>
                                    <input name="country" maxlength="50" placeholder="Eg: US" id="ven_country" class="form-control capital" type="text" />
                                </div>
                                <div class="col-sm-4">
                                    <label>State / Province </label>
                                    <input name="state" maxlength="100" placeholder="Eg: AL"  id="ven_state" class="form-control capital states" type="text" />
                                </div>
                                <div class="col-sm-4">
                                    <label>City </label>
                                    <input name="city" maxlength="100" placeholder="Eg: Huntsville" id="ven_city" class="form-control capital citys" type="text" />
                                </div>
                                <div class="col-sm-4">
                                    <label>Address 1</label>
                                    <input name="address1" placeholder="Eg: Street Address 1" maxlength="200" id="ven_address1" class="form-control capital address_field" type="text" />
                                </div>
                                <div class="col-sm-4">
                                    <label>Address 2</label>
                                    <input name="address2" placeholder="Eg: Street Address 2" maxlength="200" id="ven_address2" class="form-control capital address_field" type="text" />
                                </div>
                                <div class="col-sm-4">
                                    <label>Address 3</label>
                                    <input name="address3" placeholder="Eg: Street Address 3" maxlength="200" id="ven_address3" class="form-control capital address_field" type="text" />
                                </div>
                            </div>


                            <div class="btn-outer">
                                <button type="submit" class="blue-btn" id='VendorAddressDetial'>Update</button>
                                <button type="button" class="grey-btn" id="cancelVendoraddressAdd">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="container">
        <div class="modal fade" id="vendorModel" role="dialog">
            <div class="modal-dialog modal-md" style="width: 1000px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Vendor Information</h4>
                    </div>
                    <div class="modal-body" style="height: max-content;overflow: auto;">
                        <form name="FormSaveNewVendor" id="FormSaveNewVendor" method="post">
                            <div class="form-outer">
                                    <div class="col-sm-3">
                                        <label>Vendor ID<em class="red-star">*</em></label>
                                        <input class="form-control vendor_random_id" type="text" value=""  maxlength="6" name="vendor_random_id"/>
                                        <span id="charge_codeErr"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Salutation </label>
                                        <select class="form-control" id="salutation" name="salutation">
                                            <option value="">Select</option>
                                            <?php foreach ($satulation_array as $key => $value) { ?>
                                                <option value="<?= $key ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>First Name <em class="red-star">*</em></label>
                                        <input class="form-control capital" type="text" maxlength="20" name="first_name" id="first_name" placeholder="First Name"/>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Middle Name</label>
                                        <input class="form-control capital" type="text" maxlength="50" name="middle_name" placeholder="Middle Name"/>
                                    </div>


                                <div class="col-sm-3">
                                    <label>Last Name <em class="red-star">*</em></label>
                                    <input class="form-control capital" type="text" maxlength="20" name="last_name" placeholder="Last Name" id="last_name"/>
                                </div>
                                <div class="col-sm-3">
                                    <label>Gender</label>
                                    <select id="gender" name="gender"   class="form-control"><option value="">Select</option>
                                        <?php foreach ($gender_array as $key => $value) { ?>
                                            <option value="<?= $key ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label>Entity/Company Name</label>
                                    <input class="form-control capital" type="text" placeholder="Eg: Apexlink" maxlength="50" name="company_name"/>
                                </div>
                                <div class="col-sm-3">
                                    <label>Country Code </label>
                                    <select name="additional_country" id="additional_country" class="form-control"><option>Select</option></select>
                                </div>
                                <div class="col-sm-3">
                                    <label>Phone Number <em class="red-star">*</em> </label>
                                    <input name="additional_phone[]" maxlength="12" placeholder="123-456-7891" class="form-control phone_format add-input customPhonenumbervalidations" type="text" data_required="true"/>
                                    <span class="customError required"></span>
                                </div>
                                <div class="col-sm-3">
                                    <label>Zip / Postal Code </label>
                                    <input name="zipcode" maxlength="9" placeholder="Eg: 10001" id="zip_code" class="form-control capital" type="text" value="<?php echo $_SESSION[SESSION_DOMAIN]['default_zipcode'] ?>"/>
                                </div>
                                <div class="col-sm-3">
                                    <label>Country </label>
                                    <input name="country" maxlength="50" placeholder="Eg: US" id="country" class="form-control capital" type="text" />
                                </div>
                                <div class="col-sm-3">
                                    <label>State / Province </label>
                                    <input name="state" maxlength="100" placeholder="Eg: AL"  class="form-control capital states" type="text" />
                                </div>
                                <div class="col-sm-3">
                                    <label>City </label>
                                    <input name="city" maxlength="100" placeholder="Eg: Huntsville"  class="form-control capital citys" type="text" />
                                </div>
                                <div class="col-sm-3">
                                    <label>Address 1</label>
                                    <input name="address1" placeholder="Eg: Street Address 1" maxlength="200" id="contact_address2" class="form-control capital address_field" type="text" />
                                </div>
                                <div class="col-sm-3">
                                    <label>Address 2</label>
                                    <input name="address2" placeholder="Eg: Street Address 2" maxlength="200" id="contact_address3" class="form-control capital address_field" type="text" />
                                </div>
                                <div class="col-sm-3">
                                    <label>Address 3</label>
                                    <input name="address3" placeholder="Eg: Street Address 3" maxlength="200" id="contact_address4" class="form-control capital address_field" type="text" />
                                </div>
                                <div class="col-sm-3 emailaddressmainDiv" id="emailaddressmainDivId">
                                    <label>Email Address <em class="red-star">*</em></label>
                                    <input name="additional_email" placeholder="Eg: user@domain.com" maxlength="150" class="form-control add-input customadditionalEmailValidation" type="text" data_required="true"/>
                                     <span class="customError required"></span>
                                </div>
                                <div class="col-sm-3">
                                    <label>Vendor Type <a class="pop-add-icon vendortypeplus" onclick="clearPopUps('#Newvendortype')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                    <select class="form-control" id='vendor_type_options' name='vendor_type_id'></select>
                                    <div class="add-popup" id='Newvendortype' style="width: 127%;">
                                        <h4>Add Vendor Type</h4>
                                        <div class="add-popup-body">

                                            <div class="form-outer">
                                                <div class="col-sm-12">
                                                    <label>Vendor Type<em class="red-star">*</em></label>
                                                    <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20" name='@vendor_type' id='vendor_type' placeholder="Eg: PMB"/>
                                                    <span class="customError required"></span>
                                                </div>
                                                <div class="col-sm-12">
                                                    <label> Description <em class="red-star">*</em></label>
                                                    <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="50" name='@description' id='vendor_description' placeholder="Eg: Plumber"/>
                                                    <span class="customError required"></span>
                                                </div>
                                                <div class="btn-outer text-right">
                                                    <input type="submit"  id='NewvendortypeSave' class="blue-btn" value="Save" />
                                                    <input type="button" class="clear-btn ClearVendorTypeForm" value='Clear' />
                                                    <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label>Vendor Rate <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> </label>
                                    <input class="form-control amount number_only" type="text" id="vendor_rate" name="vendor_rate" placeholder="Vendor Rate"/>
                                    <span class='add-icon-span'>/hr </span>
                                </div>
                                <div class="col-sm-3">
                                    <label>Referral Source <a class="pop-add-icon additionalReferralResource" href="javascript:;" onclick="clearPopUps('#additionalReferralResource1')"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                    <select name="additional_referralSource" id="additional_referralSource" class="form-control"><option>Select</option></select>
                                    <div class="add-popup" id="additionalReferralResource1" style="width: 127%;">
                                        <h4>Add New Referral Source</h4>
                                        <div class="add-popup-body">
                                            <div class="form-outer">
                                                <div class="col-sm-12">
                                                    <label>New Referral Source <em class="red-star">*</em></label>
                                                    <input class="form-control reff_source1 customReferralSourceValidation capital" data_required="true" type="text" placeholder="New Referral Source">
                                                    <span class="customError required"></span>
                                                    <span class="red-star" id="reff_source1"></span>
                                                </div>
                                                <div class="btn-outer text-right">
                                                    <button type="button" class="blue-btn add_single1" data-validation-class="customReferralSourceValidation" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source1" data-name="additional_referralSource">Save</button>
                                                    <input type="button" class="clear-btn ClearReferralSource" value="Clear">
                                                    <input type="button" class="grey-btn" value="Cancel">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="btn-outer text-right">
                                        <button type="submit" class="blue-btn" id='SaveVendorAdd'>Save</button>
                                        <button type="button" class="clear-btn" id="ClearVendorForm">Clear</button>
                                        <button type="button" class="grey-btn" id="cancelVendorAdd">Cancel</button>
                                    </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){


            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });

            $(document).on('click','.clearNewcategory',function () {
                $('#category').val('');
                $('#cat_desc').val('');
            });
            $(document).on('click','.clearNewPriority',function () {
                $('#priority').val('');
                $('#prior_description').val('');
            });

            $(document).on('click','.clearPortfolio',function () {
                $('#portfolio_name').val('');
            });

            $(document).on('click','.clearStatus',function () {
                $('#work_order_status').val('');
            });

            $(document).on('click','.ClearOther',function () {
                $('#work_order_source').val('');
            });

            $(document).on('click','.ClearRequestedby',function () {
                $('#work_order_request').val('');
            });


            $(document).on('click','.clearFormReset',function () {
                resetFormClear("#FormWorkOrder",['work_order_number','created_on','work_order_cat','priority_id','portfolio_id','status_id','scheduled_on','completed_on'],'form',false);
            });
        });

        $(document).ready(function(){
            $("#maintenance_top").addClass("active");
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
        });


        $(document).on('click','.back_redirection', function () {
            localStorage.removeItem('redirection_module');
        });

        $(document).on('click','.ClearCustomFieldForm', function () {
            bootbox.confirm("Do you want to clear this form?", function (result) {
                if (result == true) {
                    $('#custom_field')[0].reset();
                }
            });
        });


    </script>
    <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/maintenance/work-order/addWorkOrder.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/vendor/file_library.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/commonPopup.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.easyui.min.js" type="text/javascript"></script>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/easyui.css" rel="stylesheet">
    <script type="text/javascript">
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
        var upload_url = "<?php echo SITE_URL; ?>";
        var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
        $(document).ready(function () {
            $('#custom_field .clear-btn').on('click',function(){
                bootbox.confirm("Do you want to clear this form?", function (result) {
                    if (result == true) {
                    $('#custom_field')[0].reset();
                    }
                });
            });

            $(document).on('click','.ClearVendorTypeForm',function(){
                        $('#vendor_type').val('');
                        $('#vendor_description').val('');
            });

            $(document).on('click','.ClearReferralSource',function(){
                $('.reff_source1').val('');
            });
            $(document).on('click','#ClearVendorForm',function(){

                resetFormClear('#FormSaveNewVendor',['vendor_random_id'],'form',false);
            });

        });

    </script>

    <!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>



