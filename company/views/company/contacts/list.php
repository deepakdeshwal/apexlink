<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);

}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/shorttermcss.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-pseudo-ripple.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-nao-calendar.css">

    <section class="main-content employee-list-page">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                People Module &gt;&gt; <span>Contact Listing</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>PEOPLE</h2>
                            <div class="list-group panel">
                                <a href="/Tenantlisting/add" class="list-group-item list-group-item-success strong collapsed">New Tenant</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Move Out</a>
                                <a id ="shorttermrentals" href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Book Now</a>Book Now</a>
                                <a href="/People/AddOwners)" class="list-group-item list-group-item-success strong collapsed">New Owner</a>
                                <a href="/Vendor/AddVendor" class="list-group-item list-group-item-success strong collapsed">New Vendor</a>
                                <a href="/People/AddEmployee" class="list-group-item list-group-item-success strong collapsed">New Employee</a>
                                <a href="/People/AddContact" class="list-group-item list-group-item-success strong collapsed">New Contact</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Recieve Payment</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Send Tenant Statements</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Bank Deposite</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Bill</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Tenant Instrument Register</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Vendor Instrument Register</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Tenant Transfer</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Tenant Transfer</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Move In</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Move Out</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Tenants</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Owners</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Vendors</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">NSF Listing</a>
                            </div>
                        </div>
                    </div>

                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->

                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Tenantlisting/Tenantlisting">Tenants</a></li>
                            <li role="presentation"><a href="/People/Ownerlisting">Owners</a></li>
                            <li role="presentation"><a href="/Vendor/Vendor" >Vendors</a></li>
                            <li role="presentation"><a href="/People/ContactListt">Contacts</a></li>
                            <li role="presentation"><a href="/People/GetEmployeeList">Employee</a></li>
                        </ul>
                        <div class="atoz-outer">
                                <em class="apex-alphabets apex-alphabets-background" id="apex-alphafilter" style="display:none;">
                                <span class="AtoZ"></span></em>
                            <em class="AZ" id="AZ">A-Z</em>
                            <span id="allAlphabet" style="cursor:pointer;">All</span>
                        </div>
                        <div style="display: none;" class="atoz-outer">
                            <em class="apex-alphabets apex-alphabets-background" id="apex-alphafilter" style="display:none;">
                                <span class="AtoZ"></span></em>
                            <em class="AZ" id="AZ">A-Z</em>
                            <span id="allAlphabet" style="cursor:pointer;">All</span>
                        </div>
                        <div style="display: none;" class="atoz-outer">
                            <em class="apex-alphabets apex-alphabets-background" id="apex-alphafilter" style="display:none;">
                                <span class="AtoZ"></span></em>
                            <em class="AZ" id="AZ">A-Z</em>
                            <span id="allAlphabet" style="cursor:pointer;">All</span>
                        </div>
                        <div style="display: none;" class="atoz-outer">
                            <em class="apex-alphabets apex-alphabets-background" id="apex-alphafilter" style="display:none;">
                                <span class="AtoZ"></span></em>
                            <em class="AZ" id="AZ">A-Z</em>
                            <span id="allAlphabet" style="cursor:pointer;">All</span>
                        </div>
                        <div style="display: none;" class="atoz-outer">
                            <em class="apex-alphabets apex-alphabets-background" id="apex-alphafilter" style="display:none;">
                                <span class="AtoZ"></span></em>
                            <em class="AZ" id="AZ">A-Z</em>
                            <span id="allAlphabet" style="cursor:pointer;">All</span>
                        </div>
                        <!-- Tab panes -->
                        <div class="sub-tabs">
                            <ul class="nav nav-tabs" role="tablist">
                                <li id="tenant_link" role="presentation"><a href="#lostfound-sub-one" class="contact-main-tab-hide" aria-controls="home" role="tab" data-toggle="tab">Tenants <span id="tenantCountNo" class="tab-count">4 </span></a></li>
                                <li id="owner_link" role="presentation"><a href="#lostfound-sub-two" class="contact-main-tab-hide" aria-controls="profile" role="tab" data-toggle="tab">Owners <span id="ownersCountNo" class="tab-count">1</span></a></li>
                                <li id="vendor_link" role="presentation"><a href="#lostfound-sub-three" class="contact-main-tab-hide" aria-controls="home" role="tab" data-toggle="tab">Vendors <span id="vendorCountNo" class="tab-count">2</span></a></li>
                                <li id="user_link" role="presentation"><a href="#lostfound-sub-four" class="contact-main-tab-hide" aria-controls="profile" role="tab" data-toggle="tab">Users <span id="userCountNo" class="tab-count">1</span></a></li>
                                <li id="other_link" role="presentation"><a href="#lostfound-sub-four" class="contact-main-tab-hide" aria-controls="profile" role="tab" data-toggle="tab">Others <span id="otherCountNo" class="tab-count">230</span></a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="property-status">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label>Status</label>
                                        <select id="jqGridStatus" class="fm-txt form-control jqGridStatusClass" data-module="CONTACT">
                                            <option value="All" selected="selected">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">Archive</option>
                                        </select>

                                    </div>
                                    <div class="col-sm-10">
                                        <div class="btn-outer text-right">
                                            <a href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/excel/ContactSample.xlsx" download class="blue-btn">Download Sample</a>
                                            <button class="blue-btn" id="import_employee">Import Contact</button>
<!--                                            <a class="blue-btn" id="export_tenant">Export Contact</a>-->
                                            <button onclick="window.location.href='/People/AddContact'" class="blue-btn">New Contact</button>
                                        </div>

                                    </div>
                                    <div class="col-sm-12 padding">
                                        <div  style="display: none;" id="import_tenant_type_div">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">

                                                        <a>Import Contact</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                    <div class="panel-body">
                                                        <form name="importTenantTypeForm" id="importTenantTypeFormId" >
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <input type="file" name="import_file" id="import_file"   accept=".csv, .xls, .xlsx" required/>
                                                                        <span class="error"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="btn-outer">
                                                                        <button type="submit" class="blue-btn">Submit</button>
                                                                        <button type="button" id="import_tenant_cancel_btn" class="grey-btn">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div role="tabpanel" class="" id="contact_grid">


                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                  <div class="table-responsive overflow-unset">
                                                                    <table id="employee-table" class="table table-bordered" style="cursor: pointer"></table>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="lostfound-sub-one">

                                <!--Import AccountChargeCode div starts here-->

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table  class="table table-hover table-dark" id="tenant_listings">
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="lostfound-sub-two">

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table  class="table table-hover table-dark" id="owner_listings" style="cursor: pointer">
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="lostfound-sub-three">
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table  class="table table-hover table-dark" id="vendorListingTable">
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="lostfound-sub-four">
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table  class="table table-hover table-dark" id="otheruserListings" style="cursor: pointer">
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div>
                        </div>
                    </div>
                    <!--Tabs Ends -->
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="PrintEmployeeEnvelope" role="dialog">
    <div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: unset;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <input type="button" class="blue-btn pull-left" id="print_building_enevelope" onclick="PrintElem('#employee_print_envelope_content')" onclick="window.print();"  value="Print"/>
                <input type="hidden" id="announcementSys_id" value="">

            </div>
            <div class="modal-body" id="employee_print_envelope_content">
                <div>
                    <div class="left-detail">
                        <ul>
                            <li class="padding-top bold mt-n1" id="user_company_name" >

                            </li>
                            <li class="padding-top bold mt-n1" id="user_address1">

                            </li>
                            <li class="padding-top bold mt-n1" id="user_address2">

                            </li>
                            <li class="padding-top bold mt-n1" id="user_address3">

                            </li >
                            <li class="padding-top bold mt-n1" id="user_address4">

                            </li>
                            <li class="padding-top bold mt-n1">
                                <span id="city" class="city"></span><span id="state" class="state" ></span> <span id="postal_code" class="postal_code"> </span>
                            </li>
                        </ul>
                    </div>
                    <div class="right-detail " style=" position:relative; left:+400px;">
                        <ul>
                            <li class="padding-top bold mt-n1" id="" >
                                <span id="employee_last_name"></span> <span id="employee_middle_name"></span> <span id="employee_first_name"></span>
                            </li>
                            <li class="padding-top bold mt-n1" id="employee_address1">

                            </li>
                            <li class="padding-top bold mt-n1" id="employee_address2">

                            </li >
                            <li class="padding-top bold mt-n1" id="employee_address3">

                            </li>
                            <li class="padding-top bold mt-n1" id="employee_address4">

                            </li>
                            <li class="padding-top bold mt-n1">
                                <span id="employee_city" class="employee_city"></span><span id="employee_state" class="employee_state" ></span> <span id="employee_postal_code" class="employee_postal_code"> </span>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="container">
<div class="modal fade" id="backgroundReport" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content  pull-left" style="width: 100%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-left">Background Reports</h4>
            </div>
            <div class="modal-body">
                <div class="tenant-bg-data">
                    <h3 style="text-align: center;">
                        Open Your Free Account Today!
                    </h3>
                    <p>
                        ApexLink is pleased to partner with Victig Screening Solutions to provide you with
                        the highest quality, timely National Credit, Criminal and Eviction Reports.
                    </p>
                    <div class="grid-outer " style="max-width: 1421px;">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                            <tr>
                                <th colspan="3" align="center" class="bg-Bluehdr">
                                    Package Pricing<br>
                                    Properties Managing
                                </th>
                            </tr>
                            <tr>
                                <td width="29%" align="left">
                                    National Criminal
                                </td>
                                <td width="29%" align="left">
                                    National Criminal and Credit
                                </td>
                                <td width="42%" align="left">
                                    National Criminal, Credit and State Eviction
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    $15
                                </td>
                                <td align="left">
                                    $20
                                </td>
                                <td align="left">
                                    $25
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    &nbsp;
                                </td>
                                <td align="left">
                                    &nbsp;
                                </td>
                                <td align="left">
                                    &nbsp;
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <p>
                            *$100 fee to authorize credit report
                        </p>
                    </div>
                    <div class="grid-outer " style="max-width: 1421px;">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                            <tr>
                                <th colspan="5" align="center" class="bg-Bluehdr">
                                    High Volume Users and Larger Properties
                                </th>
                            </tr>
                            <tr>
                                <td colspan="5" align="left">
                                    <p align="center">
                                        <strong>Contact our Victig Sales Team for custom pricing</strong>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td width="26%" align="center">
                                    <a href="mailto:sales@victig.com"><strong>sales@victig.com</strong></a>
                                </td>
                                <td width="15%" align="center">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </td>
                                <td width="21%" align="center">
                                    <strong>866.886.5644 </strong>
                                </td>
                                <td width="13%" align="center">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </td>
                                <td width="25%" align="center">
                                    <a href="http://www.victig.com" target="_blank"><strong>www.victig.com</strong></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="div-full">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                            <tr>
                                <td width="52%" height="20" style="font-size: 14px;" align="left">
                                    Already have an Account? <a href="https://apexlink.instascreen.net" target="_blank">
                                        Log in here
                                    </a>
                                </td>
                                <td width="48%" rowspan="2" align="right">
                                    <a href="https://www.victig.com/wp-content/uploads/2016/05/sample-tenant-report.jpg" target="_blank">
                                        <!--                                            <input value="View Sample Report" class="blue-btn fr" type="button" spellcheck="true" autocomplete="off">-->
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="font-size: 14px;">
                                    New Users: <a href="https://www.victig.com/victig-on-boarding/" target="_blank">
                                        Click
                                        here to register
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="div-full3">
                        <p>
                            Please be advised that all background services are provided by Victig Screening
                            Solutions. Direct all contact and questions pertaining to background check services
                            to <a href="mailto:sales@victig.com">sales@victig.com</a>, 866.886.5644, <a href="http://www.victig.com" target="_blank">www.victig.com</a>
                        </p>
                    </div>
                    <div class="notice">
                        NOTICE: The use of this system is restricted. Only authorized users may access this
                        system. All Access to this system is logged and regularly monitored for computer
                        security purposes. Any unauthorized access to this system is prohibited and is subject
                        to criminal and civil penalties under Federal Laws including, but not limited to,
                        the Computer Fraud and Abuse Act and the National Information Infrastructure Protection
                        Act.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="container">
<div class="modal fade" id="backGroundCheckPop" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content  pull-left" style="width: 100%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Disclaimer</h4>
            </div>
            <div class="modal-body">
                <div class="col-sm-12">
                    <p style="font-size: 14px; line-height: 20px;"> I authorize ApexLink Inc. to submit this request to generate and present a background/any check report for the Person/Company I have listed. I understand that ApexLink Inc. is not responsible or liable for any data breach, loss/loss of profits or any claim against the filer (Myself or my company) by any party. I understand that in no event is ApexLink Inc. liable for any and all damages. I also understand that ApexLink Inc. grants no implied warranties, including without limitation, warranties, of merchantability, or of fitness for any particular purpose. </p></div>
                <div class="col-sm-12"><input type="checkbox" name="" class="background_check_open"><label style="display: inline;"> Check to proceed further.</label></div>
                <div class="col-sm-12 btn-outer mg-btm-20">
                    <input type="button" class="blue-btn"  data-dismiss="modal" value="Cancel">
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Wrapper Ends -->
    <script>
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    </script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/contacts/contactsList.js"></script>


<script>

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";

</script>


<!-- Jquery Starts -->
<?php

include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>

<?php include_once(COMPANY_DIRECTORY_URL . "/views/company/tenants/modals.php"); ?>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/fullcal.js" integrity="sha384-vk5WoKIaW/vJyUAd9n/wmopsmNhiy+L2Z+SBxGYnUkunIxVxAv/UtMOhba/xskxh" crossorigin="anonymous"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-pseudo-ripple.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-nao-calendar.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/shortTermRental.js"></script>
<script src="https://js.stripe.com/v3/"></script>
<script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/payments/subPayment.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/contacts/flag/flag.js"></script>