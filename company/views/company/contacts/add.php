<?php
/**
 * Created by PhpStorm.
 * User: chughraghav
 * Date: 07/18/2019
 * Time: 4:19 PM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
$satulation_array =['1'=>'Dr.','2'=>'Mr.','3'=>'Mrs.','4'=>'Mr. & Mrs.','5'=>'Ms.','6'=>'Sir','7'=>'Madam','10'=>'Brother','8'=>'Sister','11'=>'Father','9'=>'Mother'];
$gender_array =['1'=>'Male','2'=>'Female','3'=>'Prefer Not To Say','4'=>'Other'];
?>
<script>
    var credential_notice_period =  "<?php echo $_SESSION[SESSION_DOMAIN]['default_notice_period']; ?>";
</script>


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>  <style>
    .multipleEmail input{margin:5px auto; }
    select[name=hobbies] + .btn-group button{
        margin-top: 0px;
    }
    .closeimagepopupicon{
        float: right;
        font-weight: bold;
        border: 2px solid #666;
        border-radius: 12px;
        padding: 0px 4px;
        cursor: pointer;
    }
    .fa-minus-circle {
        display: none;
    }
    .fa-times-circle {
        display: none;
    }

    .apx-inline-popup {
        position: relative;
    }

    .apx-inline-popup-box {
        position: absolute;
        top: 0;
        left: 0;
        background: #fff;
        z-index: 2;
        border: 1px solid #ddd;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
        padding: 15px;
        width: 100%;
    }

    .apx-inline-popup-box > h4 {
        font-size: 12px
    }

    .image-editor {
        text-align: center;
    }

    .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 5px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 250px;
        height: 250px;
        display: inline-block;
    }

    .cropit-preview-image-container {
        cursor: move;
    }

    .cropit-preview-background {
        opacity: .2;
        cursor: auto;
    }

    .image-size-label {
        margin-top: 10px;
    }

    .export {
        /* Use relative position to prevent from being covered by image background */
        position: relative;
        z-index: 10;
        display: block;
    }

    .image-editor input[type="file"] {
        opacity: 0;
        position: absolute;
        top: 45px;
        left: 91px;
    }

    .upload-logo .img-outer img {
        max-width: 100%;
    }

    .choose-img {
        margin: 0 !important;
    }

    /*button {*/
    /*margin-top: 10px;*/
    /*}*/

    .vehicle_image1, .vehicle_image2, .vehicle_image3 {
        width: 100px;
        height: 100px;
    }

    .vehicle_image1 img, .vehicle_image2 img, .vehicle_image3 img {
        width: 100%;
    }
    .capital{text-transform: capitalize;}
</style>
<body>
<div class="popup-bg"></div>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <?php
    if(isset($_GET['id']) && $_GET['id'] != ""){
        $paramId = $_GET['id'];
    }else{
        $paramId = "";
    }
    ?>
    <input type='hidden' name='id_from_other' class="id_from_other" value="<?php echo $paramId; ?>">

    <!-- Top navigation end -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                People Module >> <span>New Contact</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <form id="addEmployee" enctype='multipart/form-data'>


                    <div class="col-sm-12">
                        <div class="content-section">
                            <!--Tabs Starts -->
                            <div class="main-tabs">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a href="/Tenantlisting/Tenantlisting">Tenants</a></li>
                                    <li role="presentation"><a href="/People/Ownerlisting">Owners</a></li>
                                    <li role="presentation"><a href="/Vendor/Vendor" >Vendors</a></li>
                                    <li class="active" role="presentation"><a href="/People/ContactListt">Contacts</a></li>
                                    <li role="presentation"><a href="/People/GetEmployeeList">Employee</a></li>
                                </ul>
                                <!-- Nav tabs -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="guest-cards">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Contact Information <a class="back goback_func"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <!--                                                <div class="col-sm-12">-->
                                                    <!--                                                    <label>Upload Picture</label>-->
                                                    <!--                                                    <div class="upload-logo">-->
                                                    <!--                                                        <div class="employee_image img-outer"><img src="--><?php //echo COMPANY_SUBDOMAIN_URL ?><!--/images/dummy-img.jpg"></div>-->
                                                    <!--                                                        <a href="javascript:;">-->
                                                    <!--                                                            <i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Image</a>-->
                                                    <!--                                                        <span>(Maximum File Size Limit: 1MB)</span>-->
                                                    <!--                                                    </div>-->
                                                    <!--                                                    <div class="image-editor">-->
                                                    <!--                                                        <input type="file" class="cropit-image-input" name="employee_image">-->
                                                    <!--                                                        <div class='cropItData' style="display: none;">-->
                                                    <!--                                                            <span class="closeimagepopupicon">X</span>-->
                                                    <!--                                                            <div class="cropit-preview"></div>-->
                                                    <!--                                                            <div class="image-size-label"></div>-->
                                                    <!--                                                            <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>-->
                                                    <!--                                                            </a>-->
                                                    <!--                                                            <a href="javascript:;" id="rotate-cw" class="rotate-cw" ><i class="fa fa-repeat" aria-hidden="true"></i>-->
                                                    <!--                                                            </a>-->
                                                    <!---->
                                                    <!--                                                            <input type="range" class="cropit-image-zoom-input">-->
                                                    <!--                                                            -->
                                                    <!--                                                            <input type="hidden" name="image-data" class="hidden-image-data"/>-->
                                                    <!--                                                           <div class="btn-outer">-->
                                                    <!--                                                            <input type="button" class="blue-btn export" value="Done"-->
                                                    <!--                                                                   data-val="employee_image">-->
                                                    <!--                                                           </div>-->
                                                    <!--                                                        </div>-->
                                                    <!--                                                    </div>-->
                                                    <!--                                                </div>-->
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Salutation</label>

                                                        <select class="form-control" id="salutation" name="salutation">
                                                            <option value="">Select</option>
                                                            <?php foreach ($satulation_array as $key => $value) {?>

                                                                <option value="<?= $key?>"><?php echo  $value;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>First Name <em class="red-star">*</em></label>
                                                        <input class="form-control capsOn"  id="firstname" placeholder="First Name" name="firstname" maxlength="50" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Middle Name</label>
                                                        <input class="form-control capsOn" id="middlename" placeholder="Middle Name" name="middlename" maxlength="50" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Last Name <em class="red-star">*</em></label>
                                                        <input class="form-control capsOn" id="lastname" placeholder="Last Name" name="lastname" maxlength="50" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3 hidemaiden" style="display:none">
                                                        <label>Maiden Name</label>
                                                        <input class="form-control capsOn" id="maidenname" placeholder="Maiden Name" name="maidenname" maxlength="50"  type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Nick Name</label>
                                                        <input class="form-control capsOn" id="nickname"
                                                               name="nickname" placeholder="Nick Name" maxlength="50" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Gender</label>
                                                        <select id="gender" name="gender"   class="form-control"><option value="">Select</option>
                                                            <?php
                                                            foreach( $gender_array as $key => $value){ ?>
                                                                <option value="<?= $key?>"><?php echo  $value;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>SSN/SIN/ID</label>
                                                        <div class='multipleSsn'>
                                                            <input class="form-control add-input capsOn" maxlength="50" type="text"
                                                                   id="ssn" name="ssn[]" style="margin: 5px 0;">
                                                            <a class="add-icon ssn-remove-sign" href="javascript:;"
                                                               style="display:none"><i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i></a>
                                                            <span class="ffirst_nameErr error red-star"></span>

                                                            <a class="add-icon ssn-plus-sign" href="javascript:;"><i
                                                                        class="fa fa-plus-circle"
                                                                        aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Birth Date</label>
                                                        <input class="form-control" placeholder="Birth Date" id="date_of_birth"
                                                               name="date_of_birth" readonly type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Zip/Postal Code</label>
                                                        <input id="zip_code" name="zip_code" placeholder="Eg: 35801" maxlength="9" class="form-control capsOn" />
                                                        <span class="zip_codeErr error"></span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Country</label>
                                                        <input id="country" placeholder="Eg: US" name="country" class="form-control capsOn" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>State/Province</label>
                                                        <input id="states"  placeholder="Eg: AL" maxlength="100" name="state" class="form-control capsOn"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>City</label>
                                                        <input id="citys" name="city"  placeholder="Eg: Huntsville" maxlength="50" class="form-control capsOn" />
                                                    </div>

                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Address 1</label>
                                                        <input class="form-control capsOn" id="address1" maxlength="200" placeholder="Eg: Street address 1" name="address1" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Address 2</label>
                                                        <input class="form-control capsOn" id="address2" maxlength="250" placeholder="Eg: Street address 2" name="address2" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Address 3</label>
                                                        <input class="form-control capsOn" id="address3" maxlength="250" placeholder="Eg: Street address 3" name="address3" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Address 4</label>
                                                        <input class="form-control capsOn" placeholder="Eg: Street address 4" maxlength="250" id="address4" name="address4" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Referral Source <a class="pop-add-icon additionalReferralResource" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                        <select class="form-control"  name="additional_referralSource" id="additional_referralSource" ><option>Mobile</option></select>
                                                        <div class="add-popup" id="additionalReferralResource1">
                                                            <h4>Add New Referral Source</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Referral Source <em class="red-star">*</em></label>
                                                                        <input class="form-control reff_source1 customReferralSourceValidation capsOn" data_required="true" type="text" placeholder="New Referral Source">
                                                                        <span class="customError required"></span>
                                                                        <span class="red-star" id="reff_source1"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1" data-validation-class="customReferralSourceValidation" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source1" data-name="additional_referralSource">Save</button>
                                                                        <button type="button"  class="clear-btn referal_source_clear">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                            <div class="col-xs-12 col-sm-4 col-md-3">-->
                                                    <!--                                                <label>Previous Company</label>-->
                                                    <!--                                                <input class="form-control" id="previous_company" placeholder="Previous Company Name" name="previous_company" maxlength="200" type="text"/>-->
                                                    <!--                                            </div>-->
                                                    <!--                                            <div class="col-xs-12 col-sm-4 col-md-3">-->
                                                    <!--                                                <label>Current Company</label>-->
                                                    <!--                                                <input class="form-control" id="current_company" placeholder="Current Company Name" maxlength="200" name="current_company" type="text"/>-->
                                                    <!--                                            </div>-->
                                                    <!--                                            <div class="col-xs-12 col-sm-4 col-md-3">-->
                                                    <!--                                                <label>Driver License State/Province</label>-->
                                                    <!--                                                <input class="form-control" id="driving_license_state" maxlength="200" placeholder="Driver License State/Province" name="driving_license_state" type="text"/>-->
                                                    <!--                                            </div>-->
                                                    <!--                                            <div class="col-xs-12 col-sm-4 col-md-3">-->
                                                    <!--                                                <label>Driver License #</label>-->
                                                    <!--                                                <input class="form-control" placeholder="Driver License #" name="driving_license" maxlength="200" id="driving_license" type="text"/>-->
                                                    <!--                                            </div>-->
                                                </div>
                                                <div class="row primary-tenant-phone-row">
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Phone Type</label>
                                                        <select class="form-control additional_phoneType" name="additional_phoneType[]">
                                                            <option value="">Select</option>
                                                            <option value="1" selected>Mobile</option>
                                                            <option value="2">Work</option>
                                                            <option value="3">Fax</option>
                                                            <option value="4">Home</option>
                                                            <option value="5">Other</option>
                                                        </select>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Phone Number <em class="red-star">*</em> </label>
                                                        <input class="form-control phone_format customPhonenumbervalidations" data_required="true"  placeholder="Number" name="additional_phone[]" id="phoneNumberCheck" type="text"/>
                                                    </div>
                                                    <div class="work_extension_div" style="display: none;">
                                                        <div class="col-sm-1 col-md-1">
                                                            <label>Extension</label>
                                                            <input class="form-control number_only_for_extension" maxlength="15" type="text" id="other_work_phone_extension" name="other_work_phone_extension[]" placeholder="Eg: + 161">
                                                            <span class="other_work_phone_extensionErr error red-star"></span>
                                                        </div>
                                                        <!--<div class="col-sm-3 col-md-3 ">
                                                            <label>Office/Work Extension <em class="red-star">*</em></label>
                                                            <input class="form-control phone_format customOtherPhonevalidations" data_required="true" type="text" disabled id="work_phone_extension"  name="work_phone_extension[]" placeholder="Eg: 154-175-4301">
                                                            <span class="work_phone_extensionErr error red-star"></span>
                                                            <span class="error red-star customError"></span>
                                                        </div>-->
                                                    </div>

                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Carrier <em class="red-star">*</em></label>
                                                        <select class="form-control additional_carrier customValidationCarrier"
                                                                name="additional_carrier[]" data_required="true"></select>
                                                        <span class="error red-star customError"></span>
                                                    </div>
<!--                                                    <div class="col-xs-12 col-sm-4 col-md-3">-->
<!--                                                        <label>Country Code</label>-->
<!--                                                        <select name="additional_countryCode[]" id="additional_country" class="form-control ">-->
<!--                                                            <option value="0"></option>-->
<!--                                                        </select>-->
<!--                                                        <span class="term_planErr error red-star"></span>-->
<!--                                                    </div>-->
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Country Code</label>
                                                        <select name="additional_countryCode[]" id="additional_country" class="form-control  add-input ">
                                                            <option value="0"></option>
                                                        </select>
                                                        <span class="term_planErr error red-star"></span>
                                                        <a class="add-icon" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>
                                                        <a class="add-icon" href="javascript:;" style=""><i
                                                                    class="fa fa-times-circle red-star"
                                                                    aria-hidden="true"></i></a>
                                                        <span class="error red-star customError"></span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Add Note for this Phone Number </label>
                                                        <input class="form-control add-input capital firstletter" placeholder="Add Note for this Phone Number" id="add_note_phone_number" maxlength="250" name="add_note_phone_number" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Email <em class="red-star">*</em> </label>
                                                        <div class='additional_multipleEmail'>
                                                            <input class="form-control add-input customadditionalEmailValidation" placeholder="Email" maxlength="50" data_required="true" data_email type="text"
                                                                   name="additional_email[]" id="emailCheck">
                                                            <a class="add-icon additional_email-plus-sign"
                                                               href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>


                                                            <a class="add-icon additional_email-remove-sign" href="javascript:;" style="display: none;">
                                                                <i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i>
                                                            </a>
                                                            <span class="customError required"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Select Property</label>
                                                        <select class="form-control" id="property" name="property">
                                                            <option>Undecided</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Select Building</label>
                                                        <select class="form-control" id="building" name="building">
                                                            <option>Undecided</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Select Unit</label>
                                                        <select class="form-control" id="unit" name="unit">
                                                            <option value="">Undecided</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Ethnicity
                                                            <a class="pop-add-icon selectPropertyEthnicity" href="javascript:;">
                                                                <i class="fa fa-plus-circle"aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="ethncity">
                                                            <option value="0">Select</option>
                                                            <option value="1">Male</option>
                                                            <option value="2">Female</option>
                                                        </select>
                                                        <div class="add-popup" id="selectPropertyEthnicity1">
                                                            <h4>Add New Ethnicity</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Ethnicity <em class="red-star">*</em></label>
                                                                        <input class="form-control ethnicity_src customValidateGroup customEthnicityValidation capsOn" type="text" placeholder="Add New Ethnicity" data_required="true" >
                                                                        <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1"  data-table="tenant_ethnicity" data-validation-class="customEthnicityValidation" data-cell="title" data-class="ethnicity_src" data-name="ethncity">Save</button>
                                                                        <button type="button"  class="clear-btn clear_ethnicity">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Martial Status
                                                            <a class="pop-add-icon selectPropertyMaritalStatus" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="maritalStatus">
                                                            <option value="0">Select</option>
                                                        </select>
                                                        <div class="add-popup" id="selectPropertyMaritalStatus1">
                                                            <h4>Add New Marital Status</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Marital Status <em class="red-star">*</em></label>
                                                                        <input class="form-control maritalstatus_src customMaritalValidation capsOn" type="text" placeholder="Add New Marital Status" data_required="true">
                                                                        <span class="customError required red-star" aria-required="true" id="maritalstatus_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_marital_status" data-validation-class="customMaritalValidation" data-cell="marital" data-class="maritalstatus_src" data-name="maritalStatus">Save</button>
                                                                        <button type="button"  class="clear-btn clear_marital_status">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Hobbies
                                                            <a class="pop-add-icon selectPropertyHobbies" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="hobbies[]" id="multiselect_hobbies" multiple>
                                                            <option value="0">Select</option>
                                                        </select>
                                                        <div class="add-popup" id="selectPropertyHobbies1">
                                                            <h4>Add New Hobbies</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Hobbies <em class="red-star">*</em></label>
                                                                        <input class="form-control hobbies_src customHobbiesValidation capsOn" type="text"  placeholder="Add New Hobbies" data_required="true">
                                                                        <span class="customError required red-star" aria-required="true" id="hobbies_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1" data-table="hobbies" data-cell="hobby" data-validation-class="customHobbiesValidation" data-class="hobbies_src" data-name="hobbies">Save</button>
                                                                        <button type="button"  class="clear-btn clear_hobbies">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Veteran Status
                                                            <a class="pop-add-icon selectPropertyVeteranStatus" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="veteranStatus">
                                                            <option value="0">Select</option>
                                                        </select>
                                                        <div class="add-popup" id="selectPropertyVeteranStatus1">
                                                            <h4>Add New VeteranStatus</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New VeteranStatus <em class="red-star">*</em></label>
                                                                        <input class="form-control veteran_src customVeteranValidation capsOn" type="text" placeholder="Add New Veteran Status" data_required="true">
                                                                        <span class="customError required red-star" aria-required="true" id="veteran_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_veteran_status" data-validation-class="customVeteranValidation" data-cell="veteran" data-class="veteran_src" data-name="veteranStatus">Save</button>
                                                                        <button type="button"  class="clear-btn clear_veteran_status">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Form Outer Ends -->
                                        <!--                                <div class="form-outer">-->
                                        <!--                                    <div class="row">-->
                                        <!--                                        <div class="col-xs-12 col-sm-4 col-md-3">-->
                                        <!--                                            <label>Vehicle</label>-->
                                        <!--                                            <div class="check-outer check-marg-none">-->
                                        <!--                                                <input class="select_property_vehicle" type="radio"-->
                                        <!--                                                       name='vehicle' value='1'>-->
                                        <!--                                                <label>Yes</label>-->
                                        <!--                                            </div>-->
                                        <!--                                            <div class="check-outer check-marg-none">-->
                                        <!--                                                <input class="select_property_vehicle" type="radio"-->
                                        <!--                                                       name='vehicle' value='0' checked>-->
                                        <!--                                                <label>No</label>-->
                                        <!--                                                <span class="flast_nameErr error red-star"></span>-->
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                        <!--                                    </div>-->
                                        <!--                                </div>-->
                                        <!-- Form Outer Ends -->

                                        <!-- IF Yes Selected -->
                                        <!---->
                                        <!--                                <div class="grey-box-add form-outer property_vehicle" style="display:none;">-->
                                        <!--                                    <div class="grey-box-add-inner">-->
                                        <!--                                        <div class="grey-box-add-lt">-->
                                        <!--                                            <div class="row">-->
                                        <!--                                                <div class="col-xs-12 col-sm-4 col-md-3">-->
                                        <!--                                                    <label>Vehicle Name</label>-->
                                        <!--                                                    <input class="form-control" maxlength="30" id="vehicle_name" name="vehicle_name[]" type="text">-->
                                        <!--                                                </div>-->
                                        <!---->
                                        <!--                                                <div class="col-xs-12 col-sm-4 col-md-3">-->
                                        <!--                                                    <label>Vehicle #</label>-->
                                        <!--                                                    <input class="form-control" maxlength="30" id="vehicle_number" name="vehicle_number[]"  type="text">-->
                                        <!--                                                </div>-->
                                        <!---->
                                        <!--                                                <div class="col-xs-12 col-sm-4 col-md-3">-->
                                        <!--                                                    <label>Vehicle Type</label>-->
                                        <!--                                                    <input class="form-control" type="text" maxlength="30" id="vehicle_type" name='vehicle_type[]'>-->
                                        <!--                                                    <span class="ffirst_nameErr error red-star"></span>-->
                                        <!--                                                </div>-->
                                        <!---->
                                        <!--                                                <div class="col-xs-12 col-sm-4 col-md-3">-->
                                        <!--                                                    <label>Make</label>-->
                                        <!--                                                    <input class="form-control" maxlength="30" type="text" id="vehicle_make" name='vehicle_make[]'>-->
                                        <!--                                                    <span class="ffirst_nameErr error red-star"></span>-->
                                        <!--                                                </div>-->
                                        <!---->
                                        <!--                                                <div class="col-xs-12 col-sm-4 col-md-3">-->
                                        <!--                                                    <label>Model</label>-->
                                        <!--                                                    <input class="form-control" type="text" maxlength="30" maxlength="12" id="vehicle_model" name='vehicle_model[]'>-->
                                        <!--                                                    <span class="ffirst_nameErr error red-star"></span>-->
                                        <!--                                                </div>-->
                                        <!---->
                                        <!--                                                <div class="col-xs-12 col-sm-4 col-md-3">-->
                                        <!--                                                    <label>Vin</label>-->
                                        <!--                                                    <input class="form-control" type="text" maxlength="30" maxlength="12" id="vehicle_vin" name='vehicle_vin[]'>-->
                                        <!--                                                    <span class="ffirst_nameErr error red-star"></span>-->
                                        <!--                                                </div>-->
                                        <!---->
                                        <!--                                                <div class="col-xs-12 col-sm-4 col-md-3">-->
                                        <!--                                                    <label>Registration #</label>-->
                                        <!--                                                    <input class="form-control" type="text" maxlength="30" id="vehicle_registration" name='vehicle_registration[]'>-->
                                        <!--                                                    <span class="ffirst_nameErr error red-star"></span>-->
                                        <!--                                                </div>-->
                                        <!---->
                                        <!--                                                <div class="col-xs-12 col-sm-4 col-md-3">-->
                                        <!--                                                    <label>License Plate Number</label>-->
                                        <!--                                                    <input class="form-control" type="text" maxlength="30" id="vehicle_plate_number" name='vehicle_plate_number[]'>-->
                                        <!--                                                    <span class="ffirst_nameErr error red-star"></span>-->
                                        <!--                                                </div>-->
                                        <!---->
                                        <!--                                                <div class="col-xs-12 col-sm-4 col-md-3">-->
                                        <!--                                                    <label>Color</label>-->
                                        <!--                                                    <input class="form-control" type="text" maxlength="30" id="vehicle_color" name='vehicle_color[]'>-->
                                        <!--                                                    <span class="ffirst_nameErr error red-star"></span>-->
                                        <!--                                                </div>-->
                                        <!---->
                                        <!--                                                <div class="col-xs-12 col-sm-4 col-md-3">-->
                                        <!--                                                    <label>Year of Vehicle</label>-->
                                        <!--                                                    <select class="form-control" id="vehicle_year"-->
                                        <!--                                                            name="vehicle_year[]">-->
                                        <!--                                                        <option value="">Select</option>-->
                                        <!---->
                                        <!--                                                        --><?php
                                        //                                                        for ($vehiclyear = date("Y"); $vehiclyear >= 1900; $vehiclyear--) {
                                        //                                                            echo '<option value="' . $vehiclyear . '">' . $vehiclyear . '</option>';
                                        //                                                        }
                                        //                                                        ?>
                                        <!--                                                    </select>-->
                                        <!--                                                    <span class="term_planErr error red-star"></span>-->
                                        <!--                                                </div>-->
                                        <!---->
                                        <!--                                                <div class="col-xs-12 col-sm-4 col-md-3">-->
                                        <!--                                                    <label>Date Purchased</label>-->
                                        <!--                                                    <input class="form-control vehicle_date_purchased" readonly id="vehicle_date_purchased" name="vehicle_date_purchased[]" type="text">-->
                                        <!--                                                </div>-->
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                        <!--                                        <div class="grey-box-add-rt">-->
                                        <!--                                            <a class="pop-add-icon copyVehicle" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>-->
                                        <!--                                            <a class="pop-add-icon removeVehicle" href="javascript:;" style="display:none;"><i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i></a>-->
                                        <!--                                        </div>-->
                                        <!--                                    </div>-->
                                        <!--                                </div>-->
                                        <!--If Yes Selected Ends -->


                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Contact Emergency Contact Details</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row additional-tenant-emergency-contact">
                                                    <div class="col-xs-12 col-sm-2 col-md-2">
                                                        <label>Emergency Employee Name</label>
                                                        <input class="form-control capsOn" maxlength="50" type="text"
                                                               id="emergency" name="emergency_contact_name[]">
                                                        <span class="flast_nameErr error red-star"></span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2 col-md-2">
                                                        <label>Relation</label>
                                                        <select class="form-control" id="relationship"
                                                                name="emergency_relation[]">
                                                            <option value="">Select</option>
                                                            <option value="1">Brother</option>
                                                            <option value="2">Daughter</option>
                                                            <option value="3">Employer</option>
                                                            <option value="4">Father</option>
                                                            <option value="5">Friend</option>
                                                            <option value="6">Mentor</option>
                                                            <option value="7">Mother</option>
                                                            <option value="8">Neighbor</option>
                                                            <option value="9">Nephew</option>
                                                            <option value="10">Niece</option>
                                                            <option value="11">Owner</option>
                                                            <option value="12">Partner</option>
                                                            <option value="13">Sister</option>
                                                            <option value="14">Son</option>
                                                            <option value="15">Spouse</option>
                                                            <option value="16">Teacher</option>
                                                            <option value="17">Other</option>

                                                        </select>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2 col-md-2">
                                                        <label>Country Code</label>
                                                        <select class="form-control emergency_additional_countryCode emergency_country" name="emergency_country[]">
                                                            <option value="">Select</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2 col-md-2">
                                                        <label>Phone Number</label>
                                                        <input class="form-control capsOn phone_format" type="text"
                                                               id="phoneNumber"
                                                               name="emergency_phone[]">
                                                        <span class="ffirst_nameErr error red-star"></span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2 col-md-2">
                                                        <label>Email</label>
                                                        <input class="form-control add-input" maxlength="50" type="text"
                                                               name="emergency_email[]">
                                                        <span class="flast_nameErr error red-star"></span>
                                                        <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle additional-add-emergency-contant" aria-hidden="true"></i></a>
                                                        <a class="add-icon additional-remove-emergency-contant" style="display:none;"><i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Form Outer Ends -->

                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Contact Credential Control</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row tenant-credentials-control">
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <label>Credential Name</label>
                                                        <input class="form-control capsOn" id="credentialName" maxlength="30" name="credentialName[]"type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <label>Credential Type
                                                            <a class="pop-add-icon tenantCredentialType" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="credentialType[]">
                                                            <option value="">Select</option>
                                                            <option value="1">Bond</option>
                                                            <option value="2">Certification</option>
                                                        </select>
                                                        <div class="add-popup" id="tenantCredentialType1">
                                                            <h4>Add Credential Type</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer custom-popup-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>Credential Type <em class="red-star">*</em></label>
                                                                        <input class="form-control credential_source customCredentialValidation capsOn" data_required="true" type="text" placeholder="Ex: License">
                                                                        <span class="customError required"></span>
                                                                    </div>
                                                                    <div class="btn-outer">
                                                                        <button type="button" class="blue-btn add_single1" data-validation-class="customCredentialValidation" data-table="tenant_credential_type" data-cell="credential_type" data-class="credential_source" data-name="credentialType[]">Save</button>
                                                                        <button type="button"  class="clear-btn clear_credential_type">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <label>Acquire Date</label>
                                                        <input class="form-control capsOn calander acquireDateclass commonacquireclass" readonly type="text"
                                                               id="acquireDateid" name="acquireDate[]">
                                                        <span class="flast_nameErr error red-star"></span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <label>Expiration Date</label>
                                                        <input class="form-control capsOn calander expirationDateclass commonacquireclass" readonly type="text"
                                                               id="expirationDateid" name="expirationDate[]">
                                                        <span class="flast_nameErr error red-star"></span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <label>Notice Period</label>
                                                        <select class="form-control add-input" id="notice_period"
                                                                name="noticePeriod[]">
                                                            <option value="">Select</option>
                                                            <option value="4" <?= ($_SESSION[SESSION_DOMAIN]['default_notice_period'] == 4) ? 'selected' : '' ?>>5 day</option>
                                                            <option value="1" <?= ($_SESSION[SESSION_DOMAIN]['default_notice_period'] == 1) ? 'selected' : '' ?>>1 Month</option>
                                                            <option value="2" <?= ($_SESSION[SESSION_DOMAIN]['default_notice_period'] == 2) ? 'selected' : '' ?>>2 Month</option>
                                                            <option value="3" <?= ($_SESSION[SESSION_DOMAIN]['default_notice_period'] == 3) ? 'selected' : '' ?>>3 Month</option>
                                                        </select>
                                                        <a class="add-icon add-notice-period" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                        <a class="add-icon remove-notice-period" href="javascript:;" style="display: none;">
                                                            <i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Form Outer Ends -->

                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Notes</h3>
                                            </div>
                                            <div class="form-data ">
                                                <div class="row employee_notes" >
                                                    <div class="col-xs-12 col-sm-4 col-md-4 notes_date_right_div">
                                                        <textarea class="form-control add-input capsOn notesDateTime notes_date_right" maxlength="500" name="employee_notes[]" id="employee_notes"></textarea>
                                                        <a class="add-icon add-employee-notes" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                        <a class="add-icon remove-employee-notes" href="javascript:;" style="display: none;">
                                                            <i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Form Outer Ends -->

                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>File Library</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                        <button type="button" class="orange-btn" id="remove_library_file">Remove All Files</button>
                                                        <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                    </div>
                                                    <div class="row" id="file_library_uploads">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Custom Fields</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="custom_field_html">
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-4 custom_field_msg">
                                                        No Custom Fields
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3 text-right pull-right">
                                                        <label></label>
                                                        <button type="button" id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">Add Custom Field</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Form Outer Ends -->
                                    </div>
                                    <div class="btn-outer text-right" style="padding-right: 15px !important;">
                                        <button type="submit" class="blue-btn employee_submit">Save</button>
                                        <button type="button"  class="grey-btn employee_reset">Clear</button>
                                        <button type="button" class="grey-btn contact_cancel">Cancel</button>

                                    </div>

                                </div>
                                <!--tab Ends -->

                            </div>

                        </div>
                </form>
            </div>
        </div>
    </section>
</div>

<!-- start custom field model -->
<div class="container">
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body" style="height: 380px;">
                    <div class="form-outer col-sm-12">
                        <form id="custom_field">
                            <input type="hidden" id="customFieldModule" name="module" value="contact">
                            <input type="hidden" name="id" id="custom_field_id" value="">
                            <div class="row custom_field_form">
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Field Name <em class="red-star">*</em></label>
                                    </div>
                                    <div class="col-sm-9 field_name">
                                        <input class="form-control" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                        <span class="required error"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Data Type</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control data_type" id="data_type" name="data_type">
                                            <option value="text">Text</option>
                                            <option value="number">Number</option>
                                            <option value="currency">Currency</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="url">URL</option>
                                            <option value="date">Date</option>
                                            <option value="memo">Memo</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Default value</label>
                                    </div>
                                    <div class="col-sm-9 default_value">
                                        <input class="form-control default_value" id="default_value" type="text" name="default_value" placeholder="">
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Required Field</label>
                                    </div>
                                    <div class="col-sm-9 is_required">
                                        <select class="form-control" name="is_required" id="is_required">
                                            <option value="1">Yes</option>
                                            <option value="0" selected="selected">No</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-outer text-right">

                                <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                <button type="button"  class="clear-btn clear_custom_filed_popup">Clear</button>
                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End custom field model -->
<!-- Wrapper Ends -->
<!-- Footer Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->
<script>
    var date_format = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format'] ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var credential_notice_period =  "<?php echo $_SESSION[SESSION_DOMAIN]['default_notice_period']; ?>";
</script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/contacts/contacts.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/commonPopup.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/building/buildingFileLibrary.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/building/buildingPhotoVideos.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
    setTimeout(function(){
        var hobbies_total_length  =      $('.multiselect-container input:checkbox').length -1;
        $(document).on("click",".checkbox",function(){
            alert('dfsd');
        });
    }, 2000);
    $(document).on("click",".goback_func",function () {
        window.history.back();
    });
    <!--- Accordians -->
</script>

<script>
    var default_form_data = '';
    var defaultIgnoreArray = [];
    $(document).on('click','.referal_source_clear',function(){
        resetFormClear('#additionalReferralResource1',[],'div',false,default_form_data,defaultIgnoreArray);
    });

    $(document).on('click','.clear_ethnicity',function(){
        resetFormClear('#selectPropertyEthnicity1',[],'div',false,default_form_data,defaultIgnoreArray);
    });

    $(document).on('click','.clear_ethnicity',function(){
        resetFormClear('#selectPropertyEthnicity1',[],'div',false,default_form_data,defaultIgnoreArray);
    });

    $(document).on('click','.clear_marital_status',function(){
        resetFormClear('#selectPropertyMaritalStatus1',[],'div',false,default_form_data,defaultIgnoreArray);
    });

    $(document).on('click','.clear_hobbies',function(){
        resetFormClear('#selectPropertyHobbies1',[],'div',false,default_form_data,defaultIgnoreArray);
    });
    $(document).on('click','.clear_veteran_status',function(){
        resetFormClear('#selectPropertyVeteranStatus1',[],'div',false,default_form_data,defaultIgnoreArray);
    });
    $(document).on('click','.clear_credential_type',function(){
        resetFormClear('#tenantCredentialType1',[],'div',false,default_form_data,defaultIgnoreArray);
    });

    $(document).on('click','.clear_custom_filed_popup',function(){
        resetFormClear('#custom_field',['data_type','is_required'],'form',false,default_form_data,defaultIgnoreArray);
    });




</script>





<!-- Jquery Starts -->

</body>

</html>