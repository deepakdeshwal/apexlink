<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>
<style>
    .panel-htop .combo-panel {
        margin-top: 8px;
        margin-left: -2px;
        background: #fff;
    }
</style>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Flag Bank
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--  add flag div start -->
                    <div  id="add_flag_type_div" style="display: none;">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 id="headerDiv" class="panel-title">Add Flag Bank</h4>
                            </div>
                            <div class="panel-body">
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <form id="flagForm">
                                    <input type="hidden" id="object_type" value="">
                                    <input type="hidden" id="object_id" value="">
                                    <input type="hidden" id="property_id" value="">
                                    <input type="hidden" id="building_id" value="">
                                    <input type="hidden" id="unit_id" value="">

                                        <div class="col-sm-12" >
                                            <div class="grey-box-add">
                                                <div class="form-outer">
                                                    <input type="hidden" name="id" id="flag_id" style="text-transform: capitalize;" value="">
                                                    <div class="col-xs-12 col-sm-3 combo_div">
                                                        <label id="dynamicLabel">Flagged By <em class="red-star">*</em></label>
                                                        <input class="form-control capital dynamicInput" name="object_name" id="object_name_id" placeholder="Flagged By" maxlength="100" type="text" value="" style="text-transform: capitalize;" aria-required="true">
                                                    </div>
                                                    <div class="dynamicDiv">

                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Date</label>
                                                        <input class="form-control" name="date" id="flag_flag_date" readonly="" value="" type="text" style="text-transform: capitalize;">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Flag Name <em class="red-star">*</em></label>
                                                        <input class="form-control capital" name="flag_name" id="flag_flag_name" maxlength="100" placeholder="Please Enter the Name of this Flag" type="text" value="" style="text-transform: capitalize;" aria-required="true">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Country Code</label>
                                                        <select class="form-control" name="country_code" id="flag_country_code12"><option value="1">Abkhazia(+7 840)</option><option value="2">Afghanistan(+93)</option><option value="3">Albania(+355)</option><option value="4">Algeria(+213)</option><option value="5">American Samoa(+1 684)</option><option value="6">Andorra(+376)</option><option value="7">Angola(+244)</option><option value="8">Anguilla(+1 264)</option><option value="9">Antigua and Barbuda(+1 268)</option><option value="10">Argentina(+54)</option><option value="11">Armenia(+374)</option><option value="12">Aruba(+297)</option><option value="13">Ascension(+247)</option><option value="14">Australia(+61)</option><option value="15">Austria(+43)</option><option value="16">Azerbaijan(+994)</option><option value="17">Bahamas(+1 242)</option><option value="18">Bahrain(+973)</option><option value="19">Bangladesh(+880)</option><option value="20">Barbados(+1 246)</option><option value="21">Belarus(+375)</option><option value="22">Belgium(+32)</option><option value="23">Belize(+501)</option><option value="24">Benin(+229)</option><option value="25">Bermuda(+1 441)</option><option value="26">Bhutan(+975)</option><option value="27">Bolivia(+591)</option><option value="28">Bosnia and Herzegovina(+387)</option><option value="29">Botswana(+267)</option><option value="30">Brazil(+55)</option><option value="31">British Indian Ocean Territory(+246)</option><option value="32">British Virgin Islands(+1 284)</option><option value="33">Brunei(+673)</option><option value="34">Bulgaria(+359)</option><option value="35">Burkina Faso(+226)</option><option value="36">Burundi(+257)</option><option value="37">Cambodia(+855)</option><option value="38">Cameroon(+237)</option><option value="39">Canada(+ 1)</option><option value="40">Cape Verde(+238)</option><option value="41">Cayman Islands(+ 345)</option><option value="42">Central African Republic(+236)</option><option value="43">Chad(+235)</option><option value="44">Chile(+56)</option><option value="45">China(+86)</option><option value="46">Cocos-Keeling Islands(+891)</option><option value="47">Colombia(+57)</option><option value="48">Comoros(+269)</option><option value="49">Congo(+242)</option><option value="50">Congo, Dem. Rep. of (+243)</option><option value="51">Cook Islands(+682)</option><option value="52">Costa Rica(+506)</option><option value="53">Croatia(+385)</option><option value="54">Cuba(+53)</option><option value="55">Curacao(+599)</option><option value="56">Cyprus(+537)</option><option value="57">Czech Republic(+420)</option><option value="58">Denmark(+45)</option><option value="59">Djibouti(+253)</option><option value="60">Dominica(+1 767)</option><option value="61">Dominican Republic(+1 809)</option><option value="62">Easter Island(+56)</option><option value="63">Ecuador(+593)</option><option value="64">Egypt(+20)</option><option value="65">El Salvador(+503)</option><option value="66">Equatorial Guinea(+240)</option><option value="67">Eritrea(+291)</option><option value="68">Estonia(+372)</option><option value="69">Ethiopia(+251)</option><option value="70">Falkland Islands(+500)</option><option value="71">Faroe Islands(+298)</option><option value="72">Fiji(+679)</option><option value="73">Finland(+358)</option><option value="74">France(+33)</option><option value="75">French Antilles(+596)</option><option value="76">French Guiana(+594)</option><option value="77">French Polynesia(+689)</option><option value="78">Gabon(+241)</option><option value="79">Gambia(+220)</option><option value="80">Georgia(+995)</option><option value="81">Germany(+49)</option><option value="82">Ghana(+233)</option><option value="83">Gibraltar(+350)</option><option value="84">Greece(+30)</option><option value="85">Greenland(+299)</option><option value="86">Grenada(+1 473)</option><option value="87">Guadeloupe(+590)</option><option value="88">Guam(+1 671)</option><option value="89">Guatemala(+502)</option><option value="90">Guinea(+224)</option><option value="91">Guinea-Bissau(+245)</option><option value="92">Guyana(+595)</option><option value="93">Haiti(+509)</option><option value="94">Honduras(+504)</option><option value="95">Hong Kong SAR China(+852)</option><option value="96">Hungary(+36)</option><option value="97">Iceland(+354)</option><option value="98">India(+91)</option><option value="99">Indonesia(+62)</option><option value="100">Iran(+98)</option><option value="101">Iraq(+964)</option><option value="102">Ireland(+353)</option><option value="103">Israel(+972)</option><option value="104">Italy(+39)</option><option value="105">Ivory Coast(+225)</option><option value="106">Jamaica(+1 876)</option><option value="107">Japan(+81)</option><option value="108">Jordan(+962)</option><option value="109">Kazakhstan(+7 7)</option><option value="110">Kenya(+254)</option><option value="111">Kiribati(+686)</option><option value="112">Kuwait(+965)</option><option value="113">Kyrgyzstan(+996)</option><option value="114">Laos(+856)</option><option value="115">Latvia(+371)</option><option value="116">Lebanon(+961)</option><option value="117">Lesotho(+266)</option><option value="118">Liberia(+231)</option><option value="119">Libya(+218)</option><option value="120">Liechtenstein(+423)</option><option value="121">Lithuania(+370)</option><option value="122">Luxembourg(+352)</option><option value="123">Macau SAR China(+853)</option><option value="124">Macedonia(+389)</option><option value="125">Madagascar(+261)</option><option value="126">Malawi(+265)</option><option value="127">Malaysia(+60)</option><option value="128">Maldives(+960)</option><option value="129">Mali(+223)</option><option value="130">Malta(+356)</option><option value="131">Marshall Islands(+692)</option><option value="132">Martinique(+596)</option><option value="133">Mauritania(+222)</option><option value="134">Mauritius(+230)</option><option value="135">Mayotte(+262)</option><option value="136">Mexico(+52)</option><option value="137">Micronesia(+691)</option><option value="138">Midway Island(+1 808)</option><option value="139">Moldova(+373)</option><option value="140">Monaco(+377)</option><option value="141">Mongolia(+976)</option><option value="142">Montenegro(+382)</option><option value="143">Montserrat(+1664)</option><option value="144">Morocco(+212)</option><option value="145">Myanmar(+95)</option><option value="146">Namibia(+264)</option><option value="147">Nauru(+674)</option><option value="148">Nepal(+977)</option><option value="149">Netherlands(+31)</option><option value="150">Netherlands Antilles(+599)</option><option value="151">Nevis(+1 869)</option><option value="152">New Caledonia(+687)</option><option value="153">New Zealand(+64)</option><option value="154">Nicaragua(+505)</option><option value="155">Niger(+227)</option><option value="156">Nigeria(+234)</option><option value="157">Niue(+683)</option><option value="158">Norfolk Island(+672)</option><option value="159">North Korea(+850)</option><option value="160">Northern Mariana Islands(+1 670)</option><option value="161">Norway(+47)</option><option value="162">Oman(+968)</option><option value="163">Pakistan(+92)</option><option value="164">Palau(+680)</option><option value="165">Palestinian Territory(+970)</option><option value="166">Panama(+507)</option><option value="167">Papua New Guinea(+675)</option><option value="168">Paraguay(+595)</option><option value="169">Peru(+51)</option><option value="170">Philippines(+63)</option><option value="171">Poland(+48)</option><option value="172">Portugal(+351)</option><option value="173">Puerto Rico(+1 787)</option><option value="174">Qatar(+974)</option><option value="175">Reunion(+ 262)</option><option value="176">Romania(+40)</option><option value="177">Russia(+7)</option><option value="178">Rwanda(+250)</option><option value="179">Samoa(+685)</option><option value="180">San Marino(+378)</option><option value="181">Saudi Arabia(+966)</option><option value="182">Senegal(+221)</option><option value="183">Serbia(+381)</option><option value="184">Seychelles(+248)</option><option value="185">Sierra Leone(+232)</option><option value="186">Singapore(+65)</option><option value="187">Slovakia(+421)</option><option value="188">Slovenia(+386)</option><option value="189">Solomon Islands(+677)</option><option value="190">South Africa(+27)</option><option value="191">South Georgia and the South Sandwich Islands(+ 500)</option><option value="192">South Korea(+82)</option><option value="193">Spain(+34)</option><option value="194">Sri Lanka(+94)</option><option value="195">Sudan(+249)</option><option value="196">Suriname(+597)</option><option value="197">Swaziland(+268)</option><option value="198">Sweden(+46)</option><option value="199">Switzerland(+41)</option><option value="200">Syria(+963)</option><option value="201">Taiwan(+886)</option><option value="202">Tajikistan(+992)</option><option value="203">Tanzania(+255)</option><option value="204">Thailand(+66)</option><option value="205">Timor Leste(+670)</option><option value="206">Togo(+228)</option><option value="207">Tokelau(+690)</option><option value="208">Tonga(+676)</option><option value="209">Trinidad and Tobago(+1 868)</option><option value="210">Tunisia(+216)</option><option value="211">Turkey(+90)</option><option value="212">Turkmenistan(+993)</option><option value="213">Turks and Caicos Islands(+1 649)</option><option value="214">Tuvalu(+688)</option><option value="215">U.S. Virgin Islands(+1 340)</option><option value="216">Uganda(+256)</option><option value="217">Ukraine(+380)</option><option value="218">United Arab Emirates(+971)</option><option value="219">United Kingdom(+44)</option><option value="220" selected="">United States(+1)</option><option value="221">Uruguay(+598)</option><option value="222">Uzbekistan(+998)</option><option value="223">Vanuatu(+678)</option><option value="224">Venezuela(+58)</option><option value="225">Vietnam(+84)</option><option value="226">Wake Island(+1 808)</option><option value="227">Wallis and Futuna(+681)</option><option value="228">Yemen(+967)</option><option value="229">Zambia(+260)</option><option value="230">Zimbabwe(+263)</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Phone Number</label>
                                                        <input class="form-control phone_number phone_number_format" name="flag_phone_number" id="flag_phone_number" maxlength="12" value="" placeholder="123-456-7890" type="text" style="text-transform: capitalize;">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Flag Reason</label>
                                                        <input class="form-control capital" name="flag_reason" id="flag_flag_reason" maxlength="100" placeholder="Flag Reason" type="text" style="text-transform: capitalize;">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Completed</label>
                                                        <select class="form-control" name="completed" id="completed">
                                                            <option value="0">No</option>
                                                            <option value="1">Yes</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Flagged By <em class="red-star">*</em></label>
                                                        <input class="form-control capital" name="flag_by" id="flag_flag_by" placeholder="Flagged By" maxlength="100" type="text" value="" style="text-transform: capitalize;" aria-required="true">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Note</label>
                                                        <div class="notes_date_right_div">
                                                            <textarea class="form-control notes_date_right capital" name="flag_note" id="flag_note" placeholder="Flag Notes"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="btn-outer text-right">
                                                        <button class="blue-btn" type="submit" id="flagSaveBtnId">Save</button>
                                                        <button class="clear-btn" type="button" id="clearFlagSaveBtnId">Clear</button>
                                                        <button class="grey-btn" type="button" id="flagCancel">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  add flag div end -->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div role="tabpanel" class="" id="people-vendor">
                                <div class="property-status">
                                    <div class="col-sm-12">
                                        <div class="col-sm-2">
                                            <select id="flag_type" class="common_ddl fm-txt form-control">
                                                <option value="all">All</option>
                                                <option value="property">Property</option>
                                                <option value="tenant">Tenant</option>
                                                <option value="owner">Owner</option>
                                                <option value="vendor">Vendor</option>
                                                <option value="contact">Contact</option>
                                                <option value="employee">Employee</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="btn-outer text-right addFlagButton">
                                                <a href="javascript:void(0)" class="blue-btn margin-right" id="flagTypeModal">Add Flag</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">

                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive overflow-unset">
                                                                        <table id="flagBankTable" class="table table-bordered">
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tabs Ends -->
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->
<div class="modal fade" id="flagTypeDivModal" role="dialog">
    <div class="modal-dialog modal-sm" style="width:400px;">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: unset;background: #05A0E4;">
                <button type="button" class="bootbox-close-button close closeRating" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 style="padding:5px;color: #fff;font-weight: 500;">Flag Type</h4>
            </div>
            <div class="modal-body" id="flagTypeDiv" style="height:190px;">
                <div class="form-outer2">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="radioDiv" style="margin-left: 10px;"><input type="radio" id="tenant_radio" data_label="Tenant Name" data_name="object_name" checked name="flag_type"  value="tenant"/>Tenant</div>
                        </div>
                        <div class="col-sm-6">
                            <div class="radioDiv" style="margin-left: 10px;"><input type="radio" id="property_radio" name="property_radio" data_label="Property Name" data_name="object_name"  value="property"/>Property</div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="radioDiv" style="margin-left: 10px;"><input type="radio" data_label="Owner Name" data_name="object_name" name="flag_type"  value="owner"/>Owner</div>
                        </div>
                        <!--  <div class="col-sm-6">
                             <div class="radioDiv" style="margin-left: 10px;"><input type="radio" id="building_radio" name="building_radio" disabled data_label="Building Name" data_name="object_name"  value="building"/>Building</div>
                         </div> -->
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="radioDiv"  style="margin-left: 10px;"><input type="radio" data_label="Vendor Name" data_name="object_name" name="flag_type"  value="vendor"/>Vendor</div>
                        </div>
                        <!-- <div class="col-sm-6">
                            <div class="radioDiv" style="margin-left: 10px;"><input type="radio" id="unit_radio" name="unit_radio" disabled data_label="Unit Name" data_name="object_name"  value="unit"/>Unit</div>
                        </div> -->
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="radioDiv" style="margin-left: 10px;"><input type="radio" data_label="Contact Name" data_name="object_name" name="flag_type"  value="contact"/>Contact</div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="radioDiv" style="margin-left: 10px;"><input type="radio" data_label="Employee Name" data_name="object_name" name="flag_type"  value="employee"/>Employee</div>
                        </div>
                    </div>
                    <!--                    <div class="col-sm-12">-->
                    <!--                        <div class="col-sm-6">-->
                    <!--                            <div class="radioDiv" style="margin-left: 10px;"><input type="radio" data_label="Guest Card" data_name="object_name" name="flag_type"  value="guest"/>Guest Card</div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="col-sm-12">-->
                    <!--                        <div class="col-sm-6">-->
                    <!--                            <div class="radioDiv" style="margin-left: 10px;"><input type="radio" data_label="Wating List" data_name="object_name" name="flag_type"  value="wating"/>Wating List</div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="col-sm-12">-->
                    <!--                        <div class="col-sm-6">-->
                    <!--                            <div class="radioDiv" style="margin-left: 10px;"><input type="radio" data_label="Short Term Rental" data_name="object_name" name="flag_type"  value="short"/>Short-Term Renter</div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="col-sm-12">-->
                    <!--                        <div class="col-sm-6">-->
                    <!--                            <div class="radioDiv" style="margin-left: 10px;"><input type="radio" data_label="Rental Application" data_name="object_name" name="flag_type"  value="rental"/>Rental Application</div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->


                    <div class="btn-outer text-center">
                        <input type="button" id="addFlag" class="blue-btn" value="OK">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Jquery Starts -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var default_name = '<?php echo $_SESSION[SESSION_DOMAIN]['default_name'];?>';
    var default_number= '<?php echo $_SESSION[SESSION_DOMAIN]['phone_number'];?>';
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.easyui.min.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/flag/comboGrid.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/flag/globalFlag.js"></script>

<script>
    $('#flagTypeModal').on('click', function() {
        $('#flagTypeDivModal').modal('show');
    });
    function getNameAndPhone() {
        $('#flag_flag_by').val('<?php if (isset($_SESSION[SESSION_DOMAIN]['name'])) echo $_SESSION[SESSION_DOMAIN]['name'] ?>');
        $('#flag_phone_number').val('<?php if (isset($_SESSION[SESSION_DOMAIN]['phone_number'])) echo $_SESSION[SESSION_DOMAIN]['phone_number'] ?>');
    }

</script>
<!-- Jquery Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
