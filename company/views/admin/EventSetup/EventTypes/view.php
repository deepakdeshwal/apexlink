<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Event >> <span>Event Type</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">

                            <div class="property-status">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <select id="jqGridStatus" data-module="ADMIN-SETTINGS" class="jqGridStatusClass fm-txt form-control">
                                            <option value="All">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>

                                    </div>
                                    <div class="col-sm-10">
                                        <div class="btn-outer text-right">
                                            <button id="addUnitTypeButton" class="blue-btn">Add Event Type</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Main tabs -->
                            <div class="main-tabs">
                                <div class="form-outer" style="display: none;" id="add_unit_type_div">
                                    <div class="form-hdr">
                                        <h3 id="headerDiv">Add Event Type</h3>
                                    </div>
                                    <div class="form-data">
                                        <form name="add_event_type" id="add_event_type">
                                                <div class="row">
                                                    <div class="form-outer2">
                                                        <div class="col-xs-12 col-sm-3">
                                                            <input type="hidden" id="user_id_hidden" name="user_id_hidden">
                                                            <label>Event Type <em class="red-star">*</em></label>
                                                            <input name="event_type" id="event_type" placeholder="Eg: Move In" maxlength="100"   class="form-control disable_edit" type="text"/>
                                                            <span id="event_typeErr" class="error"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3">
                                                            <label>Description <em class="red-star">*</label>
                                                            <textarea name="description" id="description" placeholder="Eg: Move In" maxlength="500" class="form-control"></textarea>
                                                            <span id="descriptionErr" class="error"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 clear">
                                                            <label>
                                                                <div class="check-outer">
                                                                    <input name="is_default" id="is_default" type="checkbox"/>
                                                                    <label>Set as Default</label>
                                                                </div>
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>

                                        </form>
                                    </div>
                                        <div class="btn-outer text-right">
                                            <input type="hidden" name="unit_type_id" class="form-control" id="unit_type_id" />
                                            <input type="submit" value="Save" class="blue-btn" id="saveBtns"/>
                                            <input type='button'  value="Clear" class="clear-btn clearFormReset" >
                                            <input type='button'  value="Reset" class="clear-btn resetbtn" style="display: none;" >
                                            <button type="button" id="add_unit_cancel_btn" class="grey-btn">Cancel</button>
                                        </div>
                                </div>
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <!--Add unit type div starts here-->
<!--                                                <div  style="display: none;" id="add_unit_type_div">-->
<!--                                                    <div class="panel panel-default">-->
<!--                                                        <div class="panel-heading">-->
<!--                                                            <h4 id="headerDiv" class="panel-title">-->
<!--                                                                Add Event Type-->
<!--                                                            </h4>-->
<!--                                                        </div>-->
<!--                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">-->
<!--                                                            <form name="add_event_type" id="add_event_type">-->
<!--                                                                <div class="panel-body">-->
<!--                                                                    <div class="row">-->
<!--                                                                        <div class="form-outer">-->
<!--                                                                            <div class="col-xs-12 col-sm-4">-->
<!--                                                                                <input type="hidden" id="user_id_hidden" name="user_id_hidden">-->
<!--                                                                                <label>Event Type <em class="red-star">*</em></label>-->
<!--                                                                                <input name="event_type" id="event_type" maxlength="100"   class="form-control disable_edit" type="text"/>-->
<!--                                                                                <span id="unit_typeErr"></span>-->
<!--                                                                            </div>-->
<!--                                                                            <div class="col-xs-12 col-sm-4">-->
<!--                                                                                <label>Description</label>-->
<!--                                                                                <textarea name="description" id="description" maxlength="500" class="form-control"></textarea>-->
<!--                                                                            </div>-->
<!--                                                                            <div class="col-xs-12 col-sm-4">-->
<!--                                                                                <label>-->
<!--                                                                                    <div class="check-outer">-->
<!--                                                                                        <input name="is_default" id="is_default" type="checkbox"/>-->
<!--                                                                                        <label>Set as Default</label>-->
<!--                                                                                    </div>-->
<!--                                                                                </label>-->
<!--                                                                            </div>-->
<!--                                                                        </div>-->
<!--                                                                        <div class="col-xs-12">-->
<!--                                                                            <div class="btn-outer">-->
<!--                                                                                <input type="hidden" name="unit_type_id" class="form-control" id="unit_type_id" />-->
<!--                                                                                <input type="submit" value="Save" class="blue-btn" id="saveBtns"/>-->
<!--                                                                                <button type="button" id="add_unit_cancel_btn" class="grey-btn">Cancel</button>-->
<!--                                                                            </div>-->
<!--                                                                        </div>-->
<!--                                                                    </div>-->
<!--                                                                </div>-->
<!--                                                            </form>-->
<!--                                                        </div>-->
<!--                                                    </div>-->
<!--                                                </div>-->
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table id="eventType-table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--List unit type div ends here-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $('#leftnav8').addClass('in');
    $('.event_type').addClass('active');
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/eventTypes.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/common.js"></script>
<!-- Jquery Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>