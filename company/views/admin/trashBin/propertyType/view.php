<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- MAIN Navigation Ends -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="welcome-text visible-xs">
                        <div class="welcome-text-inner">
                            <div class="col-xs-9">    Welcome:  Sonny Kesseben (ACL Properties), June 01, 2018
                            </div>
                            <div class="col-xs-3">
                                <a herf="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                                <a href="javascript:;"><i class="fa fa-calculator" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="breadcrumb-outer">
                                        Trash Bin >> <span>Property Type</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="easy-search">
                                        <input placeholder="Easy Search" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-data">
                            <form name="add_property_type" id="add_property_type" action="../../function/propertytype.php" method="post" enctype="multipart/form-data" >
                            <input type="hidden" class="restoreData" id="restoreData" value="">
                                <!-- Main tabs -->
                                <div class="main-tabs">
                                    <div class="accordion-grid" >
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">

                                                    <div class="panel panel-default" style="display: none;" id="addPropertyTypeForm" >
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" id="property_type_span" data-parent="#accordion" aria-expanded="true" class=""><span class=""></span> Add Property Type</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
<!--                                                                        <input class="form-control property_type" id="form_type" name="form_type" type="hidden" value="add" type="text"/>
-->                                                                        <input class="form-control property_type_id" id="property_type_id" name="property_type_id" type="hidden"  type="text"/>
                                                                        <div class="col-xs-12 col-sm-4">
                                                                            <label>Property Type <em class="red-star">*</em></label>
                                                                            <input class="form-control property_type" maxlength="100" name="property_type" id="property_type" placeholder="Eg: GS" type="text"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4">
                                                                            <label>Description</label>
                                                                            <textarea class="form-control description" maxlength="500" name="description" placeholder="Eg: Garden Style" id="description"></textarea>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4">
                                                                            <label>
                                                                                <div class="check-outer">
                                                                                    <input id="is_default" name="is_default" type="checkbox"/>
                                                                                    <label>Set as Default</label>
                                                                                </div>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <div class="btn-outer">
                                                                            <input type="submit" value="Save" class="blue-btn" id="save_property_type"/>
                                                                            <input  type="button" value="Cancel" class="grey-btn" id="property_type_cancel"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                            </form>
                            <div class="panel panel-default" style="display: none;" id="ImportProperty">
                                <form name="importPropertyForm" id="importPropertyForm"  enctype="multipart/form-data" >
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion"   class=""><span class=""></span> Import Property Type</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="form-outer">
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <input type="file" name="import_file" id="import_file"   accept=".csv, .xls, .xlsx" required/>
                                                        <span class="error"></span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="btn-outer">
                                                        <input type="submit" value="Submit" class="blue-btn" id="save_import_file"/>
                                                        <input  type="button" value="Cancel" class="grey-btn" id="import_type_cancel"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="panel panel-default">
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <div class="panel-body pad-none">
                                        <div class="grid-outer">
                                            <div class="apx-table">
                                                <div class="table-responsive">
                                                    <table id="properttype-table" class="table table-bordered"></table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Content Data Ends ---->

<!-- Wrapper Ends -->


<!-- Footer Ends -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $('#leftnav10').addClass('in');
    $('.property_type   ').addClass('active');
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/trashBin/trashProperty.js"></script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>