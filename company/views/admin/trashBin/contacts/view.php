<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end --> <!-- MAIN Navigation Ends -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>
                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="breadcrumb-outer">
                                        Trash Bin >> <span>Contacts</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="easy-search">
                                        <input placeholder="Easy Search" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-data">

                            <!-- Main tabs -->
                            <div class="main-tabs">
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div id="addAmenitiesDivId" style="display: none;">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 id="headerDiv" class="panel-title">
                                                                New Amenity
                                                            </h4>
                                                        </div>
                                                        <div class="panel-body">
                                                            <form name="addAmenity" id="addAmenityFormId">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-xs-12 col-sm-3">
                                                                            <label>Amenity Name <em class="red-star">*</em></label>
                                                                            <input id="amenity_name" name="name" maxlength="100" class="form-control" type="text"/>
                                                                            <span id="nameErr" class="error"></span>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-3">
                                                                            <label>Amenity Code</label>
                                                                            <input id="code" name="code" maxlength="10" class="form-control" type="text"/>
                                                                            <span id="codeErr" class="error"></span>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-3">
                                                                            <label>Select Type <em class="red-star">*</em></label>
                                                                            <div class="check-outer">
                                                                                <input id="amenity1" class="type_checkbox" value="1" name="type" type="checkbox"/>
                                                                                <label>Property</label>
                                                                            </div>
                                                                            <div class="check-outer">
                                                                                <input id="amenity2" class="type_checkbox" value="2" name="type" type="checkbox"/>
                                                                                <label>Building</label>
                                                                            </div>
                                                                            <div class="check-outer">
                                                                                <input id="amenity3" class="type_checkbox" value="3" name="type" type="checkbox"/>
                                                                                <label>Unit</label>
                                                                            </div>
                                                                            <div style="margin-top: 30px;">
                                                                                <span id="typeErr"  class="error"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <div class="btn-outer">
                                                                            <input class="form-control" type="hidden" name="hidden_amenity_id" id="hidden_amenity_id"/>
                                                                            <input class="blue-btn" type="submit" value="Save" id="saveAmenityBtnId"/>
                                                                            <button id="addAmenityCancelBtn" class="grey-btn">Cancel</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--List Amenities div ends here-->
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#">
                                                                                <span class="pull-right"></span> List of Contacts</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                        <div class="panel-body pad-none">
                                                                            <div class="grid-outer">
                                                                                <div class="table-responsive">
                                                                                    <table class="table table-hover table-dark">

                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <th scope="col">Contact Name</th>
                                                                                            <th scope="col">Email</th>
                                                                                            <th scope="col">Phone</th>
                                                                                            <th scope="col">Created</th>

                                                                                            <th scope="col">Status</th>
                                                                                            <th scope="col">Actions</th>
                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>Asron Properties</a></td>

                                                                                            <td>ashleym524@gmail.com</td>
                                                                                            <td>555-444-6666</td>
                                                                                            <td>create</td>
                                                                                            <td>Active</td>
                                                                                            <td><img src="http://shrey.apex.local/company/images/icon6.png" id="restoreProperty" alt="my image"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Asron Properties</a></td>

                                                                                            <td>ashleym524@gmail.com</td>
                                                                                            <td>555-444-6666</td>
                                                                                            <td>create</td>
                                                                                            <td>Active</td>
                                                                                            <td><img src="http://shrey.apex.local/company/images/icon6.png" id="restoreProperty" alt="my image"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Asron Properties</a></td>

                                                                                            <td>ashleym524@gmail.com</td>
                                                                                            <td>555-444-6666</td>
                                                                                            <td>create</td>
                                                                                            <td>Active</td>
                                                                                            <td><img src="http://shrey.apex.local/company/images/icon6.png" id="restoreProperty" alt="my image"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Asron Properties</a></td>

                                                                                            <td>ashleym524@gmail.com</td>
                                                                                            <td>555-444-6666</td>
                                                                                            <td>create</td>
                                                                                            <td>Active</td>
                                                                                            <td><img src="http://shrey.apex.local/company/images/icon6.png" id="restoreProperty" alt="my image"></td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--List Amenities div ends here-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Content Data Ends ---->
                                </div>
                            </div>
                        </div>
                    </div>
    </section>
</div>
<!-- Wrapper Ends -->


<!-- Footer Ends -->
<!-- Jquery Starts -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $('#leftnav10').addClass('in');
    $('.amenity6').addClass('active');
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/trashBin/trashAmenities.js"></script>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
