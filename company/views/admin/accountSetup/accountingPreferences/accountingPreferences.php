<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Accounting >> <span>Accounting Preferences</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">
                            <!-- Main tabs -->
                            <div class="main-tabs">

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <!--Add unit type div starts here-->
                                                <div  id="add_unit_type_div">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 id="headerDiv" class="panel-title">
                                                                Accounting Preferences
                                                            </h4>
                                                        </div>
                                                        <div id="" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <div id="viewMode" style="display: block">
                                                                <form name="add_property_type" id="add_unit_type_form">
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <div class="account_preference_view">
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Fiscal Year First Month :</label>
                                                                                    <span class="fiscal_year_first_month"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Cash Account (Credit) for Bill Posting :</label>
                                                                                    <span class="cash_account_credit_bill_posting"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Rent Charges :</label>
                                                                                    <span class="rent_charges "></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Check Receive Charge :</label>
                                                                                    <span class="check_receive_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Debit Type Receive Payment Charge :</label>
                                                                                    <span class="debit_type_receive_payment_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Vacancy Charge :</label>
                                                                                    <span class="vacancy_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Waive-Off on Payment Received Charge :</label>
                                                                                    <span class="waiveoff_on_payment_received_charge "></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Bad Debt Charge :</label>
                                                                                    <span class="bad_debit_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Write-Off Charge :</label>
                                                                                    <span class="write_off_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Money Order Payment Received Charge :</label>
                                                                                    <span class="money_order_payment_received_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Auto Debit Payment Received Charge :</label>
                                                                                    <span class="auto_debit_payment_receive_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Cash Payment Received Charge :</label>
                                                                                    <span class="cash_payment_received_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">NSF Charge :</label>
                                                                                    <span class="nsf_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Check Bounced Charge :</label>
                                                                                    <span class="check_bounce_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Check Issue to Vendor/Tenant Charge :</label>
                                                                                    <span class="check_issue_vendor_tenant_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Refund Charge :</label>
                                                                                    <span class="refund_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Interest Received from Bank Charge :</label>
                                                                                    <span class="interest_received_from_bank_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Bank Adjustment Fee Charge :</label>
                                                                                    <span class="bank_adjustment_fee_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Concession Charge :</label>
                                                                                    <span class="concession_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Transfer Charge :</label>
                                                                                    <span class="transfer_fee_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Late Fee Charge :</label>
                                                                                    <span class="late_fee_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Discount Account Id :</label>
                                                                                    <span class="discount_account_id"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">EFT Generate Date :</label>
                                                                                    <span class="eft_generate_date"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">EFT Charge :</label>
                                                                                    <span class="eft_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">CAM Charges :</label>
                                                                                    <span class="cam_charges"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Warn if Transaction are Day(s) in Past :</label>
                                                                                    <span class="warn_transaction_days_past"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Warn if Transaction are Day(s) in Future :</label>
                                                                                    <span class="warn_transaction_days_future"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Auto Invoice :</label>
                                                                                    <span class="auto_invoice"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">SD Refund Charge :</label>
                                                                                    <span class="sd_refund_charge"></span>
                                                                                </div>
                                                                                <div class="col-sm-6 autoInvoiceDivForView">
                                                                                    <label class="blue-label">Auto Invoice Days Before End of Month :</label>
                                                                                    <span class="auto_invoice_days_before_end_of_month"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Use Parenthesis Instead of a Minus Sign :</label>
                                                                                    <span class="use_parenthesis_instead_of_a_minus_sign"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Color Negative Balances in Red :</label>
                                                                                    <span class="color_negative_balances_in_red"></span>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label class="blue-label">Lease Expire Soon Mail :</label>
                                                                                    <span class="lease_expire_soon_email"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row apx-border-row">
                                                                            <div class="col-md-2 text-right pull-right col-xs-12">
                                                                                <a id="editLink" class="apx-edt-btn"><i class="fa fa-edit"></i> Edit</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                            <div id="editMode" style="display: none">
                                                                <form  id="add_account_preference_form">
                                                                    <div class="panel-body">
                                                                        <div class="">
                                                                            <div class="account_preference_edit row">
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Fiscal Year First Month :</label>
                                                                                    <span>
                                                                                        <select class="form-control" name="fiscal_year_first_month" id="fiscal_year_first_month">
                                                                                            <option value="">Select</option>
                                                                                            <option value="January">January</option>
                                                                                            <option value="February">February</option>
                                                                                            <option value="March">March</option>
                                                                                            <option value="April">April</option>
                                                                                            <option value="May">May</option>
                                                                                            <option value="June">June</option>
                                                                                            <option value="July">July</option>
                                                                                            <option value="August">August</option>
                                                                                            <option value="September">September</option>
                                                                                            <option value="October">October</option>
                                                                                            <option value="November">November</option>
                                                                                            <option value="December">December</option>
                                                                                        </select>
                                                                                    </span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Cash Account (Credit) for Bill Posting :</label>
                                                                                    <span><select class="form-control" name="cash_account_credit_bill_posting" id="cash_account_credit_bill_posting"></select></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Rent Charges :</label>
                                                                                    <span><select class="form-control" name="rent_charges" id="rent_charges"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Check Receive Charge :</label>
                                                                                    <span><select class="form-control" name="check_receive_charge" id="check_receive_charge"></select></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Debit Type Receive Payment Charge :</label>
                                                                                    <span><select class="form-control" name="debit_type_receive_payment_charge" id="debit_type_receive_payment_charge"></select></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Vacancy Charge :</label>
                                                                                    <span><select class="form-control" name="vacancy_charge"  id="vacancy_charge"></select></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Waive-Off on Payment Received Charge :</label>
                                                                                    <span><select class="form-control" name="waiveoff_on_payment_received_charge"  id="waiveoff_on_payment_received_charge"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Bad Debt Charge :</label>
                                                                                    <span><select class="form-control" name="bad_debit_charge"  id="bad_debit_charge"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Write-Off Charge :</label>
                                                                                    <span><select class="form-control" name="write_off_charge"  id="write_off_charge"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Money Order Payment Received Charge :</label>
                                                                                    <span><select class="form-control" name="money_order_payment_received_charge"  id="money_order_payment_received_charge"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Auto Debit Payment Received Charge :</label>
                                                                                    <span><select class="form-control" name="auto_debit_payment_receive_charge"  id="auto_debit_payment_receive_charge"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Cash Payment Received Charge :</label>
                                                                                    <span><select class="form-control" name="cash_payment_received_charge"  id="cash_payment_received_charge"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">NSF Charge :</label>
                                                                                    <span><select class="form-control" name="nsf_charge"  id="nsf_charge"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Check Bounced Charge :</label>
                                                                                    <span><select class="form-control" name="check_bounce_charge"  id="check_bounce_charge"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Check Issue to Vendor/Tenant Charge :</label>
                                                                                    <span><select class="form-control" name="check_issue_vendor_tenant_charge"  id="check_issue_vendor_tenant_charge"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Refund Charge :</label>
                                                                                    <span><select class="form-control" name="refund_charge"   id="refund_charge"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Interest Received from Bank Charge :</label>
                                                                                    <span><select id="interest_received_from_bank_charge" class="form-control" name="interest_received_from_bank_charge"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Bank Adjustment Fee Charge :</label>
                                                                                    <span><select id="bank_adjustment_fee_charge" class="form-control" name="bank_adjustment_fee_charge"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Concession Charge :</label>
                                                                                    <span><select id="concession_charge" class="form-control" name="concession_charge"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Transfer Charge :</label>
                                                                                    <span><input type="text" class="form-control" id="transfer_fee_charge" name="transfer_fee_charge"></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Late Fee Charge :</label>
                                                                                    <span><select id="late_fee_charge" class="form-control" name="late_fee_charge"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">Discount Account Id :</label>
                                                                                    <span><select id="discount_account_id" class="form-control" name="discount_account_id"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">EFT Generate Date :</label>
                                                                                    <span><input type="text" name="eft_generate_date" id="eft_generate_date" maxlength="2" value="" class="form-control"></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">EFT Charge :</label>
                                                                                    <span><select id="eft_charge" class="form-control" name="eft_charge"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="">CAM Charges :</label>
                                                                                    <span><select id="cam_charges" class="form-control" name="cam_charges"></select></span>
                                                                                    <span class="first_name"></span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label>
                                                                                        <input id="warning_past_status" value="" name="warning_past_status" type="checkbox"> Warn if Transaction are Day(s) in Past
                                                                                    </label>
                                                                                    <span>
                                                                                        <input type="text" maxlength="2" value="" readonly id="warn_transaction_days_past" placeholder="Days" class="form-control warnIfInPastClass" name="warn_transaction_days_past">
                                                                                    </span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label>
                                                                                        <input id="warning_future_status" value="" name="warning_future_status" type="checkbox"> Warn if Transaction are Day(s) in Future
                                                                                    </label>
                                                                                    <span>
                                                                                        <input type="text" maxlength="2" value="" readonly id="warn_transaction_days_future" placeholder="Days" class="form-control warnIfInFutureClass" name="warn_transaction_days_future">
                                                                                    </span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label>Auto Invoice </label>
                                                                                    <span class="radio-span1">
                                                                                        <input type="radio" id="AutoInvoiceYES" class="AutoInvoiceClass" name="auto_invoice" value="1" checked> Yes
                                                                                        <input type="radio" id="AutoInvoiceNO" class="AutoInvoiceClass" name="auto_invoice" value="0" > No
                                                                                    </span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <div class="divAutoInvoiceDays">
                                                                                        <label>Auto Invoice Days Before End of Month  </label>
                                                                                        <span>
                                                                                        <select class="form-control" id="auto_invoice_days_before_end_of_month" name="auto_invoice_days_before_end_of_month">
                                                                                            <option value="1">1</option>
                                                                                            <option value="2">2</option>
                                                                                            <option value="3">3</option>
                                                                                            <option value="4">4</option>
                                                                                            <option value="5">5</option>
                                                                                            <option value="6">6</option>
                                                                                            <option value="7">7</option>
                                                                                            <option value="8">8</option>
                                                                                            <option value="9">9</option>
                                                                                            <option value="10">10</option>
                                                                                            <option value="11">11</option>
                                                                                            <option value="12">12</option>
                                                                                            <option value="13">13</option>
                                                                                            <option value="14">14</option>
                                                                                            <option value="15">15</option>
                                                                                            <option value="16">16</option>
                                                                                            <option value="17">17</option>
                                                                                            <option value="18">18</option>
                                                                                            <option value="19">19</option>
                                                                                            <option value="20">20</option>
                                                                                            <option value="21">21</option>
                                                                                            <option value="22">22</option>
                                                                                            <option value="23">23</option>
                                                                                            <option value="24">24</option>
                                                                                            <option value="25">25</option>
                                                                                            <option value="26">26</option>
                                                                                            <option value="27">27</option>
                                                                                            <option value="28">28</option>
                                                                                            <option value="29">29</option>
                                                                                            <option value="30">30</option>
                                                                                            <option value="31">31</option>
                                                                                        </select>
                                                                                    </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label>SD Refund Charge  </label>
                                                                                    <span>
                                                                                       <select id="sd_refund_charge" class="form-control" name="sd_refund_charge"></select>
                                                                                    </span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label>Color Negative Balances in Red </label>
                                                                                    <span class="radio-span1">
                                                                                        <input type="radio" id="ColorNegativeBalanceYES" name="color_negative_balances_in_red" value="1"  checked="checked"> Yes
                                                                                        <input type="radio" id="ColorNegativeBalanceNO" name="color_negative_balances_in_red" value="0" > No
                                                                                    </span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label>Use Parenthesis Instead of a Minus Sign </label>
                                                                                    <span class="radio-span1">
                                                                                        <input type="radio" id="UseParenthesisInsteadOfAMinusSignYES" name="use_parenthesis_instead_of_a_minus_sign" value="1" checked> Yes
                                                                                        <input type="radio" id="UseParenthesisInsteadOfAMinusSignNO" name="use_parenthesis_instead_of_a_minus_sign" value="0"> No
                                                                                    </span>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label>Lease Expire Soon Mail</label>
                                                                                    <span class="radio-span1">
                                                                                        <input type="radio" id="LeaseExpiryYES" name="lease_expire_soon_email" value="1" checked> Yes
                                                                                        <input type="radio" id="LeaseExpiryNo" name="lease_expire_soon_email" value="0" > No
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <div class="btn-outer text-right">
                                                                            <!--                                                                            <input type="hidden" id="account_preference_id" name="account_preference_id">-->
                                                                            <button type="submit" class="blue-btn">Update</button>
                                                                            <input type='button'  value="Reset" class="clear-btn formreset" >
                                                                            <button type="button" id="cancelEdit" class="grey-btn">Cancel</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Add unit type div ends here-->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $('#leftnav3').addClass('in');
    $('.accounting_preferences').addClass('active');
    var defaultFormData=$("#add_account_preference_form").serializeArray();
    $(document).on('click','.formreset',function () {
        resetEditForm("#add_account_preference_form",[],true,defaultFormData,[]);
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/accountingPreferences.js"></script>
<!-- Jquery Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
