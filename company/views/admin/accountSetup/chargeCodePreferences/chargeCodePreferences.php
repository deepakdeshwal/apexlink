<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Accounting >> <span>Charge Code Preferences</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">

                            <div class="property-status" >
                                <div class="row ">
                                    <div class="col-md-2">
                                        <select id="jqGridStatus" data-module="ADMIN-SETTINGS" class="jqGridStatusClass fm-txt form-control">
                                            <option value="All">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>

                                    </div>
                                    <div class="col-md-10">
                                        <div class="btn-outer text-right">
                                            <button id="addchargecodeprefButton" class="blue-btn">Add Charge Code Preferences</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="property-status  col-sm-12" >
                                <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-2">
                                        <select id="applicable_to_search_ddl " class="fm-txt admin-setting-input form-control common_ddl">
                                            <option value="all">Applicable to</option>
                                            <option value="0">Property</option>
                                            <option value="1">Unit</option>
                                        </select>

                                    </div>
                                    <div class="col-md-2">
                                        <select id="frequency_search_ddl" class="fm-txt admin-setting-input form-control common_ddl">
                                            <option value="all">Select</option>
                                            <option value="1">One Time</option>
                                            <option value="2">Monthly</option>
                                        </select>

                                    </div>
                                    <div class="col-md-2">
                                        <select id="status_search_ddl" class="fm-txt admin-setting-input form-control common_ddl">
                                            <option value="all">All</option>
                                            <option selected="selected" value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>

                                    </div>

                                </div>
                            </div>
                            <!-- Main tabs -->
                            <div class="main-tabs">

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <!--Add unit type div starts here-->
                                                <div  style="display: none;" id="add_charge_codepref_div">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 id="headerDiv" class="panel-title">
                                                                Add Charge Code Preferences
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <form name="add_chargecodepref" id="chargecodepref">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <span class="row col-md-2 "><input type="radio" id="selectProperty" name="charge_code_type" value="0" checked>Property</span>
                                                                                <span class="row col-md-2"><input type="radio" id="selectUnit" name="charge_code_type" value="1">Unit</span>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-6 col-md-3">
                                                                                <label>Select Charge Code <em class="red-star">*</em></label>
                                                                                <select id="charge_code" name="charge_code" class="fm-txt form-control">
                                                                                    <option value="">Select</option>
                                                                                </select>
                                                                                <span id="chargeCodeErr" class="error"></span>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-6 col-md-3">
                                                                                <label>Frequency <em class="red-star">*</em></label>
                                                                                <select class="form-control" name="frequency" id="frequency">
                                                                                    <option value="">Select</option>
                                                                                    <option value="1">One Time</option>
                                                                                    <option value="2">Monthly</option>
                                                                                </select>
                                                                                <span id="frequencyErr" class="error"></span>
                                                                            </div>

                                                                            <div class="col-xs-12 col-sm-6 col-md-3">
                                                                                <div class="property_ddl">
                                                                                    <label>Type <em class="red-star">*</em></label>
                                                                                    <select class="form-control" name="type" id="type">
                                                                                        <option value="">Select</option>
                                                                                        <option value="1">CAM-Per Unit</option>
                                                                                        <option value="2">CAM-Per Sq.Ft.</option>
                                                                                        <option value="3">Non-CAM</option>
                                                                                    </select>
                                                                                    <span id="typeErr" class="error"></span>
                                                                                </div>
                                                                                <div class="unit_ddl" style="display: none;">
                                                                                    <label>Type <em class="red-star">*</em></label>
                                                                                    <select class="form-control" name="unit_type_id[]" id="unit_type_id" multiple="multiple" >
                                                                                    </select>
                                                                                    <span id="unit_type_idErr" class="error"></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-12">
                                                                                <label>
                                                                                    <div class="check-outer">
                                                                                        <input name="is_default" id="is_default" type="checkbox"/>
                                                                                        <label>Set as Default</label>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="btn-outer text-right">
                                                                                <input type="hidden" name="charge_code_pref_id" class="form-control" id="charge_code_pref_id"/>
                                                                                <input type="submit" value="Save" class="blue-btn" id="saveBtnId"/>
                                                                                <button type="button"  class="clear-btn clearFormReset">Clear</button>
                                                                                <button type="button" id="add_chargeCode_cancelbtn" class="grey-btn">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Add unit type div ends here-->

                                                <!--List unit type div ends here-->
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table id="chargeCodePreference-table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--List unit type div ends here-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $('#leftnav3').addClass('in');
    $('.charge_code_preferences').addClass('active');
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });
    var defaultFormData = '';
    $(document).on('click','.formreset',function () {
        resetEditForm("#chargecodepref",[],true,defaultFormData,[]);
    });
    $(document).on('click','.clearFormReset',function () {
        resetFormClear("#chargecodepref",['charge_code','frequency','type'],'form',false);
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/ChargeCodePreferences.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<!-- Jquery Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>