<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Accounting >> <span>Chart of Accounts</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">

                            <div class="property-status">
                                <div class="row">
                                    <div class="col-md-2">
                                        <select id="jqGridStatus" data-module="ADMIN-SETTINGS" class="jqGridStatusClass fm-txt form-control">
                                            <option value="All">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>

                                    </div>
                                    <div class="col-md-10">
                                        <div class="btn-outer text-right">
                                            <button id="addChartOfAccountButton" class="blue-btn">Add Chart of Account</button>
                                            <button id="importChartOfAccountButton" class="blue-btn">Import Chart of Account</button>
                                            <button id="exportChartOfAccountButton" class="blue-btn">Export Chart of Account</button>
                                            <a href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/excel/ChartAccount.xlsx" download class="blue-btn">Download Sample</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Main tabs -->
                            <div class="main-tabs">

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <!--Add chart of account div starts here-->
                                                <div  style="display: none;" id="add_chart_account_div">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 id="headerDiv" class="panel-title">
                                                                Add Chart of Account
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <form name="add_chart_account_form" id="add_chart_account_form_id">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-6 col-md-3">
                                                                                    <label>Account Type <em class="red-star">*</em></label>
                                                                                    <select class="fm-txt form-control" name="account_type_id" id="account_type_id"></select>
                                                                                    <span id="account_type_idErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-6 col-md-3">
                                                                                    <label>Account Code <em class="red-star">*</em></label>
                                                                                    <input id="account_code" name="account_code" maxlength="4" placeholder="Eg: 1110" class="form-control" type="text"/>
                                                                                    <span id="account_codeErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-6 col-md-3">
                                                                                    <label>Account Name <em class="red-star">*</em></label>
                                                                                    <input id="account_name" name="account_name" maxlength="50" placeholder="Eg: Property Trust" class="form-control" type="text"/>
                                                                                    <span id="account_nameErr" class="error"></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-6 col-md-3">
                                                                                    <label>Reporting Code <em class="red-star">*</em></label>
                                                                                    <select class="fm-txt form-control" id="reporting_code" name="reporting_code">
                                                                                        <option value="">Select</option>
                                                                                        <option value="1">C</option>
                                                                                        <option value="2">L</option>
                                                                                        <option value="3">M</option>
                                                                                        <option value="4">B</option>
                                                                                    </select>
                                                                                    <span id="reporting_codeErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-6 col-md-3">
                                                                                    <label>Sub Account of</label>
                                                                                    <select class="fm-txt form-control" id="sub_account" name="sub_account">
                                                                                        <option value="">Select</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-sm-6 col-md-3">
                                                                                    <label>Status</label>
                                                                                    <select class="fm-txt form-control" id="status" name="status">
                                                                                        <option value="1">Active</option>
                                                                                        <option value="0">InActive</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-6 col-md-3">
                                                                                    <label>
                                                                                        <div class="check-outer">
                                                                                            <input name="is_default" id="is_default" type="checkbox"/>
                                                                                            <label>Set as Default</label>
                                                                                        </div>
                                                                                    </label>
                                                                                </div>
                                                                                <div class="col-sm-6 col-md-3">
                                                                                    <select class="fm-txt form-control" id="posting_status" name="posting_status">
                                                                                        <option value="1">Posting</option>
                                                                                        <option value="0">Non Posting</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="btn-outer text-right">
                                                                                <input type="hidden" value="" name="chart_account_edit_id" class="form-control" id="chart_account_edit_id"/>
                                                                                <input type="submit" value="Save" class="blue-btn" id="saveBtnId"/>
                                                                                <button type="button"  class="clear-btn clearFormReset">Clear</button>
                                                                                <button type="button" id="add_chart_account_cancel_btn" class="grey-btn">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Add chart of account  div ends here-->

                                                <!--Import chart of account  div starts here-->
                                                <div  style="display: none;" id="import_ChartOfAccount_div">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a >Import Chart of Account</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <div class="panel-body">
                                                                <form name="importChartOfAccountForm" id="importChartOfAccountFormId" >
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <input type="file" name="import_file" id="import_file"   accept=".csv, .xls, .xlsx" required/>
                                                                                <span class="error"></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="btn-outer">
                                                                                <button type="submit" class="blue-btn">Submit</button>
                                                                                <button type="button" id="import_ChartOfAccount_cancel_btn" class="grey-btn">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Import chart of account  div ends here-->

                                                <!--List chart of account  div ends here-->
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table id="ChartOfAccount-table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--List chart of account  div ends here-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $('#leftnav3').addClass('in');
    $('.chart_of_accounts').addClass('active');
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });
    var defaultFormData = '';
    $(document).on('click','.formreset',function () {
        resetEditForm("#add_chart_account_form_id",[],true,defaultFormData,[]);
    });
    $(document).on('click','.clearFormReset',function () {
        resetFormClear("#add_chart_account_form_id",['portfolio_id'],'form',false);
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/chartOfAccounts.js"></script>
<!-- Jquery Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>