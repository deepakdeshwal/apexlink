<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Accounting >> <span>Charge Code</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">

                            <div class="property-status">
                                <div class="row">
                                    <div class="col-md-2">
                                        <select id="jqGridStatus" data-module="ADMIN-SETTINGS" class="jqGridStatusClass fm-txt form-control">
                                            <option value="All">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>

                                    </div>
                                    <div class="col-md-10">
                                        <div class="btn-outer text-right">
                                            <button id="addChargeCodeButton" class="blue-btn">Add Charge Code</button>
                                            <button id="importChargeCodeButton" class="blue-btn">Import Charge Code</button>
                                            <button id="exportChargeCodeButton" class="blue-btn">Export Charge Code</button>
                                            <a href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/excel/chargeCode.xlsx" download class="blue-btn">Download Sample</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Main tabs -->
                            <div class="main-tabs">

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <!--Add AccountChargeCode div starts here-->
                                                <div  style="display: none;" id="add_charge_code_div">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 id="headerDiv" class="panel-title">
                                                                Add Charge Code
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <form name="add_charge_code" id="add_charge_code_form">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-6 col-md-3">
                                                                                    <label>Charge Code <em class="red-star">*</em></label>
                                                                                    <input name="charge_code" id="charge_code" maxlength="10" placeholder="Eg: 1B"   class="form-control only_letter" type="text"/>
                                                                                    <span id="charge_codeErr"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-6">
                                                                                    <label>Description</label>
                                                                                    <textarea name="description" style="resize: none;" id="description" rows="1" maxlength="250" placeholder="Eg: 1 Bedroom" class="form-control"></textarea>
                                                                                    <span id="descriptionErr"></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-6 col-md-3">
                                                                                    <label>Credit Account <em class="red-star">*</em></label>
                                                                                    <select id="credit_account" name="credit_account" class="fm-txt form-control">
                                                                                        <option value="">Select</option>
                                                                                    </select>
                                                                                    <span id="credit_accountErr"></span>
                                                                                </div>
                                                                                <div class="col-sm-6 col-md-3">
                                                                                    <label>Debit Account <em class="red-star">*</em></label>
                                                                                    <select id="debit_account" name="debit_account" class="fm-txt form-control">
                                                                                        <option value="">Select</option>
                                                                                    </select>
                                                                                    <span id="debit_accountErr"></span>
                                                                                </div>
                                                                                <div class="col-sm-6 col-md-3">
                                                                                    <label>Status <em class="red-star">*</em></label>
                                                                                    <select name="status" id="status" class="fm-txt form-control">
                                                                                        <option value="">Select</option>
                                                                                        <option value="1">Active</option>
                                                                                        <option value="0">InActive</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12 ">
                                                                            <div class="col-sm-12 btn-outer text-right">
                                                                                <input type="hidden" name="charge_code_id" class="form-control" id="charge_code_id"/>
                                                                                <input type="submit" value="save" class="blue-btn" id="saveBtnId"/>
                                                                                <button type="button" class="clear-btn clearFormReset">Clear</button>
                                                                                <button type="button" id="add_charge_code_cancel_btn" class="grey-btn">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Add AccountChargeCode div ends here-->

                                                <!--Import AccountChargeCode div starts here-->
                                                <div  style="display: none;" id="import_unit_type_div">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a >Import Charge Code</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <div class="panel-body">
                                                                <form name="importChargeCodeForm" id="importChargeCodeFormId" >
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <input type="file" name="import_file" id="import_file"   accept=".csv, .xls, .xlsx" required/>
                                                                                <span class="error"></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="btn-outer">
                                                                                <button type="submit" class="blue-btn">Submit</button>
                                                                                <button type="button" id="import_chargeCode_cancel_btn" class="grey-btn">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                                <!--Import AccountChargeCode div ends here-->

                                                <!--List AccountChargeCode div ends here-->
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table id="ChargeCode-table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--List AccountChargeCode div ends here-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $('#leftnav3').addClass('in');
    $('.charge_code').addClass('active');
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });
    var defaultFormData = '';
    $(document).on('click','.formreset',function () {
        resetEditForm("#add_charge_code_form",[],true,defaultFormData,[]);
    });
    $(document).on('click','.clearFormReset',function () {
        resetFormClear("#add_charge_code_form",['credit_account','debit_account','status'],'form',false);
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/chargeCode.js"></script>
<!-- Jquery Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>