<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Users >> <span>Role Authorization</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">

                            <div class="box-shadow-outer">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <select id="role_select" class="fm-txt form-control"></select>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="text-right edit_btn" style="display: none;">
                                            <button id="editRolesBtn" class="blue-btn">Edit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Main tabs -->
                            <div class="main-tabs">

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <!--Add unit type div starts here-->
                                                <div  style="display: none;" id="user_with_properties">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 id="headerDiv" class="panel-title">
                                                                Role Authorization
                                                            </h4>
                                                        </div>
                                                        <div class="panel-body pad-none"></div>
                                                        <form name="userAuthorizationForm"  id="userAuthorizationForm">
                                                            <input type="hidden" name="role_id" id="role_id" value="">
                                                            <div class="form-data">
                                                                <div class="col-sm-12">
                                                                    <div id="list_of_property" class="collapse in" >
                                                                        <table id="properties_list" class="table table-bordered">
<!--                                                                            <thead>-->
<!--                                                                            <tr class="table_header_values">-->
<!--                                                                                <th class="table-heading">Property</th>-->
<!--                                                                            </tr>-->
<!--                                                                            </thead>-->
<!--                                                                            <tbody class="table_body_values">-->
<!--                                                                            </tbody>-->
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="btn-outer text-right" id="save_role_athzation_btn_div" style="display:none;">
                                                                <input type="submit" id="save_role_athzation_btn" class="blue-btn" value="Save">
                                                                <input type="button" id="cancel_role_athzation_btn" class="grey-btn" value="Cancel">
                                                            </div>
                                                        </form>
                                                    </div>
                                                    </div>
                                                </div>

                                                <!--List unit type div ends here-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    $('#leftnav4').addClass('in');
    $('.role_authorization').addClass('active');
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/common.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/adminUsers/roleAuthorization.js"></script>

<!-- Jquery Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
