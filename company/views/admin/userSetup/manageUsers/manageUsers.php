<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>
<?php
global $countryArray;
$countryArray = array(
    'AB' => array('name' => 'Abkhazia', 'code' => '7840 '),
    'AD' => array('name' => 'ANDORRA', 'code' => '376'),
    'AF' => array('name' => 'AFGHANISTAN', 'code' => '93'),
    'AG' => array('name' => 'ANTIGUA AND BARBUDA', 'code' => '1268'),
    'AI' => array('name' => 'ANGUILLA', 'code' => '1264'),
    'AL' => array('name' => 'ALBANIA', 'code' => '355'),
    'AM' => array('name' => 'ARMENIA', 'code' => '374'),
    'AN' => array('name' => 'NETHERLANDS ANTILLES', 'code' => '599'),
    'AO' => array('name' => 'ANGOLA', 'code' => '244'),
    'AQ' => array('name' => 'ANTARCTICA', 'code' => '672'),
    'AR' => array('name' => 'ARGENTINA', 'code' => '54'),
    'AS' => array('name' => 'AMERICAN SAMOA', 'code' => '1684'),
    'AT' => array('name' => 'AUSTRIA', 'code' => '43'),
    'AU' => array('name' => 'AUSTRALIA', 'code' => '61'),
    'AW' => array('name' => 'ARUBA', 'code' => '297'),
    'AZ' => array('name' => 'AZERBAIJAN', 'code' => '994'),
    'BA' => array('name' => 'BOSNIA AND HERZEGOVINA', 'code' => '387'),
    'BB' => array('name' => 'BARBADOS', 'code' => '1246'),
    'BD' => array('name' => 'BANGLADESH', 'code' => '880'),
    'BE' => array('name' => 'BELGIUM', 'code' => '32'),
    'BF' => array('name' => 'BURKINA FASO', 'code' => '226'),
    'BG' => array('name' => 'BULGARIA', 'code' => '359'),
    'AE' => array('name' => 'UNITED ARAB EMIRATES', 'code' => '971'),
    'BH' => array('name' => 'BAHRAIN', 'code' => '973'),
    'BI' => array('name' => 'BURUNDI', 'code' => '257'),
    'BJ' => array('name' => 'BENIN', 'code' => '229'),
    'BL' => array('name' => 'SAINT BARTHELEMY', 'code' => '590'),
    'BM' => array('name' => 'BERMUDA', 'code' => '1441'),
    'BN' => array('name' => 'BRUNEI DARUSSALAM', 'code' => '673'),
    'BO' => array('name' => 'BOLIVIA', 'code' => '591'),
    'BR' => array('name' => 'BRAZIL', 'code' => '55'),
    'BS' => array('name' => 'BAHAMAS', 'code' => '1242'),
    'BT' => array('name' => 'BHUTAN', 'code' => '975'),
    'BW' => array('name' => 'BOTSWANA', 'code' => '267'),
    'BY' => array('name' => 'BELARUS', 'code' => '375'),
    'BZ' => array('name' => 'BELIZE', 'code' => '501'),
    'CA' => array('name' => 'CANADA', 'code' => '1'),
    'CC' => array('name' => 'COCOS (KEELING) ISLANDS', 'code' => '61'),
    'CD' => array('name' => 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'code' => '243'),
    'CF' => array('name' => 'CENTRAL AFRICAN REPUBLIC', 'code' => '236'),
    'CG' => array('name' => 'CONGO', 'code' => '242'),
    'CH' => array('name' => 'SWITZERLAND', 'code' => '41'),
    'CI' => array('name' => 'COTE D IVOIRE', 'code' => '225'),
    'CK' => array('name' => 'COOK ISLANDS', 'code' => '682'),
    'CL' => array('name' => 'CHILE', 'code' => '56'),
    'CM' => array('name' => 'CAMEROON', 'code' => '237'),
    'CN' => array('name' => 'CHINA', 'code' => '86'),
    'CO' => array('name' => 'COLOMBIA', 'code' => '57'),
    'CR' => array('name' => 'COSTA RICA', 'code' => '506'),
    'CU' => array('name' => 'CUBA', 'code' => '53'),
    'CV' => array('name' => 'CAPE VERDE', 'code' => '238'),
    'CX' => array('name' => 'CHRISTMAS ISLAND', 'code' => '61'),
    'CY' => array('name' => 'CYPRUS', 'code' => '357'),
    'CZ' => array('name' => 'CZECH REPUBLIC', 'code' => '420'),
    'DE' => array('name' => 'GERMANY', 'code' => '49'),
    'DJ' => array('name' => 'DJIBOUTI', 'code' => '253'),
    'DK' => array('name' => 'DENMARK', 'code' => '45'),
    'DM' => array('name' => 'DOMINICA', 'code' => '1767'),
    'DO' => array('name' => 'DOMINICAN REPUBLIC', 'code' => '1809'),
    'DZ' => array('name' => 'ALGERIA', 'code' => '213'),
    'EC' => array('name' => 'ECUADOR', 'code' => '593'),
    'EE' => array('name' => 'ESTONIA', 'code' => '372'),
    'EG' => array('name' => 'EGYPT', 'code' => '20'),
    'ER' => array('name' => 'ERITREA', 'code' => '291'),
    'ES' => array('name' => 'SPAIN', 'code' => '34'),
    'ET' => array('name' => 'ETHIOPIA', 'code' => '251'),
    'FI' => array('name' => 'FINLAND', 'code' => '358'),
    'FJ' => array('name' => 'FIJI', 'code' => '679'),
    'FK' => array('name' => 'FALKLAND ISLANDS (MALVINAS)', 'code' => '500'),
    'FM' => array('name' => 'MICRONESIA, FEDERATED STATES OF', 'code' => '691'),
    'FO' => array('name' => 'FAROE ISLANDS', 'code' => '298'),
    'FR' => array('name' => 'FRANCE', 'code' => '33'),
    'GA' => array('name' => 'GABON', 'code' => '241'),
    'GB' => array('name' => 'UNITED KINGDOM', 'code' => '44'),
    'GD' => array('name' => 'GRENADA', 'code' => '1473'),
    'GE' => array('name' => 'GEORGIA', 'code' => '995'),
    'GH' => array('name' => 'GHANA', 'code' => '233'),
    'GI' => array('name' => 'GIBRALTAR', 'code' => '350'),
    'GL' => array('name' => 'GREENLAND', 'code' => '299'),
    'GM' => array('name' => 'GAMBIA', 'code' => '220'),
    'GN' => array('name' => 'GUINEA', 'code' => '224'),
    'GQ' => array('name' => 'EQUATORIAL GUINEA', 'code' => '240'),
    'GR' => array('name' => 'GREECE', 'code' => '30'),
    'GT' => array('name' => 'GUATEMALA', 'code' => '502'),
    'GU' => array('name' => 'GUAM', 'code' => '1671'),
    'GW' => array('name' => 'GUINEA-BISSAU', 'code' => '245'),
    'GY' => array('name' => 'GUYANA', 'code' => '592'),
    'HK' => array('name' => 'HONG KONG', 'code' => '852'),
    'HN' => array('name' => 'HONDURAS', 'code' => '504'),
    'HR' => array('name' => 'CROATIA', 'code' => '385'),
    'HT' => array('name' => 'HAITI', 'code' => '509'),
    'HU' => array('name' => 'HUNGARY', 'code' => '36'),
    'ID' => array('name' => 'INDONESIA', 'code' => '62'),
    'IE' => array('name' => 'IRELAND', 'code' => '353'),
    'IL' => array('name' => 'ISRAEL', 'code' => '972'),
    'IM' => array('name' => 'ISLE OF MAN', 'code' => '44'),
    'IN' => array('name' => 'INDIA', 'code' => '91'),
    'IQ' => array('name' => 'IRAQ', 'code' => '964'),
    'IR' => array('name' => 'IRAN, ISLAMIC REPUBLIC OF', 'code' => '98'),
    'IS' => array('name' => 'ICELAND', 'code' => '354'),
    'IT' => array('name' => 'ITALY', 'code' => '39'),
    'JM' => array('name' => 'JAMAICA', 'code' => '1876'),
    'JO' => array('name' => 'JORDAN', 'code' => '962'),
    'JP' => array('name' => 'JAPAN', 'code' => '81'),
    'KE' => array('name' => 'KENYA', 'code' => '254'),
    'KG' => array('name' => 'KYRGYZSTAN', 'code' => '996'),
    'KH' => array('name' => 'CAMBODIA', 'code' => '855'),
    'KI' => array('name' => 'KIRIBATI', 'code' => '686'),
    'KM' => array('name' => 'COMOROS', 'code' => '269'),
    'KN' => array('name' => 'SAINT KITTS AND NEVIS', 'code' => '1869'),
    'KP' => array('name' => 'KOREA DEMOCRATIC PEOPLES REPUBLIC OF', 'code' => '850'),
    'KR' => array('name' => 'KOREA REPUBLIC OF', 'code' => '82'),
    'KW' => array('name' => 'KUWAIT', 'code' => '965'),
    'KY' => array('name' => 'CAYMAN ISLANDS', 'code' => '1345'),
    'KZ' => array('name' => 'KAZAKSTAN', 'code' => '7'),
    'LA' => array('name' => 'LAO PEOPLES DEMOCRATIC REPUBLIC', 'code' => '856'),
    'LB' => array('name' => 'LEBANON', 'code' => '961'),
    'LC' => array('name' => 'SAINT LUCIA', 'code' => '1758'),
    'LI' => array('name' => 'LIECHTENSTEIN', 'code' => '423'),
    'LK' => array('name' => 'SRI LANKA', 'code' => '94'),
    'LR' => array('name' => 'LIBERIA', 'code' => '231'),
    'LS' => array('name' => 'LESOTHO', 'code' => '266'),
    'LT' => array('name' => 'LITHUANIA', 'code' => '370'),
    'LU' => array('name' => 'LUXEMBOURG', 'code' => '352'),
    'LV' => array('name' => 'LATVIA', 'code' => '371'),
    'LY' => array('name' => 'LIBYAN ARAB JAMAHIRIYA', 'code' => '218'),
    'MA' => array('name' => 'MOROCCO', 'code' => '212'),
    'MC' => array('name' => 'MONACO', 'code' => '377'),
    'MD' => array('name' => 'MOLDOVA, REPUBLIC OF', 'code' => '373'),
    'ME' => array('name' => 'MONTENEGRO', 'code' => '382'),
    'MF' => array('name' => 'SAINT MARTIN', 'code' => '1599'),
    'MG' => array('name' => 'MADAGASCAR', 'code' => '261'),
    'MH' => array('name' => 'MARSHALL ISLANDS', 'code' => '692'),
    'MK' => array('name' => 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'code' => '389'),
    'ML' => array('name' => 'MALI', 'code' => '223'),
    'MM' => array('name' => 'MYANMAR', 'code' => '95'),
    'MN' => array('name' => 'MONGOLIA', 'code' => '976'),
    'MO' => array('name' => 'MACAU', 'code' => '853'),
    'MP' => array('name' => 'NORTHERN MARIANA ISLANDS', 'code' => '1670'),
    'MR' => array('name' => 'MAURITANIA', 'code' => '222'),
    'MS' => array('name' => 'MONTSERRAT', 'code' => '1664'),
    'MT' => array('name' => 'MALTA', 'code' => '356'),
    'MU' => array('name' => 'MAURITIUS', 'code' => '230'),
    'MV' => array('name' => 'MALDIVES', 'code' => '960'),
    'MW' => array('name' => 'MALAWI', 'code' => '265'),
    'MX' => array('name' => 'MEXICO', 'code' => '52'),
    'MY' => array('name' => 'MALAYSIA', 'code' => '60'),
    'MZ' => array('name' => 'MOZAMBIQUE', 'code' => '258'),
    'NA' => array('name' => 'NAMIBIA', 'code' => '264'),
    'NC' => array('name' => 'NEW CALEDONIA', 'code' => '687'),
    'NE' => array('name' => 'NIGER', 'code' => '227'),
    'NG' => array('name' => 'NIGERIA', 'code' => '234'),
    'NI' => array('name' => 'NICARAGUA', 'code' => '505'),
    'NL' => array('name' => 'NETHERLANDS', 'code' => '31'),
    'NO' => array('name' => 'NORWAY', 'code' => '47'),
    'NP' => array('name' => 'NEPAL', 'code' => '977'),
    'NR' => array('name' => 'NAURU', 'code' => '674'),
    'NU' => array('name' => 'NIUE', 'code' => '683'),
    'NZ' => array('name' => 'NEW ZEALAND', 'code' => '64'),
    'OM' => array('name' => 'OMAN', 'code' => '968'),
    'PA' => array('name' => 'PANAMA', 'code' => '507'),
    'PE' => array('name' => 'PERU', 'code' => '51'),
    'PF' => array('name' => 'FRENCH POLYNESIA', 'code' => '689'),
    'PG' => array('name' => 'PAPUA NEW GUINEA', 'code' => '675'),
    'PH' => array('name' => 'PHILIPPINES', 'code' => '63'),
    'PK' => array('name' => 'PAKISTAN', 'code' => '92'),
    'PL' => array('name' => 'POLAND', 'code' => '48'),
    'PM' => array('name' => 'SAINT PIERRE AND MIQUELON', 'code' => '508'),
    'PN' => array('name' => 'PITCAIRN', 'code' => '870'),
    'PR' => array('name' => 'PUERTO RICO', 'code' => '1'),
    'PT' => array('name' => 'PORTUGAL', 'code' => '351'),
    'PW' => array('name' => 'PALAU', 'code' => '680'),
    'PY' => array('name' => 'PARAGUAY', 'code' => '595'),
    'QA' => array('name' => 'QATAR', 'code' => '974'),
    'RO' => array('name' => 'ROMANIA', 'code' => '40'),
    'RS' => array('name' => 'SERBIA', 'code' => '381'),
    'RU' => array('name' => 'RUSSIAN FEDERATION', 'code' => '7'),
    'RW' => array('name' => 'RWANDA', 'code' => '250'),
    'SA' => array('name' => 'SAUDI ARABIA', 'code' => '966'),
    'SB' => array('name' => 'SOLOMON ISLANDS', 'code' => '677'),
    'SC' => array('name' => 'SEYCHELLES', 'code' => '248'),
    'SD' => array('name' => 'SUDAN', 'code' => '249'),
    'SE' => array('name' => 'SWEDEN', 'code' => '46'),
    'SG' => array('name' => 'SINGAPORE', 'code' => '65'),
    'SH' => array('name' => 'SAINT HELENA', 'code' => '290'),
    'SI' => array('name' => 'SLOVENIA', 'code' => '386'),
    'SK' => array('name' => 'SLOVAKIA', 'code' => '421'),
    'SL' => array('name' => 'SIERRA LEONE', 'code' => '232'),
    'SM' => array('name' => 'SAN MARINO', 'code' => '378'),
    'SN' => array('name' => 'SENEGAL', 'code' => '221'),
    'SO' => array('name' => 'SOMALIA', 'code' => '252'),
    'SR' => array('name' => 'SURINAME', 'code' => '597'),
    'ST' => array('name' => 'SAO TOME AND PRINCIPE', 'code' => '239'),
    'SV' => array('name' => 'EL SALVADOR', 'code' => '503'),
    'SY' => array('name' => 'SYRIAN ARAB REPUBLIC', 'code' => '963'),
    'SZ' => array('name' => 'SWAZILAND', 'code' => '268'),
    'TC' => array('name' => 'TURKS AND CAICOS ISLANDS', 'code' => '1649'),
    'TD' => array('name' => 'CHAD', 'code' => '235'),
    'TG' => array('name' => 'TOGO', 'code' => '228'),
    'TH' => array('name' => 'THAILAND', 'code' => '66'),
    'TJ' => array('name' => 'TAJIKISTAN', 'code' => '992'),
    'TK' => array('name' => 'TOKELAU', 'code' => '690'),
    'TL' => array('name' => 'TIMOR-LESTE', 'code' => '670'),
    'TM' => array('name' => 'TURKMENISTAN', 'code' => '993'),
    'TN' => array('name' => 'TUNISIA', 'code' => '216'),
    'TO' => array('name' => 'TONGA', 'code' => '676'),
    'TR' => array('name' => 'TURKEY', 'code' => '90'),
    'TT' => array('name' => 'TRINIDAD AND TOBAGO', 'code' => '1868'),
    'TV' => array('name' => 'TUVALU', 'code' => '688'),
    'TW' => array('name' => 'TAIWAN, PROVINCE OF CHINA', 'code' => '886'),
    'TZ' => array('name' => 'TANZANIA, UNITED REPUBLIC OF', 'code' => '255'),
    'UA' => array('name' => 'UKRAINE', 'code' => '380'),
    'UG' => array('name' => 'UGANDA', 'code' => '256'),
    'US' => array('name' => 'UNITED STATES', 'code' => '1'),
    'UY' => array('name' => 'URUGUAY', 'code' => '598'),
    'UZ' => array('name' => 'UZBEKISTAN', 'code' => '998'),
    'VA' => array('name' => 'HOLY SEE (VATICAN CITY STATE)', 'code' => '39'),
    'VC' => array('name' => 'SAINT VINCENT AND THE GRENADINES', 'code' => '1784'),
    'VE' => array('name' => 'VENEZUELA', 'code' => '58'),
    'VG' => array('name' => 'VIRGIN ISLANDS, BRITISH', 'code' => '1284'),
    'VI' => array('name' => 'VIRGIN ISLANDS, U.S.', 'code' => '1340'),
    'VN' => array('name' => 'VIET NAM', 'code' => '84'),
    'VU' => array('name' => 'VANUATU', 'code' => '678'),
    'WF' => array('name' => 'WALLIS AND FUTUNA', 'code' => '681'),
    'WS' => array('name' => 'SAMOA', 'code' => '685'),
    'XK' => array('name' => 'KOSOVO', 'code' => '381'),
    'YE' => array('name' => 'YEMEN', 'code' => '967'),
    'YT' => array('name' => 'MAYOTTE', 'code' => '262'),
    'ZA' => array('name' => 'SOUTH AFRICA', 'code' => '27'),
    'ZM' => array('name' => 'ZAMBIA', 'code' => '260'),
    'ZW' => array('name' => 'ZIMBABWE', 'code' => '263')
);
?>

<div id="wrapper">
    <?php
    include_once(COMPANY_DIRECTORY_URL. "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php"); ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Users >> <span>Manage Users</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">
                            <!--List & Add/Edit Mode Starts-->
                            <div id="list_mode">
                                <div class="property-status">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <select id="jqGridStatus" data-module="ADMIN-SETTINGS" class="jqGridStatusClass fm-txt form-control">
                                                <option value="All">All</option>
                                                <option value="1">Active</option>
                                                <option value="0">InActive</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <select name="select_role_type[]" class="form-control select_role_type" multiple="multiple">

                                            </select>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="btn-outer text-right">
                                                <button id="addNewUser" class="blue-btn" >Add New User</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Main tabs -->
                                <div class="main-tabs">
                                    <div class="accordion-grid">
                                        <div class="accordion-outer" >
                                            <div class="bs-example">
                                                <div class="panel-group only_edit_mode" id="addNewUserDiv"  style="display:none;">
                                                    <form id="addNewUserFormId">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <span id="headerDiv">New User</span>
                                                                    <a id="back_btn" type="button" class="back pull-right" style="cursor: pointer;"><i class="fa fa-angle-double-left" aria-hidden="true" style="cursor: pointer;"></i>Back</a>
                                                                </h4>
                                                               <!-- <h4 class="panel-title pull-right" style="cursor: pointer; padding:-10px;">
                                                                    </h4>-->

                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>First Name <em class="red-star">*</em></label>
                                                                                    <input id="first_name" name="first_name" maxlength="150" placeholder="Eg: Json" class="form-control first_capital" type="text" />
                                                                                    <span id="first_nameErr" class="error" ></span>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Middle Name </label>
                                                                                    <input id="middle_name" name="middle_name" maxlength="50" placeholder="Eg: John" class="form-control first_capital" type="text" />
                                                                                    <span id="middle_nameErr" class="errorSemi-Annual"></span>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Last Name <em class="red-star">*</em></label>
                                                                                    <input id="last_name" name="last_name" maxlength="150" placeholder="Eg: Json" class="form-control first_capital" type="text"/>
                                                                                    <span id="last_nameErr" class="error"></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>Maiden Name </label>
                                                                                    <input id="maiden_name" name="maiden_name" maxlength="150" placeholder="Eg: Json" class="form-control first_capital" type="text" />
                                                                                    <span id="maiden_nameErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Nick Name </label>
                                                                                    <input id="nick_name" name="nick_name" maxlength="50" placeholder="Eg: John" class="form-control first_capital" type="text"/>
                                                                                    <span id="nick_nameErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Email <em class="red-star">*</em></label>
                                                                                    <input id="email" name="email" maxlength="250" placeholder="Eg: Json" class="user_email form-control" type="text"/>
                                                                                    <span id="emailErr" class="error"></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>Users </label>
                                                                                    <select class="fm-txt form-control" id="admin_user_id" name="admin_user_id">
                                                                                        <option value="">Select User</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Role <em class="red-star">*</em></label>
                                                                                    <select class="fm-txt form-control" name="role" id="select_role">
                                                                                        <option>Select Role</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Status <em class="red-star">*</em></label>
                                                                                    <select class="fm-txt form-control" name="status" id="status">
                                                                                        <option value="">Select</option>
                                                                                        <option value="1">Active</option>
                                                                                        <option value="0">InActive</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>Work Phone <em class="red-star">*</em></label>
                                                                                    <input id="work_phone" name="work_phone" maxlength="12" placeholder="Eg: 154-175-4301" class="phone_number_format form-control" type="text"/>
                                                                                    <span id="work_phoneErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Office/Work Telephone Extension </label>
                                                                                    <input id="work_phone_extension" name="work_phone_extension" maxlength="12" placeholder="Eg: 154-175-4301" class="phone_number_format form-control" type="text"/>
                                                                                    <span id="work_phone_extensionErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Home Phone</label>
                                                                                    <input id="home_phone" name="home_phone" maxlength="12" placeholder="Eg: 154-175-4301" class="phone_number_format form-control" type="text"/>
                                                                                    <span id="home_phoneErr" class="error"></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>Fax</label>
                                                                                    <input id="fax" name="fax" maxlength="12" placeholder="Eg: 403-297-2706" class="phone_number_format form-control" type="text"/>
                                                                                    <span id="faxErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Country Code  <em class="red-star">*</em></label>
                                                                                    <select class="fm-txt form-control" name="country_code" id="country_code">
                                                                                        <option value="">Select Country Code</option>
                                                                                        <?php
                                                                                        foreach ($countryArray as $code => $country) {
                                                                                            $countryName = ucwords(strtolower($country["name"])); // Making it look good
                                                                                            echo "<option value='" . $code . "' " . (($code == 'US') ? "selected" : "") . ">" . $countryName . " (+" . $country["code"] . ")</option>";
                                                                                        }
                                                                                        ?>
                                                                                    </select>
                                                                                    <span id="country_codeErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Mobile Number <em class="red-star">*</em></label>
                                                                                    <input id="mobile_number" name="mobile_number" maxlength="12" placeholder="Eg: 154-175-4301" class="phone_number_format form-control" type="text"/>
                                                                                    <span id="mobile_numberErr" class="error"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 id="headerDivCustom" class="panel-title">
                                                                    Custom Fields
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                <div class="panel-body">
                                                                    <div class="form-outer">
                                                                        <input type="hidden" name="id" id="record_id">
                                                                        <div class="col-sm-12" style="margin-left: 15px;">
                                                                            <div class="collaps-subhdr">
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="custom_field_html">
                                                                                </div>
                                                                                <div class="col-sm-6 custom_field_msg">
                                                                                    No Custom Fields
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <div class="btn-outer text-right">
                                                                                        <button type="button" id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">Add Custom Field</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="btn-outer text-right" style="margin-bottom: 25px;">
                                                                <input type="hidden" name="edit_user_id" id="edit_user_id" value="">
                                                                <button class="blue-btn" type="submit" id="saveBtnId" value="Save"/>
                                                                <button class="clear-btn clearFormReset" type="button" >Clear</button>
                                                                <button class="grey-btn" type="button" id="cancelAddUsersBtn">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--List Mode Starts-->
                                                <div class="grid-outer" id="only_list_mode">
                                                    <div class="apx-table">
                                                        <div class="table-responsive">
                                                            <table id="ManageUser-table" class="table table-bordered">
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--List Mode Ends-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--List & Add/Edit Mode Ends-->

                            <!--View Mode Starts-->
                            <div id="view_mode" style="display: none">

                                    <div class="content-section">
                                        <!--single add-form-box-->
                                        <div class="apx-adformbox">
                                            <div class="apx-adformbox-title">
                                                <strong class="left">User Information</strong>
                                                <a id="back_btn" type="button" class="back right"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                            </div>
                                            <div class="apx-adformbox-content">
                                                <input type="hidden" id="view_edit_user_id" name="view_edit_user_id" value="">
                                                <input type="hidden" id="page" name="page" value="view">
                                                <div class="row">
                                                    <div class="view-outer">
                                                        <div class="col-xs-12 col-sm-6">
                                                            <label class="blue-label">First Name  :</label>
                                                            <span class="first_name"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <label class="blue-label">Last Name :</label>
                                                            <span class="last_name"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <label class="blue-label">Middle Name :</label>
                                                            <span class="middle_name"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <label class="blue-label">Role :</label>
                                                            <span class="role"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <label class="blue-label">Maiden Name :</label>
                                                            <span class="maiden_name"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <label class="blue-label">Work Phone :</label>
                                                            <span class="work_phone"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <label class="blue-label">Nick Name :</label>
                                                            <span class="nick_name"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <label class="blue-label">Office/Work Telephone Extension :</label>
                                                            <span class="work_phone_extension"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <label class="blue-label">Email :</label>
                                                            <span class="email"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <label class="blue-label">Fax :</label>
                                                            <span class="fax"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <label class="blue-label">Status :</label>
                                                            <span class="status"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <label class="blue-label">Mobile Number :</label>
                                                            <span class="mobile_number"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <label class="blue-label">Home Phone :</label>
                                                            <span class="home_phone"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <label class="blue-label">Country Code :</label>
                                                            <span class="country_code"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row apx-border-row">
                                                    <div class="col-md-2 text-right pull-right col-xs-12">
                                                        <a type="button" id="editUserBtn" class="apx-edt-btn"><i class="fa fa-edit"></i> Edit</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--End single add-form-box-->

                                        <!--single add-form-box-->
                                        <div class="apx-adformbox">
                                            <div class="apx-adformbox-title">
                                                <strong class="left">Custom Fields</strong>
                                            </div>
                                            <div class="apx-adformbox-content">
                                                <input type="hidden" id="view_id" name="id" value="">
                                                <input type="hidden" id="page" name="page" value="view">
                                                <div class="row">
                                                    <div class="view-outer">
                                                        <div class="custom_field_html_view_mode">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row apx-border-row">
                                                    <div class="col-md-2 text-right pull-right col-xs-12">
                                                        <a type="button" id="editUserBtn" class="apx-edt-btn"><i class="fa fa-edit"></i> Edit</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--End single add-form-box-->
                                    </div>
                            <!--View Mode Ends-->
                        </div>
                    </div>
                </div>
                <!-- Content Data Ends ---->
            </div>
        </div>
</div>

<!-- start custom field model -->
<div class="container">
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body" style="height: 380px;">
                    <div class="form-outer col-sm-12">
                        <form id="custom_field">
                            <input type="hidden" id="customFieldModule" name="module" value="user">
                            <input type="hidden" name="id" id="custom_field_id" value="">
                            <div class="row custom_field_form">
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Field Name <em class="red-star">*</em></label>
                                    </div>
                                    <div class="col-sm-9 field_name">
                                        <input class="form-control" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                        <span class="required error"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Data Type</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control data_type" id="data_type" name="data_type">
                                            <option value="text">Text</option>
                                            <option value="number">Number</option>
                                            <option value="currency">Currency</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="url">URL</option>
                                            <option value="date">Date</option>
                                            <option value="memo">Memo</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Default value</label>
                                    </div>
                                    <div class="col-sm-9 default_value">
                                        <input class="form-control default_value" id="default_value" type="text" name="default_value" placeholder="">
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Required Field</label>
                                    </div>
                                    <div class="col-sm-9 is_required">
                                        <select class="form-control" name="is_required" id="is_required">
                                            <option value="1">Yes</option>
                                            <option value="0" selected="selected">No</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End custom field model -->
</section>
</div>
<!-- Wrapper Ends -->

<!-- Footer Ends -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var login_user_id = "<?php echo $_SESSION[SESSION_DOMAIN]['cuser_id']; ?>";
    $('#leftnav4').addClass('in');
    $('.manage_users').addClass('active');

    $('.select_role_type').multiselect({
        includeSelectAllOption: true,
        nonSelectedText: 'Select Role Type'
    });

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
    var defaultFormData = '';
    $(document).on('click','.formreset',function () {
        resetEditForm("#addNewUserFormId",[],true,defaultFormData,[]);
    });
    $(document).on('click','.clearFormReset',function () {
        resetFormClear("#addNewUserFormId",['admin_user_id','role','status','country_code'],'form',false);
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/adminUsers/manageUsers.js"></script>

<style>
    .row.custom_field_class input {
        width: 258px;
    }
</style>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>