<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Users >> <span>Manage User Roles</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">

                            <div class="property-status">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <select id="jqGridStatus" data-module="ADMIN-SETTINGS" class="jqGridStatusClass fm-txt form-control">
                                            <option value="All">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>

                                    </div>
                                    <div class="col-sm-10">
                                        <div class="btn-outer text-right">
                                            <button id="addUserRoleBtn" class="blue-btn">Add User Role</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Main tabs -->
                            <div class="main-tabs">

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <!--Add user roles div starts here-->
                                                <div  style="display: none;" id="add_user_role_div">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 id="headerDiv" class="panel-title">
                                                                Role Access
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <form id="add_user_role_form">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-3">
                                                                                <label>Role Name <em class="red-star">*</em></label>
                                                                                <input name="role_name" id="role_name" maxlength="50" placeholder="Eg: Zonal Manager" class="form-control" type="text"/>
                                                                                <span id="role_nameErr"></span>
                                                                            </div>
                                                                            <div class="col-sm-2 wd-auto">
                                                                                <label>Copy Role</label>
                                                                                <input type="checkbox" name="copy_role" id="copy_role">
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <label>Roles</label>
                                                                                <select class="form-control" name="role" id="role" disabled>
                                                                                    <option>Select</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <!--Allowed Module Starts Here-->
                                                                <div class="main-tabs">
                                                                    <div class="tab-content">
                                                                        <div role="tabpanel" class="tab-pane active" id="yorkminster">
                                                                            <div class="form-hdr">
                                                                                <h3>Select Allowed Module</h3>
                                                                            </div>
                                                                            <div id="chartAccount">
                                                                                <div class="form-data">
                                                                                    <div class="col-sm-12">

                                                                                        <div class="collapse in" >
                                                                                            <!-- Permissions Start-->
                                                                                            <div id="tree-container">
                                                                                                <ul>
                                                                                                    <!--Setup Module-->
                                                                                                    <li>Setup
                                                                                                        <ul>
                                                                                                            <li>Company Setup
                                                                                                                <ul>
                                                                                                                    <li>Account Setup</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Property Setup
                                                                                                                <ul>
                                                                                                                    <li>Property Type
                                                                                                                        <ul>
                                                                                                                            <li>Add property Type</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                            <li>Import Property Type</li>
                                                                                                                            <li>Download Sample</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Unit Type
                                                                                                                        <ul>
                                                                                                                            <li>Add Unit Type</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>



                                                                                                                            <li>Import Unit Type</li>
                                                                                                                            <li>Download Sample</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Property Sub-Type
                                                                                                                        <ul>
                                                                                                                            <li>Add Property Sub Type</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                            <li>Import property Sub Type</li>
                                                                                                                            <li>Download Sample</li>
                                                                                                                            <li>Activate/Deactivate</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Property Style
                                                                                                                        <ul>
                                                                                                                            <li>Add Property Style</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                            <li>Import Property Style</li>
                                                                                                                            <li>Download Sample</li>
                                                                                                                            <li>Activate/Deactivate</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Property Groups
                                                                                                                        <ul>
                                                                                                                            <li>Add Group</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                            <li>Import Property Group</li>
                                                                                                                            <li>Download Sample</li>
                                                                                                                            <li>Activate/Deactivate</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Amenities
                                                                                                                        <ul>
                                                                                                                            <li>Add Amenities</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                            <li>Activate/Deactivate</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Accounting
                                                                                                                <ul>
                                                                                                                    <li>Account Type
                                                                                                                        <ul>
                                                                                                                            <li>Add Account Type</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                            <li>Activate/Deactivate</li>
                                                                                                                            <li>Import Account Type</li>
                                                                                                                            <li>Download Sample</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Chart of Accounts
                                                                                                                        <ul>
                                                                                                                            <li>Add Chart of account</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                            <li>Activate/Deactivate</li>
                                                                                                                            <li>Import Chart of Account</li>
                                                                                                                            <li>Download Sample</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Accounting Preferences</li>
                                                                                                                    <li>Charge Code
                                                                                                                        <ul>
                                                                                                                            <li>Add charge code</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                            <li>Activate/Deactivate</li>
                                                                                                                            <li>Import Charge Code</li>
                                                                                                                            <li>Download Sample</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Charge Code Preferences
                                                                                                                        <ul>
                                                                                                                            <li>Add charge code Prefernece</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                            <li>Activate/Deactivate</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Allocation Order</li>
                                                                                                                    <li>Check Setup</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Users
                                                                                                                <ul>
                                                                                                                    <li>Manage Users
                                                                                                                        <ul>
                                                                                                                            <li>Add New User</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                            <li>Activate/Deactivate</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Manage User Roles
                                                                                                                        <ul>
                                                                                                                            <li>Add User Role</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Password Setup</li>
                                                                                                                    <li>Login History</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Vendor
                                                                                                                <ul>
                                                                                                                    <li>Vendor Type
                                                                                                                        <ul>
                                                                                                                            <li>Add vendor Type</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Maintenance
                                                                                                                <ul>
                                                                                                                    <li>Category
                                                                                                                        <ul>
                                                                                                                            <li>Add category</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                            <li>Activate/Deactivate</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Priority
                                                                                                                        <ul>
                                                                                                                            <li>Add Priority</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                            <li>Activate/Deactivate</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Severity
                                                                                                                        <ul>
                                                                                                                            <li>Add Severity</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                            <li>Activate/Deactivate</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Work Order Type
                                                                                                                        <ul>
                                                                                                                            <li>Add work Order Type</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Alerts
                                                                                                                <ul>
                                                                                                                    <li>User Alerts
                                                                                                                        <ul>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Preview</li>
                                                                                                                            <li>Test Mail</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Events
                                                                                                                <ul>
                                                                                                                    <li>Event Types
                                                                                                                        <ul>
                                                                                                                            <li>Add Event Type</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Delete</li>
                                                                                                                            <li>Activate/Deactivate</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Trash Bin
                                                                                                                <ul>
                                                                                                                    <li>Property Type Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Unit Type Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Property Sub-Type Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Property Style Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Property Groups Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Amenities Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Account Type Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Chart of Accounts Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Accounting Prefernece Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Charge Code Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Charge Code Preferences Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Manage Users Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Manage User Roles Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Vendor Type Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Category Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Priority Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Severity Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Event Types Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Contacts Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Work Order Type Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Building Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Unit Trash
                                                                                                                        <ul>
                                                                                                                            <li>Restore</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                    <!--Dashboard Module-->
                                                                                                    <li>Dashboard</li>
                                                                                                    <!-- Properties Module-->
                                                                                                    <li>Properties
                                                                                                        <ul>
                                                                                                            <li>Add Property</li>
                                                                                                            <li>Import Property</li>
                                                                                                            <li>Edit Property</li>
                                                                                                            <li>View Property</li>
                                                                                                            <li>Deactivate/Activate Property</li>
                                                                                                            <li>Add Building</li>
                                                                                                            <li>Edit Building</li>
                                                                                                            <li>View Building</li>
                                                                                                            <li>Deactivate/Activate Building</li>
                                                                                                            <li>Add Unit</li>
                                                                                                            <li>Edit Unit</li>
                                                                                                            <li>View Unit</li>
                                                                                                            <li>Deactivate/Activate Unit</li>
                                                                                                            <li>Delete Building</li>
                                                                                                            <li>Delete Unit</li>
                                                                                                            <li>Property Amortization</li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                    <!-- People Module-->
                                                                                                    <li>People
                                                                                                        <ul>
                                                                                                            <li>Tenants
                                                                                                                <ul>
                                                                                                                    <li>New Tenant</li>
                                                                                                                    <li>View Tenant Details
                                                                                                                        <ul>
                                                                                                                            <li>View General Details</li>
                                                                                                                            <li>View Charge Details</li>
                                                                                                                            <li>View Payment Details</li>
                                                                                                                            <li>View Ledger Details</li>
                                                                                                                            <li>View Occupants</li>
                                                                                                                            <li>View History/Notes</li>
                                                                                                                            <li>View File</li>
                                                                                                                            <li>View Rent Insurance</li>
                                                                                                                            <li>View Tenant portal</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Edit Tenant Details
                                                                                                                        <ul>
                                                                                                                            <li>Edit General</li>
                                                                                                                            <li>Edit Charge</li>
                                                                                                                            <li>Edit Occupants</li>
                                                                                                                            <li>Edit History</li>
                                                                                                                            <li>Edit File</li>
                                                                                                                            <li>Edit Rent Insurance</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Import Tenant</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Owners
                                                                                                                <ul>
                                                                                                                    <li>New Owner</li>
                                                                                                                    <li>Edit Owner</li>
                                                                                                                    <li>View Owner</li>
                                                                                                                    <li>Import Owner</li>
                                                                                                                    <li>Owner Draw</li>
                                                                                                                    <li>Owner Receipt</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Vendors
                                                                                                                <ul>
                                                                                                                    <li>New Vendor</li>
                                                                                                                    <li>View Vendor Details
                                                                                                                        <ul>
                                                                                                                            <li>View General Details</li>
                                                                                                                            <li>View Payable</li>
                                                                                                                            <li>View Ledger Details</li>
                                                                                                                            <li>View Payment Settings</li>
                                                                                                                            <li>View History/Notes</li>
                                                                                                                            <li>View File</li>
                                                                                                                            <li>View WorkOrder</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Edit Vendor Details
                                                                                                                        <ul>
                                                                                                                            <li>Edit General Details</li>
                                                                                                                            <li>Edit History/Notes</li>
                                                                                                                            <li>Edit File</li>
                                                                                                                            <li>Edit Payment Settings</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Deactivate /Activate</li>
                                                                                                                    <li>Import Vendor</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Contacts
                                                                                                                <ul>
                                                                                                                    <li>New Contact</li>
                                                                                                                    <li>Import Contact</li>
                                                                                                                    <li>Edit Contact</li>
                                                                                                                    <li>Delete Contact</li>
                                                                                                                    <li>View Contact</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Send Tenant Statement</li>
                                                                                                            <li>New Deposit</li>
                                                                                                            <li>Deposit Register
                                                                                                                <ul>
                                                                                                                    <li>Edit</li>
                                                                                                                    <li>Mark Deposited</li>
                                                                                                                    <li>Mark Cleared</li>
                                                                                                                    <li>Print</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Instrument Register</li>
                                                                                                            <li>Tenant Instrument Register</li>
                                                                                                            <li>Vendor Instrument Register</li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                    <!--Leases Module-->
                                                                                                    <li>Leases
                                                                                                        <ul>
                                                                                                            <li>Guest Card
                                                                                                                <ul>
                                                                                                                    <li>New Guest card</li>
                                                                                                                    <li>View Guest card
                                                                                                                        <ul>
                                                                                                                            <li>Book a site Visit</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Edit Guest card</li>
                                                                                                                    <li>Generate Rental Application</li>
                                                                                                                    <li>Generate Lease</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Rental Application
                                                                                                                <ul>
                                                                                                                    <li>New Rental Application</li>
                                                                                                                    <li>View Rental Application
                                                                                                                        <ul>
                                                                                                                            <li>Run Credit and Background Check</li>
                                                                                                                            <li>Approve</li>
                                                                                                                            <li>Checklist</li>
                                                                                                                            <li>Download</li>
                                                                                                                            <li>Print</li>
                                                                                                                            <li>Generate Lease</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Edit Rental Application</li>
                                                                                                                    <li>Run Background Check</li>
                                                                                                                    <li>Print Blank</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Leases
                                                                                                                <ul>
                                                                                                                    <li>New Lease</li>
                                                                                                                    <li>View Lease
                                                                                                                        <ul>
                                                                                                                            <li>View Lease Details</li>
                                                                                                                            <li>View Charges</li>
                                                                                                                            <li>View File</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Edit Lease
                                                                                                                        <ul>
                                                                                                                            <li>Lease Details</li>
                                                                                                                            <li>Charges</li>
                                                                                                                            <li>File Library</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Lease Renewal
                                                                                                                        <ul>
                                                                                                                            <li>Renewal Lease</li>
                                                                                                                            <li>Send Renewal Letter</li>
                                                                                                                            <li>Print Letter</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Move-In
                                                                                                                <ul>
                                                                                                                    <li>Move-In</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Unit Vacancy Details</li>
                                                                                                            <li>Lease Affordability</li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                    <!-- Reports Module-->
                                                                                                    <li>Reports</li>
                                                                                                    <!-- Maintenance Module-->
                                                                                                    <li>Maintenance
                                                                                                        <ul>
                                                                                                            <li>Ticket
                                                                                                                <ul>
                                                                                                                    <li>View Ticket</li>
                                                                                                                    <li>Delete Ticket</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Work Order
                                                                                                                <ul>
                                                                                                                    <li>View Work Order</li>
                                                                                                                    <li>Edit Work Order</li>
                                                                                                                    <li>Delete Work Order</li>
                                                                                                                    <li>Create WorkOrder</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Inspection
                                                                                                                <ul>
                                                                                                                    <li>View Inspection</li>
                                                                                                                    <li>Edit Inspection</li>
                                                                                                                    <li>Delete Inspection</li>
                                                                                                                    <li>New Inspection</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                    <!--Accounting Module-->
                                                                                                    <li>Accounting
                                                                                                        <ul>
                                                                                                            <li>Receivables
                                                                                                                <ul>
                                                                                                                    <li>Receive Payment</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Payables
                                                                                                                <ul>
                                                                                                                    <li>Pay selected Bills</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Banking
                                                                                                                <ul>
                                                                                                                    <li>Write Check</li>
                                                                                                                    <li>Bank Register</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Journal Enteries
                                                                                                                <ul>
                                                                                                                    <li>View</li>
                                                                                                                    <li>Reverse JE</li>
                                                                                                                    <li>New Journal Entry</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Budgeting
                                                                                                                <ul>
                                                                                                                    <li>New Budget</li>
                                                                                                                    <li>Copy Budget</li>
                                                                                                                    <li>Edit Budget</li>
                                                                                                                    <li>View Budget</li>
                                                                                                                    <li>Accounting Closing</li>
                                                                                                                    <li>Owner Draw</li>
                                                                                                                    <li>Owner Reciept</li>
                                                                                                                    <li>Transactions</li>
                                                                                                                    <li>Process Management fees</li>
                                                                                                                    <li>Pending Transations</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Bank Reconcillation
                                                                                                                <ul>
                                                                                                                    <li>ReConcile Now</li>
                                                                                                                    <li>View</li>
                                                                                                                    <li>Print</li>
                                                                                                                    <li>Edit</li>
                                                                                                                    <li>Delete</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>EFT
                                                                                                                <ul>
                                                                                                                    <li>EFT Tenants
                                                                                                                        <ul>
                                                                                                                            <li>Add Tenant</li>
                                                                                                                            <li>View Tenant</li>
                                                                                                                            <li>Edit Tenant</li>
                                                                                                                            <li>Delete Tenant</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>EFT Transmission
                                                                                                                        <ul>
                                                                                                                            <li>Save</li>
                                                                                                                            <li>Process EFT</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>EFT Register
                                                                                                                        <ul>
                                                                                                                            <li>View</li>
                                                                                                                            <li>Edit</li>
                                                                                                                            <li>Upload To Bank</li>
                                                                                                                            <li>Delete</li>
                                                                                                                            <li>Process EFT File</li>
                                                                                                                            <li>Download</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Bill
                                                                                                                <ul>
                                                                                                                    <li>New Bill</li>
                                                                                                                    <li>View Details</li>
                                                                                                                    <li>Print</li>
                                                                                                                    <li>Edit Bill</li>
                                                                                                                    <li>Delete Bill</li>
                                                                                                                    <li>On Hold</li>
                                                                                                                    <li>Due Bill</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Apply Late Fee</li>
                                                                                                            <li>Account Closing</li>
                                                                                                            <li>Transactions</li>
                                                                                                            <li>Process Management Fees</li>
                                                                                                            <li>Pending Transactions
                                                                                                                <ul>
                                                                                                                    <li>Pending Transations</li>
                                                                                                                    <li>Tenant Transaction & NSF</li>
                                                                                                                    <li>Tenant Payment</li>
                                                                                                                    <li>Security Deposit</li>
                                                                                                                    <li>Owner Draw</li>
                                                                                                                    <li>Security Deposit</li>
                                                                                                                    <li>Owner Receipt</li>
                                                                                                                    <li>Unit Vacancy</li>
                                                                                                                    <li>Vendor Invoice</li>
                                                                                                                    <li>Vendor Payment</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Recurring transaction
                                                                                                                <ul>
                                                                                                                    <li>Recurring transaction</li>
                                                                                                                    <li>Recurring Bills</li>
                                                                                                                    <li>Recurring Invoices</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Utility Billing
                                                                                                                <ul>
                                                                                                                    <li>Pay Utility Bill</li>
                                                                                                                    <li>Edit</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                    <!--Communication Module-->
                                                                                                    <li>Communication
                                                                                                        <ul>
                                                                                                            <li>Email
                                                                                                                <ul>
                                                                                                                    <li>Drafts
                                                                                                                        <ul>
                                                                                                                            <li>Delete Mail</li>
                                                                                                                            <li>Open Mail</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Compose email</li>
                                                                                                                    <li>Sent Mails</li>
                                                                                                                    <li>Forward Mail</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Message
                                                                                                                <ul>
                                                                                                                    <li>Drafts
                                                                                                                        <ul>
                                                                                                                            <li>Delete Message</li>
                                                                                                                            <li>Open Message</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Compose message</li>
                                                                                                                    <li>Sent Message</li>
                                                                                                                    <li>Forward Message</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Letter & Notices
                                                                                                                <ul>
                                                                                                                    <li>Create new letter</li>
                                                                                                                    <li>Edit</li>
                                                                                                                    <li>Send</li>
                                                                                                                    <li>Preview</li>
                                                                                                                    <li>Download</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>New Conversation</li>
                                                                                                            <li>Group/Message</li>
                                                                                                            <li>Task And Reminder
                                                                                                                <ul>
                                                                                                                    <li>New Task And Reminder</li>
                                                                                                                    <li>Delete</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                    <!--Marketing Module-->
                                                                                                    <li>Marketing
                                                                                                        <ul>
                                                                                                            <li>Listing
                                                                                                                <ul>
                                                                                                                    <li>View Details
                                                                                                                        <ul>
                                                                                                                            <li>Units</li>
                                                                                                                            <li>Analytics</li>
                                                                                                                            <li>Edit</li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>Vacant Units</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Flyers
                                                                                                                <ul>
                                                                                                                    <li>Upload</li>
                                                                                                                    <li>Preview</li>
                                                                                                                    <li>Set as Default</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Campaign
                                                                                                                <ul>
                                                                                                                    <li>Add new Campaign</li>
                                                                                                                    <li>Edit</li>
                                                                                                                    <li>Delete</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Settings
                                                                                                                <ul>
                                                                                                                    <li>Edit</li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                            <li>Map</li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                    <!-- Settings Module-->
                                                                                                    <li>Settings
                                                                                                        <ul>
                                                                                                            <li>Settings</li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            <!-- Permissions End-->

                                                                                            <!-- Permissions Copy Start-->
                                                                                            <div id="tree-container_copy">
                                                                                            </div>
                                                                                            <!--Permissions Copy End-->

                                                                                            <!--Permissions Edit Start-->
                                                                                            <div id="tree-container_edit">
                                                                                            </div>
                                                                                            <!--Permissions Edit End-->
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="btn-outer form-outer text-right">
                                                                    <input type="hidden" name="edit_user_role_id" id="edit_user_role_id" class="blue-btn" >
                                                                    <input type="hidden" name="copy_user_role_id" id="copy_user_role_id" class="blue-btn" >
                                                                    <input type="submit" id="addUserRoleSaveBtn" class="blue-btn" value="Save">
                                                                    <a><input type="button" class="clear-btn clearFormReset" value="Clear"></a>
                                                                    <a id="addUserRoleCancelBtn"><input type="button" class="grey-btn" value="Cancel"></a>
                                                                </div>
                                                                <!--Allowed Module Ends Here-->
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Add user roles div ends here-->

                                                <!--List user roles div starts here-->
                                                <div class="panel panel-default listUserRoleDiv">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table id="ManageUserRoles-table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--List user roles div ends here-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    $('#leftnav4').addClass('in');
    $('.manage_user_Roles').addClass('active');
    var defaultFormData = '';
    $(document).on('click','.formreset',function () {
        resetEditForm("#add_user_role_form",[],true,defaultFormData,[]);
    });
    $(document).on('click','.clearFormReset',function () {
        resetFormClear("#add_user_role_form",['role'],'form',false);
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/adminUsers/manageUserRoles.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/common.js"></script>
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/plugin/jstree/dist/themes/default/style.min.css" />
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/plugin/jstree/dist/jstree.min.js"></script>

<script>
    $('input[id="ddate"]').datepicker({
        dateFormat     : datepicker
    });
    $('input[id="date"]').datepicker({
        dateFormat     : 'yy'

    });

</script>
<!-- Jquery Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>