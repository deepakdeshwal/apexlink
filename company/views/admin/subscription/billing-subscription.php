<?php

/**

 * Created by PhpStorm.

 * User: ranavivek2567

 * Date: 1/18/2019

 * Time: 11:19 AM

 */



if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {

    $url = SUBDOMAIN_URL;

    header('Location: ' . $url);

}

?>

<?php

include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");

?>

    <style type="text/css">

        .StripeElement {
            box-sizing: border-box;

            height: auto;

            padding: 10px 12px;

            border: 1px solid transparent;
            border-radius: 4px;
            background-color: white;

            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }

        #payNow {
            max-width: 600px;
            margin: auto;
            top: 20%;
        }
        #payment-form {
            background: #fff;
            padding: 20px;
        }
        #payment-form label{
            font-size: 13px;
            /*border-bottom: 1px solid #cacaca;*/
            padding-bottom: 3px;
        }
        .mt-none {
            margin-top: 0 !important;
        }
        #card-errors
        {
            color:red;
        }
        #payment-form .payment-form-label label {
    font-size: 33px;
    font-weight: normal;
}
#payment-form .payment-form-label label span {
    color: #f00;
}
.payment-form-label .payment-method-text {
    margin: 0;
    text-align: center;
    font-size: 18px;
    text-decoration: underline;
    font-weight: 600;
}
img.payment-method-logo {
    max-width: 110px;
    margin-top: 110px;
}
button.blue-btn.payment-submit-button {
    margin-top: 118px;
}
#payment-form label.card-payment-text {
    font-size: 16px;
    color: #1c91cc;
}
.submit_payment_class-new {
    position: static;
}
        /*.CardField-input-wrapper .CardField-number {
            transform: translateX(0px) !important;
            float: left !important;
            border: 1px solid rgb(221, 221, 221) !important;
            width: 100% !important;
            border-radius: 5px !important;
            padding: 5px !important;
            height: auto !important;
        }
        .CardField-expiry {
            transform: translateX(0px);
            float: left;
            border: 1px solid rgb(221, 221, 221);
            width: 60%;
            border-radius: 5px;
            padding: 5px;
            height: auto;
        }
        .CardField-cvc {
            transform: translateX(0px);
            float: left;
          border: 1px solid rgb(221, 221, 221);
            width: 40%;
            border-radius: 5px;
            padding: 5px;
            height: auto;
        }*/
    </style>

    <div id="wrapper">

        <!-- Top navigation start -->

        <?php

        include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");

        ?>

        <!-- MAIN Navigation Ends -->


        <!-- Trigger the modal with a button -->


        <!-- Modal -->
        <div class="modal fade" id="payNow" role="dialog">
            Amount to paid - <div class='amountPaid'></div>
            <div class="modal-header">

                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>

            </div>

            <form action="/charge" method="post" id="payment-form">
                <div class="row payment-form-label">
                    <div class="col-sm-12">
                        <label>Amount to be paid -    <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$'; ?> <span currecy_padding"><?php echo $default_symbol ?></span> <span class='paidAmount currecy_padding'></span> </label>
                    </div>
                </div>
                 <div class="row payment-form-label">
                    <div class="col-sm-12">
                       <p  class="payment-method-text">Payment Method</p>
                    </div>
                </div>




                <input type='hidden' class='days_remaining'>
                <input type='hidden' class='plan_id'>
                <input type='hidden' class='term_plan'>


            <input type='hidden' class='days_remaining'>
           

                <div class="form-row">
                    <label for="card-element" class="card-payment-text">
                        Credit / Debit card
                    </label>
                    <div id="card-element">
                        <!-- A Stripe Element will be inserted here. -->
                    </div>

                    <!-- Used to display form errors. -->
                    <div id="card-errors" role="alert"></div>
                </div>
               <div class="row">
                  <div class="col-sm-6">
                      <img src="<?php echo COMPANY_SUBDOMAIN_URL;?><?php echo !empty($_SESSION[SESSION_DOMAIN]['company_logo'])? $_SESSION[SESSION_DOMAIN]['company_logo'] :  '/images/logo.png' ?>"  class="payment-method-logo">
                  </div>
                   <div class="col-sm-6">
                     <button class="blue-btn submit_payment_class submit_payment_class-new pull-right payment-submit-button">Submit Payment</button>
                  </div>
               </div>
               
            </form>
        </div>


        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    <!--                                    Maintenance >> <span>Tickets</span>-->
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="content-section">
                            <!--Tabs Starts -->
                            <div class="main-tabs">
                                <!-- Nav tabs -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="guest-cards">
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Billing & Subscription</h3>
                                            </div>
                                            <div class="form-data billing-subscription">
                                                <h4>
                                                    Company Information
                                                </h4>
                                                <a class="clear pull-left grid-link subscription_modal" data-toggle="modal" data-target="#billing-subscription" href="javascript:;">Online Payment</a> <span class="account_verification_status"></span><span class="right_tick" style="display:none"><i class="fa fa-check-circle-o fa-lg text-success" ></i></span>
                                                <div class="unverified-icon">
                                                    <span><i class="fa fa-ban fa-lg" ></i></span>
                                                </div>
                                                <a class="clear pull-left grid-link"  href="/User/AccountSetup">Edit Account Information</a>



                                                <input type='button' class="clear pull-right payNow blue-btn mt-none" value='Pay Now'>



                                                <div class="subscription-box">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <h4 class="units"></h4>
                                                            <a class="blue-btn" href="/User/UserPlans">Change Subscription</a>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="detail-outer">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <label><h4 class="text-right">Next Charge Amount :</h4></label>
                                                                        <span class="next_charge_amount">--</span>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <label class="text-right">Payment Due Date :</label>
                                                                        <span class="due_date">--</span>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <label class="text-right">Payment Method :</label>
                                                                        <span class="default_source">--</span>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <label></label>
                                                                        <span class="text-right">
                                                <a href="javascript:;" id="edit_payment_method" class="grid-link">Edit Payment Method</a>
                                              </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Form Outer Ends -->
                                        <div class="grid-outer" id="only_list_mode">
                                            <div class="apx-table">
                                                <div class="table-responsive">
                                                    <table id="SubscriptionHistory-table" class="table table-bordered">
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <!--tab Ends -->

                            </div>

                        </div>
                    </div>
                </div>
        </section>

    </div>

    <div class="overlay">
        <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
            <img width="200"  height="200" src='<?php echo COMPANY_SUBDOMAIN_URL ?>/images/loading.gif'/>
        </div>
    </div>

    <div class="modal fade" id="billing-subscription" role="dialog">
        <input type="hidden" id="business_type" name="business_type" value="">
        <input type="hidden" id="account_verification"  value="">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>

                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="steps">
                        <ul>
                            <li class="payment_li">
                                <a id="paymentDiv" class="paymentDiv active" href="javascript:;">1</a>
                                <span >Payment</span>
                            </li>
                            <li class="basicDiv_li">
                                <a id="basicDiv" class="basicDiv" href="javascript:;">2</a>
                                <span class="basic_div_span">Basic</span>
                            </li>
                        </ul>
                    </div>
                    <div class="apx-adformbox-content">
                        <form method="post" id="financialCardInfo" name="financialCardInfo">
                            <h3>Payment Method <em class="red-star">*</em></h3>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label></label>
                                        <select class="form-control" name="payment_method" id="payment_method">
                                            <option value="1">Credit Card/Debit Card</option>
                                            <option value="2">ACH</option>
                                        </select>
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4 clear">
                                        <label>Card Type<em class="red-star">*</em></label>
                                        <img src="/company/images/card-payment.png" style="width: 190px;">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-4 col-md-4 clear">
                                        <label>Card Holder First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn capital" onkeypress="return (event.charCode > 64 &&
	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type="text" id="cfirst_name" name="cfirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Card Holder Last Name</label>
                                        <input class="form-control capsOn capital" onkeypress="return (event.charCode > 64 &&
	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type="text" id="clast_name" name="clast_name">
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Card Number <em class="red-star">*</em></label>
                                        <input class="form-control capsOn hide_copy" type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" id="ccard_number" name="ccard_number" type="number" maxlength="16" placeholder="1234 1234 1234 1234">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label> Expiry Year <em class="red-star">*</em></label>
                                        <select class="form-control"  id="cexpiry_year" name="cexpiry_year" >
                                        </select>
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4 fphone_num">
                                        <label> Expiry Month <em class="red-star">*</em></label>
                                        <select class="form-control"  id="cexpiry_month" name="cexpiry_month" >
                                        </select>
                                        <span class="fphone_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>CVC<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" id="ccvv" name="ccvv" type="number" maxlength="4">
                                        <span class="fbirth_dateErr error red-star"></span>
                                    </div>
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Company <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control capital" id="cCompany" name="cCompany" value="">-->
<!--                                        <span class="fzipcodeErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4 clear">-->
<!--                                        <label>Card Billing Phone Number <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control phone_format" id="cphoneNumber" name="cphoneNumber" value="" >-->
<!--                                        <span class="fcityErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Card Billing Address Line 1 <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control capital" id="caddress1" name="caddress1">-->
<!--                                        <span class="fstateErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Card Billing Address Line 2 </label>-->
<!--                                        <input type="text" class="form-control capital" id="caddress2" name="caddress2" value="">-->
<!--                                        <span class="faddressErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>City<em class="red-star">*</em></label>-->
<!--                                        <input class="form-control capsOn capital" type="text" id="ccity" name="ccity" readonly>-->
<!--                                        <span class="fbusinessErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>State/Province <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control capital" id="cstate" name="cstate" value="" maxlength="4" readonly>-->
<!--                                        <span class="fssnErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Zip/Postal Code<em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control" id="czip_code" name="czip_code" value="--><?php //$_SESSION[SESSION_DOMAIN]['default_zipcode'] ?><!--" >-->
<!--                                        <span class="faccount_holderErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Country <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control capital" id="ccountry" name="ccountry" value="" >-->
<!--                                        <span class="faccount_holdertypeErr error red-star"></span>-->
<!--                                    </div>-->
                                </div>
                            </div>
                            <div class="row" style="text-align: center;">
                                <button type="submit"  id="savefinancialCard" class="blue-btn">Continue</button>
                            </div>

                        </form>
                    </div>
                    <div class="basic-payment-detail" style="display: none;">
                        <div class="row">

                            <form method="post" id="financialCompanyInfo">
                                <input type="hidden" id="company_document_id" name="company_document_id" value="">
                                <div class="form-outer">
                                    <div class="col-sm-12 col-md-3">
                                        <label>Country<em class="red-star">*</em></label>
                                        <select class="form-control capital" name="fcountry" readonly="">
                                            <option value="usa" selected>USA</option>
                                        </select>
                                        <span class="fcountryErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>MCC <em class="red-star">*</em></label>
                                        <select class="form-control" id="fcmcc" name="fmcc" readonly="">
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>URL <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fcurl" name="furl">
                                        <span class="furlErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Business Type<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fcbusiness" name="fbusiness" readonly>
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Account Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control hide_copy" id="fcaccount_number" name="faccount_number" value="">
                                        <span class="faccount_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Routing Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fprouting_number" name="frouting_number" value="">
                                        <span class="frouting_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fccity" name="fccity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fcaddress" name="fcaddress" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2</label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fcaddress2" name="fcaddress2" >
                                        <span class="fcaddress2Err error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fczipcode" name="fczipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fcstate" name="fcstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Name <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fcname" name="fcname" value="">
                                        <span class="fcnameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fcphone_number" name="fcphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="fcphone_number_prefix" name="phone_number_prefix">
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Tax_id <em class="red-star">*</em></label>
                                        <input   class="form-control" fixedLength="10" id="fctax_id" name="fctax_id" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Company Document <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">Upload Company Document here.</span></div></label>
                                        <input type='file' name="company_document" id="company_document" />
                                        <span class="company_document_idErr error red-star"></span>
                                    </div>




                                </div>
                                <div class="col-sm-12">
                                    <div class="form-hdr">
                                        <h3>Person</h3>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-outer mt-15">
                                        <div class="col-sm-12 col-md-3">
                                            <label>First Name <em class="red-star">*</em></label>
                                            <input class="form-control capsOn capital" type="text" id="fpfirst_name" name="ffirst_name">
                                            <span class="ffirst_nameErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Last Name <em class="red-star">*</em></label>
                                            <input class="form-control capsOn capital" type="text" id="fplast_name" name="flast_name">
                                            <span class="flast_nameErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Email <em class="red-star">*</em></label>
                                            <input class="form-control" type="email" id="fpemail" name="femail" >
                                            <span class="femailErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Phone <em class="red-star">*</em></label>
                                            <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fpphone_number" name="fphone_number" value="">
                                            <span class="fphone_numberErr error red-star"></span>
                                            <input type="hidden" value="" id="phone_number_prefix" name="phone_number_prefix">
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label> Day <em class="red-star">*</em></label>
                                            <select class="form-control"  id="fpday" name="fday" >
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Month <em class="red-star">*</em></label>
                                            <select class="form-control"  id="fpmonth" name="fmonth" >
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Year <em class="red-star">*</em></label>
                                            <select class="form-control"  id="fpyear" name="fyear" >
                                            </select>
                                            <span class="fyearErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="fpssn" name="fssn" value="" maxlength="4">
                                            <span class="fssnErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-12 col-md-3">
                                            <label>Address 1 <em class="red-star">*</em></label>
                                            <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                            <input class="form-control capital" id="fpaddress" name="faddress" >
                                            <span class="faddressErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Address 2</label>
                                            <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                            <input class="form-control capital" id="fpaddress2" name="faddress2" >
                                            <span class="faddress2Err error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Postal Code <em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="fpzipcode" name="fzipcode" value=""  maxlength="9">
                                            <span class="fzipcodeErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>State <em class="red-star">*</em></label>
                                            <input type="text" class="form-control capital" id="fpstate" name="fstate" value="" readonly>
                                            <span class="fstateErr error red-star"></span>
                                        </div>
                                    </div>
                                    <div class="form-outer">
                                        <div class="col-sm-12 col-md-3">
                                            <label>City <em class="red-star">*</em></label>
                                            <input type="text" class="form-control capital" id="fpcity" name="fcity" value="" >
                                            <span class="fcityErr error red-star"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <button type="submit"  id="savecompanyfinancial" class="blue-btn">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="basic-user-payment-detail" style="display: none;">
                        <div >
                            <form method="post" id="financialInfo">

                                <div class="form-outer">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-3">
                                            <label>Country<em class="red-star">*</em></label>
                                            <select class="form-control" id="fcountry" name="fcountry" readonly="">
                                                <option value="usa" selected>USA</option>
                                            </select>
                                            <span class="fbusinessErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>MCC <em class="red-star">*</em></label>
                                            <select class="form-control" id="fmcc" name="fmcc" readonly="">
                                            </select>
                                        </div>

                                        <div class="col-sm-12 col-md-3">
                                            <label>URL <em class="red-star">*</em></label>
                                            <input class="form-control capsOn" type="text" id="furl" name="furl">
                                            <span class="furl_nameErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-12 col-md-3">
                                            <label>Business Type<em class="red-star">*</em></label>
                                            <input class="form-control capsOn" type="text" id="fbusiness" name="fbusiness" readonly>
                                            <!--                                        <select class="form-control" id="fbusiness" name="fbusiness" readonly="">-->
                                            <!--                                            <option value="individual" selected>Individual</option>-->
                                            <!--                                        </select>-->
                                            <span class="fbusinessErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Account Number<em class="red-star">*</em></label>
                                            <input type="text" class="form-control hide_copy" id="faccount_number" name="faccount_number" value="">
                                            <span class="faccount_numberErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Routing Number<em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="frouting_number" name="frouting_number" value="">
                                            <span class="frouting_numberErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>City <em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="fcity" name="fcity" value="" >
                                            <span class="fcityErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Address 1 <em class="red-star">*</em></label>
                                            <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                            <input class="form-control capital" id="faddress" name="faddress" >
                                            <span class="faddressErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Address 2</label>
                                            <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                            <input class="form-control capital" id="faddress2" name="faddress2" >
                                            <span class="faddressErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Postal Code <em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="fzipcode" name="fzipcode" value=""  maxlength="9">
                                            <span class="fzipcodeErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>State <em class="red-star">*</em></label>
                                            <input type="text" class="form-control capital" id="fstate" name="fstate" value="" readonly>
                                            <span class="fstateErr error red-star"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-outer">
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label> Day <em class="red-star">*</em></label>
                                            <select class="form-control"  id="fday" name="fday" >
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Month <em class="red-star">*</em></label>
                                            <select class="form-control"  id="fmonth" name="fmonth" >
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Year <em class="red-star">*</em></label>
                                            <select class="form-control"  id="fyear" name="fyear" >
                                            </select>
                                            <span class="fyearErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-12 col-md-3">
                                            <label>Email <em class="red-star">*</em></label>
                                            <input class="form-control" type="email" id="femail" name="femail" >
                                            <span class="femailErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>First Name <em class="red-star">*</em></label>
                                            <input class="form-control capsOn" type="text" id="ffirst_name" name="ffirst_name">
                                            <span class="ffirst_nameErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Last Name <em class="red-star">*</em></label>
                                            <input class="form-control capsOn" type="text" id="flast_name" name="flast_name">
                                            <span class="flast_nameErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Phone <em class="red-star">*</em></label>
                                            <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fphone_number" name="fphone_number" value="">
                                            <span class="fphone_numberErr error red-star"></span>
                                            <input type="hidden" value="" id="phone_number_prefix" name="phone_number_prefix">
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="fssn" name="fssn" value="" maxlength="4">
                                            <span class="fssnErr error red-star"></span>
                                        </div>

                                    </div>
                                </div>
                                <button type="submit"  id="savefinancial" class="blue-btn">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Wrapper Ends -->

    <div class="container">
        <div class="modal fade" id="financial-infotype" role="dialog">
            <div class="modal-dialog modal-md" style="width: 60%;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                        <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                        </a>
                        <h4 class="modal-title">Online Payment Details</h4>
                    </div>
                    <div class="modal-body">
                        <div class="financial-infotype-radio">
                            <label class="radio-inline">
                                <input type="radio" name="companytype" checked value="individual"> Individual
                            </label>
                            <input type="hidden" value="<?php echo $_SESSION[SESSION_DOMAIN]['cuser_id'] ?>" id="hidden_company_id" >
                            <label class="radio-inline">
                                <input type="radio" name="companytype" value="company" >Company
                            </label>
                        </div>

                        <div class="financial-infotype-btn text-right">
                            <button type="button"  id="savefinancialtype" class="blue-btn">Submit</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="modal fade" id="financial-info" role="dialog">
            <div class="modal-dialog modal-md" style="width: 60%;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                        <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                        </a>

                        <h4 class="modal-title">Online Payment Details</h4>
                    </div>
                    <div class="modal-body">
                        <div class="apx-adformbox-content" id="financial-indiviual">
                            <form method="post" id="financialInfoIndiviual">
                                <div class="form-outer">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-3">
                                            <label>Country<em class="red-star">*</em></label>
                                            <select class="form-control" id="fcountry" name="fcountry" readonly="">
                                                <option value="usa" selected>USA</option>
                                            </select>
                                            <span class="fbusinessErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fmcc">
                                            <label>MCC <em class="red-star">*</em></label>
                                            <select class="form-control" id="fmcc" name="fmcc" readonly="">
                                            </select>
                                        </div>

                                        <div class="col-sm-12 col-md-3 furl">
                                            <label>URL <em class="red-star">*</em></label>
                                            <input class="form-control capsOn" type="text" id="furl" name="furl">
                                            <span class="furl_nameErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-12 col-md-3">
                                            <label>Business Type<em class="red-star">*</em></label>
                                            <input class="form-control capsOn" type="text" id="fbusiness" name="fbusiness" value="individual" readonly>
                                            <span class="fbusinessErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Account Number<em class="red-star">*</em></label>
                                            <input type="text" class="form-control hide_copy" id="faccount_number" name="faccount_number" value="">
                                            <span class="faccount_numberErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Routing Number<em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="frouting_number" name="frouting_number" value="">
                                            <span class="frouting_numberErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>City <em class="red-star">*</em></label>
                                            <input type="text" class="form-control capital" id="fppcity" name="fcity" value="" >
                                            <span class="fcityErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Address 1 <em class="red-star">*</em></label>
                                            <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                            <input class="form-control capital" id="faddress" name="faddress" >
                                            <span class="faddressErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Address 2</label>
                                            <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                            <input class="form-control capital" id="faddress2" name="faddress2" >
                                            <span class="faddressErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Postal Code <em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="fppzipcode" name="fzipcode" value=""  maxlength="9">
                                            <span class="fzipcodeErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>State <em class="red-star">*</em></label>
                                            <input type="text" class="form-control capital" id="fppstate" name="fstate" value="" readonly>
                                            <span class="fstateErr error red-star"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-outer">
                                        <div class="date-outer-form">
                                            <label class="col-sm-12"> Date of birth    <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">DATE OF BIRTH</span></div></label>
                                        <div class="col-sm-12 col-md-3 fphone_num">
<!--                                            <label> Day <em class="red-star">*</em></label>-->
                                            <select class="form-control fpday"  id="fday" name="fday" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fdayErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
<!--                                            <label>Month <em class="red-star">*</em></label>-->
                                            <select class="form-control fpmonth"  id="fmonth" name="fmonth" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
<!--                                            <label>Year <em class="red-star">*</em></label>-->
                                            <select class="form-control fpyear"  id="fyear" name="fyear" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fyearErr error red-star"></span>
                                        </div>
                                        </div>

                                        <div class="col-sm-12 col-md-3">
                                            <label>Email <em class="red-star">*</em></label>
                                            <input class="form-control femail" type="email" id="femail" name="femail" >
                                            <span class="femailErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>First Name <em class="red-star">*</em></label>
                                            <input class="form-control capsOn fname capital" type="text" id="ffirst_name" name="ffirst_name">
                                            <span class="ffirst_nameErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Last Name <em class="red-star">*</em></label>
                                            <input class="form-control capsOn lname capital" type="text" id="flast_name" name="flast_name">
                                            <span class="flast_nameErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Phone <em class="red-star">*</em></label>
                                            <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control fphone phone_number_format" fixedLength="10" id="fphone_number" name="fphone_number" value="">
                                            <span class="fphone_numberErr error red-star"></span>
                                            <input type="hidden" value="" id="phone_number_prefix" class="fphonepre" name="phone_number_prefix">
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="fssn" name="fssn" value="" maxlength="4">
                                            <span class="fssnErr error red-star"></span>
                                        </div>

                                    </div>
                                </div>
                                <button type="submit"  id="savefinancialindi" class="blue-btn">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="modal fade" id="financial-company-info" role="dialog">
            <div class="modal-dialog modal-md" style="width: 60%;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                        <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                        </a>

                        <h4 class="modal-title">Online Payment Details</h4>
                    </div>
                    <div class="modal-body">
                        <div class="apx-adformbox-content" id="financial-company">
                            <form method="post" id="financialCompanyInfoCom">
                                <input type="hidden" id="company_document_con_id" name="company_document_id" value="">
                                <div class="row">
                                    <div class="form-outer">
                                        <div class="col-sm-12 col-md-3">
                                            <label>Country<em class="red-star">*</em></label>
                                            <select class="form-control capital" name="country" readonly="">
                                                <option value="usa" selected>USA</option>
                                            </select>
                                            <span class="fcountryErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>MCC <em class="red-star">*</em></label>
                                            <select class="form-control" id="fcmcc" name="fmcc" readonly="">
                                            </select>
                                        </div>

                                        <div class="col-sm-12 col-md-3">
                                            <label>URL <em class="red-star">*</em></label>
                                            <input class="form-control capsOn" type="text" id="fcurl" name="furl">
                                            <span class="furlErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-12 col-md-3">
                                            <label>Business Type<em class="red-star">*</em></label>
                                            <input class="form-control capsOn" type="text" id="fcbusiness" name="fbusiness" value="company" readonly>
                                            <span class="fbusinessErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Account Number<em class="red-star">*</em></label>
                                            <input type="text" class="form-control hide_copy" id="fcaccount_number" name="faccount_number" value="">
                                            <span class="faccount_numberErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Routing Number<em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="fprouting_number" name="frouting_number" value="">
                                            <span class="frouting_numberErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>City <em class="red-star">*</em></label>
                                            <input type="text" class="form-control capital" id="fcccity" name="fccity" value="" >
                                            <span class="fcityErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Address 1 <em class="red-star">*</em></label>
                                            <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                            <input class="form-control capital" id="fcaddress" name="fcaddress" >
                                            <span class="faddressErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Address 2</label>
                                            <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                            <input class="form-control capital" id="fcaddress2" name="fcaddress2" >
                                            <span class="faddressErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Postal Code <em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="fcczipcode" name="fczipcode" value=""  maxlength="9">
                                            <span class="fzipcodeErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>State <em class="red-star">*</em></label>
                                            <input type="text" class="form-control capital" id="fccstate" name="fcstate" value="" readonly>
                                            <span class="fstateErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Name <em class="red-star">*</em></label>
                                            <input type="text" class="form-control capital" id="fcname" name="fcname" value="">
                                            <span class="fcnameErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Phone <em class="red-star">*</em></label>
                                            <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fcphone_number" name="fcphone_number" value="">
                                            <span class="fphone_numberErr error red-star"></span>
                                            <input type="hidden" value="" id="fcphone_number_prefix" name="phone_number_prefix">
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Tax_id <em class="red-star">*</em></label>
                                            <input   class="form-control" fixedLength="10" id="fctax_id" name="fctax_id" value="">
                                            <span class="fphone_numberErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Company Document <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">Upload Company Document here.</span></div></label>
                                        <input type='file' name="company_document" id="company_document_con" />
                                        <span class="company_document_idErr error red-star"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-hdr">
                                        <h3>Person</h3>
                                    </div>
                                    <div class="form-outer mt-15">
                                        <div class="col-sm-12 col-md-3">
                                            <label>First Name <em class="red-star">*</em></label>
                                            <input class="form-control capsOn fname capital" type="text" id="fpfirst_name" name="ffirst_name">
                                            <span class="ffirst_nameErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Last Name <em class="red-star">*</em></label>
                                            <input class="form-control capsOn lname capital" type="text" id="fplast_name" name="flast_name">
                                            <span class="flast_nameErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Email <em class="red-star">*</em></label>
                                            <input class="form-control femail" type="email" id="fpemail" name="femail" >
                                            <span class="femailErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Phone <em class="red-star">*</em></label>
                                            <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control fphone phone_number_format" fixedLength="10" id="fpphone_number" name="fphone_number" value="">
                                            <span class="fphone_numberErr error red-star"></span>
                                            <input type="hidden" value="" id="phone_number_prefix" name="phone_number_prefix" class="fphonepre">
                                        </div>
                                        <div class="date-outer-form">
                                            <label class="col-sm-12"> Date of birth    <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">DATE OF BIRTH</span></div></label>
                                        <div class="col-sm-12 col-md-3 fphone_num">
<!--                                            <label> Day <em class="red-star">*</em></label>-->
                                            <select class="form-control fpday"  id="fpday" name="fday" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fdayErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
<!--                                            <label>Month <em class="red-star">*</em></label>-->
                                            <select class="form-control fpmonth"  id="fpmonth" name="fmonth" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
<!--                                            <label>Year <em class="red-star">*</em></label>-->
                                            <select class="form-control fpyear"  id="fpyear" name="fyear" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fyearErr error red-star"></span>
                                        </div>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="fpssn" name="fssn" value="" maxlength="4">
                                            <span class="fssnErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-12 col-md-3">
                                            <label>Address 1 <em class="red-star">*</em></label>
                                            <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                            <input class="form-control" id="fpaddress" name="faddress" >
                                            <span class="faddressErr error red-star capital"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Address 2 <em class="red-star">*</em></label>
                                            <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                            <input class="form-control capital" id="fpaddress2" name="faddress2" >
                                            <span class="faddress2Err error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Postal Code <em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="fpppzipcode" name="fzipcode" value=""  maxlength="9">
                                            <span class="fzipcodeErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>State <em class="red-star">*</em></label>
                                            <input type="text" class="form-control capital" id="fpppstate" name="fstate" value="" readonly>
                                            <span class="fstateErr error red-star"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fpppcity" name="fcity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                </div>
                        </div>
                        <button type="submit"  id="savecompanyfinancialcom" class="blue-btn">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script>

        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        var date_format = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format'] ?>";
        var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>";

    </script>
    <script src="https://js.stripe.com/v3/"></script>

    <!-- Jquery Starts -->
    <script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
    <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL;?>js/jquery.responsivetabs.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/subscription.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/subscription/subscription.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/payments/plaidIntialisation.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/payments/stripeCheckout.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/payments/subPayment.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/payments/listing.js"></script>
    <!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/validation/changepassword.js" type="text/javascript"></script>-->

    <script>



        $(document).on('click','.cancel_btn',function(){

            bootbox.confirm("Do you want to cancel this action now ?", function (result) {

                if (result == true) {

                    window.location.href = '/User/AccountSetup';

                }

            });

        });



        $('#leftnav1').addClass('in');

        $('.default_settings').addClass('active');



        $(function() {

            $('.nav-tabs').responsiveTabs();

        });



        <!--- Main Nav Responsive -->

        $("#show").click(function(){

            $("#bs-example-navbar-collapse-2").show();

        });

        $("#close").click(function(){

            $("#bs-example-navbar-collapse-2").hide();

        });

        <!--- Main Nav Responsive -->





        $(document).ready(function(){



            // $.ajax({
            // url: '/payment-ajax',
            // type: 'POST',
            // data: {
            //
            // "action": 'oneTimePayment',
            // "class": 'payment'
            // },
            // success: function (response) {
            //
            //
            // }
            // });


            $(".slide-toggle").click(function(){

                $(".box").animate({

                    width: "toggle"

                });

            });

        });



        $(document).ready(function(){

            $(".slide-toggle2").click(function(){

                $(".box2").animate({

                    width: "toggle"

                });

            });
        });

    </script>

    <script>



        $(document).ready(function() {

            $('.summernote').summernote();

        });



        $('input[type=checkbox]').click(function () {

            $(this).parent().find('li input[type=checkbox]').prop('checked', $(this).is(':checked'));

            var sibs = false;

            $(this).closest('ul').children('li').each(function () {

                if($('input[type=checkbox]', this).is(':checked')) sibs=true;

            })

            $(this).parents('ul').prev().prop('checked', sibs);

        });



    </script>

    <script type="text/javascript">

        var specialKeys = new Array();

        specialKeys.push(8); //Backspace

        $(function () {

            $(".numeric").bind("keypress", function (e) {

                var keyCode = e.which ? e.which : e.keyCode

                var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);

                $(".error").css("display", ret ? "none" : "inline");

                return ret;

            });

            $(".numeric").bind("paste", function (e) {

                return false;

            });

            $(".numeric").bind("drop", function (e) {

                return false;

            });

        });

    </script>

    <script>

        $(document).on('click','.toggle-password',function() {

            $(this).toggleClass("fa-eye fa-eye-slash");

            var input = $($(this).attr("toggle"));

            if (input.attr("type") == "password") {

                input.attr("type", "text");

            } else {

                input.attr("type", "password");

            }

        });

        $(document).on('click','#billing-subscription .paymentDiv',function(){
            $(".basic-payment-detail").hide();
            $(".apx-adformbox-content").show();
            $(this).addClass("active");
            $(".basicDiv").removeClass("active");
        });

        $(document).on('click','#billing-subscription .basicDiv',function(){
            $("#fmcc,#fcmcc").val(6513);
            setTimeout(function(){
                var account_type= $("#business_type").val();
                var verification_status = $("#account_verification").val();
                if(account_type == "company"){
                    $(".basic-payment-detail").show();
                } else if(account_type == "individual"){
                    $(".basic-user-payment-detail").show();
                    $(".fmcc").show();
                    $(".furl").show();
                } else {
                    // $(".fmcc").hide();
                    // $(".furl").hide();
                    $('#billing-subscription').modal('hide');
                    $('#financial-infotype').modal('show');
                }

                if(verification_status == 'verified'){
                    $(".basic-payment-detail").hide();
                    $(".basic-user-payment-detail").hide();
                }

                $(".apx-adformbox-content").hide();
                $(".basicDiv").addClass("active");
                $(".paymentDiv").removeClass("active");
            }, 500);
        });





        function stripeTokenHandler(token) {

            var currency = 'USD';
            var Amount = $(".paidAmount").html();
            var days =   $(".days_remaining").val();
            var plan_id =   $(".plan_id").val();
            var term_plan = $(".term_plan").val();
            var user_type = 'PM';
            var service_fee =  $(".service_fee").html();
            var admin_fee =  $(".admin_fee").html();
            var service_fee =  $(".service_fee").html();
            var user_id = "<?php echo $_SESSION[SESSION_DOMAIN]['cuser_id']; ?>";
            $(".submit_payment_class").attr("disabled",true)
            // Insert the token ID into the form so it gets submitted to the server
            $.ajax({
                url:'/stripeCheckout',
                type: 'POST',
                data: {

                    "action": 'oneTimePayment',
                    "class": 'Stripe',
                    "token_id":token,
                    "currency":currency,
                    "amount": Amount,
                    "days":days,
                    "plan_id":plan_id,
                    "user_id":user_id,
                    "user_type":user_type,
                    "term_plan":term_plan
                }, beforeSend: function() {
                    // setting a timeout
                    $('#loadingmessage').show();
                },
                success: function (response) {
                    $('#loadingmessage').hide();
                    $(".submit_payment_class").attr("disabled",false)
                    var response = JSON.parse(response);
                    if(response.status=='success')
                    {
                        toastr.success('Payment has been done successfully');
                        $('#payNow').modal('toggle');
                        $(".payNow").attr('disabled',true);
                         $(".payNow").addClass("btn-secondary");
                        $('#SubscriptionHistory-table').trigger('reloadGrid');
                    }
                    else
                    {
                        toastr.error(response.message);
                    }


                }
            });




        }




        $(document).ready(function(){





            checkFirstPaymentStatus();

            var stripe = Stripe('pk_test_jF5tXlX0qLcCR1YAX9WB8FaL');

// Create an instance of Elements.
            var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    color: '#32325d',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

// Create an instance of the card Element.
            var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
            card.mount('#card-element');

// Handle real-time validation errors from the card Element.
            card.addEventListener('change', function(event) {
                var displayError = document.getElementById('card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                } else {
                    displayError.textContent = '';
                }
            });

// Handle form submission.
            var form = document.getElementById('payment-form');
            form.addEventListener('submit', function(event) {
                $(".submit_payment_class").attr("disabled",true)
                event.preventDefault();

                stripe.createToken(card).then(function(result) {
                    if (result.error) {
                        // Inform the user if there was an error.
                        var errorElement = document.getElementById('card-errors');
                        errorElement.textContent = result.error.message;
                    } else {
                        console.log(result);
                        var token_id = result.token.id;
                        stripeTokenHandler(token_id);
                    }
                });
            });

        });



        function checkFirstPaymentStatus()
        {
            var user_id = "<?php echo $_SESSION[SESSION_DOMAIN]['cuser_id']; ?>";
            $.ajax({
                url:'/stripeCheckout',
                type: 'POST',
                data: {
                    "action": 'checkFirstPaymentStatus',
                    "class": 'Stripe',
                    "user_id":user_id

                },
                success: function (response) {


                    if(response==1)
                    {
                        $(".payNow").attr("disabled", true);
                        $(".payNow").addClass("btn btn-secondary");
                        $(".payNow").removeClass("blue-btn")
                    }
                    else
                    {
                        $(".payNow").attr("disabled", false);
                        $(".payNow").removeClass("btn btn-secondary");
                        $(".payNow").addClass("blue-btn")
                    }


                }
            });

        }

    </script>

<?php

include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");

?>