<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>
    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
        ?>
        <link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.multiselect.css" rel="stylesheet" />
        <!-- MAIN Navigation Ends -->
        <section class="main-content">
            <div class="container-fluid">
                <div class="row flex">
                    <?php
                    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                    ?>
                    <div class="col-sm-8 col-md-10 main-content-rt">
                        <div class="content-rt">


                            <div class="bread-search-outer">
                                <div class="col-sm-8">


                                </div>
                                <div class="col-sm-4">
                                    <div class="easy-search">
                                        <input placeholder="Easy Search" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="content-data">
                                <!-- Main tabs -->
                                <!--Tabs Starts -->
                                <div class="main-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#preferences" aria-controls="home" role="tab" data-toggle="tab">Preferences</a></li>
                                        <li role="presentation" ><a href="#default-settings" aria-controls="profile" role="tab" data-toggle="tab">Master</a></li>


                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">

                                        <!-- First Tab Ends -->

                                        <!-- Second Tab Ends -->
                                        <div role="tabpanel" class="tab-pane active" id="preferences">
                                            <div class="form-outer">
                                                <div class="form-hdr">
                                                    <h3>Preferences</h3>
                                                </div>
                                                <div class="form-data">
                                                    <form id="preference_tab" method="post">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-xs-12">
                                                                        <ul class="check-outer-top">
                                                                            <li class="check-outer-frst">
                                                                                <input id="inventory_item" type="checkbox"> Do you want to create an Item Numbering/Coding System for your inventory items?
                                                                                <!-- AND SHOULD CHECK HERE -->
                                                                            </li>
                                                                        </ul>
                                                                    </div>

                                                                    <div class="col-xs-12">
                                                                        <ul class="check-outer-top">
                                                                            <li class="check-outer-frst">
                                                                                <input type="checkbox" name="show_last_name" id="add_suppliers">

                                                                                Allow users to add suppliers
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <ul class="check-outer-top">
                                                                            <li class="check-outer-frst">
                                                                                <input type="checkbox" name="birthday_notifications" id="upload_images">

                                                                                Allow users to upload photos.

                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <ul class="check-outer-top">
                                                                            <div class="btn-outer text-right">
                                                                                <input type="button" class="blue-btn" id="save_preferences" value="Save">
                                                                                <input type='button'  value="Clear" class="clear-btn clearPreferences" >
                                                                                <button type="button" class="grey-btn cancel_btn">Cancel</button>
                                                                            </div>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!--                                        <div class="accordion-form">-->
                                            <!--                                            <div class="accordion-outer">-->
                                            <!--                                                <div class="bs-example">-->
                                            <!--                                                    <div class="panel-group" id="accordion">-->
                                            <!--                                                        <div class="panel panel-default">-->
                                            <!--                                                            <div class="panel-heading">-->
                                            <!--                                                                <h4 class="panel-title">-->
                                            <!--                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Preferences</a>-->
                                            <!--                                                                </h4>-->
                                            <!--                                                            </div>-->
                                            <!--                                                            <form id="preference_tab" method="post">-->
                                            <!--                                                                <input type="hidden" id="user_id_preferences" name="user_id_preferences" value="--><?php //echo $_SESSION[SESSION_DOMAIN]['cuser_id']; ?><!--">-->
                                            <!--                                                                <div id="collapseOne" class="panel-collapse collapse  in">-->
                                            <!--                                                                    <div class="panel-body">-->
                                            <!--                                                                        <div class="row">-->
                                            <!--                                                                            <div class="form-outer">-->
                                            <!--                                                                                <div class="col-xs-12">-->
                                            <!--                                                                                    <ul class="check-outer-top">-->
                                            <!--                                                                                        <li class="check-outer-frst">-->
                                            <!--                                                                                            <input id="inventory_item" type="checkbox"> Do you want to create an Item Numbering/Coding System for your inventory items?-->
                                            <!--                                                                                             AND SHOULD CHECK HERE
                                                                                                                                   </li>-->
                                            <!--                                                                                    </ul>-->
                                            <!--                                                                                </div>-->
                                            <!---->
                                            <!--                                                                                <div class="col-xs-12">-->
                                            <!--                                                                                    <ul class="check-outer-top">-->
                                            <!--                                                                                        <li class="check-outer-frst">-->
                                            <!--                                                                                        <input type="checkbox" name="show_last_name" id="add_suppliers">-->
                                            <!---->
                                            <!--                                                                                            Allow users to add suppliers-->
                                            <!--                                                                                        </li>-->
                                            <!--                                                                                    </ul>-->
                                            <!--                                                                                </div>-->
                                            <!--                                                                                <div class="col-xs-12">-->
                                            <!--                                                                                    <ul class="check-outer-top">-->
                                            <!--                                                                                        <li class="check-outer-frst">-->
                                            <!--                                                                                        <input type="checkbox" name="birthday_notifications" id="upload_images">-->
                                            <!---->
                                            <!--                                                                                            Allow users to upload photos.-->
                                            <!---->
                                            <!--                                                                                        </li>-->
                                            <!--                                                                                    </ul>-->
                                            <!--                                                                                </div>-->
                                            <!--                                                                                <div class="col-xs-12">-->
                                            <!--                                                                                    <ul class="check-outer-top">-->
                                            <!--                                                                                    <div class="btn-outer">-->
                                            <!--                                                                                        <input type="button" class="blue-btn" id="save_preferences" value="Save">-->
                                            <!--                                                                                        <button type="button" class="grey-btn cancel_btn">Cancel</button>-->
                                            <!--                                                                                    </div>-->
                                            <!--                                                                                    </ul>-->
                                            <!--                                                                                </div>-->
                                            <!--                                                                            </div>-->
                                            <!--                                                                        </div>-->
                                            <!--                                                                    </div>-->
                                            <!--                                                                </div>-->
                                            <!--                                                            </form>-->
                                            <!--                                                        </div>-->
                                            <!--                                                    </div>-->
                                            <!--                                                </div>-->
                                            <!--                                            </div>-->
                                            <!--                                        </div>-->
                                        </div>
                                        <div role="tabpane1" class="tab-pane " id="default-settings">
                                            <section class="main-content">
                                                <div class="container-fluid">
                                                    <div class="row">


                                                        <div class="col-sm-12">
                                                            <div class="content-section">
                                                                <div class="accordion-form">
                                                                    <div class="accordion-outer">

                                                                        <div class="bs-example">
                                                                            <div class="panel-group" id="accordion">

                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-heading">
                                                                                        <h4 class="panel-title">
                                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                                                                <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Category/Sub Category
                                                                                            </a>
                                                                                        </h4>
                                                                                    </div>
                                                                                    <div id="collapseTwo" class="panel-collapse collapse">
                                                                                        <div class="panel-body">
                                                                                            <div class="row">
                                                                                                <div class="col-sm-12">
                                                                                                    <div class="btn-outer text-right">
                                                                                                        <button id="addUnitTypeButton" class="blue-btn">New Category</button>

                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="spacer"></div>

                                                                                                <div class="col-sm-12">
                                                                                                    <form name="add_subcategory_type" id="add_subcategory_type">
                                                                                                        <input type="hidden" value="" id="user_id_hidden10">
                                                                                                        <div class="form-outer2">
                                                                                                            <div class="row">
                                                                                                                <div class="col-xs-12 col-sm-3">

                                                                                                                    <label>Category Name <em class="red-star">*</em></label>
                                                                                                                    <input type="hidden" id="user_id_hidden10"  name="user_id_hidden10">

                                                                                                                    <input name="category" id="category" maxlength="100"   class="form-control disable_edit" type="text"/>
                                                                                                                    <span id="categoryErr" class="error"></span>
                                                                                                                </div>



                                                                                                                <div class="col-xs-12 col-sm-3 mulSubCat" >
                                                                                                                    <label>Sub Category
                                                                                                                        <a class="pop-add-icon"id="multiplsubcategory" name="multiplsubcategory" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>

                                                                                                                        <a class="pop-minus-icon" id="multiplsubcategory1" name="multiplsubcategory1" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                                                                                    </label>
                                                                                                                    <input name="sub_category" id="sub_category" maxlength="500" class="form-control">
                                                                                                                    <!--<span id="sub_categoryErr" class="error"></span>-->
                                                                                                                    <input type="hidden" id="sub_cat_id" name="sub_cat_id">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="btn-outer text-right">

                                                                                                                <input type="submit" value="Save" class="blue-btn" id="saveBtns"/>
                                                                                                                <input type='button'  value="Clear" class="clear-btn clearForm catfrm" formid="add_subcategory_type" >
                                                                                                                <input type='button'  value="Reset" class="clear-btn resetcategoryfrm" style="display: none;" >
                                                                                                                <button type="button" id="add_unit_cancel_btn" class="grey-btn">Cancel</button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="spacer"></div>
                                                                                                    </form>
                                                                                                    <div class="grid-outer">
                                                                                                        <div class="apx-table">
                                                                                                            <div class="table-responsive">
                                                                                                                <table id="category_table"> </table>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-heading">
                                                                                        <h4 class="panel-title">
                                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                                                                <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Manage Supplier
                                                                                            </a>
                                                                                        </h4>
                                                                                    </div>
                                                                                    <div id="collapseThree" class="panel-collapse collapse">
                                                                                        <div class="panel-body">
                                                                                            <div class="col-sm-12">
                                                                                                <div class="btn-outer text-right">
                                                                                                    <button id="addUnitTypeButton2" class="blue-btn">New Supplier</button>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="spacer"></div>
                                                                                            <div id="" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                                                <form name="add_subcategory_type" id="add_supplier_type1">

                                                                                                    <div class="row">
                                                                                                        <div class="form-outer form-outer2">
                                                                                                            <div class="col-xs-12 col-sm-3">

                                                                                                                <label>Category<em class="red-star">*</em></label>
                                                                                                                <input type="hidden" id="user_id_hiddens" name="user_id_hiddens">
                                                                                                                <select name="category" id="category" maxlength="100"   class="form-control disable_edit sub_cat_id2 " type="text" data_id="">
                                                                                                                    <option value="1">Select</option>
                                                                                                                </select>
                                                                                                                <span id="category_idErr" class="error"></span>
                                                                                                            </div>
                                                                                                            <div class="col-xs-12 col-sm-3 mulSubCat" >
                                                                                                                <label>Sub Category Name<em class="red-star">*</em>

                                                                                                                </label>
                                                                                                                <select name="sub_category_supp" id="sub_category_supp" maxlength="500" class="form-control abcd" >

                                                                                                                </select>
                                                                                                                <span id="sub_category_idErr" class="error"></span>
                                                                                                            </div>
                                                                                                            <div class="col-xs-12 col-sm-3 mulSubCat" >
                                                                                                                <label>Supplier

                                                                                                                </label>
                                                                                                                <input name="supplier" id="supplier" maxlength="500" class="form-control">
                                                                                                                <span id="supplierErr" class="error"></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-xs-12">
                                                                                                            <div class="btn-outer text-right">

                                                                                                                <input type="submit" value="Save" class="blue-btn" id="saveBtnSupplier"/>
                                                                                                                <input type='button'  value="Clear" class="clear-btn clearForm supplier_frm" formid="add_supplier_type1" >
                                                                                                                <input type='button'  value="Reset" class="clear-btn ResetSupplierForm" style="display: none;" >
                                                                                                                <button type="button" id="add_unit_cancel_btn1" class="grey-btn">Cancel</button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="spacer"></div>

                                                                                                </form>
                                                                                                <div class="grid-outer">
                                                                                                    <div class="apx-table">
                                                                                                        <div class="table-responsive">
                                                                                                            <table id="category_table1"> </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>

                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-heading">
                                                                                        <h4 class="panel-title">
                                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                                                                                <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Manage Sub Location
                                                                                            </a>
                                                                                        </h4>
                                                                                    </div>
                                                                                    <div id="collapseFour" class="panel-collapse collapse">


                                                                                        <div class="panel-body">
                                                                                            <div class="property-status">
                                                                                                <div class="row">
                                                                                                    <div class="col-sm-2 admin_type_status">
                                                                                                        <label>Status</label>
                                                                                                        <select class="fm-txt form-control"  id="jqgridOptions">
                                                                                                            <option value="All">All</option>
                                                                                                            <option value="1">Active</option>
                                                                                                            <option value="0">InActive</option>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                    <div class="col-sm-10">
                                                                                                        <div class="btn-outer text-right">
                                                                                                            <button id="addUnitTypeButton3" class="blue-btn">New Sub Location</button>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="spacer"></div>
                                                                                            <div class="panel-collapse collapse in" aria-expanded="true">
                                                                                                <form name="add_sublocation_type" id="add_sublocation_type" style="display: none;">
                                                                                                    <input type="hidden" id="user_id_hiddenLoction"  name="user_id_hiddenLoction">
                                                                                                    <div class="row">
                                                                                                        <div class="form-outer2">
                                                                                                            <div class="col-xs-12 col-sm-3">
                                                                                                                <label>Property <em class="red-star">*</em></label>
                                                                                                                <select name="subLoc_property" id="subLoc_property" maxlength="100" class="form-control">

                                                                                                                </select>
                                                                                                                <span id="unit_typeErr"></span>
                                                                                                            </div>
                                                                                                            <div class="col-xs-12 col-sm-3" >
                                                                                                                <label>Building<em class="red-star">*</em>

                                                                                                                </label>
                                                                                                                <select name="sub_building" id="sub_building" maxlength="500" class="form-control"></select>
                                                                                                            </div>
                                                                                                            <div class="col-xs-12 col-sm-3" >
                                                                                                                <label>SubLocation<em class="red-star">*</em></label>
                                                                                                                <input name="sub_location" id="sub_location" maxlength="500" class="form-control" placeholder="Add New Sub-Location">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-xs-12">
                                                                                                            <div class="btn-outer text-right">
                                                                                                                <input type="submit" value="Save" class="blue-btn" id="saveBtnSubLoc"/>
                                                                                                                <input type='button' value="Clear" class="clear-btn  clearForm subCateform" formid="add_sublocation_type">
                                                                                                                <input type='button'  value="Reset" class="clear-btn reset_subCate_frm" style="display: none;">
                                                                                                                <button type="button" id="add_unit_cancel_btn2" class="grey-btn">Cancel</button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="spacer"></div>
                                                                                                    </div>
                                                                                                </form>
                                                                                                <div class="grid-outer">
                                                                                                    <div class="apx-table">
                                                                                                        <div class="table-responsive">
                                                                                                            <table id="subLocationTable" class="table table-bordered"> </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-heading">
                                                                                        <h4 class="panel-title">
                                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                                                                                <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Manage Brand
                                                                                            </a>
                                                                                        </h4>
                                                                                    </div>
                                                                                    <div id="collapseFive" class="panel-collapse collapse">
                                                                                        <div class="panel-body">
                                                                                            <div class="col-sm-12">
                                                                                                <div class="btn-outer text-right">
                                                                                                    <button id="addUnitTypeButton4" class="blue-btn">New Brand</button>

                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="spacer"></div>



                                                                                            <div id="" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                                                <form name="add_subcategory_type4" id="add_subcategory_type4">

                                                                                                    <div class="row">
                                                                                                        <div class="form-outer2">
                                                                                                            <div class="col-xs-12 col-sm-3">

                                                                                                                <label>Category <em class="red-star">*</em> </label>
                                                                                                                <input type="hidden" id="user_id_hiddensss2" name="user_id_hiddensss2">
                                                                                                                <select name="category" id="category" maxlength="100"   class="form-control disable_edit subcatbrand" type="text"/>
                                                                                                                <option value="">Select</option>
                                                                                                                </select>
                                                                                                                <!-- <span class="category_idErr error" </span>-->


                                                                                                            </div>
                                                                                                            <div class="col-xs-12 col-sm-3 mulSubCat" >
                                                                                                                <label>Sub Category<em class="red-star">*</em></label>
                                                                                                                <select name="sub_category_brand" id="sub_category_brand" maxlength="500" class="form-control subcatbrn" multiple>
                                                                                                                </select>

                                                                                                                <span class="sub_category_idErr error" ></span>
                                                                                                            </div>
                                                                                                            <div class="col-xs-12 col-sm-3 mulSubCat" >
                                                                                                                <label>Brand
                                                                                                                    <em class="red-star">*</em>
                                                                                                                </label>
                                                                                                                <input name="main_brand" id="main_brand" maxlength="500" class="form-control brandID">
                                                                                                                <!--<span class="brandErr error"></span>-->
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-xs-12">
                                                                                                            <div class="btn-outer text-right">

                                                                                                                <input type="submit" value="Save" class="blue-btn" id="saveBtnsbrand"/>
                                                                                                                <input type='button'  value="Clear" class="clear-btn clearForm brand_frm" formid="add_subcategory_type4" >
                                                                                                                <input type='button'  value="Reset" class="clear-btn resetbrandform"  style="display:none;">
                                                                                                                <button type="button" id="add_brand_cancel_btn" class="grey-btn">Cancel</button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="spacer"></div>
                                                                                                </form>
                                                                                                <div class="grid-outer">
                                                                                                    <div class="apx-table">
                                                                                                        <div class="table-responsive">
                                                                                                            <table id="category_table3"> </table>.
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>

                                                                                        </div>


                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-heading">
                                                                                        <h4 class="panel-title">
                                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                                                                                                <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Manage Volume
                                                                                            </a>
                                                                                        </h4>
                                                                                    </div>
                                                                                    <div id="collapseSix" class="panel-collapse collapse">
                                                                                        <div class="panel-body">
                                                                                            <div class="col-sm-12">
                                                                                                <div class="btn-outer text-right">
                                                                                                    <button id="addUnitTypeButton6" class="blue-btn">New Volume</button>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="spacer"></div>


                                                                                            <div id="" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                                                <form name="add_subcategory_type" id="add_subcategory_type6">

                                                                                                    <div class="row">
                                                                                                        <div class="form-outer2">
                                                                                                            <div class="col-xs-12 col-sm-3">

                                                                                                                <label>Volume <em class="red-star">*</em></label>
                                                                                                                <input type="hidden" id="user_id_hidden2" name="user_id_hidden2">
                                                                                                                <input name="volume" id="volume" maxlength="100"   class="form-control disable_edit" type="text"/>
                                                                                                                <span id="volumeErr" class="error"></span>
                                                                                                            </div>
                                                                                                            <div class="col-xs-12">
                                                                                                                <div class="btn-outer text-right">
                                                                                                                    <input type="submit" value="Save" class="blue-btn" id="saveBtnsCat"/>
                                                                                                                    <input type='button'  value="Clear" class="clear-btn clearForm volform" formid="add_subcategory_type6" >
                                                                                                                    <input type='button'  value="Reset" class="clear-btn reset_vol_frm" style="display: none;">
                                                                                                                    <button type="button" id="add_unit_cancel_btn" class="grey-btn">Cancel</button>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>



                                                                                                    <div class="spacer"></div>
                                                                                                </form>
                                                                                                <div class="grid-outer">
                                                                                                    <div class="apx-table">
                                                                                                        <div class="table-responsive">
                                                                                                            <table id="manage_volume" class="table table-bordered"> </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-heading">
                                                                                        <h4 class="panel-title">
                                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                                                                                                <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Manage Reason For Quality Change
                                                                                            </a>
                                                                                        </h4>
                                                                                    </div>
                                                                                    <div id="collapseSeven" class="panel-collapse collapse">
                                                                                        <div class="panel-body">
                                                                                            <div class="col-sm-12">
                                                                                                <div class="btn-outer text-right">
                                                                                                    <button id="addUnitTypeButton7" class="blue-btn">New Reason</button>

                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="spacer"></div>


                                                                                            <div id="" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                                                <form name="add_subcategory_type" id="add_subcategory_type7">

                                                                                                    <div class="row">
                                                                                                        <div class="form-outer2">
                                                                                                            <div class="col-xs-12 col-sm-4">

                                                                                                                <label>Reason <em class="red-star">*</em></label>
                                                                                                                <input type="hidden" id="user_id_hidden1" name="user_id_hidden1">
                                                                                                                <input name="reason" id="reason" maxlength="100"   class="form-control disable_edit" type="text"/>
                                                                                                                <span id="reasonErr" class="error"></span>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                        <div class="col-xs-12">
                                                                                                            <div class="btn-outer text-right">

                                                                                                                <input type="submit" value="Save" class="blue-btn" id="saveBtnsMan"/>
                                                                                                                <input type='button'  value="Clear" class="clear-btn clearForm reasonform" formid="add_subcategory_type7"  >
                                                                                                                <input type='button'  value="Reset" class="clear-btn resetreason_frm" style="display: none;" >
                                                                                                                <button type="button" id="add_unit_cancel_btn" class="grey-btn">Cancel</button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="spacer"></div>
                                                                                                </form>
                                                                                                <div class="grid-outer">
                                                                                                    <div class="apx-table">
                                                                                                        <div class="table-responsive">
                                                                                                            <table id="manage_reason" class="table1 table-bordered1"> </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </section>
                                        </div>
                                        <!-- First Tab Ends -->
                                    </div>
                                </div>
        </section>
        <!-- Content Data Ends ---->
    </div>
    <!-- Wrapper Ends -->

    <script>
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    </script>
    <!-- Jquery Starts -->
    <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL;?>/js/jquery.responsivetabs.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL;?>/js/company/maintenance/inventoryPreferences.js"></script>


    <!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/validation/changepassword.js" type="text/javascript"></script>-->
    <script>
        $(document).on('click','.cancel_btn',function(){
            bootbox.confirm("Do you want to cancel this action now ?", function (result) {
                if (result == true) {
                    window.location.href = '/Maintenance/InventoryTracker';
                }
            });
        });

        $('#leftnav6').addClass('in');
        $('.default_settings').addClass('active');

        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });
        });

        $(document).ready(function(){
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
        });
    </script>
    <script>

        $(document).ready(function() {
            $('.summernote').summernote({
                addclass: {
                    debug: false,
                    classTags: [{title:"Button","value":"btn btn-success"},"jumbotron", "lead","img-rounded","img-circle", "img-responsive","btn", "btn btn-success","btn btn-danger","text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
                },
                width: '60%',
                //margin-left: '15px',
                toolbar: [
                    // [groupName, [list of button]]
                    ['img', ['picture']],
                    ['style', ['style', 'addclass', 'clear']],
                    ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
                    ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
                    ['extra', ['video', 'table', 'height']],
                    ['misc', ['undo', 'redo', 'codeview', 'help']]
                ]
            });
        });

        $('input[type=checkbox]').click(function () {
            $(this).parent().find('li input[type=checkbox]').prop('checked', $(this).is(':checked'));
            var sibs = false;
            $(this).closest('ul').children('li').each(function () {
                if($('input[type=checkbox]', this).is(':checked')) sibs=true;
            })
            $(this).parents('ul').prev().prop('checked', sibs);
        });

    </script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>


    <script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(function () {
            $(".numeric").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
                $(".error").css("display", ret ? "none" : "inline");
                return ret;
            });
            $(".numeric").bind("paste", function (e) {
                return false;
            });
            $(".numeric").bind("drop", function (e) {
                return false;
            });
        });
    </script>

<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>