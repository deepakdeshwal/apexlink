<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Property Setup >> <span>Property Sub-Type</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">

                            <div class="property-status">
                                <div class="row">
                                    <div class="col-md-2">
                                        <select id="jqGridStatus" data-module="ADMIN-SETTINGS" class="jqGridStatusClass fm-txt form-control">
                                            <option value="All">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>

                                    </div>
                                    <div class="col-md-10">
                                        <div class="btn-outer text-right">
                                            <button id="addPropertySubTypeButton" class="blue-btn">Add Property Sub-Type</button>
                                            <button id="importPropertySubTypeButton" class="blue-btn">Import Property Sub-Type</button>
                                            <button id="exportPropertySubTypeButton" class="blue-btn">Export Property Sub-Type</button>
                                            <button id="exportSampleExcel" class="blue-btn">Download Sample</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Main tabs -->
                            <div class="main-tabs">

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <!--Add property sub-type div starts here-->
                                                <div style="display: none;" id="add_property_subtype_div">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 id="headerDiv" class="panel-title">
                                                                Add Property Sub-Type
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <form name="add_property_type" id="add_property_subtype_form">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-6 col-md-4">
                                                                                <label>Property Sub-Type <em class="red-star">*</em></label>
                                                                                <input name="property_subtype" id="property_subtype" maxlength="100" placeholder="Eg: MF"   class="form-control" type="text"/>
                                                                                <span id="property_subtypeErr"></span>
                                                                            </div>
                                                                            <div class="col-sm-6 col-md-4">
                                                                                <label>Description</label>
                                                                                <textarea name="description" id="description" placeholder="Eg: Multi-Family" maxlength="500" class="form-control"></textarea>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-12">
                                                                                <label>
                                                                                    <div class="check-outer">
                                                                                        <input name="is_default" id="is_default" type="checkbox"/>
                                                                                        <label>Set as Default</label>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="btn-outer text-right">
                                                                                <input type="hidden" name="property_subtype_id" class="form-control" id="property_subtype_id"/>
                                                                                <input type="submit" value="Save" class="blue-btn" id="saveBtnId"/>
                                                                                <button type="button"  class="clear-btn clearFormReset">Clear</button>
                                                                                <button type="button" id="add_property_subtype_cancel_btn" class="grey-btn">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Add property sub-type div ends here-->

                                                <!--Import property sub-type div starts here-->
                                                <div  style="display: none;" id="import_property_subtype_div">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a >Import Property Sub-Type</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <div class="panel-body">
                                                                <form name="importPropertySubTypeForm" id="importPropertySubTypeFormId" >
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <input type="file" name="import_file" id="import_file"   accept=".csv, .xls, .xlsx" required/>
                                                                                <span class="error"></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="btn-outer">
                                                                                <button type="submit" class="blue-btn">Submit</button>
                                                                                <button type="button" id="import_property_subtype_cancel_btn" class="grey-btn">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Import property sub-type div ends here-->

                                                <!--List property sub-type div ends here-->
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table id="PropertySubType-table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--List property sub-type div ends here-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $('#leftnav2').addClass('in');
    $('.property_sub_type   ').addClass('active');
    var defaultFormData = '';
    $(document).on('click','.formreset',function () {
        resetEditForm("#add_property_subtype_form",[],true,defaultFormData,[]);
    });
    $(document).on('click','.clearFormReset',function () {
        resetFormClear("#add_property_subtype_form",[],'form',false);
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/propertySetup/property_sub_type.js"></script>
<!-- Jquery Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>