<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/sidebar.php");
                ?>
                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="welcome-text visible-xs">
                        <div class="welcome-text-inner">
                            <div class="col-xs-9">    Welcome:  Sonny Kesseben (ACL Properties), June 01, 2018 </div>
                            <div class="col-xs-3">
                                <a herf="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                                <a href="javascript:;"><i class="fa fa-calculator" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="content-rt">
                        <!--- Right Quick Links ---->
                        <div class="bread-search-outer">
                            <div class="col-xs-12 col-sm-8">
                                <div class="breadcrumb-outer">
                                    Property Setup >> <span>Property Groups</span>
                                </div>

                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">
                            <div class="content-section">
                                <form name="add_property_groups_form" id="add_property_groups_form">
                                    <div class="property-status">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <select data-module="ADMIN-SETTINGS" class="jqGridStatusClass fm-txt form-control" id="jqGridStatus">
                                                    <option value="All">All</option>
                                                    <option value="1">Active</option>
                                                    <option value="0">InActive</option>
                                                </select>
                                            </div>
                                            <div class=" col-md-10">
                                                <div class="btn-outer text-right">
                                                    <input type="reset" id="add_property_groups_button" value="Add Property Group" class="blue-btn"/>
                                                    <button class="blue-btn" id="import_property_groups_button">Import Property Group</button>
                                                    <a  class="blue-btn" id="export_property_groups_button">Export Property Group</a>
                                                    <a  class="blue-btn" id="export_sample_property_groups_button">DownloadSample</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Main tabs -->
                                    <div class="main-tabs">
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default" style="display: none;" id="addPropertyGroups">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" id="property_groups_span" data-parent="#accordion"  aria-expanded="true" class=""><span class=""></span> Add Group</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <input class="form-control property_groups" id="form_type" name="form_type" type="hidden" value="add"/>
                                                                            <input class="form-control property_groups_id" id="property_groups_id" name="property_groups_id" type="hidden"/>
                                                                            <div class="col-sm-6 col-md-4">
                                                                                <label>Group Name <em class="red-star">*</em></label>
                                                                                <input class="form-control" maxlength="100" class="group_name" name="group_name" placeholder="Eg: HRP" id="group_name" type="text"/>
                                                                            </div>
                                                                            <div class="col-sm-6 col-md-4">
                                                                                <label>Description</label>
                                                                                <textarea class="form-control" maxlength="500" id="description" placeholder="Eg: High Revenue Property" name="description"></textarea>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-12">
                                                                                <label>
                                                                                    <div class="check-outer">
                                                                                        <input type="checkbox" id="is_default" name="is_default"/>
                                                                                        <label>Set as Default</label>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="btn-outer text-right">
                                                                                <button type="submit" class="blue-btn" id="save_property_groups" >Save</button>
                                                                                <button type="button" class="clear-btn clearFormReset" >Clear</button>
                                                                                <button type="button" class="grey-btn" id="add_groups_cancel">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                </form>
                                <div class="panel panel-default" style="display: none;" id="ImportPropertyGroups">
                                    <form name="importPropertyGroupForm" id="importPropertyGroupForm"   enctype="multipart/form-data" >
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse"  data-parent="#accordion" href="" aria-expanded="true" class=""><span class=""></span> Import Property Group</a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <input type="file" name="import_file" id="import_file"   accept=".csv, .xls, .xlsx" required/>
                                                            <span class="error"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="btn-outer">
                                                            <button type="submit" class="blue-btn" id="save_import_groups_file">Submit</button>
                                                            <button type="button" class="grey-btn" id="import_groups_cancel">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="panel panel-default">
                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                        <div class="panel-body pad-none">
                                            <div class="grid-outer">
                                                <div class="apx-table">
                                                    <div class="table-responsive">
                                                        <table id="propertygroups-table" class="table table-bordered"></table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Wrapper Ends -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $('#leftnav2').addClass('in');
    $('.property_group').addClass('active');
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->

    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
    var defaultFormData = '';
    $(document).on('click','.formreset',function () {
        resetEditForm("#add_property_groups_form",[],true,defaultFormData,[]);
    });
    $(document).on('click','.clearFormReset',function () {
        resetFormClear("#add_property_groups_form",[],'form',false);
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/propertySetup/propertygroups.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/propertygroups/propertygroups.js"></script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
