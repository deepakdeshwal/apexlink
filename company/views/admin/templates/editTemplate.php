<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<body>

<div id="wrapper">
    <?php
    include_once(COMPANY_DIRECTORY_URL. "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php"); ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Templates >> <span>Email Template</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">
                            <!--List & Add/Edit Mode Starts-->
                            <div id="editTemplate_mode">
                                <input type="hidden" name="editTempale_id" id="editTempale_id" value="<?php echo $_GET['id']; ?>">
                                <!-- Main tabs -->
                                <div class="main-tabs">
                                    <div class="form-outer form-outer2" style="" id="">
                                        <div class="form-hdr">
                                            <h3 id="headerDiv">Change Template</h3>
                                        </div>
                                        <div class="form-data">

                                            <form name="editTemplateEmail" id="editTemplateEmail">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-4">
                                                        <label class="">Letter Name:</label>
                                                        <input name="tempLetterName" id="tempLetterName" placeholder="" maxlength="100"   class="form-control" type="text" disabled="disabled"/>
                                                        <span id="severityErr" class="error"></span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4">
                                                        <label>Tags:</label>
                                                        <select id="tagEmailTemplate" class="fm-txt form-control">
                                                            <option value="All">Select Type</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4">
                                                        <label>&nbsp;</label>
                                                        <input type="button" value="Restore Original" class="blue-btn" id="resetBtn" />
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="editor-email-signature">
                                                            <textarea class="form-control summernote" name="email_signature_default"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-outer text-right">
                                                    <input type="hidden" name="unit_type_id" class="form-control" id="unit_type_id" />
                                                    <input type="submit" value="Update" class="blue-btn" id="ytytyty"/>
                                                    <input type="button" value="Reset" class="clear-btn ResetTemplate" />
                                                    <button type="button" id="update_email_cancel_btn" class="grey-btn">Cancel</button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Content Data Ends ---->
                </div>
            </div>
        </div>


    </section>
</div>
<!-- Wrapper Ends -->


<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>

<!-- Footer Ends -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";

    $('#leftnav9').addClass('in');
    $('.template').addClass('active');


    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });

        $('.summernote').summernote({
            addclass: {
                debug: false,
                classTags: [{title:"Button","value":"btn btn-success"},"jumbotron", "lead","img-rounded","img-circle", "img-responsive","btn", "btn btn-success","btn btn-danger","text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
            },
            width: '80%',
            height: '300px',
            //margin-left: '15px',
            toolbar: [
                // [groupName, [list of button]]
                ['img', ['picture']],
                ['style', ['style', 'addclass', 'clear']],
                ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
                ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
                ['extra', ['video', 'table', 'height']],
                ['misc', ['undo', 'redo', 'codeview', 'help']]
            ]
        });
    });

</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/adminUsers/Template/template_email.js"></script>
</body>
<style>
    .row.custom_field_class input {
        width: 258px;
    }
</style>
</html>