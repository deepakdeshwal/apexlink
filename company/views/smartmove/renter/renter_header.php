<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/images/favicon.ico" alt="apex-icon" type="image/ico" sizes="16x16">

    <title>ApexLink</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/main.css"/>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/smartmove.css"/>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.validate.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.mask.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/toastr.min.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script type="text/javascript">
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": true,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
    </script>
<?php
$path = dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/constants.php';
$dbCon = dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/DBConnection.php';
include_once ($path);
include_once ($dbCon);
?>
