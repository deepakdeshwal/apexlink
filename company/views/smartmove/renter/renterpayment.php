<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/smartmove/renter/renter_header.php");
$id = 0;
if (isset($_GET['token']) && $_GET['token'] != ""){
    $data = base64_decode($_GET['token']);
    $id   = $data;
}
/*$id = $_GET['id'];
$name = base64_decode($_GET['CPN']);
$ttoken = $_GET['ttoken'];*/
$name = "";
?>
<!--<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Signup</title>
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css" />
</head>-->
<body >
    <div id="approval-newproperty" class="modal fade in" role="dialog" style="display:block; overflow-y: scroll;">
        <div class="rental-template-outer">
            <div class="row">
                <div class="col-md-6">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <table style="width: 100%; background: #0095c0; margin-top: 30px;">
                                    <tbody>
                                        <tr>
                                            <td style="width: 72%; padding: 10px 0 0 20px;">
                                                <img src="<?php echo COMPANY_SUBDOMAIN_URL;?>/images/logo_white.png" alt="footerlogo" border="3" height="45px" width="270px">
                                            </td>
                                            <td style="width: 28%; font-size: 14px; font-style: normal; color: #ffffff; font-weight: 400; padding: 20px 0 0 0;">
                                                6430  S. Fiddler's Green Circle
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 72%;"></td>
                                            <td style="width: 28%; font-size: 14px; font-style: normal; color: #ffffff; font-weight: 400;">
                                                Suite #500 Greenwood Village
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 72%; font-size: 12px; font-style: normal; color: #ffffff; font-weight: 300; padding: 10px 0 25px 20px;">
                                                @2020 TRANSUNION RENTAL SCREENING SOLUTIONS. INC. ALL RIGHT RESERVED
                                            </td>
                                            <td style="width: 28%; font-size: 14px; font-style: normal; color: #ffffff; font-weight: 400; padding: 10px 0 25px 0px;">
                                                CO 80111
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 0 20px; display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px; margin-top: 30px">
                                <b style="font-weight: 800">To generate your SmartMove background screening report, please complete the following:
                                </b>
                            </td>
                        </tr>
                        <tr style="padding: 0 20px; display: inline-block; margin-bottom: 30px;">
                            <td style="display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px; margin-top: 30px">
                                <ul>
                                    <li>Confirm the <b>Total</b> charge, shown, below & once confirmed.</li>
                                    <li>Enter your Credit/Debit card information to submit payment.</li>
                                    <li>Upon successful, payment, your SmartMove report will be generated & sent to<em style="color:red">(<?php echo $name; ?>) for review.</em>  </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width: 100%">
                                    <thead>
                                        <th style="width: 100%; display: inline-block;">
                                            <td style="background:#fff559; display: flex; height: 40px; align-items: center; padding-left: 15px; font-weight: 600;">Payment Summary</td>
                                        </th>
                                    </thead>
                                    <tbody>
                                        <tr class="table-striped" style="width: 100%; display: flex; height: 40px; align-items: center; padding-left: 15px; font-weight: 600;">
                                            <td style="width: 80%">Criminal Report and Credit Score & Recommendation</td>
                                            <td style="width: 20%">$0.00</td>
                                        </tr>
                                        <tr style="width: 100%; display: flex; height: 40px; align-items: center; padding-left: 15px; font-weight: 600;">
                                            <td style="width: 80%">View Criminal Report</td>
                                            <td style="width: 20%">0.00</td>
                                        </tr>
                                        <tr class="table-striped" style="width: 100%; display: flex; height: 40px; align-items: center; padding-left: 15px; font-weight: 600;">
                                            <td style="width: 80%">Credit Recommendation</td>
                                            <td style="width: 20%">0.00</td>
                                        </tr>
                                        <tr style="width: 100%; display: flex; height: 40px; align-items: center; padding-left: 15px; font-weight: 600;">
                                            <td style="width: 40%">Full Credit Report</td>
                                            <td style="width: 40%">$35 x applicant(s)</td>
                                            <td style="width: 20%">35.00</td>
                                        </tr>
                                        <tr class="table-striped" style="width: 100%; display: flex; height: 40px; align-items: center; padding-left: 15px; font-weight: 600;">
                                            <td style="width: 80%">Coupon Applied</td>
                                            <td style="width: 20%">$0.00</td>
                                        </tr>
                                        <tr style="background: #555555; width: 100%; display: flex; height: 40px; align-items: center; padding-left: 15px; font-weight: 600;">
                                            <td style="width: 60%"></td>
                                            <td style="width: 20%; color: #ffffff;">Total</td>
                                            <td style="width: 20%; color: #ffffff;">$35.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>

                    </table>
                </div>
                <div class="col-md-6">
                    <p>&nbsp;</p>
                    <form name="renterPayment_frm" id="renterPayment_frm" method="POST">

                    </form>
                    <form action="/charge" method="post" id="payment-form">
                        <div class="row payment-form-label">
                            <div class="col-sm-12">
                                <label>Amount to be paid -    <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$'; ?> <span class="currecy_padding"><?php echo $default_symbol ?></span> <span class='paidAmount currecy_padding'>35.00</span> </label>
                            </div>
                        </div>
                        <div class="row payment-form-label">
                            <div class="col-sm-12">
                               <p  class="payment-method-text">Payment Method</p>
                           </div>
                       </div>


                       <div class="form-row">
                        <label for="card-element" class="card-payment-text">
                            Credit / Debit card
                        </label>
                        <div id="card-element">
                            <!-- A Stripe Element will be inserted here. -->
                        </div>

                        <!-- Used to display form errors. -->
                        <div id="card-errors" role="alert"></div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                          <img src="<?php echo COMPANY_SUBDOMAIN_URL;?><?php echo !empty($_SESSION[SESSION_DOMAIN]['company_logo'])? $_SESSION[SESSION_DOMAIN]['company_logo'] :  '/images/logo.png' ?>"  class="payment-method-logo">
                      </div>
                      <div class="col-sm-6">
                         <button class="blue-btn submit_payment_class pull-right payment-submit-button">Submit Payment</button>
                     </div>
                 </div>
             </form>
         </div>
     </div>
 </div>
</div>
    <input name="tenant_id_input" value="<?php echo $id; ?>" type="hidden">
</body>

<script type="text/javascript">
$(document).ready(function(){
    /*stripePaymentVariables();
    function stripePaymentVariables(){
        var stripe = Stripe('pk_test_jF5tXlX0qLcCR1YAX9WB8FaL');
        // Create an instance of Elements.
        var elements = stripe.elements();
        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {style: style});

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
            $(".submit_payment_class").attr("disabled",true);
            event.preventDefault();
            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    var token_id = result.token.id;
                    stripeTokenHandler(token_id);
                }
           });
        });
    }

    function stripeTokenHandler(token) {
        var currency = 'USD';
        var Amount = '35';
        var user_type = 'TENANT';
        var user_id = "<?php //echo $ttoken; ?>";
        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
                "action": 'smartMovePayment',
                "class": 'Stripe',
                "token_id":token,
                "currency":currency,
                "amount": Amount,
                "user_id":user_id,
                "user_type":user_type
            }, beforeSend: function() {
                $('#loadingmessage').show();
            },
            success: function (response) {
                $('#loadingmessage').hide();
                var response = JSON.parse(response);
                if(response.status=='success'){
                    toastr.success('Payment has been done successfully');

                    var serverTime = getServerTime();
                    var securityHeaderToken = getSecurityHeaders(serverTime);
                    //createRenter( renter, serverTime, securityHeaderToken);

                }else{
                    toastr.error(response.message);
                }
            }
        });
    }*/
});
</script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/smartmove/renter/renter_footer.php");
?>
