<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/smartmove/renter/renter_header.php");
$id = 0;
if (isset($_GET['token']) && $_GET['token'] != "") {
    $id = $_GET['token'];
}
$tagId = 0;
if (isset($_GET['tagId']) && $_GET['tagId'] != "") {
    $tagId = $_GET['tagId'];
}
?>
<div class="smart-move-modal">
    <header class="smart-move-header smart-move-header-black smart-move-header-pt-0">
        <div class="header-top">
            <div class="container">
                <div class="row d-flex align-item-center">
                    <div class="col-sm-6">
                        <a href="index.html" class="logo"><img
                                    src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/images/white-logo.png" alt="logo"
                                    class="img-responsive"></a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="mutistep-form-top">
        <h2 class="sm-main-heading black">Sign up for SmartMove
            <!--<a href="#" class="product-edit-icon"><i class="fa fa-edit"></i></a><a href="#" class="product-edit-icon"><i class="fa fa-envelope"></i></a>--></h2>
        <div class="mutistep-form-top-inner">
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                        <p>Create Account</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                        <p>Personal Information</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                        <p>Confirmation</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-4" type="button" class="btn btn-circle">4</a>
                        <p>Identify Verification</p>
                    </div>
                </div>
            </div>
        </div>
        <form role="form" id="renter_account_form">
            <input type="hidden" name="tenantid" id="tenantid" value="<?php echo $id; ?>"/>
            <input type="hidden" name="rowid" value="<?php echo $tagId; ?>"/>
            <div class=" setup-content" id="step-1">
                <div class="col-xs-12">
                    <div class="col-md-12">
                        <h3> Step 1 - Create Account</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Create SmartMove Password<span
                                                class="required">*</span></label>
                                    <input type="password" class="form-control sm_password pass" name="sm_password"
                                           placeholder="Enter Password"/>
                                    <p class="instruction">8 to 15 character long including one capital letter, one
                                        lower case letter, and one number or special character</p>
                                    <span style="color:red;" class="pass_error"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Confirm Password<span class="required">*</span></label>
                                    <input type="password" class="form-control csm_password pass" name="csm_password"
                                           placeholder="Confirm Password"/>
                                    <span style="color:red;" class="cpass_error"></span>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Security Question 1<span
                                                class="required">*</span></label>
                                    <select name="security_question_1" class="form-control">
                                        <option value="">Please Select</option>
                                        <option value="1">What was the make and model of your first car?</option>
                                        <option value="2">What is your mother’s middle name?</option>
                                        <option value="3">What is your father’s middle name?</option>
                                        <option value="4">What city were you born in?</option>
                                        <option value="5">What is your grandmother's first name (on your mother's
                                            side)?
                                        </option>
                                        <option value="6">What is your grandfather's first name (on your father's
                                            side)?
                                        </option>
                                        <option value="7">What is your spouse’s middle name?</option>
                                        <option value="8">What is your favorite teacher’s name?</option>
                                        <option value="9">What is your favorite state/country?</option>
                                        <option value="10">What is your favorite vacation spot?</option>
                                        <option value="11">What was your high school mascot?</option>
                                        <option value="12">What was your first grade teacher’s last name?</option>
                                        <option value="13">What is your anniversary (mm/dd/yyyy)?</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Security Question 2<span
                                                class="required">*</span></label>
                                    <select name="security_question_2" class="form-control">
                                        <option value="">Please Select</option>
                                        <option value="1">What was the make and model of your first car?</option>
                                        <option value="2">What is your mother’s middle name?</option>
                                        <option value="3">What is your father’s middle name?</option>
                                        <option value="4">What city were you born in?</option>
                                        <option value="5">What is your grandmother's first name (on your mother's
                                            side)?
                                        </option>
                                        <option value="6">What is your grandfather's first name (on your father's
                                            side)?
                                        </option>
                                        <option value="7">What is your spouse’s middle name?</option>
                                        <option value="8">What is your favorite teacher’s name?</option>
                                        <option value="9">What is your favorite state/country?</option>
                                        <option value="10">What is your favorite vacation spot?</option>
                                        <option value="11">What was your high school mascot?</option>
                                        <option value="12">What was your first grade teacher’s last name?</option>
                                        <option value="13">What is your anniversary (mm/dd/yyyy)?</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Security Question 3<span
                                                class="required">*</span></label>
                                    <select name="security_question_3" class="form-control">
                                        <option value="">Please Select</option>
                                        <option value="1">What was the make and model of your first car?</option>
                                        <option value="2">What is your mother’s middle name?</option>
                                        <option value="3">What is your father’s middle name?</option>
                                        <option value="4">What city were you born in?</option>
                                        <option value="5">What is your grandmother's first name (on your mother's
                                            side)?
                                        </option>
                                        <option value="6">What is your grandfather's first name (on your father's
                                            side)?
                                        </option>
                                        <option value="7">What is your spouse’s middle name?</option>
                                        <option value="8">What is your favorite teacher’s name?</option>
                                        <option value="9">What is your favorite state/country?</option>
                                        <option value="10">What is your favorite vacation spot?</option>
                                        <option value="11">What was your high school mascot?</option>
                                        <option value="12">What was your first grade teacher’s last name?</option>
                                        <option value="13">What is your anniversary (mm/dd/yyyy)?</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Answer</label>
                                    <input maxlength="100" type="text" class="form-control" name="secQstnAns1"/>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Answer</label>
                                    <input maxlength="100" type="text" class="form-control" name="secQstnAns2"/>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Answer</label>
                                    <input maxlength="100" type="text" class="form-control" name="secQstnAns3"/>
                                </div>
                            </div>
                        </div>
                        <div class="setupbutton">
                            <a class="btn btn-primary nextBtn getRenterDetail btn-lg pull-right">Next</a>
                        </div>
        </form>
    </div>
</div>
</div>

<form role="form" id="renter_personalInfo_form">
    <input type="hidden" name="rowid" value="<?php echo $tagId; ?>"/>
    <div class=" setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 2 - Personal Information</h3>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">First Name<span class="required">*</span></label>
                            <input type="text" class="form-control first_name" name="first_name"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Middle Name</label>
                            <input type="text" class="form-control middle_name" name="middle_name"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Last Name<span class="required">*</span></label>
                            <input type="text" class="form-control last_name" name="last_name"/>
                        </div>
                    </div>
                </div>
                 <div class="row">
                     <div class="col-sm-4">
                         <div class="form-group">
                             <label class="control-label">Date of Birth<span class="required">*</span></label>
                             <input type="date" class="form-control dob" name="dob"/>
                         </div>
                     </div>
                     <div class="col-sm-4">
                         <div class="form-group">
                             <label class="control-label">SSN:<span class="required">*</span></label>
                             <input type="text" class="form-control checkssn ssn_sin_id" name="ssn_sin_id"
                                    id="ssn_sin_id"/>
                             <span style="color:red;" class="ssn"></span>
                         </div>
                     </div>
                     <div class="col-sm-4">
                         <div class="form-group">
                             <label class="control-label">Confirm SSN<span class="required">*</span></label>
                             <input type="text" class="form-control checkssn cssn_sin_id" name="cssn_sin_id"/>
                             <span style="color:red;" class="cssn"></span>
                         </div>
                     </div>
                 </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Primary Phone:<span class="required">*</span></label>
                            <input type="tel" class="form-control primary_phone phone_format" name="primary_phone"/>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Alternate Phone:</label>
                            <input type="tel" class="form-control phone_number phone_format" name="phone_number"/>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Alternate Phone:</label>
                            <input type="tel" class="form-control phone_number1 phone_format" name="phone_number1"/>
                        </div>
                    </div>
                </div>


                    <!-- <div class="col-sm-1">
                            <div class="form-group">
                                    <label class="control-label">EXT<span class="required">*</span></label>
                                    <input type="tel"  maxlength="15" class="form-control ext number_only_for_extension"  name="EXT"  />
                            </div>
                    </div> -->
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label class="control-label">STREET ADDRESS<span class="required">*</span></label>
                                <input type="text" class="form-control address1" name="address1"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Address Line2:</label>
                                <input type="text" class="form-control address2" name="address2"/>
                            </div>
                        </div>
                    </div>

                   <div class="row">
                       <div class="col-sm-4">
                           <div class="form-group">
                               <label class="control-label">City<span class="required">*</span></label>
                               <input type="text" class="form-control city" name="city"/>
                           </div>
                       </div>
                       <div class="col-sm-4">
                           <div class="form-group">
                               <div class="dropdown">
                                   <label class="control-label">State<span class="required">*</span></label>
                                   <input type="text" class="form-control state" name="state"/>
                               </div>
                           </div>
                       </div>
                       <div class="col-sm-4">
                           <div class="form-group">
                               <label class="control-label">Zip Code:<span class="required">*</span></label>
                               <input type="text" class="form-control zipcode" name="zipcode"/>
                           </div>
                       </div>
                   </div>

                   <div class="row">
                       <div class="col-sm-12">
                           <h3>Income Information</h3>
                       </div>
                   </div>


                    <div class="row">
                        <div class="col-sm-6 income-information">
                            <div class="row">
                                 <div class="col-md-8">
                                     <div class="form-group">
                                         <label class="control-label">Income ($)<span class="required">*</span></label>
                                         <input type="text" class="form-control Income  other-income-control" name="income">
                                     </div>
                                 </div>
                                <div class="col-md-4">
                                    <div class="income-information-period">
                                        <label class="radio-inline">
                                            <input type="radio" name="incomePeriod" value="1" checked>Per Month
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="incomePeriod" value="2">Per Year
                                        </label>
                                    </div>
                               </div>
                            </div>

                        </div>

                        <div class="col-sm-6 income-information">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="control-label">Other Income ($)</label>
                                        <input type="text" class="form-control otherincome other-income-control" name="otherincome"/>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="income-information-period">
                                        <label class="radio-inline">
                                            <input type="radio" name="otherIncomePeriod" value="1" checked>Per Month
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="otherIncomePeriod" value="2">Per Year
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>

                        </div>
                    </div>

                    <div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">Assets ($)</label>
                                <input type="number" class="form-control assets" name="assets"/>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="dropdown">
                                    <label class="control-label">Employment Status<span class="required">*</span></label>
                                    <select name="empmntstatus" class="form-control empmntstatus">
                                        <option value="">Select</option>
                                        <option value="Employed">Employed</option>
                                        <option value="Not Employed">Not Employed</option>
                                        <option value="Self Employed">Self Employed</option>
                                        <option value="Student">Student</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>



                <div class="setupbutton setupbutton-step2">
                    <button class="btn btn-primary  getIncomeInfo btn-lg pull-right" type="button">Next</button>
                    <button class="btn btn-primary nextBtn showprevious btn-lg pull-right" type="button">Previous
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
<form role="form" id="renter_confrirmation_form">
    <input type="hidden" name="tenantid" id="tenantid" value="<?php echo $id; ?>"/>
    <input type="hidden" name="rowid" id="rowid" value="<?php echo $tagId; ?>"/>
    <div class="setup-content setup-content3" id="step-3">
        <h3>Step 3 - Confirm Account Details</h3>
        <h5>Please confirm the details of your account</h5>
        <div class="setup-content3-item">
            <p>Personal Information</p>
            <h4>First Name:<span class="first_name"></span></h4>
            <h4>Middle Name:<span class="middle_name"></span></h4>
            <h4>Last Name:<span class="last_name"></span></h4>
            <h4>SSN:<span class="ssn_sin_id"></span></h4>
            <h4>Alternate Phone<span class="phone_number"></span></h4>
            <h4>Alternate Phone<span class="phone_number1"></span></h4>
            <!-- <h4>EXT<span class="Ext"></span></h4> -->
            <h4>Street Address:<span class="address1"></span></h4>
            <h4>Address Line2: <span class="address2"></span></h4>
            <h4>City<span class="city"></span></h4>
            <h4>State<span class="state"></span></h4>
            <h4>Zip Code<span class="zipcode"></span></h4>
        </div>
        <div class="setup-content3-item">
            <p>Income Information</p>
            <h4>Income ($):<span class="incomeInfo"></span></h4>
            <h4>Other Income ($):<span class="otherincome"></span></h4>
            <h4>Assets ($):<span class="assetsInfo"></span></h4>
            <h4>Employment Status:<span class="employed_status">Employed</span></h4>
        </div>
        <div class="setupbutton setupbutton-step2">
            <!-- <button type="button" class="btn btn-primary nextBtn saveRenterInfo btn-lg pull-right">Submit</button> -->
        </div>
        <div class="setupbutton setupbutton-step2">
            <button type="button" class="btn btn-primary renter_Confrm btn-lg pull-right" data-toggle="modal"
                    data-target="#step-4">NEXT
            </button>
        </div>
    </div>
</form>
<form role="form" id="renter_verifyIdentity_form">
    <div class="setup-content setup-content4" id="step-4">
        <h3>Step 4 - Verify Identity</h3>
        <div class="row questions-block">
            <div class="col-sm-4">
                <div class="setup-content3-item">
                    <p><span>1.</span> In what country do you live?</p>
                    <label class="radio">
                        <input type="radio" name="country" value="Menard" checked><em>Menard</em>
                    </label>
                    <label class="radio">
                        <input type="radio" name="country" value="Cook"><em>Cook</em>
                    </label>
                    <label class="radio">
                        <input type="radio" name="country" value="Calhoun"><em>Calhoun</em>
                    </label>
                    <label class="radio">
                        <input type="radio" name="country" value="White"><em>White</em>
                    </label>
                    <label class="radio">
                        <input type="radio" name="country" value="none"><em>None of the above</em>
                    </label>
                </div>

                <div class="setup-content3-item">
                    <p><span>2.</span> What Zip Code has ever been a part of your address</p>
                    <label class="radio">
                        <input type="radio" name="zipcode" value="60524" checked><em>60524</em>
                    </label>
                    <label class="radio">
                        <input type="radio" name="zipcode" value="60525"><em>60525</em>
                    </label>
                    <label class="radio">
                        <input type="radio" name="zipcode" value="50655"><em>50655</em>
                    </label>
                    <label class="radio">
                        <input type="radio" name="zipcode" value="55555"><em>55555</em>
                    </label>
                    <label class="radio">
                        <input type="radio" name="zipcode" value="none"><em>None of the above</em>
                    </label>
                </div>

                <div class="setup-content3-item">
                    <p><span>3.</span> What was the original amount of your most recent mortgage?</p>
                    <label class="radio">
                        <input type="radio" name="amount" value="179200" checked><em>$179200</em>
                    </label>
                    <label class="radio">
                        <input type="radio" name="amount" value="128000"><em>$128000</em>
                    </label>
                    <label class="radio">
                        <input type="radio" name="amount" value="102400"><em>$102400</em>
                    </label>
                    <label class="radio">
                        <input type="radio" name="amount" value="76000"><em>$76000</em>
                    </label>
                    <label class="radio">
                        <input type="radio" name="amount" value="none"><em>None of the above</em>
                    </label>
                </div>
            </div>
        </div>
        <div class="setupbutton setupbutton-step2">
            <input type="hidden" name="tenantid" class="tenantid" value="<?php echo $token; ?>"/>
            <button type="button" class="btn btn-primary nextBtn saveRenterInfo btn-lg pull-right">Submit</button>
        </div>
    </div>

</form>
</div>
</div>
</div>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/smartmove/renter/renter_footer.php");
?>
