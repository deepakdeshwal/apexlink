<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    include_once ('../../constants.php');
    //  $url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};

if (isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] != '')) {
    $url = BASE_URL . "Dashboard/Dashboard";
    header('Location: ' . $url);
} else {
    // write your code here
}
include_once(SUPERADMIN_DIRECTORY_URL. "/views/layouts/admin_header.php");
?>
<div class="apxpg-login">
<div id="wrapper">
    <div class="login-bg">
        <div class="login-outer">
            <div class="login-logo"><img src="<?php echo COMPANY_SUBDOMAIN_URL;?>/images/logo-login.png"/></div>
            <div class="login-inner">
                <form name="2fa_form" id="2fa_form_id">
                    <h2>Enter 2 Factor Authorization OTP</h2>
                    <div class="login-data">
                        <label>
                            <input class="form-control" type="password" name="two_fa_otp" placeholder="Enter the code you received here." id="two_fa_otp" type="text"/>
                        </label>
                        <div class="btn-outer">
                            <input type="submit" name="send_mail" value="Submit" class="blue-btn" id="send_mail"/>
                            <input  type="button" value="Cancel" class="grey-btn" id="OTP_cancel"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL;?>/js/validation/users/users.js"></script>
<script type="text/javascript">
    $(document).on("click", "#OTP_cancel", function () {
        window.location.href = '/';
    });
    $('#2fa_form_id').validate({
        rules:{
            two_fa_otp: {
                required: true
            }
        },
        submitHandler: function () {
            var two_fa_otp = $('#two_fa_otp').val();
            $.ajax({
                type: 'post',
                url: 'company-user-ajax',
                data: {
                    class: "UserAjax",
                    action: "checkTwoFaOTP",
                    two_fa_otp: two_fa_otp
                },
                success: function (response) {
                    var response = $.parseJSON(response);

                    console.log('checkTwoFaOTP>>>', response.data);
                    if (response.status == "success")
                    {
                        localStorage.setItem("login_user_id", response.data.id);
                        localStorage.setItem("active_inactive_status", response.active_inactive_status.status);
                        if(response.data.last_user_url){

                            window.location.href = response.data.last_user_url ;
                        } else {
                            window.location.href = "/Dashboard/Dashboard" ;
                        }

                    } else
                    {
                        toastr.error(response.message);
                    }
                },
            })
        }
    })
</script>
<!-- Jquery Ends -->
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>

