<?php
if (isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] != '')) {
    $url = SUBDOMAIN_URL . "/Dashboard/Dashboard/";
    header('Location: ' . $url);
} else {

    if(isset($_GET['prefilled']))
    {
        if (!$_GET['prefilled']) {
            header('Location: /');
        }

    }
}

include_once(COMPANY_DIRECTORY_URL. "/views/layouts/admin_header.php");

$email='';
$pass='';
$check='';
$super_admin_name = '';
if(isset($_COOKIE['remember_login'])) {
    $data   = unserialize($_COOKIE['remember_login']);
    $email  =$data['email'];
    $pass   =$data['password'];
    $check  ='checked';
    $super_admin_name = '';
}

if(isset($_REQUEST['prefilled']))
{
    $data   = base64_decode($_REQUEST['prefilled']);
    $data   = json_decode($data);
    $email  = @$data->u;
    $pass   = @$data->p;
    $super_admin_name  = @$data->sa_name;
}
?>
<style>
    /***23OCt***/
    .suspend-account {
        background: #d6f0fd;
        border: 5px solid #3c42a2;
    }
    .suspend-account h4 {
        margin-top: 20px;
        color: #7f939c;
        font-size: 18px;
    }
    .suspend-account p {
        color: #7f939c;
    }
    .text-dkblue {
        color: #3e525b;
    }
    .footer-suspend  p{
        margin: 0;
        line-height: 20px;
        color: #495d68;
    }
</style>
    <div class="apxpg-login">
        <div id="wrapper">
            <div class="ie_wrapper">
                <div class="outdated-browser-warning" id="outdatedBrowserWarning" style="display: none; opacity: 1;">
                    <h1 class="outdated-browser-warning__title">Your browser is not supported</h1>
                    <p class="outdated-browser-warning__close">
                        <a class="outdated-browser-warning__close-button" id="outdatedBrowserWarningClose" href="#">×</a>
                    </p>
                    <p class="outdated-browser-warning__contents">

                        Apexlink no longer supports Internet Explorer.
                        To avoid potential issues, we recommend using a supported internet browser.
                        <br>
                    </p>
                    <a class="btn btn-secondary outdated-browser-warning__update-link" href="#" target="_blank">Find a Supported Browser</a>
                </div>
            </div>
            <div class="login-bg">
                <div class="login-outer">
                    <div class="login-logo"><img src="<?php echo COMPANY_SUBDOMAIN_URL;?>/images/logo-login.png"/></div>
                    <div class="login-inner">
                        <form id="login" >
                            <h2>SIGN IN TO YOUR APEXLINK ACCOUNT</h2>
                            <div class="login-data">
                                <label>
                                    <?php if(isset($_REQUEST['prefilled'])){ ?>
                                        <input class="form-control" placeholder="Email" value="admin@apexlink.com" type="text"/>
                                        <input class="form-control" name="email" placeholder="Email" value="<?php echo $email; ?>" id="email" type="hidden"/>
                                    <?php } else { ?>
                                        <input class="form-control" name="email" placeholder="Email" value="<?php echo $email; ?>" id="email" type="text"/>
                                    <?php } ?>
                                </label>
                                <label>
                                    <?php if(isset($_REQUEST['prefilled'])){ ?>
                                        <input class="form-control" id="password2" value="SYSTEM" placeholder="Password" type="password" autocomplete="false"/>
                                        <input class="form-control" id="super_admin_name" name="super_admin_name" value="<?php echo $super_admin_name; ?>" type="hidden"/>
                                        <input class="form-control email-psw-input" id="password" name="password" value="<?php echo $pass; ?>" placeholder="Password" style="display:none;" type="password"/>
                                        <span toggle="#password2" class="fa fa-fw fa-eye field-icon toggle-password" ></span>
                                    <?php } else { ?>
                                        <input class="form-control email-psw-input" id="password" name="password" value="<?php echo $pass; ?>" placeholder="Password" type="password"/>
                                        <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password" ></span>
                                    <?php } ?>
                                </label>
                                <div class="remember-psw">
                                    <div class="remember-psw-lt"><input type="checkbox" name='remember' id="remember"  <?php echo $check; ?> /> Remember Me</div>
                                    <div class="remember-psw-rt"><a href="/User/ForgotPassword">Forgot password?</a></div>
                                </div>
                                <div class="btn-outer">
                                    <input type="submit" name="Sign In" value="Sign In" class="blue-btn"  id="login_button"/>
                                    <!--<input  type="button" value="Cancel" class="grey-btn" id="cancel"/>-->
                                </div>
                                <br><br>
                                <div class="btn-outer-link">
                                    By signing in, you agree to ApexLink’s <a class="blue-text-company" target="_blank" href="https://www.apexlink.com/terms_use/">terms of use agreement</a>
                                    and <a class="blue-text-company" target="_blank" href="https://www.apexlink.com/terms_privacy.html">privacy statement</a>.
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Wrapper Ends -->
    <!--Modal Popup-->

    <div id="spotlight_model" class="modal fade" role="dialog">
        <div class="modal-dialog spotlight-pop">
            <!-- Modal content-->
            <div class="modal-content suspend-account">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <img src="<?php echo COMPANY_SUBDOMAIN_URL;?>/images/account-suspended.png"/>
                        </div>
                        <div class="col-xs-12">
                            <h4>Dear Valued Client,</h4>
                            <p>Sorry for Inconvenience! We have noticed that your 14 days free trial period has been expired, so your account has been suspended. Please contact to Apexlink Support Team at 772-212-1950 or <a href="javascript:;">Click Here</a> to get paid subscription.</p>
                        </div>
                        <div class="col-xs-12 footer-suspend">
                            <p class="text-dkblue"><b>Thank You</b></p>
                            <p class="text-dkblue"><b>Apexlink Team</b></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Modal Popup Ends-->

    <!-- Jquery Starts -->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL;?>/js/validation/users/users.js"></script>
    <script type="text/javascript">
        $(document).on('click','.toggle-password',function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    </script>

<?php
if (isset($_SESSION[SESSION_DOMAIN]["message"])) {
    $message = $_SESSION[SESSION_DOMAIN]["message"];
//    print_r($message); exit;
    ?>
    <script>
        toastr.success("<?php echo $message ?>");
    </script>
    <?php
    unset($_SESSION[SESSION_DOMAIN]["message"]);
}
?>
    <script>
        $(document).ready(function () {
            $('a#outdatedBrowserWarningClose').on('click',function () {
                $('#outdatedBrowserWarning').hide();
                setCookie("outdatedBrowser", true, 365);
            })
            function isIE() {
                ua = navigator.userAgent;
                /* MSIE used to detect old browsers and Trident used to newer ones*/
                var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

                return is_ie;
            }

            function getCookie(cname) {
                var name = cname + "=";
                var ca = document.cookie.split(';');
                for(var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }

            function setCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                var expires = "expires="+d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            }


            if (isIE()){
                var get_cookie = getCookie('outdatedBrowser');
                if(!get_cookie)
                {
                    $('#outdatedBrowserWarning').show();
                }
            }
        })
    </script>
<?php include_once(COMPANY_DIRECTORY_URL."/views/layouts/admin_footer.php"); ?>