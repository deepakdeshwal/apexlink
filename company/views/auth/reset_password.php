<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/17/2019
 * Time: 3:23 PM
 */
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    include_once ('../../constants.php');
    header('Location: ' . BASE_URL);
};
$token = explode('token=', $_SERVER["REQUEST_URI"]);
$token = end($token);

if (isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] != '')) {
    $url = BASE_URL . "Dashboard/Dashboard";
    header('Location: ' . $url);
}
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");

?>
    <div class="apxpg-login">
        <div id="wrapper">
            <div class="login-bg">
                <div class="login-outer">
                    <div class="login-logo"><img src="<?php echo COMPANY_SUBDOMAIN_URL;?>/images/logo-login.png"/></div>
                    <div class="login-inner" >
                        <div id="reset_password_div">
                            <form name="reset_password" id="reset_password" action="../../user.php" method="post" enctype="multipart/form-data" >
                                <h2>RESET PASSWORD</h2>
                                <div class="login-data">
                                    <label>
                                        <input class="form-control" type="password" name="new_password" placeholder="New Password" id="new_password" />
                                    </label>
                                    <label>
                                        <input class="form-control" type="password" name="confirm_password" placeholder="Confirm Password" id="confirm_passowrd"/>
                                    </label>
                                    <input class="form-control" type="hidden" value="<?php echo $token ?>" name="forgot_password_token" placeholder="Confirm Password" id="forgot_password_token"/>
                                    <div class="btn-outer">
                                        <input type="submit" name="reset" value="Reset" class="blue-btn" id="reset"/>
                                        <input  type="button" value="Cancel" class="grey-btn" id="reset_cancel"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="click_here_login_id" style="display: none; text-align: center">
                            <p>Password reset successfully.</p>
                            <a href="/" style="text-decoration: underline; color: #551A8B">Click Here to Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Wrapper Ends -->

    <!-- Jquery Starts -->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL;?>/js/validation/users/users.js"></script>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>