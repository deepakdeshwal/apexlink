<table id="background-table" style="background-color: #ececec; width: 100%;" border="0" cellspacing="0" cellpadding="0">
    <tbody>
    <tr style="border-collapse: collapse;">
        <td style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" align="center" bgcolor="#ececec">
            <table class="w640" style="margin: 0px 10px; width: 640px;" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr style="background-color: #00b0f0; height: 20px;">
                    <td>&nbsp;</td>
                </tr>
                <tr style="border-collapse: collapse; background-color: #fff;">
                    <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580">
                        <div style="margin-top: 1px; margin-bottom: 10px;  text-align:center">
                            <img alt="ApexLink" src="#logo#"/>
                        </div>
                    </td>
                </tr>
                <tr style="background-color: #00b0f0; height: 20px;">
                    <td>&nbsp;</td>
                </tr>
                <tr id="simple-content-row" style="border-collapse: collapse;">
                    <td class="w640" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" bgcolor="#ffffff" width="640">
                        <table class="w640" style="width: 640px;" border="0" cellspacing="0" cellpadding="0" align="left">
                            <tbody>
                            <tr style="border-collapse: collapse;">
                                <td class="w30" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="30">&nbsp;</td>
                                <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580">
                                    <table class="w580" style="width: 580px;" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr style="border-collapse: collapse;">
                                            <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580">
                                                <p class="article-title" style="font-size: 22px; line-height: 24px; color: #ff0000; font-weight: bold; text-align: center; margin-top: 0px; margin-bottom: 18px; font-family: Arial, Helvetica, Geneva, sans-serif;" align="left">Forgot your password?</p>
                                                <div class="article-content" style="font-size: 13px; line-height: 18px; color: #444444; margin-top: 0px; margin-bottom: 18px; font-family: Arial, Helvetica, Geneva, sans-serif;" align="left">
                                                    <p style="margin-bottom: 15px;">Dear #user_name#,</p>
                                                    <p style="margin-bottom: 15px;">This email was sent automatically to you by Apexlink in response to your request to recover your password. This is done for your protection; only you, the recipient of this email can take the necessary step in your password recover process.</p>
                                                    <p style="margin-bottom: 15px;">To reset your password and access your account either click on the following link below or copy link and paste the link into the address bar of your browser:</p>
                                                    <p style="margin-bottom: 15px;"><!--{Url}--> <a href="#reset_password_link#">Click here to reset your password.</a></p>
                                                    <p style="margin-bottom: 15px;">This request was made from:</p>
<!--                                                    <p style="margin-bottom: 15px;">IP address: #IP_address#</p>-->
                                                    <p style="margin-bottom: 15px;">Thank you</p>
                                                    <p style="margin-bottom: 15px;">Apexlink Team</p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="border-collapse: collapse;">
                                            <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580" height="10">&nbsp;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td class="w30" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="30">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr style="border-collapse: collapse;">
                    <td class="w640" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="640">
                        <table id="footer" class="w640" style="border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #00b0f0; color: #ffffff; width: 640px;" border="0" cellspacing="0" cellpadding="0" bgcolor="#00b0f0">
                            <tbody>
                            <tr style="border-collapse: collapse;">
                                <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" colspan="4" valign="top" width="360">
                                    <p class="footer-content-left" style="-webkit-text-size-adjust: none; -ms-text-size-adjust: none; font-size: 13px; line-height: 15px; color: #ffffff; margin-top: 0px; margin-bottom: 15px; width: 630px;" align="center">
                                        ApexLink Property Manager ●  <a style="color: #fff; text-decoration: none;" href="javascript:;">support@apexlink.com&nbsp;● 772-212-1950</a></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr style="border-collapse: collapse;">
                    <td class="w640" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="640" height="60">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>