
<table width="680" align="center" cellspacing="0" cellpadding="0">

  <tr>
    <td style="background: #00b0f0; height: 40px;">
      
    </td>
  </tr>
  <tr>
    <td style="border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0; padding: 0 50px 50px 50px">
      <table width="100%" align="center" cellspacing="0" cellpadding="0">
        <tr>
          <td style="font-size: 20px; color: #00b0f0; font-weight: bold; padding: 20px 0; text-align: center;"> Phone Call Log </td>
        </tr>
      </table>
      <table width="100%" align="center" border="0" cellspacing="0" cellpadding="5">
        <tr>
          <td>Dear #user_name#, </td>
        </tr>
        <tr>
          <td>Greetings! </td>
        </tr>
        <tr>
          <td>Please find attached the phone call log for today i.e. #date# </td>
        </tr>
        <tr>
          <td>Thank you for using Apexlink Property Management System to manage your property</td>
        </tr>
        <tr>
          <td>Best Regards, </td>
        </tr>
        <tr>
          <td>Apexlink Inc.</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td style="background: #00b0f0; color: #fff; padding: 10px; text-align: center; font-weight: bold;">
      Apexlink.apexlink@yopmail.com
      
    </td>
  </tr>
</table>
