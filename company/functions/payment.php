<?php
/**
 * Created by PhpStorm.
 * User: ptripathi
 * Date: 1/21/2019
 * Time: 6:00 PM
 */
include_once(ROOT_URL.'/config.php');
include_once (ROOT_URL.'/constants.php');
include_once( ROOT_URL."/company/helper/helper.php");
include_once( "$_SERVER[DOCUMENT_ROOT]/company/helper/ddl.php");
include_once( ROOT_URL."/helper/globalHelper.php");

if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
    $url = BASE_URL."login";
    header('Location: '.$url);
}

class PaymentAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }


    /**
     *  function to add card
     */
    public function addCard(){
        try {

            $data = postArray($_POST['form']);
            //Required variable array
            $required_array = ['cfirst_name','clast_name','ccard_number','cexpiry_year','cexpiry_month','ccvv','cCompany','cphoneNumber','caddress1','ccity','cstate','czip_code','ccountry'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $request =[];
                $stripe_account_id = "";
                $checkAndGetCustomerId =   $this->checkAndGetCustomerId();
                if(!empty($checkAndGetCustomerId["data"])){
                    $stripe_account_id =$checkAndGetCustomerId["data"]["stripe_account_id"];
                }
                if(empty($checkAndGetCustomerId["data"]["stripe_customer_id"])){
                    $request["email"] =$checkAndGetCustomerId["data"]["email"];
                    $create_customer =    createCustomer($request);
                    if($create_customer["code"] == 200 && $create_customer["status"]== "success"){
                        $customer_id = $create_customer["account_data"];
                        $customer_data["stripe_customer_id"] = $customer_id;
                        $save_stripe_customer_id = saveStripeCustomerId($this->companyConnection ,$customer_data,$_SESSION[SESSION_DOMAIN]['cuser_id']);
                        if($save_stripe_customer_id["code"]== 200){
                            $add_card_to_customer["customer_id"] = $customer_id;
                            $add_card_to_customer["card_number"] = $data["ccard_number"];
                            $add_card_to_customer["exp_month"] = $data["cexpiry_month"];
                            $add_card_to_customer["exp_year"] = $data["cexpiry_year"];
                            $add_card_to_customer["cvc"] = $data["cexpiry_year"];
                            $add_customer_card =    addCard($add_card_to_customer);
                            // dd($add_customer_card);
                            if($add_customer_card["code"]== 200 && $add_customer_card["status"] == "success"){
                                return array('code' => 200, 'status' => 'success','message' => "Card Attached successfully" ,"stripe_account_id"=>$stripe_account_id);
                            }else{
                                return array('code' => 400, 'status' => 'failed','message' => $add_customer_card["message"]);
                            }
                        }else{
                            return array('code' => 400, 'status' => 'failed','message' => "Error in saving customer id");
                        }
                    }else{
                        return array('code' => 400, 'status' => 'failed','message' => $create_customer["message"]);
                    }
                }else{
                    $add_card_to_customer["customer_id"] = $checkAndGetCustomerId["data"]["stripe_customer_id"];
                    $add_card_to_customer["card_number"] = $data["ccard_number"];
                    $add_card_to_customer["exp_month"] = $data["cexpiry_month"];
                    $add_card_to_customer["exp_year"] = $data["cexpiry_year"];
                    $add_card_to_customer["cvc"] = $data["cexpiry_year"];
                    $add_customer_card =    addCard($add_card_to_customer);
                    if($add_customer_card["code"]== 200 && $add_customer_card["status"] == "success"){
                        return array('code' => 200, 'status' => 'success','message' => "Card Attached successfully","stripe_account_id"=>$stripe_account_id);
                    }else{
                        return array('code' => 400, 'status' => 'failed','message' => $add_customer_card["message"]);
                    }
                }

            }
        }
        catch (PDOException $e) {
            echo '<pre>';
            print_r($e);die;
            echo $e;
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function to save stripe customer id
     */
    public function saveStripeCustomerId(){
        $userdetail= [];
        $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $userdetail = getSingleRecord($this->companyConnection ,['column'=>'id','value'=>$id], 'users');
        return $userdetail;

    }

    /**
     *  function to check user customer id
     */
    public function checkAndGetCustomerId(){
        $userdetail= [];
        $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $userdetail = getSingleRecord($this->companyConnection ,['column'=>'id','value'=>$id], 'users');
        return $userdetail;

    }

    /**
     *  function to get account verification
     */
    public function getAccountVerification() {
        try {
            //Save Data in Company Database
            $request = [];

            $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $user_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $_SESSION[SESSION_DOMAIN]['cuser_id']], 'users');
            if(!empty($user_data["data"])){
                    if(!empty($user_data["data"]["stripe_account_id"])) {
                        $account_id = $user_data["data"]["stripe_account_id"];
//                        $account_id = 'acct_1Fb1FXIYilvAmUaE';
                       $request["account_id"] = $account_id;
                       $stripe_account_array["stripe_account_id"] = $account_id;
                       $connected_account_detail = getConnectedAccount($request);
                       if($connected_account_detail['account_data']["business_type"]=="company"){
                           $get_person_detail = getPerson($request);
                           $status = (isset($get_person_detail["person_detail"]["data"][0]["verification"]["status"]) && $get_person_detail["person_detail"]["data"][0]["verification"]["status"] == "verified"  && empty($connected_account_detail["account_data"]["company"]["requirements"]["current_deadline"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["currently_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["disabled_reason"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["eventually_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["past_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["pending_verification"]) ) ? 'Verified' : "Not Verified";
                       }else {
                           $status = (isset($connected_account_detail["account_data"]["individual"]["verification"]["status"]) && $connected_account_detail["account_data"]["individual"]["verification"]["status"] == "verified" && empty($connected_account_detail["account_data"]["individual"]["requirements"]["currently_due"]) && empty($connected_account_detail["account_data"]["individual"]["requirements"]["eventually_due"])&& empty($connected_account_detail["account_data"]["individual"]["requirements"]["past_due"])&& empty($connected_account_detail["account_data"]["individual"]["requirements"]["pending_verification"])) ? 'Verified' : "Not Verified";
                       }
                    }else{
                        $connected_account_detail["account_data"] = '';
                        $status =  "Not Verified";
                    }
            }else{
                $status =  "Not Verified";
                $connected_account_detail["account_data"] = '';
            }

            return array('code' => 200, 'account_status' => $status, 'status' => 'success','account_data'=>$connected_account_detail["account_data"],   'message' => 'Account status fetched successfully.');


        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }


    /**
     *  function to get account verification
     */
    public function getUserAccountVerification() {
        try {
            //Save Data in Company Database
            $request = [];
            $data =$_POST;

            $data['user_id'] = $data["user_id"];
            $user_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $data['user_id']], 'users');
            $user_account_detail = '';
            if(!empty($user_data["data"])){
                $user_account_detail = getSingleRecord($this->companyConnection, ['column' => 'user_id', 'value' => $data['user_id']], 'user_account_detail');
                if(!empty($user_data["data"]["stripe_account_id"])) {
                    $account_id = $user_data["data"]["stripe_account_id"];
//                        $account_id = 'acct_1Fb1FXIYilvAmUaE';
                    $request["account_id"] = $account_id;
                    $stripe_account_array["stripe_account_id"] = $account_id;
                    $connected_account_detail = getConnectedAccount($request);
                    if($connected_account_detail['account_data']["business_type"]=="company"){
                        $get_person_detail = getPerson($request);
                        $status = (isset($get_person_detail["person_detail"]["data"][0]["verification"]["status"]) && $get_person_detail["person_detail"]["data"][0]["verification"]["status"] == "verified"  && empty($connected_account_detail["account_data"]["company"]["requirements"]["current_deadline"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["currently_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["disabled_reason"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["eventually_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["past_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["pending_verification"]) ) ? 'Verified' : "Not Verified";
                    }else {
                        $status = (isset($connected_account_detail["account_data"]["individual"]["verification"]["status"]) && $connected_account_detail["account_data"]["individual"]["verification"]["status"] == "verified" && empty($connected_account_detail["account_data"]["individual"]["requirements"]["currently_due"]) && empty($connected_account_detail["account_data"]["individual"]["requirements"]["eventually_due"])&& empty($connected_account_detail["account_data"]["individual"]["requirements"]["past_due"])&& empty($connected_account_detail["account_data"]["individual"]["requirements"]["pending_verification"])) ? 'Verified' : "Not Verified";
                    }
                }else{
                    $connected_account_detail["account_data"] = '';
                    $status =  "not_exists";
                }
            }else{
                $status =  "Not Verified";
                $connected_account_detail["account_data"] = '';
            }

            return array('code' => 200, 'account_status' => $status, 'status' => 'success','account_data'=>$connected_account_detail["account_data"],'user_account_detail'=>$user_account_detail,   'message' => 'Account status fetched successfully.');


        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    /**
     *  function to update customer account
     */
    public function updateAccount(){
        try {
            if(!empty($fdata['fbirth_date'])){
                $day = '';
                $month = '';
                $year = '';
                $date =     mySqlDateFormat($fdata['fbirth_date']);
                $day = date("d",strtotime($date));
                $month = date("m",strtotime($date));
                $year = date("Y",strtotime($date));
                //  dd($year);
            }
            $website_url = "https://www.apexlink.com/";
            $address1 = "address_full_match"; /*$fdata['faddress1'];*/
            $address2 = ""; /*$fdata['faddress2'];*/
            $phone = (isset($fdata['fphone_number']) && !empty($fdata['fphone_number'])) ? str_replace(["-", "–"], '', $fdata['fphone_number']) : '';

            $stripe_account_array = ["email" => $fdata['femail'],"first_name" => $fdata['ffirst_name'],"last_name" => $fdata['flast_name'],"city" => $fdata['fcity'],"state" => $fdata['fstate'],"line1" => $address1,"line2" => $address2,'postal_code'=>$fdata['fzipcode'],'day'=>$day,'month'=>$month,'year'=>$year ,'ssn_last_4'=>$fdata['fssn'],'ssn_last_4'=>$fdata['fssn'],'phone'=>$phone,'document_front'=>'','document_back'=>'','website_url'=>$website_url,'product_description'=>'','support_email'=>$fdata['femail'],'country'=>$fdata['fiso'],'currency'=>$fdata["fcurrency"],'routing_number'=>$fdata["frouting_number"],'account_number'=>$fdata["faccount_number"]];

            $update_account =  updateAccount($stripe_account_array);
            dd($update_account);
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }


    }

    /**
     *  function to get all mcc types
     */
    public function getMccTypes()
    {
        try {
            $data = [];
            $data['mcc_types'] =$this->companyConnection->query("SELECT id,name, code FROM mcc_types")->fetchAll();
            return ['code' => 200, 'status' => 'success', 'data' => $data];
        } catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    /**
     *  function to get connected account
     */
    public function getConnectedAccount()
    {
        try {
            $account_detail='';
           $user_detail =  $this->checkAndGetCustomerId();
        if(isset($user_detail["data"]["stripe_account_id"]) && !empty($user_detail["data"]["stripe_account_id"])){

            $request["account_id"] = $user_detail["data"]["stripe_account_id"];
               // $account_detail =  getConnectedAccount($request);
            $account_detail = getSingleRecord($this->companyConnection ,['column'=>'user_id','value'=>1], 'user_account_detail');

                return ['code' => 200, 'status' => 'success', 'data' => $account_detail];
        }else{
            return array('code' => 400, 'status' => 'error', 'message' => "Record not fetched successfully");
        }
        } catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    /**
     *  function tovalidate
     */
    public function validateCompanyFinancialData()
    {
        try{
            $fdata = $_REQUEST;
            $formrequiredkeys = ['ffirst_name','flast_name','femail','fphone_number','fbirth_date','fzipcode','fcity','fstate','faddress','fbusiness','fssn','faccount_holder','faccount_holdertype',
                'faccount_number','fiso','fcurrency','frouting_number','fcaddress','fcaddress','fcaddress2','fczipcode','fcstate','fcname','furl','company_document_id'
            ];
            $data = [];
            if(isset($fdata['data']))
            {
                foreach ($fdata['data'] as $key=>$value)
                {
                    if(isset($value['name']))
                    {
                        $data[$value['name']] = @$value['value'];
                    }
                }
            }
            $err_array = validation($data,$this->conn,$formrequiredkeys);
            if(!empty($err_array)){
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                return ['status'=>'success','code'=>200,'data'=>$data];
            }
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }


    public function updateCompanyConnectedAccount(){
        try {

            $fdata = $_POST['financial_data'];
            $fdata = postArray($fdata);
            if(isset($_POST["company_id"])){
            $company_id = $_POST["company_id"];
            }else{
                $company_id = 1;
            }
            //$website_url = "https://www.apexlink.com/";
            $website_url = $fdata['furl'];
            $address1 = "address_full_match"; /*$fdata['faddress1'];*/
            $address2 = ""; /*$fdata['faddress2'];*/
            $phone = (isset($fdata['fphone_number']) && !empty($fdata['fphone_number'])) ? str_replace(["-", "–"], '', $fdata['fphone_number']) : '';
//            $company_id = $_SESSION[SESSION_DOMAIN]['admindb_id'];

            $stripe_account_array = ["email" => $fdata['femail'], "first_name" => $fdata['ffirst_name'], "last_name" => $fdata['flast_name'], "city" => $fdata['fcity'], "state" => $fdata['fstate'], "line1" => $address1, "line2" => $address2, 'postal_code' => $fdata['fzipcode'], 'day' => $fdata['fday'], 'month' => $fdata['fmonth'], 'year' => $fdata['fyear'], 'ssn_last_4' => $fdata['fssn'], 'ssn_last_4' => $fdata['fssn'], 'phone' => $phone, 'document_front' => $fdata['company_document_id'], 'document_back' => '', 'website_url' => $website_url, 'product_description' => '', 'support_email' => $fdata['femail'], 'country' => 'US', 'currency' => 'USD', 'routing_number' => $fdata["frouting_number"], 'account_number' => $fdata["faccount_number"], 'mcc' => $fdata["fmcc"],'company_name'=>$fdata['fcname'],'company_line1'=>$address1,'company_line2'=>$address2,'company_country'=>'US','company_city'=>$fdata['fccity'],'company_postal_code'=>$fdata['fczipcode'],'company_state'=>$fdata['fcstate'],'company_tax_id'=>$fdata['fctax_id'],'business_type'=>$fdata['fbusiness'],'company_phone'=>$fdata['fcphone_number']];
            $userdetail = getSingleRecord($this->companyConnection ,['column'=>'id','value'=>1], 'users');




                if(!empty($userdetail["data"]["stripe_account_id"])){
                $stripe_account_array["stripe_account_id"] = $userdetail["data"]["stripe_account_id"];
                $enablePmAccount = updateCompanyAccount($stripe_account_array);
                if($enablePmAccount["code"]== 200){
                    $data["stripe_account_id"]=$userdetail["data"]["stripe_account_id"];
                    $alldata['stripe_account_id'] = $userdetail["data"]["stripe_account_id"];
                    $stripe_account_detail_array = ["user_id"=>$company_id,"email" => $fdata['femail'], "first_name" => $fdata['ffirst_name'], "last_name" => $fdata['flast_name'], "city" => $fdata['fcity'], "state" => $fdata['fstate'], "address_1" => $address1, "address_2" => $address2, 'postal_code' => $fdata['fzipcode'], 'day' => $fdata['fday'], 'month' => $fdata['fmonth'], 'year' => $fdata['fyear'], 'ssn_last' => $fdata['fssn'], 'ssn_last' => $fdata['fssn'], 'phone' => $phone, 'url' => $website_url, 'country' => 'US', 'routing_number' => $fdata["frouting_number"], 'account_number' => $fdata["faccount_number"], 'mcc' => $fdata["fmcc"],'company_name'=>$fdata['fcname'],'company_address1'=>$address1,'company_address2'=>$address2,'company_city'=>$fdata['fccity'],'company_postal_code'=>$fdata['fczipcode'],'company_state'=>$fdata['fcstate'],'tax_id'=>$fdata['fctax_id'],'business_type'=>$fdata['fbusiness'],'company_phone_number'=>$fdata['fcphone_number'],'company_document_id'=>$fdata['company_document_id']];
                    $save_account_detail = updateUserAccountDetail($this->companyConnection,$stripe_account_detail_array,$company_id);

                }
                return $enablePmAccount;
            }else{
                    return array('code' => 400, 'status' => 'error', 'message' => "Account id not exists");
            }
        }catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }

    }

    public function uploadCompanyDocument(){
        try {
            $request=[];
            if ($_FILES["company_document"]["error"] == UPLOAD_ERR_OK)
            {
                $file = $_FILES["company_document"]["tmp_name"];
                // now you have access to the file being uploaded
                // $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $company_id;
                $uploadPath = ROOT_URL . '/uploads/company_documents/' . $_FILES["company_document"]["name"];
                //perform the upload operation.
                if (!is_dir(ROOT_URL . '/uploads/company_documents')) {
                    //Directory does not exist, so lets create it.
                    mkdir(ROOT_URL . '/uploads/company_documents', 0777, true);
                }
                if( move_uploaded_file ( $file, $uploadPath)){
                    $request["path"] = $uploadPath;
                    $upload_documents = uploadDocuments($request);
                    if($upload_documents["code"] == 200){
                        unlink($uploadPath);
                        return array('code' => 200, 'status' => 'success', 'message' => "Document Uploaded Successfully",'document_id'=>$upload_documents["document_id"]);
                    }else{
                        return array('code' => 504, 'status' => 'error', 'message' => "Document Not Uploaded Successfully");
                    }
                }else{
                    return array('code' => 504, 'status' => 'error', 'message' => "Document Not Uploaded Successfully");
                }
            }
        }catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }

    }

    public function validateFinancialData()
    {
        try{
            $fdata = $_REQUEST;
            $formrequiredkeys = ['ffirst_name','flast_name','femail','fphone_number','fbirth_date','fzipcode','fcity','fstate','faddress','fbusiness','fssn','faccount_holder','faccount_holdertype',
                'faccount_number','fiso','furl','fcurrency','frouting_number'
            ];
            $data = [];
            if(isset($fdata['data']))
            {
                foreach ($fdata['data'] as $key=>$value)
                {
                    if(isset($value['name']))
                    {
                        $data[$value['name']] = @$value['value'];
                    }
                }
            }
            $err_array = validation($data,$this->conn,$formrequiredkeys);
            if(!empty($err_array)){
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                return ['status'=>'success','code'=>200,'data'=>$data];
            }
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }



    public function updateUserConnectedAccount(){
        try {

            $fdata = $_POST['financial_data'];
           // dd($fdata);
            $fdata = postArray($fdata);
            if(isset($_POST["company_id"])) {
                $company_id = $_POST["company_id"];
            }else{
                $company_id = 1;
            }
            //$website_url = "https://www.apexlink.com/";
            $website_url = $fdata['furl'];
            $address1 = "address_full_match"; /*$fdata['faddress1'];*/
            $address2 = ""; /*$fdata['faddress2'];*/
            $phone = (isset($fdata['fphone_number']) && !empty($fdata['fphone_number'])) ? str_replace(["-", "–"], '', $fdata['fphone_number']) : '';
            $stripe_account_array = ["email" => $fdata['femail'], "first_name" => $fdata['ffirst_name'], "last_name" => $fdata['flast_name'], "city" => $fdata['fcity'], "state" => $fdata['fstate'], "line1" => $address1, "line2" => $address2, 'postal_code' => $fdata['fzipcode'], 'day' => $fdata['fday'], 'month' => $fdata['fmonth'], 'year' => $fdata['fyear'], 'ssn_last_4' => $fdata['fssn'], 'phone' => $phone, 'document_front' => '', 'document_back' => '', 'website_url' => $website_url, 'product_description' => '', 'support_email' => $fdata['femail'], 'country' => 'US', 'currency' => 'USD', 'routing_number' => $fdata["frouting_number"], 'account_number' => $fdata["faccount_number"], 'mcc' => $fdata["fmcc"],'business_type'=>$fdata['fbusiness']];
            $userdetail = getSingleRecord($this->companyConnection ,['column'=>'id','value'=>$company_id], 'users');
            if(!empty($userdetail["data"]["stripe_account_id"])) {
                $stripe_account_array["stripe_account_id"] = $userdetail["data"]["stripe_account_id"];
                $enablePmAccount = enableAccount($stripe_account_array);
                if ($enablePmAccount["code"] == 200) {

                    $stripe_account_detail_array = ["user_id" =>$company_id, "email" => $fdata['femail'], "first_name" => $fdata['ffirst_name'], "last_name" => $fdata['flast_name'], "city" => $fdata['fcity'], "state" => $fdata['fstate'], "address_1" => $address1, "address_2" => $address2, 'postal_code' => $fdata['fzipcode'], 'day' => $fdata['fday'], 'month' => $fdata['fmonth'], 'year' => $fdata['fyear'], 'ssn_last' => $fdata['fssn'], 'ssn_last' => $fdata['fssn'], 'phone' => $phone, 'url' => $fdata['furl'], 'country' => 'US', 'routing_number' => $fdata["frouting_number"], 'account_number' => $fdata["faccount_number"], 'mcc' => $fdata["fmcc"], 'business_type' => $fdata['fbusiness'], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')];

                    $save_account_detail = updateUserAccountDetail($this->companyConnection, $stripe_account_detail_array,$company_id);
                }

            }

                return $enablePmAccount;

        }catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }

    }











}

$payment = new PaymentAjax();
