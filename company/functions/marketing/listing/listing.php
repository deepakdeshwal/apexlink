<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class listing extends DBConnection {

    /**
     * Marketing Listing constructor.
     */
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }
    public function onLoadPropertyListing() {
        try {
            $property_list_data =  $this->companyConnection->query("SELECT gp.id,gp.property_name,gp.address1,gp.city,gp.state,gp.country,cpt.property_type, IF(gp.is_short_term_rental = 1,'Yes', 'No') AS is_short_term_rental, gp.property_squareFootage, if(gp.property_price != ' ', gp.property_price, ' ') AS property_price, gp.vacant FROM general_property AS gp LEFT JOIN company_property_type AS cpt ON gp.payment_type = cpt.id WHERE gp.online_listing = 'on' AND gp.deleted_at IS NULL ORDER BY gp.property_name ASC")->fetchALL();
            $ddl_list_html = '<option value="">Select</option>';
            if (!empty($property_list_data)) {
                foreach ($property_list_data as $key1 => $val1) {
                    $ddl_list_html .= '<option value="'.$val1['id'].'">'.$val1['property_name'].'</option>';
                }
            }

            $page = $_POST['pagination'];

            $count = count($property_list_data);
            if($page == '0'){
                $offset = 0;
            } else {
                $offset = $page*5-5;
            }
            if($offset > $count) $offset = $count;

            $query = "SELECT gp.id,gp.property_name,gp.address1,gp.city,gp.state,gp.country,ud.id as unit_detail_id,cpt.property_type, IF(gp.is_short_term_rental = 1,'Yes', 'No') AS is_short_term_rental, gp.property_squareFootage, if(gp.property_price != ' ', gp.property_price, ' ') AS property_price, gp.vacant, gp.base_rent AS property_base_rent, ud.base_rent AS unit_base_rent  FROM general_property AS gp LEFT JOIN company_property_type AS cpt ON gp.property_type = cpt.id LEFT JOIN unit_details AS ud ON ud.property_id = gp.id WHERE gp.online_listing = 'on' AND gp.deleted_at IS NULL GROUP BY gp.id ORDER BY gp.property_name ASC LIMIT $offset,5";
            $marketing_list_data =  $this->companyConnection->query($query)->fetchALL();
            $property_images_data = '' ;
            foreach ($marketing_list_data as $key => $val) {
                $query = "SELECT pfu.file_extension, pfu.file_location FROM property_file_uploads AS pfu WHERE pfu.property_id ='".$val['id']."' AND pfu.file_type = 1 AND pfu.deleted_at IS NULL";
                $marketing_list_data[$key]['property_images_data'] =  $this->companyConnection->query($query)->fetchALL();

                $query = "SELECT mp.is_published, mp.is_featured, mp.show_on_map FROM marketing_posts AS mp WHERE mp.property_id ='".$val['id']."' AND mp.deleted_at IS NULL";
                $marketing_list_data[$key]['property_post_data'] =  $this->companyConnection->query($query)->fetch();

                $marketing_list_data[$key]['unit_base_rent'] = ($val['unit_base_rent'] != null) ? number_format($val['unit_base_rent'],2) : '';

                $marketing_list_data[$key]['count_applicants'] =  $this->companyConnection->query("SELECT count(*) as count_applicants FROM `company_rental_applications` WHERE prop_id='".$val['id']."'")->fetch ();
                $marketing_list_data[$key]['guest_card'] =  $this->companyConnection->query("SELECT count(*) as guest_card FROM `lease_guest_card` WHERE unit_id='".$val['unit_detail_id']."'")->fetch();

            }

            $pages = count($property_list_data)/5;
            if (strpos($pages, '.')) {
                $pages = explode('.', $pages);
                $total_pages = $pages[0] + 1;
            } else {
                $total_pages = $pages;
            }

            return ['status'=>'success','code'=>200,'ddl_list_html'=>$ddl_list_html, 'marketing_list_data'=>$marketing_list_data, 'data'=>$property_list_data, 'total_pages'=>$total_pages];
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function onSearchPropertyListing() {
        try {
            $and_condition = '';
            $property_price = 0;
            $property_id = '';
            $marketing_list_data = [];
            if (isset($_POST['property_price']) && $_POST['property_price'] != ""){
                $property_price = $_POST['property_price'];
                $property_price = str_replace( ',', '', $property_price );
            }
            if (isset($_POST['property_id']) && $_POST['property_id'] != ""){
                $property_id = $_POST['property_id'];
            }

            if ($property_price != 0 && $property_id !== ""){
                $query1 = "SELECT gp.id,gp.property_name,gp.address1,gp.city,gp.state,gp.country,ud.id as unit_detail_id,cpt.property_type, IF(gp.is_short_term_rental = 1,'Yes', 'No') AS is_short_term_rental, gp.property_squareFootage,  gp.vacant, gp.base_rent AS property_base_rent, ud.base_rent AS unit_base_rent  FROM general_property AS gp LEFT JOIN company_property_type AS cpt ON gp.property_type = cpt.id LEFT JOIN unit_details AS ud ON ud.property_id = gp.id WHERE gp.deleted_at IS NULL AND REPLACE(gp.base_rent,',','') <= $property_price AND gp.id = $property_id AND gp.base_rent != '' GROUP BY gp.id ORDER BY gp.property_name";
                $getPropertyPrice =  $this->companyConnection->query($query1)->fetchALL();

                $query2 = "SELECT gp.id,gp.property_name,gp.address1,gp.city,gp.state,gp.country,ud.id as unit_detail_id,cpt.property_type, IF(gp.is_short_term_rental = 1,'Yes', 'No') AS is_short_term_rental, gp.property_squareFootage, gp.vacant, gp.base_rent AS property_base_rent, ud.base_rent AS unit_base_rent FROM general_property AS gp LEFT JOIN company_property_type AS cpt ON gp.property_type = cpt.id LEFT JOIN unit_details AS ud ON ud.property_id = gp.id WHERE gp.deleted_at IS NULL AND ud.base_rent <= $property_price AND ud.base_rent != '' AND gp.id = $property_id  GROUP BY gp.id ORDER BY gp.property_name";
//                dd($query2);
                $getUnitPrice =  $this->companyConnection->query($query2)->fetchALL();

                $query3 = "SELECT gp.id,gp.property_name,gp.address1,gp.city,gp.state,gp.country,ud.id as unit_detail_id,cpt.property_type, IF(gp.is_short_term_rental = 1,'Yes', 'No') AS is_short_term_rental, gp.property_squareFootage, gp.vacant, gp.base_rent AS property_base_rent, ud.base_rent AS unit_base_rent FROM general_property AS gp LEFT JOIN company_property_type AS cpt ON gp.property_type = cpt.id LEFT JOIN unit_details AS ud ON ud.property_id = gp.id WHERE gp.deleted_at IS NULL AND gp.base_rent = '' AND ud.base_rent = '' GROUP BY gp.id ORDER BY gp.property_name";
                $nullPropertyUnitPrice =  $this->companyConnection->query($query3)->fetchALL();

                $marketing_list_data = array_merge($getPropertyPrice,$getUnitPrice);
            }

            if ($property_price != 0 && $property_id == ""){
                $query1 = "SELECT gp.id,gp.property_name,gp.address1,gp.city,gp.state,gp.country,ud.id as unit_detail_id,cpt.property_type, IF(gp.is_short_term_rental = 1,'Yes', 'No') AS is_short_term_rental, gp.property_squareFootage,  gp.vacant, gp.base_rent AS property_base_rent, ud.base_rent AS unit_base_rent  FROM general_property AS gp LEFT JOIN company_property_type AS cpt ON gp.property_type = cpt.id LEFT JOIN unit_details AS ud ON ud.property_id = gp.id WHERE gp.deleted_at IS NULL AND REPLACE(gp.base_rent,',','') <= $property_price AND gp.base_rent != '' GROUP BY gp.id ORDER BY gp.property_name";
                $getPropertyPrice =  $this->companyConnection->query($query1)->fetchALL();

                $query2 = "SELECT gp.id,gp.property_name,gp.address1,gp.city,gp.state,gp.country,ud.id as unit_detail_id,cpt.property_type, IF(gp.is_short_term_rental = 1,'Yes', 'No') AS is_short_term_rental, gp.property_squareFootage, gp.vacant, gp.base_rent AS property_base_rent, ud.base_rent AS unit_base_rent FROM general_property AS gp LEFT JOIN company_property_type AS cpt ON gp.property_type = cpt.id LEFT JOIN unit_details AS ud ON ud.property_id = gp.id WHERE gp.deleted_at IS NULL AND ud.base_rent <= $property_price AND ud.base_rent != '' GROUP BY gp.id ORDER BY gp.property_name";
                $getUnitPrice =  $this->companyConnection->query($query2)->fetchALL();

                $query3 = "SELECT gp.id,gp.property_name,gp.address1,gp.city,gp.state,gp.country,ud.id as unit_detail_id,cpt.property_type, IF(gp.is_short_term_rental = 1,'Yes', 'No') AS is_short_term_rental, gp.property_squareFootage, gp.vacant, gp.base_rent AS property_base_rent, ud.base_rent AS unit_base_rent FROM general_property AS gp LEFT JOIN company_property_type AS cpt ON gp.property_type = cpt.id LEFT JOIN unit_details AS ud ON ud.property_id = gp.id WHERE gp.deleted_at IS NULL AND gp.base_rent = '' AND ud.base_rent = '' GROUP BY gp.id ORDER BY gp.property_name";
                $nullPropertyUnitPrice =  $this->companyConnection->query($query3)->fetchALL();

                $marketing_list_data = array_merge($getPropertyPrice,$getUnitPrice);
            }

            if ($property_price == 0 && $property_id != ""){
                $property_id = $_POST['property_id'];
                if ($property_id != ' '){
                    $and_condition = ' AND gp.id = '.$property_id;
                }

                $query1 = "SELECT gp.id,gp.property_name,gp.address1,gp.city,gp.state,gp.country,ud.id as unit_detail_id,cpt.property_type, IF(gp.is_short_term_rental = 1,'Yes', 'No') AS is_short_term_rental, gp.property_squareFootage,  gp.vacant, gp.base_rent AS property_base_rent, ud.base_rent AS unit_base_rent  FROM general_property AS gp LEFT JOIN company_property_type AS cpt ON gp.property_type = cpt.id LEFT JOIN unit_details AS ud ON ud.property_id = gp.id WHERE gp.deleted_at IS NULL $and_condition GROUP BY gp.id ORDER BY gp.property_name";
                $marketing_list_data =  $this->companyConnection->query($query1)->fetchALL();
            }


            foreach ($marketing_list_data as $key => $val) {

                $query = "SELECT pfu.file_extension, pfu.file_location FROM property_file_uploads AS pfu WHERE pfu.property_id ='".$val['id']."' AND pfu.deleted_at IS NULL";
                $marketing_list_data[$key]['property_images_data'] =  $this->companyConnection->query($query)->fetchALL();

                $query = "SELECT mp.is_published, mp.is_featured, mp.show_on_map FROM marketing_posts AS mp WHERE mp.property_id ='".$val['id']."' AND mp.deleted_at IS NULL";
                $marketing_list_data[$key]['property_post_data'] =  $this->companyConnection->query($query)->fetch();

                $marketing_list_data[$key]['unit_base_rent'] = ($val['unit_base_rent'] != null) ? number_format($val['unit_base_rent'],2) : '';

                $marketing_list_data[$key]['count_applicants'] =  $this->companyConnection->query("SELECT count(*) as count_applicants FROM `company_rental_applications` WHERE prop_id='".$val['id']."'")->fetch ();
                $marketing_list_data[$key]['guest_card'] =  $this->companyConnection->query("SELECT count(*) as guest_card FROM `lease_guest_card` WHERE unit_id='".$val['unit_detail_id']."'")->fetch();


            }

            $pages = count($marketing_list_data)/5;
            if (strpos($pages, '.')) {
                $pages = explode('.', $pages);
                $total_pages = $pages[0] + 1;
            } else {
                $total_pages = $pages;
            }
            return ['status'=>'success','code'=>200, 'marketing_list_data'=>$marketing_list_data, 'total_pages'=> $total_pages];
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function updatePostData() {
        try {
            $property_id = $_POST['property_id'];
            $is_active = $_POST['is_active'];

            $selected_icon  = $_POST['selected_icon'];
            if ($selected_icon == 'publish'){
                $update_column = 'is_published';
                $change_status = $is_active == '1' ? 'no' : 'yes';
            } else if ($selected_icon == 'feature'){
                $update_column = 'is_featured';
                $change_status = $is_active == '1' ? 'no' : 'yes';
            } else {
                $update_column = 'show_on_map';
                $change_status = $is_active == '1' ? 'no' : 'yes';
            }

            $query = "SELECT * FROM marketing_posts WHERE property_id = ".$property_id;
            $post_data =  $this->companyConnection->query($query)->fetch();

            if (empty($post_data)){
                $insertData['property_id'] = $property_id;
                $insertData[$update_column] = $change_status;
                $insertData['created_at'] = date('Y-m-d H:i:s');
                $insertData['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($insertData);
                $query = "INSERT INTO  marketing_posts (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($insertData);
            } else {
                $insertData['property_id'] = $property_id;
                $insertData[$update_column] = $change_status;
                $insertData['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($insertData);
                $query = "UPDATE marketing_posts SET ".$sqlData['columnsValuesPair']." where property_id=".$property_id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
            }

            $query = "SELECT * FROM marketing_posts WHERE property_id = ".$property_id;
            $post_data =  $this->companyConnection->query($query)->fetch();

            return ['status'=>'success','code'=>200, 'data'=>$post_data];
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

}

$listing = new listing();
?>