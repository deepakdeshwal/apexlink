<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class map extends DBConnection
{

    /**
     * Marketing Listing constructor.
     */
    public function __construct()
    {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }


    public function getMapData(){
        try {
            $res=[];
            $property_list_data =  $this->companyConnection->query("SELECT gp.id,gp.property_name,gp.address1,gp.address2,gp.address3,gp.address4, IF(gp.is_short_term_rental = 1,'Yes', 'No') AS is_short_term_rental, gp.property_squareFootage, if(gp.property_price != ' ', gp.property_price, ' ') AS property_price, gp.vacant FROM general_property AS gp LEFT JOIN company_property_type AS cpt ON gp.payment_type = cpt.id WHERE gp.online_listing = 'on' AND gp.deleted_at IS NULL ORDER BY gp.property_name ASC")->fetchALL();
           if(!empty($property_list_data)) {
               foreach ($property_list_data as $propertyid) {
//                   echo "<pre>";print_r($propertyid);
                   $propertyAddress = "SELECT latitude,longitude,property_name FROM map_address as ma JOIN general_property as gp ON ma.property_id=gp.id WHERE ma.property_id=" . $propertyid['id'];
                   $res[] = $this->companyConnection->query($propertyAddress)->fetch();


               }
           }
            return array('data' => $res, 'status' => 'success');

        } catch (Exception $e){
            dd($e);
        }

    }

}

$map = new map();
?>