<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class flyer extends DBConnection
{

    /**
     * Marketing Listing constructor.
     */
    public function __construct()
    {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }
    public function ChangeDefaultTemplate(){
            try {

                $query = "UPDATE flyer_posts SET status ='1' where 	template_name='".$_POST['Fclass']."'";
//                dd($query);
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

                $query1 = "UPDATE flyer_posts SET status ='0' where template_name !='".$_POST['Fclass']."'";
                $stmt1 = $this->companyConnection->prepare($query1);
                $stmt1->execute();

                return array('code' => 200, 'status' => 'success');

            } catch (PDOException $e) {
                echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
                return;
            }

    }
    public function UploadLogo(){

        $file=$_FILES;

        if(!empty($file)){
            $domain = getDomain();
            $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
            $file_name = $file['logo']['name'];
            $file_tmp = $file['logo']['tmp_name'];
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            $name = time() . uniqid(rand());
            $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
            //Check if the directory already exists.
            if (!is_dir(ROOT_URL . '/company/' . $path)) {
                //Directory does not exist, so lets create it.
                mkdir(ROOT_URL . '/company/' . $path, 0777, true);
            }
            move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
        }
        $logo=$path.'/'.$name.'.'.$ext;

        $query1 = "UPDATE flyer_posts SET logo='".$logo."' where status ='1'";
        $stmt1 = $this->companyConnection->prepare($query1);
        $stmt1->execute();

        $query = "UPDATE flyer_posts SET logo ='' where status != '1'";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();

        return array('code' => 200, 'status' => 'success','logo'=>$logo,'name'=>$file_name);
    }
    public function getLogo(){
        try{
            $user_data =  $this->companyConnection->query("SELECT logo,status FROM flyer_posts WHERE logo !='  '")->fetch();

            return array('code' => 200, 'status' => 'success','data'=>$user_data);
        }catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }

    }







}

$flyer = new flyer();
?>