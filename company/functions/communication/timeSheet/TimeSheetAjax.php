<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};

class TimeSheetAjax extends DBConnection{
    /**
     * TimeSheetAjax constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function to get employees for time sheet module
     */
    public function getEmployees(){
        $employeeData = $this->companyConnection->query('SELECT id,name,role,phone_number,email,last_login FROM users WHERE role IS NOT NULL AND status="1" OR user_type="8" ORDER BY name ASC')->fetchAll();
        if(!empty($employeeData)){
            $data = array('code' => 200, 'status' => 'success','data'=>$employeeData);
        } else {
            $data = array('code' => 400, 'status' => 'error','data'=>$employeeData);
        }
        return $data;
    }

    /**
     * function to get employees for time sheet module
     */
    public function getPosition(){
        $roleData = $this->companyConnection->query('SELECT id,role_name FROM company_user_roles ORDER BY role_name ASC')->fetchAll();
        if(!empty($roleData)){
            $data = array('code' => 200, 'status' => 'success','data'=>$roleData);
        } else {
            $data = array('code' => 400, 'status' => 'error','data'=>$roleData);
        }
        return $data;
    }

    /**
     * function to get time sheet employee
     * @return array
     */
    public function getTimeSheetDetail(){
        try {
            $edit_id = $_POST['id'];
            $data = $this->companyConnection->query('SELECT * FROM time_sheet WHERE id='.$edit_id)->fetch();

            if (!empty($data)) {
                $data['date'] = !empty($data['date'])?dateFormatUser($data['date'],null,$this->companyConnection):'';
                $data['last_login'] = !empty($data['last_login'])?dateFormatUser($data['last_login'],null,$this->companyConnection):'';
                $data['time_in'] = !empty($data['time_in'])?date("g:i A", strtotime($data['time_in'])):'';
                $data['time_out_for_meal'] = !empty($data['time_out_for_meal'])?date("g:i A", strtotime($data['time_out_for_meal'])):'';
                $data['time_in_after_meal'] = !empty($data['time_in_after_meal'])?date("g:i A", strtotime($data['time_in_after_meal'])):'';
                $data['time_out'] = !empty($data['time_out'])?date("g:i A", strtotime($data['time_out'])):'';
                $data = array('code' => 200, 'status' => 'success', 'data' => $data);
            } else {
                $data = array('code' => 400, 'status' => 'error');
            }
            return $data;
        } catch(PDOException $e){
            dd($e);
        }
    }

    /**
     * function to save time sheet data
     */
    public function saveTimeSheet(){
        try{
            $data = $_POST['form'];
            $data = postArray($data);

            $diff = $this->calculateTimeDiff();
            if($diff['code'] == 400){
                return $diff;
            }
            /*Required variable array*/
            $required_array = ['employee_id','position','employeeEmail'];
            $maxlength_array = [];
            $number_array = [];
            /*Server side validation*/
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $id = null;
                if(isset($data['edit_id'])){
                    $id = $data['edit_id'];
                    unset($data['edit_id']);
                }
                $data['email'] = $data['employeeEmail'];
                $data['note'] = $data['note'];
                $data['date'] = !empty($data['date'])?mySqlDateFormat($data['date'],null,$this->companyConnection):null;
                $data['last_login'] = !empty($data['last_login'])?mySqlDateFormat($data['last_login'],null,$this->companyConnection):null;
                $data['time_in'] = !empty($data['time_in'])?mySqlTimeFormat($data['time_in']):null;
                $data['time_out_for_meal'] = !empty($data['time_out_for_meal'])?mySqlTimeFormat($data['time_out_for_meal']):null;
                $data['time_in_after_meal'] = !empty($data['time_in_after_meal'])?mySqlTimeFormat($data['time_in_after_meal']):null;
                $data['time_out'] = !empty($data['time_out'])?mySqlTimeFormat($data['time_out']):null;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                unset($data['employeeEmail']);
                if(empty($id)) {
                    /*Save Data in Company Database*/
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO time_sheet (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $msg = 'Record added successfully';
                } else {
//                    dd($data);
                    $sqlData = createSqlUpdateCase($data);
                    $query = "UPDATE time_sheet SET " . $sqlData['columnsValuesPair'] . " where id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);
                    $msg = 'Record updated successfully';
                }
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => $msg);
            }
        } catch (PDOException $e) {
            dd($e);
        }
    }

    /**
     * Delete TimeSheet
     * @return array
     */
    public function delete(){
        try{
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE time_sheet SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$id]);
            return ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function to get Time Sheet data
     * @return array
     */
    public function getTimeSheetPrintData() {
        $html = '';
        $current_date = date("Y-m-d");
        $formated_date = dateFormatUser($current_date,null,$this->companyConnection);
        $next_date = date('Y-m-d', strtotime("+1 day"));
        $previous_date = date('Y-m-d', strtotime("-1 days"));
        $sql =  "SELECT  time_sheet.id,  time_sheet.employee_id, time_sheet.position, time_sheet.email, time_sheet.mobile_number, time_sheet.date, time_sheet.last_login, time_sheet.total_hours,users.name,company_user_roles.role_name FROM time_sheet LEFT JOIN users ON time_sheet.employee_id=users.id LEFT JOIN company_user_roles ON time_sheet.position=company_user_roles.id WHERE ( time_sheet.deleted_at IS NULL AND DATE(time_sheet.created_at) BETWEEN '".$previous_date."' AND '".$next_date."')";
        // $sql = "SELECT * FROM  phone_call_logs WHERE user_type = '1' AND deleted_at IS NULL order by first_name ASC";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html.= '<div id="TestPhoneHeight" class="" style=" max-width: 1348px; ">
                                                    <div class="div-full3">
                                                        <div class="div-full3">
                                                            <div id="PrintPhone" style="page-break-after: always;">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<style type="text/css">
	body{ margin: 0; padding:0;  }
</style>
	<table cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">
		<tbody><tr>
			<td style="height: 11px; background: #00b0f0;"> </td>
		</tr>
		<tr>
			<td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; "><img src="../../image/avatar/" width="150" height="50" alt=""></td>
		</tr>
		<tr>
			<td style="background: #00b0f0; height: 15px;"> </td>
		</tr>
		<tr>
			<td style="padding: 20px; background: #fff; padding: 30px;">
				<table style="width: 100%; padding: 0 0 20px 0">
					<tbody><tr>
						<td width="33.33%"></td>
						<td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: center; ">Employee TimeSheet</td>
						<td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: right; ">'.$formated_date.'</td>
					</tr>
					<tr>
						<td colspan="2" height="50px"></td>
					</tr>
				</tbody></table>
				<table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0">
					<tbody><tr>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858;  font-weight: bold; font-family: arial;  font-size: 16px;">Employee Name</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Role</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Email</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Phone</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Date</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Last Login</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Time</th>
					</tr>';
        if(!empty($data)) {
//           dd($data);
            //dd(SESSION_DOMAIN);
            foreach ($data as $key => $calllogdata) {
                $due_date= '';
                $call_category = '';
                $follow_up = '';
                $role_data = (isset($calllogdata["role_name"]) && !empty($calllogdata["role_name"]))? $calllogdata["role_name"]: 'Employee';
                $due_date = (isset($calllogdata["date"]) && !empty($calllogdata["date"]))? dateFormatUser($calllogdata["date"],null,$this->companyConnection): '';
                $last_date = (isset($calllogdata["last_login"]) && !empty($calllogdata["last_login"]))? dateFormatUser($calllogdata["last_login"],null,$this->companyConnection): '';
                $time_hours = (isset($calllogdata["total_hours"]) && !empty($calllogdata["total_hours"]))? $calllogdata["total_hours"].' Hours': '';
                $html .= '<tr><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["name"].'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$role_data.'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["email"] .'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["mobile_number"]. '</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$due_date.'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$last_date.'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$time_hours.'</td></tr>';
            }
        }
        $html.='</tbody></table>

			</td>
		</tr>
		<tr>
			<td style="height: 11px; background: #00b0f0;"> </td>
		</tr>
	</tbody></table>

</div>
                                                        </div>
                                                        <div class="btn-outer3" style="padding-top: 2%;">
                                                        </div>
                                                    </div>
                                                    <!--/ END panel -->
                                                </div>';
        return array('data' => $html, 'status' => 'success','code' => 200);
    }

    public function calculateTimeDiff(){
        try{

            $time_in = $_POST['time_in'];
            $time_out_for_meal = $_POST['time_out_for_meal'];
            $time_in_after_meal = $_POST['time_in_after_meal'];
            $time_out = $_POST['time_out'];

            if(strtotime($time_in) > strtotime($time_out_for_meal)){
                return array('data' => 'Time out For Meal should be greater than Time out For Meal!', 'status' => 'warning','code' => 400);
            } elseif(strtotime($time_out_for_meal) < strtotime($time_in)){
                return array('data' => 'Time out For Meal should be greater than Time out For Meal!', 'status' => 'warning','code' => 400);
            } elseif(strtotime($time_out_for_meal) > strtotime($time_in_after_meal)){
                return array('data' => 'Time in After Meal should be greater than Time out For Meal!', 'status' => 'warning','code' => 400);
            } elseif(strtotime($time_in_after_meal) < strtotime($time_out_for_meal)){
                return array('data' => 'Time in After Meal should be greater than Time out For Meal!', 'status' => 'warning','code' => 400);
            } elseif(strtotime($time_out) < strtotime($time_in_after_meal)){
                return array('data' => 'Time out should be greater than Time In!', 'status' => 'warning','code' => 400);
            }

            $diffrence_two = strtotime($time_in_after_meal) - strtotime($time_out_for_meal);
            $diff = strtotime($time_out) - strtotime($time_in);
            $total = $diff-$diffrence_two;
            $total = date('H:i',$total);
            return array('data' => $total, 'status' => 'success','code' => 200);
        } catch(Exception $exception){
            dd($exception);
        }
    }

}
$announcementAjax = new TimeSheetAjax();