<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
/*require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/php-svg-lib/src/autoload.php');*/
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once (ROOT_URL . "/company/functions/elasticSearch/CrudFunction.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class lettersNoticesAjax extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function getTagsByLetterType(){
        try{
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM tags_letter_notices WHERE letter_type='$id' ORDER BY tag_name ASC");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function docTemplateDataDetails()
    {
        try{
            $id = $_POST['id'];


            $query = $this->companyConnection->query("SELECT * FROM letters_notices_template WHERE id =".$id);

            $settings = $query->fetch();
            $settingsAll['templateTitle'] = $settings;

            $letter_type = $settings['letter_type'];
            $query1 = $this->companyConnection->query("SELECT * FROM  tags_letter_notices  WHERE letter_type='$letter_type'");

            $settingsAll['templateTags'] = $query1->fetchAll();
            if(isset($_POST['userId'])){
                $userId = $_POST['userId'];
                $query2= $this->companyConnection->query("SELECT id,user_type FROM  users  WHERE id=$userId")->fetch();
                if ($query2['user_type'] == '1'){
                    $settingsAll['user_type'] ='PM';
                }else{
                    $settingsAll['user_type'] ='OTHER';
                }
                $sqlQuery = "SELECT * FROM `esign_signatures` where user_id=".$userId." AND temp_id=".$id;
                $user = $this->companyConnection->query($sqlQuery)->fetch();
                if(!empty($user)){
                    $settingsAll['get_signature'] =$user;
                }else{
                    $settingsAll['get_signature'] ='';
                }
            }

            $settingsAll['currentLoggedIn'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];

            return array('code' => 200, 'status' => 'success', 'data' => $settingsAll, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function updateletterNoticeData()
    {
        try {

            $id = $_POST['id'];
            $saveDoc = $_POST['saveDoc'];
            $data11 = $_POST['form'];
            $data1 = postArray($data11);


            /*$data['actualMoveOutDate'] = (isset($data1['actualMoveOutDate']) && !empty($data1['actualMoveOutDate'])) ? mySqlDateFormat($data1['actualMoveOutDate'], null, $this->companyConnection) : '';
            $data['unitAvailableDate'] = (isset($data1['unitAvailableDate']) && !empty($data1['unitAvailableDate'])) ? mySqlDateFormat($data1['unitAvailableDate'], null, $this->companyConnection) : '';
            $data['noOfKeysSigned_movein'] = $data1['noOfKeysSigned_movein'];*/
            $data['template_html'] = $_POST['template_html'];
            $data['letter_name'] = $data1['first_name'];
            $data['letter_type'] = $data1['letter_type'];
            //$data['tag_name'] = $data1['tag_name'];
            $data['description'] = $data1['middle_name'];

            $required_array = ['letter_name','letter_type'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data, $this->conn, $required_array, $maxlength_array, $number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(!empty($saveDoc) && $saveDoc == '1'){
                    $data['is_editable'] = "1";
                    $data['original_template_html'] = $_POST['template_html'];
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($data);

                    $query = "INSERT INTO letters_notices_template (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $letterAndNoticesData = $data;
                    $letterAndNoticesData['id'] = $this->companyConnection->lastInsertId();
                    $ElasticSearchSave = insertDocument('COMMUNICATION','ADD',$letterAndNoticesData,$this->companyConnection);
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The Record saved successfully');
                }else{
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE letters_notices_template SET " . $sqlData['columnsValuesPair'] . " where id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    $letterAndNoticesData = $data;
                    $letterAndNoticesData['id'] = $id;
                    $ElasticSearchSave = insertDocument('COMMUNICATION','UPDATE',$letterAndNoticesData,$this->companyConnection);

                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The Record updated successfully');
                }
            }
        }catch(Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getAllExistingLetter()
    {
        try{
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT letter_type FROM letters_notices_template WHERE id ='$id'");


            $settings1 = $query->fetch();
            $letter_type = $settings1['letter_type'];

            $query1 = $this->companyConnection->query("SELECT letter_name FROM letters_notices_template WHERE letter_type ='$letter_type' ORDER BY letter_name ASC");
            $settings = $query1->fetchAll();

            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function appendExistingLetter()
    {
        try{
            $id = $_POST['valueDdl'];
            $query = $this->companyConnection->query("SELECT * FROM letters_notices_template WHERE id =".$id);


            $settings = $query->fetch();

            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getExistingLetter()
    {
        try{
            $letterType = $_POST['letterType'];

            $query = $this->companyConnection->query("SELECT * FROM letters_notices_template WHERE letter_type =".$letterType);


            $settings = $query->fetchAll();

            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * function to soft delete Letter and Notices data
     * @return array
     */
    public function deleteLettersNotices(){
        try {
            $id = $_POST['id'];
            $data = [];
            $data['deleted_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE letters_notices_template SET ".$sqlData['columnsValuesPair']." WHERE id=".$id;
            $stmt = $this->companyConnection->prepare($query);
            $exe = $stmt->execute($sqlData['data']);
            if($exe){
                $lettersNotices = array('code' => 200, 'status' => 'success','message' => 'Record deleted successfully.');
            } else {
                $lettersNotices = array('code' => 500, 'status' => 'error','message' => 'Internal Server Error!');
            }
            return $lettersNotices;
        } catch (PDOException $e) {
            return array('code' => 500, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    public function getPropertyList()
    {
        try{
            $query = $this->companyConnection->query("SELECT * FROM general_property");

            $settings = $query->fetchAll();

            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getHtmlPdfConverter()
    {
        try{
            $tenant_id = $_POST['tenant_id'];
            $template_id = $_POST['template_id'];
            $query = $this->companyConnection->query("SELECT * FROM letters_notices_template WHERE id =".$template_id);
            $settings1 = $query->fetch();
            $htmlTemplate = $settings1['template_html'];


            $htmlTemplateNameArray = array("Bed Bug Guide.docx", "Carbon Monoxide.pdf", "Care and Maintenace of Septic System.pdf", "Earthquake Guide for Individuals with Disabilities.pdf","EPA Mold Guide.pdf","Fire Escape Planning (American Red Cross).pdf","Fire Escape Planning Grid.pdf","Fire Safety.pdf","Fire_Drill_Bookmark.pdf","Lead In Your Home Safety.docx","National Fires Fact Sheet.pdf","NCHH_Bed_Bug_Control.pdf","Save Energy.pdf","Tenant Earthquake Safety.pdf","Winterization Checklist.pdf");

            if (in_array($htmlTemplate, $htmlTemplateNameArray))
            {

                $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/upload/letterNoticesTemplate/tenantPDF/'.$htmlTemplate;
                return array('code' => 200, 'status' => 'success', 'data' => $fileUrl2,  'message' => 'Record retrieved successfully');

            }
            else
            {

                $query1 = "select u.id as userid,u.salutation as usersalutation, u.name,u.phone_number, u.first_name,u.last_name,u.email as useremail,u.ssn_sin_id as userssn_sin_id,u.middle_name,u.city as userCity,u.country as userCountry,u.zipcode as userZipcode,u.state as userState,u.company_name ,u.address1 as useraddress1,u.address2 as useraddress2,u.address3 as useraddress3,tph.phone_type,tph.carrier,tph.phone_number,td.email1,tld.id,tld.move_in,tld.move_out,tld.start_date,tld.term,tld.end_date,tld.days_remaining,tld.notice_period,tld.notice_date,tld.rent_due_day,tld.rent_amount,tld.security_deposite,tld.next_rent_incr,tld.flat_perc,tld.amount_incr,tld.tenure,tld.id,tld.cam_amount,tld.security_deposite,tld.balance,ud.unit_no,ud.unit_prefix,CONCAT( ud.unit_prefix,'-', ud.unit_no ) AS unitName, tp.property_id ,tc.grace_period,tc.fee,gp.property_name,gp.address1,gp.address2,gp.address3,gp.address4,gp.city,gp.state,gp.country,gp.zipcode,gp.owner_id,tad.first_name as tenantAddFirst,tad.last_name as tenantAddLast,bd.building_name,pt.type as petType,pt.name as petName,pt.weight as petWeight,pt.age as petAge,pt.note as petNote,pt.gender as petGender from users u JOIN tenant_details td ON u.id=td.user_id Join tenant_property tp ON u.id=tp.user_id join tenant_phone tph ON u.id=tph.user_id JOIN tenant_lease_details tld on u.id=tld.user_id JOIN unit_details ud on tp.unit_id=ud.id LEFT JOIN tenant_charge tc ON u.id=tc.user_id JOIN building_detail bd on tp.building_id=bd.id JOIN general_property gp on tp.property_id=gp.id left JOIN tenant_pet pt on td.user_id=pt.user_id LEFT JOIN tenant_additional_details tad on u.id=tad.user_id   where u.user_type='2' AND u.record_status='0' AND u.id ='$tenant_id'";
                $data = $this->companyConnection->query($query1)->fetch();
                $user_id = $data['userid'];

                $ownerIds = $data['owner_id'];


                if ($ownerIds != '') {

                    $ownerUnserialData = unserialize($ownerIds);
                    if(!empty($ownerUnserialData)) {
                        $ownerId = $ownerUnserialData[0];
                        if($ownerId != ''){
                            $ownerDetails = $this->companyConnection->query("select id as owner_id,name as ownerName,city as ownerCity,company_name as ownerCompanyName, email as ownerEmail, phone_number as OwnerNumber,address1,address2,address3 from users u where u.user_type='4' AND id ='$ownerId'");
                            $settings = $ownerDetails->fetch();
                            $ownerName = $settings['ownerName'];
                            $ownerEmail = $settings['ownerEmail'];
                            $ownerCity = $settings['ownerCity'];
                            $ownerNumber = $settings['OwnerNumber'];
                            $ownerPhone = $ownerNumber;


                            $ownerAddress = '';

                            $address1 = !empty($settings['address1'])?$settings['address1']:'';
                            $address2 = !empty($settings['address2'])?$settings['address2']:'';
                            $address3 = !empty($settings['address3'])?$settings['address3']:'';

                            $ownerAddress =  $address1.','. $address2.',' .$address3;


                        }
                        }
                    }else{
                    $ownerPhone = '';
                    $ownerAddress = '';
                    $ownerName = '';
                    $ownerEmail = '';
                    $ownerAddress = '';
                    $ownerCity = '';
                }


                $tenantvehicle = $this->companyConnection->query("select tv.type,tv.make,tv.license,tv.year,tv.color from tenant_vehicles tv where tv.user_id ='$user_id'");
                $settingvehicle = $tenantvehicle->fetchAll();
                $vehicle_type = [];
                $vehicle_make = [];
                $vehicle_color = [];
                $vehicle_license = [];
                $vehicle_year = [];
                if(is_array($settingvehicle)){
                    foreach($settingvehicle as $key => $value) {
                        array_push($vehicle_type, $value['type']);
                        array_push($vehicle_make, $value['make']);
                        array_push($vehicle_license, $value['license']);
                        array_push($vehicle_year, $value['year']);
                        array_push($vehicle_color, $value['color']);
                    }
                    $vehicleType =  implode(',',$vehicle_type);
                    $vehicleMake = implode(',',$vehicle_make);
                    $vehicleLicense = implode(',',$vehicle_license);
                    $vehicleYear = implode(',',$vehicle_year);
                    $vehicleColor = implode(',',$vehicle_color);
                }else{
                    $vehicleType =  $settingvehicle["license"];
                    $vehicleMake = $settingvehicle["type"];
                    $vehicleLicense = $settingvehicle["make"];
                    $vehicleYear = $settingvehicle["year"];
                    $vehicleColor = $settingvehicle["color"];
                }

                $tenantguarantor = $this->companyConnection->query("select tg.first_name,tg.last_name from tenant_guarantor tg where tg.user_id ='$user_id'");
                $settingguarantor = $tenantguarantor->fetchAll();

                $guarantor_type = [];
                if(is_array($settingguarantor)){
                    foreach($settingguarantor as $key => $value) {
                        array_push($guarantor_type, $value['first_name']." ".$value['last_name']);
                    }
                    $tenantguarantorName =  implode(',',$guarantor_type);


                }else{
                    $tenantguarantor =  "";
                }




                $petGender = $data['petGender'];
                $gender_value = '';
                if($petGender == "1"){
                    $gender_value = "Male";
                }else if($petGender == "2"){
                    $gender_value = "Female";
                }else if($petGender == "3"){
                    $gender_value = "Other";
                }
                //return  $gender_value;



                $currentDate =date('Y-m-d');


                $propertyAddress = '';
                $address1 = !empty($data['address1'])?$data['address1']:'';
                $address2 = !empty($data['address2'])?$data['address2']:'';
                $address3 = !empty($data['address3'])?$data['address3']:'';


                if ($address1 != '' && $address2 != '' && $address3 != '') {
                    $propertyAddress =  $address1.','. $address2.',' .$address3;
                }


                $userAddress = '';
                $useraddress1 = !empty($data['$useraddress1'])?$data['$useraddress1']:'';
                $useraddress2 = !empty($data['$useraddress2'])?$data['$useraddress2']:'';
                $useraddress3 = !empty($data['$useraddress3'])?$data['$useraddress3']:'';


                if ($useraddress1 != '' && $useraddress2 != '' && $useraddress3 != '') {
                    $userAddress =  $useraddress1.','. $useraddress2.',' .$useraddress3;
                }



                // reference the Dompdf namespace
                $replaceText = str_replace("[RentersName]",$data['first_name']." ".$data['last_name'],$htmlTemplate);
                $replaceText = str_replace("[Company_Name]",$data['company_name'],$replaceText);
                $replaceText = str_replace("Company_Name",$data['company_name'],$replaceText);
                $replaceText = str_replace("[Current_Date]",date('Y-m-d H:i:s'),$replaceText);
                $replaceText = str_replace("[Company_City]",$data['city'],$replaceText);
                $replaceText = str_replace("[Tenant_Name]",$data['first_name']." ".$data['last_name'],$replaceText);
                $replaceText = str_replace("[PropertyName]",$data['property_name'],$replaceText);
                $replaceText = str_replace("[Property_Address]",$propertyAddress,$replaceText);
                $replaceText = str_replace("[Property_City]",$data['city'],$replaceText);
                $replaceText = str_replace("Property_City",$data['city'],$replaceText);
                $replaceText = str_replace("[Property_State]",$data['state'],$replaceText);
                $replaceText = str_replace("Property_State",$data['state'],$replaceText);
                $replaceText = str_replace("[Property_Zip]",$data['zipcode'],$replaceText);
                $replaceText = str_replace("Property_Zip",$data['zipcode'],$replaceText);
                $replaceText = str_replace("[Property_Country]",$data['country'],$replaceText);
                $replaceText = str_replace("Property_Country",$data['country'],$replaceText);
                $replaceText = str_replace("[Unit_Name]",$data['unitName'],$replaceText);
                $replaceText = str_replace("[TenantLeaseId]",$data['id'],$replaceText);
                $replaceText = str_replace("[LeaseStartDate]",$data['start_date'],$replaceText);
                $replaceText = str_replace("[LeaseEndDate]",$data['end_date'],$replaceText);
                $replaceText = str_replace("[Tenant_Name]",$data['first_name']." ".$data['last_name'],$replaceText);
                $replaceText = str_replace("[Company_Name]",$data['company_name'],$replaceText);
                $replaceText = str_replace("[Company_Address]",$userAddress,$replaceText);
                $replaceText = str_replace("[Company_City]",$data['userCity'],$replaceText);
                $replaceText = str_replace("[Company_State]",$data['userState'],$replaceText);
                $replaceText = str_replace("[Company_Country]",$data['userCountry'],$replaceText);
                $replaceText = str_replace("[Company_Zip]",$data['userZipcode'],$replaceText);
                $replaceText = str_replace("[Company_Number]",$data['first_name']." ".$data['last_name'],$replaceText);

                $replaceText = str_replace("[LandLord_Number]",$ownerPhone,$replaceText);
                $replaceText = str_replace("Landlord_Number",$ownerPhone,$replaceText);

                $replaceText = str_replace("[Pet_Type]",$data['petType'],$replaceText);
                $replaceText = str_replace("[Pet_Weight]",$data['petWeight'],$replaceText);
                $replaceText = str_replace("[Pet_Name]",$data['petName'],$replaceText);
                $replaceText = str_replace("[Pet_Breed]",$data['petType'],$replaceText);
                $replaceText = str_replace("[Pet_Age]",$data['petAge'],$replaceText);
                $replaceText = str_replace("[Pet_Sex]",$gender_value,$replaceText);
                $replaceText = str_replace("[Pet_Note]",$data['petNote'],$replaceText);

                $replaceText = str_replace("[LandLord_NameTemp]",$ownerName,$replaceText);
                $replaceText = str_replace("[LandLord_Name]",$ownerName,$replaceText);
                $replaceText = str_replace("[Occupant_NameTemp]",$data['tenantAddFirst']." ".$data['tenantAddLast'],$replaceText);
                $replaceText = str_replace("[GuarantorName]",$tenantguarantorName,$replaceText);
                $replaceText = str_replace("[Occupant_Name]",$data['tenantAddFirst']." ".$data['tenantAddLast'],$replaceText);

                $replaceText = str_replace("[Owner_Name]",$ownerName,$replaceText);
                $replaceText = str_replace("[Owner_Email]",$ownerEmail,$replaceText);
                $replaceText = str_replace("[Owner_Number]",$ownerPhone,$replaceText);
                $replaceText = str_replace("[Owner_Address]",$ownerAddress,$replaceText);
                $replaceText = str_replace("[Owner_City]",$ownerCity,$replaceText);
                $replaceText = str_replace("[CurrentDate]",$currentDate,$replaceText);

                $replaceText = str_replace("[Vehicle_Name]",$vehicleType,$replaceText);
                $replaceText = str_replace("[Vehicle_Color]",$vehicleMake,$replaceText);
                $replaceText = str_replace("[Vehicle_Plate]",$vehicleLicense,$replaceText);
                $replaceText = str_replace("[Vehicle_Make]",$vehicleYear,$replaceText);
                $replaceText = str_replace("[Vehicle_Year]",$vehicleColor,$replaceText);


                // instantiate and use the dompdf class
                $dompdf = new Dompdf();
                $dompdf->loadHtml($replaceText);


                // (Optional) Setup the paper size and orientation
                $dompdf->setPaper('A4', 'landscape');

                // Render the HTML as PDF
                $dompdf->render();
                $fileUrl = COMPANY_DIRECTORY_URL . '/upload/letterNoticesTemplate/tenant_letterNoticesTemplatePDF-'.$template_id.'.pdf';

                $output = $dompdf->output();
                file_put_contents($fileUrl, $output);
                chmod($fileUrl, 0777);

                /*$fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/upload/letterNoticesTemplate/tenant_letterNoticesTemplatePDF-'.$template_id.'.pdf';
                return array('code' => 200, 'status' => 'success', 'data' => $fileUrl2,  'message' => 'Record retrieved successfully');*/


                $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/upload/letterNoticesTemplate/tenant_letterNoticesTemplatePDF-'.$template_id.'.pdf';
                return array('code' => 200, 'status' => 'success', 'data' => $fileUrl2,  'message' => 'Record retrieved successfully');
            }
            // return [$fileUrl];
        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }


    public function getTemplateDataEmail2(){
        try{
            $templateId = $_POST['searchTemplateId'];
            $query = $this->companyConnection->query("SELECT template_html,letter_name FROM letters_notices_template WHERE id =".$templateId);
            $settings1 = $query->fetch();
            $title = $settings1['letter_name'];
            $tenantTemplate_ids = $_POST['tenantTemplate_ids'];
            $currUser_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $links = [];
            $curlRes1 = [];
            $curlRes2 = [];
            foreach ($tenantTemplate_ids as $userId){
                $query = $this->companyConnection->query("SELECT id,email,name FROM users WHERE id =".$userId)->fetch();
                $email = $query['email'];
                $name = $query['name'];
                $link = 'http://'.$_SERVER['HTTP_HOST']."/EsignatureUser/esignView?templateId=$templateId&userId=$userId&CuserId=$currUser_id";
                $link1 = 'http://'.$_SERVER['HTTP_HOST']."/EsignatureUser/esignView?templateId=$templateId&userId=1&CuserId=$currUser_id";
                $links[] = 'http://'.$_SERVER['HTTP_HOST']."/EsignatureUser/esignView?templateId=$templateId&userId=$userId&CuserId=$currUser_id";
                $links[] = 'http://'.$_SERVER['HTTP_HOST']."/EsignatureUser/esignView?templateId=$templateId&userId=1&CuserId=$currUser_id";
                $curlRes1[] = $this->sendMailEmail2($link, $email,$name,$userId,$title);
                $curlRes2[] = $this->sendMailEmail2($link1, $_SESSION[SESSION_DOMAIN]['email'],$_SESSION[SESSION_DOMAIN]['name'],$currUser_id,$title);
            }
            return['status'=>'success','code'=>200,'data' =>$links, "curlRes2" => $curlRes2, "curlRes1" => $curlRes1];
        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    /**
     *
     * @param null $userId
     * @return array
     */
    public function sendMailEmail2($link, $email,$username,$userId,$title){
        try{

            $query = $this->companyConnection->query("SELECT company_name,phone_number FROM users WHERE id ='1'")->fetch();
            $compName = $query['company_name'];
            $phone_number = $query['phone_number'];
            $urlSite = SITE_URL.'/company/images/logo.png';

            $server_name = 'https://'.$_SERVER['HTTP_HOST'];
            $html = '<table align="center" border="0" width="640" style="border:1px solid #ddd;" cellpadding="0" cellspacing="0"><tbody><tr style="background-color:#00b0f0;height:20px;"><td></td>
                        </tr><tr style="border-collapse:collapse;background-color:#fff;"><td align="center" style="font-family:\'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;">
                        <div style="margin-top:1px;margin-bottom:10px;">
                         <img src="'.$urlSite.'" height="60" width="160"></img>
</div>
                        </td>
                        </tr><tr style="background-color:#00b0f0;height:20px;"><td></td>
                        </tr><tr><td style="padding:20px;">
                        <p style="font-family:arial;">Dear '.$username.',</p>
                        <p style="font-family:arial;">Please click on the link below to Electronically sign the document sent to you :-</p>
                        <p style="font-family:arial;"><a href="'.$link.'" rel="nofollow">Click here...</a></p>
                        <p style="font-family:arial;">Thanks,</p>
                        <p style="font-family:arial;">Team '.$compName.'</p>
                        </td>
                        </tr><tr><td align="center" bgcolor="#05a0e4" style="padding:10px;font-size:12px;color:#ffffff;font-weight:bold;">
                        '.$compName.'  <a style="color:#fff;text-decoration:none;">
                        &nbsp; ●'.$phone_number.'</a> </td>
                        </tr></tbody></table>';

            $request['action']  = 'SendMailPhp';
            $request['to'][]    = $email;
            $request['subject'] = $title;
            $request['message'] = $html;
            $request['portal']  = '1';
            $curlRes = curlRequest($request);
            return ['status'=>'success','code'=>200,'data'=>$request,'message' => 'Email send successfully',"curlRes" =>$curlRes];
        }catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }


    public function getTemplateDataEmail(){
        try{
            $templateId = $_POST['searchTemplateId'];
            $tenantTemplate_ids = $_POST['tenantTemplate_ids'];
            $ids = join("','",$tenantTemplate_ids);

            $query = $this->companyConnection->query("SELECT template_html,letter_name,is_editable FROM letters_notices_template WHERE id =".$templateId);
            $settings1 = $query->fetch();

            /*general manager*/
            $query1 = $this->companyConnection->query("select gp.manager_id, u.email, u.id from users u JOIN  tenant_property tp ON u.id=tp.user_id JOIN general_property gp on tp.property_id=gp.id where u.user_type='2' AND u.record_status='0' AND u.id IN ('$ids')");

            /*            $query1->execute($tenantTemplate_ids);*/
            $data = $query1->fetchAll();
            $countdata = count($data);

            $newArray = [];
            for($i = 0; $i < $countdata; $i++){
                $temArray = [];
                $temArray1 = [];
                $tenant_id = $data[$i]['id'];
                $managers_id = unserialize($data[$i]['manager_id']);
                //$temArray['tenant_id'] = $tenant_id;
                /*$temArray['tenant_id'] = $this->getEmailUserData($this->companyConnection, $tenant_id, 'tenant');
                $temArray['managers_id'] = $this->getEmailUserData($this->companyConnection,$managers_id,'manager');*/
                $temArray[] = $this->getEmailUserData($this->companyConnection, $tenant_id, 'tenant');
                $temArray[] = $this->getEmailUserData($this->companyConnection,$managers_id,'manager');
                $temArray1['tenant'] =$temArray;
                $newArray[] = $temArray1;
            }


            $returnArray = [];
            foreach ($newArray as $key=>$value){
                if(is_array($value['tenant'])){
                    $allArray = [];
                    foreach ($value['tenant'] as $key1=> $value1) {
                        if (!is_array($value1)){
                            $allArray[] = $value1;
                        }else{
                            foreach ($value1 as $newvalue1){
                                $allArray[] = $newvalue1;
                            }
                        }
                    }
                    $returnArray[] = $allArray;
                }
            }

            $emailRes = [];

            foreach ($returnArray as $key => $value){
                $valM = $value;
                $emailRes[] = $this->sendMail($valM,$settings1,$ids);
            }




            return['status'=>'success','code'=>200, 'emailRes' => $emailRes];
        }catch (Exception $exception){
            echo $exception->getMessage();
            die;
        }
    }

    public  function getEmailUserData($connection,$id, $type) {
        try {
            if($type == 'tenant') {
                $record = $connection->query("SELECT `email` FROM  users WHERE id=" . $id)->fetch();
                $data = $record['email'];
                return $data;
            } elseif ($type == 'manager'){
                $data=[];
                $idManage = $id;
                //$countdata = count($idManage);
                if(is_array($idManage)) {
                    foreach ($idManage as $value) {
                        $record = $connection->query("SELECT `email` FROM  users WHERE id=" . $value)->fetch();
                        $data[] = $record['email'];
                    }
                }

                return $data;
            }else {
                return '';
            }

        } catch (PDOException $e) {
            echo $e->getMessage();
        }


    }

    /**
     *
     * @param null $userId
     * @return array
     */
    public function sendMail($valM,$settings1,$ids)
    {

        try{
            $server_name = 'https://'.$_SERVER['HTTP_HOST'];

            /*$userId = (isset($userId) ? $userId : $_REQUEST['id']);
            $user_details = $this->companyConnection->query("SELECT * FROM users where id=".$userId)->fetch();
            $user_name = userName($user_details['id'], $this->companyConnection);*/
            /*$query1 = "select u.id as userid,u.salutation as usersalutation,u.first_name,u.last_name,u.email as useremail,u.ssn_sin_id as userssn_sin_id,u.middle_name,u.city as userCity,u.country as userCountry,u.zipcode as userZipcode,u.state as userState,u.company_name ,u.address1 as useraddress1,u.address2 as useraddress2,u.address3 as useraddress3,tph.phone_type,tph.carrier,tph.phone_number,td.email1,tld.id,tld.move_in,tld.move_out,tld.start_date,tld.term,tld.end_date,tld.days_remaining,tld.notice_period,tld.notice_date,tld.rent_due_day,tld.rent_amount,tld.security_deposite,tld.next_rent_incr,tld.flat_perc,tld.amount_incr,tld.tenure,tld.id,tld.cam_amount,tld.security_deposite,tld.balance,ud.unit_no,ud.unit_prefix,CONCAT( ud.unit_prefix,'-', ud.unit_no ) AS unitName, tp.property_id ,tc.grace_period,tc.fee,gp.property_name,gp.address1,gp.address2,gp.address3,gp.address4,gp.city,gp.state,gp.country,gp.zipcode,gp.owner_id,bd.building_name,pt.type as petType,pt.name as petName,pt.weight as petWeight,pt.age as petAge,pt.note as petNote,pt.gender as petGender from users u JOIN tenant_details td ON u.id=td.user_id Join tenant_property tp ON u.id=tp.user_id join tenant_phone tph ON u.id=tph.user_id JOIN tenant_lease_details tld on u.id=tld.user_id JOIN unit_details ud on tp.unit_id=ud.id LEFT JOIN tenant_charge tc ON u.id=tc.user_id JOIN building_detail bd on tp.building_id=bd.id JOIN general_property gp on tp.property_id=gp.id left JOIN tenant_pet pt on td.user_id=pt.user_id where u.user_type='2' AND u.record_status='0' AND u.id IN ('$ids')";*/
            $query1 = "select u.id as userid,u.salutation as usersalutation,u.name,u.phone_number, u.first_name,u.last_name,u.email as useremail,u.ssn_sin_id as userssn_sin_id,u.middle_name,u.city as userCity,u.country as userCountry,u.zipcode as userZipcode,u.state as userState,u.company_name ,u.address1 as useraddress1,u.address2 as useraddress2,u.address3 as useraddress3,tph.phone_type,tph.carrier,tph.phone_number,td.email1,tld.id,tld.move_in,tld.move_out,tld.start_date,tld.term,tld.end_date,tld.days_remaining,tld.notice_period,tld.notice_date,tld.rent_due_day,tld.rent_amount,tld.security_deposite,tld.next_rent_incr,tld.flat_perc,tld.amount_incr,tld.tenure,tld.id,tld.cam_amount,tld.security_deposite,tld.balance,ud.unit_no,ud.unit_prefix,CONCAT( ud.unit_prefix,'-', ud.unit_no ) AS unitName, tp.property_id ,tc.grace_period,tc.fee,gp.property_name,gp.address1,gp.address2,gp.address3,gp.address4,gp.city,gp.state,gp.country,gp.zipcode,gp.owner_id,tad.first_name as tenantAddFirst,tad.last_name as tenantAddLast,bd.building_name,pt.type as petType,pt.name as petName,pt.weight as petWeight,pt.age as petAge,pt.note as petNote,pt.gender as petGender from users u JOIN tenant_details td ON u.id=td.user_id Join tenant_property tp ON u.id=tp.user_id join tenant_phone tph ON u.id=tph.user_id JOIN tenant_lease_details tld on u.id=tld.user_id JOIN unit_details ud on tp.unit_id=ud.id LEFT JOIN tenant_charge tc ON u.id=tc.user_id JOIN building_detail bd on tp.building_id=bd.id JOIN general_property gp on tp.property_id=gp.id left JOIN tenant_pet pt on td.user_id=pt.user_id LEFT JOIN tenant_additional_details tad on u.id=tad.user_id where u.user_type='2' AND u.record_status='0' AND u.id IN ('$ids')";
            $data = $this->companyConnection->query($query1)->fetch();
            $user_id = $data['userid'];
            $ownerIds = $data['owner_id'];
            /*if ($ownerIds != ''){
                $ownerUnserialData = unserialize($data['owner_id']);
                $ownerId =$ownerUnserialData[0];
                $ownerDetails = "Select id as owner_id,name as ownerName,city as ownerCity,company_name as ownerCompanyName, email as ownerEmail, phone_number as OwnerNumber,address1,address2,address3 from users u where u.user_type='4' AND id =".$ownerId;
                $ownerDetails1 = $this->companyConnection->query($ownerDetails);
                $settings = $ownerDetails1->fetch();
                $ownerNumber = $settings['OwnerNumber'];
                $ownerPhone = $ownerNumber;

            }else{
                $ownerPhone = '';
                $settings['ownerName'] = '';
                $settings['ownerEmail'] = '';
                $settings['ownerAddress'] = '';
                $settings['ownerCity'] = '';
            }*/

            if ($ownerIds != '') {

                $ownerUnserialData = unserialize($ownerIds);
                if(!empty($ownerUnserialData)) {
                    $ownerId = $ownerUnserialData[0];
                    if($ownerId != ''){
                        $ownerDetails = $this->companyConnection->query("select id as owner_id,name as ownerName,city as ownerCity,company_name as ownerCompanyName, email as ownerEmail, phone_number as OwnerNumber,address1,address2,address3 from users u where u.user_type='4' AND id ='$ownerId'");
                        $settings = $ownerDetails->fetch();
                        $ownerName = $settings['ownerName'];
                        $ownerEmail = $settings['ownerEmail'];
                        $ownerCity = $settings['ownerCity'];
                        $ownerNumber = $settings['OwnerNumber'];
                        $ownerPhone = $ownerNumber;


                        $ownerAddress = '';

                        $address1 = !empty($settings['address1'])?$settings['address1']:'';
                        $address2 = !empty($settings['address2'])?$settings['address2']:'';
                        $address3 = !empty($settings['address3'])?$settings['address3']:'';

                        $ownerAddress =  $address1.','. $address2.',' .$address3;


                    }
                }
            }else{
                $ownerPhone = '';
                $ownerAddress = '';
                $ownerName = '';
                $ownerEmail = '';
                $ownerAddress = '';
                $ownerCity = '';
            }

            $tenantvehicle = $this->companyConnection->query("select tv.type,tv.make,tv.license,tv.year,tv.color from tenant_vehicles tv where tv.user_id ='$user_id'");
            $settingvehicle = $tenantvehicle->fetchAll();
            $vehicle_type = [];
            $vehicle_make = [];
            $vehicle_color = [];
            $vehicle_license = [];
            $vehicle_year = [];
            if(is_array($settingvehicle)){
                foreach($settingvehicle as $key => $value) {
                    array_push($vehicle_type, $value['type']);
                    array_push($vehicle_make, $value['make']);
                    array_push($vehicle_license, $value['license']);
                    array_push($vehicle_year, $value['year']);
                    array_push($vehicle_color, $value['color']);
                }
                $vehicleType =  implode(',',$vehicle_type);
                $vehicleMake = implode(',',$vehicle_make);
                $vehicleLicense = implode(',',$vehicle_license);
                $vehicleYear = implode(',',$vehicle_year);
                $vehicleColor = implode(',',$vehicle_color);
            }else{
                $vehicleType =  $settingvehicle["license"];
                $vehicleMake = $settingvehicle["type"];
                $vehicleLicense = $settingvehicle["make"];
                $vehicleYear = $settingvehicle["year"];
                $vehicleColor = $settingvehicle["color"];
            }


            $tenantguarantor = $this->companyConnection->query("select tg.first_name,tg.last_name from tenant_guarantor tg where tg.user_id ='$user_id'");
            $settingguarantor = $tenantguarantor->fetchAll();

            $guarantor_type = [];
            if(is_array($settingguarantor)){
                foreach($settingguarantor as $key => $value) {
                    array_push($guarantor_type, $value['first_name']." ".$value['last_name']);
                }
                $tenantguarantorName =  implode(',',$guarantor_type);


            }else{
                $tenantguarantor =  "";
            }




            $petGender = $data['petGender'];
            $gender_value = '';
            if($petGender == "1"){
                $gender_value = "Male";
            }else if($petGender == "2"){
                $gender_value = "Female";
            }else if($petGender == "3"){
                $gender_value = "Other";
            }


            $currentDate =date('Y-m-d');


            $propertyAddress = '';
            $address1 = !empty($data['address1'])?$data['address1']:'';
            $address2 = !empty($data['address2'])?$data['address2']:'';
            $address3 = !empty($data['address3'])?$data['address3']:'';


            if ($address1 != '' && $address2 != '' && $address3 != '') {
                $propertyAddress =  $address1.','. $address2.',' .$address3;
            }

            $userAddress = '';
            $useraddress1 = !empty($data['$useraddress1'])?$data['$useraddress1']:'';
            $useraddress2 = !empty($data['$useraddress2'])?$data['$useraddress2']:'';
            $useraddress3 = !empty($data['$useraddress3'])?$data['$useraddress3']:'';


            if ($useraddress1 != '' && $useraddress2 != '' && $useraddress3 != '') {
                $userAddress =  $useraddress1.','. $useraddress2.',' .$useraddress3;
            }


            $pdfDoc = $settings1['is_editable'];
            $htmlTemplate = str_replace(" ","_",$settings1['template_html']);


            $htmlTemplateNameArray = array("Bed_Bug_Guide.docx", "Carbon_Monoxide.pdf", "Care_and_Maintenace_of Septic_System.pdf", "Earthquake_Guide_for_Individuals_with_Disabilities.pdf","EPA_Mold_Guide.pdf","Fire_Escape_Planning_(American_Red_Cross).pdf","Fire_Escape_Planning_Grid.pdf","Fire_Safety.pdf","Fire_Drill_Bookmark.pdf","Lead_In_Your_Home_Safety.docx","National_Fires_Fact Sheet.pdf","NCHH_Bed_Bug_Control.pdf","Save_Energy.pdf","Tenant_Earthquake_Safety.pdf","Winterization_Checklist.pdf");

            $filePath= '';
            if (in_array($htmlTemplate, $htmlTemplateNameArray))
            {

                $companyUrl = SITE_URL."company/upload/letterNoticesTemplate/tenantPDFNew/";
                $filePath = $companyUrl.$htmlTemplate;

            }

            if($pdfDoc == '2'){
                $request["attachments"][] = $filePath;
                $body = 'PFA';
                $request['to']    = $valM;
                $request['action']  = 'SendMailPhp';
                $request['subject'] = $settings1['letter_name'];;
                $request['message'] = $body;
            }else{

                $body = $settings1['template_html'];
                $body = str_replace("[RentersName]",$data['first_name']." ".$data['last_name'],$body);
                $body = str_replace("[Company_Name]",$data['company_name'],$body);
                $body = str_replace("[Current_Date]",date('Y-m-d H:i:s'),$body);
                $body = str_replace("[Company_City]",$data['city'],$body);
                $body = str_replace("[Tenant_Name]",$data['first_name']." ".$data['last_name'],$body);

                $body = str_replace("[PropertyName]",$data['property_name'],$body);
                $body = str_replace("[Property_Address]",$propertyAddress,$body);
                $body = str_replace("[Property_City]",$data['city'],$body);
                $body = str_replace("Property_City",$data['city'],$body);
                $body = str_replace("[Property_State]",$data['state'],$body);
                $body = str_replace("Property_State",$data['state'],$body);
                $body = str_replace("[Property_Zip]",$data['zipcode'],$body);
                $body = str_replace("Property_Zip",$data['zipcode'],$body);
                $body = str_replace("[Property_Country]",$data['country'],$body);
                $body = str_replace("Property_Country",$data['country'],$body);
                $body = str_replace("[Unit_Name]",$data['unitName'],$body);

                $body = str_replace("[TenantLeaseId]",$data['id'],$body);
                $body = str_replace("[LeaseStartDate]",$data['start_date'],$body);
                $body = str_replace("[LeaseEndDate]",$data['end_date'],$body);
                $body = str_replace("[Tenant_Name]",$data['first_name']." ".$data['last_name'],$body);

                $body = str_replace("[Company_Name]",$data['company_name'],$body);
                $body = str_replace("[Company_Address]",$userAddress,$body);
                $body = str_replace("[Company_City]",$data['userCity'],$body);
                $body = str_replace("[Company_State]",$data['userState'],$body);
                $body = str_replace("[Company_Country]",$data['userCountry'],$body);
                $body = str_replace("[Company_Zip]",$data['userZipcode'],$body);
                $body = str_replace("[Company_Number]",$data['phone_number'],$body);


                $body = str_replace("[LandLord_Number]",$ownerPhone,$body);
                $body = str_replace("Landlord_Number",$ownerPhone,$body);

                $body = str_replace("[Pet_Type]",$data['petType'],$body);
                $body = str_replace("[Pet_Weight]",$data['petWeight'],$body);
                $body = str_replace("[Pet_Name]",$data['petName'],$body);
                $body = str_replace("[Pet_Breed]",$data['petType'],$body);
                $body = str_replace("[Pet_Age]",$data['petAge'],$body);
                $body = str_replace("[Pet_Sex]",$gender_value,$body);
                $body = str_replace("[Pet_Note]",$data['petNote'],$body);

                $body = str_replace("[LandLord_NameTemp]",$ownerName,$body);
                $body = str_replace("[LandLord_Name]",$ownerName,$body);
                $body = str_replace("[Occupant_NameTemp]",$data['tenantAddFirst']." ".$data['tenantAddLast'],$body);
                $body = str_replace("[GuarantorName]",$tenantguarantorName,$body);
                $body = str_replace("[Occupant_Name]",$data['tenantAddFirst']." ".$data['tenantAddLast'],$body);


                $body = str_replace("[Owner_Name]",$ownerName,$body);
                $body = str_replace("[Owner_Email]",$ownerEmail,$body);
                $body = str_replace("[Owner_Number]",$ownerPhone,$body);
                $body = str_replace("[Owner_Address]",$ownerAddress,$body);
                $body = str_replace("[Owner_City]",$ownerCity,$body);
                $body = str_replace("[CurrentDate]",$currentDate,$body);

                $body = str_replace("[Vehicle_Name]",$vehicleType,$body);
                $body = str_replace("[Vehicle_Color]",$vehicleMake,$body);
                $body = str_replace("[Vehicle_Plate]",$vehicleLicense,$body);
                $body = str_replace("[Vehicle_Make]",$vehicleYear,$body);
                $body = str_replace("[Vehicle_Year]",$vehicleColor,$body);


                $request['action']  = 'SendMailPhp';
                $request['to']    = $valM;
                $request['subject'] =$settings1['letter_name'];
                $request['message'] = $body;
            }


            $request['portal']  = '1';
            $curlData =  curlRequest($request);
            return ['status'=>'success','code'=>200,'data'=>$request,'message' => 'Email send successfully', 'curlData' => $curlData];
        }catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }


    public function getOwnerList()
    {
        try{
            $query = $this->companyConnection->query("SELECT * FROM users where user_type='4'");
            $settings = $query->fetchAll();

            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getOwnerPropertyList(){
        try{
            $id = $_POST['id'];
            $query = $this->companyConnection->query("Select gp.property_name,gp.id FROM owner_property_owned as ow LEFT JOIN general_property as gp ON ow.property_id=gp.id WHERE ow.user_id = '$id'");

            $stmt = $query->fetchAll();

            return array('code' => 200, 'status' => 'success', 'data' => $stmt, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo  $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function saveSignatureImage(){
        try{
            $temp = $_POST['tempId'];
            $uId = $_POST['id'];
            $newSql = "SELECT * FROM esign_signatures where user_id='$uId' AND temp_id ='$temp'";
            $userSig =  $this->companyConnection->query($newSql)->fetch();

            if ($userSig != ""){

                if (isset($_POST['inputVal']) && $_POST['inputVal'] == "s"){
                    $data1['signature_image'] = $_POST['img'];
                    $data2['signature_image'] = $data1['signature_image'];
                }else{
                    $data1['signature_text'] = $userSig['signature_text'];
                    $data2['signature_text'] = $data1['signature_text'];
                    $data1['style'] = $userSig['style'];
                    $data2['style'] = $data1['style'];
                }
                if (isset($_POST['inputVal']) && $_POST['inputVal'] == "t"){
                    $data1['signature_text'] = $_POST['fontText'];
                    $data2['signature_text'] = $data1['signature_text'];
                    $data1['style'] = $_POST['styleAttr'];
                    $data2['style'] = $data1['style'];
                }else{
                    $data1['signature_image'] = $userSig['signature_image'];
                    $data2['signature_image'] = $data1['signature_image'];
                }
                $data1['userType_id'] = '0';
                $data1['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data1);
                $query = "UPDATE esign_signatures SET " . $sqlData['columnsValuesPair'] . " where user_id=".$_POST['id'];

                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $id = $this->companyConnection->lastInsertId();

                $data2['user_id'] = $_POST['id'];


                $data2['status'] = 1;
                $data2['temp_id'] = $_POST['tempId'];
                //$data2['usertype'] = $_POST['dataType'];
                $data2['record_status'] = 0;

                /*if (isset($_POST['inputVal']) && $_POST['inputVal'] == "t") {
                    $data2['style'] = $data1['style'];
                }else{
                    $data2['style'] = "";
                }*/
                $data2['updated_at'] = date('Y-m-d H:i:s');

                return ['code'=>200, 'status'=>'success', 'data'=>$data2, 'id'=>$id, 'message'=> 'Signature updated successfully.'];
            }else{
                $data1['user_id'] = $_POST['id'];
                $data1['status'] = 1;

                if (isset($_POST['inputVal']) && $_POST['inputVal'] == "s"){
                    $data1['signature_image'] = $_POST['img'];
                    $data1['signature_text'] = "";
                    $data1['style'] = "";
                }
                if (isset($_POST['inputVal']) && $_POST['inputVal'] == "t"){
                    $data1['signature_image'] = "";
                    $data1['signature_text'] = $_POST['fontText'];
                    $data1['style'] = $_POST['styleAttr'];
                }

                $data1['temp_id'] = $_POST['tempId'];
                $data1['userType_id'] = '0';
                $data1['record_status'] = 0;
                $data1['created_at'] = date('Y-m-d H:i:s');
                $data1['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO `esign_signatures` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);
                $id = $this->companyConnection->lastInsertId();

                return ['code'=>200, 'status'=>'success', 'data'=>$data1, 'id'=>$id, 'message'=> 'Signature saved successfully.'];
            }
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage(), 'message'=> 'Signature not saved successfully.'];
            printErrorLog($exception->getMessage());
        }
    }

    function updateColmnHtmlData(){
        try{
            $tempId = $_POST['tempId'];
            $user_id = $_POST['user_id'];
            $htmlTemplate = $_POST['htmlTemplate'];
            $templateHtml = htmlspecialchars($htmlTemplate);
            $currLoggUserid = $_POST['currLoggUserid'];
            $newSql = "SELECT * FROM esign_signatures where user_id='$user_id' AND temp_id ='$tempId'";
            $getDataSig =  $this->companyConnection->query($newSql)->fetch();

            if ($getDataSig != ""){
                $data1['temp_id'] = $tempId;
                $data1['user_id'] = $user_id;
                $data['esign_template'] = $templateHtml;
                $data['currentLoggedin_id'] = $currLoggUserid;
                $data['userType_id']= '0';
                $data['record_status'] = '0';
                $data['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE `esign_signatures` SET " . $sqlData['columnsValuesPair'] . " where user_id=".$_POST['user_id'];
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $id = $this->companyConnection->lastInsertId();

                return ['code'=>200, 'status'=>'success', 'data'=>$data, 'id'=>$id, 'message'=> 'Template saved successfully.'];
            }else{


                $data1['temp_id'] = $tempId;
                $data1['user_id'] = $user_id;
                $data1['esign_template'] = $templateHtml;
                $data1['currentLoggedin_id'] = "";
                $data1['userType_id']= $currLoggUserid;
                $data1['record_status'] = 0;
                $data1['created_at'] = date('Y-m-d H:i:s');
                $data1['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO `esign_signatures` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);
                $id = $this->companyConnection->lastInsertId();

                return ['code'=>200, 'status'=>'success', 'data'=>$data1, 'id'=>$id, 'message'=> 'Orignator Signature saved successfully.'];
            }

        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage(), 'message'=> 'Template not saved successfully.'];
            printErrorLog($exception->getMessage());
        }
    }

    function getsignatureonreload(){
        $template_id=$_POST['templateId'];
        $query = $this->companyConnection->query("SELECT signature_image,signature_text FROM esign_signatures WHERE temp_id=".$template_id." AND user_id='1'");
        $originator_data = $query->fetch();
        $query = $this->companyConnection->query("SELECT signature_image,signature_text FROM esign_signatures WHERE temp_id=".$template_id." AND user_id!='1'");
        $signer_data = $query->fetch();
        return ['code'=>200, 'status'=>'success', 'originator_data'=>$originator_data, 'signer_data'=>$signer_data, 'message'=> 'Signature fetched successfully.'];
    }
}
$lettersNoticesAjax = new lettersNoticesAjax();