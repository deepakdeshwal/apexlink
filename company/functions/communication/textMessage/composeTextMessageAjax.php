<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class composeTextMessageAjax extends DBConnection
{

    public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function saveEmail()
    {
        try {
            $response = $_REQUEST;
            $to = [];


            if (isset($response['to'])) {
                $to = explode(',', $response['to']);
                $name = explode(',', $response['to_name']);
            }

            if (count($to) > 0) {
                foreach ($to as $key => $email) {
                    $to_mail = $email.'@yopmail.com';

                    if (!filter_var($to_mail, FILTER_VALIDATE_EMAIL)) {
                        return array('code' => 503, 'status' => 'failed', 'data' => '', 'message' => 'Please enter valid email.');
                    }
                    $all_mails[] = $to_mail;
                }
            }

            $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $user_data = getDataById($this->companyConnection, 'users', $user_id);

//dd($response);
            $to_data = explode(',',$response['to']);
            $name_data = explode(',',$response['to_name']);
            $type_data = explode(',',$response['to_usertype']);




            $data['email_to'] = $all_mails;
            $data['email_subject'] = $response['subject'];
            $data['email_message'] = $response['mesgbody'];
            $data['user_id'] = $user_id;
            $data['user_type'] = $user_data['data']['user_type'];
            $data['email_from'] = $_SESSION[SESSION_DOMAIN]['email'];
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['user_name'] = $name;
            $data['type'] = 'T';
            $edit_id = $response['edit_id'];

            if ($response['mail_type'] == 'send') {

                $to = str_replace("+", "", $to);
                $request['action'] = 'SendMailPhp';
                $request['to'] = $to;
                $request['subject'] = $data['email_subject'];
                $request['message'] = $data['email_message'];
                $request['portal'] = '1';

                $curl_response = curlRequest($request);
                if (isset($curl_response)) {
                    if (isset($curl_response->error)) {
                        $error = $curl_response->error;
                        if (isset($error->to)) {
                            //   return array('code' => 503, 'status' => 'failed', 'data' => $data, 'message' => 'Please enter valid email.');
                        }
                        $data['status'] = '0';
                    }
                    if ($curl_response->code == '200') {
                        $data['status'] = '1';
                    } else {
                        $data['status'] = '0';
                    }
                } else {
                    $data['status'] = '0';
                }
                //Temporary Code
                $data['status'] = '1';
            }


            $nameValue = [];
            if ($edit_id) {

                if(isset($name_data))
                {
                    //               dd($to_data);

                    $nameValue = [];
                    foreach ($name_data as $key=>$value)
                    {
                        $nameV = explode('>',$value);
                        $nameT = explode('>',$type_data[$key]);

                        if(in_array($nameV['1'],$to_data)){
                            $nameValue[$key]['name'] = $nameV['0'];
                            $nameValue[$key]['phone'] = $nameV['1'];
                            $nameValue[$key]['type'] = $nameT['0'];
                        }
                    }

                }

                $edit_id = $_REQUEST['edit_id'];
                $data['updated_at'] = date('Y-m-d H:i:s');
                foreach ($to as $key => $mail) {

                    $data['email_to'] = $mail;
                    $data['user_name'] = $nameValue[$key]['name'];
                    $data['selected_user_type'] = $nameValue[$key]['type'];

                    if ($key == '0') {
                        $sqlData = createSqlColValPair($data);
                        $query = "UPDATE communication_email SET " . $sqlData['columnsValuesPair'] . " where id='$edit_id'";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();
                    } else {
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO communication_email (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                    }

                }


            } else {

                if(isset($name_data))
                {
                    //               dd($to_data);
                    $nameValue = [];
                    foreach ($name_data as $key=>$value)
                    {
                        $nameV = explode('>',$value);
                        $nameT = explode('>',$type_data[$key]);

                        if(in_array($nameV['1'],$to_data)){
                            $nameValue[$key]['name'] = @$nameV['0'];
                            $nameValue[$key]['phone'] = @$nameV['1'];
                            $nameValue[$key]['type'] = @$nameT['0'];
                        }
                    }

                }
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                foreach ($to as $key => $mail) {
                    $data['email_to'] = $mail;
                    $data['user_name'] = $nameValue[$key]['name'];
                    $data['selected_user_type'] = $nameValue[$key]['type'];
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO communication_email (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                }

                $lastInsertId = $this->companyConnection->lastInsertId();
            }

            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record Saved successfully.');
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public function delete()
    {
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE communication_email SET deleted_at=? WHERE id=?";
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute([$data, $id]);
            return ['status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function getComposeForView()
    {
        try {
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM communication_email WHERE id=" . $id . " AND deleted_at IS NULL");
            $data = $query->fetch();
            $data['created_at'] = dateFormatUser($data['created_at'], null, $this->companyConnection);
            return ['status' => 'success', 'code' => 200, 'data' => $data, 'message' => 'Record fetched successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function getRecievedEmail()
    {
        try {

            $user_email = $_SESSION[SESSION_DOMAIN]['email'];
            $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];

            $user_type = $this->companyConnection->query("SELECT user_type from users WHERE id='$user_id'");
            $user_type = $user_type->fetch();

            $userType = $user_type['user_type'];
            if ($_SESSION[SESSION_DOMAIN]['current_user_type'] == 'Communication_Portal') {

                $query = $this->companyConnection->query("SELECT communication_email.*,users.name FROM communication_email JOIN users On users.email = communication_email.email_from WHERE communication_email.deleted_at IS NULL and communication_email.status='1' and (communication_email.email_parent_id IS NULL OR communication_email.email_parent_id = '0') and type = 'T' ORDER BY communication_email.created_at desc");
            } else {

                $portal_type = $_SESSION[SESSION_DOMAIN]['current_user_type'];

                $portal = $_SESSION[SESSION_DOMAIN][$portal_type];

                $email = $portal['email'];
                if ($portal_type == 'Owner_Portal') {
                    $user_type = '4';
                } elseif ($portal_type == 'Vendor_Portal') {
                    $user_type = '';
                } elseif ($portal_type == 'Tenant_Portal') {
                    $user_type = '';
                }

                $query = $this->companyConnection->query("SELECT communication_email.*,users.name FROM communication_email JOIN users On users.email = communication_email.email_to WHERE email_to='$email' AND users.user_type='$user_type' AND communication_email.deleted_at IS NULL ORDER BY communication_email.created_at");
            }

            $data = $query->fetchAll();

            if (isset($data)) {
                foreach ($data as $key => $value) {
                    $data[$key]['created_at'] = dateFormatUser($value['created_at'], null, $this->companyConnection);
                }
            }

            return ['status' => 'success', 'code' => 200, 'data' => $data, 'message' => 'Record fetched successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function getSearchUsers()
    {
        $user_search = $_POST['search'];
        $user_type = $_POST['user_type'];
        $userTypeQuery = '';
        if ($user_type) {
            $userTypeQuery = 'and user_type="' . $user_type . '"';
        }
        $getUsers = $this->companyConnection->query("SELECT id,name,first_name,phone_number,user_type FROM users where (name like '%" . $user_search . "%' OR first_name like '%" . $user_search . "%' OR phone_number like '%" . $user_search . "%' OR last_name like '%" . $user_search . "%' ) " . $userTypeQuery)->fetchAll();

        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
        <th>id</th>
        <th>Name</th>
        <th>Phone</th>
        </tr>";

        if ($_POST['id_type'] == 'bccSearch') {
            $typeClass = 'getBCCEmails';
        } else if ($_POST['id_type'] == 'ccSearch') {
            $typeClass = 'getCCEmails';
        } else if ($_POST['id_type'] == 'toSearch') {
            $typeClass = 'getEmails';
        }

        foreach ($getUsers as $users) {

            $id = $users['id'];
            $name = $users['name'];
            $email = $users['phone_number'];
            $type = $users['user_type'];
            $html .= "<tr>";
            $html .= "<td><input type='checkbox' class='" . $typeClass . "' data-email='" . $email . "' data-name='" . $name.'>'.$email. "' data-utype='" . $type.'>'.$email . "'></td>";
            $html .= "<td>" . $name . "</td>";
            $html .= "<td>" . $email . "</td>";
            $html .= "</tr>";
        }

        $html .= "</table><br>";

        echo $html;
        exit;
    }

    public function getRecievedEmailByID()
    {
        try {

            $communication_id = $_REQUEST['id'];

            $query = $this->companyConnection->query("SELECT communication_email.*,users.name FROM communication_email LEFT JOIN users On users.email = communication_email.email_from WHERE communication_email.id = $communication_id OR communication_email.email_parent_id = $communication_id");
            $data = $query->fetchAll();

            if(isset($data))
            {
                foreach ($data as $key=>$value)
                {
                    $time = timeFormat($value['created_at'], null, $this->companyConnection);
                    $data[$key]['created_at'] = dateFormatUser($value['created_at'], null, $this->companyConnection).' '.$time;
                }
            }


            return ['status' => 'success', 'code' => 200, 'data' => $data, 'message' => 'Record fetched successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function replyMail()
    {
        try {

            if (empty($_REQUEST['mesgbody'])) {
                return ['status' => 'error', 'code' => 503, 'message' => 'Comment is required'];
            }

            $reply_id = $_REQUEST['reply_id'];
            $query = $this->companyConnection->query("SELECT * FROM communication_email WHERE id=" . $reply_id . " AND deleted_at IS NULL");
            $dataCommunication = $query->fetch();

            $userid = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $userQuery = $this->companyConnection->query("SELECT * FROM users WHERE id=" . $userid . " AND deleted_at IS NULL");
            $userData = $userQuery->fetch();

            $userType = $userData['user_type'];
            $data['email_parent_id']= $dataCommunication['id'];

            //$data['email_to']       = $dataCommunication['email_from'];
            $data['email_subject']  = $dataCommunication['email_subject'];
            $data['email_message']  = $_REQUEST['mesgbody'];
            $data['user_id']        = $userid;
            $data['user_type']      = $userType;
            $data['status']         = '1';
            //  $data['email_from']     = $_SESSION[SESSION_DOMAIN]['email'];
            $data['created_at']     = date('Y-m-d H:i:s');
            //    unset($data['email_to'],);
            $sqlData = createSqlColVal($data);

            $query = "INSERT INTO communication_email (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            $last_id = $this->companyConnection->lastInsertId();

            if(isset($_FILES['file_library']))
            {
                $this->insertFileLibrary($_FILES['file_library'],$userid,$last_id);
            }

            return ['status' => 'success', 'code' => 200, 'data' => $data, 'message' => 'Message Sent Successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
        }
    }


    public function getUsersSearch()
    {
        $user_search = $_REQUEST['search'];
        $userTypeQuery = '';

        $getUsers = $this->companyConnection->query("SELECT email FROM users where (name like '%" . $user_search . "%' OR first_name like '%" . $user_search . "%' OR email like '%" . $user_search . "%' OR last_name like '%" . $user_search . "%' ) ")->fetchAll();
        $list = [];
        if(isset($getUsers))
        {
            foreach ($getUsers as $key=>$values)
            {
                $list[] = $values['email'];
            }
        }

        $cars['options'] = $list;
        $cars = (object)$cars;

        return $cars;
    }
    public function insertFileLibrary($files, $userid, $last_id, $validation_check=null)
    {
        $domain = getDomain();
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        $path = "uploads/" . 'apexlink_company_' .$userid . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $uploadPath = ROOT_URL . '/company/' . $path;
        if (isset($files)) {
            $countFiles = count($files['name']);

            if ($countFiles != 0) {

                $i = 0;
                if (isset($files)) {
                    foreach ($files['name'] as $key => $filename) {
                        $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                        if (isset($filename[$i]) && $filename[$i] != "") {
                            $randomNumber = uniqid();
                            $uniqueName = $files['name'][$key];
                            //   $uniqueName = $randomNumber . $files['name'][$key];
                            $data1['email_id'] = $last_id;
                            $data1['filename'] = $uniqueName;
                            $data1['file_location'] = $path . '/' . $uniqueName;

                            $data1['file_type'] = strstr($files['type'][$key], "image/") ? 1 : 2;
                            $data1['file_extension'] = $ext;

                            $query = $this->companyConnection->query("SELECT count(*) as image_counts FROM communication_email_attachments where filename='$uniqueName' and email_id = '$last_id'");
                            $queryData = $query->fetch();

                            $ownerimage_counts = (int)$queryData['image_counts'];

                            if((int)$ownerimage_counts > (int)0)
                            {
                                return array('code' => 500, 'status' => 'warning', 'message' => 'File already exists.');
                            }
                            if(!$validation_check)
                            {
                                $sqlData1 = createSqlColVal($data1);
                                $query1 = "INSERT INTO communication_email_attachments(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                                $stmt1 = $this->companyConnection->prepare($query1);
                                $stmt1->execute($data1);
                                $tmp_name = $files["tmp_name"][$key];
                                if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                    //Directory does not exist, so lets create it.
                                    mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                                }
                                move_uploaded_file($tmp_name, "$uploadPath/$uniqueName");
                            }
                        }
                        $i++;
                    }
                }
            }
        }
        return array('status' => 'success', 'message' => 'Record added successfully.', 'table' => 'tenant_chargefiles');
    }
    public function getUsers()
    {
        $user_type = $_POST['type'];

        $getUsers = $this->companyConnection->query("SELECT id,name,email,carrier,country_code,phone_number,user_type FROM users where user_type='$user_type' ORDER BY name ASC")->fetchAll();

        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
        <th>id</th>
        <th>Name</th>
        <th>Phone</th>
        </tr>";
        if(!$getUsers)
        {
            $html .= "<td colspan=\"3\" align=\"center\"  bgcolor=\"#f7f7f7\">
                                                No Record Found
                                            </td>";
            $html .= "</tr>";
        }
        foreach ($getUsers as $users) {

            $id = $users['id'];
            $name = $users['name'];
            $type = $users['user_type'];
            if(!empty($users['phone_number'])){
                $email = '+'.$users['country_code'].$users['phone_number'];
            }
            else{
                $email="";
            }
            $html .= "<tr>";
            $html .= "<td><input type='checkbox' class='getEmails' data-email='" . $email . "' data-name='" . $name.'>'.$email. "' data-utype='" . $type.'>'.$email . "'></td>";
            $html .= "<td>" . $name . "</td>";
            $html .= "<td>" . $email . "</td>";
            $html .= "</tr>";

        }

        $html .= "</table><br>";
        echo $html;
        exit;

    }

    public function getSearchUsersEmail()
    {
        $email = $_REQUEST['email'];
        $getUsers = $this->companyConnection->query("SELECT id,name,email,carrier,country_code,phone_number,user_type FROM users where email='$email'")->fetch();

        return $getUsers;
    }

}
$composeTextMessageAjax = new composeTextMessageAjax();
?>