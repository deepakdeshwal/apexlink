<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class InTouchList extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }
    public function getIntouchDetail(){
        $table = 'in_touch_detail';
        $columns = [$table . '.*'];
        $joins = [[
            'type' => 'LEFT',
            'table' => 'in_touch_detail',
            'column' => 'assigned_user',
            'primary' => 'id',
            'on_table' => 'users',
            'as' => 'users'
        ],[
            'type' => 'LEFT',
            'table' => 'in_touch_detail',
            'column' => 'assigned_for',
            'primary' => 'id',
            'on_table' => 'users',
            'as' => 'users'
        ], [
            'type' => 'LEFT',
            'table' => 'in_touch_detail',
            'column' => 'type',
            'primary' => 'id',
            'on_table' => 'in_touch_type',
            'as' => 'in_touch_type'
        ],
        ];
        $where = [];
        $query = selectQueryBuilder($columns, $table, $joins, $where);
        $data = $this->companyConnection->query($query)->fetch();
    }
    public function delete() {
        try {
            $id = $_POST['id'];
            $query = "DELETE FROM in_touch_detail WHERE id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            $query1 = "DELETE FROM in_touch_notes WHERE in_touch_id=" . $id;
            $stmt1 = $this->companyConnection->prepare($query1);
            $stmt1->execute();
            return array('status' => 'success', 'code' => 200, 'message' => 'Record Deleted Successfully.');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    public function updateStatus() {
        try {
            $id = $_POST['id'];
            $data['status']=$_POST['status'];
            $sqlData = createSqlUpdateCase($data);
            $query =  "UPDATE in_touch_detail SET " . $sqlData['columnsValuesPair'] . " where id=" .$id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($sqlData['data']);
            return array('status' => 'success', 'code' => 200, 'message' => 'Record added successfully.');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    public function NotesHistory(){
        $id = $_POST['id'];
        $html = '';
        $sql = "SELECT * FROM in_touch_notes WHERE in_touch_id=$id";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $LoginUserName= $_SESSION[SESSION_DOMAIN]['name'];

        foreach ($data as $d) {
            $datacreatedat = (!empty($d['created_at'])) ? dateFormatUser($d['created_at'], null, $this->companyConnection) : '';

            $html.= " <span class='popup-rgt'><span id='spanAssign'>".$LoginUserName." ".$datacreatedat."</span>";
            $html .="<p id='Notess' style='float: left;margin-left: 15px;'></p>";
            $html .="<div style='overflow: auto;max-height: 250px;'>".$d['notes']."</div><p></p></span>";

        }

        return array('data' => $html, 'status' => 'success');
    }


}
$InTouchList = new InTouchList();