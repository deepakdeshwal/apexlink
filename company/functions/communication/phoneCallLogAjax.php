<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;


include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");

if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class phoneCallLogAjax extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }



    public function getDataByID(){
        $id = $_POST['id'];
        $prev_id = $_POST['prev_id'];
        $data_type = $_POST['data_type'];
        $userData = '';
        $table = '';
        $managers_id = [];
        switch ($data_type) {
            case "property":
                $data1=[];
                $data2=[];

                $table = 'building_detail';
                $query = "SELECT id, building_name FROM `$table` WHERE property_id = ".$id;
                $sql1 = "SELECT manager_id FROM  general_property WHERE id = '$id'";
                $data1 = $this->companyConnection->query($sql1)->fetch();
                if(!empty($data1["manager_id"])) {
                    $managers_id = unserialize($data1['manager_id']);
                }
                break;
            case "building":
                $table = 'unit_details';
                $query = "SELECT id, unit_prefix, unit_no FROM `$table` WHERE property_id = ".$prev_id." && building_id = ".$id." && building_unit_status = '1'";
                break;
            case "users":
                $table = 'users';
                $sql = "SELECT id, name FROM `$table` WHERE user_type = '" . $id . "' && status = '1'";
                if ($id == 'all') {
                    if (isset($prev_id) && !empty($prev_id)) {
                        $tenantQuery = "SELECT u.id, u.name FROM users u LEFT JOIN tenant_property t ON u.id=t.user_id WHERE t.property_id=$prev_id";
                        $tenantQueryData = $this->companyConnection->query($tenantQuery)->fetchAll();

                        $ownerQuery = "SELECT  u.id, u.name  FROM users u LEFT JOIN owner_property_owned ON u.id=owner_property_owned.user_id WHERE owner_property_owned.property_id=$prev_id";
                        $ownerQueryData = $this->companyConnection->query($ownerQuery)->fetchAll();

                        $userData = array_merge($ownerQueryData,$tenantQueryData);

                        return array('code' => 200, 'status' => 'success', 'data' => $userData,'message' => 'Data loaded successfully!');
                    } else {
                        $query = "SELECT id, name FROM `$table` WHERE id != '1' && status = '1'";
                    }
                } else if($id == '2'){
                    if (isset($prev_id) && !empty($prev_id)) {
                        $query = "SELECT u.id, u.name  FROM users u LEFT JOIN tenant_property t ON u.id=t.user_id WHERE t.property_id=$prev_id";
                    } else {
                        $query = $sql;
                    }
                } else if($id == '4'){
                    if (isset($prev_id) && !empty($prev_id)) {
                        $query = "SELECT  u.id, u.name  FROM users u LEFT JOIN owner_property_owned o ON u.id=o.user_id WHERE o.property_id=$prev_id";
                    } else {
                        $query = $sql;
                    }
                } else {
                    $query = $sql;
                }
                break;
            default:
                $table = '';
                $query = '';
                break;
        }
        if (!empty($query)) {
            $userData = $this->companyConnection->query($query)->fetchAll();
        }
        return array('code' => 200, 'status' => 'success', 'data' => $userData,'property_manager_id'=> $managers_id,'message' => 'Data loaded successfully!');
    }

    /**
     * @return array
     * @throws Exception
     */
    public function addphoneCallLog() {
        try {

            $edit_id = isset($_POST['edit_phone_call_log_id']) ? $_POST['edit_phone_call_log_id'] : '';
            $data = $_POST;
            //Required variable array
            $required_array = ['caller_name'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $date =  mySqlDateFormat($data['date'], null, $this->companyConnection);
                $queryData['caller_name'] = isset($data['caller_name']) ? $data['caller_name'] : '';
                $queryData['incoming_outgoing']        = isset($data['incoming_outgoing']) ? $data['incoming_outgoing'] : '';
                $queryData['call_for']        = isset($data['call_for']) ? $data['call_for'] : '';
                $queryData['phone_number']            = isset($data['phone_number']) ? $data['phone_number']: '';
                $queryData['due_date']        =   $date;
                $queryData['time']     =  date("H:i", strtotime($data['time']));
                $queryData['call_purpose']     =  isset($data['call_purpose']) ? $data['call_purpose']: '';
                $queryData['action_taken']     = isset($data['action_taken']) ? $data['action_taken'] : '';
                $queryData['follow_up_needed']     = isset($data['follow_up']) ? $data['follow_up'] : '';
                $queryData['call_length']     = isset($data['call_length']) ? $data['call_length'] : '';
                $queryData['call_length']     = isset($data['call_length']) ? $data['call_length'] : '';
                $queryData['call_type_id']     = isset($data['call_type_id']) ? $data['call_type_id'] : '';
                $queryData['notes']     = isset($data['notes']) ? $data['notes'] : '';
                $queryData['status']          = 1;
                $queryData['created_at']      = date('Y-m-d H:i:s');
                $queryData['updated_at']      = date('Y-m-d H:i:s');
                $queryData['user_id']      =  $_SESSION[SESSION_DOMAIN]['cuser_id'];
                if (isset($edit_id) && !empty($edit_id)) {

                    $sqlData = createSqlUpdateCase($queryData);
                    $query = "UPDATE phone_call_logs SET ".$sqlData['columnsValuesPair']." WHERE id=".$edit_id;
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);
                } else {
                    $sqlData = createSqlColVal($queryData);
                    $query = "INSERT INTO phone_call_logs (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($queryData);
                    $last_id = $this->companyConnection->lastInsertId();
                }
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records created successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function getAnnouncementById(){
        try {
            $announcement_id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM announcements WHERE id = '$announcement_id' AND status = '1' ");
            $announcementData = $query->fetch();
            if($announcementData) {
                $announcementData['start_date'] = dateFormatUser($announcementData['start_date'], null, $this->companyConnection);
                $announcementData['end_date'] = dateFormatUser($announcementData['end_date'], null, $this->companyConnection);
                $username =  unserialize($announcementData['user_name']);

                if(is_array($username)) {
                    $announcementData['user_name'] = $username;
                } else {
                    $announcementData['user_name'] = [$username];
                }
                return array('status' => 'success', 'data' => $announcementData, 'code' => 200);
            }
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


    public function updateStatus(){
        try {
            $id = $_POST['id'];
            $status_type = $_POST['status_type'];
            $table = '';
            $message = '';
            switch ($status_type) {
                case "delete_announcement":
                    $data['deleted_at'] = date('Y-m-d H:i:s') ;
                    $sqlData = createSqlColValPair($data);
                    $query1  = "UPDATE announcements SET ".$sqlData['columnsValuesPair']." WHERE id =".$id;
                    $message  = 'Record deleted successfully!';
                    break;
                case "building":
                    $table = 'unit_details';
//                  $query = "SELECT id, unit_prefix, unit_no FROM `$table` WHERE property_id = ".$prev_id." && building_id = ".$id." && building_unit_status = '1'";
                    break;
                default:
                    $query = '';
                    $query1 = '';
                    $message = 'Something went wrong!';
                    break;
            }

            if (!empty($query)) {
                $data = $this->companyConnection->query($query)->fetchAll();
            }
            if (!empty($query1)) {
                $stmt1 = $this->companyConnection->prepare($query1);
                $data = $stmt1->execute();
            }
            return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => $message);
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    /*
     * function for fetch property manageers and admin
     * */
    public function fetchPropertymanagers() {
        $html = '';
        $sql = "SELECT * FROM  users WHERE user_type = '1' AND deleted_at IS NULL order by first_name ASC";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html.= "<option value=''>Select</option>";
        foreach ($data as $d) {
            $fullname = ucfirst($d['first_name']) . ' ' . ucfirst($d['last_name']);
            $html.= '<option value=' . $d['id'] . '>' . $fullname . '</option>';
        }
        return array('data' => $html, 'status' => 'success');
    }

    /*
 * function to get task and reminder by id
 */

    public function getPhoneCallLogById(){
        $data = $_POST;
        try{
            $phonecallLog = getSingleRecord($this->companyConnection ,['column'=>'id','value'=>$data['id']], '	phone_call_logs');
            $phonecallLog["data"]["due_date"] =  (isset($phonecallLog["data"]["due_date"]) && !empty($phonecallLog["data"]["due_date"]))? dateFormatUser($phonecallLog["data"]["due_date"],null,$this->companyConnection):'';
            return array('status' => 'success', 'code'=>'200',  'message' => 'Data fetched successfully.','data'=>$phonecallLog);
        }catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'emergency_details');
            printErrorLog($e->getMessage());
        }
    }

    /*
* function to delete task and reminder by id
*/
    public function deleteCallLog() {
        try {
            $data=[];
            $call_log_id = $_POST['call_log_id'];

            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                //$query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$tenant_id'";
                $query = "DELETE FROM phone_call_logs WHERE id='$call_log_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record deleted successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    public function getUsersData() {
        try {
            $users_data= [];
            $call_log_users_data=[];
            $merged_data =[];
            $search = @$_REQUEST['q'];
            if(empty($search)){
                $sql = "SELECT u.name,u.phone_number,u.email,u.user_type FROM users as u  where (status = '1' AND deleted_at =NULL) OR (user_type = '2' OR user_type ='3' OR user_type ='4' OR user_type ='5' OR user_type ='8') order by name ASC";
                $users_data = $this->companyConnection->query($sql)->fetchAll();
                $sql1 = "SELECT DISTINCT caller_name as name,phone_number FROM phone_call_logs  WHERE status='1' ORDER BY caller_name ASC;";
                $call_log_users_data = $this->companyConnection->query($sql1)->fetchAll();
                $merged_data = array_merge($call_log_users_data,$users_data);
            } else {
               // $sql = "SELECT * FROM general_property  where status = '1' and property_name LIKE '%$search%' order by property_name ASC";
                                $sql = "SELECT u.name,u.phone_number,u.email,u.user_type FROM users as u  where (status = '1' AND deleted_at =NULL ) OR (user_type = '2' OR user_type ='3' OR user_type ='4' OR user_type ='5' OR user_type ='8') AND u.name LIKE '%$search%' order by name ASC";

                $users_data = $this->companyConnection->query($sql)->fetchAll();
                $sql1 = "SELECT DISTINCT caller_name as name,phone_number FROM phone_call_logs  WHERE status='1' AND caller_name LIKE '%$search%' ORDER BY caller_name ASC;";
                $call_log_users_data = $this->companyConnection->query($sql1)->fetchAll();
                $merged_data = array_merge($call_log_users_data,$users_data);
            }
            $users_count = sizeOf($merged_data);


            $data = [];
            if (!empty($merged_data)) {
                foreach ($merged_data as $key => $user_data) {
                    $data['total'] = $users_count;
                    $data['rows'][$key]['id'] = $key+1;
                    $data['rows'][$key]['name'] = $user_data['name'];
                    $data['rows'][$key]['type'] = (isset($user_data["user_type"]) && !empty($user_data["user_type"]))? $this->getusertypedata($user_data["user_type"]):'General';
                    $data['rows'][$key]['email'] = (isset($user_data["email"]) && !empty($user_data["email"]))? $user_data['email'] :'';
                    $data['rows'][$key]['phone_number'] = $user_data['phone_number'];
                }
            }
            echo json_encode($data, JSON_UNESCAPED_SLASHES);
            exit;
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }

    function getusertypedata($user_type){
        switch ($user_type) {
            case '2':
                return "Tenant";
                break;
            case '3':
                return    "Vendor";
                break;
            case '4':
                return    "Owner";
                break;
            case '5':
                return  "Contact";
                break;
            case '8':
                return   "Employee";
                break;
            default:
                return  "General";
        }

    }

    /*
 * function for fetch property manageers and admin
 * */
    public function getTodayPhoneCallLog() {

        $html = '';
        $current_date = date("Y-m-d");

        $next_date = $date = date('Y-m-d', strtotime("+1 day"));
        $sql =  "SELECT  phone_call_logs.id,  phone_call_logs.caller_name, phone_call_logs.phone_number, phone_call_logs.due_date, phone_call_logs.time, call_type.call_type, phone_call_logs.call_length, phone_call_logs.incoming_outgoing, phone_call_logs.call_purpose, phone_call_logs.action_taken, phone_call_logs.follow_up_needed FROM phone_call_logs LEFT JOIN call_type ON phone_call_logs.call_type_id=call_type.id WHERE ( phone_call_logs.deleted_at IS NULL AND phone_call_logs.status = '1' AND phone_call_logs.created_at >='".$current_date."' AND phone_call_logs.created_at <'".$next_date."')";
       // $sql = "SELECT * FROM  phone_call_logs WHERE user_type = '1' AND deleted_at IS NULL order by first_name ASC";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $current_date = $_SESSION[SESSION_DOMAIN]["formated_date"];

        $html.= '<div id="TestPhoneHeight" class="" style=" max-width: 1348px; ">
                                                    <div class="div-full3">
                                                        <div class="div-full3">
                                                            <div id="PrintPhone" style="page-break-after: always;">



<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<style type="text/css">
	body{ margin: 0; padding:0;  }
</style>
	<table cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">
		<tbody><tr>
			<td style="height: 11px; background: #00b0f0;"> </td>
		</tr>
		<tr>
						<td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; "><img src="'.COMPANY_SITE_URL.'/images/logo.png" width="150" height="50" alt=""></td>
		</tr>
		<tr>
			<td style="background: #00b0f0; height: 15px;"> </td>
		</tr>
		<tr>
			<td style="padding: 20px; background: #fff; padding: 30px;">
				<table style="width: 100%; padding: 0 0 20px 0">
					<tbody><tr>
						<td width="33.33%"></td>
						<td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: center; ">Phone Call Log</td>
						<td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: right; ">'.$current_date.'</td>
					</tr>
					<tr>
						<td colspan="2" height="50px"></td>
					</tr>
				</tbody></table>
				<table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0">
					<tbody><tr>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858;  font-weight: bold; font-family: arial;  font-size: 16px;">Name</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Phone</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Date</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Call type</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Length of call</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Call Category</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Purpose of Call</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Action Taken</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Follow Up Needed</th>
					</tr>';
        if(!empty($data)) {
//           dd($data);
            //dd(SESSION_DOMAIN);
            foreach ($data as $key => $calllogdata) {
                $due_date= '';
                $call_category = '';
                $follow_up = '';
                $due_date = (isset($calllogdata["due_date"]) && !empty($calllogdata["due_date"]))? dateFormatUser($calllogdata["due_date"],null,$this->companyConnection): '';

                if($calllogdata["incoming_outgoing"] == 1){
                    $call_category = "Incoming";
                }else{
                    $call_category = "Outgoing";
                }

                if($calllogdata["follow_up_needed"] == 1){
                    $follow_up = 'No';
                }else{
                    $follow_up = 'Yes';
                }

                $html .= '<tr><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["caller_name"].'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["phone_number"].'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$due_date .'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["call_type"]. '</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["call_length"].'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$call_category.'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["call_purpose"].'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["action_taken"].'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$follow_up.'</td></tr>';
            }
            }
        $html.='</tbody></table>

			</td>
		</tr>
		<tr>
			<td style="height: 11px; background: #00b0f0;"> </td>
		</tr>
	</tbody></table>

</div>
                                                        </div>
                                                        <div class="btn-outer3" style="padding-top: 2%;">
                                                        </div>
                                                    </div>
                                                    <!--/ END panel -->
                                                </div>';
//        foreach ($data as $d) {
//            $fullname = ucfirst($d['first_name']) . ' ' . ucfirst($d['last_name']);
//            $html.= '<option value=' . $d['id'] . '>' . $fullname . '</option>';
//        }
        return array('data' => $html, 'status' => 'success','code' => 200);
    }


    /*
* function for fetch property manageers and admin
* */
    public function getEmailUsers() {
        $html = '';

        $sql =  "SELECT users.id,users.email,users.name FROM users WHERE deleted_at IS NULL  ";

        $data = $this->companyConnection->query($sql)->fetchAll();
        $html.= '  <div class="grid-outer"  style="max-height: 200px; overflow: auto;">
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody>';

                foreach($data as $key => $users_data) {
                    $html .= '<tr>
                            <td><input type="checkbox" name="user_email_checkbox[]" data-id="'.$users_data["id"].'" data-email="'.$users_data["email"].'" data-name="'.$users_data["name"].'" class="user_email_checkbox"/> </td>
                            <td>'.ucfirst($users_data["name"]).'</td>
                            <td>'.$users_data["email"].'</td>
                        </tr>';
                }
        $html .= '</tbody>
                </table>
            </div>
                <div class="btn-outer text-center">
                    <button class="blue-btn ok_send_email" type="button">OK</button>
                </div>';
        return array('data' => $html, 'status' => 'success','code' => 200);
    }


    /**
     * Send mail to the user for OTP
     * @param $userData
     * @param $OTP
     * @return array|mixed|string
     */
    public function sendPhoneCallMail()
    {
        try{
            $data = $_POST;
            $new_array = [];
            $new_array =  array_combine($data["names"],$data["emails"]);
            $logo = SUBDOMAIN_URL."/company/images/logo.png";
//            $phone_call_log_html = $this->getTodayPhoneCallLog();
//            $pdf = $this->createHTMLToPDF($phone_call_log_html["data"]);
            $body = file_get_contents(COMPANY_DIRECTORY_URL.'/views/Emails/phone_call_log.php');
            if(!empty($new_array)) {
                foreach($new_array as $key => $user) {
                    $body = str_replace("#user_name#", $key, $body);
                    $new_date = date("d M Y ");
                    $body = str_replace("#date#",$new_date,$body);
                    $request['to'][] = $user;
                    $request['action'] = 'SendMailPhp';
                    $request['subject'] = 'Phone Call Log';
                    $request['portal'] = '1';
                    $request['message'] = $body;
                    $response = curlRequest($request);
//                    return $response;
                }
                return array('code' => 200, 'status' => 'success','message' => "Email sent successfully");
            }

        }catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


    public function createHTMLToPDF($replaceText){
        $dompdf = new Dompdf();
        $dompdf->loadHtml($replaceText);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $path = "uploads/";
        $fileUrl = COMPANY_DIRECTORY_URL . '/'.$path.'/calllogdata.pdf';
        $output = $dompdf->output();
        file_put_contents($fileUrl, $output);
        chmod($fileUrl, 0777);
        $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/'.$path.'/calllogdata.pdf';
        return array('code' => 200, 'status' => 'success', 'data' => $fileUrl2,  'message' => 'Record retrieved successfully');
    }




}






$phoneCallLogAjax = new phoneCallLogAjax();