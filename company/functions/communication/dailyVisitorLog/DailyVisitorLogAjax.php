<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */
ini_set('memory_limit', '1024M'); // or you could use 1G
include(ROOT_URL . "/config.php");
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};

class DailyVisitorLogAjax extends DBConnection{
    /**
     * DailyVisitorLogAjax constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function to get reasons
     */
    public function getReasons(){
        try{
            $reasonsData = $this->companyConnection->query('SELECT id,reason FROM reasons')->fetchAll();
            if(!empty($reasonsData)){
                $data = array('code' => 200, 'status' => 'success','data'=>$reasonsData);
            } else {
                $data = array('code' => 400, 'status' => 'error','data'=>$reasonsData);
            }
            return $data;
        } catch(PDOException $e){
            dd($e);
        }
    }

    /**
     * function to get reasons
     */
    public function saveReasonsData(){
        try{
            $data = $_POST['reasons'];
            $dataNew['reason'] = $_POST['reasons'];
            $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
            //check if custom field already exists or not
            $duplicate = checkNameAlreadyExists($this->companyConnection,'reasons','reason',$dataNew['reason'],$id);
            if ($duplicate['is_exists'] == 1) {
                if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                    return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Reason already exists!');
                }
            }
            /*Save Data in Company Database*/
            $sqlData = createSqlColVal($dataNew);
            $query = "INSERT INTO reasons (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($dataNew);
            $id = $this->companyConnection->lastInsertId();
            $msg = 'Record added successfully';
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => $msg,'inserted_id'=>$id);
        } catch(PDOException $e){
            dd($e);
        }
    }

    /**
     * function to save visitor log
     */
    public function saveVisitorLog(){
        try{
            $data = $_POST['form'];
            $data = postArray($data);
            /*Required variable array*/
            $required_array = ['visitor'];
            $maxlength_array = [];
            $number_array = [];
            /*Server side validation*/
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $id = null;
                if(isset($data['edit_id'])){
                    $id = $data['edit_id'];
                    unset($data['edit_id']);
                }
                $data['date'] = !empty($data['date'])?mySqlDateFormat($data['date'],null,$this->companyConnection):null;
                $data['time_in'] = !empty($data['time_in'])?mySqlTimeFormat($data['time_in']):null;
                $data['time_out'] = !empty($data['time_out'])?mySqlTimeFormat($data['time_out']):null;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                if(empty($id)) {
                    /*Save Data in Company Database*/
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO daily_visitor_log (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $msg = 'Record added successfully';
                } else {
                    $sqlData = createSqlUpdateCase($data);
                    $query = "UPDATE daily_visitor_log SET " . $sqlData['columnsValuesPair'] . " where id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);
                    $msg = 'Record updated successfully';
                }
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => $msg);
            }
        } catch (PDOException $e) {
            dd($e);
        }
    }

    /**
     * function to get time sheet employee
     * @return array
     */
    public function getVisitorDetail(){
        try {
            $edit_id = $_POST['id'];
            $data = $this->companyConnection->query('SELECT * FROM daily_visitor_log WHERE id='.$edit_id)->fetch();
            if (!empty($data)) {
                $data['date'] = !empty($data['date'])?dateFormatUser($data['date'],null,$this->companyConnection):'';
                $data['time_in'] = !empty($data['time_in'])?date("g:i A", strtotime($data['time_in'])):'';
                $data['time_out'] = !empty($data['time_out'])?date("g:i A", strtotime($data['time_out'])):'';
                $data = array('code' => 200, 'status' => 'success', 'data' => $data);
            } else {
                $data = array('code' => 400, 'status' => 'error');
            }
            return $data;
        } catch(PDOException $e){
            dd($e);
        }
    }

    /**
     * Delete TimeSheet
     * @return array
     */
    public function delete(){
        try{
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE daily_visitor_log SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$id]);
            return ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function to show all users
     * @return array
     */
    public function getUsersData() {
        try {
            $search = @$_REQUEST['q'];
            if(empty($search)){
                $sql = "SELECT id,user_type,name,address1,address2,address3,address4,email,phone_number,company_name FROM users where user_type IN ('2','3','4','5') AND status = '1' order by user_type DESC";
            } else {
                $sql = "SELECT id,user_type,name,address1,address2,address3,address4,email,phone_number,company_name FROM users where user_type IN ('2','3','4','5') AND status = '1' and name LIKE '%$search%' order by user_type DESC";
            }
            $users_data = $this->companyConnection->query($sql)->fetchAll();
            $users_count = sizeOf($users_data);

            $data = [];
            if (!empty($users_data)) {

                foreach ($users_data as $key => $user_data) {
                    $data['total'] = $users_count;
                    $data['rows'][$key]['id'] = $user_data['id'];
                    $data['rows'][$key]['name'] = $user_data['name'];
                    $data['rows'][$key]['email'] = $user_data['email'];
                    $data['rows'][$key]['phone_number'] = $user_data['phone_number'];
                    $data['rows'][$key]['company_name'] = $user_data['company_name'];
                    $data['rows'][$key]['date'] = dateFormatUser(date('Y-m-d'),null,$this->companyConnection);
                    $data['rows'][$key]['address1'] = $user_data['address1'] . ', ' . $user_data['address2'] . ' ' . $user_data['address3'] . ' ' . $user_data['address4'];
                    if($user_data['user_type'] == '2'){
                        $data['rows'][$key]['user_type'] = 'Tenant';
                    } else if($user_data['user_type'] == '3'){
                        $data['rows'][$key]['user_type'] = 'Vendor';
                    } else if($user_data['user_type'] == '4'){
                        $data['rows'][$key]['user_type'] = 'Owner';
                    } else if($user_data['user_type'] == '5'){
                        $data['rows'][$key]['user_type'] = 'Contact';
                    }
                    $data['rows'][$key]['user_type'] = str_replace_last(',', '', $data['rows'][$key]['user_type']);
                }
            }
            echo json_encode($data, JSON_UNESCAPED_SLASHES);
            exit;
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }

    /**
     * function to get daily visitor log data
     * @return array
     */
    public function getDailyVisitorLog() {
        $id = $_POST['id'];
        $html = '';
        $current_date = date("Y-m-d");
        $company_logo = SITE_URL."company/images/logo.png";
        $next_date = $date = date('Y-m-d', strtotime("+1 day"));
        $sql =  "SELECT  daily_visitor_log.id,  daily_visitor_log.visitor, daily_visitor_log.company, daily_visitor_log.phone, daily_visitor_log.date, daily_visitor_log.time_in, daily_visitor_log.time_out, daily_visitor_log.reason_for_visit, daily_visitor_log.length_of_visit, daily_visitor_log.action_taken, daily_visitor_log.follow_up, daily_visitor_log.note FROM daily_visitor_log WHERE ( daily_visitor_log.deleted_at IS NULL AND id=".$id.")";
        // $sql = "SELECT * FROM  phone_call_logs WHERE user_type = '1' AND deleted_at IS NULL order by first_name ASC";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $current_date = $_SESSION[SESSION_DOMAIN]["formated_date"];
        $html.= '<div id="TestPhoneHeight" class="" style=" max-width: 1348px; ">
                                                    <div class="div-full3">
                                                        <div class="div-full3">
                                                            <div id="PrintPhone" style="page-break-after: always;">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<style type="text/css">
	body{ margin: 0; padding:0;  }
</style>
	<table cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">
		<tbody><tr>
			<td style="height: 11px; background: #00b0f0;"> </td>
		</tr>
		<tr>
			<td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">
			<img width="200" src="'.$company_logo.'"/>
			</td>
		</tr>
		<tr>
			<td style="background: #00b0f0; height: 15px;"> </td>
		</tr>
		<tr>
			<td style="padding: 20px; background: #fff; padding: 30px;">
				<table style="width: 100%; padding: 0 0 20px 0">
					<tbody><tr>
						<td width="33.33%"></td>
						<td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: center; ">Daily Visitor</td>
						<td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: right; ">'.$current_date.'</td>
					</tr>
					<tr>
						<td colspan="2" height="50px"></td>
					</tr>
				</tbody></table>
				<table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0">
					<tbody><tr>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858;  font-weight: bold; font-family: arial;  font-size: 16px;">VisitorName</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">PhoneNumber</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Date</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Reason for Visit</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Length of Visit</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Time In</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Time Out</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Action Taken</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Follow Up Needed</th>
					</tr>';
        if(!empty($data)) {
//           dd($data);
            //dd(SESSION_DOMAIN);
            foreach ($data as $key => $calllogdata) {
                $due_date= '';
                $call_category = '';
                $follow_up = '';
                $due_date = (isset($calllogdata["date"]) && !empty($calllogdata["date"]))? dateFormatUser($calllogdata["date"],null,$this->companyConnection): '';
                $time_in = !empty($calllogdata['time_in'])? timeFormat($calllogdata["time_in"], null, $this->companyConnection):'';
                $time_out = !empty($calllogdata['time_out'])? timeFormat($calllogdata["time_out"], null, $this->companyConnection):'';
                if($calllogdata["follow_up"] == 0){
                    $follow_up = 'No';
                }else{
                    $follow_up = 'Yes';
                }

                $html .= '<tr><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["visitor"].'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["phone"].'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$due_date .'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["reason_for_visit"]. '</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["length_of_visit"].'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$time_in.'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$time_out.'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["action_taken"].'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$follow_up.'</td></tr>';
            }
        }
        $html.='</tbody></table>;

			</td>
		</tr>
		<tr>
			<td style="height: 11px; background: #00b0f0;"> </td>
		</tr>
	</tbody></table>

</div>
                                                        </div>
                                                        <div class="btn-outer3" style="padding-top: 2%;">
                                                        </div>
                                                    </div>
                                                    <!--/ END panel -->
                                                </div>';
        return array('data' => $html, 'status' => 'success','code' => 200);
    }

    /**
     * function to export excel.
     */
    public function exportExcel(){
        try{
            include(ROOT_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
            $objPHPExcel = new PHPExcel();
            $table = $_REQUEST['table'];
            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
//        $query1 = "SELECT * FROM $table";
            $columns = [$table . '.*','reasons.reason'];
            $where = [['table' => $table, 'column' => 'deleted_at', 'condition' => 'IS NULL', 'value' =>'']];
            $joins = [
                [
                    'type'      =>   'LEFT',
                    'table'     =>   'daily_visitor_log',
                    'column'    =>   'reason_for_visit',
                    'primary'   =>   'id',
                    'on_table'  =>   'reasons',
                    'as'        =>   'reasons'
                ]
            ];
            $orderBy = ['column'=>'visitor','sort'=>'ASC'];
            $query = selectQueryBuilder($columns, $table, $joins, $where,$orderBy);
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            //Set header with temp array
            $tmparray = array("VisitorName", "PhoneNumber", "Date", "Reason for Visit", "Length of Visit", "Time In", "Time Out", "Action Taken", "Follow Up Needed");
            //take new main array and set header array in it.
            $sheet = array($tmparray);
            while ($property_detail_data = $stmt->fetch(PDO::FETCH_ASSOC)) {
//            dd($property_detail_data);
                $tmparray = array();
                $property_name = $property_detail_data['visitor'];
                array_push($tmparray, $property_name);
                $LegalName = $property_detail_data['phone'];
                array_push($tmparray, $LegalName);
                $PortfolioName = $property_detail_data['date'];
                array_push($tmparray, $PortfolioName);
                $ZipCode = $property_detail_data['time_in'];
                array_push($tmparray, $ZipCode);
                $Country = $property_detail_data['time_out'];
                array_push($tmparray, $Country);
                $reasonForVisit = $property_detail_data['reason'];
                array_push($tmparray, $reasonForVisit);
                $length0fVisit = $property_detail_data['length_of_visit'];
                array_push($tmparray, $length0fVisit);
                $actionTaken = $property_detail_data['action_taken'];
                array_push($tmparray, $actionTaken);
                $followUp = $property_detail_data['follow_up'] == '1'?'Yes':'No';
                array_push($tmparray, $followUp);
                array_push($sheet, $tmparray);
            }
            $worksheet = $objPHPExcel->getActiveSheet();
            foreach ($sheet as $row => $columns) {
                foreach ($columns as $column => $data) {
                    $worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
                }
            }
            //make first row bold
            $objPHPExcel->getActiveSheet()->getStyle("A1:B1:C1:D1:E1:F1:G1:H1:I1")->getFont()->setBold(true);
            $objPHPExcel->setActiveSheetIndex(0);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            if (ob_get_contents()) ob_end_clean();
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="dailyVisitorExcel.xlsx"');
            $objWriter->save('php://output');
            die();
        } catch(Exception $e){
            dd($e);
        }
    }

    /**
     * function to create daily log pdf
     */
    public function createDailyLogPdf(){
        try{
            $html = '';
            $current_date = date("Y-m-d");
            $next_date = $date = date('Y-m-d', strtotime("+1 day"));
            $sql =  "SELECT  daily_visitor_log.id,  daily_visitor_log.visitor, daily_visitor_log.company, daily_visitor_log.phone, daily_visitor_log.date, daily_visitor_log.time_in, daily_visitor_log.time_out, daily_visitor_log.reason_for_visit, daily_visitor_log.length_of_visit, daily_visitor_log.action_taken, daily_visitor_log.follow_up, daily_visitor_log.note FROM daily_visitor_log WHERE ( daily_visitor_log.deleted_at IS NULL)";
            $data = $this->companyConnection->query($sql)->fetchAll();
            $html = $this->createPdfHtml($data);
            $this->createHTMLToPDF($html);
        } catch(Exception $exception){
            dd($exception);
        }
    }

    /**
     * @param $replaceText
     * @return array
     */
    public function createHTMLToPDF($replaceText){
        // ini_set('memory_limit','512');
        //define( 'WP_MAX_MEMORY_LIMIT', '256M' );
        $dompdf = new Dompdf();
        $dompdf->loadHtml($replaceText);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $domain = getDomain();
        $path = "upload/";
        $fileUrl = COMPANY_DIRECTORY_URL . '/'.$path.'dailyVisitor.pdf';
        //dd($fileUrl);
        $output = $dompdf->output();
        file_put_contents($fileUrl, $output);
        $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/'.$path.'dailyVisitor.pdf';
        echo json_encode(array('code' => 200, 'status' => 'success', 'data' => $fileUrl2,  'message' => 'Record retrieved successfully'));
        die();
    }


    public function createPdfHtml($data){
        $current_date = date("Y-m-d");
        $rowData = '';
        if(!empty($data)) {
            foreach ($data as $key => $calllogdata) {
                $due_date = (isset($calllogdata["date"]) && !empty($calllogdata["date"]))? dateFormatUser($calllogdata["date"],null,$this->companyConnection): '';
                $time_in = !empty($calllogdata['time_in'])? timeFormat($calllogdata["time_in"], null, $this->companyConnection):'';
                $time_out = !empty($calllogdata['time_out'])? timeFormat($calllogdata["time_out"], null, $this->companyConnection):'';
                if($calllogdata["follow_up"] == 0){
                    $follow_up = 'No';
                }else{
                    $follow_up = 'Yes';
                }

                $rowData .= '<tr><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["visitor"].'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["phone"].'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$due_date .'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["reason_for_visit"]. '</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["length_of_visit"].'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$time_in.'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$time_out.'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["action_taken"].'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$follow_up.'</td></tr>';
            }
        }
        $html = '<html>
                     <head>
                         <title></title>
                     </head>
                     <body>
                         <div style="background-color: #fff">
                             <div style="background-color: #40c3f5">
                                 <div style="background-color: #40c3f5; margin: 20px;">
                                     <table cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">
                                        <tbody>
                                            <tr>
                                                <td style="padding: 20px; background: #fff; padding: 30px;">
                                                     <table style="width: 100%; padding: 0 0 20px 0">
                                                         <tbody>
                                                             <tr>
                                                                 <td width="33.33%"></td>
                                                                 <td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: center; ">Daily Visitor</td>
                                                                 <td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: right; ">'.$current_date.'</td>
                                                             </tr>
                                                             <tr>
                                                                 <td colspan="2" height="50px"></td>
                                                             </tr>
                                                         </tbody>
                                                     </table>
                                                     <table border="0" style="background-color: #fff;font-size: 12px; width: 100%;">
                                                         <thead>
                                                             <tr>
                                                                <th style="border:2px solid #e9e9e9; padding: 5px; color: #585858;  font-weight: bold; font-family: arial;  font-size: 16px;">VisitorName</th>
                                                                <th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">PhoneNumber</th>
                                                                <th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Date</th>
                                                                <th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Reason for Visit</th>
                                                                <th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Length of Visit</th>
                                                                <th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Time In</th>
                                                                <th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Time Out</th>
                                                                <th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Action Taken</th>
                                                                <th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Follow Up Needed</th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>'.$rowData.'</tbody>
                                                     </table>
                                                </td>
                                            </tr>
                                         </tbody>
                                     </table>
                                 </div>
                             </div>
                         </div>
                     </body>
                </html>';
        return $html;
    }

    public function calculateTimeDiff(){
        try{
            $time_in = $_POST['time_in'];
            $time_out = $_POST['time_out'];

            if(strtotime($time_in) > strtotime($time_out)){
                return array('data' => 'Time Out should be greater than Time In!', 'status' => 'warning','code' => 400);
            }
            $diffrence_one = strtotime($time_out) - strtotime($time_in);
            $total = date('H:i',$diffrence_one);
            return array('data' => $total, 'status' => 'success','code' => 200);
        } catch(Exception $exception){
            dd($exception);
        }
    }

}
$announcementAjax = new DailyVisitorLogAjax();